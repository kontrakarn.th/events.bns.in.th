import React from 'react';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled, { keyframes } from 'styled-components';

import { onAccountLogout } from './../../actions/AccountActions';
import { setValues } from './redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalSelectClass from '../../features/modals/ModalSelectClass';
import ModalPreview from '../../features/modals/ModalPreview';

import Menu from '../menu/Menu.js';
import {Imglist} from './../../constants/Import_Images';

class Main extends React.Component {
    actConfirm(){
        this.apiAcceptPreOrder()
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
  apiCheckin(){
        this.props.setValues({
            modal_open: "loading",
        });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content,
                    modal_open: "",
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_message: data.message,
                    modal_open: "message_nochar",
                });
                
            }else if(data.type=='no_game_info'){
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message_nochar",
                });
                
            }else {
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiAcceptPreOrder(){
        this.props.setValues({
            modal_open: "loading",
        });
        let dataSend = {type:'purchase_package', package_id: this.props.package_id, weapon_id: this.props.weapon_id };
        // console.log('dataSend:',dataSend)
        let successCallback = (data) => {
            if (data.status) {
                // console.log('data.content:',data.content);
                this.props.setValues({
                    ...data.content,
                    modal_message: data.message,
                    modal_open: "message",
                    package_id: -1,
                    weapon_id: -1,
                });
            }else{
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                    package_id: -1,
                    weapon_id: -1,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_message: data.message,
                    modal_open: "message",
                    package_id: -1,
                    weapon_id: -1,
                });
            }else {
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                    package_id: -1,
                    weapon_id: -1,
                });
            }
        };
        apiPost("REACT_APP_API_POST_PURCHASE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========

    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            modalSelectCharacter: true,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: ''
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }

    setWeaponName(weaponId){
        // console.log('weapon_id:',weaponId);
        switch(weaponId){
            case '1':
                return 'เบลดมาสเตอร์';
                break;

            case '2':
                return 'กังฟูมาสเตอร์';
                break;

            case '3':
                return 'ฟอร์ซมาสเตอร์';
                break;

            case '4':
                return 'โซลกันเนอร์';
                break;

            case '5':
                return 'เดสทรอยเยอร์';
                break;

            case '6':
                return 'ซัมมอนเนอร์';
                break;

            case '7':
                return 'แอสซาซิน';
                break;

            case '8':
                return 'เบลดแดนเซอร์';
                break;

            case '9':
                return 'วอร์ลอค';
                break;

            case '10':
                return 'โซลไฟต์เตอร์';
                break;

            case '11':
                return 'วอร์เดน';
                break;

            case '12':
                return 'อาร์เชอร์';
                break;

            default:
                return '';
                break;
        }

        // return '';
    }

    selectConfirmClass=(e)=>{
        // console.log('weapon_id:',this.props.weapon_id);
        let weaponId =  this.props.weapon_id;
        if(weaponId == -1){
            this.props.setValues({modal_open: 'message' ,modal_message:'กรุณาเลือกอาชีพ'});
        }else if(this.props.package_id == 1 || this.props.package_id == 2){

            let packageId = this.props.package_id;
            let messageShow = '';

            if(packageId=== 1){

                messageShow = 'แพ็คเกจสนับสนุนน้องใหม่ ขั้นเทพ<br />อาวุธ: '+this.setWeaponName(weaponId)+'<br />250,000 ไดมอนด์';

            }else if(packageId=== 2){

                messageShow = 'แพ็คเกจสนับสนุนน้องใหม่<br />อาวุธ: '+this.setWeaponName(weaponId)+'<br />60,000 ไดมอนด์';

            }

            this.props.setValues({
                modal_open: 'confirm',
                modal_message: messageShow,
                // weapon_id: weaponId
            })
        }
    }

    onConfirmBuyPackage(packageId,canPurchase,alreadyPurchase){
        if(canPurchase == true && alreadyPurchase == false){
            this.props.setValues({modal_open:'selectclass', package_id: packageId})
        }
    }

    render() {
        return (
            <MainWrapper>
                <div className='previewbtn' onClick={()=>this.props.setValues({modal_open:'preview'})}>
                    <img src={Imglist['zoom']} alt=""/>
                </div>
                <BuyBtn className='buy1' status={this.props.can_purchase && this.props.can_purchase.package_1 == true} already_purchase={this.props.already_purchase && this.props.already_purchase.package_1 == true} onClick={()=>this.onConfirmBuyPackage(1,this.props.can_purchase.package_1,this.props.already_purchase.package_1)} />
                <BuyBtn className='buy2' status={this.props.can_purchase && this.props.can_purchase.package_2 == true} already_purchase={this.props.already_purchase && this.props.already_purchase.package_2 == true} onClick={()=>this.onConfirmBuyPackage(2,this.props.can_purchase.package_2,this.props.already_purchase.package_2)} />
                <ModalSelectClass 
                    actConfirm={this.selectConfirmClass.bind(this)}
                />
                <ModalLoading />
                <ModalPreview/>
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
            </MainWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const MainWrapper = styled.div`
    display: block;
    margin: 0 auto;
    background: top center no-repeat url(${Imglist['bg']});
    height: 4106px;
    .previewbtn{
        position: absolute;
        top: 1950px;
        right: 115px;
        cursor:pointer;
        >img{
            display: block;
            margin: 0 auto;
        }
    }
`

const BuyBtn = styled.div`
    background: top center no-repeat url(${Imglist['buy_btn']});
    cursor: pointer;
    width: 310px;
    height: 107px;
    pointer-events: none;
    filter: grayscale(1) brightness(0.8);
    position: absolute;
    left: 50%;
    transform: translate3d(-50%,0,0);
    ${props=> props.status && `
        filter: none;
        pointer-events: visible;
        background: top center no-repeat url(${Imglist['buy_btn']});
    `}
    ${props=> props.already_purchase && `
        background: top center no-repeat url(${Imglist['disable_btn']});
    `}
    &:hover{
        background-position: bottom center;
    }
    &.buy1{
        top: 2312px;
    }
    &.buy2{
        top: 3840px;
    }

`
