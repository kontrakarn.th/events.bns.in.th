
const initialState = {

    username: '',
    message: "",
    diamonds : 0,
    package_id: -1,
    weapon_id: -1,

    modal_open: "none",
    modal_message: "ยืนยันการสั่งซื้อ",
    modal_type: 'alert',
    diamonds : 300000,
    modal_detail: "",
    selectClassId: -1,
    modal_item: {
        "icon": 0,
        "name": "",
    },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return {
          ...state,
          [action.key]: action.value
        };
      } else {
        return state;
      }
    case "SET_VALUES": return { ...state, ...action.value };
    default:
      return state;
  }
};

export const setValue  = (key, value) =>{ return ({ type: "SET_VALUE", value, key }) };
export const setValues = (value) =>{ return ({ type: "SET_VALUES", value }) };
