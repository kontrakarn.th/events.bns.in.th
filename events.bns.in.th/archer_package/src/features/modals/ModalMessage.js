import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../main/redux';

import confirm_btn from './images/confirm_btn.png';
import modal_bg from './images/modal_bg.png';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="message"
            actClickOutside={false}
            // actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <div className="contentwrap">
                    <div className='message' dangerouslySetInnerHTML={{__html: modal_message}} />
                    <Btns onClick={()=>props.setValues({modal_open:'', package_id: -1, weapon_id: -1})} />
                </div>
            </ModalMessageContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.EventReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 914px;
    height: 513px;
    color: #ffffff;
    background-size: 100% 100%;
    background: top center no-repeat url(${modal_bg});
    padding: 7%;
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    align-items: center;
    .contentwrap{
        width: 100%;
        margin: 0 auto;
    }
    .message{
        width: 80%;
        margin: 0 auto;
        font-size 2.5em;
        text-align: center;
        font-family: 'Kanit-Medium';
        overflow-wrap: break-word;
        color: #65431d;
    }
`;

const Btns = styled.div`
    display: block;
    margin: 30px auto 0;
    width: 183px;
    height: 98px;
    background:top center no-repeat url(${confirm_btn});
    position:relative;
    text-align: center;
    cursor: pointer;
    &:hover{
        background-position: bottom center;
    }
`;
