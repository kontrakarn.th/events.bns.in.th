import React from 'react';
import {connect} from 'react-redux';
import ModalCore from './ModalCore';
import { setValues } from './../main/redux';
import iconDropdown from '../../static/images/icon_dropdown.png'
import { apiPost } from './../../middlewares/Api';
import styled, { keyframes } from 'styled-components';
import cancel_btn from './images/cancel_btn.png';
import confirm_btn from './images/confirm_btn.png';
import modal_bg from './images/modal_bg.png';
import dropdown_bg from '../../static/images/event/dropdown_bg.png';

const CPN = props => {
    return (
        <ModalCore
            modalName="selectclass"
            actClickOutside={false}
        >
            <ContentWrap>
                    <div className="wrap">
                        <div className="title">กรุณาเลือกอาชีพ</div>
                                <select id="characterclass_form" className="selectlist" onChange={(e)=>props.setValues({weapon_id:e.target.value})} value={props.weapon_id}>
                                    <option value="-1"> -- เลือกอาชีพ --</option>
                                    <option value="12">อาร์เชอร์</option>
                                    <option value="1">เบลดมาสเตอร์</option>
                                    <option value="2">กังฟูมาสเตอร์</option>
                                    <option value="3">ฟอร์ซมาสเตอร์</option>
                                    <option value="4">โซลกันเนอร์</option>
                                    <option value="5">เดสทรอยเยอร์</option>
                                    <option value="6">ซัมมอนเนอร์</option>
                                    <option value="7">แอสซาซิน</option>
                                    <option value="8">เบลดแดนเซอร์</option>
                                    <option value="9">วอร์ลอค</option>
                                    <option value="10">โซลไฟต์เตอร์</option>
                                    <option value="11">วอร์เดน</option>
                                </select>
                            <ModalBottom>
                                <Btns onClick={()=>{if(props.actConfirm) props.actConfirm()}} />
                                <Btns className={'cancel'} onClick={()=>props.setValues({modal_open:"", package_id: -1, weapon_id: -1})} />
                            </ModalBottom>
                    </div>
            </ContentWrap>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.layout,...state.EventReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ContentWrap = styled.div`
    position: relative;
    display: block;
    width: 914px;
    height: 513px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${modal_bg});
    display:flex;
    justify-content: space-around;
    align-items: center;
    padding: 7%;
    box-sizing: border-box;
    .wrap{
        display: block;
        width: 100%;
        margin: 0 auto;
    }
    .title{
        width: 80%;
        margin: 0 auto;
        font-size 50px;
        text-align: center;
        font-family: 'Kanit-Medium';
        overflow-wrap: break-word;
        color: #65431d;
    }
    .selectlist {
        background: top center no-repeat url(${dropdown_bg});
        background-size: 100% 100%;
        width: 450px;
        height: 88px;
        -webkit-appearance: none;
        display: block;
        margin: 2% auto;
        font-size: 40px;
        padding-left: 5%;
        text-transform: uppercase;
        font-weight: bold;
        option{
            text-align: center;
            color: #000000;
        }
        &:focus {
            outline: 0 none;
        }
    }
    .icondrop{
        position: absolute;
        top: 20px;
        right: 20px;
    }
`
const ModalBottom = styled.div`
    position: relative;
    display: block;

`;
const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 183px;
    height: 98px;
    background:top center no-repeat url(${confirm_btn});
    position:relative;
    text-align: center;
    box-sizing: border-box;
    cursor: pointer;
    &:hover{
        background-position: bottom center;
    }
    &.cancel{
        background:top center no-repeat url(${cancel_btn});
    }
`;