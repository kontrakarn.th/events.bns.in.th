import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../main/redux';

import cancel_btn from './images/cancel_btn.png';
import confirm_btn from './images/confirm_btn.png';
import modal_bg from './images/modal_bg.png';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="confirm"
            // actClickOutside={()=>props.setValues({modal_open:""})}
            actClickOutside={false}
        >
            <ModalMessageContent>
                    <div className='message' dangerouslySetInnerHTML={{__html: modal_message}} />
                    <ModalBottom>
                        <Btns onClick={()=>{if(props.actConfirm) props.actConfirm()}} />
                        <Btns className={'cancel'} onClick={()=>props.setValues({modal_open:"", package_id: -1, weapon_id: -1})} />
                    </ModalBottom>
            </ModalMessageContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.EventReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 914px;
    height: 513px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${modal_bg});
    display:flex;
    justify-content: space-around;
    align-items: center;
    flex-flow: column;
    padding: 7%;
    box-sizing: border-box;
    h2 {
        font-family: DBXtypeX;
        position: relative;
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-family: DBXtypeX;
        position: relative;
        top: 140px;
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .message{
        width: 80%;
        margin: 0 auto;
        font-size 2.5em;
        text-align: center;
        font-family: 'Kanit-Medium';
        overflow-wrap: break-word;
        color: #65431d;
    }
`;
const ModalBottom = styled.div`
    position: relative;
    display: block;

`;
const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 183px;
    height: 98px;
    background:top center no-repeat url(${confirm_btn});
    position:relative;
    text-align: center;
    box-sizing: border-box;
    cursor: pointer;
    &:hover{
        background-position: bottom center;
    }
    &.cancel{
        background:top center no-repeat url(${cancel_btn});
    }
`;
