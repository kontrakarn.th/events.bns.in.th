import React, { Component } from 'react'
import stars from './../static/images/stars.png'
import char from './../static/images/charbox_2.png'
export default class Redeem extends Component {
    render() {
        return (
            <div className="redeem__container">
				<div className="redeem__char">
					<img src={char} alt=""/>
				</div>
                {
                    this.props.eventInfo && this.props.eventInfo.can_receive == true ?
                        (
                            this.props.eventInfo.received == true ?
                                <div className={"redeem__button disable"}>
                                    <img src={stars} alt=""/>
                                    &nbsp;รับไปแล้ว&nbsp;
                                    <img src={stars} alt=""/>
                                </div>
                            :
                                <div className={"redeem__button"} onClick={this.props.openModal}>
                                    <img src={stars} alt=""/>
                                    &nbsp;รับของรางวัล&nbsp;
                                    <img src={stars} alt=""/>
                                </div>
                        )
                    :
                        <div className={"redeem__button disable"}>
                            <img src={stars} alt=""/>
                            &nbsp;ไม่ตรงเงื่อนไข&nbsp;
                            <img src={stars} alt=""/>
                        </div>
                    
                }
                
            </div>
        )
  }
}
