import React, { Component } from 'react'
import headText from './../static/images/head_text.png'
import iconScroll from './../static/images/icon_scroll.png'
export default class Head extends Component {
    render() {
        return (
            <div className="newrevival__head">
                <div className="newrevival__head-account">{ this.props.userData.username || ''}</div>
                <div className="newrevival__head-title">
                    <img src={headText} />
                </div>
                <div className="newrevival__scroll"><img src={iconScroll} /></div>
            </div>
        )
  }
}
