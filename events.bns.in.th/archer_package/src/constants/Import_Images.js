import bg from '../static/images/event/bg.jpg';
import buy_btn from '../static/images/event/buy_btn.png';
import disable_btn from '../static/images/event/disable_btn.png';
import preview from '../static/images/event/preview.png';
import zoom from '../static/images/event/zoom.png';

export const Imglist = {
	bg,
	buy_btn,
	disable_btn,
	preview,
	zoom,
}