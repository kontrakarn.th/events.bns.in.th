import React from 'react';
import {connect} from 'react-redux';

import F11Layout from './../features/F11Layout/';
import Main from './../features/main';

export class Home extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={false}
            >
                <Main />
            </F11Layout>
        )
    }
}
