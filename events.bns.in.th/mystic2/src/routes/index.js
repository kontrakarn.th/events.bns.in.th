import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';

import OauthLogin from '../features/oauthLogin';
import Home from '../pages/Home';
import Play from '../pages/Play';
import History from '../pages/History';
import Ani from '../pages/Ani';

export default (props)=>{
  let eventPath = process.env.REACT_APP_EVENT_PATH;
  return (
    <BrowserRouter>
      <>
        <Route path={eventPath} exact component={OauthLogin}/>
        <Route path={eventPath} exact component={Home}/>
        <Route path={eventPath+"/play"} exact component={Play}/>
        <Route path={eventPath+"/history"} exact component={History}/>
        
        {/* <Route path={eventPath+"/ani"} exact component={Ani}/> */}
      </>
    </BrowserRouter>
  )
}
