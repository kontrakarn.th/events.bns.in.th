import React,{useState,useEffect} from 'react';
import {connect} from 'react-redux';
import {setValues} from './../store/redux';
import styled from 'styled-components';
import imgList from './../constants/ImportImages';
import F11Layout from './../features/f11Layout';
import {apiPost} from './../constants/Api';
import ScrollArea from 'react-scrollbar';
import Modals from './../features/modals';

const Home = (props) => {
    const apiHistory = () => {
        props.setValues({
            modal_open: "loading"
        })
        let dataSend = {type: "history"};
        let successCallback = (res) => {
          if(res.status) {
            props.setValues({
              history_reward: res.data,
              modal_open: "",
              load_reward: false
            })
          } else {
            props.setValues({
                modal_open:"message",
                modal_message: res.message,
            });
          }
        }
        let failCallback = (res) => {
          if (res.type === 'no_permission') {
            props.setValues({
                permission: false,
                modal_open: "",
            });
          }else {
            props.setValues({
                modal_open:"message",
                modal_message: res.message,
            });
          }
        }
        apiPost("REACT_APP_API_POST_HISTORY", props.jwtToken, dataSend, successCallback, failCallback);
    }
    
    useEffect(()=>{
        if( props.jwtToken!=="" && props.history_reward.length <= 0 ) apiHistory();
    },[props.jwtToken])
    return (
        <F11Layout page={"history"} hideScroll={true}>
            <Modals />
            <HistoryStyle>
                <ScrollArea className="history" style={{ width: '840', height:405, opacity:1}}>
                    <ul className="history__list">
                        {props.history_reward && props.history_reward.length>0 && props.history_reward.map((item,index)=>{
                            return (
                                <li className="history__slot" key={index}>
                                    <div className="history__name">ได้รับ {item.product_name} x{item.amount}</div>
                                    <div className="history__date">{item.date}</div>
                                    <div className="history__time">{item.time.replace(/:/g,'.')}</div>
                                </li>
                            )
                        })}

                    </ul>
                </ScrollArea>
            </HistoryStyle>
        </F11Layout>
    )
}
const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(Home);

const HistoryStyle = styled.div`
    position: relative;
    background: url(${imgList.history_background});
    height: 700px;
    padding-top: 175px;
    font-family: "Kanit-Light", Tahoma;
    .history {
        display: block;
        width: 840px;
        height: 425px;
        margin: 0px auto;

        &__list{
          padding: 20px 30px;
          color: #FFFFFF;
          font-size: 16px;
        }
        &__slot{
        }
        &__name{
          display: inline-block;
          width: 74%;
        }
        &__date{
          display: inline-block;
          width: 18%;
        }
        &__time{
          display: inline-block;
          width: 8%;
        }
    }
`;
