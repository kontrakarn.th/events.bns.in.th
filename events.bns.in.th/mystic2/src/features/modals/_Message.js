import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from "../../constants/ImportImages";

const ModalLoading = props => {
  const name = "message";
  const actClose = () => {
    if(!props.selected_char && props.characters.length !== 0) {
      props.setValues({modal_open:'character'});
    } else {
      props.setValues({modal_open:''});
    }
  }
  return (
    <ModalCore
        open={props.modal_open === name}
        onClick={()=>actClose()}
    >
      <ModalcontentStyle>
        <div className="message" dangerouslySetInnerHTML={{__html: props.modal_message}} />
        <div className="buttons">
          <a className="btn btn--confirm"onClick={()=>actClose()} />
        </div>
      </ModalcontentStyle>
    </ModalCore>
  )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalLoading);


// modal_message
const ModalcontentStyle = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 476px;
  height: 345px;
  padding: 52px 38px 71px;
  background-image: url(${imgList.modal_message});
  .message {
    color: #fff;
    font-size: 20px;
    display: block;
    display: block;
    text-align: center;
    font-family: "Kanit-Light", Tahoma;
    line-height: 1.2;

  }
  .buttons {
    position: absolute;
    left: 50%;
    bottom: 67px;
    display: flex;
    width: 202px;
    transform: translate(-50%, 0px);
    text-align: center;
    justify-content: center;
    align-items: center;
  }
  .btn {
    display: inline-block;
    width: 90px;
    height: 33px;
    background-position: top center;
    cursor: pointer;
    &--reset {
      background-image: url(${imgList.btn_reset});
    }
    &--cancel {
      background-image: url(${imgList.btn_cancel});
    }
    &--confirm {
      background-image: url(${imgList.btn_confirm});
    }
    &:hover {
      background-position: bottom center;
    }
  }
`;