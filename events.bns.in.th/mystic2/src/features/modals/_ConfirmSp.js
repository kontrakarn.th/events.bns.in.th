import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from "../../constants/ImportImages";
import {apiPost} from '../../constants/Api';

const ModalConfirmSp = (props) => {
  const name = "confirm_sp";

  const apiSelectSp = () =>{
    props.setValues({
      modal_open:"loading"
    })
    let dataSend = {type_selected: props.modal_sp_type, selected_product: props.modal_sp_select};
		let successCallback = (res) => {
			if(res.status) {
				props.setValues({
                    modal_open:"message",
                    modal_message: 'เลือกรางวัลใหญ่เรียบร้อยแล้ว',
                    special_product: res.data.special_product
				})
			} 
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		let failCallback = (res) => {
			if (res.type === 'no_permission') {
				props.setValues({
					permission: false,
					modal_open: "",
				});
			}
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		apiPost("REACT_APP_API_POST_SELECT_SP", props.jwtToken, dataSend, successCallback, failCallback);
  }

  const actClose = () => props.setValues({modal_open: "select_reward"});
  return (
    <ModalCore open={props.modal_open === name} onClick={()=>actClose()}>
      <ModalcontentStyle>
        <div className="message" dangerouslySetInnerHTML={{__html: props.modal_sp}}>
            {/* ต้องการซื้อกุญแจฮงมุนเก่าแก่<br/>
            <span>50+5 x5</span><br/>
            ราคา <span>375,000</span> ไดมอนด์ ? */}
        </div>
        <div className="buttons">
          <a className="btn btn--confirm" onClick={()=>apiSelectSp()} />
          <a className="btn btn--cancel" onClick={()=>actClose()} />
        </div>
      </ModalcontentStyle>
    </ModalCore>
  )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalConfirmSp);


// modal_message
const ModalcontentStyle = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 476px;
  height: 345px;
  padding: 52px 38px 71px;
  background-image: url(${imgList.modal_confirm});
  .message {
    color: #fff;
    font-size: 20px;
    display: block;
    text-align: center;
    font-family: "Kanit-Light", Tahoma;
    line-height: 1.2;
    span {
      color: #6fc96d;
    }

  }
  .buttons {
    position: absolute;
    left: 50%;
    bottom: 67px;
    display: flex;
    width: 202px;
    transform: translate(-50%, 0px);
    text-align: center;
    justify-content: space-between;
    align-items: center;
  }
  .btn {
    display: inline-block;
    width: 90px;
    height: 33px;
    background-position: top center;
    cursor: pointer;
    &--reset {
      background-image: url(${imgList.btn_reset_short});
    }
    &--cancel {
      background-image: url(${imgList.btn_cancel});
    }
    &--confirm {
      background-image: url(${imgList.btn_confirm});
    }
    &:hover {
      background-position: bottom center;
    }
  }
`;
// 
// 
// 