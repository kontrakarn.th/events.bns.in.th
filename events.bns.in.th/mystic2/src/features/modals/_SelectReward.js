import React,{useState,useEffect} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import {setValues} from './../../store/redux';
import imgList from "../../constants/ImportImages";

const ModalSelectReward = (props) => {
  const name = "select_reward";
  const [selected,setSelected] = useState(-1);
  let actClose = () =>  props.setValues({modal_open: ""});
  let list = [
    imgList.icon_set_1_1,
    imgList.icon_set_1_2,
    imgList.icon_set_1_3,
    imgList.icon_set_1_4,
    imgList.icon_set_1_5,
    imgList.icon_set_1_6,
    imgList.icon_set_1_7,
    imgList.icon_set_1_8,
    imgList.icon_set_1_9, 
    imgList.icon_set_1_10,
    imgList.icon_set_1_11,
    imgList.icon_set_1_12,
    imgList.icon_set_1_13,
    imgList.icon_set_1_14,
  ]
  let list_name = [
    'สัญลักษณ์สุภาพชน',
    'ชุดสง่างาม',
    'ชุดราชวงศ์แห่งการปกครอง',
    'หงส์ทองแห่งอำนาจ',
    'มงกุฏแห่งการยึดครอง',
    'แว่นแห่งอิทธิพล',
    'หินสกินผู้บรรลุ อันทรงเกียรติ',
    'ชุดยมทูตสีเลือด',
    'ประดับหน้ายมทูตสีเลือด',
    'ประดับผมยมทูตสีเลือด',
    'ประดับชุดยมทูตสีเลือด',
    'หินสัตว์เลี้ยงไซคลอปส์น้อย',
    'กล่องอาวุธลวงตายมทูตสีเลือด',
    ''
  ]
  let sizeOfSet = {
    1: { width: "740px", height: "280px", marginBottom: "45px"},
    2: { width: "740px", height: "280px", marginBottom: "45px"},
  }
  const handleSelect = () =>{
    let type = 0;
    let text = '';
    if(selected===13){
      type = 2;
      text = 'ต้องการเลือกรางวัลใหญ่แบบสุ่ม';
    }
    else{
      type = 1;
      text = 'ต้องการเลือกรางวัลใหญ่<br/><span>'+list_name[selected]+'</span></br>ค่าธรรมเนียม <span>10,000</span> ไดมอนด์ ?';
    }
    props.setValues({
      modal_open: 'confirm_sp',
      modal_sp: text,
      modal_sp_type: type,
      modal_sp_select: (selected+1)
    })
  }
  useEffect(()=>{
		if(props.modal_open==='select_reward'){
      setSelected(-1);
    }
	},[props.modal_open])
  return (
    <ModalCore
        open={props.modal_open === name}
        onClick={()=>actClose()}
    >
      <ModalContentStyle>
        <a className="close" onClick={()=>actClose()}/>
        <RewardList  set={sizeOfSet[1]}>
          {list.map((item,index)=>{
            return (
              <RewardSlot key={"reward_"+index} onClick={()=>setSelected(index)} active={index===selected}>
                <img src={item} alt="" />
              </RewardSlot>
            )
          })}
        </RewardList>
        <div className="buttons">
          {selected!==-1
          ?
            <a className="btn btn--confirm" onClick={()=>handleSelect()} />
          :
          < a className="btn btn--confirm" />
          }
        </div>
      </ModalContentStyle>
    </ModalCore>
  )
}

const mapStateToProps = state => ({...state});
// const mapDispatchToProps = (dispatch) => {
//   return ({ 
//     setValues : (obj)=> dispatch(setValues(obj))
//   })
// };
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(ModalSelectReward);

// modal_message
const ModalContentStyle = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 1105px;
  height: 700px;
  padding: 172px 135px 102px
  background-image: url(${imgList.modal_select_reward});
  
  .message {
    color: #FFFFFF;
    font-size: 20px;
    display: block;
    display: block;
    text-align: center;
    span {
      color: #d19e56;
    }

  }
  .close {
    position: absolute;
    top: 75px;
    right: 82px;
    display: block;
    width: 36px;
    height: 36px;
    background-image: url(${imgList.icon_close}); 
  }
  .buttons {
    position: absolute;
    left: 50%;
    bottom: 131px;
    display: flex;
    width: 100%;
    transform: translate(-50%, 0px);
    text-align: center;
    justify-content: center;
    align-items: center;
  }
  .btn {
    display: inline-block;
    width: 194px;
    height: 50px;
    background-position: top center;
    background-image: url(${imgList.btn_confirm_select});
    cursor: pointer;
    &:hover {
      background-position: bottom center;
    }
  }
`;
const RewardList = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  width: ${({set})=>set.width};
  height: ${({set})=>set.height};
  margin-bottom: ${({set})=>set.marginBottom};
`; 
const RewardSlot = styled.a`
  position: relative;
  display: block;
  width: 84px;
  height: 84px;
  margin: 0px 10px;
  cursor: pointer;
  &:after {
    content: " ";
    position: absolute;
    opacity: ${props=>props.active? 1:0};
    top: 0px;
    left: 0px;
    display: block;
    width: 100%;
    height: 100%;
    background-image: url(${imgList.slot_selected});
  }
`; 