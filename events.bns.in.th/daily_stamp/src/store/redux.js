import { Imglist } from "../constants/Import_Images";

const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    showCharacterName: true,
    characters:[],
    username: "",
    coin: 0,
    character: "",
    selected_char: false,
    loginStatus: false,
    showScroll: true,
    noScroll: true,
    loading: false,
    permission: true,
    status: false,
    can_play: false,
    ranking_list:  Array(10).fill(1), // default array []

    //===modal
    modal_open: "",//"loading","detail","showreward",""showreward","send","modalInput"
    modal_message: "",
    modal_item: "",

//=== EVENT ===//
    //---history values
    history_list: [],

    //---main
    clan_members: 0,
    clan_ranks: [],
    clan_rewards: {
        step1: {id: 1, can_claim: false, is_claimed: false},
        step2: {id: 2, can_claim: false, is_claimed: false},
        step3: {id: 3, can_claim: false, is_claimed: false},
    },
    //quests: [],
    quests: [
        {
            completed_count: '1/5'
        },
        {
            completed_count: '2/5'
        },
        {
            completed_count: '0/5'
        },
        {
            completed_count: '1/5'
        },
        {
            completed_count: '1/5'
        },
        {
            completed_count: '1/5'
        },
        {
            completed_count: '1/5'
        },
        {
            completed_count: '1/5'
        },
        {
            completed_count: '0/5'
        },
        {
            completed_count: '2/5'
        },

    ],
    rank_rewards: {},
    score: "0",

    // mission_past:[
    //     {
    //         day: 1,
    //         image: Imglist.day1,
    //         count: '0/5'
    //     },
    //     {
    //         day: 2,
    //         image: Imglist.day2,
    //         count: '0/5'
    //     },
    //     {
    //         day: 3,
    //         image: Imglist.day3,
    //         count: '0/5'
    //     },
    //     {
    //         day: 4,
    //         image: Imglist.day4,
    //         count: '0/5'
    //     },
    //     {
    //         day: 5,
    //         image: Imglist.day5,
    //         count: '0/5'
    //     },
    // ],
    quest_daily: {
      day: 1,
      image: Imglist.day1,
      count: '0/5'
    },
    mission_past: [
        [
            {
                day: 1,
                sub_quest: [],
                image: Imglist.day1,
                count: '0/5'
            },
            {
                day: 2,
                sub_quest: [],
                image: Imglist.day2,
                count: '0/5'
            },
            {
                day: 3,
                sub_quest: [],
                image: Imglist.day3,
                count: '0/5'
            },
            {
                day: 4,
                sub_quest: [],
                image: Imglist.day4,
                count: '0/5'
            },
            {
                day: 5,
                sub_quest: [],
                image: Imglist.day5,
                count: '0/5'
            },
        ],
        // [
        //     {
        //         day: 26,
        //         image: Imglist.day26,
        //         count: '0/5'
        //     },
        //     {
        //         day: 27,
        //         image: Imglist.day27,
        //         count: '0/5'
        //     },
        //     {
        //         day: 28,
        //         image: Imglist.day28,
        //         count: '0/5'
        //     },
        //     {
        //         day: 29,
        //         image: Imglist.day29,
        //         count: '0/5'
        //     },
        //     {
        //         day: 30,
        //         image: Imglist.day30,
        //         count: '0/5'
        //     },
        // ],
    ],

    //=== Redeem ===//
    total_stamp: 0,
    redeem_list: [
        {
            image: Imglist.redeem1,
            stamp: 3,
            status: 'not_success',
        },
        {
            image: Imglist.redeem2,
            stamp: 7,
            status: 'not_success',
        },
        {
            image: Imglist.redeem3,
            stamp: 11,
            status: 'not_success',
        },
        {
            image: Imglist.redeem4,
            stamp: 14,
            status: 'not_success',
        },
        {
            image: Imglist.redeem5,
            stamp: 18,
            status: 'not_success',
        },
        {
            image: Imglist.special_items,
            stamp: 25,
            status: 'not_success',
        },
    ]
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         : return ({ ...state, sessionKey: action.value });
        case 'ACCOUNT_AUTHED'       : return ({ ...state, userData: action.value });
        case 'RECEIVE_SESSION_KEY'  : return ({ ...state, sessionKey: action.value });
        case 'SET_LOGIN_URL'        : return ({ ...state, loginUrl: action.value });
        case 'SET_LOGOUT_URL'       : return ({ ...state, logoutUrl: action.value });
        case 'ACCOUNT_LOGIN'        : return ({ ...state, authState: "LOGGED_IN" });
        case 'ACCOUNT_LOGOUT'       : return ({ ...state, authState: "LOGGED_OUT" });
        case 'ACCOUNT_AUTH_CHECKED' : return ({ ...state, authed: true });
        case 'SET_JWT_TOKEN'        : return ({ ...state, jwtToken: action.value });

        case "SET_VALUE"            : return (Object.keys(defaultState).indexOf(action.key) >= 0) ? { ...state, [action.key]: action.value } : state;
        case "SET_VALUES"           : return { ...state, ...action.value };
        default:
            return state;
    }
}

export const onAccountAuth          = (sessionKey)  => ({ type: 'ACCOUNT_AUTH',         value: sessionKey });
export const onAccountAuthed        = (userData)    => ({ type: 'ACCOUNT_AUTHED',       value: userData });
export const onAccountLogin         = ()            => ({ type: 'ACCOUNT_LOGIN',        value: "" });
export const onAccountLogout        = ()            => ({ type: 'ACCOUNT_LOGOUT',       value: "" });
export const receiveSessionKey      = (sessionKey)  => ({ type: 'RECEIVE_SESSION_KEY',  value: sessionKey });
export const setLoginUrl            = (url)         => ({ type: 'SET_LOGIN_URL',        value: url });
export const setLogoutUrl           = (url)         => ({ type: 'SET_LOGOUT_URL',       value: url });
export const onAccountAuthChecked   = ()            => ({ type: 'ACCOUNT_AUTH_CHECKE',  value: true })
export const setJwtToken            = (token)       => ({ type: 'SET_JWT_TOKEN',        value: token });

export const setValue               = (key, value) => ({ type: "SET_VALUE", value, key });
export const setValues              = (value) => ({ type: "SET_VALUES", value });
