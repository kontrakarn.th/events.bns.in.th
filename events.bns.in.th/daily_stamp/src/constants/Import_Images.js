import bg 						from '../static/images/bg.jpg';
import btn_cancel 				from '../static/images/btn_cancel.png';
import btn_confirm 				from '../static/images/btn_confirm.png';
import btn_home 				from '../static/images/btn_home.png';
import btn_inactive 			from '../static/images/btn_inactive.png';
import detail 					from '../static/images/detail.png';
import history_section 			from '../static/images/history_section.png';
import ico_scroll 				from '../static/images/ico_scroll.png';
import icon_dropdown 			from '../static/images/icon_dropdown.png';
import icon_menu 				from '../static/images/icon_menu.png';
import menu_bg 					from '../static/images/menu_bg.jpg';
import left_arrow 				from '../static/images/left_arrow.png';
import right_arrow  			from '../static/images/right_arrow.png';

import mission_daily_header 	from '../static/images/mission_daily_header.png'
import mission_past_header		from '../static/images/mission_past_header.png'
import bg_history				from '../static/images/bg_history.jpg'
import bg_redeem				from '../static/images/bg_redeem.jpg'
import redeem_header			from '../static/images/redeem_header.png'
import redeem1					from '../static/images/redeem1.jpg'
import redeem2					from '../static/images/redeem2.jpg'
import redeem3					from '../static/images/redeem3.jpg'
import redeem4					from '../static/images/redeem4.jpg'
import redeem5					from '../static/images/redeem5.jpg'
import btn_redeem				from '../static/images/btn_redeem.png'
import special_header			from '../static/images/special_header.png'
import special_items			from '../static/images/special_items.png'
import day1						from '../static/images/mission/day1.png'
import day2						from '../static/images/mission/day2.png'
import day3						from '../static/images/mission/day3.png'
import day4						from '../static/images/mission/day4.png'
import day5						from '../static/images/mission/day5.png'
import day6						from '../static/images/mission/day6.png'
import day7						from '../static/images/mission/day7.png'
import day8						from '../static/images/mission/day8.png'
import day9						from '../static/images/mission/day9.png'
import day10					from '../static/images/mission/day10.png'
import day11					from '../static/images/mission/day11.png'
import day12					from '../static/images/mission/day12.png'
import day13					from '../static/images/mission/day13.png'
import day14					from '../static/images/mission/day14.png'
import day15					from '../static/images/mission/day15.png'
import day16					from '../static/images/mission/day16.png'
import day17					from '../static/images/mission/day17.png'
import day18					from '../static/images/mission/day18.png'
import day19					from '../static/images/mission/day19.png'
import day20					from '../static/images/mission/day20.png'
import day21					from '../static/images/mission/day21.png'
import day22					from '../static/images/mission/day22.png'
import day23					from '../static/images/mission/day23.png'
import day24					from '../static/images/mission/day24.png'
import day25					from '../static/images/mission/day25.png'
import day26					from '../static/images/mission/day26.png'
import day27					from '../static/images/mission/day27.png'
import day28					from '../static/images/mission/day28.png'
import day29					from '../static/images/mission/day29.png'
import day30					from '../static/images/mission/day30.png'

import daily_day26				from '../static/images/daily/day26.png'
import daily_day27				from '../static/images/daily/day27.png'
import daily_day28				from '../static/images/daily/day28.png'
import daily_day29				from '../static/images/daily/day29.png'
import daily_day30				from '../static/images/daily/day30.png'

import btn_double				from '../static/images/btn_double.png'
import btn_receive_items		from '../static/images/btn_receive_items.png'
import stamp					from '../static/images/stamp.png'
import icon_back				from '../static/images/icon_back.png'
import icon_next				from '../static/images/icon_next.png'
import btn_unlock				from '../static/images/btn_unlock.png'
import bg_gradient				from '../static/images/bg_gradient.png'


export const Imglist = {
	bg,
	btn_cancel,
	btn_confirm,
	btn_home,
	btn_inactive,
	detail,
	history_section,
	ico_scroll,
	icon_dropdown,
	icon_menu,
	menu_bg,
	left_arrow,
	right_arrow,

	mission_daily_header,
	mission_past_header,
	bg_history,
	bg_redeem,
	redeem_header,
	redeem1,
	redeem2,
	redeem3,
	redeem4,
	redeem5,
	btn_redeem,
	special_header,
	special_items,
	day1,
	day2,
	day3,
	day4,
	day5,
	day6,
	day7,
	day8,
	day9,
	day10,
	day11,
	day12,
	day13,
	day14,
	day15,
	day16,
	day17,
	day18,
	day19,
	day20,
	day21,
	day22,
	day23,
	day24,
	day25,
	day26,
	day27,
	day28,
	day29,
	day30,
	daily_day26,
	daily_day27,
	daily_day28,
	daily_day29,
	daily_day30,
	btn_double,
	btn_receive_items,
	stamp,
	icon_back,
	icon_next,
	btn_unlock,
	bg_gradient,
}
