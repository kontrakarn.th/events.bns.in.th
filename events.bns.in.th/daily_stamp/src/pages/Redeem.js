import React from 'react';

import F11Layout from '../features/F11Layout';
import Redeem from './../features/redeem';

export class RedeemPage extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
            >
                <Redeem/>
            </F11Layout>
        )
    }
}
