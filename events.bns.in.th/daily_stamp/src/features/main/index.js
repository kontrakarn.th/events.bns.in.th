import React from 'react';
// import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
import Logo from './BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirmUnlock from './../../features/modals/ModalConfirmUnlock';
import ModalConfirmClaimDiamond from './../../features/modals/ModalConfirmClaimDiamond';
import {Imglist} from './../../constants/Import_Images';
import ModalSelectCharacter from "../modals/ModalSelectCharacter";

function SampleNextArrow(props) {
    const { style, onClick } = props;
    return (
      <div
        className="btn_next"
        style={{ ...style, display: "block" }}
        onClick={onClick}
      >
        <img src={Imglist.icon_next} alt="" />
      </div>
    );
}
function SamplePrevArrow(props) {
    const { style, onClick } = props;
    return (
      <div
        className="btn_prev"
        style={{ ...style, display: "block"}}
        onClick={onClick}
      >
        <img src={Imglist.icon_back} alt="" />
      </div>
    );
}

class Main extends React.Component {

    actConfirmUnlock(value){
        if (value === undefined || value === ""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไม่สามารถปลดล็อคได้",
            });
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'unlock', value: value};
        let successCallback = (data) => {
            if (data.status === true) {
                this.mangeResponse(data);
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_UNLOCK_DAILY_QUEST", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    actConfirmClaimFree(value){
        if (value === undefined || value === ""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไม่สามารถปลดล็อคได้",
            });
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'claim_free', value: value};
        let successCallback = (data) => {
            if (data.status === true) {
                this.mangeResponse(data);
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_CLAIM_FREE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    actConfirmClaimDiamond(value){
        if (value === undefined || value === ""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไม่สามารถปลดล็อคได้",
            });
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'claim_diamond', value: value};
        let successCallback = (data) => {
            if (data.status === true) {
                this.mangeResponse(data);
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_CLAIM_DIAMOND", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true) {
                let quests = [];
                let quest_all = [];
                let index = 0;
                quests[index] = [];

                for (let i = 0; i < data.data.quests.length; i++) {
                    if ((i % 5) === 0 && i != 0) {
                        index++;
                        quests[index] = [];
                    }

                    quests[index].push({
                        index: data.data.quests[i].quest_id,
                        day: data.data.quests[i].quest_id,
                        sub_quest: data.data.quests[i].sub_quest,
                        image: Imglist['day' + data.data.quests[i].quest_id.toString()],
                        status: data.data.quests[i].status,
                        claim_free_status: data.data.quests[i].claim_free_status,
                        claim_diamond_status: data.data.quests[i].claim_diamond_status,
                    });

                    if (data.data.quests[i].quest_id === 24) {
                        if (data.data.quests[i].status === 'success') {
                            for (let j = 0; j < data.data.quests[i].sub_quest.length; j++) {
                                data.data.quests[i].sub_quest[j].amount = 1;
                            }
                        }
                    }

                    quest_all.push({
                        index: data.data.quests[i].quest_id,
                        day: data.data.quests[i].quest_id,
                        sub_quest: data.data.quests[i].sub_quest,
                        image: Imglist['day' + data.data.quests[i].quest_id.toString()],
                        status: data.data.quests[i].status,
                        claim_free_status: data.data.quests[i].claim_free_status,
                        claim_diamond_status: data.data.quests[i].claim_diamond_status,
                    });




                }

                let quest_daily = {}

                if (quest_all.length > 0) {
                    quest_daily = quest_all[quest_all.length - 1];

                    if (quest_daily.index > 25) {
                        quest_daily.image = Imglist['daily_day' + (quest_daily.index).toString()];
                    }
                }

                this.props.setValues({
                    modal_open: "",
                    username: data.data.username,
                    character: data.data.character_name,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters||[],
                    mission_past: quests || [],
                    quest_daily: quest_daily,
                });

                console.log(quest_daily.index)
                console.log(quest_all)
                console.log(quest_daily)

                if (data.data.selected_char === false){
                    this.props.setValues({ modal_open: "selectcharacter" });
                }
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSelectCharacter(id = ""){
        if (id === undefined || id === ""){
            return;
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type: 'select_character', id: id};
        let successCallback = (data) => {
            if (data.status) {
                this.mangeResponse(data);
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    mangeResponse(data) {
        let quests = [];
        let quest_all = [];
        let index = 0;
        quests[index] = [];

        for (let i = 0; i < data.data.quests.length; i++) {
            if ((i % 5) === 0 && i != 0) {
                index++;
                quests[index] = [];
            }

            quests[index].push({
                index: data.data.quests[i].quest_id,
                day: data.data.quests[i].quest_id,
                sub_quest: data.data.quests[i].sub_quest,
                image: Imglist['day' + data.data.quests[i].quest_id.toString()],
                status: data.data.quests[i].status,
                claim_free_status: data.data.quests[i].claim_free_status,
                claim_diamond_status: data.data.quests[i].claim_diamond_status,
            });

            if (data.data.quests[i].quest_id === 24) {
                if (data.data.quests[i].status === 'success') {
                    for (let j = 0; j < data.data.quests[i].sub_quest.length; j++) {
                        data.data.quests[i].sub_quest[j].amount = 1;
                    }
                }
            }

            quest_all.push({
                index: data.data.quests[i].quest_id,
                day: data.data.quests[i].quest_id,
                sub_quest: data.data.quests[i].sub_quest,
                image: Imglist['day' + data.data.quests[i].quest_id.toString()],
                status: data.data.quests[i].status,
                claim_free_status: data.data.quests[i].claim_free_status,
                claim_diamond_status: data.data.quests[i].claim_diamond_status,
            });




        }

        let quest_daily = {}

        if (quest_all.length > 0) {
            quest_daily = quest_all[quest_all.length - 1];

            if (quest_daily.index > 25) {
                quest_daily.image = Imglist['daily_day' + (quest_daily.index).toString()];
            }
        }

        this.props.setValues({
            modal_open: "message",
            modal_message: data.message,
            username: data.data.username,
            character: data.data.character_name,
            selected_char: data.data.selected_char,
            characters: data.data.characters||[],
            mission_past: quests || [],
            quest_daily: quest_daily,
        });

        if (data.data.selected_char === false){
            this.props.setValues({ modal_open: "selectcharacter" });
        }
    }

    // apiRedeem(type_id){
    //     this.props.setValues({ modal_open: "loading" });
    //     let dataSend = {type_id};
    //     let successCallback = (data) => {
    //         if (data.status) {
    //             this.props.setValues({
    //                 ...data.content.response,
    //                 modal_open:"message",
    //                 modal_message: "ส่งของรางวัลสำเร็จ<br/>กรุณาตรวจสอบจดหมายภายในเกม"
    //             });
    //
    //         }
    //     };
    //     const failCallback = (data) => {
    //         if (data.type === 'no_permission') {
    //             this.props.setValues({
    //                 permission: false,
    //                 modal_open: "",
    //             });
    //         }else {
    //             this.props.setValues({
    //                 modal_open:"message",
    //                 modal_message: data.message,
    //             });
    //         }
    //     };
    //     apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    // }

    checkBtn(step) {
        let received = this.props.clan_rewards[step].is_claimed || false;
        let can = this.props.clan_rewards[step].can_claim || false;
        if(received) {
            return "disable"
        } else if(can) {
            return "pending"
        } else {
            return "disable"
        }
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }

    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }

    handleConfirmUnlock(value) {
        let message = 'ต้องการปลดล็อคภารกิจย้อนหลัง<br>ค่าธรรมเนียม 10,000 ไดมอนด์ ?';

        if (value === 25) {
            message = 'ต้องการปลดล็อคภารกิจย้อนหลัง<br>ค่าธรรมเนียมฟรี ?';
        }

        this.props.setValues({
            modal_open: 'confirm',
            modal_message: message,
            modal_item_token: value,
        })
    }

    handleConfirmClaimDiamond(value) {
        this.props.setValues({
            modal_open: 'confirm_claim_diamond',
            modal_message: 'ต้องการรับไอเทมเพิ่ม<br>ค่าธรรมเนียม 1,000 ไดมอนด์ ?',
            modal_item_token: value,
        })
    }

    render() {
        let quests = this.props.quests || [];

        const settings = {
            dots: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true,
            swipeToSlide: true,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />,
        };
        return (
            <PageWrapper>
                <Logo/>
                <Menu page='home'/>
                <Content>
                    <DetailFrame src={Imglist['detail']} alt='detail'/>
                    <MissionDaily>
                        <div className="header">
                            <img src={Imglist['mission_daily_header']} alt="" />
                        </div>
                        { this.props.quest_daily &&
                            <div className="content">
                                { this.props.quest_daily.index < 26 ?
                                    <div className="contentInner">
                                        {this.props.quest_daily.status === 'success' &&
                                            <div className="received"><img src={Imglist.stamp} alt="" /></div>
                                        }

                                        { this.props.quest_daily.sub_quest.length > 0 &&
                                            <div className="count">{this.props.quest_daily.sub_quest[0].total_quest}/{this.props.quest_daily.sub_quest[0].amount}</div>
                                        }

                                        <img src={this.props.quest_daily.image} alt="" />

                                        {this.props.quest_daily.status === 'success' && this.props.quest_daily.claim_free_status === 'pending' &&
                                            <div className="btn_receive" onClick={() => this.actConfirmClaimFree(this.props.quest_daily.index)}></div>
                                        }

                                        {this.props.quest_daily.status === 'success' && this.props.quest_daily.claim_free_status === 'success' &&
                                            <div className="btn_receive disabled"></div>
                                        }

                                        {this.props.quest_daily.status === 'pending' &&
                                            <div className="btn_receive grey"></div>
                                        }

                                        {this.props.quest_daily.status === 'not_success' &&
                                            <div className="btn_receive grey"></div>
                                        }

                                        {this.props.quest_daily.status === 'success' && this.props.quest_daily.claim_diamond_status === 'pending' &&
                                            <div className="btn_double" onClick={() => this.handleConfirmClaimDiamond(this.props.quest_daily.index)}></div>
                                        }

                                        {this.props.quest_daily.status === 'success' && this.props.quest_daily.claim_diamond_status === 'success' &&
                                            <div className="btn_double disabled"></div>
                                        }

                                        {this.props.quest_daily.status === 'pending' &&
                                            <div className="btn_double grey"></div>
                                        }

                                        {this.props.quest_daily.status === 'not_success' &&
                                            <div className="btn_double grey"></div>
                                        }
                                    </div>
                                :
                                    <div className="contentInner morethan25">
                                        <img src={this.props.quest_daily.image} alt="" />
                                        {/* <div className="btn_receive disabled" onClick={()=>this.props.setValues({modal_open:'message'})}></div>
                                        <div className="btn_double disabled"></div> */}

                                        { this.props.quest_daily.status === 'success' &&
                                            <div className="received"><img src={Imglist.stamp} alt="" /></div>
                                        }

                                        { this.props.quest_daily.status === 'success' && this.props.quest_daily.claim_free_status === 'pending' &&
                                            <div className="btn_receive" onClick={() => this.actConfirmClaimFree(this.props.quest_daily.index)}></div>
                                        }

                                        { this.props.quest_daily.status === 'success' && this.props.quest_daily.claim_free_status === 'success' &&
                                            <div className="btn_receive disabled"></div>
                                        }

                                        { this.props.quest_daily.status === 'pending' &&
                                            <div className="btn_receive grey"></div>
                                        }

                                        { this.props.quest_daily.status === 'not_success' &&
                                            <div className="btn_receive grey"></div>
                                        }

                                        { this.props.quest_daily.status === 'success' && this.props.quest_daily.claim_diamond_status === 'pending' &&
                                            <div className="btn_double" onClick={() => this.handleConfirmClaimDiamond(this.props.quest_daily.index)}></div>
                                        }

                                        { this.props.quest_daily.status === 'success' && this.props.quest_daily.claim_diamond_status === 'success' &&
                                            <div className="btn_double disabled"></div>
                                        }

                                        { this.props.quest_daily.status === 'pending' &&
                                            <div className="btn_double grey"></div>
                                        }

                                        { this.props.quest_daily.status === 'not_success'  &&
                                            <div className="btn_double grey"></div>
                                        }

                                        <div>
                                            { this.props.quest_daily.sub_quest && this.props.quest_daily.sub_quest.length > 0 && this.props.quest_daily.sub_quest.map((item, key) => {
                                                return (
                                                    <Box key={'box_'+(key+1)} pos={Math.floor(key/4) * 161} start={165}>
                                                        {item.total_quest}/{item.amount}
                                                    </Box>
                                                )
                                            })}
                                        </div>
                                    </div>
                               }
                            </div>
                        }
                    </MissionDaily>
                    <MissionPast>
                        <div className="header">
                            <img src={Imglist['mission_past_header']} alt="" />
                        </div>
                        <div className="slider_vertical">
                            <Slider {...settings}>
                                { this.props.mission_past.length > 0 && this.props.mission_past.map((items, value) => {
                                    return (
                                        <div>
                                            { items.map((obj, key) => {
                                                  return (
                                                      <div key={'group' + value + key}>
                                                          {/* { this.props.quest_daily.index !== obj.index && */}
                                                          {value !== 5 ?
                                                              <div>
                                                                  { obj.index !== this.props.quest_daily.index &&
                                                                      <div className="slider_wrapper" key={key}>
                                                                          <img src={obj.image} alt=""/>
                                                                          { obj.status === 'success' &&
                                                                              <div className="received"><img src={Imglist.stamp} alt="" /></div>
                                                                          }

                                                                          { obj.sub_quest.length > 0 &&
                                                                              <div className="count">{obj.sub_quest[0].total_quest}/{obj.sub_quest[0].amount}</div>
                                                                          }

                                                                          { obj.status === 'success' && obj.claim_free_status === 'pending' &&
                                                                              <div className="btn_receive" onClick={() => this.actConfirmClaimFree(obj.index)}></div>
                                                                          }

                                                                          { obj.status === 'success' && obj.claim_free_status === 'success' &&
                                                                              <div className="btn_receive disabled"></div>
                                                                          }

                                                                          { obj.status === 'pending' &&
                                                                              <div className="btn_receive grey"></div>
                                                                          }

                                                                          { obj.status === 'not_success' &&
                                                                              <div className="btn_receive grey"></div>
                                                                          }

                                                                          { obj.status === 'success' && obj.claim_diamond_status === 'pending' &&
                                                                              <div className="btn_double" onClick={() => this.handleConfirmClaimDiamond(obj.index)}></div>
                                                                          }

                                                                          { obj.status === 'success' && obj.claim_diamond_status === 'success' &&
                                                                              <div className="btn_double disabled"></div>
                                                                          }

                                                                          { obj.status === 'pending' &&
                                                                              <div className="btn_double grey"></div>
                                                                          }

                                                                          { obj.status === 'not_success'  &&
                                                                              <div className="btn_double grey"></div>
                                                                          }

                                                                          { obj.status === 'not_success' &&
                                                                              <div className="btn_unlock" onClick={() => this.handleConfirmUnlock(obj.index)}></div>
                                                                          }
                                                                      </div>
                                                                  }
                                                              </div>
                                                          :
                                                              <div>
                                                                  { obj.index !== this.props.quest_daily.index &&
                                                                      <div className="slider_wrapper morethan25" key={key}>
                                                                          <img src={obj.image} alt=""/>

                                                                          { obj.status === 'success' &&
                                                                              <div className="received"><img src={Imglist.stamp} alt="" /></div>
                                                                          }

                                                                          { obj.status === 'success' && obj.claim_free_status === 'pending' &&
                                                                              <div className="btn_receive" onClick={() => this.actConfirmClaimFree(obj.index)}></div>
                                                                          }

                                                                          { obj.status === 'success' && obj.claim_free_status === 'success' &&
                                                                              <div className="btn_receive disabled"></div>
                                                                          }

                                                                          { obj.status === 'pending' &&
                                                                              <div className="btn_receive grey"></div>
                                                                          }

                                                                          { obj.status === 'not_success' &&
                                                                              <div className="btn_receive grey"></div>
                                                                          }

                                                                          { obj.status === 'success' && obj.claim_diamond_status === 'pending' &&
                                                                              <div className="btn_double" onClick={() => this.handleConfirmClaimDiamond(obj.index)}></div>
                                                                          }

                                                                          { obj.status === 'success' && obj.claim_diamond_status === 'success' &&
                                                                              <div className="btn_double disabled"></div>
                                                                          }

                                                                          { obj.status === 'pending' &&
                                                                              <div className="btn_double grey"></div>
                                                                          }

                                                                          { obj.status === 'not_success'  &&
                                                                              <div className="btn_double grey"></div>
                                                                          }

                                                                          {/* <div className="btn_receive" onClick={()=>this.props.setValues({modal_open:'message'})}></div> */}
                                                                          {/* <div className="btn_double"></div> */}
                                                                      </div>
                                                                  }
                                                              </div>
                                                          }

                                                          {/* } */}
                                                      </div>
                                                  )
                                              })
                                            }
                                         </div>
                                        )
                                    })
                                }


                                 {/* <div>
                                   { this.props.mission_past.group2.map((obj, key) => {
                                         return (
                                             <div key={'group2' + key}>
                                                 { this.props.quest_daily.index !== obj.index &&
                                                     <div className="slider_wrapper morethan25" key={key}>
                                                         <img src={obj.image} alt=""/>
                                                         <div className="btn_receive" onClick={()=>this.props.setValues({modal_open:'message'})}></div>
                                                         <div className="btn_double"></div>
                                                     </div>
                                                 }
                                             </div>
                                         )
                                       })
                                   }
                                 </div> */}
                            </Slider>
                        </div>
                    </MissionPast>

                </Content>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirmUnlock actConfirm={this.actConfirmUnlock.bind(this)}/>
                <ModalConfirmClaimDiamond actConfirm={this.actConfirmClaimDiamond.bind(this)}/>
                <ModalSelectCharacter
                  actSelectCharacter={this.apiSelectCharacter.bind(this)}
                />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #33281d;
    width: 1105px;
    padding-top: 740px;
    padding-bottom: 116px;
    text-align: center;
    position: relative;
    z-index: 20;
`

const Content = styled.div `
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }
`

const DetailFrame = styled.img`
    display: block;
    margin: 60px auto 69px;
`

// const ScoreFrame = styled.div`
//     background: top center no-repeat url(${Imglist['main_content1']}),bottom center no-repeat url(${Imglist['main_content2']});
//     width: 922px;
//     height: 2793px;
//     display: block;
//     margin: 45px auto 0;
//     position: relative;
//     z-index: 25;
//     .wrapper{
//         width: 700px;
//         margin: 0 auto;
//         height: 1360px;
//         padding-top: 174px;
//         position: relative;
//     }
//     .wrapper2{
//         width: 700px;
//         margin: 0 auto;
//         height: 1360px;
//         position: relative;
//     }
// `

const Box = styled.div`
        width: 33px;
        height: 33px;
        margin: 0 auto;
        position: absolute;
        left: 170px;
        top: ${props=>props.start}px;
        color: #ffffff;
        line-height: 33px;
        // &:nth-child(2){
        //     left: 416px;
        // }
        // &:nth-child(3n){
        //     left: 662px;
        // }
        // &:nth-child(3n+4){
        //     top: calc(${props=>props.start}px + ${props=>props.pos}px);
        //     left: 170px;
        // }
        // &:nth-child(3n+5){
        //      top: calc(${props=>props.start}px + ${props=>props.pos}px);
        //      left: 416px;
        // }
        // &:nth-child(3n+6){
        //      top: calc(${props=>props.start}px + ${props=>props.pos}px);
        // }

        left: 163px;
        &:nth-child(2){
            left: 377px;
        }
        &:nth-child(3n){
            left: 590px;
        }
        &:nth-child(4n){
            left: 803px;
        }
        &:nth-child(4n+5){
            top: calc(${props=>props.start}px + ${props=>props.pos}px);
            left: 163px;
        }
        &:nth-child(4n+6){
            top: calc(${props=>props.start}px + ${props=>props.pos}px);
            left: 376px;
        }
        &:nth-child(4n+7){
            top: calc(${props=>props.start}px + ${props=>props.pos}px);
            left: 590px;
        }
        &:nth-child(4n+8){
            top: calc(${props=>props.start}px + ${props=>props.pos}px);
            left: 803px;
       }
`

const MissionDaily = styled.div `
    .header{
        margin: 30px 0;
    }
    .contentInner{
        position: relative;
        display: inline-block;
    }
    .count{
        position: absolute;
        left: 202px;
        top: 148px;
        color: #fff;
        font-size: 13px;
    }
    .btn_receive{
        background: top center no-repeat url(${Imglist['btn_receive_items']});
        width: 140px;
        height: 37px;
        cursor: pointer;
        position: absolute;
        right: 70px;
        top: 75px;
        &:hover{
            background-position: center center;
        }
        &.disabled{
            background-position: bottom center;
            cursor: default;
        }
        &.grey {
            pointer-events: none;
            filter: grayscale(100%);
        }
    }
    .btn_double{
        background: top center no-repeat url(${Imglist['btn_double']});
        width: 140px;
        height: 37px;
        cursor: pointer;
        position: absolute;
        right: 70px;
        bottom: 75px;
        &:hover{
            background-position: center center;
        }
        &.disabled{
            background-position: bottom center;
            cursor: default;
        }
        &.grey {
            pointer-events: none;
            filter: grayscale(100%);
        }
    }
    .received{
        position: absolute;
        left: 87px;
        top: 80px;
    }
    .morethan25{
        .btn_receive{
            bottom: 210px;
            top: auto;
            right: 85px;
        }
        .btn_double{
            bottom: 130px;
            top: auto;
            right: 85px;
        }
        .received {
            top: 410px;
            left: 480px;
        }
    }
`

const MissionPast = styled.div `
    background: top center no-repeat url(${Imglist['bg_gradient']});
    padding-top: 20px;
    margin-top: 70px;
    .header{
        margin: 30px 0;
    }
    .slider_vertical{
        img{
            display:inline-block;
        }
        .slick-dots li.slick-active button:before{
            color: #b9a06f;
        }
        .slick-dots li button:before{
            color: #615743;
            font-size: 12px;
        }
        .btn_prev{
            width: 20px;
            height: 20px;
            position: absolute;
            left: 30%;
            bottom: -25px;
            z-index: 1;
            cursor:pointer;
            &:hover{
                opacity: .7;
            }
        }
        .btn_next{
            width: 20px;
            height: 20px;
            position: absolute;
            right: 30%;
            bottom: -25px;
            z-index: 1;
            cursor:pointer;
            &:hover{
                opacity: .7;
            }
        }
    }
    .slider_wrapper{
        position: relative;
        display:inline-block;
        margin-bottom: 0px;
    }
    .count{
        position: absolute;
        left: 202px;
        top: 148px;
        color: #fff;
        font-size: 13px;
    }
    .btn_receive{
        background: top center no-repeat url(${Imglist['btn_receive_items']});
        width: 140px;
        height: 37px;
        cursor: pointer;
        position: absolute;
        right: 70px;
        top: 75px;
        &:hover{
            background-position: center center;
        }
        &.disabled{
            background-position: bottom center;
            cursor: default;
        }
        &.grey {
            pointer-events: none;
            filter: grayscale(100%);
        }
    }
    .btn_double{
        background: top center no-repeat url(${Imglist['btn_double']});
        width: 140px;
        height: 37px;
        cursor: pointer;
        position: absolute;
        right: 70px;
        bottom: 75px;
        &:hover{
            background-position: center center;
        }
        &.disabled{
            background-position: bottom center;
            cursor: default;
        }
        &.grey {
            pointer-events: none;
            filter: grayscale(100%);
        }
    }
    .received{
        position: absolute;
        left: 87px;
        top: 80px;
    }
    .btn_unlock{
        background: top center no-repeat url(${Imglist['btn_unlock']});
        width: 140px;
        height: 37px;
        cursor: pointer;
        position: absolute;
        margin: 0 auto;
        left: 0;
        right: 0;
        bottom: 20px;
        &:hover{
            background-position: bottom center;
        }
    }
    .morethan25{
        .btn_receive{
            right: 80px;
            top: 150px;
        }
        .btn_double{
            right: 80px;
            bottom: 115px;
        }
        .received{
          width: 90px;
          right: 100px;
          left: auto;
          top: 20px;
        }
    }
`
