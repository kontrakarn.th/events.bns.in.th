import React from 'react';
import logo from '../../static/images/logo.png';
import styled from 'styled-components';

export default props=>(<BNSLogo src={logo} alt='logo'/>	)

const BNSLogo = styled.img`
    position: absolute;
    left: 160px;
    top: 5px;
    z-index: 5;
`