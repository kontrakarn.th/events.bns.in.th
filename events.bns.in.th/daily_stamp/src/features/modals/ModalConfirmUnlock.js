import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';
import {Imglist} from './../../constants/Import_Images';

const CPN = props => {
    let {modal_message, modal_item_token} = props;
    return (
        <ModalCore
            modalName="confirm"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <div className="contenttitle">ยืนยัน</div>
                <div className="contenttext" dangerouslySetInnerHTML={{__html: modal_message}} />

                <div>
                    <Btns className='confirm' onClick={()=>props.actConfirm(modal_item_token)}>

                    </Btns>
                    <Btns className='cancel' onClick={()=>props.setValues({modal_open:''})}>

                    </Btns>
                </div>
            </ModalMessageContent>

        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 476px;
    height: 336px;
    text-align: center;
    color: #FFFFFF;
    background-color: #615743;
    //background: no-repeat center url(${Imglist['modal_bg']});
    box-sizing: border-box;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttitle{
        font-family: 'Kanit-Light';
        font-size: 21px;
        background-color: #44341b;
        padding: 20px;
    }
    .contenttext{
        padding: 10px 30px;
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 21px;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 180px;
        //height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;

const Btns = styled.div`
    width: 140px;
    height: 37px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #000;
    line-height: 36px;
    font-size: 20px;
    background-size: contain;
    opacity: 0.8;
    &.confirm{
        background: no-repeat 0 0 url(${Imglist['btn_confirm']});
    }
    &.cancel{
        background: no-repeat 0 0 url(${Imglist['btn_cancel']});
    }
    &:hover{
        background-position: bottom center;
    }
`
