import React from 'react';
// import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import styled from 'styled-components';
import Logo from '../main/BNSLogo';
import { apiPost } from './../../middlewares/Api';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import {Imglist} from './../../constants/Import_Images';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirmUnlock from './../../features/modals/ModalConfirmUnlock';

class Redeem extends React.Component {

    actConfirmClaimStamp(value){
        if (value === undefined || value === ""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไม่สามารถปลดล็อคได้",
            });
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'claim_stamp', value: value};
        let successCallback = (data) => {
            if (data.status === true) {
                let stamp = data.data.stamp;
                let redeemList = [];

                for (let i = 0; i < stamp.length; i++) {
                    redeemList.push({
                        image: this.props.redeem_list[i].image,
                        stamp: this.props.redeem_list[i].stamp,
                        status: stamp[i],
                    });
                }

                this.props.setValues({
                    modal_open: "message",
                    modal_message: data.message,
                    username: data.data.username,
                    character: data.data.character_name,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters||[],
                    redeem_list: redeemList,
                });

                if (data.data.selected_char === false){
                    this.props.setValues({ modal_open: "selectcharacter" });
                }
            }else{
                this.props.setValues({
                    modal_open: "message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_CLAIM_STAMP", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true) {
                let stamp = data.data.stamp;
                let redeemList = [];

                for (let i = 0; i < stamp.length; i++) {
                    redeemList.push({
                        image: this.props.redeem_list[i].image,
                        stamp: this.props.redeem_list[i].stamp,
                        status: stamp[i],
                    });
                }

                this.props.setValues({
                    modal_open: "",
                    username: data.data.username,
                    character: data.data.character_name,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters||[],
                    redeem_list: redeemList,
                });

                if (data.data.selected_char === false){
                    this.props.setValues({ modal_open: "selectcharacter" });
                }
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }



    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    render() {
        return (
            <PageWrapper>
                <Logo/>
                <Menu page='redeem'/>
                <Content>
                    <RedeemWrapper>
                        <div className="header">
                            <img src={Imglist.redeem_header} alt="" />
                        </div>

                        <div className="redeemList">
                            {
                                this.props.redeem_list.map((item, key) => {
                                    return(
                                        <div key={key} className="redeemList__items">
                                            { key !== this.props.redeem_list.length - 1 &&
                                                <div>
                                                    <img src={item.image} alt="" />
                                                    { item.status === 'success' &&
                                                        <RedeemBtn status='success'></RedeemBtn>
                                                    }

                                                    { item.status === 'pending' &&
                                                        <RedeemBtn status='pending' onClick={() => this.actConfirmClaimStamp(key)}></RedeemBtn>
                                                    }

                                                    { item.status === 'not_success' &&
                                                        <RedeemBtn status='disable'></RedeemBtn>
                                                    }
                                                </div>
                                            }
                                        </div>
                                    )})
                            }
                        </div>
                    </RedeemWrapper>

                    <SpecialWrapper>
                        <div className="header">
                            <img src={Imglist.special_header} alt="" />
                        </div>

                        <div className="redeemList">
                            <div className="redeemList__items">
                                <img src={this.props.redeem_list[this.props.redeem_list.length - 1].image} alt="" />
                                { this.props.redeem_list[this.props.redeem_list.length - 1].status === 'success' &&
                                    <RedeemBtn status='success'></RedeemBtn>
                                }

                                { this.props.redeem_list[this.props.redeem_list.length - 1].status === 'pending' &&
                                    <RedeemBtn status='pending' onClick={() => this.actConfirmClaimStamp(this.props.redeem_list.length - 1)}></RedeemBtn>
                                }

                                { this.props.redeem_list[this.props.redeem_list.length - 1].status === 'not_success' &&
                                    <RedeemBtn status='disable'></RedeemBtn>
                                }
                            </div>
                        </div>
                    </SpecialWrapper>
                </Content>
                <ModalLoading/>
                <ModalMessage/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Redeem);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_redeem']});
    width: 1105px;
    padding-top: 122px;
    padding-bottom: 100px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const Content = styled.div`
    //background: top center no-repeat url(${Imglist['clan_score']});
    width: 922px;
    height: 2008px;
    margin: 0 auto;
    box-sizing: border-box;
    color: #ffffff;
    position: relative;
    img{
        display: block;
        margin: 0 auto;
    }

`
const RedeemWrapper = styled.div `
    .header{
        margin: 30px 0;
    }
    .redeemList{
        &__items{
            position: relative;
            margin-bottom: 20px;
        }
    }
`
const SpecialWrapper = styled.div `
    .header{
        margin: 100px 0 30px;
    }
    .redeemList{
        &__items{
            position: relative;
            margin-bottom: 20px;
        }
    }
`

const RedeemBtn = styled.div`
    background: top center no-repeat url(${Imglist['btn_redeem']});
    width: 140px;
    height: 37px;
    cursor: pointer;
    position: absolute;
    bottom: 20px;
    left: 0;
    right: 0;
    margin: 0 auto;
    ${props=>props.status === 'disable' &&
        `
            pointer-events: none;
            background-position: top center;
            filter: grayscale(100%);
        `
    }
    ${props=>props.status === 'pending' &&
        `
            background-position: center center;
        `
    }
    ${props=>props.status === 'success' &&
        `
            pointer-events: none;
            background-position: bottom center;
        `
    }
    &:hover{
        background-position: center;
    }
    &.prize1{
        top: 783px;
    }
    &.prize2{
        top: 1212px;
    }
    &.prize3{
        bottom: 98px;
    }
`
