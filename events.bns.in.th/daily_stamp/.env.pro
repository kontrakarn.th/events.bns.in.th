# REACT_APP_EVENT_PATH=/connectapi
REACT_APP_EVENT_PATH=/daily_stamp
REACT_APP_STATIC_PATH=/static/
REACT_APP_LOCALE=th-TH
REACT_APP_CSRF_COOKIE_NAME=csrf_token
REACT_APP_COOKIE_DOMAIN=.bns.in.th

# API related
REACT_APP_API_SERVER_HOST=https://events.bns.in.th
REACT_APP_API_GET_OAUTH_INFO=/oauth/get_oauth

# API FOR EVENT
REACT_APP_API_POST_EVENT_INFO=/api/daily_stamp/event_info
REACT_APP_API_POST_SELECT_CHAR=/api/daily_stamp/select_char
REACT_APP_API_CLAIM_FREE=/api/daily_stamp/claim_free
REACT_APP_API_CLAIM_DIAMOND=/api/daily_stamp/claim_diamond
REACT_APP_API_HISTORY=/api/daily_stamp/history
REACT_APP_API_UNLOCK_DAILY_QUEST=/api/daily_stamp/unlock_daily_quest
REACT_APP_API_CLAIM_STAMP=/api/daily_stamp/claim_stamp

# SSO related
#REACT_APP_SSO_APP_ID=10046
#REACT_APP_SSO_URL=https://sso.garena.com
#REACT_APP_SSO_PARAM_NAME=session_key
#REACT_APP_SSO_COOKIE_NAME=sso_session

# OAUTH related
REACT_APP_OAUTH_APP_ID=10007
REACT_APP_OAUTH_APP_NAME=bns_daily_stamp
REACT_APP_OAUTH_PLATFORM=1
REACT_APP_OAUTH_URL=https://auth.garena.com
REACT_APP_OAUTH_PARAM_NAME=token
REACT_APP_OAUTH_COOKIE_NAME=oauth_session

# JWT
REACT_APP_JWT_SECRET=LUtTEDS5M3P37RAwQZvQkFZvtwDXcrDY

# Landing Page
REACT_APP_LANDING_SERVER_HOST=https://landing.garena.in.th
REACT_APP_LANDING_JSON=/static/data/get_landing.json
