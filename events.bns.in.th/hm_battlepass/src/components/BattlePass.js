import React, { Component } from 'react'
import $ from 'jquery';
import owlFashion from '../static/images/owl-fashion.png'
import imgOwl from '../static/images/img_owl.png'

import level1 from '../static/images/number/1.png'
import level2 from '../static/images/number/2.png'
import level3 from '../static/images/number/3.png'
import level4 from '../static/images/number/4.png'
import level5 from '../static/images/number/5.png'
import level6 from '../static/images/number/6.png'
import level7 from '../static/images/number/7.png'
import level8 from '../static/images/number/8.png'
import level9 from '../static/images/number/9.png'
import level10 from '../static/images/number/10.png'
import level11 from '../static/images/number/11.png'
import level12 from '../static/images/number/12.png'
import level13 from '../static/images/number/13.png'
import level14 from '../static/images/number/14.png'
import level15 from '../static/images/number/15.png'
import level16 from '../static/images/number/16.png'
import level17 from '../static/images/number/17.png'
import level18 from '../static/images/number/18.png'
import level19 from '../static/images/number/19.png'
import level20 from '../static/images/number/20.png'
import level21 from '../static/images/number/21.png'
import level22 from '../static/images/number/22.png'
import level23 from '../static/images/number/23.png'
import level24 from '../static/images/number/24.png'
import level25 from '../static/images/number/25.png'
import level26 from '../static/images/number/26.png'
import level27 from '../static/images/number/27.png'
import level28 from '../static/images/number/28.png'
import level29 from '../static/images/number/29.png'
import level30 from '../static/images/number/30.png'
import level31 from '../static/images/number/31.png'
import level32 from '../static/images/number/32.png'
import level33 from '../static/images/number/33.png'
import level34 from '../static/images/number/34.png'
import level35 from '../static/images/number/35.png'
import level36 from '../static/images/number/36.png'
import level37 from '../static/images/number/37.png'
import level38 from '../static/images/number/38.png'
import level39 from '../static/images/number/39.png'
import level40 from '../static/images/number/40.png'

// import item1 from '../static/images/item/item1.png'
// import item2 from '../static/images/item/item2-3.png'
// import item4 from '../static/images/item/item4.png'
// import item5_1 from '../static/images/item/item5_1.png'
// import item5_2 from '../static/images/item/item5_2.png'
// import item6_15 from '../static/images/item/item6-15.png'
// import item7 from '../static/images/item/item7.png'
// import item8 from '../static/images/item/item8.png'
// import item9 from '../static/images/item/item9.png'
// import item10_1 from '../static/images/item/item10_1.png'
// import item10_2 from '../static/images/item/item10_2.png'
import diamond from '../static/images/icon_diamond.png'
import random from '../static/images/icon_random.png'
import x2 from '../static/images/icon_x2.png'
import iconMission from '../static/images/icon_mission.png'
import pass5level from '../static/images/icon_pass.png'
import unlock from '../static/images/icon_unlock.png'

import blackshark from '../static/images/item/blackshark.png'
import cat_pirate_medal from '../static/images/item/cat_pirate_medal.png'
import clock_weapon_skin from '../static/images/item/clock_weapon_skin.png'
import copper_axe_headband from '../static/images/item/copper_axe_headband.png'
import element_powder from '../static/images/item/element_powder.png'
import gentleman_pet_bag from '../static/images/item/gentleman_pet_bag.png'
import glittering_costume from '../static/images/item/glittering_costume.png'
import greatest_lucky_potion from '../static/images/item/greatest_lucky_potion.png'
import hive_queen_soul from '../static/images/item/hive_queen_soul.png'
import hive_queen_soulshield_box_no1 from '../static/images/item/hive_queen_soulshield_box_no1.png'
import hive_queen_soulshield_box_no2 from '../static/images/item/hive_queen_soulshield_box_no2.png'
import hive_queen_soulshield_box_no3 from '../static/images/item/hive_queen_soulshield_box_no3.png'
import hive_queen_wing from '../static/images/item/hive_queen_wing.png'
import hongmoon_crystal from '../static/images/item/hongmoon_crystal.png'
import hongmoon_medal from '../static/images/item/hongmoon_medal.png'
import jupiter_orb_stage1 from '../static/images/item/jupiter_orb_stage1.png'
import mercury_orb_stage1 from '../static/images/item/mercury_orb_stage1.png'
import moonstone_crystal from '../static/images/item/moonstone_crystal.png'
import owl_pet from '../static/images/item/owl_pet.png'
import pet_pod_crystal from '../static/images/item/pet_pod_crystal.png'
import precious_stones from '../static/images/item/precious_stones.png'
import raven_feather from '../static/images/item/raven_feather.png'
import raven_king_soul from '../static/images/item/raven_king_soul.png'
import sacred_vial from '../static/images/item/sacred_vial.png'
import sage_cap from '../static/images/item/sage_cap.png'
import sage_costume from '../static/images/item/sage_costume.png'
import sage_glasses from '../static/images/item/sage_glasses.png'
import snowfield_sunglasses from '../static/images/item/snowfield_sunglasses.png'
import soulstone_crystal from '../static/images/item/soulstone_crystal.png'
import tiger_fang_weapon_skin from '../static/images/item/tiger_fang_weapon_skin.png'
import venus_orb_stage1 from '../static/images/item/venus_orb_stage1.png'
import yeonhwarin_glove from '../static/images/item/yeonhwarin_glove.png'
import yin_yang_crystal from '../static/images/item/yin_yang_crystal.png'

const missionOrder = {
    1:level1,
    2:level2,
    3:level3,
    4:level4,
    5:level5,
    6:level6,
    7:level7,
    8:level8,
    9:level9,
    10:level10,
    11:level11,
    12:level12,
    13:level13,
    14:level14,
    15:level15,
    16:level16,
    17:level17,
    18:level18,
    19:level19,
    20:level20,
    21:level21,
    22:level22,
    23:level23,
    24:level24,
    25:level25,
    26:level26,
    27:level27,
    28:level28,
    29:level29,
    30:level30,
    31:level31,
    32:level32,
    33:level33,
    34:level34,
    35:level35,
    36:level36,
    37:level37,
    38:level38,
    39:level39,
    40:level40,
}

const imageItemList = {
    'blackshark' : blackshark,
    'cat_pirate_medal' : cat_pirate_medal,
    'clock_weapon_skin' : clock_weapon_skin,
    'copper_axe_headband' : copper_axe_headband,
    'element_powder' : element_powder,
    'gentleman_pet_bag' : gentleman_pet_bag,
    'glittering_costume' : glittering_costume,
    'greatest_lucky_potion' : greatest_lucky_potion,
    'hive_queen_soul' : hive_queen_soul,
    'hive_queen_soulshield_box_no1' : hive_queen_soulshield_box_no1,
    'hive_queen_soulshield_box_no2' : hive_queen_soulshield_box_no2,
    'hive_queen_soulshield_box_no3' : hive_queen_soulshield_box_no3,
    'hive_queen_wing' : hive_queen_wing,
    'hongmoon_crystal' : hongmoon_crystal,
    'hongmoon_medal' : hongmoon_medal,
    'jupiter_orb_stage1' : jupiter_orb_stage1,
    'mercury_orb_stage1' : mercury_orb_stage1,
    'moonstone_crystal' : moonstone_crystal,
    'owl_pet' : owl_pet,
    'pet_pod_crystal' : pet_pod_crystal,
    'precious_stones' : precious_stones,
    'raven_feather' : raven_feather,
    'raven_king_soul' : raven_king_soul,
    'sacred_vial' : sacred_vial,
    'sage_cap' : sage_cap,
    'sage_costume' : sage_costume,
    'sage_glasses' : sage_glasses,
    'snowfield_sunglasses' : snowfield_sunglasses,
    'soulstone_crystal' : soulstone_crystal,
    'tiger_fang_weapon_skin' : tiger_fang_weapon_skin,
    'venus_orb_stage1' : venus_orb_stage1,
    'yeonhwarin_glove' : yeonhwarin_glove,
    'yin_yang_crystal' : yin_yang_crystal,
}

export default class BattlePass extends Component {  
    constructor(props) {
      super(props)
    
      this.state = {
         /* battle:[
             {
                 level:level1,
                 mission:"กินยา",
                 require_completed:10,
                 completed_count:3,
                 has_sub:false,
                 sub_quest:[],
                 awaken:item1,
                 awakenItem:30,
                 free:item1,
                 freeItem:15,
                 can_receive: true,
                 itemTitle:"คริสตัลหินจันทรา"

             },
         ] */
      }
    }

    actionSlideTo(targetElement,index){
        // var element = document.getElementById(targetElement);
        $('html, body').animate({
            scrollTop: $('#' + targetElement).offset().top
        },500);
        // element.scrollIntoView({block: 'start', behavior: 'smooth', inline:'center'});
        this.setState({
            active:index
        })
    }

    actConfirmUnlockPaidReward(){
        // console.log("actConfirme");
        this.props.unlockPaidReward();
    }

    actconfirmMultipliedReward(e,step){
        e.preventDefault();
        this.props.multipliedReward(step);
    }
    
    actConfirmClaimReward(e,step,rewardType,txtReward){
        e.preventDefault();
        // console.log(txtReward);
        this.props.claimReward(step,rewardType,txtReward);
    }

    actConfirmTicketPass(e,step){
        e.preventDefault();
        // console.log(txtReward);
        this.props.ticketPass(step);
    }

    actConfirmTicketFivePass(e,step){
        e.preventDefault();
        // console.log(txtReward);
        this.props.ticketFivePass(step);
    }

    actConfirmRandomNewQuest(e,step){
        e.preventDefault();
        // console.log(txtReward);
        this.props.randomNewQuest(step);
    }

    render() {
        // console.log(this.props.eventInfo);
        return (
            <div className="battlepass__content-blog">
                <img src={imgOwl} alt="" className="battlepass__owl" />
                <table className="battlepass__table">
                    <thead>
                        <tr>
                            <td>L.v. <br/>(เลเวล)</td>
                            <td>Quest <br/>(ภารกิจ)</td>
                            <td>Awaken <br/>(สิทธิ์ขั้นสูง)</td>
                            <td>Free <br/>(เล่นฟรี)</td>
                            <td>Item <br/>(ไอเทมช่วยเหลือ)</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.eventInfo && this.props.eventInfo.quests && this.props.eventInfo.quests.length > 0 && this.props.eventInfo.quests.map((item,index) => {
                            // console.log(index);
                            return [
                                <tr key={'rows' + index} id={'row' + index} className={(index+1) % 5 === 0 ? 'violet ' : ' '}>
                                    <td rowSpan="2"> <img src={missionOrder[index+1]} /></td>
                                    <td rowSpan="2">
                                        <div className="battlepass__quest-title">
                                             <p className="">{item.quest_title}</p>
                                            {
                                                item.active == true ?
                                                    (
                                                        item.sub_quests && item.sub_quests.length > 0 ?
                                                            <ul className="battlepass__quest-inner">
                                                                {item.sub_quests && item.sub_quests.length > 0 && item.sub_quests.map((itemInner, indexInner) => {
                                                                    return (
                                                                        <li className="sub-quest" key={indexInner}>{itemInner.title} <span>{itemInner.count_completed} / {itemInner.count_required}</span></li>
                                                                    )
                                                                })}
                                                            </ul>
                                                        :
                                                            <p className="battlepass__quest-mission">
                                                                {item.count_completed} / {item.count_required}
                                                            </p>
                                                    )
                                                :
                                                    null
                                            }
                                        </div>
                                        {(index+1) % 5 === 0 ?
                                                null
                                            :
                                                (
                                                    item.active == true ?
                                                        (
                                                            item.can_random == true && item.status == false ?
                                                                <a className="btns__text" onClick={(event,step)=>this.actConfirmRandomNewQuest(event,item.step)}>
                                                                    <img className="battlepass__icons" src={random} alt="สุ่มภารกิจ" />สุ่มภารกิจ <br/>500 <img src={diamond}  alt="diamond" />
                                                                </a>
                                                            :
                                                                <a className="btns__text received">
                                                                    <img className="battlepass__icons" src={random} alt="สุ่มภารกิจ" />สุ่มภารกิจ <br/>500 <img src={diamond}  alt="diamond" />
                                                                </a>
                                                        )
                                                        
                                                    :
                                                        null
                                                )
                                                
                                        }
                                        
                                    </td>
                                    
                                    {
                                        item.active == true ?
                                            <td className="nopadding">
                                                {
                                                    this.props.eventInfo.can_unlock_paid_reward == true ?
                                                        <div className="battlepass__lock" onClick={this.actConfirmUnlockPaidReward.bind(this)}>
                                                            <div><img src={unlock} alt="" /></div>
                                                            <span>ปลดล็อคสิทธิ์ขั้นสูงทั้งหมด 20,000 <img src={diamond} alt="diamond" /><br />คลิ๊ก</span>
                                                        </div>
                                                    :
                                                        null
                                                }
                                                
                                                <img src={imageItemList[item.paid_reward.reward_key]} alt="" />
                                                <p>{item.paid_reward.title}</p>
                                                <p>({item.paid_reward.amount})</p>
                                                {
                                                    this.props.eventInfo.is_unlock_paid_reward == true ?
                                                        (
                                                            item.paid_reward.received == true ?
                                                                <a className={"btns__reward disable"}>รับไปแล้ว</a>
                                                            :
                                                                (
                                                                    item.paid_reward.can_receive == true ?
                                                                        <a className={"btns__reward"} onClick={(event,step,rewardType,txtReward)=>this.actConfirmClaimReward(event,item.step,'paid',item.paid_reward.title+' (x'+item.paid_reward.amount+')')}>รับไอเทม</a>
                                                                    :
                                                                        <a className={"btns__reward disable"}>ไม่ตรงเงื่อนไข</a>
                                                                )
                                                        )
                                                    :
                                                        <a className={"btns__reward disable"}>ไม่ตรงเงื่อนไข</a>
                                                }
                                                
                                            </td>
                                        :
                                            (
                                                (index+1) % 5 === 0 ?
                                                    <td className="nopadding">
                                                        {
                                                            this.props.eventInfo.can_unlock_paid_reward == true ?
                                                                <div className="battlepass__lock" onClick={this.actConfirmUnlockPaidReward.bind(this)}>
                                                                    <div><img src={unlock} alt="" /></div>
                                                                    <span>ปลดล็อคสิทธิ์ขั้นสูงทั้งหมด 20,000 <img src={diamond} alt="diamond" /><br />คลิ๊ก</span>
                                                                </div>
                                                            :
                                                                null
                                                        }
                                                        <img src={imageItemList[item.paid_reward.reward_key]} alt="" />
                                                        <p>{item.paid_reward.title}</p>
                                                        <p>({item.paid_reward.amount})</p>
                                                    </td>
                                                :
                                                    <td className="nopadding"></td>
                                            )
                                    }
                                    {
                                        item.active == true ?
                                            <td>
                                                <img src={imageItemList[item.free_reward.reward_key]} alt="" />
                                                <p>{item.free_reward.title}</p>
                                                <p>({item.free_reward.amount})</p>
                                                {
                                                    item.free_reward.received == true ?
                                                        <a className={"btns__reward disable"}>รับไปแล้ว</a>
                                                    :
                                                        (
                                                            item.free_reward.can_receive == true ?
                                                                <a className={"btns__reward"} onClick={(event,step,rewardType,txtReward)=>this.actConfirmClaimReward(event,item.step,'free',item.free_reward.title+' (x'+item.free_reward.amount+')')}>รับไอเทม</a>
                                                            :
                                                                <a className={"btns__reward disable"}>ไม่ตรงเงื่อนไข</a>
                                                        )
                                                }
                                            </td>
                                        :
                                            (
                                                (index+1) % 5 === 0 ?
                                                    <td>
                                                        <img src={imageItemList[item.free_reward.reward_key]} alt="" />
                                                        <p>{item.free_reward.title}</p>
                                                        <p>({item.free_reward.amount})</p>
                                                    </td>
                                                :
                                                    <td></td>
                                            )
                                            
                                    }
                                    <td rowSpan="2">
                                        {   
                                            (index === 0) && item.active == true ?
                                            (
                                                this.props.eventInfo.can_ticket_5_pass == true ?
                                                    <a className="btns__text" onClick={(event,step)=>this.actConfirmTicketFivePass(event,item.step)}><img className="battlepass__icons" src={pass5level} /> ข้ามเลเวล <br/>30,000 <img src={diamond} alt="diamond" /></a>
                                                :
                                                    <a className="btns__text received"><img className="battlepass__icons" src={pass5level} /> ข้ามเลเวล <br/>30,000 <img src={diamond} alt="diamond" /></a>
                                            )
                                                
                                            :
                                                null
                                        }
                                        {
                                            item.active == true ?
                                                (
                                                    this.props.eventInfo.ticket_pass.remain > 0 && item.status == false ?
                                                        <a className="btns__text" onClick={(event,step)=>this.actConfirmTicketPass(event,item.step)}>
                                                            <span><img className="battlepass__icons" src={iconMission} /> สำเร็จภารกิจ <span className="text--small">{'('+this.props.eventInfo.ticket_pass.used+'/'+this.props.eventInfo.ticket_pass.total+')'}</span></span>
                                                        <br/>10,000 <img src={diamond} alt="diamond" /></a>
                                                    :
                                                        <a className="btns__text received">
                                                            <span><img className="battlepass__icons" src={iconMission} /> สำเร็จภารกิจ <span className="text--small">{'('+this.props.eventInfo.ticket_pass.used+'/'+this.props.eventInfo.ticket_pass.total+')'}</span></span>
                                                        <br/>10,000 <img src={diamond} alt="diamond" /></a>
                                                )
                                                
                                            :
                                                null
                                        }
                                        
                                    </td>
                                </tr>,
                                <tr className={(index+1) % 5 === 0 ? 'violet ' : '' + ("border")}>
                                    <td colSpan="2">
                                        {(index+1) % 5 === 0 ?
                                            null
                                            :
                                                (
                                                    item.active == true ?
                                                        (
                                                            item.can_multiplied_reward == true ?
                                                                <a className="btns__text" onClick={(event,step)=>this.actconfirmMultipliedReward(event,item.step)}><img className="battlepass__icons" src={x2} /> รางวัล x2 <br/>5,000 <img src={diamond} alt="diamond" /></a>
                                                            :
                                                                <a className="btns__text received"><img className="battlepass__icons" src={x2} /> รางวัล x2 <br/>5,000 <img src={diamond} alt="diamond" /></a>
                                                        )
                                                        
                                                    :
                                                        null
                                                )
                                        }
                                        
                                    </td>
                                </tr>,

                                [index === 4 ?
                                    <tr className="battlepass__img">
                                        <td colSpan="5"><img src={owlFashion} /></td>
                                    </tr>
                                    :
                                    false
                                ]
                            ]
                        })}
                    </tbody>
                </table>
            </div>
        )
  }
}
