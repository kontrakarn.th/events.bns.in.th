import React, { Component } from 'react'
import iconDropdown from './../static/images/icon_dropdown.png'
export default class ModalSelectCharacter extends Component {

    actSelectCharacter(e){
        e.preventDefault();
        let indexCharacter = document.getElementById("charater_form").value;
        let idCharacter = this.props.listCharater[indexCharacter].id;
        let nameCharacter = this.props.listCharater[indexCharacter].char_name;
        this.props.actConfirme(idCharacter,nameCharacter)
    }
    render() {
        return (
            <div className={"modal "+(this.props.open ? "":"close")} >
                <div className="modal__backdrop" />
                <div className={"modal__content"}>
                    <div className={"modal__incontent modal__incontent--selectcharacter"}>
                        เลือกตัวละคร<br />
                        <form className="formselect">
                            <label >
                                <select id="charater_form" className="fromselect__list">
                                    {this.props.open && this.props.listCharater && this.props.listCharater.map((item,index)=>{
                                        return (
                                            <option key={index} value={index}>{item.char_name}</option>
                                        )
                                    })}
                                </select>
                                <img src={iconDropdown} alt="" />
                            </label>
                        </form>
                    </div>
                    <div className={"modal__bottom"}>
                        <a
                            onClick={this.actSelectCharacter.bind(this)}
                            className="modal__button"
                        >
                            ตกลง
                        </a>
                    </div>
                </div>
            </div>
        )
  }
}
