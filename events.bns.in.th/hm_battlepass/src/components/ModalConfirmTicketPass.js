import React, { Component } from 'react'
import diamond from '../static/images/icon_diamond.png'

export default class ModalConfirmTicketPass extends Component {
    actConfirme(){
        // console.log("actConfirme");
        this.props.actConfirme();
    }
    actCancel(){
        // console.log("actCancel");
        this.props.actCancel();
    }
    render() {
        return (
            <div className={"modal "+(this.props.open ? "":"close")} >
                <div className="modal__backdrop" />
                <div className={"modal__content"}>
                    <div className={"modal__incontent modal__incontent--selectcharacter"}>
                        ยืนยัน<br />
                        ใช้บัตรผ่าน 10,000 <img src={diamond} alt="diamond" /><br />
                        สำหรับสำเร็จภารกิจนี้<br />
                        หรือไม่?
                    </div>
                    <div className={"modal__bottom"}>
                        <a
                            onClick={this.actConfirme.bind(this)}
                            className="modal__button"
                        >
                            ยืนยัน
                        </a>
                        <a
                            onClick={this.actCancel.bind(this)}
                            className="modal__button"
                        >
                            ยกเลิก
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}
