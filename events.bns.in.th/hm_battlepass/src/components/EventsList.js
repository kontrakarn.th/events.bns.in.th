import React, { Component } from 'react'
import Swiper from 'react-id-swiper';
export default class EventsList extends Component {
    render() {
        const params = {
            slidesPerView: 5,
            slidesPerColumn: 3,
            slidesPerGroup:1,
            // slidesPerColumnFill:'row',
            spaceBetween: 0,
            pagination: {
              el: '.events-bns-slide__dot',
              clickable: true,
            }
          };
        return (
            <ul className="events-bns__list">
                <Swiper {...params}>
                {this.props.appList.map((item,index) => {
                    return (
                        <li key={index}>
                            <a href={item.url}>
                                <img src={item.img} alt="" />
                                {item.name}
                            </a>
                        </li>
                    )
                })}
                </Swiper>
            </ul>
            
        )
  }
}
