import React, { Component } from 'react'
import up from '../static/images/icon_up.png'
import down from '../static/images/icon_down.png'

export default class Menu extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         items:[],
         list1:[
             '1','2','3','4','5','6','7','8','9','10',
             '11','12','13','14','15','16','17','18','19','20'
         ],
         list2:[
             '21','22','23','24','25','26','27','28','29','30',
             '31','32','33','34','35','36','37','38','39','40'
         ],
         visible:false,
         active:0,
         scrolling:false,
         index:0
      }
      this.handleScroll = this.handleScroll.bind(this)
      this.randomItem = this.randomItem.bind(this)
    }
    onSlide() {
        this.setState({
            visible:!this.state.visible,
        })
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
        // this.randomItem(this.state.items)
    }

    actionSlideTo(targetElement,index){
        var element = document.getElementById(targetElement);
        element.scrollIntoView({block: 'start', behavior: 'smooth', inline:'center'});
        this.setState({
            active:index
        })
    }
    randomItem() {
        var newItems = this.state.items;
        for(var i=1; i<=40; i++) {
            newItems.push(i)
        }
        this.setState({
            items:newItems
        })
    }
    handleShow(i) {
        this.setState({
            index:i
        })
        var element = document.getElementById('jom')
        element.scrollIntoView({block: 'end', behavior: 'smooth'});
    }
    
    handleScroll() {
        if (window.scrollY === 0 && this.state.scrolling === true) {
            this.setState({scrolling: false});
        }
        else if (window.scrollY > 500 && this.state.scrolling !== true) {
            this.setState({scrolling: true});
        }
    }

    
    render() {
        return (
            <div className={"battlepass-menu " + (this.state.scrolling ? 'battlepass-menu--show' : '')} id="battlemenu">
                <a className="btns__up" onClick={()=>this.onSlide()}><img src={up} alt="" /></a>
                <ul className={"battlepass-menu__list "}>
                    {this.state.visible ?
                        this.state.list2.map((item,index) => {
                            return (
                                <li key={index} className={this.state.active === index+20 ? 'active' : ''}>
                                    <a href="javascript:void(0)" onClick={()=>this.actionSlideTo(('row'+(index+20)),index)}>{item}</a>
                                </li>
                            )
                        })
                        :
                        this.state.list1.map((item,index) => {
                            return (
                                <li key={index} className={this.state.active === index ? 'active' : ''}>
                                    <a onClick={()=>this.actionSlideTo('row'+index,index)}>{item}</a></li>
                            )
                        })
                    }
                </ul>
                <a className="btns__bottom"onClick={()=>this.onSlide()}><img src={down} alt="" /></a>
            </div>
        )
  }
}
