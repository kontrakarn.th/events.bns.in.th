import React, { Component } from 'react'
import headText from './../static/images/head_text.png'
import iconScroll from './../static/images/icon_scroll.png'
export default class Head extends Component {
    render() {
        return (
            <div className="events-head">
                <div className="events-head__account">{ this.props.char_name || ''}</div>
                <div className="events-head__title">
                    <img src={headText} alt="Hongmoon Pass ภารกิจพิชิตกาลเวลา" />
                </div>
                <div className="events-head__scroll"><img src={iconScroll} alt="" /></div>
            </div>
        )
  }
}
