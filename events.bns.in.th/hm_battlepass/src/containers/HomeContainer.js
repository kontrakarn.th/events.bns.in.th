import React, { Component } from 'react'
import {connect} from 'react-redux'
// import ScrollArea from 'react-scrollbar';
import { Scrollbars } from 'react-custom-scrollbars';
import 'whatwg-fetch'
import './../styles/index.css'

import {apiPost} from './../middlewares/Api';
import Head from '../components/Head';
import BattlePass from '../components/BattlePass'
import Menu from '../components/Menu'
import ModalSelectCharacter from '../components/ModalSelectCharacter'
import ModalConfirmCharacter from '../components/ModalConfirmCharacter'
import ModalConfirmUnlockPaidReward from '../components/ModalConfirmUnlockPaidReward'
import ModalConfirmMultipliedReward from '../components/ModalConfirmMultipliedReward'
import ModalConfirmClaimReward from '../components/ModalConfirmClaimReward'
import ModalConfirmTicketPass from '../components/ModalConfirmTicketPass'
import ModalConfirmTicketFivePass from '../components/ModalConfirmTicketFivePass'
import ModalConfirmRandomNewQuest from '../components/ModalConfirmRandomNewQuest'
import ModalMessage from '../components/ModalMessage'
import ModalLoading from '../components/ModalLoading';

import {Permission} from './../components/Permission';

import home from './../static/images/btn_home.png'

import part3_box from './../static/images/part3_box.png'

class HomeContainer extends Component {

    // Start API

    apiPostEventInfo(){
        let self = this;
        let apiName="REACT_APP_API_POST_EVENT_INFO";
        let dataSend={ type : "event_info" };
        let successCallback = (data)=>{
            self.setCloseLoading();
            self.setState({
                selected_char_name: data.content.char_name,
                eventInfo: data.content,
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }
            if(data.type === "no_char"){
                this.apiGetCharacter();
            }
        };
        apiPost(apiName,dataSend,successCallback,failCallback);
    }

    apiGetCharacter(){
        this.setState({modalLoading:true});
        let self = this;
        let apiName="REACT_APP_API_POST_GET_CHARACTER";
        let dataSend={ type : "get_characters" };
        let successCallback = (data)=>{
            self.setCloseLoading();
            self.setState({
                modalSelectCharacter: true,
                listCharater: data.content,
            });
        };
        let failCallback = (data)=>{
            self.setCloseLoading();
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }
        };
        apiPost(apiName,dataSend,successCallback,failCallback);
    }

    apiAcceptCharacter(_id){
        let self = this;
        this.setState({modalLoading:true});
        let apiName="REACT_APP_API_POST_ACCEPT_CHARACTER";
        let dataSend={ type : "accept_character", id: _id };
        let successCallback = (data)=>{
            self.setCloseLoading();
            self.setState({
                modalSelectCharacter: false,
                modalConfirmCharacter:false,
                selected_char_name: data.content.char_name,
                eventInfo: data.content
            });
        };
        let failCallback = (data)=>{
            self.setCloseLoading();
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }else{
                this.setState({
                    modalSelectCharacter: false,
                    modalConfirmCharacter:false,
                    modalShowMessage: true,
                    modalMessage: data.message,
                })
            }
        };
        apiPost(apiName,dataSend,successCallback,failCallback);
    }

    apiUnlockPaidReward(){
        let self = this;
        this.setState({
            modalLoading:true,
            modalConfirmUnlockPaid:false,
        });
        let apiName="REACT_APP_API_POST_UNLOCK_SPECIAL_REWARD";
        let dataSend={ type : "unlock_special_reward" };
        let successCallback = (data)=>{
            self.setCloseLoading();
            self.setState({
                modalConfirmUnlockPaid:false,
                selected_char_name: data.content.char_name,
                eventInfo: data.content
            });
        };
        let failCallback = (data)=>{
            self.setCloseLoading();
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }else{
                this.setState({
                    modalConfirmUnlockPaid: false,
                    modalShowMessage: true,
                    modalMessage: data.message,
                })
            }
        };
        apiPost(apiName,dataSend,successCallback,failCallback);
    }

    apiMultipliedReward(_step){
        let self = this;
        this.setState({
            modalLoading:true,
            modalConfirmMultipliedReward: false,
        });
        let apiName="REACT_APP_API_POST_MULTIPLIED_REWARD";
        let dataSend={ type : "multiplied_reward", step: _step };
        let successCallback = (data)=>{
            self.setCloseLoading();
            self.setState({
                step:0,
                modalConfirmMultipliedReward:false,
                selected_char_name: data.content.char_name,
                eventInfo: data.content
            });
        };
        let failCallback = (data)=>{
            self.setCloseLoading();
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }else{
                this.setState({
                    step:0,
                    modalConfirmMultipliedReward: false,
                    modalShowMessage: true,
                    modalMessage: data.message,
                })
            }
        };
        apiPost(apiName,dataSend,successCallback,failCallback);
    }

    apiClaimReward(_step,_rewardType){
        let self = this;
        this.setState({
            modalLoading:true,
            modalConfirmClaimReward:false,
        });
        let apiName="REACT_APP_API_POST_REDEEM";
        let dataSend={ type : "redeem_reward", step: _step, reward_type: _rewardType };
        let successCallback = (data)=>{
            self.setCloseLoading();
            self.setState({
                step:0,
                modalShowMessage: true,
                modalMessage: data.message,
                txtClaimReward: "",
                selected_char_name: data.content.char_name,
                eventInfo: data.content
            });
        };
        let failCallback = (data)=>{
            self.setCloseLoading();
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }else{
                this.setState({
                    step:0,
                    txtClaimReward: "",
                    modalShowMessage: true,
                    modalMessage: data.message,
                })
            }
        };
        apiPost(apiName,dataSend,successCallback,failCallback);
    }

    apiTicketPass(_step){
        let self = this;
        this.setState({
            modalLoading:true,
            modalConfirmTicketPass:false,
        });
        let apiName="REACT_APP_API_POST_TICKET_PASS";
        let dataSend={ type : "ticket_pass", step: _step };
        let successCallback = (data)=>{
            self.setCloseLoading();
            self.setState({
                step:0,
                modalConfirmTicketPass:false,
                selected_char_name: data.content.char_name,
                eventInfo: data.content
            });
        };
        let failCallback = (data)=>{
            self.setCloseLoading();
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }else{
                this.setState({
                    step:0,
                    modalConfirmTicketPass: false,
                    modalShowMessage: true,
                    modalMessage: data.message,
                })
            }
        };
        apiPost(apiName,dataSend,successCallback,failCallback);
    }

    apiTicketFivePass(_step){
        let self = this;
        this.setState({
            modalLoading:true,
            modalConfirmTicketFivePass:false,
        });
        let apiName="REACT_APP_API_POST_TICKET_FIVE_PASS";
        let dataSend={ type : "ticket_five_pass", step: _step };
        let successCallback = (data)=>{
            self.setCloseLoading();
            self.setState({
                step:0,
                modalConfirmTicketFivePass:false,
                selected_char_name: data.content.char_name,
                eventInfo: data.content
            });
        };
        let failCallback = (data)=>{
            self.setCloseLoading();
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }else{
                this.setState({
                    step:0,
                    modalConfirmTicketFivePass: false,
                    modalShowMessage: true,
                    modalMessage: data.message,
                })
            }
        };
        apiPost(apiName,dataSend,successCallback,failCallback);
    }

    apiRandomNewQuest(_step){
        let self = this;
        this.setState({
            modalLoading:true,
            modalConfirmRandomNewQuest:false,
        });
        let apiName="REACT_APP_API_POST_RANDOM_MISSION";
        let dataSend={ type : "random_new_quest", step: _step };
        let successCallback = (data)=>{
            self.setCloseLoading();
            self.setState({
                step:0,
                modalConfirmRandomNewQuest:false,
                selected_char_name: data.content.char_name,
                eventInfo: data.content
            });
        };
        let failCallback = (data)=>{
            self.setCloseLoading();
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }else{
                this.setState({
                    step:0,
                    modalConfirmRandomNewQuest: false,
                    modalShowMessage: true,
                    modalMessage: data.message,
                })
            }
        };
        apiPost(apiName,dataSend,successCallback,failCallback);
    }

    // End API

    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            modalSelectCharacter:false,
            modalConfirmCharacter:false,
            modalConfirmToRedeem:false,
            redeem:false,
            permission: false,
            modalLoading: true,
            modalShowMessage: false,
            modalMessage: '',
            transform:0,
            showMenu:false,

            listCharater: [],
            char_name: "",
            selected_char_name: "",

            step: 0,
            reward_type: "",
            txtClaimReward: "",
            modalConfirmUnlockPaid: false,
            modalConfirmMultipliedReward: false,
            modalConfirmClaimReward: false,
            modalConfirmTicketPass: false,
            modalConfirmTicketFivePass: false,
            modalConfirmRandomNewQuest: false,
        }
    }
    componentDidMount() {
    }

    openModal(){
        this.setState({
            modalConfirmToRedeem:true
        })
    }
    closeModal(){
        this.setState({
            modalSelectCharacter: false,
            modalConfirmCharacter: false,
            modalLoading: false,
            modalConfirmToRedeem: false,
            modalShowMessage: false,
            modalMessage: '',
            txtClaimReward: "",
            characters: [],
        });
    }

    componentWillMount() {

        this.apiPostEventInfo()
    }

    setCloseLoading(){
        setTimeout(()=>{ this.setState({modalLoading:false}); },1500);
    }

    actConfirmSelectCharacter(){
        this.apiAcceptCharacter(this.state.char_id);
        this.closeModal();
    }

    actSelectCharacter(idCharacter,nameCharacter){
        // console.log(nameCharacter);
        this.setState({
            char_id: idCharacter,
            char_name: nameCharacter,
            modalConfirmCharacter:true
        });
    }

    actUnlockPaidReward(){
        this.setState({
            modalConfirmUnlockPaid:true
        });
    }

    actConfirmUnlockPaidReward(){
        this.apiUnlockPaidReward();
    }

    actMultipliedReward(_step){
        this.setState({
            modalConfirmMultipliedReward:true,
            step: _step
        });
    }

    actConfirmMultipliedReward(){
        this.apiMultipliedReward(this.state.step);
    }

    actClaimReward(_step,rewardType,txtReward=''){
        this.setState({
            modalConfirmClaimReward:true,
            step: _step,
            reward_type: rewardType,
            txtClaimReward: txtReward,
        });
    }

    actConfirmClaimReward(){
        this.apiClaimReward(this.state.step,this.state.reward_type);
    }

    actTicketPass(_step){
        this.setState({
            modalConfirmTicketPass:true,
            step: _step,
        });
    }

    actConfirmTicketPass(){
        this.apiTicketPass(this.state.step);
    }

    actTicketFivePass(_step){
        this.setState({
            modalConfirmTicketFivePass:true,
            step: _step,
        });
    }

    actConfirmTicketFivePass(){
        this.apiTicketFivePass(this.state.step);
    }

    actRandomNewQuest(_step){
        this.setState({
            modalConfirmRandomNewQuest:true,
            step: _step,
        });
    }

    actConfirmRandomNewQuest(){
        this.apiRandomNewQuest(this.state.step);
    }

    render() {
        return (
            <div className="events-bns">

                {
                    this.state.permission && this.state.permission == true ?
                        <Permission open={this.state.permission} />
                    :
                        <Scrollbars
                            style={{width:'100%',height: 700, }}
                            renderThumbVertical={({ style, ...props }) =>
                                <div className="" {...props} style={{ ...style, backgroundColor: 'rgba(231,215,181,0.6)' }}/>
                            }
                        >
                            <div className="battlepass">
                                <Head
                                    userData={this.props.userData}
                                    char_name={this.state.selected_char_name || ''}
                                />
                                <Menu />
                                <BattlePass
                                    eventInfo={this.state.eventInfo}
                                    unlockPaidReward={()=>this.actUnlockPaidReward()}
                                    multipliedReward={(step)=>this.actMultipliedReward(step)}
                                    claimReward={(step,rewardType,txtReward)=>this.actClaimReward(step,rewardType,txtReward)}
                                    ticketPass={(step)=>this.actTicketPass(step)}
                                    ticketFivePass={(step)=>this.actTicketFivePass(step)}
                                    randomNewQuest={(step)=>this.actRandomNewQuest(step)}
                                />
                                <div className="battlepass__fashion">
                                    <img src={part3_box} alt="fashion" />
                                </div>
                                <ModalMessage
                                    open={this.state.modalShowMessage}
                                    msg={this.state.modalMessage}
                                    closeModal={this.closeModal.bind(this)}
                                />
                                <ModalLoading
                                    open={this.state.modalLoading}
                                />
                                <ModalSelectCharacter
                                    open={this.state.modalSelectCharacter}
                                    listCharater={this.state.listCharater || []}
                                    actConfirme={(idCharacter,nameCharacter)=>this.actSelectCharacter(idCharacter,nameCharacter)}
                                    actCancel={()=>this.setState({modalSelectCharacter: false})}
                                />
                                <ModalConfirmCharacter
                                    char_name={this.state.char_name}
                                    open={this.state.modalConfirmCharacter}
                                    actConfirme={()=>this.actConfirmSelectCharacter()}
                                    actCancel={()=>this.setState({modalConfirmCharacter:false,char_name:""})}
                                />
                                <ModalConfirmUnlockPaidReward
                                    open={this.state.modalConfirmUnlockPaid}
                                    actConfirme={()=>this.actConfirmUnlockPaidReward()}
                                    actCancel={()=>this.setState({modalConfirmUnlockPaid:false})}
                                />
                                <ModalConfirmMultipliedReward
                                    open={this.state.modalConfirmMultipliedReward}
                                    actConfirme={()=>this.actConfirmMultipliedReward()}
                                    actCancel={()=>this.setState({modalConfirmMultipliedReward:false,step:0})}
                                />
                                <ModalConfirmClaimReward
                                    open={this.state.modalConfirmClaimReward}
                                    msg={this.state.txtClaimReward}
                                    actConfirme={()=>this.actConfirmClaimReward()}
                                    actCancel={()=>this.setState({modalConfirmClaimReward:false,step:0,reward_type:""})}
                                />
                                <ModalConfirmTicketPass
                                    open={this.state.modalConfirmTicketPass}
                                    actConfirme={()=>this.actConfirmTicketPass()}
                                    actCancel={()=>this.setState({modalConfirmTicketPass:false,step:0})}
                                />
                                <ModalConfirmTicketFivePass
                                    open={this.state.modalConfirmTicketFivePass}
                                    actConfirme={()=>this.actConfirmTicketFivePass()}
                                    actCancel={()=>this.setState({modalConfirmTicketFivePass:false,step:0})}
                                />
                                <ModalConfirmRandomNewQuest
                                    open={this.state.modalConfirmRandomNewQuest}
                                    actConfirme={()=>this.actConfirmRandomNewQuest()}
                                    actCancel={()=>this.setState({modalConfirmRandomNewQuest:false,step:0})}
                                />
                                <div className="battlepass__home">
                                    <a href={process.env.REACT_APP_API_SERVER_HOST}>
                                        <img src={home} alt="home" />
                                    </a>
                                </div>
                            </div>
                        </Scrollbars>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
	loginUrl: state.AccountReducer.loginUrl,
	logoutUrl: state.AccountReducer.logoutUrl,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
