import bg from '../static/images/background.png';

import slide1 from '../static/images/slide/1.png'
import slide2 from '../static/images/slide/2.png'
import slide3 from '../static/images/slide/3.png'
import slide4 from '../static/images/slide/4.png'
import slide5 from '../static/images/slide/5.png'

import modal_bg from '../static/images/modal_bg.png'
import btn_default from '../static/images/btn_default.png'

import content1 from '../static/images/content1.png'
import content2 from '../static/images/content2.png'
import content3 from '../static/images/content3.png'
import btn_prev from '../static/images/btn_prev.png'
import btn_next from '../static/images/btn_next.png'
import bg_sidebar from '../static/images/bg_sidebar.png'
import sidebar_icon from '../static/images/sidebar_icon.png'

//https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival
var quest1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/1.png'
var quest3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/3.png'
var quest2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/2.png'
var quest4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/4.png'
var quest5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/5.png'
var quest6 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/6.png'
var quest7 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/7.png'
var quest8 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/8.png'
var quest9 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/9.png'
var quest10 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/10.png'
var quest11 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/11.png'
var quest12 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/12.png'
var quest13 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/13.png'
var quest14 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival/quest/14.png'

export const Imglist = {
	bg,
	modal_bg,
	btn_default,
	slide1,
	slide2,
	slide3,
	slide4,
	slide5,
	content1,
	content2,
	content3,
	btn_prev,
	btn_next,
	bg_sidebar,
	sidebar_icon,

	quest1, 
	quest3, 
	quest2, 
	quest4, 
	quest5, 
	quest6, 
	quest7, 
	quest8, 
	quest9, 
	quest10,
	quest11,
	quest12,
	quest13,
	quest14,

}
