import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import {Imglist} from './../../constants/Import_Images';
import ModalConfirmReceive from '../modals/ModalConfirmReceive';
import ModalSelectCharacter from "../modals/ModalSelectCharacter";

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div className={className} onClick={onClick}><img src={Imglist.btn_next}/></div>
    );
}
  
function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div className={className} onClick={onClick}><img src={Imglist.btn_prev}/></div>
    );
}

class Main extends React.Component {

    actConfirm(token){
        if (token === undefined || token === ""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไม่สามารถแลกของไอเทมได้",
            });
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'redeem',token:token};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                    coin:data.data.coin,
                });
                this.setState({
                    itemLists : data.data.itemLists,
                    questLists : data.data.questLists,
                })
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
        // this.apiRedeem(
        //     this.props.modal_confirmitem && this.props.modal_confirmitem !== ''
        //     && this.props.modal_confirmitem
        // );
    }

    apiSelectCharacter(id){
        if (id === undefined || id === ""){
            return;
        }
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'select_character',id:id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selectd_char: data.data.selectd_char,
                    characters: data.data.characters,
                    coin:data.data.coin,
                    modal_open: "",
                });
                this.setState({
                    itemLists : data.data.itemLists,
                    questLists : data.data.questLists,
                })
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true){
                this.setState({
                    itemLists : data.data.itemLists,
                    questLists : data.data.questLists,
                })
                this.props.setValues({
                    modal_open: "",
                    username: data.data.username,
                    character_name: data.data.character_name,
                    coin: data.data.coin,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters,
                });

                if (data.data.selected_char === false){
                    this.props.setValues({ modal_open: "selectcharacter" });
                }
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiRedeem(type_id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type_id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content.response,
                    modal_open:"message",
                    modal_message: "ส่งของรางวัลสำเร็จ<br/>กรุณาตรวจสอบจดหมายภายในเกม"
                });

            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,

            active: [],
            //itemLists: [],
            questLists : [],
            quest: [
                {
                    image: Imglist.quest1,
                    complete: false,
                },
                {
                    image: Imglist.quest2,
                    complete: false,
                },
                {
                    image: Imglist.quest3,
                    complete: false,
                },
                {
                    image: Imglist.quest4,
                    complete: false,
                },
                {
                    image: Imglist.quest5,
                    complete: false,
                },
                {
                    image: Imglist.quest6,
                    complete: false,
                },
                {
                    image: Imglist.quest7,
                    complete: false,
                },
                {
                    image: Imglist.quest8,
                    complete: false,
                },
                {
                    image: Imglist.quest9,
                    complete: false,
                },
                {
                    image: Imglist.quest10,
                    complete: false,
                },
                {
                    image: Imglist.quest11,
                    complete: false,
                },
                {
                    image: Imglist.quest12,
                    complete: false,
                },
                {
                    image: Imglist.quest13,
                    complete: false,
                },
                {
                    image: Imglist.quest14,
                    complete: false,
                },
            ],
            itemLists: [],
            slider_image: [
                {
                    image: Imglist.slide1,
                    title: 'ชุดชาวเล'
                },
                {
                    image: Imglist.slide2,
                    title: 'ชุดผู้ฝึกฝนฮงมุน'
                },
                {
                    image: Imglist.slide3,
                    title: 'ชุดพักผ่อนวันหยุด'
                },
                {
                    image: Imglist.slide4,
                    title: 'กงจักรนรกโลกันต์'
                },
                {
                    image: Imglist.slide5,
                    title: 'กงจักรโลกันต์'
                },
            ]
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }

    render() {
        const settings = {
            dots: true,
            infinite: true,
            fade: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        return (
            <PageWrapper>
                <Content>
                    {/* section 1 */}
                    <div className="section section1">
                        {
                            this.state.quest.map((items,key)=>{
                                return(  
                                    <div key={key} className={"questItems " + (this.state.questLists && this.state.questLists.length > 0 ? this.state.questLists[key] !== false ? "" : "disabled" : "disabled") }>
                                        <img src={items.image}/>
                                    </div>
                                )
                            })
                        }  
                    </div>

                    {/* section 2 */}
                    <div className="section section2">
                        {
                            this.state.itemLists.map((items,key)=>{
                                return(  
                                    <div key={key} className="items">
                                        <div className="items__img">
                                            <img src={items.image}/>
                                        </div>
                                        <div className="items__name">{items.name}</div>
                                        {
                                            items.canRedeem === false ?
                                                <>
                                                    <div className={"btn btn-exchange inactive"}>แลกไอเทม</div>
                                                </>
                                                :
                                                <>
                                                    <div className={"btn btn-exchange"} onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการแลกไอเทม '+items.name + ' ?',modal_item_token: items.token})}>แลกไอเทม</div>
                                                </>
                                        }
                                        <small>ใช้ {items.coin} เหรียญ</small> <br/>
                                        {
                                            items.limit && parseInt(items.limit) !== 0 ?
                                                <small className="limit">จำกัด {items.limit} ชิ้น</small>
                                            : null
                                        }
                                    </div>
                                )
                            })
                        }       
                    </div>

                    {/* section 3 */}
                    <div className="section section3">
                        <div>
                            <Slider {...settings}>
                            {
                                this.state.slider_image.map((items,key)=>{
                                    return(
                                        <div key={key} className=""> 
                                            <p>{items.title}</p>
                                            <img src={items.image}/>
                                        </div>
                                    )
                                })
                            }
                            </Slider>
                        </div>
                    </div>
              
                </Content>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                <ModalConfirmReceive actConfirm={this.actConfirm.bind(this)}/>
                <ModalSelectCharacter apiSelectCharacter={this.apiSelectCharacter.bind(this)}/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #11122d;
    width: 1105px;
    padding-top: 740px;
    text-align: center;
`

const Content = styled.div `  
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }
    & .section{
        color:#fff;
        padding-top: 10%;
        padding-bottom: 10%;
        &__header{
            font-size: 20px;
            font-weight: bold;
            margin-bottom: 30px;
            text-align: center;
        }
        &__step1{
            text-align:center;
            padding-top: 3%;
        }
        &__step2{
            text-align: center;
            padding-top: 4%;
        }
    }
    & small{
        font-size: 80%;
        line-height: 1;
    }
    & .limit{
        color: #e4e0e0;
    }
    & .section1{
        //padding-top:0;

        background: url(${Imglist['content1']}) no-repeat top center; 
        padding: 15%;
        text-align:left;
        & .questItems{
            width: 33.33%;
            display: inline-block;
            vertical-align: top;
            text-align: center;
            padding: 10px 5px;
            &.disabled{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%); 
            }
        }
    }
    & .section2{
        background: url(${Imglist['content2']}) no-repeat top center; 
        padding: 10% 16%;
        z-index: 1;
        position: relative;
        text-align:left;
    }
    & .items{
        width: 20%;
        display:inline-block;
        vertical-align: top;
        font-size: 14px;
        padding: 15px 10px;
        color: #939393;
        text-align: center;
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%); 
            &.active{
                -webkit-filter: none;
                filter: none;  
            }
        }
        &__img{
            position:relative;  
        }
        &__name{
            min-height: 35px;
            line-height: 1.2;
            margin: 5px 0;
        }
    }

    & .btn{
        cursor:pointer;
        // &-default{
        //     background: no-repeat 0 0 url(${Imglist['btn_default']});
        //     width: 155px;
        //     height: 45px;
        //     display: inline-block;
        //     cursor: pointer;
        //     margin: 0 15px;
        //     color: #fff;
        //     line-height: 36px;
        //     font-size: 20px;
        //     &:hover{
        //         background-position: 0 -55px;
        //     }
        //     &.disabled{
        //         -webkit-filter: grayscale(100%);
        //         filter: grayscale(100%);
        //         color: #bfbfbf;
        //         cursor: not-allowed;
        //         &:hover{
        //             background-position: 0 0;
        //         }
        //     }
        // }
        &-exchange{
            background: top center no-repeat url(${Imglist['btn_default']});
            width: 80px;
            height: 35px;
            background-size: contain;
            color: #000;
            margin: 0 auto;
            opacity: 0.8;
            font-size: 14px;
            line-height: 2;
            &:hover{
                opacity: 1;
            }
            &.inactive{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);  
                cursor:default;
                &:hover{
                    opacity: 0.8;
                } 
            }
        }
       
    }
    & .section3{
        background: top center no-repeat url(${Imglist['content3']});
        color: #fff;
        text-align: center;
        padding-bottom: 14%;
        & .slick-slide img{
            margin: 0 auto;
        }
        & .slick-dots li.slick-active button:before {
            opacity: 1;
            color: #fff;
        }
        & .slick-dots li button:before{
            font-size: 11px;
            opacity: 1;
            color: #646464;
        }
        & .slick-prev{
            left:120px;
        }
        & .slick-next{
            right:120px;
        }
        & .slick-prev,
        & .slick-next{
            width: auto;
            height: auto;
            z-index: 1;
            &::before{
                font-size:0;
            }
        }
    }

    

`

