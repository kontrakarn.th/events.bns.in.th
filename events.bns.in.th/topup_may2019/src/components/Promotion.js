import React, { Component } from 'react'
import promotionTitle from './../static/images/title_condition.png';
import promotionPin from './../static/images/icon_pin.png';
export default class Promotion extends Component {
    render() {
        return (
            <div className="topup__promotion">
                <div className="topup__title"><img src={promotionTitle} /></div>
                <div className="topup__promotion-content">
                    <ul className="topup__promotion-list">
                        <li><img src={promotionPin} />เริ่มโปรโมชั่น วันที่ 12 พฤษภาคม 2562 เวลา 12:00:00 น.</li>
                        <li><img src={promotionPin} />สิ้นสุดโปรโมชั่น วันที่ 4 มิถุนายน 2562 เวลา 23:59:59 น.</li>
                        <li><img src={promotionPin} />ผู้เล่นสามารถใช้สิทธิ์ในการแลกโบนัสไดมอนด์ 1 ครั้ง ต่อ 1 UID เท่านั้น</li>
                        <li><img src={promotionPin} />ระบบนับการเติมไดมอนด์แบบครั้งเดียว ไม่สามารถสะสมได้ ผู้เล่นต้องแลกไดมอนด์ตามจำนวนขั้นที่กำหนดภายในการแลกครั้งเดียว เช่น หากต้องการโบนัสไดมอนด์ 50% ต้องแลก 200,000 ไดมอนด์</li>
                        <li><img src={promotionPin} />กรณีที่สิทธิ์แลกโบนัสไดมอนด์ขั้นสูงสุดที่สามารถกดรับได้เต็มแล้ว สามารถกดรับสิทธิ์ขั้นที่ต่ำกว่าได้</li>
                        <li><img src={promotionPin} />สามารถแลกไดมอนด์ได้ทุกช่องทาง</li>


                        {/* *ข้อแนะนำ: หากแลกไดมอนด์ตามเงื่อนไขแล้ว กรุณากดรับสิทธิ์ได้ทันที เพื่อรักษาสิทธิ์ในการได้รับโบนัสไดมอนด์ */}
                    </ul>
                </div>
            </div>
        )
  }
}
