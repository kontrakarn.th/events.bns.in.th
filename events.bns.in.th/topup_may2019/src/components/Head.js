import React, { Component } from 'react'
import headText from './../static/images/head_text.png'
import iconScroll from './../static/images/icon_scroll.png'
export default class Head extends Component {
    render() {
        return (
            <div className="topup__head">
                {
                    Object.keys(this.props.userData).length>0
                    ?
                        <div className="topup__head-account">{this.props.userData.nickname}</div>

                    :
                        <div className="topup__head-account"><a  href={this.props.loginUrl}>Login</a></div>
                }
                <div className="topup__head-title">
                    <img src={""} />
                </div>
                <div className="topup__scroll"><img src={""} /></div>
            </div>
        )
  }
}
