import React, { Component } from 'react'
export default class ModalMessage extends Component {

    constructor(props){
        super(props)

    }

    render() {
        return (
            <div className={"modal "+(this.props.open ? "":"close")} >
                <div className="modal__backdrop" />
                <div className={"modal__content"}>
                    <div className={"modal__incontent"}>
                        <div className="modal__content-inner">
                            <div dangerouslySetInnerHTML={{
                                    __html: this.props.message
                                }}/>
                        </div>
                    </div>
                    <div className={"modal__bottom"}>
                        {
                            this.props.showLoading
                            ?
                                null
                            :
                                <a className="modal__button modal__button--ok" onClick={()=>this.props.closeModal()} />
                        }
                    </div>
                </div>
            </div>
        )
  }
}
