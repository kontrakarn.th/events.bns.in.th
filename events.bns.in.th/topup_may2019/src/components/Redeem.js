import React, { Component } from 'react'
import redeemTitle from './../static/images/title_redeem.png';
import character from './../static/images/img_character.png';
import textPoint from './../static/images/text_point.png';

import point0 from './../static/images/number/0.png';
import point1 from './../static/images/number/1.png';
import point2 from './../static/images/number/2.png';
import point3 from './../static/images/number/3.png';
import point4 from './../static/images/number/4.png';
import point5 from './../static/images/number/5.png';
import point6 from './../static/images/number/6.png';
import point7 from './../static/images/number/7.png';
import point8 from './../static/images/number/8.png';
import point9 from './../static/images/number/9.png';

import imgSlot50k from './../static/images/slot_50k_diamond.png';
import imgSlot100k from './../static/images/slot_100k_diamond.png';
import imgSlot300k from './../static/images/slot_300k_diamond.png';

import RedeemList from './RedeemList';
import RedeemList2 from './RedeemList2';

const set_number=[point0,point1,point2,point3,point4,point5,point6,point7,point8,point9]
const slotRedeem = [
    imgSlot50k,
    imgSlot100k,
    imgSlot300k,
]
const list = [
    "bonus_1",
    "bonus_2",
    "bonus_3",
]
export default class Redeem extends Component {
    actClick(status,index){
        if(status === 1){
            this.props.confirmRedeem(index);
        }

    }
    genCountForK(n){
        let str = n+"";
        if( str.length > 3) str = str.substr(0,str.length - 3)+","+str.substr(str.length - 3);
        return str;
        // return Math.floor(n/1000)+","+(n%1000);
        // var s = "000000000" + num;
        // return s.substr(s.length-size);
    }
    render() {
        let {bonus, bonus_counter} = this.props;
        return (
            <div className="topup__redeem">
                <div className="topup__title"><img src={redeemTitle} alt="แลกของรางวัล" /></div>
                <div className="topup__redeem-content">
                    {slotRedeem.map((item,index)=>{
                        let status = bonus[list[index]];
                        let classButton = "redeemslot__button redeemslot__button--"+status;
                        return (
                            <div className="redeemslot">
                                <div className="redeemslot__image" >
                                    <img src={item} />
                                    <div className="redeemslot__count">สิทธิ์คงเหลือ<br/><span>{this.genCountForK(bonus_counter[list[index]])}</span></div>
                                </div>
                                <a
                                    className={classButton}
                                    onClick={()=>this.actClick(status,index)}
                                />
                            </div>
                        )
                    })}
                </div>
            </div>
        )
  }
}
