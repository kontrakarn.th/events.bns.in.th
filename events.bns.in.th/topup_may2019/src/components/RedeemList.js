import React, { Component } from 'react'
export default class RedeemList extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <div className="topup__redeem-blog topup__redeem-blog--3">
                <img src={this.props.image} />
                {
                    this.props.showLoading
                    ?
                        null
                    :
                        this.props.package && this.props.package.received==true
                        ?
                            <a className="topup__btns redeem"></a>
                        :
                            this.props.package && this.props.package.can_receive==false
                            ?
                                <a className="topup__btns condition"></a>
                            :
                                <a onClick={()=>this.props.confirmRedeem(this.props.package)} className="topup__btns"></a>

                }

            </div>
        )
  }
}
