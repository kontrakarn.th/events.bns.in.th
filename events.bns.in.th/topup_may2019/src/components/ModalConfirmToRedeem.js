import React, { Component } from 'react'
export default class ModalConfirmToRedeem extends Component {

    constructor(props){
        super(props)
        this.state={
            // redeemlist:[]
        }
    }

    // componentDidUpdate(prevProps, prevState) {
    //     if(prevProps.package_select!=this.props.package_select){
    //         let sep=this.props.package_select.package_desc.split("|")
    //         this.setState({
    //             redeemlist:sep
    //         });
    //     }
    // }
    //
    // componentWillMount() {
    //     if(Object.keys(this.props.package_select).length>0){
    //         let sep=this.props.package_select.package_desc.split("|")
    //         this.setState({
    //             redeemlist:sep
    //         });
    //     }
    // }

    render() {
        let costList =["12,500","50,000","300,000"];
        return (
            <div className={"modal "+(this.props.open ? "":"close")} >
                <div className="modal__backdrop" />
                <div className={"modal__content"}>
                    <div className={"modal__incontent"}>
                        ยืนยัน
                        <br />
                        <div className="modal__content-inner">
                            การกดรับโบนัสไดมอนด์<br />
                            มูลค่า {costList[this.props.package_select]} ไดมอนด์<br />
                            โดยไม่สามารถเปลี่ยนแปลงภายหลัง<br />
                            ใช่หรือไม่?<br />
                        </div>
                    </div>
                    <div className={"modal__bottom"}>
                        <a onClick={()=>this.props.redeemItem()} className="modal__button modal__button--ok" />
                        <a className="modal__button modal__button--cancel" onClick={()=>this.props.closeModal()} />
                    </div>
                </div>
            </div>
        )
  }
}
