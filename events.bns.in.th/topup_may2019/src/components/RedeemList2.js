import React, { Component } from 'react'
export default class RedeemList2 extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <span className="topup__redeem-blog topup__redeem-blog--inner">
                <img src={this.props.image} />
                {
                    this.props.showLoading
                    ?
                        null
                    :
                        this.props.package && this.props.package.received==true
                        ?
                            <a className="topup__btns redeem"></a>
                        :
                            this.props.package && this.props.package.can_receive==false
                            ?
                                <a className="topup__btns condition"></a>
                            :
                                <a onClick={()=>this.props.confirmRedeem(this.props.package)} className="topup__btns"></a>

                }
            </span>
        )
  }
}
