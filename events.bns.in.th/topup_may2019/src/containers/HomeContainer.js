import React, { Component } from "react";
import { connect } from "react-redux";
import ScrollArea from "react-scrollbar";
import "whatwg-fetch";
import "./../styles/index.css";
import character01 from "./../static/images/character01.png";
import character02 from "./../static/images/character02.png";
import Head from "../components/Head";
import Promotion from "../components/Promotion";
import Redeem from "../components/Redeem";
import ModalConfirmToRedeem from "../components/ModalConfirmToRedeem";
import ModalMessage from "../components/ModalMessage";
import { Permission } from "../components/Permission";
import ModalSelectCharacter from "../components/ModalSelectCharacter";
import home from "./../static/images/btn_home.png";
import { onAccountLogout } from "../actions/AccountActions";

class HomeContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      permission: true,
      showLoading: false,
      modalConfirmToRedeem: false,
      modalMessage: false,
      alertMessage: "",
      package_select: 0,
      bonus: {
        bonus_1: 0,
        bonus_2: 0,
        bonus_3: 0
      },
      bonus_counter: {
        bonus_1: 0,
        bonus_2: 0,
        bonus_3: 0
      },
      diamond: 0
    };
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.userData != this.props.userData) {
      this.getEventInfo();
    }
  }

  getEventInfo() {
    let self = this;
    fetch(
      process.env.REACT_APP_API_SERVER_HOST +
        process.env.REACT_APP_API_POST_EVENT_INFO,
      {
        method: "GET",
        // body: JSON.stringify({
        //     "type":"event_info"
        // }),
        credentials: "same-origin",
        headers: {
          Accept: "application/json",
          "Content-Type":
            "application/x-www-form-urlencoded; charset=UTF-8; application/json"
        }
      }
    )
      .then(response => response.json())
      .then(
        response => {
          // console.log(response)
          if (response.status) {
            this.setState({
              bonus: response.data.bonus,
              bonus_counter: response.data.bonus_counter,
              diamond: response.data.diamond
            });
          } else {
            if (response.type == "no_permission") {
              this.setState({
                permission: false
              });
            }
          }
        },
        error => {}
      );
  }

  redeemItem() {
    let name = this.props.userData ? this.props.userData.nickname : "";
    this.setState({
      showLoading: true,
      modalConfirmToRedeem: false
    });
    setTimeout(() => {
      fetch(
        process.env.REACT_APP_API_SERVER_HOST +
          process.env.REACT_APP_API_POST_REDEEM,
        {
          method: "POST",
          body: JSON.stringify({
            bonus_id: this.state.package_select + 1,
            name: name
          }),
          credentials: "same-origin",
          headers: {
            Accept: "application/json",
            "Content-Type":
              "application/x-www-form-urlencoded; charset=UTF-8; application/json"
          }
        }
      )
        .then(response => response.json())
        .then(
          response => {
            this.setState({
              showLoading: false,
              modalMessage: true,
              alertMessage: response.message
            });
            this.getEventInfo();
          },
          error => {}
        );
    }, 1000);
  }

  confirmRedeem(package_select) {
    this.setState({
      package_select,
      modalConfirmToRedeem: true
    });
  }

  closeModal() {
    this.setState({
      modalConfirmToRedeem: false,
      modalMessage: false
    });
  }

  render() {
    //filter permission is false
    if (this.state.permission === false) return <Permission />;

    return (
      <div className="events-bns">
        <ScrollArea
          speed={0.8}
          className="area"
          contentClassName=""
          horizontal={false}
          style={{ width: "100%", height: 700, opacity: 1 }}
          verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
        >
          <div className="topup">
            <Head
              userData={this.props.userData}
              loginUrl={this.props.loginUrl}
              logoutUrl={this.props.logoutUrl}
              onAccountLogout={this.props.onAccountLogout}
            />
            <Promotion />
            <Redeem
              bonus={this.state.bonus}
              bonus_counter={this.state.bonus_counter}
              pointarr={this.state.pointarr}
              packages={this.state.packages}
              confirmRedeem={this.confirmRedeem.bind(this)}
              showLoading={this.state.showLoading}
            />
            <ModalConfirmToRedeem
              package_select={this.state.package_select}
              open={this.state.modalConfirmToRedeem}
              redeemItem={this.redeemItem.bind(this)}
              closeModal={this.closeModal.bind(this)}
            />
            <ModalMessage
              open={this.state.modalMessage}
              showLoading={this.state.showLoading}
              message={this.state.alertMessage}
              closeModal={this.closeModal.bind(this)}
            />
            <div className="topup__home">
              <a href={process.env.REACT_APP_API_SERVER_HOST}>
                <img src={home} />
              </a>
            </div>
          </div>
        </ScrollArea>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userData: state.AccountReducer.userData,
  loginUrl: state.AccountReducer.loginUrl,
  logoutUrl: state.AccountReducer.logoutUrl
});

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch,
  onAccountLogout: () => {
    dispatch(onAccountLogout());
  }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContainer);
