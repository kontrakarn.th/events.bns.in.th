import React from 'react';
import Modal from './Modal';

export default class ModalConfirmSend extends React.Component {
    render() {
        let title = this.props.data && this.props.data || [];
        return (
            <Modal
                open={this.props.open}
                title="ยืนยัน"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox" dangerouslySetInnerHTML={{__html: this.props.msg}} />
            </Modal>
        )
  }
}
