import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import ModalMessage from '../components/ModalMessage';
import ModalConfirm from '../components/ModalConfirm';

import { Menu } from './../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import card1 from './../static/images/card1.png'
import card5 from './../static/images/card5.png'
import card10 from './../static/images/card10.png'
// import btnDefault from './../static/images/btn_default.png'


/* import iconScroll from './../static/images/icons/icon_scroll.png';
import iconMenu from './../static/images/icon_menu.png' */

import {
    onAccountLogout,
    setUsername,
    setCanPlay,
    setBuySoulResult
} from '../actions/AccountActions';

class SoulContainer extends Component {

    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: false,
            scrollDown: false,
            showModalConfirmOpenSoul: false,
            modalContent: {},
            soulItems: [
                {
                    id: 1,
                    img: card1,
                    count: '500 ไดมอนด์',
                    img_h: '157px',
                    img_w: '216px'
                },
                {
                    id: 2,
                    img: card5,
                    count: '2,500 ไดมอนด์',
                    img_h: '157px',
                    img_w: '216px'
                },
                {
                    id: 3,
                    img: card10,
                    count: '5,000 ไดมอนด์',
                    img_h: '157px',
                    img_w: '216px'
                }
            ],
            package_id:0,
            showModalMessage: "",
            username: "",
            buy_soul_info: [],
            buy_soul_result : [],
        }
    }


    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 1000);

    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo();
            }, 1000);
        }
    }

    apiEventInfo() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "event_info" };
        let successCallback = (data) => {
            if (data.status) {
                this.props.setUsername(data.data.nickname);
                this.props.setCanPlay(data.data.can_play);
                this.setState({
                    permission: true,
                    showLoading: false,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scrollDown === false) this.setState({ scrollDown: true });
        } else {
            if (this.state.scrollDown === true) this.setState({ scrollDown: false });
        }
    }

    ConfirmOpenSoul(id=1) {
        // console.log(id);
        this.setState({
            showModalConfirmOpenSoul: true,
            modalContent: this.state.soulItems[id-1]
        });
        /* switch (id) {
            case 1:
                this.setState({
                    showModalConfirmOpenSoul: true,
                    modalContent: this.state.soulItems.find(item => item.id === id)
                });
                break;
            case 2:
                this.setState({
                    showModalConfirmOpenSoul: true,
                    modalContent: this.state.soulItems.find(item => item.id === id)
                });
                break;
            case 3:
                this.setState({
                    showModalConfirmOpenSoul: true,
                    modalContent: this.state.soulItems.find(item => item.id === id)
                });
                break;
            default:
                break;
        } */
    }

    apiConfirmOpenSoul() {
        const package_id = this.state.package_id;

        this.props.setBuySoulResult([]);

            this.setState({
                showLoading: true
            });
            apiPost(
                "REACT_APP_API_POST_BUY_CARD",
                this.props.jwtToken,
                {
                    type: 'buy_card',
                    package_id: package_id
                },
                (resp) => { // success response
                    if (resp.status) {
                        this.props.setBuySoulResult(resp.content);
                        this.props.setCanPlay(resp.data.can_play);
                        this.props.history.push(process.env.REACT_APP_EVENT_PATH + '/open-soul')
                    } else {
                        this.setState({
                            showLoading: false,
                            showModalMessage: resp.message,
                        });
                    }
                },
                (data) => { // error response
                    if (data.type == 'no_permission') {
                        this.setState({
                            showLoading: false,
                            permission: false,
                        });

                    }else {
                        this.setState({
                            showLoading: false,
                            showModalMessage: data.message,
                        });
                    }
                });

    }

    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: 700, opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                            onScroll={this.handleScroll.bind(this)}
                        >
                            <div className="preorder">
                                <div className="page__wrapper soul_page">
                                    <Menu />
                                    <section className="preorder__section soul">

                                        <div className="container">
                                            <div className="section__head">
                                                <h3>ซื้อการ์ด</h3>
                                            </div>
                                            <div className="section__content clearfix">
                                                {this.state.soulItems.map((items, index) => {
                                                    return (
                                                        <div key={index} className="soul__items">
                                                            <div className="soul__items--img">
                                                                <img src={items.img}/>
                                                                {this.props.can_play["card_"+(index+1)] ?
                                                                    <a
                                                                        className="btn-soul"
                                                                        onClick={()=>this.setState({
                                                                            package_id:items.id,
                                                                            showModalConfirmOpenSoul:true
                                                                        })}
                                                                        style={{
                                                                            fontSize: "18px",
                                                                            lineHeight: "28px",
                                                                        }}
                                                                    >
                                                                        {items.count}
                                                                    </a>

                                                                    :
                                                                    <div
                                                                        className="btn-soul gray-btn"
                                                                        
                                                                    >
                                                                        ไดมอนด์ไม่พอ
                                                                    </div>
                                                                }

                                                            </div>
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </ScrollArea>

                        <ModalConfirm
                            open={this.state.showModalConfirmOpenSoul}
                            actConfirm={() => this.apiConfirmOpenSoul()}
                            actClose={() => this.setState({
                                showModalConfirmOpenSoul: false
                            })}
                            setMsg={this.state.package_id}
                        />

                        <ModalMessage
                            open={this.state.showModalMessage.length > 0}
                            actClose={()=>this.setState({showModalMessage: ""})}
                            msg={this.state.showModalMessage}
                        />
                    </div>
                    :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    can_play: state.AccountReducer.can_play,
    buy_soul_result: state.AccountReducer.buy_soul_result
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    setCanPlay,
    setBuySoulResult
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(SoulContainer))
