import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from './../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';

import ModalMessage from '../components/ModalMessage';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';

import {
    onAccountLogout,
    setUsername,
    setSoulPermission,
    setCanPlay
} from '../actions/AccountActions';

class HomeContainer extends Component {

    apiEventInfo() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "event_info" };
        let successCallback = (data) => {
            if (data.status) {
                this.props.setUsername(data.data.nickname);
                this.props.setCanPlay(data.data.can_play);
                this.setState({
                    permission: true,
                    showLoading: false,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
            open_card:[]
        }
    }

    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 1000);

    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo()
            }, 1000);
        }
    }

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scollDown === false) this.setState({ scollDown: true });
        } else {
            if (this.state.scollDown === true) this.setState({ scollDown: false });
        }
    }

    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: '100%', opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                        >
                            <div className="preorder">
                                <div class="preorder-inner">
                                    <section className="preorder__section preorder__section--header">
                                        <div className="header">
                                            <div className="header__scroll"><img src={iconScroll} alt="" /></div>
                                            <Link to="/gachapon_5bath/soul" className="btn btn-promotion">เข้าร่วมโปรโมชั่น</Link>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </ScrollArea>
                    </div>
                    :
                    <Permission />
                }

                {/* <Loading
                    open={this.state.showLoading}
                /> */}
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    permission: state.AccountReducer.permission,

})

const mapDispatchToProps = {
    onAccountLogout, setUsername, setSoulPermission,setCanPlay
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
