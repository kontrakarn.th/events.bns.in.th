import React, { Component } from 'react';
import {connect} from 'react-redux';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import {apiPost} from './../middlewares/Api';

// import ModalConfirmToRedeem from '../components/ModalConfirmToRedeem';

import ModalMessage from '../components/ModalMessage';
import ModalItemCode from '../components/ModalItemCode';
import ModalConfirmPurchase from '../components/ModalConfirmPurchase';
// import ModalConfirmGacha from '../components/ModalConfirmGacha';
// import ModalReceiveGacha from '../components/ModalReceiveGacha';
// import ModalConfirmExchange from '../components/ModalConfirmExchange';
// import ModalReceiveExchange from '../components/ModalReceiveExchange';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
// import titleCondition from './../static/images/title_condition.png'
// import titlePackage from './../static/images/title_package.png'
import itemsPackage from './../static/images/items.png'
import imgWoman from './../static/images/woman.png'

import {
    onAccountLogout
} from '../actions/AccountActions';

class HomeContainer extends Component {
   
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiEventInfo(){
        console.log(1);
        this.setState({ showLoading: true});
        let self = this;
        let dataSend={ type : "event_info" };
        let successCallback = (data)=>{
            // console.log("data",data);
            this.setState({
                permission: true,
                showLoading: false,
                ...data.content
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                })
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO",this.props.jwtToken,dataSend,successCallback,failCallback);
    }

    apiRedeemPackage(){
        this.setState({ showLoading: true, ModalConfirmPurchase: false});
        let self = this;
        let dataSend={ type : "purchase_package", package_id: 1 };
        let successCallback = (data)=>{
            // console.log("data",data);
            this.setState({
                permission: true,
                showLoading: false,
                showModalMessage: data.message,
                ...data.content
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                })
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM",this.props.jwtToken,dataSend,successCallback,failCallback);
    }
    
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: false,

            name: "",
            username: "",
            discount: false,
            package_price: 120000,
            can_receive: false,
            received: false,
            has_item_code: false,
            my_code: "",

            showModalMessage: "",
            showModalItemCode: false,
            ModalConfirmPurchase: false,

            scollDown: false,

            status:'no_discount',

        }
    }
    componentDidMount(){
        if(this.props.jwtToken !== "") setTimeout(()=>this.apiEventInfo(),600);
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.jwtToken !== prevProps.jwtToken){
            setTimeout(()=>{
                this.apiEventInfo()
            },600);
        }
    }


    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }

    actRedeem(){
        this.setState({
            ModalConfirmPurchase: true,
        })
    }

    actConfirmRedeem(){
        this.apiRedeemPackage();
    }

    actShowMyCode(){
        this.setState({
            showModalItemCode: true,
        })
    }

    render() {
        // let username = (this.props.userData)? this.props.userData.username : "";
        return (
            <div >
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.state.username}</div>
                        {/* <div className="f11layout__user--style">ล็อกเอ้าท์</div> */}
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                    {/* <div className={"f11layout__scroll"+(this.state.scollDown? " hide":"")} ><img src={iconScroll} alt="" /></div> */}
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="preorder">
                                <section className="preorder__section preorder__section--header">
                                    <div className="header">
                                        <div className="header__inner">
                                            {/* <div className="header__tag header__tag--date"></div>
                                            <div className="header__tag header__tag--promotion"></div> */}
                                        </div>
                                        <div className="header__scroll"><img src={iconScroll} alt="" /></div>
                                    </div>
                                </section>
                                <section className="preorder__section preorder__section--condition">
                                    <div className="condition">
                                        <div className="condition__inner">
                                            {/* <div className="condition__head"><img src={titleCondition} alt="" /></div> */}
                                            <ul className="condition__text">
                                                <li>ผู้เล่นที่สร้างตัวละครใหม่ ตั้งแต่ <span className="condition__text--orange"> 16 มกราคม 2562 หลังปิดปรับปรุงเซิร์ฟเวอร์</span></li>
                                                <li>สามารถกดรับ <span className="condition__text--orange">ของขวัญต้อนรับวอร์เดน ได้</span></li>
                                                <li>สามารถกดรับได้ถึงวันที่ <span className="condition__text--orange">19 กุมภาพันธ์ 2562 (23:59 น.)</span></li>
                                                <li>จำกัดสิทธิ์ 1 ครั้งต่อ UID</li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                                <section className="preorder__section preorder__section--status">
                                    <div className="package">
                                        <div className="package__inner">
                                           <img className="package__imgItems" src={itemsPackage} alt="" />
                                           <img className="package__imgWoman" src={imgWoman} alt="" />
                                           {
                                                this.state.received == true ?
                                                    <a className="btns__normal received"><div>รับไปแล้ว</div></a>
                                                :
                                                    (
                                                        this.state.can_receive == true ?
                                                            <a className="btns__normal" onClick={this.actRedeem.bind(this)}>
                                                              
                                                                    <div>รับของขวัญ</div>
                                                              
                                                            </a>
                                                        :
                                                            <a className="btns__normal received"><div>ไม่ตรงเงื่อนไข</div></a>
                                                    )

                                           }
                                        </div>
                                    </div>
                                </section>
                                
                            </div>
                        </ScrollArea>
                    </div>
                    :
                    <Permission />
                }


                <Loading
                    open={this.state.showLoading}
                />
                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
                <ModalItemCode
                    open={this.state.showModalItemCode}
                    actClose={()=>this.setState({showModalItemCode: false})}
                    msg={this.state.my_code}
                />
                <ModalConfirmPurchase
                    open={this.state.ModalConfirmPurchase}
                    actConfirm={()=>this.actConfirmRedeem()}
                    actClose={()=>this.setState({ModalConfirmPurchase: false})}
                    msg="รับไอเทม<br/>กล่องของขวัญ<br/>ต้อนรับวอร์เดน?"
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
    onAccountLogout: () => {
        dispatch(onAccountLogout());
    }
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
