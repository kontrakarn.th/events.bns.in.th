import React from 'react';
import Modal from './Modal';

export default class ModalReceiveGacha extends React.Component {
    render() {
        let title = this.props.data && this.props.data || [];
        return (
            <Modal
                open={this.props.open}
                title="ยืนยัน"
                confirmMode={false}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox">
                    <span className="inbox__head">ได้รับ</span>
                    {title.map((item,index)=>{
                        return (<div>- {item}</div>)
                    })}
                </div>
            </Modal>
        )
  }
}
