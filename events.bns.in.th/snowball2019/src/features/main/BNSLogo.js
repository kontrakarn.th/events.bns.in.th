import React from 'react';
import logo from '../../static/images/logo.png';
import styled from 'styled-components';

export default props=>(<BNSLogo src={logo} alt='logo'/>	)

const BNSLogo = styled.img`
    position: absolute;
    left: 70px;
    top: 15px;
    z-index: 5;
    pointer-events: none;
`