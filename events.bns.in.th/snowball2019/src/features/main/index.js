import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
import Logo from './BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalExchange from './../../features/modals/ModalExchange';
import ModalQuest from './../../features/modals/ModalQuest';
import {imageList} from './../../constants/Import_Images';
import ModalSelectCharacter from "../modals/ModalSelectCharacter";
import LoadingScreen from '../LoadingScreen/LoadingScreen';
// import UserInfo from '../Layout/UserInfo';
import Titlebox from '../Layout/Titlebox';
import BottomBox from '../Layout/BottomBox';
import SnowBallGame from '../../features/SnowBallGame/SnowBallGame';

class Main extends React.Component {

    actConfirm(token){
        if (token === undefined || token === ""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไม่สามารถแลกของไอเทมได้",
            });
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'redeem',token:token};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                    coin:data.data.coin,

                    // clan_members: data.data.clan_members,
                    // clan_ranks: data.data.clan_ranks,
                    // clan_rewards: data.data.clan_rewards,
                    // quests: data.data.quests,
                    // rank_rewards: data.data.rank_rewards,
                    // score: data.data.score,
                });
                // this.setState({
                //     itemLists : data.data.itemLists,
                //     questLists : data.data.questLists,
                // })
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
        // this.apiRedeem(
        //     this.props.modal_confirmitem && this.props.modal_confirmitem !== ''
        //     && this.props.modal_confirmitem
        // );
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    modal_open: "",
                    username: data.data.username,
                    character: data.data.character_name,
                    // coin: data.data.coin,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters||[],

                    clan_members: data.data.clan_members||0,
                    clan_ranks: data.data.clan_ranks||[],
                    clan_rewards: data.data.clan_rewards|| {
                        step1: {id: 1, can_claim: false, is_claimed: false},
                        step2: {id: 2, can_claim: false, is_claimed: false},
                        step3: {id: 3, can_claim: false, is_claimed: false},
                    },
                    quests: data.data.quests||[],
                    rank_rewards: data.data.rank_rewards||{},
                    score: data.data.score||"0",
                });

                if (data.data.selected_char === false){
                    this.props.setValues({ modal_open: "selectcharacter" });
                }
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiSelectCharacter(id=""){
        if (id === undefined || id === ""){
            return;
        }
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'select_character',id:id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open: "",
                    username: data.data.username,
                    character: data.data.character_name,
                    selectd_char: data.data.selected_char,
                    characters: data.data.characters,

                    clan_members: data.data.clan_members,
                    clan_ranks: data.data.clan_ranks,
                    clan_rewards: data.data.clan_rewards,
                    quests: data.data.quests,
                    rank_rewards: data.data.rank_rewards,
                    score: data.data.score,
                });
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiRedeem(type_id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type_id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content.response,
                    modal_open:"message",
                    modal_message: "ส่งของรางวัลสำเร็จ<br/>กรุณาตรวจสอบจดหมายภายในเกม"
                });

            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }

    render() {
        let quests = this.props.quests || [];
        return (
            <PageWrapper>
                <Logo/>
                {/*<Mock/>*/}
                <Content>
                        <LoadingScreen hide={true}/>
                        <Titlebox />
                        <BottomBox/>
                        <SnowBallGame/>
                </Content>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                <ModalSelectCharacter actSelectCharacter={(e)=>this.apiSelectCharacter(e)}/>
                <ModalExchange/>
                <ModalQuest/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${imageList['main_bg']});
    background-size: 100% 100%;
    width: 1105px;
    height: 700px;
    text-align: center;
    position: relative;
    z-index: 20;
    overflow: hidden;
    font-family: 'Kanit-Regular','tahoma';
    pointer-events: none;
`

const Content = styled.div `
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }

`

const Mock = styled.div`
    position: absolute;
    opacity: 0.5;
    background: top center no-repeat url(${imageList['mock']});
    background-size: 100% 100%;
    top: 0;
    left:0;
     right: 0;
     bottom: 0;
     pointer-events: none;
`