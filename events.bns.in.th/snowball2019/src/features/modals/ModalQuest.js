import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';
import {imageList} from "../../constants/Import_Images";

const ModalQuest = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="quest"
            actClickOutside={false}
        >
            <ModalContent>
                    <CloseBtn src={imageList['btn_close']} alt='' onClick={()=>props.setValues({modal_open: ''})}/>
                    <ul className="questlist">
                            <li>
                                <div className='questlist__name'>
                                            รับบอลฟรีวันละ 2 ลูก
                                </div>
                                <RedeemBtn/>
                            </li>
                            {
                               props.quest_list && props.quest_list.map((item,index) => {
                                    return(
                                        <li key={'quest_'+(index+1)}>
                                            <div className='questlist__questbox'>
                                                    <div className="questlist__questbox--title">ผ่านเควสต์ลึกลงไปในกรุสมบัติ</div>
                                                    <div className="questlist__questbox--detail">ดันเจี้ยน กรุสมบัตินาริว 1 ครั้ง รับ 2 ลูก (สูงสุด 4 ลูก)</div>
                                            </div>
                                            <RedeemBtn status={props.modal_open === 'quest' ? 'active' : null} />
                                        </li>
                                    )
                                })
                            }
                    </ul>
            </ModalContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(ModalQuest);
//className={"modal__incontent modal__incontent--loading"}
const ModalContent = styled.div`
    position: relative;
    display: block;
    width: 507px;
    height: 611px;
    text-align: center;
    color: #ffffff;
    background: no-repeat center url(${imageList['frame_quest']});
    box-sizing: border-box;
    padding: 17% 5% 3%;
    box-shadow: 0 0 10px 1px #000000;
    border-radius: 15px;
    .questlist{
        width: 100%;
        height: 100%;
        padding: 0 2%;
        box-sizing: border-box;
        overflow: auto;
        overflow-x: hidden;
        >li{
            height: 70px;
            display: flex;
            justify-content: space-between;
            align-items: center;
            border-bottom: 1px solid #4a5669;
            padding: 0 10px;
        }
        &__name{
            
        }
        &__questbox{
            font-size: 12px;
            width: 320px;
            position: relative;
            padding-left: 30px;
            box-sizing: border-box;
            text-align: left;
            &:after{
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                background: top center no-repeat url(${imageList['icon_quest']});
                width: 26px;
                height: 26px;
            }
            &--title{
                color: #90e36c;
            }
            &--detail{
                
            }
        }
    }
`;

const RedeemBtn  =  styled.div`
    background: top center no-repeat url(${imageList['btn_redeem']});
    background-size: 100% 300%;
    position: relative;
    cursor: pointer;
    pointer-events: none;
    width: 67px;
    height: 33px;
    ${props=>props.status === 'active' && `pointer-events: visible;background-position: center;`}
    &:hover{
        background-position: bottom center;
    }
`

const CloseBtn = styled.img`
    display: block;
    cursor: pointer;
    position: absolute;
    top: -30px;
    right: -30px;
`
