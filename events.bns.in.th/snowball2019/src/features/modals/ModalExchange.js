import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';
import {imageList} from "../../constants/Import_Images";

const ModalExchange = props => {
    let {modal_message} = props;
    let itemList = [
        {x:'20',y:'30'},
        {x:'133',y:'30'},
        {x:'246',y:'30'},
        {x:'360',y:'30'},
        {x:'75',y:'165'},
        {x:'188',y:'165'},
        {x:'302',y:'165'},
        {x:'131',y:'301'},
        {x:'244',y:'301'}
    ]
    return (
        <ModalCore
            modalName="exchange"
            actClickOutside={false}
        >
            <ModalContent>
            <Mock/>
                    <CloseBtn src={imageList['btn_close']} alt='' onClick={()=>props.setValues({modal_open: ''})}/>
                    <div className="wrapper">
                        {
                            itemList.map((item,key) => {
                                return (
                                    <ItemBox x={item.x} y={item.y} key={'item_'+(key+1)} active={props.modal_item === key} onClick={()=>props.setValues({modal_item: key})}>
                                            <img src={imageList[`item_${key+1}`]} alt=""/>
                                    </ItemBox>
                                )
                            })
                        }
                    </div>
                    <ExchangeBtn active={props.modal_item}/>
                    <a className="detailbtn">
                        <img src={imageList['icon_h2p']} alt=""/>
                    </a>
            </ModalContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(ModalExchange);
//className={"modal__incontent modal__incontent--loading"}
const ModalContent = styled.div`
    position: relative;
    display: block;
    width: 507px;
    height: 611px;
    text-align: center;
    color: #ffffff;
    background: no-repeat center url(${imageList['frame_exchange']});
    box-sizing: border-box;
    padding: 21% 5% 5%;
    box-shadow: 0 0 10px 1px #000000;
    border-radius: 15px;
    .wrapper{
        width: 100%;
        height: 100%;
        position: relative;
    }
    .detailbtn{
        position: absolute;
        right: 40px;
        bottom: 20px;
        >img{
            display: block;
            cursor: pointer;
        }
        &:hover{
            &:after{
                content: '';
                background: top center no-repeat url(${imageList['modal_tooltips']});
                position: absolute;
                right: -15px;
                bottom: 35px;
                width: 92px;
                height: 44px;
            }
        }
    }
`;
const Mock = styled.div`
    background: no-repeat center url(${imageList['modal_mock']});
    position: absolute;
    top:0;
    left:0;
    right: 0;
    bottom: 0;
    opacity: 0.5;
`

const Btns = styled.div`
    width: 104px;
    height: 44px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #000;
    line-height: 36px;
    font-size: 20px;
    background-size: contain;
    opacity: 0.8;
    &.confirm{
        background: no-repeat 0 0 url(${imageList['btn_confirm']});
    }
    &.cancel{
        background: no-repeat 0 0 url(${imageList['btn_cancel']});
    }
    &:hover{
        background-position: bottom center;
    }
`

const CloseBtn = styled.img`
    display: block;
    cursor: pointer;
    position: absolute;
    top: -30px;
    right: -30px;
`
const ItemBox = styled.div`
    position: absolute;
    top: ${props=>props.y}px;
    left: ${props=>props.x}px;
    cursor: pointer;
    display: block;
    ${
        props=>props.active  &&
            `&:after{
                    content: '';
                    position: absolute;
                    top: 0;
                    left: 0;
                    right: 0;
                    bottom:0;
                    background: no-repeat url(${imageList['selected']});
                }
            `
    }
`

const ExchangeBtn = styled.div`
    position: absolute;
    bottom: 25px;
    left: 200px;
    background: top center no-repeat url(${imageList['btn_exchange_modal']});
    width: 113px;
    height: 53px;
    ${props=>props.active 
        ?
            `cursor: pointer;`
        :
            `pointer-events: none;
              filter: grayscale(1) brightness(0.6);`
    }
`