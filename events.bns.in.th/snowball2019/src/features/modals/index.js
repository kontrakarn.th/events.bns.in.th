import React from 'react';
import {connect} from 'react-redux';

import ModalMessage from './ModalMessage';
import ModalLoading from './ModalLoading';
import ModalSelectCharacter from './ModalSelectCharacter';
import ModalExchange from './ModalExchange';

const Modals = (props)=> (
    <>
        <ModalMessage />
        <ModalLoading />
        <ModalSelectCharacter />
        <ModalExchange/>
    </>
)

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = {  };

export default connect( mapStateToProps, mapDispatchToProps )(Modals);
