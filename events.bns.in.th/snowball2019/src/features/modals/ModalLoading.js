import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';

const ModalLoading = props => {
    return (
        <ModalCore
            modalName="loading"
        >
            <ModalLoadingContent>
                <h3>กำลังดำเนินการ...</h3>
            </ModalLoadingContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(ModalLoading);
//className={"modal__incontent modal__incontent--loading"}
const ModalLoadingContent = styled.div`
    text-align: center;
    color: #FFFFFF;
`;
