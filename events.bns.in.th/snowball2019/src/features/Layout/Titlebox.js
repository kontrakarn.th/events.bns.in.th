import React from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';
import { imageList } from '../../constants/Import_Images';

const Titlebox = props => {
     const numberWithCommas = x => {
         return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    };
	return(
		<Container>
                <Title src={imageList['title_text']} alt=''/>
                <HpBar>
                    <div className="levelbox">
                            50
                    </div>
                    <div className="hpinfo">
                            <div className="percent">
                                    50%
                            </div>
                            {numberWithCommas(500)} / {numberWithCommas(120000)}
                    </div>
                    <img className='cover' src={imageList['bar_cover']} alt=""/>
                    <div className="bar"/>
                </HpBar>
        </Container>
	)
}

const mapStateToProps = state => ({...state.Main});
export default connect( mapStateToProps, {})(Titlebox);

const Container = styled.div`
    position: absolute;
    top: 39px;
    left: 364px;
    display: block;
    pointer-events: none;
`;

const Title = styled.img`
    display: block;
    margin: 0 auto;
    height: 105%;
    max-height: unset;
`
const HpBar = styled.div`
    background: #555555;
    position: relative;
    margin: 20px auto 0;
    width: 380px;
    height: 36px;
    border-radius: 20px;
    .cover{
        display: block;
        margin: 0 auto;
        position:relative;
        z-index: 5;
    }
    .bar{
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width:  50%;
        border-radius: 20px;
        background: #ef9f00;
        z-index: 2;
    }
    .levelbox{
        background: top center no-repeat url(${imageList['level_box']});
        width: 58px;
        height: 57px;
        background-size: 100% 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        -webkit-text-fill-color: white;
        -webkit-text-stroke-width: 2.5px;
        -webkit-text-stroke-color: #2b1200;
        color: #ffffff;
        font-size: 40px;
        position: absolute;
        top: -10px;
        left: -28px;
        z-index: 10;
        font-family: 'ARMVicheneBold';
    }
    .hpinfo{
        width: 100%;
        position: absolute;
        top: 6px;
        bottom: 0;
        left: 0;
        color: #ffffff;
        z-index: 3;
        text-align: center;
        .percent{
            position: absolute;
            top: 0;
            right: 25px;
        }
    }

`