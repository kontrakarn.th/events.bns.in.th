import React from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';
import { imageList } from '../../constants/Import_Images';
import {setValues} from '../../store/redux';
const Titlebox = props => {
    const numberWithCommas = x => {
         return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    };
	return(
		<>
                <SnowballBox>
                        <div className="desc">
                            ลูกบอลที่มี
                        </div>
                        <div className="amt">
                                {numberWithCommas(888888)}
                        </div>
                </SnowballBox>
                <AmtBox>
                        <div className="desc">จำนวนที่ใช้</div>
                        <div className={"numberbox" + (props.snowball_amt === 1 ? ' selected' : '')} onClick={()=>props.setValues({snowball_amt:1})}>1</div>
                        <div className={"numberbox" + (props.snowball_amt === 5 ? ' selected' : '')} onClick={()=>props.setValues({snowball_amt:5})}>5</div>
                        <div className={"numberbox" + (props.snowball_amt === 50 ? ' selected' : '')} onClick={()=>props.setValues({snowball_amt:50})}>50</div>
                        <a className="h2pbtn">
                                <img src={imageList['icon_h2p']} alt=""/>
                        </a>
                </AmtBox>
                <BtnBox>
                        <div className="questbtn get" onClick={()=>props.setValues({modal_open:'quest'})}></div>
                        <div className="questbtn exchange" onClick={()=>props.setValues({modal_open:'exchange'})}></div>
                </BtnBox>
        </>
	)
}

const mapStateToProps = state => ({...state.Main});
export default connect( mapStateToProps, {setValues})(Titlebox);

const SnowballBox = styled.div`
    color: #ffffff;
    display: flex;
    justify-content: space-around;
    align-items: center;
    background: rgba(0,0,0,0.5);
    width: 215px;
    border-radius: 10px;
    padding: 10px;
    position: absolute;
    bottom: 30px;
    left: 66px;
    .desc{
        border-right: 1px solid #ffffff;
        padding-right: 25px;
    }
    .amt{
        
    }
`
const AmtBox = styled.div`
    color: #ffffff;
    display: flex;
    justify-content: space-around;
    align-items: center;
    background: rgba(0,0,0,0.5);
    width: 280px;
    position: absolute;
    bottom: 30px;
    left: 410px;
    padding: 10px;
    border-radius: 10px;
    >div{
        border-right: 1px solid #ffffff;
    }
    .desc{
        width: 105px;
    }
    >div:last-child , >div:nth-child(4){
        border-right: none;
    }
    .numberbox{
        width: 50px;
        position: relative;
        cursor: pointer;
        pointer-events: visible;
        &.selected{
            &:after{
                content: '';
                border: 1px solid #ffffff;
                border-radius: 50%;
                width: 30px;
                height: 30px;
                position: absolute;
                top: 50%;
                left: 50%;
                bottom: 0;
                right: 0;
                transform: translate3d(-50%,-50%,0)
            }
        }
    }
    .h2pbtn{
        position: absolute;
        right: -50px;
        pointer-events: visible;
        >img{
            display: block;
            cursor: pointer;
        }
        &:hover{
            &:after{
                content: '';
                background: top center no-repeat url(${imageList['h2p_tooltip']});
                position: absolute;
                right: -15px;
                bottom: 35px;
                width: 92px;
                height: 44px;
            }
        }
    }
`

const BtnBox  = styled.div`
    position: absolute;
    right: 65px;
    bottom 30px;
    .questbtn{
        width: 142px;
        height: 47px;
        cursor: pointer;
        background-size: 100% 200%;
        &.get{
            background: top center no-repeat url(${imageList['btn_redeem_ball']});
            pointer-events: visible;
        }
        &.exchange{
            background: top center no-repeat url(${imageList['btn_exchange']});
            margin-top: 10px;
            pointer-events: visible;
        }
        &:hover{
            background-position: bottom center;
        }
    }
`