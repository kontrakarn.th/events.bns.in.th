import React from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';
import { setValue, setValues,onAccountLogout } from './../../store/redux';
const UserBox = props => {
	let nameText = "";
	 if(props.showUserName && props.username !== "" && props.character != ""){
        nameText = props.username;
    }
    if(props.character !== ""){
        nameText = (nameText !== "")? nameText+" : "+props.character : props.character;
    }
	return(
		<TopData>
                {nameText !== "" &&
                    <SlotText>{nameText}</SlotText>
                }
                {props.loginStatus &&
                    <SlotBtn onClick={()=>onAccountLogout()}>Logout</SlotBtn>
                }
         </TopData>
	)
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValue, setValues };
export default connect( mapStateToProps, mapDispatchToProps )(UserBox);

const TopData = styled.div`
    /* opacity: 0; */
    position: absolute;
    top: 0px;
    right: 0px;
    display: block;
    margin-top: 17px;
    margin-right: 17px;
`;
const SlotText = styled.div`
    pointer-events: auto;
    display: inline-block;
    padding: 0px 25px;
    margin-left: 17px;
    /* width: fit-content; */
    /* max-width: 170px; */
    text-align: center;
    height: 45px;
    line-height: 45px;
    background-color: rgba(0,0,0,0.7);
    border-radius: 15px;
    color: #FFFFFF;
    font-size: 14px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    /* transition: width 0.3s ease-in-out; */
`;

const SlotBtn = styled.a`
    pointer-events: auto;
    cursor: pointer;
    display: inline-block;
    padding: 0px 20px;
    margin-left: 10px;
    width: fit-content;
    max-width: 170px;
    text-align: center;
    height: 45px;
    line-height: 45px;
    background-color: rgba(0,0,0,0.7);
    border-radius: 15px;
    color: #FFFFFF;
    font-size: 14px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;