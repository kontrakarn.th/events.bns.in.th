
const initialState = {
    // ---------
    // showUserName: true,
    username: "",
    // ---------
    // showCharacterName: true,
    character: "",
    // ---------
    // showLogin: true,
    loginStatus: false,
    // ---------
    showScroll: true,
    // ---------
    loading: false,
    permission: true,
    status: false,
    can_play: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return {
          ...state,
          [action.key]: action.value
        };
      } else {
        return state;
      }
    case "SET_VALUES": return { ...state, ...action.value };
    default:
      return state;
  }
};

export const setValue = (key, value) =>{
    // console.log("setValue",key, value);
    return ({
        type: "SET_VALUE",
        value,
        key
    })
};
export const setValues = (value) =>{
    // console.log("setValue", value);
    return ({
        type: "SET_VALUES",
        value
    })
};
