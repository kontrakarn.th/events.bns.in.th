import React from 'react';
import styled from 'styled-components';
import { imageList } from '../../constants/Import_Images';
import Redux from 'react-redux';

export default props =>{
	return(
		<PageContainer hide={props.hide}/>
	)
}

const PageContainer = styled.div`
	background: top center no-repeat url(${imageList['loading_screen']});
	background-size: cover;
	width: 1105px;
	height: 700px;
	position: absolute;
	top: 0;
	left: 0;
	display: block;
	${props=>props.hide
			&& `visibility: hidden; display: none;`
	}
`