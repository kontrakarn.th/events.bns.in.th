import React,{useEffect} from 'react';
import styled from 'styled-components';
import { imageList } from '../../constants/Import_Images';
import {connect} from 'react-redux';

const SnowBallGame =props=>{
	const snowballSizeList = {
	    "1":75,
	    "5":95,
	    "50":150
	}
	const cWidth = 1105;
	const cHeight = 700;
	const yWidth = 533;
    const yHeight = 960;
    const yScale = 0.8;
	const bossHeadWidth = 185*yScale;
	const bossHeadHeight = 168*yScale;
	const bossHeadX = (cWidth - bossHeadWidth)/2 + 5;
	const bossHeadY = 255;
	const bossBodyWidth = 445*yScale;
	const bossBodyHeight = 240*yScale;
	const bossBodyX = (cWidth - bossBodyWidth)/2 + 10;
	const bossBodyY = (cHeight - bossBodyHeight)/2 + bossHeadHeight - 20;
	const bossDistance = 20;
	const gravity = 3;// ratio unreal
	const speedZ = 2;
	const ballScalePreZ = 4;
	const damageBorderColor="127,66,0";
	const damageValueColor="255,198,24";
	let interval   = null;
    let mouseDown  = false;
    let mosueX     = 0;
    let mosueY     = 0;
    let mosueHistory=6;
    let mosuePos   = [];
    let canvas     = null;
    let ctx    = null;
    let canvasReady= false;
    let ball       = new Image();
    let balls      = 5;
    let ballSize   = snowballSizeList["1"];
    let ballThrowedList    = [];
    let ballBrokenList     = [];
    let damageList         = [];
    let ballBroken = new Image();
    let yeti       = new Image();
    let hitpoint   = 5;
    let yetiType   = -1;
    let yetiCount  = 0;
    let yetiNext   = 0;
    let yetiMax    = 24;
    let yetiReady  = false;
    let ballReady  = false;
    let ballBrokenReady = true;
	const areaYeti=()=>{
	   ctx.fillStyle = "#ff0000f0";
       ctx.fillRect(bossHeadX,bossHeadY,bossHeadWidth,bossHeadHeight);
       ctx.stroke();
       ctx.fillStyle = "#00ff0033";
       ctx.fillRect(bossBodyX,bossBodyY,bossBodyWidth,bossBodyHeight);
       ctx.stroke();
    }
	
	//mouse event=======================================
	const mouseDownHandler=(e)=>{
        if(hitpoint > 0 && balls > 0){
            if(!mouseDown){
                let mousePos = getMousePos(canvas,e);
                mosuePos=[]
                mosuePos.push(mousePos);
                mouseDown=true;
            }
        }
    }

    const mouseUpHandle=(e)=>{
        if(mouseDown){
        	let startPos = mosuePos[0];
		    let lastPos = mosuePos[mosuePos.length-1];
		    let checkThrow = Math.abs(lastPos.x-startPos.x);
			if((lastPos.y-startPos.y) < -50 || checkThrow > 25){
				gameCheckHit(mosuePos);
			}
			mouseDown = false;
        }
    }

    const mouseMoveHandle=(e)=>{
        if(mouseDown){
            let mousePos = getMousePos(canvas,e);
            mosuePos.push(mousePos);
            while(mosuePos.length > mosueHistory){
                mosuePos.shift();
            }
        }
    }

	  const getMousePos=(canvas,e)=>{
	    var rect = canvas.getBoundingClientRect();
	    return {
	        x: e.clientX - rect.left,
	        y: e.clientY - rect.top
	    };
	  }
	//end mouse event ======================================
		
	//draw yeti  ===========================================
    const drawYeti=(count)=>{
        let px = yWidth * (count % 6);
        let py = yHeight * Math.floor(count / 6);
        ctx.drawImage(yeti,px, py,yWidth,yHeight,(cWidth-yWidth*yScale)/2,(cHeight-yHeight*yScale)/2,yWidth*yScale,yHeight*yScale);
    }
    const updateYeti=()=>{
        let count = yetiCount = yetiNext;
        if(canvasReady && yetiReady){
            updateYeti(count);
        }
        yetiNext += 1;
        if(yetiNext >= 24) yetiNext -= 24 ;
    }
	const setYetiSpriteSheet=(n)=>{
        n = (n % 4);
        switch(n){
            case 1 :
                yeti.src = imageList['yeti_blue'];
                break;
            case 2 :
                yeti.src = imageList['yeti_red'];
                break;
            case 3 :
                yeti.src = imageList['yeti_colorful'];
                break;
            default :
                yeti.src = imageList['yeti_default'];
        }
    }
    //end draw yeti ============================================
	
	//start snowball ===============================================
		const drawSnowball=(ballX,ballY,ballSize)=>{
	        ballSize = Math.max(0,ballSize);
	        ctx.drawImage(ball,0,0,95,95,ballX-(ballSize/2),ballY-(ballSize/2),ballSize,ballSize);
	    }
	     const drawSnowballBroken=(count,ballX,ballY,ballSize)=>{
	     	let effesizeW = 378 / 3;
	     	let effesizeH = 354 /3;
	        let px = effesizeW * (count % 3);
	        let py = effesizeH * Math.floor(count / 3);
	        ballSize = Math.max(0,ballSize);
	        ctx.drawImage(ballBroken,px, py,effesizeW,effesizeH,ballX-(ballSize/2),ballY-(ballSize/2),ballSize,ballSize);
	    }
	     const updateBall=(e)=>{
	        if(ballReady && ballBrokenReady){
	            if(mouseDown){
	                let ms = mosuePos[mosuePos.length-1];
	                drawSnowball(ms.x,ms.y,ballSize);
	            }
	        }
	    }
	//end snowball ===============================================

	//start DrawHit==============================================
		const drawDamage=(value,x,y,fillStyle,strokeStyle)=>{
	        ctx.font = "60px GOODBRUSH";
	        ctx.lineWidth = 3;
	        ctx.fillStyle = fillStyle;
	        ctx.strokeStyle = strokeStyle;
	        ctx.fillText("-"+value,x,y);
	        ctx.strokeText("-"+value,x,y);
	    }

	//end DraweHit==============================================

	//start CalculateHit ================================================
		const gameCheckHit=(mousePos)=>{
	        let startPos = mousePos[0];
	        let lastPos = mousePos[mousePos.length-1];
	        let ux = (lastPos.x-startPos.x)*0.25;//smae Math.cos()* U;
	        let uy = (lastPos.y-startPos.y)*0.25;//smae Math.sin()* U;
	        let hit = "none";
	        let count = Math.floor(bossDistance / speedZ);
	        let positionList = calPositionMove([lastPos.x,lastPos.y],[ux,uy],[0,gravity],count+1);
	        let dropX = positionList[count][0];
	        let dropY = positionList[count][1];
	        // check head
	        if(dropX >= bossHeadX
	            && dropX <= bossHeadX+bossHeadWidth
	            && dropY >= bossHeadY
	            && dropY <= bossHeadY+bossHeadHeight){
	                // console.log("head");
	            hit="head";
	        } else  if(dropX >= bossBodyX
	            && dropX <= bossBodyX+bossBodyWidth
	            && dropY >= bossBodyY
	            && dropY <= bossBodyY+bossBodyHeight){
	                // console.log("body");
	            hit="body";
	        }
	        gameSetBallThrowed(ballSize,lastPos.x,lastPos.y,positionList,hit);
	    }

		 const calPositionMove=(positionStart,velocity,acceleration,count)=>{
	        let ary=[positionStart];
	        let nextPosition    = [positionStart[0] + velocity[0] ,positionStart[1] + velocity[1]];
	        let newVelocity     = [velocity[0] + acceleration[0]  ,velocity[1] + acceleration[1]];

	        if(count === 0) return ary;
	        return ary.concat(calPositionMove(nextPosition,newVelocity,acceleration,count-1));
	    }

	    const gameSetBallThrowed=(ballType,positionX,positionY,positionList,hitBossType)=>{
	        let ball = {
	            size: ballType,
	            x: positionX,
	            y: positionY,
	            count: 0,
	            positionList: positionList,
	            hitBossType: hitBossType,
	            active: true,
	            hitpoint: 0
	        }
	        ballThrowedList.push(ball);
	    }

	    const updateBallThrowed=(backOfBoss)=>{
	        if(ballThrowedList.length>0){
	            let positionX   = 0;
	            let positionY   = 0;
	            let ballSize    = 0;
	            let item;
	            let index       = 0;
	            for(let i=0; i<ballThrowedList.length; i++ ){
	                item        = ballThrowedList[i];
	                index       = i;
	                positionX   = item.positionList[item.count][0];
	                positionY   = item.positionList[item.count][1];
	                ballSize    = item.size - (item.count*ballScalePreZ);
	                if(item.active){
	                    if(item.count < item.positionList.length-1){
	                        drawSnowball(positionX,positionY,ballSize);
	                        item.count++;
	                    } else {
	                        drawSnowball(positionX,positionY,ballSize);
	                        let ballBroken = {x:positionX,y:positionY,active:true,count:0,size:ballSize}
	                        ballBrokenList.push(ballBroken);
	                        // props.actPlay(this.props.selectBall,(item.hitBossType==="head"),(item.hitBossType==="none"),
	                        // 	(damage)=>{this.addDamageList(damage,positionX,positionY,item.hitBossType==="head");
	                        // });
	                        addDamageList(5000,positionX,positionY,item.hitBossType==="head");
	                        ctx.fillStyle = "#ff000033";
						    ctx.fillRect(0,0,1105,700);
	                        item.active =false;
	                    }
	                }else {
	                    ballThrowedList.splice(index, 1);
	                    i--;
	                }
	            }
	        }
	    }
	    const ballBrokenUpdate=()=>{
	        if(canvasReady && ballBrokenReady){
	            if(ballBrokenList.length > 0 ){
	                let item;
	                let index= 0;
	                for(let i=0; i<ballBrokenList.length; i++ ){
	                    item  = ballBrokenList[i];
	                    index = i;
	                    if(item.active){
	                        if(item.count<9){
	                            drawSnowballBroken(item.count,item.x,item.y,item.size*2);
	                            item.count++;
	                        } else {
	                            item.active = false;
	                        }
	                    } else {
	                        ballBrokenList.splice(index, 1);
	                        i--;
	                    }
	                }
	            }
	        }
	    }
	    const addDamageList=(damage,positionX,positionY,head=false)=>{
	        let dam = {
	            damage: damage,
	            x: positionX,
	            y: positionY,
	            count: 0,
	            special: head,
	            active: true
	        }
	        damageList.push(dam);
	    }
		const updateDamage=()=>{
	        if(damageList.length>0){
	            let item,dam,px,py,fillStyle,strokeStyle;
	            for(let i=0; i<damageList.length; i++ ){
	                item = damageList[i];
	                dam = "-"+item.damage
	                px = item.x;
	                py = item.y-(item.count*2);
	                fillStyle = "white";
	                strokeStyle = "black";
	                if(item.special){
	                    fillStyle = "rgba("+damageValueColor+","+Math.min(1,(30-item.count)/15)+")";
	                    strokeStyle = "rgba("+damageBorderColor+","+Math.min(1,(30-item.count)/15)+")";
	                }

	                if(item.active){
	                    if(item.count < 30){
	                        drawDamage(item.damage,px,py,fillStyle,strokeStyle)
	                        item.count++;
	                    } else {
	                        item.active = false;
	                    }
	                }else {
	                    damageList.splice(i, 1);
	                    i--;
	                }
	            }
	        }
	    }
	//end CalculateHit==============================================

    const gameClear=()=> {
        ctx.clearRect(0, 0, cWidth, cHeight);
    }
    const gameUpdate=()=>{
    	gameClear();
        updateYeti();
        drawYeti(yetiCount);
        // areaYeti();
        updateDamage();
        updateBall();
        updateBallThrowed();
        ballBrokenUpdate();
    }
	useEffect(()=>{
		canvas = document.getElementById("snowballgame");
		ballBroken.src= imageList['snowball_effect'];
		ball.src= imageList['snowball'];
		 if(canvas !== null){
		 	ctx = canvas.getContext("2d");
			ctx.fillStyle = 'transparent';
			ctx.fillRect(0,0,cWidth,cHeight);
            canvas.addEventListener("mousedown",(e)=>mouseDownHandler(e));
            canvas.addEventListener("mouseup",(e)=>mouseUpHandle(e));
            canvas.addEventListener("mousemove",(e)=>mouseMoveHandle(e));
            canvasReady = true;
		 	setYetiSpriteSheet(0);
		 	interval = setInterval(()=>gameUpdate(), 1000/24);
		 }
	},[])
	return(
		 <Game
            id="snowballgame"
            width={cWidth}
            height={cHeight}
            style={{opacity:1}}
         />
	)
}
const mapStateToProps = state => ({...state.Main});
export default connect(mapStateToProps,{})(SnowBallGame)

const Game = styled.canvas`
	pointer-events: all;

`