import bar_cover from '../static/images/bar_cover.png';
import btn_exchange from '../static/images/btn_exchange.png';
import btn_redeem_ball from '../static/images/btn_redeem_ball.png';
import h2p_tooltip from '../static/images/h2p_tooltip.png';
import icon_h2p from '../static/images/icon_h2p.png';
import loading_screen from '../static/images/loading_screen.jpg';
import logo from '../static/images/logo.png';
import main_bg from '../static/images/main_bg.png';
import snowball_effect from '../static/images/snowball_effect.png';
import snowball from '../static/images/snowball.png';
import title_text from '../static/images/title_text.png';
import yeti_blue from '../static/images/yeti_blue.png';
import yeti_colorful from '../static/images/yeti_colorful.png';
import yeti_default from '../static/images/yeti_default.png';
import yeti_red from '../static/images/yeti_red.png';
import level_box from '../static/images/level_bg.png';
import frame_exchange from '../static/images/frame_exchange.png';
import frame_quest from '../static/images/frame_quest.png';
import btn_close from '../static/images/btn_close.png';
import icon_quest from '../static/images/icon_quest.png';
import btn_redeem from '../static/images/btn_redeem.png';
import btn_exchange_modal from '../static/images/btn_exchange_modal.png';
import modal_tooltips from '../static/images/modal_tooltips.png';
import modal_bg from '../static/images/modal_bg.png';
import btn_confirm from '../static/images/btn_confirm.png';
import btn_cancel from '../static/images/btn_cancel.png';
import selected from '../static/images/item/selected.png';
import item_1 from '../static/images/item/item_1.png';
import item_2 from '../static/images/item/item_2.png';
import item_3 from '../static/images/item/item_3.png';
import item_4 from '../static/images/item/item_4.png';
import item_5 from '../static/images/item/item_5.png';
import item_6 from '../static/images/item/item_6.png';
import item_7 from '../static/images/item/item_7.png';
import item_8 from '../static/images/item/item_8.png';
import item_9 from '../static/images/item/item_9.png';
import mock from '../static/images/mock.png';
import modal_mock from '../static/images/modal_mock.png';

export const imageList = {
	bar_cover,
	btn_exchange,
	btn_redeem_ball,
	h2p_tooltip,
	icon_h2p,
	loading_screen,
	logo,
	main_bg,
	snowball_effect,
	snowball,
	title_text,
	yeti_blue,
	yeti_colorful,
	yeti_default,
	yeti_red,
	level_box,
	frame_exchange,
	frame_quest,
	btn_close,
	icon_quest,
	btn_redeem,
	selected,
	item_1,
	item_2,
	item_3,
	item_4,
	item_5,
	item_6,
	item_7,
	item_8,
	item_9,
	btn_exchange_modal,
	modal_tooltips,
	modal_bg,
	btn_cancel,
	btn_confirm,

	mock,
	modal_mock
}