const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    showCharacterName: true,
    characters:[],
    username: "asdasdasd",
    coin: 0,
    character: "asdasdasd",
    selected_char: false,
    loginStatus: true,
    showScroll: true,
    noScroll: true,
    loading: false,
    permission: true,
    status: false,
    can_play: false,
    quest_list:  Array(6).fill(1), // default array []
    //===modal
    modal_open: "",//"loading","detail","showreward",""showreward","send","modalInput"
    modal_message: "ต้องการแลกของรางวัล",
    modal_item: "",

    //=== EVENT ===//
    snowball_amt: 1,
    //====Animation ===//

    //---history values
    history_list: [],

    //---main
    clan_members: 0,
    clan_ranks: [],
    clan_rewards: {
        step1: {id: 1, can_claim: false, is_claimed: false},
        step2: {id: 2, can_claim: false, is_claimed: false},
        step3: {id: 3, can_claim: false, is_claimed: false},
    },
    quests: [],
    rank_rewards: {},
    score: "0",
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         : return ({ ...state, sessionKey: action.value });
        case 'ACCOUNT_AUTHED'       : return ({ ...state, userData: action.value });
        case 'RECEIVE_SESSION_KEY'  : return ({ ...state, sessionKey: action.value });
        case 'SET_LOGIN_URL'        : return ({ ...state, loginUrl: action.value });
        case 'SET_LOGOUT_URL'       : return ({ ...state, logoutUrl: action.value });
        case 'ACCOUNT_LOGIN'        : return ({ ...state, authState: "LOGGED_IN" });
        case 'ACCOUNT_LOGOUT'       : return ({ ...state, authState: "LOGGED_OUT" });
        case 'ACCOUNT_AUTH_CHECKED' : return ({ ...state, authed: true });
        case 'SET_JWT_TOKEN'        : return ({ ...state, jwtToken: action.value });

        case "SET_VALUE"            : return (Object.keys(defaultState).indexOf(action.key) >= 0) ? { ...state, [action.key]: action.value } : state;
        case "SET_VALUES"           : return { ...state, ...action.value };
        default:
            return state;
    }
}

export const onAccountAuth          = (sessionKey)  => ({ type: 'ACCOUNT_AUTH',         value: sessionKey });
export const onAccountAuthed        = (userData)    => ({ type: 'ACCOUNT_AUTHED',       value: userData });
export const onAccountLogin         = ()            => ({ type: 'ACCOUNT_LOGIN',        value: "" });
export const onAccountLogout        = ()            => ({ type: 'ACCOUNT_LOGOUT',       value: "" });
export const receiveSessionKey      = (sessionKey)  => ({ type: 'RECEIVE_SESSION_KEY',  value: sessionKey });
export const setLoginUrl            = (url)         => ({ type: 'SET_LOGIN_URL',        value: url });
export const setLogoutUrl           = (url)         => ({ type: 'SET_LOGOUT_URL',       value: url });
export const onAccountAuthChecked   = ()            => ({ type: 'ACCOUNT_AUTH_CHECKE',  value: true })
export const setJwtToken            = (token)       => ({ type: 'SET_JWT_TOKEN',        value: token });

export const setValue               = (key, value) => ({ type: "SET_VALUE", value, key });
export const setValues              = (value) => ({ type: "SET_VALUES", value });
