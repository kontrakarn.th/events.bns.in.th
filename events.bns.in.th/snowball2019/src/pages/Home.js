import React from 'react';
import Main from './../features/main';
import F11Layout from '../features/F11Layout';
export class Home extends React.Component {
    render() {
        return (
        	<F11Layout
        		showUserName={false}
                showCharacterName={true}
        	>
	            <Main/>
        	</F11Layout>
        )
    }
}
