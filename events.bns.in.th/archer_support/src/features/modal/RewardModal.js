import React from 'react';
import ModalCore from './ModalCore.js';
import styled from 'styled-components';
import board_reward from './../../static/images/board_reward.png';
import close from './../../static/images/btn_close.png';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';

const RewardModal = (props) => {
	return (
		<ModalCore name="reward" outSideClick={true}>
            <Reward>
                <Close onClick={() => props.setValues({ modal_open: '' })}>
                    <img src={close}/>
                </Close>
            </Reward>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RewardModal);

const Reward = styled.div`
    position: relative;
    pointer-events: visible;
    background:url(${board_reward})top center no-repeat;
    background-size: cover;
    width: 965px;
    height 622px;
`;

const Close = styled.div`
    position: absolute;
    top: -33px;
    right: 0;
    width: 40px;
    cursor: pointer;
`;
