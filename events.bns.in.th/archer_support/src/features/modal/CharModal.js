import React from 'react';
import  ModalCore from './ModalCore.js';
import styled from 'styled-components';
import board_char from './../../static/images/board_char.png';
import bg_select from './../../static/images/bg_select.png';
import btn_ok from './../../static/images/btn_ok-all.png';
import { apiPost } from './../../middlewares/Api';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';

const CharModal = (props) => {

	return (
		<ModalCore name="char" outSideClick={false}>
			<Char className="char">
				<div className="char__inner">
                    <select name="select_char" id="select_char" className="char__option">
                        <option value="-1"> -- ตัวละคร --</option>
						{
							props.characters && props.characters.length>0 && props.characters.map((value,key)=>{
								return(
									<option value={value.char_id}>{value.char_name}</option>
								)
							})
						}
                    </select>
				</div>
				<Ok onClick={() => props.apiSelectChar()}></Ok>
			</Char>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CharModal);

const Char = styled.div`
    position: relative;
    pointer-events: visible;
    background:url(${board_char})top center no-repeat;
    background-size: cover;
    width: 746px;
    height 501px;
	.char{
		&__inner{
			position: absolute;
			top: 60%;
			left: 50%;
			transform: translate(-50%, -50%);
			width: 612px;
			text-align: center;
		}
        &__option{
            background:url(${bg_select})top center no-repeat;
            background-size: 100%;
            -webkit-appearance: none;
            display: inline-block;
            color: #ececeb;
            width: 313px;
            height: 65px;
            font-size: 30px;
            padding-left: 20px;
            font-family: 'Kanit-ExtraLight', tahoma;
            border: none;
            >option{
                color: #000;
            }
        }
	}
`;
const Ok = styled.div`
	position: absolute;
	width: 248px;
	height: 70px;
	background:url(${btn_ok})top center no-repeat;
	background-size: 100%;
	cursor: pointer;
	bottom: -32px;
	left: 50%;
	transform: translate(-50%, 0);
	&:hover{
		background-position: bottom center;
	}
`;
