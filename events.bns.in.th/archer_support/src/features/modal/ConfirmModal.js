import React from "react";
import { connect } from "react-redux";
import ModalCore from "./ModalCore.js";
import styled from 'styled-components';
import board_confirm from './../../static/images/board_confirm.png';
import btn_ok from './../../static/images/btn_ok-all.png';
import btn_no from './../../static/images/btn_no-all.png';
import { setValues, actCloseModal } from "./redux";

const ConfirmModal = (props) => {
	return (
    <ModalCore name="confirm" outSideClick={false}>
		<Confirm className="confirm">
			<div className="confirm__inner">
				<div className="confirm__text" dangerouslySetInnerHTML={{ __html: props.modal_message }}></div>
			</div>
			<Ok onClick={()=>{if(props.actConfirm) props.actConfirm(props.item_index)}}></Ok>
			<No onClick={() => props.setValues({ modal_open: '' })}></No>
		</Confirm>
    </ModalCore>
	)
}

const mapStateToProps = state => ({
  ...state.ModalReducer,
  ...state.Modal
});

const mapDispatchToProps = { setValues, actCloseModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmModal);

const Confirm = styled.div`
    position: relative;
    pointer-events: visible;
    background:url(${board_confirm})top center no-repeat;
    background-size: cover;
    width: 746px;
	height 501px;
	.confirm{
		&__inner{
			position: absolute;
			top: 60%;
			left: 50%;
			transform: translate(-50%, -50%);
			width: 612px;
			text-align: center;
		}
		&__text{
			font-family: 'Kanit-ExtraLight', tahoma;
			font-size: 40px;
			color: #fff;
		}
	}
`;

const Ok = styled.div`
	position: absolute;
	width: 248px;
	height: 70px;
	background:url(${btn_ok})top center no-repeat;
	background-size: 100%;
	cursor: pointer;
	bottom: -32px;
	left: 30%;
	transform: translate(-50%, 0);
	&:hover{
		background-position: bottom center;
	}
`;
const No = styled.div`
	position: absolute;
	width: 248px;
	height: 70px;
	background:url(${btn_no})top center no-repeat;
	background-size: 100%;
	cursor: pointer;
	bottom: -32px;
	left: 70%;
	transform: translate(-50%, 0);
	&:hover{
		background-position: bottom center;
	}
`;
