import React from 'react';
import  ModalCore from './ModalCore.js';
import styled from 'styled-components';
import board_message from './../../static/images/board_message.png';
import btn_ok from './../../static/images/btn_ok-all.png';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';

const MessageModal = (props) => {
	return (
		<ModalCore name="message" outSideClick={props.go_select_char ? false : true}>
			<Message className="message">
				<div className="message__inner">
					<div className="message__text" dangerouslySetInnerHTML={{ __html: props.modal_message }}></div>
				</div>
				{
					props.go_select_char
					?
						<Ok onClick={() => props.setValues({ modal_open: 'char',go_select_char:false })}></Ok>
					:
						<Ok onClick={() => props.setValues({ modal_open: '',go_select_char:false })}></Ok>
				}
			</Message>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageModal);

const Message = styled.div`
    position: relative;
    pointer-events: visible;
    background:url(${board_message})top center no-repeat;
    background-size: cover;
    width: 746px;
	height 509px;
	.message{
		&__inner{
			position: absolute;
			top: 60%;
			left: 50%;
			transform: translate(-50%, -50%);
			width: 612px;
			text-align: center;
		}
		&__text{
			font-family: 'Kanit-ExtraLight', tahoma;
			font-size: 40px;
			color: #fff;
		}
	}
`;
const Ok = styled.div`
	position: absolute;
	width: 248px;
	height: 70px;
	background:url(${btn_ok})top center no-repeat;
	background-size: 100%;
	cursor: pointer;
	bottom: -32px;
	left: 50%;
	transform: translate(-50%, 0);
	&:hover{
		background-position: bottom center;
	}
`;
