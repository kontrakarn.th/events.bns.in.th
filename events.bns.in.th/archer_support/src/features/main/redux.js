// import axios from "axios";
const initialState = {
    // modal_open: "",
    message: "",
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return { ...state, ...action.value };
      } else {
        return state;
      }
    default:
      return state;
  }
};

// export const setValue  = (key, value) => ({ type: "SET_VALUE", value, key });
export const setValue  = (key, value) =>{ return ({ type: "SET_VALUE", value, key }) };
export const setValues = (value) =>{ return ({ type: "SET_VALUES", value }) };
// export const submit = (fullName, email) => ({
//   type: "SUBMIT",
//   // payload: axios.post("http://www.mocky.io/v2/5bfbb9c23100002b0039b9c1")
//   payload: axios.post("http://www.mocky.io/v2/5bfbc91a310000730039ba6c")
// });
