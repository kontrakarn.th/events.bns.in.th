import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import styled from 'styled-components';
import { apiPost } from './../../middlewares/Api';

import { onAccountLogout } from './../../actions/AccountActions';

import { setValues } from "./redux";

import { ConfirmModal, MessageModal, LoadingModal, RewardModal, CharModal } from "../../features/modal";

import {Imglist} from './../../constants/Import_Images';

class Main extends Component {
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiEventInfo();
        }
    }

    apiEventInfo(){
        this.props.setValues({'modal_open':'loading'})
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {

                if(data.data.selected_char==false){
                    this.props.setValues({...data.data,'modal_open':'char'})
                }else{
                    this.props.setValues({...data.data,'modal_open':''})
                }
                // this.props.setValues({'username':data.data.username,'character_name':data.data.character_name})
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    permission: false,
                    showModalMessage: "",
                });
            }else {
                this.props.setValues({'modal_open':'message','modal_message':data.message})
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSelectChar(){

        var char_id=document.getElementById('select_char').value
        if(char_id<=0){
            this.props.setValues({'modal_open':'message','modal_message':'โปรดเลือกตัวละคร','go_select_char':true})
        }else{
            this.props.setValues({'modal_open':'loading'})
            let dataSend = {type:'select_character',id:char_id};
            let successCallback = (data) => {
                    this.props.setValues({...data.data,'modal_open':''})
            };
            const failCallback = (data) => {
                if (data.type == 'no_permission') {
                    this.setState({
                        permission: false,
                        showModalMessage: "",
                    });
                }else {
                    this.props.setValues({'modal_open':'message','modal_message':data.message})
                }
            };
            apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
        }

    }

    redeemItem(){
        this.props.setValues({'modal_open':'loading'})
        let dataSend = {type:'redeem'};
        let successCallback = (data) => {
            this.props.setValues({...data.data,'modal_open':''})
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    permission: false,
                    showModalMessage: "",
                });
            }else {
                this.props.setValues({...data.data,'modal_open':'message','modal_message':data.message})
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }


    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scollDown === false) this.setState({ scollDown: true });
        } else {
            if (this.state.scollDown === true) this.setState({ scollDown: false });
        }
    }

    handleViewReward(){
        this.props.setValues({
            modal_open: "reward",
        });
        // console.log('clicktoviewreward');
    }
    handleClaim(index){
        this.props.setValues({
            modal_open: "confirm",
            modal_message: "ยืนยันการรับรางวัล ภารกิจที่ "+index,
        });
    }

    render() {
        return (
            <div className="events-bns">
                {this.state.permission ?
                    <Archer className="archer">
                        <div className="archer__inner">
                            <div className="archer__board">
                                <img src={Imglist['board_1']} alt=''/>
                                <ul className="archer__list">
                                    {this.props.quest_log_step_1 && this.props.quest_log_step_1.quest_log && this.props.quest_log_step_1.quest_log.map((item,index) => {
                                        return (
                                            <li key={"quest_1_"+index}>
                                                <span><img src={Imglist[(item.status ? 'check_done' : 'check_no')]} alt=''/></span>
                                                {item.name}
                                            </li>
                                        )
                                    })}
                                </ul>
                                {
                                    this.props.quest_log_step_1.status_step==true && this.props.quest_log_step_1.received==false
                                    ?
                                        <div className="archer__btn-claim archer__btn-claim--1" onClick={()=>{this.handleClaim(1)}}/>
                                    :
                                        this.props.quest_log_step_1.status_step==false && this.props.quest_log_step_1.received==false
                                        ?
                                            <div className="archer__btn-claim archer__btn-claim--1 disable"/>
                                        :
                                            <div className="archer__btn-claim archer__btn-claim--1 claimed"/>
                                }
                            </div>
                            <div className="archer__board archer__board--2">
                                <img src={Imglist['board_2']} alt=''/>
                                <ul className="archer__list">
                                    {this.props.quest_log_step_2 && this.props.quest_log_step_2.quest_log && this.props.quest_log_step_2.quest_log.map((item,index) => {
                                        return (
                                            <li key={index}>
                                                <span><img src={Imglist[(item.status?'check_done':'check_no')]} alt=''/></span>
                                                {item.name}
                                            </li>
                                        )
                                    })}
                                </ul>
                                {
                                    this.props.quest_log_step_2.status_step==true && this.props.quest_log_step_2.received==false
                                    ?
                                        <div className="archer__btn-claim archer__btn-claim--2" onClick={()=>{this.handleClaim(2)}}/>
                                    :
                                        this.props.quest_log_step_2.status_step==false && this.props.quest_log_step_2.received==false
                                        ?
                                            <div className="archer__btn-claim archer__btn-claim--2 disable"/>
                                        :
                                            <div className="archer__btn-claim archer__btn-claim--2 claimed"/>
                                }
                            </div>
                            <div className="archer__board archer__board--3">
                                <img src={Imglist['board_3']} alt=''/>
                                <ul className="archer__list">
                                {this.props.quest_log_step_3 &&
                                    this.props.quest_log_step_3.quest_log && this.props.quest_log_step_3.quest_log.map((item,index) => {
                                    return (
                                        <li key={index}>
                                            <span><img src={Imglist[(item.status ? 'check_done':'check_no')]} alt=''/></span>
                                            {item.name}
                                        </li>
                                    )
                                })}
                                </ul>
                                <img className="archer__btn-view" src={Imglist['btn_view']} onClick={()=>{this.handleViewReward()}}/>
                                {
                                    this.props.quest_log_step_3.status_step==true && this.props.quest_log_step_3.received==false
                                    ?
                                        <div className="archer__btn-claim archer__btn-claim--3" onClick={()=>{this.handleClaim(3)}}/>
                                    :
                                        this.props.quest_log_step_3.status_step==false && this.props.quest_log_step_3.received==false
                                        ?
                                            <div className="archer__btn-claim archer__btn-claim--3 disable"/>
                                        :
                                            <div className="archer__btn-claim archer__btn-claim--3 claimed"/>
                                }
                            </div>
                        </div>
                        <LoadingModal/>
                        <CharModal apiSelectChar={this.apiSelectChar.bind(this)}/>
                        <MessageModal/>
                        <ConfirmModal actConfirm={this.redeemItem.bind(this)}/>
                        <RewardModal/>
                    </Archer>
                    :
                    null
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.Modal});

const mapDispatchToProps = {
    onAccountLogout,
    setValues
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main)

const Archer = styled.div`
    width: 1105px;
    min-height: 300vh;
    background: #110f0a url(${Imglist['bg']}) no-repeat top center;
    padding-top: 794px;
    .archer{
        &__inner{
            text-align: center;
        }
        &__board{
            position: relative;
            &--2{
                margin-top: 180px;
            }
            &--3{
                margin-top: 184px;
            }
        }
        &__btn-claim{
            position: absolute;
            width: 425px;
            height: 96px;
            background:url(${Imglist['btn_claim']})top center no-repeat;
            background-size: 100%;
            cursor: pointer;
            &--1{
                bottom: -46px;
                left: 339px;
            }
            &--2{
                bottom: -46px;
                left: 339px;
            }
            &--3{
                bottom: 167px;
                left: 339px;
            }
            &:hover{
                background-position: bottom center;
            }
            &.disable{
                background:url(${Imglist['btn_claimdisable']})top center no-repeat;
                background-size: 100%;
                cursor: default;
            }
            &.claimed{
                background:url(${Imglist['btn_claimed']})top center no-repeat;
                background-size: 100%;
                cursor: default;
            }
        }
        &__list{
            position: absolute;
            color: #fff;
            font-family: 'Kanit-ExtraLight';
            font-size: 26px;
            top: 322px;
            left: 254px;
            text-align: left;
            >li{
                >span{
                    margin-right: 36px;
                    >img{
                        width: 35px;
                    }
                }
                margin: 22px 0;
            }
        }
        &__btn-view{
            position: absolute;
            top: 557px;
            left: 207px;
            cursor: pointer;
        }
    }
`;
