import React from 'react';
import Modal from './Modal';
import items from '../static/images/items.png'
export default class ModalConfirmReceiveGacha extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                title="แจ้งเตือน"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox">
                    <img src={items}/>
                   <h2>ยินดีด้วย คุณได้รับ</h2>
                    <div className="text--brown">package name(1)</div>
                </div>
            </Modal>
        )
  }
}
