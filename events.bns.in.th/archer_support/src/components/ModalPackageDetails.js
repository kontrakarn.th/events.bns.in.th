import React from 'react';
import Modal from './Modal';
import items from '../static/images/items.png'

export default class ModalPackageDetails extends React.Component {
    render() {
        let title = this.props.data && this.props.data || [];
        return (           
                <Modal
                    open={this.props.open}
                    title="package name"
                    confirmMode={false}
                    actConfirm={()=>this.props.actConfirm()}
                    actClose={()=>this.props.actClose()}
                    page={'packageDetail'}
                >
                    <div onClick={()=>this.props.actClose()} className="icon-close">x</div>
                    <div className="modalPackage__wrapper">

                        <div className="itemsInline">
                            <div className="itemsInline__img">
                                <img src={items}/>
                            </div>
                            <div className="itemsInline__content">
                                <div>name</div>
                                <div>(1)</div>
                            </div>
                        </div>
                    </div>
                    <div className="inbox" dangerouslySetInnerHTML={{__html: this.props.msg}} />
                </Modal>
        )
  }
}
