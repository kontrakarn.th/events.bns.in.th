import React from 'react';
import { Link } from 'react-router-dom';
import iconMenu from './../static/images/icon_menu.png';
import iconMenuClose from './../static/images/menu_close.png';
export class Menu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showMenu:false

        }
    }

    handleClickMenu() {
        this.setState({
            showMenu:!this.state.showMenu
        })
    }

    render() {
        return (
            <div className="left__menu">
                <div className={"bg-overlay " + (this.state.showMenu ? "show" : " ") }></div>
                <a onClick={this.handleClickMenu.bind(this)} className="menu-icon"><img alt="icon" src={iconMenu} /></a>
                <ul className={"left__menu--list " + (this.state.showMenu ? "show" : " ") }>
                    <li className="menu-close"><a onClick={this.handleClickMenu.bind(this)}></a></li>
                    <li><Link to="/olympia_gachapon">หน้าหลัก</Link></li>
                    <li><Link to="/olympia_gachapon/history">ประวัติ</Link></li>
                    <li><Link to="/olympia_gachapon/exchange">ระบบแลกเปลี่ยน</Link></li>
                </ul>

            </div>
        )
    }
}
