import iconHome from '../static/images/buttons/btn_home.png';
import iconScroll from '../static/images/icons/icon_scroll.png';

import bg from '../static/images/background.jpg';
import board_1 from '../static/images/board_1.png';
import board_2 from '../static/images/board_2.png';
import board_3 from '../static/images/board_3.png';
import check_done from '../static/images/check_done.png';
import check_no from '../static/images/check_no.png';
import btn_view from '../static/images/btn_view.png';
import btn_claim from '../static/images/btn_claim-all.png';
import btn_claimdisable from '../static/images/btn_claim-disable.png';
import btn_claimed from '../static/images/btn_claimed.png';


export const Imglist = {
	iconHome,
	iconScroll,

	bg,
	board_1,
	board_2,
	board_3,
	check_done,
	check_no,
	btn_view,
	btn_claim,
	btn_claimdisable,
	btn_claimed
}