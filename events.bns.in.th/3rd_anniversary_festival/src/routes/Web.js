import React, {Component} from 'react';
import {Route} from 'react-router-dom';

import OauthLogin from './../middlewares/OauthLogin';
import Modals from './../features/modals';
import Home from './../pages/Home';

export const Web = (props) => {
  let dmp = process.env.REACT_APP_EVENT_PATH;

    //
  return (
    <>
      <Route path={""} component={OauthLogin} />
      <Route path={""} component={Modals} />
      <Route path={dmp} exact component={Home} />
    </>
  );
}
