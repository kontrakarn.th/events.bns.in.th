const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    showCharacterName: true,
    characters:[],
    username: "",
    character_name: "",
    coin: 0,
    selected_char: false,
    loginStatus: false,
    showScroll: true,
    showMenu: false,
    noScroll: true,
    loading: false,
    permission: true,
    status: false,
    menu: [
      {title: "หน้าหลัก",     link:process.env.REACT_APP_EVENT_PATH },
      {title: "ส่งของขวัญ",   link:process.env.REACT_APP_EVENT_PATH+"/gift" },
      {title: "ประวัติการแลก",link:process.env.REACT_APP_EVENT_PATH+"/history" },
    ],
    //=== MODAL ===//
    modal_open: "loading",//"loading","selectcharacter","confirm"
    modal_message: "ต้องการเปิดกล่อง?<br/>ต้องการเปิดกล่อง?<br/>ต้องการเปิดกล่อง?",//ต้องการเปิดกล่อง?

    //=== EVENT ===//
    status: false,
    current_points: 0,
    current_percentage: 0,
    rewards: [
      {id: 0,title: "",points: 0,points_text: "",can_claim: false,claimed: false},
      {id: 1,title: "",points: 0,points_text: "",can_claim: false,claimed: false},
      {id: 2,title: "",points: 0,points_text: "",can_claim: false,claimed: false},
      {id: 3,title: "",points: 0,points_text: "",can_claim: false,claimed: false},
      {id: 4,title: "",points: 0,points_text: "",can_claim: false,claimed: false},
    ],
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         : return ({ ...state, sessionKey: action.value });
        case 'ACCOUNT_AUTHED'       : return ({ ...state, userData: action.value });
        case 'RECEIVE_SESSION_KEY'  : return ({ ...state, sessionKey: action.value });
        case 'SET_LOGIN_URL'        : return ({ ...state, loginUrl: action.value });
        case 'SET_LOGOUT_URL'       : return ({ ...state, logoutUrl: action.value });
        case 'ACCOUNT_LOGIN'        : return ({ ...state, authState: "LOGGED_IN" });
        case 'ACCOUNT_LOGOUT'       : return ({ ...state, authState: "LOGGED_OUT" });
        case 'ACCOUNT_AUTH_CHECKED' : return ({ ...state, authed: true });
        case 'SET_JWT_TOKEN'        : return ({ ...state, jwtToken: action.value });

        case "SET_VALUE"            : return (Object.keys(defaultState).indexOf(action.key) >= 0) ? { ...state, [action.key]: action.value } : state;
        case "SET_VALUES"           : return ({ ...state, ...action.value });
        default                     : return state;
    }
}

export const onAccountAuth          = (sessionKey)  => ({ type: 'ACCOUNT_AUTH',         value: sessionKey });
export const onAccountAuthed        = (userData)    => ({ type: 'ACCOUNT_AUTHED',       value: userData });
export const onAccountLogin         = ()            => ({ type: 'ACCOUNT_LOGIN',        value: "" });
export const onAccountLogout        = ()            => ({ type: 'ACCOUNT_LOGOUT',       value: "" });
export const receiveSessionKey      = (sessionKey)  => ({ type: 'RECEIVE_SESSION_KEY',  value: sessionKey });
export const setLoginUrl            = (url)         => ({ type: 'SET_LOGIN_URL',        value: url });
export const setLogoutUrl           = (url)         => ({ type: 'SET_LOGOUT_URL',       value: url });
export const onAccountAuthChecked   = ()            => ({ type: 'ACCOUNT_AUTH_CHECKE',  value: true })
export const setJwtToken            = (token)       => ({ type: 'SET_JWT_TOKEN',        value: token });

export const setValue               = (key, value)  => ({ type: "SET_VALUE", value, key });
export const setValues              = (value)       => ({ type: "SET_VALUES", value });
