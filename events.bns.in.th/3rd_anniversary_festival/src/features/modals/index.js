import React from 'react';
import {connect} from 'react-redux';

import ModalMessage from './ModalMessage';
import ModalLoading from './ModalLoading';

const Modals = (props)=> (
    <>
      <ModalLoading />
      <ModalMessage />
    </>
)

const mstp = state => ({...state.Main});
const mdtp = {  };

export default connect( mstp, mdtp )(Modals);
