import React,{useState,useEffect} from 'react';
import styled from 'styled-components';

export default (props) => {
  //type
  // 0 "default"        : "horizontal"
  // 1 "horizontal"     : horizontal>vertical
  // 2 "vertical"       : vertical>horizontal
  // 3 "spiral"         : spiral-right
  // 4 "reverse spiral" : spiral-keft

  let [spriteSheet,setSpriteSheet] = useState(new Image());
  let [aryCount,setAryCount] = useState([]);
  // let [spriteSheet,setSpriteSheet] = useState(new Image());
  useEffect(()=>{
    if(props.spriteSheet !== ""){
      spriteSheet.src = props.spriteSheet;
      spriteSheet.onload = (e)=> {
        let rowMax = Math.floor((e.target.width / props.width)+0.9);
        let columMax =  Math.floor((e.target.height / props.height)+0.9);
      }
    }
  },[props.spriteSheet])
  return (
    <AniStep
      ss={props.spriteSheet||""}
      className={props.className || ""}
      width={props.width || 100}
      height={props.height || 100}
      fps={props.fps || 12}
      onClick={()=>props.onClick? props.onClick():()=>console.log("onclick")}
    />
  )
}

const AniStep = styled.canvas`

  @keyframes ani_loop {
    from {
      background-position: 0% 0px;
    }
    to {
      background-position: 100% 0px;
    }
  }
  background-repeat: no-repeat;
  background-image: url(${props=>props.ss});
  background-position: 0px 0px;
  animation: loani_loopad ${props=>1/props.fps}s steps(2, end) forwards;
  // background-size: ${props=>props.width}px ${props=>props.height}px;ß
`;
