import React,{useEffect,useState} from 'react';
import styled from 'styled-components';

const randomMaxMin = (max,min)=>{
  return Math.floor(Math.random()*(max-min))+min;
}


export default (props) => {
  const [childrenStatus,setChildrenStatus] = useState([]);
  let speedMoveMax = 50;
  let speedMoveMin = 40;

  let speedRotateMax = 30;
  let speedRotateMin = 20;

  let layers = [0.6,0.8,1,1.2];

  useEffect(()=>{
    if(props.children){
      let _list = props.children.map((item,index) => {
        let child = item;
        let rnd = Math.floor(Math.random()*layers.length);
        let rndLeftRight = Math.floor(Math.random()*2) >0;
        let layer = layers[rnd];
        let timeRotate = 360 / randomMaxMin(speedRotateMax,speedRotateMin);
        let timeMove = 2586 / randomMaxMin(speedMoveMax,speedMoveMin);
        let rndX = Math.floor(Math.random()*90)+5;

        return { child, rnd ,layer ,timeRotate ,timeMove, rndLeftRight, rndX}
      });
      setChildrenStatus(_list);
    }
  },[props.children.length]);

  return (
    <SnowingStyle
      zIndex={props.zIndex||0}
      key={"Snowing"}
    >
      {childrenStatus.map((item,index)=>{
        return (
          <Chip
            scale={item.layer}
            timeRotate={item.timeRotate}
            timeMove={item.timeMove}
            key={"sn_"+index}
            lr={item.rndLeftRight}
            rndX={item.rndX}
            delay={index}
          >
            <div className="rotate">
              {item.child}
            </div>
          </Chip>
        )
      })}
    </SnowingStyle>

  )
}
// React.cloneElement(item, {
//   className: `${item.props.className} oong`
// })

const SnowingStyle = styled.div`
  z-index: ${props=>props.zIndex};
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100%;
  height: 100%;
  user-select: none;
  pointer-events: none;
`;

const Chip = styled.div`
  @keyframes down {
    from { top: -2%; }
    to { top: 102%; }
  }
  @keyframes rotate_right {
    from { transform: rotate(0deg); }
    to { transform: rotate(360deg); }
  }
  @keyframes rotate_left {
    from { transform: rotate(0deg); }
    to { transform: rotate(-360deg); }
  }

  position: absolute;
  top:  -10%;
  left: ${props=>props.rndX}%;
  display: inline-block;
  transform: scale(${props=>props.scale});
  animation: down ${props=>props.timeMove}s linear infinite;
  animation-delay: ${props=>props.delay}s;

  .rotate {
    display: inline-block;
    animation: ${props=>props.lr?"rotate_right":"rotate_left"} ${props=>props.timeRotate}s linear infinite;
  }
`;
