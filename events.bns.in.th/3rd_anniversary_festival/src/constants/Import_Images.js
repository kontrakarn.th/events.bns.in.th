const icon 								= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/icon.png';
const ani_cake 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/ani_cake.png';
const background 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/background.jpg';
const banner_char 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/banner_char.png';
const banner_date 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/banner_date.png';
const banner_title 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/banner_title.png';
const btn_question 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/button_question.png';
const btn_confirm 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/btn_confirm.png';
const board_slide 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/board_slide.png';
const btn_inactive 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/btn_inactive.png';
const btn_receive 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/btn_receive.png';
const btn_received 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/btn_received.png';
const popup_1 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/popup_1.png';
const popup_2 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/popup_2.png';
const popup_3 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/popup_3.png';
const popup_4 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/popup_4.png';
const popup_5 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/popup_5.png';
const popup_question 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/popup_question.png';
const modal_bg 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/modal_bg.png';
const slide_1 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/slide_1.png';
const ribbon_sea 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/ribbon/ribbon_sea.png';
const ribbon_red 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/ribbon/ribbon_red.png';
const ribbon_pink 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/ribbon/ribbon_pink.png';
const ribbon_green 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/ribbon/ribbon_green.png';
const ribbon_blue 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/ribbon/ribbon_blue.png';
const score_cake_gray 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/score_cake_gray.png';
const score_cake_color 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/score_cake_color.png';
const score_line 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/score_line.png';
const board_game 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/board_game.png';

const menu_bg 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/f11/menu_bg.jpg';
const bg_permission 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/f11/permission.jpg';
const icon_scroll 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/f11/icon_scroll.png';
const btn_home 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/f11/btn_home.png';
const menu_special_btn 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/menu_special_btn.jpg';
const icon_dropdown 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_anniversary_festival/images/icon_dropdown.png';

//==============================================================================================================

export default {
	// base Layout
	bg_permission,
	menu_bg,
	icon_scroll,
	btn_home,
	menu_special_btn,
	icon_dropdown,

	//Event
	icon,
	ani_cake,
	background,
	banner_char,
	banner_date,
	banner_title,
	btn_question,
	btn_confirm,
	board_slide,
	btn_inactive,
	btn_receive,
	btn_received,
	popup_1,
	popup_2,
	popup_3,
	popup_4,
	popup_5,
	popup_question,
	modal_bg,
	slide_1,
	ribbon_sea,
	ribbon_red,
	ribbon_pink,
	ribbon_green,
	ribbon_blue,
	score_cake_gray,
	score_cake_color,
	score_line,
	board_game,
}
