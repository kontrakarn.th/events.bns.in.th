import React,{useState,useEffect} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import imgList from './../constants/Import_Images';

import Slider from "react-slick";
import F11Layout from './../features/F11Layout';
import {apiPost} from './../constants/Api';
import {setValues} from './../store/redux';
import Snowing from './../features/Snowing';
import AniStep from './../features/AnimationStep';

const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

const HomePage = props => {
  const [itemSlide,setItemSlide] = useState(null);
  const [ribbonList,setRibbonList] = useState([]);
  const itemSlideSettings = {
      dots: false,
      speed: 500,
      dots: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: null,
      prevArrow: null,
      // afterChange: (p)=>setItemPage(p),
      appendDots: dots => (
        <div>
          <ul style={{ margin: "0px" }}> {dots} </ul>
        </div>
      ),
      customPaging: i => (
        <div />
      )
  };

  const apiEventInfo = () => {
    if(props.jwtToken !== "") {
      props.setValues({modal_open: "loading"});
      apiPost(
        "REACT_APP_API_POST_EVENT_INFO",
        props.jwtToken,
        {type: "event_info"},
        // successCallback
        (d)=>{
          props.setValues({
            ...d.data,
            modal_open: "",

          });
        },
        // failCallback=
        (d)=>{
          props.setValues({
            modal_open: "message",
            modal_message: d.message,
          });
        }
      )
    }
  }

  const apiClaimReward = (active,id,n) => {
    if(active){
      props.setValues({modal_open: "loading"});
      apiPost(
        "REACT_APP_API_POST_CLAIM_REWARD",
        props.jwtToken,
        {
          type: "claim_reward",
          id: id
        },
        // successCallback
        (d)=>{
          props.setValues({
            ...d.data,
            modal_open: "message",
            // modal_message: d.message,
            modal_message: "ได้รับ "+props.rewards[n].title+" <br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
          });
        },
        // failCallback=
        (d)=>{
          props.setValues({
            modal_open: "message",
            modal_message: d.message,
          });
        }
      )
    }
  }
  useEffect(()=>{
    let ary = []
    for(let i = 0; i<= 30; i++ ){

      ary.push(imgList.ribbon_sea);
      ary.push(imgList.ribbon_red);
      ary.push(imgList.ribbon_pink);
      ary.push(imgList.ribbon_green);
      ary.push(imgList.ribbon_blue);

    }
    setRibbonList(ary);
  },[]);

  useEffect(()=>{ apiEventInfo() },[props.jwtToken]);

  return (
    <F11Layout showUserName={true} showCharacterName={true} page={""}>
      <HomeStyle className="home" level={1}>
        <Snowing>
          {ribbonList.map((item,index)=>{
            return <img key={"str_"+index} src={item} />
          })}
        </Snowing>
        <section className="home__section home__section--1">
          <img className="home__char" src={imgList.banner_char} alt=""/>
          <AniStep className="home__cake" width={274} height={383} spriteSheet={imgList.ani_cake} fps={60}/>
          <img className="home__title" src={imgList.banner_title} alt=""/>
          <img className="home__date" src={imgList.banner_date} alt=""/>

        </section>
        <section className="home__section home__section--2">
          <div className="home__game">
            <div className="home__tube cake">
              <ScoreCake percent={props.current_percentage} point={props.current_points}>
                <div className="bar" />
                <div className="text" />
              </ScoreCake>
            </div>
            <div className="home__question">
              <div className="popup popup--question" />
            </div>
            <div className="home__rewards" >
              {props.rewards.map((item,index)=>{
                let btnImg = imgList.btn_inactive;
                let active = item.can_claim && !item.claimed;
                if(item.can_claim) btnImg = imgList.btn_receive;
                if(item.claimed) btnImg = imgList.btn_received;
                return (
                  <RewardBtn
                    key={"rw_"+index}
                    active={active}
                    src={btnImg}
                    onClick={()=>apiClaimReward(active,item.id,index)}
                  >
                    <img className={"rewardpopup rewardpopup--"+index} src={imgList["popup_"+(index+1)]} />
                  </RewardBtn>
                )
              })}
            </div>
          </div>
        </section>
        <section className="home__section home__section--3">
          <div className="home__board">
            <div className="home__slide">
              <Slider key="pointslide" {...itemSlideSettings} ref={e=>{if(e)setItemSlide(e)}}>
                <div><img className="home__itemslot" src={imgList.slide_1} /></div>
              </Slider>
            </div>
          </div>
        </section>
      </HomeStyle>
    </F11Layout>
  )
}
const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(HomePage);


const HomeStyle= styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: 2586px;
  background-image: url(${imgList.background});
  background-color: #000000;

  .popup {
    position: absolute;
    opacity: 0;
    pointer-events: none;
    &--question {
      top: 50%;
      left: 50%;
      transform: translate(-5.5%, -100%);
      display: block;
      width: 660px;
      height: 449px;
      background: no-repeat center url(${imgList.popup_question});
    }
  }
  .home {
    &__section {
      position: relative;
      display: block;
      &--1 {
        height: 700px;
      }
      &--2 {
        height: 1150px;
      }
      &--3 {
        height: 736px;
      }
    }
// banner section
  &__char {
    position: absolute;
    top: 95px;
    left: 17px;
  }
  &__cake {
    position: absolute;
    top: 62px;
    left: 417px;
    display: block;
    width: 274px;
    height: 383px;
  }
  &__title {
    position: absolute;
    top: 390px;
    left: 188px;
  }
  &__date {
    position: absolute;
    top: 568px;
    left: 389px;
  }

//game section
    &__game {
      position: relative;
      top: 80px;
      margin: 0px auto;
      width: 875px;
      height: 1054px;
      background: no-repeat center url(${props=>imgList.board_game});

    }
    &__tube {
      position: absolute;
      top: 219px;
      left: 271px;
      display: block;
      width: 333px;
      height: 341px;
    }
    &__question {
      position: absolute;
      top: 470px;
      left: 215px;
      display: block;
      width: 40px;
      height: 40px;
      background: no-repeat center url(${imgList.btn_question});
      &:hover .popup {
        opacity: 1;
        pointer-events: all;
      }
    }
    &__rewards {
      position: absolute;
      left: 50%;
      bottom: 10.7%;
      transform: translate(-49.8%, 0px);
      display: flex;
      justify-content: space-between;
      align-items: center;
      height: 32px;
      width: 685px;
    }

// reward section
    &__board {
      position: relative;
      top: 12px;
      margin: 0px auto;
      display: block;
      width: 874px;
      height: 649px;
      background: no-repeat center url(${imgList.board_slide});
    }
    &__slide {
      top: 104px;
      position: relative;
      display: block;
      width: 714px;
      height: 452px;
      margin: 0px auto;
    }
  }
`;

const RewardBtn = styled.div`
  position: relative;
  display: block;
  width: 80px;
  height: 32px;
  // pointer-events: ${props=>props.active?"all":"none"};
  background: no-repeat top center url(${props=>props.src});
  :hover {
    background-position: bottom center;
  }
  :hover .rewardpopup {
    opacity: 1;
  }
  .rewardpopup {
    user-select: none;
    pointer-events: none;
    position: absolute;
    top: -200%;
    left: 50%;
    display: block;
    widh: 320px;
    height: 341px;
    transform:translate(-50%,-100%);
    opacity: 0;
    &--0 {

    }
    &--1 {

    }
    &--2 {

    }
    &--3 {

    }
    &--4 {

    }
  }
`;

const ScoreCake = styled.div`
  display: block;
  width: 333px;
  height: 341px;
  background: no-repeat bottom center url(${imgList.score_cake_gray});
  color: #f9267d;
  .bar {
    position: absolute;
    bottom: 0px;
    left: 0px;
    width: 333px;
    height: ${props=>props.percent}%;
    background: no-repeat bottom center url(${imgList.score_cake_color});
  }
  .text {
    position: absolute;
    bottom: ${props=>ScoreCakePointBottom(props.percent)}%;
    left: ${props=>ScoreCakePointLeft(props.percent)}%;
    width: 222px;
    height: 71px;
    transform: translate(0, 8px);
    background: no-repeat bottom center url(${imgList.score_line});
    :after {
      content: '${props=>numberWithCommas(props.point)} คะแนน';
      position: absolute;
      top: 16px;
      left: 86px;
    }
  }
`;
const ScoreCakePointBottom = (n)=>{
  return Math.min(90,Math.max(20,n));
  // return Math.min((n/100*70)+20,90);
}
const ScoreCakePointLeft = (n)=>{
  // let m =  Math.min((n/100*70)+20,90);
  let m = Math.min(90,Math.max(20,n));
  if(m > 88) return 71;
  if(m > 73) return 76;
  if(m > 60) return 80;
  if(m > 43) return 87;
  if(m > 19) return 98;
}
