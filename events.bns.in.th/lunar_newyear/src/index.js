import 'core-js/es6/map';
import 'core-js/es6/set';
import 'raf/polyfill';
import React from 'react';

import { hydrate, render }  from 'react-dom';

import ReactDOM from 'react-dom';
// import {render} from 'react-snapshot';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import {combineReducers, createStore} from 'redux';
import {Web} from './routes/Web';
import serviceWorker from './serviceWorker';
import AccountReducer from './reducers/AccountReducer';

import layout from  './features/f11layout/redux';
import loading from  './features/loading/redux';
import permission from  './features/permission/redux';
import selectcharacter from  './features/select_character/redux';

const store = createStore(
	combineReducers({
		layout,
		loading,
		permission,
		AccountReducer,
		selectcharacter,
	})
);
const rootElement = document.getElementById('root');
ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<Web />
		</BrowserRouter>
 	</Provider>,
	document.getElementById('root')
)
