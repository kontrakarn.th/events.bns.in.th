import React from 'react';
import {connect} from 'react-redux';
import ScrollArea from 'react-scrollbar';
import styled, { keyframes } from 'styled-components';
import {
    onAccountLogout
} from '../../actions/AccountActions';

import iconHome from './images/btn_home.png';
import iconScroll from './images/icon_scroll.png';
import bgPermission from './images/permission.jpg';
import { setValue } from './redux';

// import { setValue, submit, submit2 } from "./redux";
const CPN = props => {
    let nameText = "";
    if(props.showUserName && props.username !== ""){
        nameText = props.username;
    }
    if(props.showCharacterName && props.character !== ""){
        nameText = (nameText !== "")? nameText+" : "+props.character : props.character;
    }
    return (
        <Layout background={props.background}>
            {!props.permission &&
                <Permission src={bgPermission}/>
            }
            {props.loading &&
                <Loading src={bgPermission}/>
            }
            <OverAll>
                <TopData>
                    {nameText !== "" &&
                        <SlotText>{nameText}</SlotText>
                    }
                    {props.showLogin &&
                        <SlotBtn onClick={()=>onAccountLogout()}>Logout</SlotBtn>
                    }
                </TopData>
                <ButtomHome href="/" icon={iconHome} />
                {props.showScroll && <IconScroll /> }
            </OverAll>

            <ScrollArea
                speed={0.8}
                className="area"
                contentClassName=""
                // vertical={true}
                horizontal={false}
                onScroll={(value) => {
                    if(value.containerHeight >= value.realHeight) {
                        if(props.showScroll === true) props.setValue("showScroll",false);
                    } else {
                        if(value.topPosition > 10){
                            if(props.showScroll === true)props.setValue("showScroll",false);
                        } else {
                            if(props.showScroll === false) props.setValue("showScroll",true);
                        }
                    }

                }}
                style={{ width: '100%', height:700, opacity:1}}
                verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                // onScroll={this.handleScroll.bind(this)}
            >
                <TextBox style={{top: "570px",right: "350px",fontSize: "20px"}}>{props.date}</TextBox>
                <TextBox style={{top: "570px",right: "125px",fontSize: "20px"}}>{props.type}</TextBox>
                {props.children && props.children}
            </ScrollArea>
        </Layout>
    )
}
// const mapStateToProps = state => state.layout;
const mapStateToProps = state => ({...state.layout});
//     loading: state.layout.loading,
//     permission: state.layout.permission,
//     showScroll: state.layout.showScroll
// })
const mapDispatchToProps = { onAccountLogout, setValue };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);


const Layout = styled.div`
    position: relative;
    display: block;
    width: 1105px;
    height: 700px;
    max-width: 100vw;
    max-height: 100vh;
    overflow: hidden;
    background: no-repeat top center url(${props => props.background});
    font-family: "Kanit-Light",tahoma;
`;
const OverAll = styled.div`
    z-index: 100;
    position: absolute;
    top: 0px;
    left: 0px;
    display: block;
    width: 100%;
    height: 100%;
    pointer-events: none;
`;
const TopData = styled.div`
    /* opacity: 0; */
    position: absolute;
    top: 0px;
    right: 0px;
    display: block;
    margin-top: 17px;
    margin-right: 17px;
`;
const SlotText = styled.div`
    pointer-events: auto;
    display: inline-block;
    padding: 0px 25px;
    margin-left: 17px;
    /* width: fit-content; */
    /* max-width: 170px; */
    text-align: center;
    height: 45px;
    line-height: 45px;
    background-color: rgba(0,0,0,0.7);
    border-radius: 15px;
    color: #FFFFFF;
    font-size: 14px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    /* transition: width 0.3s ease-in-out; */
`;
const SlotBtn = styled.a`
    pointer-events: auto;
    cursor: pointer;
    display: inline-block;
    padding: 0px 10px;
    margin-left: 17px;
    width: fit-content;
    max-width: 170px;
    text-align: center;
    height: 45px;
    line-height: 45px;
    background-color: rgba(0,0,0,0.7);
    border-radius: 15px;
    color: #FFFFFF;
    font-size: 14px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;
const ButtomHome = styled.a`
    position: absolute;
    top: calc(50% - 32px);
    right: 14px;
    display: block;
    width: 63px;
    height: 63px;
    background: no-repeat center url(${props => props.icon});
    user-select: auto;
    pointer-events:  all;
`;

const Permission = styled.div`
    z-index: 6000;
    position: fixed;
    top: 0px;
    left: 0px;
    display: block;
    width: 1105px;
    height: 700px;
    background: no-repeat center url(${props => props.src}) #c5c0ad;
`;
const Loading = styled.div`
    position: absolute;
    top: 0px;
    left: 0px;
    display: block;
    width: 1105px;
    height: 700px;
    background: no-repeat center url(${props => props.src}) #c5c0ad;
`;

const IconScrollAnimation = keyframes`
    0% { transform: translateY(0); }
    100% { transform: translateY(-10px); }
`;
const IconScroll = styled.div`
    position: absolute;
    bottom: 20px;
    left: 50%;
    transform: translate3d( -50%, 0, 0);
    display: block;
    width: 52px;
    height: 71px;
    background: no-repeat center url(${iconScroll});
    animation: ${IconScrollAnimation} 1s infinite alternate;
`;
const TextBox= styled.div`
    z-index: 1;
    position: absolute;
    display: block;
    width: 200px;
    height: 42px;
    border-radius: 25px;
    border: 2px solid #f9b93e;
    color: #f9b93e;
    font-weight: bolder;
    line-height: 45px;
    background-color: #aa2727;
    text-align: center;
`;
