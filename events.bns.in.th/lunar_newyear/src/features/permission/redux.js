
const initialState = {
  status: true,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_PERMISSION_STATUS":
        return {
            ...state,
        }
    default:
      return state;
  }
};

export const setPremission = (key, value) => ({ type: "SET_VALUE", value, key });
