import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import BgPermission from './images/permission.jpg';

const CPN = props => (
    <div
        style={{
            position: "fixed",
            top: "0px",
            left: "0px",
            display: "block",
            width: "1105px",
            height: "700px",
            background: "no-repeat center url("+BgPermission+") #c5c0ad"
        }}
    />
)
const mapStateToProps =(state) => ({
    permissionStatus: state.permission.status
})
const mapDispatchToProps = {};

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
