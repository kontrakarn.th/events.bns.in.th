import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';

const CPN = props => {
    return (
        <Loading active={props.status}>
            <h3>กำลังดำเนินการ...</h3>
        </Loading>
    )
}
const mapStateToProps =(state) => ({...state.loading});
const mapDispatchToProps = {};

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const Loading = styled.div`
    z-index: 5000;
    position: fixed;
    color: #FFFFFF;
    top: 0px;
    left: 0px;
    display: ${props=>props.active? "flex": "none"};
    justify-content: center;
    align-items: center;
    width: 1105px;
    height: 700px;
    background: rgba(0,0,0,0.7);
`;
