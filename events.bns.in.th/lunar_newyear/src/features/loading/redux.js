
const initialState = {
  status: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_LOADING":
        return { ...state, status: action.value }
    default:
        return state;
  }
};

export const setLoading = (value) => ({ type: "SET_LOADING", value });
