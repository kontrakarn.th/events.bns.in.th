
const initialState = {
    // ---------
    // showUserName: true,
    username: "",
    // ---------
    // showCharacterName: true,
    charactername: "",
    // ---------
    showLogin: true,
    loginStatus: false,
    // ---------
    showScroll: true,
    // ---------
    loading: false,
    permission: true,
    status: false,
};
export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return {
          ...state,
          [action.key]: action.value
        };
      } else {
        return state;
      }
    default:
      return state;
  }
};

export const setValue = (key, value) =>{
    return ({
        type: "SET_VALUE",
        value,
        key
    })
};
