import React from 'react';
import 'whatwg-fetch';
import KJUR from 'jsrsasign';
import { connect } from 'react-redux';
import { setValue } from './../../features/f11layout/redux';
import $ from "jquery";

// ========= ========= ========= ========= ========= ========= ========= ========= =========
// BASE API
const createJwt = (data)=>{
    if(data){
        let oHeader = {
          alg: 'HS256',
          typ: 'JWT'
        };
        let sHeader = JSON.stringify(oHeader);
        let sPayload = '{"' + decodeURI(data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}';
        let sJWT = KJUR.jws.JWS.sign("HS256", sHeader, sPayload, process.env.REACT_APP_JWT_SECRET);

        let parts = sJWT.split(".");
        const header_payload = (["G", "a", "r", "e", "n", "a", "T", "H"].join("")) + " " + btoa(parts[0] + "." + parts[2]);
        const body_payload = btoa(parts[1]);
        return {
            headers:{
                'Authorization':header_payload
            },
            body:body_payload
        }
    }else{
        return {
            headers:{},
            body:{}
        };
    }
}
const createAuthorizationHeaders = (token) => {
    // Header
    let headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': (["B", "e", "a", "r", "e", "r"].join("")) + " " + token,
    };
    return headers;
}
const apiPost = (
        api_name,
        token,
        api_data,
        successCallback=()=>{},
        failCallback=()=>{},
        responseCallback=()=>{},
        responsefail=()=>{},
        errorCallback=()=>{}
    ) => {
    fetch(process.env.REACT_APP_API_SERVER_HOST+process.env[api_name], {
        method: 'POST',
        credentials: 'same-origin',
        headers: createAuthorizationHeaders(token),
        body: JSON.stringify(api_data)
        // body: createJwt(JSON.stringify(api_data)).body

    }).then(response => {
        if (response.status == 200) {
            response.json().then(data => {
                if (data.status) {
                    successCallback(data);
                } else {
                    failCallback(data);
                }
                responseCallback(data);
            });
        } else {
            responsefail(response);
        }
    }, error => {
        errorCallback(error);
    });
}
const apiPostAjax = (
        api_name,
        token,
        api_data,
        successCallback=()=>{},
        failCallback=()=>{},
        responseCallback=()=>{},
        responsefail=()=>{},
        errorCallback=()=>{}
    ) => {
    $.ajax({
        url: process.env.REACT_APP_API_SERVER_HOST+process.env.REACT_APP_API_POST_LOBBY_INFO,
        dataType: "json",
        type: "POST",
        beforeSend: (xhr) => {
            xhr.setRequestHeader('Authorization', (["B", "e", "a", "r", "e", "r"].join("")) + " " + token);
        },
        data: {
            "type":"lobby_info"
        },
        success: (data) => {
            if (data.status == true) {
                successCallback(data);
            } else {
                failCallback(data);
            }
            responseCallback(data);
        },
        error: (error) => {
            errorCallback(error);
        }
    });
}

// ========= ========= ========= ========= ========= ========= ========= ========= =========
// CENTER
export const apiGetLobbyInfo = (props)=>{
    let dataSend={
        type : "lobby_info"
    };
    let successCallback = (data)=>{
        if(props.actSuccess)props.actSuccess(data.content);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            if(props.actNoPermission)props.actNoPermission();
        }
        if(data.type=="maintenance") {
            if(props.actNoMaintenance)props.actNoMaintenance();
        }

        if(data.type=="no_char") {
            if(props.actNoChar)props.actNoChar();
        }
    };
    apiPost("REACT_APP_API_POST_LOBBY_INFO",props.jwtToken,dataSend,successCallback,failCallback);
};
export const apiGetCharacter = (props)=>{
    let dataSend={
        type : "get_character"
    };
    let successCallback = (data)=>{
        if(props.actSuccess)props.actSuccess(data.content);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            if(props.actNoPermission)props.actNoPermission();
        }
    };
    apiPost("REACT_APP_API_POST_GET_CHARACTER",props.jwtToken,dataSend,successCallback,failCallback);
};
export const apiAcceptCharacter = (props)=>{
    let dataSend={
        type : "accept_character",
        id: props.id,
    };
    let successCallback = (data)=>{
        if(props.actSuccess)props.actSuccess(data.content);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            if(props.actNoPermission)props.actNoPermission();
        }
        if(props.actFail)props.actFail(data.content);
    };
    apiPost("REACT_APP_API_POST_ACCEPT_CHARACTER",props.jwtToken,dataSend,successCallback,failCallback);
};

// ========= ========= ========= ========= ========= ========= ========= ========= =========
// DAILY
export const apiDailyInfo = (props)=>{
    let successCallback = (data)=>{
        props.actSuccess(data.content);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            props.actNoPermission();
        }
    };
    apiPost("REACT_APP_API_POST_DAILY_INFO",props.jwtToken,{ type : "daily_info" },successCallback,failCallback);
};

// ========= ========= ========= ========= ========= ========= ========= ========= =========
// BUNS
export const apiBunsInfo = props=>{
    let successCallback = (data)=>{
        props.actSuccess(data.content);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            props.actNoPermission();
        }
    };
    apiPost("REACT_APP_API_POST_BUNS_INFO",props.jwtToken,{ type : "buns_info" },successCallback,failCallback);
};
export const apiPlayBunsFortune = props=>{
    let successCallback = (data)=>{
        props.actSuccess(data.content);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            props.actNoPermission();
        }
    };
    apiPost("REACT_APP_API_POST_PLAY_BUNS_FORTUNE",props.jwtToken,{ type : "play_buns_fortune" },successCallback,failCallback);
};

// ========= ========= ========= ========= ========= ========= ========= ========= =========
// ANGPAO
export const apiAngpaoInfo = props=>{
    let successCallback = (data)=>{
        props.actSuccess(data.content);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            props.actNoPermission();
        }
    };
    apiPost("REACT_APP_API_POST_ANGPAO_INFO",props.jwtToken,{ type : "angpao_info" },successCallback,failCallback);
};
export const apiPlayAngpao = props=>{
    let successCallback = (data)=>{
        if(props.actSuccess) props.actSuccess(data.content);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            if(props.actNoPermission) props.actNoPermission();
        } else {
            if(props.actFail) props.actFail(data.message);
        }
    };
    apiPost("REACT_APP_API_POST_PLAY_ANGPAO",props.jwtToken,{ type : "play_angpao" },successCallback,failCallback);
};
export const apiSaveAngpaoResult = props=>{
    let data = {
        play_key: props.play_key,
        score: props.score,
    }
    let successCallback = (data)=>{
        if(props.actSuccess) props.actSuccess(data.content);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            if(props.actNoPermission) props.actNoPermission();
        } else {
            if(props.actFail) props.actFail(data.message);
        }
    };
    apiPost("REACT_APP_API_POST_SAVE_ANGPAO_RESULT",props.jwtToken,{ type : "save_angpao_result", ...data },successCallback,failCallback);
};
export const apiUpdateShopScore = props=>{
    let data = {
        play_key: props.play_key,
    }
    let successCallback = (data)=>{
        props.actSuccess(data.content,data.message);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            if(props.actNoPermission)props.actNoPermission();
        }
    };
    apiPost("REACT_APP_API_POST_UPDATE_SHOP_SCORE",props.jwtToken,{ type : "score_to_shop", ...data },successCallback,failCallback);
};

// ========= ========= ========= ========= ========= ========= ========= ========= =========
// SHOP
export const apiShopInfo = props=>{
    let successCallback = (data)=>{
        props.actSuccess(data.content);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            if(props.actNoPermission)props.actNoPermission();
        }
    };
    apiPost("REACT_APP_API_POST_SHOP_INFO",props.jwtToken,{ type : "shop_info" },successCallback,failCallback);
};
export const apiExchange = props=>{
    let successCallback = (data)=>{
        if(props.actDone) props.actDone(data.message);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            if(props.actNoPermission)props.actNoPermission();
        }
    };
    let responseCallback = (data)=>{
        if(props.actSuccess) props.actSuccess(data.message,data.content);
    };
    apiPost("REACT_APP_API_POST_EXCHANGE",props.jwtToken,{ type : "exchange_reward", package_id: props.package_id },successCallback,failCallback,responseCallback);
};
// ========= ========= ========= ========= ========= ========= ========= ========= =========
// RANK
export const apiRankInfo = props=>{
    let successCallback = (data)=>{
        props.actSuccess(data.content);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            if(props.actNoPermission)props.actNoPermission();
        }
    };
    apiPost("REACT_APP_API_POST_RANK_INFO",props.jwtToken,{ type : "rank_info" },successCallback,failCallback);
};
export const apiRedeemRankReward = props=>{
    let successCallback = (data)=>{
        props.actSuccess(data.content,data.message);
    };
    let failCallback = (data)=>{
        if(data.type=="no_permission") {
            if(props.actNoPermission)props.actNoPermission();
        } else {
            props.actFalse(data.message);
        }
    };
    apiPost("REACT_APP_API_POST_REDEEM_RANK_REWARD",props.jwtToken,{ type : "redeem_rank_reward", package_id: props.package_id },successCallback,failCallback);
};

// ========= ========= ========= ========= ========= ========= ========= ========= =========
