import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import {
    apiGetCharacter,
    apiAcceptCharacter,
} from './../../features/api';

import Modal from './../../components/Modal';
import { setLoading } from './../../features/loading/redux';

import iconDropdown from './images/icon_dropdown.png';

class SelectCharacter extends React.Component {
    actSelectCharacter() {
        let index = document.getElementById("select_charater_form").value;
        let value = this.state.list[index];
        this.setState({
            characterId: value.id,
            characterName: value.char_name
        })
    }
    actConfirmSelected(){
        this.apiAcceptCharacter();
    }
    actCancelSelected(){
        this.actClearCharacterSelected();
    }
    actDone(data_name){
        if(this.props.actDone){
            this.props.actDone(data_name);
        }
    }
    actClearCharacterSelected(){
        this.setState({
            characterId: -1,
            characterName: "",
        });
    }
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiGetCharacter() {
        this.props.setLoading(true);
        apiGetCharacter({
            jwtToken: this.props.jwtToken,
            actSuccess:(data)=>{
                console.log("apiGetCharacter",data);
                this.setState({
                    list: data,
                    characterId: -1,
                    characterName: "",
                })
                this.props.setLoading(false);
            }
        });
    };
    apiAcceptCharacter(){
        this.props.setLoading(true);
        apiAcceptCharacter({
            jwtToken: this.props.jwtToken,
            id:  this.state.characterId,
            actSuccess:(data)=>{
                let {character,username} = data;
                this.actDone({character ,username});
                setTimeout(()=>this.props.setLoading(false),1000);
            },
            actFail:()=>{
                this.actClearCharacterSelected();
            }
        });
    }
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props){
        super(props);
        this.state = {
            open: false,
            characterSelected: -1,
            characterName: "",
            characterId: -1,
            list: [],
        }
    }
    componentWillMount(){
        if(this.props.open){
            this.apiGetCharacter();
            this.setState({open: this.props.open})
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(prevProps.open !== this.props.open) {
            if(this.state.characterName === "" && this.state.characterId < 0) this.apiGetCharacter();
            this.setState({open: this.props.open})
        }
    }
    render() {
        let { open, characterName, characterId, list } = this.state;
        if(!open){
            return false;
        }
        if(characterId < 0) {
            return (
                <Modal
                    open={open}
                    title={"เลือกตัวละคร"}
                    confirmMode={false}
                    actConfirm={()=>{}}
                    actClose={this.actSelectCharacter.bind(this)}
                >
                    <SelectCharacterForm>
                        <label >
                            <select id="select_charater_form" className="fromselect__list">
                                {list.map((item,index)=>{
                                    return (
                                        <option key={index} value={index}>{item.char_name}</option>
                                    )
                                })}
                            </select>
                            <img src={iconDropdown} alt="" />
                        </label>
                    </SelectCharacterForm>
                </Modal>
            )
        } else {
            return (
                <Modal
                    open={open}
                    title={"ยืนยัน"}
                    confirmMode={true}
                    actConfirm={this.actConfirmSelected.bind(this)}
                    actClose={this.actCancelSelected.bind(this)}
                >
                    <div>ท่านเลือก <span>{characterName}</span> ?</div>
                </Modal>
            )
        }
    }
}

const mapStateToProps = (state) => ({
    jwtToken: state.AccountReducer.jwtToken
})

const mapDispatchToProps = {setLoading}
export default connect(mapStateToProps,mapDispatchToProps)(SelectCharacter)

const SelectCharacterForm = styled.form`
    margin: 0px auto;
    label {
        position: relative;
        color:#fff;
        background-color: #626262;
        width: 240px;
        height: 26px;
        line-height: 26px;
        display: block;
        margin: 10px auto;
        padding: 5px;
        border-radius: 10px;
        overflow: hidden;
        &:focus {
            outline: 0 none;
        }
    }
    select {
        font-size: 20px;
        border: 0px;
        background: transparent;
        vertical-align: middle;
        width: 110%;
        left: 0;
        position: absolute;
        z-index: 1;
        &:focus {
            outline: 0 none;
        }
    }
    img {
        position: absolute;
        display: block;
        top: 50%;
        width: 24px;
        height: 21px;
        right: 0px;
        transform: translate( -50% , -50% );
        user-select: none;
    }
`;
