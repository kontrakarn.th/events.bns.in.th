
const initialState = {
    status: false,
    select: false,
    list: [],
};

export default (state = initialState, action) => {
    switch (action.type) {
        case "SET_VALUE":
            return { ...action.value, ...state, };
        default:
            return state;
    }
};

export const setSelectCharacter = (value) => ({ type: "SET_VALUE", value });
