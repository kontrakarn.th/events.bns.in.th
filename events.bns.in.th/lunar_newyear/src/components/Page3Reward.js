import React from "react";
import styled, { keyframes } from 'styled-components';

import AmountBar from './../components/AmountBar';
import {BtnHome,BtnPlay} from './../components/Buttons';
import Modal from './../components/Modal';
import AngpaoGame from './AngpaoGame';

import bgTop from './../static/images/lunar_newyear/page_2_buns_top_background.png';
import bgBot from './../static/images/lunar_newyear/page_2_buns_bottom_background.png';
import iconBuns from './../static/images/lunar_newyear/icon_buns.png';
import iconCoin from './../static/images/lunar_newyear/icon_coin.png';

export default class extends React.Component {
    actClickToPlay(canClick){
        if(this.state.play === false && canClick) this.setState({openModalConfirm:true});
    }
    actPlayGame(){
        if(this.state.play === false && this.state.lock_click === false) {
            this.setState({lock_click: true});
            this.props.apiPlayAngpao({
                actSuccess: (data)=>{
                    this.setState({
                        openModalConfirm: false,
                        play: true,
                        play_key: data.play_key,
                    })
                    this.props.setMainState({
                        points_summary: data.points_summary
                     });
                },
                actFail: (msg)=>{
                    this.setState({
                        openModalConfirm: false,
                        openModalMessage: true,
                        modalMessage: msg,
                        play: false,
                        lock_click: false,
                    })
                }
            });
        }
    }
    actEndGame(){
        this.props.apiSaveAngpaoResult({
            play_key: this.state.play_key,
            score: this.state.gold,
            actSuccess: (data)=>{
                this.props.setMainState({points_summary:data.points_summary });
                if(data.gold_shop_updated) {
                    this.setState({
                        openModalMessage: true,
                        modalMessage: "ยินดีด้วย!<br/><br/>ท่านได้รับเหรียญทอง<br/><span>จำนวน "+data.current_score+" เหรียญ</span>",
                        end: true,
                    })
                } else {
                    this.setState({
                        openModalUpdate: true,
                        modalMessage: "ยินดีด้วย!<br/><br/>ท่านได้รับเหรียญทองสูงสุด<br/><span>จำนวน "+data.current_score+" เหรียญ</span><div>ท่านต้องการที่<br/>จะบันทึกเข้าร้านค้าหรือไม่?</div>",
                        end: true,
                    })
                }

            },
            actFail: (msg)=>{
                // this.props.setMainState({points_summary:content.points_summary });
                this.setState({
                    openModalMessage: true,
                    modalMessage: msg,
                    end: true,
                })
            }
        });
    }
    actUpdateGold(){
        this.setState({openModalUpdate: false});
        if(this.props.apiUpdateShopScore) {
            this.props.apiUpdateShopScore({
                play_key:this.state.play_key,
                actSuccess: (data,msg)=>{
                    this.props.setMainState({points_summary:data.points_summary });
                    this.setState({
                        openModalMessage: true,
                        modalMessage: msg
                    })
                }
            })};
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props){
        super(props);
        this.state = {
            openModalConfirm: false,
            openModalMessage: false,
            openModalUpdate: false,
            modalMessage: "",
            gold: 0,
            play: false,
            end: false,
            time: 15,
            play_key: "",
            lock_click: false
        }
    }
    render() {
        let remain_red_gamo = this.props.pointsSummary && this.props.pointsSummary.remain_red_gamo > 0;
        let { gold, play, end, time, lock_click, openModalConfirm, openModalMessage, openModalUpdate} = this.state;

        let checkForPlay = !play && !lock_click && remain_red_gamo > 0 && !openModalConfirm && !openModalMessage && !openModalUpdate;
        return (
            <div className="page page--page3reward">
                <div className="page__section page__section--head" />
                <div className="page__section">
                    <div className="page__content">
                        <div className="page__header">
                            3 | ยิงอั่งเปาเพิ่มความสุข
                            <div className="page__btnhome">
                                <BtnHome onClick={()=>this.props.actChangePage("main")} />
                            </div>
                        </div>
                        <br/>
                        <ul className="page__rule">
                            <li>ระยะเวลายิงอั่งเปา เพิ่มความสุข 8 - 25 กุมภาพันธ์ 2562 (05:59)</li>
                            <li>ใช้สิทธิ์เมื่อตอบถูกจากการเล่นกิจกรรม เสี่ยงทายซาลาเปา</li>
                            <li>วิธีการเล่น</li>
                            <ul>
                            	<li style={{"list-style-type": "none"}}>- ระยะเวลา 15 วินาที ต่อ 1 เกม</li>
                            	<li style={{"list-style-type": "none"}}>- อั่งเปาร่วงจากด้านบนของหน้าจอกิจกรรม</li>
                            	<li style={{"list-style-type": "none"}}>- กดอั่งเปาที่ร่วงจากด้านบนของหน้าจอกิจกรรมมากสุดจนหมดเวลา</li>
                            	<li style={{"list-style-type": "none"}}>- ไม่จำกัดคะแนนต่อรอบ</li>
                            	<li style={{"list-style-type": "none"}}>- 1 อั่งเปา = 1 คะแนน (เหรียญทอง) เพื่อนำไปแลกของใน ร้านโชห่วย เร่เข้ามา</li>
                            </ul>
                            <li>เมื่อตอบถูกจะได้สิทธิ์ (แบงค์กาโม่แดง) ในการเล่น เกมยิงอั่งเปา เพิ่มความสุข 1 สิทธิ์</li>
                            <li>เมื่อตอบผิดจะได้รับคะแนน (เหรียญทอง) เพื่อนำไปใช้แลกของ ในร้านโชห่วย เร่เข้ามา</li>
                            <li style={{"list-style-type": "none"}}>(จำกัด 25 คะแนน ต่อวัน)</li>
                        </ul>
                    </div>
                </div>
                <br />
                <div className="page__section">
                    <AmountBar amounts={this.props.pointsSummary}/>
                </div>
                <br />
                <div className="page__section page__section--pagereward">
                    <div className="page__content">
                        <SlotTime>เวลาที่เหลือ {time} วินาที</SlotTime>
                        <SlotCoin>เหรียญทองรอบนี้ <span>{gold}</span><img src={iconCoin} alt="" /></SlotCoin>
                        <GameZone>
                            {play === false &&
                                <OverGame>
                                    <BtnPlay inactive={!checkForPlay} onClick={()=>this.setState({openModalConfirm: checkForPlay})}/>
                                    {/*<BtnPlay inactive={!checkForPlay} onClick={()=>this.actClickToPlay(checkForPlay)}/>*/}

                                </OverGame>
                            }
                            <AngpaoGame
                                play={play}
                                end={end}
                                actUpdate={(val)=>this.setState({...val})}
                                actEndGame={()=>this.actEndGame()}
                            />
                        </GameZone>
                    </div>
                </div>
                {/* ========= ========= ========= ========= ========= ========= ========= ========= ========= */}
                {/* Modal */}
                <Modal
                    open={this.state.openModalConfirm}
                    title={"แจ้งเตือน"}
                    confirmMode={true}
                    actConfirm={this.actPlayGame.bind(this)}
                    actClose={()=>this.setState({openModalConfirm: false})}
                >
                    <ModalContent>
                        <div>ยืนยันการแลก</div><br/>
                        <div>ใช้แบงค์กาโม่แดง 1 แบงค์เพื่อเล่น 1 ครั้ง</div>
                    </ModalContent>
                </Modal>
                <Modal
                    open={this.state.openModalUpdate}
                    title={"แจ้งเตือน"}
                    confirmMode={true}
                    actConfirm={this.actUpdateGold.bind(this)}
                    actClose={()=>this.setState({
                        openModalUpdate: false,
                        lock_click: false
                    })}
                >
                    <ModalContent>
                        <div dangerouslySetInnerHTML={{
                            __html: this.state.modalMessage
                        }}/>
                    </ModalContent>
                </Modal>
                <Modal
                    open={this.state.openModalMessage}
                    title={"แจ้งเตือน"}
                    confirmMode={false}
                    actConfirm={()=>{}}
                    actClose={()=>this.setState({
                        openModalMessage: false,
                        play: false,
                        end: false,
                        lock_click: false
                    })}
                >
                    <ModalContent>
                        <div dangerouslySetInnerHTML={{
                            __html: this.state.modalMessage
                        }}/>
                    </ModalContent>
                </Modal>

                {/* ========= ========= ========= ========= ========= ========= ========= ========= ========= */}
            </div>
        )
    }
}
const SlotTime = styled.div`
    position: absolute;
    top: 60px;
    left: 160px;
    color: #af2c2c;
    font-size: 24px;
    font-weight: bolder;
`;
const SlotCoin = styled.div`
    position: absolute;
    top: 40px;
    right: 60px;
    display: block;
    width: 275px;
    height: 45px;
    color: #FFFFFF;
    font-size: 20px;
    line-height: 45px;
    border-radius: 15px;
    border: 2px solid #530c0c;
    background-color: #af2c2c;
    text-align: center;
    img {
        vertical-align: top;
        display: inline-block;
        height: 45px;
    }
    span {
        font-size: 1.2em;
        font-weight: bolder;
        margin: 0px 10px;
    }
`;

const ModalContent = styled.div`
    position: relative;
    display: block;
    width: 80%;
    min-height: 150px;
    margin: 0px auto;
    padding: 10px;
    border-radius: 20px;
    background-color: #626262;
    text-align: center;
    span {
        display:block;
        color: #fdba2c;
        font-size: 1.3em;
    }
    div>div {
        color: #ff0000;
    }
`;

const GameZone = styled.div`
    position: relative;
    /* margin: 70px 40px 0px auto; */
    margin: 70px auto 0px 140px;
    display: block;
    width: 730px;
    height: 510px;
    overflow: hidden;
    border-radius: 40px;
    /*
    background-color: #ffedd4;
    border: 2px solid #c5b59f; */
`;
const OverGame = styled.div`
    position: absolute;
    top: 0px;
    left: 0px;
    display: flex;
    width: 100%;
    height: 100%;
    justify-content: center;
    align-items: center;
    z-index: 1;
    background-color: rgba(0,0,0,0.7);
`;
