import React from 'react';
import styled from 'styled-components';
import bgBtn from './../static/images/lunar_newyear/btn_cicle_background.png';

const Btn = styled.a`
    cursor: ${props=>props.inactive? "no-drop":"pointer"} ;
    pointer-events: auto;
    position: relative;
    display: block;
    width: 92px;
    height: 92px;
    /* background: no-repeat top center url(${bgBtn}) / 100% 200%; */
    background: no-repeat top center / 100% 200%;
    filter: grayscale(${props=>props.inactive? "1":"0"});
    &:hover {
        background-position: ${props=>props.inactive? "top center" : "bottom center" };
        /* background-position: bottom center; */
    }
`;
const TextHome = styled.span`
    color: #f9b93e;
    font-size: 24px;
    display: block;
    line-height: 1.2em;
    text-align: center;
    padding-top: 20%;
    pointer-events: none;
    user-select: none;
`;
const TextPlay = styled.span`
    color: #f9b93e;
    font-size: 20px;
    display: block;
    line-height: 1.2em;
    text-align: center;
    padding-top: 20%;
    pointer-events: none;
    user-select: none;
`;

const BtnColor = styled.a`
    cursor: position;
    pointer-events: auto;
    user-select: none;
    display: block;
    width: 140px;
    height: 35px;
    border-radius: 18px;
    line-height: 35px;
    margin-top: 10px;
    text-align: center;
    color: #FFFFFF;
    background-color: #893229;
    &.inactive {
        cursor: no-drop;
        background-color: #4d3e3d !important;
    }
    &:hover {
        background-color: #6b1c14;
    }
`;

export const BtnHome = props => (
    <Btn className="bgBtn" onClick={()=>props.onClick()}>
        <TextPlay>กลับ<br />หน้าหลัก</TextPlay>
    </Btn>
)
export const BtnPlay = props => (
    <Btn className="bgBtn" inactive={props.inactive} onClick={()=>props.onClick()}>
        <TextHome>เริ่ม<br />เล่น</TextHome>
    </Btn>
)

export const BtnText = props => (
    <BtnColor onClick={()=>props.onClick()} className={props.status ? "":"inactive"}>
        {props.text}
    </BtnColor>
)
