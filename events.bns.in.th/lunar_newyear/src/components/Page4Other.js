import React from "react";
import styled from 'styled-components';

import AmountBar from './../components/AmountBar';
import {BtnHome,BtnText} from './../components/Buttons';
import Modal from './../components/Modal';

import imgTagHot from './../static/images/lunar_newyear/item_tag_hot.png';
import imgItem1 from './../static/images/lunar_newyear/item_1.png';
import imgItem2 from './../static/images/lunar_newyear/item_2.png';
import imgItem3 from './../static/images/lunar_newyear/item_3.png';
import imgItem4 from './../static/images/lunar_newyear/item_4.png';
import imgItem5 from './../static/images/lunar_newyear/item_5.png';
import imgItem6 from './../static/images/lunar_newyear/item_6.png';
import imgItem7 from './../static/images/lunar_newyear/item_7.png';
import imgItem8 from './../static/images/lunar_newyear/item_8.png';
import imgItem9 from './../static/images/lunar_newyear/item_9.png';
import imgItem10 from './../static/images/lunar_newyear/item_10.png';
import imgItem11 from './../static/images/lunar_newyear/item_11.png';
import imgItemSlot from './../static/images/lunar_newyear/item_slot_bg.png';
import imgItemOver from './../static/images/lunar_newyear/item_slot_over.png';

const itemList = [
    {image:imgItem1,  hot:true,  limit:true, count:500, title:"ชุดเช็ท<br/>ความหวังใหม่" },
    {image:imgItem2,  hot:true,  limit:true, count:200, title:"ชุดตะวันฉาย<br/>ประกายแดง" },
    {image:imgItem3,  hot:true,  limit:true, count:200, title:"กล่องอาวุธลวงตา<br/>ราชันย์" },
    {image:imgItem4,  hot:true,  limit:true, count:50, title:"เกล็ดสีคราม<br/>(1)" },
    {image:imgItem5,  hot:false, limit:false, count:25, title:"ประกายฉลามดำ<br/>(1)", },
    {image:imgItem6,  hot:false, limit:false, count:25, title:"ลูกแก้วเบลูก้า<br/>(1)", },
    {image:imgItem7,  hot:false, limit:false, count:25, title:"หินล้ำค่าหายาก<br/>(20)", },
    {image:imgItem8,  hot:false, limit:false, count:25, title:"คริสตัลหินจันทรา<br/>(25)", },
    {image:imgItem9,  hot:false, limit:false, count:15, title:"คริสตัลหินโซล<br/>(100)", },
    {image:imgItem10, hot:false, limit:false, count:10, title:"สัญลักษณ์ฮงมุน<br/>(100)", },
    {image:imgItem11, hot:false, limit:false, count:1, title:"ยาโชคชะตา<br/>(1)", },
]

export default class extends React.Component {
    actExchange(){
        if(!this.state.lock_click) {
            this.props.setLoading(true);
            this.setState({lock_click: true})
            this.props.actExchange({
                package_id: this.state.package_id,
                actSuccess: (msg,content)=> {
                    this.setState({
                        openModalMessage: true,
                        openModalConfirm: false,
                        lock_click: false,
                        modalMessage: msg,
                        // pointsSummary: content.points_summary,
                    });
                    this.props.setLoading(false);
                    this.props.setMainState({
                        points_summary:content.points_summary,
                        reward_list: content.reward_list
                    });
                },
                actNoPermission: ()=>{},
            });
        }
    }
    //========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props){
        super(props);
        this.state = {
            openModalConfirm: false,
            openModalMessage: false,
            modalMessage: "",
            package_id: 0,
            package_title: "",
            lock_click: false,
            // pointsSummary: {},
        }
    }

    componentDidUpdate(prevProps, prevState){
        // if(prevState.pointsSummary !== this.state.pointsSummary) {
        //     this.setState({
        //         pointsSummary: this.state.pointsSummary
        //     })
        // }
        // if(prevProps.pointsSummary != this.props.pointsSummary){
        //     this.setState({
        //         pointsSummary: this.props.pointsSummary
        //     })
        // }
    }

    render() {
        return (
            <div className="page page--page4other">
                <div className="page__section page__section--head" />
                <div className="page__section">
                    <div className="page__content page__content--page1detail">
                        <div className="page__header">
                            4 | ร้านโชห่วย เร่เข้ามา
                            <div className="page__btnhome">
                                <BtnHome onClick={()=>this.props.actChangePage("main")} />
                            </div>
                        </div>
                        <br/>
                        <ul className="page__rule">
                            <li>ระยะเวลาการแลกไอเทมในร้านโซห่วย เร่เข้ามา 8 กุมภาพันธ์ 2562 - 2 มีนาคม 2562 (23:59)</li>
                            <li>ใช้เหรียญทองในการแลก หาได้จากการ 2 ส่วนคือ</li>
                            <ul>
                            	<li style={{"list-style-type": "none"}}>- ตอบผิดจากเกม เสี่ยงทายซาลาเปา</li>
                            	<li style={{"list-style-type": "none"}}>- สะสมจากเกม ยิงอั่งเปา เพิ่มความสุข</li>
                            </ul>
                        </ul>
                    </div>
                </div>
                <br /><br />
                <div className="page__section">
                    <AmountBar amounts={this.props.pointsSummary}/>
                </div>
                <br /><br /><br />
                <div className="page__section page__section--pageother">
                    <div className="page__content">
                        {this.props.reward_list.map((item,index)=>{
                            let text = "";
                             if(item.exchange_limit != 9999){
                                 if(item.can_exchange) {
                                     if(item.exchange_count >= item.exchange_limit){
                                         text = "แลกครบแล้ว "+item.exchange_count+"/"+item.exchange_limit;
                                     } else {
                                         text = "แลกไอเทม "+item.exchange_count+"/"+item.exchange_limit;
                                     }

                                 } else {
                                     text = "ไม่ตรงเงื่อนไข "+item.exchange_count+"/"+item.exchange_limit;
                                 }
                            } else {
                                if(item.can_exchange) {
                                    text = "แลกไอเทม";
                                } else {
                                    text = "ไม่ตรงเงื่อนไข";
                                }
                            }
                            return (
                                <ItemSlot key={"item_slot_"+index}>
                                    <ItemCoin className={item.hot&&"hot"}>
                                        <img src={itemList[index].image} />
                                        <span dangerouslySetInnerHTML={{ __html: itemList[index].title }} />
                                        <ItemCost>{item.require_points}</ItemCost>
                                    </ItemCoin>
                                    <BtnText
                                        onClick={()=>{
                                            if(item.can_exchange && item.exchange_count < item.exchange_limit)
                                                this.setState({
                                                    openModalConfirm: item.can_exchange,
                                                    package_id: item.id,
                                                    package_title: item.title
                                                })
                                        }}
                                        status={item.can_exchange && (item.exchange_count < item.exchange_limit)}
                                        text={ text }
                                    />
                                </ItemSlot>
                            )
                        })}
                    </div>
                </div>
                {/* ========= ========= ========= ========= ========= ========= ========= ========= ========= */}
                {/* Modal */}
                <Modal
                    open={this.state.openModalConfirm}
                    title={"แจ้งเตือน"}
                    confirmMode={true}
                    actConfirm={this.actExchange.bind(this)}
                    actClose={()=>this.setState({openModalConfirm: false})}
                >
                    <ModailContent>
                        <div>ยืนยันการแลก</div><br/>
                        {/*<div>ท่านต้องการแลกของรางวัล <span>Rank {this.props.my_rank} ?</span></div>*/}
                        <div>{this.state.package_title} ?</div>
                    </ModailContent>
                </Modal>
                <Modal
                    open={this.state.openModalMessage}
                    title={"แจ้งเตือน"}
                    confirmMode={false}
                    actConfirm={()=>{}}
                    actClose={()=>this.setState({openModalMessage: false})}
                >
                    <ModailContent>
                        <div dangerouslySetInnerHTML={{
                            __html: this.state.modalMessage
                        }}/>
                    </ModailContent>
                </Modal>
                {/* ========= ========= ========= ========= ========= ========= ========= ========= ========= */}
            </div>
        )
    }
}

const ItemSlot = styled.div`
    position: relative;
    vertical-align: top;
    display: inline-flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 25%;
    height: 240px;
`;

const ItemCoin = styled.div`
    position: relative;
    display: block;
    width: 178px;
    height: 178px;
    color: #FFFFFF;
    font-size: 16px;
    text-align: center;
    background: no-repeat top left url(${imgItemSlot});
    &.hot:after {
        content: "";
        position: absolute;
        top: -5px;
        left: -4px;
        display: block;
        width: 85px;
        height: 87px;
        background: no-repeat top left url(${imgTagHot});
    }
    img {
        position: absolute;
        bottom: 70px;
        left: 50%;
        transform: translate3d(-50%,0,0);
    }
    span {
        position: absolute;
        display: block;
        bottom: 30px;
        width: 100%;
        font-size: 14px;
    }
`;
const ItemCost = styled.div`
    position: absolute;
    right: -15px;
    bottom: 0px;
    display:block;
    width: 63px;
    height: 63px;
    background: no-repeat top left url(${imgItemOver});
    color: #c38f3d;
    font-size: 24px;
    line-height: 63px;
    text-align: center;
`;

const ModailContent = styled.div`
    position: relative;
    display: block;
    width: 80%;
    min-height: 150px;
    margin: 0px auto;
    padding: 10px;
    border-radius: 20px;
    background-color: #626262;
    text-align: center;
    span {
        display:block;

    }
`;
