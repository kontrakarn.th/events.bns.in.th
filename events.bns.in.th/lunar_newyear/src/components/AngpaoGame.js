import React from 'react';
import styled, { keyframes } from 'styled-components';

import iconCount from './../static/images/lunar_newyear/count_down.png';
import iconAngpao from './../static/images/lunar_newyear/angpao.jpg';


// ========= ========= ========= ========= ========= ========= ========= ========= =========
const angpaoW = 57;
const angpaoH = 82;
const canvasW = 730;
const canvasH = 510;

var gameTime = 15;
var fps = 30;
var unitPreSec = 4;
var maxAngpao = 9999;
var angpaos = [];

var canClickPreSec = 10;
var mouseDown = false;
var point = [ -500, -500];
const imgAngpao = new Image(angpaoW,angpaoH);
imgAngpao.src = iconAngpao;

// ========= ========= ========= ========= ========= ========= ========= ========= =========

export default class extends React.Component {
// actUpdate
// actEndGame
    actOutClick(){
        if(this.props.onclick) this.props.onClick();
    }
    actClickAngpao(e){
        if(this.state.play && mouseDown === false){
            mouseDown = true;
            point = [e.layerX,e.layerY];
            // for(let i  = 0 ; i < angpaos.length ; i++ ){
            //     let item = angpaos[i]
            //     if(!item.hit){
            //         let side = Math.max(angpaoH,angpaoW)
            //         let nx = e.layerX - item.x + (side*0.5);
            //         let ny = e.layerY - item.y + (side*0.5);
            //         if(nx >= 0 && nx <= side && ny >=0 && ny <= side) {
            //             // angpaoSelected = index;
            //             item.hit = true;
            //             this.setState({score: this.state.score + 1});
            //
            //             break;
            //         }
            //     }
            // }
            setTimeout(()=>{
                mouseDown = false;
                console.log("can click next time");
            }
            ,1000 / canClickPreSec);
        }
    }
    // actMouseUp(){
    //     mouseDown = false
    //
    //     // console.log(angpaos);
    // }
    actPlay(){
        angpaos = [];
        this.setState({
            play: true,
            score: 0,
            counter: 0,
        })
        if(this.clock !== null) {
            clearInterval(this.clock);
            this.clock = null;
        };
        setTimeout(()=>{
            this.clock = setInterval(()=>this.actPreFrame(), 1000 / fps );
        }, 2000);
        // setTimeout( this.actEnd.bind(this),gameTime * 1000 )
    }
    actEnd(){
        if(this.clock !== null) clearInterval(this.clock);
        if(this.props.actEndGame) this.props.actEndGame();
    }

    actPreFrame(){
        let {counter,play} = this.state;
        this.clearCanvas();
        //check angpao
        if(counter % Math.floor(fps / unitPreSec) == 0 && angpaos.length < maxAngpao) {
            let x = angpaoW + (Math.random()*(canvasW - angpaoW - angpaoW));
            let y = -angpaoH;
            let r =  Math.floor(Math.random()*360);

            let vr = (2 - Math.floor(Math.random()*10) / 10);
            let vx = 0;
            let vy = 5 + Math.floor(Math.random()*3);
            if(this.state.score > 25 ){
                vy *= 2;
            } else if(this.state.score > 30 ){
                vy *= 6;
            } else  if(this.state.score > 45 ){
                vy *= 12;
            }
            let opacity = 1;
            let hit = false;

            let setAngpao = {x, y, r, vx, vy, vr, hit, opacity}

            angpaos.push(setAngpao);
        }
        let nx = 0;
        let ny = 0;
        // let item;
        let side = Math.max(angpaoH,angpaoW)
        // for(let i = angpaos.length - 1 ; i >= 0; i--) {
        //     this.fncDraw(imgAngpao,angpaos[i].x,angpaos[i].y,angpaos[i].r);
        //     angpaos[i].x += angpaos[i].vx;
        //     angpaos[i].y += angpaos[i].vy;
        //     angpaos[i].r += angpaos[i].vr;
        // }
        for(let i = 0; i < angpaos.length; i++) {
            let item = angpaos[i];
            if(!item.hit){
                // console.log(item);
                if(mouseDown === true) {
                    nx = point[0] - item.x + (side*0.5);
                    ny = point[1] - item.y + (side*0.5);
                    // console.log("check before",i,point[0],item.x,side,(nx >= 0 && nx <= side && ny >=0 && ny <= side));
                    if(nx >= 0 && nx <= side && ny >=0 && ny <= side) {
                        mouseDown = false;
                        item.hit = true;
                        this.setState({score: this.state.score + 1});
                    }
                }

                this.fncDraw(imgAngpao,item.x,item.y,item.r);
                item.x += item.vx;
                item.y += item.vy;
                item.r += item.vr;

                if(mouseDown === true) {
                    // console.log("check after",i);
                    item = angpaos[i];
                    nx = point[0] - item.x + (side*0.5);
                    ny = point[1] - item.y + (side*0.5);
                    if(nx >= 0 && nx <= side && ny >=0 && ny <= side) {
                        mouseDown = false;
                        item.hit = true;
                        this.setState({score: this.state.score + 1});
                    }
                }
            }
        }
        mouseDown = false;
        if(play){
            counter = counter+1;
            if(counter >= gameTime * fps){
                this.clearCanvas();
                play = false;
            }
            this.setState({counter,play});
        }

        if(this.props.actUpdate) this.props.actUpdate({
            gold: this.state.score,
            time: gameTime - Math.floor(this.state.counter / fps),
            play
        });
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    initialCanvas(e){
        if(e && this.cv === null){
            this.cv = e ;
            this.ctx = this.cv.getContext('2d');
            // this.ctx.drawImage(imgAngpao,0,0);
            this.cv.addEventListener("mousedown",this.actClickAngpao.bind(this));
            // this.cv.addEventListener("mouseup",this.actMouseUp.bind(this));
        }
    }
    clearCanvas(){
        if(this.ctx !== null) {
            this.ctx.clearRect(0, 0, canvasW, canvasH);
        }
    }
    fncDraw(img,x,y,r){
        if(this.ctx !== null) {
            this.ctx.translate(x, y);
            this.ctx.rotate(r * Math.PI / 180);
            this.ctx.drawImage(img,-(angpaoW*0.5),-(angpaoH*0.5));
            this.ctx.rotate(-r * Math.PI / 180);
            this.ctx.translate(-x, -y);
        }
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props){
        super(props);
        this.state = {
            play: false,
            end: false,
            score: 0,
            counter: 0,
        }
        this.clock = null;
        this.cv = null;
        this.ctx = null;
    }
    componentWillMount(){

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.play !== prevProps.play) {
            if(this.props.play){
                this.actPlay();
            } else {
                this.actEnd();
            }
        }
    }
    render() {
        let {play} = this.state;
        return (
            <GameZone>
                {play &&
                    [
                        <CountDown num={1} delay={2.1} />,
                        <CountDown num={2} delay={1.4} />,
                        <CountDown num={3} delay={0.7} />,
                    ]
                }
                <canvas
                    ref={(e)=>this.initialCanvas(e)}
                    width={canvasW}
                    height={canvasH}
                />
            </GameZone>
        )
    }
}


const animationZoomout = keyframes`
  from {
      transform: translate3d(-50%, -50%, 0) scale(1);
      opacity: 1;
  }
  to {
      transform: translate3d(-50%, -50%, 0) scale(2);
      opacity: 0;
  }
`;

const GameZone = styled.div`
    position: relative;
    /* margin: 70px 40px 0px auto; */
    display: block;
    width: 730px;
    height: 510px;
    border-radius: 40px;
    overflow: hidden;
    background-color: #ffedd4;
    border: 2px solid #c5b59f;
    user-select: none;
`;
const CountDown = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    display: block;
    transform: translate3d(-50%, -50%, 0);
    width: 92px;
    height: 92px;
    animation: ${animationZoomout} 0.3s ${props=>props.delay}s  ease-out alternate forwards;
    background: no-repeat ${props=>{
        if(props.num == 1) return "top center";
        if(props.num == 2) return "center";
        if(props.num == 3) return "bottom center";
    }} url(${iconCount});
`;
