import React from 'react';
import styled from 'styled-components';

import bgTop from './../static/images/lunar_newyear/content_top_background.jpg';
import bgMid from './../static/images/lunar_newyear/content_middle_background.jpg';
import bgBot from './../static/images/lunar_newyear/content_bottom_background.jpg';

const Board = styled.div`
    position: relative;
    width: 862px;
    padding: 45px;
    background: url(${bgTop}) top center no-repeat, url(${bgBot}) bottom center no-repeat, url(${bgMid}) center;
`;
export default Board;
