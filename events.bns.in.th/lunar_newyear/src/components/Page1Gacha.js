import React from "react";
import styled from 'styled-components';

import AmountBar from './../components/AmountBar';
import {BtnHome} from './../components/Buttons';
import iconBank from './../static/images/lunar_newyear/icon_blue_bank.png';
import imgBank1 from './../static/images/lunar_newyear/page1_bank_1.png';
import imgBank2 from './../static/images/lunar_newyear/page1_bank_2.png';
import imgBank3 from './../static/images/lunar_newyear/page1_bank_3.png';
import imgBank4 from './../static/images/lunar_newyear/page1_bank_4.png';
import imgBank5 from './../static/images/lunar_newyear/page1_bank_5.png';
import imgBank6 from './../static/images/lunar_newyear/page1_bank_6.png';
import imgBank7 from './../static/images/lunar_newyear/page1_bank_7.png';
import imgBank8 from './../static/images/lunar_newyear/page1_bank_8.png';
import imgBank9 from './../static/images/lunar_newyear/page1_bank_9.png';
import imgBank10 from './../static/images/lunar_newyear/page1_bank_10.png';

const list = [
    {title:"ภัยคุกคามปริศนาที่ตื่นขึ้น", image:imgBank1},
    {title:"ฝันร้ายแห่งกรุสมบัติ", image:imgBank2},
    {title:"เสียงเพลงอันโหยหวน", image:imgBank3},
    {title:"ผู้พลัดถิ่นแห่งเมืองโบราณพรรณไม้", image:imgBank4},
    {title:"เสียงเพรียกของสายลม", image:imgBank5},
    {title:"หุบเขาเหมันต์", image:imgBank6},
    {title:"รุกฆาต", image:imgBank7},
    {title:"ผู้บุกรุกที่ศาลนักพรตนาริว", image:imgBank8},
    {title:"เสียงหอนของเตาหลอม", image:imgBank9},
    {title:"พันธะที่ผูกเราไว้", image:imgBank10},
]
const PageComponent = (props)=>(
    <div className="page page--page1gacha">
        <div className="page__section page__section--head" />
        <div className="page__section">
            <div className="page__content page__content--page1detail">
                <div className="page__header">
                    1 | ล่าแบงค์กาโม่
                    <div className="page__btnhome">
                        <BtnHome onClick={()=>props.actChangePage("main")} />
                    </div>

                </div>
                <br />
                <ul className="page__rule">
                    <li>ลงดันสะสมแบงค์กาโม่ เพื่อใช้ในการ เสี่ยงทายซาลาเปา</li>
                    <li>แบงค์กาโม่รีเซ็ททุก 6 โมงเช้า</li>
                    <li>ระยะเวลาตามล่าแบงค์กาโม่ 8 - 25 กุมภาพันธ์ 2562 (05:59)</li>
                    <li>พิเศษสำหรับอาชีพวอร์เดนจะได้รับแบงค์กาโม่ x2 จากการลงดัน</li>
                </ul>
            </div>
        </div>
        <br /><br />
        <div className="page__section">
            <AmountBar amounts={props.pointsSummary}/>
        </div>
        <div className="page__section page__section--pagegacha">
            <div className="page__content page__content--pagegacha">
                {props.quests.map((item,index)=>{
                    return (
                        <SlotGacha src={list[index].image} active={item.status} >
                            <span>{item.blue_gamo}</span>
                        </SlotGacha>
                    )
                })}
            </div>
        </div>
    </div>
)
export default PageComponent;
const BackScore = styled.div`
    position: relative;
    display: block;
    width: 40%;
    height: 80%;
    text-align: center;
    font-size: 24px;
    font-weight: bolder;
    top: 20px;
    img {
        vertical-align: middle;
    }
    div {
        line-height: 1em;
        font-size: 2.5em;
    }
`;
const Area = styled.div`
    display: flex;
    width: 880px;
    justify-content: space-around;
    align-items: center;
    flex-flow: wrap;
`;
const SlotGacha = styled.div`
    position: relative;
    display: inline-block;
    width: 165px;
    height: 278px;
    background: no-repeat center url(${props => props.src});
    margin: 5px;
    filter: grayscale(${props => props.active ? "0":"1"});
    span {
        position: absolute;
        bottom: 0px;
        width: 100%;
        color: #d53b3a;
        font-size: 20px;
        font-weight: bolder;
        text-align: center;
    }
`;
