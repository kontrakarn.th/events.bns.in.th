import React from "react";
import styled from 'styled-components';

import iconBlueBank from './../static/images/lunar_newyear/icon_blue_bank.png';
import iconRedBank from './../static/images/lunar_newyear/icon_red_bank.png';
import iconCoin from './../static/images/lunar_newyear/icon_coin.png';

import imgBackground from './../static/images/lunar_newyear/amount_bar_background.jpg';
// import imgBackground from './../static/images/lunar_newyear/position_amount_bar.png';

const list = [
    {image:iconBlueBank, title:"แบงค์กาโม่ฟ้าที่มี", key:"remain_blue_gamo"},
    {image:iconBlueBank, title:"แบงค์กาโม่ฟ้าที่ใช้ไป", key:"used_blue_gamo"},
    {image:iconRedBank, title:"แบงค์กาโม่แดงที่มี", key:"remain_red_gamo"},
    {image:iconRedBank, title:"แบงค์กาโม่แดงที่ใช้ไป", key:"used_red_gamo"},
    {image:iconCoin, title:"เหรียญทองที่มี", key:"remain_gold"},
    {image:iconCoin, title:"เหรียญทองที่ใช้ไป", key:"used_gold"},
]
export default class extends React.Component {
    constructor(props){
        super(props);
        this.state = {

        }
    }
    render() {
        return (
            <Amountbar>
                <Amountslot/>
                {list.map((item,index)=>{
                    return (
                        <Amountslot key={"slot_"+index}>
                            <img src={item.image} />
                            <div>{item.title}</div>
                            <span>{this.props.amounts[item.key]}</span>
                        </Amountslot>
                    )
                })}
                <Amountslot/>
            </Amountbar>
        )
    }
}
const styles = {};
const Amountbar = styled.div`
    position: relative;
    display: block;
    /* display: grid; */
    /* grid-template-columns: auto 148px 160px 162px 160px 155px 145px auto; */
    width: 952px;
    height: 208px;
    background: no-repeat center url(${imgBackground});
    border-radius: 5px;
    left: 70px;
    /* bolder: 20px solid rgba(100,100,0,0.3); */
    /* border-image: url(${imgBackground}) 30 round; */
`;
const Amountslot = styled.div`
    position: relative;
    display: inline-block;
    /* width: calc(100% / 6); */
    padding-top: 35px;
    text-align: center;
    font-size: 18px;
    font-weight: bolder;
    span {
        font-size: 2.8em;
        line-height: 1em;
    }
    &:nth-child(1) {
        width: 12px;
    }
    &:nth-child(2) {
        width: 148px;
    }
    &:nth-child(3) {
        width: 160px;
    }
    &:nth-child(4) {
        width: 162px;
    }
    &:nth-child(5) {
        width: 160px;
    }
    &:nth-child(6) {
        width: 155px;
    }
    &:nth-child(7) {
        width: 145px;
    }
    &:nth-child(8) {
        width: auto;
    }

`;
