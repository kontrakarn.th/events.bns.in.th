import React from "react";
import styled from 'styled-components';

import iconBank from './../static/images/lunar_newyear/page0_icon1_bank.png';
import iconBuns from './../static/images/lunar_newyear/page0_icon2_buns.png';
import iconReward from './../static/images/lunar_newyear/page0_icon3_reward.png';
import iconOther from './../static/images/lunar_newyear/page0_icon4_other.png';
import iconAnnounce from './../static/images/lunar_newyear/page0_icon5_announce.png';

const list = [
    {title:"ล่าแบงค์กาโม่", link:"gacha", icon:iconBank},
    {title:"เสี่ยงทายซาลาเปา", link:"buns", icon:iconBuns},
    {title:"ยิงอั่งเปาเพิ่มความสุข", link:"reward", icon:iconReward},
    {title:"ร้านโชห่วย เร่เข้ามา", link:"other", icon:iconOther},
    {title:"กระดานประกาศศักดา", link:"announce", icon:iconAnnounce},
]

const PageComponent = (props)=>(
    <div className="page page--page0main">
        <div className="page__section page__section--head" />
        <div className="page__section">
            <div className="page__content page__content--page0detail">
                <div className="page__header">หน้าหลัก | ลำดับกิจกรรม</div>
                <ul className="page__menu">
                    {list.map((item,index)=>(
                        <li key={"p0slot_"+index}>
                            <a onClick={()=>props.actChangePage(item.link)} >
                                <div className="title">{(index+1)+" | "+item.title}</div>
                                <div className="icon">
                                    <img src={item.icon} />
                                </div>
                            </a>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    </div>
)
export default PageComponent;
