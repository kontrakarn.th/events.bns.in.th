import React from "react";
import styled from 'styled-components';
import ScrollArea from 'react-scrollbar';

import AmountBar from './../components/AmountBar';
import { BtnHome, BtnText } from './../components/Buttons';
import Modal from './../components/Modal';

import bgTop from './../static/images/lunar_newyear/page_2_buns_top_background.png';
import bgBot from './../static/images/lunar_newyear/page_2_buns_bottom_background.png';

import iconCytalBlue from './../static/images/lunar_newyear/cytal_blue.png';
import iconCytalHongmoon from './../static/images/lunar_newyear/cytal_hongmoon.png';
import iconCytalMoon from './../static/images/lunar_newyear/cytal_moon.png';
import iconCytalSecret from './../static/images/lunar_newyear/cytal_secret.png';
import iconCytalSoul from './../static/images/lunar_newyear/cytal_soul.png';
import iconCytalSpirit from './../static/images/lunar_newyear/cytal_spirit.png';
import iconCytalYingyang from './../static/images/lunar_newyear/cytal_yingyang.png';
import iconCoin1 from './../static/images/lunar_newyear/coin_1.png';
import iconCoin2 from './../static/images/lunar_newyear/coin_2.png';
import iconCoin3 from './../static/images/lunar_newyear/coin_3.png';
import iconCoin4 from './../static/images/lunar_newyear/coin_4_7.png';
import iconCoin8 from './../static/images/lunar_newyear/coin_8_10.png';
import iconCoin11 from './../static/images/lunar_newyear/coin_11_50.png';

const cytalsImage = [
    iconCytalYingyang,
    iconCytalSpirit,
    iconCytalSecret,
    iconCytalHongmoon,
    iconCytalBlue,
    iconCytalMoon,
    iconCytalSoul,
]
const levelList = [
    {image:iconCoin1,  cytalList:[0,1,2,3,4]},
    {image:iconCoin2,  cytalList:[1,3,4]},
    {image:iconCoin3,  cytalList:[2,3,4]},
    {image:iconCoin4,  cytalList:[4,3]},
    {image:iconCoin8,  cytalList:[3]},
    {image:iconCoin11, cytalList:[5,6]}
]

const c50 = [
    0,1,2,3,4,5,6,7,8,9,
    10,11,12,13,14,15,16,17,18,19,
    20,21,22,23,24,25,26,27,28,29,
    30,31,32,33,34,35,36,37,38,39,
    40,41,42,43,44,45,46,47,48,49,
]
export default class extends React.Component {
    fncCheckRank(rank,setRank){
        // setRank = [0,1,2,3,4,5]
        if(setRank < 3 && rank === setRank + 1) return true;
        // rank 4-7
        else if(setRank === 3 && rank >= 4 && rank <= 7) return true;
        // rank 8-10
        else if(setRank === 4 && rank >= 8 && rank <= 10) return true;
        // rank 11-50
        else if(setRank === 5 && rank >= 11 && rank <= 50) return true;

    }
    actRedeemRankReward(){
        this.props.setLoading(true);
        this.props.actRedeemRankReward({
            package_id: this.props.package_id,
            actSuccess: (content,msg)=>{
                this.props.setMainState({...content})
                this.setState({openModalMessage: true,openModalConfirm: false, modalMessage: msg});
                this.props.setLoading(false);
            },
            actNoPermission: ()=>{},
            actFalse:(msg)=>{
                this.setState({openModalMessage: true,openModalConfirm: false,modalMessage: msg});
                this.props.setLoading(false);
            }
        });
    }
    //========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props){
        super(props);
        this.state = {
            openModalConfirm: false,
            openModalMessage: false,
            modalMessage: "",
        }
    }
    render() {
        return (
            <div className="page page--page5announce">
                <div className="page__section page__section--head" />
                <div className="page__section">
                    <div className="page__content page__content--page1detail">
                        <div className="page__header">
                            5 | กระดานประกาศศักดา
                            <div className="page__btnhome">
                                <BtnHome onClick={()=>this.props.actChangePage("main")} />
                            </div>
                        </div>
                        <br/>
                        <ul className="page__rule">
                            <li>ระยะเวลาการแข่งขันเพื่อเก็บคะแนน 8 - 25 กุมภาพันธ์ 2562 (เวลา 05:59 น.)</li>
                            <li>ระยะเวลาในการกดรับของ 25 กุมภาพันธ์ 2562 (06:00) - 2 มีนาคม 2562 (23:59)</li>
                        </ul>
                    </div>
                </div>
                <br />
                <div className="page__section">
                    <AmountBar amounts={this.props.pointsSummary}/>
                </div>
                <br />
                <div className="page__section">
                    <div className="page__content page__content--level">
                        {levelList.map((item,index)=>{
                            return (
                                <SlotLevel index={index}>
                                    <img src={item.image} />
                                    <div className="cytals" index={index}>
                                    {item.cytalList.map((cytal,cytal_index)=>{
                                        return (
                                            <img style={{margin:"5px"}} src={cytalsImage[cytal]} />
                                        )
                                    })}
                                    </div>
                                    {
                                        this.props.received && this.fncCheckRank(this.props.my_rank,index) ?
                                            <BtnText
                                                onClick={()=> {}}
                                                status={false}
                                                text={"รับไปแล้ว"}
                                            />
                                        :
                                            (
                                                this.props.can_receive && this.fncCheckRank(this.props.my_rank,index) ?
                                                    <BtnText
                                                        onClick={()=> this.fncCheckRank(this.props.my_rank,index) ? this.setState({openModalConfirm: true}) : null}
                                                        status={this.fncCheckRank(this.props.my_rank,index)}
                                                        text={"รับของรางวัล"}
                                                    />
                                                :
                                                    <BtnText
                                                        onClick={()=> {}}
                                                        status={false}
                                                        text={"ไม่ตรงเงื่อนไข"}
                                                    />
                                                    
                                            )
                                            
                                    }
                                </SlotLevel>
                            )
                        })}
                    </div>
                    <br/><br/>
                </div>
                <div className="page__section page__section--list">
                    <ListTop>
                        <ListHeader>อันดับของฉัน</ListHeader>
                        {this.props.ranks.map((item,index)=>{
                            if(item.char_name === this.props.my_char_name) {
                                return (
                                    <SlotRank mark={false}>
                                        <div style={{width:"140px"}}>{item.rank}</div>
                                        <div style={{width:"280px"}}>{item.char_name}</div>
                                        <div style={{width:"210px"}}>{item.world}</div>
                                        <div>{item.score}</div>
                                    </SlotRank>
                                )
                            }
                        })}
                    </ListTop>
                    <ListBot>
                        <ListHeader>อันดับทั้งหมด</ListHeader>
                            {this.props.ranks.map((item,index)=>{
                                return (
                                    <SlotRank mark={item.char_name === this.props.my_char_name}>
                                        <div style={{width:"140px"}}>{item.rank}</div>
                                        <div style={{width:"280px"}}>{item.char_name}</div>
                                        <div style={{width:"210px"}}>{item.world}</div>
                                        <div>{item.score}</div>
                                    </SlotRank>
                                )
                            })}
                    </ListBot>
                </div>
                {/* ========= ========= ========= ========= ========= ========= ========= ========= ========= */}
                {/* Modal */}
                <Modal
                    open={this.state.openModalConfirm}
                    title={"ยื่นยัน"}
                    confirmMode={true}
                    actConfirm={this.actRedeemRankReward.bind(this)}
                    actClose={()=>this.setState({openModalConfirm: false})}
                >
                    <ConfirmRedeem>
                        {/*<div>ท่านต้องการแลกของรางวัล <span>Rank {this.props.my_rank} ?</span></div>*/}
                        <div>ท่านต้องการแลกของรางวัล ?</div>
                    </ConfirmRedeem>
                </Modal>
                <Modal
                    open={this.state.openModalMessage}
                    title={""}
                    confirmMode={false}
                    actConfirm={()=>{}}
                    actClose={()=>this.setState({openModalMessage: false})}
                >
                    <ConfirmRedeem>
                        {/* <div>คุณได้รับ</div><br/> */}
                        <div dangerouslySetInnerHTML={{
                            __html: this.state.modalMessage
                        }}/>
                    </ConfirmRedeem>
                </Modal>
                {/* ========= ========= ========= ========= ========= ========= ========= ========= ========= */}
            </div>
        )
    }
}

const SlotLevel = styled.div`
    position: relative;
    display: inline-flex;
    /* width: 100%; */
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    vertical-align: top;
    margin: 20px 0px;
    margin: ${props=> (props.index<3)? "0px 0px 20px":"20px 0px"};
    &>img {
        margin-bottom: 20px;
    }
    .cytals {
        display: block;
        width: 160px;
        height: ${props=> (props.index<3)? "270px":"100px"};
        margin: 0px auto;
    }
    .cytals_shot {
        height: 100px;
    }
    &:nth-child(1){
        width:  298px;
    }
    &:nth-child(2) {
        width: 316px;
    }
    &:nth-child(3) {
        width: 298px;
    }
    &:nth-child(4){
        width:  298px;
    }
    &:nth-child(5) {
        width: 316px;
    }
    &:nth-child(6) {
        width: 298px;
    }

`;
const ListTop = styled.div`
    display: block;
    width: 760px;
    margin: 0px auto;
    padding-top: 130px;
    height: 100px;
    font-size: 22px;
    line-height: 2em;
    font-weight: bolder;
`;
const ListBot = styled.div`
    display: block;
    width: 760px;
    margin: 0px auto;
    font-size: 20px;
    font-weight: bolder;
    padding-bottom: 145px;
`;
const ListHeader = styled.div`
    color: #7d1010;
    font-size: 22px;
    line-height: 2em;
`;
const SlotRank = styled.div`
    display: flex;
    width: 100%;
    margin: 0px auto;
    /* height: 1.2em; */
    /* line-height: 1em; */
    color: ${props=> props.mark? "#bb2c2c" : "#000000"};
`;
const ConfirmRedeem = styled.div`
    position: relative;
    display: flex;
    width: 80%;
    min-height: 150px;
    justify-content: center;
    align-items: center;
    margin: 0px auto;
    padding: 10px;
    border-radius: 20px;
    background-color: #626262;
    /* text-align: center; */
    span {
        display:block;

    }
`;
// bgBot
