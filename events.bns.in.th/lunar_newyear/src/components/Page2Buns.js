import React from "react";
import styled, { keyframes } from 'styled-components';

import AmountBar from './../components/AmountBar';
import {BtnHome,BtnPlay} from './../components/Buttons';
import Modal from './../components/Modal';

import bgTop from './../static/images/lunar_newyear/page_2_buns_top_background.png';
import bgBot from './../static/images/lunar_newyear/page_2_buns_bottom_background.png';
import iconBuns from './../static/images/lunar_newyear/icon_buns.png';

const msg = [
    "ยินดีด้วยท่านทายถูก<br/><br/>ได้รับแบงค์กาโม่แดง<br/>จำนวน 1 แบงค์​<br/>",
    "เสียใจจังท่านทายผิด<br/><br/>ได้รับเหรียญทอง<br/>จำนวน 25 เหรียญ<br/>",
    "เสียใจจังท่านทายผิด<br/><br/>ท่านจะไม่ได้รับเหรียญ<br/>เนื่องจากเกินกำหนด<br/>การรับต่อวันแล้ว<br/>",
]
export default class extends React.Component {
    actPlayBunsFortune(index){
        if(this.state.play_game&&!this.state.selected&&!this.state.lock_click){
            this.setState({lock_click: true});
            this.props.actPlayBunsFortune({
                actSuccess: (data)=> {
                    let sub = [ "smile", "angry", "cry" ];
                    let bunPosition = ["", "smile", "angry", "cry" ];
                    let msgIndex = data.buns_result === 1 ? 0 : (data.has_gold ? 1 : 2);
                    while(sub[index] !== bunPosition[data.buns_result]) { sub.push(sub.shift()); }

                    this.setState({ buns:sub, selected: true });
                    this.props.setMainState({
                        points_summary: data.points_summary,
                        free_rights: data.free_rights
                     });

                    setTimeout( ()=>{ this.setState({ openModalMessage: true, modalMessage: msg[msgIndex] }) }, 700);
                },
                actNoPermission: ()=>{},
            });
        }
    }
    //========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props){
        super(props);
        this.state = {
            openModalConfirm: false,
            openModalMessage: false,
            modalMessage: "",
            play_game: false,
            selected: false,
            buns: ["","",""],
            lock_click: false,
        }
    }
    render() {
        let { play_game, selected, buns, openModalConfirm, openModalMessage} = this.state;
        let free_rights = this.props.free_rights;
        let remain_blue_gamo = this.props.pointsSummary && this.props.pointsSummary.remain_blue_gamo;


        // let checkForPlay = play_game || !(free_rights || remain_blue_gamo > 0) ;
        let checkForPlay = !play_game && (free_rights || remain_blue_gamo > 0) && !openModalConfirm && !openModalMessage;


        return (
            <div className="page page--page1gacha">
                <div className="page__section page__section--head" />
                <div className="page__section">
                    <div className="page__content page__content--page1detail">
                        <div className="page__header">
                            2 | เสี่ยงทายซาลาเปา
                            <div className="page__btnhome">
                                <BtnHome onClick={()=>this.props.actChangePage("main")} />
                            </div>
                        </div>
                        <br/>
                        <ul className="page__rule">
                            <li>ระยะเวลาเสี่ยงทายซาลาเปา 8 - 25 กุมภาพันธ์ 2562 (05:59)</li>
                            <li>เล่นฟรีวันละ 1 ครั้ง</li>
                            <li>ระบบจะกำหนดใส้ซาลาเปาว่าเป็น "หน้ายิ้ม" และมีซาลาเปา 3 ลูก ให้เลือกว่าหน้ายิ้มอยู่อันไหน</li>
                            <li>เมื่อตอบถูกจะได้สิทธิ์ (แบงค์กาโม่แดง) ในการเล่น เกมยิงอั่งเปา เพิ่มความสุข 1 สิทธิ์</li>
                            <li>เมื่อตอบผิดจะได้รับคะแนน (เหรียญทอง) เพื่อนำไปใช้แลกของ ในร้านโชห่วย เร่เข้ามา</li>
                            <li style={{"list-style-type": "none"}}>(จำกัด 25 คะแนน ต่อวัน)</li>
                        </ul>
                    </div>
                </div>
                <br /><br />
                <div className="page__section">
                    <AmountBar amounts={this.props.pointsSummary}/>
                </div>
                <br /><br /><br />
                <div className="page__section page__section--pagebuns">

                    <div className="page__content">
                        <BunsTop>
                            <div>
                                <div>ซาลาเปาหน้ายิ้มอยู่ไหนกันนะ ?</div>
                                <IconBuns className="smile"/>
                            </div>
                            <div>
                                <BtnPlay inactive={!checkForPlay} onClick={()=>this.setState({openModalConfirm: checkForPlay})}/>
                            </div>
                        </BunsTop>
                        <BunsBot>
                            <a onClick={()=>this.actPlayBunsFortune(0)}><IconBuns animation={play_game&&!selected} className={buns[0]}/></a>
                            <a onClick={()=>this.actPlayBunsFortune(1)}><IconBuns animation={play_game&&!selected} className={buns[1]}/></a>
                            <a onClick={()=>this.actPlayBunsFortune(2)}><IconBuns animation={play_game&&!selected} className={buns[2]}/></a>
                        </BunsBot>
                    </div>
                </div>
                {/* ========= ========= ========= ========= ========= ========= ========= ========= ========= */}
                {/* Modal */}
                <Modal
                    open={this.state.openModalConfirm}
                    title={"แจ้งเตือน"}
                    confirmMode={true}
                    actConfirm={()=>this.setState({play_game: true, openModalConfirm: false})}
                    actClose={()=>this.setState({openModalConfirm: false,lock_click:false})}
                >
                    <ModalContent>
                        <div>ยืนยันการแลก</div><br/>
                        <div>ใช้แบงค์กาโม่ฟ้า 1 แบงค์เพื่อเปิดกาชาเสี่ยงทายซาลาเปา 1 ครั้ง</div>
                    </ModalContent>
                </Modal>
                <Modal
                    open={this.state.openModalMessage}
                    title={"แจ้งเตือน"}
                    confirmMode={false}
                    actConfirm={()=>{}}
                    actClose={()=>this.setState({
                        openModalMessage: false,
                        play_game: false,
                        selected: false,
                        buns: ["","",""],
                        lock_click:false,
                    })}
                >
                    <ModalContent>
                        <div dangerouslySetInnerHTML={{
                            __html: this.state.modalMessage
                        }}/>
                    </ModalContent>
                </Modal>

                {/* ========= ========= ========= ========= ========= ========= ========= ========= ========= */}
            </div>
        )
    }
}
const BunsTop = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-item: center;
    width: 674px;
    height: 218px;
    margin-left: 200px;
    background: no-repeat center url(${bgTop});
    &>div:nth-child(1) {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        width: 400px;
        text-align: center;
        line-height: 1.5em
        color: #FFFFFF;
        font-size: 26px;
        font-weight: bolder;
    }
    &>div:nth-child(2) {
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        width: 265px;
        text-align: center;
    }

`;
const BunsBot = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 674px;
    height: 218px;
    margin-top: 20px;
    margin-left: 200px;
    background: no-repeat center url(${bgBot});
`;
const animationBlink = keyframes`
  from { filter: drop-shadow(0px 0px 0px #FFFFFF); }
  to { filter: drop-shadow(0px 0px 20px #e5bd82); }
`;

const IconBuns = styled.div`
    position: relative;
    display: block;
    width: 101px;
    height: 87px;
    margin: 0px 20px;
    background: no-repeat top left url(${iconBuns});
    &.smile { background-position: top right; }
    &.angry { background-position: bottom right; }
    &.cry { background-position: bottom left; }
    &:hover {
        animation: ${animationBlink} 0.3s ease-in-out alternate infinite;
        ${props=>props.animation ? "": "animation: none;"}
    }
`;
const ModalContent = styled.div`
    position: relative;
    display: block;
    width: 80%;
    min-height: 150px;
    margin: 0px auto;
    padding: 10px;
    border-radius: 20px;
    background-color: #626262;
    text-align: center;
    span {
        display:block;

    }
`;
