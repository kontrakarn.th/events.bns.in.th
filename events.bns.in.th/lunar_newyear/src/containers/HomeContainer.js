import React, { Component } from 'react';

import {connect} from 'react-redux';
import 'whatwg-fetch';
// import {apiPost} from './../middlewares/Api';

import $ from "jquery";

import './../styles/index.css';
import './../styles/modal.css';
import './../styles/lunar_newyear.css';

import F11Layout from './../features/f11layout';
import { setValues } from './../features/f11layout/redux';
import Main from './../components/Page0Main';
import Gacha from './../components/Page1Gacha';
import Buns from './../components/Page2Buns';
import Reward from './../components/Page3Reward';
import Other from './../components/Page4Other';
import Announce from './../components/Page5Announce';

import FullPagePermission from './../features/permission';
import FullPageLoading from './../features/loading';
import { setLoading } from './../features/loading/redux';
import ModalSelectCharacter from './../features/select_character';

import imgBackground from './../static/images/background.jpg';
import {
    apiGetLobbyInfo,
    apiDailyInfo,
    apiBunsInfo,
    apiPlayBunsFortune,
    apiAngpaoInfo,
    apiPlayAngpao,
    apiSaveAngpaoResult,
    apiUpdateShopScore,
    apiShopInfo,
    apiExchange,
    apiRankInfo,
    apiRedeemRankReward,
} from './../features/api';


import {
    onAccountLogout
} from '../actions/AccountActions';

const pagelist = ["main", "gacha", "buns", "reward", "other", "announce"];

class HomeContainer extends Component {
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showpage: "main",
            modalConfirmToRedeem:false,
            modalMessage:false,
            alertMessage:'',
            package_select:{},
            showModalSelectCharacter: false,

            username: "",
            character: "",
            // amountbar
            points_summary: {
                total_blue_gamo: 0,
                used_blue_gamo: 0,
                remain_blue_gamo: 0,
                total_red_gamo: 0,
                used_red_gamo: 0,
                remain_red_gamo: 0,
                total_gold: 0,
                used_gold: 0,
                remain_gold: 0
            },
            // page1gacha
            quests: [],

            // buns
            free_rights: false,

            // shop
            reward_list: [],

            // rank
            my_rank: "",
            my_score: "",
            ranks: [],
            can_receive: false,
            received: false,
            package_id: false,

        }
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.jwtToken != this.props.jwtToken && this.props.jwtToken !== ""){
            this.props.setLoading(true);
            apiGetLobbyInfo({
                jwtToken: this.props.jwtToken,
                actSuccess: (e)=>{
                    this.props.setValues({character:e.character,username:e.username});
                    setTimeout(()=>this.props.setLoading(false),1000);
                },
                actNoChar: ()=>{
                    this.setState({showModalSelectCharacter: true});
                    setTimeout(()=>this.props.setLoading(false),1000);
                },
                actNoPermission: ()=>this.setState({permission: false}),
                actNoMaintenance: ()=>this.setState({permission: false}),
            });
        }
        if(prevState.showpage !== this.state.showpage) {
            if(this.state.showpage === "gacha"){
                this.props.setLoading(true);
                apiDailyInfo({
                    jwtToken: this.props.jwtToken,
                    actSuccess: (e)=>{
                        this.setState({...e})
                        setTimeout(()=>this.props.setLoading(false),1000);
                    },
                    actNoPermission: ()=>this.setState({permission: false})
                })
            }
            if(this.state.showpage === "buns"){
                this.props.setLoading(true);
                apiBunsInfo({
                    jwtToken: this.props.jwtToken,
                    actSuccess: (e)=>{
                        this.setState({...e});
                        setTimeout(()=>this.props.setLoading(false),1000);
                    },
                    actNoPermission: ()=>this.setState({permission: false})
                })
            }
            if(this.state.showpage === "reward"){
                this.props.setLoading(true);
                apiAngpaoInfo({
                    jwtToken: this.props.jwtToken,
                    actSuccess: (e)=>{
                        this.setState({...e});
                        setTimeout(()=>this.props.setLoading(false),1000);
                    },
                    actNoPermission: ()=>this.setState({permission: false}),
                })
            }
            if(this.state.showpage === "other"){
                this.props.setLoading(true);
                apiShopInfo({
                    jwtToken: this.props.jwtToken,
                    actSuccess: (e)=>{
                        this.setState({...e});
                        setTimeout(()=>this.props.setLoading(false),1000);
                    },
                    actNoPermission: ()=>this.setState({permission: false}),
                })
            }
            if(this.state.showpage === "announce"){
                this.props.setLoading(true);
                apiRankInfo({
                    jwtToken: this.props.jwtToken,
                    actSuccess: (e)=>{
                        this.setState({...e});
                        setTimeout(()=>this.props.setLoading(false),1000);
                    },
                    actNoPermission: ()=>this.setState({permission: false}),
                })
            }
        }

    }
    render() {
        const { jwtToken } = this.props;
        let {showpage} = this.state;
        if(!this.state.permission) return <FullPagePermission />
        return (

            <F11Layout
                showUserName={true}
                showCharacterName={true}
                showLogin={false}
                background={imgBackground}
                // permission={this.state.permission}
                date={"8 ก.พ. - 2 มี.ค. 2562"}
                type={"EVENT"}
            >
                {showpage === pagelist[0] &&
                    <Main
                        actChangePage={(showpage)=>this.setState({showpage})}
                    />
                }
                {showpage === pagelist[1] &&
                    <Gacha
                        actChangePage={(showpage)=>this.setState({showpage})}
                        pointsSummary={this.state.points_summary}
                        quests={this.state.quests}
                    />
                }
                {showpage === pagelist[2] &&
                    <Buns
                        actChangePage={(showpage)=>this.setState({showpage})}
                        pointsSummary={this.state.points_summary}
                        setMainState={(value)=>this.setState(value)}

                        actPlayBunsFortune={(value)=>apiPlayBunsFortune({jwtToken,...value})}
                        free_rights={this.state.free_rights}
                    />
                }
                {showpage === pagelist[3] &&
                    <Reward
                        actChangePage={(showpage)=>this.setState({showpage})}
                        pointsSummary={this.state.points_summary}
                        setMainState={(value)=>this.setState(value)}

                        apiPlayAngpao={(value)=>apiPlayAngpao({jwtToken,...value})}
                        apiSaveAngpaoResult={(value)=>apiSaveAngpaoResult({jwtToken,...value})}
                        apiUpdateShopScore={(value)=>apiUpdateShopScore({jwtToken,...value})}

                    />
                }
                {showpage === pagelist[4] &&
                    <Other
                        actChangePage={(showpage)=>this.setState({showpage})}
                        pointsSummary={this.state.points_summary}
                        setMainState={(value)=>this.setState(value)}

                        actExchange={(value)=>apiExchange({jwtToken,...value})}
                        reward_list={this.state.reward_list}
                        setLoading={this.props.setLoading.bind(this)}
                    />
                }
                {showpage === pagelist[5] &&
                    <Announce

                        actChangePage={(showpage)=>this.setState({showpage})}
                        pointsSummary={this.state.points_summary}

                        actRedeemRankReward={(value)=>apiRedeemRankReward({jwtToken,...value})}
                        my_char_name={this.state.character}
                        // my_name={this.state.character}
                        my_rank={this.state.my_rank}
                        my_score={this.state.my_score}
                        ranks={this.state.ranks}
                        can_receive={this.state.can_receive}
                        received={this.state.received}
                        package_id={this.state.package_id}
                        setLoading={this.props.setLoading.bind(this)}
                        setMainState={(value)=>this.setState(value)}
                    />
                }
                <FullPageLoading />

                <ModalSelectCharacter
                    open={this.state.showModalSelectCharacter}
                    actDone={(data_name)=>{
                        this.props.setValues(data_name);
                        this.setState({showModalSelectCharacter:false})
                    }}/>
            </F11Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    showScroll: state.layout.showScroll
})
const mapDispatchToProps = {onAccountLogout,setValues,setLoading};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
