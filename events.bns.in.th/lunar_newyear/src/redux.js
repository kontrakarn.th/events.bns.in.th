import axios from "axios";

const initialState = {
  fullName: "",
  email: "",
  agreeTerms: false,
  countdownText: "",
  loading: false,
  message: "",
  status: false,
};

export default (state = initialState, action) => {
  // console.log("action.typ", action.typ);
  switch (action.type) {
    case "SET_PERMISSION_STATUS":
        return {
            ...state,
        }
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return {
          ...state,
          [action.key]: action.value
        };
      } else {
        return state;
      }

    case "SUBMIT_PENDING":
      return {
        ...state,
        loading: true
      };
    case "SUBMIT_FULFILLED":
      // console.log(action.payload.data);

      return {
        ...state,
        loading: false,
        message: action.payload.data.message
      };
    case "SUBMIT_REJECTED":
      return {
        ...state,
        loading: false,
        message: "error"
      };

    case "USE_STATE":
      return {
        ...state,
        [action.key]: action.value
      };
    case "SUBMIT2_PENDING":
      // console.log("SUBMIT2_PENDING");
      return {
        ...state,
        loading: true
      };
    case "SUBMIT2_FULFILLED":
      // console.log("SUBMIT2_FULFILLED", action.payload);
      return {
        ...state,
        loading: false,
        message: action.payload.message
      };
    case "SUBMIT2_REJECTED":
      // console.log("SUBMIT2_REJECTED");
      return {
        ...state,
        loading: false,
        message: action.payload.message
      };
    // case "SET_FULL_NAME":
    //   return {
    //     ...state,
    //     fullName: action.value
    //   };
    // case "SET_EMAIL":
    //   return {
    //     ...state,
    //     email: action.value
    //   };
    // case "SET_AGREE_TERMS":
    //   return {
    //     ...state,
    //     agreeTerms: action.value
    //   };
    // case "SET_COUNTDOWN_TEXT":
    //   return {
    //     ...state,
    //     countdownText: action.value
    //   };
    default:
      return state;
  }
};

export const setValue = (key, value) => ({
  type: "SET_VALUE",
  value,
  key
});
// export const setFullName = value => ({
//   type: "SET_FULL_NAME",
//   value: value
// });
// export const setEmail = value => ({
//   type: "SET_EMAIL",
//   value: value
// });
// export const setAgreeTerms = value => ({
//   type: "SET_AGREE_TERMS",
//   value: value
// });
// export const setCountdownText = value => ({
//   type: "SET_COUNTDOWN_TEXT",
//   value: value
// });

export const submit = (fullName, email) => ({
  type: "SUBMIT",
  // payload: axios.post("http://www.mocky.io/v2/5bfbb9c23100002b0039b9c1")
  payload: axios.post("http://www.mocky.io/v2/5bfbc91a310000730039ba6c")
});
export const submit2 = (fullName, email) => {
  // console.log("submit2");

  return {
    type: "SUBMIT2",
    payload: fetch("http://www.mocky.io/v2/5bfbc91a310000730039ba6c")
      .then(res => res.json())
      .then(json => {
        return json;
      })
  };
};
