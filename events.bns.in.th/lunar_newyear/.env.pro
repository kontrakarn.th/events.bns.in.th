REACT_APP_EVENT_PATH=/lunar_newyear
REACT_APP_LOCALE=th-TH
REACT_APP_CSRF_COOKIE_NAME=csrf_token
REACT_APP_STATIC_PATH=/static/
REACT_APP_COOKIE_DOMAIN=.bns.in.th

REACT_APP_API_SERVER_HOST=https://events.bns.in.th
#REACT_APP_API_SERVER_HOST=http://testevents.bns.in.th
REACT_APP_API_GET_ACCOUNT_INFO=/oauth/get_account_info
REACT_APP_API_GET_OAUTH_INFO=/oauth/get_oauth

REACT_APP_API_POST_GET_CHARACTER        =/api/lunar_newyear/get_character
REACT_APP_API_POST_ACCEPT_CHARACTER     =/api/lunar_newyear/accept_character
REACT_APP_API_POST_LOBBY_INFO           =/api/lunar_newyear/lobby_info
REACT_APP_API_POST_DAILY_INFO           =/api/lunar_newyear/daily_info
REACT_APP_API_POST_BUNS_INFO            =/api/lunar_newyear/buns_info
REACT_APP_API_POST_PLAY_BUNS_FORTUNE    =/api/lunar_newyear/play_buns_fortune
REACT_APP_API_POST_ANGPAO_INFO          =/api/lunar_newyear/angpao_info
REACT_APP_API_POST_PLAY_ANGPAO          =/api/lunar_newyear/play_angpao
REACT_APP_API_POST_SAVE_ANGPAO_RESULT   =/api/lunar_newyear/save_angpao_result
REACT_APP_API_POST_UPDATE_SHOP_SCORE    =/api/lunar_newyear/update_shop_score
REACT_APP_API_POST_SHOP_INFO            =/api/lunar_newyear/shop_info
REACT_APP_API_POST_EXCHANGE             =/api/lunar_newyear/exchange
REACT_APP_API_POST_RANK_INFO            =/api/lunar_newyear/rank_info
REACT_APP_API_POST_REDEEM_RANK_REWARD   =/api/lunar_newyear/redeem_rank_reward

# SSO related
REACT_APP_SSO_APP_ID=10046
REACT_APP_SSO_URL=https://sso.garena.com
REACT_APP_SSO_PARAM_NAME=session_key
REACT_APP_SSO_COOKIE_NAME=sso_session

# OAUTH related
REACT_APP_OAUTH_APP_ID=10007
#REACT_APP_OAUTH_APP_NAME=bns_lunar_newyear_test
REACT_APP_OAUTH_APP_NAME=bns_lunar_newyear
REACT_APP_OAUTH_PLATFORM=1
REACT_APP_OAUTH_URL=https://auth.garena.com
REACT_APP_OAUTH_PARAM_NAME=token
REACT_APP_OAUTH_COOKIE_NAME=oauth_session

# Landing Page
REACT_APP_LANDING_SERVER_HOST=https://landing.garena.in.th
REACT_APP_LANDING_JSON=/static/data/get_landing.json

# JWT
REACT_APP_JWT_SECRET=PFbeH0I4qJ2SwDyp6EO62SaSX217m1iZ
