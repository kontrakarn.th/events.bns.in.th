import React from 'react'
import {setValues , actCloseModal} from './redux';
import { connect } from "react-redux";

const ModalCore =(props)=>{
	//input
	let {name,outSideClick} = props;
	//redux
	return(
		<div className={"modal " + (props.modal_open === name ? 'open':'close')}>
			<div className={"modal__backdrop " + (props.modal_open === name ? 'bopen' : 'bclose')} onClick={()=>{outSideClick && props.actCloseModal() }} />
			<div className="modal__content">
				{props.children && props.children }
			</div>
		</div>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect( 
    mapStateToProps, 
    mapDispatchToProps 
)(ModalCore);