import React from "react";
import { connect } from "react-redux";
import ModalCore from "./ModalCore.js";
import './styles.scss';
import { setValues, actCloseModal } from "./redux";

const ConfirmReceiveModal = (props) => {
  let {modal_message} = props;
	return (
    <ModalCore name="receive" outSideClick={false}>
      	<div className="contentwrap">
					<div className="text text--head">แจ้งเตือน</div>
					<div className="inner" dangerouslySetInnerHTML={{ __html: props.modal_message }}></div>
					<div className="modal_btn">
						<div className="btn" onClick={() => props.setValues({ modal_open: '' })}>ตกลง</div>
					</div>
				</div>
    </ModalCore>
	)
}

const mapStateToProps = state => ({
  ...state.ModalReducer,
  ...state.Modal
});

const mapDispatchToProps = { setValues, actCloseModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmReceiveModal);
