import React from 'react';
import {connect} from 'react-redux';

import F11Layout from './../features/F11Layout/';

import { apiPost } from './../middlewares/Api';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../store/redux';

import ModalLoading from './../features/modals/ModalLoading';
import ModalMessage from './../features/modals/ModalMessage';
import ModalConfirm from './../features/modals/ModalConfirm';
import {Imglist} from './../constants/Import_Images';
// import Main from './../features/main';

class Home extends React.Component {
    actConfirm(){
        this.apiBookingPackage(
            this.props.modal_confirmitem && this.props.modal_confirmitem !== ''
            && this.props.modal_confirmitem
        );
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            this.props.setValues({
                ...data.data,
                modal_open: "",
            });
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiBookingPackage(bookingType){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'booking_package', booking_type: bookingType};
        let successCallback = (data) => {
            this.props.setValues({
                ...data.data,
                modal_open:"message",
                modal_message: data.message
            });
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_BOOKING", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiPurchasePackage(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'purchase_package'};
        let successCallback = (data) => {
            this.props.setValues({
                ...data.data,
                modal_open:"message",
                modal_message: data.message
            });
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_PURCHASE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            modalConfirmToRedeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,
            active: [],
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }
    getClassBooking = (bookingType, userType) => {
        let className = "btn btn-booking" + bookingType
        if(
            ((userType === 1) && (bookingType === 1 || bookingType === 2 || bookingType === 3)) ||
            ((userType === 2) && (bookingType === 2 || bookingType === 3)) ||
            ((userType === 3) && (bookingType === 3))
        );
        else{
            className += ' disabled'
        }
        return className
    }
    render() {
        let {user_type, booking_type, purchased} = this.props
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={false}
            >
                <PageWrapper> 
                <div className="section section__promotion">
                    <img src={Imglist.promotion}/>
                </div>
                <div className="section section__package">
                    <div className="section__package--img">
                        <img src={Imglist.package_details}/>
                        <div className="block-booking">
                            {
                                booking_type <= 0 ? 
                                    <>
                                        <div className={this.getClassBooking(1, user_type)} onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการจองแพ็คเกจ',actConfirm:()=>this.apiBookingPackage(1)})}>
                                            <span>จองราคา<br/>50,000 </span><img src={Imglist.icon_diamond}/>
                                        </div>
                                        <div className={this.getClassBooking(2, user_type)} onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการจองแพ็คเกจ',actConfirm:()=>this.apiBookingPackage(2)})}>
                                            <span>จองราคา<br/>80,000 </span><img src={Imglist.icon_diamond}/>
                                        </div>
                                        <div className={this.getClassBooking(3, user_type)} onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการจองแพ็คเกจ',actConfirm:()=>this.apiBookingPackage(3)})}>
                                            <span>จองราคา<br/>125,000 </span><img src={Imglist.icon_diamond}/>
                                        </div>
                                    </>
                                :
                                    purchased === 0 ?
                                        <div className="btn btn-booking1" onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการซื้อแพ็คเกจ',actConfirm:()=>this.apiPurchasePackage()})}>
                                            <span>ซื้อในราคา<br/>
                                                {booking_type === 1 && '50,000'}
                                                {booking_type === 2 && '80,000'}
                                                {booking_type === 3 && '125,000'}
                                            </span><img src={Imglist.icon_diamond}/>
                                        </div>
                                    :
                                        <p>ซื้อแพ็คเกจนี้แล้ว</p>
                            }
                            
                            {/* button disabled */}
                            {/* <p>ซื้อแพ็คเกจนี้แล้ว</p> */}
                            {/* <div className="btn btn-booking1 disabled" onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการซื้อแพ็คเกจ'})}>
                                <span>ซื้อในราคา<br/>50,000 </span><img src={Imglist.icon_diamond}/>
                            </div> */}
                            
                        </div>
                    </div>
                </div>  
                <div className="section section__example">
                    <img src={Imglist.character}/>
                </div>
          
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm />
            </PageWrapper>
            </F11Layout>
        )
    }
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Home);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #11122d;
    width: 1105px;
    padding-top: 700px;
    text-align: center;
    color: #ebd9b1;
    *, *::before, *::after {
        box-sizing: border-box;
    }
    & p {
        margin: 15px 0;
    }
    & img {
        vertical-align: middle;
    }
    & .btn{
        cursor:pointer;
        &-booking1,
        &-booking2,
        &-booking3{
            background: url(${Imglist['btn_booking1']}) no-repeat center top; 
            width: 125px;
            height: 47px; 
            & span{
                color: #fff;
                display: inline-block;
                vertical-align: middle;
                line-height: 1.2;
                font-size: 13px;
                padding: 5px;
            }
            &:hover{
                background-position: bottom center;
            }
            &.disabled{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);
                cursor: not-allowed;
                color: #bfbfbf;
                pointer-events: none;
                &:hover{
                    background-position: center top;
                }
            }
        }
        &-booking2{
            background-image: url(${Imglist['btn_booking2']});
        }
        &-booking3{
            background-image: url(${Imglist['btn_booking3']});
        }
    }
    & .section__example{
        padding: 5% 0;
        & .slick-slide img{
            margin: 0 auto;
        }
        .slick-dots li.slick-active button:before {
            opacity: 1;
            color: #ff9567;
        }
        .slick-dots li button:before{
            font-size: 11px;
            opacity: 1;
            color: #fff;
        }
    }
    & .section__package{
        &--img{
            position: relative;
            display: inline-block;
        }
        & .block-booking{
            position: absolute;
            left:0;
            right: 0;
            bottom: 50px;
            margin: 0 auto;
            & .btn{
                display:inline-block;
                vertical-align: middle;
                margin: 0 20px;
            }
        }
    }
`

