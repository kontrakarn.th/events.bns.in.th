import bg from '../static/images/background.png';
import icon_diamond from '../static/images/diamond.png'

import modal_bg from '../static/images/modal_bg.png'
import modal_btn from '../static/images/modal_btn.png'
import promotion from '../static/images/promotion.png'

import btn_booking1 from '../static/images/btn_booking1.png'
import btn_booking2 from '../static/images/btn_booking2.png'
import btn_booking3 from '../static/images/btn_booking3.png'
import character from '../static/images/character.png'
import package_details from '../static/images/package.png'

export const Imglist = {
	bg,
	icon_diamond,
	modal_bg,
	modal_btn,
	promotion,
	btn_booking1,
	btn_booking2,
	btn_booking3,
	character,
	package_details,	
}
