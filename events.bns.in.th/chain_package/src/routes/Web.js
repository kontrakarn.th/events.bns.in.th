import React, {Component} from 'react';
import {Route} from 'react-router-dom';
// import OauthMiddleware from './../middlewares/OauthMiddleware';
import OauthMiddleware from './../features/oauthLogin';
import Modals from './../features/modals';

import { Home } 	from './../pages/Home';

export class Web extends Component {
	render() {
		return (
			<>
				<Route path={process.env.REACT_APP_EVENT_PATH} component={OauthMiddleware} />
				<Route path={process.env.REACT_APP_EVENT_PATH} component={Modals} />
				<Route path={process.env.REACT_APP_EVENT_PATH} exact component={Home} />
			</>
		);
	}
}
