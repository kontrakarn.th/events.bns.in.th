const bg_main = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/bg_main.jpg';
const content_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/content_1.png';
const content_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/content_2.png';
const content_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/content_3.png';
const btn_buy = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/btn_buy.png';
const btn_claim = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/btn_claim.png';
const board_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/board_confirm.png';
const btn_ok = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/btn_ok.png';
const btn_cancel = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/btn_cancel.png';
const board_message = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/board_message.png';
const slide_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/slide_1.png';
const slide_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/slide_2.png';
const btn_next = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/btn_next.png';
const btn_prev = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/btn_prev.png';
const arrow_left = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/arrow_left.png';
const arrow_right = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/arrow_right.png';
const title_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/title_1.png';
const title_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/title_2.png';
const btn_claimed = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/btn_claimed.png';
const btn_bought = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/chain_package/btn_bought.png';

//const slide1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/slide/1.png'

export const Imglist = {
	bg_main,
	content_1,
	content_2,
	content_3,
	btn_buy,
	btn_claim,
	board_confirm,
	btn_ok,
	btn_cancel,
	board_message,
	slide_1,
	slide_2,
	btn_prev,
	btn_next,
	arrow_left,
	arrow_right,
	title_1,
	title_2,
	btn_claimed,
	btn_bought
}
