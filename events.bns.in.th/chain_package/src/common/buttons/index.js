import React        from 'react';
import styled       from 'styled-components';
import {Imglist}    from './../../constants/Import_Images';

export const CustomBtn = styled.div`
    width: 225px;
    height: 51px;
    background: top center no-repeat url(${Imglist['btn']});
    color: #ffffff;
    cursor: pointer;
    font-size: 1.5em;
    line-height: 2em;
    font-weight: 600;
    filter: drop-shadow(0px 3px 2px #000000);
    &:hover{
        background-position: bottom center;
    }
`

export const YellowBtn = styled.a`
    position: relative;
    display: block;
    width: 115px;
    height: 35px;
    background-color: #ffd073;
    color: #000000;
    cursor: pointer;
    border-radius: 7px;
    font-size: 1em;
    line-height: 35px;
    font-weight: 600;
    filter: drop-shadow(0px 2px 1px #000000);
    &:hover{
        background-color: #ffdc98;
    }
    ${props=>props.lock && " pointer-events: none; filter: grayscale(1); "}

`

export const MiniBtn = styled.a`
    position: relative;
    display: inline-block;
    width: 30px;
    height: 30px;
    background: center no-repeat url(${props=>Imglist['redeem_'+props.type+'_btn']});

    cursor: pointer;
    filter: grayscale(0.1);
    &:hover{
        filter: grayscale(0);
    }
    ${props=>props.lock && "filter: grayscale(0.7); pointer-events: none;"}
`
