import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
import { setValues, onAccountLogout } from './../../store/redux';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import {Imglist} from './../../constants/Import_Images';

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div className={className} onClick={onClick}><img src={Imglist.btn_next}/></div>
    );
}
  
function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div className={className} onClick={onClick}><img src={Imglist.btn_prev}/></div>
    );
}

class Main extends React.Component {
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========

    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    modal_open: "",
                    ...data.content,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiConfirmPurchase(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'purchase_package', package_key: this.props.modal_package_key};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                    modal_package_key: -1,
                    modal_package_name: "",
                    ...data.content,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_PURCHASE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            itemChoose: '',
            permission: false,
            slider_image: [
                {
                    image: Imglist.slide_1,
                    title: 'อาวุธลวงตามังกรสกุณา'
                },
                {
                    image: Imglist.slide_2,
                    title: 'เซ็ตมังกรสกุณา'
                }
            ]
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }

    handleBuy(package_key,package_name){
        // console.log('clicktobuy',index);
        /* this.setState({
            itemChoose: index
        }) */
        this.props.setValues({
            modal_open: 'confirm',
            modal_message: 'ต้องการซื้อแพ็คเกจ?<br/>'+package_name,
            modal_package_key: package_key,
            modal_package_name: package_name,
        })
    }
    handleConfirm(){
        // let index = this.state.itemChoose;
        // let item = ['แพ็คเกจอัญมณีแห่งมนตรา ขั้นต้น','แพ็คเกจอัญมณีแห่งมนตรา ขั้นสูง','แพ็คเกจอัญมณีแห่งมนตรา หายาก']
        // console.log('clicktoconfirm',index);
        /* this.props.setValues({
            modal_open: 'message',
            modal_message: 'ได้รับ '+item[index]+'<br/>ไอเทมส่งเข้าไปในกล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อรับของรางวัล'
        }) */
        this.apiConfirmPurchase();
    }
    /* handleClaim(){
        console.log('clicktoclaim');
        this.props.setValues({
            modal_open: 'message',
            modal_message: 'ได้รับ แพ็คเกจอัญมณีแห่งมนตรา ในตำนาน<br/>ไอเทมส่งเข้าไปในกล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อรับของรางวัล'
        })
    } */

    setPurchaseBtnClass(can_purchase,already_purchased){
        if(already_purchased == true){
            return "purchased";
        }
        if(can_purchase == false){
            return "close";
        }
    }

    render() {
        const settings = {
            dots: true,
            infinite: true,
            fade: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        let rewards = this.props.rewards;
        return (
            <PageWrapper className="main">
                <div className="main__content-1">
                    <img src={Imglist.content_1}/>
                </div>
                <div className="main__content-2">
                    <img src={Imglist.content_2}/>
                    <div className={"main__btn-1 "+((rewards[0] && rewards[0].can_purchase == false) || (rewards[0] && rewards[0].already_purchased == true) ? this.setPurchaseBtnClass(rewards[0].can_purchase,rewards[0].already_purchased) : "")} onClick={()=>this.handleBuy(rewards[0].id,rewards[0].title)}/>
                    <div className={"main__btn-2 "+((rewards[1] && rewards[1].can_purchase == false) || (rewards[1] && rewards[1].already_purchased == true) ? this.setPurchaseBtnClass(rewards[1].can_purchase,rewards[1].already_purchased) : "")} onClick={()=>this.handleBuy(rewards[1].id,rewards[1].title)}/>
                    <div className={"main__btn-3 "+((rewards[2] && rewards[2].can_purchase == false) || (rewards[2] && rewards[2].already_purchased == true) ? this.setPurchaseBtnClass(rewards[2].can_purchase,rewards[2].already_purchased) : "")} onClick={()=>this.handleBuy(rewards[2].id,rewards[2].title)}/>
                    <div className={"main__btn-4 "+((rewards[3] && rewards[3].can_purchase == false) || (rewards[3] && rewards[3].already_purchased == true) ? this.setPurchaseBtnClass(rewards[3].can_purchase,rewards[3].already_purchased) : "")} onClick={()=>this.handleBuy(rewards[3].id,rewards[3].title)}/>
                </div>
                <div className="main__content-3">
                    <div>
                        <Slider {...settings}>
                        {
                            this.state.slider_image.map((items,key)=>{
                                return(
                                    <div key={key} className=""> 
                                        <div className="text-image">
                                            <img src={Imglist.arrow_left}/>
                                            <div>{items.title}</div>
                                            <img src={Imglist.arrow_right}/>
                                        </div>
                                        <img src={items.image} className="show"/>
                                    </div>
                                )
                            })
                        }
                        </Slider>
                    </div>
                </div>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm handleConfirm={()=>this.handleConfirm()}/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_main']}) #11122d;
    width: 1105px;
    padding-top: 733px;
    text-align: center;
    .main{
        &__content-1{
            position: relative;
            text-align: right;
            >img{
                width: 1008px;
            }
        }
        &__content-2{
            position: relative;
            margin-top: -270px;
            >img{
                width: 946px;
                margin-left: -30px;
            }
        }
        &__content-3{
            position: relative;
            margin-top: 75px;
            text-align: center;
            >img{
                width: 915px;
            }
        }
        &__content-3{
            background: top center no-repeat url(${Imglist.content_3});
            color: #fff;
            text-align: center;
            margin-top: 75px;
            padding-top: 145px;
            padding-bottom: 240px;
            & .show{
                margin: 0 auto;
                width: 601px;
                display: block;
            }
            & .slick-dots li.slick-active button:before {
                opacity: 1;
                color: #fff;
            }
            & .slick-dots li button:before{
                font-size: 11px;
                opacity: 1;
                color: #646464;
            }
            & .slick-prev{
                left:120px;
            }
            & .slick-next{
                right:120px;
            }
            & .slick-prev,
            & .slick-next{
                width: auto;
                height: auto;
                z-index: 1;
                &::before{
                    font-size:0;
                }
            }
            & .text-image{
                margin-bottom: 10px;
                >div{
                    display: inline-block;
                    vertical-align: middle;
                    width: 250px;
                }
                >img{
                    display: inline-block;
                    vertical-align: middle;
                    width: 40px;
                }
            }
        }
        &__btn-1{
            width: 119px;
            height: 51px;
            position: absolute;
            background: url(${Imglist.btn_buy}) no-repeat;
            background-position: top center;
            top: 21.3%;
            left: 50%;
            transform: translate(-50%,0);
            cursor: pointer;
            &:hover{
                background-position: center center;
            }
            &.close{
                cursor: default;
                pointer-events: none;
                background-position: bottom center;
            }
            &.purchased{
                background: url(${Imglist.btn_bought}) no-repeat;
                background-position: top center;
                cursor: default;
                pointer-events: none;
            }
        }
        &__btn-2{
            width: 119px;
            height: 51px;
            position: absolute;
            background: url(${Imglist.btn_buy}) no-repeat;
            background-position: top center;
            top: 41%;
            left: 50%;
            transform: translate(-50%,0);
            cursor: pointer;
            &:hover{
                background-position: center center;
            }
            &.close{
                cursor: default;
                pointer-events: none;
                background-position: bottom center;
            }
            &.purchased{
                background: url(${Imglist.btn_bought}) no-repeat;
                background-position: top center;
                cursor: default;
                pointer-events: none;
            }
        }
        &__btn-3{
            width: 119px;
            height: 51px;
            position: absolute;
            background: url(${Imglist.btn_buy}) no-repeat;
            background-position: top center;
            top: 67.8%;
            left: 50%;
            transform: translate(-50%,0);
            cursor: pointer;
            &:hover{
                background-position: center center;
            }
            &.close{
                cursor: default;
                pointer-events: none;
                background-position: bottom center;
            }
            &.purchased{
                background: url(${Imglist.btn_bought}) no-repeat;
                background-position: top center;
                cursor: default;
                pointer-events: none;
            }
        }
        &__btn-4{
            width: 119px;
            height: 51px;
            position: absolute;
            background: url(${Imglist.btn_claim}) no-repeat;
            background-position: top center;
            top: 91.8%;
            left: 50%;
            transform: translate(-50%,0);
            cursor: pointer;
            &:hover{
                background-position: center center;
            }
            &.close{
                cursor: default;
                pointer-events: none;
                background-position: bottom center;
            }
            &.purchased{
                background: url(${Imglist.btn_claimed}) no-repeat;
                background-position: top center;
                cursor: default;
                pointer-events: none;
            }
        }
    }
`


