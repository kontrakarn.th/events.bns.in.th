import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';

import {Imglist} from './../../constants/Import_Images';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="message"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <div className="contenttext" dangerouslySetInnerHTML={{__html: modal_message}} />
                <ModalBottom>
                    <div>
                        <Btns_Ok onClick={()=>props.setValues({modal_open:""})}/>
                    </div>
                </ModalBottom>
            </ModalMessageContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
const ModalBottom = styled.div`
    display: block;
    width: 100%;
    bottom: 0px;
    z-index: 50;
    text-align: center;
    height: 60px;
`;


const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 440px;
    height: 310px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist['board_message']});
    box-sizing: border-box;
    padding: 8% 5% 2%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 21px;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 200px;
        //height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
`;

const Btns_Ok = styled.div`
    background: no-repeat 0 0 url(${Imglist['btn_ok']});
    background-position: top center;
    width: 92px;
    height: 40px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #fff;
    line-height: 36px;
    font-size: 20px;
    &:hover{
        background-position: bottom center;
    }
`