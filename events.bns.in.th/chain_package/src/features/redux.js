
const initialState = {
    //--- event_info
    modal_open: "none",
    modal_message: "",
    modal_confirmitem: '',
    modal_type: '',
    modal_item: '',
    history_list: Array(30).fill(2),

    username: '',
    current_round: -1,
    diamonds: 0,
    item_list: [],
    pool_list: [],
    pool_price: -1,
    pool_status: false,
    selected_pool: false,
    selectedItemKey: -1,
    selectedGroup: -1,

};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return {
          ...state,
          [action.key]: action.value
        };
      } else {
        return state;
      }
    case "SET_VALUES": return { ...state, ...action.value };
    default:
      return state;
  }
};

export const setValue  = (key, value) =>{ return ({ type: "SET_VALUE", value, key }) };
export const setValues = (value) =>{ return ({ type: "SET_VALUES", value }) };
