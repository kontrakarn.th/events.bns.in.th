import React,{useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';

const Random=(props)=>{
	const [activeItem,setActiveItem] = useState(-1);
	const [hasItv,setItv] = useState(false);
	const { firstPosition, lastPosition, time, loop } = props;
	const maxItem = props.children.length;
	let count = 0;


	const modifyChildren=( child, index )=>{
		const props = {
			className: child.props.className,
			active: ( child.props.disabled ? false : activeItem == index),
		};
		return React.cloneElement(child, props);
	}
	const checkSetActiveItem=()=>{

	}
	useEffect(()=>{
		setActiveItem(firstPosition);
		let maxCount = (maxItem*loop)+(lastPosition+maxItem-firstPosition);
		let allTime = time*1000;
		if( firstPosition !== lastPosition && !hasItv ){

			let _itv = setInterval(()=>{
				// console.log("count",count,count%maxItem,maxItem);
				if(count >= maxCount ) {
					clearInterval(_itv);
					setItv(false);
					if(props.callback){
						props.callback()
					}
				};
				let val = (firstPosition + maxItem + count) % maxItem;
				setActiveItem(val);
				count++;
			},allTime/maxCount);
			setItv(true);
		};
	},[ firstPosition, lastPosition]);
	return(
	    <React.Fragment>
	     		{React.Children.map(props.children, ( child, index ) => modifyChildren( child, index ))}
	    </React.Fragment>
    )
}

const mapStateToProps = state => ({...state.Main});

const mapDispatchToProps = {};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Random));
