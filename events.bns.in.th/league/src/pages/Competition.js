import React,{useState,useEffect} from 'react';
import {connect} from 'react-redux';
import F11Layout from './../features/F11Layout/';
import {setValues} from './../store/redux';
import styled from 'styled-components';
import {imgList} from './../constants/Import_Images';
import Pagination from './../features/Pagination';
import Slider from "react-slick";
import {apiPost} from './../constants/Api';

const numberFigPosition = (nunber,position) => {
  let str = "0000000000000000000000"+nunber;
  return str.slice(str.length-position);
};

const CompetitionPage = props => {
  const logo_team = [
    "",
    imgList.icon_team_green,
    imgList.icon_team_yellow,
    imgList.icon_team_blue,
    imgList.icon_team_red,
    imgList.icon_team_pink,
  ]
  const rankStatus = {
    "up" :imgList.icon_rank_up,
    "down" :imgList.icon_rank_down,
    "none" :""
  }

  const [competitionSlide,setCompetitionSlide] = useState(null);
  const [page,setPage] = useState(0);

  const rankingSettings = {
      dots: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: null,
      prevArrow: null,
      afterChange: (p)=>setPage(p),
  };

  const apiCompetitionInfo = () => {
    if(props.jwtToken) {
      props.setValues({modal_open: "loading"});
      apiPost(
        "REACT_APP_API_POST_COMPETITION_INFO",
        props.jwtToken,
        {type: "competition_info"},
        (data)=>{//function successCallback
          props.setValues({ ...data.data, modal_open: "" });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message
          });
        },
      );
    }
  }

  useEffect(()=>{ apiCompetitionInfo(); },[props.jwtToken]);

  let pageCount = Math.ceil(props.rank_list.length/25);
  let list = props.rank_list;
  let pageList = [0,1,2,4].splice(0,pageCount);
  let pageSet = [[0,24],[25,49],[50,74],[75,99]].splice(0,pageCount);

  useEffect(()=>{
    setTimeout(()=>{
      if(competitionSlide && competitionSlide !== null) {
        competitionSlide.slickGoTo(0);
      }
    },500);
  },[props.rank_list]);

  return (
    <F11Layout showUserName={true} showCharacterName={true} page={"competition"}>
      <CompetitionStyle className="competition">
        <div className="competition__section">
          <ul style={{padding: "30px 50px 0px"}}>
            <li className="competition__slot" key="cs_00">
              <div className="competition__status"/>
              <div className="competition__number">อันดับ</div>
              <div className="competition__player">ชื่อ</div>
              <div className="competition__point">คะแนน</div>
            </li>
          </ul>
          <Slider key="rankingslide" {...rankingSettings} ref={e=>{if(e)setCompetitionSlide(e)}}>
          {pageSet.map((set,setIndex)=>{
            return (
              <div key={"csl_"+setIndex}>
                <ul style={{padding: "0px 50px"}}>
                  {list.map((item,index)=>{
                    if(index >= set[0] && index <= set[1])
                    return (
                      <li className="competition__slot" key={"cs_"+index}>
                        <img className="competition__status" src={rankStatus[item.group_rank_status] } />
                        <div className="competition__number">{numberFigPosition(index+1,2)}</div>
                        <div className="competition__player">
                          <img className="competition__logo" src={logo_team[item.color||0]} />
                          <span className="competition__name">{item.name}</span>
                        </div>
                        <div className="competition__point">{item.points}</div>
                      </li>
                    )
                  })}
                </ul>
              </div>
            )
          })}
          </Slider>
          <div className="competition__dots">
            <Pagination
              list={pageList}
              page={page}
              pinColor="#8b79a9"
              pinActiveColor="#bda4e5"
              left_img={imgList.ranking_arrow_left}
              right_img={imgList.ranking_arrow_right}
              gotoPage={(p)=>competitionSlide.slickGoTo(p)}
              gotoPrev={()=>competitionSlide.slickPrev()}
              gotoNext={()=>competitionSlide.slickNext()}
            />
          </div>
        </div>
      </CompetitionStyle>
    </F11Layout>
  )
}

const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(CompetitionPage);

const CompetitionStyle= styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: 2082px;
  background-image: url(${imgList.bg_competition});
  /* background-image: url(${imgList.l_competition}); */

  .competition{
    &__section{
      position: relative;
      left: 106px;
      top: 340px;
      display:block;
      width: 896px;
      height: 1480px
    }

    &__slot {
        position: relative;
        display: flex;
        justify-content: flex-start;
        align-items: center;
        width: 100%;
        height: 52px;
    }

    &__status {
      width: 20px;
    }

    &__number {
      width: 60px;
      text-align: center;
    }

    &__player {
      width: 600px;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    &__logo {
      height: 2em;
      vertical-align: middle;
    }

    &__name {
      padding-left: 10px;
    }

    &__point {
      width: 120px;
      text-align: center;
    }

    &__dots {
      position: absolute;
      bottom: 40px;
      left: 0px;
      width: 100%;
    }
  }
`;
