import React,{useState,useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import styled from 'styled-components';
import {imgList} from './../constants/Import_Images';
import Slider from "react-slick";
import F11Layout from './../features/F11Layout';
import Pagination from './../features/Pagination';
import {apiPost} from './../constants/Api';
import {setValues} from './../store/redux';

const HomePage = props => {
  const [rankingSlide,setRankingSlide] = useState(null);
  const [rankingPage,setRankingPage] = useState(0);
  const [colorPage,setColorPage] = useState(1);
  const [pointSlide,setPointSlide] = useState(null);
  const [pointPage,setPointPage] = useState(0);

  const rankingSettings = {
      dots: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
      afterChange: (p)=>setRankingPage(p),
  };
  const pointSettings = {
      dots: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: null,
      prevArrow: null,
      afterChange: (p)=>setPointPage(p),
  };

  const apiEventInfo = () => {
    if(props.jwtToken !== "" && !props.status) {
      apiPost(
        "REACT_APP_API_POST_EVENT_INFO",
        props.jwtToken,
        {type: "event_info"},
        (data)=>{//function successCallback
          props.setValues({
              ...data.data,
              modal_open: (data.data.character_name === ""? "selectcharacter":""),
              // modal_open: "selectcharacter",
          });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message,
          });
        },
      );
    }
  }
  const apiRegister = () => {
    if(props.jwtToken && props.jwtToken !== "") {
      apiPost(
        "REACT_APP_API_POST_REGISTER",
        props.jwtToken,
        {type: "register"},
        (data)=>{//function successCallback
          props.setValues({
              ...data.data,
              modal_open: "message",
              modal_message: "ลงทะเบียนแล้ว",
          });
        },

        (data)=>{
          props.setValues({
              modal_open: "message",
              modal_message:  data.message,
          });
        },//function failCallback\
      );
    }
  }

  useEffect(()=>{ apiEventInfo() },[props.jwtToken]);

  return (
    <F11Layout showUserName={true} showCharacterName={true} page={""}>
      <HomeStyle className="home">
        <section className="home__banner">
          <img src={imgList.banner} alt=""/>
          <div className="home__bannerbtns">
            <Btn  active={props.selected_char&&!props.is_register} registered={props.is_register} onClick={()=>apiRegister()}/>
          </div>
        </section>
        <section className="home__rule">
          <img src={imgList.detail} alt=""/>
        </section>
        <section className="home__ranking">
          <div className="home__rankingslide">
            <Slider key="rankingslide" {...rankingSettings} ref={e=>{if(e)setRankingSlide(e)}}>
              <div><img  className="home__rankingslot" src={imgList.rank1_5}/></div>
              <div><img  className="home__rankingslot" src={imgList.rank6_10}/></div>
              <div><img  className="home__rankingslot" src={imgList.rank11_50}/></div>
              <div><img  className="home__rankingslot" src={imgList.rank51_100}/></div>
            </Slider>
            <div className="home__rankingdots">
              <Pagination
                list={[0,1,2,3]}
                page={rankingPage}
                pinColor="#8b79a9"
                pinActiveColor="#bda4e5"
                left_img={imgList.ranking_arrow_left}
                right_img={imgList.ranking_arrow_right}
                gotoPage={(p)=>rankingSlide.slickGoTo(p)}
                gotoPrev={()=>rankingSlide.slickPrev()}
                gotoNext={()=>rankingSlide.slickNext()}
              />
            </div>
          </div>
        </section>
        <section className="home__color">
          <div className="home__colortabs">
            {[1,2,3].map((item,index)=>{
              return (
                <a
                  key={"ht_"+index}
                  className={"home__colorbtn"+(item===colorPage ? " active":"")}
                  onClick={()=>setColorPage(item)}
                >อันดับ {item}</a>
              )
            })}
          </div>
          <div className="home__colorpage">
            <img className="home__colortitle" src={imgList["home_color_"+colorPage+"_text"]} />
            <img className="home__coloritems" src={imgList["home_color_"+colorPage+"_items"]} />
          </div>
        </section>
        <section className="home__point">
          <div className="home__pointslide">
            <Slider key="pointslide" {...pointSettings} ref={e=>{if(e)setPointSlide(e)}}>
              <div><img  className="home__rankingslot" src={imgList.home_point_set_1}/></div>
              <div><img  className="home__rankingslot" src={imgList.home_point_set_2}/></div>
              <div><img  className="home__rankingslot" src={imgList.home_point_set_3}/></div>
              <div><img  className="home__rankingslot" src={imgList.home_point_set_4}/></div>
              <div><img  className="home__rankingslot" src={imgList.home_point_set_5}/></div>
            </Slider>
            <div className="home__pointdots">
              <Pagination
                list={[0,1,2,3,4]}
                page={pointPage}
                pinColor="#433160"
                pinActiveColor="#8c79ac"
                left_img={imgList.home_point_prev}
                right_img={imgList.home_point_next}
                gotoPage={(p)=>pointSlide.slickGoTo(p)}
                gotoPrev={()=>pointSlide.slickPrev()}
                gotoNext={()=>pointSlide.slickNext()}
              />
            </div>
          </div>
        </section>
      </HomeStyle>
    </F11Layout>
  )
}
const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(HomePage);


const HomeStyle= styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: 4580px;
  background-image: url(${imgList.home_bg});
  /* background-image: url(${imgList.l_home}); */
  background-color: #000000;
  .home {
    &__banner {
      height: 700px;
      img{
        display: block;
      }
      &btns {
        position: absolute;
        top: 550px;
        left: 442px;
      }
    }
    &__rule {
      height: 1000px;
      padding-left: 12px;
      padding-top: 175px;
      background-repeat: no-repeat;
    }
    &__ranking {
      height: 950px;
      background-image: url(${imgList.home_ranking});

      &slide {
        position: relative;
        top: 310px;
        left: 135px;
        width: 830px;
        height: 404px;
      }
      &slot {
        margin: 0 auto;
      }
      &dots {
        margin-top: 30px;
      }
    }
    &__color {
      height: 950px;
      background-image: url(${imgList.home_color});
      &tabs {
        position: relative;
        padding-top: 260px;
        padding-left: 170px;
        transform: rotate(-0.6deg);
      }
      &btn {
        display: inline-block;
        width: 136px;
        height: 50px;
        background-image: url(${imgList.home_color_btn});
        background-position: top center;
        color: #9c9c9c;
        text-align: center;
        line-height: 50px;
        margin-right: 18px;
        user-select: none;
        &.active {
          background-position: bottom center;
        }

      }
      &title{
        display: block;
        margin: 25px auto 0px;
      }
      &items{
        display: block;
        margin: 90px auto 0px;
      }
    }
    &__point {
      height: 980px;
      background-image: url(${imgList.home_point});
      &slide {
        position: relative;
        top: 305px;
        left: 135px;
        width: 830px;
        height: 404px;
      }
      &dots {
        margin-top: 30px;
      }
    }
  }
  section {
    position: relative;
    display: block;
    width: 100%;

  }
`;
const Btn = styled.a`
  display: block;
  width: 222px;
  height: 58px;
  filter: grayscale(${props=>props.active? "0":"1"});
  pointer-events: ${props=>props.active ? "all":"none"};
  background-image: url(${props=>props.registered ? imgList.btn_registered: imgList.btn_register});
`;

const HideBeforeAfter = styled.div`
  &:before,&:after {
    content: "";
  }
`;
function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <HideBeforeAfter
        className={className}
        style={{ ...style, display: "block"}}
        onClick={onClick}
      >
        <img src={imgList.ranking_arrow_right}/>
      </HideBeforeAfter>
    );
}
function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <HideBeforeAfter
          className={className}
          style={{ ...style, display: "block"}}
          onClick={onClick}
        >
            <img src={imgList.ranking_arrow_left}/>
        </HideBeforeAfter>
    );
}
