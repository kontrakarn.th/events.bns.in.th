import React,{useEffect} from 'react';
import {connect} from 'react-redux';
import F11Layout from './../features/F11Layout/';
import {setValues} from './../store/redux';
import styled from 'styled-components';
import {imgList} from './../constants/Import_Images';
import ScrollArea from 'react-scrollbar';
import {apiPost} from './../constants/Api';

const HistoryPage = props => {
  const apiHistory = () => {
    if(props.jwtToken) {
      props.setValues({modal_open: "loading"});
      apiPost(
        "REACT_APP_API_POST_HISTORY",
        props.jwtToken,
        {type: "history_info"},
        (data)=>{//function successCallback
          props.setValues({ ...data.data, modal_open: "" });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message
          });
        },
      );
    }
  };

  useEffect(()=>{ apiHistory(); },[props.jwtToken]);

  return (
    <F11Layout showUserName={true} showCharacterName={true} page={"history"}>
      <HistoryStyle className="history">
        <div className="history__section">
          <ScrollArea
            speed={0.8}
            horizontal={false}
            style={{ width: '100%', height:'100%', opacity:1}}
            verticalScrollbarStyle={{backgroundColor:"#000", opacity:1}}
          >
            <ul style={{padding: "30px 80px"}}>
            {props.quest_history.map((item,index)=>{
              return (
                <li className="history__slot" key={"hs_"+index}>
                  <span className="history__questname">{item.title}</span>
                  <span className="history__questtime">{item.datetime}</span>
                </li>
              )
            })}
            </ul>
          </ScrollArea>
        </div>
      </HistoryStyle>
    </F11Layout>
  )
}

const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(HistoryPage);

const HistoryStyle= styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: 700px;
  background-image: url(${imgList.bg_history});

  .history {
    &__section {
      position: relative;
      left: 120px;
      top: 195px;
      display:flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
      width: 855px;
      height: 390px;
    }
    &__slot {
      display: block;
      width: 100%;
      font-size: 1em;
      line-height: 1.5em;
    }
    &__questname {
      display: inline-block;
      width: 70%;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      vertical-align: middle;
    }
    &__questtime {
      display: inline-block;
      width: 30%;
      text-align: right;
      vertical-align: middle;
    }
  }

`;
