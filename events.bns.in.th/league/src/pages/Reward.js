import React,{useEffect} from 'react';
import {connect} from 'react-redux';
import F11Layout from './../features/F11Layout/';
import {setValues} from './../store/redux';
import styled from 'styled-components';
import {imgList} from './../constants/Import_Images';
import {apiPost} from './../constants/Api';

const RewardPage = props => {
  const apiRewardInfo = () => {
    if(props.jwtToken) {
      props.setValues({modal_open: "loading"});
      apiPost(
        "REACT_APP_API_POST_REWARD_INFO",
        props.jwtToken,
        {type: "reward_info"},
        (data)=>{//function successCallback
          props.setValues({ ...data.data, modal_open: "" });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message
          });
        },
      );
    }
  };
  const apiCraimRankReward = () => {
    if(props.jwtToken) {
      props.setValues({modal_open: "loading"});
      apiPost(
        "REACT_APP_API_POST_CLAIM_RANK_REWARD",
        props.jwtToken,
        {type: "claim_rank_reward"},
        (data)=>{//function successCallback
          props.setValues({
            ...data.data,
            modal_open: "message",
            modal_message: data.message,
          });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message,
          });
        },
      );
    }
  };
  const apiCraimColorReward = () => {
    if(props.jwtToken) {
      props.setValues({modal_open: "loading"});
      apiPost(
        "REACT_APP_API_POST_CLAIM_COLOR_REWARD",
        props.jwtToken,
        {type: "claim_color_reward"},
        (data)=>{//function successCallback
          props.setValues({
            ...data.data,
            modal_open: "message",
            modal_message: data.message
          });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message
          });
        },
      );
    }
  };

  useEffect(()=>{ apiRewardInfo(); },[props.jwtToken]);

  let colorReward = props.color_reward.rank;
  let count = Math.max(props.rank_reward.rank,1);
  let setRankReward = imgList.rank1_5;
  if(count > 5 ) setRankReward = imgList.rank6_10;
  if(count > 10 ) setRankReward = imgList.rank11_50;
  if(count > 50 ) setRankReward = imgList.rank51_100;
  return (
    <F11Layout showUserName={true} showCharacterName={true} page={"reward"}>
      <RewardStyle className="reward" color_win={colorReward >= 1 && colorReward <= 3}>
        <div className="reward__section reward__section--1">
          <div className="reward__ranking"><img src={setRankReward} /></div>
          <div className="reward__rankbtn">
            <BtnReward
              canReceive={props.rank_reward.can_claim}
              received={props.rank_reward.claimed}
              onClick={()=>apiCraimRankReward()}
            />
          </div>
        </div>
        <div className="reward__section reward__section--2">
          <img className="reward__colortitle" src={imgList["home_color_"+colorReward+"_text"]} />
          <img className="reward__coloritems" src={imgList["home_color_"+colorReward+"_items"]} />
          <div className="reward__rankbtn">
            <BtnReward
              canReceive={props.color_reward.can_claim}
              received={props.color_reward.claimed}
              onClick={()=>apiCraimColorReward()}
            />
          </div>
        </div>

      </RewardStyle>
    </F11Layout>
  )
}

const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(RewardPage);

const RewardStyle= styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: ${props=>props.color_win ? "1837":"1030"}px;
  background-image: url(${imgList.bg_reward});
  overflow: hidden;
  .reward {
    &__section {
      position: relative;
      width: 100%;
      padding-left: 135px;
      &--1 {
        height: 1030px
        padding-top: 315px;
      }
      &--2 {
        height: 807px
        padding-top: 530px;
      }
    }
    &__ranking {
      position: relative;
      display: flex;
      justify-content: center;
      align-items: center;
      width: 830px;
      height: 400px;
    }
    &__rankbtn {
      display: flex;
      justify-content: center;
      align-items: center;
      width: 830px;
      height: 100px;
    }
    &__colortitle {
      position: absolute;
      top: 220px;
      left: 50%;
      transform: translate(-50%, 0px);
    }
    &__coloritems {
      position: absolute;
      top: 334px;
      left: 50%;
      transform: translate(-50%, 0px);
    }
  }
`;
const BtnReward= styled.a`
  position: absolute;
  display: block;
  width: 222px;
  height: 58px;
  filter: grayscale(${props=>props.canReceive? "0":"1"});
  pointer-events: ${props=>props.canReceive? "all":"none"};
  background-image: url(${props=>props.received? imgList.btn_received : imgList.btn_receive});
`;
