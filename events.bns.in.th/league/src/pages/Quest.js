import React,{useEffect} from 'react';
import {connect} from 'react-redux';
import F11Layout from './../features/F11Layout/';
import {setValues} from './../store/redux';
import styled from 'styled-components';
import {imgList} from './../constants/Import_Images';
import {apiPost} from './../constants/Api';

const buttonPosition = {
  d0: "top: 495px; left: 246px;",
  d1: "top: 495px; left: 495px;",
  d2: "top: 495px; left: 741px;",
  d3: "top: 717px; left: 246px;",
  d4: "top: 717px; left: 495px;",
  d5: "top: 717px; left: 741px;",

  q0: "top: 296px; left: 212px;",
  q1: "top: 296px; left: 442px;",
  q2: "top: 296px; left: 669px;",
  q3: "top: 456px; left: 212px;",
  q4: "top: 456px; left: 442px;",
  s0: "top: 670px; left: 444px;",

  r0: "top: 695px; left: 444px;",
  r1: "top: 1202px; left: 444px;",
  r2: "top: 1554px; left: 444px;",
  r3: "top: 1913px; left: 444px;",
  r4: "top: 2276px; left: 444px;",
}

// REACT_APP_API_POST_CLEAIM_PERSONAL_REWARD=/api/league//claim_personal_reward
const QuestPage = props => {
  let {
    color_id,
    colorList,
    special_quest_daily,
    free_daily_points,
    is_select_special_quest,
    member_special_quest_daily,
    personal_reward,
    quest_daily_list
  } = props;

  let color = colorList[color_id];
  let specialId = member_special_quest_daily.id || 0;

  const apiPersonalInfo = () => {
    props.setValues({modal_open: "loading"});
    apiPost(
      "REACT_APP_API_POST_PERSONAL_INFO",
      props.jwtToken,
      {type: "personal_info"},
      (data)=>{//function successCallback
        props.setValues({ ...data.data, modal_open: "" });
      },
      (data)=>{//function failCallback
        props.setValues({
          modal_open: "message",
          modal_message: data.message
        });
      },
    );
  }
  const apiClaimFreePoints = () => {
    props.setValues({modal_open: "loading"});
    apiPost(
      "REACT_APP_API_POST_CLAIM_FREE_POINTS",
      props.jwtToken,
      {type: "claim_free_points"},
      (data)=>{//function successCallback
        props.setValues({
          ...data.data,
          modal_open: "message",
          modal_message: data.message||"รับคะแนนแล้ว",
        });
      },
      (data)=>{//function failCallback
        props.setValues({
          modal_open: "message",
          modal_message: data.message,
        });
      },
    );
  }
  const apiClaimQuestPoints = (id) => {
    props.setValues({modal_open: "loading"});
    apiPost(
      "REACT_APP_API_POST_CLAIM_QUEST_POINTS",
      props.jwtToken,
      {type: "claim_quest_points",id},
      (data)=>{//function successCallback
        props.setValues({
          ...data.data,
          modal_open: "message",
          modal_message: data.message||"done",
        });
      },
      (data)=>{//function failCallback
        props.setValues({
          modal_open: "message",
          modal_message: data.message,
        });
      },
    );
  }
  const actSelectSpecialQuest = (id) => {
    if(!props.is_select_special_quest){
      props.setValues({
        modal_open: "confirm",
        modal_select: id,
        modal_message: "ต้องการเลือกทำภารกิจนี้ ?<br/>(ไม่สามารถเปลี่ยนแปลงได้ในภายหลัง)",
      });
    }
  }
  const actClaimSpecialQuestPoints = (id) => {
    props.setValues({modal_open: "loading"});
    apiPost(
      "REACT_APP_API_POST_CLEAIM_SPECIAL_QUEST_POINTS",
      props.jwtToken,
      {type: "claim_special_quest_points",id},
      (data)=>{//function successCallback
        props.setValues({
          ...data.data,
          modal_open: "message",
          modal_message: data.message||"done",
        });
      },
      (data)=>{//function failCallback
        props.setValues({
          modal_open: "message",
          modal_message: data.message,
        });
      },
    );
  }
  const actClaimPersonalReward = (id) => {
    props.setValues({modal_open: "loading"});
    apiPost(
      "REACT_APP_API_POST_CLEAIM_PERSONAL_REWARD",
      props.jwtToken,
      {type: "claim_personal_reward",id},
      (data)=>{//function successCallback
        props.setValues({
          ...data.data,
          modal_open: "message",
          modal_message: data.message||"done",
        });
      },
      (data)=>{//function failCallback
        props.setValues({
          modal_open: "message",
          modal_message: data.message,
        });
      },
    );
  }

  useEffect(()=>{ apiPersonalInfo(); },[props.jwtToken]);

  return (
    <F11Layout showUserName={true} showCharacterName={true} page={"quest"}>
      <FinScore>คะแนนรวมทั้งหมด<br/>{props.points} คะแนน</FinScore>
      <QuestStyle className="quest" color={color}>
        <section className="quest__banner">
          <div className="quest__name">{props.character_name}</div>
        </section>
        <section className="quest__daily">
          <BtnDaily set="d0" color={color} canReceive={free_daily_points.can_claim||false} received={free_daily_points.claimed||false} onClick={()=>apiClaimFreePoints()} />
          {quest_daily_list.map((item,index)=>{
            return (
              <BtnDaily
                set={"d"+(index+1)}
                color={color}
                canReceive={item.can_claim}
                received={item.claimed}
                onClick={()=>apiClaimQuestPoints(item.id)}
              />
            )
          })}
        </section>
        <section className="quest__specail">
          {special_quest_daily.map((item,index)=>{
            return (
              <SlotSpecail
                set={index}
                color={color}
                active={!is_select_special_quest || specialId === item.id}
                onClick={()=>actSelectSpecialQuest(item.id)}
              />
            )
          })}
          {is_select_special_quest &&
            <span className="quest__count" >ทำภารกิจไปแล้ว {member_special_quest_daily.remain_count} ครั้ง/ได้รับ {member_special_quest_daily.remain_points} คะแนน</span>
          }
          <BtnSpecail
            set="s0"
            color={color}
            canReceive={member_special_quest_daily.can_claim}
            received={!member_special_quest_daily.can_claim}
            onClick={()=>actClaimSpecialQuestPoints(member_special_quest_daily.id)}
          />
        </section>
        <section className="quest__reward">
          {personal_reward.map((item,index)=>{
            return (
              <BtnReward
                set={"r"+(4-index)}
                color={color}
                canReceive={item.can_claim}
                received={item.claimed}
                onClick={()=>actClaimPersonalReward(item.id)}
              />
            )
          })}
        </section>
      </QuestStyle>
    </F11Layout>
  )
}

const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(QuestPage);

const QuestStyle= styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: 5254px;
  background-image: url(${props=>imgList["bg_"+props.color]});
  /* background-image: url(${imgList.l_quest}); */

  .quest {
    &__banner {
      height: 700px;
    }
    &__name {
      text-align: center;
      font-size: 20px;
      color: #FFFFFF;
      position: absolute;
      top: 380px;
      right: 160px;
      display: block;
      width: 370px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    &__daily {
      height: 1000px;
    }
    &__specail {
      height: 940px;
    }
    &__reward {
      height: 2614px;
    }
    &__count {
      position: absolute;
      top: 630px;
      display: block;
      width: 100%;
      text-align: center;
      color: #ad621f;
    }
  }
  section {
    position: relative;
    display: block;
    width: 100%;
  }
`;
const BtnDaily= styled.a`
  position: absolute;
  ${props=>buttonPosition[props.set]}
  display: block;
  width: 108px;
  height: 41px;
  filter: grayscale(${props=>props.canReceive? "0":"1"});
  pointer-events: ${props=>props.canReceive? "all":"none"};
  background-image: url(${props=>props.received? imgList.btn_point_received : imgList["btn_point_receive_"+props.color]});
`;
const SlotSpecail = styled.a`
  position: absolute;
  ${props=>buttonPosition["q"+props.set]}
  display: block;
  width: 220px;
  height: 150px;
  filter: grayscale(${props=>props.active? "0":"1"});
  pointer-events: ${props=>props.canReceive? "none":"all"};
  background-image: url(${props=>imgList["quest_specail_"+props.set+"_"+props.color]});
  background-position: top center;
  background-repeat: no-repeat;
`;
const BtnSpecail = styled.a`
  position: absolute;
  ${props=>buttonPosition[props.set]}
  display: block;
  width: 222px;
  height: 58px;
  filter: grayscale(${props=>props.canReceive? "0":"1"});
  pointer-events: ${props=>props.canReceive? "all":"none"};
  /* background-image: url(${props=>props.received? imgList.btn_point_2_received : imgList["btn_point_2_receive_"+props.color]}); */
  background-image: url(${props=>imgList["btn_point_2_receive_"+props.color]});
`;
const BtnReward= styled.a`
  position: absolute;
  ${props=>buttonPosition[props.set]}
  display: block;
  width: 222px;
  height: 58px;
  filter: grayscale(${props=>props.canReceive? "0":"1"});
  pointer-events: ${props=>props.canReceive? "all":"none"};
  /* background-image: url(${props=>props.received? imgList.btn_reward_received : imgList["btn_reward_receive_"+props.color]}); */
  ${props=>!props.canReceive && "background-image: url("+imgList.btn_reward_inactive+");"}
  ${props=>props.canReceive && "background-image: url("+imgList["btn_reward_receive_"+props.color]+");"}
  ${props=>props.received && "background-image: url("+imgList.btn_reward_received+");"}
`;
const FinScore = styled.div`
color: #FFFFFF;
  z-index: 1;
  position: fixed;
  top: 500px;
  right: 0px;
  display: block;
  width: 154px;
  height: 72px;
  pointer-events: "none";
  background-image: url(${imgList.fin_score});
  background-repeat: no-repeat;
  padding-top: 18px;
  padding-left: 15px;
  line-height: 1em;
`;
