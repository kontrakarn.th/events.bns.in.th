import React,{useEffect} from 'react';
import {connect} from 'react-redux';
import F11Layout from './../features/F11Layout/';
import {setValues} from './../store/redux';
import styled from 'styled-components';
import {imgList} from './../constants/Import_Images';
import {apiPost} from './../constants/Api';

const numberWithCommas = x => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

const RankingPage = props => {
  let {color_list} = props;
  const list = [
    {name: "#ทีมจูเลีย",     logo: imgList.icon_team_green,  font_color:"#01b473",  bar_color: "#56ffb1"},
    {name: "#ทีมจินซอยอน",  logo: imgList.icon_team_yellow, font_color:"#b6870b",  bar_color: "#ffe932"},
    {name: "#ทีมจินโซอา",   logo: imgList.icon_team_blue,   font_color:"#4788ff",  bar_color: "#4788ff"},
    {name: "#ทีมโพฮวารัน",  logo: imgList.icon_team_red,    font_color:"#ff6147",  bar_color: "#ff6147"},
    {name: "#ทีมนัมโซยู",    logo: imgList.icon_team_pink,   font_color:"#ff47ab",  bar_color: "#ff46ab"},
  ]
  const apiColorRankInfo = () => {
    if(props.jwtToken) {
      props.setValues({modal_open: "loading"});
      apiPost(
        "REACT_APP_API_POST_COLOR_RANK_INFO",
        props.jwtToken,
        {type: "color_rank_info"},
        (data)=>{//function successCallback
          props.setValues({ ...data.data, modal_open: "" });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message
          });
        },
      );
    }
  }

  useEffect(()=>{ apiColorRankInfo(); },[props.jwtToken]);

  let maxPoint = Math.max(
    color_list[0].points_full,
    color_list[1].points_full,
    color_list[2].points_full,
    color_list[3].points_full,
    color_list[4].points_full,
  )
  
  return (
    <F11Layout showUserName={true} showCharacterName={true} page={"ranking"}>
      <RankingStyle className="ranking">
        <ul className="ranking__section">
          {color_list.map((item,index)=>{
            let precent = 15 + Math.round(item.points_full/maxPoint*85);
            let name = item.top_player_name!== "" ? item.top_player_name:"-";
            return (
              <li className="ranking__slot" key={"rs_"+index}>
                <img className="ranking__icon" src={list[index].logo} />
                <div className="ranking__score">
                  <RankingBarStyle
                    precent={precent}
                    color={list[index].bar_color}
                    top={item.points_full == maxPoint}
                  >{item.points_shot}</RankingBarStyle>
                </div>
                <div className="ranking__top">
                  <div><span style={{color: list[index].font_color}}>{item.title}</span> คะแนนรวม {numberWithCommas(item.points_full)}</div>
                  <div>นำทัพโดย {name} คะแนน {item.top_player_points}</div>
                </div>
              </li>
            )
          })}

        </ul>
      </RankingStyle>
    </F11Layout>
  )
}

const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(RankingPage);

const RankingStyle= styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: 700px;
  background-image: url(${imgList.bg_ranking});

  .ranking {
    &__section {
      position: relative;
      left: 120px;
      top: 195px;
      display:flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
      width: 855px;
      height: 390px;
    }
    &__slot {
      display: flex;
      justify-content: flex-start;
      align-items: center;
      width: 810px;
      height: 70px;
    }
    &__icon {
      height: 45px;
      margin: 5px;
    }
    &__score {
      height: 35px;
      width: 380px;
      margin-left: 20px;
      line-height: 35px;
      font-weight: bolder;
      font-style: italic;
    }
    &__top {
      width: 310px;
      margin-left: 35px;

      span {
          // team name
          font-weight: bolder;
      }
    }
  }
`;
const RankingSlotStyle= styled.div`
`;
const RankingBarStyle= styled.div`
  position: relative;
  display: block;
  width: ${props=>props.precent}%;
  min-width: fit-content;
  height: 100%;
  background-color: ${props=>props.color};
  padding: 0 10px;
  &:after {
    position: absolute;
    top: 0px;
    right: 0px;
    transform: translate(50%,-75%);
    display: ${props=>props.top?"block":"none"};
    width: 32px;
    height: 30px;
    content: " ";
    background-image: url(${imgList.icon_crown});
  }
`;
