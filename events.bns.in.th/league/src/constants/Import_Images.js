
// import bg_permission				from '../static/images/f11/permission.jpg';
// import menu_bg							from '../static/images/f11/menu_bg.jpg';
// import icon_scroll					from '../static/images/f11/icon_scroll.png';
// import btn_home							from '../static/images/f11/btn_home.png';
// import menu_special_btn			from '../static/images/menu_special_btn.jpg';
// import homeo_bg 						from '../static/images/home_bg.jpg';
// import rank1_5							from '../static/images/rank1_5.png';
// import rank6_10							from '../static/images/rank6_10.png';
// import rank11_50						from '../static/images/rank11_50.png';
// import rank51_100						from '../static/images/rank51_100.png';
// import ranking_arrow_left		from '../static/images/ranking_arrow_left.png';
// import ranking_arrow_right	from '../static/images/ranking_arrow_right.png';
//
// // main page
// import banner 							from '../static/images/home_banner.jpg';
// import detail 							from '../static/images/detail.png';
// import home_bg 			  			from '../static/images/home_bg.jpg';
// import home_ranking 			  from '../static/images/home_ranking.png';
// import home_color 				  from '../static/images/home_color.png';
// import home_point 				  from '../static/images/home_point.png';
// import home_color_btn 			from '../static/images/home_color_btn.png';
// import home_color_1_text 	 	from '../static/images/home_color_1_text.png';
// import home_color_1_items 	from '../static/images/home_color_1_items.png';
// import home_color_2_text 	 	from '../static/images/home_color_2_text.png';
// import home_color_2_items 	from '../static/images/home_color_2_items.png';
// import home_color_3_text 	 	from '../static/images/home_color_3_text.png';
// import home_color_3_items 	from '../static/images/home_color_3_items.png';
// import home_point_next		 	from '../static/images/home_point_next.png';
// import home_point_prev		 	from '../static/images/home_point_prev.png';
// import home_point_set_1		 	from '../static/images/home_point_set_1.png';
// import home_point_set_2		 	from '../static/images/home_point_set_2.png';
// import home_point_set_3		 	from '../static/images/home_point_set_3.png';
// import home_point_set_4		 	from '../static/images/home_point_set_4.png';
// import home_point_set_5		 	from '../static/images/home_point_set_5.png';
// import btn_register		 			from '../static/images/btn_register.png';
// import btn_registered		 		from '../static/images/btn_registered.png';
// import fin_score		 				from '../static/images/fin_score.png';
//
// // competition
// import bg_competition				from '../static/images/bg_competition.jpg';
// import icon_next_arrow			from '../static/images/icon_next_arrow.png';
// import icon_prev_arrow			from '../static/images/icon_prev_arrow.png';
// import icon_rank_down				from '../static/images/icon_rank_down.png';
// import icon_rank_up					from '../static/images/icon_rank_up.png';
//
// // quest
// import btn_point_receive_green		from '../static/images/btn_point_receive_green.png';
// import btn_point_2_receive_green	from '../static/images/btn_point_2_receive_green.png';
// import btn_reward_receive_green		from '../static/images/btn_reward_receive_green.png';
// import btn_point_receive_yellow		from '../static/images/btn_point_receive_yellow.png';
// import btn_point_2_receive_yellow	from '../static/images/btn_point_2_receive_yellow.png';
// import btn_reward_receive_yellow	from '../static/images/btn_reward_receive_yellow.png';
// import btn_point_receive_blue			from '../static/images/btn_point_receive_blue.png';
// import btn_point_2_receive_blue		from '../static/images/btn_point_2_receive_blue.png';
// import btn_reward_receive_blue		from '../static/images/btn_reward_receive_blue.png';
// import btn_point_receive_red			from '../static/images/btn_point_receive_red.png';
// import btn_point_2_receive_red		from '../static/images/btn_point_2_receive_red.png';
// import btn_reward_receive_red			from '../static/images/btn_reward_receive_red.png';
// import btn_point_receive_pink			from '../static/images/btn_point_receive_pink.png';
// import btn_point_2_receive_pink		from '../static/images/btn_point_2_receive_pink.png';
// import btn_reward_receive_pink		from '../static/images/btn_reward_receive_pink.png';
// import btn_point_2_received				from '../static/images/btn_point_2_received.png';
// import btn_point_received					from '../static/images/btn_point_received.png';
// import btn_reward_received				from '../static/images/btn_reward_received.png';
// import btn_reward_inactive				from '../static/images/btn_reward_inactive.png';
// import bg_green										from '../static/images/bg_green.jpg';
// import bg_yellow									from '../static/images/bg_yellow.jpg';
// import bg_blue										from '../static/images/bg_blue.jpg';
// import bg_red											from '../static/images/bg_red.jpg';
// import bg_pink										from '../static/images/bg_pink.jpg';
// import quest_specail_0_green		  from '../static/images/quest_specail_0_green.png';
// import quest_specail_1_green		  from '../static/images/quest_specail_1_green.png';
// import quest_specail_2_green		  from '../static/images/quest_specail_2_green.png';
// import quest_specail_3_green		  from '../static/images/quest_specail_3_green.png';
// import quest_specail_4_green		  from '../static/images/quest_specail_4_green.png';
// import quest_specail_0_yellow		  from '../static/images/quest_specail_0_yellow.png';
// import quest_specail_1_yellow		  from '../static/images/quest_specail_1_yellow.png';
// import quest_specail_2_yellow		  from '../static/images/quest_specail_2_yellow.png';
// import quest_specail_3_yellow		  from '../static/images/quest_specail_3_yellow.png';
// import quest_specail_4_yellow		  from '../static/images/quest_specail_4_yellow.png';
// import quest_specail_0_blue		  	from '../static/images/quest_specail_0_blue.png';
// import quest_specail_1_blue		  	from '../static/images/quest_specail_1_blue.png';
// import quest_specail_2_blue		  	from '../static/images/quest_specail_2_blue.png';
// import quest_specail_3_blue		  	from '../static/images/quest_specail_3_blue.png';
// import quest_specail_4_blue		  	from '../static/images/quest_specail_4_blue.png';
// import quest_specail_0_red			  from '../static/images/quest_specail_0_red.png';
// import quest_specail_1_red			  from '../static/images/quest_specail_1_red.png';
// import quest_specail_2_red			  from '../static/images/quest_specail_2_red.png';
// import quest_specail_3_red			  from '../static/images/quest_specail_3_red.png';
// import quest_specail_4_red			  from '../static/images/quest_specail_4_red.png';
// import quest_specail_0_pink			  from '../static/images/quest_specail_0_pink.png';
// import quest_specail_1_pink			  from '../static/images/quest_specail_1_pink.png';
// import quest_specail_2_pink			  from '../static/images/quest_specail_2_pink.png';
// import quest_specail_3_pink			  from '../static/images/quest_specail_3_pink.png';
// import quest_specail_4_pink			  from '../static/images/quest_specail_4_pink.png';
//
// // ranking
// import icon_crown						from '../static/images/icon_crown.png';
// import bg_ranking						from '../static/images/bg_ranking.jpg';
// import icon_team_yellow			from '../static/images/icon_team_yellow.png';
// import icon_team_blue				from '../static/images/icon_team_blue.png';
// import icon_team_green			from '../static/images/icon_team_green.png';
// import icon_team_pink				from '../static/images/icon_team_pink.png';
// import icon_team_red				from '../static/images/icon_team_red.png';
//
// // history
// import bg_history						from '../static/images/bg_history.jpg';
//
// // reward
// import bg_reward						from '../static/images/bg_reward.jpg';
// import btn_receive					from '../static/images/btn_receive.png';
// import btn_received					from '../static/images/btn_received.png';
//
// // modal
// import btn_confirm					from '../static/images/btn_confirm.png';
// import btn_cancel						from '../static/images/btn_cancel.png';
// import popup_confirm				from '../static/images/popup_confirm.png';
// import popup_message				from '../static/images/popup_message.png';
// import popup_select					from '../static/images/popup_select.png';
// import icon_dropdown				from '../static/images/icon_dropdown.png';
//==============================================================================================================

const bg_permission = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/f11/permission.jpg';
const menu_bg = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/f11/menu_bg.jpg';
const icon_scroll = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/f11/icon_scroll.png';
const btn_home = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/f11/btn_home.png';
const menu_special_btn = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/menu_special_btn.jpg';
const homeo_bg = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_bg.jpg';
const rank1_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/rank1_5.png';
const rank6_10 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/rank6_10.png';
const rank11_50 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/rank11_50.png';
const rank51_100 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/rank51_100.png';
const ranking_arrow_left = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/ranking_arrow_left.png';
const ranking_arrow_right = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/ranking_arrow_right.png';

// main page
const banner = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_banner.jpg';
const detail = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/detail.png';
const home_bg = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_bg.jpg';
const home_ranking = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_ranking.png';
const home_color = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_color.png';
const home_point = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_point.png';
const home_color_btn = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_color_btn.png';
const home_color_1_text = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_color_1_text.png';
const home_color_1_items = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_color_1_items.png';
const home_color_2_text = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_color_2_text.png';
const home_color_2_items = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_color_2_items.png';
const home_color_3_text = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_color_3_text.png';
const home_color_3_items = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_color_3_items.png';
const home_point_next = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_point_next.png';
const home_point_prev = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_point_prev.png';
const home_point_set_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_point_set_1.png';
const home_point_set_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_point_set_2.png';
const home_point_set_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_point_set_3.png';
const home_point_set_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_point_set_4.png';
const home_point_set_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/home_point_set_5.png';
const btn_register = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_register.png';
const btn_registered = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_registered.png';
const fin_score = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/fin_score.png';

// competition
const bg_competition = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/bg_competition.jpg';
const icon_next_arrow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/icon_next_arrow.png';
const icon_prev_arrow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/icon_prev_arrow.png';
const icon_rank_down = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/icon_rank_down.png';
const icon_rank_up = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/icon_rank_up.png';

// quest
const btn_point_receive_green = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_point_receive_green.png';
const btn_point_2_receive_green = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_point_2_receive_green.png';
const btn_reward_receive_green = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_reward_receive_green.png';
const btn_point_receive_yellow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_point_receive_yellow.png';
const btn_point_2_receive_yellow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_point_2_receive_yellow.png';
const btn_reward_receive_yellow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_reward_receive_yellow.png';
const btn_point_receive_blue = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_point_receive_blue.png';
const btn_point_2_receive_blue = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_point_2_receive_blue.png';
const btn_reward_receive_blue = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_reward_receive_blue.png';
const btn_point_receive_red = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_point_receive_red.png';
const btn_point_2_receive_red = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_point_2_receive_red.png';
const btn_reward_receive_red = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_reward_receive_red.png';
const btn_point_receive_pink = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_point_receive_pink.png';
const btn_point_2_receive_pink = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_point_2_receive_pink.png';
const btn_reward_receive_pink = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_reward_receive_pink.png';
const btn_point_2_received = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_point_2_received.png';
const btn_point_received = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_point_received.png';
const btn_reward_received = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_reward_received.png';
const btn_reward_inactive = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_reward_inactive.png';
const bg_green = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/bg_green.jpg';
const bg_yellow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/bg_yellow.jpg';
const bg_blue = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/bg_blue.jpg';
const bg_red = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/bg_red.jpg';
const bg_pink = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/bg_pink.jpg';
const quest_specail_0_green = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_0_green.png';
const quest_specail_1_green = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_1_green.png';
const quest_specail_2_green = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_2_green.png';
const quest_specail_3_green = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_3_green.png';
const quest_specail_4_green = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_4_green.png';
const quest_specail_0_yellow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_0_yellow.png';
const quest_specail_1_yellow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_1_yellow.png';
const quest_specail_2_yellow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_2_yellow.png';
const quest_specail_3_yellow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_3_yellow.png';
const quest_specail_4_yellow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_4_yellow.png';
const quest_specail_0_blue = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_0_blue.png';
const quest_specail_1_blue = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_1_blue.png';
const quest_specail_2_blue = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_2_blue.png';
const quest_specail_3_blue = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_3_blue.png';
const quest_specail_4_blue = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_4_blue.png';
const quest_specail_0_red = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_0_red.png';
const quest_specail_1_red = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_1_red.png';
const quest_specail_2_red = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_2_red.png';
const quest_specail_3_red = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_3_red.png';
const quest_specail_4_red = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_4_red.png';
const quest_specail_0_pink = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_0_pink.png';
const quest_specail_1_pink = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_1_pink.png';
const quest_specail_2_pink = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_2_pink.png';
const quest_specail_3_pink = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_3_pink.png';
const quest_specail_4_pink = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/quest_specail_4_pink.png';

// ranking
const icon_crown = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/icon_crown.png';
const bg_ranking = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/bg_ranking.jpg';
const icon_team_yellow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/icon_team_yellow.png';
const icon_team_blue = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/icon_team_blue.png';
const icon_team_green = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/icon_team_green.png';
const icon_team_pink = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/icon_team_pink.png';
const icon_team_red = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/icon_team_red.png';

// history
const bg_history = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/bg_history.jpg';

// reward
const bg_reward = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/bg_reward.jpg';
const btn_receive = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_receive.png';
const btn_received = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_received.png';

// modal
const btn_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_confirm.png';
const btn_cancel = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_cancel.png';
const popup_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/popup_confirm.png';
const popup_message = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/popup_message.png';
const popup_select = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/popup_select.png';
const icon_dropdown = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/icon_dropdown.png';

export const imgList = {

	bg_permission,
	menu_bg,
	icon_scroll,
	btn_home,
	menu_special_btn,
	homeo_bg,
	detail,
	rank1_5,
	rank6_10,
	rank11_50,
	rank51_100,
	ranking_arrow_left,
	ranking_arrow_right,
	fin_score,

	banner,
	home_bg,
	home_ranking,
	home_color,
	home_point,
	home_color_btn,
	home_color_1_text,
	home_color_1_items,
	home_color_2_text,
	home_color_2_items,
	home_color_3_text,
	home_color_3_items,
	home_point_next,
	home_point_prev,
	home_point_set_1,
	home_point_set_2,
	home_point_set_3,
	home_point_set_4,
	home_point_set_5,
	btn_register,
	btn_registered,

	//ranking
	icon_crown,
	bg_ranking,
	icon_team_yellow,
	icon_team_blue,
	icon_team_green,
	icon_team_pink,
	icon_team_red,

	//history
	bg_history,

	//competition
	bg_competition,
	icon_next_arrow,
	icon_prev_arrow,
	icon_rank_down,
	icon_rank_up,

	//quest
	btn_point_receive_green,
	btn_point_2_receive_green,
	btn_reward_receive_green,
	btn_point_receive_yellow,
	btn_point_2_receive_yellow,
	btn_reward_receive_yellow,
	btn_point_receive_blue,
	btn_point_2_receive_blue,
	btn_reward_receive_blue,
	btn_point_receive_red,
	btn_point_2_receive_red,
	btn_reward_receive_red,
	btn_point_receive_pink,
	btn_point_2_receive_pink,
	btn_reward_receive_pink,
	btn_point_received,
	btn_point_2_received,
	btn_reward_received,
	btn_reward_inactive,
	bg_green,
	bg_yellow,
	bg_blue,
	bg_red,
	bg_pink,
	quest_specail_0_green,
	quest_specail_1_green,
	quest_specail_2_green,
	quest_specail_3_green,
	quest_specail_4_green,
	quest_specail_0_yellow,
	quest_specail_1_yellow,
	quest_specail_2_yellow,
	quest_specail_3_yellow,
	quest_specail_4_yellow,
	quest_specail_0_blue,
	quest_specail_1_blue,
	quest_specail_2_blue,
	quest_specail_3_blue,
	quest_specail_4_blue,
	quest_specail_0_red,
	quest_specail_1_red,
	quest_specail_2_red,
	quest_specail_3_red,
	quest_specail_4_red,
	quest_specail_0_pink,
	quest_specail_1_pink,
	quest_specail_2_pink,
	quest_specail_3_pink,
	quest_specail_4_pink,

	//reward
	bg_reward,
	btn_receive,
	btn_received,

	//popup
	btn_confirm,
	btn_cancel,
	popup_confirm,
	popup_message,
	popup_select,
	icon_dropdown,
}
