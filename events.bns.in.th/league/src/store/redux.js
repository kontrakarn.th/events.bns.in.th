const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    showCharacterName: true,
    characters:[],
    username: "",
    character_name: "",
    coin: 0,
    selected_char: false,
    loginStatus: false,
    showScroll: true,
    noScroll: true,
    loading: false,
    permission: true,
    status: false,
    menu: [
      {title: "หน้าหลัก",          link:process.env.REACT_APP_EVENT_PATH },
      {title: "การแข่งขัน",        link:process.env.REACT_APP_EVENT_PATH+"/competition" },
      {title: "ภารกิจสะสมคะแนน",  link:process.env.REACT_APP_EVENT_PATH+"/quest" },
      {title: "อันดับปัจจุบัน",       link:process.env.REACT_APP_EVENT_PATH+"/ranking" },
      {title: "ประวัติทำภารกิจ",    link:process.env.REACT_APP_EVENT_PATH+"/history" },
      // {title: "รับของรางวัลพิเศษ",  link:"reward",      value: "check"},
    ],
    colorList:[
        "",
        "green",
        "yellow",
        "blue",
        "red",
        "pink"
    ],
    //===modal
    modal_open: "",//"loading","selectcharacter","confirm"
    modal_message: "",
    modal_item: "",
    modal_select: "",

    //=== EVENT ===//
    open_claim_reward: false,
    open_competition: false,
    is_register: false,
    group: 0,
    color: 0,
    rank_list: [],
    color_title: "",
    color_id: 0,
    color_npc: "",
    color_rank: 0,
    group: 0,
    free_daily_points: {can_claim: true, claimed: false},
    quest_daily_list: [
      {id: 2, can_claim: false, claimed: false},
      {id: 3, can_claim: false, claimed: false},
      {id: 4, can_claim: false, claimed: false},
      {id: 5, can_claim: false, claimed: false},
      {id: 6, can_claim: false, claimed: false},
    ],
    special_quest_daily: [
      {id: 7},
      {id: 8},
      {id: 9},
      {id: 10},
      {id: 11},
    ],
    member_special_quest_daily: {
      id: "",
      title: "",
      dungeon: "",
      points: 1,
      can_claim: false,
      total_count: 0,
      total_points: 0,
      remain_count: 0,
      remain_points: 0,
    },
    is_select_special_quest: false,
    personal_reward: [
      {id: 1, can_claim: false, claimed: false},
      {id: 2, can_claim: false, claimed: false},
      {id: 3, can_claim: false, claimed: false},
      {id: 4, can_claim: false, claimed: false},
      {id: 5, can_claim: false, claimed: false},
    ],
    color_list: [
      {id: 1, title: "", npc: "", rank: 1, points_shot: "0k", points_full: 0,top_player_name: "[GmTH] SF Event Test"},
      {id: 2, title: "", npc: "", rank: 1, points_shot: "0k", points_full: 0,top_player_name: "[GmTH] SF Event Test"},
      {id: 3, title: "", npc: "", rank: 1, points_shot: "0k", points_full: 0,top_player_name: "[GmTH] SF Event Test"},
      {id: 4, title: "", npc: "", rank: 1, points_shot: "0k", points_full: 0,top_player_name: "[GmTH] SF Event Test"},
      {id: 5, title: "", npc: "", rank: 1, points_shot: "0k", points_full: 0,top_player_name: "[GmTH] SF Event Test"}
    ],
    quest_history: [],
    rank_reward: {
      can_claim: false,
      claimed: false,
      id: 0,
      title: "",
      amount: 0,
      rank: 0,
    },
    color_reward: {
      can_claim: false,
      claimed: false,
      id: 0,
      title: "",
      amount: 0,
      rank: 0,
    },
}


export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         : return ({ ...state, sessionKey: action.value });
        case 'ACCOUNT_AUTHED'       : return ({ ...state, userData: action.value });
        case 'RECEIVE_SESSION_KEY'  : return ({ ...state, sessionKey: action.value });
        case 'SET_LOGIN_URL'        : return ({ ...state, loginUrl: action.value });
        case 'SET_LOGOUT_URL'       : return ({ ...state, logoutUrl: action.value });
        case 'ACCOUNT_LOGIN'        : return ({ ...state, authState: "LOGGED_IN" });
        case 'ACCOUNT_LOGOUT'       : return ({ ...state, authState: "LOGGED_OUT" });
        case 'ACCOUNT_AUTH_CHECKED' : return ({ ...state, authed: true });
        case 'SET_JWT_TOKEN'        : return ({ ...state, jwtToken: action.value });

        case "SET_VALUE"            : return (Object.keys(defaultState).indexOf(action.key) >= 0) ? { ...state, [action.key]: action.value } : state;
        case "SET_VALUES"           : return ({ ...state, ...action.value });
        default                     : return state;
    }
}

export const onAccountAuth          = (sessionKey)  => ({ type: 'ACCOUNT_AUTH',         value: sessionKey });
export const onAccountAuthed        = (userData)    => ({ type: 'ACCOUNT_AUTHED',       value: userData });
export const onAccountLogin         = ()            => ({ type: 'ACCOUNT_LOGIN',        value: "" });
export const onAccountLogout        = ()            => ({ type: 'ACCOUNT_LOGOUT',       value: "" });
export const receiveSessionKey      = (sessionKey)  => ({ type: 'RECEIVE_SESSION_KEY',  value: sessionKey });
export const setLoginUrl            = (url)         => ({ type: 'SET_LOGIN_URL',        value: url });
export const setLogoutUrl           = (url)         => ({ type: 'SET_LOGOUT_URL',       value: url });
export const onAccountAuthChecked   = ()            => ({ type: 'ACCOUNT_AUTH_CHECKE',  value: true })
export const setJwtToken            = (token)       => ({ type: 'SET_JWT_TOKEN',        value: token });

export const setValue               = (key, value)  => ({ type: "SET_VALUE", value, key });
export const setValues              = (value)       => ({ type: "SET_VALUES", value });
