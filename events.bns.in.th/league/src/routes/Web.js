import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import OauthLogin from './../middlewares/OauthLogin';
import Modals from './../features/modals';
import Home from './../pages/Home';
import Competition from './../pages/Competition';
import Quest from './../pages/Quest';
import Ranking from './../pages/Ranking';
import History from './../pages/History';
import Reward from './../pages/Reward';

export const Web = (props) => {

  let dmp = process.env.REACT_APP_EVENT_PATH;

  return (
    <>
      <Route path={""} component={Modals} />
      <Route path={""} component={OauthLogin}/>

      <Route path={dmp} exact component={Home} />
      <Route path={dmp+'/competition'} exact component={Competition} />
      <Route path={dmp+'/quest'} exact component={Quest} />
      <Route path={dmp+'/ranking'} exact component={Ranking} />
      <Route path={dmp+'/history'} exact component={History} />
      <Route path={dmp+'/reward'} exact component={Reward} />
    </>
  );
}
