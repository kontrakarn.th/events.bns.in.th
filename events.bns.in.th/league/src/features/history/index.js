import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
import Logo from '../main/BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import {Imglist} from './../../constants/Import_Images';

class History extends React.Component {

 apiGetHistory(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'history'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open: '',
                    history_list: data.data
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_HISTORYLIST", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    componentDidMount() {
        if(this.props.jwtToken !== "" && this.props.history_list && this.props.history_list.length==0){
            // this.apiGetHistory()
        }
    }

    render() {
        return (
            <PageWrapper>
                <Logo/>
                <Menu page='history'/>
                <HistroyFrame>
                        <ul className="list">
                                    {
                                        this.props.history_list.map((item,key) => {
                                            return(
                                                <li key={`history_${key+1}`}>
                                                    <div>
                                                            Item Name
                                                    </div>
                                                    <div>
                                                            Date/Time
                                                    </div>
                                                </li>
                                            )})
                                    }
                        </ul>
                </HistroyFrame>
                <ModalLoading />
                <ModalMessage />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(History);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_2']}) #050506;
    width: 1105px;
    padding-top: 118px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const HistroyFrame = styled.div`
    background: top center no-repeat url(${Imglist['history_section']});
    width: 922px;
    height: 582px;
    margin: 0 auto;
    box-sizing: border-box;
    padding: 125px 96px 0;
    .list{
        width: 700x;
        height: 433px;
        margin: 0 auto;
        padding: 0;
        overflow: auto;
        overflow-x: hidden;
        color: #ffffff;
        >li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 0 2%;
            box-sizing: border-box;
            >div:first-child{
                width: 50%;
                text-align: left;
            }
        }
        &::-webkit-scrollbar {
            width: 8px;
        }
        &::-webkit-scrollbar-track {
          background: rgba(173,173,173,0.05);
        }
        &::-webkit-scrollbar-thumb {
            background: #000000;
        }
    }
`
