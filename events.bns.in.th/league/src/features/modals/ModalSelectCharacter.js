import React,{useState} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import iconDropdown from './../../static/images/icon_dropdown.png';
import {imgList} from './../../constants/Import_Images';
import {apiPost} from './../../constants/Api';

const CPN = (props) => {
  const [selectcharacter,setSelectcharacter] = useState("")
  const actSelectCharacter = ()=>{
    if(selectcharacter === "") {
      props.setValues({
          modal_open:"message",
          modal_message: "โปรดเลือกตัวละคร",
          selected_char:false,
      });
    } else {
      apiSelectCharacter(selectcharacter);
    }
  }

  const apiSelectCharacter = (val) => {
    console.log("apiSelectCharacter",val);
    apiPost(
      "REACT_APP_API_POST_SELECT_CHAR",
      props.jwtToken,
      {type: "select_character",id: selectcharacter},
      (data)=>{//function successCallback
        console.log("apiEventInfo",data.data.character);
        props.setValues({
            ...data.data,
            modal_open: "message",
            modal_message: "เรียบร้อยแล้ว",
        });
      },
      ()=>{},//function failCallback\
    );

    props.setValues({
        modal_open:"message",
        modal_message: "สำเร็จ",
        character: val,
    });
  }

  return (
    <ModalCore modalName="selectcharacter" actClickOutside={false} >
       <ModalContent className="mdselect">
          <form className="mdselect__form" id="charater_form">
            <select className="mdselect__select" onChange={(e) => setSelectcharacter(e.target.value)}>
              <option value="" disabled selected>เลือกตัวละคร</option>
              {props.characters && props.characters.map((item,index)=>{
                  return (
                      <option key={index} value={item.id}>{item.char_name}</option>
                  )
              })}
            </select>
          </form>
          <div className="mdselect__btns">
            <Btn className='confirm' onClick={()=>actSelectCharacter()} />
          </div>
      </ModalContent>
    </ModalCore>
  )
}
const mstp = state => ({...state.Main});
const mdtp = { setValues };

export default connect( mstp, mdtp )(CPN);

const ModalContent = styled.div`
  position: relative;
  display: block;
  width: 503px;
  height: 345px;
  font-size: 1em;
  color: #FFFFFF;
  background: no-repeat center url(${imgList.popup_select});
  padding: 120px 65px 0px;

  .mdselect{
    &__form {
      position: relative;
      display: block;
      height: 50px;
      background-color: #000000;
      &:after {
        content: " ";
        position: absolute;
        top: 0px;
        right: 0px;
        display: block;
        width: 50px;
        height: 50px;
        background: no-repeat center url(${imgList.icon_dropdown});
        pointer-events: none;
      }
    }
    &__select {
      color: #FFFFFF;
      font-size: 1.2em;
      position: relative;
      width: 100%;
      height: 50px;
      line-height: 50px;
      background-color: #00000000;
      border: none;
      border-radius: none;
      option {
        background-color: #000000;
        color: #FFFFFF;
      }

    }
    &__btns {
      position: relative;
      margin: 100px auto;
      display: flex;
      justify-content: center;
      align-items: center;
      margin: 83px auto;
    }
  }
`
const Btn = styled.div`
  width: 108px;
  height: 41px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #000;
    line-height: 36px;
    font-size: 20px;
    background-size: contain;
    opacity: 0.8;
    transition: filter -0.3s ease-in-out;
    &.confirm{
        background: no-repeat 0 0 url(${imgList.btn_confirm});
    }
    &.cancel{
        background: no-repeat 0 0 url(${imgList.btn_cancel});
    }
    &:hover{
      filter: grayscale(0.9)
    }
`
