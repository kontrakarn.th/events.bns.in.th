import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import {imgList} from './../../constants/Import_Images';
import {apiPost} from './../../constants/Api';

const CPN = props => {
  const actSelectSpecialQuest_ = () => {
    if(!props.is_select_special_quest){
      props.setValues({modal_open: "loading"});
      apiPost(
        "REACT_APP_API_POST_SELECT_SPECIAL_QUEST",
        props.jwtToken,
        {type: "select_special_quest",id: props.modal_select},
        (data)=>{//function successCallback
          console.log("actSelectSpecialQuest",data);
          props.setValues({
            ...data.data,
            modal_open: "message",
            modal_message: "เลือกทำภารกิจสำเร็จแล้ว",
          });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message,
          });
        },
      );
    }
  }

  return (
    <ModalCore
        modalName="confirm"
        actClickOutside={()=>props.setValues({modal_open:""})}
    >
        <ModalConfirmContent className="mdconfirm">
            <div
              className="mdconfirm__text"
              dangerouslySetInnerHTML={{__html: props.modal_message}}
            />
            <div className="mdconfirm__btns">
                <Btn className='confirm' onClick={()=>actSelectSpecialQuest_()} />
                <Btn className='cancel' onClick={()=>props.setValues({modal_open:''})} />
            </div>
        </ModalConfirmContent>

    </ModalCore>
  )
}
const mstp = state => ({...state.Main});
const mdtp = { setValues };
export default connect( mstp, mdtp )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalConfirmContent = styled.div`
  position: relative;
  display: block;
  width: 503px;
  height: 345px;
  font-size: 1em;
  color: #FFFFFF;
  background: no-repeat center url(${imgList.popup_confirm});
  padding: 65px 55px 0px;
  .mdconfirm{
    &__text {
      width: 100%;
      height: 155px;
      display: flex;
      justify-content: center;
      align-items: center;
      color: #000000;
      font-size: 21px;
      line-height: 1.5em;
      word-break: break-word;
      text-align: center;
    }
    &__btns {
      position: relative;
      margin: 100px auto;
      display: flex;
      justify-content: center;
      align-items: center;
      margin: 30px auto 0px;
    }

  }
`;

const Btn = styled.div`
  width: 108px;
  height: 41px;
  display: inline-block;
  cursor: pointer;
  margin: 0 15px;
  color: #000;
  line-height: 36px;
  font-size: 20px;
  background-size: contain;
  opacity: 0.8;
  &.confirm{
    background: no-repeat 0 0 url(${imgList['btn_confirm']});
  }
  &.cancel{
    background: no-repeat 0 0 url(${imgList['btn_cancel']});
  }
  &:hover{
    filter: grayscale(0.9)
  }
`
