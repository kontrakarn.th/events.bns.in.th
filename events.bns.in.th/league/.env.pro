REACT_APP_EVENT_PATH=/league
REACT_APP_STATIC_PATH=/static/
REACT_APP_LOCALE=th-TH
REACT_APP_CSRF_COOKIE_NAME=csrf_token
REACT_APP_COOKIE_DOMAIN=.bns.in.th

# API related
REACT_APP_API_SERVER_HOST=https://events.bns.in.th
REACT_APP_API_GET_OAUTH_INFO=/oauth/get_oauth

# API FOR EVENT
REACT_APP_API_POST_EVENT_INFO=/api/league/event_info
REACT_APP_API_POST_SELECT_CHAR=/api/league/select_char
REACT_APP_API_POST_REGISTER=/api/league/register
REACT_APP_API_POST_COMPETITION_INFO=/api/league/competition_info
REACT_APP_API_POST_PERSONAL_INFO=/api/league/personal_info
REACT_APP_API_POST_CLAIM_FREE_POINTS=/api/league/claim_free_points
REACT_APP_API_POST_CLAIM_QUEST_POINTS=/api/league/claim_quest_points
REACT_APP_API_POST_SELECT_SPECIAL_QUEST=/api/league/select_special_quest
REACT_APP_API_POST_CLEAIM_SPECIAL_QUEST_POINTS=/api/league/claim_special_quest_points
REACT_APP_API_POST_CLEAIM_PERSONAL_REWARD=/api/league/claim_personal_reward
REACT_APP_API_POST_COLOR_RANK_INFO=/api/league/color_rank_info
REACT_APP_API_POST_HISTORY=/api/league/history
REACT_APP_API_POST_REWARD_INFO=/api/league/reward_info
REACT_APP_API_POST_CLAIM_RANK_REWARD=/api/league/claim_rank_reward
REACT_APP_API_POST_CLAIM_COLOR_REWARD=/api/league/claim_color_reward

# SSO related
#REACT_APP_SSO_APP_ID=10046
#REACT_APP_SSO_URL=https://sso.garena.com
#REACT_APP_SSO_PARAM_NAME=session_key
#REACT_APP_SSO_COOKIE_NAME=sso_session

# OAUTH related
REACT_APP_OAUTH_APP_ID=10007
REACT_APP_OAUTH_APP_NAME=bns_league_live
REACT_APP_OAUTH_PLATFORM=1
REACT_APP_OAUTH_URL=https://auth.garena.com
REACT_APP_OAUTH_PARAM_NAME=token
REACT_APP_OAUTH_COOKIE_NAME=oauth_session

# JWT
REACT_APP_JWT_SECRET=bJZ6rhanFZ4rdYb3jwSX2CVSLQyzp8t5

# Landing Page
REACT_APP_LANDING_SERVER_HOST=https://landing.garena.in.th
REACT_APP_LANDING_JSON=/static/data/get_landing.json
