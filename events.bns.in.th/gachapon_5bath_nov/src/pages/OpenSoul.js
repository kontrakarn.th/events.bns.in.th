import React from 'react';

import './../styles/index.css';
import './../styles/modal.css';
import './../styles/f11layout.css';
import OpenSoulContainer from '../containers/OpenSoulContainer';

export class OpenSoul extends React.Component {
    render() {
        return (<OpenSoulContainer />)
    }
}
