import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import OauthMiddleware from './../middlewares/OauthMiddleware';

import { Home } from './../pages/Home';
import { Soul } from './../pages/Soul'
import { HistoryList } from './../pages/HistoryList'
import { OpenSoul } from './../pages/OpenSoul'

export class Web extends Component {
	render() {
		return (
			<div>
			 	<Route path={process.env.REACT_APP_EVENT_PATH} component={OauthMiddleware} />
				<Route path={process.env.REACT_APP_EVENT_PATH} exact component={Home} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/soul'} exact component={Soul} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/history'} exact component={HistoryList} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/open-soul'} exact component={OpenSoul} />
			</div>
		);
	}
}
