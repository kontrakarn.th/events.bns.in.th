import React from 'react';
import './style.css';
import cb from '../../static/images/card/card_back.png';
import cf from '../../static/images/card/card_01.png';
export default (props) => (
    <div onClick={props.onClick.bind(this)} className={"card "+(props.open? "filpopen": "filpclose")}>
    	<img className={"card__back"} src={props.srcBackCard || cb} />
    	<img className={"card__front"} src={props.srcFrontCard || cf} />
        <img className={"card__size"} src={props.srcBackCard || cb} />
    </div>
)
