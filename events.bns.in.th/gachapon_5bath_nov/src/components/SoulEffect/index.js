import React from 'react';
import styled, { keyframes } from 'styled-components';
import imgSoulBreak from './soul_break.png';

const color = {
    pink: "#ff00cb",
}
export default class extends React.Component {
    actClick(){
        if(this.props.active === true) {
            // console.log("actClick");
            if(this.props.onClick){
                 this.props.onClick()
            };
            this.actOpenSoul();
        }
    }
    actOpenSoul(){
        this.setState({open: true});
        setTimeout(()=>this.setState({break: true}),800);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props){
        super(props);
        this.state = {
            open: false,
            break: false
        }
    }
    componentDidUpdate(prevProp,prevState) {
        if(this.props.open !== prevProp.open){
            if(this.props.open === true) {
                this.actOpenSoul();
            }
        }
    }
    render() {
        let w = this.props.width || 100;
        let h = this.props.height|| 100;
        let c = this.state.open || false;
        let b = this.state.break || false;
        return(
            <SoulUnit break={b} onClick={this.actClick.bind(this)}>
                <SoulItem   break={c}  width={w} height={h} src={this.props.item}/>
                <SoulBreak  break={c} width={w} height={h} src={imgSoulBreak}/>
                <SoulLive   break={c}  width={w} height={h} src={this.props.soul}/>
            </SoulUnit>
        )
    }
}

const blink = keyframes`
    from {
        filter: hue-rotate(0deg) drop-shadow(0px 0px 0px ${color.pink});
    }
    50% {
        filter: hue-rotate(0deg) drop-shadow(0px 0px 10px ${color.pink});
    }
    to {
        filter: hue-rotate(0deg) drop-shadow(0px 0px 0px ${color.pink});
    }
`;
const animationshow = keyframes`
    0% {
        opacity: 0;
        transform: scale(0.1);
    }
    100% {
        opacity: 1;
        transform: scale(1);
    }
`;

const animationbreak = keyframes`
    0% {
        opacity: 1;
    }
    100% {
        background-position: 0 110%;
    }
`;

const SoulUnit = styled.div`
    position: relative;
    display: inline-block;
    width: 75%;
    animation: ${props => props.break? "none" :blink} 2s linear infinite ;
`;
const SoulLive = styled.canvas`
    /* opacity: 1; */
    position: relative;
    display: block;
    width: 100%;
    height: 100%;
    overflow: hidden;
    background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
    background-image: url(${props => props.src});
    transition: 0.3s opacity linear;
    opacity: ${props=>props.break ? "0":"1"};
`;
const SoulBreak = styled.canvas`
    opacity: 0;
    position: absolute;
    top: 0px;
    left: 0px;
    display: ${props => props.break?"block": "none"};
    width: 100%;
    height: 100%;
    background-size: 100% auto;
    background-position: 0px 0%;
    background-repeat: no-repeat;
    background-image: url(${props => props.src});
    animation: ${animationbreak} 1s steps(10) alternate forwards;
    transition: 0.3s opacity linear;
    filter: hue-rotate(304deg);
`;
const SoulItem = styled.canvas`
    opacity: 0;
    position: absolute;
    top: 0px;
    left: 0px;
    display: ${props => props.break?"block": "none"};
    width: 100%;
    height: 100%;
    background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
    background-image: url(${props => props.src});
    animation: ${animationshow} 0.3s 0.7s linear forwards ;
    transition: 1s 1s opacity linear;
`;
