import React from 'react';
import Modal from './Modal';

export default class ModalConfirm extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                title="แจ้งเตือน"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                {this.props.setMsg === 1 &&
                    <div className="modal__content--inner">
                        <span className="text--darkgreen">
                            ต้องการที่จะซื้อการ์ด<br/>
                            จำนวน 1 ใบ<br/>
                        </span>
                        ในราคา 500 ไดมอนด์<br/>
                        ใช่หรือไม่
                    </div>
                }
                {this.props.setMsg === 2 &&
                    <div className="modal__content--inner">
                        <span className="text--darkgreen">
                            ต้องการที่จะซื้อการ์ด<br/>
                            จำนวน 5 ใบ<br/>
                        </span>
                        ในราคา 2,500 ไดมอนด์<br/>
                        ใช่หรือไม่
                    </div>
                }
                {this.props.setMsg === 3 &&
                    <div className="modal__content--inner">
                        <span className="text--darkgreen">
                            ต้องการที่จะซื้อการ์ด<br/>
                            จำนวน 10 ใบ<br/>
                        </span>
                        ในราคา 5,000 ไดมอนด์<br/>
                        ใช่หรือไม่
                    </div>
                }
            </Modal>
        )
    }
}
