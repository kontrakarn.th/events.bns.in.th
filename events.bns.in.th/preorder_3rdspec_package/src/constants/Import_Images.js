const iconScroll 	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/desktop/icons/icon_scroll.png'
const bg_box1 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/desktop/bg_box1.png'
const bg_box2 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/desktop/bg_box2.png'
const bg_box3 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/desktop/bg_box3.png'
const test_items 	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/desktop/test_items.png'
const btn_zoom		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/desktop/btn_zoom.png'
const character	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/desktop/character.png'
const close		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/desktop/close.png'

export const Imglist = {
	iconScroll,
	bg_box1,
	bg_box2,
	bg_box3,
	test_items,
	btn_zoom,
	character,
	close
}