import React, { Component } from 'react';
import {connect} from 'react-redux';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/airpay_dec2018.css';

// import ModalConfirmToRedeem from '../components/ModalConfirmToRedeem';

import ModalMessage from '../components/ModalMessage';
import ModalConfirmGacha from '../components/ModalConfirmGacha';
import ModalReceiveGacha from '../components/ModalReceiveGacha';
import ModalConfirmExchange from '../components/ModalConfirmExchange';
import ModalReceiveExchange from '../components/ModalReceiveExchange';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';

import imgItemHat from './../static/images/airpay_dec2018/item_hat.png';


import {
    onAccountLogout
} from '../actions/AccountActions';

class HomeContainer extends Component {
    actGachaRandom(){
        this.setState({
            gacha: -1
        })
        this.apiPlay();
    }
    actExchange(){

        this.apiExchange();
    }
    genClassGacha(baseClass,index,gachaSelected,infinite=false){
        if(gachaSelected < 0) {
            return baseClass;
        }
        if(infinite){
            return baseClass + " " + baseClass + "--rnd"+index+"infinite";
        }
        if(gachaSelected === index) {
            return baseClass + " " + baseClass + "--selected"
        } else {
            return baseClass + " " + baseClass + "--rnd"+index
        }



    }
    numberWithCommas(x){
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiEventInfo(){
        this.setState({ showLoading: true});
        let self = this;
        fetch( process.env.REACT_APP_API_POST_EVENT, {
            method: 'POST',
            body: JSON.stringify({
                "service":"event_info"
            }),
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'
            }
        }).then(response => response.json()).then(response=>{
            if(response.status){
                this.setState({
                    permission: true,
                    showLoading: false,
                    ...response.data
                })
            }else{
                if(response.type=='no_permission'){
                    this.setState({
                        permission: false
                    })
                } else {
                    this.setState({
                        showModalMessage: response.message,
                        showLoading: false,
                    });
                }
            }

        }, error => {

        });
    }
    apiPlay(){
        this.setState({
            showModalConfirmGacha: false,
            showLoading: true,
            // rnd_gacha: true,
        });
        let self = this;
        // fetch(process.env.REACT_APP_API_SERVER_HOST + process.env.REACT_APP_API_POST_PLAY, {
        fetch( process.env.REACT_APP_API_POST_EVENT, {
            method: 'POST',
            body: JSON.stringify({
                "service":"play"
            }),
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'
            }
        }).then(response => response.json()).then(response=>{
            if(response.status){
                this.setState({
                    gacha: response.gacha,
                    showLoading: false,
                    // rnd_gacha: false,
                    showModalReceiveGacha:true,
                    ...response.data
                })
                // setTimeout(()=>this.setState({showModalReceiveGacha:true}),4000);
            }else{
                if(response.type=='no_permission'){
                    this.setState({
                        permission: false
                    })
                } else {
                    this.setState({
                        showModalMessage: response.message,
                        showLoading: false,
                        // rnd_gacha: false,
                    });
                }
            }

        }, error => {

        });
    }

    apiExchange(){
        this.setState({
            showModalConfirmExchange: false,
            showLoading: true
        });
        let self = this;
        fetch( process.env.REACT_APP_API_POST_EVENT, {
            method: 'POST',
            body: JSON.stringify({
                "service":"exchange",
            }),
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'
            }
        }).then(response => response.json()).then(response=>{
            if(response.status){
                this.setState({
                    permission: true,
                    // showModalReceiveExchange: true,
                    showModalReceiveExchangeMessage: "รูดอล์ฟ",
                    showModalMessage: response.message,
                    canClickExchange: true,
                    showLoading: false,
                    ...response.data,
                })
            }else{
                if(response.type=='no_permission'){
                    this.setState({
                        permission: false,
                        showLoading: false,
                    })
                } else {
                    this.setState({
                        showModalMessage: response.message,
                        showLoading: false,
                    });
                }
            }

        }, error => {

        });
    }
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: false,

            name: "",
            canplay: false,
            gachas:[],
            can_get_special: false,
            received_special: 0,
            total_play: 0,
            valueStarList: [false, false, false, false, false, false, false, false, false, false, false],

            package_select:{},
            gacha: "",
            // rnd_gacha: false,
            exchange_package: "รูดอล์ฟ",

            showModalMessage: "",
            showModalConfirmGacha: false,
            showModalReceiveGacha: false,
            showModalConfirmExchange: false,
            showModalReceiveExchange: false,
            showModalReceiveExchangeMessage: "",

            canClickRandom: true,
            canClickExchange: true,
            scollDown: false,
        }
    }
    componentDidMount(){
        setTimeout(()=>this.apiEventInfo(),600);
    }
    // componentDidUpdate(prevProps, prevState) {
    //     if(prevProps.userData != this.props.userData){
    //         this.getEventInfo()
    //     }
    // }

    render() {
        let username = (this.props.userData)? this.props.userData.username : "";
        return (
            <div >
                <div className="f11layout">
                    <div className="f11layout__name">{username}</div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                    <div className={"f11layout__scroll"+(this.state.scollDown? " hide":"")} ><img src={iconScroll} alt="" /></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">

                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="airpay_dec2018">
                                <section className="airpay_dec2018__section airpay_dec2018__section--header">
                                    <div className="header">
                                        <div className="header__inner">
                                            <div className="header__tag header__tag--date"></div>
                                            <div className="header__tag header__tag--event"></div>
                                        </div>
                                    </div>
                                </section>
                                <section className="airpay_dec2018__section airpay_dec2018__section--condition">
                                    <div className="condition">
                                        <div className="condition__inner">
                                            <div className="condition__head"></div>
                                            <ul className="condition__text">
                                                <li>ระยะเวลากิจกรรม 19 ธ.ค. 2561 (12:00 น.) - 16 ม.ค. 2562 (03:00 น.)</li>
                                                <li>ผู้เล่นเข้าหน้ากิจกรรม สามารถสอยดาวที่ติดอยู่กับต้นคริสต์มาส จะได้รับไอเทมตามดวงชะตาของตัวเอง</li>
                                                <li>การสอยแต่ละครั้ง จะได้รับดาว 1 ดวง</li>
                                                <li>เมื่อดาวครบ 10 ดวง สามารถแลกหมวกรูดอล์ฟ ได้ฟรี (จำกัด 1 ชิ้น ต่อ UID)</li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                                <section className="airpay_dec2018__section airpay_dec2018__section--wishingtree">
                                    <div className="wishingtree">
                                        <div className="wishingtree__inner">
                                            <div className="wishingtree__head"></div>
                                            <div className="wishingtree__group" style={{width: '55%'}}>
                                                <div className="wishingtree__christmastree">
                                                    <div className="wishingtree__starbox">
                                                        {
                                                            this.state.valueStarList.map((star, index)=>{
                                                                if(this.state.canplay){
                                                                    return (
                                                                        <a onClick={()=>this.setState({showModalConfirmGacha:true})} key={"can_not_star_"+index} className={"wishingtree__star ani--rnd"+index+"infinite"}
                                                                         starChild={index}></a>
                                                                    )
                                                                }else{
                                                                    return (
                                                                        <a key={"can_not_star_"+index} className={"wishingtree__star ani--rnd"+index+"infinite"}
                                                                         starChild={index}></a>
                                                                    )
                                                                }
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="wishingtree__group" style={{width: '45%'}}>
                                                <div className="wishingtree__box">
                                                    <div className="wishingtree__headbox wishingtree__headbox--numstar"></div>
                                                    <div className="wishingtree__boxbottom">
                                                        {this.state.total_play}
                                                    </div>
                                                </div>
                                                <div className="wishingtree__box">
                                                    <div className="wishingtree__headbox wishingtree__headbox--changehat"></div>
                                                    <div className="wishingtree__boxbottom">
                                                        <img src={imgItemHat} />
                                                    </div>
                                                    {
                                                        this.state.received_special
                                                        ?
                                                            <a className="wishingtree__btnchangehat haveitem"></a>
                                                        :
                                                            this.state.total_play>=10 && this.state.can_get_special
                                                            ?
                                                                <a onClick={()=>this.setState({showModalConfirmExchange:true})} className="wishingtree__btnchangehat getitem"></a>
                                                            :

                                                                <a className="wishingtree__btnchangehat"></a>
                                                    }

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </ScrollArea>
                    </div>
                    :
                    <Permission />
                }


                <Loading
                    open={this.state.showLoading}
                />
                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
                <ModalConfirmGacha
                    open={this.state.showModalConfirmGacha}
                    actConfirm={()=>this.actGachaRandom()}
                    actClose={()=>this.setState({showModalConfirmGacha: false, canClickRandom: true})}
                />
                <ModalReceiveGacha
                    open={this.state.showModalReceiveGacha}
                    actClose={()=>this.setState({showModalReceiveGacha: false, canClickRandom: true})}
                    data={this.state.gacha !="" ? this.state.gacha : ""}
                />
                <ModalConfirmExchange
                    open={this.state.showModalConfirmExchange}
                    actConfirm={()=>this.actExchange()}
                    actClose={()=>this.setState({showModalConfirmExchange: false, canClickExchange: true})}
                    data={this.state.exchange_package}

                />
                <ModalReceiveExchange
                    open={this.state.showModalReceiveExchange}
                    actClose={()=>this.setState({showModalReceiveExchange: false, canClickExchange: true})}
                    msg={this.state.showModalReceiveExchangeMessage}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
    onAccountLogout: () => {
        dispatch(onAccountLogout());
    }
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
