const bg = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/background.png';
const bg1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/bg1.png'

const item1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/items/item1.png'
const item2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/items/item2.png'
const item3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/items/item3.png'
const item4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/items/item4.png'
const item5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/items/item5.png'
const item6 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/items/item6.png'
const item7 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/items/item7.png'
const item8 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/items/item8.png'
const item9 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/items/item9.png'

const items_active = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/items_active.png'
const bg2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/bg2.png'
const btn_gacha = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/btn_gacha.png'
const icon_diamond = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/diamond.png'
const frame_spin = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/frame_spin.png'

const bg3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/bg3.png'
const slide1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/slide/1.png'
const slide2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/slide/2.png'
const slide3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/slide/3.png'
const slide4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/slide/4.png'

const modal_bg = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/modal_bg.png'
const btn_default = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/greyed_out2019/images/btn_default.png'
export const Imglist = {
	bg,
	bg1,
	item1,
	item2,
	item3,
	item4,
	item5,
	item6,
	item7,
	item8,
	item9,
	items_active,
	bg2,
	btn_gacha,
	icon_diamond,
	frame_spin,
	bg3,
	slide1,
	slide2,
	slide3,
	slide4,
	modal_bg,
	btn_default,
}
