import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import {Imglist} from './../../constants/Import_Images';
import ModalConfirmLetsPool from '../modals/ModalConfirmLetsPool';

import Random from '../../features/Random';

const settings = {
    dots: true,
    arrows: false,
    infinite: true,
    fade: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
};

class Main extends React.Component {
    actConfirm(){
        this.apiRedeem(
            this.props.modal_confirmitem && this.props.modal_confirmitem !== ''
            && this.props.modal_confirmitem
        );
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiEventInfo(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
                this.props.setValues({
                    ...data.content,
                    modal_open: "",
                });
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiSelectItem(){
        if(this.props.selectedItemKey > 0 && this.props.selectedGroup > 0){
            this.props.setValues({ modal_open: "loading" });
            let dataSend = {type:'select_reward', item_key: this.props.selectedItemKey, group: this.props.selectedGroup};
            let successCallback = (data) => {
                if (data.status) {
                    this.props.setValues({
                        ...data.content,
                        modal_open:"message",
                        modal_message: data.message
                    });
                }else{
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: data.message,
                    });
                }
            };
            const failCallback = (data) => {
                if (data.type == 'no_permission') {
                    this.props.setValues({
                        permission: false,
                        modal_open: "",
                    });
                }else {
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: data.message,
                    });
                }
            };
            apiPost("REACT_APP_API_POST_SELECCT_REWARD", this.props.jwtToken, dataSend, successCallback, failCallback);
        }else{
            this.props.setValues({
                modal_open:"message",
                modal_message: 'กรุณาเลือกไอเทม',
            });
        }
        
    }
    apiLetsPool(){
        if(this.props.pool_status == false && this.props.pool_running == false){
            this.props.setValues({ modal_open: "loading", pool_running: true });
            // this.props.setValues({modal_open:"", pool_running: true, playing: true,first_position:0})
            
            let dataSend = {type:'lets_pool'};
            let successCallback = (data) => {
                if (data.status) {
                    this.props.setValues({modal_open: "", playing: false, first_position:0,last_position: data.last_position, loop: 2})
                    setTimeout(()=>{
                        this.props.setValues({
                            ...data.content,
                            modal_open:"message",
                            modal_message: data.message,
                            pool_running: false,
                        });
                    }, 3000);
                }else{
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: data.message,
                    });
                }
            };
            const failCallback = (data) => {
                if (data.type == 'no_permission') {
                    this.props.setValues({
                        permission: false,
                        modal_open: "",
                    });
                }else {
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: data.message,
                    });
                }
            };
            apiPost("REACT_APP_API_POST_LETS_POOL", this.props.jwtToken, dataSend, successCallback, failCallback);
        }
        
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,
            slider_image: [
                {
                    title: 'กล่องอาวุธลวงตาแวมไพร์กระหายเลือด',
                    image: Imglist.slide1
                },
                {
                    title: 'ชุดผู้นอบน้อม',
                    image: Imglist.slide2
                },
                {
                    title: 'ชุดบุษกรครามและปีกพยัคฆ์ดำ',
                    image: Imglist.slide3
                },
                {
                    title: 'ชุดแวมไพร์กระหายเลือด',
                    image: Imglist.slide4
                },
            ]
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiEventInfo();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiEventInfo();
        }
    }

    selectItem(item_key,group,title,selected){
        if(selected == false){
            this.props.setValues({
                selectedGroup: group,
                selectedItemKey: item_key,
                selectedItemTitle: title,
            })
        }else{
            this.props.setValues({
                modal_open:"message",
                modal_message: 'ไอเทมนี้ถูกเลือกไปแล้ว',
            });
        }
    }

    onConfirmSelectItem(){
        if(this.props.selectedItemKey > 0 && this.props.selectedGroup > 0){
            this.props.setValues({
                modal_open: 'confirm',
                modal_message: 'เลือก '+this.props.selectedItemTitle+' หรือไม่?'
            })
        }else{
            this.props.setValues({
                modal_open:"message",
                modal_message: 'กรุณาเลือกไอเทม',
            });
        }
    }

    resetSelectItem(){
        this.props.setValues({
            selectedGroup: -1,
            selectedItemKey: -1,
            selectedItemTitle: '',
            modal_open: '',
            modal_message: '',
        })
    }

    onConfirmLetsPool(poolPrice){
        if(this.props.pool_status == false && this.props.pool_running == false){
            this.props.setValues({
                modal_open: 'letspool',
                modal_message: 'ยืนยันหมุน Lucky Spin<br />'+poolPrice+' ไดมอนด์?'
            })
        }
        
    }

    fncCallback(){
        this.props.setValues({modal_open:"",playing:false,first_position:this.props.last_position});
    }

    render() {
        
        return (
            <PageWrapper>
                <Content>
                    <div className="wrapper__promotion__step1">
                        <div className="section section__promotion">
                            <div className="section__header">รายละเอียดโปรโมชั่น</div>
                            <ul className="list-unstyled">
                                <li>1. เข้าร่วมโปรโมชั่นได้ตั้งแต่วันที่ 16 ตุลาคม - 30 ตุลาคม 2562 เวลา 23:59 น. จำกัด 1 ครั้ง ต่อ 1 UID</li>
                                <li>2. ผู้เล่นเลือกรับไอเทม 2 อย่าง ก่อนเข้าร่วม โดยไอเทมดังกล่าวไม่สามารถเปลี่ยนได้ในภายหลัง</li>
                                <li>3. ค่าใช้จ่ายในการหมุน Lucky Spin เริ่มต้นที่ 3,000 ไดมอนด์ และจะเพิ่มขึ้นตามจำนวนครั้งที่หมุน</li>
                                <li>4. ภายใน 6 ครั้ง ที่ผู้เล่นหมุน Lucky Spin จะได้รับของรางวัลที่เลือกครบทั้งหมด</li>
                                <li>5. ไอเทมที่ได้รับจะส่งเข้ากล่องจดหมายกลางในเกม </li>
                            </ul>
                        </div>
                        <div className="section section__step1">
                            <div className="section__header">STEP 1</div>
                            <div>
                                เลือกไอเท็มที่ต้องการ 2 ชิ้น<br/>
                                <small>(สามารถเลือกไอเทมที่เหมือนกันได้)</small>
                                <div>
                                    {
                                        this.props.item_list && this.props.item_list.length > 0 && this.props.item_list.map((items,key)=>{
                                            return(
                                                // **** if selected items = add class name disabled ****//
                                                this.props.selected_pool && this.props.selected_pool == true ?
                                                    (
                                                        <div key={key} className={"items " + (items.selected == true? ' selected ' : ' disabled ')}>
                                                            <div className="items__img">
                                                                <img src={items.image}/>
                                                            </div>
                                                            <div>{items.title}</div>
                                                            <div>{items.amount} ชิ้น</div>
                                                        </div>
                                                    )
                                                :
                                                    (
                                                        <div key={key} className={"items " + (this.props.selectedGroup == items.group && this.props.selectedItemKey == items.item_key && items.selected == false? ' active ' : '') + (items.selected == true ? ' selected ' : '')} onClick={()=>this.selectItem(items.item_key,items.group,items.title,items.selected)}>
                                                            <div className="items__img">
                                                                <img src={items.image}/>
                                                            </div>
                                                            <div>{items.title}</div>
                                                            <div>{items.amount} ชิ้น</div>
                                                        </div>
                                                    )
                                                
                                                
                                            )

                                        })
                                    }
                                </div>
                            </div>
                            <div>
                                {
                                    this.props.selected_pool && this.props.selected_pool == true ?
                                        <div className="btn btn-default disabled">
                                            เลือกครบแล้ว
                                        </div>
                                    :
                                        <div className="btn btn-default" onClick={()=>this.onConfirmSelectItem()}>
                                            ยืนยัน
                                        </div>
                                }
                                
                            </div>
                        </div>
                    </div>

                    <div className="wrapper__step2">
                        {
                            this.props.selected_pool && this.props.selected_pool == true ?
                                <div className="section section__step2">
                                    <div className="section__header">STEP 2</div>
                                    <div className="section__step2--wrapper">
                                        <Random firstPosition={this.props.first_position} lastPosition={this.props.last_position} time={2} loop={this.props.loop} callback={()=>this.fncCallback()}>
                                        {
                                            this.props.pool_list &&  this.props.pool_list.length > 0 && this.props.pool_list.map((items,key)=>{
                                                return(
                                                    // <div key={key} className={"items " + (this.state.active === key ? ' active ' : '') + (key == 1 ? ' disabled ':'')} onClick={()=>this.setState({active: key})}>
                                                    <Itembox key={key} className={`items i${key+1}`} disabled={items.status == true}>
                                                        <div className="items__img">
                                                            <img src={items.image}/>
                                                        </div>
                                                        <div>{items.product_title}</div>
                                                        <div>{items.amount} ชิ้น</div>
                                                    </Itembox>
                                                )

                                            })
                                        }
                                        </Random>

                                        {
                                            this.props.pool_status && this.props.pool_status == true ?
                                                <div className="btn btn-gacha inactive">
                                                    ได้รับไอเทมครบแล้ว
                                                </div>
                                            :
                                                <div className={"btn btn-gacha " + (this.props.pool_running ? "inactive":"")} onClick={()=>this.onConfirmLetsPool(this.props.pool_price)}>
                                                    หมุนครั้งที่ {this.props.current_round} | {this.props.pool_price} <img src={Imglist['icon_diamond']}/>
                                                </div>
                                        }
                                        

                                        {/* btn inactive */}
                                        {/* <div className="btn btn-gacha inactive">
                                            ได้รับไอเทมครบแล้ว
                                        </div> */}
                                    </div> 
                                </div>
                            :
                                null
                        }
                    </div>
                   
                    <div className="wrapper__example">
                        <div className="section section__example">
                            <div className="section__header">ตัวอย่าง</div>
                            
                            <div>
                                <Slider {...settings}>
                            
                                {
                                    this.state.slider_image.map((items,key)=>{
                                        return(
                                            <div key={key} className=""> 
                                                <p>{items.title}</p>
                                                <img src={items.image}/>
                                            </div>
                                        )
                                    })
                                }
                        
                                </Slider>
                            </div>
                        </div>
                    </div>
                </Content>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.apiSelectItem.bind(this)} actCancel={this.resetSelectItem.bind(this)} />
                <ModalConfirmLetsPool actConfirm={this.apiLetsPool.bind(this)} actCancel={this.resetSelectItem.bind(this)} />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

/* const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #11122d;
    width: 1105px;
    padding-top: 680px;
    text-align: center;

`

const Content = styled.div `
    & .wrapper__promotion__step1{
        background: top center no-repeat url(${Imglist['bg1']});
        color: #fff;
        text-align:left;
    }
    & .wrapper__step2{
        background: top center no-repeat url(${Imglist['bg2']});
        color: #fff;
    }
    & .wrapper__example{
        background: top center no-repeat url(${Imglist['bg3']});
        color: #fff;
        text-align: center;
    }
    
    *,
    *::before,
    *::after {
    box-sizing: border-box;
    }
    & .section{
        padding: 6% 20%;
        &__promotion{
            //padding-top: 70px;
        }
        &__header{
            font-size: 20px;
            font-weight: bold;
            margin-bottom: 30px;
            text-align: center;
        }
        &__step1{
            text-align:center;
            padding-top: 3%;

        }
        &__step2{
            text-align: center;
            padding-top: 4%;
        }
    }
    .items{
        width: 25%;
        display:inline-block;
        vertical-align: top;
        font-size: 14px;
        padding: 10px 20px;
        // cursor:pointer;
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%); 
            &.active{
                -webkit-filter: none;
                filter: none;  
            }
        }
        &__img{
            position:relative;  
        }
        &.active .items__img::after{
            content: '';
            background: top center no-repeat url(${Imglist['items_active']}); 
            left: 0;
            right: 0;
            top: -7px;
            bottom: 0;
            display: block;
            position: absolute;
        }
        &.selected .items__img::after{
            content: '';
            background: top center no-repeat url(${Imglist['items_active']}); 
            left: 0;
            right: 0;
            top: -7px;
            bottom: 0;
            display: block;
            position: absolute;
        }
    }
    .section__step2{
        & .items{
            width: 33.33%;
            &__img{
               
            }
            &.active .items__img::after{
                content: '';
                background: top center no-repeat url(${Imglist['frame_spin']}); 
                top: 7px;
            }
            &:nth-child(1),
            &:nth-child(2),
            &:nth-child(3){
                margin-bottom: 60px;
            }
        }
        &--wrapper{
            position: relative;
            & .btn-gacha{
                position: absolute;
                top: 50%;
                transform: translateY(-50%);
                -webkit-transform: translateY(-50%);
                left: 0;
                right:0;
                margin: 0 auto;
                z-index: 2;
            }
        }
    }
    .btn{
        cursor:pointer;
        &-default{
            background: no-repeat 0 0 url(${Imglist['btn_default']});
            width: 155px;
            height: 45px;
            display: inline-block;
            cursor: pointer;
            margin: 0 15px;
            color: #fff;
            line-height: 36px;
            font-size: 20px;
            &:hover{
                background-position: 0 -55px;
            }
            &.disabled{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);
                color: #bfbfbf;
                cursor: not-allowed;
                &:hover{
                    background-position: 0 0;
                }
            }
        }
        &-gacha{
            background: 0 0 no-repeat url(${Imglist['btn_gacha']}); 
            width: 290px;
            height: 50px;
            margin: 20px auto;
            font-size: 18px;
            padding: 5px;
            &:hover{
                background-position: 0 -61px;
            }
            & img{
                vertical-align:middle;
            }
            &.inactive{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);
                cursor: not-allowed;
                color: #bfbfbf;
                &:hover{
                    background-position: 0 0;
                }
            }
        }
    }
    .section__example{
        padding-bottom: 25%;
        & .slick-slide img{
            margin: 0 auto;
        }
        .slick-dots li.slick-active button:before {
            opacity: 1;
            color: #ff9567;
        }
        .slick-dots li button:before{
            font-size: 11px;
            opacity: 1;
            color: #fff;
        }
    }
` */

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #11122d;
    width: 1105px;
    padding-top: 680px;
    text-align: center;

`

const Content = styled.div `
    & .wrapper__promotion__step1{
        background: top center no-repeat url(${Imglist['bg1']});
        color: #fff;
        text-align:left;
    }
    & .wrapper__step2{
        background: top center no-repeat url(${Imglist['bg2']});
        color: #fff;
    }
    & .wrapper__example{
        background: top center no-repeat url(${Imglist['bg3']});
        color: #fff;
        text-align: center;
    }
    
    *,
    *::before,
    *::after {
    box-sizing: border-box;
    }
    & .section{
        padding: 6% 20%;
        &__promotion{
            //padding-top: 70px;
        }
        &__header{
            font-size: 20px;
            font-weight: bold;
            margin-bottom: 30px;
            text-align: center;
        }
        &__step1{
            text-align:center;
            padding-top: 3%;

        }
        &__step2{
            text-align: center;
            padding-top: 4%;
        }
    }
    .items{
        width: 25%;
        display:inline-block;
        vertical-align: top;
        font-size: 14px;
        padding: 10px 20px;
        cursor:pointer;
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%); 
            &.active{
                -webkit-filter: none;
                filter: none;  
            }
        }
        &__img{
            position:relative;  
        }
        &.active .items__img::after{
            content: '';
            background: top center no-repeat url(${Imglist['items_active']}); 
            left: 0;
            right: 0;
            top: -7px;
            bottom: 0;
            display: block;
            position: absolute;
        }
        &.selected .items__img::after{
            content: '';
            background: top center no-repeat url(${Imglist['items_active']}); 
            left: 0;
            right: 0;
            top: -7px;
            bottom: 0;
            display: block;
            position: absolute;
        }
    }
    .section__step2{
        & .items{
            width: 33.33%;
            &__img{
               
            }
            &.active .items__img::after{
                content: '';
                background: top center no-repeat url(${Imglist['frame_spin']}); 
                top: 7px;
            }
            &:nth-child(1),
            &:nth-child(2),
            &:nth-child(3){
                margin-bottom: 60px;
            }
        }
        &--wrapper{
            position: relative;
            width: 663px;
            height: 398px;
            & .btn-gacha{
                position: absolute;
                top: 50%;
                transform: translateY(-50%);
                -webkit-transform: translateY(-50%);
                left: 0;
                right:0;
                margin: 0 auto;
                z-index: 2;
            }
        }
    }
    .btn{
        cursor:pointer;
        &-default{
            background: no-repeat 0 0 url(${Imglist['btn_default']});
            width: 155px;
            height: 45px;
            display: inline-block;
            cursor: pointer;
            margin: 0 15px;
            color: #fff;
            line-height: 36px;
            font-size: 20px;
            &:hover{
                background-position: 0 -55px;
            }
            &.disabled{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);
                color: #bfbfbf;
                cursor: not-allowed;
                &:hover{
                    background-position: 0 0;
                }
            }
        }
        &-gacha{
            background: 0 0 no-repeat url(${Imglist['btn_gacha']}); 
            width: 290px;
            height: 50px;
            margin: 20px auto;
            font-size: 18px;
            padding: 5px;
            &:hover{
                background-position: 0 -61px;
            }
            & img{
                vertical-align:middle;
            }
            &.inactive{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);
                cursor: not-allowed;
                color: #bfbfbf;
                &:hover{
                    background-position: 0 0;
                }
            }
        }
    }
    .section__example{
        padding-bottom: 25%;
        & .slick-slide img{
            margin: 0 auto;
        }
        .slick-dots li.slick-active button:before {
            opacity: 1;
            color: #ff9567;
        }
        .slick-dots li button:before{
            font-size: 11px;
            opacity: 1;
            color: #fff;
        }
    }
`

const Itembox = styled.div`
            width: 33.33%;
            cursor:pointer;
            position: absolute;
            &__img{
                position: relative;
            }
            ${props=>props.active && `
                .items__img::after{
                    content: '';
                    background: top center no-repeat url(${Imglist['frame_spin']}); 
                    right: 0;
                    left: 0;
                    top: 7px;
                    bottom: 0;
                    display: block;
                    position: absolute;
                }
            `}
            ${props => props.disabled && `
                    -webkit-filter: grayscale(100%);
                    filter: grayscale(100%); 
                    pointer-events: none;
                `
            }
            &.i1{
                left: 0;
                top: 0;  
            } 
            &.i2{
                left: 225px;
                top: 0;
            }
            &.i3{
                right: 0;
                top:0
            }
            &.i4{
                bottom: 0;
                right: 0;
            }
            &.i5{
                left: 225px;
                bottom: 0;
            }
            &.i6{
                left: 0;
                bottom: 0;
            }
            &:nth-child(1),
            &:nth-child(2),
            &:nth-child(3){
                margin-bottom: 60px;
            }
        }
`

