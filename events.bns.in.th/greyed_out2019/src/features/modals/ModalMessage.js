import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';

import modal_bg from './images/modal_bg.png';
import title_text from './images/title_text.png';
import confirm_btn from './images/confirm_btn.png';

import {Imglist} from './../../constants/Import_Images';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="message"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            {/* <ModalMessageContent>
                <img className="titletext" src={title_text} alt=""/>
                <div className='contenttext' dangerouslySetInnerHTML={{__html: modal_message}} />
            </ModalMessageContent>
            <ModalBottom>
                    <Btns src={confirm_btn} onClick={()=>props.setValues({modal_open:''})}/>
            </ModalBottom> */}

            <ModalMessageContent>
                {/* <img className="titletext" src={title_text} alt=""/> */}
                <div className='contenttext' dangerouslySetInnerHTML={{__html: modal_message}} />

                
            </ModalMessageContent>
            <ModalBottom>
                <Btns onClick={()=>props.setValues({modal_open:'',modal_message:''})}>
                    ยืนยัน
                </Btns>
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
/* const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 410px;
    height: 314px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${modal_bg});
    box-sizing: border-box;
    padding: 10% 5% 15%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Medium';
        font-size: 24px;
        line-height: 1.5em;
        word-break: break-word;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`; */
const ModalBottom = styled.div`
    display: block;
    width: 100%;
    position: absolute;
    bottom: 0px;
    z-index: 50;
    text-align: center;
`;

const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 420px;
    height: 314px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist['modal_bg']});
    box-sizing: border-box;
    padding: 10% 5% 15%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttitle{
        font-size: 21px;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 21px;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 150px;
        //height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;

const Btns = styled.div`
    background: no-repeat 0 0 url(${Imglist['btn_default']});
    width: 155px;
    height: 45px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #fff;
    line-height: 36px;
    font-size: 20px;
    &:hover{
        background-position: 0 -55px;
    }
`

/* const Btns = styled.img`
    display: inline-block;
    cursor: pointer;
`
 */