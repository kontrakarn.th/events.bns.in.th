import { Imglist } from "../constants/Import_Images";

const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    // showCharacterName: true,
    characters:[],
    modal_confirm_char_selected: null,
    username: "",
    character: "",
    loginStatus: false,
    showScroll: true,
    noScroll: true,
    loading: false,
    permission: true,
    status: false,
    can_play: false,


    //===modal
    modal_open: "",//"loading","detail","showreward",""showreward","send","modalInput"
    modal_message: "",
    modal_item: "",
    modal_uid: "",
    modal_get_name: "",
    modal_combin: -1,
    modal_trade: -1,


    //===history values
    items_history: [],


    //========Add new
    costume_slider:[
        {
            img: Imglist.preview_1
        },
        {
            img: Imglist.preview_2
        },
        {
            img: Imglist.preview_3
        },
        {
            img: Imglist.preview_4
        },
        {
            img: Imglist.preview_5
        },
        {
            img: Imglist.preview_6
        },
        {
            img: Imglist.preview_7
        },
    ],
    level: '10',
    daily_quests: [],
    weekly_quests: [],
    rewards: [],
    daily_item:[
        {
            count: '1',
            img: Imglist.quest_1,
            received: true
        },
        {
            count: '0',
            img: Imglist.quest_2,
            received: false
        },
        {
            count: '0',
            img: Imglist.quest_3,
            received: false
        },
        {
            count: '0',
            img: Imglist.quest_4,
            received: false
        },
        {
            count: '0',
            img: Imglist.quest_5,
            received: false
        },
    ],
    week_item:[
        {
            img: Imglist.quest_1,
            received: true
        },
        {
            img: Imglist.quest_2,
            received: false
        },
        {
            img: Imglist.quest_3,
            received: false
        },
        {
            img: Imglist.quest_4,
            received: false
        },
        {
            img: Imglist.quest_5,
            received: false
        },
        {
            img: Imglist.quest_1,
            received: false
        },
        {
            img: Imglist.quest_2,
            received: false
        },
        {
            img: Imglist.quest_3,
            received: false
        },
        {
            img: Imglist.quest_4,
            received: false
        },
        {
            img: Imglist.quest_5,
            received: false
        },
    ],
    level_item: [
        {
            level: '1',
            free_item: Imglist.free_1,
            free_hover: Imglist.hover_1,
            paid_item: Imglist.paid_1,
            paid_hover: Imglist.hover_3
        },
        {
            level: '2',
            free_item: Imglist.free_42,
            free_hover: Imglist.hover_2,
            paid_item: Imglist.paid_2,
            paid_hover: Imglist.hover_2
        },
        {
            level: '3',
            free_item: Imglist.free_43,
            free_hover: Imglist.hover_1,
            paid_item: Imglist.paid_3,
            paid_hover: Imglist.hover_2
        },
        {
            level: '4',
            free_item: Imglist.free_44,
            free_hover: Imglist.hover_2,
            paid_item: Imglist.paid_4,
            paid_hover: Imglist.hover_2
        },
        {
            level: '5',
            free_item: Imglist.free_45,
            free_hover: Imglist.hover_1,
            paid_item: Imglist.paid_5,
            paid_hover: Imglist.hover_1
        },
        {
            level: '6',
            free_item: Imglist.free_46,
            free_hover: Imglist.hover_2,
            paid_item: Imglist.paid_6,
            paid_hover: Imglist.hover_2
        },
        {
            level: '7',
            free_item: Imglist.free_46,
            free_hover: Imglist.hover_1,
            paid_item: Imglist.paid_6,
            paid_hover: Imglist.hover_1
        },
        {
            level: '8',
            free_item: Imglist.free_46,
            free_hover: Imglist.hover_1,
            paid_item: Imglist.paid_6,
            paid_hover: Imglist.hover_1
        },
        {
            level: '9',
            free_item: Imglist.free_46,
            free_hover: Imglist.hover_1,
            paid_item: Imglist.paid_6,
            paid_hover: Imglist.hover_1
        },
        {
            level: '10',
            free_item: Imglist.free_46,
            free_hover: Imglist.hover_1,
            paid_item: Imglist.paid_6,
            paid_hover: Imglist.hover_1,
           
        },
    ],
    special_items: [20,30,40,50,60],
    special_item:20,
    slide_show: 0,
    popup_show: "t",
    unlock_item:[
        {
            img: Imglist.unlock_1,
            type: 'normal',
            message: '<div>ซื้อแพ็คเกจปลดล็อกคัมภีร์ล้ำค่า<br/>ได้รับเพิ่ม <span class="green">1</span> เลเวล<br/>ราคา <span class="green">19,900</span> ไดมอนด์ ?</div>'
        },
        {
            img: Imglist.unlock_2,
            type: 'advanced',
            message: '<div>ซื้อแพ็คเกจปลดล็อกคัมภีร์ล้ำค่า (ขั้นสูง)<br/>ได้รับเพิ่ม <span class="green">20</span> เลเวล<br/>ราคา <span class="green">54,900</span> ไดมอนด์ ?</div>'
        },
    ],
    uplevel: 1,
    hm_pass_lvl: 0,
    current_points: 0,
    hover_img: '',
    show_compensate_history: false,
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         : return ({ ...state, sessionKey: action.value });
        case 'ACCOUNT_AUTHED'       : return ({ ...state, userData: action.value });
        case 'RECEIVE_SESSION_KEY'  : return ({ ...state, sessionKey: action.value });
        case 'SET_LOGIN_URL'        : return ({ ...state, loginUrl: action.value });
        case 'SET_LOGOUT_URL'       : return ({ ...state, logoutUrl: action.value });
        case 'ACCOUNT_LOGIN'        : return ({ ...state, authState: "LOGGED_IN" });
        case 'ACCOUNT_LOGOUT'       : return ({ ...state, authState: "LOGGED_OUT" });
        case 'ACCOUNT_AUTH_CHECKED' : return ({ ...state, authed: true });
        case 'SET_JWT_TOKEN'        : return ({ ...state, jwtToken: action.value });

        case "SET_VALUE"            : return (Object.keys(defaultState).indexOf(action.key) >= 0) ? { ...state, [action.key]: action.value } : state;
        case "SET_VALUES"           : return { ...state, ...action.value };
        default:
            return state;
    }
}

export const onAccountAuth          = (sessionKey)  => ({ type: 'ACCOUNT_AUTH',         value: sessionKey });
export const onAccountAuthed        = (userData)    => ({ type: 'ACCOUNT_AUTHED',       value: userData });
export const onAccountLogin         = ()            => ({ type: 'ACCOUNT_LOGIN',        value: "" });
export const onAccountLogout        = ()            => ({ type: 'ACCOUNT_LOGOUT',       value: "" });
export const receiveSessionKey      = (sessionKey)  => ({ type: 'RECEIVE_SESSION_KEY',  value: sessionKey });
export const setLoginUrl            = (url)         => ({ type: 'SET_LOGIN_URL',        value: url });
export const setLogoutUrl           = (url)         => ({ type: 'SET_LOGOUT_URL',       value: url });
export const onAccountAuthChecked   = ()            => ({ type: 'ACCOUNT_AUTH_CHECKE',  value: true })
export const setJwtToken            = (token)       => ({ type: 'SET_JWT_TOKEN',        value: token });

export const setValue               = (key, value) => ({ type: "SET_VALUE", value, key });
export const setValues              = (value) => ({ type: "SET_VALUES", value });
