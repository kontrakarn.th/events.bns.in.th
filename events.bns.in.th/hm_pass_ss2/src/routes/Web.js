import React, {Component} from 'react';
import {Route} from 'react-router-dom';
//import OauthMiddleware from './../middlewares/OauthMiddleware';
import OauthMiddleware from './../features/oauthLogin';
import Modals from './../features/modals';

import Home			from './../pages/Home';
import History  			from './../pages/History';
import DiamondHistory	from './../pages/DiamondHistory'
import ResendHistory		from './../pages/ResendHistory'
import MissionHistory	from './../pages/MissionHistory'
import LevelReward 			from '../pages/LevelReward';

export class Web extends Component {
	render() {
		return (
			<>
				<Route path={process.env.REACT_APP_EVENT_PATH} component={OauthMiddleware} />
				<Route path={process.env.REACT_APP_EVENT_PATH} component={Modals} />

				<Route path={process.env.REACT_APP_EVENT_PATH} exact component={Home} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/history'} exact component={History} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/diamond-history'} exact component={DiamondHistory} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/resend-history'} exact component={ResendHistory} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/mission-history'} exact component={MissionHistory} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/level'} exact component={LevelReward} />
			</>
		);
	}
}
