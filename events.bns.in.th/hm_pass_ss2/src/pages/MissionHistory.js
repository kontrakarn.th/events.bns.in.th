import React from 'react';

import F11Layout from '../features/F11Layout';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { apiPost } from '../middlewares/Api';
import { setValues } from '../store/redux';
import ScrollArea from 'react-scrollbar';
import {Imglist} from '../constants/Import_Images';
import Menu from '../features/menu';
import { connect } from 'react-redux';

export class MissionHistory extends React.Component {
     // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
     apiGetHistory(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'history'};
        let successCallback = (data) => {
            this.props.setValues({
                ...data.data,
                modal_open: ''
            });
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_ITEM_HISTORY", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiGetHistory();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiGetHistory();
        }
    }
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
            >
               <PageWrapper>
                <Menu page='missionhistory'/>
                <TitleBox>
                    <img src={Imglist.history_mission_header}/>
                </TitleBox>
                <ContentFrame>
                    <ScrollArea
                        speed={0.8}
                        className="area"
                        contentClassName=""
                        horizontal={false}
                        style={{ width: '100%', height: '100%', opacity: 1 }}
                        verticalScrollbarStyle={{ backgroundColor: "#000000", opacity: 1 }}
                    >
                        <ul className="list">
                            {
                                this.props.points_histories && this.props.points_histories.length > 0 &&
                                this.props.points_histories.map((item,index) => {
                                    return(
                                        <li key={`history_${index+1}`}>
                                            <div>{item.title}</div>
                                            <div>{item.date}</div>
                                            <div>{item.time}</div>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </ScrollArea>
                </ContentFrame>
                </PageWrapper>
            </F11Layout>
        )
    }
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(MissionHistory);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['history_bg']}) #11122d;
    width: 1105px;
    height: 700px;
    box-sizing: border-box;
    padding: 65px 0 35px;
    text-align: center;
`
const ContentFrame = styled.div`
    background: top center no-repeat url(${Imglist['history_frame']});
    width: 904px;
    height: 533px;
    display: block;
    margin: 0 auto;
    position: relative;
    box-sizing: border-box;
    padding: 6.5% 6%;
    .list{
        margin: 0;
        padding: 0;
        list-style-type: none;
        height: 100%;
        overflow: auto;
        >li{
            color: #ffffff;
            display: flex;
            justify-content: space-between;
            align-items: center;
            white-space: nowrap;
            >div:first-child{
                width: 60%;
                text-align: left;
            }
            >div:nth-child(2){
                width: 30%;
                text-align: center;
            }
            /* >div:last-child{
                width: 15%;
                text-align: center;
            } */
        }
        .color{
            color: #00po;
            font-size: 10px;
            font-weight: bold;
            positionm: relative;
        }
    }
`
const TitleBox = styled.div`
    margin-bottom: 10px;
`
const CustomBtn = styled.div`
    width: 225px;
    height: 55px;
    background: top center no-repeat url(${Imglist['btn']});
    color: #ffffff;
    cursor: pointer;
    font-size: 1.5em;
    line-height: 2em;
    font-weight: 600;
    margin: 9px auto 0;
    &:hover{
        background-position: bottom center;
    }
`