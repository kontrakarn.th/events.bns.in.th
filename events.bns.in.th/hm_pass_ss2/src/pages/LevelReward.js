import React from 'react';
import { connect } from 'react-redux';
import F11Layout from '../features/F11Layout';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { apiPost } from '../middlewares/Api';
import { setValues, onAccountLogout } from '../store/redux';
import ScrollArea from 'react-scrollbar';
import { Imglist } from '../constants/Import_Images';
import Menu from '../features/menu';
import Levelbar from '../features/levelbar';
import Slider from 'react-slick';
import ModalUnlock from '../features/modals/ModalUnlock';
import ModalBuyLevel from '../features/modals/ModalBuyLevel';
import ModalConfirm from '../features/modals/ModalConfirm';
import ModalConfirmPurchaseLevel from '../features/modals/ModalConfirmPurchaseLevel';
import ModalConfirmUnlock from '../features/modals/ModalConfirmUnlock';

const ArrowNext = (props) => {
    return (
        <div
            className="arrow--next"
            onClick={props.onClick}
        />
    )
}
const ArrowPrev = (props) => {
    return (
        <div
            className="arrow--prev"
            onClick={props.onClick}
        />
    )
}

const position_popup_hover = {
    t: { left: "0px", bottom: "0px" },
    t0: { left: "80px", bottom: "90px" },
    t1: { left: "200px", bottom: "90px" },
    t2: { left: "350px", bottom: "90px" },
    t3: { left: "500px", bottom: "90px" },
    t4: { left: "650px", bottom: "90px" },
    t5: { left: "520px", bottom: "90px" },
    b0: { left: "80px", bottom: "100px" },
    b1: { left: "200px", bottom: "100px" },
    b2: { left: "350px", bottom: "100px" },
    b3: { left: "500px", bottom: "100px" },
    b4: { left: "650px", bottom: "100px" },
    b5: { left: "520px", bottom: "100px" },

};
export class LevelReward extends React.Component {
    constructor(props) {
        super(props);

    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    hoverImgFree(img, key, type) {
        let show = type + (key - this.props.slide_show);
        show = (type === "st") ? "t5" : show;
        show = (type === "sb") ? "b5" : show;
        this.props.setValues({ hover_img: img, popup_show: show });
    }
    hoverImgPaid(img, key) {
        this.props.setValues({ hover_img: img, popup_show: key });
    }
    apiClaimAll() {
        let dataSend = { type: "claim_all" };
        let successCallback = (res) => {
            if (res.status) {
                this.props.setValues({
                    ...res.data,
                    modal_open: 'message',
                    modal_message: res.message
                })
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_CLAIM_ALL", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiClaimReward(id) {
        let dataSend = { type: "claim_reward", id };
        let successCallback = (res) => {
            if (res.status) {
                this.props.setValues({
                    ...res.data,
                    modal_open: 'message',
                    modal_message: res.message
                })
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_CLAIM_REWARD", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    actConfirmItem() {
        this.props.setValues({
            modal_open: 'message',
            modal_message: '<div>ได้รับ <span class="green">xxxxx</span><br/>ไอเทมถูกส่งเข้ากล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อกดรับ</div>'
        })
    }

    apiCheckin() {
        this.props.setValues({ modal_open: "loading" });
        let dataSend = { type: 'event_info' };
        let successCallback = (res) => {
            if (res.status) {
                if (res.data && !res.data.selected_char && res.data.characters.length === 0) {
                    this.props.setValues({
                        ...res.data,
                        modal_open: "message",
                        modal_message: "ไม่มีตัวละครในเกม",
                    })
                } else {
                    this.props.setValues({
                        ...res.data,
                        modal_open: ""
                    })
                }
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiPurchaseLevel(level) {
        let dataSend = { type: "purchase_level", level };
        let successCallback = (res) => {
            if (res.status) {
                this.props.setValues({
                    ...res.data,
                    modal_open: "",
                    uplevel: 1,
                })
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_PURCHASE_LEVEL", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiUnlockPackage() {
        let dataSend = { type: "unlock_package" };
        let successCallback = (res) => {
            if (res.status) {
                this.props.setValues({
                    ...res.data,
                    modal_open: "",
                })
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_UNLOCK_PACKAGE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiUnlockAdvancedPackage() {
        let dataSend = { type: "unlock_adv_package" };
        let successCallback = (res) => {
            if (res.status) {
                this.props.setValues({
                    ...res.data,
                    modal_open: "",
                })
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_UNLOCK_ADVANCED_PACKAGE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiClaimSpecialGift() {
        console.log('test')
        let dataSend = { type: "claim_special_gift" };
        let successCallback = (res) => {
            if (res.status) {
                this.props.setValues({
                    ...res.data,
                    modal_open: "message",
                    modal_message: res.message,
                })
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_CLAIM_SPECIAL_GIFT", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    componentWillMount() {
        if (this.props.jwtToken !== "") {
            this.apiCheckin();
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== "") {
            this.apiCheckin();
        }
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========

    render() {
        const settings = {
            dots: false,
            infinite: false,
            speed: 500,
            slidesToShow: 5,
            slidesToScroll: 5,
            nextArrow: <ArrowNext />,
            prevArrow: <ArrowPrev />,
            afterChange: (index) => {
                // let ary = [6,6,1,6,9,6,6,10,10,10];
                // this.props.setValues({special_item:ary[index]})
                let special_item = this.props.special_items[0];
                for (let i = 0; i < this.props.special_items.length; i++) {
                    if (index + 4 < this.props.special_items[i]) {
                        special_item = this.props.special_items[i];
                        break;
                    }
                }
                this.props.setValues({ special_item: special_item, slide_show: index });
            }
        };

        const bigReward = this.props.rewards[this.props.special_item - 1] || {};

        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
            >
                <PageWrapper>
                    <Menu page='level' />

                    <ContentFrame>
                        <Levelbar />
                        <TableWrapper>
                            <div className="slidewrapper">
                                <Slider {...settings}>
                                    {
                                        this.props.rewards.map((item, key) => {
                                            return (
                                                <div className="item" key={key}>
                                                    <div className={"item__level " + (item.free_reward_status == 'locked' && item.status == 'locked' ? "locked" : "")}>{item.required_lvl}</div>
                                
                                                    <div className={"item__paid item__style " + (item.status == 'claimed' ? "received" : "") + (item.status == 'locked' ? "locked" : "") + (item.status == 'not_in_condition' ? "not_in_condition" : "")} onClick={item.status == 'not_in_condition' || item.status == 'locked' || item.status == 'claimed' ? () => {} : () => this.apiClaimReward(item.id)} onMouseOver={() => this.hoverImgFree(item.hover, key, "t")} onMouseOut={() => this.props.setValues({ hover_img: '' })}>
                                                        <img src={Imglist['paid_' + item.icon]} />
                                                    </div>
                                                    {
                                                        item.free_reward_id ?
                                                            <div className={"item__free item__style " + (item.free_reward_status == 'claimed' ? "received" : "") + (item.free_reward_status == 'locked' ? "locked" : "")} onClick={item.free_reward_status == 'locked' || item.free_reward_status == 'claimed' ? () => {} : () => this.apiClaimReward(item.free_reward_id)} onMouseOver={() => this.hoverImgFree(item.free_reward_hover, key, "b")} onMouseOut={() => this.props.setValues({ hover_img: '' })}>
                                                                <img src={Imglist['free_' + item.free_reward_icon]} />
                                                            </div> : <div className="item__free item__style"></div>
                                                    }
                                                </div>
                                            )
                                        })
                                    }
                                </Slider>
                                <HoverFree
                                    src={Imglist[this.props.hover_img]}
                                    show={this.props.slide_show}
                                    left={position_popup_hover[this.props.popup_show] ? position_popup_hover[this.props.popup_show].left : '0px'}
                                    bottom={position_popup_hover[this.props.popup_show] ? position_popup_hover[this.props.popup_show].bottom : '0px'}
                                    // {position_popup_hover[this.props.popup_show - this.props.slide_show]}
                                    id="hover_img"
                                />
                            </div>
                            {/* ===== special item ===== */}
                            <div className="special">
                                <div className="item">
                                    <div className="item__level">{bigReward ? bigReward.required_lvl : 0}</div>
                                    <div className={"item__paid item__style " + (bigReward && bigReward.status == 'claimed' ? "received" : "") + (bigReward && bigReward.status == 'locked' ? "locked" : "") + (bigReward && bigReward.status == 'not_in_condition' ? "not_in_condition" : "")} onClick={bigReward.status == 'claimed' || bigReward.status == 'locked' || bigReward.status == 'not_in_condition' ? () => {} : () => this.apiClaimReward(bigReward.id)} onMouseOver={() => this.hoverImgFree(bigReward.hover, 5, "st")} onMouseOut={() => this.props.setValues({ hover_img: '' })}>
                                        <img src={bigReward ? Imglist['paid_' + bigReward.icon] : ''} />
                                    </div>
                                    <div className={"item__free item__style " + (bigReward && bigReward.free_reward_status == 'claimed' ? "received" : "") + (bigReward && bigReward.free_reward_status == 'locked' ? "locked" : "")} onClick={bigReward.free_reward_status == 'claimed' || bigReward.free_reward_status == 'locked' ? () => {} : () => this.apiClaimReward(bigReward.free_reward_id)} onMouseOver={() => this.hoverImgFree(bigReward.free_reward_hover, 5, "sb")} onMouseOut={() => this.props.setValues({ hover_img: '' })} >
                                        <img src={bigReward ? Imglist['free_' + bigReward.free_reward_icon] : ''} />
                                    </div>
                                </div>
                            </div>
                            {/* ===== end special item ===== */}
                        </TableWrapper>
                        <BtnWrapper>
                            {
                                this.props.is_unlocked == 1 || this.props.is_adv_unlocked == 1
                                    ? <div className={"btn_receive_all " + (this.props.can_claim_all ? "" : "disabled")} onClick={() => this.apiClaimAll()}>รับทั้งหมด</div>
                                    :
                                    <div className="btn_receive_all" onClick={() => this.props.setValues({ modal_open: 'unlock' })}>ปลดล็อกคัมภีร์ล้ำค่า</div>

                            }
                            {
                                this.props.claim_special_gift_status == 'claimed' && <div className={"btn_receive disabled"}>รับของขวัญแล้ว</div>
                            }
                            {
                                this.props.claim_special_gift_status == 'not_in_condition' && <div className={"btn_receive disabled"}  >รับของขวัญ สำหรับผู้มั่งคั่ง</div>
                            }
                            {
                                this.props.claim_special_gift_status == 'can_claim' && <div className={"btn_receive"} onClick={() => { this.apiClaimSpecialGift() }}>รับของขวัญ สำหรับผู้มั่งคั่ง</div>
                            }
                            <div className="hover"><img src={Imglist.Hover_50} /></div>
                        </BtnWrapper>
                    </ContentFrame>
                    <ModalUnlock />
                    <ModalBuyLevel minrange={1} maxrange={60 - this.props.hm_pass_lvl} />
                    <ModalConfirmPurchaseLevel actConfirm={this.apiPurchaseLevel.bind(this)} />
                    <ModalConfirmUnlock
                        unlockPackage={this.apiUnlockPackage.bind(this)}
                        unlockAdvancedPackage={this.apiUnlockAdvancedPackage.bind(this)}
                    />
                </PageWrapper>
            </F11Layout>
        )
    }
}
const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect(mapStateToProps, mapDispatchToProps)(LevelReward);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['other_bg']}) #11122d;
    width: 1105px;
    height: 700px
    box-sizing: border-box;
    padding: 70px 0 20px;
    text-align: center;
`
const ContentFrame = styled.div`
    //background: top center no-repeat url(${Imglist['history_frame']});
    width: 904px;
    height: 533px;
    display: block;
    margin: 0 auto;
    position: relative;
    box-sizing: border-box;
`
const TableWrapper = styled.div`
    margin-top: 30px;
    position: relative;
    img{
        max-width: 100%;
        max-height: 100%;
    }
    .slidewrapper{
        width: 80%;
        display: inline-block;
    }
    .special{
        vertical-align: top;
        width: 20%;
        display: inline-block;
        transform: scale(1.2);
    }
    .item{
        & img{
            width: 100%;
        }     
        &__level{
            background: top center no-repeat url(${Imglist['level_active']});
            width: 146px;
            height: 42px;
            font-size: 1.5em;
            line-height: 1.7;
            box-sizing: border-box;
            &.locked{
                filter: grayscale(100%);
            }
        }
        &__style{
            &.received{
                &:before{
                    content: '';
                    background-color: rgba(0,0,0,0.8);
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    z-index: 2;
                }
                &:after{
                    content: '';
                    background: top center no-repeat url(${Imglist['icon_check']});
                    width: 42px;
                    height: 34px;
                    z-index: 3;
                    position: absolute;
                    left: 0;
                    right: 0;
                    top: 50%;
                    transform: translateY(-50%);
                    -webkit-transform: translateY(-50%);
                    margin:  0 auto;
                }
            }
            &.locked{
                cursor: default;
                &:before{
                    content: '';
                    background-color: rgba(0,0,0,0.8);
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    z-index: 2;
                }
                &:after{
                    content: '';
                    background: top center no-repeat url(${Imglist['lock_icon']});
                    width: 42px;
                    height: 34px;
                    z-index: 3;
                    position: absolute;
                    left: 0;
                    right: 0;
                    top: 50%;
                    transform: translateY(-50%);
                    -webkit-transform: translateY(-50%);
                    margin:  0 auto;
                }
            }  
            &.not_in_condition{
                cursor: default;
                &:before{
                    content: '';
                    // background-color: rgba(0,0,0,0.8);
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    z-index: 2;
                }
                &:after{
                    content: '';
                    background: top center no-repeat url(${Imglist['lock_icon']});
                    width: 42px;
                    height: 34px;
                    z-index: 3;
                    position: absolute;
                    left: 0;
                    right: 0;
                    top: 50%;
                    transform: translateY(-50%);
                    -webkit-transform: translateY(-50%);
                    margin:  0 auto;
                }
            }  
        }
        &__free{
            background: top center no-repeat url(${Imglist['free_active']});
            width: 146px;
            height: 146px;
            padding: 20px;
            box-sizing: border-box;
            position: relative;
            cursor: pointer;
        }
        &__paid{
            background: top center no-repeat url(${Imglist['paid_active']});
            width: 146px;
            height: 146px;
            padding: 20px;
            box-sizing: border-box;
            position: relative;
            cursor: pointer;
        }
    }
    .arrow{
        &--next{
            display: block;
            width: 36px;
            height: 54px;
            background: url(${Imglist['btn_next']}) top center no-repeat;
            top: 50%;
            transform: translate3d(0,-50%,0);
            position: absolute;
            right: -200px;
            z-index: 100;
            cursor:pointer;
        }
        &--prev{
            display: block;
            width: 36px;
            height: 54px;
            background: url('${Imglist['btn_prev']}') top center no-repeat;
            top: 50%;
            transform: translate3d(0,-50%,0);
            left: -50px;
            position: absolute;
            z-index: 100;
            cursor:pointer;
        }
    }
    
`
const BtnWrapper = styled.div`
    display: flex;
    margin-top: 50px;
    justify-content: space-between;
    position: relative;
    .hover{
        position: absolute;
        right: 220px;
        bottom: 0;
        z-index: 100;
        opacity: 0;
        pointer-events: none;
        transition: all 0.5s ease 0s;
        img{
            display: block;
        }
    }

    .btn_receive_all{
        background: url('${Imglist['btn_receive_all']}') top center no-repeat;  
        width: 218px;
        height: 52px;
        line-height: 52px;
        cursor: pointer;
        color: #fff;
        text-shadow: 0 0 3px #000;
        &:hover{
            background-position: bottom center;
        }
        &.disabled{
            pointer-events: none;
            filter: grayscale(1) contrast(0.7);
        } 
    }

    .btn_receive{
        background: url('${Imglist['btn_receive']}') top center no-repeat;  
        width: 218px;
        height: 52px;
        line-height: 52px;
        cursor: pointer;
        color: #fff;
        text-shadow: 0 0 3px #000;
        &:hover{
            background-position: bottom center;
        }
        &.disabled{
            filter: grayscale(100%);
            cursor: default;
            // pointer-events: none;
        } 
        &:hover + .hover{
            opacity: 1;
        } 
    }

`

const HoverFree = styled.img`
    position: absolute;
    opacity: ${props => (props.src) ? "1" : "0"};
    width: ${props => (props.src) ? "300px" : "0px"};
    max-height: unset;

    left: ${props => props.left};
    bottom: ${props => props.bottom};
    z-index: 2;
    pointer-events: none;
    transition: all 0.5s ease 0s;
    max-width: 300px !important;
`;