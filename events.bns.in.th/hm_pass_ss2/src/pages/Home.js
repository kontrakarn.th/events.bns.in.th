import React from 'react';
import { Link } from 'react-router-dom';
import { setValues, onAccountLogout } from '../store/redux';
import { apiPost } from '../middlewares/Api';
import { connect } from 'react-redux';
import F11Layout from './../features/F11Layout/';
//import Main from './../features/main';
import styled from 'styled-components';
import Slider from 'react-slick';


import ModalLoading from '../features/modals/ModalLoading';
import ModalMessage from '../features/modals/ModalMessage';
import ModalConfirm from '../features/modals/ModalConfirm';
import ModalConfirmReceive from '../features/modals/ModalConfirmReceive'
import ModalSelectCharacter from '../features/modals/ModalSelectCharacter';
import ModalBuyLevel from '../features/modals/ModalBuyLevel';

import { Imglist } from '../constants/Import_Images';
import Menu from '../features/menu';
import Levelbar from '../features/levelbar';
import ModalConfirmChar from '../features/modals/ModalConfirmChar';
import ModalConfirmPurchaseLevel from '../features/modals/ModalConfirmPurchaseLevel';

const ArrowNext = (props) => {
    return (
        <div
            className="arrow--next"
            onClick={props.onClick}
        />
    )
}
const ArrowPrev = (props) => {
    return (
        <div
            className="arrow--prev"
            onClick={props.onClick}
        />
    )
}

export class Home extends React.Component {
    actConfirm() {
        this.apiRedeem(
            this.props.modal_confirmitem && this.props.modal_confirmitem !== ''
            && this.props.modal_confirmitem
        );
    }

    apiPurchaseLevel(level) {
        let dataSend = { type: "purchase_level", level };
        let successCallback = (res) => {
            if (res.status) {
                this.props.setValues({
                    ...res.data,
                    modal_open: "",
                    uplevel: 1,
                })
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_PURCHASE_LEVEL", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSelectCharacter(id) {
        let dataSend = { type: "select_character", id };
        let successCallback = (res) => {
            if (res.status) {
                this.props.setValues({
                    ...res.data,
                    modal_open: "message",
                    modal_message: "เลือกตัวละครแล้ว"
                })
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin() {
        this.props.setValues({ modal_open: "loading" });
        let dataSend = { type: 'event_info' };
        let successCallback = (res) => {
            if (res.status) {
                if (res.data && !res.data.selected_char && res.data.characters.length === 0) {
                    this.props.setValues({
                        ...res.data,
                        modal_open: "message",
                        modal_message: "ไม่มีตัวละครในเกม",
                    })
                } else {
                    this.props.setValues({
                        ...res.data,
                        modal_open: ""
                    })
                }
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            } else {
                this.props.setValues({
                    modal_open: "message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
        }
    }
    componentWillMount() {
        if (this.props.jwtToken !== "") {
            this.apiCheckin();
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== "") {
            this.apiCheckin();
        }
    }
    render() {
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <ArrowNext />,
            prevArrow: <ArrowPrev />
        };
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
            >
                <PageWrapper>
                    <Menu page='main' />
                    <EnterBtn
                        onClick={() => {
                            this.props.selected_char ?
                                this.props.history.push(process.env.REACT_APP_EVENT_PATH + '/level') :
                                this.props.setValues({
                                    modal_open: "selectcharacter"
                                })
                        }} />
                    <Levelbar />
                    {this.props.username && (
                        <DailyWrapper>
                            <img src={Imglist.daily_header} />
                            <div className="daily__list">
                                {
                                    this.props.daily_quests.map((item, key) => {
                                        return (
                                            <div className={"daily__item " + (item.status === 'success' ? 'received' : '')} key={key}>
                                                <div className="daily__itemInner">
                                                    <div className="count">{item.completed_count}/{item.quest_limit}</div>
                                                    <img src={Imglist[item.img]} />
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </DailyWrapper>
                    )}
                    {this.props.username && (
                        <WeekWrapper>
                            <img src={Imglist.week_header} />
                            <div className="week__list">
                                {
                                    this.props.weekly_quests.map((item, key) => {
                                        return (
                                            <div className={"week__item " + (item.status === 'success' ? 'received' : '')} key={key}>
                                                <div className="week__itemInner">
                                                    <div className="count">{item.completed_count}/{item.quest_limit}</div>
                                                    <img src={Imglist[item.img]} />
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </WeekWrapper>
                    )}
                    <CostumeWrapper>
                        <img src={Imglist.costume_header} />
                        <div className="costume_slide">
                            <div className="slidebox">
                                <Slider {...settings}>
                                    {
                                        this.props.costume_slider.map((item, index) => {
                                            return (
                                                <div className="slider" key={`item_${index + 1}`}>
                                                    <img className="slider__img" src={item.img} alt="" />
                                                </div>
                                            )
                                        })
                                    }
                                </Slider>
                            </div>
                        </div>
                    </CostumeWrapper>
                    <ConditionBoard src={Imglist['condition_frame']} alt='' />
                    <ModalLoading />
                    <ModalMessage />
                    <ModalConfirm actConfirm={this.actConfirm.bind(this)} />
                    <ModalConfirmChar actConfirm={this.apiSelectCharacter.bind(this)} />
                    <ModalConfirmReceive />
                    <ModalBuyLevel minrange={1} maxrange={60 - parseInt(this.props.hm_pass_lvl)} />
                    <ModalConfirmPurchaseLevel actConfirm={this.apiPurchaseLevel.bind(this)} />
                    <ModalSelectCharacter />
                </PageWrapper>
            </F11Layout>
        )
    }
}
const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_main']}) #070606;
    width: 1105px;
    padding-top: 730px;
    text-align: center;
`

const EnterBtn = styled.div`
    background: top center no-repeat url(${Imglist['btn_enter']});
    width: 310px;
    height: 53px;
    cursor: pointer;
    position: absolute;
    top: 540px;
    left: 0;
    right: 0;
    margin: 0 auto;
    &:hover{
        background-position: bottom center;
    }
`
const ConditionBoard = styled.img`
    display: block;
    margin: 0 auto;
    padding: 140px 0 0;
`


const DailyWrapper = styled.div`
    margin-bottom: 70px;
    .daily{
        &__list{
            display: flex;
            align-items: center;
            justify-content: center;
            width: 920px;
            margin: 0 auto;
        }
        &__itemInner{
            position: relative;
            img{
                display: block;
                max-width: 100%;
                max-height: 100%;
            }
        }
        &__item{
            padding:5px;    
            .count{
                color: #ffcd76;
                position: absolute;
                left: 0;
                right: 0;
                top: 10px;
                margin: 0 auto;
                z-index: 4;
            }
            &.received{
                & .daily__itemInner{
                    &:before{
                        content: '';
                        background-color: rgba(0,0,0,0.8);
                        position: absolute;
                        top: 0;
                        left: 0;
                        width: 100%;
                        height: 100%;
                        z-index: 2;
                        border-radius: 12px;
                    }
                    &:after{
                        content: '';
                        background: top center no-repeat url(${Imglist['icon_check']});
                        width: 42px;
                        height: 34px;
                        z-index: 3;
                        position: absolute;
                        left: 0;
                        right: 0;
                        top: 80px;
                        margin:  0 auto;
                    }
           

                }
            }
        }
       
 
     
    }
    
`
const WeekWrapper = styled.div`
    margin-bottom: 70px;
    .week{
        &__list{
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
            width: 920px;
            margin: 0 auto;
        }
        &__itemInner{
            position: relative;
            img{
                display: block;
                max-width: 100%;
                max-height: 100%;
            }
        }
        &__item{ 
            padding: 5px;
            width: 20%;
            box-sizing: border-box;  
            &.received{
                & .week__itemInner{
                    &:before{
                        content: '';
                        background-color: rgba(0,0,0,0.8);
                        position: absolute;
                        top: 0;
                        left: 0;
                        width: 100%;
                        height: 100%;
                        z-index: 2;
                        border-radius: 12px;
                    }
                    &:after{
                        content: '';
                        background: top center no-repeat url(${Imglist['icon_check']});
                        width: 42px;
                        height: 34px;
                        z-index: 3;
                        position: absolute;
                        left: 0;
                        right: 0;
                        top: 80px;
                        margin:  0 auto;
                    }
                }
            }
            .count{
                color: #ffcd76;
                position: absolute;
                left: 0;
                right: 0;
                top: 10px;
                margin: 0 auto;
                z-index: 4;
            }
        }     
    }
`
const CostumeWrapper = styled.div`
    .costume_slide{
        display: block;
        width: 920px;
        height: 585px;
        margin: 0 auto;
        background: top center no-repeat url(${Imglist['costume_frame']});
        background-size: 100%;
        box-sizing: border-box;
        padding: 15px;
        .slidebox{
            width: 884px;
            position: relative;
            margin: 0 auto;
            .slider{
                &__img{
                    display: block;
                    margin: 0 auto;
                    max-width: 100%;
                    max-height: 100%;
                }
            }
        }
        .arrow{
            &--next{
                display: block;
                width: 36px;
                height: 54px;
                background: url(${Imglist['btn_next']}) top center no-repeat;
                top: 50%;
                transform: translate3d(0,-50%,0);
                position: absolute;
                right: -75px;
            }
            &--prev{
                display: block;
                width: 36px;
                height: 54px;
                background: url('${Imglist['btn_prev']}') top center no-repeat;
                top: 50%;
                transform: translate3d(0,-50%,0);
                left: -75px;
                position: absolute;
            }
        }
        .slick-dots li {
            width: 30px;
            height: 5px;
            & button{
                padding: 2px;
                width: 100%;
                height: 100%;
                &::before{
                    content: '';
                    background-color: #8e6f60;
                    width: 100%;
                    height: 100%;
                    opacity:1;
                }
            }
            &.slick-active button:before{
                background-color: #a99e85;
            }
        }
        .slick-dots{
            bottom: 0;
        }
    }
`

