import React from 'react';
import styled from 'styled-components';
import {Imglist} from './../../constants/Import_Images';

// export const Item = props => {
//     return (
//         <ContentItem className="" name={props.name} count={props.count}>
//         </ContentItem>
//     )
// }

export const Item = styled.div`
    position: relative;
    background: top left no-repeat url(${props=>Imglist[props.name]});
    width: 98px;
    height: 118px;
    display: block;
    box-sizing: border-box;
    margin: 0 auto;
    &::before {
        content: "${props=>props.count}";
        position: absolute;
        bottom: 7px;
        right: 39px;
        display: block;
        width: 25px;
        text-align: center;
        color: #FFFFFF;
        font-size: 0.7em;
        line-height: 3em;
        font-family: 'Kanit-Light';
    }
`;
