import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import { apiPost } from './../../middlewares/Api';
import { Imglist } from './../../constants/Import_Images';

const CPN = props => {
    const actSelectCharacter = (e) => {
        e.preventDefault();
        let charId = props.value_select;
        const selectedChar = props.characters.filter((c) => c.id == charId)[0];
        if (charId !== "") {
            // apiSelectCharacter(indexCharacter);

            props.setValues({
                modal_open: "confirmchar",
                modal_confirm_char_selected: charId,
                modal_message: "<div>ต้องการเลือกตัวละคร<br/> <span class='green'>" + selectedChar.char_name + "</span><br/>อาชีพ <span class='green'>" + selectedChar.job_name + '</span> เลเวล <span class="green">' + selectedChar.level + '</span> ?</div>',
            });

        } else {
            props.setValues({
                modal_open: "message",
                modal_message: "โปรดเลือกตัวละคร",
                selected_char: false,
            });
        }
    }

    function handleChange(e) {
        props.setValues({
            value_select: e.target.value,
        })
    }

    return (
        <ModalCore
            modalName="selectcharacter"
            actClickOutside={false}>

            <ModalMessageContent>
                <div className="">
                    <div className="contenttitle">เลือกตัวละคร</div>
                    <div className="contentwrapper">
                        <div className="contenttext">
                            <div className="modal_close" onClick={() => props.setValues({ modal_open: '' })}><img src={Imglist.modal_close} /></div>
                            <FormSelect>
                                <form className="formselect">
                                    <div className="custom_select">
                                        <select id="charater_form" className="fromselect__list" onChange={(e) => handleChange(e)}>
                                            <option value="" disabled selected>เลือกตัวละคร</option>
                                            {props.characters && props.characters.map((item, index) => {
                                                return (
                                                    <option key={index} value={item.id}>{item.char_name}</option>
                                                )
                                            })}
                                        </select>
                                        {/* <img src={Imglist.icon_dropdown} alt="" /> */}
                                    </div>

                                    {/* <select id="charater_form" className="fromselect__list custom-select" onChange={(e) => handleChange(e)}>
                                        <option value="" disabled selected>เลือกตัวละคร</option>
                                        {props.characters && props.characters.map((item,index)=>{
                                            return (
                                                <option key={index} value={item.id}>{item.char_name}</option>
                                            )
                                        })}
                                    </select> */}
                                </form>
                            </FormSelect>
                        </div>
                        <Btns onClick={(e) => actSelectCharacter(e)}>ตกลง</Btns>
                    </div>
                </div>

                {/* <div>
                    <Btns onClick={(e)=>actSelectCharacter(e)}>
                        ตกลง
                    </Btns>
                    <Btns onClick={()=>props.setValues({modal_open:''})}>
                        ยกเลิก
                    </Btns>
                </div> */}
            </ModalMessageContent>

        </ModalCore>
    )
}


const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(mapStateToProps, mapDispatchToProps)(CPN);


const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    // width: 424px;
    // height: 291px;
    text-align: center;
    color: #FFFFFF;
    //background: no-repeat center url(${Imglist['modal_bg']});
    box-sizing: border-box;
    //padding: 6% 8%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttitle{
        font-family: 'Kanit-Medium';
        font-size: 23px;
    }
    .contentwrapper{
        background: url(${Imglist['modal_bg']}) center/ cover no-repeat ;
        width: 424px;
        height: 291px;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 20px;
        line-height: 1.5em;
        word-break: break-word;
        //min-height: 130px;
        height: 75%;
        display: flex;
        justify-content: center;
        align-items: center;
        position: relative;

    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
    .modal_close{
        right: 15px;
        top: 15px;
        position: absolute;
        z-index: 2;
        cursor:pointer;
    }
`;

const Btns = styled.div`
    background: no-repeat top center url(${Imglist['btn_default']});
    width:116px;
    height:38px;
    line-height: 38px;
    cursor: pointer;
    color: #fff;
    text-shadow: 0 0 3px #000;
    margin: 15px;
    display: inline-block;
    margin: 0 15px;
    font-size: 18px;
    &:hover{
        background-position: bottom center;
    }
`

const FormSelect = styled.div`
    .formselect {
        margin: 0px auto;
        .custom_select {
            position: relative;
            color: #fff;
            background-color: #000;
            width: 332px;
            height: 35px;
            display: block;
            margin: 10px auto;
            padding: 5px 10px;
            overflow: hidden;
            &:focus {
                outline: 0 none;
            }
        }
        select {
            font-family: 'Kanit-Light';
            font-size: 21px;
            border: 0px;
            background: #000;
            vertical-align: middle;
            width: 100%;
            height: 100%;
            z-index: 1;
            color: inherit;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            background: no-repeat right center url(${Imglist['icon_dropdown']});
            &:focus {
                outline: 0 none;
            }
            option{
                background-color: #000;
            }
        }
        img {
            position: absolute;
            display: block;
            top: 50%;
            // width: 24px;
            // height: 21px;
            right: 0px;
            transform: translate( -50% , -50% );
            user-select: none;
        }
    }


   
`