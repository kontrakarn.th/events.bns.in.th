import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import {Imglist} from './../../constants/Import_Images';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="receive"
            actClickOutside={false}
        >
            <ModalMessageContent>
                <div className="contenttitle">ยืนยัน</div>
                <div className="contentwrapper">
                    <div className="contenttext" dangerouslySetInnerHTML={{__html: modal_message}} />
                    <BtnWrapper>
                        <Btns onClick={()=>{if(props.actConfirm) props.actConfirm()}}>ยืนยัน</Btns>
                        <Btns onClick={()=>props.setValues({modal_open:''})}>ยกเลิก</Btns>
                    </BtnWrapper>
                </div>  
            </ModalMessageContent>  
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    text-align: center;
    color: #FFFFFF;
    box-sizing: border-box;
    .contenttitle{
        font-family: 'Kanit-Medium';
        font-size: 23px;
    }
    .contentwrapper{
        background: url(${Imglist['modal_bg']}) center/ cover no-repeat ;
        width: 424px;
        height: 291px;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 20px;
        line-height: 1.5em;
        word-break: break-word;
        //min-height: 130px;
        height: 75%;
        display: flex;
        justify-content: center;
        align-items: center;

    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
    .green{
        color: #6fc96d;
    }
`;

const BtnWrapper = styled.div`
    display: block;
    text-align: center;
`;

const Btns = styled.div`
    background: no-repeat top center url(${Imglist['btn_default']});
    width:116px;
    height:38px;
    line-height: 38px;
    cursor: pointer;
    color: #fff;
    text-shadow: 0 0 3px #000;
    margin: 15px;
    display: inline-block;
    margin: 0 15px;
    font-size: 18px;
    &:hover{
        background-position: bottom center;
    }
`
