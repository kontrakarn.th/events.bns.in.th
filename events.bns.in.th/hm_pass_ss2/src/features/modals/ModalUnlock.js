import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import { Imglist } from '../../constants/Import_Images';

const CPN = props => {
    let { modal_message } = props;
    return (
        <ModalCore
            modalName="unlock"
            actClickOutside={false}
        >
            <ModalMessageContent>
                <div className="contenttitle">ปลดล็อกคัมภีร์ล้ำค่า</div>
                <div className="contentwrapper">
                    <div className="modal_close" onClick={() => props.setValues({ modal_open: '' })}><img src={Imglist.modal_close} /></div>
                    <div className="contenttext">
                        {
                            props.unlock_item.map((item, key) => {
                                return (
                                    <div className="item" key={key}>
                                        <img src={item.img} />
                                        <Btns onClick={() => {
                                            props.setValues({
                                                modal_open: "confirmunlock",
                                                unlock_type: item.type,
                                                modal_message: item.message,
                                            })
                                        }}>ซื้อ</Btns>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </ModalMessageContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(mapStateToProps, mapDispatchToProps)(CPN);

const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    // width: 424px;
    // height: 291px;
    text-align: center;
    color: #FFFFFF;
    //background: no-repeat center url(${Imglist['modal_bg']});
    box-sizing: border-box;
    //padding: 6% 8%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttitle{
        font-family: 'Kanit-Medium';
        font-size: 23px;
    }
    .modal_close{
        right: 15px;
        top: 15px;
        position: absolute;
        z-index: 2;
        cursor:pointer;
    }
    .contentwrapper{
        background: url(${Imglist['modal_bg']}) center/ cover no-repeat ;
        width: 424px;
        height: 291px;
        position: relative;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 20px;
        line-height: 1.5em;
        word-break: break-word;
        //min-height: 130px;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        box-sizing: border-box;
        padding: 8% 12%;
   
        img{
            max-width: 100%;
            max-height: 100%;
        }

    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;

const BtnWrapper = styled.div`
    display: block;
    // position: absolute;
    // bottom: -55px;
    // z-index: 50;
    // left: 0;
    // right: 0;
    text-align: center;
`;

const Btns = styled.div`
    background: no-repeat top center url(${Imglist['btn_default']});
    width:116px;
    height:38px;
    line-height: 38px;
    cursor: pointer;
    color: #fff;
    text-shadow: 0 0 3px #000;
    margin: 15px;
    display: inline-block;
    margin: 0 15px;
    font-size: 18px;
    &:hover{
        background-position: bottom center;
    }
`
