import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import { Imglist } from '../../constants/Import_Images';
import InputRange from 'react-input-range';

const CPN = props => {
    let { modal_message } = props;
    const purchasedLevelDiamonds = 5000;

    const actClickMinus = (count) => {
        if (count > props.minrange) {
            props.setValues({
                uplevel: count - 1
            });
        }
    }
    const actClickPlus = (count) => {
        if (count < props.maxrange) {
            props.setValues({
                uplevel: count + 1
            });
        }
    }
    const requiredDiamonds = (props.uplevel * purchasedLevelDiamonds).toLocaleString('en-EN');

    return (
        <ModalCore
            modalName="buylevel"
            actClickOutside={false}
        >
            <ModalMessageContent>
                <div className="contenttitle">ซื้อเลเวลเพิ่ม</div>
                <div className="contentwrapper">
                    <div className="contenttext">
                        <div className="modal_close" onClick={() => props.setValues({ modal_open: '', uplevel: 1 })}><img src={Imglist.modal_close} /></div>
                        <div>ซื้อถึง LV.<span className="green">{parseInt(props.hm_pass_lvl) + parseInt(props.uplevel)}</span></div>
                        <div className="sliderRange">
                            <div onClick={() => actClickMinus(props.uplevel)}><img src={Imglist.icon_minus} /></div>
                            <InputRange
                                maxValue={props.maxrange}
                                minValue={props.minrange}
                                value={props.uplevel}
                                onChange={value => props.setValues({ uplevel: value })}
                            />
                            <div onClick={() => actClickPlus(props.uplevel)}><img src={Imglist.icon_plus} /></div>
                        </div>
                        <div><span className="green">{requiredDiamonds}</span> ไดมอนด์</div>

                    </div>
                    <BtnWrapper>
                        <Btns onClick={() => {
                            props.setValues({
                                modal_open: "confirmpurchaselevel",
                                modal_message: "<div>ปลดล็อก <span class='green'>" + props.uplevel + "</span> เลเวล<br/>ราคา <span class='green'>" + requiredDiamonds + '</span> ไดมอนด์ ?</div>',
                            })
                        }}>ซื้อ</Btns>
                    </BtnWrapper>
                </div>
            </ModalMessageContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(mapStateToProps, mapDispatchToProps)(CPN);

const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    text-align: center;
    color: #FFFFFF;
    box-sizing: border-box;
    img{
        max-width: 100%;
        max-height: 100%;
        display:block;
    }
    .modal_close{
        right: 15px;
        top: 15px;
        position: absolute;
        z-index: 2;
        cursor:pointer;
    }
    .contenttitle{
        font-family: 'Kanit-Medium';
        font-size: 23px;
    }
    .contentwrapper{
        background: url(${Imglist['modal_bg']}) center/ cover no-repeat ;
        width: 424px;
        height: 291px;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 20px;
        line-height: 1.5em;
        word-break: break-word;
        //min-height: 130px;
        height: 75%;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        position: relative;
    }
    .green{
        color: #6fc96d;
    }
    .sliderRange{
        width: 80%;
        margin: 40px auto 20px;
        display: flex;
        align-items: center;  
        > div{
            cursor: pointer;
        }  
    }
    .input-range{
        &__track{
           background-color: #000;
            border-radius: 0;
            height: 7px;
            &--active{
                background-color: #724722; 
            }
        }
        &__slider{
            background: url(${Imglist['icon_slider_range']}) center no-repeat ;
            border:0;
            border-radius: 0;
            width: 5px;
            height: 22px;
            margin-left: 0;
            margin-top:-1rem;
        }
        &__label--min,
        &__label--max{
            display: none;
        }
        &__label-container{
            display: none;
        }
    }
`;

const BtnWrapper = styled.div`
    display: block;
    text-align: center;
`;

const Btns = styled.div`
    background: no-repeat top center url(${Imglist['btn_default']});
    width:116px;
    height:38px;
    line-height: 38px;
    cursor: pointer;
    color: #fff;
    text-shadow: 0 0 3px #000;
    margin: 15px;
    display: inline-block;
    margin: 0 15px;
    font-size: 18px;
    &:hover{
        background-position: bottom center;
    }
`
