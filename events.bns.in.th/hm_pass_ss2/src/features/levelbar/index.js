import React from 'react';
import { connect } from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { setValues } from './../../store/redux';

import { Imglist } from './../../constants/Import_Images';
import Menu from '../../features/menu';
import ScrollArea from 'react-scrollbar';
import 'react-input-range/lib/css/index.css';

class Levelbar extends React.Component {
    render() {
        return (
            <LevelWrapper>
                <div className="level">{this.props.hm_pass_lvl}</div>
                <div>
                    <div className="count">
                        <img src={Imglist.icon_coffee} />
                        ผลงานปัจจุบัน {this.props.current_points}/200
                    </div>
                    <div className="progress">
                        <div className="progress_bar" style={{ width: this.props.current_points * 0.5 + '%' }}></div>
                    </div>
                </div>
                <div className={"btn " + (this.props.can_purchase_level ? "" : "disabled")} onClick={() => this.props.setValues({ modal_open: 'buylevel' })}>ซื้อ LV. เพิ่ม</div>
            </LevelWrapper>
        )
    }
}
const mapStateToProps = state => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(mapStateToProps, mapDispatchToProps)(Levelbar);

const LevelWrapper = styled.div`
    display: flex;
    align-items: flex-end;
    justify-content: center;
    .level{
        background: top center no-repeat url(${Imglist['level']}); 
        width: 88px;
        height: 64px; 
        line-height: 64px;
        font-size: 1.8em; 
        display: inline-block;
    }
    .count{
        text-align: left;
        img{
            vertical-align: middle;
        }
    }
    .progress_bar{
        float: left;
        width: 0%;
        height: 100%;
        -webkit-transition: width .6s ease;
        -o-transition: width .6s ease;
        transition: width .6s ease;
        background: url(${Imglist['bar']}) top center/cover no-repeat ; 
        // background: rgba(62,22,15,1);
        // background: -moz-linear-gradient(left, rgba(62,22,15,1) 0%, rgba(62,22,15,1) 24%, rgba(148,90,49,1) 54%, rgba(218,185,151,1) 78%, rgba(148,90,49,1) 100%);
        // background: -webkit-gradient(left top, right top, color-stop(0%, rgba(62,22,15,1)), color-stop(24%, rgba(62,22,15,1)), color-stop(54%, rgba(148,90,49,1)), color-stop(78%, rgba(218,185,151,1)), color-stop(100%, rgba(148,90,49,1)));
        // background: -webkit-linear-gradient(left, rgba(62,22,15,1) 0%, rgba(62,22,15,1) 24%, rgba(148,90,49,1) 54%, rgba(218,185,151,1) 78%, rgba(148,90,49,1) 100%);
        // background: -o-linear-gradient(left, rgba(62,22,15,1) 0%, rgba(62,22,15,1) 24%, rgba(148,90,49,1) 54%, rgba(218,185,151,1) 78%, rgba(148,90,49,1) 100%);
        // background: -ms-linear-gradient(left, rgba(62,22,15,1) 0%, rgba(62,22,15,1) 24%, rgba(148,90,49,1) 54%, rgba(218,185,151,1) 78%, rgba(148,90,49,1) 100%);
        // background: linear-gradient(to right, rgba(62,22,15,1) 0%, rgba(62,22,15,1) 24%, rgba(148,90,49,1) 54%, rgba(218,185,151,1) 78%, rgba(148,90,49,1) 100%);
        // filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3e160f', endColorstr='#945a31', GradientType=1 );
    }
    .progress{
        width: 560px;
        height: 22px;
        margin-bottom: 20px;
        overflow: hidden;
        background-color: #000;
        border: 2px solid #834720;
    }
    .btn{
        background: top center no-repeat url(${Imglist['btn_default']});
        width:116px;
        height:38px;
        line-height: 38px;
        cursor: pointer;
        color: #fff;
        text-shadow: 0 0 3px #000;
        margin: 15px;
        &:hover{
            background-position: bottom center;
        
        }
        &.disabled{
            filter: grayscale(1);
            pointer-events: none;
        }
    }
`
