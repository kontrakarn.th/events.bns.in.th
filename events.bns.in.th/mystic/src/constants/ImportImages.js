const menu_background = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/menu_background.png';
const permission = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/permission.jpg';
const icon_scroll = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/icon_scroll.png';
const btn_home = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_home.png';

// home
const home_background = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/home_background.jpg';
const bg_home_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/bg_home_1.jpg';
const bg_home_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/bg_home_3.jpg';
const btn_join = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_join.png';
const slide_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/slide_1.png';
const slide_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/slide_2.png';
const slide_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/slide_3.png';
const slide_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/slide_4.png';
const slide_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/slide_5.png';
const slide_6 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/slide_6.png';
const slide_7 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/slide_7.png';
const btn_arrow_left = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_arrow_left.png';
const btn_arrow_right = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_arrow_right.png';
const btn_top = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_top.png';


// play
const play_background = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/play_background.jpg';
const slot_selected = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/slot_selected.png';
const animate = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/animate.png';
const btn_select_reward = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_select_reward.png';
const btn_open = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_open.png';
const btn_open_inactive = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_open_inactive.png';
const btn_buy = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_buy.png';
const btn_reset = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_reset.png';
const btn_random_reward = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_random_reward.png';
const btn_selected_reward = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_selected_reward.png';

// history
const history_background = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/history_background.jpg';


// select reward
const modal_select_reward = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/modal_select_reward.png';
const icon_set_1_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_2.png';
const icon_set_1_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_1.png';
const icon_set_1_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_3.png';
const icon_set_1_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_4.png';
const icon_set_1_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_5.png';
const icon_set_1_6 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_6.png';
const icon_set_1_7 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_7.png';
const icon_set_1_8 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_8.png';
const icon_set_1_9 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_9.png';
const icon_set_1_10 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_10.png';
const icon_set_1_11 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_11.png';
const icon_set_1_12 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_12.png';
const icon_set_1_13 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_13.png';
const icon_set_1_14 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_1_14.png';
const icon_set_2_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_2_1.png';
const icon_set_2_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_2_2.png';
const icon_set_2_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_2_3.png';
const icon_set_2_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_2_4.png';
const icon_set_2_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_2_5.png';
const icon_set_3_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_3_1.png';
const icon_set_3_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_3_2.png';
const icon_set_3_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_3_3.png';
const icon_set_3_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_3_4.png';
const icon_set_3_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_3_5.png';
const icon_set_3_6 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_3_6.png';
const icon_set_3_7 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_3_7.png';
const icon_set_3_8 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_3_8.png';
const icon_set_4_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_4_1.png';
const icon_set_4_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_4_2.png';
const icon_set_4_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_4_3.png';
const icon_set_4_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_4_4.png';
const icon_set_4_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_4_5.png';
const icon_set_4_6 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_4_6.png';
const icon_set_4_7 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_4_7.png';
const icon_set_4_8 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_4_8.png';
const icon_set_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_5.png';
const icon_set_6_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_6_1.png';
const icon_set_6_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_6_2.png';
const icon_set_6_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_6_3.png';
const icon_set_6_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_6_4.png';
const icon_set_6_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_6_5.png';
const icon_set_7_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_7_1.png';
const icon_set_7_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_7_2.png';
const icon_set_7_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_7_3.png';
const icon_set_7_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_7_4.png';
const icon_set_7_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_7_5.png';
const icon_set_8_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_8_1.png';
const icon_set_8_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_8_2.png';
const icon_set_8_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_8_3.png';
const icon_set_8_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_8_4.png';
const icon_set_8_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_8_5.png';
const icon_set_9_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_9_1.png';
const icon_set_9_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_9_2.png';
const icon_set_9_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_9_3.png';
const icon_set_9_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_9_4.png';
const icon_set_9_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_9_5.png';
const icon_set_10_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_10_1.png';
const icon_set_10_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/rewards/icon_set_10_2.png';

// modal 
const modal_reward = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/modal_reward.png';
const modal_message = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/modal_message.png';
const modal_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/modal_confirm.png';
const btn_confirm_select = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_confirm_select.png';
const btn_reset_short = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_reset_short.png';
const btn_cancel = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_cancel.png';
const btn_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_confirm.png';
const icon_close = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/icon_close.png';
const btn_buy_key = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_buy_key.png';
const bg_modal_buy_key = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/bg_modal_buy_key.png';


export default {
	permission,
	icon_scroll,
	btn_home,
	menu_background,

	// home
  home_background,
  bg_home_1,
  bg_home_2,
	btn_join,
	slide_1,
	slide_2,
	slide_3,
	slide_4,
	slide_5,
	slide_6,
	slide_7,
	btn_arrow_left,
  btn_arrow_right,
  btn_top,

	// play
	play_background,
	slot_selected,
	animate,
	btn_select_reward,
	btn_open,
	btn_open_inactive,
	btn_buy,
  btn_reset,
  btn_random_reward,
  btn_selected_reward,

	// history
  history_background,
  
  // select reward
  modal_select_reward,
  icon_set_1_2,
  icon_set_1_1,
  icon_set_1_3,
  icon_set_1_4,
  icon_set_1_5,
  icon_set_1_6,
  icon_set_1_7,
  icon_set_1_8,
  icon_set_1_9,
  icon_set_1_10,
  icon_set_1_11,
  icon_set_1_12,
  icon_set_1_13,
  icon_set_1_14,
  icon_set_2_1,
  icon_set_2_2,
  icon_set_2_3,
  icon_set_2_4,
  icon_set_2_5,
  icon_set_3_1,
  icon_set_3_2,
  icon_set_3_3,
  icon_set_3_4,
  icon_set_3_5,
  icon_set_3_6,
  icon_set_3_7,
  icon_set_3_8,
  icon_set_4_1,
  icon_set_4_2,
  icon_set_4_3,
  icon_set_4_4,
  icon_set_4_5,
  icon_set_4_6,
  icon_set_4_7,
  icon_set_4_8,
  icon_set_5,
  icon_set_6_1,
  icon_set_6_2,
  icon_set_6_3,
  icon_set_6_4,
  icon_set_6_5,
  icon_set_7_1,
  icon_set_7_2,
  icon_set_7_3,
  icon_set_7_4,
  icon_set_7_5,
  icon_set_8_1,
  icon_set_8_2,
  icon_set_8_3,
  icon_set_8_4,
  icon_set_8_5,
  icon_set_9_1,
  icon_set_9_2,
  icon_set_9_3,
  icon_set_9_4,
  icon_set_9_5,
  icon_set_10_1,
  icon_set_10_2,

  //modal
  modal_reward,
  modal_message,
  modal_confirm,
  btn_confirm_select,
  btn_reset_short,
  btn_cancel,
  btn_confirm,
  icon_close,
  btn_buy_key,
  bg_modal_buy_key,

}
