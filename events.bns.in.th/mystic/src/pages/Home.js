import React,{useState,useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {setValues} from './../store/redux';
import styled from 'styled-components';
import imgList from './../constants/ImportImages';
import F11Layout from './../features/f11Layout';
import Modals from './../features/modals';
import {apiPost} from './../constants/Api';
import Slider from "react-slick";

const Home = (props) => {
    const [numPage,setNumPage] = useState(0);
    const [slide,setSlide] = useState(null);
    const [random,setRandom] = useState(1);
    const [scroll,setScroll] = useState(null);
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      afterChange: (index) => setNumPage(index),
    };
    const slideList = [
    	imgList.slide_1,
    	imgList.slide_2,
    	imgList.slide_3,
    	imgList.slide_4,
    	imgList.slide_5,
    	imgList.slide_6,
    	imgList.slide_7,
    ] 

    const apiEventInfo = () => {
		let dataSend = {type: "event_info"};
		let successCallback = (res) => {
			if(res.status) {
				props.setValues({
					...res.data,
					modal_open:""
				})
			} 
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		let failCallback = (res) => {
			if (res.type === 'no_permission') {
				props.setValues({
					permission: false
				});
			}
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
    	apiPost("REACT_APP_API_POST_EVENT_INFO", props.jwtToken, dataSend, successCallback, failCallback);
	}
    useEffect(()=>{
		setRandom(Math.floor(Math.random() * 2) + 1);
    },[])
    useEffect(()=>{
		if(props.jwtToken !== "" && !props.status) apiEventInfo();
    },[props.jwtToken])
    return (
        <F11Layout page={"main"} objScroll={(e)=>{if(e) setScroll(e)}}>
            <Modals />
            <HomeStyle bg={random===1?imgList.bg_home_1:imgList.bg_home_2}>
                <BtnTop onClick={()=>scroll.scrollArea.scrollTop()}>
                    <img src={imgList.btn_top} alt=''/>
                </BtnTop>
                <section className="section section--banner">
                    <Btn
                        as={Link}
                        to={process.env.REACT_APP_EVENT_PATH+"/play"}
                        name="join"
                        src={imgList.btn_join}
                    />
                    
                </section>
                <section className="section section--rule"/>
                <section className="section section--reward"/>
                <section className="section section--item">

                    <BtnArrow className="o-left" name="arrow_left" active={false} src={imgList.btn_arrow_left} onClick={()=>slide && slide.slickPrev()} />
                    <div className="slide">
                        <Slider {...settings} ref={c => setSlide(c)}>
                            {slideList.map((item,index)=>{
                                return (
                                    <div key={"slide_"+index}>
                                        <img src={item} alt="" />
                                    </div>
                                )
                            })}
                        </Slider>
                        <div className="paging">
                            {slideList.map((item,index)=>{
                                return (
                                    <a
                                        key={"paging_"+index}
                                        className={numPage === index ? "active" : ""}
                                    />
                                )
                            })}
                        </div>
                    </div>
                    <BtnArrow className="o-right" name="arrow_right" active={false} src={imgList.btn_arrow_right} onClick={()=>slide && slide.slickNext()} />
                </section>
                <section className="section section--other"/>
            </HomeStyle>
        </F11Layout>
    )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(Home);

const HomeStyle = styled.div`
    position: relative;
    background: url(${props=>props.bg});
    height: 6387px;
    .section {
        position: relative;
        display: block;
        &--banner {
            height: 700px;
            padding-top: 413px;
            padding-left: 212px;
        }
        &--rule {
            height: 650px;
        }
        &--reward {
            height: 820px;
        }
        &--item {
            height: 750px;
            padding: 141px 103px 0px;
        }
        &--other {
            height: 3467px;
        }
    }
    .slide {
        position: relative
        width: 900px;
        height: 540px;
    }
    .paging {
        position: absolute;
        left: 50%;
        transform: translate(-50%,0);
        bottom: 13px;
        display: flex;
        justify-content: center;
        align-items: center;
        a {
            display: block;
            width: 29px;
            height: 4px;
            margin: 0px 4px;
            background-color: #FFFFFF;
            opacity: 0.4;
            &.active {
                opacity: 0.8;
            }
        }
    }
`;

const Btn = styled.a`
  display: block;
  width: 230px;
  height: 52px;
  background-image: url(${props=>props.src});
  background-position: top center;
  &:hover {
      background-position: bottom center;
  }
`;
const BtnArrow = styled.a`
    position: absolute
    top: 384px;
    ${props=>props.name=="arrow_left"? "left: 35px;": ""}
    ${props=>props.name=="arrow_right"? "right: 35px;": ""}
    display: block;
    width: 36px;
    height: 54px;
    background-image: url(${props=>props.src});
    background-position: top center;
    cursor: pointer;
    &:hover {
        background-position: bottom center;

    }
`;
const BtnTop = styled.div`
    position: absolute;
    width: 40px;
    right: 15px;
    bottom: 25px;
    cursor: pointer;
    z-index: 10;
    >img{
        width: 100%;
        display: block;
    }
`;