import React,{useState,useEffect} from 'react';
import SlotGacha from './../features/slotGacha';
import imgList from './../constants/ImportImages';
import {connect} from 'react-redux';
import {setValues} from './../store/redux';

const Ani = (props) => {
  const [selectedSlot,setSelectedSlot] = useState([]);
  const [list,setList] = useState([]);

  const actSelectSlot = (key) => {
    //check select
    if(list[key] && list[key].img === "") {
      let _selectedSlot = [...selectedSlot]
      let pos = _selectedSlot.indexOf(key);
      if(pos < 0) {
          _selectedSlot = [..._selectedSlot,key];
      } else {
          _selectedSlot = _selectedSlot.filter(v => v !== key);
      }
      setSelectedSlot(_selectedSlot);
    }
  }
  useEffect(()=>{
    let newList = props.product_slot.map((item,index)=>{
      if(list[index] && item !== list[index].img) {
        return {img: item, animation: "play"};
      } else {
        return {img: item, animation: "flash"};
      }
    });
    setList(newList);
    //clear active 
    setSelectedSlot([]);

  },[props.product_slot]);

  /* function like api */
  const likeApi = () => {
     
    let ary = list.map((item,index)=>{
      return selectedSlot.indexOf(index) >= 0 ? imgList.icon_set_10_1 : item.img;
    });
    ary = ary.length > 0 ? ary:[imgList.icon_set_10_1,"","","","",""];
    console.log(ary.length,ary);
    props.setValues({product_slot: ary})
  }

  return (
    <div>
      {list.map((item,index)=>{
        return (
          <div key={"a_"+index} style={{width: "84px",height: "84px", display: "inline-block"}}>
            <SlotGacha 
              className="play__slot"
              item={item.img}
              animation={item.animation}// play, flash
              active={selectedSlot.indexOf(index) >= 0}
              onClick={()=>actSelectSlot(index)}
            />
          </div>
        )
      })}
      <button onClick={()=>likeApi()}>open</button>
    </div>
  )
}
const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(Ani);
