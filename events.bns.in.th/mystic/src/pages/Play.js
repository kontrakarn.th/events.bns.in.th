import React,{useState,useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {setValues} from './../store/redux';
import styled from 'styled-components';
import imgList from './../constants/ImportImages';
import F11Layout from './../features/f11Layout';
import Modals from './../features/modals';
import {apiPost} from './../constants/Api';
import SlotText from './../features/f11Layout/_SlotText';
import SlotGacha from './../features/slotGacha';

const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
const Play = (props) => {
	const [listAni,setListAni] = useState([]);
    const [selectedSlot,setSelectedSlot] = useState([]);

    const apiOpen = () => {
		if(selectedSlot.length<=0){
			return;
		}
		if(parseInt(props.hongmoon_key)<selectedSlot.length){
			props.setValues({
				modal_open: "message",
				modal_message: "กุญแจไม่เพียงพอ"
			})
			return;
		}
		props.setValues({
			modal_open: "loading",
		})
        let dataSend = {slot: selectedSlot};
		let successCallback = (res) => {
			if(res.status) {
				setListAni(selectedSlot);
				setSelectedSlot([]);
				if(res.data.special_product_slot && res.data.special_product_slot.length>0){
					props.setValues({
						can_reset: res.data.can_reset,
						product_slot: res.data.slot,
						hongmoon_key: res.data.hongmoon_key,
						history_reward: [],
						modal_open: "",
					})
					setTimeout(() => {
						props.setValues({
							modal_open: "receive_reward",
							modal_message: "ได้รับ <span>"+res.data.special_product_slot[0].item_name+"</span> x1 <br/>ไอเทมจะถูกส่งเข้ากล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อกดรับ"
						})
					}, 1000);
				}
				else{
					props.setValues({
						can_reset: res.data.can_reset,
						product_slot: res.data.slot,
						hongmoon_key: res.data.hongmoon_key,
						history_reward: [],
						modal_open: ""
					})
				}
			} 
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		let failCallback = (res) => {
			if (res.type === 'no_permission') {
				props.setValues({
					permission: false,
					modal_open: "",
				});
			}
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		apiPost("REACT_APP_API_POST_RANDOM", props.jwtToken, dataSend, successCallback, failCallback);
    }
    const actSelectSlot = (key) => {
        let _selectedSlot = [...selectedSlot]
        let pos = _selectedSlot.indexOf(key);
        if(pos < 0) {
            _selectedSlot = [..._selectedSlot,key];
        } else {
            _selectedSlot = _selectedSlot.filter(v => v !== key);
        }
        setSelectedSlot(_selectedSlot);
    }
			  
	const apiEventInfo = () => {
		let dataSend = {type: "event_info"};
		let successCallback = (res) => {
			if(res.status) {
				props.setValues({
					...res.data,
					modal_open:"",
				})
			} 
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		let failCallback = (res) => {
			if (res.type === 'no_permission') {
				props.setValues({
					permission: false,
					modal_open: "",
				});
			}
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		apiPost("REACT_APP_API_POST_EVENT_INFO", props.jwtToken, dataSend, successCallback, failCallback);
	}
	useEffect(()=>{
		if(props.jwtToken !== "" && !props.status) apiEventInfo();
	},[props.jwtToken])

	let onlyNo = props.product_slot.map(item => parseInt(item.slot_no));
    return (
        <F11Layout page={"play"} hideScroll={true}>
            <Modals />
            <PlayStyle>
				<div className="hongmoonkey">
					<SlotText>กุญแจฮงมุนเก่าแก่: {numberWithCommas(props.hongmoon_key)}</SlotText>
				</div>
              	<div className="play__table">
					{props.product_slot && props.product_slot.length>0
					?
						new Array(50).fill(0).map((item, index) => {
							let no = onlyNo.indexOf(index+1)
							console.log(onlyNo,no);
							return (
								no>=0
								?
									<SlotGacha
										key={"play_choosed_"+index}
										className="play__slot 1"
										item={props.product_slot[no].product.product_img}
										animation={listAni.indexOf(index+1) >= 0?"play":"flash"}
										choosed={true}
										onClick={()=>null}
									/>
									// props.product_slot.map((item_product, index_product) => {
									// 	if(parseInt(item_product.slot_no)===(index+1)){
									// 		return (
									// 			<SlotGacha
									// 				key={"play_choosed"+index_product}
									// 				className="play__slot 1"
									// 				item={item_product.product.product_img}
									// 				animation={listAni.indexOf(index+1) >= 0?"play":"flash"}
									// 				choosed={true}
									// 				onClick={()=>null}
									// 			/>
									// 		);
									// 	}
									// })
								:
									props.can_reset
									?
										<SlotGacha
											key={"play_"+index}
											className="play__slot"
											item={""}
											animation={"flash"}
											choosed={true}
											onClick={()=>null}
										/>
									:
										<SlotGacha
											key={"play_"+index}
											className="play__slot"
											item={""}
											animation={"flash"}
											choosed={false}
											active={selectedSlot.indexOf(index+1) >= 0}
											onClick={()=>actSelectSlot(index+1)}
										/>
							)
						})
					:
						props.special_product===null
						?
							new Array(50).fill(0).map((item, index) => {
								return (
									<SlotGacha
										key={"play_"+index}
										className="play__slot"
										item={""}
										animation={"flash"}
										choosed={true}
										onClick={()=>null}
									/>
								);
							})
						:
							new Array(50).fill(0).map((item, index) => {
								return (
									<SlotGacha
										key={"play_"+index}
										className="play__slot"
										item={""}
										animation={"flash"}
										choosed={false}
										active={selectedSlot.indexOf(index+1) >= 0}
										onClick={()=>actSelectSlot(index+1)}
									/>
								);
							})
					}
              	</div>
              <div className="play__btns">
				{props.special_product===null
				?
					<Btn name="select" active={true} src={imgList.btn_select_reward} onClick={()=>props.setValues({modal_open: "select_reward"})} /> 
				:
					props.can_reset
					?
						<Btn name="select" active={true} src={imgList.btn_reset} onClick={()=>props.setValues({modal_open: "confirm_reset"})}/>
					:
					props.special_product.select_type===1
					?
						<Btn className="show" name="select" active={false} src={imgList.btn_selected_reward}>
							<Show src={props.special_product.product_img} alt=''/>
						</Btn>
					:
						<Btn name="select" active={false} src={imgList.btn_random_reward} />
				}
				{props.special_product===null
				?
					<Btn name="open" active={false} src={imgList.btn_open_inactive}/>
				:
					props.can_reset
					?
						<Btn name="open" active={false} src={imgList.btn_open_inactive}/>
					:
						<Btn name="open" active={true} src={imgList.btn_open} onClick={()=>apiOpen()} />
				}	
                <Btn name="buy" active={true} src={imgList.btn_buy} onClick={()=>props.setValues({modal_open: "buy_key"})} />
              </div>
            </PlayStyle>
        </F11Layout>
    )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(Play);

const PlayStyle = styled.div`
    position: relative;
    background: url(${imgList.play_background});
    height: 700px;
    padding-top: 149px;
    .hongmoonkey {
      position: absolute;
      top: 18px;
      left: 80px;
    }
    .play {
      &__table {
        margin: 0px auto;
        display: block;
        width: 840px;
        height: 425px;
        background-color: #FF000033;
        font-size: 0;
      }
      &__slot {
        // display: inline-block;
        // width: 84px;
        // height: 84px;
        // background-image: url(${imgList.animate});
      }
      &__btns {
		position: relative;
        display: flex;
        width: 807px;
        height: 52px;
        margin: 45px auto 0px;
        justify-content: space-between;
      }
    }
`;

const Slot = styled.div`
	position: relative;
	display: inline-block;
	width: 84px;
	height: 84px;
	background-image: url(${imgList.animate});
	cursor: pointer;
	${props=>props.choosed && 'cursor: default !important;'}
	&:before {
		content: " ";
		position: absolute;
		top: 0px;
		left: 0px;
		display: block;
		width: 100%;
		height: 100%;
		background-image: url(${props=>props.item});
	}
	&:after {
		content: " ";
		position: absolute;
		opacity: ${props=>props.active? 1:0};
		top: 0px;
		left: 0px;
		display: block;
		width: 100%;
		height: 100%;
		background-image: url(${imgList.slot_selected});
	}
`;

const Btn = styled.a`
	display: block;
	width: 230px;
	height: 52px;
	background-image: url(${props=>props.src});
	background-position: top center;
	cursor: pointer;
	&.show{
		&:hover {
			>img{
				opacity: 1;
			}
		}
	}
	&:hover {
		background-position: bottom center;
	}
  	${props=>!props.active && 
		`cursor: default`
	}
`;
const Show = styled.img`
	position: absolute;
	width: 60px;
	top: -49px;
	left: -40px;
	opacity: 0;
	transition: .3s all;
`;
