import React,{useState,useEffect} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import {setValues} from './../../store/redux';
import imgList from "../../constants/ImportImages";

const ModalBuyKey = (props) => {
    const name = "buy_key";
    const [num,setNum] = useState([0,0,0]);
    const numberWithCommas = (x) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    const handleChange = (e,key,text,price) =>{
        if (RegExp(/^[0-9]*$/).test(e.target.value)) {
            if(key===0){
                setNum([e.target.value,0,0]);
            }
            else if(key===1){
                setNum([0,e.target.value,0]);
            }
            else if(key===2){
                setNum([0,0,e.target.value]);
            }

            if(e.target.value!=='' && e.target.value!==0){
                props.setValues({
                    modal_buy_type: (key+1),
                    modal_buy_quantity: e.target.value,
                    modal_buy: 'ต้องการซื้อกุญแจฮงมุนเก่าแก่<br/><span>'+text+' x'+e.target.value+'</span><br/>ราคา <span>'+(numberWithCommas(price*e.target.value || 0))+'</span> ไดมอนด์ ?'
                })
            }
        }
    }
    const handleConfirm = () =>{
        props.setValues({
            modal_open: 'confirm_buy_key'
        })
    }
    useEffect(()=>{
		if(props.modal_open==='buy_key'){
            setNum([0,0,0]);
        }
	},[props.modal_open])
    let actClose = () =>  props.setValues({modal_open: ""});
    return (
        <ModalCore
            open={props.modal_open === name}
            onClick={()=>actClose()}
        >
        <ModalContentStyle>
            <a className="close" onClick={()=>actClose()}/>
            <input type="text" className="input input__1" id="1" name="1" value={num[0] || ""} onChange={(e) => handleChange(e,0,'1',1500)} maxLength="1"/>
            <input type="text" className="input input__2" id="2" name="2" value={num[1] || ""} onChange={(e) => handleChange(e,1,'10+1',15000)} maxLength="3"/>
            <input type="text" className="input input__3" id="3" name="3" value={num[2] || ""} onChange={(e) => handleChange(e,2,'50+5',75000)} maxLength="3"/>
            <div className="buttons">
                {num.reduce((sum, item) => sum + item)>0
                ?
                    <a className="btn" onClick={()=>handleConfirm()} />
                :
                    <a className="btn" />
                }
            </div>
        </ModalContentStyle>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state});

const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(ModalBuyKey);

const ModalContentStyle = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 1105px;
  height: 700px;
  padding: 172px 135px 102px
  background-image: url(${imgList.bg_modal_buy_key});
  
  .message {
    color: #FFFFFF;
    font-size: 20px;
    display: block;
    display: block;
    text-align: center;
    span {
      color: #d19e56;
    }

  }
  .close {
    position: absolute;
    top: 58px;
    right: 144px;
    display: block;
    width: 35px;
    height: 35px;
    background-image: url(${imgList.icon_close}); 
    cursor: pointer;
  }
  .buttons {
    position: absolute;
    left: 50%;
    bottom: 29px;
    display: flex;
    width: 100%;
    transform: translate(-50%, 0px);
    text-align: center;
    justify-content: center;
    align-items: center;
  }
    .btn {
        display: inline-block;
        width: 230px;
        height: 52px;
        background-position: top center;
        background-image: url(${imgList.btn_buy_key});
        cursor: pointer;
        &:hover {
        background-position: bottom center;
        }
    }
    .input{
        position: absolute;
        color: #fff;
        top: 455px;
        width: 38px;
        background: #000;
        outline: 0;
        border: 0;
        border-radius: 4px;
        text-align: center;
        font-family: "Kanit-Regular", Tahoma;
        font-size: 16px;
        line-height: 25px;
        &__1{
            left: 322px;
        }
        &__2{
            left: 534px;
        }
        &__3{
            left: 745px;
        }
    }
`;