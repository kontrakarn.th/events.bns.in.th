import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import imgList from "../../constants/ImportImages";

export default props => {
    return (
      <ModalStyle open={props.open||false}>
        <div className="backdrop" onClick={()=>{if(props.onClick)props.onClick()}} />
        <div className="content">
          {props.children && props.children}
        </div>
      </ModalStyle>
    )
}

const ModalStyle = styled.div`
    position: fixed;
    top: 0px;
    left: 0px;
    display: block;
    width: 100vw;
    height: 100vh;
    max-width: 1105px;
    max-height: 700px;
    overflow: hidden;
    z-index: 7000;
    opacity: ${props=>props.open ? "1":"0"};
    pointer-events: ${props=>props.open ? "all":"none"};
    transition: .3s all;
  >.backdrop {
    position: relative;
    display: block;
    width:100%;
    height: 100%;
    background-color: rgba(0,0,0,0.9);
  }
  >.content {
    position: absolute;
    top: 50%;
    left: 50%;
    display: block;
    max-width: 100%;
    max-height: 100%;
    transform: translate3d(-50%,-50%,0);
  }
`;
