import React from 'react';

import ModalMessage from './_Message';
import ModalLoading from './_Loading';
// import ModalSelectCharacter from './_SelectCharacter';
import ModalSelectReward from './_SelectReward';
import ModalReceiveReward from './_ReceiveReward';
import ModalBuyKey from './_BuyKey';
import ModalConfirmBuyKey from './_ConfirmBuyKey';
import ModalReceiveKey from './_ReceiveKey';
import ModalConfirmSp from './_ConfirmSp';
import ModalConfirmReset from './_ConfirmReset';

export default (props)=> (
  <>
    <ModalLoading />
    <ModalMessage />
    <ModalSelectReward />
    <ModalReceiveReward />
    <ModalBuyKey/>
    <ModalConfirmBuyKey/>
    <ModalReceiveKey/>
    <ModalConfirmSp/>
    <ModalConfirmReset/>
  </>
)
