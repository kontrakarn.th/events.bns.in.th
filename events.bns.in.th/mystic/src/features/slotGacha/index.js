import React,{useState} from 'react';
import styled,{keyframes} from 'styled-components';
import imgList from './../../constants/ImportImages';

export default (props) => {
  return(
    <SlotAnimation
      item={props.item || ""}
      active={props.active || false}
      animation={props.animation}
      duration={props.duration || 0.7}
      choosed={props.choosed || false}
      onClick={()=>props.onClick()}
    >
      <canvas
        width={props.width || 84}
        height={props.heught || 84}
      />
    </SlotAnimation>
    
  )
}
const aniSpriteSheet  = keyframes`
    0% {
        opacity: 1;
    }
    100% {
        background-position: 0 100%;
    }
`;
const aniFadeIn  = keyframes`
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
      background-position: 0 100%;
    }
`;
const SlotAnimation = styled.div`
  position: relative;
  display: inline-block;
  width: 84px;
  height: 84px;
  background-color: #FF000044;
  font-size: 0;
  cursor: pointer;
  ${props=>props.choosed && 'cursor: default !important;'}
  canvas {
    z-index: 0;
    position: relative;
    width: 100%;
    height: auto;
    background-size: 100%;
    background-position: 0px 0%;
    background-repeat: no-repeat;
    background-image: url(${imgList.animate});
    animation: ${props=>(props.animation==="play" ? aniSpriteSheet : "")} ${props=>props.duration}s steps(7) alternate forwards;
  }
  &:before {
    z-index: 1;
    opacity: ${props=>(props.animation==="flash" ? 1 : 0)};
    content: "";
    user-select: none;
    pointer-events: none;
    position: absolute;
    top: 0px;
    left: 0px;
    display: block;
    width: 100%;
    height: 100%;
    background-image: url(${props=>props.item});
    animation: ${props=>(props.animation==="play" ? aniFadeIn : "")} 0.3s ${props=>props.duration}s linear alternate forwards;
  }
  &:after {
    z-index: 2;
    content: "";
    user-select: none;
    pointer-events: none;
    position: absolute;
    opacity: ${props=>props.active? 1:0};
    top: 0px;
    left: 0px;
    display: block;
    width: 100%;
    height: 100%;
    background-image: url(${imgList.slot_selected});
  }
  
`;
// opacity: 0;
// position: absolute;
// top: 0px;
// left: 0px;
// display: ${props => props.break?"block": "none"};
// width: 100%;
// height: 100%;
// background-size: 100% auto;
// background-position: 0px 0%;
// background-repeat: no-repeat;
// background-image: url(${props => props.src});
// animation: ${animationbreak} 1s steps(10) alternate forwards;
// transition: 0.3s opacity linear;
// filter: hue-rotate(304deg);