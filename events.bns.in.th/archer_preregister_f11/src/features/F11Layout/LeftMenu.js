import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import icon_berger from './../../static/images/icons/icon_berger.png';
import icon_close from './../../static/images/icons/icon_close.png';

export default class extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showMenu: false,
            list: [
                {title: "หน้าหลัก", link: process.env.REACT_APP_EVENT_PATH},
                {title: "ระบบส่งของขวัญ", link: process.env.REACT_APP_EVENT_PATH+"/exchange"},
                {title: "ประวัติ", link: process.env.REACT_APP_EVENT_PATH+"/history"},
            ]
        }
    }
    render() {
        let { showMenu, list } = this.state;
        return (
            <LeftMenuSet active={showMenu} className="menu">
                <div className="menu__backdrop"></div>
                <a className="menu__berger" onClick={()=>this.setState({showMenu:true})} />

                <ul className="menu__bar">
                    <a className="menu__close" onClick={()=>this.setState({showMenu:false})} />
                    {list.map((item,index)=>(
                        <li key={"menu"+index}><Link to={item.link}>{item.title}</Link></li>
                    ))}
                </ul>
            </LeftMenuSet>
        )
    }
}

const LeftMenuSet = styled.div`
    position: absolute;
    top: 0px;
    left: 0px;
    display: block;
    width: 100vw;
    height: 100vh;
    pointer-events: ${props=>(props.active? "all":"none")};
    .menu {
        &__backdrop {
            background-color: rgba(0,0,0,0.7);
            position: absolute;
            top: 0;
            left: 0;
            display: block
            width: 100vw;
            height: 100vh;
            opacity: ${props=>(props.active? "1":"0")};
            transition: all 0.3s linear;
        }
        &__berger {
            display: block;
            width: 60px;
            height: 60px;
            cursor: pointer;
            pointer-events: all;
            background: no-repeat center url(${icon_berger});
        }
        &__bar {
            z-index: 1;
            position: absolute;
            top: 0;
            left: ${props=>(props.active? "0px":"-300px")};
            display: block;
            width: 280px;
            height: 100%;
            background-color: #231f20;
            border-right: 10px solid #414042;
            transition: left 0.3s ease-in-out;
            box-sizing: border-box;
            padding: 100px 0px 0px 40px;
            color: #c4a193;
            font-size: 20px;
        }
        &__close {
            position: absolute;
            top: 0;
            right: 0;
            cursor: pointer;
            width: 60px;
            height: 60px;
            background: no-repeat center url(${icon_close});
        }
    }
`;
