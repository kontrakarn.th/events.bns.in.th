import React from 'react';
import Modal from './Modal';

export default class ModalConfirm extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                actClose={()=>this.props.actClose()}
                content='modal__content2'
            >
                <div className="modal__message" dangerouslySetInnerHTML={{ __html: this.props.msg }}></div>
                <div className="modal__btngroup">
                    <div className="modal__btnokinline" onClick={()=>this.props.actConfirm()}/>
                    <div className="modal__btnnoinline" onClick={()=>this.props.actClose()}/>
                </div>
            </Modal>
        )
  }
}
