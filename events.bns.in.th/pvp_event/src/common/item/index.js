import React from 'react';
import styled from 'styled-components';
import {Imglist} from './../../constants/Import_Images';

// export const Item = props => {
//     return (
//         <ContentItem className="" name={props.name} count={props.count}>
//         </ContentItem>
//     )
// }

export const Item = styled.div`
    position: relative;
    background: bottom left no-repeat url(${props=>Imglist[props.name]});
    width: 84px;
    height: 83px;
    display: block;
    box-sizing: border-box;
    &::before {
        content: "${props=>props.count}";
        position: absolute;
        top: 3px;
        right: 0px;
        display: block;
        width: 30px;
        text-align: center;
        color: #FFFFFF;
        font-size: 1em;
    }
`;
