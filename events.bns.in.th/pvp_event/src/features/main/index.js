import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import {Imglist} from './../../constants/Import_Images';
// import ModalConfirmReceive from '../modals/ModalConfirmReceive';
import ModalSelectCharacter from "../modals/ModalSelectCharacter";

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div className={className} onClick={onClick}><img src={Imglist.btn_next}/></div>
    );
}
  
function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div className={className} onClick={onClick}><img src={Imglist.btn_prev}/></div>
    );
}

class Main extends React.Component {

    actConfirm(exchange_key){
        if (exchange_key === undefined || exchange_key === ""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไม่สามารถแลกของไอเทมได้",
            });
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'exchange_rewards',package_key:exchange_key};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selectd_char: data.data.selectd_char,
                    characters: data.data.characters,
                    points:data.data.points,
                    points_text:data.data.points_text,
                    quests:data.data.quests,
                    rewards:data.data.rewards,
                });
                /* this.setState({
                    itemLists : data.data.itemLists,
                    questLists : data.data.questLists,
                }) */
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_EXCHANGE", this.props.jwtToken, dataSend, successCallback, failCallback);

    }

    apiSelectCharacter(id){
        if (id === undefined || id === ""){
            return;
        }
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'select_character',id:id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selectd_char: data.data.selectd_char,
                    characters: data.data.characters,
                    points:data.data.points,
                    points_text:data.data.points_text,
                    quests:data.data.quests,
                    rewards:data.data.rewards,
                    modal_open: "",
                });
                /* this.setState({
                    itemLists : data.data.rewards,
                    questLists : data.data.quests,
                }) */
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true){
                /* this.setState({
                    itemLists : data.data.rewards,
                    questLists : data.data.quests,
                }) */
                this.props.setValues({
                    modal_open: "",
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selectd_char: data.data.selectd_char,
                    characters: data.data.characters,
                    points:data.data.points,
                    points_text:data.data.points_text,
                    quests:data.data.quests,
                    rewards:data.data.rewards,
                    modal_open: "",
                });

                if (data.data.selected_char === false){
                    this.props.setValues({ modal_open: "selectcharacter" });
                }
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,

            active: [],
            //itemLists: [],
            // questLists : [],
            /* quests: [
                {
                    image: Imglist.quest1,
                    points: '0',
                },
                {
                    image: Imglist.quest2,
                    points: '0',
                },
            ],
            itemLists: [
                {
                    image: Imglist.items1,
                    name: 'ชุดราชันสมรภูมิ',
                    points: '500'
                },
                {
                    image: Imglist.items2,
                    name: 'เครื่องประดับ ราชันสมรภูมิ',
                    points: '250'
                },
                {
                    image: Imglist.items3,
                    name: 'ชุดผู้พิพากษา',
                    points: '450'
                },
                {
                    image: Imglist.items4,
                    name: 'สัญลักษณ์ ผู้พิพากษา',
                    points: '200'
                },
                {
                    image: Imglist.items5,
                    name: 'พละกำลังราชา',
                    points: '50'
                },
                {
                    image: Imglist.items6,
                    name: 'ยาที่สกัดจากแมลง',
                    points: '50'
                },
                {
                    image: Imglist.items7,
                    name: 'รากพันปี',
                    points: '50'
                },
                {
                    image: Imglist.items8,
                    name: 'พายุหิมะซอลซัม',
                    points: '50'
                },
                {
                    image: Imglist.items9,
                    name: 'สมุนไพรดอกไม้หิมะ',
                    points: '50'
                },
                {
                    image: Imglist.items10,
                    name: 'ยาโชคชะตา ขนาดใหญ่',
                    points: '1'
                },
                {
                    image: Imglist.items11,
                    name: 'มันจูฮงมุน',
                    points: '50'
                },
                {
                    image: Imglist.items12,
                    name: 'ลูกแก้วยอดนักรบ',
                    points: '65'
                },
                {
                    image: Imglist.items13,
                    name: 'เศษเกล็ดสีคราม',
                    points: '50'
                },
                {
                    image: Imglist.items14,
                    name: 'กระดาษม้วนวิชา',
                    points: '35'
                },
                {
                    image: Imglist.items15,
                    name: 'ประกายฉลามดำ',
                    points: '70'
                },
                {
                    image: Imglist.items16,
                    name: 'เหรียญฝึกฝน ฮงซอกกึน',
                    points: '50'
                },
                {
                    image: Imglist.items17,
                    name: 'กล่องโซลชิลด์ทางช้างเผือก ส่วนที่ 1',
                    points: '50'
                },
                {
                    image: Imglist.items18,
                    name: 'กล่องโซลชิลด์ทางช้างเผือก ส่วนที่ 2',
                    points: '100'
                },
                {
                    image: Imglist.items19,
                    name: 'กล่องโซลชิลด์ทางช้างเผือก ส่วนที่ 3',
                    points: '150'
                },
                {
                    image: Imglist.items20,
                    name: 'กล่องโซลชิลด์ทางช้างเผือก ส่วนที่ 4',
                    points: '200'
                },
                {
                    image: Imglist.items21,
                    name: 'กล่องโซลชิลด์ทางช้างเผือก ส่วนที่ 5',
                    points: '250'
                },
                {
                    image: Imglist.items22,
                    name: 'กล่องโซลชิลด์ทางช้างเผือก ส่วนที่ 6',
                    points: '300'
                },
                {
                    image: Imglist.items23,
                    name: 'กล่องโซลชิลด์ทางช้างเผือก ส่วนที่ 7',
                    points: '350'
                },
                {
                    image: Imglist.items24,
                    name: 'กล่องโซลชิลด์ทางช้างเผือก ส่วนที่ 8',
                    points: '400'
                },
            ], */
            slider_image: [
                {
                    image: Imglist.slide1,
                    title: 'ชุดราชันสมรภูมิ และ เครื่องประดับราชันสมรภูมิ'
                },
                {
                    image: Imglist.slide2,
                    title: 'ชุดผู้พิพากษา และ สัญลักษณ์ผู้พิพากษา'
                }
            ]
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }

    render() {
        const settings = {
            dots: true,
            infinite: true,
            fade: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        return (
            <PageWrapper>
                <Content>
                    {/* section 1 */}
                    <div className="section section1">
                        <ol>
                            <li>ผู้เล่นล็อกอินเข้ากิจกรรมในหน้าต่างฮงมุนแพด (F11) และเลือกตัวละครที่ต้องการร่วมกิจกรรม (จำกัด 1 ตัวละคร ต่อ 1 การีนา UID)</li>
                            <li>เมื่อสำเร็จเควสต์ตามที่กำหนดจะได้รับเหรียญกิจกรรม</li>
                            <li>นำเหรียญกิจกรรมกรรมที่ได้รับไปแลกเปลี่ยนเป็นไอเทมได้ที่หน้ารายการแลกเปลี่ยน</li>
                            <li>ผู้เล่นสามารถสำเร็จเควสต์ รับเหรียญกิจกรรม และแลกเปลี่ยนไอเทมในรายการแลกเปลี่ยนได้ไม่จำกัด</li>
                        </ol>
                        <div><img src={Imglist.remark}/> หมายเหตุ</div>
                        <div>ไอเทม<span className="text-yellow">ชุดราชันสมรภูมิ เครื่องประดับราชันสมรภูมิ ชุดผู้พิพากษา สัญลักษณ์ผู้พิพากษา สามารถแลกเปลี่ยนได้</span></div>
                        <div><span className="text-yellow">ตั้งแต่วันที่ 11 ธันวาคม 2562</span> ไอเทมอื่น ๆ นอกเหนือจากนี้ สามารถแลกได้ในวันที่ 4 ธันวาคม 2562 จนถึงวันสิ้นสุดกิจกรรม</div>
                        
                        <div className="quest text-center">
                            <div className="quest__title text-yellow">รายละเอียดเควสต์ และ การสะสมเหรียญกิจกรรม</div>

                            {
                                this.props.quests && this.props.quests.length > 0 && this.props.quests.map((items,key)=>{
                                    return(  
                                        <div key={key} className="questItems">
                                            <img src={items.image}/>
                                            <div className="questItems__points">{items.completed_count}</div>
                                        </div>
                                    )
                                })
                            } 
                        </div>
                         
                    </div>

                    {/* section 2 */}
                    <div className="section section2">
                        {
                            this.props.rewards && this.props.rewards.length > 0 && this.props.rewards.map((items,key)=>{
                                return(  
                                    <div key={key} className="items">
                                        <div className="items__img">
                                            <img src={items.image}/>
                                        </div>
                                        <div className="items__name">{items.title}</div>
                                        {
                                            items.can_exchange === false ?
                                                <>
                                                    <div className={"btn btn-exchange inactive"}>แลกไอเทม</div>
                                                </>
                                                :
                                                <>
                                                    <div className={"btn btn-exchange"} onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการแลกไอเทม '+items.title + ' ?',exchange_key: items.id})}>แลกไอเทม</div>
                                                </>
                                        }
                                        <small>ใช้ {items.required_points} เหรียญ</small> <br/>
                                        {
                                            items.limit && parseInt(items.limit) !== 0 ?
                                                <small className="limit">จำกัด {items.limit} ชิ้น</small>
                                            : null
                                        }
                                    </div>
                                )
                            })
                        }       
                    </div>

                    {/* section 3 */}
                    <div className="section section3">
                        <div>
                            <Slider {...settings}>
                            {
                                this.state.slider_image.map((items,key)=>{
                                    return(
                                        <div key={key} className=""> 
                                            <p>{items.title}</p>
                                            <img src={items.image}/>
                                        </div>
                                    )
                                })
                            }
                            </Slider>
                        </div>
                    </div>
              
                </Content>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                {/* <ModalConfirmReceive actConfirm={this.actConfirm.bind(this)}/> */}
                <ModalSelectCharacter actSelectCharacter={this.apiSelectCharacter.bind(this)}/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #11122d;
    width: 1105px;
    padding-top: 740px;
    text-align: center;
`

const Content = styled.div `  
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }
    & .text-yellow{
        color: #f2ff67;
    }
    & .text-center{
        text-align: center;
    }
    & .section{
        color:#fff;
        padding-top: 10%;
        padding-bottom: 10%;
        &__header{
            font-size: 20px;
            font-weight: bold;
            margin-bottom: 30px;
            text-align: center;
        }
        &__step1{
            text-align:center;
            padding-top: 3%;
        }
        &__step2{
            text-align: center;
            padding-top: 4%;
        }
    }
    & small{
        font-size: 80%;
        line-height: 1;
    }
    & .limit{
        color: #e4e0e0;
    }
    & .section1{
        background: url(${Imglist['content1']}) no-repeat top center; 
        text-align:left;
        padding: 10% 14%;
        text-align: left;
        font-size: 15px;
        position: relative;
        &::after{
            content: '';
            background: url(${Imglist['character1']}) no-repeat top center; 
            width: 270px;
            height: 680px;
            position: absolute;
            right: 0;
            top: 28%;
        }
        & .quest__title{
            margin: 50px 0 25px 0;
        }
        & .questItems{
            display: inline-block;
            vertical-align: top;
            text-align: center;
            padding: 10px 20px;
            position: relative;
            &__points{
                text-align: center;
                position: absolute;
                right: 10%;
                top: 50%;
                font-size: 18px;
                line-height: 1.5;
                min-width: 28px;
            }
            &.disabled{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%); 
            }
        }
        & ol{
            padding-left: 15px;
        }
    }
    & .section2{
        background: url(${Imglist['content2']}) no-repeat top center; 
        padding: 10% 16% 20% 16%;
        z-index: 1;
        position: relative;
        text-align:left;
        &::after{
            content: '';
            background: url(${Imglist['character2']}) no-repeat top center; 
            width: 365px;
            height: 870px;
            position: absolute;
            right: 0;
            top: 72%;
        }
    }
    & .items{
        width: 20%;
        display:inline-block;
        vertical-align: top;
        font-size: 14px;
        padding: 15px 10px;
        color: #939393;
        text-align: center;
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%); 
            &.active{
                -webkit-filter: none;
                filter: none;  
            }
        }
        &__img{
            position:relative;  
        }
        &__name{
            min-height: 35px;
            line-height: 1.2;
            margin: 5px 0;
        }
    }

    & .btn{
        cursor:pointer;
        // &-default{
        //     background: no-repeat 0 0 url(${Imglist['btn_default']});
        //     width: 155px;
        //     height: 45px;
        //     display: inline-block;
        //     cursor: pointer;
        //     margin: 0 15px;
        //     color: #fff;
        //     line-height: 36px;
        //     font-size: 20px;
        //     &:hover{
        //         background-position: 0 -55px;
        //     }
        //     &.disabled{
        //         -webkit-filter: grayscale(100%);
        //         filter: grayscale(100%);
        //         color: #bfbfbf;
        //         cursor: not-allowed;
        //         &:hover{
        //             background-position: 0 0;
        //         }
        //     }
        // }
        &-exchange{
            background: top center no-repeat url(${Imglist['btn_default']});
            width: 80px;
            height: 35px;
            background-size: cover;
            color: #000;
            margin: 0 auto;
            opacity: 0.8;
            font-size: 14px;
            line-height: 2;
            &:hover{
               background-position: bottom center;
            }
            &.inactive{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);  
                cursor:default;
                &:hover{
                    opacity: 0.8;
                } 
            }
        }
       
    }
    & .section3{
        background: top center no-repeat url(${Imglist['content3']});
        color: #fff;
        text-align: center;
        padding-bottom: 14%;
        z-index: 1;
        position: relative;
        & .slick-slide img{
            margin: 0 auto;
        }
        & .slick-dots li.slick-active button:before {
            opacity: 1;
            color: #fff;
        }
        & .slick-dots li button:before{
            font-size: 11px;
            opacity: 1;
            color: #646464;
        }
        & .slick-prev{
            left:120px;
        }
        & .slick-next{
            right:120px;
        }
        & .slick-prev,
        & .slick-next{
            width: auto;
            height: auto;
            z-index: 1;
            &::before{
                font-size:0;
            }
        }
    }

    

`

