import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';
import { apiPost } from './../../middlewares/Api';
import {Imglist} from './../../constants/Import_Images';

const CPN = props => {

    const actSelectCharacter=(e)=>{
        e.preventDefault();
        let indexCharacter = props.value_select;
        if(indexCharacter !== "" ){
            props.actSelectCharacter(indexCharacter);

            /* props.setValues({
                modal_open:"receive",
                modal_message: "ตัวละคร " +indexCharacter+ " เข้าร่วมกิจกรรมเรียบร้อยแล้ว",
            }); */

        }else{
            props.setValues({
                modal_open:"message",
                modal_message: "โปรดเลือกตัวละคร",
                selected_char:false,
            });
        }
    }

    function handleChange(e) {
        props.setValues({
            value_select : e.target.value,
        })
    }

    return (
        <ModalCore
            modalName="selectcharacter"
            actClickOutside={false}>

            <ModalMessageContent>       
                <div className="">
                    <div className="contenttitle">เลือกตัวละครที่ต้องการเข้าร่วมกิจกรรม</div>
                    <div className="contenttext">
                        <FormSelect>
                            <form className="formselect">
                                <div className="custom_select">
                                    <select id="charater_form" className="fromselect__list" onChange={(e) => handleChange(e)}>
                                        <option value="" disabled selected>เลือกตัวละคร</option>
                                        {props.characters && props.characters.map((item,index)=>{
                                            return (
                                                <option key={index} value={item.id}>{item.char_name}</option>
                                            )
                                        })}
                                    </select>
                                    <img src={Imglist.icon_dropdown} alt="" />
                                </div>
                            </form>
                        </FormSelect>
                    </div>
                </div> 
        
                <div>
                    <Btns onClick={(e)=>actSelectCharacter(e)}>
                        ตกลง
                    </Btns>
                    {/* <Btns onClick={()=>props.setValues({modal_open:''})}>
                        ยกเลิก
                    </Btns> */}
                </div>
            </ModalMessageContent>
            
        </ModalCore>
    )
}


const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);


const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 450px;
    height: 315px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist['modal_bg']});
    box-sizing: border-box;
    padding: 4% 8%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttitle{
        font-size: 21px;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 21px;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 180px;
        //height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;

const Btns = styled.div`
    background: no-repeat top center url(${Imglist['btn_default']});
    width: 95px;
    height: 42px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #000;
    line-height: 36px;
    font-size: 20px;
    background-size: cover;
    &:hover{
        background-position: bottom center;
    }
`

const FormSelect = styled.div`
    .formselect {
        margin: 0px auto;
        .custom_select {
            position: relative;
            color: #fff;
            background-color: #000;
            width: 370px;
            height: 35px;
            display: block;
            margin: 10px auto;
            padding: 5px;
            overflow: hidden;
            &:focus {
                outline: 0 none;
            }
        }
        select {
            font-family: 'Kanit-Light';
            font-size: 21px;
            border: 0px;
            background: #000;
            vertical-align: middle;
            width: 110%;
            height: 100%;
            top: 0;
            left: 0;
            position: absolute;
            z-index: 1;
            color: inherit;
            &:focus {
                outline: 0 none;
            }
        }
        img {
            position: absolute;
            display: block;
            top: 50%;
            // width: 24px;
            // height: 21px;
            right: 0px;
            transform: translate( -50% , -50% );
            user-select: none;
        }
    }
`