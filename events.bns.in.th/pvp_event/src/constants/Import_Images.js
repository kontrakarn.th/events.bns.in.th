import bg from '../static/images/background.jpg';

import slide1 from '../static/images/slide/1.png'
import slide2 from '../static/images/slide/2.png'

import modal_bg from '../static/images/modal_bg.png'
import btn_default from '../static/images/btn_default.png'

import content1 from '../static/images/content1.png'
import content2 from '../static/images/content2.png'
import content3 from '../static/images/content3.png'
import btn_prev from '../static/images/btn_prev.png'
import btn_next from '../static/images/btn_next.png'
import sidebar_bg from '../static/images/sidebar_bg.png'
import sidebar_icon from '../static/images/sidebar_icon.png'
import remark from '../static/images/remark.png'
import character1 from '../static/images/character1.png'
import character2 from '../static/images/character2.png'
import icon_dropdown from '../static/images/icon_dropdown.png'

import items1 	from '../static/images/items/1.png'
import items2 	from '../static/images/items/2.png'
import items3 	from '../static/images/items/3.png'
import items4 	from '../static/images/items/4.png'
import items5 	from '../static/images/items/5.png'
import items6 	from '../static/images/items/6.png'
import items7 	from '../static/images/items/7.png'
import items8 	from '../static/images/items/8.png'
import items9 	from '../static/images/items/9.png'
import items10 	from '../static/images/items/10.png'
import items11 	from '../static/images/items/11.png'
import items12 	from '../static/images/items/12.png'
import items13 	from '../static/images/items/13.png'
import items14 	from '../static/images/items/14.png'
import items15 	from '../static/images/items/15.png'
import items16 	from '../static/images/items/16.png'
import items17 	from '../static/images/items/17.png'
import items18 	from '../static/images/items/18.png'
import items19 	from '../static/images/items/19.png'
import items20 	from '../static/images/items/20.png'
import items21 	from '../static/images/items/21.png'
import items22 	from '../static/images/items/22.png'
import items23 	from '../static/images/items/23.png'
import items24 	from '../static/images/items/24.png'

import quest1 from '../static/images/quest/1.png'
import quest2 from '../static/images/quest/2.png'

//https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival


export const Imglist = {
	bg,
	modal_bg,
	btn_default,
	slide1,
	slide2,
	content1,
	content2,
	content3,
	btn_prev,
	btn_next,
	sidebar_bg,
	sidebar_icon,
	remark,
	character1,
	character2,
	icon_dropdown,

	items1 ,
	items2 ,
	items3 ,
	items4 ,
	items5 ,
	items6 ,
	items7 ,
	items8 ,
	items9 ,
	items10,
	items11,
	items12,
	items13,
	items14,
	items15,
	items16,
	items17,
	items18,
	items19,
	items20,
	items21,
	items22,
	items23,
	items24,

	quest1,
	quest2,

}
