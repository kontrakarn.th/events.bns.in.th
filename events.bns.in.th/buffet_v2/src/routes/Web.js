import React, {Component} from 'react';
import {Route} from 'react-router-dom';
// import SSOMiddleware from './../middlewares/SSOMiddleware';
import OauthMiddleware from './../middlewares/OauthMiddleware';

import { Home } from './../pages/Home';
import { Trade } from './../pages/Trade';
import { History } from './../pages/History';

export class Web extends Component {
	render() {
    console.log("Route",process.env.REACT_APP_EVENT_PATH);
		return (
			<>
				<Route path={process.env.REACT_APP_EVENT_PATH} component={OauthMiddleware} />
				<Route path={process.env.REACT_APP_EVENT_PATH} exact component={Home} />
				<Route path={process.env.REACT_APP_EVENT_PATH +'/trade'} exact component={Trade} />
				<Route path={process.env.REACT_APP_EVENT_PATH +'/history'} exact component={History} />
			</>
		);
	}
}
