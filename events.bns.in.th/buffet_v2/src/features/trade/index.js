import React from 'react';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled, { keyframes } from 'styled-components';

import { onAccountLogout } from './../../actions/AccountActions';
import { setValues } from './../main/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalConfirmTrade from './../../features/modals/ModalConfirmTrade';
import ModalShowReward from './../../features/modals/ModalShowReward';
import ModalPreview from './../../features/modals/ModalPreview';
import './css/style.css';
import Menu from '../menu/Menu.js';
import {Imglist} from '../../constants/Import_Images';

const numberWithCommas=(x)=> {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
class Trade extends React.Component {
    actConfirmRedeem(){
        // this.apiExchangeItem(this.props.modal_send_item.id);
    }
    actConfirmSendItem(){
        this.apiExchangeItem(this.props.modal_item.id);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    // apiExchangeInfo(){
    //     this.props.setValues({ modal_open: "loading" });
    //     let dataSend = {type:'getexchangeinfo'};
    //     let successCallback = (data) => {
    //         if (data.status) {
    //             this.props.setValues({
    //                 ...data.response,
    //                 modal_open: "",
    //                 itemlist: data.content,
    //             });
    //         }
    //     };
    //     const failCallback = (data) => {
    //         if (data.type == 'no_permission') {
    //             this.props.setValues({
    //                 permission: false,
    //                 modal_open: "",
    //             });
    //         }else {
    //             this.props.setValues({
    //                 modal_open:"message",
    //                 modal_message: data.message,
    //             });
    //         }
    //     };
    //     apiPost("REACT_APP_API_POST_GETEXCHANGEINFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    // }
    apiExchangeItem(id){
        if(id>0){
            this.props.setValues({ modal_open: "loading" });
            let dataSend = {type:'redeem',package_id:id};

            let successCallback = (data) => {
                if (data.status) {
                    this.props.setValues({
                        token: data.data.token,
                        modal_open:"showreward",
                        modal_message: "คุณได้รับ",
                        modal_item: data.reward,
                        history_list: [],
                    });
                }
            };
            const failCallback = (data) => {
                if (data.type == 'no_permission') {
                    this.props.setValues({
                        permission: false,
                        modal_open: "",
                    });
                }else {
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: data.message.replace(' ',"<br/>"),
                    });
                }
            };
            apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
        }

    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: ''
        }
    }

    componentDidMount() {
        // this.apiExchangeInfo();
    }


    render() {
        return (
            <div className="trade">
                <Menu page={'trade'} />
                <div className="trade__content">
                    <img className="trade__content--character" src={Imglist['exchange_character']} alt=""/>
                    <div className="trade__content--coupon">
                            <div className="number">
                                    {numberWithCommas(this.props.token)}
                            </div>
                    </div>
                    <div className="trade__content--inner">
                        {
                            this.props.itemlist && this.props.itemlist.length > 0 &&
                            this.props.itemlist.map((item,index) => {
                                return(
                                        <div className="trade__box" key={item.name}>
                                             <div className="trade__box--itemname" dangerouslySetInnerHTML={{ __html: item.name.replace("|","<br/>") + `(${item.amt})`}}/>
                                                <div className={"trade__box--itembox "}>
                                                    <img src={Imglist[item.icon]} alt="" />
                                                    <img className="previewbtn" src={Imglist['item_ribbon']} alt="" onClick={()=>this.props.setValues({modal_open: 'preview',modal_preview: item.preview})}/>
                                                </div>
                                                <div className={"trade__box--btn"+(this.props.token < item.amt ? ' disable' : '')} onClick={()=>this.props.setValues({modal_open:'confirmtrade',modal_item:{...item}})}/>
                                        </div>
                                )
                            })
                        }
                    </div>
                </div>
                <ModalPreview/>
                <ModalConfirmTrade actConfirm={this.actConfirmSendItem.bind(this)}/>
                <ModalLoading />
                <ModalMessage />
                <ModalShowReward actConfirm={()=>this.props.setValues({modal_open:'',modal_item:{icon:0,name:""}})}/>

            </div>
        )
    }
}
const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Trade);
