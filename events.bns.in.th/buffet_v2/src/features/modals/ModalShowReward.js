import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../main/redux';

import {Imglist} from './../../constants/Import_Images';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="showreward"
            actClickOutside={()=>props.setValues({modal_open:"",modal_item:{icon:0,name:""}})}
        >
            <ModalContent>
                <div className='titlebox'>
                        <img src={Imglist.reward_title} alt=""/>
                </div>
                <div className='itembox'>
                  {(props.modal_item && props.modal_item.icon && props.modal_item.icon !== "") ?
                    <div className='item'>
                      <img src={Imglist['gacha_'+(props.modal_item.icon)]} alt=""/>
                    </div>
                    :
                    ""
                  }
                  <div className='itemname' dangerouslySetInnerHTML={{ __html: props.modal_item && props.modal_item.name.replace("|","<br/>")}}/>
                  {/* <div className='itemname'>หินจันทรา(100)</div> */}
                </div>
            </ModalContent>
            <ModalBottom>
                <Btns onClick={()=>{if(props.actConfirm) props.actConfirm()}} />
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.EventReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
const ModalContent = styled.div`
    position: relative;
    display: block;
    width: 628px;
    height: 472px;
    color: #FFFFFF;
    background-size: 100% 100%;
    background: top center no-repeat url(${Imglist.modal_bg});
    padding: 7%;
    box-sizing: border-box;
    .titlebox{
        >img{
            display: block;
            margin: -20px auto 0;
        }
    }
    .itembox{
        height: 280px;
        margin: 0 auto;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-flow: column;
        .item{
            width: 139px;
            height: 113px;
            >img{
                display: block;
                margin: 0 auto;
            }
        }
        .itemname{
            width: 206px;
            text-align: center;
            color: #ffffff;
            margin: 20px auto 0;
        }
    }
`;

const ModalBottom = styled.div`
    position: relative;
    display: inline-block;
    left: 50%;
    transform: translate3d(-50%,-50%,0);

`;

const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 214px;
    height: 67px;
    background:top center no-repeat url(${Imglist.btn_confirm});
    position:relative;
    text-align: center;
    box-sizing: border-box;
    padding-top: 3%;
    &:hover{
        background-position: bottom center;
    }
    &.cancel{
        background:top center no-repeat url(${Imglist.btn_cancel});
    }
`;
