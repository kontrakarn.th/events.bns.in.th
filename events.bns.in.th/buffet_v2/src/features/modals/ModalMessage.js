import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../main/redux';

import {Imglist} from './../../constants/Import_Images';
const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="message"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <Titlebox>
                    <img src={props.modal_type === 'alert' ?Imglist.alert_title : Imglist.reward_title} alt=""/>
                </Titlebox>
                <div className='message' dangerouslySetInnerHTML={{__html: modal_message}} />
            </ModalMessageContent>
            {
                props.to_select_char
                ?
                    <ModalBottom>
                        <Btns onClick={()=>props.setValues({modal_open:'selectcharacter'})} />
                    </ModalBottom>
                :
                    <ModalBottom>
                        <Btns onClick={()=>props.setValues({modal_open:''})} />
                    </ModalBottom>
            }

        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.EventReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 628px;
    height: 472px;
    color: #FFFFFF;
    background-size: 100% 100%;
    background: top center no-repeat url(${Imglist.modal_bg});
    padding: 7%;
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    align-items: center;
    .message{
        width: 80%;
        margin: 40px auto 0;
        font-size 30px;
        text-align: center;
        overflow-wrap: break-word;
    }
`;

const Titlebox = styled.div`
    position: absolute;
    left: 50%;
    top: 9%;
    transform: translate3d(-50%,0,0);
    img{
        display: block;
        margin: -20px auto 0;
    }
`
const ModalBottom = styled.div`
    position: relative;
    display: inline-block;
    left: 50%;
    transform: translate3d(-50%,-50%,0);

`;

const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 214px;
    height: 67px;
    background:top center no-repeat url(${Imglist.btn_confirm});
    position:relative;
    text-align: center;
    box-sizing: border-box;
    padding-top: 3%;
    &:hover{
        background-position: bottom center;
    }
`;
