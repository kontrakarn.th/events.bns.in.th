import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../main/redux';
import {Imglist} from '../../constants/Import_Images';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="preview"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent onClick={()=>props.setValues({modal_open:""})}>
                    <img key={'preview'+props.modal_preview} src={Imglist[props.modal_preview]} alt=""/>
            </ModalMessageContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.EventReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 977px;
    height: 641px;
    text-align: center;
    color: #FFFFFF;
    display:flex;
    justify-content: center;
    align-items: center;
    h2 {
        font-family: DBXtypeX;
        position: relative;
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-family: DBXtypeX;
        position: relative;
        top: 140px;
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
`;
