import React ,{Fragment}from 'react';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled, { keyframes } from 'styled-components';

import { onAccountLogout } from './../../actions/AccountActions';
import { setValues } from './../main/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ScrollArea from 'react-scrollbar';

import './css/style.css';
import Menu from '../menu/Menu.js';

class History extends React.Component {
     apiGetHistory(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'history'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open: '',
                    history_list: data.data
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_HISTORYLIST", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    componentDidMount() {
        if(this.props.jwtToken !== "" && this.props.history_list && this.props.history_list.length==0){
            this.apiGetHistory()
        }
    }
    render() {
      
        // let hisarry = Array.from(Array(1),( x , index)=> index+1);
        return (
            <div className="history">
                <Menu page={'history'}/>
                <div className="history__content">
                  <ScrollArea
                    speed={0.8}
                    key={'history_list_scroll'}
                    className="area"
                    contentClassName=""
                    horizontal={false}
                    style={{ width: "85%",height: "95%",opacity: "1",margin: "0 auto",padding: "0 3%"}}
                    verticalScrollbarStyle={{backgroundColor:"#000", opacity:1}}
                    smoothScrolling={true}
                    isDynamic={true}
                >
                  <ul className="history__list">
                    {this.props.history_list && this.props.history_list.length>0 && this.props.history_list.map((item,index) => {
                      return (
                                        <li class="history__list--list">
                                            <div>
                                            {
                                                item.token>0
                                                ?
                                                    "ใช้"
                                                :
                                                    "ได้รับ"
                                            }

                                            </div>
                                            <div>
                                            {
                                                item.token>0
                                                ?
                                                    "คูปองกินบุฟเฟ่ต์ฟรี จำนวน "+item.token+" ชิ้น แลก "+item.package_title
                                                :
                                                    item.package_title
                                            }

                                            </div>
                                            <div>{item.created_at_string_time}</div>
                                            <div></div>
                                        </li>
                                )
                            })
                        }
                        </ul>
                  </ScrollArea>
                </div>
                <ModalLoading />
                <ModalMessage />
            </div>
        )
    }
}
const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(History);
