import React from 'react';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled, { keyframes } from 'styled-components';

import { onAccountLogout } from './../../actions/AccountActions';
import { setValues } from './redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalShowReward from './../../features/modals/ModalShowReward';
import ModalSelectCharacter from '../../features/modals/ModalSelectCharacter';
import ModalPreview from '../../features/modals/ModalPreview';

import './css/style.css';
import Menu from '../menu/Menu.js';
import {Imglist} from './../../constants/Import_Images';

class Main extends React.Component {
    actConfirm(){
        this.apiEatBuffet();
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status) {
                if(data.data.selectd_char==true){
                    this.props.setValues({
                        username: data.data.username,
                        character_name: data.data.character_name,
                        selectd_char: data.data.selectd_char,
                        characters: data.data.characters,
                        can_play_1: data.data.can_play_1,
                        can_play_2: data.data.can_play_2,
                        can_play_3: data.data.can_play_3,
                        token: data.data.token,
                        modal_open: "",

                        count_playable_1: data.data.count_playable_1 || 0,
                        count_playable_2: data.data.count_playable_2 || 0,
                        count_playable_3: data.data.count_playable_3 || 0,
                        cumu_playable_1: data.data.cumu_playable_1 || 0,
                        cumu_playable_2: data.data.cumu_playable_2 || 0,
                        cumu_playable_3: data.data.cumu_playable_3 || 0,
                        event_can_play: data.data.event_can_play || false,
                    });
                }else{
                    this.props.setValues({
                        username: data.data.username,
                        character_name: data.data.character_name,
                        selectd_char: data.data.selectd_char,
                        characters: data.data.characters,
                        can_play_1: data.data.can_play_1,
                        can_play_2: data.data.can_play_2,
                        can_play_3: data.data.can_play_3,
                        token: data.data.token,
                        modal_open: "selectcharacter",

                        count_playable_1: data.data.count_playable_1 || 0,
                        count_playable_2: data.data.count_playable_2 || 0,
                        count_playable_3: data.data.count_playable_3 || 0,
                        cumu_playable_1: data.data.cumu_playable_1 || 0,
                        cumu_playable_2: data.data.cumu_playable_2 || 0,
                        cumu_playable_3: data.data.cumu_playable_3 || 0,
                        event_can_play: data.data.event_can_play || false,
                    });
                }

            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CHECKIN", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSelectCharacter(id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'select_character',id:id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selectd_char: data.data.selectd_char,
                    characters: data.data.characters,
                    can_play_1: data.data.can_play_1,
                    can_play_2: data.data.can_play_2,
                    can_play_3: data.data.can_play_3,
                    token: data.data.token,
                    modal_open: "",
                    
                    count_playable_1: data.data.count_playable_1 || 0,
                    count_playable_2: data.data.count_playable_2 || 0,
                    count_playable_3: data.data.count_playable_3 || 0,
                    cumu_playable_1: data.data.cumu_playable_1 || 0,
                    cumu_playable_2: data.data.cumu_playable_2 || 0,
                    cumu_playable_3: data.data.cumu_playable_3 || 0,
                    event_can_play: data.data.event_can_play || false,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiEatBuffet(){
        if(this.props.type_eat>0){
            this.props.setValues({ modal_open: "loading" });
            let dataSend = {type:'eat_buffet',id:this.props.type_eat};
            let successCallback = (data) => {
                if (data.status) {
                    this.props.setValues({
                        username: data.data.username,
                        character_name: data.data.character_name,
                        selectd_char: data.data.selectd_char,
                        characters: data.data.characters,
                        can_play_1: data.data.can_play_1,
                        can_play_2: data.data.can_play_2,
                        can_play_3: data.data.can_play_3,
                        token: data.data.token,
                        modal_open:'showreward',
                        modal_message:'ยินดีด้วยคุณได้รับ',
                        modal_item: data.reward,
                        history_list: [],

                        count_playable_1: data.data.count_playable_1 || 0,
                        count_playable_2: data.data.count_playable_2 || 0,
                        count_playable_3: data.data.count_playable_3 || 0,
                        cumu_playable_1: data.data.cumu_playable_1 || 0,
                        cumu_playable_2: data.data.cumu_playable_2 || 0,
                        cumu_playable_3: data.data.cumu_playable_3 || 0,
                        event_can_play: data.data.event_can_play || false,
                    });
                }
            };
            const failCallback = (data) => {
                if (data.type == 'no_permission') {
                    this.props.setValues({
                        permission: false,
                        modal_open: "",
                    });
                }else {
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: data.message,
                    });
                }
            };
            apiPost("REACT_APP_API_POST_EAT_BUFFET", this.props.jwtToken, dataSend, successCallback, failCallback);
        }

    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            modalSelectCharacter: true,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: ''
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }



    render() {
        return (
            <div className="main">
                <Menu page={'main'}/>
                <div className="main__contentwrap">
                        <div className="main__box1">
                        </div>
                        <div className="main__box2">
                               <div className="main__box2--gachawrap">
                                        <div className="main__box2--boxwrap">
                                                <img className="main__box2--ribbonbtn" src={Imglist.detail_ribbon} alt="" onClick={()=>this.props.setValues({modal_open:'preview',modal_preview:'modal_dessert'})}/>
                                                <div 
                                                  className={"main__box2--btn" + ((this.props.can_play_1 && this.props.event_can_play) ? '' : ' disable')}
                                                  onClick={()=>this.props.setValues({modal_open: 'confirm',modal_preview: 'dessert',type_eat:1})}
                                                >{(this.props.can_play_1 && this.props.event_can_play) ? "กินบุฟเฟ่ต์ ("+this.props.count_playable_1+")":"ไม่ตรงเงื่อนไข"}</div>
                                        </div>
                                        <div className="main__box2--boxwrap">
                                                <img className="main__box2--ribbonbtn" src={Imglist.detail_ribbon} alt="" onClick={()=>this.props.setValues({modal_open:'preview',modal_preview:'modal_fastfood'})}/>
                                                <div 
                                                  className={"main__box2--btn" + ((this.props.can_play_2 && this.props.event_can_play) ? '' : ' disable')} 
                                                  onClick={()=>this.props.setValues({modal_open: 'confirm',modal_preview: 'fastfood',type_eat:2})}
                                                >{(this.props.can_play_2 && this.props.event_can_play) ? "กินบุฟเฟ่ต์ ("+this.props.count_playable_2+")":"ไม่ตรงเงื่อนไข"}</div>
                                        </div>
                                        <div className="main__box2--boxwrap">
                                                <img className="main__box2--ribbonbtn" src={Imglist.detail_ribbon} alt="" onClick={()=>this.props.setValues({modal_open:'preview',modal_preview:'modal_drinks'})}/>
                                                <div 
                                                  className={"main__box2--btn" + ((this.props.can_play_3 && this.props.event_can_play) ? '' : ' disable')} 
                                                  onClick={()=>this.props.setValues({modal_open: 'confirm',modal_preview: 'drinks',type_eat:3})}
                                                > 
                                                  {(this.props.can_play_3 && this.props.event_can_play) ? "กินบุฟเฟ่ต์ ("+this.props.count_playable_3+")":"ไม่ตรงเงื่อนไข"}
                                                </div>
                                        </div>
                               </div>
                        </div>
                </div>
                <ModalSelectCharacter apiSelectCharacter={this.apiSelectCharacter.bind(this)}/>
                <ModalLoading />
                <ModalPreview/>
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                <ModalShowReward actConfirm={()=>this.props.setValues({modal_open:'',modal_item:{icon:0,name:""}})}/>
            </div>
        )
    }
}
const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);
