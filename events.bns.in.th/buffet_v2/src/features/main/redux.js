
const initialState = {
    modal_open: "none",
    modal_message: "",
    modal_type:'alert',
    to_select_char:false,
    modal_detail: "",
    modal_item: {
        "icon": 0,
        "name": "",
    },
    input_message: '',
    modal_send_item:{},
    modal_preview: '',
    type_eat:0,

    //--- event_info
    can_play_1: false,
    can_play_2: false,
    can_play_3: false,
    count_playable_1: 0,
    count_playable_2: 0,
    count_playable_3: 0,
    cumu_playable_1: 0,
    cumu_playable_2: 0,
    cumu_playable_3: 0,
    event_can_play: false,
    type_id: 0,
    token: 0,
    history_list:[
    ],
    itemlist : [
        {
            "id": 1,
            "name": "ชุดอสูรครามทอง",
            "icon": "gacha_6",
            "key": "gacha_6",
            "amt":75,
            "preview": 'preview_1',
        },
        {
            "id": 2,
            "name": "เครื่องประดับ|อสูรครามทอง",
            "icon": "gacha_7",
            "key": "gacha_7",
            "amt": 35,
            "preview": 'preview_1',
        },
        {
            "id": 3,
            "name": "กล่องอาวุธลวงตากุหลาบซันตามาเรีย",
            "icon": "gacha_8",
            "key": "gacha_8",
            "amt": 20,
            "preview": 'preview_2',
        },
        {
            "id": 4,
            "name": "ชุดนิมิตราชันย์เหมันต์",
            "icon": "gacha_9",
            "key": "gacha_9",
            "amt": 75,
            "preview": 'preview_3',
        },
        {
            "id": 5,
            "name": "หมวกกุ๊กไก่",
            "icon": "gacha_10",
            "key": "gacha_10",
            "amt": 20,
            "preview": 'preview_4',
        },
        {
            "id": 6,
            "name": "กิ๊บติดผมกุหลาบชมพู",
            "icon": "gacha_11",
            "key": "gacha_11",
            "amt": 20,
            "preview": 'preview_5',
        },
        {
            "id": 7,
            "name": "ชุดกมลพรรณอันทรงเกียรติ",
            "icon": "gacha_12",
            "key": "gacha_12",
            "amt": 50,
            "preview": 'preview_6',
        },
        {
            "id": 8,
            "name": "เครื่องประดับกมลพรรณอันทรงเกียรติ",
            "icon": "gacha_13",
            "key": "gacha_13",
            "amt": 20,
            "preview": 'preview_6',
        },
    ]

};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return {
          ...state,
          [action.key]: action.value
        };
      } else {
        return state;
      }
    case "SET_VALUES": return { ...state, ...action.value };
    default:
      return state;
  }
};

export const setValue  = (key, value) =>{ return ({ type: "SET_VALUE", value, key }) };
export const setValues = (value) =>{ return ({ type: "SET_VALUES", value }) };
