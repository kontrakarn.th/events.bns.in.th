// import coupon_ribbon from '../static/images/event/coupon_ribbon.png';
// import detail_ribbon from '../static/images/event/detail_ribbon.png';
// import exchange_character from '../static/images/event/exchange_character.png';
// import item_ribbon from '../static/images/event/item_ribbon.png';

// //item
import gacha_1 from '../static/images/event/item/gacha_1.png';
import gacha_2 from '../static/images/event/item/gacha_2.png';
import gacha_3 from '../static/images/event/item/gacha_3.png';
import gacha_4 from '../static/images/event/item/gacha_4.png';
import gacha_5 from '../static/images/event/item/gacha_5.png';
// import gacha_6 from '../static/images/event/item/gacha_6.png';
// import gacha_7 from '../static/images/event/item/gacha_7.png';
// import gacha_8 from '../static/images/event/item/gacha_8.png';
// import gacha_9 from '../static/images/event/item/gacha_9.png';
// import gacha_10 from '../static/images/event/item/gacha_10.png';
// import gacha_11 from '../static/images/event/item/gacha_11.png';
// import gacha_12 from '../static/images/event/item/gacha_12.png';
// import gacha_13 from '../static/images/event/item/gacha_13.png';
// import preview_1 from '../static/images/event/item/preview_1.png';
// import preview_2 from '../static/images/event/item/preview_2.png';
// import preview_3 from '../static/images/event/item/preview_3.png';
// import preview_4 from '../static/images/event/item/preview_4.png';

// import modal_fastfood from '../features/modals/images/modal_fastfood.jpg';
// import modal_dessert from '../features/modals/images/modal_dessert.jpg';
// import modal_drinks from '../features/modals/images/modal_drinks.jpg';

const cdnPath = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/buffet2/';
const coupon_ribbon = cdnPath+'coupon_ribbon.png';
const detail_ribbon = cdnPath+'detail_ribbon.png';
const exchange_character = cdnPath + 'exchange_character.png';
const item_ribbon = cdnPath + 'item_ribbon.png';

//item
// const gacha_1 = cdnPath + 'item/gacha_1.png';
// const gacha_2 = cdnPath + 'item/gacha_2.png';
// const gacha_3 = cdnPath + 'item/gacha_3.png';
// const gacha_4 = cdnPath + 'item/gacha_4.png';
// const gacha_5 = cdnPath + 'item/gacha_5.png';
const gacha_6 = cdnPath + 'item/gacha_6.png';
const gacha_7 = cdnPath + 'item/gacha_7.png';
const gacha_8 = cdnPath + 'item/gacha_8.png';
const gacha_9 = cdnPath + 'item/gacha_9.png';
const gacha_10 = cdnPath + 'item/gacha_10.png';
const gacha_11 = cdnPath + 'item/gacha_11.png';
const gacha_12 = cdnPath + 'item/gacha_12.png';
const gacha_13 = cdnPath + 'item/gacha_13.png';

const preview_1 = cdnPath + 'preview_1.jpg';
const preview_2 = cdnPath + 'preview_2.jpg';
const preview_3 = cdnPath + 'preview_3.jpg';
const preview_4 = cdnPath + 'preview_4.jpg';
const preview_5 = cdnPath + 'preview_5.jpg';
const preview_6 = cdnPath + 'preview_6.jpg';

const modal_fastfood = cdnPath + 'modal_fastfood.jpg';
const modal_dessert = cdnPath + 'modal_dessert.jpg';
const modal_drinks = cdnPath + 'modal_drinks.jpg';

// Buffet_btn_frame
const modal_bg = cdnPath + 'modal_bg.jpg';
const btn_confirm = cdnPath + 'confirm_btn.png';
const btn_cancel = cdnPath + 'cancel_btn.png';
const confirm_fastfood = cdnPath + 'confirm_fastfood.png';
const confirm_dessert = cdnPath + 'confirm_dessert.png';
const confirm_drinks = cdnPath + 'confirm_drinks.png';
const confirmtrade_title = cdnPath + 'confirmtrade_title.png';
const reward_title = cdnPath + 'reward_title.png';
const alert_title = cdnPath + 'alert_title.png';
const buffet_btn_frame = cdnPath + 'Buffet_btn_frame.png';

export const Imglist = {
	coupon_ribbon,
	detail_ribbon,
	exchange_character,
	item_ribbon,
	gacha_1,
	gacha_2,
	gacha_3,
	gacha_4,
	gacha_5,
	gacha_6,
	gacha_7,
	gacha_8,
	gacha_9,
	gacha_10,
	gacha_11,
	gacha_12,
	gacha_13,
	preview_1,
	preview_2,
	preview_3,
	preview_4,
	preview_5,
	preview_6,
	modal_fastfood,
	modal_dessert,
  modal_drinks,
  
  modal_bg,
  btn_confirm,
  btn_cancel,
  confirm_fastfood,
  confirm_dessert,
  confirm_drinks,
  confirmtrade_title,
  reward_title,
  alert_title,
  buffet_btn_frame,
}