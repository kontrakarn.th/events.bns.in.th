
import $ from "jquery";

export function apiGet(
        apiName,
        data,
        successCallback=()=>{},
        failCallback=()=>{},
        responseCallback=()=>{},
        errorCallback=()=>{}
    )
{
    fetch(process.env.REACT_APP_API_SERVER_HOST + process.env[apiName], {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'
        },
        body: JSON.stringify(data)
    }).then(response => {
        if (response.status == 200) {
            response.json().then(data => {
                if (data.status) {
                    successCallback(data);
                } else {
                    failCallback(data);
                }
            });
        } else {
            responseCallback(response);
        }
    }, error => {
        errorCallback(error);
    });
}

export function apiPost(
        apiName,
        dataIn,
        successCallback=()=>{},
        failCallback=()=>{},
        errorCallback=()=>{}
    )
{
    // console.log("apiGetCharacter");
    $.ajax({
        type: 'POST',
        url: process.env.REACT_APP_API_SERVER_HOST + process.env[apiName],
        data: dataIn,
        success: (data) => {

            if(data.status && data.status === true){
                successCallback(data);
            } else {
                failCallback(data);
            }
        },
        dataType: 'JSON',
        async: true,
        error: (error) => {
            //error
            errorCallback(error)
        }
    });
}