import bg from './../static/images/background.png';

import slide1 from './../static/images/slide/1.jpg'
import slide2 from './../static/images/slide/2.jpg'
import slide3 from './../static/images/slide/3.jpg'
import slide4 from './../static/images/slide/4.png'
import slide5 from './../static/images/slide/5.png'

import modal_bg from './../static/images/modal_bg.png'
import btn_default from './../static/images/btn_default.png'

import content1 from './../static/images/content1.png'
import content2 from './../static/images/content2.png'
import content3 from './../static/images/content3.png'
import btn_prev from './../static/images/btn_prev.png'
import btn_next from './../static/images/btn_next.png'
import bg_slidebar from './../static/images/bg_slidebar.png'
import pumpkin from './../static/images/pumpkin.png'

export const Imglist = {
	bg,
	modal_bg,
	btn_default,
	slide1,
	slide2,
	slide3,
	slide4,
	slide5,
	content1,
	content2,
	content3,
	btn_prev,
	btn_next,
	bg_slidebar,
	pumpkin 
}
