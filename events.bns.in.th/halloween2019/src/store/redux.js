const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    // showCharacterName: true,
    characters:[],
    username: "",
    coin: 0,
    character: "",
    selected_char: false,
    loginStatus: false,
    showScroll: true,
    noScroll: true,
    loading: false,
    permission: true,
    status: false,
    can_play: false,


    //===modal
    modal_open: "",//"loading","detail","showreward",""showreward","send","modalInput"
    modal_message: "",
    modal_item_token: "",
    modal_item_name: "",
    modal_uid: "",
    modal_get_name: "",
    modal_combin: -1,
    modal_trade: -1,

    //===event base value
    list_item: [
        {id: 1, name:"S_Moon_01_R", count: 0},
        {id: 2, name:"S_Moon_01_G", count: 0},
        {id: 3, name:"S_Moon_01_B", count: 0},
        {id: 4, name:"S_Moon_02_R", count: 0},
        {id: 5, name:"S_Moon_02_G", count: 0},
        {id: 6, name:"S_Moon_02_B", count: 0},
        {id: 7, name:"S_Moon_03_R", count: 0},
        {id: 8, name:"S_Moon_03_G", count: 0},
        {id: 9, name:"S_Moon_03_B", count: 0},
        {id: 10, name:"S_Moon_04_R", count: 0},
        {id: 11, name:"S_Moon_04_G", count: 0},
        {id: 12, name:"S_Moon_04_B", count: 0},
        {id: 13, name:"S_Moon_05_R", count: 0},
        {id: 14, name:"S_Moon_05_G", count: 0},
        {id: 15, name:"S_Moon_05_B", count: 0},
        {id: 16, name:"S_Moon_06_R", count: 0},
        {id: 17, name:"S_Moon_06_G", count: 0},
        {id: 18, name:"S_Moon_06_B", count: 0},
        {id: 19, name:"S_Moon_07_R", count: 0},
        {id: 20, name:"S_Moon_07_G", count: 0},
        {id: 21, name:"S_Moon_07_B", count: 0},
        {id: 22, name:"S_Moon_08_R", count: 0},
        {id: 23, name:"S_Moon_08_G", count: 0},
        {id: 24, name:"S_Moon_08_B", count: 0},
    ],

    list_redeem: [
        {package_id:1,name:"ชุดจันทราสีเงิน",icon:"S_Moon_01_Base"},
        {package_id:2,name:"เครื่องประดับจันทราสีเงิน",icon:"S_Moon_02_Base"},
        {package_id:3,name:"ต่างหูจันทราสีเงิน",icon:"S_Moon_04_Base"},
        {package_id:4,name:"ทรงผมจันทราสีเงิน",icon:"S_Moon_03_Base"},
        {package_id:5,name:"เครื่องประดับนักเรียนเกาหลียุค 80",icon:"S_Moon_07_Base"},
        {package_id:6,name:"หมวกนักเรียนเกาหลียุค 80",icon:"S_Moon_08_Base"},
        {package_id:7,name:"ชุดนักเรียนเกาหลียุค 80",icon:"S_Moon_06_Base"},
        {package_id:8,name:"หินสัตว์เลี้ยงสาวน้อยจากจันทรา",icon:"S_Moon_05_Base"},
    ],
    img2name: {
        "S_Moon_01_Base":"ชุดจันทราสีเงิน",
        "S_Moon_02_Base":"เครื่องประดับจันทราสีเงิน",
        "S_Moon_04_Base":"ต่างหูจันทราสีเงิน",
        "S_Moon_03_Base":"ทรงผมจันทราสีเงิน",
        "S_Moon_05_Base":"หินสัตว์เลี้ยงสาวน้อยจากจันทรา",
        "S_Moon_06_Base":"ชุดนักเรียนเกาหลียุค 80",
        "S_Moon_07_Base":"เครื่องประดับนักเรียนเกาหลียุค 80",
        "S_Moon_08_Base":"หมวกนักเรียนเกาหลียุค 80",
    },
    //combine
    materials: [],

    //===soul values
    buy_soul_result: [],
    buy_soul_count: 0,
    buy_soul_cost: 0,


    //===history values
    items_history: [],

    //===trade values
    exchangeinfo: {
        amt: 0,
        icon: "",
        id: 0,
        key: "",
        product_title: ""
    },

    //===send item
    target_uid:-1,
    target_username: "",
    my_bag: [
        {id:1,title:"ชุดจันทราสีเงิน",icon:"S_Moon_01_Base",key:"S_Moon_01",quantity:0},
        {id:2,title:"เครื่องประดับจันทราสีเงิน",icon:"S_Moon_02_Base",key:"S_Moon_02",quantity:0},
        {id:3,title:"ต่างหูจันทราสีเงิน",icon:"S_Moon_04_Base",key:"S_Moon_04",quantity:0},
        {id:4,title:"ทรงผมจันทราสีเงิน",icon:"S_Moon_03_Base",key:"S_Moon_03",quantity:0},
        {id:5,title:"เครื่องประดับนักเรียนเกาหลียุค 80",icon:"S_Moon_07_Base",key:"S_Moon_07",quantity:0},
        {id:6,title:"หมวกนักเรียนเกาหลียุค 80",icon:"S_Moon_08_Base",key:"S_Moon_08",quantity:0},
        {id:7,title:"ชุดนักเรียนเกาหลียุค 80",icon:"S_Moon_06_Base",key:"S_Moon_06",quantity:0},
        {id:8,title:"หินสัตว์เลี้ยงสาวน้อยจากจันทรา",icon:"S_Moon_05_Base",key:"S_Moon_05",quantity:0},
        {id:9,title:"เซ็ทเหมียวจันทราสีเงิน",icon:"S_Moon_09_Base",key:"S_Moon_09",quantity:0},
        {id:10,title:"เซ็ทนักเรียนเกาหลียุค 80",icon:"S_Moon_10_Base",key:"S_Moon_10",quantity:0},
        {id:11,title:"กล่องอาวุธลวงตาจันทราสีเงิน",icon:"S_Moon_11_Base",key:"S_Moon_11",quantity:0},
    ],
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         : return ({ ...state, sessionKey: action.value });
        case 'ACCOUNT_AUTHED'       : return ({ ...state, userData: action.value });
        case 'RECEIVE_SESSION_KEY'  : return ({ ...state, sessionKey: action.value });
        case 'SET_LOGIN_URL'        : return ({ ...state, loginUrl: action.value });
        case 'SET_LOGOUT_URL'       : return ({ ...state, logoutUrl: action.value });
        case 'ACCOUNT_LOGIN'        : return ({ ...state, authState: "LOGGED_IN" });
        case 'ACCOUNT_LOGOUT'       : return ({ ...state, authState: "LOGGED_OUT" });
        case 'ACCOUNT_AUTH_CHECKED' : return ({ ...state, authed: true });
        case 'SET_JWT_TOKEN'        : return ({ ...state, jwtToken: action.value });

        case "SET_VALUE"            : return (Object.keys(defaultState).indexOf(action.key) >= 0) ? { ...state, [action.key]: action.value } : state;
        case "SET_VALUES"           : return { ...state, ...action.value };
        default:
            return state;
    }
}

export const onAccountAuth          = (sessionKey)  => ({ type: 'ACCOUNT_AUTH',         value: sessionKey });
export const onAccountAuthed        = (userData)    => ({ type: 'ACCOUNT_AUTHED',       value: userData });
export const onAccountLogin         = ()            => ({ type: 'ACCOUNT_LOGIN',        value: "" });
export const onAccountLogout        = ()            => ({ type: 'ACCOUNT_LOGOUT',       value: "" });
export const receiveSessionKey      = (sessionKey)  => ({ type: 'RECEIVE_SESSION_KEY',  value: sessionKey });
export const setLoginUrl            = (url)         => ({ type: 'SET_LOGIN_URL',        value: url });
export const setLogoutUrl           = (url)         => ({ type: 'SET_LOGOUT_URL',       value: url });
export const onAccountAuthChecked   = ()            => ({ type: 'ACCOUNT_AUTH_CHECKE',  value: true })
export const setJwtToken            = (token)       => ({ type: 'SET_JWT_TOKEN',        value: token });

export const setValue               = (key, value) => ({ type: "SET_VALUE", value, key });
export const setValues              = (value) => ({ type: "SET_VALUES", value });

