const item_set_2_2 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/item_set_2_2.gif';

const home_1 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/home_1.jpg';
const home_2 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/home_2.jpg';
const home_3 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/home_3.jpg';
const home_4 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/home_4.jpg';
const gift_1 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/gift_1.jpg';
const history_1 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/history_1.jpg';
const logo = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/logo.png';
const btn_gacha = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/btn_gacha.png';
const btn_gacha_hover = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/btn_gacha_hover.png';
const item_set_1 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/item_set_1.png';
const item_set_2 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/item_set_2.png';
const btn_back = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/btn_back.png';
const btn_next = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/btn_next.png';
const item_1 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/item_1.png';
const item_2 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/item_2.png';
const btn_receive = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/btn_receive.png';
const btn_receive_hover = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/btn_receive_hover.png';
const btn_send = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/btn_send.png';
const btn_send_hover = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/btn_send_hover.png';
const label_number = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/label_number.png';
const bg_menu = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/bg_menu.png';
const bg_modal_message = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/bg_modal_message.png';
const bg_modal_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/bg_modal_confirm.png';
const btn_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/btn_confirm.png';
const btn_cancel = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/btn_cancel.png';
const bg_modal_uid = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/bg_modal_uid.png';
const bg_modal_reward = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/bg_modal_reward.png';
const btn_close = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/btn_close.png';
const bg_permission = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/f11/permission.jpg';
const icon_scroll = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/f11/icon_scroll.png';
const btn_home = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/bluejeans/f11/btn_home.png';

export default {
	home_2,
	home_1,
	home_3,
	home_4,
	gift_1,
	history_1,
	logo,
	btn_gacha,
	btn_gacha_hover,
	item_set_2,
	item_set_1,
	btn_back,
	btn_next,
	item_1,
	item_2,
	btn_receive,
	btn_receive_hover,
	btn_send,
	btn_send_hover,
	label_number,
	bg_menu,
	bg_modal_message,
	bg_modal_confirm,
	btn_confirm,
	btn_cancel,
	bg_modal_uid,
	bg_modal_reward,
	btn_close,
	bg_permission,
	icon_scroll,
	btn_home,
	item_set_2_2
}
