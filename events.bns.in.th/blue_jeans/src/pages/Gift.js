import React,{useState,useEffect} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import imgList from '../constants/Import_Images';
import F11Layout from '../features/F11Layout';
import {apiPost} from '../constants/Api';
import {setValues} from '../store/redux';
import Btn from "../features/Buttons";

const GiftPage = props => {
	const actSelcet = (id,remain)=>{
		if(remain > 0) {
			props.setValues({
				item_select: id
			});
		}
	}
	const actOpenReceiveModal = ()=>{
		let {item_list,item_select} = props;
		props.setValues({
			modal_open: "receive",
			modal_message: "ต้องการรับไอเทม<br/><span>"+item_list[item_select].package_name+'</span> ?',
		});
	}
	const actOpenSendModal = ()=>{
		props.setValues({
			modal_open: "send",
			friend_uid: "",
		});
	}
	const apiExchangeInfo = () => {
		if(props.jwtToken !== "") {
			props.setValues({ modal_open: "loading" });
			apiPost(
				"REACT_APP_API_POST_EXCHANGE_INFO",
				props.jwtToken,
				{type: "exchange_info"},
				(data)=>{//function successCallback
				props.setValues({
					...data.data,
					modal_open:"",
				});
				},
				(data)=>{//function failCallback
				props.setValues({
					modal_open: "message",
					modal_message: data.message,
				});
				},
			);
		}
	}
  	useEffect(()=>{ apiExchangeInfo() },[props.jwtToken]);
	let canReceive = (props.item_select !== -1) ? true: false;
	let item_name = [
		"หินล้ำค่า<br/>ลูกเจี๊ยบ",
		"หินสัตว์เลี้ยง<br/>น้องหมาสิงโต",
		"หินสัตว์เลี้ยง<br/>อัลปาก้า",
		"หินสัตว์เลี้ยง<br/>ยูนิคอร์น",
		"หินสัตว์เลี้ยง<br/>แร็คคูน",
		"หินสัตว์เลี้ยง<br/>โชกี้",
		"หินสัตว์เลี้ยง<br/>นกฮูก",
		"หินสัตว์เลี้ยง<br/>นางเงือก",
		"หินสัตว์เลี้ยง<br/>สาวน้อยใต้ทะเล",
		"หินสัตว์เลี้ยง<br/>ผีเสื้อโคมไฟ",
		"หินสัตว์เลี้ยง<br/>ภูติหิมะตัวน้อย",
	]
  	return (
		<F11Layout showUserName={true} page="gift">
			<GiftStyle>
				<img src={imgList.logo} alt='' className="gift__logo"/>
				{[1,2].map((item,index)=>{
				let {id,package_name,remain} = props.item_list[index];
					return (
						<Item
							num={item}
							lock={remain<=0}
							active={(props.item_select === -1) ? true : (index === props.item_select)}
						>
							<img src={imgList['item_'+item]} className="item__card"/>
							<div className="item__click" onClick={()=>actSelcet(index,remain)}/>
							<div className="item__btn">
								<img src={imgList.label_number} alt=''/>
								<div>จำนวนที่มี {remain}</div>
							</div>
						</Item>
					)
				})}
				<div className="gift__bottom">
					<Btn
						width="144px"
						height="49px"
						grayscale={canReceive? "0":"1"}
						active={canReceive}
						src={imgList.btn_receive}
						hover={imgList.btn_receive_hover}
						onClick={()=>actOpenReceiveModal()}
					/>
					<Btn
						width="144px"
						height="49px"
						grayscale={(canReceive && props.can_send_gift)? "0": "1"}
						active={(canReceive && props.can_send_gift)}
						src={imgList.btn_send}
						hover={imgList.btn_send_hover}
						onClick={()=>actOpenSendModal()}
					/>
				</div>
			</GiftStyle>
		</F11Layout>
  	)
}

const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(GiftPage);

const GiftStyle = styled.div`
	position: relative;
	display: block;
	width: 100%;
	height: 700px;
	background-image: url(${imgList.gift_1});
	background-color: #000000;
	.gift{
		&__logo{
			position: absolute;
			top: 15px;
			left: 162px;
			width: 134px !important;
			height: 61px !important;
		}
		&__bottom {
			display: flex;
			justify-content: center;
			align-items: center;
			position: absolute;
			bottom: 99px;
			left: 50%;
			transform: translate(-50%, 0px);
			>a{
				margin: 0 22px;
			}
		}
	}
`;
const Item = styled.div`
	position: absolute;
	${props=> props.num === 1 && "width: 116px;top: 293px;left: 419px;"}
	${props=> props.num === 2 && "width: 96px;top: 293px;left: 579px;"}
	pointer-events: ${props=>props.lock ? "none":"all"};
	filter: grayscale(${props=>props.lock ? "1":"0"});
	.item{
		&__card{
			width: 100%;
			display: block;
			${props=> props.num === 1 && "margin-bottom: 33px;"}
			${props=> props.num === 2 && "margin-bottom: 19px;"}
			filter: grayscale( ${props=>props.active?"0":"1"});
		}
		&__btn{
			position: relative;
			width: 121px;
			${props=> props.num === 1 && "margin-left: -5px;"}
			${props=> props.num === 2 && "margin-left: -9px;"}
			>img{
				width: 100%;
				display: block;
			}
			>div{
				position: absolute;
				width: 100%;
				top: 50%;
				transform: translate(0, -50%);
				text-align: center;
				font-family: "Kanit-Light", tahoma;
				color: #ffffff;
			}
		}
		&__click{
			position: absolute;
			top: 0;
			cursor: pointer;
			pointer-events: ${props=>props.lock ? "none":"all"};
			${props=> props.num === 1 && "left: 11px;width: 96px;height: 96px;"}
			${props=> props.num === 2 && "left: 0px;width: 96px;height: 96px;"}
		}
	}
`;
const GiftStyle_bg = styled.div`
	position: absolute;
	display: block;
	width: 980px;
	height: 1189px;
	background-image: url(${imgList.trade_frame});
	background-position: center;
	background-color: transparent ;
	background-repeat: no-repeat;
	top: 100px;
	left: 50%;
    transform: translate(-50%, 0);
  	.gift {
		&__bottom {
			display: flex;
			justify-content: center;
			align-items: center;
			width: 100%;
			position: absolute;
			top: 1115px;
			left: 50%;
			transform: translate(-50%, 0px);
			>a{
				margin: 0 20px;
			}
		}
  	}
`;
const SlotItem = styled.a`
  	position: absolute;
	${props=> props.num === 1 && "top: 340px;left: 239px;"}
	${props=> props.num === 2 && "top: 340px;left: 406px;"}
	${props=> props.num === 3 && "top: 340px;left: 574px;"}
	${props=> props.num === 4 && "top: 340px;left: 741px;"}

	${props=> props.num === 5 && "top: 573px;left: 239px;"}
	${props=> props.num === 6 && "top: 573px;left: 406px;"}
	${props=> props.num === 7 && "top: 573px;left: 574px;"}
	${props=> props.num === 8 && "top: 573px;left: 741px;"}

	${props=> props.num === 9 && "top: 806px;left: 322px;"}
	${props=> props.num === 10 && "top: 806px;left: 489px;"}
	${props=> props.num === 11 && "top: 806px;left: 657px;"}
	display: flex;
	justify-content: space-between;
	align-items: center;
	flex-direction: column;
	width: 120px;
	transform: translate(-50%,0);
	pointer-events: ${props=>props.lock ? "none":"all"};
	padding: 10px 0px;
	.txt_item_name{
		text-align: center;
		margin-top: 16px;
		line-height: 1.2;
		color: #543c24;
		font-family: "Kanit-Light", tahoma;
	}
	.frame{
		position: relative;
		width: 100%;
		height: 121px;
		${props=>props.lock
		?
			`background: url(${imgList.frame_disable}) no-repeat center;`
		:
			props.active
			?
				`background: url(${imgList.frame_active}) no-repeat center;`
			:
				`background: url(${imgList.frame}) no-repeat center;`
		}
		background-size: cover;
		>img{
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			width: 64px;
			height: 64px;
			filter: grayscale(${props=>props.lock ? "1":"0"});
		}
	}
`;
