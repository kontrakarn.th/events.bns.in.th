import React,{useState,useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import styled from 'styled-components';
import imgList from '../constants/Import_Images';
import Slider from "react-slick";
import F11Layout from '../features/F11Layout';
import Btn from "../features/Buttons";
import Pagination from '../features/Pagination';
import {apiPost} from '../constants/Api';
import {setValues} from '../store/redux';

const HomePage = props => {
  	const [itemSlide,setItemSlide] = useState(null);
  	const [itemPage,setItemPage] = useState(0);
	const itemSlideSettings = {
		dots: false,
		speed: 500,
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow: <SampleNextArrow />,
		prevArrow: <SamplePrevArrow />,
		afterChange: (p)=>setItemPage(p),
		appendDots: dots => (
			<div>
			<ul style={{ margin: "0px" }}> {dots} </ul>
			</div>
		),
		customPaging: i => (
			<div />
		)
	};
  	const apiEventInfo = () => {
		if(props.jwtToken !== "" && !props.status) {
			props.setValues({ modal_open: "loading" });
			apiPost(
				"REACT_APP_API_POST_EVENT_INFO",
				props.jwtToken,
				{type: "event_info"},
				(data)=>{//function successCallback
					props.setValues({
						...data.data,
						modal_open: ""
					});
				},
				(data)=>{//function failCallback
					props.setValues({
						modal_open: "message",
						modal_message: data.message,
					});
				},
			);
		}
  	}
  	const actShowModalConfirm = () => {
		console.log("actShowModalConfirm");
		if(props.jwtToken && props.jwtToken !== "") {
			props.setValues({
				modal_open: "confirm",
				modal_message: "ต้องการเปิดกล่อง ?",
			});
		}
  	}
  	useEffect(()=>{ apiEventInfo() },[props.jwtToken]);
  	return (
		<F11Layout showUserName={true} showCharacterName={true} page="home">
			<HomeStyle className="home">
				<section className="home__banner">
					<img src={imgList.home_1} alt=''/>
					<img src={imgList.logo} alt='' className="home__logo"/>
				</section>
				<section className="home__rule">
					<img src={imgList.home_2} alt=''/>
					<div className="home__btn-popup" onClick={()=>props.setValues({modal_open: 'reward'})}/>
				</section>
				<section className="home__gacha">
					<img src={imgList.home_3} alt=''/>
					<div className="home__gachabtn">
						<Btn
							width="117px"
							height="49px"
							grayscale={0}
							active={true}
							src={imgList.btn_gacha}
							hover={imgList.btn_gacha_hover}
							onClick={()=>actShowModalConfirm()}
						/> 
					</div>
				</section>
				<section className="home__items">
					<img src={imgList.home_4} alt=''/>
					<div className="home__slide">
						<Slider key="pointslide" {...itemSlideSettings} ref={e=>{if(e)setItemSlide(e)}}>
							<div><img className="home__itemslot" src={imgList.item_set_1} /></div>
							{/* <div><img className="home__itemslot" src={imgList.item_set_2} /></div> */}
							<div><img className="home__itemslot home__itemslot--gif" src={imgList.item_set_2_2} /></div>
						</Slider>
					</div>
				</section>
			</HomeStyle>
		</F11Layout>
  	)
}
const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(HomePage);


const HomeStyle= styled.div`
	position: relative;
	display: block;
	width: 100%;
	height: 3345px;
  	.home {
		&__banner {
			position: relative;
			width: 100%;
			height: 700px;
			>img{
				width: 100%;
				display: block;
			}
		}
		&__logo{
			position: absolute;
			top: 15px;
			left: 162px;
			width: 134px !important;
			height: 61px !important;
		}
		&__rule {
			position: relative;
			width: 100%;
			height: 1050px;
			>img{
				width: 100%;
				display: block;
			}
		}
		&__btn-popup{
			position: absolute;
			top: 543px;
			left: 306px;
			width: 32px;
			height: 32px;
			cursor: pointer;
		}
		&__gacha {
			position: relative;
			width: 100%;
			height: 600px;
			>img{
				width: 100%;
				display: block;
			}
		}
		&__gachabtn {
			position: absolute;
			bottom: 251px;
			left: 50%;
			transform: translate(-50%, 0);
			width: 117px;
			height: 49px;
			pointer-events: all;
		}
		&__items {
			position: relative;
			width: 100%;
			height: 995px;
			>img{
				width: 100%;
				display: block;
			}
		}
		&__itemslot {
			width: 100%;
			display: block;
			&--gif{
				width: auto;
				height: 690px;
				margin: -35px auto 0;
			}
		}
		&__slide {
			position: absolute;
			left: 50%;
			transform: translate(-50%, 0);
			top: 116.4px;
			width: 865px;
			.slick-dots {
				display: none;
				.slick-active {
					background-color: #e5e5e5;
				}
			}
		}
	}
	.btn {
		position: absolute;
		top: 543px;
		left: 305px;
		display: block;
		width: 33px;
		height: 33px;
	}
`;

const HideBeforeAfter = styled.div`
	display: block;
	width: 44px;
	height: 44px;
	z-index: 2;
	&.slick-next {
		top: 335px;
		right: -9px;
		// right: 23px;
	}
	&.slick-prev {
		top: 335px;
		left: -10px;
		// left: 13px;
	}
	&:before,&:after {
		content: "";
	}
`;
function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <HideBeforeAfter
        className={className}
        style={{ ...style, display: "block"}}
        onClick={onClick}
      >
        <img src={imgList.btn_next}/>
      </HideBeforeAfter>
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <HideBeforeAfter
          className={className}
          style={{ ...style, display: "block"}}
          onClick={onClick}
        >
          <img src={imgList.btn_back}/>
        </HideBeforeAfter>
    );
}
