import 'core-js/es6/map';
import 'core-js/es6/set';
import 'raf/polyfill';
import 'cross-fetch/polyfill';
import React from 'react';

import ReactDOM,{ hydrate, render }  from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import {combineReducers, createStore} from 'redux';
import {Web} from './routes/Web';
import serviceWorker from './serviceWorker';
import MainRedux from './store/redux';
import './styles/index.scss';

const store = createStore(combineReducers({ Main: MainRedux }));
const rootElement = document.getElementById('root');

ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<Web />
		</BrowserRouter>
 	</Provider>,
	document.getElementById('root')
)
