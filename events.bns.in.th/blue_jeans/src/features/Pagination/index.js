import React,{useState} from 'react';
import styled from 'styled-components';

export default (props) => {
  return (
    <PaginationStyle
      className="pagination"
      pinColor={props.pinColor|| "#FFFFFF"}
      pinActiveColor={props.pinActiveColor || "#999999"}
    >
      {(props.left_img && props.gotoPrev) &&
        <a onClick={()=>props.gotoPrev()}><img src={props.left_img} /></a>
      }
      {props.list.map((item,index)=>{
        return (
          <a
            key={"pin_"+index}
            className={(props.classPin || "pin") + (props.page === item ? " active":"")}
            onClick={()=>props.gotoPage(item)}
          />
        )
      })}
      {(props.right_img && props.gotoNext) &&
        <a onClick={()=>props.gotoNext()}><img src={props.right_img} /></a>
      }
      <a><div className="home__rankingdot" /></a>
      <a><div className="home__rankingdot" /></a>
      <a><img src={props.left_right} /></a>
    </PaginationStyle>
  )
}
const PaginationStyle= styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  .pin {
    width: 10px;
    height: 10px;
    margin: 0 5px;
    border-radius: 50%;
    background-color: ${props=>props.pinColor};
    &.active {
      background-color: ${props=>props.pinActiveColor};
    }
  }
  img {
    display:block;
  }
`;
