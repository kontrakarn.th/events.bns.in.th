import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import imgList from "../../constants/Import_Images";

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="message"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent className="mdmessage">
                <div className="mdmessage__content">
					<div className="mdmessage__text" dangerouslySetInnerHTML={{__html: modal_message}}/>
				</div>
                <div className="mdmessage__btns">
                  <Btn className='confirm' onClick={()=>props.setValues({modal_open: props.character === "" ?"selectcharacter" :""})} />
                </div>
            </ModalMessageContent>
        </ModalCore>
    )
}

const mstp = state => ({...state.Main});
const mdtp = { setValues };
export default connect( mstp, mdtp )(CPN);

const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 458px;
	height: 295px;
    color: #FFFFFF;
    background: no-repeat center url(${imgList.bg_modal_message});
    padding: 63px 55px 0px;
    .mdmessage{
		&__content{
			width: 100%;
			height: 155px;
			display: flex;
			justify-content: center;
			align-items: center;
		}
      	&__text {
			color: #040608;
			font-size: 20px;
			line-height: 28px;
			word-break: break-word;
			text-align: center;
			font-family: "Kanit-Light", tahoma;
			>span{
				color: #00633d;
			}
      	}
		&__btns {
			position: relative;
			margin: 100px auto;
			display: flex;
			justify-content: center;
			align-items: center;
			margin: 0 auto;
		}
  	}
`;

const Btn = styled.div`
	width: 94px;
	height: 40px;
	cursor: pointer;
	margin: 0 16px;
	background-size: cover;
	&.confirm{
		background: no-repeat center top url(${imgList.btn_confirm});
	}
	&.cancel{
		background: no-repeat center top url(${imgList.btn_cancel});
	}
	&:hover{
		background-position: bottom center;
	}
`
