import React from 'react';
import {connect} from 'react-redux';

import ModalMessage from './ModalMessage';
import ModalLoading from './ModalLoading';
import ModalConfirm from './ModalConfirm';
import ModalReceive from './ModalReceive';
import ModalSendFriend from './ModalSendFriend';
import ModalSendConfirm from './ModalSendConfirm';
import ModalReward from './ModalReward';

const Modals = (props)=> (
    <>
      <ModalLoading />
      <ModalMessage />
      <ModalConfirm />
      <ModalReceive />
      <ModalSendFriend />
      <ModalSendConfirm />
      <ModalReward />
    </>
)

const mstp = state => ({...state.Main});
const mdtp = {  };

export default connect( mstp, mdtp )(Modals);
