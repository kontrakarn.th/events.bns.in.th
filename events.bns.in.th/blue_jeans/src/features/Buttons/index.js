import styled from 'styled-components';

export default styled.a`
    display: block;
    width: ${props=>props.width};
    height: ${props=>props.height};
    filter: grayscale(${props=>props.grayscale});
    pointer-events: ${props=>props.active ? "all":"none"};
    background-image: url(${props=>props.src});
    background-repeat: no-repeat;
    &:hover {
      background-image: url(${props=>props.hover});
    }
`;
