#============#
# Production #
#============#

REACT_APP_EVENT_PATH=/blue_jeans
REACT_APP_STATIC_PATH=/static/
REACT_APP_LOCALE=th-TH
REACT_APP_CSRF_COOKIE_NAME=csrf_token
REACT_APP_COOKIE_DOMAIN=.bns.in.th

# API related
REACT_APP_API_SERVER_HOST=https://events.bns.in.th
REACT_APP_API_GET_OAUTH_INFO=/oauth/get_oauth

# API FOR EVENT
REACT_APP_API_POST_EVENT_INFO=/api/blue_jeans/event_info
REACT_APP_API_POST_BUY_GACHAPON=/api/blue_jeans/buy_gachapon
REACT_APP_API_POST_EXCHANGE_INFO=/api/blue_jeans/exchange_info
REACT_APP_API_POST_CHECK_UID=/api/blue_jeans/check_uid
REACT_APP_API_POST_SEND_REWARD=/api/blue_jeans/send_reward
REACT_APP_API_POST_HISTORY=/api/blue_jeans/history

# OAUTH related
REACT_APP_OAUTH_APP_ID=10007
REACT_APP_OAUTH_APP_NAME=bns_blue_jeans
REACT_APP_OAUTH_PLATFORM=1
REACT_APP_OAUTH_URL=https://auth.garena.com
REACT_APP_OAUTH_PARAM_NAME=token
REACT_APP_OAUTH_COOKIE_NAME=oauth_session

# JWT
REACT_APP_JWT_SECRET=bJZ6rhanFZ4rdYb3jwSX2CVSLQyzp8t5

# Landing Page
REACT_APP_LANDING_SERVER_HOST=https://landing.garena.in.th
REACT_APP_LANDING_JSON=/static/data/get_landing.json
