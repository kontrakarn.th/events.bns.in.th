import React, { Component } from 'react';
import {connect} from 'react-redux';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import {apiPost} from './../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';

import ModalMessage from '../components/ModalMessage';
import ModalConfirmRandomGachapon from '../components/ModalConfirmRandomGachapon';
import ModalConfirmExchangeReward from '../components/ModalConfirmExchangeReward';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
import itemsPackage from './../static/images/items.png'
import imgWoman from './../static/images/woman.png'
import imgMan from './../static/images/man.png'

import airpayItems1 from './../static/images/airpay_items1.png';
import airpayItems2 from './../static/images/airpay_items2.png';
import airpayItems3 from './../static/images/airpay_items3.png';
import airpayItems4 from './../static/images/airpay_items4.png';
import airpayItems5 from './../static/images/airpay_items5.png';
import airpayItems6 from './../static/images/airpay_items6.png';
import airpayItems7 from './../static/images/airpay_items7.png';

import gachapon1 from './../static/images/gachapon/1.png';
import gachapon2 from './../static/images/gachapon/2.png';
import gachapon3 from './../static/images/gachapon/3.png';
import gachapon4 from './../static/images/gachapon/4.png';
import gachapon5 from './../static/images/gachapon/5.png';
import gachapon6 from './../static/images/gachapon/6.png';
import gachapon7 from './../static/images/gachapon/7.png';
import gachapon8 from './../static/images/gachapon/8.png';

import {
    onAccountLogout
} from '../actions/AccountActions';

class HomeContainer extends Component {

    genClassGacha(baseClass,index,gachaSelected,infinite=false){
        if(gachaSelected < 0) {
            return baseClass;
        }
        if(infinite){
            return baseClass + " " + baseClass + "--rnd"+index+"infinite";
        }
        if(gachaSelected === index) {
            return baseClass + " " + baseClass + "--selected"
        } else {
            return baseClass + " " + baseClass + "--rnd"+index
        }
    }
// ========= ========= ========= ========= ========= ========= ========= ========= =========

    apiEventInfo(){
        this.setState({ showLoading: true});
        let self = this;
        let dataSend={ type : "event_info" };
        let successCallback = (data)=>{
            // console.log("data",data);
            this.setState({
                permission: true,
                showLoading: false,
                ...data.content
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            }else if(data.type === "no_char"){
                this.apiGetCharacter();
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                })
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO",this.props.jwtToken,dataSend,successCallback,failCallback);
    }

    apiRandomGachapon(){
        this.setState({ showModalConfirmRandomGachapon: false, rnd_gacha: true, onPlay: true});
        let self = this;
        let dataSend={ type : "airpay_gachapon" };
        let successCallback = (data)=>{
            this.setState({
                permission: true,
                showLoading: false,
                gacha: data.id-1,
                // rnd_gacha: false,
                // onPlay: false,
                // showModalMessage: data.message,
                ...data.content
            })

            setTimeout(()=>{
                this.setState({
                    rnd_gacha: false,
                    onPlay: false,
                })
            }, 800);
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    showLoading: false,
                    permission: false,
                    onPlay: false,
                });
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                    onPlay: false,
                })
            }
        };
        apiPost("REACT_APP_API_POST_GACHAPON",this.props.jwtToken,dataSend,successCallback,failCallback);
    }

    apiExchangeReward(packageId=0){
        this.setState({ showLoading: true, showModalConfirmExchangeReward: false});
        let self = this;
        let dataSend={ type : "exchange_reward", package_id: packageId };
        let successCallback = (data)=>{
            // console.log("data",data);
            this.setState({
                permission: true,
                showLoading: false,
                showModalMessage: data.message,
                codeVal: "",
                ...data.content
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    showLoading: false,
                    permission: false,
                    codeVal: "",
                });
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                    codeVal: "",
                })
            }
        };
        apiPost("REACT_APP_API_POST_EXCHANGE",this.props.jwtToken,dataSend,successCallback,failCallback);
    }
   
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
            showModalMessage: "",

            username: "",
            can_play: false,
            remain_all_rights: 0,
            my_total_diamonds: "0",
            my_total_rights: 0,
            my_remain_rights: 0,
            my_used_rights: 0,
            my_remain_points: 0,

            onPlay: false,

            showModalConfirmRandomGachapon: false,
            gacha: -1,
            rnd_gacha: false,

            showModalConfirmExchangeReward: false,
            packageName: "",
            packageId: 0,
            gachaponList: [
                {
                    img: gachapon1,
                },
                {
                    img: gachapon2,
                },
                {
                    img: gachapon3,
                },
                {
                    img: gachapon4,
                },
                {
                    img: gachapon5,
                },
                {
                    img: gachapon6,
                },
                {
                    img: gachapon7,
                },
                {
                    img: gachapon8,
                },
            ],

        }
    }

    componentDidMount(){
        // console.log(this.props.jwtToken);
        if(this.props.jwtToken !== "") setTimeout(()=>this.apiEventInfo(),600);
        
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.jwtToken !== prevProps.jwtToken){
            setTimeout(()=>{
                this.apiEventInfo()
            },600);
        }
    }

    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }

    actConfirmRandomGachapon(){
        this.setState({
            showModalConfirmRandomGachapon: true,
            gacha: -1,
            rnd_gacha: false,
        });
    }

    actConfirmExchangeReward(event,canExchange=false,packageId=0,packageName=''){
        if(canExchange==true){
            this.setState({
                showModalConfirmExchangeReward: true,
                packageName: packageName,
                packageId: packageId,
            });
        }
    }
   
    render() {
        return (
            <div>
            <div className="f11layout">
                <div className="f11layout__user">
                    <div className="f11layout__user--style">{this.state.username}</div>
                </div>
                <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
            </div>
            {this.state.permission ?
                <div className="events-bns">
                    <ScrollArea
                        speed={0.8}
                        className="area"
                        contentClassName=""
                        horizontal={false}
                        style={{ width: '100%', height:700, opacity:1}}
                        verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                        onScroll={this.handleScroll.bind(this)}

                    >
                        <div className="preorder">
                            <section className="preorder__section preorder__section--header">
                                <div className="header">
                                    <div className="header__scroll"><img src={iconScroll} alt="" /></div>
                                </div>
                            </section>
                            <section className="preorder__section condition">
                                <div className="container">
                                    <div className="section__inner container__bg">
                                        <div className="section__head">
                                            <h3>เงื่อนไขและโปรโมชั่น</h3>
                                        </div>
                                        <ul className="condition__text">
                                            <li>ระยะเวลาโปรโมชั่น 23 ม.ค. (00:00 น.) - 6 ก.พ. 2562 (23:59 น.)</li>
                                            <li>ผู้เล่นแลกไดมอนด์ผ่าน <span className="text--blue">AirPay</span> เท่านั้น</li>
                                            <li>เมื่อครบ 50,000 ไดมอนด์ จะได้รับสิทธิ์ในการสุ่ม <span className="text--blue">AirPay Gachapon</span>  1 สิทธิ์ (5 สิทธิ์ต่อ UID)</li>
                                            <li>ผู้เล่นสามารถแลกไดมอนด์แบบสะสมได้</li>
                                            <li>จำกัดสิทธิ์ 2,000 สิทธิ์แรกที่สุ่ม <span className="text--blue">AirPay Gachapon</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </section>
                            <section className="preorder__section current__status">
                                <div className="container">
                                    <img className="current__status--img" src={imgWoman} alt="" />
                                    <div className="section__inner container__bg">
                                        <div className="section__head">
                                            <h3>สถานะปัจจุบัน</h3>
                                        </div>
                                        <div>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>จำนวนไดมอนด์ที่แลก</th>
                                                    <th>สิทธิ์ที่คุณใช้</th>
                                                    <th>สิทธิ์ที่คุณมีทั้งหมด</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>{this.state.my_total_diamonds}</td>
                                                    <td>{this.state.my_used_rights}</td>
                                                    <td>{this.state.my_total_rights}</td>
                                                </tr>
                                            </tbody>
                                            </table>
                                            <div className="block__count">
                                                จำนวนสิทธิ์ที่เหลือทั้งหมด
                                                <div className="block__count--number">{this.state.remain_all_rights}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            {/* <section className="preorder__section text-center gachapon">
                                <div className="package container">
                                    <div className="section__inner container__bg">
                                        <div className="section__head">
                                            <h3>AirPay Gachapon</h3>
                                        </div>
                                       <img className="" src={itemsPackage} alt="" />
                                
                                       {
                                            this.state.can_play == true && this.state.onPlay == false ?
                                                <a className="btns__normal" onClick={this.actConfirmRandomGachapon.bind(this)}>  
                                                    <div>สุ่มของรางวัล</div>
                                                </a>
                                            :
                                                <a className="btns__normal received"><div>ไม่ตรงเงื่อนไข</div></a>

                                       }
                                    </div>
                                </div>
                            </section> */}

                            <section className="preorder__section text-center gachapon">
                                <div className="package container">
                                    <div className="section__inner container__bg">
                                        <div className="section__head">
                                            <h3>AirPay Gachapon</h3>
                                        </div>
                                        <div>
                                        {

                                            this.state.gachaponList.map((items, index) => {
                                                return(
                                                    <div className={this.genClassGacha("gachapon__items",index,this.state.gacha,this.state.rnd_gacha) + ( index < 2 ? " rare": "")}>
                                                        <img src={items.img}/>
                                                    </div>   
                                                )
                                            })
                                        }
                                        </div>
                                        {
                                            this.state.can_play == true && this.state.onPlay == false ?
                                                <a className="btns__normal" onClick={this.actConfirmRandomGachapon.bind(this)}>  
                                                    <div>สุ่มของรางวัล</div>
                                                </a>
                                                // <a className="gachapon__btnrandomgift" onClick={this.actConfirmRandomAdviceGachapon.bind(this)}><img src={btn_randomGift} /></a>
                                            :
                                                <a className="btns__normal received"><div>ไม่ตรงเงื่อนไข</div></a>
                                                // <a className="gachapon__btnrandomgift diabled"><img src={btn_randomGift} /></a>
                                        }    
                                    </div>
                                </div>
                            </section>
                            <section className="preorder__section airpay text-center">
                                <div className="container">
                                    <div className="section__inner container__bg">
                                        <div className="section__head">
                                            <h3>เหรียญ AirPay แลกไอเทม</h3>
                                        </div>
                                        <div class="clearfix">
                                            <div className="airpay__items">
                                                <img src={airpayItems1}/>
                                                <div className={"airpay__items--btn" + (this.state.my_remain_points < 5 ? " disabled" : "")} onClick={(event,canExchange,packageId,packageName)=>this.actConfirmExchangeReward(event,(this.state.my_remain_points >= 5),1,"ชุดระบำสิงโต")}>
                                                {
                                                    this.state.my_remain_points >= 5 ?
                                                        "แลกไอเทม"
                                                    :
                                                        "ไม่ตรงเงื่อนไข"
                                                }
                                                </div>
                                            </div>
                                            <div className="airpay__items">
                                                <img src={airpayItems2}/>
                                                <div className={"airpay__items--btn" + (this.state.my_remain_points < 3 ? " disabled" : "")} onClick={(event,canExchange,packageId,packageName)=>this.actConfirmExchangeReward(event,(this.state.my_remain_points >= 3),2,"หมวกระบำสิงโต")}>
                                                {
                                                    this.state.my_remain_points >= 3 ?
                                                        "แลกไอเทม"
                                                    :
                                                        "ไม่ตรงเงื่อนไข"
                                                }
                                                </div>
                                            </div>
                                            <div className="airpay__items">
                                                <img src={airpayItems3}/>
                                                <div className={"airpay__items--btn" + (this.state.my_remain_points < 3 ? " disabled" : "")} onClick={(event,canExchange,packageId,packageName)=>this.actConfirmExchangeReward(event,(this.state.my_remain_points >= 3),3,"หินเปลี่ยนรูปชั้นสูง")}>
                                                {
                                                    this.state.my_remain_points >= 3 ?
                                                        "แลกไอเทม"
                                                    :
                                                        "ไม่ตรงเงื่อนไข"
                                                }
                                                </div>
                                            </div>                               
                                        </div>
                                        <div className="clearfix">
                                            <div className="airpay__items">
                                                <img src={airpayItems4}/>
                                                <div className={"airpay__items--btn" + (this.state.my_remain_points < 2 ? " disabled" : "")} onClick={(event,canExchange,packageId,packageName)=>this.actConfirmExchangeReward(event,(this.state.my_remain_points >= 2),4,"หินจันทรา x50")}>
                                                {
                                                    this.state.my_remain_points >= 2 ?
                                                        "แลกไอเทม"
                                                    :
                                                        "ไม่ตรงเงื่อนไข"
                                                }
                                                </div>
                                            </div>
                                            <div className="airpay__items">
                                                <img src={airpayItems5}/>
                                                <div className={"airpay__items--btn" + (this.state.my_remain_points < 2 ? " disabled" : "")} onClick={(event,canExchange,packageId,packageName)=>this.actConfirmExchangeReward(event,(this.state.my_remain_points >= 2),5,"ยันต์อัพเกรด วิถีฮงมุนที่ส่องสว่าง")}>
                                                {
                                                    this.state.my_remain_points >= 2 ?
                                                        "แลกไอเทม"
                                                    :
                                                        "ไม่ตรงเงื่อนไข"
                                                }
                                                </div>
                                            </div>
                                            <div className="airpay__items">
                                                <img src={airpayItems6}/>
                                                <div className={"airpay__items--btn" + (this.state.my_remain_points < 1 ? " disabled" : "")} onClick={(event,canExchange,packageId,packageName)=>this.actConfirmExchangeReward(event,(this.state.my_remain_points >= 1),6,"ประกายฉลามดำ x5")}>
                                                {
                                                    this.state.my_remain_points >= 1 ?
                                                        "แลกไอเทม"
                                                    :
                                                        "ไม่ตรงเงื่อนไข"
                                                }
                                                </div>
                                            </div>
                                            <div className="airpay__items">
                                                <img src={airpayItems7}/>
                                                <div className={"airpay__items--btn" + (this.state.my_remain_points < 1 ? " disabled" : "")} onClick={(event,canExchange,packageId,packageName)=>this.actConfirmExchangeReward(event,(this.state.my_remain_points >= 1),7,"หินโซล x100")}>
                                                {
                                                    this.state.my_remain_points >= 1 ?
                                                        "แลกไอเทม"
                                                    :
                                                        "ไม่ตรงเงื่อนไข"
                                                }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="coin">
                                        <div className="coin__wrapper">
                                            จำนวนเหรียญ<br/>
                                            AirPay<br/>
                                            ของคุณ<br/>
                                            <div className="coin__wrapper--text">{this.state.my_remain_points}</div>
                                        </div>
                                        <img src={imgMan}/>
                                    </div>
                                </div>
                            </section>
                            
                        </div>
                    </ScrollArea>
                </div>
            :
                <Permission />
            }

            <Loading
                open={this.state.showLoading}
            />
            <ModalMessage
                open={this.state.showModalMessage.length > 0}
                actClose={()=>this.setState({showModalMessage: ""})}
                msg={this.state.showModalMessage}
            />
            <ModalConfirmRandomGachapon
                open={this.state.showModalConfirmRandomGachapon}
                actConfirm={()=>this.apiRandomGachapon()}
                actClose={()=>this.setState({
                    showModalConfirmRandomGachapon: false
                })}
                title={"ยืนยัน"}
                msg={'สุ่มของรางวัลจาก<br />Airpay Gachapon?'}
            />
            <ModalConfirmExchangeReward
                open={this.state.showModalConfirmExchangeReward}
                actConfirm={()=>this.apiExchangeReward(this.state.packageId)}
                actClose={()=>this.setState({
                    showModalConfirmExchangeReward: false
                })}
                title={"ยืนยัน"}
                msg={'แลก "' + this.state.packageName + '" ?'}
            />
        </div>          
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
    onAccountLogout: () => {
        dispatch(onAccountLogout());
    }
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
