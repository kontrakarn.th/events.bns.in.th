import { Imglist } from "../constants/Import_Images";

const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    showCharacterName: true,
    characters:[],
    username: "",
    coin: 0,
    character: "",
    selected_char: false,
    loginStatus: false,
    showScroll: true,
    noScroll: true,
    loading: false,
    permission: true,
    status: false,
    can_play: false,
    ranking_list:  Array(10).fill(1), // default array []

    //===modal
    modal_open: "",//"loading","detail","showreward",""showreward","send","modalInput"
    modal_message: "",
    modal_item: "",

//=== EVENT ===//
    //---history values
    history_list: [],

    //---main
    clan_members: 0,
    clan_ranks: [],
    clan_rewards: {
        step1: {id: 1, can_claim: false, is_claimed: false},
        step2: {id: 2, can_claim: false, is_claimed: false},
        step3: {id: 3, can_claim: false, is_claimed: false},
    },
    quests: [],
    rank_rewards: {},
    score: "0",

    choosegen_list: [
        {
            gen: 'ระดับเริ่มต้น',
            items: Imglist.items_box_beginner,
            diamond: '500'
        },
        {
            gen: 'ระดับกลาง',
            items: Imglist.items_box_middle,
            diamond: '1,000'
        },
        {
            gen: 'ระดับสูง',
            items: Imglist.items_box_high,
            diamond: '3,000'
        },
        {
            gen: 'ตำนาน',
            items: Imglist.items_box_legend,
            diamond: '5,000'
        },
    ],
    gen1_list: [
        {
            items_name: 'กล่องล้ำค่าของนักผจญภัยเริ่มต้น',
            items: Imglist.items2,
            open1_diamond: '500',
            open5_diamond: '2,500',
            type: "gen1PVE",
            index1: 0,
            index5: 1,
        },
        {
            items_name: 'กล่องล้ำค่าของนักสู้เริ่มต้น',
            items: Imglist.items1,
            open1_diamond: '500',
            open5_diamond: '2,500',
            type: "gen1PVP",
            index1: 2,
            index5: 3,
        }
    ],
    gen2_list:[
        {
            items_name: 'กล่องล้ำค่าของนักผจญภัยระดับกลาง',
            items: Imglist.middle_items2,
            open1_diamond: '1,000',
            open5_diamond: '5,000',
            type: "gen2PVE",
            index1: 4,
            index5: 5,
        },
        {
            items_name: 'กล่องล้ำค่าของนักสู้ระดับกลาง',
            items: Imglist.middle_items1,
            open1_diamond: '2,000',
            open5_diamond: '10,000',
            type: "gen2PVP",
            index1: 6,
            index5: 7,
        }
    ],
    gen3_list:[
        {
            items_name: 'กล่องล้ำค่าของนักผจญภัยระดับสูง',
            items: Imglist.high_items1,
            open1_diamond: '3,000',
            open5_diamond: '15,000',
            type: "gen3PVE",
            index1: 8,
            index5: 9,
        },
    ],
    gen4_list:[
        {
            items_name: 'กล่องของยอดฝีมือระดับตำนาน',
            items: Imglist.legend_items1,
            open1_diamond: '5,000',
            open5_diamond: '25,000',
            type: "gen4ALL",
            index1: 10,
            index5: 11,
        },
    ],
    receive_items:[
        {
            key: Imglist['Icon_20'],
            product_title: 'ลูกแก้วดาวเสาร์',
        },
        // {
        //     icon: Imglist.legend_items1,
        //     items_name: 'ลูกแก้วดาวเสาร์',
        // },
        // {
        //     icon: Imglist.legend_items1,
        //     items_name: 'ลูกแก้วดาวเสาร์',
        // },
        // {
        //     icon: Imglist.legend_items1,
        //     items_name: 'ลูกแก้วดาวเสาร์',
        // },
        // {
        //     icon: Imglist.legend_items1,
        //     items_name: 'ลูกแก้วดาวเสา',
        // },
    ]
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         : return ({ ...state, sessionKey: action.value });
        case 'ACCOUNT_AUTHED'       : return ({ ...state, userData: action.value });
        case 'RECEIVE_SESSION_KEY'  : return ({ ...state, sessionKey: action.value });
        case 'SET_LOGIN_URL'        : return ({ ...state, loginUrl: action.value });
        case 'SET_LOGOUT_URL'       : return ({ ...state, logoutUrl: action.value });
        case 'ACCOUNT_LOGIN'        : return ({ ...state, authState: "LOGGED_IN" });
        case 'ACCOUNT_LOGOUT'       : return ({ ...state, authState: "LOGGED_OUT" });
        case 'ACCOUNT_AUTH_CHECKED' : return ({ ...state, authed: true });
        case 'SET_JWT_TOKEN'        : return ({ ...state, jwtToken: action.value });

        case "SET_VALUE"            : return (Object.keys(defaultState).indexOf(action.key) >= 0) ? { ...state, [action.key]: action.value } : state;
        case "SET_VALUES"           : return { ...state, ...action.value };
        default:
            return state;
    }
}

export const onAccountAuth          = (sessionKey)  => ({ type: 'ACCOUNT_AUTH',         value: sessionKey });
export const onAccountAuthed        = (userData)    => ({ type: 'ACCOUNT_AUTHED',       value: userData });
export const onAccountLogin         = ()            => ({ type: 'ACCOUNT_LOGIN',        value: "" });
export const onAccountLogout        = ()            => ({ type: 'ACCOUNT_LOGOUT',       value: "" });
export const receiveSessionKey      = (sessionKey)  => ({ type: 'RECEIVE_SESSION_KEY',  value: sessionKey });
export const setLoginUrl            = (url)         => ({ type: 'SET_LOGIN_URL',        value: url });
export const setLogoutUrl           = (url)         => ({ type: 'SET_LOGOUT_URL',       value: url });
export const onAccountAuthChecked   = ()            => ({ type: 'ACCOUNT_AUTH_CHECKE',  value: true })
export const setJwtToken            = (token)       => ({ type: 'SET_JWT_TOKEN',        value: token });

export const setValue               = (key, value) => ({ type: "SET_VALUE", value, key });
export const setValues              = (value) => ({ type: "SET_VALUES", value });
