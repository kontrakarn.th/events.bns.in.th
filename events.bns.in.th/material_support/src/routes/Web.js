import React, {Component} from 'react';
import {Route} from 'react-router-dom';
// import OauthMiddleware from './../middlewares/OauthMiddleware';
import OauthMiddleware from './../features/oauthLogin';
// import Modals from './../features/modals';

import {Home}           from './../pages/Home';
import {HistoryPage}    from './../pages/History';
import {ChoosegenPage}  from './../pages/Choosegen'
import {Gen1Page}       from './../pages/Gen1'
import {Gen2Page}       from './../pages/Gen2'
import {Gen3Page}       from './../pages/Gen3'
import {Gen4Page}       from './../pages/Gen4'

export class Web extends Component {
    render() {
        return (
            <>
                <Route path={process.env.REACT_APP_EVENT_PATH} component={OauthMiddleware}/>
                {/*<Route path={process.env.REACT_APP_EVENT_PATH} component={Modals}/>*/}
                <Route path={process.env.REACT_APP_EVENT_PATH} exact component={Home}/>
                <Route path={process.env.REACT_APP_EVENT_PATH+'/history'} exact component={HistoryPage}/>
                <Route path={process.env.REACT_APP_EVENT_PATH+'/choosegen'} exact component={ChoosegenPage}/>
                <Route path={process.env.REACT_APP_EVENT_PATH+'/gen1'} exact component={Gen1Page}/>
                <Route path={process.env.REACT_APP_EVENT_PATH+'/gen2'} exact component={Gen2Page}/>
                <Route path={process.env.REACT_APP_EVENT_PATH+'/gen3'} exact component={Gen3Page}/>
                <Route path={process.env.REACT_APP_EVENT_PATH+'/gen4'} exact component={Gen4Page}/>
            </>
        );
    }
}
