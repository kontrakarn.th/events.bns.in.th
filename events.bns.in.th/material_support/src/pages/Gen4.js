import React from 'react';

import F11Layout from '../features/F11Layout';
import Gen4 from '../features/gen4';
import ModalConfrim from './../features/modals/ModalConfirm';

export class Gen4Page extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
            >
                <Gen4/>
                {/* <ModalConfrim/> */}
            </F11Layout>
        )
    }
}
