import React from 'react';

import F11Layout from '../features/F11Layout';
import Gen1 from '../features/gen1';
import ModalConfrim from './../features/modals/ModalConfirm';

export class Gen1Page extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
            >
                <Gen1/>
                {/* <ModalConfrim/> */}
            </F11Layout>
        )
    }
}
