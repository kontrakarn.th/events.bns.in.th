import React from 'react';

import F11Layout from '../features/F11Layout';
import Gen2 from '../features/gen2';
import ModalConfrim from './../features/modals/ModalConfirm';

export class Gen2Page extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
            >
                <Gen2/>
                {/* <ModalConfrim/> */}
            </F11Layout>
        )
    }
}
