import React from 'react';

import F11Layout from '../features/F11Layout';
import Choosegen from '../features/choosegen';

export class ChoosegenPage extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
            >
                <Choosegen/>
            </F11Layout>
        )
    }
}
