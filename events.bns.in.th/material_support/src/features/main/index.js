import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
// import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
import Logo from './BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import {Imglist} from './../../constants/Import_Images';
import { apiCheckin, actConfirm } from './../redux';

class Main extends React.Component {

    // actConfirm(token){
    //     if (token === undefined || token === ""){
    //         this.props.setValues({
    //             modal_open:"message",
    //             modal_message: "ไม่สามารถแลกของไอเทมได้",
    //         });
    //     }
    //
    //     this.props.setValues({ modal_open: "loading" });
    //     let dataSend = {type:'redeem',token:token};
    //     let successCallback = (data) => {
    //         if (data.status) {
    //             this.props.setValues({
    //                 modal_open:"message",
    //                 modal_message: data.message,
    //                 coin:data.data.coin,
    //
    //                 // clan_members: data.data.clan_members,
    //                 // clan_ranks: data.data.clan_ranks,
    //                 // clan_rewards: data.data.clan_rewards,
    //                 // quests: data.data.quests,
    //                 // rank_rewards: data.data.rank_rewards,
    //                 // score: data.data.score,
    //             });
    //             // this.setState({
    //             //     itemLists : data.data.itemLists,
    //             //     questLists : data.data.questLists,
    //             // })
    //         }else{
    //             this.props.setValues({
    //                 modal_open:"message",
    //                 modal_message: data.message,
    //             });
    //         }
    //     };
    //     const failCallback = (data) => {
    //         this.props.setValues({
    //             modal_open:"message",
    //             modal_message: data.message,
    //         });
    //     };
    //     // apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    //     // this.apiRedeem(
    //     //     this.props.modal_confirmitem && this.props.modal_confirmitem !== ''
    //     //     && this.props.modal_confirmitem
    //     // );
    // }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    // apiCheckin(){
    //     this.props.setValues({ modal_open: "loading" });
    //     let dataSend = {type:'event_info'};
    //     let successCallback = (data) => {
    //         if (data.status === true){
    //             this.props.setValues({
    //                 modal_open: "",
    //                 uid: data.data.uid,
    //                 character: data.data.nickname,
    //                 // coin: data.data.coin,
    //                 // selected_char: data.data.selected_char,
    //                 // characters: data.data.characters||[],
    //                 //
    //                 // clan_members: data.data.clan_members||0,
    //                 // clan_ranks: data.data.clan_ranks||[],
    //                 // clan_rewards: data.data.clan_rewards|| {
    //                 //     step1: {id: 1, can_claim: false, is_claimed: false},
    //                 //     step2: {id: 2, can_claim: false, is_claimed: false},
    //                 //     step3: {id: 3, can_claim: false, is_claimed: false},
    //                 // },
    //                 // quests: data.data.quests||[],
    //                 // rank_rewards: data.data.rank_rewards||{},
    //                 // score: data.data.score||"0",
    //             });
    //
    //             if (data.data.selected_char === false){
    //                 this.props.setValues({ modal_open: "selectcharacter" });
    //             }
    //         }
    //     };
    //     const failCallback = (data) => {
    //         if (data.type === 'no_permission') {
    //             this.props.setValues({
    //                 permission: false,
    //                 modal_open: "",
    //             });
    //         }else {
    //             this.props.setValues({
    //                 modal_open:"message",
    //                 modal_message: data.message,
    //             });
    //         }
    //     };
    //     apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    // }
    apiSelectCharacter(id=""){
        if (id === undefined || id === ""){
            return;
        }
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'select_character',id:id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open: "",
                    username: data.data.username,
                    character: data.data.character_name,
                    selectd_char: data.data.selected_char,
                    characters: data.data.characters,

                    clan_members: data.data.clan_members,
                    clan_ranks: data.data.clan_ranks,
                    clan_rewards: data.data.clan_rewards,
                    quests: data.data.quests,
                    rank_rewards: data.data.rank_rewards,
                    score: data.data.score,
                });
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiRedeem(type_id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type_id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content.response,
                    modal_open:"message",
                    modal_message: "ส่งของรางวัลสำเร็จ<br/>กรุณาตรวจสอบจดหมายภายในเกม"
                });

            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,
        }
    }

    // componentWillMount() {
    //     if(this.props.jwtToken !== ""){
    //         apiCheckin(this.props);
    //     }
    // }
    //
    // componentDidUpdate(prevProps, prevState){
    //     if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
    //         apiCheckin(this.props);
    //     }
    // }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            if (!this.props.uid) {
                apiCheckin(this.props);
            }
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            if (!this.props.uid) {
                apiCheckin(this.props);
            }
        }
    }

    render() {
        // let quests = this.props.quests || [];
        return (
            <PageWrapper>
                <Logo/>
                <Menu page='home'/>
                <Content>
                        <JoinBtn>
                            <Link to={`${process.env.REACT_APP_EVENT_PATH}/choosegen`}></Link>
                        </JoinBtn>
                        <DetailFrame src={Imglist['detail']} alt='detail'/>
                        {/* <ScoreFrame>
                                <div className="wrapper">
                                    {quests.map((item,key) => {
                                        if(key < 17) {
                                            return (
                                                <Box key={'box_'+(key+1)} pos={Math.floor(key/3)*200} start={268}>
                                                    {item.completed_count}
                                                </Box>
                                            )
                                        }

                                    })}
                                </div>
                                <div className="wrapper2">
                                    {quests.map((item,key) => {
                                        let n = key - 17;
                                        if(key > 16) {
                                            return (
                                                <Box key={'box2_'+(n+1)} pos={Math.floor(n/3)*200} start={248}>
                                                    {item.completed_count}
                                                </Box>
                                            )
                                        }

                                    })}
                                </div>
                        </ScoreFrame> */}
                </Content>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm
                  actConfirm={actConfirm.bind(this)}
                />
                {/* <ModalSelectCharacter actSelectCharacter={(e)=>this.apiSelectCharacter(e)}/> */}
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #050506;
    width: 1105px;
    padding-top: 740px;
    padding-bottom: 116px;
    text-align: center;
    position: relative;
    z-index: 20;
`

const Content = styled.div `
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }
    position: relative;

`
const DetailFrame = styled.img`
    display: block;
    margin: 60px auto 69px;
`

// const ScoreFrame = styled.div`
//     background: top center no-repeat url(${Imglist['main_content1']}),bottom center no-repeat url(${Imglist['main_content2']});
//     width: 922px;
//     height: 2793px;
//     display: block;
//     margin: 45px auto 0;
//     position: relative;
//     z-index: 25;
//     .wrapper{
//         width: 700px;
//         margin: 0 auto;
//         height: 1360px;
//         padding-top: 174px;
//         position: relative;
//     }
//     .wrapper2{
//         width: 700px;
//         margin: 0 auto;
//         height: 1360px;
//         position: relative;
//     }
// `

// const Box = styled.div`
//         width: 33px;
//         height: 33px;
//         margin: 0 auto;
//         position: absolute;
//         left: 170px;
//         top: ${props=>props.start}px;
//         color: #ffffff;
//         line-height: 33px;
//         &:nth-child(2){
//             left: 416px;
//         }
//         &:nth-child(3n){
//             left: 662px;
//         }
//         &:nth-child(3n+4){
//             top: calc(${props=>props.start}px + ${props=>props.pos}px);
//             left: 170px;
//         }
//         &:nth-child(3n+5){
//              top: calc(${props=>props.start}px + ${props=>props.pos}px);
//              left: 416px;
//         }
//         &:nth-child(3n+6){
//              top: calc(${props=>props.start}px + ${props=>props.pos}px);
//         }
// `

const JoinBtn = styled.div `
    a{
        background: top center no-repeat url(${Imglist['btn_join']});
        width: 211px;
        height: 54px;
        cursor: pointer;
        margin: 0 auto;
        position: absolute;
        top: -40%;
        left: 0;
        right: 0;
        &:hover{
            background-position: bottom center;
        }
    }
`
