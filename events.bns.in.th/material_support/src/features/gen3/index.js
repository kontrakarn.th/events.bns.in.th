import React from 'react';
// import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

// import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
import Logo from '../main/BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalReceive from './../../features/modals/ModalReceive'
import {Imglist} from './../../constants/Import_Images';
import { apiCheckin, actConfirm } from './../redux';

class Gen3 extends React.Component {

    // componentDidMount() {
    //     if(this.props.jwtToken !== "" && this.props.history_list && this.props.history_list.length === 0){
    //         // this.apiGetHistory()
    //     }
    // }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            if (!this.props.uid) {
                apiCheckin(this.props);
            }
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            if (!this.props.uid) {
                apiCheckin(this.props);
            }
        }
    }

    confirmOpen=(items_name, count, type, index)=>{
        this.props.setValues({
            modal_open: 'confirm',
            modal_message:'ต้องการซื้อกล่อง '+ items_name + ' x ' + count + ' ?',
            modal_item_token: type,
            modal_count: index,
            //modal_item: item,
        });
    }

    render() {
        return (
            <PageWrapper>
                <Logo/>
                <Menu page='gen3'/>
                <ChoosegenFrame>
                    <div className="header">ระดับสูง</div>
                    <div className="wrapper">
                        <ul className="gen">
                            {
                                this.props.gen3_list.map((item,key) => {
                                    return(
                                        <li key={key}>
                                            <div>{item.items_name}</div>
                                            <img src={item.items} className="gen__items" alt="" />

                                            <div className="btnwrapper">
                                                <div>
                                                    <div className="btn_open1" onClick={()=>this.confirmOpen(item.items_name,1, item.type, item.index1)}></div>
                                                    <div className="diamond">
                                                        {item.open1_diamond} <img src={Imglist.diamond} alt="" />
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="btn_open5" onClick={()=>this.confirmOpen(item.items_name,5, item.type, item.index5)}></div>
                                                    <div className="diamond">
                                                        {item.open5_diamond} <img src={Imglist.diamond} alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    )})
                            }
                        </ul>
                    </div>
                </ChoosegenFrame>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm
                  actConfirm={actConfirm.bind(this)}
                />
                <ModalReceive/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Gen3);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_other']}) #050506;
    width: 1105px;
    padding-top: 118px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const ChoosegenFrame = styled.div`
    background: top center no-repeat url(${Imglist['bg_content']});
    width: 922px;
    height: 582px;
    margin: 0 auto;
    box-sizing: border-box;
    padding: 50px;
    font-family: "Kanit-Medium",tahoma;
    .gen{
        //width: 700x;
        //height: 433px;
        margin: 0 auto;
        padding: 0;
        // overflow: auto;
        // overflow-x: hidden;
        color: #fff;
        font-size: 16px;
        text-shadow: 0 0 10px #8d92e8;
        -webkit-text-fill-color: #fff;
        -webkit-text-stroke-width: 1px;
        -webkit-text-stroke-color: #b3b7fb;

        display: flex;
        //justify-content: space-between;
        align-items: center;
        padding: 0 2%;
        box-sizing: border-box;
        &__items{
            margin: 40px 0;
        }
        li{
            flex: 1;
            border-right: 1px solid #5a628d;
            padding: 0 30px;
            &:last-child{
                border-right: 0;
            }
        }
        .diamond{
            font-size: 14px;
            img{
                vertical-align: middle;
            }
        }
        &::-webkit-scrollbar {
            width: 8px;
        }
        &::-webkit-scrollbar-track {
          background: rgba(173,173,173,0.05);
        }
        &::-webkit-scrollbar-thumb {
            background: #000;
        }
    }
    .header{
        color: #fff;
        font-size: 22px;
        text-shadow: 0 0 10px #8d92e8;
        -webkit-text-fill-color: #fff;
        -webkit-text-stroke-width: 1px;
        -webkit-text-stroke-color: #b3b7fb;
    }
    .btnwrapper{
        display:flex;
        justify-content: center;
        > div{
            margin: 0 10px;
        }
    }
    .btn_open1{
        background: top center no-repeat url(${Imglist['btn_open1']});
        width: 123px;
        height: 38px;
        margin: 0 auto 5px;
        cursor:pointer;
        &:hover{
            background-position: bottom center;
        }
    }
    .btn_open5{
        background: top center no-repeat url(${Imglist['btn_open5']});
        width: 123px;
        height: 38px;
        margin: 0 auto 5px;
        cursor:pointer;
        &:hover{
            background-position: bottom center;
        }
    }
    .wrapper{
        background: url(${Imglist['bg_content_inner']}) top center / 100% 100% no-repeat;
        padding: 60px 20px;
        margin-top: 30px;
    }
`
