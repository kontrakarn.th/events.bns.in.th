import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

// import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
import Logo from '../main/BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import {Imglist} from './../../constants/Import_Images';

class Choosegen extends React.Component {

    componentDidMount() {
        if(this.props.jwtToken !== "" && this.props.history_list && this.props.history_list.length === 0){
            // this.apiGetHistory()
        }
    }

    render() {
        return (
            <PageWrapper>
                <Logo/>
                <Menu page='choosegen'/>
                <ChoosegenFrame>
                    <div className="header">- เลือกระดับที่ต้องการ - </div>
                    <div className="wrapper">
                        <ul className="gen">
                            {
                                this.props.choosegen_list.map((item,key) => {
                                    return(
                                        <li key={key}>
                                            <div>{item.gen}</div>
                                            <img src={item.items} className="gen__items" alt="" />
                                            <div className="diamond">
                                                {item.diamond} <img src={Imglist.diamond} alt="" />
                                            </div>
                                            <Link className="btn_open" to={`${process.env.REACT_APP_EVENT_PATH}/gen` + (key+ 1)}></Link>
                                        </li>
                                    )})
                            }
                        </ul>
                    </div>
                </ChoosegenFrame>
                <ModalLoading />
                <ModalMessage />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Choosegen);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_other']}) #050506;
    width: 1105px;
    padding-top: 118px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const ChoosegenFrame = styled.div`
    background: top center no-repeat url(${Imglist['bg_content']});
    width: 922px;
    height: 582px;
    margin: 0 auto;
    box-sizing: border-box;
    padding: 50px;
    .gen{
        //width: 700x;
        //height: 433px;
        margin: 0 auto;
        padding: 0;
        // overflow: auto;
        // overflow-x: hidden;
        color: #fff;
        font-size: 16px;
        text-shadow: 0 0 10px #8d92e8;
        -webkit-text-fill-color: #fff;
        -webkit-text-stroke-width: 1px;
        -webkit-text-stroke-color: #b3b7fb;

        display: flex;
        //justify-content: space-between;
        align-items: center;
        padding: 0 2%;
        box-sizing: border-box;
        &__items{
            margin: 30px 0 7px ;
        }
        li{
            flex: 1;
            border-right: 1px solid #5a628d;
            padding: 0 30px;
            &:last-child{
                border-right: 0;
            }
        }
        .diamond{
            img{
                vertical-align: middle;
            }
        }
        &::-webkit-scrollbar {
            width: 8px;
        }
        &::-webkit-scrollbar-track {
          background: rgba(173,173,173,0.05);
        }
        &::-webkit-scrollbar-thumb {
            background: #000;
        }
    }
    .header{
        color: #fff;
        font-size: 22px;
        text-shadow: 0 0 10px #8d92e8;
        -webkit-text-fill-color: #fff;
        -webkit-text-stroke-width: 1px;
        -webkit-text-stroke-color: #b3b7fb;
        font-family: "Kanit-Medium",tahoma;
    }
    .btn_open{
        background: top center no-repeat url(${Imglist['btn_open']});
        width: 99px;
        height: 31px;
        margin: 10px auto 0;
        cursor:pointer;
        display: block;
        &:hover{
            background-position: bottom center;
        }
    }
    .wrapper{
        background: url(${Imglist['bg_content_inner']}) top center / 100% 100% no-repeat;
        padding: 60px 20px;
        margin-top: 30px;
        font-family: "Kanit-Medium",tahoma;
    }
`
