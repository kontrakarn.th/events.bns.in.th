import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import Logo from '../main/BNSLogo';
import { apiPost } from './../../middlewares/Api';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import {Imglist} from './../../constants/Import_Images';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';

class Ranking extends React.Component {
    numberWithCommas=(x)=> {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    confirmRedeemItem=(item)=>{
        this.props.setValues({
            modal_open: 'confirm',
            modal_message:'คุณยืนยันที่จะรับไอเทม ?',
            modal_item: item,
        });
    }
    actRedeemItem(){
        this.apiRankRedeem();
    }
// Rank_rewards: {rank: 0, clan_name: "", clan_score: "0", can_claim: false, is_claimed: false}
// can_claim: false
// clan_name: ""
// clan_score: "0"
// is_claimed: false
// rank: 0
    checkBtn(min, max) {
        let received = this.props.rank_rewards.is_claimed || false;
        let can = this.props.rank_rewards.can_claim || false;
        let in_rank = (this.props.rank_rewards.rank >=min && this.props.rank_rewards.rank <=max)? true: false
        if(!in_rank) {
            return "disable"
        }else if(received) {
            return "disable"
        } else if(can && in_rank) {
            return "pending"
        } else {
            return "disable"
        }
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiRankRedeem(id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {
            type:'redeem_rank_reward',
            package_key: this.props.modal_item
        };
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    ...data.data,

                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_RANK_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    render() {
        let {rank_rewards,clan_ranks} = this.props;
        let can_claim = rank_rewards.can_claim|| false;
        let clan_name = rank_rewards.clan_name|| "";
        let clan_score = rank_rewards.clan_score|| "0";
        let is_claimed = rank_rewards.is_claimed|| false;
        let rank = rank_rewards.rank|| 0;

        return (
            <PageWrapper>
                <Logo/>
                <Menu page='ranking'/>
                <Content>
                    <RankList>
                            {this.props.clan_ranks.map((clan,key) => {
                                return(
                                        <li key={`rank_${key+1}`}>
                                                <div>{clan.rank}</div>
                                                <div>{clan.name}</div>
                                                <div>{clan.score}</div>
                                        </li>
                                )
                            })}
                    </RankList>
                    <RedeemBtn className='prize1' status={this.checkBtn(1,1)} onClick={()=>this.confirmRedeemItem(4)}/>
                    <RedeemBtn className='prize2' status={this.checkBtn(2,5)} onClick={()=>this.confirmRedeemItem(5)}/>
                    <RedeemBtn className='prize3' status={this.checkBtn(6,10)} onClick={()=>this.confirmRedeemItem(6)}/>
                </Content>
                <ModalLoading/>
                <ModalMessage/>
                <ModalConfirm actConfirm={()=>this.actRedeemItem()} modal_item_token={"use redux"}/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Ranking);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_2']}) #050506;
    width: 1105px;
    padding-top: 122px;
    padding-bottom: 60px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const Content = styled.div`
    background: top center no-repeat url(${Imglist['ranking']});
    width: 922px;
    height: 1986px;
    margin: 0 auto;
    box-sizing: border-box;
    color: #ffffff;
    padding-top: 180px;
    position: relative;
    img{
        display: block;
        margin: 0 auto;
    }
`
const RankList = styled.ul`
    width: 595px;
    height: 320px;
    margin: 0 auto;
    overflow: auto;
    >li{
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin: 1% auto;
        >div:first-child{
            width: 80px;
        }
        >div:nth-child(2){
            width: 420px;
        }
        >div:last-child{
            width: 90px;
        }
    }
`

const RedeemBtn = styled.div`
    background: top center no-repeat url(${Imglist['btn_redeem']});
    width: 104px;
    height: 44px;
    cursor: pointer;
    position: absolute;
    left: 50%;
    transform: translate3d(-50%,0,0);
    ${props=>props.status === 'disable' &&
        `
            pointer-events: none;
            background-position: bottom center;
        `
    }
    &:hover{
        background-position: center;
    }
    &.prize1{
        top: 1047px;
    }
    &.prize2{
        top: 1451px;
    }
    &.prize3{
        bottom: 95px;
    }
`
