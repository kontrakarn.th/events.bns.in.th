import React from 'react';
// import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
import Logo from '../main/BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import {Imglist} from './../../constants/Import_Images';

class History extends React.Component {

    apiGetHistory(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'history'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open: '',
                    history_list: data.content
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_ITEM_HISTORY", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // componentDidMount() {
    //     if(this.props.jwtToken !== "" && this.props.history_list && this.props.history_list.length === 0){
    //         // this.apiGetHistory()
    //     }
    // }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            // if (!this.props.uid) {
                this.apiGetHistory(this.props);
            // }
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            if (!this.props.uid) {
                this.apiGetHistory(this.props);
            }
        }
    }

    render() {
        return (
            <PageWrapper>
                <Logo/>
                <Menu page='history'/>
                <HistroyFrame>
                    <div className="header">- ประวัติ - </div>
                        <ul className="list">
                            {
                                this.props.history_list.map((item,key) => {
                                    return(
                                        <li key={`history_${key+1}`}>
                                            <div>
                                                    {item.product_title} x {item.amount}
                                            </div>
                                            <div>
                                                    {item.log_time_string}
                                            </div>
                                        </li>
                                    )})
                            }
                        </ul>
                </HistroyFrame>
                <ModalLoading />
                <ModalMessage />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(History);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_other']}) #050506;
    width: 1105px;
    padding-top: 118px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const HistroyFrame = styled.div`
    background: top center no-repeat url(${Imglist['bg_history']});
    width: 922px;
    height: 582px;
    margin: 0 auto;
    box-sizing: border-box;
    padding: 50px 95px 0;
    font-family: "Kanit-Medium",tahoma;
    .list{
        width: 700x;
        height: 433px;
        margin: 0 auto;
        padding: 0;
        overflow: auto;
        overflow-x: hidden;
        color: #ffffff;
        >li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 0 2%;
            box-sizing: border-box;
            >div:first-child{
                width: 50%;
                text-align: left;
            }
        }
        &::-webkit-scrollbar {
            width: 8px;
        }
        &::-webkit-scrollbar-track {
          background: rgba(173,173,173,0.05);
        }
        &::-webkit-scrollbar-thumb {
            background: #000000;
        }
    }
    .header{
        color: #fff;
        font-size: 22px;
        text-shadow: 0 0 10px #8d92e8;
        -webkit-text-fill-color: #fff;
        -webkit-text-stroke-width: 1px;
        -webkit-text-stroke-color: #b3b7fb;
    }
`
