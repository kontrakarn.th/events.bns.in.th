import { apiPost } from './../middlewares/Api';
import { Imglist } from "../constants/Import_Images";

const initialState = {
    //--- event_info
    modal_open: "none",
    modal_message: "",
    modal_item_token: "",
    modal_confirmitem: '',
    modal_type: '',
    modal_item: '',
    modal_count: '',
};

export default (state = initialState, action) => {
    switch (action.type) {
        case "SET_VALUE":
            if (Object.keys(initialState).indexOf(action.key) >= 0) {
                return {
                    ...state,
                    [action.key]: action.value
                };
            } else {
                return state;
            }
        case "SET_VALUES": return { ...state, ...action.value };
        default:
            return state;
    }
};

export const setValue  = (key, value) =>{ return ({ type: "SET_VALUE", value, key }) };
export const setValues = (value) =>{ return ({ type: "SET_VALUES", value }) };

export const apiCheckin = (props) => {
    props.setValues({ modal_open: "loading" });
    let dataSend = {type:'event_info'};
    let successCallback = (data) => {
        if (data.status === true){
            props.setValues({
                modal_open: "",
                uid: data.data.uid,
                character: data.data.nickname,
                // coin: data.data.coin,
                // selected_char: data.data.selected_char,
                // characters: data.data.characters||[],
                //
                // clan_members: data.data.clan_members||0,
                // clan_ranks: data.data.clan_ranks||[],
                // clan_rewards: data.data.clan_rewards|| {
                //     step1: {id: 1, can_claim: false, is_claimed: false},
                //     step2: {id: 2, can_claim: false, is_claimed: false},
                //     step3: {id: 3, can_claim: false, is_claimed: false},
                // },
                // quests: data.data.quests||[],
                // rank_rewards: data.data.rank_rewards||{},
                // score: data.data.score||"0",
            });

            if (data.data.selected_char === false){
                props.setValues({ modal_open: "selectcharacter" });
            }
        }
    };
    const failCallback = (data) => {
        if (data.type === 'no_permission') {
            props.setValues({
                permission: false,
                modal_open: "",
            });
        }else {
            props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        }
    };
    apiPost("REACT_APP_API_POST_EVENT_INFO", props.jwtToken, dataSend, successCallback, failCallback);
}

export const actConfirm = (props, type) => {
    console.log(type)
    if (type === undefined || type === ""){
        props.setValues({
            modal_open:"message",
            modal_message: "ไม่สามารถแลกของไอเทมได้",
        });
    }

    props.setValues({ modal_open: "loading" });
    let dataSend = {type: type, number: props.modal_count};
    let successCallback = (data) => {
        if (data.status) {
            let receive_items = [];
            for (let i = 0 ; i < data.content.length; i++) {
                receive_items.push({
                    product_title: data.content[i].product_title,
                    key: Imglist[data.content[i].key],
                    amount: data.content[i].amount,
                });
            }

            console.log(receive_items)

            props.setValues({
                modal_open: "receive",
                modal_message: data.message,
                receive_items : receive_items,

                // clan_members: data.data.clan_members,
                // clan_ranks: data.data.clan_ranks,
                // clan_rewards: data.data.clan_rewards,
                // quests: data.data.quests,
                // rank_rewards: data.data.rank_rewards,
                // score: data.data.score,
            });
            // setState({
            //     itemLists : data.data.itemLists,
            //     questLists : data.data.questLists,
            // })
        }else{
            props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        }
    };
    const failCallback = (data) => {
        props.setValues({
            modal_open:"message",
            modal_message: data.message,
        });
    };
    apiPost("REACT_APP_API_POST_BUY_GACHA", props.jwtToken, dataSend, successCallback, failCallback);
    // this.apiRedeem(
    //     this.props.modal_confirmitem && this.props.modal_confirmitem !== ''
    //     && this.props.modal_confirmitem
    // );
}
