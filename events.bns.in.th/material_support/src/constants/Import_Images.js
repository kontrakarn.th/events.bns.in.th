import bg 						from '../static/images/bg.jpg';
import btn_cancel 				from '../static/images/btn_cancel.png';
import btn_confirm 				from '../static/images/btn_confirm.png';
import btn_home 				from '../static/images/btn_home.png';
import ranking 					from '../static/images/ranking.png';
import detail 					from '../static/images/detail.png';
import ico_scroll 				from '../static/images/ico_scroll.png';
import icon_dropdown 			from '../static/images/icon_dropdown.png';
import icon_menu 				from '../static/images/icon_menu.png';

import btn_join					from '../static/images/btn_join.png'
import bg_sidebar				from '../static/images/bg_sidebar.png'
import bg_history				from '../static/images/bg_history.png'
import bg_content				from '../static/images/bg_content.png'
import bg_content_inner			from '../static/images/bg_content_inner.png'
import bg_other					from '../static/images/bg_other.jpg'
import items_box				from '../static/images/items_box.jpg'
import diamond					from '../static/images/diamond.png'
import btn_open					from '../static/images/btn_open.png'
import items1					from '../static/images/items1.png'
import items2					from '../static/images/items2.png'
import btn_open1				from '../static/images/btn_open1.png'
import btn_open5				from '../static/images/btn_open5.png'
import bg_modal					from '../static/images/bg_modal.png'
import bg_modal_inner			from '../static/images/bg_modal_inner.png'
import items_box_beginner   	from '../static/images/items_box_beginner.jpg'
import items_box_middle   		from '../static/images/items_box_middle.jpg'
import items_box_high   		from '../static/images/items_box_high.jpg'
import items_box_legend   		from '../static/images/items_box_legend.jpg'
import middle_items1 			from '../static/images/middle_items1.jpg'
import middle_items2 			from '../static/images/middle_items2.jpg'
import high_items1				from '../static/images/high_items1.jpg'
import legend_items1			from '../static/images/legend_items1.jpg'
import bg_modal_receive_items	from '../static/images/bg_modal_receive_items.png'

import Icon_1	from '../static/images/icons/Icon_1.png'
import Icon_2	from '../static/images/icons/Icon_2.png'
import Icon_3	from '../static/images/icons/Icon_3.png'
import Icon_4	from '../static/images/icons/Icon_4.png'
import Icon_5	from '../static/images/icons/Icon_5.png'
import Icon_6	from '../static/images/icons/Icon_6.png'
import Icon_7	from '../static/images/icons/Icon_7.png'
import Icon_8	from '../static/images/icons/Icon_8.png'
import Icon_9	from '../static/images/icons/Icon_9.png'
import Icon_10	from '../static/images/icons/Icon_10.png'
import Icon_11	from '../static/images/icons/Icon_11.png'
import Icon_12	from '../static/images/icons/Icon_12.png'
import Icon_13	from '../static/images/icons/Icon_13.png'
import Icon_14	from '../static/images/icons/Icon_14.png'
import Icon_15	from '../static/images/icons/Icon_15.png'
import Icon_16	from '../static/images/icons/Icon_16.png'
import Icon_17	from '../static/images/icons/Icon_17.png'
import Icon_18	from '../static/images/icons/Icon_18.png'
import Icon_19	from '../static/images/icons/Icon_19.png'
import Icon_20	from '../static/images/icons/Icon_20.png'
import Icon_21	from '../static/images/icons/Icon_21.png'
import Icon_22	from '../static/images/icons/Icon_22.png'
import Icon_23	from '../static/images/icons/Icon_23.png'
import Icon_24	from '../static/images/icons/Icon_24.png'
import Icon_25	from '../static/images/icons/Icon_25.png'
import Icon_26	from '../static/images/icons/Icon_26.png'
import Icon_27	from '../static/images/icons/Icon_27.png'
import Icon_28	from '../static/images/icons/Icon_28.png'
// import Icon_29	from '../static/images/icons/Icon_29.png'
import Icon_30	from '../static/images/icons/Icon_30.png'
import Icon_31	from '../static/images/icons/Icon_31.png'
import Icon_32	from '../static/images/icons/Icon_32.png'
import Icon_33	from '../static/images/icons/Icon_33.png'
import Icon_34	from '../static/images/icons/Icon_34.png'
import Icon_35	from '../static/images/icons/Icon_35.png'
import Icon_36	from '../static/images/icons/Icon_36.png'
import Icon_37	from '../static/images/icons/Icon_37.png'
import Icon_38	from '../static/images/icons/Icon_38.png'
// import Icon_39	from '../static/images/icons/Icon_39.png'
import Icon_40	from '../static/images/icons/Icon_40.png'
import Icon_41	from '../static/images/icons/Icon_41.png'


export const Imglist = {
	bg,
	btn_cancel,
	btn_confirm,
	btn_home,
	ranking,
	detail,
	ico_scroll,
	icon_dropdown,
	icon_menu,
	btn_join,
	bg_sidebar,
	bg_history,
	bg_content,
	bg_content_inner,
	bg_other,
	items_box,
	diamond,
	btn_open,
	items1,
	items2,
	btn_open1,
	btn_open5,
	bg_modal,
	bg_modal_inner,
	items_box_beginner,
	items_box_middle ,
	items_box_high,
	items_box_legend,
	middle_items1,
	middle_items2,
	high_items1,
	legend_items1,
	bg_modal_receive_items,

	Icon_1,
	Icon_2,
	Icon_3,
	Icon_4,
	Icon_5,
	Icon_6,
	Icon_7,
	Icon_8,
	Icon_9,
	Icon_10,
	Icon_11,
	Icon_12,
	Icon_13,
	Icon_14,
	Icon_15,
	Icon_16,
	Icon_17,
	Icon_18,
	Icon_19,
	Icon_20,
	Icon_21,
	Icon_22,
	Icon_23,
	Icon_24,
	Icon_25,
	Icon_26,
	Icon_27,
	Icon_28,
	// Icon_29,
	Icon_30,
	Icon_31,
	Icon_32,
	Icon_33,
	Icon_34,
	Icon_35,
	Icon_36,
	Icon_37,
	Icon_38,
	// Icon_39,
	Icon_40,
	Icon_41,
}
