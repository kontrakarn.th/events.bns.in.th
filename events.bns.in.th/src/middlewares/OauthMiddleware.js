import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getCookie, setCookie, delCookie} from './../constants/Cookie';
import {QueryString} from './../constants/QueryString';
import {
	onAccountAuthed,
	onAccountLogin,
	onAccountLogout,
	eceiveSessionKey,
	setLoginUrl,
	setLogoutUrl,
	receiveSessionKey
} from './../actions/AccountActions';
import KJUR from 'jsrsasign';
import $ from "jquery";

/**
 * This container deals with OAUTH authentication; thus the name "OauthMiddleware".
 */
class OauthMiddleware extends Component {

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.authState !== this.props.authState) {
			switch (this.props.authState) {
				case "LOGGED_IN":
					this.login();
					break;
				case "LOGGED_OUT":
					this.logout();
					break;
			}
		}
	}

	componentDidMount() {
		// set login & logout url
		let currentLoc = window.location.href.toString();
		currentLoc = currentLoc.substring(currentLoc.length - 1) == "/" ? currentLoc : currentLoc + "/";
		let loginUrl = process.env.REACT_APP_OAUTH_URL + "/oauth/login?response_type=token&client_id="
			+ process.env.REACT_APP_OAUTH_APP_ID + "&redirect_uri=https://auth.garena.in.th/login/callback/" + process.env.REACT_APP_OAUTH_APP_NAME + "/"
			+ "&locale=" + process.env.REACT_APP_LOCALE + "&display=page&all_platform=1&theme=mshop_iframe_white";
		this.props.setLoginUrl(loginUrl);
		// console.log(QueryString[process.env.REACT_APP_OAUTH_PARAM_NAME]);
		if (QueryString[process.env.REACT_APP_OAUTH_PARAM_NAME]) {
			this.props.receiveSessionKey(QueryString[process.env.REACT_APP_OAUTH_PARAM_NAME]);
			this.props.onAccountLogin();
		}

		// if (!this.isLoggedIn()) {
		// 	this.checkAuth();
		// }
	}

	componentWillMount() {
		this.setupAjax();

        if (!this.isLoggedIn()) {
            this.checkAuth(false);
        }
	}

	setupAjax() {
        // check csrf token
        let token = getCookie(process.env.REACT_APP_CSRF_COOKIE_NAME);
        let headers = {};
        if (token) {
            delCookie(process.env.REACT_APP_CSRF_COOKIE_NAME);
            headers["X-CSRF-TOKEN"] = token;
        }

        /**
         * JWT
         */
        let beforeSend = (xhr, data) => {
            if (data.data) {
                // Header
                let oHeader = {
                    alg: 'HS256',
                    typ: 'JWT'
                };
                // Sign JWT
                let sHeader = JSON.stringify(oHeader);
                let sPayload = '{"' + decodeURI(data.data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}';
                let sJWT = KJUR.jws.JWS.sign("HS256", sHeader, sPayload, process.env.REACT_APP_JWT_SECRET);

                // Separate jwt header and signature and put them in authorization
                let parts = sJWT.split(".");
                xhr.setRequestHeader("Authorization", (["G", "a", "r", "e", "n", "a", "T", "H"].join("")) + " " + btoa(parts[0] + "." + parts[2]));
				// console.log("xhr",xhr);
                // Put jwt payload inside body
                data.data = btoa(parts[1]);
            }
        }

        $.ajaxSetup({
            headers: headers,
            beforeSend: beforeSend
        });
    }

    isLoggedIn() {
        return this.props.userData && this.props.userData.uid;
    }

    checkAuth() {
		fetch(process.env.REACT_APP_API_SERVER_HOST + process.env.REACT_APP_API_GET_ACCOUNT_INFO, {
			method: 'POST',
			body: '',
			credentials: 'same-origin',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			}
		}).then(response => {
			if (response.status === 200) {
				response.json().then(data => {
					if (data.account_info) {
						// session key exists in cookies when you are logged in
						let sessionKey = getCookie(process.env.REACT_APP_OAUTH_COOKIE_NAME);
						this.props.receiveSessionKey(sessionKey);
						this.props.onAccountAuthed(data.account_info);
						let redirect = "https://auth.garena.in.th/login/callback/" + process.env.REACT_APP_OAUTH_APP_NAME + "/logout";
						let logoutUrl = process.env.REACT_APP_OAUTH_URL + '/oauth/logout?access_token=' + sessionKey + "&format=redirect&redirect_uri=" + redirect;
						this.props.setLogoutUrl(logoutUrl);
					}/*  else {
						window.location.href = this.props.loginUrl;
					} */
				});
			} else {
			}
		}, error => {
			console.error(error);
		});
	}

	login() {
		setCookie(process.env.REACT_APP_OAUTH_COOKIE_NAME, this.props.sessionKey, 1, process.env.REACT_APP_COOKIE_DOMAIN);
		let uri = window.location.href.substring(0, window.location.href.lastIndexOf("?")); // get rid of the query string
		window.location.href = uri; // we need hard refresh
	}

	delNewCookie(name) {
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

	logout() {
		this.delNewCookie("oauth_session");
		delCookie(process.env.REACT_APP_OAUTH_COOKIE_NAME, process.env.REACT_APP_COOKIE_DOMAIN);
		window.location.href = this.props.logoutUrl;
	}

	render() {
		return (false);
	}
}

const mapStateToProps = (state) => ({
	sessionKey: state.AccountReducer.sessionKey,
	userData: state.AccountReducer.userData,
	authState: state.AccountReducer.authState,
	loginUrl: state.AccountReducer.loginUrl,
	logoutUrl: state.AccountReducer.logoutUrl
})

const mapDispatchToProps = (dispatch) => ({
	onAccountAuthed: userData => {
		dispatch(onAccountAuthed(userData));
	},
	onAccountLogin: () => {
		dispatch(onAccountLogin());
	},
	onAccountLogout: () => {
		dispatch(onAccountLogout());
	},
	receiveSessionKey: sessionKey => {
		dispatch(receiveSessionKey(sessionKey));
	},
	setLoginUrl: url => {
		dispatch(setLoginUrl(url));
	},
	setLogoutUrl: url => {
		dispatch(setLogoutUrl(url));
	}
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(OauthMiddleware);
