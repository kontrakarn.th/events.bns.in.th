import React, {Component} from 'react';
import {Route} from 'react-router-dom';
// import SSOMiddleware from './../middlewares/SSOMiddleware';
import OauthMiddleware from './../middlewares/OauthMiddleware';

import { Home } from './../pages/Home';

export class Web extends Component {
	render() {
		return (
			<div>
				<Route path="/" component={OauthMiddleware} />
				<Route path="/" exact component={Home} />
			</div>
		);
	}
}
