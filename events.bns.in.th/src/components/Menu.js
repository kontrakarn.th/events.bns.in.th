import React, { Component } from 'react'
import fb from './../static/images/btn_fb.png'
import cs from './../static/images/btn_cs.png'
import login from './../static/images/btn_login.png'
import logout from './../static/images/btn_logout.png'
import live from './../static/images/btn_live.png'

export default class Menu extends Component {
    render() {
        return (
            <div className="events-bns__bottom">
                <ul className="events-bns__bottom-list">
                    <li>
                        {this.props.userData && this.props.userData.uid ?
                            <a onClick={this.props.onLogout}>
                                <img src={logout} />
                            </a>
                        :
                            <a href={this.props.loginUrl}>
                                <img src={login} />
                            </a>
                        }
                    </li>
                    <li>
                        <a target="_self" href="https://www.facebook.com/th.bns/"><img src={fb} /></a>
                    </li>
                    <li>
                        <a target="_self" href="https://support.garena.in.th/new/games/faqs/2/blade-soul"><img src={cs} /></a>
                    </li>
                    <li>
                        <a target="_blank" href="https://garena.live/bnsliveth"><img src={live} /></a>
                    </li>
                </ul>
            </div>
        )
  }
}
