import React, { Component } from 'react'
// import Swiper from 'react-id-swiper';

import Slider from "react-slick";

/* const params = {
    slidesPerView: 4,
    slidesPerColumn: 2,
    slidesPerGroup:2,
    // slidesPerColumnFill:'row',
    spaceBetween: 0,
    rebuildOnUpdate: true,
    shouldSwiperUpdate: true,
    pagination: {
        el: '.events-bns-slide__dot',
        clickable: true,
    }
}; */

export default class EventsList extends Component {

    constructor(props){
        super(props);

        this.state = {
            eventList: [],
        };
        
        // this.swiper = null;
        this.slider = null;
    }

    componentDidMount() {
        // this.deferInitSwiper();
       
    }

    componentDidUpdate(prevProps, prevState){
        if(prevProps.eventList != this.props.eventList && this.props.eventList.length > 0){
            this.setState({
                eventList: this.props.eventList,
            });
        }
    }
       
   /*  deferInitSwiper() {
        let self = this;
        if (this.swiper && typeof this.swiper.update == 'function') {
            this.swiper.update();
        } else {
            setTimeout(function() {
                self.deferInitSwiper();
            }, 100);
        }
    } */

    render() {
        const settings = {
            arrows: false,
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 1,
            rows: 2,
            slidesPerRow: 4,
            centerPadding: "60px",
        };
        return (
            <div className="events-bns__list">
            {
                this.props.eventList.length > 0 ?
                    <Slider 
                        // ref={slider => (this.slider = slider)}
                        key="slick"
                        {...settings}
                    >
                        {
                            this.state.eventList.map((item,index) => {
                                return (
                                    <div className="events-bns__list--item" key={index}>
                                        <a href={item.event_path} target="_self">
                                            <img src={item.icon_path} alt="" />
                                            <span className="iconbutton__title">{item.title}</span>
                                        </a>
                                    </div>
                                    )
                                })
                        }
                    </Slider>
                :
                    null
            }
            </div>
            
        )
  }
}
