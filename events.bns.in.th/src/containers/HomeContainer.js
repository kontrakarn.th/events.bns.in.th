import React, { Component } from 'react'
import {connect} from 'react-redux'
import 'whatwg-fetch'
import './../styles/index.css'
import staticBgImg from './../static/images/background.jpg';
// import app1 from './../static/images/app_01.png'
// import app2 from './../static/images/app_02.png'
// import app3 from './../static/images/app_03.png'
// import app4 from './../static/images/app_04.png'
import {apiPost} from './../middlewares/Api';
import EventsList from '../components/EventsList';
import Menu from '../components/Menu';

import {Permission} from './../components/Permission';

import {
    onAccountLogout
} from './../actions/AccountActions';

class HomeContainer extends Component {
    constructor(props) {
        super(props)

        this.state = {
            login: false,
            permission: false,
            eventList: {},
            backGround: ""
        }
    }

    componentWillMount() {
        setTimeout(()=>{
            this.apiPostEventInfo()
        }, 600);
    }

    apiPostEventInfo(){
        let apiName="REACT_APP_API_POST_EVENT_INFO";
        let dataSend={ type : "hm_pad" };
        let successCallback = (data)=>{
            this.setState({
                eventList: data.content,
                backGround: data.background
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }
        };
        apiPost(apiName,dataSend,successCallback,failCallback);
    }

    onLogout(e) {
        e.preventDefault();
        const {dispatch} = this.props;
        dispatch(onAccountLogout());
    }
    
    render() {
        let backGroundImg = this.state.backGround != '' ? this.state.backGround : "";
        // console.log(backGroundImg)
        return (
                <div className="events-bns">
                    {this.state.permission && this.state.permission == true ?
                        <Permission open={this.state.permission} />
                    :
                        <div className="events-bns__bg" style={{backgroundImage: `url(${backGroundImg})`}}>
                            <div className="events-bns__wrapper">
                                <EventsList
                                    eventList={this.state.eventList}
                                />
                                <Menu
                                    userData={this.props.userData}
                                    loginUrl={this.props.loginUrl}
                                    logoutUrl={this.props.logoutUrl}
                                    onLogout={this.onLogout.bind(this)}
                                />
                            </div>
                        </div>
                    }
                </div>
            )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
	loginUrl: state.AccountReducer.loginUrl,
	logoutUrl: state.AccountReducer.logoutUrl,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)