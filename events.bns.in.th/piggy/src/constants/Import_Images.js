const bg = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/bg.jpg';
const content_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/content_1.png';
const content_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/content_2.png';
const content_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/content_3.png';
const content_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/content_4.png';
const btn_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/btn_confirm.png';
const btn_drop = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/btn_drop.png';
const btn_smash = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/btn_smash.png';
const bg_history = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/bg_history.jpg';
const board_history = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/board_history.png';
const icon_info = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/icon_info.png';
const info_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/info_1.png';
const info_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/info_2.png';
const icon_hover = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/icon_hover.png';
const bg_modal = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/bg_modal.png';
const btn_ok = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/btn_ok.png';
const btn_cancel = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/btn_cancel.png';
const icon_arrow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/icon_arrow.png';
const frame = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/frame.png';


const quest_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_1.png';
const quest_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_2.png';
const quest_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_3.png';
const quest_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_4.png';
const quest_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_5.png';
const quest_6 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_6.png';
const quest_7 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_7.png';
const quest_8 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_8.png';
const quest_9 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_9.png';
const quest_10 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_10.png';
const quest_11 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_11.png';
const quest_12 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_12.png';
const quest_13 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_13.png';
const quest_14 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_14.png';
const quest_15 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_15.png';
const quest_16 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/quest/quest_16.png';

const items_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/items/items_1.png';
const items_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/items/items_2.png';
const items_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/items/items_3.png';
const items_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/items/items_4.png';
const items_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/items/items_5.png';
const items_6 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/items/items_6.png';
const items_7 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/items/items_7.png';

const btn_droped = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/piggy/images/btn_droped.png';

export const Imglist = {
	bg,
	content_1,
	content_2,
	content_3,
	content_4,
	btn_confirm,
	btn_drop,
	btn_smash,
	bg_history,
	board_history,
	bg_modal,
	btn_ok,
	btn_cancel,
	icon_info,
	info_1,
	info_2,
	icon_hover,
	icon_arrow,
	frame,
	quest_1,
	quest_2,
	quest_3,
	quest_4,
	quest_5,
	quest_6,
	quest_7,
	quest_8,
	quest_9,
	quest_10,
	quest_11,
	quest_12,
	quest_13,
	quest_14,
	quest_15,
	quest_16,
	items_1,
	items_2,
	items_3,
	items_4,
	items_5,
	items_6,
	items_7,
	btn_droped
}
