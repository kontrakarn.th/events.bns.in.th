import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../../store/redux';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import {Imglist} from './../../constants/Import_Images';

import { apiPost } from './../../middlewares/Api';

class History extends React.Component {

    // ===========

    apiHistoryInfo(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'history'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    modal_open: "",
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters,
                    daily_quests: data.data.daily_quests,
                    claim_quests: data.data.claim_quests,
                    claim_bank: data.data.claim_bank,
                });

                if (data.data.selected_char === false){
                    this.props.setValues({ modal_open: "selectcharacter" });
                }
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_HISTORY", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ===========


    constructor(props) {
        super(props)
        this.state = {
            activeTab: 1
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiHistoryInfo();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiHistoryInfo();
        }

    }

    handleChangeTab(index){
        if(this.state.activeTab!==index){
            this.setState({
                activeTab: index
            })
        }
    }
    render() {
        return (
            <PageWrapper>
                <Content>
                    <Content_1 className="content_1">
                        <img src={Imglist.board_history} alt=''/>
                        <div className="content_1__menu">
                            <div className={(this.state.activeTab===1?"active":"")} onClick={()=>this.handleChangeTab(1)}>ประวัติการเลือกภารกิจ</div>
                            <div className={(this.state.activeTab===2?"active":"")} onClick={()=>this.handleChangeTab(2)}>ประวัติการหยอดกระปุกออมสินหมู</div>
                            <div className={(this.state.activeTab===3?"active":"")} onClick={()=>this.handleChangeTab(3)}>ประวัติการทุบกระปุกออมสินหมู</div>
                        </div>
                        {this.state.activeTab===1 &&
                            <div className="content_1__detail">
                                {
                                    this.props.daily_quests && this.props.daily_quests.length > 0 ?  
                                        this.props.daily_quests.map((item, key) => {
                                            return (
                                                <React.Fragment key={key}>
                                                    <div className="content_1__head">
                                                        {item.date}
                                                    </div>
                                                    {
                                                        item.quest_list && item.quest_list.length > 0 ?
                                                            item.quest_list.map((item2, key2) => {
                                                                return (
                                                                    <div className="content_1__row">
                                                                        <div>{item2.quest_title}</div>
                                                                        <div>{item2.status}</div>
                                                                        <div>{item2.completed_time}</div>
                                                                    </div>
                                                                )
                                                            })
                                                        :
                                                            null
                                                    }
                                                    <div className="content_1__line"></div>
                                                </React.Fragment>
                                            );
                                        })
                                    :
                                        null
                                }
                            </div>
                        }
                        {this.state.activeTab===2 &&
                            <div className="content_1__detail">
                                {
                                    this.props.claim_quests && this.props.claim_quests.length > 0 ?  
                                        this.props.claim_quests.map((item, key) => {
                                            return (
                                                <React.Fragment key={key}>
                                                    <div className="content_1__head">
                                                        {item.date}
                                                    </div>
                                                    {
                                                        item.quest_list && item.quest_list.length > 0 ?
                                                            item.quest_list.map((item2, key2) => {
                                                                return (
                                                                    <div className="content_1__row">
                                                                        <div>{item2.title}</div>
                                                                        <div>{item2.completed_time}</div>
                                                                    </div>
                                                                )
                                                            })
                                                        :
                                                            null
                                                    }
                                                    <div className="content_1__line"></div>
                                                </React.Fragment>
                                            );
                                        })
                                    :
                                        null
                                }
                            </div>
                        }
                        {this.state.activeTab===3 &&
                            <div className="content_1__detail">
                                {
                                    this.props.claim_bank && this.props.claim_bank.length > 0 ?  
                                        this.props.claim_bank.map((item, key) => {
                                            return (
                                                <React.Fragment key={key}>
                                                    <div className="content_1__row">
                                                        <div>รอบวันที่ {item.round}</div>
                                                        <div>{item.status}</div>
                                                    </div>
                                                    <div className="content_1__line"></div>
                                                </React.Fragment>
                                            );
                                        })
                                    :
                                        null
                                }
                            </div>
                        }
                    </Content_1>
                </Content>
                <ModalLoading/>
                <ModalMessage/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(History);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_history']}) #11122d;
    width: 1105px;
    height: 700px;
    text-align: center;
`
const Content = styled.div `  
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }
    display: flex;
    align-items: flex-end;
    height: 100%;
`

const Content_1 = styled.div `  
    position: relative;
    width: 80%
    margin: 0 auto;
    >img{
        width: 100%;
        display: block;
    }
    .content_1{
        &__menu{
            position: absolute;
            width: 70%;
            top: 9%;
            left: 50%;
            transform: translate(-50%, 0);
            font-family: "Kanit-Regular",tahoma;
            display: flex;
            align-items: center;
            justify-content: space-between;
            >div{
                color: #d99d9d;
                font-size: 15px;
                text-decoration: underline;
                cursor: pointer;
                &:hover{
                    color: #fff;
                }
            }
        }
        &__detail{
            position: absolute;
            width: 65%;
            height: 71%;
            overflow-y: scroll;
            top: 29%;
            left: 50%;
            transform: translate(-50%, 0);
            &::-webkit-scrollbar {
                width: 12px;
                background: #4d2e2e;
            }
            &::-webkit-scrollbar-thumb {
                background: #6e5454;
            }
        }
        &__head{
            font-family: "Kanit-Regular",tahoma;
            color: #fff;
            font-size: 15px;
            text-align: left;
            margin-bottom: 1%;
        }
        &__row{
            display: flex;
            align-items: flex-start;
            justify-content: space-between;
            font-family: "Kanit-Light",tahoma;
            font-size: 15px;
            color: #dcb9b9;
            margin: .5% 0;
            >div{
                width: 20%;
                &:first-child{
                    width: 60%;
                    text-align: left;
                }
            }
        }
        &__row2{
            display: flex;
            align-items: flex-start;
            justify-content: space-between;
            font-family: "Kanit-Light",tahoma;
            font-size: 15px;
            color: #dcb9b9;
            margin: .5% 0;
            >div{
                width: 20%;
                &:first-child{
                    width: 80%;
                    text-align: left;
                }
            }
        }
        &__line{
            margin: 5% 0;
            border-bottom: 1px solid #d29494;
        }
    }
`
