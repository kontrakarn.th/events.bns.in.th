import React from 'react';
import {connect} from 'react-redux';
import { Menu } from './../../features/Menu';
import ScrollArea from 'react-scrollbar';
import styled from 'styled-components';
import Loading from './Loading';
import Permission from './Permission';
import MenuOverDisplay from './MenuOverDisplay';

import { setValue, onAccountLogout } from './../../store/redux';

// import { setValue, submit, submit2 } from "./redux";
const LayoutF11 = props => {
    let nameText = "";
    if(props.showUserName && props.username !== ""){
        nameText = props.username;
    }
    if(props.showCharacterName && props.character !== ""){
        nameText = (nameText !== "")? nameText+" : "+props.character : props.character;
    }
    if(!props.permission) return <Permission />;
    return (
        <Layout background={props.background}>
            <Menu
                page={props.page}
            />
            {props.loading &&
                <Loading />
            }
            <OverAll>
                <MenuOverDisplay
                    showUserName={props.showUserName}
                    showCharacterName={props.showCharacterName}
                    noScroll={props.noScroll}
                />
            </OverAll>

            <ScrollArea
                speed={0.8}
                className="area"
                contentClassName=""
                horizontal={false}
                onScroll={(value) => {
                    if(value.topPosition!=undefined){
                        if(value.containerHeight >= value.realHeight) {
                            if(props.showScroll === true) props.setValue("showScroll",false);
                        } else {
                            if(value.topPosition > 10){
                                if(props.showScroll === true)props.setValue("showScroll",false);
                            } else {
                                if(props.showScroll === false) props.setValue("showScroll",true);
                            }
                        }
                    }
                }}
                style={{ width: '100%', height:700, opacity:1}}
                verticalScrollbarStyle={{backgroundColor:"#000", opacity:1}}
            >
                {props.children && props.children}
            </ScrollArea>
        </Layout>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValue };
export default connect( mapStateToProps, mapDispatchToProps )(LayoutF11);


const Layout = styled.div`
    position: relative;
    display: block;
    width: 1105px;
    height: 700px;
    max-width: 100vw;
    max-height: 100vh;
    overflow: hidden;
    background: no-repeat top center url(${props => props.background});
    font-family: "Kanit",tahoma;
`;
const OverAll = styled.div`
    z-index: 100;
    position: absolute;
    top: 0px;
    left: 0px;
    display: block;
    width: 100%;
    height: 100%;
    pointer-events: none;
`;

// background: no-repeat center url(${props => props.src}) #c5c0ad;
