import React,{useState} from 'react';
import { Link } from 'react-router-dom';
import iconMenu from './../static/images/icon_menu.png';

export const Menu = props => {
    const [showMenu, setShowMenu] = useState(false);
    return (
        <div className="left__menu">
            <div className={"bg-overlay " + (showMenu ? "show" : " ") }></div>
            <a onClick={()=>setShowMenu(!showMenu)} className="menu-icon"><img alt="icon" src={iconMenu} /></a>
            <ul className={"left__menu--list " + (showMenu ? "show" : " ") }>
                <li className="menu-close"><a onClick={()=>setShowMenu(!showMenu)}></a></li>
                <li><Link to={process.env.REACT_APP_EVENT_PATH} className={(props.page==="home"?"active":"")}>หน้าหลัก</Link></li>
                <li><Link to={process.env.REACT_APP_EVENT_PATH+"/history"}className={(props.page==="history"?"active":"")}>ประวัติ</Link></li>
            </ul>

        </div>
    )
}
