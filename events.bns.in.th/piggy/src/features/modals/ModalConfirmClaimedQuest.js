import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';
import {Imglist} from './../../constants/Import_Images';

const CPN = props => {
    let {modal_message,modal_item_token} = props;
    return (
        <ModalCore
            modalName="confirm_claim_quest"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <div className="contenttitle">ยืนยัน</div>
                <div className="contenttext" dangerouslySetInnerHTML={{__html: modal_message}} />

                <div>
                    <Btns onClick={()=>props.actConfirm()}/>
                    <BtnsCancel onClick={()=>props.setValues({modal_open:''})}/>
                </div>
            </ModalMessageContent>
  
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 543px;
    height: 341px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist['bg_modal']});
    box-sizing: border-box;
    padding: 8% 10%;
    font-family: "Kanit-Light",tahoma;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttitle{
        font-size: 21px;
    }
    .contenttext{
        color: #FFFFFF;
        font-size: 21px;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 180px;
        //height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;

const Btns = styled.div`
    background: no-repeat center top url(${Imglist['btn_ok']});
    background-size: 100%;
    width: 95px;
    height: 39px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #000;
    font-size: 20px;
    &:hover{
        background-position: bottom center;
    }
`
const BtnsCancel = styled.div`
    background: no-repeat center top url(${Imglist['btn_cancel']});
    background-size: 100%;
    width: 95px;
    height: 39px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #000;
    font-size: 20px;
    &:hover{
        background-position: bottom center;
    }
`