import React from 'react';
import {connect} from 'react-redux';

import ModalMessage from './ModalMessage';
import ModalLoading from './ModalLoading';
import ModalSelectCharacter from './ModalSelectCharacter';
import ModalConfirmSelectQuest from './ModalConfirmSelectQuest';
import ModalConfirmClaimedQuest from './ModalConfirmClaimedQuest';
import ModalConfirmClaimedBank from './ModalConfirmClaimedBank';

const Modals = (props)=> (
    <>
        <ModalMessage />
        <ModalLoading />
        <ModalSelectCharacter />
        <ModalConfirmSelectQuest />
        <ModalConfirmClaimedQuest />
        <ModalConfirmClaimedBank />
    </>
)

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = {  };

export default connect( mapStateToProps, mapDispatchToProps )(Modals);
