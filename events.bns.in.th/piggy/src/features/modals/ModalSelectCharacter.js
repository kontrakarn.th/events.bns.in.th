import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';
import {Imglist} from './../../constants/Import_Images';
import iconDropdown from '../../static/images/icon_dropdown.png'
import { apiPost } from './../../middlewares/Api';


const CPN = props => {

    const actSelectCharacter=(e)=>{
        e.preventDefault();
        let indexCharacter = props.value_select;
        if(indexCharacter !== "" ){
            props.apiSelectCharacter(indexCharacter)
        }else{
            props.setValues({
                modal_open:"message",
                modal_message: "โปรดเลือกตัวละคร",
                selected_char:false,
            });
        }
    }

    function handleChange(e) {
        props.setValues({
            value_select : e.target.value,
        })
    }

    return (
        <ModalCore
            modalName="selectcharacter"
            actClickOutside={false}
        >
            <ModalMessageContent>
                <div className="contenttitle">กรุณาเลือกตัวละครที่ต้องการเข้าร่วมกิจกรรม</div>
                <div className="contenttext">
                    <Form className="form">
                        <label className="form__label">
                            <select id="charater_form" className="form__select" onChange={(e) => handleChange(e)}>
                                <option value="" disabled selected>เลือกตัวละคร</option>
                                {props.characters && props.characters.map((item,index)=>{
                                    return (
                                        <option key={index} value={item.id}>{item.char_name}</option>
                                    )
                                })}
                            </select>
                            <img src={Imglist.icon_arrow} alt="" className="form__img"/>
                        </label>
                    </Form>
                </div>
                <div>
                    <Btns onClick={(e)=>actSelectCharacter(e)}/>
                    {/* <BtnsCancel onClick={()=>props.setValues({modal_open:''})}/> */}
                </div>
            </ModalMessageContent>
        </ModalCore>
    )
}


const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 543px;
    height: 341px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist['bg_modal']});
    box-sizing: border-box;
    padding: 8% 10%;
    font-family: "Kanit-Light",tahoma;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttitle{
        font-size: 21px;
    }
    .contenttext{
        color: #FFFFFF;
        font-size: 21px;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 180px;
        //height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;

const Btns = styled.div`
    background: no-repeat center top url(${Imglist['btn_ok']});
    background-size: 100%;
    width: 95px;
    height: 39px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #000;
    font-size: 20px;
    &:hover{
        background-position: bottom center;
    }
`
const BtnsCancel = styled.div`
    background: no-repeat center top url(${Imglist['btn_cancel']});
    background-size: 100%;
    width: 95px;
    height: 39px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #000;
    font-size: 20px;
    &:hover{
        background-position: bottom center;
    }
`
const Form = styled.div`
    .form{
        &__label{
            position: relative;
            color: #fff;
            background-color: #000;
            width: 370px;
            height: 34px;
            line-height: 34px;
            display: block;
            margin: 10px auto;
            padding: 5px;
            overflow: hidden;
        }
        &__select{
            font-size: 21px;
            border: 0px;
            background: transparent;
            vertical-align: middle;
            width: 110%;
            left: 0;
            position: absolute;
            z-index: 1;
            color: #fff;
            font-family: "Kanit-Light",tahoma;
        }
        &__img{
            position: absolute;
            display: block;
            top: 50%;
            width: 15px;
            height: 8px;
            right: 0px;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    }
`