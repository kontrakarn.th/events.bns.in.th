import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled,{ keyframes } from 'styled-components';
import { setValues, onAccountLogout } from './../../store/redux';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalSelectCharacter from './../../features/modals/ModalSelectCharacter';
import ModalConfirmSelectQuest from './../../features/modals/ModalConfirmSelectQuest';
import ModalConfirmClaimedQuest from './../../features/modals/ModalConfirmClaimedQuest';
import ModalConfirmClaimedBank from './../../features/modals/ModalConfirmClaimedBank';
import {Imglist} from './../../constants/Import_Images';

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div className={className} onClick={onClick}><img src={Imglist.btn_next}/></div>
    );
}
  
function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div className={className} onClick={onClick}><img src={Imglist.btn_prev}/></div>
    );
}

class Main extends React.Component {

    // =================

    apiEventInfo(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    claim_quest_id: 0,
                    modal_open: "",
                    end_round_time: data.data.end_round_time,
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters,
                    quests: data.data.quests,
                    max_daily_diamonds: data.data.max_daily_diamonds,
                    max_round_diamonds: data.data.max_round_diamonds,
                    max_round_lootboxes: data.data.max_round_lootboxes,
                    is_select_quests: data.data.is_select_quests,
                    selected_quests: data.data.selected_quests,
                    select_quests_id: data.data.select_quests_id,
                    completed_select_quest_count: data.data.completed_select_quest_count,
                    selected_quests_count: data.data.selected_quests_count,
                    can_claim_piggy_status: data.data.can_claim_piggy_status,
                    claimed_piggy_status: data.data.claimed_piggy_status,
                    bank_list: data.data.bank_list,
                });

                if (data.data.selected_char === false){
                    this.props.setValues({ modal_open: "selectcharacter" });
                }
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSelectCharacter(id){
        if (id === undefined || id === ""){
            return;
        }
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'select_character',id:id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    claim_quest_id: 0,
                    modal_open: "",
                    end_round_time: data.data.end_round_time,
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters,
                    quests: data.data.quests,
                    max_daily_diamonds: data.data.max_daily_diamonds,
                    max_round_diamonds: data.data.max_round_diamonds,
                    max_round_lootboxes: data.data.max_round_lootboxes,
                    is_select_quests: data.data.is_select_quests,
                    selected_quests: data.data.selected_quests,
                    select_quests_id: data.data.select_quests_id,
                    completed_select_quest_count: data.data.completed_select_quest_count,
                    selected_quests_count: data.data.selected_quests_count,
                    can_claim_piggy_status: data.data.can_claim_piggy_status,
                    claimed_piggy_status: data.data.claimed_piggy_status,
                    bank_list: data.data.bank_list,
                });
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSelectQuest(){
        if (this.props.select_quests_id.length <= 0 || this.props.select_diamonds != this.props.max_daily_diamonds){
            return;
        }
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'select_quests',selected_list:this.props.select_quests_id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    claim_quest_id: 0,
                    modal_open: "",
                    end_round_time: data.data.end_round_time,
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters,
                    quests: data.data.quests,
                    max_daily_diamonds: data.data.max_daily_diamonds,
                    max_round_diamonds: data.data.max_round_diamonds,
                    max_round_lootboxes: data.data.max_round_lootboxes,
                    is_select_quests: data.data.is_select_quests,
                    selected_quests: data.data.selected_quests,
                    select_quests_id: data.data.select_quests_id,
                    completed_select_quest_count: data.data.completed_select_quest_count,
                    selected_quests_count: data.data.selected_quests_count,
                    can_claim_piggy_status: data.data.can_claim_piggy_status,
                    claimed_piggy_status: data.data.claimed_piggy_status,
                    bank_list: data.data.bank_list,
                });
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_SELECT_QUEST", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiClaimQuest(){
        if(this.props.claim_quest_id == 0){
            return false;
        }
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'claim_daily_quest',quest_id:this.props.claim_quest_id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    claim_quest_id: 0,
                    modal_open: "",
                    end_round_time: data.data.end_round_time,
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters,
                    quests: data.data.quests,
                    max_daily_diamonds: data.data.max_daily_diamonds,
                    max_round_diamonds: data.data.max_round_diamonds,
                    max_round_lootboxes: data.data.max_round_lootboxes,
                    is_select_quests: data.data.is_select_quests,
                    selected_quests: data.data.selected_quests,
                    select_quests_id: data.data.select_quests_id,
                    completed_select_quest_count: data.data.completed_select_quest_count,
                    selected_quests_count: data.data.selected_quests_count,
                    can_claim_piggy_status: data.data.can_claim_piggy_status,
                    claimed_piggy_status: data.data.claimed_piggy_status,
                    bank_list: data.data.bank_list,
                });
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_CLAIM_QUEST", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiClaimBank(){
        if(this.props.can_claim_piggy_status == true && this.props.claimed_piggy_status == false){
            this.props.setValues({ modal_open: "loading" });
            let dataSend = {type:'claim_piggy_bank'};
            let successCallback = (data) => {
                if (data.status) {
                    this.props.setValues({
                        claim_quest_id: 0,
                        modal_open: "message",
                        modal_message: data.message,
                        end_round_time: data.data.end_round_time,
                        username: data.data.username,
                        character_name: data.data.character_name,
                        selected_char: data.data.selected_char,
                        characters: data.data.characters,
                        quests: data.data.quests,
                        max_daily_diamonds: data.data.max_daily_diamonds,
                        max_round_diamonds: data.data.max_round_diamonds,
                        max_round_lootboxes: data.data.max_round_lootboxes,
                        is_select_quests: data.data.is_select_quests,
                        selected_quests: data.data.selected_quests,
                        select_quests_id: data.data.select_quests_id,
                        completed_select_quest_count: data.data.completed_select_quest_count,
                        selected_quests_count: data.data.selected_quests_count,
                        can_claim_piggy_status: data.data.can_claim_piggy_status,
                        claimed_piggy_status: data.data.claimed_piggy_status,
                        bank_list: data.data.bank_list,
                    });
                }else{
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: data.message,
                    });
                }
            };
            const failCallback = (data) => {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            };
            apiPost("REACT_APP_API_POST_CLAIM_BANK", this.props.jwtToken, dataSend, successCallback, failCallback);
        }else{
            this.props.setValues({
                modal_open:"message",
                modal_message: "คุณได้ทุบกระปุกเสบียงน้อมหุรอบนี้ไปแล้ว",
            });
        }
    }

    // =================

    constructor(props) {
        super(props)
        this.state = {
            info: 0,
            timeLeft: "00:00:00:00",
            // select_quests_id: []
        }
    }


    handleChangeInfo(index){
        if(this.state.info===index){
            this.setState({
                info: 0
            })
        }
        else{
            this.setState({
                info: index
            })
        }
    }
    handleActConfirm(){
        // console.log('clicktoconfirm');
    }
    handleActConfirmSelectQuest(){
        this.apiSelectQuest()
    }

    handleActConfirmClaimQuest(){
        this.apiClaimQuest()
    }

    handleActConfirmClaimBank(){
        this.apiClaimBank()
    }

    handleChoose(questId,questDiamonds){
        if(this.props.is_select_quests == false &&  this.props.claimed_piggy_status == false){
            // console.log('questId:',questId)
            let indexOf = this.props.select_quests_id.indexOf(questId)
            let select_quests_id = [...this.props.select_quests_id]
            let select_diamonds = this.props.select_diamonds
            let cal_diamonds = select_diamonds
            // console.log("indexOf:",indexOf)
            if( indexOf < 0){
                select_quests_id.push(questId);
                cal_diamonds = select_diamonds + questDiamonds
                if(cal_diamonds > this.props.max_daily_diamonds){
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: "เควสที่เลือก<br />ต้องรวมมูลค่าไดมอนด์เท่ากับ "+this.numberWithCommas(this.props.max_daily_diamonds)+" เท่านั้น",
                    });
                }else{
                    this.props.setValues({
                        select_diamonds: cal_diamonds
                    })
                }
                
            }else{
                select_quests_id.splice(indexOf,1);
                cal_diamonds = select_diamonds - questDiamonds
                this.props.setValues({
                    select_diamonds: cal_diamonds
                })
            }

            if(cal_diamonds <= this.props.max_daily_diamonds){
                this.props.setValues({
                    select_quests_id
                })
            }
        }
        
    }

    onConfirmSelectQuest(){
        if(this.props.select_quests_id.length > 0 && this.props.select_diamonds == this.props.max_daily_diamonds){
            this.props.setValues({
                modal_open:"confirm_select_quest",
                modal_message: "ยืนยันรับภารกิจจากเควสที่เลือก?"
            })
        }
    }

    onConfirmClaimQuest(questId=0,diamonds="",lootboxed=0){
        if(questId > 0){
            this.props.setValues({
                claim_quest_id: questId,
                modal_open:"confirm_claim_quest",
                modal_message: "ยืนยันหยอดกระปุก<br />"+ diamonds + " ไดมอนด์ <br />และ " + lootboxed + " กล่องเสบียงน้องหมู?"
            })
        }
    }

    confirm_claim_bank(){
        if(this.props.can_claim_piggy_status == true && this.props.claimed_piggy_status == false){
            this.props.setValues({
                modal_open:"confirm_claim_bank",
                modal_message: "จะทุบเค้าเลยหรอ?<br />(อย่าลืมทำเสียงน่ารักตอนอ่านนะคะ)"
            })
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiEventInfo();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiEventInfo();
        }

    }

    componentDidMount(){
        setInterval(() => {
            this.setState({
                timeLeft : this.calculateTimeLeft(this.props.end_round_time)
            })
        }, 1000);
    }

    calculateTimeLeft(currDateTime="") {
        let timeLeft = {
            days:0,
            hours:0,
            minutes:0,
            seconds:0,
        };
        if(currDateTime != ""){
            const difference = +new Date(currDateTime) - +new Date();
        
            if (difference > 0) {
                timeLeft = {
                    days: Math.floor(difference / (1000 * 60 * 60 * 24)),
                    hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
                    minutes: Math.floor((difference / 1000 / 60) % 60),
                    seconds: Math.floor((difference / 1000) % 60)
                };
            }
        }
        
    
        return timeLeft.days+" วัน "+ timeLeft.hours + " ชม. " + timeLeft.minutes + " นาที " + timeLeft.seconds + " วินาที";
    };

    numberWithCommas(x=0) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    render() {
        const settings = {
            dots: true,
            infinite: false,
            fade: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        // console.log("Select Quest:", this.props.select_quests_id)
        return (
            <PageWrapper>
                <Content>
                    <Content_1>
                        <img src={Imglist.content_1}/>
                    </Content_1>
                    <Content_2 className="content_2">
                        <img src={Imglist.content_2}/>
                        <div className="content_2__head">ไดมอนด์จากภารกิจที่เลือกปัจจุบัน {this.numberWithCommas(this.props.select_diamonds)}/{this.numberWithCommas(this.props.max_daily_diamonds)} ไดมอนด์</div>
                        <div className="content_2__slide">
                            <Slider {...settings}>
                                <div>
                                    {this.props.quests && this.props.quests.length > 0 && this.props.quests.map((items,key)=>{
                                        if(key<=5){
                                            return(
                                                <div key={"quest_set_1"+key} className="content_2__item"> 
                                                    <div className="content_2__title-1">
                                                        {items.quest_title}
                                                    </div>
                                                    <div className="content_2__title-2">
                                                        {items.quest_dungeon}
                                                    </div>
                                                    <div className={"content_2__img"+(this.props.select_quests_id.indexOf(items.quest_id) >= 0?" choose":"")} onClick={()=>this.handleChoose(items.quest_id,items.diamonds)}>
                                                        <img src={items.image}/>
                                                        {this.props.select_quests_id.indexOf(items.quest_id) >= 0 && <img src={Imglist.frame} alt='' className="content_2__frame"/>}
                                                    </div>
                                                    <div className="content_2__detail">
                                                        ได้รับ {items.diamonds_text} ไดมอนด์
                                                    </div>
                                                    <div className="content_2__detail">
                                                        กล่องเสบียงน้องหมู x{items.lootboxed}
                                                    </div>
                                                </div>
                                            )
                                        }
                                    })}
                                </div>
                                <div>
                                    {this.props.quests && this.props.quests.length > 6 && this.props.quests.map((items,key)=>{
                                        if(key>=6 && key<=11){
                                            return(
                                                <div key={"quest_set_1"+key} className="content_2__item"> 
                                                    <div className="content_2__title-1">
                                                        {items.quest_title}
                                                    </div>
                                                    <div className="content_2__title-2">
                                                        {items.quest_dungeon}
                                                    </div>
                                                    <div className={"content_2__img"+(this.props.select_quests_id.indexOf(items.quest_id)>= 0?" choose":"")} onClick={()=>this.handleChoose(items.quest_id,items.diamonds)}>
                                                        <img src={items.image}/>
                                                        {this.props.select_quests_id.indexOf(items.quest_id)>= 0 && <img src={Imglist.frame} alt='' className="content_2__frame"/>}
                                                    </div>
                                                    <div className="content_2__detail">
                                                        ได้รับ {items.diamonds_text} ไดมอนด์
                                                    </div>
                                                    <div className="content_2__detail">
                                                        กล่องเสบียงน้องหมู x{items.lootboxed}
                                                    </div>
                                                </div>
                                            )
                                        }
                                    })}
                                </div>
                                <div>
                                    {this.props.quests && this.props.quests.length > 12 && this.props.quests.map((items,key)=>{
                                        if(key>=12){
                                            return(
                                                <div key={"quest_set_1"+key} className="content_2__item"> 
                                                    <div className="content_2__title-1">
                                                        {items.quest_title}
                                                    </div>
                                                    <div className="content_2__title-2">
                                                        {items.quest_dungeon}
                                                    </div>
                                                    <div className={"content_2__img"+(this.props.select_quests_id.indexOf(items.quest_id)>= 0?" choose":"")} onClick={()=>this.handleChoose(items.quest_id,items.diamonds)}>
                                                        <img src={items.image}/>
                                                        {this.props.select_quests_id.indexOf(items.quest_id)>= 0 && <img src={Imglist.frame} alt='' className="content_2__frame"/>}
                                                    </div>
                                                    <div className="content_2__detail">
                                                        ได้รับ {items.diamonds_text} ไดมอนด์
                                                    </div>
                                                    <div className="content_2__detail">
                                                        กล่องเสบียงน้องหมู x{items.lootboxed}
                                                    </div>
                                                </div>
                                            )
                                        }
                                    })}
                                </div>
                            </Slider>
                        </div>
                        {
                            this.props.claimed_piggy_status == false && this.props.is_select_quests == false && this.props.select_diamonds == this.props.max_daily_diamonds && <div className="content_2__btn" onClick={()=>this.onConfirmSelectQuest()} />
                        }
                        
                        {/* <div className="content_2__choosed">
                            เลือกภารกิจเรียบร้อยแล้ว
                        </div> */}
                    </Content_2>
                    <Content_3>
                        <img src={Imglist.content_3}/>
                        <div className="content_3__head">ภารกิจที่สำเร็จแล้ว {this.props.completed_select_quest_count || 0}/{this.props.selected_quests_count || 0} ภารกิจ</div>
                        <div className="content_3__slide">
                            {
                                this.props.selected_quests && this.props.selected_quests.length > 0 && <Slider {...settings}>
                                        {this.props.selected_quests && this.props.selected_quests.length > 0 && <div>
                                            {this.props.selected_quests.map((items,key)=>{
                                                if(key<=5){
                                                    return(
                                                        <div key={"quest_set_1"+key} className="content_3__item"> 
                                                            <div className="content_3__title-1">
                                                                {items.quest_title}
                                                            </div>
                                                            <div className="content_3__title-2">
                                                                {items.quest_dungeon}
                                                            </div>
                                                            <img src={items.image}/>
                                                            <div className="content_3__detail">
                                                                ได้รับ {items.diamonds_text} ไดมอนด์
                                                            </div>
                                                            <div className="content_3__detail">
                                                                กล่องเสบียงน้องหมู x{items.lootboxed}
                                                            </div>
                                                            {
                                                                items.status == 'success' && items.claim_status == 'pending' ?
                                                                    <div className="content_3__btn" onClick={()=>this.onConfirmClaimQuest(items.quest_id,items.diamonds_text,items.lootboxed)} >
                                                                        <div className=""/>
                                                                    </div>
                                                                :
                                                                    (
                                                                        items.status == 'success' && items.claim_status == 'claimed' ?
                                                                            <div className="content_3__btn">
                                                                                <div className="claimed"/>
                                                                            </div>
                                                                        :
                                                                            <div className="content_3__btn">
                                                                                <div className="close"/>
                                                                            </div>
                                                                    )
                                                            }
                                                        </div>
                                                    )
                                                }
                                            })}
                                        </div>
                                    }
                                    {
                                        this.props.selected_quests && this.props.selected_quests.length > 6 && <div>
                                            {this.props.selected_quests.map((items,key)=>{
                                                if(key>=6 && key<=11){
                                                    return(
                                                        <div key={"quest_set_1"+key} className="content_3__item"> 
                                                            <div className="content_3__title-1">
                                                                {items.quest_title}
                                                            </div>
                                                            <div className="content_3__title-2">
                                                                {items.quest_dungeon}
                                                            </div>
                                                            <img src={items.image}/>
                                                            <div className="content_3__detail">
                                                                ได้รับ {items.diamonds_text} ไดมอนด์
                                                            </div>
                                                            <div className="content_3__detail">
                                                                กล่องเสบียงน้องหมู x{items.lootboxed}
                                                            </div>
                                                            {
                                                                items.status == 'success' && items.claim_status == 'pending' ?
                                                                    <div className="content_3__btn" onClick={()=>this.onConfirmClaimQuest(items.quest_id,items.diamonds_text,items.lootboxed)} >
                                                                        <div className=""/>
                                                                    </div>
                                                                :
                                                                    (
                                                                        items.status == 'success' && items.claim_status == 'claimed' ?
                                                                            <div className="content_3__btn">
                                                                                <div className="claimed"/>
                                                                            </div>
                                                                        :
                                                                            <div className="content_3__btn">
                                                                                <div className="close"/>
                                                                            </div>
                                                                    )
                                                            }
                                                        </div>
                                                    )
                                                }
                                            })}
                                        </div>
                                    }
                                    
                                    {
                                        this.props.selected_quests && this.props.selected_quests.length > 12 && <div>
                                            {this.props.selected_quests.map((items,key)=>{
                                                if(key>=12){
                                                    return(
                                                        <div key={"quest_set_1"+key} className="content_3__item"> 
                                                            <div className="content_3__title-1">
                                                                {items.quest_title}
                                                            </div>
                                                            <div className="content_3__title-2">
                                                                {items.quest_dungeon}
                                                            </div>
                                                            <img src={items.image}/>
                                                            <div className="content_3__detail">
                                                                ได้รับ {items.diamonds_text} ไดมอนด์
                                                            </div>
                                                            <div className="content_3__detail">
                                                                กล่องเสบียงน้องหมู x{items.lootboxed}
                                                            </div>
                                                            {
                                                                items.status == 'success' && items.claim_status == 'pending' ?
                                                                    <div className="content_3__btn" onClick={()=>this.onConfirmClaimQuest(items.quest_id,items.diamonds_text,items.lootboxed)} >
                                                                        <div className=""/>
                                                                    </div>
                                                                :
                                                                    (
                                                                        items.status == 'success' && items.claim_status == 'claimed' ?
                                                                            <div className="content_3__btn">
                                                                                <div className="claimed"/>
                                                                            </div>
                                                                        :
                                                                            <div className="content_3__btn">
                                                                                <div className="close"/>
                                                                            </div>
                                                                    )
                                                            }
                                                        </div>
                                                    )
                                                }
                                            })}
                                        </div>
                                    }
                                </Slider>
                            }
                            
                        </div>
                    </Content_3>
                    <Content_4 className="content_4">
                        <img src={Imglist.content_4} alt=''/>
                        <div className="content_4__icon-group">
                            <div onClick={()=>this.handleChangeInfo(1)}>
                                <img src={Imglist.icon_info} alt=""/>
                                <div className="content_4__hover">
                                    <img src={Imglist.icon_hover} alt=""/>
                                </div>
                            </div>
                            <div onClick={()=>this.handleChangeInfo(2)}>
                                <img src={Imglist.icon_info} alt=""/>
                                <div className="content_4__hover">
                                    <img src={Imglist.icon_hover} alt=""/>
                                </div>
                            </div>
                        </div>
                        {this.state.info===1 &&
                            <img src={Imglist.info_1} alt="" className="content_4__info"/>
                        }
                        {this.state.info===2 &&
                            <img src={Imglist.info_2} alt="" className="content_4__info"/>
                        }
                        <div className={"content_4__btn "+(this.props.claimed_piggy_status == true || this.props.can_claim_piggy_status == false  ? "close" : "")} onClick={()=>this.confirm_claim_bank()} />
                        <div className="content_4__reset">
                            จะรีเซ็ตในอีก<br />{this.state.timeLeft}
                        </div>
                        <div className="content_4__scroll"
                        >
                            <div className="content_4__items">
                                <img src={Imglist.items_1} alt=''/>
                                <div className="content_4__detail">
                                    <div className="content_4__detail-text">ไดมอนด์ที่สะสมปัจจุบัน</div>
                                    <div className="content_4__progress">
                                        <span style={{width:(this.props.bank_list && this.props.bank_list.diamonds > 0 ? (this.props.bank_list.diamonds / this.props.max_round_diamonds) * 100 : 0)+"%"}} />
                                        <div className="content_4__text-over">
                                            {this.props.bank_list && this.props.bank_list.diamonds > 0 ? this.props.bank_list.diamonds_text : 0}/{this.numberWithCommas(this.props.max_round_diamonds)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="content_4__items">
                                <img src={Imglist.items_2} alt=''/>
                                <div className="content_4__detail">
                                    <div className="content_4__detail-text">กล่องเสบียงน้องหมูที่สะสมปัจจุบัน</div>
                                    <div className="content_4__progress">
                                        <span style={{width:(this.props.bank_list && this.props.bank_list.lootboxes > 0 ? (this.props.bank_list.lootboxes / this.props.max_round_lootboxes) * 100 : 0)+"%"}}/>
                                        <div className="content_4__text-over">
                                            {this.props.bank_list && this.props.bank_list.lootboxes > 0 ? this.props.bank_list.lootboxes : 0}/{this.props.max_round_lootboxes}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="content_4__head">
                                รายการไอเท็มในกล่องเสบียงน้องหมู
                            </div>
                            {
                                    this.props.bank_list &&  this.props.bank_list.materials && this.props.bank_list.materials.length > 0 ? this.props.bank_list.materials.map((items,key)=>{

                                        return(
                                            <div className="content_4__items" key={"materials_"+key}>
                                                <img src={items.image} alt={items.title+"ที่สะสมปัจจุบัน"}/>
                                                <div className="content_4__detail">
                                                    <div className="content_4__detail-text">{items.title}ที่สะสมปัจจุบัน</div>
                                                    <div className="content_4__progress">
                                                        <span style={{width:((items.quantity / items.limit) * 100 || 0)+"%"}} />
                                                        <div className="content_4__text-over">
                                                            {items.quantity}/{items.limit}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )

                                    })
                                :
                                    null
                            }
                            
                            
                        </div>
                    </Content_4>
              
                </Content>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm
                    actConfirm={()=>this.handleActConfirm()}
                />
                <ModalConfirmClaimedBank 
                    actConfirm={()=>this.handleActConfirmClaimBank()}
                />
                <ModalConfirmClaimedQuest
                    actConfirm={()=>this.handleActConfirmClaimQuest()}
                />
                <ModalConfirmSelectQuest 
                    actConfirm={()=>this.handleActConfirmSelectQuest()}
                />
                <ModalSelectCharacter apiSelectCharacter={this.apiSelectCharacter.bind(this)} />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const blink = keyframes`
    0%    {opacity: 0}
    50%    {opacity: 1}
    100%    {opacity: 0}
`;

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #11122d;
    width: 1105px;
    padding-top: 683px;
    text-align: center;
`
const Content = styled.div `  
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }
`

const Content_1 = styled.div `  
    width: 80%
    margin: 0 auto;
    >img{
        width: 100%;
        display: block;
    }
`

const Content_2 = styled.div `  
    position: relative;
    width: 80%
    margin: 9% auto 0;
    font-family: "Kanit-Light",tahoma;
    >img{
        width: 100%;
        display: block;
    }
    .content_2{
        &__head{
            position: absolute;
            top: 21.5%;
            left: 50%;
            transform: translate(-50%, 0);
            color: #fff;
            font-size: 14px;
        }
        &__slide{
            position: absolute;
            top: 31%;
            left: 50%;
            transform: translate(-50%, 0);
            width: 75%;
            margin: 0 auto;
            text-align: left;
        }
        &__item{
            font-family: "Kanit-Regular",tahoma;
            display: inline-block;
            width: calc(94% / 3);
            margin: 3% 1% 0;
            text-align: center;
            cursor: pointer;
            // >img{
            //     width: 60%;
            //     margin: 2% auto 0;
            // }
        }
        &__img{
            position: relative;
            width: 60%;
            margin: 2% auto 0;
            z-index: 10;
            >img{
                width: 100%;
                display:block;
            }
            &.choose{
                >img{
                    &:first-child{
                        filter: grayscale(1);
                    }
                }
            }
        }
        &__frame{
            position: absolute;
            width: 100%;
            top: 0;
            left: 0;
        }
        &__title-1{
            color: #90e36c
            font-size: 11px;
            white-space: nowrap;
        }
        &__title-2{
            color: #c0c0c0;
            font-size: 10px;
        }
        &__detail{
            color: #ffffff;
            font-size: 10px;
        }
        &__btn{
            position: absolute;
            bottom: 8%;
            left: 50%;
            transform: translate(-50%, 0);
            background: url(${Imglist.btn_confirm}) no-repeat center top;
            background-size: 100%;
            width 17%;
            padding-top: calc(17% * (69 / 165 ));
            cursor: pointer;
            &:hover{
                background-position: bottom center;
            }
        }
        &__choosed{
            position: absolute;
            bottom: 11%;
            left: 50%;
            transform: translate(-50%, 0);
            font-family: "Kanit-SemiBold",tahoma;
            color: #fff;
            font-size: 20px;
        }
    }
    & .slick-dots{
        bottom: -40px;
    }
    & .slick-dots li button:before{
        font-size: 10px;
        opacity: 1;
        color: #b86f6f;
    }
    & .slick-dots li.slick-active button:before{
        color: #d29494;
    }
    & .slick-prev,
    & .slick-next{
        width: auto;
        height: auto;
        z-index: 1;
        top: 55%;
        &::before{
            font-size:0;
        }
    }
    & .slick-prev{
        left: -25px;
    }
    & .slick-next{
        right: -25px;
    }
    & .slick-disabled{
        display: none;
    }
`

const Content_3 = styled.div `  
    position: relative;
    width: 80%
    margin: 9% auto 0;
    font-family: "Kanit-Light",tahoma;
    >img{
        width: 100%;
        display: block;
    }
    .content_3{
        &__head{
            position: absolute;
            top: 21.5%;
            left: 50%;
            transform: translate(-50%, 0);
            color: #fff;
            font-size: 14px;
        }
        &__slide{
            position: absolute;
            top: 31%;
            left: 50%;
            transform: translate(-50%, 0);
            width: 75%;
            margin: 0 auto;
            text-align: left;
        }
        &__item{
            font-family: "Kanit-Regular",tahoma;
            display: inline-block;
            width: calc(94% / 3);
            margin: 3% 1% 0;
            text-align: center;
            >img{
                width: 60%;
                margin: 2% auto 0;
            }
        }
        &__title-1{
            color: #90e36c
            font-size: 11px;
            white-space: nowrap;
        }
        &__title-2{
            color: #c0c0c0;
            font-size: 10px;
        }
        &__detail{
            color: #ffffff;
            font-size: 10px;
        }
        &__btn{
            position: relative;
            width 87px;
            height: 31px;
            margin: 5% auto 0;
            >div{
                position: absolute;
                width: 100%;
                height: 100%;
                top: 0;
                background: url(${Imglist.btn_drop}) no-repeat top center;
                background-size: 100% 200%;
                z-index: 2;
                cursor: pointer;
                &:hover{
                    background-position: bottom center;
                }
                &.close{
                    filter: grayscale(.9);
                    pointer-events: none;
                    cursor: default;
                }
                &.claimed{
                    background: url(${Imglist.btn_droped}) no-repeat top center;
                    filter: grayscale(.9);
                    pointer-events: none;
                    cursor: default;
                }
            }
        }
    }

    & .slick-dots{
        bottom: -40px;
    }
    & .slick-dots li button:before{
        font-size: 10px;
        opacity: 1;
        color: #b86f6f;
    }
    & .slick-dots li.slick-active button:before{
        color: #d29494;
    }
    & .slick-prev,
    & .slick-next{
        width: auto;
        height: auto;
        z-index: 1;
        top: 55%;
        &::before{
            font-size:0;
        }
    }
    & .slick-prev{
        left: -25px;
    }
    & .slick-next{
        right: -25px;
    }
    & .slick-disabled{
        display: none;
    }
`
const Content_4 = styled.div `  
    position: relative;
    width: 95%
    margin: 9% auto 0;
    font-family: "Kanit-Light",tahoma;
    >img{
        width: 100%;
        display: block;
    }
    .content_4{
        &__icon-group{
            position: absolute;
            top: 27%;
            left: 11%;
            width: 3%;
            >div{
                position: relative;
                display: block;
                width: 100%;
                margin: 20% 0;
                cursor: pointer;
                >img{
                    display: block;
                    width: 100%;
                    cursor: pointer;
                }
            }
        }
        &__hover{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 128%;
            animation: ${blink} 2s 0s infinite linear;
            opacity: 0;
            >img{
                width: 100%;
                display: block;
            }
        }
        &__info{
            position: absolute;
            top: 19%;
            width: 27%;
            left: 15%;
        }
        &__btn{
            position: absolute;
            background: url(${Imglist.btn_smash}) no-repeat center top;
            background-size: 100% 200%;
            width: 175px;
            height: 71px;
            cursor: pointer;
            bottom: 14%;
            left: 20.5%;
            &:hover{
                background-position: bottom center;
            }
            &.close{
                filter: grayscale(.9);
                pointer-events: none;
                cursor: default;
            }
        }
        &__reset{
            position: absolute;
            bottom: 7%;
            left: 16.5%;
            color: #fff;
            text-shadow: 0px 0px 5px rgba(255,122,129,0.71);
            font-family: "Kanit-Regular",tahoma;
            font-size: 20px;
        }
        &__scroll{
            position: absolute;
            top: 25%;
            right: 14%;
            width: 38%;
            overflow-y: auto !important;
            height: 550px;
            padding: 2%;
            &:hover{
                overflow-y: auto !important;
            }
            &::-webkit-scrollbar {
                width: 12px;
                background: #4d2e2e;
            }
            &::-webkit-scrollbar-thumb {
                background: #6e5454;
            }
        }
        &__items{
            font-family: "Kanit-Regular",tahoma;
            display: flex;
            align-items: center;
            justify-content: space-between;
            background: #100404;
            padding: 3% 5%;
            margin-bottom: 1.5%;
            >img{
                width: 25%;
                display: block;
            }
        }
        &__detail{
            width: 70%;
        }
        &__detail-text{
            text-shadow: 0px 0px 5px rgba(255,122,129,0.71);
            color: #fff;
            font-size: 12px;
        }
        &__progress{
            position: relative;
            width: 100%;
            height: 20px;
            border: 1px solid #fff;
            margin-top: 5%;
            >span{
                position: absolute;
                width: 0%;
                max-width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                background: #ffe153;
            }
        }
        &__text-over{
            position: absolute;
            top: 50%;
            left: 50%;
            color: #fff;
            font-size: 12px;
            transform: translate(-50%, -50%);
        }
        &__head{
            text-shadow: 0px 0px 5px rgba(255,122,129,0.71);
            color: #fff;
            font-size: 12px;
            margin: 5% 0;
        }
    }
`

