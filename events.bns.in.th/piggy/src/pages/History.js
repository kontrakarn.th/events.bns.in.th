import React from 'react';
import F11Layout from './../features/F11Layout/';
import HistoryFeatures from './../features/history';

export class History extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={false}
                page="history"
            >
                <HistoryFeatures/>
            </F11Layout>
        )
    }
}
