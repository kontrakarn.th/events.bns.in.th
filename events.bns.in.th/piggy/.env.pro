REACT_APP_EVENT_PATH=/piggy
REACT_APP_STATIC_PATH=/static/
REACT_APP_LOCALE=th-TH
REACT_APP_CSRF_COOKIE_NAME=csrf_token
REACT_APP_COOKIE_DOMAIN=.bns.in.th

# API related
REACT_APP_API_SERVER_HOST=https://events.bns.in.th
REACT_APP_API_GET_OAUTH_INFO=/oauth/get_oauth

# API FOR EVENT
REACT_APP_API_POST_EVENT_INFO=/api/piggy/event_info
REACT_APP_API_POST_SELECT_CHAR=/api/piggy/select_char
REACT_APP_API_POST_SELECT_QUEST=/api/piggy/select_daily_quest
REACT_APP_API_POST_CLAIM_QUEST=/api/piggy/claim_daily_quest
REACT_APP_API_POST_CLAIM_BANK=/api/piggy/claim_piggy_bank
REACT_APP_API_POST_HISTORY=/api/piggy/history

# SSO related
#REACT_APP_SSO_APP_ID=10046
#REACT_APP_SSO_URL=https://sso.garena.com
#REACT_APP_SSO_PARAM_NAME=session_key
#REACT_APP_SSO_COOKIE_NAME=sso_session

# OAUTH related
REACT_APP_OAUTH_APP_ID=10007
REACT_APP_OAUTH_APP_NAME=bns_piggy
REACT_APP_OAUTH_PLATFORM=1
REACT_APP_OAUTH_URL=https://auth.garena.com
REACT_APP_OAUTH_PARAM_NAME=token
REACT_APP_OAUTH_COOKIE_NAME=oauth_session

# JWT
REACT_APP_JWT_SECRET=fknh3XCFwZvC8RyQurQjWhQT5GXreas9

# Landing Page
REACT_APP_LANDING_SERVER_HOST=https://landing.garena.in.th
REACT_APP_LANDING_JSON=/static/data/get_landing.json
