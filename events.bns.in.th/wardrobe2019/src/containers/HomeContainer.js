import React, { Component } from 'react';
import {connect} from 'react-redux';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import {apiPost} from './../middlewares/Api';

// import ModalConfirmToRedeem from '../components/ModalConfirmToRedeem';

import ModalMessage from '../components/ModalMessage';
import ModalItemCode from '../components/ModalItemCode';
import ModalConfirmPurchase from '../components/ModalConfirmPurchase';
// import ModalConfirmGacha from '../components/ModalConfirmGacha';
// import ModalReceiveGacha from '../components/ModalReceiveGacha';
// import ModalConfirmExchange from '../components/ModalConfirmExchange';
// import ModalReceiveExchange from '../components/ModalReceiveExchange';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
import items1 from './../static/images/items1.png'
import items2 from './../static/images/items2.png'
import imgWoman from './../static/images/charactor.png'

import {
    onAccountLogout
} from '../actions/AccountActions';

class HomeContainer extends Component {
   
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiEventInfo(){
        this.setState({ showLoading: true});
        let self = this;
        let dataSend={ type : "event_info" };
        let successCallback = (data)=>{
            // console.log("data",data);
            this.setState({
                permission: true,
                showLoading: false,
                ...data.content
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                })
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO",this.props.jwtToken,dataSend,successCallback,failCallback);
    }

    apiPurchasePackage(){
        this.setState({ showLoading: true, ModalConfirmPurchase: false});
        let self = this;
        let dataSend={ type : "purchase_package", package_id: this.state.packageId };
        let successCallback = (data)=>{
            // console.log("data",data);
            this.setState({
                permission: true,
                showLoading: false,
                showModalMessage: data.message,
                ...data.content
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                })
            }
        };
        apiPost("REACT_APP_API_POST_PURCHASE",this.props.jwtToken,dataSend,successCallback,failCallback);
    }
    
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,

            username: "",
            has_wardrobe: false,
            package_1: {},
            package_2: {},

            packageId: 0,
            packageName: "",

            showModalMessage: "",
            showModalItemCode: false,
            ModalConfirmPurchase: false,

            scollDown: false,
        }
    }
    componentDidMount(){
        if(this.props.jwtToken !== "") setTimeout(()=>this.apiEventInfo(),600);
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.jwtToken !== prevProps.jwtToken){
            setTimeout(()=>{
                this.apiEventInfo()
            },600);
        }
    }


    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }

    actRedeem(e,package_id,package_name){
        e.preventDefault();
        this.setState({
            ModalConfirmPurchase: true,
            packageId: package_id,
            packageName: package_name,
        })
    }

    actConfirmPurchase(){
        this.apiPurchasePackage();
    }

    render() {
        return (
            <div >
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.state.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="preorder">
                                <section className="preorder__section preorder__section--header">
                                    <div className="header">
                                        <div className="header__inner">
                                        </div>
                                        <div className="header__scroll"><img src={iconScroll} alt="" /></div>
                                    </div>
                                </section>
                                <section className="preorder__section">
                                    <div className="package">
                                        <div className="package__inner">
                                           <img className="package__imgWoman" src={imgWoman} alt="" />
                                           <div className="package__col">
                                                <h5>สำหรับไอดีที่ยังไม่มีตู้เสื้อผ้า</h5>
                                                <img src={items1}/>
                                                <div className="package__col--btn text-center">
                                                    {
                                                        this.state.has_wardrobe == true ? 
                                                            <a className="btns__normal received"><div>มีตู้เสื้อผ้าแล้ว</div></a>
                                                        :
                                                            (
                                                                this.state.package_1 && this.state.package_1.already_purchase == true ?
                                                                    <a className="btns__normal received"><div>ซื้อไปแล้ว</div></a>
                                                                :
                                                                    (
                                                                        this.state.package_1 && this.state.package_1.can_purchase == true ?
                                                                            <a className="btns__normal" onClick={(event,package_id,package_name)=>this.actRedeem(event,1,"แพ็คเกจ 90,000 ไดมอนด์")}>
                                                                                <div>ซื้อ</div>
                                                                            </a>
                                                                        :
                                                                            <a className="btns__normal received"><div>ไม่ตรงเงื่อนไข</div></a>
                                                                    )
                                                            )
                                                        
                                                    }
                                                </div>
                                           </div>
                                           <div className="package__col">
                                                <h5>สำหรับไอดีที่มีตู้เสื้อผ้าแล้ว</h5>
                                                <img src={items2}/>
                                                <div className="package__col--btn text-center">
                                                    {
                                                        this.state.package_2 && this.state.package_2.already_purchase == true ?
                                                            <a className="btns__normal received"><div>{"ซื้อครบ "+this.state.package_2.purchase_count+"/"+this.state.package_2.purchase_limit}</div></a>
                                                        :
                                                            (
                                                                this.state.package_2 && this.state.package_2.can_purchase == true ?
                                                                    <a className="btns__normal" onClick={(event,package_id,package_name)=>this.actRedeem(event,2,"แพ็คเกจ 40,000 ไดมอนด์")}>
                                                                        <div>{"ซื้อ "+ this.state.package_2.purchase_count +"/"+this.state.package_2.purchase_limit}</div>
                                                                    </a>
                                                                :
                                                                    <a className="btns__normal received"><div>ไม่ตรงเงื่อนไข</div></a>
                                                            )
                                                    }
                                                </div>
                                           </div>
                                          
                                        </div>
                                    </div>
                                </section>
                                
                            </div>
                        </ScrollArea>
                    </div>
                    :
                    <Permission />
                }


                <Loading
                    open={this.state.showLoading}
                />
                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
                <ModalConfirmPurchase
                    open={this.state.ModalConfirmPurchase}
                    actConfirm={()=>this.actConfirmPurchase()}
                    actClose={()=>this.setState({ModalConfirmPurchase: false})}
                    msg={"ยืนยันซื้อ &quot;"+this.state.packageName+"&quot;"}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
    onAccountLogout: () => {
        dispatch(onAccountLogout());
    }
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
