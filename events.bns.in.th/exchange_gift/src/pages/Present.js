import React from 'react';

import F11Layout from './../features/F11Layout/';
import Present from './../features/present';

export class PresentPage extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
            >
                <Present/>
            </F11Layout>
        )
    }
}
