// import bg_2 from '../static/images/bg_2.jpg';
import ico_scroll from '../static/images/ico_scroll.png';
import icon_dropdown from '../static/images/icon_dropdown.png';
import icon_menu from '../static/images/icon_menu.png';

import bg_2 from '../static/images/bg_2.jpg';
import bg_history from '../static/images/bg_history.jpg';
import bg_main_1 from '../static/images/bg_main_1.jpg';
import bg_main_2 from '../static/images/bg_main_2.jpg';
import btn_exchange from '../static/images/btn_exchange.png';
import btn_prizewrap from '../static/images/btn_prizewrap.png';
import content_hisotry from '../static/images/content_hisotry.png';
import content_info_3 from '../static/images/content_info_3.png';
import content_main_1 from '../static/images/content_main_1.png';
import content_main_2 from '../static/images/content_main_2.png';
import char_info_2 from '../static/images/char_info_2.png';
import content_info_1 from '../static/images/content_info_1.png';
import contetn_info_2 from '../static/images/contetn_info_2.png';
import bg_modal from '../static/images/bg_modal.png';
import btn_cancel from '../static/images/btn_cancel.png';
import btn_confirm from '../static/images/btn_confirm.png';

export const Imglist = {
	ico_scroll,
	icon_dropdown,
	icon_menu,
	bg_2,
	bg_history,
	bg_main_1,
	bg_main_2,
	btn_exchange,
	btn_prizewrap,
	content_hisotry,
	content_info_3,
	content_main_1,
	content_main_2,
	char_info_2,
	content_info_1,
	contetn_info_2,
	bg_modal,
	btn_cancel,
	btn_confirm,
}
