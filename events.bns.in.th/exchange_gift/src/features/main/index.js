import React from 'react';
import {Link,withRouter} from 'react-router-dom';

import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
import Logo from './BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import {Imglist} from './../../constants/Import_Images';
import ModalSelectCharacter from "../modals/ModalSelectCharacter";

class Main extends React.Component {

    actConfirm(token){
        if (token === undefined || token === ""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไม่สามารถห่อของขวัญได้",
            });
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'set_gift',token:token,gift:this.props.itemList};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                    uid: data.data.uid,
                    username: data.data.nickname,
                    character: data.data.character_name,
                    gifted: data.data.gifted,
                    exchanged: data.data.exchanged,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters||[],
                    item_list: data.data.item_list||[],
                    current_page: data.data.current_page,
                    price:0,
                    itemList:Array(60).fill(0),
                });

                if(data.data.gifted==true){
                    setTimeout(()=>{
                        this.apiWhoGetGM();
                        this.apiHighPrice();
                    }, 1000);
                    this.props.history.push(process.env.REACT_APP_EVENT_PATH+'/present');
                }

            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_SET_GIFT", this.props.jwtToken, dataSend, successCallback, failCallback);

    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    modal_open: "",
                    uid: data.data.uid,
                    username: data.data.nickname,
                    character: data.data.character_name,
                    gifted: data.data.gifted,
                    exchanged: data.data.exchanged,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters||[],
                    item_list: data.data.item_list||[],
                    current_page: data.data.current_page,
                });

                if (data.data.selected_char === false){
                    this.props.setValues({ modal_open: "selectcharacter" });
                }
                if(data.data.gifted==true){
                    setTimeout(()=>{
                        this.apiWhoGetGM();
                        this.apiHighPrice();
                    }, 1000);
                    this.props.history.push(process.env.REACT_APP_EVENT_PATH+'/present');
                }
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiWhoGetGM(){
        // this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'who_get_gm'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    gift_list:data.data
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_WHO_GET_GM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiHighPrice(){
        // this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'high_price'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    ranking_list:data.data
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_HIGH_PRICE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSelectCharacter(id=""){
        if (id === undefined || id === ""){
            return;
        }
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'select_character',id:id};
        let successCallback = (data) => {
            if (data.status) {

                this.props.setValues({
                    modal_open: "message",
                    modal_message:data.message,
                    uid: data.data.uid,
                    username: data.data.nickname,
                    character: data.data.character_name,
                    gifted: data.data.gifted,
                    exchanged: data.data.exchanged,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters||[],
                    item_list: data.data.item_list||[],
                    current_page: data.data.current_page,
                });
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiRedeem(type_id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type_id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content.response,
                    modal_open:"message",
                    modal_message: "ส่งของรางวัลสำเร็จ<br/>กรุณาตรวจสอบจดหมายภายในเกม"
                });

            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,
            positionbox: {
                20: 197,
                25: 198,
                30: 199
            }
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    //
    fncCalculus(ary){
        let full_price = 0;
        for(let i=0;i<ary.length;i++ ){
            full_price += parseInt(ary[i])*parseInt(this.props.item_list[i].price);
        }
        return full_price
    }


    onEnterAmount(e,key){
        var numbers = /^[-+]?[0-9]+$/;
        let ary = [...this.props.itemList];
         if(e.target.value.match(numbers))
         {
             let num_add=parseInt(e.target.value)
             if(num_add>10){
                 num_add=10
                 document.getElementById("box_item_"+key).value=10
             }

             ary[key] = num_add;
         }else{
              document.getElementById("box_item_"+key).value=""
              ary[key] = 0;
         }
         let price = this.fncCalculus(ary);
         this.props.setValues({itemList: ary,price:price})
    }


    render() {

        let itemList = this.props.itemList || [];
        return (
            <PageWrapper>
                <Logo/>
                <Content>
                        <DetailFrame src={Imglist['content_main_1']} alt='detail'/>
                        <ScoreFrame>
                                <div className="wrapper">
                                    {itemList.map((item,key) => {
                                        let pos = 196;
                                        pos = (key>19) ? 197 : pos;
                                        pos = (key>24) ? 198 : pos;
                                        pos = (key>29) ? 198 : pos;
                                        let offset = 0;
                                        offset = (key>14 && key < 20) ? 1 : offset;
                                        offset = (key>24 && key < 30) ? -2 : offset;
                                        offset = (key>39) ? 2 : offset;
                                        offset = (key>44)? 3: offset;
                                        offset = (key>49)? 5: offset;
                                        return (
                                            <Box key={'box_'+(key+1)} pos={offset + Math.floor(key/5) * pos} start={295} type='text' id={"box_item_"+key} name={"box_item_"+key} onChange={this.props.gifted==false ? (e)=>this.onEnterAmount(e,key) : null}>
                                                {item.completed_count}
                                            </Box>
                                        )
                                    })}
                                </div>
                                <FooterBox>
                                    ราคารวม {new Intl.NumberFormat().format(this.props.price)} ไดมอนด์
                                    <Btns status={'met'} active={(this.props.gifted==true || this.props.modal_open=="loading" || this.props.price<5000 )==true ? false : true } onClick={()=>this.props.setValues({modal_open: 'confirm',modal_message: 'คุณยืนยันที่จะห่อของขวัญ'})}/>
                                </FooterBox>
                        </ScoreFrame>
                </Content>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                <ModalSelectCharacter actSelectCharacter={(e)=>this.apiSelectCharacter(e)}/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(withRouter(Main));

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_main_1']}) ,bottom center no-repeat url(${Imglist['bg_main_2']});
    width: 1105px;
    padding-top: 716px;
    padding-bottom: 81px;
    text-align: center;
    position: relative;
    z-index: 20;
`

const Content = styled.div `
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }

`
const DetailFrame = styled.img`
    display: block;
    margin: 0 auto 50px;
`

const ScoreFrame = styled.div`
    background: top center no-repeat url(${Imglist['content_main_2']});
    width: 1105px;
    height: 2855px;
    display: block;
    margin: 0 auto;
    position: relative;
    z-index: 25;
    .wrapper{
        width: 700px;
        margin: 0 auto;
        height: 1360px;
        padding-top: 174px;
        position: relative;
    }
    .wrapper2{
        width: 700px;
        margin: 0 auto;
        height: 1360px;
        position: relative;
    }
`

const Box = styled.input`
        width: 55px;
        height: 30px;
        margin: 0 auto;
        position: absolute;
        left: 45px;
        top: ${props=>props.start}px;
        color: #ffffff;
        line-height: 33px;
        font-family: 'Kanit-Light';
        text-align: center;
        background: rgba(0,0,0,0);
        border: 0;
        &:focus{
            outline: none;
        }
        &:nth-child(2){
            left: 184px;
        }
        &:nth-child(3){
            left: 323px;
        }
        &:nth-child(4){
            left: 461px;
        }
        &:nth-child(5){
            left: 600px;
        }
        &:nth-child(5n+6){
            top: calc(${props=>props.start}px + ${props=>props.pos}px);
            left: 45px;
        }
        &:nth-child(5n+7){
             top: calc(${props=>props.start}px + ${props=>props.pos}px);
             left: 184px;
        }
        &:nth-child(5n+8){
             top: calc(${props=>props.start}px + ${props=>props.pos}px);
             left: 323px;
        }
        &:nth-child(5n+9){
             top: calc(${props=>props.start}px + ${props=>props.pos}px);
             left: 461px;
        }
        &:nth-child(5n+10){
             top: calc(${props=>props.start}px + ${props=>props.pos}px);
             left: 600px;
        }
`

const FooterBox = styled.div`
    position: absolute;
    width: 60%;
    bottom: 110px;
    text-align: center;
    color: #ffffff;
    left: 0;
    right: 0;
    margin: 0 auto;
    font-family: 'Kanit-Light';

`
const Btns = styled.div`
    display: block;
    margin: 30px auto 0;
    bottom: 110px;
    cursor: pointer;
    background:center no-repeat url(${Imglist['btn_prizewrap']});
    width: 148px;
    height: 46px;
    background-size: 100% 300%;
    ${props=>props.status === 'notmet' && 'pointer-events: none;'}
    ${props=>props.active === false ? '-webkit-filter: grayscale(100%); filter: grayscale(100%); cursor: not-allowed;pointer-events: none;': ''}
    &:hover{
        background-position: bottom center;
    }
`
