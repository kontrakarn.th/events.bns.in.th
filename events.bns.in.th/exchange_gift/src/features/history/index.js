import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
import Logo from '../main/BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import {Imglist} from './../../constants/Import_Images';

class History extends React.Component {

 apiGetHistory(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'my_history_pack'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open: '',
                    history_list: data.data
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_MY_HISTORY_PACK", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    componentDidMount() {
        if(this.props.jwtToken !== "" && this.props.history_list && this.props.history_list.length==0){
            this.apiGetHistory()
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== "" && this.props.username==""){
            this.apiGetHistory()
        }

    }

    render() {
        return (
            <PageWrapper>
                <Logo/>
                <Menu page='history'/>
                <HistroyFrame>
                        <ul className="list">
                                    {
                                        this.props.history_list.map((item,key) => {
                                            return(
                                                <li key={`history_${key+1}`}>
                                                    <div>
                                                            ไอเทม {item.product_title} จำนวน {item.product_quantity} ชิ้น
                                                    </div>
                                                    <div>
                                                            ราคา {item.price} ไดมอนด์
                                                    </div>
                                                </li>
                                            )})
                                    }
                        </ul>
                </HistroyFrame>
                <ModalLoading />
                <ModalMessage />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(History);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_history']});
    width: 1105px;
    height: 700px;
    // padding-top: 153px;
    text-align: center;
    position: relative;
    z-index: 20;
    overflow: hidden;
`
const HistroyFrame = styled.div`
    background: top center no-repeat url(${Imglist['content_hisotry']});
    width: 902px;
    height: 548px;
    box-sizing: border-box;
    margin: 110px auto 0;
    padding: 90px 96px 0;
    .list{
        width: 700x;
        height: 433px;
        margin: 0 auto;
        padding: 0;
        overflow: auto;
        overflow-x: hidden;
        color: #ffffff;
        >li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 0 2%;
            box-sizing: border-box;
            >div:first-child{
                width: 50%;
                text-align: left;
            }
        }
        &::-webkit-scrollbar {
            width: 8px;
        }
        &::-webkit-scrollbar-track {
          background: rgba(173,173,173,0.05);
        }
        &::-webkit-scrollbar-thumb {
            background: #000000;
        }
    }
`
