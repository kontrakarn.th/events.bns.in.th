import React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

import styled from 'styled-components';
import Logo from '../main/BNSLogo';
import { apiPost } from './../../middlewares/Api';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import {Imglist} from './../../constants/Import_Images';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';

class Present extends React.Component {

    numberWithCommas=(x)=> {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    modal_open: "",
                    uid: data.data.uid,
                    username: data.data.nickname,
                    character: data.data.character_name,
                    gifted: data.data.gifted,
                    exchanged: data.data.exchanged,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters||[],
                    item_list: data.data.item_list||[],
                    current_page: data.data.current_page,
                });
                if(data.data.gifted==false){
                    this.props.history.push(process.env.REACT_APP_EVENT_PATH+"/");
                }
                if (data.data.selected_char === false){
                    this.props.setValues({ modal_open: "selectcharacter" });
                }

            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiExchangeGift(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'exchange_gift'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    modal_open: "message",
                    modal_message: data.message,
                    uid: data.data.uid,
                    username: data.data.nickname,
                    character: data.data.character_name,
                    gifted: data.data.gifted,
                    exchanged: data.data.exchanged,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters||[],
                    item_list: data.data.item_list||[],
                    current_page: data.data.current_page,
                    gift_list: data.data.gift_list,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EXCHANGE_GIFT", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiWhoGetGM(){
        // this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'who_get_gm'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    gift_list:data.data
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_WHO_GET_GM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiHighPrice(){
        // this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'high_price'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    ranking_list:data.data
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_HIGH_PRICE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== "" && this.props.username==""){
            this.apiCheckin();
            setTimeout(()=>{
                this.apiWhoGetGM();
                this.apiHighPrice();
            }, 1000);
        }
    }

    componentDidMount() {
        if(this.props.jwtToken !== "" && this.props.username==""){
            setTimeout(()=>{
                this.apiWhoGetGM();
                this.apiHighPrice();
            }, 1000);
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== "" && this.props.username==""){
            this.apiCheckin();

            setTimeout(()=>{
                this.apiWhoGetGM();
                this.apiHighPrice();
            }, 1000);


        }

    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    render() {

        return (
            <PageWrapper>
                <Logo/>
                <Menu page='present'/>
                <Content>
                    <Btns status={'met'} active={(this.props.exchanged==true || this.props.modal_open=="loading")==true ? false : true } onClick={()=>this.apiExchangeGift()}/>
                </Content>
                <NameListBoard>
                        <ul>
                                {
                                    this.props.gift_list && this.props.gift_list.length>0 && this.props.gift_list.map((item,key) => {
                                        if(item.get_by_character_name!=""){
                                            return(
                                                <li key={"gift_list_"+key}>
                                                    <div>{item.get_by_character_name}</div>
                                                    <div>เซิร์ฟเวอร์ {item.get_by_world_name}</div>
                                                    <div>ได้รับ {item.character_name}</div>
                                                </li>
                                            )
                                        }else{
                                            return(
                                                <li key={"gift_list_"+key}>
                                                    <div></div>
                                                    <div></div>
                                                    <div></div>
                                                </li>
                                            )
                                        }

                                    })
                                }
                        </ul>
                            <Character src={Imglist['char_info_2']} alt=''/>
                </NameListBoard>
                <RankingList>
                        <ul>
                                <li className="header">
                                        <div>อันดับ</div>
                                        <div>ชื่อ</div>
                                        <div>เซิร์ฟเวอร์</div>
                                        <div>ราคาของขวัญ</div>
                                </li>
                                {
                                    this.props.ranking_list && this.props.ranking_list.length>0 && this.props.ranking_list.map((item,key) => {
                                        return(
                                            <li key={"ranking_"+key}>
                                                <div>{key+1}</div>
                                                <div>{item.character_name}</div>
                                                <div>{item.world_name}</div>
                                                <div>{this.numberWithCommas(item.diamond)}</div>
                                            </li>
                                        )
                                    })
                                }
                        </ul>
                </RankingList>
                <ModalLoading/>
                <ModalMessage/>
                <ModalConfirm actConfirm={()=>this.actRedeemItem()} modal_item_token={"use redux"}/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(withRouter(Present));

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_2']});
    width: 1105px;
    padding-top: 90px;
    padding-bottom: 110px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const Content = styled.div`
    background: top center no-repeat url(${Imglist['content_info_1']});
    width: 1099px;
    height: 921px;
    margin: 0 auto 14px;
    box-sizing: border-box;
    color: #ffffff;
    padding-top: 180px;
    position: relative;
`
const NameListBoard = styled.div`
    background: top center no-repeat url(${Imglist['contetn_info_2']});
    width: 902px;
    height: 567px;
    margin: 0 auto 55px;
    position: relative;
    padding: 100px 110px 50px;
    box-sizing: border-box;
    color:#ffffff;
    >ul{
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 95%;
        margin: 0 auto;
        height: 100%;
        overflow: auto;
        overflow-x: hidden;
        padding-right: 20px;
        font-family: 'Kanit-Light';
        li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            >div{
                width: 30%;
            }
            >div:last-child{
                width: 39%;
                text-align:left;
            }
        }
    }
`
const Character = styled.img`
    position: absolute;
    pointer-events: none;
    bottom: -160px;
    right: -100px;
    z-index: 3;
`
const Btns = styled.div`
    display: block;
    margin: 390px auto 0;
    cursor: pointer;
    background:center no-repeat url(${Imglist['btn_exchange']});
    width: 210px;
    height: 69px;
    background-size: 100% 300%;
    ${props=>props.status === 'notmet' && 'pointer-events: none;'}
    ${props=>props.active === false ? '-webkit-filter: grayscale(100%); filter: grayscale(100%); cursor: not-allowed;pointer-events: none;': ''}
    &:hover{
        background-position: bottom center;
    }
`

const RankingList = styled.div`
    display: block;
    margin: 0 auto;
    background: center no-repeat url(${Imglist['content_info_3']});
    width: 910px;
    height: 657px;
    position: relative;
    z-index: 5;
    padding: 130px 80px 80px;
    box-sizing: border-box;
    color: #ffffff;
     >ul{
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 95%;
        margin: 0 auto;
        height: 100%;
        overflow: auto;
        overflow-x: hidden;
        padding-right: 20px;
        font-family: 'Kanit-Light';
        li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin: 1.5% auto;
            >div:first-child{
                width: 10%;
            }
            >div:nth-child(2){
                width: 30%;
            }
             >div:nth-child(3){
                width: 30%;
            }
            >div:last-child{
                width: 30%;
            }
        }
    }
     .header{

    }
`
