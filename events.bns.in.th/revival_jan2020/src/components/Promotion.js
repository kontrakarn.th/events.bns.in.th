import React, { Component } from 'react'
import fortune_potion from './../static/images/item/items/fortune_potion.png'
import soup from './../static/images/item/items/soup.png'
import extenstion_ticket from './../static/images/item/items/extenstion_ticket.png'
import lv10 from './../static/images/item/lv10.png'
import lv20 from './../static/images/item/lv20.png'
import lv30 from './../static/images/item/lv30.png'
import pentagon_jewelbox from './../static/images/item/items/pentagon_jewelbox.png'
import hexagon_jewelbox from './../static/images/item/items/hexagon_jewelbox.png'
import marble_box from './../static/images/item/items/marble_box.png'
import wooden_piece from './../static/images/item/items/wooden_piece.png'
import s_wood from './../static/images/item/items/s_wood.png'
import gold_marblebox from './../static/images/item/items/gold_marblebox.png'
import red_box from './../static/images/item/items/red_box.png'
import zoom from './../static/images/item/items/zoom_icon.png'
import costume from './../static/images/item/items/costume.png'

import popup_marble from './../static/images/item/popup/popup_marble.png'
import popup_jewel from './../static/images/item/popup/popup_jewel.png'
import popup_goldmarble from './../static/images/item/popup/popup_goldmarble.png'
import popup_goldjewel from './../static/images/item/popup/popup_goldjewel.png'

import border from './../static/images/item/border.png'

import char_left from './../static/images/char_left.png'
import char_right from './../static/images/char_right.png'
import char_left_bottom from './../static/images/char_left_bottom.png'

const itemlist = 
[
    {
        item_left: lv10,
        des: 'กล่องพิชิตเลเวล 10',
        item_right:
        [
            {
                item: fortune_potion,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'blue'
            },
            {
                item: soup,
                des: 'ซุปโอสถ ฮันกาแมง 5 ชิ้น',
                color: 'blue'
            },
            {
                item: extenstion_ticket,
                des: 'ตั๋วขยาย ดราก้อนเทรดเดอร์ 5 ชิ้น',
                color: 'purple'
            },
            {
                item: lv20,
                des: 'กล่องพิชิตเลเวล 20<br /> 1 กล่อง',
                color: 'purple'
            },
        ]
    },
     {
        item_left: lv20,
        des: 'กล่องพิชิตเลเวล 20',
        item_right:
        [
            {
                item: fortune_potion,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'blue'
            },
            {
                item: soup,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'blue'
            },
            {
                item: extenstion_ticket,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'purple'
            },
            {
                item: pentagon_jewelbox,
                des: 'กล่องอัญมณี<br />ห้าเหลี่ยมของฮงมุน<br /> 1 กล่อง',
                color: 'purple',
                zoom: true,
                zoom_img: popup_jewel
            },
            {
                item: marble_box,
                des: 'กล่องลูกแก้ว ขั้น 1<br />(ดาวอังคาร พุธ พฤหัส)<br /> 1 กล่อง',
                color: 'purple',
                zoom: true,
                zoom_img: popup_marble
            },
            {
                item: wooden_piece,
                des: 'ชิ้นไม้ ผสมประดิษฐ์ 1 ชิ้น',
                color: 'blue'
            },
            {
                item: lv20,
                des: 'กล่องพิชิต เลเวล 30<br /> 1 กล่อง',
                color: 'purple'
            },
        ]
    },
    {
        item_left: lv20,
        des: 'กล่องพิชิตเลเวล 30',
        item_right:
        [
            {
                item: fortune_potion,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'blue'
            },
            {
                item: soup,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'blue'
            },
            {
                item: extenstion_ticket,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'purple'
            },
            {
                item: pentagon_jewelbox,
                des: 'กล่องอัญมณี<br />ห้าเหลี่ยมของฮงมุน 2 กล่อง',
                color: 'purple',
                zoom: true,
                zoom_img: popup_jewel
            },
            {
                item: marble_box,
                des: 'กล่องลูกแก้ว ขั้น 1<br />(ดาวอังคาร พุธ พฤหัส) 2 กล่อง',
                color: 'purple',
                zoom: true,
                zoom_img: popup_marble
            },
            {
                item: wooden_piece,
                des: 'ชิ้นไม้ ผสมประดิษฐ์ 2 ชิ้น',
                color: 'blue'
            },
            {
                item: lv20,
                des: 'กล่องพิชิต เลเวล 30<br /> 1 กล่อง',
                color: 'purple'
            },
        ]
    },
    {
        item_left: lv30,
        des: 'กล่องพิชิตเลเวล 40',
        item_right:
        [
            {
                item: fortune_potion,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'blue'
            },
            {
                item: soup,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'blue'
            },
            {
                item: extenstion_ticket,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'purple'
            },
            {
                item: hexagon_jewelbox,
                des: 'กล่องอัญมณี<br />ห้าเหลี่ยมของฮงมุน พิเศษ 1 กล่อง',
                color: 'purple',
                zoom: true,
                zoom_img: popup_goldjewel
            },
            {
                item: gold_marblebox,
                des: 'กล่องลูกแก้ว ขั้น 1<br />(ดาวศุกร์ เสาร์)<br /> 1 กล่อง',
                color: 'purple',
                zoom: true,
                zoom_img: popup_goldmarble
            },
            {
                item: wooden_piece,
                des: 'ชิ้นไม้ ผสมประดิษฐ์ 3 ชิ้น',
                color: 'blue'
            },
            {
                item: lv20,
                des: 'กล่องพิชิต เลเวล 50<br /> 1 กล่อง',
                color: 'purple'
            },
        ]
    },
    {
        item_left: lv30,
        des: 'กล่องพิชิตเลเวล 50',
        item_right:
        [
            {
                item: fortune_potion,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'blue'
            },
            {
                item: soup,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'blue'
            },
            {
                item: extenstion_ticket,
                des: 'ยาโชคชะตา 5 ชิ้น',
                color: 'purple'
            },
            {
                item: hexagon_jewelbox,
                des: 'กล่องอัญมณี<br />ห้าเหลี่ยมของฮงมุน พิเศษ 2 กล่อง',
                color: 'purple',
                zoom: true,
                zoom_img: popup_goldjewel
            },
            {
                item: gold_marblebox,
                des: 'กล่องลูกแก้ว ขั้น 1<br />(ดาวศุกร์ เสาร์)<br /> 2 กล่อง',
                color: 'purple',
                zoom: true,
                zoom_img: popup_goldmarble
            },
            {
                item: s_wood,
                des: 'ชิ้นไม้ผสม ยอดเยี่ยม 1 ชิ้น',
                color: 'purple'
            },
            {
                item: red_box,
                des: 'กล่องอาวุธ แห่งเนบิวลา ขั้น 1,<br /> 1 กล่อง',
                color: 'red'
            },
            {
                item: red_box,
                des: 'กล่องอาวุธ พายุ ขั้น 1,<br /> 1 กล่อง',
                color: 'red'
            },
            {
                item: costume,
                des: 'ชุดหมอกสีคราม 1 ชุด',
                color: 'purple'
            },
        ]
    },
]

export default class Promotion extends Component {  
    render() {
        return (
            <div className="newrevival__promotion">
                <div className="newrevival__container">
                    <div className="newrevival__border"><img src={border} alt=""/></div>
                    <div className="newrevival__char_left"><img src={char_left} alt=""/></div>
                    <div className="newrevival__char_right"><img src={char_right} alt=""/></div>
                    <div className="newrevival__char_left_bottom"><img src={char_left_bottom} alt=""/></div>
                    <div className="newrevival__contentbox newrevival__contentbox--condition">
                        <div className="newrevival__condition">
                             <div className="newrevival__condition--title">
                                    เงื่อนไขกิจกรรม
                             </div>
                             ผู้เล่นที่สร้างตัวละครใหม่ตั้งแต่ <span className="text--lightbrown">21 พฤศจิกายน 2561 หลังปิดปรับปรุงเซิร์ฟเวอร์</span><br/>
                             จะได้รับ <span className="text--lightbrown">กล่องพิชิตเลเวล 10 </span>จากเควสเนื้อเรื่องบทที่ 1<br/><br/>
                             <span className="text--lightbrown">หรือ </span>ผู้เล่นที่สร้างตัวละครผ่าน <span className="text--lightbrown">ตั๋วอัพเกรดด่วนเลเวล 55</span> จะได้รับ 
                             <span className="text--lightbrown">กล่องพิชิตเลเวล 10</span>
                        </div>
                    </div>
                    <div className="newrevival__tabletitle">
                        <div className="newrevival__tabletitle--titlebox">
                                (กล่องพิชิตเลเวล)
                        </div>
                        <div className="newrevival__tabletitle--titlebox">
                                (ไอเทมภายในกล่อง)
                        </div>
                    </div>
                    <div className="newrevival__tablecontainer">
                        {
                            itemlist.map((item,key) => {
                                return(
                                    <div className="newrevival__tablecontainer--each" key={'row'+ key}>
                                        <div className="newrevival__tablecontainer--content">
                                            <div className="newrevival__itemcontain">
                                                 <div className="newrevival__itemborder newrevival__itemborder--gold">
                                                       <img src={item.item_left} alt=""/>
                                                 </div>

                                                <div className="newrevival__itemcontain--text">
                                                     {item.des}
                                                </div>
                                             </div>
                                        </div>
                                        <div className="newrevival__tabletitle--content2">
                                            <div className="newrevival__tabletitle--contentwrap">
                                            {
                                                item.item_right.map((initem,index) => {
                                                    return(
                                                        <div className="newrevival__itemcontain">
                                                             <div className={"newrevival__itemborder newrevival__itemborder--" + initem.color}>
                                                                   <img src={initem.item} alt=""/>
                                                                   {
                                                                        initem.zoom
                                                                        ?
                                                                             <div className="newrevival__zoom">
                                                                                <img src={zoom} alt=""/>
                                                                                <div className="newrevival__itempopup">
                                                                                    <img src={initem.zoom_img} alt=""/>
                                                                                </div>
                                                                           </div>
                                                                        :
                                                                            null
                                                                   }
                                                             </div>
                                                            <div className="newrevival__itemcontain--text" dangerouslySetInnerHTML={{__html: initem.des}}>
                                                                
                                                            </div>
                                                         </div>
                                                   )
                                                })
                                            }
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        )
  }
}
