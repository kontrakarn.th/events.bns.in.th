import * as actionTypes from './../constants/Actions';

export const onAccountAuth = (sessionKey) => {
	return {
		type: 'ACCOUNT_AUTH',
		value: sessionKey
	}
}

export const onAccountAuthed = (userData) => {
	return {
		type: actionTypes.ACCOUNT_AUTHED,
		value: userData
	}
}

export const onAccountLogin = () => {
	return {
		type: actionTypes.ACCOUNT_LOGIN,
		value: ""
	}
}

export const onAccountLogout = () => {
	console.log('logout')
	return {
		type: actionTypes.ACCOUNT_LOGOUT,
		value: ""
	}
}

export const receiveSessionKey = (sessionKey) => {
	return {
		type: actionTypes.RECEIVE_SESSION_KEY,
		value: sessionKey
	}
}

export const setLoginUrl = (url) => {
	return {
		type: actionTypes.SET_LOGIN_URL,
		value: url
	}
}

export const setLogoutUrl = (url) => {
	return {
		type: actionTypes.SET_LOGOUT_URL,
		value: url
	}
}

export const onAccountAuthChecked = () => {
	return {
		type: actionTypes.ACCOUNT_AUTH_CHECKED,
		value: true
	}
}

export const setJwtToken = (token) => {
	return {
		type: 'SET_JWT_TOKEN',
		value: token
	}
}

export const setUsername = (payload) => ({
	type: 'SET_USERNAME',
	value: payload
});

export const setSoulPermission = (payload) => ({
	type: 'SET_SOUL_PERMISSION',
	value: payload
})

export const setBuySoulResult = (payload) => ({
	type: 'SET_BUY_SOUL_RESULT',
	value: payload
})

export const setItemHistory = (payload) => ({
	type: 'SET_ITEM_HISTORY',
	value: payload
})

