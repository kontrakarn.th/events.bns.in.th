import React, {Component} from 'react';
import {Route} from 'react-router-dom';
// import SSOMiddleware from './../middlewares/SSOMiddleware';
import OauthMiddleware from './../middlewares/OauthMiddleware';

import { Home } from './../pages/Home';

export class Web extends Component {
	render() {
		return (
			<>
				<Route path={process.env.REACT_APP_EVENT_PATH} component={OauthMiddleware} />
				<Route path={process.env.REACT_APP_EVENT_PATH} exact component={Home} />
			</>
		);
	}
}
