import React from 'react';
import styled from 'styled-components';

export default (props) => {
  const actClick = ()=>{
    if(props.onClick) props.onClick();
  }
  return (
    <BtnStyle
      width={ props.width || 150}
      height={ props.height || 50}
      onClick={()=>actClick()}
      src={props.src || ""}
    />
  )
}

const BtnStyle = styled.canvas`
  background-image: url(${props=>props.src});
  background-position: top center;
  background-size: 100% auto;
  &:hover {
    background-position: bottom center;
  }
`;
