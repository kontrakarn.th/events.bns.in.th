import React from "react";
import { BrowserRouter, Route } from "react-router-dom";

//======= middlewares =======
// import OauthMiddleware from "./../middlewares/OauthLogin";
import OauthMiddleware from "./../middlewares/OauthMiddleware";
import Modals from "./../features/Modals";

//======= pages =======
import Home from "./../pages/Home";

export default (props) => {
  let path = process.env.REACT_APP_EVENT_PATH;
  return (
    <BrowserRouter>
      <>
        <Route path={path} component={OauthMiddleware} />
        <Route path={path} component={Modals} />
        <Route path={path} exact component={Home} />
      </>
    </BrowserRouter>
  );
};
