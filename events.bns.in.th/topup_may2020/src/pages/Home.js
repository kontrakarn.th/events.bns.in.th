import React, { useEffect } from "react";
import { connect } from "react-redux";
import { setValues } from "./../store";
import { apiPost } from "./../constants/Api";
import styled from "styled-components";
import imgList from "./../constants/ImportImages";
import F11Layout from "./../features/F11Layout";
import Btn from "./../common/Btn";

const HomePage = (props) => {
  const F11Setting = {
    showUserName: true,
    showCharacterName: true,
    showMenu: false,
    page: "",
  };
  const actConfirm = (index,id) => {
    let msg= "<div>ต้องการกดรับโบนัสไดมอนด์<br/>มูลค่า <span style='color:#51d944'>"+props.packages[index].topup_bonus+"</span> ไดมอนด์ ?<br/><span style='color:#51d944'>(ไม่สามารถเปลี่ยนแปลงภายหลัง)</span></div>";
    props.setValues({
      modal_open: "confirm",
      modal_message: msg,
      modal_id: id,
      modal_index: index,
    })

  }
  const apiEventInfo = () => {
    props.setValues({ modal_open:"loading"});
    apiPost(
      "REACT_APP_API_POST_EVENT_INFO",
      props.jwtToken,
      { type: "event_info" },
      (res) => {
        props.setValues({
          ...res.data,
          status: res.status,
          modal_open: "",
        })
      },
      (res) => {
        props.setValues({ 
          modal_open:"message",
          modal_message: res.message,
        });
      }
    );
  };
  // const apiClaimReward = (id) => {
  //   apiPost(
  //     "REACT_APP_API_POST_CLAIM_REWARD",
  //     props.jwtToken,
  //     { type: "redeem", id},
  //     (res) => {
  //       console.log("ok2", res);
  //     },
  //     (res) => {

  //       props.setValues({ 
  //         modal_open:"message",
  //         modal_message: res.message,
  //       });
  //     }
  //   );
  // };

  useEffect(() => {
    if (props.jwtToken !== "") {
      apiEventInfo();
    }
  }, [props.jwtToken]);
  return (
    <F11Layout {...F11Setting}>
      <HomeStyle>
        <section className="banner"></section>
        <section className="detail">
          <img src={imgList.detail} alt="" />
        </section>
        <section className="bonus">
          {props.packages.map((item,index)=>{
            let btnClass = "bonus__btn";
            // if(props.already_claimed) btnClass = "bonus__btn bonus__btn--cannot";
            // if(item.remain_rights === "0") btnClass = "bonus__btn bonus__btn--inactive";
            // if(item.id === props.selected_package) btnClass = "bonus__btn bonus__btn--selected";
            switch(item.status) {
              case 1: btnClass = "bonus__btn bonus__btn--canreceive"; break;
              case 2: btnClass = "bonus__btn bonus__btn--selected"; break;
              case 3: btnClass = "bonus__btn bonus__btn--inactive"; break;
              default : btnClass = "bonus__btn bonus__btn--cannot";
            }
            return (
              <div className="bonus__slot" key={"packages_"+index}>
                <div className="bonus__text">
                  สิทธิ์คงเหลือ
                  <br />
                  {item.remain_rights}
                </div>
                <a className={btnClass} onClick={()=>actConfirm(index,item.id)}></a>
              </div>
            )
          })}
        </section>
      </HomeStyle>
    </F11Layout>
  );
};

const mstp = (state) => ({ ...state });
const mdtp = { setValues };
export default connect(mstp, mdtp)(HomePage);

const HomeStyle = styled.div`
  position: relative;
  display: block;
  width: 1105px;
  height: 2113px;
  background-image: url(${imgList.background});
  background-position: top center;
  background-size: 100% auto;
  overflow: hidden;
  .banner {
    display: block;
    width: 100%;
    height: 700px;
  }
  .detail {
    display: block;
    width: 949px;
    height: 569px;
    margin: 40px auto;
    background-image: url(${imgList.detail});
  }
  .bonus {
    position: relative;
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    width: 949px;
    height: 650px;
    padding: 140px 40px 0px;
    margin: 68px auto;
    background-image: url(${imgList.bonus});
    background-repeat: no-repeat;
    background-position: top center;
    box-sizing: border-box;
    &__slot {
      position: relative;
      display: block;
      width: 240px;
      height: 400px;
    }
    &__text {
      position: absolute;
      top: 260px;
      display: block;
      width: 100%;
      color: #ceaa7a;
      text-align: center;
    }
    &__btn {
      position: absolute;
      bottom: 0px;
      left: 50%;
      transform: translate(-50%, 0px);
      width: 139px;
      height: 46px;
      background-image: url(${imgList.btn_receive});
      background-position: top center;
      background-size: 100% auto;
      &:hover {
        background-position: bottom center;
      }
      &--cannot {
        background-image: url(${imgList.btn_cannot});
        pointer-events: none;
      }
      &--inactive {
        background-image: url(${imgList.btn_inactive});
        pointer-events: none;
      }
      &--selected {
        background-image: url(${imgList.btn_receive});
        filter: grayscale(1);
        pointer-events: none;
      }
    }
  }
`;
