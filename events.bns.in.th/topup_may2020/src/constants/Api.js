
export const apiPost = ( api_name, token, api_data, successCallback=()=>{}, failCallback=()=>{}, responseCallback=()=>{}, errorCallback=()=>{} )=>{
  fetch(
    process.env.REACT_APP_API_SERVER_HOST+process.env[api_name],
    {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': (["B", "e", "a", "r", "e", "r"].join("")) + " " + token,
      },
      body: JSON.stringify(api_data)
    }
  ).then(response => {
    if (response.status == 200) {
      response.json().then(res => {
        if (res.status)
          successCallback(res);
        else
          failCallback(res);
      });
    } else {
      responseCallback(response);
    }
  }, error => {
      errorCallback(error);
  });
}

// import KJUR from 'jsrsasign';
// const createJwt = (data)=>{
//         if(data){
//             let oHeader = {
//               alg: 'HS256',
//               typ: 'JWT'
//             };
//             let sHeader = JSON.stringify(oHeader);
//             let sPayload = '{"' + decodeURI(data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}';
//             let sJWT = KJUR.jws.JWS.sign("HS256", sHeader, sPayload, process.env.REACT_APP_JWT_SECRET);
//
//             let parts = sJWT.split(".");
//             const header_payloazd = (["G", "a", "r", "e", "n", "a", "T", "H"].join("")) + " " + btoa(parts[0] + "." + parts[2]);
//             const body_payload = btoa(parts[1]);
//             return {
//                 headers:{
//                     'Authorization':header_payload
//                 },
//                 body:body_payload
//             }
//         }else{
//             return {
//                 headers:{},
//                 body:{}
//             };
//         }
// }
// const createAuthorizationHeaders = (token) => {
//     // Header
//     let headers = {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json; charset=UTF-8',
//         'Authorization': (["B", "e", "a", "r", "e", "r"].join("")) + " " + token,
//     };
//     return headers;
// }
