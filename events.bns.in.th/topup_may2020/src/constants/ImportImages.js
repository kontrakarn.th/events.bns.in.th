const local             = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/local.jpg';
const background        = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/background.jpg';
const bonus             = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/bonus.png';
const btn_cannot        = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/btn_cannot.png';
const btn_inactive      = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/btn_inactive.png';
const btn_receive       = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/btn_receive.png';
const detail            = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/detail.png';
const modal_background  = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/modal_background.png';
const modal_confirm     = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/modal_confirm.png';
const modal_message     = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/modal_message.png';
const btn_confirm       = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/btn_confirm.png';
const btn_cancel        = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/btn_cancel.png';
const btn_received        = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/btn_received.png';

const menu_bg           =  'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/f11/menu_bg.jpg';
const bg_permission     =  'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/f11/permission.jpg';
const icon_scroll       =  'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/f11/icon_scroll.png';
const btn_home          =  'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/f11/btn_home.png';
const icon_dropdown     =  'https://cdngarenanow-a.akamaihd.net/webth/bns/events/topup_may2020/images/f11/icon_dropdown.png';

//==============================================================================================================

export default {
	local,
	background,
	bonus,
	btn_cannot,
	btn_inactive,
	btn_receive,
	detail,
	modal_background,
	modal_confirm,
	modal_message,
	btn_confirm,
	btn_cancel,
  btn_received,
  
	menu_bg,
	bg_permission,
	icon_scroll,
	btn_home,
	icon_dropdown,

}
