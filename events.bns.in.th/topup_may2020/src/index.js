import "core-js/es6/map";
import "core-js/es6/set";
import "raf/polyfill";
import 'cross-fetch/polyfill';
import React from "react";

import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { combineReducers, createStore } from "redux";
import Routes from "./routes";
import serviceWorker from "./serviceWorker";
import AccountReducer from "./store";
import "./styles/index.css";

ReactDOM.render(
  <Provider store={createStore(AccountReducer)}>
    <Routes />
  </Provider>,
  document.getElementById("root")
);
