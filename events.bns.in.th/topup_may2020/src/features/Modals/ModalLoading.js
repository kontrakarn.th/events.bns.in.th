import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import ModalCore from "./ModalCore";
import { setValues } from "./../../store";

const CPN = (props) => {
  return (
    <ModalCore modalName="loading">
      <ModalLoadingContent>
        <h3>กำลังดำเนินการ...</h3>
      </ModalLoadingContent>
    </ModalCore>
  );
};

const mstp = (state) => ({ ...state });
const mdtp = { setValues };

export default connect(mstp, mdtp)(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalLoadingContent = styled.div`
  text-align: center;
  color: #ffffff;
`;
