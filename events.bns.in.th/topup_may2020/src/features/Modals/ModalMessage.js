import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import ModalCore from "./ModalCore";
import { setValues, closeModal } from "./../../store";
import imgList from "../../constants/ImportImages";

const CPN = (props) => {

  return (
    <ModalCore modalName="message" actClickOutside={() => props.closeModal()}>
      <MessageStyle>
        <div
          className="text"
          dangerouslySetInnerHTML={{ __html: props.modal_message }}
        />
        <div className="bottom">
          <Btn src={imgList.btn_confirm} onClick={() => props.closeModal()} />
        </div>
      </MessageStyle>
    </ModalCore>
  );
};
// รับโบนัสไดมอนด์
// มูลค่า 12,500 ไดมอนด์
// เรียบร้อยแล้ว


const mstp = (state) => ({ ...state });
const mdtp = { setValues, closeModal };
export default connect(mstp, mdtp)(CPN);

const MessageStyle = styled.div`
  position: relative;
  display: block;
  width: 555px;
  height: 333px;
  font-size: 1em;
  color: #ffffff;
  background: no-repeat center url(${imgList.modal_message});
  padding: 100px 30px 0px;
  .text {
    width: 100%;
    height: 132px;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    font-size: 20px;
    line-height: 1.5em;
    word-break: break-word;
  }
  .bottom {
    position: absolute;
    bottom: -20px;
    left: 0px;
    display: flex;
    width: 100%;
    justify-content: center;
    align-items: center;
  }
`;

const Btn = styled.div`
  width: 139px;
  height: 46px;
  display: inline-block;
  cursor: pointer;
  color: #000;
  background: no-repeat top center url(${(props) => props.src});
  &:hover {
    opacity: 0.9;
  }
`;