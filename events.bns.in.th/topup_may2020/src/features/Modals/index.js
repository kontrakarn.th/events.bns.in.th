import React from 'react';

import ModalMessage from './ModalMessage';
import ModalLoading from './ModalLoading';
import ModalConfirm from './ModalConfirm';

export default (props)=> (
    <>
      <ModalLoading />
      <ModalMessage />
      <ModalConfirm />
    </>
)
