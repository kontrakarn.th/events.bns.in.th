import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import ModalCore from "./ModalCore";
import { setValues, closeModal } from "./../../store";
import imgList from "../../constants/ImportImages";
import { apiPost } from "./../../constants/Api";

const CPN = (props) => {
  const apiClaimReward = () => {
    let msg= "<div>รับโบนัสไดมอนด์<br/>มูลค่า <span style='color:#51d944'>"+props.packages[props.modal_index].topup_bonus+"</span> ไดมอนด์<br/>เรียบร้อยแล้ว</div>";
   
    props.setValues({ modal_open:"loading"});
    apiPost(
      "REACT_APP_API_POST_CLAIM_REWARD",
      props.jwtToken,
      { type: "claim_reward", id: props.modal_id},
      (res) => {
        props.setValues({
          ...res.data,
          modal_open:"message",
          modal_message: msg,
        })
      },
      (res) => {

        props.setValues({ 
          modal_open:"message",
          modal_message: res.message,
        });
      }
    );
  };

  return (
    <ModalCore modalName="confirm" actClickOutside={() => {}}>
      <ConfirmStyle>
        <div
          className="text"
          dangerouslySetInnerHTML={{ __html: props.modal_message }}
        />
        <div className="bottom">
          <Btn src={imgList.btn_confirm} onClick={() => apiClaimReward()} />
          <Btn src={imgList.btn_cancel} onClick={() => props.closeModal()} />
        </div>
      </ConfirmStyle>
    </ModalCore>
  );
};
// ต้องการกดรับโบนัสไดมอนด์
// มูลค่า 12,500 ไดมอนด์ ?
// (ไม่สามารถเปลี่ยนแปลงภายหลัง)

const mstp = (state) => ({ ...state });
const mdtp = { setValues, closeModal };
export default connect(mstp, mdtp)(CPN);

const ConfirmStyle = styled.div`
  position: relative;
  display: block;
  width: 555px;
  height: 333px;
  font-size: 1em;
  color: #ffffff;
  background: no-repeat center url(${imgList.modal_confirm});
  padding: 100px 30px 0px;
  .text {
    width: 100%;
    height: 132px;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    font-size: 20px;
    line-height: 1.5em;
    word-break: break-word;
  }
  .bottom {
    position: absolute;
    bottom: -15px;
    left: 0px;
    display: flex;
    width: 100%;
    justify-content: space-evenly;
    align-items: center;
  }
`;

const Btn = styled.div`
  width: 139px;
  height: 46px;
  display: inline-block;
  cursor: pointer;
  color: #000000;
  background: no-repeat top center url(${(props) => props.src});
  &:hover {
    opacity: 0.9;
  }
`;
