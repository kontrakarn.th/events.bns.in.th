import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import {onAccountLogout } from './../../store';
import imgList from './../../constants/ImportImages';

const CPN = (props) => {

    let dmp = process.env.REACT_APP_API_SERVER_HOST;

    let nameText = [];
    if( props.showUserName && props.username && props.username !== "" ){
      console.log("username",props.username);
      nameText.push(props.username);
    };
    if( props.showCharacterName && props.character_name && props.character_name !== "" ){
      console.log("character",props.character_name);
      nameText.push(props.character_name);
    }

    let coin = props.coin || 0;

    return (
      <>
        {nameText.length > 0 &&
          <SlotText>{nameText.join(":")}</SlotText>
        }
        {props.showLogin &&
          <SlotBtn onClick={()=>onAccountLogout()}>Logout</SlotBtn>
        }
        <ButtomHome href={dmp} />
        {props.showScroll &&
          <IconScroll />
        }
      </>
    )
}
const mstp = state => ({...state});
const mdtp = { onAccountLogout };
export default connect( mstp, mdtp )(CPN);

const SlotText = styled.div`
  float: right;
  pointer-events: auto;
  display: inline-block;
  padding: 0px 25px;
  margin-right: 17px;
  margin-top:17px;
  /* width: fit-content; */
  /* max-width: 170px; */
  text-align: center;
  height: 45px;
  line-height: 45px;
  background-color: rgba(0,0,0,0.7);
  border-radius: 15px;
  color: #FFFFFF;
  font-size: 14px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  /* transition: width 0.3s ease-in-out; */
`;
const SlotBtn = styled.a`
    pointer-events: auto;
    cursor: pointer;
    display: inline-block;
    padding: 0px 10px;
    margin-left: 17px;
    width: fit-content;
    max-width: 170px;
    text-align: center;
    height: 45px;
    line-height: 45px;
    background-color: rgba(0,0,0,0.7);
    border-radius: 15px;
    color: #FFFFFF;
    font-size: 14px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;
const ButtomHome = styled.a`
    position: absolute;
    top: calc(50% - 32px);
    right: 14px;
    display: block;
    width: 63px;
    height: 63px;
    background: no-repeat center url(${imgList.btn_home});
    user-select: auto;
    pointer-events:  all;
`;
const IconScrollAnimation = keyframes`
    0% { transform: translate3d(-50%,0,0); }
    100% { transform: translate3d(-50%,-10%,0); }
`;
const IconScroll = styled.div`
    position: absolute;
    bottom: 10px;
    left: 50%;
    transform: translate3d( -50%, 0, 0);
    display: block;
    width: 52px;
    height: 71px;
    background: no-repeat center url(${imgList.icon_scroll});
    animation: ${IconScrollAnimation} 1s infinite alternate;
`;
