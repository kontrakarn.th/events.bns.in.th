// import * as actionTypes from './../constants/Actions';
//Actions
export const onAccountAuth        = (sessionKey)  => ({ type: "ACCOUNT_AUTH", value: sessionKey });
export const onAccountAuthed      = (userData)    => ({ type: "ACCOUNT_AUTHED", value: userData });
export const onAccountLogin       = ()            => ({ type: "ACCOUNT_LOGIN", value: "" });
export const onAccountLogout      = ()            => ({ type: "ACCOUNT_LOGOUT", value: "" });
export const receiveSessionKey    = (sessionKey)  => ({ type: "RECEIVE_SESSION_KEY", value: sessionKey });
export const setLoginUrl          = (url)         => ({ type: "SET_LOGIN_URL", value: url });
export const setLogoutUrl         = (url)         => ({ type: "SET_LOGOUT_URL", value: url });
export const onAccountAuthChecked = ()            => ({ type: "ACCOUNT_AUTH_CHECKE", value: true });
export const setJwtToken          = (token)       => ({ type: "SET_JWT_TOKEN", value: token });
export const setValue             = (key, value)  => ({ type: "SET_VALUE", value, key });
export const setValues            = (value)       => ({ type: "SET_VALUES", value });
export const closeModal           = ()            => ({ type: "CLOSE_MODAL" });

const defaultState = {
  //===login values
  sessionKey: "",
  userData: {},
  authState: "",
  loginUrl: "",
  logoutUrl: "",
  authed: false,
  jwtToken: "",

  //===layout values
  showCharacterName: true,
  characters: [],
  username: "",
  character_name: "",
  coin: 0,
  selected_char: false,
  loginStatus: false,
  showScroll: true,
  showMenu: false,
  noScroll: true,
  permission: true,
  status: false,

  menu: [
    { title: "หน้าหลัก", link: process.env.REACT_APP_EVENT_PATH },
    { title: "ส่งของขวัญ", link: process.env.REACT_APP_EVENT_PATH + "/gift" },
    {
      title: "ประวัติการแลก",
      link: process.env.REACT_APP_EVENT_PATH + "/history",
    },
  ],
  //=== MODAL ===//
  modal_open: "", //"loading","selectcharacter","confirm"
  modal_message: "",
  modal_id: 0,
  modal_index: 0,
  
  //=== EVENT ===//
  status: false,

  already_claimed: false,
  packages: [
    {
      id: 1,
      remain_rights: "1,500",
      status: 0,
      title: "12,500 Diamond ",
      topup_bonus: "12,500",
      topup_require: "50,000",
    },
    {
      id: 2,
      remain_rights: "999",
      status: 0,
      title: "50,000 Diamond",
      topup_bonus: "100,000",
      topup_require: "100,000",
    },
    {
      id: 3,
      remain_rights: "0",
      status: 3,
      title: "300,000 Diamond",
      topup_bonus: "300,000",
      topup_require: "300,000",
    },  
  ],
  selected_package: 0,
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case "ACCOUNT_AUTH":          return { ...state, sessionKey: action.value };
    case "ACCOUNT_AUTHED":        return { ...state, userData: action.value };
    case "RECEIVE_SESSION_KEY":   return { ...state, sessionKey: action.value };
    case "SET_LOGIN_URL":         return { ...state, loginUrl: action.value };
    case "SET_LOGOUT_URL":        return { ...state, logoutUrl: action.value };
    case "ACCOUNT_LOGIN":         return { ...state, authState: "LOGGED_IN" };
    case "ACCOUNT_LOGOUT":        return { ...state, authState: "LOGGED_OUT" };
    case "ACCOUNT_AUTH_CHECKED":  return { ...state, authed: true };
    case "SET_JWT_TOKEN":         return { ...state, jwtToken: action.value };

    case "SET_VALUE":
      return Object.keys(defaultState).indexOf(action.key) >= 0
        ? { ...state, [action.key]: action.value }
        : state;

    case "SET_VALUES":            return { ...state, ...action.value };
    case "CLOSE_MODAL":           return { ...state, modal_open: "" };
    default:                      return state;
  }
};
