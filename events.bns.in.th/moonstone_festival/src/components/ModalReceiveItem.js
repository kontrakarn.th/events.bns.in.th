import React from 'react';
import Modal from './Modal';

export default class ModalReceiveItem extends React.Component {
    render() {
        let title = this.props.data && this.props.data || [];
        return (
            <Modal
                open={this.props.open}
                title={this.props.title}
                confirmMode={false}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox">
                    <span className="inbox__head">คุณได้รับไอเทม</span>
                    {this.props.msg}
                </div>
            </Modal>
        )
    }
}
