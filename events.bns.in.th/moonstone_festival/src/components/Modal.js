import React, { Component } from 'react'
export default class Modal extends Component {
    render() {
        return (
            <div className={"modal "+(this.props.open ? "":"close")} >
                <div className="modal__backdrop" />
                <div className={"modal__content"}>
                    {this.props.title === "" ?
                        <br />
                        :
                        <div className={"modal__head"}>{this.props.title}</div>
                    }
                    {this.props.children}
                    <div className="modal__bottom">

                        {this.props.confirmMode &&
                            <a className="modal__button" onClick={()=>this.props.actConfirm()}>
                                ตกลง
                            </a>
                        }
                        {this.props.confirmMode &&
                            <a className="modal__button" onClick={()=>this.props.actClose()}>
                                ยกเลิก
                            </a>

                        }
                        {!this.props.confirmMode &&
                            <a className="modal__button" onClick={()=>this.props.actClose()}>
                                ตกลง
                            </a>
                        }
                    </div>
                </div>
            </div>
        )
  }
}
