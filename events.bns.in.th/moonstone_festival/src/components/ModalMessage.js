import React from 'react';
import Modal from './Modal';

export default class ModalMessage extends React.Component {
    render() {
        let title = this.props.data && this.props.data || [];
        return (
            <Modal
                open={this.props.open}
                title=" "
                confirmMode={false}
                actClose={()=>this.props.actClose()}
            >
                <div dangerouslySetInnerHTML={{__html: this.props.msg}} />
            </Modal>
        )
  }
}
