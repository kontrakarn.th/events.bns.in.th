import React, { Component } from 'react';
import {connect} from 'react-redux';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/moonstone_festival.css';

import {apiPost} from './../middlewares/Api';

import ModalMessage from '../components/ModalMessage';
import ModalSelectCharacter from '../components/ModalSelectCharacter';
import ModalConfirme from '../components/ModalConfirme';
import ModalReceiveItem from '../components/ModalReceiveItem';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';

import iconMoonstone from './../static/images/moonstone_festival/icon_moonstone.png';
import iconSoulstone from './../static/images/moonstone_festival/icon_soulstone.png';

import {
    onAccountLogout
} from '../actions/AccountActions';

class HomeContainer extends Component {
    actSelectCharacter(character_id,character_name){
        this.apiAcceptCharacter(character_id)
    }
    actExchangeMoonstone(){
        this.apiRedeem(1);
    }
    actExchangeSoulstone(){
        this.apiRedeem(2);
    }
    numberWithCommas(x){
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }
// ========= ========= ========= ========= ========= ========= ========= ========= =========
// REACT_APP_API_POST_EVENT_INFO
// REACT_APP_API_POST_GET_CHARACTER
// REACT_APP_API_POST_ACCEPT_CHARACTER
// REACT_APP_API_POST_REDEEM

    apiEventInfo(){
        this.setState({ showLoading: true});
        let self = this;
        let dataSend={ type : "event_info" };
        let successCallback = (data)=>{
            console.log("data",data);
            this.setState({
               permission: true,
               showLoading: false,
               ...data.content
           })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }
            if(data.type === "no_char"){
                this.apiGetCharacter();
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO",this.props.jwtToken,dataSend,successCallback,failCallback);
    }
    apiGetCharacter(){
        this.setState({ showLoading: true});
        let self = this;
        let dataSend={ type : "get_character" };
        let successCallback = (data)=>{
            this.setState({
               permission: true,
               showLoading: false,
               listCharater: data.content,
               showModalSelectCharacter: true
           })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }
        };
        apiPost("REACT_APP_API_POST_GET_CHARACTER",this.props.jwtToken,dataSend,successCallback,failCallback);
    }
    apiAcceptCharacter(character_id){
        this.setState({ showLoading: true});
        let self = this;
        let dataSend={
            type : "accept_character",
            id: character_id,
        };
        let successCallback = (data)=>{
            console.log("data",data);
            this.setState({
               permission: true,
               showLoading: false,
               showModalConfirmCharacter: false,
               showModalMessage: data.message,
               ...data.content
           })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            } else {
                this.setState({
                    showModalMessage: data.message,
                    showModalSelectCharacter: true,
                    showLoading: false,
                });
            }
        };
        apiPost("REACT_APP_API_POST_ACCEPT_CHARACTER",this.props.jwtToken,dataSend,successCallback,failCallback);
    }
    apiRedeem(packet_id){
        this.setState({
            showModalConfirmMoonstone: false,
            showModalConfirmSoulstone: false,
            showLoading: true
        });
        let self = this;
        let dataSend={
            type : "accept_character",
            package_id: packet_id,
        };
        let successCallback = (data)=>{
            if(packet_id === 1 ){
                this.setState({
                   permission: true,
                   showLoading: false,
                   showModalReceiveMoonstone: true,
                   ...data.content
               });
            }
            if(packet_id === 2) {
                this.setState({
                   permission: true,
                   showLoading: false,
                   showModalReceiveSoulstone: true,
                   ...data.content
               });
            }

        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                    showLoading: false
                });
            } else {
                this.setState({
                    showModalMessage: data.message,
                    showLoading: false,
                });
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM",this.props.jwtToken,dataSend,successCallback,failCallback);
    }
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,

            can_receive: false,
            character: "",
            monster_kill: 0,
            received: false,

            showModalMessage: "",
            showModalSelectCharacter: false,
            showModalConfirmCharacter: false,
                character_id: -1,
                character_name: "",
            showModalConfirmMoonstone: false,
            showModalReceiveMoonstone: false,
            showModalConfirmSoulstone: false,
            showModalReceiveSoulstone: false,
            showModalReceiveExchangeMessage: "",

            canClick: true,
            canClickExchange: true,
            scollDown: false,
        }
    }
    componentDidMount(){
        console.log(this.props.jwtToken);
        if(this.props.jwtToken !== "") setTimeout(()=>this.apiEventInfo(),600);
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken){
            setTimeout(()=>{
                this.apiEventInfo()
            },600);
        }
    }

    render() {
        return (
            <div >
                <div className="f11layout">
                    <div className="f11layout__name">{this.state.character}</div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                    <div className={"f11layout__scroll"+(this.state.scollDown? " hide":"")} ><img src={iconScroll} alt="" /></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">

                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="moonstone_festival">
                                <section className="moonstone_festival__section moonstone_festival__section--head">
                                </section>
                                <section className="moonstone_festival__section moonstone_festival__section--condition">
                                    <div className="condition">
                                        <div className="condition__inner">
                                            <div className="condition__head"></div>
                                            <ul className="condition__text">
                                                <li>ระยะเวลากิจกรรม 14 ธ.ค. 2561 (12:00 น.) - 20 ธ.ค. 2561 (23:59 น.)</li>
                                                <li>ผู้เล่นเลือกตัวละครที่จะเข้าร่วมกิจกรรม <span>และไม่สามารถเปลี่ยนแปลงตัวละครภายหลังได้</span></li>
                                                <li>ผู้เล่นกำจัดมอนเตอร์ 3000 ตัว จะได้รับสิทธิ์ในแลก หินโซล "หรือ" หินจันทรา 1 สิทธิ์</li>
                                                <li>รีเซ็ทปุ่มและจำนวนการกำจัดมอนเตอร์ เวลา 00:00 น. ของทุกวัน <span>(ควรเผื่อเวลาในการอัพเดทคะแนน)</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                                <section className="moonstone_festival__section moonstone_festival__section--exchange">
                                    <div className="exchange">
                                        <div className="exchange__set">
                                            <div className="exchange__head exchange__head--monster"></div>
                                            <div className="exchange__text">{this.numberWithCommas(this.state.monster_kill)}</div>
                                        </div>
                                        <div className="exchange__set exchange__set--moonstone">
                                            <div className="exchange__head exchange__head--moonstone"></div>
                                            <div className="exchange__image"><img src={iconMoonstone} alt="" /></div>
                                            {!this.state.can_receive && !this.state.received &&
                                                <div className="exchange__button exchange__button--inactive" />
                                            }
                                            {this.state.can_receive && !this.state.received &&
                                                <a
                                                    className="exchange__button exchange__button--active"
                                                    onClick={()=>this.setState({ showModalConfirmMoonstone: true, canClick: false})}
                                                />
                                            }
                                            {this.state.received &&
                                                <div className="exchange__button exchange__button--received" />
                                            }
                                        </div>
                                        <div className="exchange__set exchange__set--soulstone">
                                            <div className="exchange__head exchange__head--soulstone"></div>
                                            <div className="exchange__image"><img src={iconSoulstone} alt="" /></div>

                                            {!this.state.can_receive && !this.state.received &&
                                                <div className="exchange__button exchange__button--inactive" />
                                            }
                                            {this.state.can_receive && !this.state.received &&
                                                <a
                                                    className="exchange__button exchange__button--active"
                                                    onClick={()=>this.setState({ showModalConfirmSoulstone: true, canClick: false})}
                                                />
                                            }
                                            {this.state.received &&
                                                <div className="exchange__button exchange__button--received" />
                                            }
                                        </div>
                                    </div>
                                </section>

                            </div>
                        </ScrollArea>
                    </div>
                    :
                    <Permission />
                }


                {/* Character */}
                <ModalSelectCharacter
                    open={this.state.showModalSelectCharacter}
                    listCharater={this.state.listCharater|| []}
                    actSelectCharacter={(character_id,character_name)=>this.setState({
                        showModalSelectCharacter: false,
                        showModalConfirmCharacter: true,
                        character_id: character_id,
                        character_name: character_name
                    })}
                />
                <ModalConfirme
                    open={this.state.showModalConfirmCharacter}
                    actConfirm={()=>this.actSelectCharacter(this.state.character_id)}
                    actClose={()=>this.setState({
                        showModalSelectCharacter: true,
                        showModalConfirmCharacter: false
                    })}
                    title={"ยืนยัน"}
                    msg={'ท่านเลือก <span>"'+this.state.character_name +'"</span> ?'}
                />
                {/* Moonstone */}
                <ModalConfirme
                    open={this.state.showModalConfirmMoonstone}
                    actConfirm={()=>this.actExchangeMoonstone()}
                    actClose={()=>this.setState({showModalConfirmMoonstone: false, canClick: true})}
                    title={"ยืนยัน"}
                    msg={"ต้องการแลก หินจันทรา ?"}
                />
                <ModalReceiveItem
                    open={this.state.showModalReceiveMoonstone}
                    actClose={()=>this.setState({showModalReceiveMoonstone: false, canClick: true})}
                    title={""}
                    msg={"หินจันทรา x 24"}
                />
                {/* Soulstone */}
                <ModalConfirme
                    open={this.state.showModalConfirmSoulstone}
                    actConfirm={()=>this.actExchangeSoulstone()}
                    actClose={()=>this.setState({showModalConfirmSoulstone: false, canClick: true})}
                    title={"ยืนยัน"}
                    msg={"ต้องการแลก หินโซล ?"}
                />
                <ModalReceiveItem
                    open={this.state.showModalReceiveSoulstone}
                    actClose={()=>this.setState({showModalReceiveSoulstone: false, canClick: true})}
                    title={""}
                    msg={"หินโซล x 100"}
                />

                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
                <Loading
                    open={this.state.showLoading}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
    onAccountLogout: () => {
        dispatch(onAccountLogout());
    }
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
