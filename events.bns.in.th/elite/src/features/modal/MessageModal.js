import React from 'react';
import  ModalCore from './ModalCore.js';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';
import { imgList } from '../../constants/Import_Images';
const MessageModal = (props) => {
	return (
		<ModalCore name="message" outSideClick={false}>
			<div className="contentwrap contentwrap--message">
				<img className="label-top label-top--message" src={imgList.label_error} alt=''/>
				<div className="inner inner--message">
					<div className="text text--message" dangerouslySetInnerHTML={{ __html: props.modal_message }}>
					</div>
				</div>
				<div className="btn-group btn-group--big">
                    <div className="agree agree--big" onClick={() => props.setValues({ modal_open: '' })}/>
                </div>
			</div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageModal);
