import React from 'react';
import  ModalCore from './ModalCore.js';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';
import { imgList } from '../../constants/Import_Images';
import { apiPost } from './../../middlewares/Api';

const UidModal = (props) => {

	const getNameUid = ()=>{
		props.setValues({'modal_open':'loading'})
		let dataSend = {
            type:'check_uid',
            uid:document.getElementById('uid_send_item').value,
        };
		let successCallback = (data) => {

			let msg_confirm ="ต้องการส่ง <br/>"+props.excusive_item_prev[props.select_shop_index].product_title+" <br/>ให้ "+data.data.target_username+" ใช่หรือไม่?"
			props.setValues({...data.data,'modal_open':'confirm','modal_message':msg_confirm})
		};
		const failCallback = (data) => {
			props.setValues({'modal_open':'message','target_uid':-1,'target_username':"",'modal_message':data.message})
		};

        apiPost("REACT_APP_API_POST_CHECK_UID", props.jwtToken, dataSend, successCallback, failCallback);
	}

	return (
		<ModalCore name="uid" outSideClick={true}>
			<div className="contentwrap contentwrap--message">
				<img className="label-top label-top--message" src={imgList.label_sent} alt=''/>
				<div className="inner inner--message">
					<div className="text text--message">
                        <input type="text" id="uid_send_item" className="input" placeholder="กรอก UID เพื่อน"/>
					</div>
				</div>
				<div className="btn-group btn-group--big">
                    <div className="agree agree--big" onClick={() => getNameUid()}/>
                </div>
			</div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UidModal);
