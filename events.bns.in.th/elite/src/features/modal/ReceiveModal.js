import React from "react";
import { connect } from "react-redux";
import ModalCore from "./ModalCore.js";
import './styles.scss';
import { setValues, actCloseModal } from "./redux";
import { imgList } from '../../constants/Import_Images';
import { ItemFree } from '../../features/itemfree';
import { ItemShop } from '../../features/itemshop';
const ReceiveModal = (props) => {
	return (
    <ModalCore name="receive" outSideClick={true}>
      	<div className="contentwrap contentwrap--reward">
				<img className="label-top label-top--reward" src={imgList.label_error} alt=''/>
				<div className="inner inner--reward">
					{/* <div className="text text--message" dangerouslySetInnerHTML={{ __html: props.modal_message }}>
                    </div> */}
                    <div className="text text--receive">
                        ได้รับ
                    </div>
					{props.item_get && props.item_get.length>0 && props.item_get.map((item, key) => {
						return (
							<ItemFree
								key={"item_get_"+key}
								number={item.amt}
								text={item.product_title}
								icon={item.icon}
							/>
						);
					})}

					<div className="text text--receive" dangerouslySetInnerHTML={{ __html: props.modal_message }} />
				</div>
				<div className="btn-group btn-group--middle">
                    <div className="agree agree--middle" onClick={() => props.setValues({ modal_open: '' })}/>
                </div>
			</div>
    </ModalCore>
	)
}

const mapStateToProps = state => ({
  ...state.ModalReducer,
  ...state.Modal
});

const mapDispatchToProps = { setValues, actCloseModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReceiveModal);
