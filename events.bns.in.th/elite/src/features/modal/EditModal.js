import React from 'react';
import  ModalCore from './ModalCore.js';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';
import { apiPost } from './../../middlewares/Api';

const EditModal = (props) => {
    let day = Array(31).fill();
    let year = Array(100).fill();
    let cur_date = new Date()
    let year_begin = cur_date.getFullYear();

    let birthday_split=props.profile.birthday.split("-")

    const submitForm = ()=>{
        props.setValues({'modal_open':'loading'})
		let dataSend = {
            type:'update_member',
            firstname:props.profile.firstname,
            lastname:props.profile.lastname,
            nickname:props.profile.nickname,
            tel:props.profile.tel,
            email:props.profile.email,
            birthday:props.b_year_sel.concat("-",props.b_month_sel,"-",props.b_day_sel),
        };
		let successCallback = (data) => {
			props.setValues({...data.data,'modal_open':''})
		};
		const failCallback = (data) => {
            let old_profile = Object.assign({}, props.default_profile);
			props.setValues({'modal_open':'message','modal_message':data.message,profile:old_profile})
		};

        apiPost("REACT_APP_API_POST_UPDATE_MEMBER", props.jwtToken, dataSend, successCallback, failCallback);

    }

	return (
		<ModalCore name="edit" outSideClick={false}>
			<div className="contentwrap contentwrap--edit">
                <div className="inner inner--edit">
                    <input type="text" className="input" value={props.profile.uid} placeholder="UID" disabled={true}/>
                    <input type="text" className="input" value={props.profile.firstname} placeholder="ชื่อ" maxLength="64" onChange={(e) => props.setValues({profile: {...props.profile,firstname:e.target.value}})}/>
                    <input type="text" className="input" value={props.profile.lastname} placeholder="นามสกุล" maxLength="64" onChange={(e) => props.setValues({profile: {...props.profile,lastname:e.target.value}})}/>
                    <input type="text" className="input" value={props.profile.nickname} placeholder="ชื่อที่ต้องการให้แสดงบนเว็บ" maxLength="64" onChange={(e) => props.setValues({profile: {...props.profile,nickname:e.target.value}})}/>
                    <input type="text" className="input" value={props.profile.tel} placeholder="เบอร์โทรศัพท์" maxLength="10" onChange={(e) => props.setValues({profile: {...props.profile,tel:e.target.value}})}/>
                    <input type="text" className="input" value={props.profile.email} placeholder="Email" maxLength="64" onChange={(e) => props.setValues({profile: {...props.profile,email:e.target.value}})}/>
                    <div className="select">
                        <div>
                            วันเกิด ว/ด/ป
                        </div>
                        <div>
                            <select name="b_day_sel" id="b_day_sel" onChange={(e) => props.setValues({b_day_sel:e.target.value})} disabled={birthday_split[2]=="00" ? false :true}>
                                <option value="">วัน</option>
                                {
                                    birthday_split[2]=="00"
                                    ?
                                        day.map((item, key) => {
                                            return (
                                                <option value={('0' + (key+1)).slice(-2)} key={"day_"+key}>{('0' + (key+1)).slice(-2)}</option>
                                            );
                                        })
                                    :
                                        <option value={birthday_split[2]} key={"day_"+birthday_split[2]} selected>{birthday_split[2]}</option>
                                }

                            </select>
                            <select name="b_month_sel" id="b_month_sel" onChange={(e) => props.setValues({b_month_sel:e.target.value})} disabled={birthday_split[2]=="00" ? false :true}>
                                <option value="">เดือน</option>
                                {
                                    birthday_split[2]=="00"
                                    ?
                                        props.month_date.map((item, key) => {
                                            return (
                                                <option value={('0' + item.month_no).slice(-2)} key={"month_"+key}>{('0' + item.month_no).slice(-2)}</option>
                                            );
                                        })
                                    :
                                        <option value={birthday_split[1]} key={"month_"+birthday_split[1]} selected>{birthday_split[1]}</option>
                                }
                            </select>

                            <select name="b_year_sel" id="b_year_sel" onChange={(e) => props.setValues({b_year_sel:e.target.value})} disabled={birthday_split[2]=="00" ? false :true}>
                                <option value="">ปี</option>
                                {
                                    birthday_split[2]=="00"
                                    ?
                                        year.map((item, key) => {
                                            var show_year = year_begin-key
                                            return (
                                                <option value={show_year} key={"year_"+key} >{show_year}</option>
                                            )
                                        })
                                    :
                                        <option value={birthday_split[0]} key={"year_"+birthday_split[0]} selected >{birthday_split[0]}</option>
                            }
                            </select>
                        </div>
                    </div>
                </div>
                <div className="btn-group">
                    <div className="ok" onClick={() => submitForm()}/>
                    <div className="notok" onClick={() => props.setValues({ modal_open: '',profile:props.default_profile })}/>
                </div>
			</div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.AccountReducer,
    ...state.layout,
    ...state.EventReducer,
    ...state.Modal,
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditModal);
