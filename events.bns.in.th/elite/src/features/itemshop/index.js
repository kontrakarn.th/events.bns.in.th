import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { imgList } from '../../constants/Import_Images';
import { setValues } from "../../features/redux";

class ItemShop extends React.Component {

    constructor(props){
        super(props);
    }
    render() {
        return (
            <ItemShopContent className="itemshop">
                <div className="itemshop__item-img">
                    <img src={this.props.icon} alt=''/>
                </div>
                <div className="itemshop__text">
                    {this.props.text}
                </div>
                {this.props.page==='shop' &&
                    <>
                        {
                            this.props.show_only==false
                            ?
                                <div className="itemshop__price">
                                    <span>{this.props.price.toLocaleString()}</span>
                                    <img src={imgList.diamond} alt=''/>
                                </div>
                            :
                                null
                        }

                        {
                            this.props.show_only==false
                            ?
                                this.props.profile.vip==3
                                ?
                                    this.props.diamond>=this.props.price
                                    ?
                                        <div className="itemshop__btn-buy" onClick={()=>this.props.buy(this.props.itemId,this.props.item_key)}/>
                                    :
                                        <div className="itemshop__btn-buy disable" />
                                :
                                    <div className="itemshop__btn-buy disable" />
                            :
                                null
                        }

                    </>
                }
            </ItemShopContent>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = {
    setValues
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ItemShop)

const ItemShopContent = styled.div`
    width: 20%;
    display: inline-block;
    vertical-align: top;
    margin: 2% 2.5%;
    .itemshop{
        &__item-img{
            position: relative;
            width: 75%;
            margin: 0 auto;
        }
        &__text{
            color: #aa9b7c;
            font-size: .8em;
            margin-top: 5%;
            // min-height: 3em;
            @media(max-width: 1024px) {
                font-size: .7em;
            }
            @media(max-width: 750px) {
                font-size: 1.3vw;
            }
            @media(max-width: 640px) {
                font-size: 2vw;
                // min-height: 6vw;
            }
        }
        &__btn-buy{
            margin: 10% auto;
            width: 75%;
            padding-bottom: calc(75%*(28/83));
            background: url(${imgList.btn_buy});
            background-size: 100% 200%;
            cursor: pointer;
            &:hover{
                background-size: 100% 200%;
                background-position: bottom;
            }
        }
        &__price{
            >span{
                color: #aa9b7c;
                font-size: .8em;
                display: inline-block;
                vertical-align: middle;
                @media(max-width: 1024px) {
                    font-size: .7em;
                }
                @media(max-width: 750px) {
                    font-size: 1.3vw;
                }
                @media(max-width: 640px) {
                    font-size: 2vw;
                }
            }
            >img{
                width: 15%;
                margin-left: 5%;
                display: inline-block;
                vertical-align: middle;
            }
        }
    }
`;
