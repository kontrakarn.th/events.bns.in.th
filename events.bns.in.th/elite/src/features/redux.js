// import axios from "axios";
const initialState = {
    current_page: "",
    current_play:"",
    month: 0,
    previous_month: 0,
    daily_item: [],
    weekly_item: [],
    birthday_item: [],
    daily_item_prev: [],
    weekly_item_prev: [],
    birthday_item_prev: [],
    excusive_item_prev: [],
    excusive_item: [],
    ranking_3_item: [],
    ranking_10_item: [],
    vip_3_item: [],
    diamond:0,
    profile: {
        uid: 0,
        firstname: "",
        lastname: "",
        nickname: "",
        tel: 0,
        email: "",
        birthday: "0000-00-00",
        vip: 0,
        vip_next: 0
    },
    default_profile: {
        uid: 0,
        firstname: "",
        lastname: "",
        nickname: "",
        tel: 0,
        email: "",
        birthday: "0000-00-00",
        vip: 0,
        vip_next: 0
    },
    b_day_sel:"00",
    b_month_sel:"00",
    b_year_sel:"0000",
    daily: {
        open: false,
        played: true
    },
    birthday: {
        open: false,
        played: true
    },
    weekly: {
        open: false,
        played: true
    },
    excusive: {
        open: false,
    },
    collect_time_start: "",
    collect_time_end: "",
    remain_diamond: 0,
    percent: 0,
    month_list:{
        1:"มกราคม",
        2:"กุมภาพันธ์",
        3:"มีนาคม",
        4:"เมษายน",
        5:"พฤษภาคม",
        6:"มิถุนายน",
        7:"กรกฎาคม",
        8:"สิงหาคม",
        9:"กันยายน",
        10:"ตุลาคม",
        11:"พฤศจิกายน",
        12:"ธันวาคม",
    },
    month_date:[
        {
            month_no:1,
            name:"มกราคม",
            num_date:31
        },
        {
            month_no:2,
            name:"กุมภาพันธ์",
            num_date:29
        },
        {
            month_no:3,
            name:"มีนาคม",
            num_date:31
        },
        {
            month_no:4,
            name:"เมษายน",
            num_date:30
        },
        {
            month_no:5,
            name:"พฤษภาคม",
            num_date:31
        },
        {
            month_no:6,
            name:"มิถุนายน",
            num_date:30
        },
        {
            month_no:7,
            name:"กรกฎาคม",
            num_date:31
        },
        {
            month_no:8,
            name:"สิงหาคม",
            num_date:31
        },
        {
            month_no:9,
            name:"กันยายน",
            num_date:30
        },
        {
            month_no:10,
            name:"ตุลาคม",
            num_date:31
        },
        {
            month_no:11,
            name:"พฤศจิกายน",
            num_date:30
        },
        {
            month_no:12,
            name:"ธันวาคม",
            num_date:31
        },
    ],
    vip_list:{
        0:"-",
        1:"Guardian",
        2:"Legend",
        3:"Divine",
    },
    item_get: [],
    ranking:[],
    select_shop_id:-1,
    select_shop_index:-1,
    target_uid:-1,
    target_username:"",

};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return { ...state, ...action.value };
      } else {
        return state;
      }
    case "SET_VALUES":
        return { ...state, ...action.value };
    default:
      return state;
  }
};

export const setValue  = (key, value) =>{ return ({ type: "SET_VALUE", value, key }) };
export const setValues = (value) =>{ return ({ type: "SET_VALUES", value }) };
