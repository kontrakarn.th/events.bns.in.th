import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import 'whatwg-fetch';
import {imgList} from '../../constants/Import_Images';
import { onAccountLogout } from './../../actions/AccountActions';

class Navbar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showMobile: false
        }
    }

    onLogout() {
        this.props.onAccountLogout();
    }
    handleMobile(){
        this.setState({
            showMobile: !this.state.showMobile
        })
    }
    render() {
        let menuList = [
            {
                name: 'Exclusive Shop',
                link: process.env.REACT_APP_EVENT_PATH+"#shop",
                type: "href",
            },
            {
                name: 'สิทธิประโยชน์ Elite',
                link: process.env.REACT_APP_EVENT_PATH+"#detail",
                type: "href",
            },
            {
                name: 'อันดับ Elite',
                link: process.env.REACT_APP_EVENT_PATH+"/rank",
                type: "link",
            },
            // {
            //     name: 'บัญชี',
            //     link: process.env.REACT_APP_EVENT_PATH+"/account",
            //     type: "link",
            // }
        ]
        return (
            <NavContent className="navbar">
                <div className="navbar__inner">
                    <div className="navbar__logo">
                        <Link to={process.env.REACT_APP_EVENT_PATH}><img src={imgList.logo} alt=''/></Link>
                    </div>
                    <div className="navbar__menu">
                        <ul>
                            {menuList.map((item, key) => {
                                return (
                                    <li key={key}>
                                        {
                                            item.type=="href"
                                            ?
                                                <a href={item.link} className={(this.props.page===item.name?' active':'')+(item.name==='Exclusive Shop'?' text--pink':'')}>{item.name}</a>
                                            :
                                                <Link to={item.link} className={(this.props.page===item.name?' active':'')+(item.name==='Exclusive Shop'?' text--pink':'')}>{item.name}</Link>
                                        }

                                    </li>
                                );
                            })}
                            {
                                this.props.jwtToken && this.props.jwtToken!=undefined && this.props.jwtToken!=""
                                ?
                                    <>
                                        <li>
                                            <Link to={process.env.REACT_APP_EVENT_PATH+"/account"} className={(this.props.page==="บัญชี"?' active':'')}>บัญชี</Link>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" onClick={()=>this.onLogout()}>ออกจากระบบ</a>
                                        </li>
                                    </>
                                :
                                    <li>
                                        <a href={this.props.loginUrl}>เข้าสู่ระบบ</a>
                                    </li>
                            }
                        </ul>
                        <div className="navbar__btn-menu" >
                            <img src={imgList.btn_menu} onClick={()=>this.handleMobile()} alt=''/>
                        </div>
                    </div>
                    <div className={"navbar__backdrop" + (this.state.showMobile?' open':'')}/>
                    <div className={"navbar__menu-mobile" + (this.state.showMobile?' open':'')}>
                        <div className="navbar__btn-menu-close" onClick={()=>this.handleMobile()}>
                            <img src={imgList.btn_close} alt=''/>
                        </div>
                        <ul>
                            {menuList.map((item, key) => {
                                return (
                                    <li key={key}>
                                        {
                                            item.type=="href"
                                            ?
                                                <a href={item.link} className={item.name==='Exclusive Shop'?'text--pink':''}>{item.name}</a>
                                            :
                                                <Link to={item.link} className={item.name==='Exclusive Shop'?'text--pink':''}>{item.name}</Link>
                                        }

                                    </li>
                                );
                            })}
                            {
                                this.props.jwtToken && this.props.jwtToken!=undefined && this.props.jwtToken!=""
                                ?
                                    <>
                                        <li>
                                            <Link to={process.env.REACT_APP_EVENT_PATH+"/account"} className={(this.props.page==="บัญชี"?' active':'')}>บัญชี</Link>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" onClick={()=>this.onLogout()}>ออกจากระบบ</a>
                                        </li>
                                    </>
                                :
                                    <li>
                                        <a href={this.props.loginUrl}>เข้าสู่ระบบ</a>
                                    </li>
                            }

                        </ul>
                    </div>
                </div>
            </NavContent>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = {
    onAccountLogout
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Navbar)

const NavContent = styled.div`
    position: fixed;
    width: 100%;
    font-family: 'Kanit-ExtraLight', tahoma;
    background: #000;
    z-index: 100;
    .navbar{
        &__inner{
            width: 100%;
            max-width: 1105px;
            margin: 0 auto;
            display: flex;
            align-items: center;
            justify-content: space-between;
            height: 120px;
            padding-right: 2%;
            padding-left: 0;
            box-sizing: border-box;
            @media(max-width: 991px) {
                height: 85px;
            }
        }
        &__logo{
            width: 22%;
            text-align: center;
            >img{
                width: 70%;
            }
            @media(max-width: 640px) {
                width: 125px;
                >img{
                    width: 100%;
                }
            }
        }
        &__menu{
            width: 57%;
            >ul{
                list-style: none;
                display: flex;
                align-items: center;
                justify-content: space-between;
                >li{
                    >a{
                        color: #fff;
                        &:hover{
                            font-family: 'Kanit-Light', 'tahoma';
                        }
                        &.active{
                            color: #3a3a3a;
                        }
                    }
                }
                @media(max-width: 640px) {
                    display: none;
                }
            }
            @media(max-width: 991px) {
                width: 80%;
            }
        }
        &__btn-menu{
            display: none;
            text-align: right;
            padding-right: 5%;
            >img{
                cursor: pointer;
            }
            @media(max-width: 640px) {
                display: block;
            }
        }
        &__backdrop{
            display: none;
            background-color: rgba(0, 0, 0, 0.7);
            position: absolute;
            left: 0px;
            right: 0px;
            width: 100%;
            height: 100vh;
            bottom: 0px;
            top: 0px;
            z-index: 200;
            @media(max-width: 640px) {
                &.open{
                    display: block;
                }
            }
        }
        &__menu-mobile{
            position: absolute;
            top: 0;
            left: -100%;
            height: 100vh;
            width: 90%;
            z-index: 999;
            background: #000;
            transition: all 0.3s ease 0s;
            >ul{
                list-style: none;
                padding-left: 10%;
                padding-top: 20%;
                >li{
                    margin: 3% 0;
                    >a{
                        padding: 1% 0;
                        color: #fff;
                    }
                }
            }
            @media(max-width: 640px) {
                &.open{
                    left: 0;
                }
            }
        }
        &__btn-menu-close{
            position: absolute;
            top: 5%;
            right: 5%;
            width: 7%;
            cursor: pointer;
        }
    }
`;
