import React, { Component } from 'react';
import styled from 'styled-components';
import $ from 'jquery';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';

import { apiPost } from './../../middlewares/Api';
import { onAccountLogout } from './../../actions/AccountActions';

import { setValues } from "../../features/redux";
import Navbar from '../../features/navbar/' ;

import { imgList } from '../../constants/Import_Images';
import { Detail } from '../../features/detail';
import { Privilege } from '../../features/privilege';
import Rewards from './../../features/rewards';
import Shop from '../../features/shop';
import ShopShow from '../../features/shopshow';
import Vip3 from '../../features/vip3';
import TopTen from '../../features/topten';
import TopThree from '../../features/topthree';
import { ConfirmModal, EditModal, MessageModal, LoadingModal, ReceiveModal, SentModal, UidModal } from "../../features/modal";

class Main extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeScroll: false
        }
        this.handleScroll = this.handleScroll.bind(this)
    }

    onLogout() {
        this.props.onAccountLogout();
    }
    handlebuy(id,index){
        console.log('clicktobuyitem',index);
        this.props.setValues({
            modal_open: 'sent',
            select_shop_id:id,
            select_shop_index:index,
        })
    }

    sendToOther(){
		this.props.setValues({'modal_open':'loading'})
		let dataSend = {
            type:'buy_excusive_other',
            id:this.props.select_shop_id,
            target_uid:this.props.target_uid,
        };
		let successCallback = (data) => {
			this.props.setValues({...data.data,'modal_open':'message','modal_message':data.message})
		};
		const failCallback = (data) => {
			this.props.setValues({'modal_open':'message','modal_message':data.message})
		};

        apiPost("REACT_APP_API_POST_BUY_OTHER", this.props.jwtToken, dataSend, successCallback, failCallback);
	}

    handleScroll(){
        let window = document.documentElement.scrollTop;
        let banner = document.getElementById('banner').clientHeight-200;
        let detail = document.getElementById('detail').clientHeight+700;
        let reward = document.getElementById('reward').clientHeight-700;
        if(window>=banner){
            document.getElementById("float").classList.add('active');
            if(this.state.activeScroll!=='detail'){
                this.setState({
                    activeScroll: 'detail'
                })
            }
        }
        else{
            document.getElementById("float").classList.remove('active');
            if(this.state.activeScroll!==false){
                this.setState({
                    activeScroll: false
                })
            }
        }
        if(window>=banner+detail){
            if(this.state.activeScroll!=='reward'){
                this.setState({
                    activeScroll: 'reward'
                })

            }
        }
        if(window>=banner+detail+reward){
            if(this.state.activeScroll!=='shop'){
                this.setState({
                    activeScroll: 'shop'
                })

            }
        }
    }
    handleScrollTo(index){
        if(index==='reward'){
            $('html, body').animate({scrollTop: $('#'+index).offset().top +500 }, 'slow');
        }
        else
        {
            $('html, body').animate({scrollTop: $('#'+index).offset().top -200 }, 'slow');
        }
        this.setState({
            activeScroll: index
        })
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    render() {
        return (
            <div className="events-bns">
                <MainContent className="main">
                    <Navbar/>
                    <div className="main__float" id="float">
                        <div>
                            <img src={imgList.line_float}/>
                            <div className={"main__menu-float" + (this.state.activeScroll==='detail'?' active':'')} onClick={()=>this.handleScrollTo('detail')}>
                                TOP
                            </div>
                            {
                                this.props.jwtToken && this.props.jwtToken!=undefined && this.props.jwtToken!=""
                                ?
                                    <Link to={process.env.REACT_APP_EVENT_PATH+"/account"} className="main__menu-float text--orange">
                                        กดรับรางวัล
                                    </Link>
                                :
                                    null
                            }


                            <div className={"main__menu-float" + (this.state.activeScroll==='reward'?' active':'')} onClick={()=>this.handleScrollTo('reward')}>
                                รายละเอียดของรางวัล
                            </div>
                            <div className={"main__menu-float" + (this.state.activeScroll==='shop'?' active':'')} onClick={()=>this.handleScrollTo('shop')}>
                                Exclusive Shop
                            </div>
                        </div>
                    </div>
                    <div className="main__banner" id="banner">
                        <picture>
                            <source media="(max-width: 640px)" srcSet={imgList.banner_mb}/>
                            <img src={imgList.banner} alt=''/>
                        </picture>
                        <div className="main__scroll">
                            <img src={imgList.iconScroll} alt=""/>
                        </div>
                    </div>
                    <div className="main__detail" id="detail">
                        <Detail/>
                    </div>
                    <div className="main__reward" id="reward">
                        <Privilege/>
                        <Rewards/>
                    </div>
                    <div className="main__exclusive" id="shop">
                        <div className="main__inner">
                            <img className="main__label" src={imgList.label_shop}/>
                            {
                                this.props.profile.vip==3
                                ?
                                    <>
                                        <Shop
                                            handlebuy={(id,index)=>this.handlebuy(id,index)}
                                            show_only={false}
                                        />
                                        <br/>
                                    </>
                                :
                                    null
                            }

                            <ShopShow
                                show_only={true}
                            />
                            <Vip3/>
                            <TopTen/>
                            <TopThree/>
                        </div>
                    </div>
                </MainContent>
                <LoadingModal/>
                <MessageModal/>
                <EditModal/>
                <ConfirmModal handleConfrim={()=>this.sendToOther()}/>
                <ReceiveModal/>
                <SentModal/>
                <UidModal/>
            </div>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = {
    onAccountLogout,
    setValues
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main)

const MainContent = styled.div`
    width: 100%;
    .main{
        &__banner{
            width: 100%;
        }
        &__scroll {
            text-align: center;
            width:100%;
            position: absolute;
            top: 47vw;
            >img{
                animation: mover 1s infinite alternate;
            }
            @media(max-width: 640px) {
                display: none;
            }
        }
        &__detail{
            background: #000;
            color: #aa9b7c;
            margin-top: 2%;
        }
        &__reward{
            background: url(${imgList.bg_reward}) no-repeat top center #060606;
            background-size: cover;
            position: relative;
            padding-top: 4%;
            @media(max-width: 640px) {
                padding-top: 13%;
            }
        }
        &__exclusive{
            background: url(${imgList.bg_exclusive}) no-repeat top center #060606;
            background-size: cover;
            padding-bottom: 13%;
        }
        &__inner{
            width: 90%;
            max-width: 802px;
            margin: 0 auto;
            text-align: center;
            @media(max-width: 640px) {
                width: 100%;
            }
        }
        &__label{
            width: 49%;
            margin-top: 16%;
        }
        &__float{
            z-index: 50;
            position: fixed;
            top: 50%;
            right: 79%;
            display: none;
            -webkit-transform: translate3d(0px, -50%, 0px);
            transform: translate3d(0px, -50%, 0px);
            width: 200px;
            @media(max-width: 1105px) {
                left: 0;
            }
            @media(max-width: 1100px) {
                display: none !important;
            }
            &.active{
                display: block;
            }
            >div{
                position: relative;
                >img{
                    position: absolute;
                    width: .6%;
                    height: 86%;
                    top: 8%;
                    left: 7%;
                    z-index: -1;
                }
            }
        }
        &__menu-float{
            font-size: .8em;
            margin: 5% 0;
            color: #58595b;
            cursor: pointer;
            &::before{
                content: "";
                background: no-repeat center center url(${imgList.dot});
                display: inline-block;
                width: 30px;
                height: 30px;
                margin-right: 10px;
                vertical-align: middle;
            }
            &.active{
                color: #fff;
                &::before{
                    content: "";
                    background: no-repeat center center url(${imgList.dot_active});
                    display: inline-block;
                    width: 30px;
                    height: 30px;
                    margin-right: 10px;
                    vertical-align: middle;
                }
            }
        }
    }
`;
