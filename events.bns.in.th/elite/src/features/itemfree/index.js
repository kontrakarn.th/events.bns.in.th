import React from 'react';
import styled from 'styled-components';
import { imgList } from '../../constants/Import_Images';
export class ItemFree extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <ItemFreeContent className="itemfree">
                <div className="itemfree__item-img">
                    <img src={this.props.icon} alt=''/>
                    <div className="itemfree__number">
                        <img src={imgList.circle} alt=''/>
                        <span>{this.props.number}</span>
                    </div>
                </div>
                <div className="itemfree__text">
                    {this.props.text}
                </div>
            </ItemFreeContent>
        )
    }
}

const ItemFreeContent = styled.div`
    width: 20%;
    display: inline-block;
    vertical-align: top;
    margin: 2% 0;
    .itemfree{
        &__item-img{
            position: relative;
            width: 75%;
            margin: 0 auto;
        }
        &__text{
            color: #aa9b7c;
            font-size: .8em;
            margin-top: 5%;
            @media(max-width: 1024px) {
                font-size: .7em;
            }
            @media(max-width: 750px) {
                font-size: 1.3vw;
            }
            @media(max-width: 640px) {
                font-size: 2vw;
            }
        }
        &__number{
            position: absolute;
            width: 30%;
            bottom:0;
            right: -8%;
            >img{
                display: block;
            }
            >span{
                position: absolute;
                left: 50%;
                top: 50%;
                transform: translate(-50%,-50%);
                color: #000;
                font-family: 'Kanit-SemiBold','tahoma';
                @media(max-width: 750px) {
                    font-size: 1.8vw;
                }
                @media(max-width: 640px) {
                    font-size: 2.4vw;
                }
            }
        }
    }
`;
