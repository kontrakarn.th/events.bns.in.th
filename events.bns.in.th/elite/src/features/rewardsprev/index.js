import React from 'react';
import { connect } from 'react-redux';
import { setValues } from "../../features/redux";
import styled from 'styled-components';
import { imgList } from '../../constants/Import_Images';
import { ItemFree } from '../../features/itemfree';
import { ConfirmModal, MessageModal, LoadingModal, ReceiveModal } from "../../features/modal";
import { apiPost } from './../../middlewares/Api';

class RewardsPrev extends React.Component {
    constructor(props){
        super(props);
    }

    handleClaim(type){
        console.log("type",type)
        if(type=="daily"){
            this.props.setValues({
                modal_open: 'confirm',
                modal_message: 'คุณยืนยันสุ่มไอเทมรายวัน?',
                current_play:type,
            })
        }else if(type=="birthday"){
            this.props.setValues({
                modal_open: 'confirm',
                modal_message: 'คุณยืนยันรับไอเทมวันเกิด?',
                current_play:type,
            })
        }else if(type=="weekly"){
            this.props.setValues({
                modal_open: 'confirm',
                modal_message: 'คุณยืนยันสุ่มไอเทมรายสัปดาห์?',
                current_play:type,
            })
        }

    }

    render() {
        return (
            <RewardsContent className="rewards" id="reward_section">
                <div className="rewards__label-top">
                    <img src={imgList.label} alt=''/>
                    <span>รายละเอียดของรางวัลเดือน {this.props.month_list[this.props.previous_month]}</span>
                </div>
                <div>
                    <img className="rewards__label" src={imgList.label_daliy} alt=''/>
                    <div className="rewards__list">
                        {this.props.daily_item_prev && this.props.daily_item_prev.length>0 && this.props.daily_item_prev.map((item, key) => {
                            return (
                                <ItemFree
                                    key={"daily_"+key}
                                    number={item.amt}
                                    text={item.product_title}
                                    icon={item.icon}
                                />
                            );
                        })}
                    </div>
                    {this.props.btn
                        ?
                            <>
                                {this.props.profile.vip<1 || this.props.daily.played==true
                                    ?
                                        <div className="rewards__btn-claim disable"/>
                                    :
                                        <div className="rewards__btn-claim" onClick={()=>this.handleClaim('daily')}/>
                                }
                                <img className="rewards__line-end active" src={imgList.line_end} alt=''/>
                            </>
                        :
                            <img className="rewards__line-end" src={imgList.line_end} alt=''/>
                    }
                </div>
                <div>
                    <img className="rewards__label" src={imgList.label_birthday} alt=''/>
                    <div className="rewards__list">
                        {this.props.birthday_item_prev && this.props.birthday_item_prev.length>0 && this.props.birthday_item_prev.map((item, key) => {
                            return (
                                <ItemFree
                                    key={"birthday_item_"+key}
                                    number={item.amt}
                                    text={item.product_title}
                                    icon={item.icon}
                                />
                            );
                        })}
                    </div>
                    {this.props.btn
                        ?
                            <>
                                {this.props.profile.vip<1 || this.props.birthday.played==true
                                    ?
                                        <div className="rewards__btn-claim disable"/>
                                    :
                                        <div className="rewards__btn-claim" onClick={()=>this.handleClaim('birthday')}/>
                                }
                                <img className="rewards__line-end active" src={imgList.line_end} alt=''/>
                            </>
                        :
                            <img className="rewards__line-end" src={imgList.line_end} alt=''/>
                    }
                </div>
                <div>
                    <img className="rewards__label week" src={imgList.label_weekly} alt=''/>
                    <div className="rewards__list">
                        {this.props.weekly_item_prev && this.props.weekly_item_prev.length>0 && this.props.weekly_item_prev.map((item, key) => {
                            return (
                                <ItemFree
                                    key={"weekly_item_"+key}
                                    number={item.amt}
                                    text={item.product_title}
                                    icon={item.icon}
                                />
                            );
                        })}
                    </div>
                    {this.props.btn
                        ?
                            <>
                                {this.props.profile.vip<2 || this.props.weekly.played==true
                                    ?
                                        <div className="rewards__btn-claim disable"/>
                                    :
                                        <div className="rewards__btn-claim" onClick={()=>this.handleClaim('weekly')}/>
                                }
                                <img className="rewards__line-end active" src={imgList.line_end} alt=''/>
                            </>
                        :
                            <img className="rewards__line-end" src={imgList.line_end} alt=''/>
                    }
                </div>

            </RewardsContent>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = {
    setValues
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RewardsPrev)

const RewardsContent = styled.div`
    position: relative;
    box-sizing: border-box;
    width: 90%;
    max-width: 802px;
    margin: 7% auto 0;
    padding: 70px 100px 50px;
    border: 1px solid #84611e;
    border-radius: 5px;
    background: #000;
    text-align: center;
    @media(max-width: 640px) {
        width: 100%;
        border-right: unset;
        border-left: unset;
        border-radius: unset;
        padding: 7% 4% 5%;
        margin: 13% auto 0;
    }
    >div{
        position: relative;
        &:first-child{
            position: absolute;
        }
        &:nth-child(3){
            margin-top: 5%;
        }
        &:nth-child(4){
            margin-top: -13%;
        }
    }
    .rewards{
        &__list{
            width: 100%;
        }
        &__reward{
            margin-top: 4%;
        }
        &__line-end{
            margin-top: 10%;
            &.active{
                margin-top: 0;
            }
        }
        &__btn-claim{
            margin: 10% auto;
            width: 18%;
            padding-bottom: calc(18%*(32/93));
            background: url(${imgList.btn_claim});
            background-size: 100% 200%;
            cursor: pointer;
            &:hover{
                background-size: 100% 200%;
                background-position: bottom;
            }
        }
        &__label{
            width: 26%;
            &.week{
                width: 100%;
                margin-left: 16%;
            }
        }
        &__label-top{
            width: 34%;
            position: absolute;
            left: 50%;
            transform: translate(-50%,0);
            top: -23px;
            >img{
                display: block;
                margin: 0 auto;
            }
            >span{
                color: #30190a;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                width: 100%;
                text-align: center;
                font-family: 'Kanit-SemiBold','tahoma';
                font-size: 1em;
            }
            @media(max-width: 640px) {
                width: 50%;
                font-size: 2.3vw;
                top: -4vw;
            }
        }
    }
`;
