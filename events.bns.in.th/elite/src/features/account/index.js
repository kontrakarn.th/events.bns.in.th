import React, { Component } from 'react';
import styled,{keyframes} from 'styled-components';
import { connect } from 'react-redux';
import 'whatwg-fetch';
import { apiPost } from './../../middlewares/Api';
import { onAccountLogout } from './../../actions/AccountActions';
import { setValues } from "../../features/redux";
import Navbar from '../../features/navbar/' ;
import RewardsPrev from '../../features/rewardsprev';
import { imgList,vip_img_list } from '../../constants/Import_Images';
import { ConfirmModal, EditModal, MessageModal, LoadingModal, ReceiveModal } from "../../features/modal";

class Account extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // percent: 10,
            percentIcon: 0,
        }
    }

    onLogout() {
        this.props.onAccountLogout();
    }
    calIcon(index){
        let cal = index;
        if(cal>89){
            cal = 89;
        }
        this.setState({
            percentIcon: cal+(-23)
        })
    }

    handleConfrim(){
        this.props.setValues({'modal_open':'loading'})
        if(this.props.current_play=="daily"){
    		let dataSend = {type:'play_daily'};
    		let successCallback = (data) => {
    			this.props.setValues({...data.data,'item_get':data.item_get,'modal_open':'receive','modal_message':data.message})
    		};
    		const failCallback = (data) => {
    			this.props.setValues({'modal_open':'message','modal_message':data.message})
    		};
			apiPost("REACT_APP_API_POST_PLAY_DAILY", this.props.jwtToken, dataSend, successCallback, failCallback);
        }else if(this.props.current_play=="birthday"){
            let dataSend = {type:'play_birthday'};
            let successCallback = (data) => {
                this.props.setValues({...data.data,'item_get':data.item_get,'modal_open':'receive','modal_message':data.message})
            };
            const failCallback = (data) => {
                this.props.setValues({'modal_open':'message','modal_message':data.message})
            };
            apiPost("REACT_APP_API_POST_PLAY_BIRTHDAY", this.props.jwtToken, dataSend, successCallback, failCallback);
        }else if(this.props.current_play=="weekly"){
            let dataSend = {type:'play_weekly'};
    		let successCallback = (data) => {
    			this.props.setValues({...data.data,'item_get':data.item_get,'modal_open':'receive','modal_message':data.message})
    		};
    		const failCallback = (data) => {
    			this.props.setValues({'modal_open':'message','modal_message':data.message})
    		};
			apiPost("REACT_APP_API_POST_PLAY_WEEKLY", this.props.jwtToken, dataSend, successCallback, failCallback);

        }
    }

    handleEdit(){
        let old_profile = Object.assign({}, this.props.default_profile);
        this.props.setValues({
            modal_open: 'edit',
            profile:old_profile
        })
    }
    componentDidMount(){
        this.calIcon(this.props.percent);
    }
    render() {
        return (
            <AccountContent className="events-bns">
                <div className="account">
                    <Navbar/>
                    <div className="account__inner">
                        <div className="account__head">
                            <div className="account__title">ข้อมูลส่วนบุคคล</div>
                            <div className="account__btn-group">
                                <a href={process.env.REACT_APP_EVENT_PATH+"/account#reward_section"}><div className="account__claim"></div></a>
                                <div className="account__edit" onClick={()=>this.handleEdit()}>
                                    <img src={imgList.btn_edit} alt=''/>
                                </div>
                            </div>
                        </div>
                        <div className="account__board">
                            <div className="account__info">
                                <div>UID : {this.props.profile.uid}</div>
                                <div>ชื่อ : {this.props.profile.firstname} {this.props.profile.lastname}</div>
                                <div>ชื่อหน้าเว็บ : {this.props.profile.nickname}</div>
                                <div>เบอร์โทรศัพท์ : {this.props.profile.tel}</div>
                                <div>วันเกิด : {this.props.profile.birthday}</div>
                                <div>สถานะ Elite ปัจจุบัน  : {this.props.vip_list[this.props.profile.vip]}</div>
                                <div>สถานะ Elite เดือนหน้า  : {this.props.vip_list[this.props.profile.vip_next]}</div>
                            </div>
                            <div className="account__rank">
                                <img src={this.props.profile.vip>0 ? vip_img_list[this.props.profile.vip] : null} alt=''/>
                            </div>
                            <img src={imgList.line_account} className="account__line" alt=''/>
                            <div className="account__text">
                                สถานะ Elite ในเดือนถัดไป
                            </div>
                            <div className="account__text account__text--small">
                                สะสมยอดในเดือนนี้เพื่อรับสิทธิประโยชน์ Elite ในเดือนถัดไป
                            </div>
                            <div className="">
                                <div className="account__progress">
                                    <span style={{ width: this.props.percent + '%' }}/>
                                    {/*<img style={{ left: this.state.percentIcon + '%' }} className="account__icon" src={imgList.loading_lyn}/>*/}
                                    <div className="account__icon" style={{ left: this.state.percentIcon + '%' }} />
                                </div>
                                <img className="account__rank-go" src={this.props.profile.vip_next==3 ? vip_img_list[this.props.profile.vip_next] : vip_img_list[this.props.profile.vip_next+1]} alt=''/>
                            </div>
                            <div className="account__text">
                                ต้องการยอดการเติมเงินสะสมอีก : {this.props.remain_diamond} Diamond<br/>
                                ระยะเวลาสะสมยอด  : {this.props.collect_time_start} - {this.props.collect_time_end}
                            </div>
                        </div>
                        <RewardsPrev
                            btn={true}
                        />
                    </div>
                </div>
                <LoadingModal/>
                <MessageModal/>
                <EditModal/>
                <ConfirmModal handleConfrim={()=>this.handleConfrim()}/>
                <ReceiveModal/>
            </AccountContent>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = {
    onAccountLogout,
    setValues
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Account)

const Loading_move_step = keyframes`
	100% { background-position: 0 -1024px;}
`


const AccountContent = styled.div`
    width: 100%;
    background: url(${imgList.bg_account}) no-repeat center top;
    background-size: cover;
    padding-bottom: 5%;
    .account{
        &__inner{
            padding-top: 13%;
            @media(max-width: 640px) {
                padding-top: 100px;
                padding-bottom: 10%;
            }
        }
        &__head{
            display: flex;
            align-items: center;
            justify-content: space-between;
            width: 90%;
            max-width: 802px;
            margin: 0 auto 1%;
            @media(max-width: 640px) {
                width: 95%;
            }
        }
        &__title{
            color: #fff;
            font-size: 1.5em;
            @media(max-width: 640px) {
                font-size: 3vw;
            }
        }
        &__btn-group{
            width: 60%;
            text-align: right;
            @media(max-width: 640px) {
                width: 75%;
            }
        }
        &__claim{
            width: 18%;
            padding-bottom: calc(18%*(32/93));
            background: url(${imgList.btn_claim});
            background-size: 100% 200%;
            display: inline-block;
            cursor: pointer;
            &:hover{
                background-size: 100% 200%;
                background-position: bottom;
            }
        }
        &__claim{
            width: 18%;
            padding-bottom: calc(18%*(32/93));
            background: url(${imgList.btn_claim});
            background-size: 100% 200%;
            display: inline-block;
            vertical-align: middle;
            cursor: pointer;
            &:hover{
                background-size: 100% 200%;
                background-position: bottom;
            }
        }
        &__edit{
            width: 19%;
            display: inline-block;
            vertical-align: middle;
            margin-left: 3%;
            cursor: pointer;
            >img{
                display: block;
            }
        }
        &__board{
            border: 1px solid #84611e;
            border-radius: 5px;
            width: 90%;
            max-width: 802px;
            margin: 0 auto;
            text-align: center;
            padding: 60px 80px 50px;
            background: #000;
            box-sizing: border-box;
            @media(max-width: 900px) {
                padding: 7% 4% 5%;
            }
            @media(max-width: 640px) {
                width: 100%;
                margin: 0 auto 25%;
                border-left: none;
                border-right: none;
            }
        }
        &__info{
            width: 50%;
            display: inline-block;
            vertical-align: middle;
            color: #fff;
            text-align: left;
            font-size: 1.2em;
            >div{
                margin: 1% 0;
            }
            @media(max-width: 640px) {
                font-size: 3vw;
                width: 60%;
            }
        }
        &__rank{
            width: 50%;
            display: inline-block;
            vertical-align: middle;
            @media(max-width: 640px) {
                width: 40%;
            }
        }
        &__line{
            width: 70%;
            margin: 5% auto;
        }
        &__text{
            color: #fff;
            font-size: 1.3em;
            &--small{
                font-size: 1em;
            }
            @media(max-width: 640px) {
                font-size: 3vw;
                &--small{
                    font-size: 2vw;
                }
            }
        }
        &__progress{
            position: relative;
            width: 75%;
            display: inline-block;
            vertical-align: middle;
            height: 20px;
            box-sizing: unset;
            border-radius: 20px;
            margin-top: 12%;
            background: gray;
            >span{
                display: block;
                width: 0;
                max-width: 100%;
                height: 100%;
                border-radius: 15px;
                background: rgba(255,119,0,1);
                background: -moz-linear-gradient(left, rgba(255,119,0,1) 0%, rgba(255,179,0,1) 44%, rgba(255,179,0,1) 100%);
                background: -webkit-gradient(left top, right top, color-stop(0%, rgba(255,119,0,1)), color-stop(44%, rgba(255,179,0,1)), color-stop(100%, rgba(255,179,0,1)));
                background: -webkit-linear-gradient(left, rgba(255,119,0,1) 0%, rgba(255,179,0,1) 44%, rgba(255,179,0,1) 100%);
                background: -o-linear-gradient(left, rgba(255,119,0,1) 0%, rgba(255,179,0,1) 44%, rgba(255,179,0,1) 100%);
                background: -ms-linear-gradient(left, rgba(255,119,0,1) 0%, rgba(255,179,0,1) 44%, rgba(255,179,0,1) 100%);
                background: linear-gradient(to right, rgba(255,119,0,1) 0%, rgba(255,179,0,1) 44%, rgba(255,179,0,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff7700', endColorstr='#ffb300', GradientType=1 );
            }
            @media(max-width: 640px) {
                height: 10px;
            }
        }
        &__rank-go{
            position: relative;
            width: 25%;
            margin-left: -10%;
            display: inline-block;
            vertical-align: middle;
        }
        &__icon{
            display: block;
            width: 150px;
            height: 64px;
    		background: no-repeat center top url(${imgList.loading_lyn});
            animation: ${Loading_move_step} .8s steps(16) infinite;

             position: absolute;
            top: -300%;
            left: -23%;
            width: 28%;
            /*max-height: unset;
            z-index: 1;
            @media(max-width: 900px) {
                top: -8vw;
            } */
        }
    }
`;
