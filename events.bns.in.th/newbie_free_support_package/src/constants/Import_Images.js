import iconHome from '../static/images/buttons/btn_home.png';
import iconScroll from '../static/images/icons/icon_scroll.png';

//home
import bg from '../static/images/bg.png';
import bg_mb from '../static/images/bg_mb.png';
import btn_received from '../static/images/btn_received.png';
import btn_receive from '../static/images/btn_receive.png';

export const Imglist = {
    iconHome,
    iconScroll,

    bg,
    bg_mb,
    btn_received,
    btn_receive,
};
