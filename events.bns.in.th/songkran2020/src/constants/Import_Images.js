const bg = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/background.png';

const slide1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/slide/1.png'
const slide2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/slide/2.png'
const slide3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/slide/3.png'
const slide4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/slide/4.png'

const modal_bg 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/modal_bg.png'
const btn_default 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/btn_default.png'

const content1 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/content1.png'
const content2 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/content2.png'
const content3 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/content3.png'
const btn_prev 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/btn_prev.png'
const btn_next 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/btn_next.png'
const bg_sidebar 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/bg_sidebar.png'
const sidebar_icon 	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/sidebar_icon.png'
const quest1			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/quest/1.png'
const quest2			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/quest/2.png'
const quest3			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/quest/3.png'
const quest4			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/quest/4.png'
const quest5			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/quest/5.png'
const quest6			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/quest/6.png'
const quest7			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/quest/7.png'
const btn_history		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/btn_history.png'
const btn_join			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/btn_join.png'
const history			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/history.png'
const tab_use_coin		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/tab_use_coin.png'
const tab_receive_coin	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/songkran/tab_receive_coin.png'

export const Imglist = {
	bg,
	modal_bg,
	btn_default,
	slide1,
	slide2,
	slide3,
	slide4,
	content1,
	content2,
	content3,
	btn_prev,
	btn_next,
	bg_sidebar,
	sidebar_icon,

	quest1, 
	quest3, 
	quest2, 
	quest4, 
	quest5, 
	quest6, 
	quest7, 
	btn_history,
	btn_join,
	history,
	tab_use_coin,
	tab_receive_coin
}
