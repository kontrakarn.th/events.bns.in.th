import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';
import iconDropdown from '../../static/images/icon_dropdown.png'
import { apiPost } from './../../middlewares/Api';


const CPN = props => {

    const actSelectCharacter=(e)=>{
        e.preventDefault();
        let indexCharacter = props.value_select;
        if(indexCharacter !== "" ){
            props.apiSelectCharacter(indexCharacter)
        }else{
            props.setValues({
                modal_open:"message",
                modal_message: "โปรดเลือกตัวละคร",
                selected_char:false,
            });
        }
    }

    function handleChange(e) {
        props.setValues({
            value_select : e.target.value,
        })
    }

    return (
        <ModalCore
            modalName="selectcharacter"
            actClickOutside={false}
        >
                <div className={"modal__content"}>
                    <div className={"modal__incontent modal__incontent--selectcharacter"}>
                        เลือกตัวละคร<br />
                        <form className="formselect">
                            <label >
                                <select id="charater_form" className="fromselect__list" onChange={(e) => handleChange(e)}>
                                    <option value="" disabled selected>เลือกตัวละคร</option>
                                    {props.characters && props.characters.map((item,index)=>{
                                        return (
                                            <option key={index} value={item.char_id}>{item.char_name}</option>
                                        )
                                    })}
                                </select>
                                <img src={iconDropdown} alt="" />
                            </label>
                        </form>
                    </div>
                    <div className={"modal__bottom"}>
                        <a
                            onClick={(e)=>actSelectCharacter(e)}
                            className="modal__button"
                        >
                            ตกลง
                        </a>
                    </div>
                </div>
        </ModalCore>
    )
}


const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
