import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import {Imglist} from './../../constants/Import_Images';
import ModalConfirmReceive from '../modals/ModalConfirmReceive';
import ModalSelectCharacter from "../modals/ModalSelectCharacter";

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div className={className} onClick={onClick}><img src={Imglist.btn_next}/></div>
    );
}
  
function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div className={className} onClick={onClick}><img src={Imglist.btn_prev}/></div>
    );
}

class Main extends React.Component {

    actConfirm(token){
        if (token === undefined || token === ""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไม่สามารถแลกของไอเทมได้",
            });
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'redeem',token:token};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                    coin:data.data.coin,
                });
                this.setState({
                    itemLists : data.data.itemLists,
                    questLists : data.data.questLists,
                })
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
        // this.apiRedeem(
        //     this.props.modal_confirmitem && this.props.modal_confirmitem !== ''
        //     && this.props.modal_confirmitem
        // );
    }

    apiSelectCharacter(id){
        if (id === undefined || id === ""){
            return;
        }
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'select_character',id:id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selectd_char: data.data.selectd_char,
                    characters: data.data.characters,
                    coin:data.data.coin,
                    modal_open: "",
                    show_item: data.data.show_item
                });
                this.setState({
                    itemLists : data.data.itemLists,
                    questLists : data.data.questLists,
                })
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true){
                this.setState({
                    itemLists : data.data.itemLists,
                    questLists : data.data.questLists,
                })
                this.props.setValues({
                    modal_open: "",
                    username: data.data.username,
                    character_name: data.data.character_name,
                    coin: data.data.coin,
                    selected_char: data.data.selected_char,
                    characters: data.data.characters,
                    show_item: data.data.show_item
                });

                if (data.data.selected_char === false){
                    this.props.setValues({ modal_open: "selectcharacter" });
                }
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiRedeem(type_id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type_id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content.response,
                    modal_open:"message",
                    modal_message: "ส่งของรางวัลสำเร็จ<br/>กรุณาตรวจสอบจดหมายภายในเกม"
                });

            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,

            active: [],
            questLists : [],
            quest: [
                {
                    image: Imglist.quest1,
                    complete: false,
                },
                {
                    image: Imglist.quest2,
                    complete: false,
                },
                {
                    image: Imglist.quest3,
                    complete: false,
                },
                {
                    image: Imglist.quest4,
                    complete: false,
                },
                {
                    image: Imglist.quest5,
                    complete: false,
                },
                {
                    image: Imglist.quest6,
                    complete: false,
                },
                {
                    image: Imglist.quest7,
                    complete: false,
                },
            ],
            itemLists: [],
            slider_image: [
                {
                    image: Imglist.slide1,
                    title: 'ชุดทะเลฤดูร้อนหญิง'
                },
                {
                    image: Imglist.slide2,
                    title: 'ชุดทะเลฤดูร้อนชาย'
                },
                {
                    image: Imglist.slide3,
                    title: 'ชุดซัมเมอร์ปาร์ตี้หญิง'
                },
                {
                    image: Imglist.slide4,
                    title: 'ชุดซัมเมอร์ปาร์ตี้ชาย'
                },
            ]
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }

    render() {
        const settings = {
            dots: true,
            infinite: true,
            fade: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        return (
            <PageWrapper>
                <Content>

                    {/* section 1 */}
                    <div className="section section1">
                        {
                            this.state.quest.map((items,key)=>{
                                return(  
                                    <div key={key} className={"questItems " + (this.state.questLists && this.state.questLists.length > 0 ? this.state.questLists[key] !== false ? "" : "" : "") }>
                                        <img src={items.image}/>
                                    </div>
                                )
                            })
                        }  
                    </div>

                    {/* section 2 */}
                    <div className="section section2">
                    {
                        this.props.show_item &&
                        <div className="exchange-text">
                        คุณสามารถแลกเปลี่ยนได้ <br/>
                        ตั้งแต่วันที่ 15 เมษายน 2563 เป็นต้นไป
                        </div>
                    }
                       
                        {
                            this.state.itemLists.map((items,key)=>{
                                return(  
                                    <div key={key} className="items">
                                        <div className="items__img">
                                            <img src={items.image}/>
                                        </div>
                                        <div className="items__name">{items.name}</div>
                                       
                                        {
                                            items.canRedeem === false ?
                                                <>
                                                    <div className={"btn btn-exchange inactive"}>แลกไอเทม</div>
                                                </>
                                                :
                                                <>
                                                    <div className={"btn btn-exchange"} onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการแลกไอเทม '+items.name + ' ?',modal_item_token: items.token})}>แลกไอเทม</div>
                                                </>
                                        }
                                        <small>ใช้ {items.coin} เหรียญ</small>
                                        {
                                            items.limit && parseInt(items.limit) !== 0 ?
                                                items.token === "63048" ?
                                                    <small className="limit">จำกัดเพียง {items.limit} ครั้ง / วัน</small>
                                                    :
                                                    <small className="limit">จำกัดเพียง {items.limit} ครั้ง</small>
                                            : null
                                        }
                                        {
                                            items.token === "63048" &&
                                                <small className="limit">จำกัดเพียง 1 ครั้ง / วัน</small>
                                        }
                                    </div>
                                )
                            })
                        }       
                    </div>

                    {/* section 3 */}
                    <div className="section section3">
                        <div>
                            <Slider {...settings}>
                            {
                                this.state.slider_image.map((items,key)=>{
                                    return(
                                        <div key={key} className=""> 
                                            <p>{items.title}</p>
                                            <img src={items.image}/>
                                        </div>
                                    )
                                })
                            }
                            </Slider>
                        </div>
                    </div>
              
                </Content>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                <ModalConfirmReceive actConfirm={this.actConfirm.bind(this)}/>
                <ModalSelectCharacter apiSelectCharacter={this.apiSelectCharacter.bind(this)}/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #11122d;
    width: 1105px;
    padding-top: 740px;
    text-align: center;
`

const Content = styled.div `  
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }
    & .section{
        color:#fff;
        padding-top: 10%;
        padding-bottom: 10%;
        &__header{
            font-size: 20px;
            font-weight: bold;
            margin-bottom: 30px;
            text-align: center;
        }
        &__step1{
            text-align:center;
            padding-top: 3%;
        }
        &__step2{
            text-align: center;
            padding-top: 4%;
        }
    }
    & small{
        font-size: 80%;
        line-height: 1;
    }
    & .limit{
        font-style: italic;
        font-family: 'Kanit-Light';
        display: block;
    }
    & .section1{
        background: url(${Imglist['content1']}) no-repeat top center; 
        padding: 20% 15% 12%;
        & .questItems{
            width: 33.33%;
            display: inline-block;
            vertical-align: top;
            text-align: center;
            padding: 10px 5px;
            &.disabled{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%); 
            }
        }
    }
    & .section2{
        background: url(${Imglist['content2']}) no-repeat top center; 
        padding: 16% 16% 8%;
        z-index: 1;
        position: relative;
        text-align:left;
    }
    & .items{
        width: 20%;
        display:inline-block;
        vertical-align: top;
        font-size: 13px;
        padding: 10px;
        color: #c6c2c2;
        text-align: center;
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%); 
            &.active{
                -webkit-filter: none;
                filter: none;  
            }
        }
        &__img{
            position:relative; 
            img{
                display: block;
                margin: 0 auto;
            } 
        }
        &__name{
            min-height: 35px;
            line-height: 1.2;
            margin: 5px 0;
            color: #bbd0ff;
        }
    }

    & .btn{
        cursor:pointer;
        // &-default{
        //     background: no-repeat 0 0 url(${Imglist['btn_default']});
        //     width: 155px;
        //     height: 45px;
        //     display: inline-block;
        //     cursor: pointer;
        //     margin: 0 15px;
        //     color: #fff;
        //     line-height: 36px;
        //     font-size: 20px;
        //     &:hover{
        //         background-position: 0 -55px;
        //     }
        //     &.disabled{
        //         -webkit-filter: grayscale(100%);
        //         filter: grayscale(100%);
        //         color: #bfbfbf;
        //         cursor: not-allowed;
        //         &:hover{
        //             background-position: 0 0;
        //         }
        //     }
        // }
        &-exchange{
            background: top no-repeat url(${Imglist['btn_default']});
            height: 30px;
            text-align: center;
            width: 77px;
            color: #000;
            margin: 0 auto;
            font-size: 14px;
            line-height: 2;
            &:hover{
                background-position: bottom;
            }
            &.inactive{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);  
                cursor:default;
                &:hover{
                    background-position: top;
                }
            }
        }
       
    }
    & .section3{
        background: top center no-repeat url(${Imglist['content3']});
        color: #fff;
        text-align: center;
        padding: 20% 0 15%;
        & .slick-slide img{
            margin: 0 auto;
        }
        & .slick-dots li.slick-active button:before {
            opacity: 1;
            color: #fff;
        }
        & .slick-dots li button:before{
            font-size: 11px;
            opacity: 1;
            color: #646464;
        }
        & .slick-prev{
            left:120px;
        }
        & .slick-next{
            right:120px;
        }
        & .slick-prev,
        & .slick-next{
            width: auto;
            height: auto;
            z-index: 1;
            &::before{
                font-size:0;
            }
        }
    }
    .exchange-text{
        text-align: center;
        border-radius: 10px;
        position: absolute;
        width: 600px;
        z-index: 1;
        border: 3px solid #aed7e8;
        background-color: rgba(0,0,0,0.8);
        padding: 65px 20px;
        font-size: 20px;
    }


`

