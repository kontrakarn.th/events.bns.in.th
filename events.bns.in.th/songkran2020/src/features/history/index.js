import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import {Imglist} from './../../constants/Import_Images';
import ModalConfirmReceive from '../modals/ModalConfirmReceive';
import ModalSelectCharacter from "../modals/ModalSelectCharacter";


class History extends React.Component {

    apiGetHistory(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'getHistory'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open: '',
                    received: data.data.received,
                    redeem: data.data.redeem
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_GET_HISTORY", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',

            activetab : "tab1",
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiGetHistory(this.props);
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            if (!this.props.uid) {
                this.apiGetHistory(this.props);
            }
        }

    }

    render() {
        return (
            <PageWrapper>
                <Content>
                    <div className="section history">
                        <div className="tab-nav">
                            <div className={(this.state.activetab === 'tab1' ? 'active' : ' ')} onClick={()=>{this.setState({activetab: 'tab1'})}}><img src={Imglist.tab_receive_coin}/></div>
                            <div className={(this.state.activetab === 'tab2' ? 'active' : ' ')} onClick={()=>{this.setState({activetab: 'tab2'})}}><img src={Imglist.tab_use_coin}/></div>
                        </div>

                        {this.state.activetab === 'tab1' &&
                            <div className="tab-content">
                                {
                                    this.props.received !== '' && this.props.received.map((items,key)=>{
                                        return(  
                                            <div key={key} className="redeem__list">
                                               <div className="redeem__list--coin">ได้รับ &nbsp;&nbsp; {items.coin} เหรียญ</div>
                                               <div className="redeem__list--exchange">จาก  {items.quest}</div>
                                               <div className="redeem__list--date">{items.date_log}</div>
                                            </div>
                                        )
                                    })
                                }  
                            </div>
                        }
                        {this.state.activetab === 'tab2' &&
                            <div className="tab-content">
                                {
                                    this.props.redeem !== '' && this.props.redeem.map((items,key)=>{
                                        return(  
                                            <div key={key} className="redeem__list">
                                               <div className="redeem__list--coin">ใช้ &nbsp;&nbsp; {items.coin} เหรียญ</div>
                                               <div className="redeem__list--exchange">แลก  {items.item}</div>
                                               <div className="redeem__list--date">{items.date}</div>
                                               <div className="redeem__list--time">{items.time}</div>
                                            </div>
                                        )
                                    })
                                }  
                            </div>
                        }
                      
                    </div>            
                </Content>
                <ModalLoading />
         
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(History);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #11122d;
    width: 1105px;
    padding-top: 740px;
`

const Content = styled.div `  
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }
    & .section{
        color:#fff;
        padding-top: 10%;
        padding-bottom: 10%;
      
    }
    & small{
        font-size: 80%;
        line-height: 1;
    }
    & .limit{
        font-style: italic;
        font-family: 'Kanit-Light';
        display: block;
    }
    & .history{
        background: url(${Imglist['history']}) no-repeat top center; 
        padding: 15% 15% 5% 15%;
        height: 800px;
        & .questItems{
            width: 33.33%;
            display: inline-block;
            vertical-align: top;
            text-align: center;
            padding: 10px 5px;
            &.disabled{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%); 
            }
        }
        .tab-nav{
            text-align: center;
            > div{
                display: inline-block;
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%); 
                margin:  5px;
                cursor:pointer;
                &.active{
                    -webkit-filter: none;
                    filter: none; 
                }
            }
        }
        .tab-content{
            height: 529px;
            overflow-y: auto;
            ::-webkit-scrollbar {
                width: 7px;
            }
            ::-webkit-scrollbar-track {
                border-radius: 10px;
            }
            ::-webkit-scrollbar-thumb {
                background: #10263a;
                border-radius: 10px;
            }
        }
    }
    .redeem__list{
        display: table;
        width: 100%;
        table-layout: fixed;
        > div{
            display: table-cell;
            width: 25%;
        }
        .redeem__list{
            &--exchange{
                width: 35%
            }
            &--date,
            &--time{
                width: 20%
            }
        }
    }
   
    & .items{
        width: 20%;
        display:inline-block;
        vertical-align: top;
        font-size: 13px;
        padding: 10px;
        color: #c6c2c2;
        text-align: center;
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%); 
            &.active{
                -webkit-filter: none;
                filter: none;  
            }
        }
        &__img{
            position:relative; 
            img{
                display: block;
                margin: 0 auto;
            } 
        }
        &__name{
            min-height: 35px;
            line-height: 1.2;
            margin: 5px 0;
            color: #bbd0ff;
        }
    }

    & .btn{
        cursor:pointer;
        // &-default{
        //     background: no-repeat 0 0 url(${Imglist['btn_default']});
        //     width: 155px;
        //     height: 45px;
        //     display: inline-block;
        //     cursor: pointer;
        //     margin: 0 15px;
        //     color: #fff;
        //     line-height: 36px;
        //     font-size: 20px;
        //     &:hover{
        //         background-position: 0 -55px;
        //     }
        //     &.disabled{
        //         -webkit-filter: grayscale(100%);
        //         filter: grayscale(100%);
        //         color: #bfbfbf;
        //         cursor: not-allowed;
        //         &:hover{
        //             background-position: 0 0;
        //         }
        //     }
        // }
        &-exchange{
            background: top no-repeat url(${Imglist['btn_default']});
            height: 30px;
            text-align: center;
            width: 77px;
            color: #000;
            margin: 0 auto;
            font-size: 14px;
            line-height: 2;
            &:hover{
                background-position: bottom;
            }
            &.inactive{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);  
                cursor:default;
                &:hover{
                    background-position: top;
                }
            }
        }
       
    }
   

`

