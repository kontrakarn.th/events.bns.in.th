import React,{useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {setValues} from './../store/redux';
import styled from 'styled-components';
import imgList from './../constants/ImportImages';
import F11Layout from './../features/f11Layout';
import Modals from './../features/modals';
import {apiPost} from './../constants/Api';

const Home = (props) => {
  	const apiEventInfo = () => {
		let dataSend = {type: "event_info"};
		let successCallback = (res) => {
			if(res.status) {
				props.setValues({
					...res.data,
					modal_open:""
				})
			} 
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		let failCallback = (res) => {
			if (res.type === 'no_permission') {
				props.setValues({
					permission: false,
					modal_open: "",
				});
			}
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
    	apiPost("REACT_APP_API_POST_EVENT_INFO", props.jwtToken, dataSend, successCallback, failCallback);
	}
	useEffect(()=>{
		if(props.jwtToken !== "" && !props.status) apiEventInfo();
	},[props.jwtToken])
	return (
		<F11Layout page={"main"}>
			<Modals />
			<HomeStyle>
				{/* <section className="banner">
					<Link className="banner__btn" to={props.eventPath("/promotion")} />
				</section> */}
				<section className="detail">
					<img className="detail__board detail__board--1" src={imgList.content_1} alt=''/>
					{props.can_buy
					?
						<div className="detail__btn" onClick={()=>props.setValues({modal_open: 'character'})}/>
					:
						<div className="detail__btn detail__btn--notbuy"/>
					}
					<div className="detail__click detail__click--1">
						<img src={imgList.reward_hot_1} alt=''/>
					</div>
					<div className="detail__click detail__click--2">
						<img src={imgList.reward_hot_2} alt=''/>
					</div>
				</section>
				<section className="detail">
					<img className="detail__board detail__board--2" src={imgList.content_2} alt=''/>
				</section>
			</HomeStyle>
		</F11Layout>
	)
}
const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(Home);

const HomeStyle = styled.div`
	position: relative;
	display: block;
	width: 1105px;
	height: 2260px;
	background-image: url(${imgList.bg});
	padding-top: 829px;
  	.banner {
		position: relative;
		display: block;
		height: 700px;
		&__btn {
			position: absolute;
			top: 410px;
    		left: 228px;
			display: block;
			width: 306px;
			height: 53px;
			background-image: url(${imgList.btn_join});
			background-position: top center;
			&:hover {
				background-position: bottom center;
			}
		}
  	}
	.detail {
		position: relative;
		display: block;
		&__board {
			display: block;
			&--1{
				width: 726px;
				height: 896px;
				margin-left: 195px;
			}
			&--2{
				width: 1105px;
				height: 457px;
				margin-top: 78px;
			}
		}
		&__click{
			position: absolute;
			width: 120px;
			height: 128px;
			>img{
				// position: absolute;
				// opacity: 0;
				// display: block;
				// width: 461px;
				// height: 612px;
				position: fixed;
				// width: 461px;
				// height: 612px;
				top: 45%;
				transform: translate(0, -50%);
				transition: .3s opacity;
				pointer-events: none;
				opacity: 0;
			}
			&--1{
				top: 98px;
				left: 197px;
				>img{
					left: 330px;
				}
			}
			&--2{
				right: 341px;
				bottom: 235px;
				>img{
					left: 80px;
				}
			}
			&:hover{
				>img{
					opacity: 1;
				}
			}
		}
		&__btn {
			position: absolute;
			bottom: 50px;
			left: 50%;
			transform: translate(-50%, 0);
			display: block;
			width: 228px;
			height: 66px;
			background-image: url(${imgList.btn_buy});
			background-position: top center;
			cursor: pointer;
			&:hover {
				background-position: bottom center;
			}
			&--notbuy{
				background-image: url(${imgList.btn_buyed});
				pointer-events: none;
			}
		}
	}
`;
