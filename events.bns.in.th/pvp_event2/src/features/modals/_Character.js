import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import imgList from "../../constants/ImportImages";
import {apiPost} from '../../constants/Api';

const ModalConfirm = props => {
    const name = "character";
    
    const select = () => {
        props.setValues({
            modal_open:"confirm"
        })
    }
    
    return (
        <ModalCore
            open={props.modal_open === name}
            onClick={()=>props.setValues({modal_open:''})}
        >
            <ModalcontentStyle>
                <div className="message">
                    <select className="message__select" onChange={(e) => props.setValues({select_character: e.target.value})}>
                        <option value="">กรุณาเลือก</option>
                        {
                            props.characters.map((character, index) => {
                                return <option key={"character_"+index} value={character.character_id}>{`${character.character_name} - ${character.character_class_desc}`}</option>
                            })
                        }
                    </select>
                    <div>
                        ไอเทมอาวุธเพลิงพยัคฆ์และสร้อยข้อมือวารีสวรรค์<br />จะได้รับตามอาชีพผู้เล่นที่เลือก
                    </div>
                </div>
                <div className="buttons">
                    <BtnStyle click={props.select_character !== ""} className="btn btn--confirm" onClick={()=>select()} />
                </div>
            </ModalcontentStyle>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalConfirm);

const ModalcontentStyle = styled.div`
    position: relative;
    display: block;
    width: 461px;
	height: 334px;
	padding: 90px 10px 0px;
    background-image: url(${imgList.bg_character});
    .message {
        color: #FFFFFF;
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 55%;
        text-align: center;
        font-family: "Kanit-Light", Tahoma;
        >div{
            color: #fff;
            line-height: 1.2;
        }
        &__select {
            width: 90%;
            margin-bottom: 15px;
            font-size: 20px;
            background: center right -15px no-repeat url(${imgList.icon_down_arrow}) #252525;
            border-color: #252525;
            color: white;
            padding: 10px;
            position: relative;
            -webkit-appearance: none;
            font-family: "Kanit-Regular";
        }
    }
    .buttons {
        position: absolute;
        left: 0px;
        bottom: 33px;
        display: block;
        width: 100%;
        text-align: center;
    }
    .btn {
        display: inline-block;
        width: 143px;
		height: 40px;
		margin: 0px 7px;
        background-position: top center;
        cursor: pointer;
        &--confirm {
            background-image: url(${imgList.btn_confirm});
        }
        &--cancel {
            background-image: url(${imgList.btn_cancel});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;

const BtnStyle = styled.a`
    ${props => !props.click && 
        `pointer-events: none;
        filter: grayscale(1);`
    }
`;
