import React from 'react';
import ModalMessage from './_Message';
import ModalLoading from './_Loading';
import ModalConfirm from './_Confirm';
import ModalReward from './_Reward';
import ModalCharacter from './_Character';

export default (props)=> (
  <>
    <ModalLoading />
    <ModalMessage />
    <ModalConfirm/>
    <ModalReward/>
    <ModalCharacter />
  </>
)
