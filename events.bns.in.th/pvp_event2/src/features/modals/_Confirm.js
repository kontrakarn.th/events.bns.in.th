import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import imgList from "../../constants/ImportImages";
import {apiPost} from '../../constants/Api';

const ModalConfirm = props => {
    const name = "confirm";
    const apiBuy = () => {
        props.setValues({
            modal_open:"loading"
        })
        let dataSend = {character_id: props.select_character};
        let successCallback = (res) => {
            if(res.status) {
                props.setValues({
                    ...res.data,
                    modal_open: "reward"
                })
            } 
            else {
                props.setValues({
                    modal_open:"message",
                    modal_message: res.message,
                    select_character: "",
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: "",
                    select_character: ""
                });
            }
            else {
                props.setValues({
                    modal_open:"message",
                    modal_message: res.message,
                    select_character: ""
                });
            }
        }
        apiPost("REACT_APP_API_POST_BUY", props.jwtToken, dataSend, successCallback, failCallback);
    }
    return (
        <ModalCore
            open={props.modal_open === name}
            onClick={()=>props.setValues({modal_open:''})}
        >
            <ModalcontentStyle>
                <div className="message">
                    <div>
                        ต้องการซื้อแพ็คเกจนักบู้ สู้ยิบตา<br/>150,000 ไดมอนด์ ?
                    </div>
                </div>
                <div className="buttons">
                    <a className="btn btn--confirm" onClick={()=>apiBuy()} />
                    <a className="btn btn--cancel" onClick={()=>props.setValues({modal_open:'', select_character: ""})} />
                </div>
            </ModalcontentStyle>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalConfirm);

const ModalcontentStyle = styled.div`
    position: relative;
    display: block;
    width: 461px;
	height: 334px;
	padding: 90px 10px 0px;
    background-image: url(${imgList.bg_confirm});
    .message {
        color: #FFFFFF;
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 55%;
        text-align: center;
        font-family: "Kanit-Light", Tahoma;
        >div{
            color: #fff;
            line-height: 1.2;
        }
    }
    .buttons {
        position: absolute;
        left: 0px;
        bottom: 33px;
        display: block;
        width: 100%;
        text-align: center;
    }
    .btn {
        display: inline-block;
        width: 143px;
		height: 40px;
		margin: 0px 7px;
        background-position: top center;
        cursor: pointer;
        &--confirm {
            background-image: url(${imgList.btn_confirm});
        }
        &--cancel {
            background-image: url(${imgList.btn_cancel});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;
