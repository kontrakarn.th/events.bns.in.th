import React from "react";
import Section1 from "./../features/section1";
import Section2 from "./../features/section2";
import Section3 from "./../features/section3";
import Section4 from "./../features/section4";
import Section5 from "./../features/section5";
import swal from "sweetalert";
import logo from "../static/images/logo_bns.png";
import $ from "jquery";
import { QrModal } from "../features/modal";

import { connect } from "react-redux";
import {
  onAccountLogoutWalkin,
  onAccountLogout
} from "./../actions/AccountActions";

export class Home extends React.Component {
  //========= ========= ========= ========= ========= ========= ========= ========= =========

  //========= ========= ========= ========= ========= ========= ========= ========= =========
  constructor(props) {
    super(props);
    this.state = {
      qrcode: "",
      username: "",
      type: "qrcode"
    };
    this.parallaxInstance = null;
  }
  componentWillMount() {
    var type = "qrcode";
    if (this.props.match.params.type == "walkin") {
      type = "login";
      this.setState({
        type: "login",
        autoLogout: false
      });
    }
    let self = this;

    // $.ajax({
    //   method: "POST",
    //   url:
    //     process.env.REACT_APP_API_SERVER_HOST +
    //     process.env.REACT_APP_API_POST_REGISTER,
    //   data: {
    //     type: type
    //   },
    //   success: function(result) {
    //     let response = result.json();
    //     if (result.status == true) {
    //       if (type == "qrcode") {
    //         self.setState({
    //           qrcode: result.data.qrcode,
    //           username: result.data.username
    //         });
    //       } else if (type == "login") {
    //         swal({
    //           title: result.message,
    //           text: "",
    //           icon: "success",
    //           button: "ตกลง",
    //           closeOnClickOutside: false
    //         }).then(function(result) {});
    //       }
    //     } else {
    //       console.log(1, response.type);
    //       if (
    //         result.type == "cant_create_member" ||
    //         result.type == "register_already"
    //       ) {
    //         console.log(2);
    //         swal({
    //           title: result.message,
    //           text: "",
    //           icon: "error",
    //           button: "ตกลง",
    //           closeOnClickOutside: false
    //         }).then(function(result) {
    //           self.onAccountLogout();
    //         });
    //       }
    //     }
    //   }
    // });

    fetch(
      process.env.REACT_APP_API_SERVER_HOST +
        process.env.REACT_APP_API_POST_REGISTER,
      {
        method: "POST",
        credentials: "same-origin",
        // ...result
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        // headers: createAuthorizationHeaders(user_data),
        // headers: {
        //     'Accept': 'application/json',
        //     'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'
        // },
        body: JSON.stringify({ type: type })
        // body: createJwt(JSON.stringify(api_data)).body
      }
    )
      .then(response => response.json())
      .then(result => {
        if (result.status == true) {
          if (type == "qrcode") {
            self.setState({
              qrcode: result.data.qrcode,
              username: result.data.username
            });
          } else if (type == "login") {
            swal({
              title: result.message,
              text: "",
              icon: "success",
              button: "ตกลง",
              closeOnClickOutside: false
            }).then(function(result) {
              self.setState({
                autoLogout: true
              });
            });
          }
        } else {
          //swal false
          if (
            result.type == "cant_create_member" ||
            result.type == "register_already"
          ) {
            swal({
              title: result.message,
              text: "",
              icon: "error",
              button: "ตกลง",
              closeOnClickOutside: false
            }).then(function(result) {
              // log out auto
              self.setState({
                autoLogout: true
              });
            });
          }
        }
      });
  }
  render() {
    return (
      <div className="landing">
        {/* menu */}
        <div className="landing__topmenu" style={{ display: "none" }}>
          <img className="landing__logo" src={logo} />
          <div className="landing__buttons">
            <a
              className="landing__pathnote"
              href="https://bns.garena.in.th/tournament/view/bstc-2019-main-event"
              target="_blank"
            >
              Patch Note
            </a>
            <a
              className="landing__home"
              href="https://bns.garena.in.th/"
              target="_blank"
            >
              เข้าสู่เว็บไซต์
            </a>
          </div>
        </div>

        <Section1
          type={this.state.type}
          qrcode={this.state.qrcode}
          username={this.state.username}
          autoLogout={this.state.autoLogout}
        />
        <Section2 />
        <Section3 />
        <Section4 />
        <Section5 />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  ...state.layout,
  ...state.AccountReducer
});
const mapDispatchToProps = {
  onAccountLogout,
  onAccountLogoutWalkin
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
