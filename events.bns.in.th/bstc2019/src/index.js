import "core-js/es6/map";
import "core-js/es6/set";
import "raf/polyfill";
import "cross-fetch/polyfill";
import React from "react";

import { hydrate, render } from "react-dom";

import ReactDOM from "react-dom";
// import {render} from 'react-snapshot';
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { combineReducers, createStore } from "redux";
import { Web } from "./routes/Web";
import serviceWorker from "./serviceWorker";
import { ModalReducer } from "./features/modal/redux";
import AccountReducer from "./reducers/AccountReducer";

import "./styles/index.scss";

const store = createStore(
  combineReducers({
    AccountReducer,
    Modal: ModalReducer
  })
);
const rootElement = document.getElementById("root");
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Web />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
