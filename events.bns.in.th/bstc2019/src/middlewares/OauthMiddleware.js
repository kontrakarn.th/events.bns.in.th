import React from "react";
import { connect } from "react-redux";
import { getCookie, setCookie, delCookie } from "./../constants/Cookie";
import { QueryString } from "./../constants/QueryString";
import {
  onAccountAuthed,
  onAccountLogin,
  onAccountLogout,
  onAccountLogoutWalkin,
  setLoginUrl,
  setLoginUrlWalkin,
  setLogoutUrl,
  receiveSessionKey,
  setJwtToken
} from "./../actions/AccountActions";
import $ from "jquery";
// import KJUR from 'jsrsasign';

/**
 * This container deals with OAUTH authentication; thus the name "OauthMiddleware".
 */
class OauthMiddleware extends React.Component {
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.authState !== this.props.authState) {
      switch (this.props.authState) {
        case "LOGGED_IN":
          this.login();
          break;
        case "LOGGED_OUT":
          this.logout();
          break;
        case "LOGGED_OUT_WALKIN":
          this.logoutWalkin();
          break;
        default:
      }
    }
  }

  componentDidMount() {
    this.checkLogin();
  }

  checkLogin() {
    const { dispatch } = this.props;
    var self = this;
    var oauthParam = this.getParam("access_token") || this.getParam("token");
    var oauthCookie = this.getCookie("oauth_session");
    if (oauthParam && oauthParam.length > 0) {
      this.setCookie("oauth_session", oauthParam, 1);
      var url = window.location.href.substring(
        0,
        window.location.href.lastIndexOf("?")
      ); // get rid of the query string
      setTimeout(() => {
        window.location.href = url; // we need hard refresh
      }, 800);
    } else if (oauthCookie && oauthCookie.length > 0) {
      // get user info
      $.ajax({
        url:
          process.env.REACT_APP_API_SERVER_HOST +
          process.env.REACT_APP_API_GET_OAUTH_INFO +
          "?access_token=" +
          oauthCookie,
        dataType: "json",
        type: "GET",
        success: function(result) {
          if (result.status == true) {
            if (result.token != "") {
              self.props.setJwtToken(result.token);
              // let sessionKey = getCookie(process.env.REACT_APP_OAUTH_COOKIE_NAME);
              // let redirect = "https://auth.garena.in.th/login/callback/" + process.env.REACT_APP_OAUTH_APP_NAME + "/logout";
              // let logoutUrl = process.env.REACT_APP_OAUTH_URL + '/oauth/logout?access_token=' + sessionKey + "&format=redirect&redirect_uri=" + redirect;
              // self.props.setLogoutUrl(logoutUrl);
            } else {
              self.delCookie("oauth_session");
              window.location.reload();
            }
          }
        }
      });
    } else {
      let loginUrl =
        process.env.REACT_APP_OAUTH_URL +
        "/oauth/login?response_type=token&client_id=" +
        process.env.REACT_APP_OAUTH_APP_ID +
        "&redirect_uri=https://auth.garena.in.th/login/callback/" +
        process.env.REACT_APP_OAUTH_APP_NAME +
        "/" +
        "&locale=" +
        process.env.REACT_APP_LOCALE +
        "&display=page&all_platform=1&theme=mshop_iframe_white";
      this.props.setLoginUrl(loginUrl);

      let loginUrlWalkin =
        process.env.REACT_APP_OAUTH_URL +
        "/oauth/login?response_type=token&client_id=" +
        process.env.REACT_APP_OAUTH_APP_ID +
        "&redirect_uri=https://auth.garena.in.th/login/callback/" +
        process.env.REACT_APP_OAUTH_APP_NAME_WALKIN +
        "/" +
        "&locale=" +
        process.env.REACT_APP_LOCALE +
        "&display=page&all_platform=1&theme=mshop_iframe_white";
      this.props.setLoginUrlWalkin(loginUrlWalkin);

      // console.log(QueryString[process.env.REACT_APP_OAUTH_PARAM_NAME]);
      /* if (QueryString[process.env.REACT_APP_OAUTH_PARAM_NAME]) {
				this.props.receiveSessionKey(QueryString[process.env.REACT_APP_OAUTH_PARAM_NAME]);
				this.props.onAccountLogin();
			} */
      //   window.location.href = loginUrl;
    }
  }

  getParam(_param) {
    var pStr =
        window.location.hash.toString() || window.location.search.toString(),
      r = new RegExp("[?&]*" + _param + "=([^&]+)"),
      m = pStr.match(r);
    if (m) return m[1].replace('"', "");
    else return "";
  }

  getCookie(name) {
    var match = document.cookie.match(new RegExp("(^| )" + name + "=([^;]+)"));
    if (match) return match[2];
  }
  delCookie(name) {
    document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
  }
  setCookie(name, value, days) {
    var expires = "";
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
  }

  isLoggedIn() {
    return this.props.userData && this.props.userData.uid;
  }

  /* checkAuth() {
		fetch(process.env.REACT_APP_API_GET_ACCOUNT_INFO, {
			method: 'POST',
			body: '',
			credentials: 'same-origin',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			}
		}).then(response => {
			if (response.status === 200) {
				response.json().then(data => {
					if (data.account_info) {
						// session key exists in cookies when you are logged in
						let sessionKey = getCookie(process.env.REACT_APP_OAUTH_COOKIE_NAME);
						this.props.receiveSessionKey(sessionKey);
						this.props.onAccountAuthed(data.account_info);
						let redirect = "https://auth.garena.in.th/login/callback/" + process.env.REACT_APP_OAUTH_APP_NAME + "/logout";
						let logoutUrl = process.env.REACT_APP_OAUTH_URL + '/oauth/logout?access_token=' + sessionKey + "&format=redirect&redirect_uri=" + redirect;
						this.props.setLogoutUrl(logoutUrl);
					} else {
						window.location.href = this.props.loginUrl;
					}
				});
			} else {
			}
		}, error => {
			console.error(error);
		});
	} */

  login() {
    setCookie(
      process.env.REACT_APP_OAUTH_COOKIE_NAME,
      this.props.sessionKey,
      1,
      process.env.REACT_APP_COOKIE_DOMAIN
    );
    let uri = window.location.href.substring(
      0,
      window.location.href.lastIndexOf("?")
    ); // get rid of the query string
    window.location.href = uri; // we need hard refresh
  }

  logout() {
    let sessionKey = getCookie(process.env.REACT_APP_OAUTH_COOKIE_NAME);
    // console.log("sessionKey :", sessionKey);
    // this.props.receiveSessionKey(sessionKey);
    // this.props.onAccountAuthed(data.account_info);

    delCookie(
      process.env.REACT_APP_OAUTH_COOKIE_NAME,
      process.env.REACT_APP_COOKIE_DOMAIN
    );

    let redirect =
      "https://auth.garena.in.th/login/callback/" +
      process.env.REACT_APP_OAUTH_APP_NAME +
      "/logout";
    let logoutUrl =
      process.env.REACT_APP_OAUTH_URL +
      "/oauth/logout?access_token=" +
      sessionKey +
      "&format=redirect&redirect_uri=" +
      redirect;

    window.location.href = logoutUrl;
  }

  logoutWalkin() {
    let sessionKey = getCookie(process.env.REACT_APP_OAUTH_COOKIE_NAME);
    // console.log("sessionKey walk :", sessionKey);
    // this.props.receiveSessionKey(sessionKey);
    // this.props.onAccountAuthed(data.account_info);

    delCookie(
      process.env.REACT_APP_OAUTH_COOKIE_NAME,
      process.env.REACT_APP_COOKIE_DOMAIN
    );

    let redirectWalkin =
      "https://auth.garena.in.th/login/callback/" +
      process.env.REACT_APP_OAUTH_APP_NAME_WALKIN +
      "/logout";
    let logoutUrlWalkin =
      process.env.REACT_APP_OAUTH_URL +
      "/oauth/logout?access_token=" +
      sessionKey +
      "&format=redirect&redirect_uri=" +
      redirectWalkin;
    window.location.href = logoutUrlWalkin;
  }

  render() {
    return false;
  }
}

const mapStateToProps = state => ({
  sessionKey: state.AccountReducer.sessionKey,
  userData: state.AccountReducer.userData,
  authState: state.AccountReducer.authState,
  loginUrl: state.AccountReducer.loginUrl,
  logoutUrl: state.AccountReducer.logoutUrl,
  logoutUrlWalkin: state.AccountReducer.logoutUrlWalkin
});

const mapDispatchToProps = dispatch => ({
  onAccountAuthed: userData => {
    dispatch(onAccountAuthed(userData));
  },
  onAccountLogin: () => {
    dispatch(onAccountLogin());
  },
  onAccountLogout: () => {
    dispatch(onAccountLogout());
  },
  onAccountLogoutWalkin: () => {
    dispatch(onAccountLogoutWalkin());
  },
  receiveSessionKey: sessionKey => {
    dispatch(receiveSessionKey(sessionKey));
  },
  setLoginUrl: url => {
    dispatch(setLoginUrl(url));
  },
  setLoginUrlWalkin: url => {
    dispatch(setLoginUrlWalkin(url));
  },
  setLogoutUrl: url => {
    dispatch(setLogoutUrl(url));
  },
  setJwtToken: token => {
    dispatch(setJwtToken(token));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OauthMiddleware);
