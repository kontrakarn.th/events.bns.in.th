
import scroll from './../static/images/icon_scroll.png'
import section1_character from './../static/images/section1_character.png'
import section1_bg_mb from './../static/images/section1_bg_mb.png'
import section1_text from './../static/images/section1_text.png'
import logo_ais from './../static/images/logo_ais.png'
import qr from './../static/images/test_qr.jpg'
import section2_text from './../static/images/section2_text.png'
import slide1 from './../static/images/slide/1.png'
import slide2 from './../static/images/slide/2.png'
import slide3 from './../static/images/slide/3.png'
import slide4 from './../static/images/slide/4.png'
import slide5 from './../static/images/slide/5.png'
import slide6 from './../static/images/slide/6.png'
import slide7 from './../static/images/slide/7.png'
import slide8 from './../static/images/slide/8.png'
import section3_text from './../static/images/section3_text.png'
import section3_character1 from './../static/images/section3_character1.png'
import section3_character2 from './../static/images/section3_character2.png'
import section4_text from './../static/images/section4_text.png'
import slide_character1 from './../static/images/slide_character/1.png'
import slide_character2 from './../static/images/slide_character/2.png'
import slide_character3 from './../static/images/slide_character/3.png'
import slide_character4 from './../static/images/slide_character/4.png'
import slide_character5 from './../static/images/slide_character/5.png'
import slide_character6 from './../static/images/slide_character/6.png'
import slide_character7 from './../static/images/slide_character/7.png'
import section5_character from './../static/images/section5_character.png'
import section5_text from './../static/images/section5_text.png'

export const Imglist = {
	scroll,
	section1_bg_mb,
	section1_character,
	logo_ais,
	section1_text,
	qr,
	section2_text,
	slide1,
	slide2,
	slide3,
	slide4,
	slide5,
	slide6,
	slide7,
	slide8,
	section3_text,
	section3_character1,
	section3_character2,
	section4_text,
	slide_character1,
	slide_character2,
	slide_character3,
	slide_character4,
	slide_character5,
	slide_character6,
	slide_character7,
	section5_character,
	section5_text
}