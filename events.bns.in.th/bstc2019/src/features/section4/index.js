import React from "react";
import { connect } from "react-redux";
import styled, { keyframes } from "styled-components";
import Slider from "react-slick";
import { QrModal } from "../../features/modal";
import { Parallax } from "../../features/pattern";
//import ModalQr from './../../features/modals/ModalLoading';

import { Imglist } from "./../../constants/Import_Images";

class Section4 extends React.Component {
  // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========

  // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
  constructor(props) {
    super(props);

    this.state = {
      slide: [
        {
          img: Imglist.slide_character1
        },
        {
          img: Imglist.slide_character2
        },
        {
          img: Imglist.slide_character3
        },
        {
          img: Imglist.slide_character4
        },
        {
          img: Imglist.slide_character5
        },
        {
          img: Imglist.slide_character6
        },
        {
          img: Imglist.slide_character7
        }
      ]
    };
  }

  componentWillMount() {}

  render() {
    const settings = {
      dots: true,
      arrows: false,
      fade: true,
      infinite: true,
      autoplay: true,
      speed: 2000,
      autoplaySpeed: 2000,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <section className="section4">
        <div className="container clearfix">
          <div className="is-mobile">
            <img src={Imglist.logo_ais} />
          </div>

          {/* col-image */}
          <div className="col-image">
            <Slider {...settings}>
              {this.state.slide.map((item, key) => {
                return (
                  <div>
                    <img src={item.img} />
                  </div>
                );
              })}
            </Slider>
          </div>
          {/* end col-image */}

          {/* col-content */}
          <div className="col-content text-center clear-padding">
            <img src={Imglist.logo_ais} className="is-desktop" />
            <img src={Imglist.section4_text} />
            <div>
              <a
                className="btn btn-white"
                href="https://bns.garena.in.th/tournament/view/bstc-2019-main-event"
                target="_blank"
              >
                รายละเอียดงาน
              </a>
            </div>
          </div>
          {/* end col-content */}
        </div>
        <QrModal />
      </section>
    );
  }
}
const mapStateToProps = state => ({
  ...state.layout,
  ...state.AccountReducer,
  ...state.EventReducer
});
const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Section4);
