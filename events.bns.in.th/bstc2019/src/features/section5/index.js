import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import { setValues } from './redux';
import { QrModal } from '../../features/modal';
import { Parallax } from '../../features/pattern';
import {Imglist} from './../../constants/Import_Images';

class Section1 extends React.Component {

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
  
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: ''
        }
    }

    componentWillMount() {
    
    }

    render() {
        return (
            <section className="section5 pb-0">
                <div className="container clearfix">
                    <div className="is-mobile logo_ais_mb">
                        <img src={Imglist.logo_ais}/>
                    </div>
                    {/* col-parallex */}
                    <div className="col-parallex">
                        <div className="landing__set landing__setcharacter">
                            <div className="landing--container">      
                                <Parallax limitY={5} limitX={null}>
                                    <div></div>
                                    <div></div>
                                    <div className="landing__layer landing__layer--f1">
                                        <div className="landing__charactor">
                                            <img className="landing__l0" src={Imglist.section5_character} alt="" />
                                        </div>
                                    </div>
                                </Parallax>       
                            </div>
                        </div>
                    </div>
                    {/* end col-parallex */}

                    {/* col-content */}
                    <div className="col-content">
                        <img src={Imglist.logo_ais} className="is-desktop"/>
                        <img src={Imglist.section5_text}/>
                        <div>
                            <a className="btn btn-white" target="_blank" href="https://bns.garena.in.th/promotions/view/bstc2019-preorder-package">promotion</a>        
                        </div>
                    </div>
                    {/* end col-content */}

                </div> 
                <QrModal/>
            </section>   
        )
    }
}
const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Section1);
