import React from 'react';
import Parallax from 'parallax-js';

export class ParallaxG extends React.Component {
    setParallax(e){
        if(e && this.parallaxInstance === null) {
            // console.log("limitY : "+this.state.limitY);
            // console.log("limitX : "+this.state.limitX);
            this.parallaxInstance = new Parallax(e,{
                limitY: this.state.limitY,
                limitX: this.state.limitX,
            })
            // this.parallaxInstance.friction(0.3,0);
            // this.parallaxInstance.limit(true, false);
        }
    }
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props){
        super(props);
        this.state = {
            depth: 0.2,
            limitY: this.props.limitY || null,
            limitX: this.props.limitX || null,
        }
        this.parallaxInstance = null;
    }
    componentDidMount(){
        this.setState({
            depth:this.props.depth || 0.2
        });
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.depth !== prevProps.depth){
            this.setState({
                depth:this.props.depth || 0.2
            });
        }
        if(this.props.limitY !== prevProps.limitY){
            this.setState({
                limitY:this.props.limitY,
            });
        }
        if(this.props.limitX !== prevProps.limitX){
            this.setState({
                limitX:this.props.limitX,
            });
        }
    }
    render() {
        let children = this.props.children ? this.props.children : [];
        return (
            <div ref={this.setParallax.bind(this)} style={{overflow: "hidden"}}>
                {children.map((item,index)=>{
                    return (
                        <div key={"prl"+index} data-depth={String(index *this.state.depth)} >
                            {item}
                        </div>
                    )
                })}
            </div>
        )
    }
}
