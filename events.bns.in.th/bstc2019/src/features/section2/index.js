import React from "react";
import { connect } from "react-redux";
import styled, { keyframes } from "styled-components";
import Slider from "react-slick";
import { QrModal } from "../../features/modal";
import { Parallax } from "../../features/pattern";
//import ModalQr from './../../features/modals/ModalLoading';
import { Imglist } from "./../../constants/Import_Images";

class Section2 extends React.Component {
  // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========

  // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
  constructor(props) {
    super(props);

    this.state = {
      slide: [
        {
          img: Imglist.slide1
        },
        {
          img: Imglist.slide2
        },
        {
          img: Imglist.slide3
        },
        {
          img: Imglist.slide4
        },
        {
          img: Imglist.slide5
        },
        {
          img: Imglist.slide6
        },
        {
          img: Imglist.slide7
        },
        {
          img: Imglist.slide8
        }
      ]
    };
  }

  componentWillMount() {}

  render() {
    const settings = {
      dots: true,
      arrows: false,
      fade: true,
      infinite: true,
      autoplay: true,
      speed: 2000,
      autoplaySpeed: 3000,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <section className="section2">
        <div className="container clearfix">
          <div className="is-mobile">
            <img src={Imglist.logo_ais} />
          </div>
          {/* col-image */}
          <div className="col-image">
            <Slider {...settings}>
              {this.state.slide.map((item, key) => {
                return (
                  <div>
                    <img src={item.img} />
                  </div>
                );
              })}
            </Slider>
          </div>
          {/* end col-image */}

          {/* col-content */}
          <div className="col-content text-center clear-padding">
            <img src={Imglist.logo_ais} className="is-desktop" />
            <img src={Imglist.section2_text} />
            <div>
              <a
                className="btn btn-white"
                href="https://bns.garena.in.th/tournament/view/bstc-2019-final"
                target="_blank"
              >
                รายละเอียดการแข่งขัน
              </a>
            </div>
          </div>
          {/* end col-content */}
        </div>
        <QrModal />
      </section>
    );
  }
}
const mapStateToProps = state => ({
  ...state.layout,
  ...state.AccountReducer,
  ...state.EventReducer
});
const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Section2);
