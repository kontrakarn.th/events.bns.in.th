import React from "react";
import { connect } from "react-redux";
import styled, { keyframes } from "styled-components";
import { setValues } from "./redux";
import { Parallax } from "../../features/pattern";
//import ModalQr from './../../features/modals/ModalLoading';

import { Imglist } from "./../../constants/Import_Images";

class Section3 extends React.Component {
  // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========

  // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {}

  render() {
    return (
      <section className="section3 pb-0">
        <div className="container clearfix">
          <div className="col-parallex">
            <div className="landing__set landing__setcharacter">
              <div className="landing--container">
                <Parallax limitY={5} limitX={null}>
                  <div />
                  <div />
                  <div className="landing__layer landing__layer--f1">
                    <div className="landing__charactor">
                      <img
                        className="landing__l0"
                        src={Imglist.section3_character1}
                        alt=""
                      />
                    </div>
                  </div>
                </Parallax>
              </div>
            </div>
          </div>
          <div className="col-content text-center">
            <img src={Imglist.logo_ais} />
            <img src={Imglist.section3_text} />
            <div>
              <a
                className="btn btn-white"
                href="https://bns.garena.in.th/event/view/bstc-cosplay-contest-2019"
                target="_blank"
              >
                รายละเอียดการประกวด
              </a>
            </div>
          </div>
          <div className="col-parallex">
            <div className="landing__set landing__setcharacter">
              <div className="landing--container">
                <Parallax limitY={5} limitX={null}>
                  <div />
                  <div />
                  <div className="landing__layer landing__layer--f1">
                    <div className="landing__charactor">
                      <img
                        className="landing__l0"
                        src={Imglist.section3_character2}
                        alt=""
                      />
                    </div>
                  </div>
                </Parallax>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
const mapStateToProps = state => ({
  ...state.layout,
  ...state.AccountReducer,
  ...state.EventReducer
});
const mapDispatchToProps = { setValues };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Section3);
