
const initialState = {
    //--- event_info
    // condition_1_pass: true,
    // condition_1_received: true,
    // condition_2_pass: false,
    // condition_2_received: true,
    // type_id: 0,
    mypoint: 0,
    packages: [
      {
          can_receive: false,
          package_desc: "อัญมณี หินพิทักษ์ทะเลสาบ,หินโซล,หินจันทรา",
          package_id: 1,
          package_title: "Topup 30,000 Diamonds",
          received: false,
      },
      {
          can_receive: false,
          package_desc: "ไหมห้าสี,เกล็ดสีคราม,ลูกแก้วยอดนักรบ,กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์)",
          package_id: 2,
          package_title: "Topup 50,000 Diamonds",
          received: false,
      },
      {
          can_receive: false,
          package_desc: "ชุดท้องฟ้าไร้เมฆา,เครื่องประดับเมฆาลิขิต",
          package_id: 3,
          package_title: "Topup 100,000 Diamonds",
          received: false,
      }
    ]
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return {
          ...state,
          [action.key]: action.value
        };
      } else {
        return state;
      }
    case "SET_VALUES": return { ...state, ...action.value };
    default:
      return state;
  }
};

export const setValue  = (key, value) =>{ return ({ type: "SET_VALUE", value, key }) };
export const setValues = (value) =>{ return ({ type: "SET_VALUES", value }) };
