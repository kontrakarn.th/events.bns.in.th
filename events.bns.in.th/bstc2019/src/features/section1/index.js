import React from "react";
import { connect } from "react-redux";
import styled, { keyframes } from "styled-components";
import { setValues } from "./redux";
import { QrModal } from "../../features/modal";
import { Parallax } from "../../features/pattern";
import { Imglist } from "./../../constants/Import_Images";
import {
  onAccountLogin,
  onAccountLoginWalkin,
  onAccountLogoutWalkin,
  onAccountLogout
} from "./../../actions/AccountActions";

class Section1 extends React.Component {
  // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========

  // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
  constructor(props) {
    super(props);

    this.state = {
      modalLoading: false,
      modalShowMessage: false,
      modalMessage: "",
      qrcode: "",
      username: "",
      first: true
    };
  }

  componentWillMount() {
    this.setState({
      qrcode: this.props.qrcode,
      username: this.props.username
    });
  }
  componentWillUpdate(nextProps, nextState) {
    if (this.props.type == "qrcode") {
      if (nextProps.qrcode !== nextState.qrcode && this.state.first == true) {
        this.props.setValues({
          modal_open: "qr",
          qrcode: this.props.qrcode
        });

        this.setState({
          first: false
        });
      }
    }
    if (nextProps.autoLogout == true) {
      if (this.props.type == "qrcode") {
        this.props.onAccountLogout();
      } else {
        this.props.onAccountLogoutWalkin();
      }
    }
  }

  render() {
    return (
      <section className="section1">
        <div className="container clearfix">
          {/* col-content */}
          <div className="col-content">
            <img src={Imglist.logo_ais} />
            <img src={Imglist.section1_text} />
            <div>
              {this.props.qrcode == "" ? (
                <a
                  className="btn btn-white"
                  href="https://bns.garena.in.th/tournament/view/bstc-2019-main-event"
                  target="_blank"
                >
                  รายละเอียดงาน
                </a>
              ) : (
                <div
                  className="btn btn-white"
                  onClick={() =>
                    this.props.setValues({
                      modal_open: "qr",
                      qrcode: this.props.qrcode
                    })
                  }
                >
                  QR code ของคุณ
                </div>
              )}

              {this.props.jwtToken == "" ? (
                <a
                  href={
                    this.props.type == "login"
                      ? this.props.loginUrlWalkin
                      : this.props.loginUrl
                  }
                  className="btn btn-white"
                >
                  ลงทะเบียนเข้างาน
                </a>
              ) : this.props.type == "login" ? (
                <a
                  className="btn btn-white"
                  onClick={() => this.props.onAccountLogoutWalkin()}
                >
                  logout
                </a>
              ) : (
                <a
                  className="btn btn-white"
                  onClick={() => this.props.onAccountLogout()}
                >
                  logout
                </a>
              )}
            </div>
          </div>
          {/* end col-content */}

          {/* col-parallex */}
          <div className="col-parallex">
            <div className="landing__set landing__setcharacter">
              <div className="landing--container">
                <Parallax limitY={5} limitX={null}>
                  <div />
                  <div />
                  <div className="landing__layer landing__layer--f1">
                    <img
                      className="landing__l0"
                      src={Imglist.section1_character}
                      alt=""
                    />
                  </div>
                </Parallax>
              </div>
            </div>
          </div>
          {/* end col-parallex */}
          <div className="scrolldown">
            <img src={Imglist.scroll} />
          </div>
        </div>
        <QrModal qrcode={this.state.qrcode} username={this.state.username} />
      </section>
    );
  }
}
const mapStateToProps = state => ({
  ...state.layout,
  ...state.AccountReducer
  // ...state.EventReducer
});
const mapDispatchToProps = {
  setValues,
  onAccountLogout,
  onAccountLogoutWalkin,
  onAccountLogin,
  onAccountLoginWalkin
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Section1);
