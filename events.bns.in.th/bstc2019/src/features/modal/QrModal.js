import React from "react";
import ModalCore from "./ModalCore.js";
import styles from "./styles.module.scss";
import { connect } from "react-redux";
import { setValues, actCloseModal } from "./redux";
import { Imglist } from "./../../constants/Import_Images";

// import QRCode from "react-qrcode-logo";
import QRCode from "qrcode-react";

class QrModal extends React.Component {
  // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========

  // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
  constructor(props) {
    super(props);

    this.state = {
      qrcode: "",
      username: ""
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.qrcode !== prevProps.qrcode) {
      this.setState({ qrcode: this.props.qrcode });
    }
    if (this.props.username !== prevProps.username) {
      this.setState({ username: this.props.username });
    }
  }

  render() {
    return (
      <ModalCore name="qr" outSideClick={true}>
        <div className={[styles.contentwrap].join(" ")}>
          <div className={styles.contentwrap__bg}>
            <div className={styles.contentwrap__fontregis}>
              ลงทะเบียนล่วงหน้าสำเร็จ <br />
            </div>
            กรุณากดบันทึก QR Code เพื่อใช้ลงทะเบียนที่หน้างาน
            <div className={styles.bgqr}>
              <div className={styles.contentInner} id="qrcode">
                <QRCode
                  value={this.state.qrcode}
                  // value="career.seathailand.com/gmap"
                  size={250}
                  // logoWidth={1.5}
                  // logoHeight={1.5}
                  // logo="https://static2.garena.in.th/data/career/career/logo/logo_garena_non_transparent/20190821/bce3d7803c298066e1207c1194c40b30.jpg"
                  // logo="https://cdngarenanow-a.akamaihd.net/webth/garena/logo-garena-white-icon-32.png"
                  // logo="https://static2.garena.in.th/data/career/career/logo/logo_sea_non_transparent/20190821/efe4f1b6994a75c9f004e6d27509610a.jpg"
                />
              </div>
            </div>
            <div className={[styles.btnbox].join(" ")}>
              <a id="download">
                <div
                  className={styles.btn}
                  onClick={() => {
                    var download = document.getElementById("download");
                    var iOS =
                      !!navigator.platform &&
                      /iPad|iPhone|iPod/.test(navigator.platform);

                    if (iOS) {
                      let image = document
                        .getElementById("qrcode")
                        .childNodes[0].toDataURL("image/png");
                      window.open(image);
                    } else {
                      let image = document
                        .getElementById("qrcode")
                        .childNodes[0].toDataURL("image/png")
                        .replace("image/png", "image/octet-stream");
                      download.setAttribute("download", "qrcode.png");
                      download.setAttribute("href", image);
                      document.body.appendChild(download);
                      download.click();
                    }
                    // console.log("image :", image, "download :", download);
                    // window.open(image);
                  }}
                >
                  บันทึก
                </div>
              </a>
              <div
                className={styles.btn}
                onClick={() => this.props.setValues({ modal_open: "" })}
              >
                ปิด
              </div>
            </div>
          </div>
        </div>
      </ModalCore>
    );
  }
}

// const QrModal = props => {
//   console.log("props aaa:", props.qrcode);
//   return (
//     <ModalCore name="qr" outSideClick={true}>
//       <div className={[styles.contentwrap].join(" ")}>
//         <div className={styles.contentwrap__bg}>
//           <div className={styles.contentInner}>
//             <QRCode
//               value={props.qrcode}
//               size={280}
//               logo="https://cdngarenanow-a.akamaihd.net/webth/garena/logo-garena-white-icon-32.png"
//             />
//           </div>
//           <div className={[styles.btnbox].join(" ")}>
//             <div
//               className={styles.btn}
//               onClick={() => props.setValues({ modal_open: "" })}
//             >
//               บันทึก
//             </div>
//             <div
//               className={styles.btn}
//               onClick={() => props.setValues({ modal_open: "" })}
//             >
//               ปิด
//             </div>
//           </div>
//         </div>
//       </div>
//     </ModalCore>
//   );
// };

const mapStateToProps = state => ({
  ...state.Modal
});

const mapDispatchToProps = { setValues, actCloseModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QrModal);
