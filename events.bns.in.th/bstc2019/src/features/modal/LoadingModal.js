import React from 'react';
import  ModalCore from './ModalCore.js';
import styles from './styles.module.scss';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';

const LoadingModal = (props) => {
	return (
		<ModalCore name="loading" outSideClick={false}>
			<div className={styles.contentwrap}>
				<div className={styles.titlebox}>
					กรุณารอสักครู่...
				</div>
				<div className={styles.loading}>
					<div className={styles['loadingring']}></div>
				</div>
			</div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoadingModal);
