import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues, setDuoRequests } from '../../store/redux';
import imgList from '../../constants/ImportImages';
import { apiPut } from '../../constants/Api';

const ModalConfirmClaimReward = (props) => {
    const name = 'confirm_reject_duo';
    const apiAcceptDuoReceives = (row, e) => {
        let duoAcceptSuccessCallback = (res) => {
            if (res.status) {
                props.setDuoRequests({
                    ...res.data,
                    modal_open: 'message',
                    modal_message: 'ปฏิเสธคำเชิญแล้ว',
                });
            }
        };

        let duoAcceptFailCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };

        apiPut(
            'REACT_APP_API_POST_GET_DUO_RECEIVES',
            props.jwtToken,
            row.id,
            'reject',
            duoAcceptSuccessCallback,
            duoAcceptFailCallback
        );
    };

    let data = props.modal_reward_data;
    return (
        <ModalCore
            open={props.modal_open === name}
            onClick={() => props.setValues({ modal_open: '' })}
        >
            <ModalcontentStyle className='kanit_light_font'>
                <div className='message'>
                    <div>
                        ปฏิเสธคำเชิญ <span>{data.sender_name} ?</span>
                    </div>
                </div>
                <div className='buttons'>
                    <a
                        className='btn btn--confirm'
                        onClick={() =>
                            apiAcceptDuoReceives(props.modal_reward_data)
                        }
                    />
                    <a
                        className='btn btn--cancel'
                        onClick={() => props.setValues({ modal_open: '' })}
                    />
                </div>
            </ModalcontentStyle>
        </ModalCore>
    );
};

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues, setDuoRequests };
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ModalConfirmClaimReward);

const ModalcontentStyle = styled.div`
    position: relative;
    display: block;
    width: 417px;
    height: 308px;
    padding: 78px 10px 0px;
    background-image: url(${imgList.confirm_modal});
    .message {
        color: #ffffff;
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 55%;
        text-align: center;
        span {
            color: #d19e56;
        }
        img {
            vertical-align: middle;
        }
    }
    .buttons {
        position: absolute;
        left: 0px;
        bottom: 40px;
        display: block;
        width: 100%;
        heihgt: 42px;
        text-align: center;
    }
    .btn {
        display: inline-block;
        width: 96px;
        height: 38px;
        margin: 0px 20px;
        background-position: top center;
        &--confirm {
            background-image: url(${imgList.btn_confirm});
        }
        &--cancel {
            background-image: url(${imgList.btn_cancel});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;
