import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from '../../constants/ImportImages';

const ModalLoading = (props) => {
    const name = 'message';
    const actClose = () => {
        if (!props.selected_char && props.characters.length !== 0) {
            props.setValues({ modal_open: 'character' });
        } else {
            props.setValues({ modal_open: '' });
        }
    };
    return (
        <ModalCore open={props.modal_open === name} onClick={() => actClose()}>
            <ModalcontentStyle className='kanit_light_font'>
                <div className='message'>
                    <div
                        className='data_cell'
                        dangerouslySetInnerHTML={{
                            __html: props.modal_message,
                        }}
                    />
                </div>

                <div className='buttons'>
                    <a
                        className='btn btn--confirm'
                        onClick={() => actClose()}
                    />
                </div>
            </ModalcontentStyle>
        </ModalCore>
    );
};

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues };
export default connect(mapStateToProps, mapDispatchToProps)(ModalLoading);

const ModalcontentStyle = styled.div`
    position: relative;
    display: block;
    width: 417px;
    height: 308px;
    padding: 78px 10px 0px;
    background-image: url(${imgList.message_modal});
    .message {
        color: #ffffff;
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        margin: 0 5px;
        height: 55%;
        text-align: center;
        .data_cell {
        }
        span {
            color: #d19e56;
        }
    }
    .buttons {
        position: absolute;
        left: 0px;
        bottom: 40px;
        display: block;
        width: 100%;
        height: 42px;
        text-align: center;
    }
    .btn {
        display: inline-block;
        width: 96px;
        height: 38px;
        margin: 0px 20px;
        background-position: top center;
        &--confirm {
            background-image: url(${imgList.btn_confirm});
        }
        &--cancel {
            background-image: url(${imgList.btn_cancel});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;
