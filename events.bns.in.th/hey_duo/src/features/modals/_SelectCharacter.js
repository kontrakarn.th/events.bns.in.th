import React, { useState } from 'react';
import { connect } from 'react-redux';
import ScrollArea from 'react-scrollbar';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from '../../constants/ImportImages';
import { apiPost } from './../../constants/Api';

const ModalLoading = (props) => {
    const name = 'character';
    const [slected, setSlected] = useState(-1);
    const [show, setShow] = useState(false);

    const actSelected = (index, id) => {
        setSlected(index);
    };
    const actSelectCharacter = () => {
        if (slected < 0) {
            props.setValues({
                modal_open: 'message',
                modal_message: 'โปรดเลือกตัวละคร',
                selected_char: false,
            });
        } else {
            apiSelectCharacter(props.characters[slected].id);
        }
    };
    const apiSelectCharacter = (id) => {
        let dataSend = { type: 'select_character', id };
        let successCallback = (res) => {
            if (res.status) {
                props.setValues({
                    ...res.data,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        apiPost(
            'REACT_APP_API_POST_SELECT_CHAR',
            props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );
    };
    let char_name = props.characters[slected]
        ? props.characters[slected].char_name
        : '';
    return (
        <ModalCore
            open={props.modal_open === name}
            onClick={() => props.setValues({ modal_open: '' })}
        >
            <ModalcontentStyle
                className='kanit_light_font'
                showList={show}
                selected={slected < 0}
            >
                <div className='select' onClick={() => setShow(!show)}>
                    <div className='select__name'>
                        {slected < 0 ? 'โปรดเลือกตัวละคร' : char_name}
                    </div>
                    <img
                        className='select__icon'
                        src={imgList.icon_down_arrow}
                        alt=''
                    />
                    <ScrollArea
                        peed={0.8}
                        className='select__list'
                        contentClassName=''
                        horizontal={false}
                    >
                        <ul className=''>
                            {props.characters &&
                                props.characters.map((item, index) => {
                                    return (
                                        <li
                                            key={index}
                                            className='select__slot'
                                            onClick={() =>
                                                actSelected(index, item.char_id)
                                            }
                                        >
                                            {item.char_name}
                                        </li>
                                    );
                                })}
                        </ul>
                    </ScrollArea>
                </div>
                <div className='buttons'>
                    <a
                        className='btn btn--confirm'
                        onClick={() => actSelectCharacter()}
                    />
                </div>
            </ModalcontentStyle>
        </ModalCore>
    );
};

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues };
export default connect(mapStateToProps, mapDispatchToProps)(ModalLoading);

const ModalcontentStyle = styled.div`
    position: relative;
    display: block;
    width: 417px;
    height: 308px;
    padding: 90px 10px 0px;
    background-image: url(${imgList.select_character_modal});

    .select {
        position: relative;
        color: #ffffff;
        font-size: 20px;
        display: flex;
        width: 350px;
        justify-content: space-between;
        align-items: center;
        background-color: #000000;
        pointer-events: ${(props) => (props.showList ? 'none' : 'all')};
        margin: 0px auto;
        cursor: pointer;
        &__name {
            padding: 0px 22.5px;
        }
        &__icon {
        }
        &__list {
            z-index: 1;
            position: absolute;
            top: 75px;
            left: 0px;
            display: block;
            width: 100%;
            max-height: 300px;
            background-color: #000000;

            opacity: ${(props) => (props.showList ? '1' : '0')};
            pointer-events: ${(props) => (props.showList ? 'all' : 'none')};
        }
        &__slot {
            padding: 22.5px;
            background-color: #000000;
            color: #ffffff;
            &:hover {
                color: #000000;
                background-color: #ffffff;
            }
        }
    }
    .buttons {
        position: absolute;
        left: 0px;
        bottom: 40px;
        display: block;
        width: 100%;
        height: 42px;
        text-align: center;
    }
    .btn {
        display: inline-block;
        width: 96px;
        height: 38px;
        margin: 0px 20px;
        background-position: top center;
        &--confirm {
            background-image: url(${imgList.btn_confirm});
        }
        &--cancel {
            background-image: url(${imgList.btn_cancel});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;
