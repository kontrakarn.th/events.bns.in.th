import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { UseFormInput } from '../../components';

import ModalCore from './ModalCore';
import { setValues, setDuoRequests } from './../../store/redux';
import imgList from '../../constants/ImportImages';
import { apiPost } from './../../constants/Api';

const ModalRegisterCode = (props) => {
    const name = 'register_code';
    const [codeFriend, setCodeFriend] = useState({});

    const code = UseFormInput(codeFriend && codeFriend.code);

    const submitForm = () => {
        props.setValues({
            modal_open: 'loading',
        });

        let dataSend = { code: code.value };

        let successCallback = (res) => {
            if (res.status) {
                if (res.data) {
                    props.setValues({
                        ...res.data,
                        modal_open: 'confirm_request_duo',
                        modal_message: res.message,
                        duo_code: dataSend,
                    });
                } else {
                    props.setValues({
                        ...res.data,
                        modal_open: '',
                    });
                }
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };

        let failCallback = (res) => {
            if (res.status === false) {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };

        apiPost(
            'REACT_APP_API_POST_DUO_CONFIRM',
            props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );
    };

    return (
        <ModalCore
            open={props.modal_open === name}
            onClick={() => props.setValues({ modal_open: '' })}
        >
            <ModalcontentStyle className='kanit_light_font'>
                <InputCode modal_open={props.modal_open}>
                    <div className='input_container'>
                        <input {...code} name='code' autocomplete='off' />
                    </div>
                </InputCode>
                <div className='buttons'>
                    <a
                        className='btn btn--confirm'
                        onClick={() => submitForm()}
                    />
                </div>
            </ModalcontentStyle>
        </ModalCore>
    );
};

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues, setDuoRequests };
export default connect(mapStateToProps, mapDispatchToProps)(ModalRegisterCode);

const InputCode = styled.div`
    display: ${(props) =>
        props.modal_open === 'register_code'
            ? 'flex!important'
            : 'none !important'};
    position: relative;
    color: #ffffff;
    font-size: 20px;
    display: flex;
    width: 356px;
    height: 73px;
    justify-content: space-between;
    align-items: center;
    background-color: #000000;
    pointer-events: ${(props) => (props.showList ? 'none' : 'all')};
    margin: 0px auto;
    &__name {
        padding: 0px 22.5px;
    }
    &__icon {
    }
    &__list {
        z-index: 1;
        position: absolute;
        top: 75px;
        left: 0px;
        display: block;
        width: 100%;
        max-height: 300px;
        background-color: #000000;

        opacity: ${(props) => (props.showList ? '1' : '0')};
        pointer-events: ${(props) => (props.showList ? 'all' : 'none')};
    }
    &__slot {
        padding: 22.5px;
        background-color: #000000;
        color: #ffffff;
        &:hover {
            color: #000000;
            background-color: #ffffff;
        }
    }
`;

const ModalcontentStyle = styled.div`
    position: relative;
    display: block;
    width: 417px;
    height: 308px;
    padding: 100px 10px 0px;
    background-image: url(${imgList.register_friend_modal});

    .input_container {
        margin: 1rem;
        display: flex;
        flex: 1;
        justify-content: space-between;
        align-items: center;

        input {
            border: none;
            color: #f5f5f5;
            background: none;
            width: 100%;
            padding: 0.5rem;
            text-align: left;
            font-size: 20px;

            &:focus {
                outline: none;
            }
        }
    }

    .input_code {
        display: ${(props) =>
            props.modal_open === 'modal_open' ? '' : 'none !important'};
        position: relative;
        color: #ffffff;
        font-size: 20px;
        display: flex;
        width: 356px;
        height: 73px;
        justify-content: space-between;
        align-items: center;
        background-color: #000000;
        pointer-events: ${(props) => (props.showList ? 'none' : 'all')};
        margin: 0px auto;
        &__name {
            padding: 0px 22.5px;
        }
        &__icon {
        }
        &__list {
            z-index: 1;
            position: absolute;
            top: 75px;
            left: 0px;
            display: block;
            width: 100%;
            max-height: 300px;
            background-color: #000000;

            opacity: ${(props) => (props.showList ? '1' : '0')};
            pointer-events: ${(props) => (props.showList ? 'all' : 'none')};
        }
        &__slot {
            padding: 22.5px;
            background-color: #000000;
            color: #ffffff;
            &:hover {
                color: #000000;
                background-color: #ffffff;
            }
        }
    }
    .buttons {
        position: absolute;
        left: 0px;
        bottom: 40px;
        display: block;
        width: 100%;
        height: 42px;
        text-align: center;
    }
    .btn {
        display: inline-block;
        width: 96px;
        height: 38px;
        margin: 0px 20px;
        background-position: top center;
        &--confirm {
            background-image: url(${imgList.btn_confirm});
        }
        &--cancel {
            background-image: url(${imgList.btn_cancel});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;
