import React, { useState } from 'react';
import { connect } from 'react-redux';
import ScrollArea from 'react-scrollbar';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import imgList from '../../constants/ImportImages';
import { apiPost } from '../../constants/Api';

const ModalLoading = (props) => {
    const name = 'turban';

    return (
        <ModalCore
            open={props.modal_open === name}
            onClick={() => props.setValues({ modal_open: '' })}
        >
            <ModalcontentStyle className='kanit_light_font'>
                <a
                    className='close'
                    onClick={() => props.setValues({ modal_open: '' })}
                />
            </ModalcontentStyle>
        </ModalCore>
    );
};

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues };
export default connect(mapStateToProps, mapDispatchToProps)(ModalLoading);

const ModalcontentStyle = styled.div`
    position: relative;
    display: block;
    width: 760px;
    height: 684px;
    padding: 78px 10px 0px;
    background-image: url(${imgList.turban_modal});
    .clase {
    }
    .close {
        position: absolute;
        right: 15px;
        top: 15px;
        display: flex;
        width: 36px;
        height: 36px;
        background-image: url(${imgList.icon_cross});
    }
`;
