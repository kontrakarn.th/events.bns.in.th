import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues, setDuoRequests } from '../../store/redux';
import imgList from '../../constants/ImportImages';
import { apiPost } from '../../constants/Api';

const ModalConfirmRequestDuo = (props) => {
    const name = 'confirm_request_duo';
    const apiAcceptDuoReceives = () => {
        props.setValues({
            modal_open: 'loading',
        });
        let successCallback = (res) => {
            if (res.status) {
                props.setDuoRequests({
                    ...res.data,
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };

        let failCallback = (res) => {
            console.log(res, 'duoAcceptFailCallback');
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };

        apiPost(
            'REACT_APP_API_POST_REQUSET_DUO',
            props.jwtToken,
            props.duo_code,
            successCallback,
            failCallback
        );
    };

    return (
        <ModalCore open={props.modal_open === name}>
            <ModalcontentStyle className='kanit_light_font'>
                <div className='message'>
                    <div
                        className='data_cell'
                        dangerouslySetInnerHTML={{
                            __html: props.modal_message,
                        }}
                    />
                </div>
                <div className='buttons'>
                    <a
                        className='btn btn--confirm'
                        onClick={() => apiAcceptDuoReceives()}
                    />
                    <a
                        className='btn btn--cancel'
                        onClick={() => props.setValues({ modal_open: '' })}
                    />
                </div>
            </ModalcontentStyle>
        </ModalCore>
    );
};

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues, setDuoRequests };
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ModalConfirmRequestDuo);

const ModalcontentStyle = styled.div`
    position: relative;
    display: block;
    width: 417px;
    height: 308px;
    padding: 78px 10px 0px;
    background-image: url(${imgList.confirm_modal});
    .message {
        color: #ffffff;
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 55%;
        text-align: center;
        span {
            color: #d19e56;
        }
        img {
            vertical-align: middle;
        }
    }
    .buttons {
        position: absolute;
        left: 0px;
        bottom: 40px;
        display: block;
        width: 100%;
        heihgt: 42px;
        text-align: center;
    }
    .btn {
        display: inline-block;
        width: 96px;
        height: 38px;
        margin: 0px 20px;
        background-position: top center;
        &--confirm {
            background-image: url(${imgList.btn_confirm});
        }
        &--cancel {
            background-image: url(${imgList.btn_cancel});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;
