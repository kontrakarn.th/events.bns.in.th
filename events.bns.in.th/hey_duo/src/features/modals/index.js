import React from 'react';

import ModalMessage from './_Message';
import ModalLoading from './_Loading';
import ModalSelectCharacter from './_SelectCharacter';
import ModalDetailTurban from './_DetailTurban';
import ModalDetailTowelSet from './_DetailTowelSet';
import ModalPurchasePackage from './_ConfirmPurchasePackage';
import ModalConfirmClaimPurchasePoint from './_ConfirmClaimPurchasePoint';
import ModalConfirmClaimReward from './_ConfirmClaimReward';
import ModalConfirmOpenBox from './_ConfirmOpenBox';
import ModalRegisterCode from './_RegisterCode';
import ModalConfirmExchangeSpecialReward from './_ConfirmExchangeSpecialReward';
import ModalConfirmAcceptDuo from './_ConfirmAcceptDuo';
import ModalConfirmRejectDuo from './_ConfirmRejectDuo';
import ModalConfirmRejectDuoRequest from './_ConfirmRejectDuoRequest';
import ModalConfirmRequestDuo from './_ConfirmRequestDuo';

export default (props) => (
    <>
        <ModalLoading />
        <ModalMessage />
        <ModalSelectCharacter />
        <ModalDetailTurban />
        <ModalDetailTowelSet />
        <ModalPurchasePackage />
        <ModalConfirmClaimPurchasePoint />
        <ModalConfirmClaimReward />
        <ModalConfirmOpenBox />
        <ModalRegisterCode />
        <ModalConfirmExchangeSpecialReward />
        <ModalConfirmAcceptDuo />
        <ModalConfirmRejectDuo />
        <ModalConfirmRejectDuoRequest />
        <ModalConfirmRequestDuo />
    </>
);
