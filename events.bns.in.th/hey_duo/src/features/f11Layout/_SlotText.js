import React from 'react';
import styled from 'styled-components';

export default styled.div`
    pointer-events: auto;
    display: inline-block;
    padding: 0px 25px;
    margin-left: 17px;
    /* width: fit-content; */
    /* max-width: 170px; */
    text-align: center;
    height: 45px;
    line-height: 45px;
    background-color: ${(props) =>
        props.color ? props.color : 'rgba(0, 0, 0, 0.7)'};
    border-radius: 15px;
    color: #fff;
    font-size: 20px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    /* transition: width 0.3s ease-in-out; */
`;
