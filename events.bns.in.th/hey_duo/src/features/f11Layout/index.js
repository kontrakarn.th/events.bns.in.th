import React, { useState } from 'react';
import { connect } from 'react-redux';
import { setValues, setLogoutUrl } from './../../store/redux';
import ScrollArea from 'react-scrollbar';
import styled from 'styled-components';
import Permission from './_Permission';
import BtnHome from './_BtnHome';
import IconScroll from './_IconScroll';
import Menu from './_Menu';
import SlotText from './_SlotText';

const LayoutF11 = (props) => {
    const [showScroll, setShowScroll] = useState(true);
    let nameText = [];
    if (props.username !== '') nameText.push(props.username);
    if (props.character_name !== '') nameText.push(props.character_name);

    const handleScroll = (hide) => {
        if (props.hideScroll) {
            if (showScroll) setShowScroll(false);
        } else {
            if (showScroll !== !hide) setShowScroll(!hide);
        }
    };

    if (!props.permission) return <Permission />;

    return (
        <Layout background={props.background}>
            <OverAll>
                <BtnHome to='/' />
                {showScroll && <IconScroll />}
                {props.showMenu && (
                    <Menu duo={props.duo} list={props.menu} page={props.page} />
                )}
                <div className='topright'>
                    {props.duo && (
                        <SlotText color='#b11e49'>
                            คู่หู คู่ซี้ : {props.duo.char_name}
                        </SlotText>
                    )}

                    {nameText.length > 0 && (
                        <SlotText>{nameText.join(' : ')}</SlotText>
                    )}
                    {/* <SlotText>logout</SlotText> */}
                </div>
            </OverAll>

            <ScrollArea
                speed={0.8}
                horizontal={false}
                vertical={props.modal_open === ''}
                onScroll={(value) => handleScroll(value.topPosition > 10)}
                style={{ width: '100%', height: 700, opacity: 1 }}
                verticalScrollbarStyle={{ backgroundColor: '#000', opacity: 1 }}
            >
                {props.children && props.children}
            </ScrollArea>
        </Layout>
    );
};

const mstp = (state) => ({ ...state });
const mdtp = { setValues, setLogoutUrl };
export default connect(mstp, mdtp)(LayoutF11);

const Layout = styled.div`
    position: relative;
    display: block;
    width: 1105px;
    height: 700px;
    max-width: 100vw;
    max-height: 100vh;
    overflow: hidden;
    background: no-repeat top center url(${(props) => props.background});
    font-family: 'Kanit-Regular', tahoma;
    .topright {
        right: 22px;
        top: 18px;
        position: absolute;
    }
`;
const OverAll = styled.div`
    z-index: 100;
    position: absolute;
    top: 0px;
    left: 0px;
    display: block;
    width: 100%;
    height: 100%;
    pointer-events: none;
`;
// background: no-repeat center url(${props => props.src}) #c5c0ad;
