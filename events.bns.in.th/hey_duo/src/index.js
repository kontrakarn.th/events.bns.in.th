import 'core-js/es6/map';
import 'core-js/es6/set';
import 'raf/polyfill';
import 'cross-fetch/polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
// import {BrowserRouter} from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';

import Routes from './routes';
// import serviceWorker from './serviceWorker';
import ReduxStore from './store/redux';
import './styles/index.scss';

// const rootElement = document.getElementById('root');
ReactDOM.render(
    <Provider store={createStore(ReduxStore, applyMiddleware(logger))}>
        <Routes />
    </Provider>,
    document.getElementById('root')
);
