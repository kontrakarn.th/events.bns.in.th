import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';
import styled from 'styled-components';
import { connect } from 'react-redux';

import imgList from './../constants/ImportImages';
import { setValues, setDuoRequests } from './../store/redux';
import { apiDelete } from './../constants/Api';

const DataTableSended = (props) => {
    const columns = [
        {
            name: 'รหัส คู่หู คู่ซี้',
            selector: 'receiver_code',
        },
        {
            name: 'ชื่อ',
            selector: 'receiver_name',
            width: '216px',
        },
        {
            name: 'LV. / ฮงมุนLV.',
            selector: 'receiver_level',
        },
        {
            name: 'SV.',
            selector: 'receiver_world',
        },
        {
            name: 'สถานะ',
            selector: 'status',
            cell: (row) => (
                <span
                    style={{
                        color: row.status === 'accepted' ? '#84ff95' : '#fff',
                    }}
                >
                    {row.status === 'accepted'
                        ? 'ตอบรับ'
                        : row.status === 'pending'
                        ? 'รอ'
                        : row.status === 'rejected'
                        ? 'ปฏิเสธ'
                        : ''}
                </span>
            ),
        },
        {
            name: '',
            selector: 'id',
            cell: (row) => (
                <ActionButton>
                    <BtnStatus
                        status={row.status}
                        onClick={() => rejectDuoRequests(row)}
                    ></BtnStatus>
                </ActionButton>
            ),
        },
    ];

    const rejectDuoRequests = (row) => {
        props.setValues({
            modal_open: 'confirm_reject_duo_requests',
            modal_reward_data: row,
        });
    };

    return (
        <div className='App'>
            <DataTableCustom
                noDataComponent={false}
                columns={columns}
                data={props.duo_requests}
            />
        </div>
    );
};

const DataTableCustom = styled(DataTable)`
    .rdt_TableHeader {
        display: none;
        font-size: 16px;
    }
    .rdt_TableHeadRow {
        border: none;
        min-height: 115px;
        font-size: 16px;
    }
    .rdt_TableHead {
        background-color: #0f0907;
        font-size: 16px;
    }
    .rdt_TableCol {
        color: #ffe04f;
        background-color: #0f0907;
        justify-content: center;
        font-size: 16px;
    }
    .rdt_TableCell {
        color: #fff;
        background-color: #0f0907;
        justify-content: center;
        font-size: 16px;
    }
    .rdt_TableRow {
        border: none;
        font-size: 16px;
    }
`;

const BtnStatus = styled.div`
    width: 70px;
    height: 28px;

    &:nth-child(1) {
        margin-right: 9px;
        width: 70px;
        height: 28px;
        pointer-events: ${(props) =>
            props.status === 'accepted'
                ? 'none'
                : props.status === 'rejected'
                ? 'none'
                : props.status === 'pending'
                ? 'auto'
                : ''};
        background-image: url('${(props) =>
            props.status === 'accepted'
                ? imgList.btn_delete_request_inactive
                : props.status === 'rejected'
                ? imgList.btn_delete_request_inactive
                : props.status === 'pending'
                ? imgList.btn_reject_sender
                : ''}');
        background-position: top center;
        &:hover {
            background-position: bottom center;
        }
    }
`;

const ActionButton = styled.div`
    display: flex;
    justify-content: center;
    .btn {
        width: 70px;
        height: 28px;

        &:nth-child(1) {
            background-image: url(${imgList.btn_reject_sender});
            background-position: top center;
            &:hover {
                background-position: bottom center;
            }
        }
    }
`;

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues, setDuoRequests };

export default connect(mapStateToProps, mapDispatchToProps)(DataTableSended);
