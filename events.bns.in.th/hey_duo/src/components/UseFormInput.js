import { useState, useEffect } from 'react';

export default function useFormInput(initialValue) {
    const [value, setValue] = useState(initialValue);

    useEffect(() => {
        setValue(initialValue);
    }, [initialValue]);

    function handleChange({ target }) {
        setValue(target.value);
    }

    return {
        value,
        onChange: handleChange,
    };
}
