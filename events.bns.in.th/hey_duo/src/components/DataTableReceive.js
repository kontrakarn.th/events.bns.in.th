import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { setValues } from './../store/redux';
import imgList from '../constants/ImportImages';
import { apiPut } from './../constants/Api';

const DataTableReceive = (props) => {
    console.log('DataTableReceive', props);
    const columns = [
        {
            name: 'รหัส คู่หู คู่ซี้',
            selector: 'sender_code',
        },
        {
            name: 'ชื่อ',
            selector: 'sender_name',
            width: '216px',
        },
        {
            name: 'LV. / ฮงมุนLV.',
            selector: 'sender_level',
        },
        {
            name: 'SV.',
            selector: 'sender_world',
        },
        {
            name: 'สถานะ',
            selector: 'status',
            cell: (row) => (
                <span
                    style={{
                        color: row.status === 'accepted' ? '#84ff95' : '#fff',
                    }}
                >
                    {row.status === 'accepted'
                        ? 'ตอบรับ'
                        : row.status === 'pending'
                        ? 'รอ'
                        : row.status === 'rejected'
                        ? 'ปฏิเสธ'
                        : ''}
                </span>
            ),
        },
        {
            name: '',
            width: '216px',
            selector: 'id',
            cell: (row) => (
                <ActionButtonGrup>
                    {props.duo === null ? (
                        <ActionButton>
                            <BtnStatus
                                status={row.status}
                                register_status={props.register_status}
                                onClick={() => acceptDuoReceives(row)}
                            ></BtnStatus>
                            <BtnStatus
                                status={row.status}
                                register_status={props.register_status}
                                onClick={() => rejectDuoReceives(row)}
                            ></BtnStatus>
                        </ActionButton>
                    ) : (
                        <ActionButton>
                            <BtnStatusInActive
                                status={row.status}
                                register_status={props.register_status}
                                onClick={() => acceptDuoReceives(row)}
                            ></BtnStatusInActive>
                            <BtnStatusInActive
                                status={row.status}
                                register_status={props.register_status}
                                onClick={() => rejectDuoReceives(row)}
                            ></BtnStatusInActive>
                        </ActionButton>
                    )}
                </ActionButtonGrup>
            ),
        },
    ];
    const acceptDuoReceives = (row) => {
        props.setValues({
            modal_open: 'confirm_accept_duo',
            modal_reward_data: row,
        });
    };

    const rejectDuoReceives = (row) => {
        props.setValues({
            modal_open: 'confirm_reject_duo',
            modal_reward_data: row,
        });
    };

    return (
        <div className='App'>
            <DataTableCustom
                noDataComponent={false}
                columns={columns}
                data={props.duo_receives}
            />
        </div>
    );
};

const DataTableCustom = styled(DataTable)`
    .rdt_TableHeader {
        display: none;
        font-size: 16px;
    }
    .rdt_TableHeadRow {
        border: none;
        min-height: 115px;
        font-size: 16px;
    }
    .rdt_TableHead {
        background-color: #0f0907;
        font-size: 16px;
    }
    .rdt_TableCol {
        color: #ffe04f;
        background-color: #0f0907;
        justify-content: center;
        font-size: 16px;
    }
    .rdt_TableCell {
        color: #fff;
        background-color: #0f0907;
        justify-content: center;
        font-size: 16px;
    }
    .rdt_TableRow {
        border: none;
        font-size: 16px;
    }
`;

const ActionButtonGrup = styled.div`
    display: flex;
`;

const BtnStatusInActive = styled.div`
    width: 70px;
    height: 28px;

    &:nth-child(1) {
        margin-right: 9px;
        width: 70px;
        height: 28px;
        pointer-events: none;
        background-image: url(${imgList.btn_reject_inactive});
        background-position: top center;
        &:hover {
            background-position: bottom center;
        }
    }
    &:nth-child(2) {
        margin-right: 9px;
        pointer-events: none;
        background-image: url(${imgList.btn_reject_inactive});
        background-position: top center;
        &:hover {
            background-position: bottom center;
        }
    }
`;

const BtnStatus = styled.div`
    width: 70px;
    height: 28px;

    &:nth-child(1) {
        margin-right: 9px;
        width: 70px;
        height: 28px;
        pointer-events: ${(props) =>
            props.status === 'accepted'
                ? 'none'
                : props.status === 'rejected'
                ? 'none'
                : props.status === 'pending'
                ? 'auto'
                : ''};
        background-image: url('${(props) =>
            props.status === 'accepted'
                ? imgList.btn_accept_inactive
                : props.status === 'rejected'
                ? imgList.btn_accept_inactive
                : props.status === 'pending'
                ? imgList.btn_accept
                : ''}');
        background-position: top center;
        &:hover {
            background-position: bottom center;
        }
    }
    &:nth-child(2) {
        margin-right: 9px;
        pointer-events: ${(props) =>
            props.status === 'accepted'
                ? 'none'
                : props.status === 'rejected'
                ? 'none'
                : props.status === 'pending'
                ? 'auto'
                : props.register_status === 'completed_register'
                ? 'none'
                : ''};
        background-image: url('${(props) =>
            props.status === 'accepted'
                ? imgList.btn_reject_inactive
                : props.status === 'rejected'
                ? imgList.btn_reject_inactive
                : props.status === 'pending'
                ? imgList.btn_reject
                : props.register_status === 'completed_register'
                ? imgList.btn_reject_inactive
                : ''}');
        background-position: top center;
        &:hover {
            background-position: bottom center;
        }
    }
`;

const ActionButton = styled.div`
    display: flex;
    .btn {
        width: 70px;
        height: 28px;

        &:nth-child(1) {
            margin-right: 9px;
            background-image: url(${imgList.btn_accept});
            background-position: top center;
            &:hover {
                background-position: bottom center;
            }
        }
        &:nth-child(2) {
            margin-right: 9px;
            background-image: url(${imgList.btn_reject});
            background-position: top center;
            &:hover {
                background-position: bottom center;
            }
        }
    }
`;

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues };

export default connect(mapStateToProps, mapDispatchToProps)(DataTableReceive);
