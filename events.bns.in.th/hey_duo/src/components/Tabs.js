import React from 'react';
import styled from 'styled-components';

let hashKeys = {};
const Tabs = (props) => {
    const { activeKey, onSelect, children } = props;
    hashKeys[activeKey] = true;

    return (
        <Container>
            <TabsContainer>
                {React.Children.map(children, (child, i) => (
                    <Title
                        key={child.props.eventkey}
                        className={`tab-element ${activeKey ===
                            child.props.eventKey && 'active'}`}
                        onClick={() => {
                            onSelect(child.props.eventKey);
                        }}
                    >
                        {child.props.title}
                    </Title>
                ))}
            </TabsContainer>

            {React.Children.map(children, (child, i) => (
                <TabsPanel>
                    <div
                        key={child.props.eventKey}
                        className={`tab-panel ${child.props.eventKey !==
                            activeKey && 'hidden'}`}
                    >
                        {hashKeys.hasOwnProperty(child.props['eventKey']) &&
                            child}
                    </div>
                </TabsPanel>
            ))}
        </Container>
    );
};

export default Tabs;

const Container = styled.div`
    width: 953px;
    margin: 0 auto;
`;

const Title = styled.div`
    display: flex;
    justify-content: center;
    align-self: center;
    font-size: 20px;
`;

const TabsContainer = styled.div`
    display: flex;

    .tab-element {
        padding: 10px;
        color: #fff;
        cursor: pointer;
        background-color: #24160f;
        margin-right: 1px;
        width: 178px;
        height: 53px;

        &:hover {
            /* background-color: red; */
        }
    }

    .active {
        background-color: #0f0907;
    }
`;

const TabsPanel = styled.div`
    .tab-panel {
        height: 622px;
        background-color: #0f0907;
    }
    .hidden {
        display: none;
    }
`;
