import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { setValues } from '../store/redux';
import styled from 'styled-components';
import imgList from '../constants/ImportImages';
import F11Layout from '../features/f11Layout';
import Modals from './../features/modals';
import { apiPost } from './../constants/Api';

const Package = (props) => {
    const apiEventInfo = () => {
        let dataSend = { type: 'event_info' };
        let successCallback = (res) => {
            if (res.status) {
                props.setValues({
                    ...res.data,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        apiPost(
            'REACT_APP_API_POST_EVENT_INFO',
            props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );
    };
    // const apiPurchasePackage = () => {
    //   props.setValues({modal_open: "loading"});
    //   let dataSend = {type: "purchase_package"};
    //   let successCallback = (res) => {
    //     if(res.status) {
    //       props.setValues({
    //         ...res.data,
    //         modal_open:"message",
    //         modal_message: res.message,
    //       })
    //     } else {
    //       props.setValues({
    //           modal_open:"message",
    //           modal_message: res.message,
    //       });
    //     }
    //   }
    //   let failCallback = (res) => {
    //     if (res.type === 'no_permission') {
    //       props.setValues({
    //           permission: false,
    //           modal_open: "",
    //       });
    //     }else {
    //       props.setValues({
    //           modal_open:"message",
    //           modal_message: res.message,
    //       });
    //     }
    //   }
    //   apiPost("REACT_APP_API_POST_PURCHASE_PACKAGE", props.jwtToken, dataSend, successCallback, failCallback);
    // };
    const apiCliamFreePoint = () => {
        props.setValues({ modal_open: 'loading' });
        let dataSend = { type: 'claim_free_points' };
        let successCallback = (res) => {
            if (res.status) {
                props.setValues({
                    ...res.data,
                    modal_open: 'message',
                    modal_message: res.message,
                    history_point: [],
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        apiPost(
            'REACT_APP_API_POST_CLAIM_FREE_POINTS',
            props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );
    };
    // const apiCliamPurchasePoint = () => {
    //   props.setValues({modal_open: "loading"});
    //   let dataSend = {type: "claim_purchase_points"};
    //   let successCallback = (res) => {
    //     if(res.status) {
    //       props.setValues({
    //         ...res.data,
    //         modal_open:"message",
    //         modal_message: res.message,
    //       })
    //     } else {
    //       props.setValues({
    //           modal_open:"message",
    //           modal_message: res.message,
    //       });
    //     }
    //   }
    //   let failCallback = (res) => {
    //     if (res.type === 'no_permission') {
    //       props.setValues({
    //           permission: false,
    //           modal_open: "",
    //       });
    //     }else {
    //       props.setValues({
    //           modal_open:"message",
    //           modal_message: res.message,
    //       });
    //     }
    //   }
    //   apiPost("REACT_APP_API_POST_CLAIM_PURCHASE_POINTS", props.jwtToken, dataSend, successCallback, failCallback);
    // };
    const apiCliamSpecialQuestPoint = (id, type) => {
        props.setValues({ modal_open: 'loading' });
        let dataSend = { type: type, id };
        let successCallback = (res) => {
            if (res.status) {
                props.setValues({
                    ...res.data,
                    modal_open: 'message',
                    modal_message: res.message,
                    history_point: [],
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        apiPost(
            'REACT_APP_API_POST_CLAIM_SPECIAL_QUEST_POINTS',
            props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );
    };
    const apiOpenBox = () => {
        props.setValues({ modal_open: 'loading' });
        let dataSend = { type: 'open_box' };
        let successCallback = (res) => {
            if (res.status) {
                props.setValues({
                    ...res.data,
                    modal_open: 'message',
                    modal_message: res.message,
                    history_reward: [],
                    history_testure: [],
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        apiPost(
            'REACT_APP_API_POST_OPEN_BOX',
            props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );
    };
    const apiCliamReward = (id) => {
        props.setValues({ modal_open: 'loading' });
        let dataSend = { type: 'claim_reward', id };
        let successCallback = (res) => {
            if (res.status) {
                props.setValues({
                    ...res.data,
                    modal_open: '',
                    history_reward: [],
                    history_testure: [],
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        apiPost(
            'REACT_APP_API_POST_CLIAM_SPECIAL_REWARD',
            props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );
    };

    const actPurchasePackage = () => {
        props.setValues({ modal_open: 'confirm_package' });
    };
    const actCliamOpenBox = () => {
        props.setValues({ modal_open: 'confirm_open' });
    };
    const actCliamPurchasePoint = () => {
        props.setValues({ modal_open: 'confirm_claim' });
    };
    const actCliamReward = (data) => {
        props.setValues({
            modal_open: 'confirm_reward',
            modal_reward_data: data,
        });
    };
    const actExchangeSpecialReward = (data) => {
        props.setValues({
            modal_open: 'confirm_exchange_special_reward',
            modal_reward_data: data,
        });
    };

    useEffect(() => {
        if (props.jwtToken !== '' && !props.status) apiEventInfo();
    }, [props.jwtToken]);

    return (
        <F11Layout page={'package'}>
            <Modals />
            <PackageStyle>
                <div className='towels'>
                    คะแนนปัจจุบัน
                    <br />
                    {props.points}
                </div>
                <section className='quest'>
                    <div className='quest__head'></div>
                    <div className='quest__content'>
                        {props.daily_free_points_status === 'can_claim' ? (
                            <div
                                className='quest__btn quest__btn--3point'
                                onClick={() => apiCliamFreePoint()}
                            ></div>
                        ) : props.daily_free_points_status === 'claimed' ? (
                            <div className='quest__btn quest__btn--3point_inactive'></div>
                        ) : (
                            <div className='quest__btn quest__btn--3point_not_in_condition'></div>
                        )}
                        <div className='btn__group'>
                            <div className='content'>
                                {props.bosskill_quests &&
                                    props.bosskill_quests.map((data) => {
                                        return (
                                            <div className='box'>
                                                <GotScoreBtn
                                                    canClaim={data.status}
                                                    onClick={() =>
                                                        apiCliamSpecialQuestPoint(
                                                            data.id,
                                                            ''
                                                        )
                                                    }
                                                ></GotScoreBtn>
                                                <DuoScoreBtn
                                                    canClaim={data.duo_status}
                                                    onClick={() =>
                                                        apiCliamSpecialQuestPoint(
                                                            data.id,
                                                            'duo'
                                                        )
                                                    }
                                                ></DuoScoreBtn>
                                            </div>
                                        );
                                    })}
                            </div>
                        </div>
                        <div className='btn__group2'>
                            <div className='content'>
                                {props.area_quests &&
                                    props.area_quests.map((data) => {
                                        return (
                                            <div className='box'>
                                                <GotScoreBtn
                                                    canClaim={data.status}
                                                    onClick={() =>
                                                        apiCliamSpecialQuestPoint(
                                                            data.id,
                                                            ''
                                                        )
                                                    }
                                                ></GotScoreBtn>
                                                <DuoScoreBtn
                                                    canClaim={data.duo_status}
                                                    onClick={() =>
                                                        apiCliamSpecialQuestPoint(
                                                            data.id,
                                                            'duo'
                                                        )
                                                    }
                                                ></DuoScoreBtn>
                                            </div>
                                        );
                                    })}
                            </div>
                        </div>
                    </div>
                </section>

                <section className='quest2'>
                    <div className='quest2__content2'></div>
                    <div className='btn__group3'>
                        <div className='content'>
                            {props.special_rewards &&
                                props.special_rewards.claim_start_special.map(
                                    (data) => {
                                        return (
                                            <div className='box'>
                                                <RedeemItemBtn
                                                    onClick={() =>
                                                        actExchangeSpecialReward(
                                                            data
                                                        )
                                                    }
                                                    canClaim={data.can_claim}
                                                ></RedeemItemBtn>
                                            </div>
                                        );
                                    }
                                )}
                        </div>
                    </div>
                    <div className='btn__group4'>
                        <div className='content'>
                            {props.special_rewards &&
                                props.special_rewards.claim_limit_special.map(
                                    (data) => {
                                        return (
                                            <div className='box text__limit'>
                                                {data.limit_count}/{data.limit}
                                                <RedeemItemBtn
                                                    onClick={() =>
                                                        actExchangeSpecialReward(
                                                            data
                                                        )
                                                    }
                                                    canClaim={data.can_claim}
                                                ></RedeemItemBtn>
                                            </div>
                                        );
                                    }
                                )}
                        </div>
                    </div>
                </section>

                <section className='quest3'>
                    <div className='quest3__content3'></div>
                    <div className='btn__group5'>
                        <div className='content'>
                            {props.rewards &&
                                props.rewards.claim_start.map((data) => {
                                    return (
                                        <div className='box'>
                                            <div className='box'>
                                                <RedeemItemBtn
                                                    onClick={() =>
                                                        actCliamReward(data)
                                                    }
                                                    canClaim={data.can_claim}
                                                ></RedeemItemBtn>
                                            </div>
                                        </div>
                                    );
                                })}
                        </div>
                    </div>
                    <div className='btn__group6'>
                        <div className='content'>
                            {props.rewards &&
                                props.rewards.claim_limit.map((data) => {
                                    return (
                                        <div className='box text__limit'>
                                            {data.limit_count}/{data.limit}
                                            <RedeemItemBtn
                                                onClick={() =>
                                                    actCliamReward(data)
                                                }
                                                canClaim={data.can_claim}
                                            ></RedeemItemBtn>
                                        </div>
                                    );
                                })}
                        </div>
                    </div>
                    <div className='btn__group7'>
                        <div className='content'>
                            {props.rewards &&
                                props.rewards.claim_unlimit.map((data) => {
                                    return (
                                        <div className='box'>
                                            <RedeemItemBtn
                                                onClick={() =>
                                                    actCliamReward(data)
                                                }
                                                canClaim={data.can_claim}
                                            ></RedeemItemBtn>
                                        </div>
                                    );
                                })}
                        </div>
                    </div>
                </section>

                <section className='quest4'>
                    <div className='quest4__content4'></div>
                </section>
            </PackageStyle>
        </F11Layout>
    );
};

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues };
export default connect(mapStateToProps, mapDispatchToProps)(Package);

const RedeemItemBtn = styled.div`
    margin-top: 5px;
    width: 88;
    height: 28px;
    background-position: top center;
    background-repeat: no-repeat;
    cursor: pointer;
    pointer-events: ${(props) => (props.canClaim ? 'auto' : 'none')};
    background-image: url('${(props) =>
        props.canClaim
            ? imgList.btn_redeem_item
            : imgList.btn_exchange_inactive}');

    &:hover {
        background-position: bottom center;
    }
`;

const GotScoreBtn = styled.div`
    margin: 20px auto 15px;
    width: 88px;
    height: 28px;
    background-position: top center;
    background-repeat: no-repeat;
    cursor: pointer;
    pointer-events: ${(props) =>
        props.canClaim === 'can_claim' ? 'auto' : 'none'};

    background-image: url('${(props) =>
        props.canClaim === 'not_in_condition'
            ? imgList.btn_got_score_inactive
            : props.canClaim === 'claimed'
            ? imgList.btn_got_point_inactive
            : props.canClaim === 'can_claim'
            ? imgList.btn_got_score
            : ''}');

    &:hover {
        background-position: bottom center;
    }
`;

const DuoScoreBtn = styled.div`
    margin: 0 auto;
    width: 88px;
    height: 28px;
    background-position: top center;
    background-repeat: no-repeat;
    cursor: pointer;
    pointer-events: ${(props) =>
        props.canClaim === 'can_claim' ? 'auto' : 'none'};

    background-image: url('${(props) =>
        props.canClaim === 'not_in_condition'
            ? imgList.btn_duo_getpoint_inactive
            : props.canClaim === 'claimed'
            ? imgList.btn_got_point_inactive
            : props.canClaim === 'can_claim'
            ? imgList.btn_duo_getpoint
            : ''}');

    &:hover {
        background-position: bottom center;
    }
`;

const PackageStyle = styled.div`
    position: relative;
    display: block;
    width: 1105px;
    height: 6650px;
    margin: 0px;
    background-image: url(${imgList.package_background});
    .towels {
        z-index: 1;
        color: #ffffff;
        text-align: right;
        line-height: 1.9em;
        font-size: 15.28px;
        position: fixed;
        top: 500px;
        right: 0px;
        width: 150px;
        height: 72px;
        align-items: center;
        display: flex;
        justify-content: flex-end;
        padding-right: 15px;
        background-image: url(${imgList.towels_count});
        &__icon {
            vertical-align: middle;
            margin-left: 5px;
        }
    }
    .text__limit {
        text-align: center;
        font-size: 12px;
        color: #d69e75;
    }
    .quest {
        position: relative;
        display: block;
        height: 1798px;
        padding-top: 40px;
        &__head {
            display: block;
            margin: 84px auto 39px;
            width: 963px;
            text-align: center;
        }
        &__content {
            display: block;
            width: 963px;
            height: 1726px;
            margin: 0px auto;
            background-image: url(${imgList.package_section_points});

            .btn__group {
                top: 607px;
                position: relative;
                right: 6px;
                .content {
                    display: flex;
                    justify-content: center;
                    flex-flow: row wrap;
                    height: 670px;
                    /* grid-gap: 225px 9px;
                    grid-template-columns: repeat(8, 100px);
                    grid-template-areas:
                        'box1 box1 box2 box2 box3 box3 box4 box4'
                        'empt box5 box5 box6 box6 box7 box7 emp2';
                    grid-template-rows: 1fr 1fr; */

                    .box {
                        flex-shrink: 3;

                        width: 204px;
                        height: 110px;
                        cursor: pointer;

                        &:nth-child(1) {
                            grid-area: box1;
                            padding-right: 40px;
                        }
                        &:nth-child(2) {
                            grid-area: box2;
                            padding-right: 5px;
                        }
                        &:nth-child(3) {
                            grid-area: box3;
                            padding-left: 10px;
                        }
                        &:nth-child(4) {
                            grid-area: box4;
                            padding-left: 29px;
                        }
                        &:nth-child(5) {
                            grid-area: box5;
                            padding-right: 38px;
                        }
                        &:nth-child(6) {
                            grid-area: box6;
                        }
                        &:nth-child(7) {
                            grid-area: box7;
                            padding-left: 15px;
                        }
                    }
                }
            }
            .btn__group2 {
                top: 820px;
                right: 13px;
                position: relative;
                .content {
                    display: flex;
                    flex-flow: row wrap;
                    justify-content: center;
                    /* grid-gap: 10px;
                    grid-template-columns: repeat(3, 100px);
                    grid-template-areas: 'box1 box1 box2 box2 box3 box3'; */

                    .box {
                        width: 204px;
                        height: 123px;
                        cursor: pointer;

                        &:nth-child(1) {
                            grid-area: box1;
                            padding-right: 27px;
                        }
                        &:nth-child(2) {
                            grid-area: box2;
                        }
                        &:nth-child(3) {
                            grid-area: box3;
                            padding-left: 18px;
                        }
                    }
                }
            }
        }
        &__btn {
            position: relative;
            display: block;
            width: 277px;
            height: 58px;
            background-position: top center;
            &--3point {
                top: 185px;
                display: block;
                position: relative;
                margin: 0 auto;
                width: 277px;
                height: 58px;
                background-image: url(${imgList.btn_3points});
                cursor: pointer;
                &.not_in_condition {
                    filter: grayscale(1);
                    user-select: none;
                    pointer-events: none;
                }
                &.claimed {
                    user-select: none;
                    pointer-events: none;
                    background-image: url(${imgList.btn_got_3points});
                }
                &.can_claim {
                }
                &_inactive {
                    top: 185px;
                    display: block;
                    position: relative;
                    margin: 0 auto;
                    background-image: url(${imgList.btn_3points_inactive});
                }
                &_not_in_condition {
                    top: 185px;
                    display: block;
                    position: relative;
                    margin: 0 auto;
                    background-image: url(${imgList.btn_3points_daily_inactive});
                }
            }
            &--10point {
                top: 331px;
                left: 416px;
                background-image: url(${imgList.btn_10points});
                &.not_in_condition {
                    filter: grayscale(1);
                    user-select: none;
                    pointer-events: none;
                }
                &.claimed {
                    user-select: none;
                    pointer-events: none;
                    background-image: url(${imgList.btn_got_10points});
                }
                &.can_claim {
                }
            }
            &:hover {
                background-position: bottom center;
            }
            // btn_got_package.png
        }
        &__list {
            position: absolute;
            top: 543px;
            left: 204px;
            display: flex;
            width: 695px;
            align-items: flex-start;
            justify-content: space-between;
            flex-wrap: wrap;
        }
        &__slot {
            position: relative;
            display: block;
            width: 217px;
            height: 276px;
            margin-bottom: 34px;
        }
        &__slotbtn {
            position: absolute;
            bottom: 33px;
            left: 37px;
            display: block;
            width: 143px;
            height: 40px;
            background-image: url(${imgList.btn_point});
            background-position: top center;
            &:hover {
                background-position: bottom center;
            }
            &.not_in_condition {
                filter: grayscale(1);
                user-select: none;
                pointer-events: none;
            }
            &.claimed {
                user-select: none;
                pointer-events: none;
                background-image: url(${imgList.btn_got_point});
            }
            &.can_claim {
            }
        }
    }

    .quest2 {
        margin-top: 91px;
        position: relative;
        display: block;
        &__content2 {
            display: block;
            width: 963px;
            height: 1246px;
            margin: 0px auto;
            background-image: url(${imgList.package_section_points2});
        }

        .btn__group3 {
            top: 400px;
            position: absolute;
            left: 49.5%;
            transform: translate(-50%, 0px);
            .content {
                display: flex;
                flex-flow: row wrap;
                row-gap: 1rem;
                column-gap: 1rem;
                /* grid-gap: 48px;
                grid-template-columns: repeat(3);
                grid-template-areas: 'box1 box1 box2 box2 box3 box3'; */

                .box {
                    width: 109px;
                    height: 61px;
                    cursor: pointer;

                    &:nth-child(1) {
                        margin: 0 25px;
                    }
                    &:nth-child(2) {
                        margin: 0 25px;
                    }
                    &:nth-child(3) {
                        margin: 0 25px;
                    }
                }
            }
        }

        .btn__group4 {
            top: 815px;
            position: absolute;
            left: 49.5%;
            transform: translate(-50%, 0px);
            .content {
                display: flex;
                flex-flow: row wrap;
                row-gap: 1rem;
                column-gap: 1rem;
                justify-content: center;
                height: 554px;
                /* display: grid;
                grid-gap: 215px 48px;
                grid-template-columns: repeat(6);
                grid-template-areas:
                    'box1 box1 box2 box2 box3 box3'
                    'box4 box4 box5 box5 box6 box6'; */

                .box {
                    width: 109px;
                    height: 61px;
                    cursor: pointer;

                    &:nth-child(1) {
                        margin: 0 25px;
                    }
                    &:nth-child(2) {
                        margin: 0 25px;
                    }
                    &:nth-child(3) {
                        margin: 0 25px;
                    }
                    &:nth-child(4) {
                        margin: 0 25px;
                    }
                    &:nth-child(5) {
                        margin: 0 25px;
                    }
                    &:nth-child(6) {
                        margin: 0 25px;
                    }
                }
            }
        }
    }

    .quest3 {
        margin-top: 91px;
        position: relative;
        display: block;
        &__content3 {
            display: block;
            width: 963px;
            height: 2548px;
            margin: 0px auto;
            background-image: url(${imgList.package_section_points3});
        }

        .btn__group5 {
            top: 390px;
            position: absolute;
            left: 49.5%;
            transform: translate(-50%, 0px);
            .content {
                display: flex;
                flex-flow: row wrap;
                row-gap: 1rem;
                column-gap: 1rem;
                justify-content: center;
                /* height: 554px; */
                /* display: grid;
                grid-gap: 48px;
                grid-template-columns: repeat(3);
                grid-template-areas: 'box1 box1 box2 box2 box3 box3'; */

                .box {
                    width: 109px;
                    height: 61px;
                    cursor: pointer;
                    flex-shrink: 3;

                    &:nth-child(1) {
                    }
                    &:nth-child(2) {
                        margin: 0 50px;
                    }
                    &:nth-child(3) {
                    }
                }
            }
        }
        .btn__group6 {
            top: 800px;
            position: absolute;
            left: 49.5%;
            transform: translate(-50%, 0px);
            width: 500px;
            .content {
                display: flex;
                flex-flow: row wrap;
                row-gap: 1rem;
                column-gap: 1rem;
                justify-content: center;
                /* display: grid;
                grid-gap: 200px 48px;
                grid-template-columns: repeat(6);
                grid-template-areas:
                    'box1 box1 box2 box2 box3 box3'
                    'box4 box4 box5 box5 box6 box6'; */

                .box {
                    width: 109px;
                    height: 61px;
                    cursor: pointer;
                    margin-bottom: 200px;

                    &:nth-child(1) {
                        grid-area: box1;
                    }
                    &:nth-child(2) {
                        grid-area: box2;
                        margin: 0 40px;
                    }
                    &:nth-child(3) {
                        grid-area: box3;
                    }
                    &:nth-child(4) {
                        grid-area: box4;
                    }
                    &:nth-child(5) {
                        grid-area: box5;
                        margin: 0 40px;
                    }
                    &:nth-child(6) {
                        grid-area: box6;
                    }
                }
            }
        }
        .btn__group7 {
            top: 1470px;
            position: absolute;
            left: 49.5%;
            transform: translate(-50%, 0px);
            width: 640px;
            .content {
                display: flex;
                flex-flow: row wrap;
                row-gap: 1rem;
                column-gap: 1rem;
                justify-content: center;
                /* display: grid;
                grid-gap: 184px 48px;
                grid-template-columns: repeat(20);
                grid-template-areas:
                    'box1 box1 box2 box2 box3 box3 box4 box4'
                    'box5 box5 box6 box6 box7 box7 box8 box8'
                    'box9 box9 box10 box10 box11 box11 box12 box12'
                    'box13 box13 box14 box14 box15 box15 box16 box16'
                    'box17 box17 box18 box18 box19 box19 box20 box20'; */

                .box {
                    width: 109px;
                    height: 61px;
                    cursor: pointer;

                    &:nth-child(1) {
                        grid-area: box1;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(2) {
                        grid-area: box2;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(3) {
                        grid-area: box3;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(4) {
                        grid-area: box4;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(5) {
                        grid-area: box5;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(6) {
                        grid-area: box6;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(7) {
                        grid-area: box7;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(8) {
                        grid-area: box8;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(9) {
                        grid-area: box9;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(10) {
                        grid-area: box10;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(11) {
                        grid-area: box11;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(12) {
                        grid-area: box12;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(13) {
                        grid-area: box13;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(14) {
                        grid-area: box14;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(15) {
                        grid-area: box15;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(16) {
                        grid-area: box16;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(17) {
                        grid-area: box17;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(18) {
                        grid-area: box18;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(19) {
                        grid-area: box19;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                    &:nth-child(20) {
                        grid-area: box20;
                        margin: 0 25px;
                        margin-bottom: 183px;
                    }
                }
            }
        }
    }

    .quest4 {
        margin-top: 91px;
        &__content4 {
            display: block;
            width: 963px;
            height: 2548px;
            margin: 0px auto;
            background-image: url(${imgList.package_section_points4});
            background-repeat: no-repeat;
        }
    }

    .treasure {
        display: block;
        padding-top: 40px;
        &__content {
            position: relative;
            display: block;
            height: 1027px;
            background-image: url(${imgList.treasure_section});
        }
        &__title {
            color: #ffffff;
            font-size: 20px;
            text-align: center;
            position: absolute;
            top: 79px;
            left: 315px;
            display: block;
            width: 475px;
        }
        &__help {
            z-index: 1;
            position: absolute;
            top: 144px;
            left: 495px;
            display: block;
            width: 115px;
            height: 113px;
            opacity: 0;
            transition: opacity 0.3s linear;
            &:hover {
                opacity: 1;
            }
        }
        &__100 {
            position: absolute;
            top: 0px;
            left: 0px;
            transform: translate(-340px, -205px);
            user-select: none;
            pointer-events: none;
        }
        &__maybe {
            position: absolute;
            top: 0px;
            right: 0px;
            transform: translate(482px, -205px);
            user-select: none;
            pointer-events: none;
        }
        &__popup {
            position: absolute;
            display: block;
            &--100 {
                // top: -60px;
                // left: 155px;
            }
            &--maybe {
                // top: -60px;
                // left: 610px;
            }
        }
        &__btn {
            position: absolute;
            top: 295px;
            left: 455px;
            display: block;
            width: 192px;
            height: 40px;
            background-position: top center;
            background-image: url(${imgList.btn_treasure});
            cursor: pointer;
            &:hover {
                background-position: bottom center;
            }
            &.lock {
                background-image: url(${imgList.btn_got_treasure});
                user-select: none;
                pointer-events: none;
            }
        }
    }
    .exchange {
        position: absolute;
        top: 3040px;
        display: block;
        width: 100%;
        height: 2754px;
        padding-top: 40px;
        &__content {
            position: relative;
            display: block;
            height: 2712px;
            background-position: 3px 0px;
            background-image: url(${imgList.exchange_section});
        }
        &__count {
            position: absolute;
            line-height: 39px;
            color: #ffffff;
            font-size: 20px;
            text-align: center;
            display: block;
            width: 39px;
            height: 39px;
            background-image: url(${imgList.exchange_count});
            &--fix1 {
                top: 545px;
                left: 290px;
            }
            &--fix2 {
                top: 545px;
                left: 453px;
            }
            &--fix3 {
                top: 545px;
                left: 615px;
            }
            &--fix4 {
                top: 545px;
                left: 778px;
            }
        }
        &__btn {
            position: absolute;
            display: block;
            width: 84px;
            height: 36px;
            background-position: top center;
            background-image: url(${imgList.btn_exchange});
            cursor: pointer;
            &:hover {
                background-position: bottom center;
            }
            &.lock {
                background-image: url(${imgList.btn_exchange_inactive});
                user-select: none;
                pointer-events: none;
            }
            &--day1 {
                top: 335px;
                left: 430px;
            }
            &--day2 {
                top: 335px;
                left: 587px;
            }

            &--fix1 {
                top: 776px;
                left: 269px;
            }
            &--fix2 {
                top: 776px;
                left: 432px;
            }
            &--fix3 {
                top: 776px;
                left: 594px;
            }
            &--fix4 {
                top: 776px;
                left: 757px;
            }

            &.c0 {
                left: 269px;
            }
            &.c1 {
                left: 432px;
            }
            &.c2 {
                left: 594px;
            }
            &.c3 {
                left: 757px;
            }

            &.r0 {
                top: 1212px;
            }
            &.r1 {
                top: 1488px;
            }
            &.r2 {
                top: 1811px;
            }
            &.r3 {
                top: 2109px;
            }
            &.r4 {
                top: 2409px;
            }
        }

        &__plus {
            position: absolute;
            display: block;
            width: 32px;
            height: 32px;
            background-image: url(${imgList.btn_plus});
            cursor: pointer;
            &--1 {
                top: 159px;
                left: 499px;
            }
            &--2 {
                top: 161px;
                left: 657px;
            }
        }
    }
`;
