import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { setValues, setDuoRequests, setDuoReceives } from './../store/redux';
import styled from 'styled-components';
import imgList from './../constants/ImportImages';
import F11Layout from './../features/f11Layout';
import Modals from './../features/modals';
import { apiPost } from './../constants/Api';

import { Tab, Tabs, DataTableSended, DataTableReceive } from '../components';

const Sended = (props) => {
    useEffect(() => {
        return function() {
            console.log('Unmount DataTableSended');
        };
    }, []);

    return <DataTableSended />;
};

const Received = () => {
    useEffect(() => {
        return function() {
            console.log('Unmount DataTableReceive');
        };
    }, []);

    return <DataTableReceive />;
};

const Home = (props) => {
    const apiEventInfo = () => {
        let dataSend = { type: 'event_info' };
        let successCallback = (res) => {
            if (res.status) {
                if (
                    res.data &&
                    !res.data.selected_char &&
                    res.data.characters.length === 0
                ) {
                    props.setValues({
                        ...res.data,
                        modal_open: 'message',
                        modal_message: 'ไม่มีตัวละครตรงตามเงื่อนไข',
                        claim_reward_list: props.claim_reward_list,
                        quest_daily_list: props.quest_daily_list,
                    });
                } else {
                    props.setValues({
                        ...res.data,
                        modal_open: res.data.selected_char ? '' : 'character',
                    });
                }
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        apiPost(
            'REACT_APP_API_POST_EVENT_INFO',
            props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );

        let duoRequsetSuccessCallback = (res) => {
            if (res.status) {
                props.setDuoRequests({
                    ...res.data,
                });
            }
        };

        let duoRequsetFailCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };

        apiPost(
            'REACT_APP_API_POST_GET_DUO_REQUSET',
            props.jwtToken,
            '',
            duoRequsetSuccessCallback,
            duoRequsetFailCallback
        );

        let duoReceivesSuccessCallback = (res) => {
            console.log(res, 'duoReceivesSuccessCallback');
            if (res.status) {
                props.setDuoReceives({
                    ...res.data,
                });
            }
        };

        let duoReceivesFailCallback = (res) => {
            console.log(res, 'duoReceivesFailCallback');
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };

        apiPost(
            'REACT_APP_API_POST_GET_DUO_RECEIVES',
            props.jwtToken,
            '',
            duoReceivesSuccessCallback,
            duoReceivesFailCallback
        );
    };

    useEffect(() => {
        if (props.jwtToken !== '' && !props.status) {
            let dataSend = { type: 'event_info' };
            let successCallback = (res) => {
                if (res.status) {
                    props.setValues({
                        modal_open: '',
                    });
                    if (
                        res.data &&
                        !res.data.selected_char &&
                        res.data.characters.length === 0
                    ) {
                        props.setValues({
                            ...res.data,
                        });
                    } else {
                        props.setValues({
                            ...res.data,
                        });
                    }
                } else {
                    props.setValues({
                        modal_open: 'message',
                        modal_message: res.message,
                    });
                }
            };
            let failCallback = (res) => {
                if (res.type === 'no_permission') {
                    props.setValues({
                        permission: false,
                        modal_open: '',
                    });
                } else {
                    props.setValues({
                        modal_open: 'message',
                        modal_message: res.message,
                    });
                }
            };
            apiPost(
                'REACT_APP_API_POST_EVENT_INFO',
                props.jwtToken,
                dataSend,
                successCallback,
                failCallback
            );

            let duoRequsetSuccessCallback = (res) => {
                if (res.status) {
                    props.setDuoRequests({
                        ...res.data,
                    });
                }
            };

            let duoRequsetFailCallback = (res) => {
                if (res.type === 'no_permission') {
                    props.setValues({
                        permission: false,
                        modal_open: '',
                    });
                } else {
                    props.setValues({
                        modal_open: 'message',
                        modal_message: res.message,
                    });
                }
            };

            apiPost(
                'REACT_APP_API_POST_GET_DUO_REQUSET',
                props.jwtToken,
                '',
                duoRequsetSuccessCallback,
                duoRequsetFailCallback
            );

            let duoReceivesSuccessCallback = (res) => {
                console.log(res, 'duoReceivesSuccessCallback');
                if (res.status) {
                    props.setDuoReceives({
                        ...res.data,
                    });
                }
            };

            let duoReceivesFailCallback = (res) => {
                console.log(res, 'duoReceivesFailCallback');
                if (res.type === 'no_permission') {
                    props.setValues({
                        permission: false,
                        modal_open: '',
                    });
                } else {
                    props.setValues({
                        modal_open: 'message',
                        modal_message: res.message,
                    });
                }
            };

            apiPost(
                'REACT_APP_API_POST_GET_DUO_RECEIVES',
                props.jwtToken,
                '',
                duoReceivesSuccessCallback,
                duoReceivesFailCallback
            );
        }
    }, [props.jwtToken]);

    const [key, setKey] = useState(0);

    const registerDuo = () => {
        props.setValues({
            modal_open: 'register_code',
        });
    };

    return (
        <F11Layout duo={props.duo} page={'main'}>
            <Modals />
            <HomeStyle>
                <section className='banner'>
                    {props.register_status === 'completed_register' ? (
                        <Link
                            className='banner__btn__completed_register'
                            to={props.eventPath('/package')}
                        />
                    ) : props.register_status === 'can_register' ? (
                        <div
                            className='banner__btn'
                            onClick={() => apiEventInfo()}
                        />
                    ) : (
                        <div
                            className='banner__btn__inactive'
                            onClick={() => apiEventInfo()}
                        />
                    )}
                </section>
                <section className='detail'>
                    <img className='detail__head' src={imgList.detail_head} />
                    <img className='detail__board' src={imgList.detail} />
                </section>
                <section className='event_code'>
                    <div className='event_code__board'>
                        <div className='event_code__board__codename'>
                            <span className='event_code__textcode'>
                                รหัสของคุณคือ :
                            </span>{' '}
                            <span>{props.code}</span>
                        </div>
                        {props.register_status &&
                        props.register_status === 'completed_register' ? (
                            <DetailDuo>
                                <span className='first'>
                                    คู่หู คู่ซี้ของคุณ :
                                </span>
                                {props.duo.char_name}
                                <span>LV.</span>
                                {props.duo.level} <span>ฮงมุนLV.</span>
                                {props.duo.mastery_level} <span>SV.</span>
                                {props.duo.world}
                            </DetailDuo>
                        ) : (
                            <div
                                onClick={() => registerDuo()}
                                className={
                                    'event_code__board__btn ' +
                                    props.register_status
                                }
                            ></div>
                        )}
                    </div>
                </section>
                <section className='tabs'>
                    <Tabs
                        activeKey={key}
                        onSelect={(k) => {
                            setKey(k);
                        }}
                    >
                        <Tab eventKey={0} title='คำขอที่ส่ง'>
                            <Sended />
                        </Tab>
                        <Tab eventKey={1} title='คำขอที่ได้รับ'>
                            <Received />
                        </Tab>
                    </Tabs>
                </section>
            </HomeStyle>
        </F11Layout>
    );
};
const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues, setDuoRequests, setDuoReceives };
export default connect(mapStateToProps, mapDispatchToProps)(Home);

const DetailDuo = styled.div`
    color: #fff;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;

    .first {
        margin-right: 10px;
    }

    span {
        color: #7bd875;
        &:not(:first-child) {
            margin-left: 10px;
        }
    }
`;

const HomeStyle = styled.div`
    position: relative;
    display: block;
    width: 1105px;
    height: 2779px;
    background-image: url(${imgList.home_background});
    .banner {
        position: relative;
        display: block;
        height: 700px;
        &__btn {
            position: absolute;
            top: 478px;
            left: 50%;
            display: block;
            width: 390px;
            height: 54px;
            cursor: pointer;
            transform: translate(-50%, 0px);
            background-image: url(${imgList.btn_join});
            background-position: top center;
            &:hover {
                background-position: bottom center;
            }
        }
        &__btn__inactive {
            position: absolute;
            top: 478px;
            left: 50%;
            display: block;
            width: 390px;
            height: 54px;
            pointer-events: none;
            transform: translate(-50%, 0px);
            background-image: url(${imgList.btn_join_inactive});
            background-position: top center;
        }
        &__btn__completed_register {
            position: absolute;
            top: 478px;
            left: 50%;
            display: block;
            width: 390px;
            height: 54px;
            transform: translate(-50%, 0px);
            background-image: url(${imgList.btn_can_join});
            background-position: top center;
            &:hover {
                background-position: bottom center;
            }
        }
        &__time {
            position: absolute;
            top: 565px;
            left: 50%;
            display: block;
            transform: translate(-50%, 0px);
        }
    }
    .detail {
        position: relative;
        display: block;

        &__head {
            display: block;
            margin: 80px auto 0px;
        }
        &__board {
            display: block;
            width: 953px;
            height: 824px;
            margin: 37px auto 0px;
        }
    }
    .event_code {
        position: relative;
        display: block;
        margin-top: 55px;

        &__board {
            display: block;
            position: relative;
            width: 953px;
            height: 273px;
            margin: 0px auto;
            background-image: url(${imgList.rectangle_event_code});

            &__codename {
                display: block;
                position: absolute;
                margin-top: 67px;
                left: 50%;
                transform: translate(-50%, 0px);
                color: #fff;

                span {
                    font-size: 20px;
                }
            }
            &__btn {
                display: block;
                position: absolute;
                margin-top: 132px;
                width: 277px;
                height: 58px;
                left: 50%;
                cursor: pointer;
                transform: translate(-50%, 0px);
                background-image: url(${imgList.btn_register});
                background-position: top center;
                filter: grayscale(1);
                pointer-events: none;
                &:hover {
                    background-position: bottom center;
                }
                &.can_register {
                }
                &.not_completed_register{
                    filter: grayscale(0);
                    pointer-events: all;
                }
                &.completed_register{
                    
                }
                /* &__inactive {
                    background-image: url(${imgList.btn_register_inactive});
                } */
            }
        }
        &__textcode {
            color: #ffe04f;
        }
    }
    .tabs {
        position: relative;
        display: block;
        margin-top: 66px;
        height: 675px;
    }
`;
// home_background
// detail
