const BG =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/BG.png';
const BGPackage =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/BG-package.png';
const BGPointHistory =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/BG-point-history.png';
const Detail =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/content-detail-1.png';
const BtnRegisterHead =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn-register.png';
const BtnRegister =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/register.png';
const DetailHead =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/detail-head.png';
const RectangleEventCode =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/RectangleEventCode.png';
const HeadQuest =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/head-quest.png';
const contentPoints1 =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/content-package1.png';
const contentPoints2 =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/content-package2.png';
const contentPoints3 =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/content-package3.png';
const contentPoints4 =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/content-package4.png';
const cover_score =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/cover_score.png';
const PointHistoryFrame =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/point-history-frame.png';
const RewardHistoryFram =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/reward-history-frame.png';
const TestureHistoryFram =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/testure-history-frame.png';
const MenuBackground =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/menu-background.png';
const Btn3Point =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_3points.png';
const BtnGotScore =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_got_score.png';
const BtnGotScoreInactive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_got_score_inactive.png';
const BtnDuoScore =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_duo_score.png';
const BtnRedeemItem =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_redeem_item.png';
const BtnAccept =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_accept.png';
const BtnReject =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_reject.png';
const BtnRejectSender =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_reject_sender.png';
const SelectCharacterModal =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/select_character_modal.png';
const BtnConfirm =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_confirm.png';
const MessageModal =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/message_modal.png';
const ConfirmModal =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/confirm_modal.png';
const BtnCancel =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_cancel.png';
const RegisterFriendModal =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/register_friend_modal.png';
const BtnRegisterInactive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn-register-inactive.png';
const BtnCanJoin =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_join.png';
const Btn3PointInactive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_got_3points.png';
const BtnExchangeInactive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_exchange_inactive.png';
const BtnRejectInactive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_reject_inactive.png';
const BtnAcceptInactive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_accept_inactive.png';
const BtnDeleteRequesrInactive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_delete_duo_inactive.png';
const BtnGotPointInactive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_got_point_inactive.png';
const BtnDuoGetpointInactive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_duo_getpoint_inactive.png';
const BtnDuoGetpoint =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_duo_getpoint.png';
const BtnGot3PointDailyInactive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_3points_daily_inactive.png';
const BtnRegisterInactiveHome =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hey_duo/images/btn_register_inactive.jpg';

const permission =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/permission.jpg';
const icon_scroll =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/icon_scroll.png';
const btn_home =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_home.png';
const menu_background = MenuBackground;

const home_background = BG;
const detail = Detail;
const detail_head = DetailHead;
const rectangle_event_code = RectangleEventCode;
const time =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/time.png';
const btn_join = BtnRegister;
const btn_can_join = BtnCanJoin;
const btn_join_inactive = BtnRegisterInactive;
const btn_register = BtnRegisterHead;
const head_quest = HeadQuest;

const package_background = BGPackage;
const package_section_points = contentPoints1;
const package_section_points2 = contentPoints2;
const package_section_points3 = contentPoints3;
const package_section_points4 = contentPoints4;
const package_section =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/package_section.png';
const btn_got_package =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_got_package.png';
const btn_package =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_package.png';
const quest_section =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/quest_section.png';
const btn_got_3points =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_got_3points.png';
const btn_got_10points =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_got_10points.png';
const btn_10points =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_10points.png';
const btn_3points = Btn3Point;
const btn_3points_inactive = Btn3PointInactive;
const btn_3points_daily_inactive = BtnGot3PointDailyInactive;
const btn_got_score = BtnGotScore;
const btn_got_score_inactive = BtnGotScoreInactive;
const btn_duo_score = BtnDuoScore;
const btn_redeem_item = BtnRedeemItem;

const boss1 =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss1.png';
const boss2 =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss2.png';
const boss3 =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss3.png';
const boss4 =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss4.png';
const boss5 =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss5.png';
const boss6 =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss6.png';
const boss7 =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss7.png';
const btn_point =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_point.png';
const treasure_section =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/treasure_section.png';
const btn_treasure =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_treasure.png';
const btn_got_treasure =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_got_treasure.png';
const popup_treasure_100 =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/popup_treasure_100.png';
const popup_treasure_maybe =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/popup_treasure_maybe.png';
const exchange_section =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/exchange_section.png';
const exchange_count =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/exchange_count.png';
const btn_exchange =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_exchange.png';
const btn_exchange_inactive = BtnExchangeInactive;
const btn_reject_inactive = BtnRejectInactive;
const btn_accept_inactive = BtnAcceptInactive;
const btn_register_inactive = BtnRegisterInactiveHome;
const btn_delete_request_inactive = BtnDeleteRequesrInactive;
const btn_plus =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_plus.png';
const btn_accept = BtnAccept;
const btn_reject = BtnReject;
const btn_got_point_inactive = BtnGotPointInactive;
const btn_duo_getpoint_inactive = BtnDuoGetpointInactive;
const btn_duo_getpoint = BtnDuoGetpoint;
const btn_reject_sender = BtnRejectSender;
const towels_count = cover_score;
const towels_icon =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/towels_icon.png';

const history_background = BGPointHistory;
const point_history_frame = PointHistoryFrame;
const reward_history_frame = RewardHistoryFram;
const testure_history_frame = TestureHistoryFram;

const message_modal = MessageModal;
const confirm_modal = ConfirmModal;
const register_friend_modal = RegisterFriendModal;
const select_character_modal = SelectCharacterModal;
const btn_confirm = BtnConfirm;
const btn_cancel = BtnCancel;
const icon_down_arrow =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/icon_down_arrow.png';
const turban_modal =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/turban_modal.png';
const towelset_modal =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/towelset_modal.png';
const icon_cross =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/icon_cross.png';

export default {
    permission,
    icon_scroll,
    btn_home,
    menu_background,

    home_background,
    detail,
    detail_head,
    time,
    btn_join,
    btn_join_inactive,
    btn_can_join,
    btn_register,
    rectangle_event_code,

    package_background,
    package_section,
    btn_got_package,
    btn_package,
    quest_section,
    package_section_points,
    package_section_points2,
    package_section_points3,
    package_section_points4,
    head_quest,
    btn_got_3points,
    btn_got_score,
    btn_got_score_inactive,
    btn_duo_score,
    btn_redeem_item,
    btn_got_10points,
    btn_10points,
    btn_3points,
    btn_3points_inactive,
    btn_3points_daily_inactive,
    boss1,
    boss2,
    boss3,
    boss4,
    boss5,
    boss6,
    boss7,
    btn_point,
    btn_got_point_inactive,
    btn_duo_getpoint_inactive,
    btn_duo_getpoint,
    treasure_section,
    btn_treasure,
    btn_got_treasure,
    popup_treasure_100,
    popup_treasure_maybe,
    exchange_section,
    exchange_count,
    btn_exchange,
    btn_exchange_inactive,
    btn_reject_inactive,
    btn_accept_inactive,
    btn_register_inactive,
    btn_delete_request_inactive,
    btn_plus,
    btn_accept,
    btn_reject,
    btn_reject_sender,
    towels_count,
    towels_icon,

    history_background,
    point_history_frame,
    reward_history_frame,
    testure_history_frame,

    message_modal,
    confirm_modal,
    select_character_modal,
    btn_confirm,
    btn_cancel,
    icon_down_arrow,
    turban_modal,
    towelset_modal,
    register_friend_modal,
    icon_cross,
};
