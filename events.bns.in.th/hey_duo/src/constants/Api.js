import KJUR from 'jsrsasign';
// import $ from "jquery";

const createAuthorizationHeaders = (token) => {
    // Header
    let headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        Authorization: ['B', 'e', 'a', 'r', 'e', 'r'].join('') + ' ' + token,
    };
    return headers;
};
export function apiPost(
    api_name,
    token,
    api_data,
    successCallback = () => {},
    failCallback = () => {},
    responseCallback = () => {},
    errorCallback = () => {}
) {
    fetch(process.env.REACT_APP_API_SERVER_HOST + process.env[api_name], {
        method: 'POST',
        credentials: 'same-origin',
        // ...result
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json; charset=UTF-8',
            Authorization:
                ['B', 'e', 'a', 'r', 'e', 'r'].join('') + ' ' + token,
        },
        body: JSON.stringify(api_data),
    }).then(
        (response) => {
            if (response.status == 200) {
                response.json().then((data) => {
                    if (data.status) {
                        successCallback(data);
                        console.log(data, api_name, 'successCallback');
                    } else {
                        failCallback(data);
                        console.log(data, api_name, 'failCallback');
                    }
                });
            } else {
                responseCallback(response);
            }
        },
        (error) => {
            errorCallback(error);
        }
    );
}

export function apiDelete(
    api_name,
    token,
    api_data,
    successCallback = () => {},
    failCallback = () => {},
    responseCallback = () => {},
    errorCallback = () => {}
) {
    fetch(
        process.env.REACT_APP_API_SERVER_HOST +
            process.env[api_name] +
            '/' +
            api_data,
        {
            method: 'DELETE',
            credentials: 'same-origin',
            // ...result
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                Authorization:
                    ['B', 'e', 'a', 'r', 'e', 'r'].join('') + ' ' + token,
            },
            body: JSON.stringify(api_data),
        }
    ).then(
        (response) => {
            if (response.status === 200) {
                response.json().then((data) => {
                    if (data.status) {
                        successCallback(data);
                        console.log(data, api_name, 'successCallback');
                    } else {
                        failCallback(data);
                        console.log(data, api_name, 'failCallback');
                    }
                });
            } else {
                responseCallback(response);
            }
        },
        (error) => {
            errorCallback(error);
        }
    );
}

export function apiPut(
    api_name,
    token,
    api_data,
    condition,
    successCallback = () => {},
    failCallback = () => {},
    responseCallback = () => {},
    errorCallback = () => {}
) {
    fetch(
        process.env.REACT_APP_API_SERVER_HOST +
            process.env[api_name] +
            '/' +
            api_data +
            '/' +
            condition,
        {
            method: 'PUT',
            credentials: 'same-origin',
            // ...result
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                Authorization:
                    ['B', 'e', 'a', 'r', 'e', 'r'].join('') + ' ' + token,
            },
            body: JSON.stringify(api_data),
        }
    ).then(
        (response) => {
            if (response.status === 200) {
                response.json().then((data) => {
                    if (data.status) {
                        successCallback(data);
                        console.log(data, api_name, 'successCallback');
                    } else {
                        failCallback(data);
                        console.log(data, api_name, 'failCallback');
                    }
                });
            } else {
                responseCallback(response);
            }
        },
        (error) => {
            errorCallback(error);
        }
    );
}
