REACT_APP_EVENT_PATH=/archer_preregister
REACT_APP_STATIC_PATH=/static/
REACT_APP_LOCALE=th-TH
REACT_APP_CSRF_COOKIE_NAME=csrf_token
REACT_APP_COOKIE_DOMAIN=.bns.in.th

# API related
REACT_APP_API_SERVER_HOST=https://events.bns.in.th
REACT_APP_API_GET_ACCOUNT_INFO=/oauth/get_account_info
REACT_APP_API_GET_OAUTH_INFO=/oauth/get_oauth

# API FOR EVENT
REACT_APP_API_POST_PREREGISTER_INFO         =/api/archer_preregister/pre_register_info
REACT_APP_API_POST_EVENT_INFO               =/api/archer_preregister/event_info
REACT_APP_API_POST_ACCEPT_PREREGISTER       =/api/archer_preregister/accept_preregister

# OAUTH related
REACT_APP_OAUTH_APP_ID=10007
REACT_APP_OAUTH_APP_NAME=bns_archer_preregister
REACT_APP_OAUTH_PLATFORM=1
REACT_APP_OAUTH_URL=https://auth.garena.com
REACT_APP_OAUTH_PARAM_NAME=token
REACT_APP_OAUTH_COOKIE_NAME=oauth_session

# JWT
REACT_APP_JWT_SECRET=Qd3UsCmjj3YqLwDtNHshAgLQjk8Bhxye

REACT_APP_STATIC_PATH=/static/
REACT_APP_LOCALE=th-TH
REACT_APP_CSRF_COOKIE_NAME=csrf_token
