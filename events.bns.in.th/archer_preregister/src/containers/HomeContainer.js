import React, { Component } from 'react';
import {connect} from 'react-redux';
import 'whatwg-fetch';

import $ from "jquery";

import {
    onAccountLogout
} from './../actions/AccountActions';

import {apiPost, apiGet} from './../middlewares/Api';
import { Loading } from '../components/Loading';
import ModalMessage from '../components/ModalMessage';
import ModalConfirm from '../components/ModalConfirm';
import ModalPreRegisterMessage from '../components/ModalPreRegisterMessage';

import { Permission } from '../components/Permission';
import iconScroll from './../static/images/icon_scroll.png';
import logo_top from './../static/images/logo_top.png'
import btn_top from './../static/images/btn_top.png'
import btn_patch from './../static/images/btn_patch.png'
import btn_login from './../static/images/btn_login.png'
import btn_logout from './../static/images/btn_logout.png'
import buy from './../static/images/buy.png'
import buy_not from './../static/images/buy_not.png'

class HomeContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            permission: false,
            showLoading: false,
            showModalMessage: '',
            register: false,
            preOrder: false,
            showConfirm: false,
            showModalPreRegisterMessage: ''
        }
    }
    
    handleRegister(){
        this.setState({
            // showLoading: true,
            showConfirm: true,
            // showModalMessage: 'ลงทะเบียนเรียบร้อย'
        });

        // this.apiAcceptPreregister();
    }

    apiEventInfo(){
        this.setState({ showLoading: true, });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status) {
                // console.log('data.content:',data.content);
                this.setState({
                    ...data.content,
                    showLoading: false,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    permission: false,
                    showModalMessage: "",
                });
            }else {
                this.setState({
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiPreRegisterInfo(){
        this.setState({ showLoading: true, });
        // console.log('Bello');
        // let dataSend = {type:'pre_register'};
        let successCallback = (data) => {
            if (data.status) {
                // console.log('data.content:',data.content);
                this.setState({
                    ...data.content,
                    showLoading: false,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    permission: false,
                    showModalMessage: "",
                });
            }else {
                this.setState({
                    showModalMessage: data.message,
                });
            }
        };
        apiGet("REACT_APP_API_POST_PREREGISTER_INFO", 'type=pre_register', successCallback, failCallback);
    }

    apiAcceptPreregister(){
        this.setState({ showLoading: true, showModalMessage: "", showModalPreRegisterMessage: "", showConfirm: false });
        let dataSend = {type:'accept_preregister'};
        let successCallback = (data) => {
            if (data.status) {
                // console.log('data.content:',data.content);
                this.setState({
                    ...data.content,
                    showLoading: false,
                    showModalPreRegisterMessage: data.message,
                });
                this.apiPreRegisterInfo()
            }else{
                this.setState({
                    showModalMessage: data.message,
                    showConfirm: false,
                    permission: false,
                    showLoading: false,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    permission: false,
                    showModalMessage: "",
                    showConfirm: false,
                    showLoading: false,
                });
            }else {
                this.setState({
                    showModalMessage: data.message,
                    showConfirm: false,
                    permission: false,
                    showLoading: false,
                });
            }
        };
        apiPost("REACT_APP_API_POST_ACCEPT_PREREGISTER", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    isPreorder(date) {
        const target = new Date('2019-09-18 12:00:00');
        if (Date.parse(target)<=Date.parse(date)){
            return true
        }
        else
        {
            return false
        }
    }

    componentDidMount() {
        
        setTimeout(() => this.apiPreRegisterInfo(), 1000);
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 1000);
        /* if(this.isPreorder(new Date())){
            this.setState({
                preOrder: true
            });
        } */
    }

    componentDidUpdate(prevProps, prevState) {
        // this.apiPreRegisterInfo()
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo()
            }, 1000);
        }
    }

    onLogout() {
        // console.log('Bello');
        this.setState({
            showModalMessage: ""
        });
        const {dispatch} = this.props;
        dispatch(onAccountLogout());
    }

    render() {
        return (
            <div className="events-bns">
                <div className="archer">
                    <div className="archer__logo">
                        <img src={logo_top}/>
                    </div>
                    <div className="archer__btngroup">
                        {
                            this.props.jwtToken && this.props.jwtToken !== "" ?
                                <a onClick={this.onLogout.bind(this)}>
                                    <img src={btn_logout}/>
                                </a>
                            :
                                <a href={this.props.loginUrl}>
                                    <img src={btn_login}/>
                                </a>
                        }
                        
                        {/* <a href="">
                            <img src={btn_patch}/>
                        </a> */}
                        <a href="https://bns.garena.in.th/" target="_blank">
                            <img src={btn_top}/>
                        </a>
                    </div>
                    <div className="archer__scroll">
                        <img src={iconScroll} alt=""/>
                    </div>
                    <div className="archer__board">
                        <div className="archer__board-register">
                            {
                                /* this.state.preOrder===true
                                ?
                                    <a href="https://events.bns.in.th/archer_preorder" target="_blank" className="archer__buy">
                                        <img src={buy}/>
                                    </a>
                                :
                                    <div className="archer__buy">
                                        <img src={buy_not}/>
                                    </div> */
                                <a href="https://events.bns.in.th/archer_preorder" target="_blank" className="archer__buy">
                                    <img src={buy}/>
                                </a>
                            }
                            {
                                this.state.username && this.state.username != '' ?
                                    (
                                        this.state.is_registered == true ?
                                            <div className="archer__btnregister registered" />
                                        :
                                            <div className="archer__btnregister" onClick={this.handleRegister.bind(this)} />
                                    )
                                :
                                    <a className="archer__btnregister" href={this.props.loginUrl}></a>
                            }
                            
                            <div className="archer__text">
                                <div className="archer__title">มีคนลงทะเบียนไปแล้ว</div>
                                <div className="archer__count"><span>{this.state.total_registered}</span> คน</div>
                            </div>
                        </div>
                        <div className="archer__board-reward"/>
                    </div>
                </div>
                <Loading
                    open={this.state.showLoading}
                />
                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={this.onLogout.bind(this)}
                    msg={this.state.showModalMessage}
                />
                <ModalConfirm
                    open={this.state.showConfirm}
                    actConfirm={()=>this.apiAcceptPreregister()}
                    actClose={()=>this.setState({showConfirm: false})}
                />
                <ModalPreRegisterMessage 
                    open={this.state.showModalPreRegisterMessage.length > 0}
                    actClose={()=>this.setState({showModalPreRegisterMessage: ''})}
                    msg={this.state.showModalPreRegisterMessage}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
