import KJUR from 'jsrsasign';
// import $ from "jquery";

const createAuthorizationHeaders = (token) => {
    // Header
    let headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': (["B", "e", "a", "r", "e", "r"].join("")) + " " + token,
    };
    return headers;
}

const createJwt = (data)=>{
        if(data){
            let oHeader = {
              alg: 'HS256',
              typ: 'JWT'
            };
            let sHeader = JSON.stringify(oHeader);
            let sPayload = '{"' + decodeURI(data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}';
            let sJWT = KJUR.jws.JWS.sign("HS256", sHeader, sPayload, process.env.REACT_APP_JWT_SECRET);

            let parts = sJWT.split(".");
            const header_payload = (["G", "a", "r", "e", "n", "a", "T", "H"].join("")) + " " + btoa(parts[0] + "." + parts[2]);
            const body_payload = btoa(parts[1]);
            return {
                headers:{
                    'Authorization':header_payload
                },
                body:body_payload
            }
        }else{
            return {
                headers:{},
                body:{}
            };
        }
}
// let beforeSend = (xhr, data) => {
//             if (data.data) {
//                 // Header
//                 let oHeader = {
//                     alg: 'HS256',
//                     typ: 'JWT'
//                 };
//                 // Sign JWT
//                 let sHeader = JSON.stringify(oHeader);
//                 let sPayload = '{"' + decodeURI(data.data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}';
//                 let sJWT = KJUR.jws.JWS.sign("HS256", sHeader, sPayload, process.env.REACT_APP_JWT_SECRET);
//
//                 // Separate jwt header and signature and put them in authorization
//                 let parts = sJWT.split(".");
//                 xhr.setRequestHeader("Authorization", (["G", "a", "r", "e", "n", "a", "T", "H"].join("")) + " " + btoa(parts[0] + "." + parts[2]));
// 				// console.log("xhr",xhr);
//                 // Put jwt payload inside body
//                 data.data = btoa(parts[1]);
//             }
//         }
export function apiPost(
        api_name,
        token,
        api_data,
        successCallback=()=>{},
        failCallback=()=>{},
        responseCallback=()=>{},
        errorCallback=()=>{}
    )
{
    // console.log("apiPost");
    // const result = createJwt(api_data);
        // var datas=$.extend(user_data,dataIn)
    fetch(process.env.REACT_APP_API_SERVER_HOST+process.env[api_name], {
        method: 'POST',
        credentials: 'same-origin',
        // ...result
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': (["B", "e", "a", "r", "e", "r"].join("")) + " " + token,
        },
        // headers: createAuthorizationHeaders(user_data),
        // headers: {
        //     'Accept': 'application/json',
        //     'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'
        // },
        body: JSON.stringify(api_data)
        // body: createJwt(JSON.stringify(api_data)).body

    }).then(response => {
        if (response.status == 200) {
            response.json().then(data => {
                if (data.status) {
                    successCallback(data);
                } else {
                    failCallback(data);
                }
            });
        } else {
            responseCallback(response);
        }
    }, error => {
        errorCallback(error);
    });
}

export function apiGet(
    api_name,
    api_get_params,
    successCallback=()=>{},
    failCallback=()=>{},
    responseCallback=()=>{},
    errorCallback=()=>{}
)
{
    // console.log("API Path : "+process.env.REACT_APP_API_SERVER_HOST+process.env[api_name]+'?'+api_get_params);
    // const result = createJwt(api_data);
        // var datas=$.extend(user_data,dataIn)
    fetch(process.env.REACT_APP_API_SERVER_HOST+process.env[api_name]+'?'+api_get_params).then(response => {
        if (response.status == 200) {
            response.json().then(data => {
                if (data.status) {
                    successCallback(data);
                } else {
                    failCallback(data);
                }
            });
        } else {
            responseCallback(response);
        }
    }, error => {
        errorCallback(error);
    });
}
