import React from 'react';
import Modal from './Modal';

export default class ModalPreRegisterMessage extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                actClose={()=>this.props.actClose()}
                content='modal__content1'
            >
                <div className="modal__message" dangerouslySetInnerHTML={{ __html: this.props.msg }}></div>
                <div className="modal__btnok btn_response" id="pre-register-success-confirmation" onClick={()=>this.props.actClose()}/>
            </Modal>
        )
  }
}
