import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { setValues } from './../store/redux';
import styled from 'styled-components';
import imgList from './../constants/ImportImages';
import F11Layout from './../features/f11Layout';
import Modals from './../features/modals';
import { apiPost } from './../constants/Api';

const Home = (props) => {
    const [scroll, setScroll] = useState(null);
    const apiEventInfo = () => {
        let dataSend = { type: 'event_info' };
        let successCallback = (res) => {
            if (res.status) {
                props.setValues({
                    ...res.data,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        apiPost(
            'REACT_APP_API_POST_EVENT_INFO',
            props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );
    };

    const getItem = () => {
        let dataSend = { type: '' };
        let successCallback = (res) => {
            if (res.status) {
                props.setValues({
                    ...res.data,
                    modal_open: 'message',
                    modal_message: res.message,
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        apiPost(
            'REACT_APP_API_POST_GET_ITEM',
            props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );
    };

    useEffect(() => {
        if (props.jwtToken !== '' && !props.status) apiEventInfo();
    }, [props.jwtToken]);

    return (
        <F11Layout
            page={'main'}
            objScroll={(e) => {
                if (e) setScroll(e);
            }}
        >
            <Modals />
            <HomeStyle bg={imgList.bg_home_1}>
                {props.can_get_package && props.can_get_package === 1 ? (
                    <Btn
                        onClick={() => getItem()}
                        name='receive'
                        src={imgList.btn_receive}
                    />
                ) : (
                    <Btn
                        onClick={() => getItem()}
                        name='receive'
                        received
                        src={imgList.btn_received}
                    />
                )}
            </HomeStyle>
        </F11Layout>
    );
};

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues };
export default connect(mapStateToProps, mapDispatchToProps)(Home);

const HomeStyle = styled.div`
    position: relative;
    background: url(${(props) => props.bg});
    height: 2034px;
`;

const Btn = styled.a`
    display: block;
    width: 326px;
    height: ${(props) => (props.received ? '90px' : '91px')};
    margin: 0 auto;
    top: 1370px;
    position: relative;
    background-image: url(${(props) => props.src});
    background-position: top center;
    cursor: ${(props) => (props.received ? '' : 'pointer')};
    pointer-events: ${(props) => (props.received ? 'none' : '')};
    &:hover {
        background-position: ${(props) =>
            props.received ? '' : 'bottom center'};
    }
`;
