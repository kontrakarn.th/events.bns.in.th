const BG =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/newbie_free_support_package/bg_home_f11.png';
const BtnReceive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/newbie_free_support_package/btn_receive.png';
const BtnReceived =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/newbie_free_support_package/btn_received.png';
const ModalMessage =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/newbie_free_support_package/modal_message.png';
const BtnConfirm =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/newbie_free_support_package/btn_confirm.png';

const menu_background =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/menu_background.png';
const permission =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/permission.jpg';
const icon_scroll =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/icon_scroll.png';
const btn_home =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/mystic/btn_home.png';

// home
const bg_home_1 = BG;
const btn_receive = BtnReceive;
const btn_received = BtnReceived;
const btn_confirm = BtnConfirm;

// modal
const modal_message = ModalMessage;

export default {
    permission,
    icon_scroll,
    btn_home,
    menu_background,

    // home
    bg_home_1,
    btn_receive,
    btn_received,
    btn_confirm,

    //modal
    modal_message,
};
