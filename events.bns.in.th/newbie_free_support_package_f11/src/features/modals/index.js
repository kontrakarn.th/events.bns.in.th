import React from 'react';

import ModalMessage from './_Message';
import ModalLoading from './_Loading';

export default (props) => (
    <>
        <ModalLoading />
        <ModalMessage />
    </>
);
