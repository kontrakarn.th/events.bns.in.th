import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import OauthLogin from '../features/oauthLogin';
import Home from '../pages/Home';

export default (props) => {
    let eventPath = process.env.REACT_APP_EVENT_PATH;
    return (
        <BrowserRouter>
            <>
                <Route path={eventPath} component={OauthLogin} />
                <Route path={eventPath} exact component={Home} />
            </>
        </BrowserRouter>
    );
};
