import React, { Component } from 'react'
import {connect} from 'react-redux'
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch'
import './../styles/index.css'
import character01 from './../static/images/character01.png'
import character02 from './../static/images/character02.png'
import Head from '../components/Head';
import Promotion from '../components/Promotion'
import Redeem from '../components/Redeem'
import ModalConfirmToRedeem from '../components/ModalConfirmToRedeem'
import ModalMessage from '../components/ModalMessage'
import {Permission} from '../components/Permission'
import ModalSelectCharacter from '../components/ModalSelectCharacter'
import home from './../static/images/btn_home.png'
import {
    onAccountLogout
} from '../actions/AccountActions';

class HomeContainer extends Component {
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: false,
            modalConfirmToRedeem:false,
            modalMessage:false,
            alertMessage:'',
            packages:[],
            pointarr:[],
            package_select:{}
        }

        this.confirmRedeem.bind(this)
    }
    componentDidUpdate(prevProps, prevState) {
        if(prevProps.userData != this.props.userData){
            this.getEventInfo()
        }
    }

    getEventInfo(){
        let self = this;
        fetch(process.env.REACT_APP_API_SERVER_HOST + process.env.REACT_APP_API_POST_EVENT_INFO, {
                method: 'POST',
                body: JSON.stringify({
                    "type":"event_info"
                }),
                credentials: 'same-origin',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'
                }
            }).then(response => response.json()).then(response=>{
                // console.log(response)
                if(response.status){
                    this.setState({
                        packages: response.content.packages,
                        pointarr: response.content.mypoint_arr
                    });
                }else{
                    if(response.type=='no_permission'){
                        this.setState({
                            permission:false
                        })
                    }
                    this.setState({
                        packages:[],
                        pointarr:[]
                    })
                }

            }, error => {

            });
    }

    redeemItem(package_data){
        let self = this;

        this.setState({
            showLoading:true,
            modalConfirmToRedeem:false,
            modalMessage:true,
            alertMessage:'โปรดรอการทำรายการ'
        });
        setTimeout(function(){
            fetch(process.env.REACT_APP_API_SERVER_HOST + process.env.REACT_APP_API_POST_REDEEM, {
                    method: 'POST',
                    body: JSON.stringify({
                        "type":"redeem",
                        "package_id":package_data.package_id
                    }),
                    credentials: 'same-origin',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'
                    }
                }).then(response => response.json()).then(response=>{

                    self.setState({
                        showLoading:false,
                        modalMessage:true,
                        alertMessage:response.message
                    });
                    self.getEventInfo()
                }, error => {

                });
        }, 2000);

    }

    confirmRedeem(package_data){
        this.setState({
            package_select:package_data,
            modalConfirmToRedeem:true
        });
    }

    closeModal(){
        this.setState({
            modalConfirmToRedeem: false,
            modalMessage: false,
        });
    }

    render() {
            return (
                <div>
                    {
                        this.state.permission
                        ?
                            <div className="events-bns">
                                <ScrollArea
                                    speed={0.8}
                                    className="area"
                                    contentClassName=""
                                    horizontal={false}
                                    style={{ width: '100%', height:700, opacity:1}}
                                    verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                                >
                                    <div className="topup">
                                        <Head userData={this.props.userData} loginUrl={this.props.loginUrl} logoutUrl={this.props.logoutUrl} onAccountLogout={this.props.onAccountLogout}  />
                                        <Promotion />
                                        {
                                            this.state.packages.length>0
                                            ?
                                                <Redeem pointarr={this.state.pointarr} packages={this.state.packages} confirmRedeem={this.confirmRedeem.bind(this)} showLoading={this.state.showLoading} />
                                            :
                                                null
                                        }


                                        <div className="topup__fashion">
                                            <img src={character01} />
                                        </div>
                                        <div className="topup__fashion">
                                            <img src={character02} />
                                        </div>

                                        <ModalConfirmToRedeem
                                            package_select={this.state.package_select}
                                            open={this.state.modalConfirmToRedeem}
                                            redeemItem={this.redeemItem.bind(this)}
                                            closeModal={this.closeModal.bind(this)}
                                        />
                                        <ModalMessage
                                            open={this.state.modalMessage}
                                            showLoading={this.state.showLoading}
                                            message={this.state.alertMessage}
                                            closeModal={this.closeModal.bind(this)}
                                        />
                                        {/*<ModalSelectCharacter
                                            open={this.state.modalSelectCharacter}
                                        />*/}


                                        <div className="topup__home">
                                            <a href={process.env.REACT_APP_API_SERVER_HOST}><img src={home} /></a>
                                        </div>
                                    </div>
                                </ScrollArea>
                            </div>
                            
                        :
                            <Permission />
                    }
                </div>
            )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
    onAccountLogout: () => {
        dispatch(onAccountLogout());
    }
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
