import React, { Component } from 'react'
import redeemTitle from './../static/images/title_redeem.png'
import character from './../static/images/img_character.png'
import textPoint from './../static/images/text_point.png'
import redeem1 from './../static/images/redeem01.png'
import redeem2 from './../static/images/redeem02.png'
import redeem3 from './../static/images/redeem03.png'
import redeem4 from './../static/images/redeem04.png'
import redeem5 from './../static/images/redeem05.png'
import redeem6 from './../static/images/redeem06.png'
import redeem7 from './../static/images/redeem07.png'

import point0 from './../static/images/number/0.png'
import point1 from './../static/images/number/1.png'
import point2 from './../static/images/number/2.png'
import point3 from './../static/images/number/3.png'
import point4 from './../static/images/number/4.png'
import point5 from './../static/images/number/5.png'
import point6 from './../static/images/number/6.png'
import point7 from './../static/images/number/7.png'
import point8 from './../static/images/number/8.png'
import point9 from './../static/images/number/9.png'
import RedeemList from './RedeemList'
import RedeemList2 from './RedeemList2'

const set_number=[point0,point1,point2,point3,point4,point5,point6,point7,point8,point9]

export default class Redeem extends Component {
    render() {
        return (
            <div className="topup__redeem">
                <div className="topup__title"><img src={redeemTitle} alt="แลกของรางวัล" /></div>
                <div className="topup__redeem-content">
                    <div className="topup__redeem-score">
                        <div className="topup__character">
                            <img src={character} alt="character" />
                        </div>
                        <div className="topup__inline--absolute">
                            <img src={textPoint} />
                            <div>
                            {
                                this.props.pointarr.map((data,index)=>{
                                        return (<img src={set_number[data]} />)
                                })
                            }
                            </div>
                        </div>
                    </div>
                    <div>
                        <RedeemList image={redeem1} package={this.props.packages[6]} confirmRedeem={this.props.confirmRedeem} showLoading={this.props.showLoading} />
                        <RedeemList image={redeem2} package={this.props.packages[5]} confirmRedeem={this.props.confirmRedeem} showLoading={this.props.showLoading} />
                        <RedeemList image={redeem3} package={this.props.packages[4]} confirmRedeem={this.props.confirmRedeem} showLoading={this.props.showLoading} />
                    </div>
                    <div>
                        <div className="topup__redeem-blog topup__redeem-blog--2">
                            <RedeemList2 image={redeem4} package={this.props.packages[3]} confirmRedeem={this.props.confirmRedeem} showLoading={this.props.showLoading} />
                            <RedeemList2 image={redeem5} package={this.props.packages[2]} confirmRedeem={this.props.confirmRedeem} showLoading={this.props.showLoading} />
                        </div>
                        <div className="topup__redeem-blog topup__redeem-blog--2">
                            <RedeemList2 image={redeem6} package={this.props.packages[1]} confirmRedeem={this.props.confirmRedeem} showLoading={this.props.showLoading} />
                            <RedeemList2 image={redeem7} package={this.props.packages[0]} confirmRedeem={this.props.confirmRedeem} showLoading={this.props.showLoading} />
                        </div>
                    </div>
                </div>
            </div>
        )
  }
}
