import React, { Component } from 'react'
export default class ModalConfirmToRedeem extends Component {

    constructor(props){
        super(props)
        this.state={
            redeemlist:[]
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.package_select!=this.props.package_select){
            let sep=this.props.package_select.package_desc.split("|")
            this.setState({
                redeemlist:sep
            });
        }
    }

    componentWillMount() {
        if(Object.keys(this.props.package_select).length>0){
            let sep=this.props.package_select.package_desc.split("|")
            this.setState({
                redeemlist:sep
            });
        }
    }

    render() {
        return (
            <div className={"modal "+(this.props.open ? "":"close")} >
                <div className="modal__backdrop" />
                <div className={"modal__content modal__content--height"}>
                    <div className={"modal__incontent modal__incontent--selectcharacter"}>
                        ยืนยัน<br />
                        <div className="modal__content-inner">
                            การแลก
                            <ul className="modal__redeemlist">
                                {this.state.redeemlist.map((item,index) => {
                                    return (
                                        <li key={index}>{item}</li>
                                    )
                                })}
                            </ul>
                        </div>
                    </div>
                    <div className={"modal__bottom"}>
                        <a onClick={()=>this.props.redeemItem(this.props.package_select)} className="modal__button">
                            ตกลง
                        </a>
                        <a className="modal__button" onClick={()=>this.props.closeModal()}>
                            ยกเลิก
                        </a>
                    </div>
                </div>
            </div>
        )
  }
}
