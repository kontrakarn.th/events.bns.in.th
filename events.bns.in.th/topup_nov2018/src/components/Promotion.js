import React, { Component } from 'react'
import promotionTitle from './../static/images/title_condition.png'
export default class Promotion extends Component {
    render() {
        return (
            <div className="topup__promotion">
                <div className="topup__title"><img src={promotionTitle} /></div>
                <div className="topup__promotion-content">
                    <ol className="topup__promotion-list">
                        <li>ระยะเวลากิจกรรม 21 พ.ย. 2561 (00:00 น.) - 12 ธ.ค. 2561 (23:59 น.)</li>
                        <li>ผู้เล่นสามารถสะสมคะแนน โดยแลกไดมอนด์เข้าเกมในทุกช่องทาง</li>
                        <li>การสะสมคะแนนสามารถนำไปแลกชุดแฟชั่นและโบนัสไดมอนด์ภายในโปรโมชั่นนี้ได้</li>
                        <li>อัตราการได้รับคะแนน 1 คะแนน เท่ากับ 1 ไดมอนด์</li>
                    </ol>
                </div>
            </div>
        )
  }
}
