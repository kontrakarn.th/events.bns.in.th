import React from 'react';
import {connect} from 'react-redux';
import {getCookie, setCookie, delCookie} from './../constants/Cookie';
import {QueryString} from './../constants/QueryString';
import {onAccountAuth, onAccountAuthed, onAccountLogin, setLoginUrl, setLogoutUrl} from './../actions/AccountActions';


import KJUR from 'jsrsasign';
import $ from "jquery";

/**
 * This container deals with SSO authentication; thus the name "SSOMiddleware".
 */
class SSOMiddleware extends React.Component {

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.authState !== this.props.authState) {
			switch (this.props.authState) {
				case "LOGGED_IN":
					this.login();
					break;
				case "LOGGED_OUT":
					this.logout();
					break;
				default :

			}
		}
	}
	componentDidMount() {
		const {dispatch} = this.props;
		// set login & logout url
		let currentLoc = window.location.href.toString();
		currentLoc = currentLoc.substring(currentLoc.length - 1) === "/" ? currentLoc : currentLoc + "/";
		let loginUrl = process.env.REACT_APP_SSO_URL + "/ui/login?app_id="
			+ process.env.REACT_APP_SSO_APP_ID + "&redirect_uri=" + currentLoc + "&locale="
			+ process.env.REACT_APP_LOCALE + "&display=page&display=page&all_platform=1&theme=mshop_iframe_white";
		dispatch(setLoginUrl(loginUrl));

		if (QueryString[process.env.REACT_APP_SSO_PARAM_NAME]) {
			dispatch(onAccountAuth(QueryString[process.env.REACT_APP_SSO_PARAM_NAME]));
			dispatch(onAccountLogin());
		}
	}

	componentWillMount() {
		this.setupAjax();

        if (!this.isLoggedIn()) {
            this.checkAuth(false);
        }
	}

	setupAjax() {
        // check csrf token
        let token = getCookie(process.env.REACT_APP_CSRF_COOKIE_NAME);
        let headers = {};
        if (token) {
            delCookie(process.env.REACT_APP_CSRF_COOKIE_NAME);
            headers["X-CSRF-TOKEN"] = token;
        }

        /**
         * JWT
         */
        let beforeSend = (xhr, data) => {
            if (data.data) {
                // Header
                let oHeader = {
                    alg: 'HS256',
                    typ: 'JWT'
                };
                // Sign JWT
                let sHeader = JSON.stringify(oHeader);
                let sPayload = '{"' + decodeURI(data.data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}';
                let sJWT = KJUR.jws.JWS.sign("HS256", sHeader, sPayload, process.env.REACT_APP_JWT_SECRET);

                // Separate jwt header and signature and put them in authorization
                let parts = sJWT.split(".");
                xhr.setRequestHeader("Authorization", (["G", "a", "r", "e", "n", "a", "T", "H"].join("")) + " " + btoa(parts[0] + "." + parts[2]));
				// console.log("xhr",xhr);
                // Put jwt payload inside body
                data.data = btoa(parts[1]);
            }
        }

        $.ajaxSetup({
            headers: headers,
            beforeSend: beforeSend
        });
    }

    isLoggedIn() {
        return this.props.userData && this.props.userData.uid;
    }

	checkAuth(async) {
		const {dispatch} = this.props;
		$.ajax({
			type: 'POST',
			url: process.env.REACT_APP_API_SERVER_HOST + process.env.REACT_APP_API_GET_ACCOUNT_INFO,
			success: function(data) {
				let currentLoc = window.location.href.toString();
				if (data.account_info) {
					// session key exists in cookies when you are logged in
					let sessionKey = getCookie(process.env.REACT_APP_SSO_COOKIE_NAME);
					dispatch(onAccountAuth(sessionKey));
					dispatch(onAccountAuthed(data.account_info));
					// let currentLoc = window.location.href.toString();
					currentLoc = currentLoc.substring(currentLoc.length - 1) === "/" ? currentLoc : currentLoc + "/";
					let logoutUrl = process.env.REACT_APP_SSO_URL + '/ui/logout?app_id='
						+ process.env.REACT_APP_SSO_APP_ID + '&redirect_uri=' + currentLoc
						+ '&session_key=' + sessionKey + "&uid=" + data.account_info.uid;
					dispatch(setLogoutUrl(logoutUrl));
				} else {
					let loginUrl = process.env.REACT_APP_SSO_URL + "/ui/login?app_id="
						+ process.env.REACT_APP_SSO_APP_ID + "&redirect_uri=" + currentLoc + "&locale="
						+ process.env.REACT_APP_LOCALE + "&display=page&display=page&all_platform=1&theme=mshop_iframe_white";
					window.location.href = loginUrl;
				}
			},
			dataType: 'JSON',
			async: async,
			error: () => {
				console.log("error");
			}
		});
	}

	login() {
		setCookie(process.env.REACT_APP_SSO_COOKIE_NAME, this.props.sessionKey, 30, process.env.REACT_APP_COOKIE_DOMAIN);
		let uri = window.location.href.substring(0, window.location.href.lastIndexOf("?")); // get rid of the query string
		window.location.href = uri; // we need hard refresh
	}

	logout() {
		delCookie(process.env.REACT_APP_SSO_COOKIE_NAME, process.env.REACT_APP_COOKIE_DOMAIN);
		window.location.href = this.props.logoutUrl;
	}

	render() {
		// console.log(process.env);
		return (
			<div id="app">
				{this.props.children ? this.props.children : ""}
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	sessionKey: state.AccountReducer.sessionKey,
	userData: state.AccountReducer.userData,
	authState: state.AccountReducer.authState,
	loginUrl: state.AccountReducer.loginUrl,
	logoutUrl: state.AccountReducer.logoutUrl
})

const mapDispatchToProps = (dispatch) => ({
	dispatch: dispatch
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SSOMiddleware);
