import React from 'react';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { setValues } from './../../store/redux';

import {Imglist} from './../../constants/Import_Images';
import Menu from '../../features/menu';
import ScrollArea from 'react-scrollbar';

class History extends React.Component {

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiItemHistory() {
        this.setState({ modal_open: 'loading' });
        let self = this;
        let dataSend = { type: "historylist" };
        let successCallback = (data) => {
            this.props.setValues({
                modal_open: '',
                items_history: data.content
            })

        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    modal_open: '',
                    permission: false,
                });

            }else {
                this.props.setValues({
                    modal_open: 'message',
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_HISTORYLIST", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiItemHistory();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiItemHistory();
        }
    }
    render() {
        return (
            <PageWrapper>
                <Menu page='history'/>
                
                <ContentFrame>
                        <TitleBox>- ประวัติการแลก -</TitleBox>
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: '100%', opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#000000", opacity: 1 }}
                        >
                            <ul className="list">
                                {
                                    this.props.items_history && this.props.items_history.length > 0 &&
                                    this.props.items_history.map((items, index) => {
                                        if(items.send_type=="owner"){
                                            if(items.product_title=="เซ็ทชุดเกราะนักรบหมาป่า"){
                                                return(
                                                    <li>
                                                        <div className="history__list--left">ส่ง {items.product_title} ให้ตัวเอง</div>
                                                        <div className="history__list--right">{items.updated_at}</div>
                                                    </li>
                                                )
                                            }else{
                                                return(
                                                    <li>
                                                        <div className="history__list--left">ได้รับ {items.product_title}</div>
                                                        <div className="history__list--right">{items.updated_at}</div>
                                                    </li>
                                                )
                                            }

                                        }else if(items.send_type=="none"){
                                            return(
                                                <li>
                                                    <div className="history__list--left">จัดเก็บ {items.product_title} ในเว็ป</div>
                                                    <div className="history__list--right">{items.updated_at}</div>
                                                </li>
                                            )
                                        }else{
                                            return(
                                                <li>
                                                    <div className="history__list--left">ส่ง {items.product_title} ให้(UID {items.send_to})</div>
                                                    <div className="history__list--right">{items.updated_at}</div>
                                                </li>
                                            )
                                        }

                                    })
                                }
                            </ul>
                        </ScrollArea>
                </ContentFrame>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(History);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['other_bg']});
    width: 1105px;
    padding: 100px 0 40px;
    text-align: center;
`

const ContentFrame = styled.div`
    background: top center no-repeat url(${Imglist['frame']});
    width: 990px;
    height: 560px;
    display: block;
    margin: 0 auto;
    position: relative;
    box-sizing: border-box;
    padding: 7% 10%;
    color: #fff;
    .list{
        margin: 0;
        padding: 0;
        list-style-type: none;
        height: 100%;
        overflow: auto;
        >li{
            color: #ffffff;
            display: flex;
            justify-content: space-between;
            align-items: center;
            white-space: nowrap;
            >div:first-child{
                width: 60%;
                text-align: left;
            }
            >div:nth-child(2){
                width: 30%;
                text-align: center;
            }
            /* >div:last-child{
                width: 15%;
                text-align: center;
            } */
        }
    }
`
const TitleBox = styled.div`
    font-size: 21px;
`
const CustomBtn = styled.div`
    width: 225px;
    height: 51px;
    background: top center no-repeat url(${Imglist['btn']});
    color: #ffffff;
    cursor: pointer;
    font-size: 1.5em;
    line-height: 2em;
    font-weight: 600;
    margin: 17px auto 0;
    &:hover{
        background-position: bottom center;
    }
`
