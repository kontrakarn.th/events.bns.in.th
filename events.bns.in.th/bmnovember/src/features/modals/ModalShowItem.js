import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';

import {Imglist} from '../../constants/Import_Images';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="item"
            actClickOutside={()=>props.setValues({modal_open:""})}
            key={`modal_${props.modal_open}_${props.modal_item}`}
        >
            <ModalContent>
                <div className="contenttitle">- ภายในกล่อง เซ็ทเกราะนักรบหมาป่า -</div>
                <CloseBtn src={Imglist.close_btn}  alt="" onClick={()=>props.setValues({modal_open: ''})}/>
                <div className="">
                    <div className="items">
                        <img src={Imglist.items_details1} alt=""/>
                        <div className="items-name">ชุดกระเกราะนักรบหมาป่า</div>
                    </div>
                    <div className="items">
                        <img src={Imglist.items_details2} alt=""/>
                        <div className="items-name">ผ้าคลุมเกราะนักรบหมาป่า</div>
                    </div>
                    <div className="items">
                        <img src={Imglist.items_details3} alt=""/>
                        <div className="items-name">หมวกเกราะนักรบหมาป่า</div>
                    </div>
                    <div className="items">
                        <img src={Imglist.items_details4} alt=""/>
                        <div className="items-name">กล่องอาวุธลวงตานักรบหมาป่า</div>
                    </div>
                </div>
            </ModalContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalContent = styled.div`
    background: no-repeat center url(${Imglist.modal_bg_details});
    position: relative;
    display: block;
    text-align: center;
    box-sizing: border-box;
    width: 870px;
    height: 520px;
    padding: 8% 12%;
    color: #fff;
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    .contenttitle{
        font-family: 'Kanit-Regular';
        font-size: 21px;
        text-shadow: 1px 1px 1px #000;
        margin: 30px 0;
    }
    .items{
        display:inline-block;
        width: 25%;
        padding: 0 20px;
        vertical-align: middle;
        text-align: center;
    }
    .items-name{
        font-size: 14px;
        line-height: 1;
    }
    .contenttext{
        display: flex;
        justify-content: center;
        align-items: center;
    }
`;

const CloseBtn = styled.img`
    position: absolute;
    top: -30px;
    right: 50px;
    cursor:pointer;
`
