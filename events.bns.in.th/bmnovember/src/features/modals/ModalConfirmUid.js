import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';

import modal_bg from './images/modal_bg.png';
import {Imglist} from './../../constants/Import_Images';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="uid"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <div className="contenttitle">ยืนยัน</div>
                <div className="contenttext">
                    <div>
                        <div>กรุณากรอก UID ของเพื่อน</div>
                        <Input
                            id="f_uid"
                            type="text"
                            value={props.modal_uid}
                            onChange={(e)=>props.setValues({modal_uid:e.target.value})}
                        />
                    </div>
                </div>
                <div>
                    <Btns onClick={()=>{if(props.actConfirm)props.actConfirm()}}>ตกลง</Btns>
                    <Btns onClick={()=>props.setValues({modal_open:''})}>ยกเลิก</Btns>
                </div>      
            </ModalMessageContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 530px;
    height: 350px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist.modal_bg});
    box-sizing: border-box;
    padding: 9%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 21px;
        line-height: 1.5em;
        word-break: break-word;
        height: 180px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .contenttitle{
       font-family: 'Kanit-Regular';
       font-size: 21px;
       text-shadow: 1px 1px 1px #000;
    }
`;
const ModalBottom = styled.div`
    display: block;
    position: absolute;
    bottom: 0px;
    z-index: 50;
`;

const Btns = styled.div`
    background: no-repeat top center url(${Imglist.btn_default});
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    width: 100px;
    height: 42px;
    line-height: 1.7;
    font-size: 20px;
    &:hover{
        background-position: bottom center;
    }
`
const Input = styled.input`
    display: block;
    cursor: pointer;
    width: 90%;
    margin-top: 20px;
    text-align: center;
    font-size: 1em;
    border: 0;
    padding: 5px 10px;
`
