import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';

import {Imglist} from '../../constants/Import_Images';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="get_item"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <div className="contenttitle">ยินดีด้วย</div>
                <div className="items">
                    <img src={props.item_get.id >0 ? Imglist["items"+(props.item_get.id)] : ""} alt=""/>
                    <div className="items-name">ได้รับ {props.item_get.id >0 ? props.item_get.product_title : ""}</div>
                    <div className="contenttext" dangerouslySetInnerHTML={{__html: modal_message}} />
                </div>
                <div className="get_item_btn">
                    <Btns onClick={()=>props.setValues({modal_open:'', modal_message: '',item_get:{id: 0,
                    item_type: "",
                    product_title: "",
                    icon: "",
                    key: ""} })}>ตกลง</Btns>
                </div>
            </ModalMessageContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 530px;
    height: 350px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist.modal_bg});
    box-sizing: border-box;
    padding: 9%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 1em;
        line-height: 1.5em;
        word-break: break-word;
        height: auto;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .contenttitle{
       font-family: 'Kanit-Regular';
       font-size: 21px;
       text-shadow: 1px 1px 1px #000;
    }
    .get_item_btn{
        margin-top:1em;
    }
    .items{
        .items-name{
            margin-top: -10px;
        }
        img{
            width:20%;
            margin:0.9em 0;
        }
    }
`;

const Btns = styled.div`
    background: no-repeat top center url(${Imglist.btn_default});
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    width: 100px;
    height: 42px;
    line-height: 1.7;
    font-size: 20px;
    &:hover{
        background-position: bottom center;
    }
`
