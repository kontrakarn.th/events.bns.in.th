import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
import Slider from "react-slick";
import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalShowItem from '../../features/modals/ModalShowItem';
import ModalGetItem from '../../features/modals/ModalGetItem';
import {Imglist} from './../../constants/Import_Images';
import Menu from '../../features/menu';
import ModalConfirmReceive from '../modals/ModalConfirmReceive';

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div className={className} onClick={onClick}><img src={Imglist.btn_next}/></div>
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div className={className} onClick={onClick}><img src={Imglist.btn_prev}/></div>
    );
}

class Main extends React.Component {
    actConfirm(){
        this.apiRandomItem();
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
                this.props.setValues({
                    ...data.data,
                    username:data.data.nickname,
                    modal_open: "",
                });
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CHECKIN", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiRandomItem() {
        this.props.setValues({ modal_open: "loading" });

        let self = this;
        let dataSend = { type: "randomitem" };
        let successCallback = (data) => {
            this.props.setValues({
                item_get:data.content[0],
                modal_open: "get_item",
                modal_message:data.content[0].id==10 ? "ไอเทมถูกส่งเข้าไปที่ระบบส่งของขวัญ" : "ส่งเข้าไปในกล่องจดหมาย กรุณาเข้าเกมเพื่อรับของรางวัล",
                can_play: data.data.can_play,
            })
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });

            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_RANDOM_ITEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,

            items: [
                {
                    image: Imglist.items10,
                    name: 'เซ็ทชุดเกราะนักรบหมาป่า',
                    showDetails: true,
                },
                {
                    image: Imglist.items9,
                    name: 'ชุดวันที่หอมหวาน',
                    showDetails: false,
                },
                {
                    image: Imglist.items8,
                    name: 'คริสตัลอัญมณีหยินหยาง x3',
                    showDetails: false,
                },
                {
                    image: Imglist.items7,
                    name: 'คริสตัลอัญมณีหยินหยาง x2',
                    showDetails: false,
                },
                {
                    image: Imglist.items6,
                    name: 'ไหมห้าสี x15',
                    showDetails: false,
                },
                {
                    image: Imglist.items5,
                    name: 'คริสตัลอัญมณี<br/>ฮงมุน x100',
                    showDetails: false,
                },
                {
                    image: Imglist.items4,
                    name: 'ไหมห้าสี x10',
                    showDetails: false,
                },
                {
                    image: Imglist.items3,
                    name: 'คริสตัลอัญมณีหยินหยาง x1',
                    showDetails: false,
                },
                {
                    image: Imglist.items2,
                    name: 'หินเปลี่ยนรูป<br/>ชั้นสูง x2',
                    showDetails: false,
                },
                {
                    image: Imglist.items1,
                    name: 'หินเปลี่ยนรูป<br/>ชั้นสูง x1',
                    showDetails: false,
                },

            ],
            slider_image: [
                {
                    image: Imglist.slide1,
                    title: 'เซ็ทชุดเกราะนักรบหมาป่า'
                },
                {
                    image: Imglist.slide2,
                    title: 'ชุดวันที่หอมหวาน'
                },
                {
                    image: Imglist.slide3,
                    title: 'อาวุธลวงตานักรบหมาป่า'
                },
            ]
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }

    render() {
        const settings = {
            dots: true,
            infinite: true,
            fade: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };

        return (
            <PageWrapper>
                <Menu page='main'/>
                <section className="preorder__section preorder__section--header">
                    <div className="header">
                        <div className="header__scroll"><img src={Imglist.icon_scroll} alt="" /></div>
                    </div>
                </section>
                <ConditionBoard>
                    <div className="condition">
                        <div className="header">- รายละเอียด -</div>
                        <ol>
                            <li>ผู้เล่นสามารถเปิดกล่องนักรบหมาป่า ในราคา 10,000 ไดมอนด์/ชิ้น ซึ่งจะสุ่มไอเทม 1 ประเภท ตามรายการด้านล่าง</li>
                            <li>ไอเทมกล่องนักรบหมาป่า จะแสดงผลบนเว็บไซต์โปรโมชั่นนี้เท่านั้น</li>
                            <li>ไอเทมทุกชิ้นที่ได้รับจะส่งเข้ากล่องจดหมายในไอดีที่ใช้งานอัตโนมัติ ยกเว้น แพ็คเกจชุดเกราะนักรบหมาป่า</li>
                            <li>หากผู้เล่นได้รับแพ็คเกจชุดเกราะนักรบหมาป่า จะต้องกดรับด้วยตนเองในระบบ 'ส่งของขวัญ' หรือสามารถส่งไอเทมนี้ให้กับผู้อื่นได้ โดยมีค่าใช้จ่ายที่ 10,000 ไดมอนด์/ครั้ง/ชิ้น</li>
                            <li>ระยะเวลาโปรโมชั่น วันที่  13 - 27 พฤศจิกายน 2562 เวลา 23:59 น.</li>
                            <li>ปิดระบบส่งของขวัญ วันที่ 4 ธันวาคม 2562 เวลา 23:59 น.</li>
                        </ol>
                    </div>

                    <div className="items__wrapper">
                        <div className="header">- ไอเทมที่มีโอกาสได้รับ -</div>
                        <div>
                            {
                                this.state.items.map((item,key)=>{
                                    return(
                                        <div key={key} className="items">
                                            <div className="items-inner">
                                                <img src={item.image} alt=""/>
                                                {
                                                    item.showDetails ?
                                                        <div className="icon-plus" onClick={()=>this.props.setValues({modal_open:'item'})}><img src={Imglist.icon_plus}/></div>
                                                    : null
                                                }
                                            </div>
                                            <div dangerouslySetInnerHTML={{__html: item.name}} />
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </ConditionBoard>
                <OpenSoulBoard>
                    <div className="gacha_box">
                        <div className="header">- กล่องนักรบหมาป่า -</div>
                        <img src={Imglist.item_box}/>
                        {
                            this.props.can_play==true
                            ?
                                <div className="btn btn-default" onClick={()=>this.props.setValues({modal_open:'confirm',modal_message:'ยืนยันการเปิดกล่อง<br/> มูลค่า 10,000 ไดมอนด์ ?'})}>เปิดกล่อง</div>
                            :
                                <div className="btn btn-default disable">เปิดกล่อง</div>
                        }

                        <div className="price">ราคา 10,000 <img src={Imglist.diamond}/></div>
                    </div>
                </OpenSoulBoard>

                <CharacterBoard>
                    <div className="header">ตัวอย่าง</div>
                    <Slider {...settings}>
                        {
                            this.state.slider_image.map((items,key)=>{
                                return(
                                    <div key={key} className="">
                                        <p>{items.title}</p>
                                        <img src={items.image}/>
                                    </div>
                                )
                            })
                        }
                    </Slider>
                </CharacterBoard>

                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                <ModalConfirmReceive />
                <ModalShowItem />
                <ModalGetItem />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_costume']}) #11122d;
    width: 1105px;
    padding-top: 650px;
    & .header{
        font-family: 'Kanit-Regular', san-sarif;
        text-align: center;
        margin-bottom: 40px;
    }
`

const ConditionBoard = styled.div`
    margin: 0 auto;
    padding: 6% 15%;
    background: top center no-repeat url(${Imglist['bg_content1']});
    color: #fff;
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & .condition{
        margin-bottom: 55px;

        ol{
            li{
                line-height:2em;
                padding-bottom:0.3em;
            }
        }
    }
    & .items{
        width: 20%;
        display: inline-block;
        vertical-align: top;
        text-align: center;
        padding: 20px;
        &-inner{
            display:inline-block;
            position: relative;
        }
        & .icon-plus{
            position: absolute;
            right: -10px;
            top: -10px;
            cursor: pointer;
        }
    }
    & .items__wrapper{
        margin: 20px 0;
    }

`
const OpenSoulBoard = styled.div`
    margin: 0 auto;
    padding: 7% 15% 20%;
    background: top center no-repeat url(${Imglist['bg_content2']});
    color: #fff;
    text-align: center;
    position: relative;
    & .btn-default{
        background: top center no-repeat url(${Imglist['btn_default']});
        width: 100px;
        height: 42px;
        cursor:pointer;
        margin: 30px auto;
        font-size: 18px;
        line-height: 35px;
        &:hover{
           background-position: bottom center;
        }
        &.disable{
            filter: grayscale(1);
            pointer-events:none;
        }
    }
    & .price{
        color: #ffd427;
        font-family: 'Kanit-Medium';
    }
    & img{
        vertical-align: middle;
    }
    & .header{
        font-family: 'Kanit-Regular', san-sarif;
        text-align: center;
        margin-bottom: 40px;
    }
    & .gacha_box{
        font-size:1.2em;
    }
`
const CharacterBoard = styled.div`
    margin: 0 auto;
    padding: 7% 15% 25%;
    background: top center no-repeat url(${Imglist['bg_content3']});
    text-align: center;
    color: #fff;
    & .slick-slide img{
        margin: 0 auto;
    }
    & .slick-dots li.slick-active button:before {
        opacity: 1;
        color: #fff;
    }
    & .slick-dots li button:before{
        font-size: 11px;
        opacity: 1;
        color: #646464;
    }
    & .slick-prev,
    & .slick-next{
        width: auto;
        height: auto;
        z-index: 1;
        &::before{
            font-size:0;
        }
    }
`
