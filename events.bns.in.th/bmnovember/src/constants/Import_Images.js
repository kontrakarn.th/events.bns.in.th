
import bg_costume from '../static/images/bg_costume.jpg';
import icon_menu from '../static/images/icon_menu.png';
import menu_close from '../static/images/menu_close.png';
import menu_bg from '../static/images/menu_bg.jpg';
import btn from '../static/images/btn.png';


import bg_content1 from './../static/images/bg_content1.png'
import bg_content2 from './../static/images/bg_content2.png'
import content2_character from './../static/images/content2_character.png'
import bg_content3 from './../static/images/bg_content3.png'

import item_box from './../static/images/item_box.png'
import icon_scroll from './../static/images/icon_scroll.png'

import items1 from './../static/images/item/1.png'
import items2 from './../static/images/item/2.png'
import items3 from './../static/images/item/3.png'
import items4 from './../static/images/item/4.png'
import items5 from './../static/images/item/5.png'
import items6 from './../static/images/item/6.png'
import items7 from './../static/images/item/7.png'
import items8 from './../static/images/item/8.png'
import items9 from './../static/images/item/9.png'
import items10 from './../static/images/item/10.png'



import icon_plus from './../static/images/icon_plus.png'
import line from './../static/images/line.png'
import btn_default from './../static/images/btn-default.png'
import diamond from './../static/images/diamond.png'

import slide1 from './../static/images/slide/1.png'
import slide2 from './../static/images/slide/2.png'
import slide3 from './../static/images/slide/3.png'
import btn_next from './../static/images/btn_next.png'
import btn_prev from './../static/images/btn_prev.png'

import modal_bg from './../static/images/modal_bg.png'
import modal_bg_details from './../static/images/modal_bg_details.png'
import close_btn from './../static/images/close.png'

import items_details1 from './../static/images/items_details/1.png'
import items_details2 from './../static/images/items_details/2.png'
import items_details3 from './../static/images/items_details/3.png'
import items_details4 from './../static/images/items_details/4.png'
import other_bg from './../static/images/other_bg.jpg'
import frame from './../static/images/frame.png'

import send_test from './../static/images/send/test.png'

export const Imglist = {
	btn,
	icon_menu,
	menu_close,
	menu_bg,
	bg_costume,
	item_box,
	icon_scroll,

	bg_content1,
	bg_content2,
	content2_character,
	bg_content3,
	items1,
	items2,
	items3,
	items4,
	items5,
	items6,
	items7,
	items8,
	items9,
	items10,
	icon_plus,
	line,
	btn_default,
	diamond,
	slide1,
	slide2,
	slide3,
	btn_next,
	btn_prev,
	modal_bg,
	modal_bg_details,
	close_btn,
	items_details1,
	items_details2,
	items_details3,
	items_details4,
	other_bg,
	frame,
	send_test
}
