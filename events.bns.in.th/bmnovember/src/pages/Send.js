import React                from 'react';
import Style                from 'styled-components';
import F11Layout            from './../features/F11Layout/';
import Menu                 from './../features/menu';
import { connect }          from 'react-redux';
import { setValues }        from './../store/redux';
import {Imglist}            from './../constants/Import_Images';
import {Frame}              from './../common/frame';
import {CustomBtn}          from './../common/buttons';
import {Item}               from './../common/item';
import { Link, Redirect }   from 'react-router-dom';
import ModalConfirmReceive  from './../features/modals/ModalConfirmReceive';
import ModalConfirmSend     from './../features/modals/ModalConfirmSend';
import ModalConfirmUid      from './../features/modals/ModalConfirmUid';
import {apiPost}              from './../constants/Api';

export class Send extends React.Component {
    apiGetExchangeInfo() {
        this.props.setValues({ modal_open: 'loading' });
        let self = this;
        let dataSend = { type: "getexchangeinfo" };
        let successCallback = (data) => {
                this.props.setValues({
                    permission: true,
                    modal_open: '',
                    exchangeinfo: data.content,
                })

        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    modal_open: '',
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    modal_open: 'message',
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_GETEXCHANGEINFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSendExchangeToMe() {

        this.props.setValues({ modal_open: 'loading',target_uid:-1,target_username:"" });
        let self = this;
        let dataSend = { type: "exchangeitem" };
        let successCallback = (data) => {

            this.props.setValues({
                modal_open: 'message',
                modal_message: "ส่งไอเท็มสำเร็จ",
                exchangeinfo: data.content,
            })

        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    modal_open: 'loading',
                    permission: false,
                });

            }else {
                this.props.setValues({
                    modal_open: 'loading',
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EXCHANGEITEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSendExchangeToOther() {
        // this.resetState()
        if(this.props.target_uid<=0){
            this.props.setValues({
                modal_open: 'message',
                modal_message: "โปรดกรอก UID ที่ต้องการส่ง",
            });
        }else{
            this.props.setValues({ modal_open: 'loading' });
            let self = this;
            let dataSend = { type: "exchangeitem",sendto:this.props.target_uid };
            let successCallback = (data) => {

                this.props.setValues({
                    permission: true,
                    exchangeinfo: data.content,
                    modal_open: 'message',
                    modal_message: "ส่งไอเท็มสำเร็จ",
                })

            };
            const failCallback = (data) => {
                if (data.type == 'no_permission') {
                    this.props.setValues({
                        modal_open: '',
                        permission: false,
                    });

                }else {
                    this.props.setValues({
                        modal_open: 'message',
                        modal_message: "ส่งไอเท็มสำเร็จ",
                    });
                }
            };
            apiPost("REACT_APP_API_POST_EXCHANGEITEM", this.props.jwtToken, dataSend, successCallback, failCallback);
        }

    }

    apiGetName() {
        this.props.setValues({ modal_open: 'loading' });
        let self = this;
        let dataSend = { type: "getname",sendto:document.getElementById('f_uid').value };
        let successCallback = (data) => {
            this.props.setValues({
                ...data.content,
                modal_open: 'send',
                modal_message:"ยืนยันการส่งแพ็คเกจ<br/>ค่าธรรมเนียมการส่ง 10,000 ไดมอนด์ <br/>ไปยัง UID : "+data.content.target_uid+" ("+data.content.target_username+")"
            })
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    modal_open: '',
                    permission: false,
                });

            }else {
                this.props.setValues({
                    modal_open: 'message',
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_GET_NAME", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    //==========================================================================

    actOpenModalUid() {
        this.props.setValues({
            modal_open: "uid",
            modal_message: "",
            modal_uid: "",
        })
    }

    actConfirmSend(){
        this.apiSendExchangeToOther();
    }
    //==========================================================================
    constructor(props){
        super(props);
        this.state = {
            select_id: -1,
        }
    }

    componentDidMount() {
        if (this.props.jwtToken !== "") setTimeout(() => this.apiGetExchangeInfo(), 1000);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiGetExchangeInfo();
            }, 1000);
        }
    }

    render() {
        let { amt, icon,id,key,product_title } = this.props.exchangeinfo[0];
        return (
            <F11Layout showUserName={true} showCharacterName={false}>
                <Menu page='send'/>
                <OpenSoulContent>
                    <Frame title="ส่งของขวัญ">
                        <div className="list">
                            <Items>
                                <div>{product_title}</div>
                                <div className="items-image">
                                    <img src={Imglist.items10} alt=""/>
                                </div>
                                <div className="count">จำนวนที่มี {amt}</div>
                            </Items>
                        </div>
                        <div className="btngroup">
                            <a
                                className={"btngroup__btn "+(!amt || amt<=0 ? "disabled" : "")}
                                onClick={()=>this.props.setValues({modal_open:"receive",modal_message:"ยืนยันการรับ "+product_title})}
                            >
                                <CustomBtn>รับของขวัญ</CustomBtn>
                            </a>
                            <a
                                className={"btngroup__btn "+(!amt || amt<=0 ? "disabled" : "")}
                                onClick={()=>this.actOpenModalUid()}
                            >
                                <CustomBtn>ส่งให้เพื่อน</CustomBtn>
                            </a>
                        </div>

                        <img className="char2" src={Imglist['send_char']} />
                    </Frame>
                </OpenSoulContent>
                <ModalConfirmReceive
                    actConfirm={()=>this.apiSendExchangeToMe()}
                />
                <ModalConfirmUid
                    actConfirm={()=>this.apiGetName()}
                />
                <ModalConfirmSend
                    actConfirm={()=>this.apiSendExchangeToOther()}
                />
            </F11Layout>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Send);

const OpenSoulContent = Style.div`
    background: top center no-repeat url(${Imglist['other_bg']}) #11122d;
    width: 1105px;
    padding: 100px 0 40px;
    text-align: center;
    .char {
        position: absolute;
        top: 31%;
        left: -12%;
    }
    .char2 {
        position: absolute;
        top: 23%;
        right: -10%;
    }
    .list {
        margin: 0 auto;
        display: block;
        width: 60%;
        height: 100%;
        &__slot {
            display: inline-block;
            margin: 15px;

            &.lock {
                filter: grayscale(1);
            }
            &.disable {
                pointer-events: none;
                filter: grayscale(1);
                opacity: 0.6;
            }
        }
    }
    .btngroup {
        // position: absolute;
        // bottom: 12px;
        // left: 0px;
        display: block;
        width: 100%;
        margin-top: 50px;
        &__btn {
            display: inline-block
            margin: 0px 10px;
            &.lock,
            &.disabled{
                pointer-events: none;
                filter: grayscale(1);
            }
        }
    }
`;

const Items = Style.div `
    .count{
        border: 1px solid #ac9b7e;
        color: #fff;
        background-color: #0c1014;
        border-radius: 20px;
        display: inline-block;
        padding: 3px 15px;
        font-size: 16px;
    }
    .items-image{
        margin: 30px 0;
    }
`;
