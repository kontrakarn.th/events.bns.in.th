import React from 'react';
import styled from 'styled-components';
import {Imglist} from './../../constants/Import_Images';

export const Frame = props => {
    return (
        <ContentFrame className="\">
            {props.title !== "" &&
                <div className="frame__title">- {props.title} -</div>
            }
            <div className="frame__content">
                {props.children && props.children}
            </div>
        </ContentFrame>
    )
}

const ContentFrame = styled.div`
    background: top center no-repeat url(${Imglist['frame']});
    width: 990px;
    height: 560px;
    display: block;
    margin: 0 auto;
    position: relative;
    box-sizing: border-box;
    padding: 7% 6%;
    color: #fff;
    .frame {
        &__title {
            font-size: 1em;
            line-height: 1.8em;
            color: #ffffff;
            text-align: center;
            font-weight: 600;
            margin-bottom: 40px;
        }
        &__content {

        }
    }
`;
