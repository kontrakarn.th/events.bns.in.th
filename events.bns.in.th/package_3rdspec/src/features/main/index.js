import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';

import { apiPost } from './../../middlewares/Api';

// import { onAccountLogout } from './../../actions/AccountActions';

import { setValues, onAccountLogout } from './../../store/redux';
import { Imglist } from '../../constants/Import_Images';

import { ConfirmModal, MessageModal, LoadingModal, ConfirmReceiveModal, CharacterModal, PurchasedInfoModal } from "../../features/modal";

class Main extends Component {
    constructor(props) {
        super(props)
        this.state = {
            registered: false
        }
    }

    ////////////   API   ////////////

    apiEventInfo(){
        this.props.setValues({
            modal_open: "loading",
        });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content,
                    modal_open: "",
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_message: data.message,
                    modal_open: "message_nochar",
                });

            }else if(data.type=='no_game_info'){
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message_nochar",
                });

            }else {
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiAcceptPurchase(packageId){
        this.props.setValues({
            modal_open: "loading",
        });
        let dataSend = {type:'purchase_package', package_id: packageId};
        let successCallback = (data) => {
            if (data.status) {
                // console.log('data.content:',data.content);
                this.props.setValues({
                    ...data.content,
                    modal_message: data.message,
                    modal_open: "receive",
                });
            }else{
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_message: data.message,
                    modal_open: "message_nochar",
                });
            }else {
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                });
            }
        };
        apiPost("REACT_APP_API_POST_PURCHASE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ////////////   API   ////////////

    componentDidMount() {

        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 800);

    }

    componentDidUpdate(prevProps, prevState) {

        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            setTimeout(() => {
                this.apiEventInfo()
            }, 800);
        }
    }

    handleConfirm(packageId=0,packageName='',packagePrice='') {

        if(packageId == 1 ||  packageId == 2){
            this.props.setValues({
                package_id: packageId,
                modal_open: "confirm",
                modal_message: 'ซื้อ '+packageName+'<br />ราคา '+packagePrice+' ไดมอนด์?',
            });
        }

    }
    handleBuy(){
        this.apiAcceptPurchase(this.props.package_id);
    }

    handleViewPurchased(purchased_package,purchased_date){
        this.props.setValues({
            modal_open: 'purchase_info',
            purchased_message: purchased_package+'<br />วันที่สั่งซื้อ: '+purchased_date,
        });
    }

    onLogout() {
        this.props.onAccountLogout();
    }

    render() {
        let packages = this.props.packages && this.props.packages.length > 0 ? this.props.packages : []
        // console.log("this.props.can_purchase:",this.props.can_purchase)
        return (
            <div className="events-bns">
                <div className="preorder">
                    {/* <div className="preorder__btngroup">
                        <div className="username">xxx xxx</div>
                        <div className="logout">Logout</div>
                    </div> */}

                    <div className="container">
                        {/* <div className="preorder__scroll">
                            <img src={Imglist.iconScroll} alt=""/>
                        </div> */}
                        <div className="block block1">
                            <img src={Imglist.bg_box1}/>
                            <div className="content">
                                {/*<ol className="list_role">
                                    <li>เปิดสั่งซื้อ แพ็คเกจคมดาบเทวาพิทักษ์ และแพ็คเกจเขี้ยวเล็บของหมาป่าทมิฬ</li>
                                    <li>เมื่อทำการสั่งซื้อแล้ว ผู้เล่นจะได้รับไอเทมที่สั่งซื้อทั้งหมด ภายหลังการอัปเดตแพทช์ อุทยานบุปผาพิษ <br />ในวันพุธที่ 15 มกราคม 2563</li>
                                    <li>ไอเทมจะถูกส่งเข้ากล่องจดหมายภายในไอดี</li>
                                    <li>*พิเศษ เฉพาะผู้เล่นที่สั่งซื้อเท่านั้น* จะได้รับไอเทม ปีกอันทรงเกียรติ จำนวน 1 ชิ้น ภายในแพ็คเกจ</li>
                                    <li>จำกัดการสั่งซื้อ แพ็คเกจละ 1 ครั้ง/การีนาไอดี</li>
                                    <li>ผู้เล่นจะไม่ได้รับค่าประสบการณ์ VIP จากโปรโมชั่นนี้</li>
                                    <li>ทางทีมงานขอสงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข หรือรายละเอียดต่างๆ ของโปรโมชั่นโดยไม่ต้องแจ้งให้ทราบล่วงหน้า</li>
                                </ol>*/}
                                {/*<div className="text-center">
                                    <div className="block1_items">
                                        <img src={Imglist.test_items}/>
                                        <div className="btn_zoom" onClick={()=>this.props.setValues({modal_open:'character', modal_message: 'ตัวอย่างปีกอันทรงเกียรติ'})}>
                                            <img src={Imglist.btn_zoom}/>
                                        </div>
                                    </div>
                                </div>*/}
                            </div>
                        </div>
                        <div className="block block2">
                            <img src={Imglist.bg_box2}/>
                            <div className="bottom">
                                <div className="bottom_wrapper">
                                    {
                                        this.props.jwtToken && this.props.jwtToken !== "" ?
                                            (
                                                packages && packages.length > 0 && packages[0].already_purchase == true && packages[0].id == 1 ?
                                                    <div className="btn_default disabled" >ซื้อไปแล้ว</div>
                                                :
                                                    (
                                                        packages && packages.length > 0 && packages[0].can_purchase == true ?
                                                            <div className="btn_default" onClick={()=>{this.handleConfirm(packages[0].id, packages[0].name, packages[0].price)}}>
                                                                ซื้อ
                                                            </div>
                                                        :
                                                            <div className="btn_default disabled">
                                                                ไม่ตรงเงื่อนไข
                                                            </div>
                                                    )
                                            )
                                        :
                                            null
                                    }
                                    {/*<div>ราคา 150,000 ไดมอนด์</div>*/}
                                </div>
                            </div>
                        </div>
                        <div className="block block3">
                            <img src={Imglist.bg_box3}/>
                            <div className="bottom">
                                <div className="bottom_wrapper">
                                {
                                        this.props.jwtToken && this.props.jwtToken !== "" ?
                                            (
                                                packages && packages.length > 0 && packages[1].already_purchase == true && packages[1].id == 2 ?
                                                    <div className="btn_default disabled" >ซื้อไปแล้ว</div>
                                                :
                                                    (
                                                        packages && packages.length > 0 && packages[1].can_purchase == true ?
                                                            <div className="btn_default" onClick={()=>{this.handleConfirm(packages[1].id, packages[1].name, packages[1].price)}}>
                                                                ซื้อ
                                                            </div>
                                                        :
                                                            <div className="btn_default disabled">
                                                                ไม่ตรงเงื่อนไข
                                                            </div>
                                                    )
                                            )
                                        :
                                            null
                                    }
                                    {/*<div>ราคา 150,000 ไดมอนด์</div>*/}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <LoadingModal />
                <ConfirmModal actConfirm={this.handleBuy.bind(this)}/>
                <ConfirmReceiveModal/>
                <CharacterModal/>
                <MessageModal />
                <PurchasedInfoModal
                    msg={this.props.purchased_message}
                />
            </div>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = {
    onAccountLogout,
    setValues
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main)
