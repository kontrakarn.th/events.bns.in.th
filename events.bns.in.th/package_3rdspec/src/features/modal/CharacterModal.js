import React from "react";
import { connect } from "react-redux";
import ModalCore from "./ModalCore.js";
import './styles.scss';
import { setValues, actCloseModal } from "./redux";
import { Imglist } from '../../constants/Import_Images'

const CharacterModal = (props) => {
  let {modal_message} = props;
	return (
    <ModalCore name="character" outSideClick={false}>
			<div className="contentwrap characterModal">
				<div className="btn_close" onClick={() => props.setValues({ modal_open: '' })}>
					<img src={Imglist.close}/>
				</div>
				<div className="" dangerouslySetInnerHTML={{ __html: props.modal_message }}></div>
				<img src={Imglist.character}/>		
			</div>
    </ModalCore>
	)
}

const mapStateToProps = state => ({
  ...state.ModalReducer,
  ...state.Modal
});

const mapDispatchToProps = { setValues, actCloseModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CharacterModal);
