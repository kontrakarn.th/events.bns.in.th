import React from 'react';
import  ModalCore from './ModalCore.js';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';

const MessageModal = (props) => {
	return (
		<ModalCore name="message" outSideClick={false}>
			<div className="contentwrap">
				<div className="text text--head">แจ้งเตือน</div>
				<div className="inner" dangerouslySetInnerHTML={{ __html: props.modal_message }}></div>
				<div className="modal_btn">
					<div className="btn" onClick={() => props.setValues({ modal_open: '' })}>ปิด</div>
				</div>
			</div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageModal);
