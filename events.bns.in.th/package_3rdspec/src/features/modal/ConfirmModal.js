import React from "react";
import { connect } from "react-redux";
import ModalCore from "./ModalCore.js";
import './styles.scss';
import { setValues, actCloseModal } from "./redux";

const ConfirmModal = (props) => {
  let {modal_message} = props;
	return (
    <ModalCore name="confirm" outSideClick={false}>
			<div className="contentwrap">
				<div className="text text--head">ยืนยัน</div>
				<div className="inner" dangerouslySetInnerHTML={{ __html: props.modal_message }}></div>
				<div className="modal_btn">
					{/* <div className="btn" onClick={()=>props.setValues({modal_open:'receive', modal_message: 'ซื้อพรีออเดอร์แพ็คเกจ<br/>ดาบเทวาพิทักษ์ เรียบร้อยแล้ว <br/>ไอเทมส่งเข้าไปในกล่องจดหมาย<br/>ในวันที่ 15 มกราคม 2563'})}>ตกลง</div> */}
					<div className="btn" onClick={()=>{if(props.actConfirm) props.actConfirm()}}>ตกลง</div>
					<div className="btn" onClick={() => props.setValues({ modal_open: '' })}>ยกเลิก</div>
				</div>
			</div>
    </ModalCore>
	)
}

const mapStateToProps = state => ({
  ...state.ModalReducer,
  ...state.Modal
});

const mapDispatchToProps = { setValues, actCloseModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmModal);
