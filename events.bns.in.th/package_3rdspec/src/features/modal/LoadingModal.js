import React from 'react';
import  ModalCore from './ModalCore.js';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';

const LoadingModal = (props) => {
	return (
		<ModalCore name="loading" outSideClick={false}>
			<div className="page">
                 <div className="page__loading">
                    กำลังดำเนินการ...
                </div> 
            </div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoadingModal);
