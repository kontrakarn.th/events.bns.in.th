import React, { Component } from 'react';
import {connect} from 'react-redux';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/advice.css';

import $ from "jquery";

import Inputmask from "inputmask";

import {apiPost} from './../middlewares/Api';

// import ModalConfirmToRedeem from '../components/ModalConfirmToRedeem';

import ModalSelectCharacter from '../components/ModalSelectCharacter';
import ModalConfirmSelectCharacter from '../components/ModalConfirmSelectCharacter';
import ModalConfirmRedeemCode from '../components/ModalConfirmRedeemCode';
import ModalConfirmRandomAdviceGachapon from '../components/ModalConfirmRandomAdviceGachapon';
import ModalConfirmRandomFreeGachapon from '../components/ModalConfirmRandomFreeGachapon';
import ModalConfirmExchangeReward from '../components/ModalConfirmExchangeReward';
import ModalMessage from '../components/ModalMessage';
// import ModalConfirmPurchase from '../components/ModalConfirmPurchase';
// import ModalConfirmGacha from '../components/ModalConfirmGacha';
// import ModalReceiveGacha from '../components/ModalReceiveGacha';
// import ModalConfirmExchange from '../components/ModalConfirmExchange';
// import ModalReceiveExchange from '../components/ModalReceiveExchange';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import iconHome from './../static/images/buttons/btn_home.png';
import bg_home1 from './../static/images/bg_home1.png';
import bg_home2 from './../static/images/bg_home2.png';
import bg_home3 from './../static/images/bg_home3.png';
import bg_home4 from './../static/images/bg_home4.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
import titleCondition from './../static/images/title_condition.png';
import titleCondition2 from './../static/images/title_condition2.png';
import titleOpenboxfree from './../static/images/title_openboxfree.png'
import titleFreegachapon from './../static/images/title_freegachapon.png';
import titleStatus from './../static/images/title_status.png';
import titleGachapon from './../static/images/title_gachapon.png';
import titleMoneychangeitem from './../static/images/title_moneychangeitem.png';
import btn_codeitem from './../static/images/buttons/btn_codeitem.png';
import item1 from './../static/images/items/item1.png';
import item2 from './../static/images/items/item2.png';
import item3 from './../static/images/items/item3.png';
import item4 from './../static/images/items/item4.png';
import item5 from './../static/images/items/item5.png';
import item6 from './../static/images/items/item6.png';
import item7 from './../static/images/items/item7.png';
import item8 from './../static/images/items/item8.png';
import item9 from './../static/images/items/item9.png';
import item10 from './../static/images/items/item10.png';
import item11 from './../static/images/items/item11.png';
import item12 from './../static/images/items/item12.png';
import item13 from './../static/images/items/item13.png';
import btn_randomGift from './../static/images/buttons/btn_randomGift.png';
import item_changeMoney1 from './../static/images/changeMoney/item1.png';
import item_changeMoney2 from './../static/images/changeMoney/item2.png';
import item_changeMoney3 from './../static/images/changeMoney/item3.png';
import item_changeMoney4 from './../static/images/changeMoney/item4.png';
import item_changeMoney5 from './../static/images/changeMoney/item5.png';
import item_changeMoney6 from './../static/images/changeMoney/item6.png';
import item_changeMoney7 from './../static/images/changeMoney/item7.png';
import item_changeMoney8 from './../static/images/changeMoney/item8.png';
import item_changeMoney9 from './../static/images/changeMoney/item9.png';
import item_changeMoney10 from './../static/images/changeMoney/item10.png';
import item_changeMoney11 from './../static/images/changeMoney/item11.png';
import item_changeMoney12 from './../static/images/changeMoney/item12.png';
import item_changeMoney13 from './../static/images/changeMoney/item13.png';
import item_changeMoney14 from './../static/images/changeMoney/item14.png';
import item_changeMoney15 from './../static/images/changeMoney/item15.png';

import {
    onAccountLogout
} from '../actions/AccountActions';

const itemsbox = [
    {
        name: "ชุดศิลปแจกันโบราณ",
        img: item1
    },
    {
        name: "เหรียญ Advice",
        img: item2
    },
    {
        name: "สัญลักษณ์ฮงมุน",
        img: item3
    },
    {
        name: "หมวกศิลปพัดโบราณ",
        img: item4
    },
    {
        name: "หินเปลี่ยนรูปขั้นสูง",
        img: item5
    },
    {
        name: "ยันต์ผนึกบุปผาน้ำแข็งที่ส่องสว่าง",
        img: item6
    },
    {
        name: "กล่องโซลชิลด์พายุดำ หมายเลข1",
        img: item7
    },
    {
        name: "กล่องโซลชิลด์พายุดำ หมายเลข2",
        img: item7
    },
    {
        name: "กล่องโซลชิลด์พายุดำ หมายเลข3",
        img: item7
    },
    {
        name: "กล่องโซลชิลด์พายุดำ หมายเลข4",
        img: item7
    },
    {
        name: "กล่องโซลชิลด์พายุดำ หมายเลข5",
        img: item7
    },
    {
        name: "กล่องโซลชิลด์พายุดำ หมายเลข6",
        img: item7
    },
    {
        name: "กล่องโซลชิลด์พายุดำ หมายเลข7",
        img: item7
    },
    {
        name: "กล่องโซลชิลด์พายุดำ หมายเลข8",
        img: item7
    },
    {
        name: "เกล็ดมังกรไฟ",
        img: item8
    },
    {
        name: "หินจันทรา",
        img: item9
    },
    {
        name: "ยันต์ท้าทายรวดเร็ววิถีฮงมุน",
        img: item10
    },
    {
        name: "หินโซล",
        img: item11
    },
    {
        name: "ชิ้นส่วน สัญลักษณ์กลุ่ม โจรสลัดแมว",
        img: item12
    },
    {
        name: "ชิ้นส่วนฉลามดำ",
        img: item13
    }
]

class HomeContainer extends Component {

    genClassGacha(baseClass,index,gachaSelected,infinite=false){
        if(gachaSelected < 0) {
            return baseClass;
        }
        if(infinite){
            return baseClass + " " + baseClass + "--rnd"+index+"infinite";
        }
        if(gachaSelected === index) {
            return baseClass + " " + baseClass + "--selected"
        } else {
            return baseClass + " " + baseClass + "--rnd"+index
        }

    }

    resetGacha(){
        $('.gachapon__boxitem').removeClass('gachapon__boxitem--selected');
    }
   
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiEventInfo(){
        this.setState({ showLoading: true});
        let self = this;
        let dataSend={ type : "event_info" };
        let successCallback = (data)=>{
            // console.log("data",data);
            this.setState({
                permission: true,
                showLoading: false,
                ...data.content
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            }else if(data.type === "no_char"){
                this.apiGetCharacter();
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                })
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO",this.props.jwtToken,dataSend,successCallback,failCallback);
    }

    apiGetCharacter(){
        this.setState({ showLoading: true});
        let self = this;
        let dataSend={ type : "get_character" };
        let successCallback = (data)=>{
            this.setState({
               permission: true,
               showLoading: false,
               listCharater: data.content,
               showModalSelectCharacter: true
           })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }
        };
        apiPost("REACT_APP_API_POST_GET_CHARACTER",this.props.jwtToken,dataSend,successCallback,failCallback);
    }
    apiAcceptCharacter(character_id){
        this.setState({ showLoading: true, showModalConfirmCharacter: false,});
        let self = this;
        let dataSend={
            type : "accept_character",
            id: character_id,
        };
        let successCallback = (data)=>{
            console.log("data",data);
            this.setState({
               permission: true,
               showLoading: false,
               showModalMessage: data.message,
               ...data.content
           })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            } else {
                this.setState({
                    showModalMessage: data.message,
                    showModalSelectCharacter: true,
                    showLoading: false,
                });
            }
        };
        apiPost("REACT_APP_API_POST_ACCEPT_CHARACTER",this.props.jwtToken,dataSend,successCallback,failCallback);
    }

    apiRedeemCode(codeVal=''){
        this.setState({ showLoading: true, showModalConfirmRedeemCode: false});
        let self = this;
        let dataSend={ type : "redeem_advice_code", code: codeVal };
        let successCallback = (data)=>{
            // console.log("data",data);
            this.setState({
                permission: true,
                showLoading: false,
                showModalMessage: data.message,
                codeVal: "",
                ...data.content
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    showLoading: false,
                    permission: false,
                    codeVal: "",
                });
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                    codeVal: "",
                })
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM_CODE",this.props.jwtToken,dataSend,successCallback,failCallback);
    }

    apiRandomAdviceGachapon(){
        this.setState({ showLoading: true, showModalConfirmRandomAdviceGachapon: false, rnd_gacha: true});
        let self = this;
        let dataSend={ type : "advice_gachapon" };
        let successCallback = (data)=>{
            // console.log("data",data);
            this.setState({
                permission: true,
                showLoading: false,
                // showModalMessage: data.message,
                gacha: data.id-1,
                rnd_gacha: false,
                ...data.content
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                })
            }
        };
        apiPost("REACT_APP_API_POST_ADVICE_GACHAPON",this.props.jwtToken,dataSend,successCallback,failCallback);
    }

    apiRandomFreeGachapon(){
        this.setState({ showLoading: true, showModalConfirmRandomFreeGachapon: false, free_rnd_gacha: true});
        let self = this;
        let dataSend={ type : "free_gachapon" };
        let successCallback = (data)=>{
            // console.log("data",data);
            this.setState({
                permission: true,
                showLoading: false,
                // showModalMessage: data.message,
                free_gacha: data.id-1,
                free_rnd_gacha: false,
                ...data.content
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                })
            }
        };
        apiPost("REACT_APP_API_POST_FREE_GACHAPON",this.props.jwtToken,dataSend,successCallback,failCallback);
    }

    apiExchangeReward(packageId=''){
        this.setState({ showLoading: true, showModalConfirmExchangeReward: false});
        let self = this;
        let dataSend={ type : "exchange_reward", package_id: packageId };
        let successCallback = (data)=>{
            // console.log("data",data);
            this.setState({
                permission: true,
                showLoading: false,
                showModalMessage: data.message,
                codeVal: "",
                ...data.content
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    showLoading: false,
                    permission: false,
                    codeVal: "",
                });
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                    codeVal: "",
                })
            }
        };
        apiPost("REACT_APP_API_POST_EXCHANGE",this.props.jwtToken,dataSend,successCallback,failCallback);
    }
    
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.inputRef = React.createRef();

        this.state = {
            permission: true,
            showLoading: true,

            gacha: -1,
            rnd_gacha: false,

            free_gacha: -1,
            free_rnd_gacha: false,

            showModalSelectCharacter: false,
            showModalConfirmCharacter: false,

            showModalConfirmRedeemCode: false,
            codeVal: "",

            showModalConfirmRandomAdviceGachapon: false,

            showModalConfirmRandomFreeGachapon: false,

            showModalConfirmExchangeReward: false,
            
            packageId: 0,
            packageName: "",

            name: "",
            username: "",
            character: "",
            all_remain_code: 0,
            can_redeem_advice_gachapon: false,
            can_random_advice_gachapon: false,
            remain_points: 0,
            total_advice_gachapon: 0,
            remain_advice_gachapon: 0,
            used_advice_gachapon: 0,
            total_free_gachapon: 0,
            remain_free_gachapon: 0,
            used_free_gachapon: 0,
            can_random_free_gachapon: false,
            quests: [],

            num_statusPlayer: 1,
            numMax_statusPlayer: 3,
            num_staus: 4980,
            gachaponList: [
                {
                    active: true,
                    itemsBox: [
                        {
                            imgIndex: 0,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 10,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 3,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 10,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 4,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 10,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 5,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 10,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 13,
                            num: 1,
                            align: "left",
                        },
                        {
                            imgIndex: 1,
                            num: 10,
                            align: "left",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "left",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 12,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 10,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 11,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 10,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 10,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 10,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 14,
                            num: 5,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 10,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 15,
                            num: 10,
                            align: "left",
                        },
                        {
                            imgIndex: 1,
                            num: 10,
                            align: "left",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "left",
                        }
                    ]
                }
            ],
            questsList: [
                {
                    name: "เกาะแห่งพันธนาการ",
                    value: true
                },
                {
                    name: "รังมังกรเพลิง",
                    value: false
                },
                {
                    name: "โรงหลอมอัคคี",
                    value: false
                },
                {
                    name: "โรงหลอมเครื่องจักร",
                    value: true
                },
                {
                    name: "หุบเขาอาถรรพ์",
                    value: true
                }
            ],
            freeGachaponList: [
                {
                    active: true,
                    itemsBox: [
                        {
                            imgIndex: 15,
                            num: 5,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 16,
                            num: 2,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 17,
                            num: 10,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 18,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 9,
                            num: 1,
                            align: "left",
                        },
                        {
                            imgIndex: 1,
                            num: 1,
                            align: "left",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "left",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 8,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 7,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 6,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 14,
                            num: 2,
                            align: "right",
                        },
                        {
                            imgIndex: 1,
                            num: 1,
                            align: "right",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "right",
                        }
                    ]
                },
                {
                    active: false,
                    itemsBox: [
                        {
                            imgIndex: 19,
                            num: 1,
                            align: "left",
                        },
                        {
                            imgIndex: 1,
                            num: 1,
                            align: "left",
                        },
                        {
                            imgIndex: 2,
                            num: 5,
                            align: "left",
                        }
                    ]
                }
            ],
            exchangeItemsList: [
                {
                    id: 1,
                    name: "หินเปลี่ยนรูปชั้นสูง",
                    img: item_changeMoney1,
                    numAdvice: 50
                },
                {
                    id: 2,
                    name: "ชุดศิลปแจกันโบราณ",
                    img: item_changeMoney2,
                    numAdvice: 50
                },
                {
                    id: 3,
                    name: "ถุงเพชรแปดเหลี่ยม",
                    img: item_changeMoney3,
                    numAdvice: 50
                },
                {
                    id: 4,
                    name: "อาวุธลวงตาโพไซดอน",
                    img: item_changeMoney4,
                    numAdvice: 50
                },
                {
                    id: 5,
                    name: "หมวกศิลปพัดโบราณ",
                    img: item_changeMoney5,
                    numAdvice: 30
                },
                {
                    id: 6,
                    name: "วิญญาณวายุทมิฬ",
                    img: item_changeMoney6,
                    numAdvice: 25
                },
                {
                    id: 7,
                    name: "กล่องอัญมณีหกเหลี่ยมของฮงมุนพิเศษ",
                    img: item_changeMoney7,
                    numAdvice: 10
                },
                {
                    id: 8,
                    name: "หินจันทรา (10)",
                    img: item_changeMoney8,
                    numAdvice: 5
                },
                {
                    id: 9,
                    name: "ประกายฉลามดำ (5)",
                    img: item_changeMoney9,
                    numAdvice: 5
                },
                {
                    id: 10,
                    name: "สัญลักษณ์กลุ่มโจรสลัดแมว",
                    img: item_changeMoney10,
                    numAdvice: 5
                },
                {
                    id: 11,
                    name: "สัญลักษณ์ฮงมุน",
                    img: item_changeMoney11,
                    numAdvice: 5
                },
                {
                    id: 12,
                    name: "ยันต์ท้าทายรวดเร็ววิถีฮงมุน",
                    img: item_changeMoney12,
                    numAdvice: 3
                },
                {
                    id: 13,
                    name: "ยาโชคชะตา",
                    img: item_changeMoney13,
                    numAdvice: 1
                },
                {
                    id: 14,
                    name: "พลุขนาดเล็ก",
                    img: item_changeMoney14,
                    numAdvice: 1
                },
                {
                    id: 15,
                    name: "ยันต์มิตรภาพ",
                    img: item_changeMoney15,
                    numAdvice: 1
                }
            ],
            numAdvicePlayer: 10,

            showModalMessage: "",
            ModalConfirmPurchase: false,

            scollDown: false,

            status:'no_discount',

        }
    }
    componentDidMount(){
        if(this.props.jwtToken !== "") setTimeout(()=>this.apiEventInfo(),600);

        if(this.state.can_redeem_advice_gachapon == true){
            setTimeout(()=>{
                Inputmask({mask: '****-****-****-****', casing:"upper"}).mask(this.inputRef);
            }, 800)
        }
        
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.jwtToken !== prevProps.jwtToken){
            setTimeout(()=>{
                this.apiEventInfo()
            },600);
        }
        if(this.state.can_redeem_advice_gachapon !== prevState.can_redeem_advice_gachapon && this.state.can_redeem_advice_gachapon == true){
            Inputmask({mask: '****-****-****-****', casing:"upper"}).mask(this.inputRef);
        }
    }


    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }

    actPurchase(){
        this.setState({
            ModalConfirmPurchase: true,
        })
    }

    actConfirmPurchase(){
        this.apiPurchasePackage();
    }

    commaNumber(n){
        return (n).toLocaleString('en')
    }

    actOnChangeCode(e){
        this.setState({
            codeVal: e.currentTarget.value,
        })
    }

    actSelectCharacter(character_id,character_name){
        this.apiAcceptCharacter(character_id)
    }

    actConfirmRedeemCode(){
        if(this.state.codeVal != ""){
            this.setState({
                showModalConfirmRedeemCode: true,
            });
        }
    }

    actConfirmRandomAdviceGachapon(){
        this.setState({
            showModalConfirmRandomAdviceGachapon: true,
            gacha: -1,
            rnd_gacha: false,
        });
    }

    actConfirmRandomFreeGachapon(){
        this.setState({
            showModalConfirmRandomFreeGachapon: true,
            free_gacha: -1,
            free_rnd_gacha: false,
        });
    }

    actConfirmExchangeReward(event,canExchange=false,packageId=0,packageName=''){
        // console.log(packageId);
        if(canExchange==true){
            this.setState({
                showModalConfirmExchangeReward: true,
                packageName: packageName,
                packageId: packageId,
            });
        }
        
    }

    render() {
        return (
            <div >
                <div className="f11layout">
                    <div className="f11layout__name">{this.state.username} : {this.state.character}</div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                    <div className={"f11layout__scroll"+(this.state.scollDown? " hide":"")} ><img src={iconScroll} alt="" /></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="advice">
                                <section className="advice__section advice__section--header">
                                    <div className="header">
                                        <div className="header__inner">
                                            {/* <div className="header__tag header__tag--date"></div>
                                            <div className="header__tag header__tag--promotion"></div> */}
                                        </div>
                                    </div>
                                </section>
                                <section className="advice__section advice__section--condition">
                                    <div className="condition">
                                        <div className="condition__inner">
                                            <div className="condition__head"><img src={titleCondition} alt="" /></div>
                                            <ul className="condition__text">
                                                <li>เมื่อซื้อสินค้าในร้านค้า Advice ที่ร่วมรายการ <span className="condition__black">(จุดสังเกตุ มี Standee ของอาชีพวอร์เดนยืนอยู่หน้าร้าน)</span><br /> ครบ 500 บาท จะได้รับไอเทมโค้ด <span className="condition__black">(1 ใบเสร็จรับไอเทมโค้ดสูงสุด 5 ไอเทมโค้ด)</span></li>
                                                <li>นำไอเทมโค้ดมาเติมในกิจกรรม <span className="condition__yellow">BNS x Advice</span> จะได้รับสิทธิ์ในการเปิดกล่อง  <span className="condition__yellow">Advice Gachapon</span> <span className="condition__black">(จำกัด 5 ครั้ง ต่อ 1 UID )</span></li>
                                                <li>ระยะเวลาที่จะได้รับไอเทมโค้ด 16 มกราคม (12:00 น.) - 15 กุมภาพันธ์ 2562 (23:59 น.)</li>
                                                <li>ระยะเวลาที่ในการใช้ไอเทมโค้ด 16 มกราคม (12:00 น.) - 15 มีนาคม 2562 (23:59 น.)</li>
                                                <li>จำกัดสิทธิ์ 5,000 สิทธิ์แรกที่เปิดกล่อง  <span className="condition__yellow">Advice Gachapon</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                                <section className="advice__section">
                                    <img className="advice__bg advice__bg--1" src={bg_home1} />
                                    <div className="status">
                                        <div className="status__inner">
                                            <div className="status__head"><img src={titleStatus} alt="" /></div>
                                            <div className="status__box">
                                                <div className="status__inline">
                                                    <div className="status__title">เติมไอเทมโค้ด</div>
                                                    <div className="status__contentplayer">
                                                        {
                                                            this.state.can_redeem_advice_gachapon == true ?
                                                                <div>
                                                                    <input
                                                                        className="status__itemmoneycode"
                                                                        ref={this.inputRef}
                                                                        onChange={(event)=>this.actOnChangeCode(event)}
                                                                        value={this.state.codeVal}
                                                                    />
                                                                    <a className="status__btnitemcode" onClick={this.actConfirmRedeemCode.bind(this)}><img src={btn_codeitem} /></a>
                                                                </div>
                                                            :
                                                                <h3>สิทธิ์การเติมไอเทมโค้ดเต็ม</h3>
                                                        }
                                                        
                                                    </div>
                                                </div>
                                                <div className="status__inline">
                                                    <div className="status__title font18">สิทธิ์ที่ใช้ไปแล้ว / สิทธิ์ทั้งหมด</div>
                                                    <div className="status__contentplayer">
                                                        <div>
                                                            {this.state.used_advice_gachapon} / {this.state.total_advice_gachapon}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="status__box">
                                                <div className="status__title">จำนวนสิทธิ์ที่เหลือทั้งหมด</div>
                                                <div>{this.commaNumber(this.state.all_remain_code)}</div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <section className="advice__section">
                                    <div className="gachapon">
                                        <div className="gachapon__inner">
                                            <div className="gachapon__head"><img src={titleGachapon} alt="" /></div>
                                            {
                                                this.state.gachaponList.map((gachapon, indexGachapon) => {
                                                    return(
                                                        // <div key={indexGachapon} className={"gachapon__boxitem" + (gachapon.active ? " active" : "")}>
                                                        <div key={indexGachapon} className={this.genClassGacha("gachapon__boxitem",indexGachapon,this.state.gacha,this.state.rnd_gacha)}>
                                                            <div className="gachapon__table">
                                                                <div className="gachapon__tablecell">
                                                                    {
                                                                        gachapon.itemsBox.map((item, indexItem) => {
                                                                            return (
                                                                                <div key={indexItem} className="gachapon__item">
                                                                                    <img src={itemsbox[item.imgIndex].img} />
                                                                                    <div className={"gachapon__nameitem "+(item.align=="left" ? "item-left" : "")}>{itemsbox[item.imgIndex].name + " (" + item.num + ")"}</div>
                                                                                </div>
                                                                            )
                                                                        })
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            }
                                            {
                                                this.state.can_random_advice_gachapon == true ?
                                                    <a className="gachapon__btnrandomgift" onClick={this.actConfirmRandomAdviceGachapon.bind(this)}><img src={btn_randomGift} /></a>
                                                :
                                                    <a className="gachapon__btnrandomgift diabled"><img src={btn_randomGift} /></a>
                                            }
                                            
                                        </div>
                                    </div>
                                </section>
                                <section className="advice__section advice__section--condition">
                                    <img className="advice__bg advice__bg--2" src={bg_home2} />
                                    <div className="condition">
                                        <div className="condition__inner">
                                            <div className="condition__head"><img src={titleCondition2} alt="" /></div>
                                            <ul className="condition__text condition__text--2">
                                                <li>ระยะเวลาของกิจกรรม 16 มกราคม (12:00 น.) - 15 กุมภาพันธ์ 2562 (23:59 น.)</li>
                                                <li>ผู้เล่นสำเร็จเควสท์ตามตารางที่กำหนด จะได้รับสิทธิ์ในการเปิดกล่อง Free Advice Gachapon <div className="condition__black">(สำเร็จ 1 เควสท์ได้รับ 1 สิทธิ์ สูงสุด 5 สิทธิ์)</div></li>
                                                <li>ระยะเวลาในการอัพเดทสิทธิ์ 15 - 30 นาที</li>
                                                <li>เควสจะรีเซ็ททุกวันเวลา 06:00 น.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                                <section className="advice__section">
                                    <img className="advice__bg advice__bg--3" src={bg_home3} />
                                    <div className="openboxfree">
                                        <div className="openboxfree__inner">
                                            <div className="openboxfree__head"><img src={titleOpenboxfree} alt="" /></div>
                                            <div>
                                                <div className="openboxfree__inline">
                                                    <div className="openboxfree__title">เควสท์ประจำวันของดันเจี้ยน</div>
                                                    <div className="openboxfree__contentplayer">
                                                        <div>
                                                            {
                                                                this.state.quests.length > 0 ?
                                                                    this.state.questsList.map((quest, indexQuest) => {
                                                                        return (
                                                                            <div key={indexQuest} className="openboxfree__quest">
                                                                                <div className={"openboxfree__questarrow" + (this.state.quests[indexQuest].status ? " active" : "")}></div> 
                                                                                <div>{quest.name}</div>
                                                                            </div>
                                                                        )
                                                                    })
                                                                :
                                                                    null
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="openboxfree__inline">
                                                    <div className="openboxfree__title font18">สิทธิ์ที่ใช้ไปแล้ว / สิทธิ์ทั้งหมด</div>
                                                    <div className="openboxfree__contentplayer">
                                                        <div>
                                                            {this.state.used_free_gachapon} / {this.state.total_free_gachapon}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <section className="advice__section">
                                    <div className="gachapon">
                                        <div className="gachapon__inner">
                                            <div className="gachapon__head"><img src={titleFreegachapon} alt="" /></div>
                                            {
                                                this.state.freeGachaponList.map((gachapon, indexGachapon) => {
                                                    return (
                                                        <div key={indexGachapon} className={this.genClassGacha("gachapon__boxitem",indexGachapon,this.state.free_gacha,this.state.free_rnd_gacha)}>
                                                            <div className="gachapon__table">
                                                                <div className="gachapon__tablecell">
                                                                    {
                                                                        gachapon.itemsBox.map((item, indexItem) => {
                                                                            return (
                                                                                <div key={indexItem} className="gachapon__item">
                                                                                    <img src={itemsbox[item.imgIndex].img} />
                                                                                    <div className={"gachapon__nameitem "+(item.align=="left" ? "item-left" : "")}>{itemsbox[item.imgIndex].name + " (" + item.num + ")"}</div>
                                                                                </div>
                                                                            )
                                                                        })
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            }
                                            {
                                                this.state.can_random_free_gachapon == true ?
                                                    <a className="gachapon__btnrandomgift" onClick={this.actConfirmRandomFreeGachapon.bind(this)}><img src={btn_randomGift} /></a>
                                                :
                                                    <a className="gachapon__btnrandomgift diabled"><img src={btn_randomGift} /></a>
                                            }
                                        </div>
                                    </div>
                                </section>
                                <section className="advice__section">
                                    <div className="changemoney">
                                        <div className="changemoney__inner">
                                            <div className="changemoney__head"><img src={titleMoneychangeitem} /></div>
                                            <div className="changemoney__content">
                                                <div className="changemoney__inline">
                                                    {
                                                        this.state.exchangeItemsList.map((itemChange, indexItemChange) => {
                                                            let can_exchange = (this.state.remain_points >= itemChange.numAdvice) ? true : false
                                                            return (
                                                                <div key={indexItemChange} className={"changemoney__boxcard" + (this.state.remain_points < itemChange.numAdvice ? " changemoney__boxcard--gray" : "")} onClick={(event,canExchange,packageId,packageName)=>this.actConfirmExchangeReward(event,can_exchange,itemChange.id,itemChange.name)}>
                                                                    <div className="changemoney__headcard">
                                                                        <div className="changemoney__numadvice">{itemChange.numAdvice}</div>
                                                                    </div>
                                                                    <div className="changemoney__contentcard">
                                                                        <div>
                                                                            <img src={itemChange.img} />
                                                                            <div className="changemoney__nameitemcard">{itemChange.name}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            )
                                                        })
                                                    }
                                                </div>
                                                <div className="changemoney__inline changemoney__inline--boxnumactive">
                                                    <div className="changemoney__headnumadvice">จำนวนเหรียญ<br />Advice<br />ของคุณ</div>
                                                    <div>{this.state.remain_points}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <img className="advice__bg advice__bg--4" src={bg_home4} />
                            </div>
                        </ScrollArea>
                    </div>
                    :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />
                <ModalSelectCharacter
                    open={this.state.showModalSelectCharacter}
                    listCharater={this.state.listCharater|| []}
                    actSelectCharacter={(character_id,character_name)=>this.setState({
                        showModalSelectCharacter: false,
                        showModalConfirmCharacter: true,
                        character_id: character_id,
                        character_name: character_name
                    })}
                />
                <ModalConfirmSelectCharacter
                    open={this.state.showModalConfirmCharacter}
                    actConfirm={()=>this.actSelectCharacter(this.state.character_id)}
                    actClose={()=>this.setState({
                        showModalSelectCharacter: true,
                        showModalConfirmCharacter: false
                    })}
                    title={"ยืนยัน"}
                    msg={'เลือก <span>"'+this.state.character_name +'"</span> ?'}
                />
                <ModalConfirmRedeemCode
                    open={this.state.showModalConfirmRedeemCode}
                    actConfirm={()=>this.apiRedeemCode(this.state.codeVal)}
                    actClose={()=>this.setState({
                        showModalConfirmRedeemCode: false
                    })}
                    title={"ยืนยัน"}
                    msg={'เติมโค้ด<br /><span>"'+ this.state.codeVal +'"</span> ?'}
                />
                <ModalConfirmRandomAdviceGachapon
                    open={this.state.showModalConfirmRandomAdviceGachapon}
                    actConfirm={()=>this.apiRandomAdviceGachapon()}
                    actClose={()=>this.setState({
                        showModalConfirmRandomAdviceGachapon: false
                    })}
                    title={"ยืนยัน"}
                    msg={'สุ่มของรางวัลจาก<br />Advice Gachapon?'}
                />
                <ModalConfirmRandomFreeGachapon
                    open={this.state.showModalConfirmRandomFreeGachapon}
                    actConfirm={()=>this.apiRandomFreeGachapon()}
                    actClose={()=>this.setState({
                        showModalConfirmRandomFreeGachapon: false
                    })}
                    title={"ยืนยัน"}
                    msg={'สุ่มของรางวัลจาก<br />Free Advice Gachapon?'}
                />
                <ModalConfirmExchangeReward
                    open={this.state.showModalConfirmExchangeReward}
                    actConfirm={()=>this.apiExchangeReward(this.state.packageId)}
                    actClose={()=>this.setState({
                        showModalConfirmExchangeReward: false
                    })}
                    title={"ยืนยัน"}
                    msg={'แลก "' + this.state.packageName + '" ?'}
                />
                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
                
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
    onAccountLogout: () => {
        dispatch(onAccountLogout());
    }
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
