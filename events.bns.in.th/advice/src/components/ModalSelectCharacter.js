import React from 'react';
import Modal from './Modal';
import iconDropdown from './../static/images/icons/icon_dropdown.png'

export default class ModalSelectCharacter extends React.Component {

    actSelectCharacter(){
        let indexCharacter = document.getElementById("select_charater_form").value;
        let idCharacter = this.props.listCharater[indexCharacter].id;
        let nameCharacter = this.props.listCharater[indexCharacter].char_name;
        this.props.actSelectCharacter(idCharacter,nameCharacter);
    }
    render() {
        return (
            <Modal
                open={this.props.open}
                title="เลือกตัวละคร"
                confirmMode={false}
                actClose={()=>this.actSelectCharacter()}
            >
                <form className="formselect">
                    <label >
                        <select id="select_charater_form" className="fromselect__list">
                            {this.props.open && this.props.listCharater && this.props.listCharater.map((item,index)=>{
                                return (
                                    <option key={index} value={index}>{item.char_name}</option>
                                )
                            })}
                        </select>
                        <img src={iconDropdown} alt="" />
                    </label>
                </form>
            </Modal>
        )
    }
}
