import React from 'react';
import Modal from './Modal';

export default class ModalConfirmPurchase extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                title="ยืนยัน"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                ยืนยันซื้อแพ็คเกจ?
            </Modal>
        )
  }
}
