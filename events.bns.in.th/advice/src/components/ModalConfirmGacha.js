import React from 'react';
import Modal from './Modal';

export default class ModalConfirmGacha extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                title="ยืนยัน"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                ต้องการสุ่มกาชา?
            </Modal>
        )
  }
}
