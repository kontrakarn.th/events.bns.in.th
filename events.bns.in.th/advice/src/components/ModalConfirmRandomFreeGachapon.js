import React from 'react';
import Modal from './Modal';

export default class ModalConfirmRandomFreeGachapon extends React.Component {
    render() {
        let title = this.props.data && this.props.data || [];
        return (
            <Modal
                open={this.props.open}
                title={this.props.title}
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div dangerouslySetInnerHTML={{__html: this.props.msg}} />
            </Modal>
        )
  }
}
