import React from 'react';
import  ModalCore from './ModalCore.js';
import './styles.scss';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';

const RewardModal = (props) => {
	return (
		<ModalCore name="reward" outSideClick={true}>
			<div className="contentwrap contentwrap--reward">
				{/* <div className="btn__ok btn__ok--reward" onClick={() => props.setValues({ modal_open: '' })}></div> */}
			</div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RewardModal);
