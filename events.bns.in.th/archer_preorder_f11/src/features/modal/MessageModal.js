import React from 'react';
import  ModalCore from './ModalCore.js';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';

const MessageModal = (props) => {
	return (
		<ModalCore name="message" outSideClick={false}>
			<div className="contentwrap contentwrap--board">
				<div className="inner inner--boardmaxwidth">
					<div className="text text--text" dangerouslySetInnerHTML={{ __html: props.modal_message }}>
					</div>
					<div className="btn__ok" onClick={() => props.setValues({ modal_open: '' })}></div>
				</div>
			</div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageModal);
