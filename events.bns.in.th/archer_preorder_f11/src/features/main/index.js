import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';

import { apiPost } from './../../middlewares/Api';

import { onAccountLogout } from './../../actions/AccountActions';

import { setValues } from "./redux";

import { ConfirmModal, MessageModal, LoadingModal, RewardModal, JobModal, PurchasedInfoModal, MessageNoCharModal } from "../../features/modal";
import iconScroll from './../../static/images/icons/icon_scroll.png';
import btn_viewreward from './../../static/images/btn_viewreward.png';
import btn_login from './../../static/images/btn_login.png';
import btn_logout from './../../static/images/btn_logout.png';
import text_buy from './../../static/images/text_buy.png';
import text_buy_mb from './../../static/images/text_buy_mb.png';
import register_text from './../../static/images/register_text.png';

class Main extends Component {
    constructor(props) {
        super(props)
        this.state = {
            registered: false
        }
    }

    ////////////   API   ////////////

    apiEventInfo(){
        this.props.setValues({
            modal_open: "loading",
        });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content,
                    modal_open: "",
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_message: data.message,
                    modal_open: "message_nochar",
                });
                
            }else if(data.type=='no_game_info'){
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message_nochar",
                });
                
            }else {
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiAcceptPreOrder(packageId,packageType,weaponId){
        this.props.setValues({
            modal_open: "loading",
        });
        let dataSend = {type:'purchase_package', package_id: packageId, package_type: packageType, weapon_id: weaponId };
        let successCallback = (data) => {
            if (data.status) {
                // console.log('data.content:',data.content);
                this.props.setValues({
                    ...data.content,
                    modal_message: data.message,
                    modal_open: "message",
                });
            }else{
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_message: data.message,
                    modal_open: "message_nochar",
                });
            }else {
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                });
            }
        };
        apiPost("REACT_APP_API_POST_PURCHASE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    ////////////   API   ////////////

    componentDidMount() {
        
        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 800);

    }

    componentDidUpdate(prevProps, prevState) {
        
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo()
            }, 800);
        }
    }

    handleLoading() {
        this.props.setValues({
            modal_open: "loading",
        });
    }
    handleReward() {
        this.props.setValues({
            modal_open: "reward",
        });
    }
    handleConfirm(packageId,packageType) {
        let _price = '';

        if(packageId=== 1){

            if(packageType == 1){
                _price = '<br />250,000 ไดมอนด์';
            }else if(packageType == 2){
                _price = '<br />125,000 ไดมอนด์';
            }

            this.props.setValues({
                package_id: packageId,
                package_type: packageType,
                modal_open: "confirm",
                modal_message: "แพ็คเกจสนับสนุนน้องใหม่ ขั้นเทพ"+_price
            });
        }
        else{

            if(packageType == 1){
                _price = '<br />60,000 ไดมอนด์';
            }else if(packageType == 2){
                _price = '<br />30,000 ไดมอนด์';
            }
            
            this.props.setValues({
                package_id: packageId,
                package_type: packageType,
                modal_open: "confirm",
                modal_message: "แพ็คเกจสนับสนุนน้องใหม่"+_price
            });
        }
    }
    handleBuy(){
        this.props.setValues({
            modal_open: "job",
            weapon_id: -1,
        });
    }
    handleSelect(){
        if(this.props.weapon_id != -1){
            // console.log('weapon : ',this.props.weapon_id);
            // console.log('package_id : ',this.props.package_id);
            // console.log('package_type : ',this.props.package_type);

            this.apiAcceptPreOrder(this.props.package_id,this.props.package_type,this.props.weapon_id);
        }
    }

    handleViewPurchased(){
        this.props.setValues({modal_open: 'purchase_info'});
    }

    changeSelectedWeapon(e){
        this.props.setValues({weapon_id: e.target.value});
    }

    onLogout() {
        this.props.onAccountLogout();
    }

    render() {
        return (
            <div className="events-bns">
                <div className="archer">
                    <div className="archer__scroll">
                        <img src={iconScroll} alt=""/>
                    </div>
                    <div className="archer__board-reward">
                        <div className="archer__view">
                            <img src={btn_viewreward} alt='' onClick={this.handleReward.bind(this)}/>
                        </div>
                    </div>
                    <div className="archer__board-reward2"/>
                    <div className="archer__board-buy">
                    {
                            this.props.jwtToken && this.props.jwtToken !== "" && this.props.username && <div className="userinfo">
                                Username: {this.props.username} &nbsp;&nbsp;&nbsp; ไดมอนด์: {this.props.diamonds}
                            </div>
                        }
                        {
                            this.props.jwtToken && this.props.jwtToken !== "" ?
                                (
                                    this.props.has_discount ?
                                        <div className="archer__registered">
                                            <picture>
                                                <source srcset={text_buy_mb} media="(max-width: 640px)"/>
                                                <source srcset={text_buy}/>
                                                <img srcset={text_buy} alt=''/>
                                            </picture>
                                            {
                                                this.props.already_purchase && this.props.purchased_package_id == 1 ?
                                                    <div className="archer__btn-description archer__btn-description--left" onClick={()=>{this.handleViewPurchased()}}/>
                                                :
                                                    <div className={"archer__btn-preorder archer__btn-preorder--left "+( this.props.can_purchase && this.props.can_purchase.package_1_discount == true ? "" : "disable")} onClick={()=>{this.handleConfirm(1,2)}}/>
                                            }

                                            {
                                                this.props.already_purchase && this.props.purchased_package_id == 2 ?
                                                    <div className="archer__btn-description archer__btn-description--right" onClick={()=>{this.handleViewPurchased()}}/>
                                                :
                                                    <div className={"archer__btn-preorder archer__btn-preorder--right "+( this.props.can_purchase && this.props.can_purchase.package_2_discount == true ? "" : "disable")} onClick={()=>{this.handleConfirm(2,2)}}/>
                                            }
                                        </div>
                                    :
                                        <div className="archer__nonregister">
                                            {
                                                this.props.has_discount == false && this.props.already_purchase == false ?
                                                    [
                                                        <img src={register_text} alt=''/>,
                                                        <a href="https://events.bns.in.th/archer_preregister" target="_blank" className="archer__btn-register"/>
                                                    ]
                                                :
                                                    null
                                            }
                                            
                                            {
                                                this.props.already_purchase ?
                                                    <div className="archer__btn-description archer__btn-description--center" onClick={()=>{this.handleViewPurchased()}}/>
                                                :
                                                    [
                                                        <div className={"archer__btn-pack1 "+( this.props.can_purchase && this.props.can_purchase.package_1 == true ? "" : "disable")} onClick={()=>{this.handleConfirm(1,1)}}/>,
                                                        <div className={"archer__btn-pack2 "+( this.props.can_purchase && this.props.can_purchase.package_2 == true ? "" : "disable")} onClick={()=>{this.handleConfirm(2,1)}}/>
                                                    ]
                                            }
                                            
                                        </div>
                                )
                            :
                                    <h1 class="h-login-text">กรุณาล็อคอิน</h1>
                        }
                    </div>
                </div>
                <LoadingModal />
                <RewardModal/>
                <ConfirmModal actConfirm={this.handleBuy.bind(this)}/>
                <MessageModal />
                <MessageNoCharModal
                    setLogout={this.onLogout.bind(this)}
                />
                <JobModal 
                    changeSelectedWeapon={this.changeSelectedWeapon.bind(this)}
                    selectedWeapon={this.props.weapon_id}
                    actJob={this.handleSelect.bind(this)}
                />
                <PurchasedInfoModal
                    msg={'แพ็คเกจ: '+this.props.purchased_package+'<br />อาวุธที่เลือก: '+this.props.purchased_weapon+'<br />วันที่สั่งซื้อ: '+this.props.purchased_date}
                />
            </div>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = {
    onAccountLogout,
    setValues
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main)
