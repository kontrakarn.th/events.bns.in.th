import iconHome from '../static/images/buttons/btn_home.png';
import iconScroll from '../static/images/icons/icon_scroll.png';

//collection1
import collection1_1 from '../static/images/collection1/1.png';
import collection1_2 from '../static/images/collection1/2.png';
import collection1_3 from '../static/images/collection1/3.png';
//collection2
import collection2_1 from '../static/images/collection2/1.png';
import collection2_2 from '../static/images/collection2/2.png';
import collection2_3 from '../static/images/collection2/3.png';
import collection2_4 from '../static/images/collection2/4.png';

//collection3
import collection3_1 from '../static/images/collection3/1.png';
import collection3_2 from '../static/images/collection3/2.png';
import collection3_3 from '../static/images/collection3/3.png';
import collection3_4 from '../static/images/collection3/4.png';


export const Imglist = {
	iconHome,
	iconScroll,
	collection1_1,
	collection1_2,
	collection1_3,
	collection2_1,
	collection2_2,
	collection2_3,
	collection2_4,
	collection3_1,
	collection3_2,
	collection3_3,
	collection3_4
}