import React from 'react';
import Modal from './Modal';

export default class ModalConfirm extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                title="แจ้งเตือน"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div className="modal__content--inner" dangerouslySetInnerHTML={{__html: this.props.msg}}/>
            </Modal>
        )
  }
}
