import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import OauthMiddleware from './../middlewares/OauthMiddleware';

import { Home } from './../pages/Home';
import { Dungeon } from '../pages/Dungeon';
import { HistoryList } from './../pages/HistoryList';
import { Exchange } from './../pages/Exchange';
import { Lucky } from '../pages/Lucky';

export class Web extends Component {
	render() {
		return (
			<div>
			 	<Route path={process.env.REACT_APP_EVENT_PATH} component={OauthMiddleware} />
				<Route path={process.env.REACT_APP_EVENT_PATH} exact component={Home} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/dungeon'} exact component={Dungeon} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/history'} exact component={HistoryList} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/exchange'} exact component={Exchange} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/lucky'} exact component={Lucky} />
			</div>
		);
	}
}
