
const initialState = {
    status: false,
    select: false,
    list: [],
};

export default (state = initialState, action) => {
    switch (action.type) {
        case "SET_VALUE":
            return { ...action.value, ...state, };
        case "SET_VALUES": return { ...state, ...action.value };
        default:
            return state;
    }
};

export const setSelectCharacter = (value) => ({ type: "SET_VALUE", value });
export const setValues = (value) => ({ type: "SET_VALUES", value });
