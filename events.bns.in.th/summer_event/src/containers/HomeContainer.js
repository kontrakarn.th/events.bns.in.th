import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from './../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';

import ModalMessage from '../components/ModalMessage';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
import title from './../static/images/title_condition.png';
import {
    onAccountLogout,
    setUsername,
    setSoulPermission
} from '../actions/AccountActions';
import ModalSelectCharacter from './../features/select_character';
import { setValues } from './../features/select_character/redux';

class HomeContainer extends Component {

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scollDown === false) this.setState({ scollDown: true });
        } else {
            if (this.state.scollDown === true) this.setState({ scollDown: false });
        }
    }
    //==========================================================================
    apiLobbyInfo() {
        this.setState({ showLoading: true });

        let dataSend = { type: "lobby_info" };
        let successCallback = (data) => {
            // console.log(data);
            if (data.status) {
                this.props.setUsername(data.content.character);
                this.setState({
                    permission: true,
                    showLoading: false
                })
            }
        };
        let failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            } else if (data.type == 'no_char') {
                this.setState({
                    showLoading: false,
                    showModalSelectCharacter: true,
                });
            } else {

                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_LOBBY_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    //==========================================================================
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
            showModalSelectCharacter: false,
        }
    }

    componentDidMount() {
        if (this.props.jwtToken !== "") this.apiLobbyInfo();
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            this.apiLobbyInfo();
        }
    }

    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: '700px', opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                            onScroll={this.handleScroll.bind(this)}
                        >
                            <div className="preorder">
                                <div className="preorder-inner">
                                    <section className="preorder__section preorder__section--header">
                                        <div className="header">
                                            <div className="header__scroll"><img src={iconScroll} alt="" /></div>
                                            <Link to="/summer_event/dungeon" className="btn btn-join">เข้าร่วมกิจกรรม</Link>
                                        </div>
                                    </section>
                                    <section className="preorder__section condition">
                                        <div className="container">
                                            <div className="section__head">
                                                <img src={title}/>
                                            </div>
                                            <div className="section__content">
                                                <ul className="condition__list">
                                                    <li>
                                                        ลงดันเจี้ยนตามที่กำหนดและสำเร็จเควสพื้นที่ มีโอกาสรับของรางวัล
                                                        <ul className="condition__list--sublist">
                                                            <li>- หยดน้ำ (สะสมในเว็ป)</li>
                                                            <li>- ขันน้ำ (สะสมในเว็ป)</li>
                                                            <li>- สัญลักษณ์ฮงมุน 1 ชิ้น (ส่งเข้าทางจดหมาย)</li>
                                                        </ul>
                                                    </li>
                                                    <li>ผู้เล่นจำเป็นต้องเลือกตัวละครก่อนร่วมกิจกรรม (ไม่สามารถเปลี่ยนตัวละครได้ ถ้าเลือกไปแล้ว)</li>
                                                    <li>หยดน้ำสามารถสะสมได้ จนกว่ากิจกรรมจะสิ้นสุด</li>
                                                    <li>ขันน้ำสามารถสะสมได้ จนกว่ากิจกรรมจะสิ้นสุด</li>
                                                    <li>หยดน้ำ 1 หยด สามารถสุ่มกาชาปองได้ 1 ครั้ง</li>
                                                    <li>ขันน้ำสามารถสะสมเพื่อแลกของรางวัลได้ในระบบแลกเปลี่ยน</li>
                                                    <li>ระยะเวลากิจกรรม 24 เมษายน (19:00:00) - 15 พฤษภาคม 2562 (05:59:59)</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </section>
                                    {/* <div className="bg-bottom"></div> */}
                                </div>
                            </div>
                        </ScrollArea>
                    </div>
                    :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />

                <ModalSelectCharacter
                    open={this.state.showModalSelectCharacter}
                    actDone={(data_name)=>{
                        this.props.setValues(data_name);
                        this.setState({showModalSelectCharacter:false})
                    }}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    permission: state.AccountReducer.permission
})

const mapDispatchToProps = {
    onAccountLogout, setUsername, setSoulPermission,setValues
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
