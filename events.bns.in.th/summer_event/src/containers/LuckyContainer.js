import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import ModalMessage from '../components/ModalMessage';
import ModalConfirm from '../components/ModalConfirm';

import { Menu } from '../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import ball1 from './../static/images/ball1.png'
import ball2 from './../static/images/ball2.png'
import ball3 from './../static/images/ball3.png'

import SoulEffect from '../components/SoulEffect';

import title from './../static/images/title_lucky.png'
import charactor from './../static/images/charactor_lucky.png'
import count from './../static/images/count.png'
import used from './../static/images/used.png'
import itemBox from './../static/images/box.jpg'

// Items
import waterBowl from './../static/images/items/water_bowl.png'
import soulStone from './../static/images/items/soul_stone.png'
import chandraStone from './../static/images/items/chandra_stone.png'
import skyBlueStone from './../static/images/items/sky_blue_stone.png'

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
} from '../actions/AccountActions';

class LuckyContainer extends Component {

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scrollDown === false) this.setState({ scrollDown: true });
        } else {
            if (this.state.scrollDown === true) this.setState({ scrollDown: false });
        }
    }

    ConfirmOpen() {
        this.setState({
            showModalConfirm: true,
            //modalContent: this.state.soulItems[id-1]
        });
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiWaterDropInfo() {
        this.setState({ showLoading: true });

        let dataSend = { type: "water_drop_info" };
        let successCallback = (data) => {
            if (data.status) {
                this.props.setUsername(data.content.character);
                this.setState({
                    water_drop_remain: data.content.water_drop_remain,
                    water_drop_used: data.content.water_drop_used,
                    permission: true,
                    showLoading: false,
                })
            }
        };
        let failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            } else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_WATER_DROP_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiPlayWaterDrop() {
        this.setState({ showLoading: true});

        let dataSend = { type: "play_water_drop" };
        let successCallback = (data) => {
            if (data.status) {

                let respMsg = '';
                data.content.rewards.map((value, index) => {
                    respMsg += value.title+"<br/>";
                })

                this.setState({
                    showLoading: false,
                    showModalConfirm: false,
                    showModalMessage: "คุณได้รับ<br />"+respMsg,
                    water_drop_remain: data.content.water_drop_remain,
                    water_drop_used: data.content.water_drop_used,
                });
            }
        };
        let failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                    showModalConfirm: false
                });
            } else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                    showModalConfirm: false
                });
            }
        };
        apiPost("REACT_APP_API_PLAY_WATER_DROP", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scrollDown: false,
            showModalConfirm: false,
            modalContent: {},
            water_drop_remain: 0,
            water_drop_used: 0,
            items: [
                {
                    img: waterBowl,
                    percent: '100%',
                    rareItems: '',
                    name: "ขันน้ำ",
                    piece: "1 ชิ้น"
                },
                {
                    img: soulStone,
                    percent: '',
                    rareItems: 'หายาก',
                    name: "แพ็คเกจหินโซล",
                    piece: "20 ชิ้น"
                },
                {
                    img: chandraStone,
                    percent: '',
                    rareItems: 'หายาก',
                    name: "แพ็คเกจหินจันทรา",
                    piece: "3 ชิ้น"
                },
                {
                    img: skyBlueStone,
                    percent: '',
                    rareItems: 'หายาก',
                    name: "เศษเกล็ดสีคราม",
                    piece: "1 ชิ้น"
                },
            ],

            showModalMessage: "",
            username: "",
            buy_soul_info: [],
            buy_soul_result : [],
        }
    }


    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") this.apiWaterDropInfo();

    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            this.apiWaterDropInfo();
        }
    }

    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: 700, opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                            onScroll={this.handleScroll.bind(this)}
                        >
                            <div className="preorder">
                                <div className="page__wrapper lucky-page">
                                    <Menu />
                                    <section className="preorder__section lucky">

                                        <div className="container">
                                            <div className="section__head">
                                                <img src={title}/>
                                                <div className="block__count">

                                                    <div className="block__count--wrapper"><img src={count}/> <span className="block__count--sum">{this.state.water_drop_remain}</span></div>
                                                    <div className="block__count--wrapper"><img src={used}/> <span className="block__count--sum">{this.state.water_drop_used}</span></div>
                                                </div>
                                            </div>
                                            <div className="section__content clearfix">
                                                <div className="block__items clearfix">
                                                    <div className="block__items--left text-center">
                                                        <div className="block__items--leftInner">
                                                            <div className="block__items--leftImg best">
                                                                <img src={itemBox}/>
                                                            </div>
                                                            <div className="block__items--leftContent">
                                                                <div>
                                                                    กล่องซัมเมอร์ <br/>เยือกแข็ง
                                                                </div>
                                                                <p className="text--blue">มีอะไรบ้างนะ ?</p>
                                                            </div>
                                                        </div>
                                                        <a className={"btn-default"+(this.state.water_drop_remain === 0? " disabled": "")} onClick={()=>this.ConfirmOpen()}>เปิดกล่อง</a>
                                                    </div>
                                                    <div className="block__items--right">
                                                        <div className="block__items--rightInner">
                                                        {
                                                            this.state.items.map((items, index) => {

                                                                return(
                                                                    <div>
                                                                        <div className="block__items--rightImg">
                                                                            <img src={items.img}/>
                                                                        </div>
                                                                        {
                                                                            items.percent !== '' ?
                                                                                <div className="percent">{items.percent}</div>
                                                                            :
                                                                             null
                                                                        }
                                                                        {
                                                                            items.rareItems !== '' ?
                                                                                <div className="rareItems">{items.rareItems}</div>
                                                                            :
                                                                             null
                                                                        }
                                                                        <div>{items.name}</div>
                                                                        <div>{items.piece}</div>
                                                                    </div>
                                                                )
                                                            })
                                                        }

                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </section>

                                </div>
                            </div>
                        </ScrollArea>
                    </div>
                    :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />
                <ModalConfirm
                    open={this.state.showModalConfirm}
                    actConfirm={() => this.apiPlayWaterDrop()}
                    actClose={() => this.setState({ showModalConfirm: false })}
                    msg={"ยืนยันเปิดกล่องซัมเมอร์เยือกแข็ง?"}
                />
                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    // permission: state.AccountReducer.permission,
    buy_soul_result: state.AccountReducer.buy_soul_result
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(LuckyContainer))
