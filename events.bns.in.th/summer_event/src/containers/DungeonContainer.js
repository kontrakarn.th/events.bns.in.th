import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import ModalMessage from '../components/ModalMessage';
import ModalConfirm from '../components/ModalConfirm';

import { Menu } from '../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import ball1 from './../static/images/ball1.png'
import ball2 from './../static/images/ball2.png'
import ball3 from './../static/images/ball3.png'

import SoulEffect from '../components/SoulEffect';

import title from './../static/images/title_event.png'
// Card
import card1 from './../static/images/dungeon_card/Dungeon_icon01.png'
import card2 from './../static/images/dungeon_card/Dungeon_icon02.png'
import card3 from './../static/images/dungeon_card/Dungeon_icon03.png'
import card4 from './../static/images/dungeon_card/Dungeon_icon04.png'
import card5 from './../static/images/dungeon_card/Dungeon_icon05.png'
import card6 from './../static/images/dungeon_card/Dungeon_icon06.png'
import card7 from './../static/images/dungeon_card/Dungeon_icon07.png'
import card8 from './../static/images/dungeon_card/Dungeon_icon08.png'
import card9 from './../static/images/dungeon_card/Dungeon_icon09.png'
import card10 from './../static/images/dungeon_card/Dungeon_icon010.png'
import card11 from './../static/images/dungeon_card/Dungeon_icon011.png'
import card12 from './../static/images/dungeon_card/Dungeon_icon012.png'

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
} from '../actions/AccountActions';

class DungeonContainer extends Component {

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scrollDown === false) this.setState({ scrollDown: true });
        } else {
            if (this.state.scrollDown === true) this.setState({ scrollDown: false });
        }
    }

    actConfirmOpen(id) {
        // console.log("quest id : "+id)
        this.setState({
            showModalConfirm: true,
            quest_id: id,
        });
    }
    //==========================================================================
    apiDailyInfo() {
        this.setState({ showLoading: true });

        let dataSend = { type: "daily_info" };
        let successCallback = (data) => {
            if (data.status) {
                this.props.setUsername(data.content.character);
                this.setState({
                    quests: data.content.quests,
                    permission: true,
                    showLoading: false,
                })
            }
        };
        let failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            } else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_DAILY_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiPlayWaterGachapon() {
        this.setState({ showLoading: true });
        
        let dataSend = { type: "play_water_gachapon", quest_id: this.state.quest_id };
        let successCallback = (data) => {
            if (data.status) {

                let questId = data.content.quest_id;
                let quests = [...this.state.quests]
                quests[questId-1] = {
                    id: data.content.quest_id,
                    title: data.content.quest_title,
                    remain_rights: data.content.remain_rights,
                }

                this.setState({
                    showLoading: false,
                    showModalMessage: "คุณได้รับ <br />"+data.content.rewards,
                    showModalConfirm: false,
                    quests: quests,//<<<<<<this update quests values from played
                });
            }
        };
        let failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                    showModalConfirm: false
                });
            } else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                    showModalConfirm: false
                });
            }
        };
        apiPost("REACT_APP_API_PLAY_WATER_GACHPON", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    //==========================================================================
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scrollDown: false,
            showModalConfirm: false,
            modalConfirmMessage: "ยืนยันเปิดหา<br/>ขันน้ำ และ หยดน้ำ ?",
            showModalMessage: "",
            modalContent: {},
            quest_id: 0,
            cardItems: [
                { img: card1, value: "/100" },
                { img: card2, value: "/10" },
                { img: card3, value: "/10" },
                { img: card4, value: "/10" },
                { img: card5, value: "/100" },
                { img: card6, value: "/10" },
                { img: card7, value: "/10" },
                { img: card8, value: "/10" },
                { img: card9, value: "/100" },
                { img: card10, value: "/10" },
                { img: card11, value: "/10" },
                { img: card12, value: "/10" },
            ],
            quests: [
                {id: 1, title: "เรือโจรสลัดต้องสาป", remain_rights: 0},
                {id: 2, title: "คำสาปแห่งความโลภ", remain_rights: 0},
                {id: 3, title: "ลึกลงไปในกรุสมบัติ", remain_rights: 0},
                {id: 4, title: "ลำนำแห่งชาวเรือ", remain_rights: 0},
                {id: 5, title: "บั้นปลายของผู้พิทักษ์", remain_rights: 0},
                {id: 6, title: "ตัวตนของหัวหน้าช่างแห่งโรงหลอมเครื่องจักร", remain_rights: 0},
                {id: 7, title: "การพักผ่อนของผู้ตาย", remain_rights: 0},
                {id: 8, title: "ผู้ปกครองแห่งหุบเขาหิวกระหาย", remain_rights: 0},
                {id: 9, title: "ผู้เฝ้ามองอมตะ", remain_rights: 0},
                {id: 10, title: "เปลวเพลิงที่ไม่มีวันดับ", remain_rights: 0},
                {id: 11, title: "ตกสู่เปลวเพลิง", remain_rights: 0},
                {id: 12, title: "หัวหน้าอสูรแห่งลัทธิเงามังกรดำ", remain_rights: 0},

            ],
        }
    }


    componentDidMount() {
        if (this.props.jwtToken !== "") this.apiDailyInfo();
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            this.apiDailyInfo();
        }
    }

    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: 700, opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                            onScroll={this.handleScroll.bind(this)}
                        >
                            <div className="preorder">
                                <div className="page__wrapper event-page">
                                    <Menu />
                                    <section className="preorder__section soul">

                                        <div className="container">
                                            <div className="section__head">
                                                <img src={title}/>
                                                <p>”1 ดันเจี้ยนเท่ากับ 1 สิทธิ์ ใช้เวลาในการอัพเดทข้อมูลประมาณ 15 นาที “ <br/>
                                                    (สามารถเคลียร์ดันเจี้ยนเดิมได้ไม่จำกัดจำนวนครั้งในการเคลียร์)
                                                </p>
                                            </div>
                                            <div className="section__content clearfix">
                                                {this.state.cardItems.map((items, index) => {
                                                    let count = this.state.quests[index].remain_rights;
                                                    return (
                                                        // if ไม่ตรงเงื่อนไข : เพิ่ม class disabled ที่ dungeon__items
                                                        <div key={"dungeon__items_"+index} className={"dungeon__items "+(count === 0 ?" disabled":"")}>
                                                            <div className="dungeon__items--img">
                                                                <img src={items.img}/>
                                                            </div>
                                                            {count === 0 ?
                                                                <div className="btn-default disabled" >
                                                                    ไม่ตรงเงื่อนไข
                                                                </div>
                                                                :
                                                                <div className="btn-default" onClick={()=>this.actConfirmOpen(this.state.quests[index].id)}>
                                                                    {count}
                                                                </div>
                                                            }
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>

                                    </section>

                                </div>
                            </div>
                        </ScrollArea>



                    </div>
                    :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />
                <ModalConfirm
                    open={this.state.showModalConfirm}
                    actConfirm={() => this.apiPlayWaterGachapon()}
                    actClose={() => this.setState({
                        showModalConfirm: false
                    })}
                    msg={this.state.modalConfirmMessage}
                />
                <ModalMessage
                    open={this.state.showModalMessage !== ""}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    permission: state.AccountReducer.permission,
    buy_soul_result: state.AccountReducer.buy_soul_result
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(DungeonContainer))
