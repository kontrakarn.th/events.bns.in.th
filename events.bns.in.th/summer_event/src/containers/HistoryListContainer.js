import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';

import ModalMessage from '../components/ModalMessage';

import { Menu } from '../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
import iconMenu from './../static/images/icon_menu.png'
import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
} from '../actions/AccountActions';

class HistoryListContainer extends Component {


    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scollDown === false) this.setState({ scollDown: true });
        } else {
            if (this.state.scollDown === true) this.setState({ scollDown: false });
        }
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiItemHistory() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "item_history" };
        let successCallback = (data) => {

            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    historyList: data.content.items_list
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_ITEM_HISTORY", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
            showModalMessage: "",

            username: "",
            historyList: [],
        }
    }

    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") this.apiItemHistory();

    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            this.apiItemHistory();
        }
    }


    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: 700, opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="preorder">
                                <div className="page__wrapper history-page">
                                    <Menu />
                                    <section className="preorder__section history">
                                        <div className="container">
                                            <div className="section__head">
                                                <h3>ประวัติ</h3>
                                            </div>
                                            <div className="section__content">
                                                <ul className="list-unstyled history__list content__scroll">
                                                {
                                                    this.state.historyList && this.state.historyList.length > 0 ?
                                                        this.state.historyList.map((items, index) => {
                                                            return(
                                                                <li>
                                                                    <div className="history__list--left">{items.item_title}</div>
                                                                    <div className="history__list--right">
                                                                        <span className="history__list--date">{items.created_at}</span>
                                                                        {/* <span>20:00:00</span> */}
                                                                    </div>
                                                                </li>
                                                            )
                                                        })
                                                    :
                                                        null
                                                }
                                                </ul>
                                            </div>
                                            <div className="section__btn">
                                                <Link to="/summer_event" className="btn btn-default">หน้าหลัก</Link>
                                            </div>
                                        </div>
                                    </section>

                                </div>
                            </div>
                        </ScrollArea>

                    </div>
                    :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />
                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    // item_history: state.AccountReducer.item_history
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HistoryListContainer)
