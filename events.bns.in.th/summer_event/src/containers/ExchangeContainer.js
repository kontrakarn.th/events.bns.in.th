import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import ModalMessage from '../components/ModalMessage';
import ModalConfirm from '../components/ModalConfirm';

import { Menu } from '../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import ball1 from './../static/images/ball1.png'
import ball2 from './../static/images/ball2.png'
import ball3 from './../static/images/ball3.png'

import SoulEffect from '../components/SoulEffect';

import title from './../static/images/title_exchange.png'
import charactor from './../static/images/charactor_lucky.png'
import textWaterDrop from './../static/images/text_water_drop.png'
import itemBox from './../static/images/box.jpg'

// Items
import exchangeItem1 from './../static/images/exchange/costume_summer.jpg'
import exchangeItem2 from './../static/images/exchange/costume_snorkel.jpg'
import exchangeItem3 from './../static/images/exchange/costume_hat.jpg'
import exchangeItem4 from './../static/images/exchange/costume_hat_rose.jpg'
import exchangeItem5 from './../static/images/exchange/costume_hat_tour.jpg'

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
} from '../actions/AccountActions';

class ExchangeContainer extends Component {
    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scrollDown === false) this.setState({ scrollDown: true });
        } else {
            if (this.state.scrollDown === true) this.setState({ scrollDown: false });
        }
    }

    ConfirmOpen(index) {
        // console.log(id);
        this.setState({
            showModalConfirm: true,
            package_id: this.state.items[index].id,
            modalConfirmMessage: "คุณต้องการแลก<br />"+this.state.items[index].name+" ?",
        });
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= =========

    apiExchangeInfo() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "exchange_info" };
        let successCallback = (data) => {
            if (data.status) {
                this.props.setUsername(data.content.character);
                this.setState({
                    permission: true,
                    showLoading: false,
                    water_bowl_remain: data.content.water_bowl_remain,
                    water_bowl_used: data.content.water_bowl_used,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_EXCHANGE_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiExchangeReward() {

        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "exchange_reward", package_id: this.state.package_id };
        let successCallback = (data) => {
            if (data.status) {
                this.setState({
                    permission: true,
                    showLoading: false,
                    showModalConfirm: false,
                    showModalMessage: data.message,
                    water_bowl_remain: data.content.water_bowl_remain,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                    showModalConfirm: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                    showModalConfirm: false,
                });
            }
        };
        apiPost("REACT_APP_API_EXCHANGE_REWARD", this.props.jwtToken, dataSend, successCallback, failCallback);
}
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)
        this.state = {
            permission: true,
            showLoading: true,
            scrollDown: false,
            showModalConfirm: false,
            modalConfirmMessage: "",
            showModalMessage: "",
            modalContent: {},
            package_id: 0,
            water_bowl_remain: 0,
            water_bowl_used: 0,
            items: [
                {
                    id: 1,
                    img: exchangeItem1,
                    name: "ชุดทะเลฤดูร้อน",
                    exchange: 20,
                    tag: "ทุกเผ่า"
                },
                {
                    id: 2,
                    img: exchangeItem2,
                    name: "สน็อกเกิ้ล",
                    exchange: 15,
                    tag: "ชาย"
                },
                {
                    id: 3,
                    img: exchangeItem3,
                    name: "หมวกคลุมผม",
                    exchange: 15,
                    tag: "ลินชาย"
                },
                {
                    id: 4,
                    img: exchangeItem4,
                    name: "หมวกกุหลาบบาน",
                    exchange: 15,
                    tag: "ลินหญิง"
                },
                {
                    id: 5,
                    img: exchangeItem5,
                    name: "หมวกท่องเที่ยวหน้าร้อน",
                    exchange: 15,
                    tag: "หญิง"
                },
            ],

        }
    }

    componentDidMount() {
        if (this.props.jwtToken !== "") this.apiExchangeInfo();

    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            this.apiExchangeInfo();
        }
    }


    render() {
        let {water_bowl_remain} = this.state
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: 700, opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                            onScroll={this.handleScroll.bind(this)}
                        >
                            <div className="preorder">
                                <div className="page__wrapper exchange-page">
                                    <Menu />
                                    <section className="preorder__section exchange">

                                        <div className="container">
                                            <div className="section__head">
                                                <img src={title}/>
                                                <div className="block__count">
                                                    <div className="block__count--wrapper"><img src={textWaterDrop}/> <span className="block__count--sum">{water_bowl_remain}</span></div>
                                                </div>
                                            </div>
                                            <div className="section__content clearfix">
                                                <div className="clearfix">
                                                    {
                                                        this.state.items.map((items, index) => {

                                                            return(
                                                                <div className="text-center block__exchange">
                                                                    <div className="block__exchange--wrapper">
                                                                        <div className="block__exchange--img">
                                                                            <div className="tag">{items.tag}</div>
                                                                            <img src={items.img}/>
                                                                        </div>
                                                                        <div className="block__exchange--content">
                                                                            <div>
                                                                               {items.name}
                                                                            </div>
                                                                            <p className="text--blue">
                                                                                จำนวนที่ใช้แลก <br/>
                                                                                <span className="block__exchange--count">{items.exchange}</span>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div className="block__btn-exchange">
                                                                        <a
                                                                            className={"btn-default"+(water_bowl_remain<items.exchange ? " disabled":"") }
                                                                            onClick={()=>this.ConfirmOpen(index)}
                                                                        >แลกไอเทม</a>
                                                                    </div>
                                                                </div>
                                                            )
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        </div>

                                    </section>

                                </div>
                            </div>
                        </ScrollArea>

                    </div>
                    :
                    <Permission />
                }
                <Loading
                    open={this.state.showLoading}
                />
                <ModalConfirm
                    open={this.state.showModalConfirm}
                    actConfirm={() => this.apiExchangeReward()}
                    actClose={() => this.setState({
                        showModalConfirm: false
                    })}
                    msg={this.state.modalConfirmMessage}
                />
                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    // permission: state.AccountReducer.permission,
    buy_soul_result: state.AccountReducer.buy_soul_result
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(ExchangeContainer))
