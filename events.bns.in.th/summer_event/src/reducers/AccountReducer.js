import * as actionTypes from './../constants/Actions';

const defaultState = {
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",
    username: "",
    permission: {},
    buy_soul_result: [],
    item_history: []
};

const AccountReducer = (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH':
            return Object.assign({}, state, {
                sessionKey: action.value
            });
        case actionTypes.ACCOUNT_AUTHED:
            return Object.assign({}, state, {
                userData: action.value
            });
        case actionTypes.RECEIVE_SESSION_KEY:
            return Object.assign({}, state, {
                sessionKey: action.value
            });
        case actionTypes.SET_LOGIN_URL:
            return Object.assign({}, state, {
                loginUrl: action.value
            });
        case actionTypes.SET_LOGOUT_URL:
            return Object.assign({}, state, {
                logoutUrl: action.value
            });
        case actionTypes.ACCOUNT_LOGIN:
            return Object.assign({}, state, {
                authState: "LOGGED_IN"
            });
        case actionTypes.ACCOUNT_LOGOUT:
            return Object.assign({}, state, {
                authState: "LOGGED_OUT"
            });
        case actionTypes.ACCOUNT_AUTH_CHECKED:
            return Object.assign({}, state, {
                authed: true
            });
        case 'SET_JWT_TOKEN':
            return Object.assign({}, state, {
                jwtToken: action.value
            });
        case 'SET_USERNAME':
            return Object.assign({}, state, {
                username: action.value
            });
        case 'SET_SOUL_PERMISSION':
            return Object.assign({}, state, {
                permission: action.value
            });
        case 'SET_BUY_SOUL_RESULT':
            return Object.assign({}, state, {
                buy_soul_result: action.value
            })
        case 'SET_ITEM_HISTORY':
            return Object.assign({}, state, {
                item_history: action.value
            })
        case "SET_VALUES": return { ...state, ...action.value };
        default:
            return state;
    }
}

export default AccountReducer;
