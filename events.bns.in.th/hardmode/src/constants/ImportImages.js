const permission                  = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/permission.jpg';
const icon_scroll                 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/icon_scroll.png';
const btn_home                    = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_home.png';
const menu_background             = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/menu_background.png';

const home_background             = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/home_background.jpg';
const   detail                    = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/detail.png';
const   time                      = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/time.png';
const   btn_join                  = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_join.png';

const package_background          = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/package_background.jpg';
const   package_section           = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/package_section.png';
const     btn_got_package         = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_got_package.png';
const     btn_package             = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_package.png';
const   quest_section             = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/quest_section.png';
const     btn_got_3points         = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_got_3points.png';
const     btn_got_10points        = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_got_10points.png';
const     btn_10points            = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_10points.png';
const     btn_3points             = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_3points.png';
const     boss1                   = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss1.png';
const     boss2                   = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss2.png';
const     boss3                   = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss3.png';
const     boss4                   = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss4.png';
const     boss5                   = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss5.png';
const     boss6                   = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss6.png';
const     boss7                   = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/boss7.png';
const     btn_point               = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_point.png';
const     btn_got_point           = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_got_point.png';
const   treasure_section          = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/treasure_section.png';
const     btn_treasure            = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_treasure.png';
const     btn_got_treasure        = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_got_treasure.png';
const     popup_treasure_100      = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/popup_treasure_100.png';
const     popup_treasure_maybe    = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/popup_treasure_maybe.png';
const   exchange_section          = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/exchange_section.png';
const     exchange_count          = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/exchange_count.png';
const     btn_exchange            = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_exchange.png';
const     btn_exchange_inactive   = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_exchange_inactive.png';
const     btn_plus                = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_plus.png';
const   towels_count              = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/towels_count.png';
const     towels_icon             = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/towels_icon.png';

const history_background          = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/history_background.jpg';
const   point_history_frame       = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/point_history_frame.png';
const   reward_history_frame      = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/reward_history_frame.png';
const   testure_history_frame     = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/testure_history_frame.png';

const message_modal               = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/message_modal.png';
const confirm_modal               = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/confirm_modal.png';
const select_character_modal      = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/select_character_modal.png';
const btn_confirm                 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_confirm.png';
const btn_cancel                  = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/btn_cancel.png';
const icon_down_arrow             = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/icon_down_arrow.png';
const turban_modal                = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/turban_modal.png';
const towelset_modal              = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/towelset_modal.png';
const icon_cross                  = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hardmode/images/icon_cross.png';

export default {
	permission,
  icon_scroll,
  btn_home,
	menu_background,

  home_background,
    detail,
    time,
    btn_join,

  package_background,
    package_section,
      btn_got_package,
      btn_package,
    quest_section,
      btn_got_3points,
      btn_got_10points,
      btn_10points,
      btn_3points,
      boss1,
      boss2,
      boss3,
      boss4,
      boss5,
      boss6,
      boss7,
      btn_point,
      btn_got_point,
    treasure_section,
      btn_treasure,
      btn_got_treasure,
      popup_treasure_100,
      popup_treasure_maybe,
    exchange_section,
      exchange_count,
      btn_exchange,
      btn_exchange_inactive,
      btn_plus,
    towels_count,
      towels_icon,

	history_background,
		point_history_frame,
		reward_history_frame,
		testure_history_frame,

	message_modal,
	confirm_modal,
	select_character_modal,
  btn_confirm,
  btn_cancel,
	icon_down_arrow,
  turban_modal,
  towelset_modal,
  icon_cross,
}
