const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    showUserName: true,
    showCharacterName: false,
    showMenu: true,
    username: "",
    character_name: "",
    menu: [
      {title: "หน้าหลัก", page: "main", link: process.env.REACT_APP_EVENT_PATH+"/"},
      {title: "กิจกรรม", page: "package", link: process.env.REACT_APP_EVENT_PATH+"/package"},
      {title: "ประวัติการได้รับคะแนน", page: "point", link: process.env.REACT_APP_EVENT_PATH+"/point"},
      {title: "ประวัติการแลกไอเทม", page: "reward", link: process.env.REACT_APP_EVENT_PATH+"/reward"},
      {title: "ประวัติการเปิดหีบห่อผ้าน้อยชิ้น", page: "testure", link: process.env.REACT_APP_EVENT_PATH+"/testure"},
    ],
    characters: [
    ],
    selected_char: false,
    loginStatus: false,
    permission: true,

    //===modal
    modal_open: "loading",//"loading","message","character",""confirm","turban","towelset","confirm_package","confirm_claim"
    modal_message: "",
    modal_reward_data: {
      can_claim: false,
      limit_count: 0,
      reward_id: 0,
      reward_name: "",
      tokens: 0,
    },

    //===event base value
    eventPath: (path)=>(process.env.REACT_APP_EVENT_PATH+path),
    status: false,

    already_purchased: false,
    claim_daily_free_points: "not_in_condition",
    claim_daily_purchase_points: "not_in_condition",
    open_claim_reward: false,
    points: 0,
    tokens: 0,
    claim_reward_list: {
      claim_start: [
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
      ],
      claim_limit: [
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
      ],
      claim_unlimit: [
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
        {reward_id: 0, can_claim: false, reward_name: "", tokens: 0},
      ],
    },
    quest_daily_list: [
      {id: 0, title: "", dungeon: "", points: 0, status: "not_in_condition"},
      {id: 1, title: "", dungeon: "", points: 0, status: "not_in_condition"},
      {id: 2, title: "", dungeon: "", points: 0, status: "not_in_condition"},
      {id: 3, title: "", dungeon: "", points: 0, status: "not_in_condition"},
      {id: 4, title: "", dungeon: "", points: 0, status: "not_in_condition"},
      {id: 5, title: "", dungeon: "", points: 0, status: "not_in_condition"},
      {id: 6, title: "", dungeon: "", points: 0, status: "not_in_condition"},
    ],
    history_point: [],
    history_reward: [],
    history_testure: [],
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         :return ({...state, sessionKey: action.value});
        case 'ACCOUNT_AUTHED'       :return ({...state, userData: action.value});
        case 'RECEIVE_SESSION_KEY'  :return ({...state, sessionKey: action.value});
        case 'SET_LOGIN_URL'        :return ({...state, loginUrl: action.value});
        case 'SET_LOGOUT_URL'       :return ({...state, logoutUrl: action.value});
        case 'ACCOUNT_LOGIN'        :return ({...state, authState: "LOGGED_IN"});
        case 'ACCOUNT_LOGOUT'       :return ({...state, authState: "LOGGED_OUT"});
        case 'ACCOUNT_AUTH_CHECKED' :return ({...state, authed: true});
        case 'SET_JWT_TOKEN'        :return ({...state, jwtToken: action.value});

        case "SET_VALUE"            :
            return (Object.keys(defaultState).indexOf(action.key) >= 0) ? {
                ...state,
                [action.key]: action.value
            } : state;
        case "SET_VALUES"           :return {...state, ...action.value};
        default:
            return state;
    }
}

export const onAccountAuth = (sessionKey) => ({type: 'ACCOUNT_AUTH', value: sessionKey});
export const onAccountAuthed = (userData) => ({type: 'ACCOUNT_AUTHED', value: userData});
export const onAccountLogin = () => ({type: 'ACCOUNT_LOGIN', value: ""});
export const onAccountLogout = () => ({type: 'ACCOUNT_LOGOUT', value: ""});
export const receiveSessionKey = (sessionKey) => ({type: 'RECEIVE_SESSION_KEY', value: sessionKey});
export const setLoginUrl = (url) => ({type: 'SET_LOGIN_URL', value: url});
export const setLogoutUrl = (url) => ({type: 'SET_LOGOUT_URL', value: url});
export const onAccountAuthChecked = () => ({type: 'ACCOUNT_AUTH_CHECKE', value: true})
export const setJwtToken = (token) => ({type: 'SET_JWT_TOKEN', value: token});

export const setValue = (key, value) => ({type: "SET_VALUE", value, key});
export const setValues = (value) => ({type: "SET_VALUES", value});
