import React from 'react';

import ModalMessage from './_Message';
import ModalLoading from './_Loading';
import ModalSelectCharacter from './_SelectCharacter';
import ModalDetailTurban from './_DetailTurban';
import ModalDetailTowelSet from './_DetailTowelSet';
import ModalPurchasePackage from './_ConfirmPurchasePackage';
import ModalConfirmClaimPurchasePoint from './_ConfirmClaimPurchasePoint';
import ModalConfirmClaimReward from './_ConfirmClaimReward';
import ModalConfirmOpenBox from './_ConfirmOpenBox';

export default (props)=> (
  <>
    <ModalLoading />
    <ModalMessage />
    <ModalSelectCharacter />
    <ModalDetailTurban />
    <ModalDetailTowelSet />
    <ModalPurchasePackage />
    <ModalConfirmClaimPurchasePoint />
    <ModalConfirmClaimReward />
    <ModalConfirmOpenBox />
  </>
)
