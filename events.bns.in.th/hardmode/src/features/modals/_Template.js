import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from "../../constants/ImportImages";

const ModalLoading = props => {
  const name = "message";
  const onClick = ()=>{};

  return (
    <ModalCore
        open={props.modal_open === name}
        onClick={()=>onClick()}
    >
      <ModalcontentStyle>
        <div className="message" dangerouslySetInnerHTML={{__html: props.modal_message}} />
        <div className="buttons">
          <a className="btn btn--confirm"onClick={()=>props.setValues({modal_open:''})} />
          <a className="btn btn--cancel"onClick={()=>props.setValues({modal_open:''})} />
        </div>
      </ModalcontentStyle>
    </ModalCore>
  )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalLoading);

const ModalcontentStyle = styled.div`
  position: relative;
  display: block;
  width: 417px;
  height: 308px;
  padding: 70px 10px 0px;
  background-image: url(${imgList.message_modal});
  background-image: url(${imgList.l_msg_modal});
  .message {
    color: #FFFFFF;
    font-size: 20px;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 55%;
    text-align: center;
  }
  .buttons {
    position: absolute;
    left: 0px;
    bottom: 40px;
    display: block;
    width: 100%;
    heihgt: 42px;
    text-align: center;
  }
  .btn {
    display: inline-block;
    width: 96px;
    height: 42px;
    margin: 0px 20px;
    background-position: top center;
    &--confirm {
      background-image: url(${imgList.btn_confirm});
    }
    &--cancel {
      background-image: url(${imgList.btn_cancel});
    }
    &:hover {
      background-position: bottom center;
    }
  }
`;
