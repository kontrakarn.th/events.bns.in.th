import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import imgList from "../../constants/ImportImages";
import {apiPost} from '../../constants/Api';

const ModalConfirmOpenBox = props => {
  const name = "confirm_open";
  
  const apiOpenBox = () => {
    props.setValues({modal_open: "loading"});
    let dataSend = {type: "open_box"};
    let successCallback = (res) => {
      if(res.status) {
        props.setValues({
          ...res.data,
          modal_open:"message",
          modal_message: res.message,
          history_reward: [],
          history_testure: [],
        })
      } else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    let failCallback = (res) => {
      if (res.type === 'no_permission') {
        props.setValues({
            permission: false,
            modal_open: "",
        });
      }else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    apiPost("REACT_APP_API_POST_OPEN_BOX", props.jwtToken, dataSend, successCallback, failCallback);
  };

  let data = props.modal_reward_data;
  return (
    <ModalCore
        open={props.modal_open === name}
        onClick={()=>props.setValues({modal_open:''})}
    >
      <ModalcontentStyle>
        <div className="message">
         <div>ต้องการเปิดหีบห่อผ้าน้อยชิ้น ?</div>
        </div>
        <div className="buttons">
          <a className="btn btn--confirm"onClick={()=>apiOpenBox()} />
          <a className="btn btn--cancel"onClick={()=>props.setValues({modal_open:''})} />
        </div>
      </ModalcontentStyle>
    </ModalCore>
  )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalConfirmOpenBox);

const ModalcontentStyle = styled.div`
  position: relative;
  display: block;
  width: 417px;
  height: 308px;
  padding: 70px 10px 0px;
  background-image: url(${imgList.confirm_modal});
  .message {
    color: #FFFFFF;
    font-size: 20px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    height: 55%;
    text-align: center;
    span {
      color: #d19e56;
    }
    img {
      vertical-align: middle;
    }
  }
  .buttons {
    position: absolute;
    left: 0px;
    bottom: 40px;
    display: block;
    width: 100%;
    heihgt: 42px;
    text-align: center;
  }
  .btn {
    display: inline-block;
    width: 96px;
    height: 42px;
    margin: 0px 20px;
    background-position: top center;
    &--confirm {
      background-image: url(${imgList.btn_confirm});
    }
    &--cancel {
      background-image: url(${imgList.btn_cancel});
    }
    &:hover {
      background-position: bottom center;
    }
  }
`;
