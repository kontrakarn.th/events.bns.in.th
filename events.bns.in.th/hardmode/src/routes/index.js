import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';

import OauthLogin from '../features/oauthLogin';
import Home from '../pages/Home';
import Package from '../pages/Package';
import PointHistory from '../pages/PointHistory';
import RewardHistory from '../pages/RewardHistory';
import TestureHistory from '../pages/TestureHistory';

export default (props)=>{
  let eventPath = process.env.REACT_APP_EVENT_PATH;
  return (
    <BrowserRouter>
      <>
        <Route path={eventPath} component={OauthLogin}/>
        <Route path={eventPath} exact component={Home}/>
        <Route path={eventPath+"/package"} exact component={Package}/>
        <Route path={eventPath+"/point"} exact component={PointHistory}/>
        <Route path={eventPath+"/reward"} exact component={RewardHistory}/>
        <Route path={eventPath+"/testure"} exact component={TestureHistory}/>
      </>
    </BrowserRouter>
  )
}
