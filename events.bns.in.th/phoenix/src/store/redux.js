const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    // showCharacterName: true,
    characters:[],
    username: "",
    character: "",
    loginStatus: false,
    showScroll: true,
    noScroll: true,
    loading: false,
    permission: true,
    status: false,
    can_play: false,


    //===modal
    modal_open: "",//"loading","detail","showreward",""showreward","send","modalInput"
    modal_message: "",
    modal_item: "",
    modal_uid: "",
    modal_get_name: "",
    modal_combin: -1,
    modal_trade: -1,

    //===event base value
    list_item: [
        {id: 1, name:"icon_1_R", count: 0},
        {id: 2, name:"icon_1_G", count: 0},
        {id: 3, name:"icon_1_B", count: 0},
        {id: 4, name:"icon_2_R", count: 0},
        {id: 5, name:"icon_2_G", count: 0},
        {id: 6, name:"icon_2_B", count: 0},
        {id: 7, name:"icon_3_R", count: 0},
        {id: 8, name:"icon_3_G", count: 0},
        {id: 9, name:"icon_3_B", count: 0},
        {id: 10, name:"icon_4_R", count: 0},
        {id: 11, name:"icon_4_G", count: 0},
        {id: 12, name:"icon_4_B", count: 0},
        // {id: 13, name:"item_05_R", count: 0},
        // {id: 14, name:"item_05_G", count: 0},
        // {id: 15, name:"item_05_B", count: 0},
        // {id: 16, name:"item_06_R", count: 0},
        // {id: 17, name:"item_06_G", count: 0},
        // {id: 18, name:"item_06_B", count: 0},
    ],

    list_redeem: [
        {package_id:1,name:"ชุดเทพพิทักษ์",icon:"icon_1_Base"},
        {package_id:2,name:"เครื่องประดับเทพพิทักษ์",icon:"icon_2_Base"},
        {package_id:3,name:"ปีกเทพพิทักษ์",icon:"icon_3_Base"},
        {package_id:4,name:"กล่องอาวุธลวงตาเทพพิทักษ์",icon:"icon_4_Base"},
        // {package_id:5,name:"ชุดอาทิตย์อัสดง",icon:"item_05_Base"},
        // {package_id:6,name:"เซ็ทกุหลาบซันตามาเรีย",icon:"item_06_Base"},
    ],
    img2name: {
        "icon_1_Base":"ชุดจุมพิตอสูร",
        "icon_2_Base":"หมวกจุมพิตอสูร",
        "icon_3_Base":"ปีกจุมพิตอสูร",
        "icon_4_Base":"กล่องอาวุธลวงตาจุมพิตอสูร",
        "icon_5_Base":"ชุดอาทิตย์อัสดง",
        "icon_6_Base":"เซ็ทกุหลาบซันตามาเรีย",
    },
    //combine
    materials: [],

    //===soul values
    buy_soul_result: [],
    buy_soul_count: 0,
    buy_soul_cost: 0,


    //===history values
    items_history: [],

    //===trade values
    exchangeinfo: {
        amt: 0,
        icon: "",
        id: 0,
        key: "",
        product_title: ""
    },

    //===send item
    target_uid:-1,
    target_username: "",
    my_bag: [
        {id:1,title:"ชุดเทพพิทักษ์",icon:"icon_1",key:"icon_1",quantity:0},
        {id:2,title:"เซตเทพพิทักษ์",icon:"icon_2",key:"icon_2",quantity:0},
        {id:3,title:"เครื่องประดับเทพพิทักษ์",icon:"icon_3",key:"icon_3",quantity:0},
        {id:4,title:"ปีกเทพพิทักษ์",icon:"icon_4",key:"icon_4",quantity:0},
        {id:5,title:"กล่องอาวุธลวงตาเทพพิทักษ์",icon:"icon_5",key:"icon_5",quantity:0},
        {id:6,title:"หินสัตว์เลี้ยงเทพพิทักษ์",icon:"icon_6",key:"icon_6",quantity:0},
        {id:7,title:"เซ็ตเทพพิทักษ์เมี๊ยว",icon:"icon_7",key:"icon_7",quantity:0},
        // {id:8,title:"หินสัตว์เลี้ยงสาวน้อยจากจันทรา",icon:"S_Moon_05_Base",key:"S_Moon_05",quantity:0},
        // {id:9,title:"เซ็ทเหมียวจันทราสีเงิน",icon:"S_Moon_09_Base",key:"S_Moon_09",quantity:0},
        // {id:10,title:"เซ็ทนักเรียนเกาหลียุค 80",icon:"S_Moon_10_Base",key:"S_Moon_10",quantity:0},
        // {id:11,title:"กล่องอาวุธลวงตาจันทราสีเงิน",icon:"S_Moon_11_Base",key:"S_Moon_11",quantity:0},
    ],
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         : return ({ ...state, sessionKey: action.value });
        case 'ACCOUNT_AUTHED'       : return ({ ...state, userData: action.value });
        case 'RECEIVE_SESSION_KEY'  : return ({ ...state, sessionKey: action.value });
        case 'SET_LOGIN_URL'        : return ({ ...state, loginUrl: action.value });
        case 'SET_LOGOUT_URL'       : return ({ ...state, logoutUrl: action.value });
        case 'ACCOUNT_LOGIN'        : return ({ ...state, authState: "LOGGED_IN" });
        case 'ACCOUNT_LOGOUT'       : return ({ ...state, authState: "LOGGED_OUT" });
        case 'ACCOUNT_AUTH_CHECKED' : return ({ ...state, authed: true });
        case 'SET_JWT_TOKEN'        : return ({ ...state, jwtToken: action.value });

        case "SET_VALUE"            : return (Object.keys(defaultState).indexOf(action.key) >= 0) ? { ...state, [action.key]: action.value } : state;
        case "SET_VALUES"           : return { ...state, ...action.value };
        default:
            return state;
    }
}

export const onAccountAuth          = (sessionKey)  => ({ type: 'ACCOUNT_AUTH',         value: sessionKey });
export const onAccountAuthed        = (userData)    => ({ type: 'ACCOUNT_AUTHED',       value: userData });
export const onAccountLogin         = ()            => ({ type: 'ACCOUNT_LOGIN',        value: "" });
export const onAccountLogout        = ()            => ({ type: 'ACCOUNT_LOGOUT',       value: "" });
export const receiveSessionKey      = (sessionKey)  => ({ type: 'RECEIVE_SESSION_KEY',  value: sessionKey });
export const setLoginUrl            = (url)         => ({ type: 'SET_LOGIN_URL',        value: url });
export const setLogoutUrl           = (url)         => ({ type: 'SET_LOGOUT_URL',       value: url });
export const onAccountAuthChecked   = ()            => ({ type: 'ACCOUNT_AUTH_CHECKE',  value: true })
export const setJwtToken            = (token)       => ({ type: 'SET_JWT_TOKEN',        value: token });

export const setValue               = (key, value) => ({ type: "SET_VALUE", value, key });
export const setValues              = (value) => ({ type: "SET_VALUES", value });
