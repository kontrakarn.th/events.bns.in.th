import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';

import modal_bg from './images/modal_bg.png';
import title_text from './images/title_text.png';
import confirm_btn from './images/confirm_btn.png';
import cancel_btn from './images/cancel_btn.png';
import modal_btn    from './images/modal_btn.png'

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="uid"
            actClickOutside={false}
        >
            <ModalMessageContent>
                <div className="contenttitle">ยืนยัน</div>
                <div className="contenttext">
                    <div>กรุณากรอก UID ของเพื่อน</div>
                    <Input
                        id="f_uid"
                        type="text"
                        value={props.modal_uid}
                        onChange={(e)=>props.setValues({modal_uid:e.target.value})}
                    />
                </div>
            </ModalMessageContent>
            <ModalBottom>
                    <Btns onClick={()=>{if(props.actConfirm)props.actConfirm()}}>ตกลง</Btns>
                    <Btns onClick={()=>props.setValues({modal_open:''})}>ยกเลิก</Btns>
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 438px;
    height: 257px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${modal_bg});
    background-size: 100% 100%;
    box-sizing: border-box;
    padding: 0% 5% 15%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttitle{
        font-size: 21px;
        width: 215px;
        height: 55px;
        line-height: 55px;
        text-shadow: 1px 1px 5px #000;
        margin: 0 auto;
        font-family: 'Kanit-SemiBold';
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 20px;
        line-height: 1.4em;
        word-break: break-word;
        height: 82%;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;
const ModalBottom = styled.div`
    display: block;
    position: absolute;
    bottom: -55px;
    z-index: 50;
    left: 0;
    right: 0;
    text-align: center;
`;

const Btns = styled.div`
    display: inline-block;
    cursor: pointer;
    margin: 0 10px;
    background: url(${modal_btn}) top center no-repeat;
    width: 171px;
    height: 43px;
    font-family: 'Kanit-Medium';
    line-height: 40px;
    color: #fff;
    text-shadow: 1px 1px 2px #000;
    &:hover{
        background-position: bottom center;
    }
`
const Input = styled.input`
    display: block;
    cursor: pointer;
    width: 90%;
    margin-top: 10px;
    text-align: center;
    font-size: 1em;
    background-color: #161312;
    border: 0;
    padding: 5px;
    color: #fff;
    &:focus{
        border: 0;
        outline: 0;
    }
`
