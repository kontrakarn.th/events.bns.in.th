import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';

import close_btn from './images/close_btn.png';
import {Imglist} from '../../constants/Import_Images';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="item"
            actClickOutside={false}
            key={`modal_${props.modal_open}_${props.modal_item}`}
        >
            <ModalContent>
                    <img src={Imglist[props.modal_item]} alt=""/>
                    <CloseBtn src={close_btn}  alt="" onClick={()=>props.setValues({modal_open: ''})}/>
                    {
                        props.modal_item === 'item_3' &&
                            <MagIcon src={Imglist['magnify_icon']} alt='' onClick={()=>props.setValues({modal_open: 'item',modal_item:'item_2'})}/>
                    }
            </ModalContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalContent = styled.div`
    position: relative;
    display: block;
    text-align: center;
    box-sizing: border-box;
`;

const CloseBtn = styled.img`
    position: absolute;
    top: -10px;
    right: -10px;
    cursor:pointer;
`
const MagIcon = styled.img`
    position: absolute;
    top: 10%;
    right: 4%;
    cursor:pointer;
`
