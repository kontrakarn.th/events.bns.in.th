import React from 'react';
import "./soulanimation.css";
import imgSoulBreak from './soul_break.png';
import imgSoulLoop from './soul_loop.png';

export default class SoulBall extends React.Component {
    actClick(){
        if(this.props.click){
            this.props.click();
        }
        // this.setState({break:true});
    }
//========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props){
        super(props);
        this.timeout=null;
        this.state = {
            break: false,
            paraT: 0
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.break !== prevProps.break){
            this.setState({break:this.props.break});
        }
    }
    render() {
        return (
            <div className={"soulanimation"+(this.state.break? "":" blink")} onClick={()=>this.actClick()}>
                {this.state.break ?
                    <div className="soulanimation__soul soulanimation__soul--break"/>
                :
                    <div className="soulanimation__soul soulanimation__soul--loop"/>
                }
                <img
                    className={"soulanimation__item"+(this.state.break? " show":"")}
                    src={this.props.itemImg || ""}
                    alt=""
                />
            </div>
        )
    }
}
