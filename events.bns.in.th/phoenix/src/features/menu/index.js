import React from 'react';
import { Link } from 'react-router-dom';
import {Imglist} from '../../constants/Import_Images';
import styled from 'styled-components';

export default class Menu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showMenu:false
        }
    }

    handleClickMenu() {
        this.setState({
            showMenu:!this.state.showMenu
        })
    }
    addActiveClass(check){
        if(this.props.page === check){
            return 'active'
        }
    }
    render() {
        return (
            <MenuLayout>
                <div className={"backdrop " + (this.state.showMenu ? "show" : " ") }></div>
                <a onClick={this.handleClickMenu.bind(this)} className="menu-icon"><img alt="icon" src={Imglist['icon_menu']} /></a>
                <ul className={"list " + (this.state.showMenu ? "show" : " ") }>
                    <li className="menu-close"><div onClick={this.handleClickMenu.bind(this)}></div></li>
                    <li><Link className={this.addActiveClass('soul') } to={`${process.env.REACT_APP_EVENT_PATH}/buy-soul`}>ซื้อโซล</Link></li>
                    <li><Link className={this.addActiveClass('history')} to={`${process.env.REACT_APP_EVENT_PATH}/history`}>ประวัติการแลก</Link></li>
                    <li><Link className={this.addActiveClass('send')} to={`${process.env.REACT_APP_EVENT_PATH}/send`}>ส่งของรางวัล</Link></li>
                    <li><Link className={this.addActiveClass('combine')} to={`${process.env.REACT_APP_EVENT_PATH}/combine`}>ผสมชิ้นส่วน</Link></li>
                    <li><Link className={this.addActiveClass('redeem')} to={`${process.env.REACT_APP_EVENT_PATH}/redeem`}>แลกชิ้นส่วน</Link></li>
                    <li><Link className={this.addActiveClass('trade')} to={`${process.env.REACT_APP_EVENT_PATH}/trade`}>แลกหญ้าซาฮวา</Link></li>
                    <li><Link className={this.addActiveClass('main')} to={`${process.env.REACT_APP_EVENT_PATH}`}>เงื่อนไขโปรโมชั่น</Link></li>
                    <li><Link className={this.addActiveClass('costume')} to={`${process.env.REACT_APP_EVENT_PATH}/costume`}>ตัวอย่างชุด</Link></li>
                </ul>
            </MenuLayout>
        )
    }
}

const MenuLayout = styled.div`
    font-family: 'Kanit-Medium';
    .menu-icon{
        margin: 20px 0 0 20px;
        display: inline-block;
        cursor: pointer;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 10;
    }
    .menu-close{
        position: absolute;
        top: 10px;
        right: 15px;
        cursor: pointer;
        > div {
            width:25px;
            height: 30px;
            position: relative;
            &::before,
            &::after{
                content: '';
                position: absolute;
                left: 0;
                right: 0;
                margin: 0 auto;
                height: 25px;
                width: 3px;
                top: 5px;
                background-color: #fff;
            }
        }
        > div:before{
            -o-transform: rotate(45deg);
            -moz-transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        > div::after{
            -o-transform: rotate(-45deg);
            -moz-transform: rotate(-45deg);
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
        }
    }
    .list{
        position: fixed;
        top: 0;
        margin: 0;
        z-index: 999;
        left: -1000px;
        width: 250px;
        background:url(${Imglist['menu_bg']}) bottom center no-repeat;
        background-size: cover;
        padding: 100px 30px;
        height: 100vh;
        list-style-type: none;
        -webkit-transition: all 0.3s ease;
        -moz-transition: all 0.3s ease;
        transition: all 0.3s ease;
        text-align: left;
        &.show{
            left: 0;
        }
        & a {
            text-decoration: none;
            color: #959595;
            font-size: 18px;
            display: block;
            padding: 5px 0;
            &:hover, &.active{
                color: #ffffff;
            }
        }
    }
    .backdrop{
        background-color: rgba(0,0,0,0.9);
        position: absolute;
        left: 0;
        right: 0;
        width: 100%;
        bottom: 0;
        top: 0;
        z-index: 200;
        display: none;
        &.show{
            display: block;
        }
    }
`
