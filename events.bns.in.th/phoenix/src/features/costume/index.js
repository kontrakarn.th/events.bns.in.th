import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
import Slider from 'react-slick';
import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalShowItem from '../../features/modals/ModalShowItem';
import {Imglist} from './../../constants/Import_Images';
import Menu from '../../features/menu';

const ArrowNext =(props)=>{
    return(
            <div
                  className="arrow--next"
                  onClick={props.onClick}
            />
    )
}

const ArrowPrev =(props)=>{
    return(
         <div
          className="arrow--prev"
          onClick={props.onClick}
        />
    )
}

class Costume extends React.Component {


    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
         let settings = {
          dots: true,
          infinite: true,
          speed: 500,
          fade: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          nextArrow: <ArrowNext/>,
          prevArrow: <ArrowPrev/>
        };
        return (
            <PageWrapper>
                {/* <Menu page='costume'/> */}
                <Link to={process.env.REACT_APP_EVENT_PATH + '/buy-soul'}><EnterBtn/></Link>
                {/* <EnterBtn/> */}
                <Board>
                    <div className="slidebox">
                        <Slider {...settings}>
                                {
                                    Array(4).fill(1).map((item,index) => {
                                        return(
                                                <div className="slider" key={`item_${index+1}`}>
                                                        <img className="slider__img" src={Imglist[`preview_${index+1}`]} alt=""/>
                                                </div>
                                        )
                                    })
                                }
                        </Slider>
                    </div>
                </Board>
                <ModalLoading />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Costume);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_main']}) #110d0c;
    width: 1105px;
    padding-top: 630px;
    text-align: center;
`

const EnterBtn = styled.div`
    background: top center no-repeat url(${Imglist['btn_enter']});
    width: 305px;
    height: 65px;
    cursor: pointer;
    position: absolute;
    top: 440px;
    left: 410px;
    &:hover{
        background-position: bottom center;
    }
`
const Board = styled.div`
    display: block;
    margin: 130px auto 0;
    width: 931px;
    height: 894px;
    padding-bottom: 124px;
    background: top center no-repeat url(${Imglist['frame_costume']});
    background-size: 100%;
    .slidebox{
        // background: rgba(255,255,255,0.5);
        width: 750px;
        height: 700px;
        position: relative;
        margin: 0 auto;
        top: 40px;
        .slider{
            &__img{
                display: block;
                margin: 0 auto;
            }
        }
    }
    .arrow{
        &--next{
            display: block;
            width: 26px;
            height: 49px;
            background: url(${Imglist['btn_next']}) top center no-repeat;
            top: 50%;
            transform: translate3d(0,-50%,0);
            position: absolute;
            right: -25px;
        }
        &--prev{
            display: block;
            width: 26px;
            height: 49px;
            background: url('${Imglist['btn_prev']}') top center no-repeat;
            top: 50%;
            transform: translate3d(0,-50%,0);
            left: -25px;
            position: absolute;
        }
    }
    .slick-dots li button{
        padding: 2px;
    }
    .slick-dots li button:before{
        color: #969696;
        font-size: 12px;
    }
    .slick-dots li.slick-active button:before{
        opacity: 1;
    }
    .slick-dots{
        bottom: 0;
    }
`
