import btn_enter from '../static/images/btn_enter.png'
import condition_frame from '../static/images/condition_frame.png';
import other_bg from '../static/images/other_bg.jpg';
import bg_main from '../static/images/bg_main.jpg';
import icon_menu from '../static/images/icon_menu.png';
import menu_close from '../static/images/menu_close.png';
import menu_bg from '../static/images/menu_bg.jpg';
import title_frame from '../static/images/title_frame.png';
import frame from '../static/images/frame.png';
import btn from '../static/images/btn.png';
import frame_costume from '../static/images/frame_costume.png';
import diamond from './../static/images/diamond.png';
import buy_soul_button from './../static/images/buy_soul_button.png';
import buy_soul_border from './../static/images/buy_soul_border.png';
import buy_soul_char from '../static/images/buy_soul_char.png';
import btn_buy_soul	 from '../static/images/btn_buy_soul.png'


import combine_arrow from './../static/images/combine_arrow.png';

import redeem_plus_btn from './../static/images/redeem_plus_btn.png';
import redeem_line_btn from './../static/images/redeem_line_btn.png';

import preview_1 from '../static/images/preview_1.png'
import preview_2 from '../static/images/preview_2.png'
import preview_3 from '../static/images/preview_3.png'
import preview_4 from '../static/images/preview_4.png'

import btn_next from '../static/images/btn_next.png';
import btn_prev from '../static/images/btn_prev.png';

import icon_1 			from '../static/images/items/icon_1.png';
import icon_1_set		from '../static/images/items/icon_1_set.png'
import icon_1_set_get	from '../static/images/items/icon_1_set_get.png'
import icon_1_Base 		from '../static/images/items/icon_1_Base.png';
import icon_1_get 		from '../static/images/items/icon_1_get.png';
import icon_1_R from '../static/images/items/icon_1_R.png';
import icon_1_G from '../static/images/items/icon_1_G.png';
import icon_1_B from '../static/images/items/icon_1_B.png';
import icon_1_R_get from '../static/images/items/icon_1_R_get.png';
import icon_1_G_get from '../static/images/items/icon_1_G_get.png';
import icon_1_B_get from '../static/images/items/icon_1_B_get.png';
import icon_1_NL	from '../static/images/items/icon_1_NL.png'
import icon_2_B_get from '../static/images/items/icon_2_B_get.png';
import icon_2 from '../static/images/items/icon_2.png';
import icon_2_Base from '../static/images/items/icon_2_Base.png';
import icon_2_get from '../static/images/items/icon_2_get.png';
import icon_2_R from '../static/images/items/icon_2_R.png';
import icon_2_G from '../static/images/items/icon_2_G.png';
import icon_2_B from '../static/images/items/icon_2_B.png';
import icon_2_R_get from '../static/images/items/icon_2_R_get.png';
import icon_2_G_get from '../static/images/items/icon_2_G_get.png';
import icon_2_NL	from '../static/images/items/icon_2_NL.png'
import icon_3 from '../static/images/items/icon_3.png';
import icon_3_Base from '../static/images/items/icon_3_Base.png';
import icon_3_get from '../static/images/items/icon_3_get.png';
import icon_3_R from '../static/images/items/icon_3_R.png';
import icon_3_G from '../static/images/items/icon_3_G.png';
import icon_3_B from '../static/images/items/icon_3_B.png';
import icon_3_R_get from '../static/images/items/icon_3_R_get.png';
import icon_3_G_get from '../static/images/items/icon_3_G_get.png';
import icon_3_B_get from '../static/images/items/icon_3_B_get.png';
import icon_3_NL	from '../static/images/items/icon_3_NL.png'
import icon_4 from '../static/images/items/icon_4.png';
import icon_4_Base from '../static/images/items/icon_4_Base.png';
import icon_4_get from '../static/images/items/icon_4_get.png';
import icon_4_R from '../static/images/items/icon_4_R.png';
import icon_4_G from '../static/images/items/icon_4_G.png';
import icon_4_B from '../static/images/items/icon_4_B.png';
import icon_4_R_get from '../static/images/items/icon_4_R_get.png';
import icon_4_G_get from '../static/images/items/icon_4_G_get.png';
import icon_4_B_get from '../static/images/items/icon_4_B_get.png';
import icon_4_NL	from '../static/images/items/icon_4_NL.png'
import icon_5 from '../static/images/items/icon_5.png';
import icon_5_get	from '../static/images/items/icon_5_get.png'
// import item_05_Base from '../static/images/items/item_05_Base.png';
// import item_05_get from '../static/images/items/item_05_get.png';
// import item_05_R from '../static/images/items/item_05_R.png';
// import item_05_G from '../static/images/items/item_05_G.png';
// import item_05_B from '../static/images/items/item_05_B.png';
// import item_05_R_get from '../static/images/items/item_05_R_get.png';
// import item_05_G_get from '../static/images/items/item_05_G_get.png';
// import item_05_B_get from '../static/images/items/item_05_B_get.png';
import icon_6 		from '../static/images/items/icon_6.png';
import icon_6_get	from '../static/images/items/icon_6_get.png'
// import item_06_Base from '../static/images/items/item_06_Base.png';
// import item_06_get from '../static/images/items/item_06_get.png';
// import item_06_R from '../static/images/items/item_06_R.png';
// import item_06_G from '../static/images/items/item_06_G.png';
// import item_06_B from '../static/images/items/item_06_B.png';
// import item_06_R_get from '../static/images/items/item_06_R_get.png';
// import item_06_G_get from '../static/images/items/item_06_G_get.png';
// import item_06_B_get from '../static/images/items/item_06_B_get.png';
import icon_7 from '../static/images/items/icon_7.png';
import icon_8 from '../static/images/items/icon_8.png';
import icon_9 from '../static/images/items/icon_9.png';
import icon_10 from '../static/images/items/icon_10.png';
import icon_11 from '../static/images/items/icon_11.png';
import icon_12 from '../static/images/items/icon_12.png';
import icon_13 from '../static/images/items/icon_13.png';
import icon_14 from '../static/images/items/icon_14.png';
import icon_15 from '../static/images/items/icon_15.png';
import icon_16 from '../static/images/items/icon_16.png';
import icon_17 from '../static/images/items/icon_17.png';
import icon_18 from '../static/images/items/icon_18.png';
import icon_19 from '../static/images/items/icon_19.png';
import icon_20 from '../static/images/items/icon_20.png';
import icon_21 from '../static/images/items/icon_21.png';
import icon_22 from '../static/images/items/icon_22.png';
import icon_23 from '../static/images/items/icon_23.png';
import icon_24 from '../static/images/items/icon_24.png';
import icon_25 from '../static/images/items/icon_25.png';
import icon_26 from '../static/images/items/icon_26.png';
import icon_27 from '../static/images/items/icon_27.png';
import icon_28 from '../static/images/items/icon_28.png';
import icon_29 from '../static/images/items/icon_29.png';



//
// import items_01 	from '../static/images/items/items_01.png'
// import items_01_R 	from '../static/images/items/items_01_R.png';
// import items_01_G 	from '../static/images/items/items_01_G.png';
// import items_01_B 	from '../static/images/items/items_01_B.png';
// import items_02		from '../static/images/items/items_02.png'
// import items_03		from '../static/images/items/items_03.png'
// import items_04 	from '../static/images/items/items_04.png'
// import items_05 	from '../static/images/items/items_05.png'
// import items_06 	from '../static/images/items/items_06.png'
// import items_07 	from '../static/images/items/items_07.png'


export const Imglist = {
	btn,
	icon_menu,
	menu_close,
	menu_bg,
	btn_enter,
	condition_frame,
	other_bg,
	title_frame,
	frame,
	bg_main,
	frame_costume,

	preview_1,
	preview_2,
	preview_3,
	preview_4,

	diamond,
	buy_soul_button,
	buy_soul_border,
	buy_soul_char,
	btn_buy_soul,


	combine_arrow,
	redeem_plus_btn,
	redeem_line_btn,
	btn_next,
	btn_prev,

	icon_1_B_get,
	icon_1_NL,
	icon_1_B,
	icon_1_Base,
	icon_1_G_get,
	icon_1_G,
	icon_1_get,
	icon_1_R_get,
	icon_1_R,
	icon_1,
	icon_1_set,
	icon_1_set_get,
	icon_2_B_get,
	icon_2_B,
	icon_2_Base,
	icon_2_G_get,
	icon_2_NL,
	icon_2_G,
	icon_2_get,
	icon_2_R_get,
	icon_2_R,
	icon_2,
	icon_3_B_get,
	icon_3_B,
	icon_3_Base,
	icon_3_G_get,
	icon_3_G,
	icon_3_get,
	icon_3_R_get,
	icon_3_R,
	icon_3,
	icon_3_NL,
	icon_4_B_get,
	icon_4_B,
	icon_4_Base,
	icon_4_G_get,
	icon_4_G,
	icon_4_get,
	icon_4_R_get,
	icon_4_R,
	icon_4,
	icon_4_NL,
	// item_05_B_get,
	// item_05_B,
	// item_05_Base,
	// item_05_G_get,
	// item_05_G,
	// item_05_get,
	// item_05_R_get,
	// item_05_R,
	icon_5,
	icon_5_get,
	// item_06_B_get,
	// item_06_B,
	// item_06_Base,
	// item_06_G_get,
	// item_06_G,
	// item_06_get,
	// item_06_R_get,
	// item_06_R,
	icon_6,
	icon_6_get,
	icon_7,
	icon_8,
	icon_9,
	icon_10,
	icon_11,
	icon_12,
	icon_13,
	icon_14,
	icon_15,
	icon_16,
	icon_17,
	icon_18,
	icon_19,
	icon_20,
	icon_21,
	icon_22,
	icon_23,
	icon_24,
	icon_25,
	icon_26,
	icon_27,
	icon_28,
	icon_29,

	// items_01,
	// items_01_R,
	// items_01_G,
	// items_01_B,
	// items_02,
	// items_03,
	// items_04,
	// items_05,
	// items_06,
	// items_07,
}
