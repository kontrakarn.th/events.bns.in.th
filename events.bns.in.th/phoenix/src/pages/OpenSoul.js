import React                from 'react';
import Style                from 'styled-components';
import F11Layout            from './../features/F11Layout/';
import Menu                 from './../features/menu';
import SoulBall             from './../features/SoulBall/';
import SoulBallRareItem     from './../features/SoulBallRareItem/'
import { connect }          from 'react-redux';
import { setValues }        from './../store/redux';
import { Imglist }          from './../constants/Import_Images';
import { Frame }            from './../common/frame';
import { CustomBtn }        from './../common/buttons';
import { Link, Redirect }   from 'react-router-dom';

class OpenSoul extends React.Component {
    //==========================================================================
    actOpenSoul (n) {
        let ary = [...this.state.openSoul];
        ary[n] = true;
        this.setState({openSoul: ary})
    }
    actOpenAll(){
        let ary = this.state.openSoul.map(()=>true);
        this.setState({openSoul: ary})
    }
    //==========================================================================
    constructor(props){
        super(props);
        this.state = {
            openSoul: [false,false,false,false,false,false,false,false,false,false,false],
        }
    }
    render() {
        let { buy_soul_result } = this.props;
        console.log(buy_soul_result);
        return (
            <F11Layout showUserName={true} showCharacterName={false}>
                <Menu page='soul'/>
                <OpenSoulContent>
                    <Frame title="เปิดโซล">
                        <div className="souls">
                            
                            {buy_soul_result.map((item,index)=>{
                                let open = this.state.openSoul[index];
                                return (
                            
                                    item.item_type === 'rare_item' ?
                                        <SoulBallRareItem
                                            key={"soul_"+index}
                                            break={open}
                                            itemImg={Imglist[item.icon]}
                                            click={()=>this.actOpenSoul(index)}
                                        />
                                    :
                                        <SoulBall
                                            key={"soul_"+index}
                                            break={open}
                                            itemImg={Imglist[item.icon]}
                                            click={()=>this.actOpenSoul(index)}
                                        />
                                )
                            })}
                        </div>
                        <div className="btngroup">
                            <Link className="btngroup__btn" to={process.env.REACT_APP_EVENT_PATH + '/buy-soul'}><CustomBtn>ซื้อโซลเพิ่ม</CustomBtn></Link>
                            <a className="btngroup__btn" onClick={()=>this.actOpenAll()}><CustomBtn>เปิดทั้งหมด</CustomBtn></a>
                        </div>
                    </Frame>
                </OpenSoulContent>
            </F11Layout>
        )
    }
}
// const mapStateToProps = state => ({...state.Main});
const mapStateToProps = state =>{
    return ({...state.Main});
};
// const mapStateToProps = state => ({Main:state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(OpenSoul);

const OpenSoulContent = Style.div`
    background: top center no-repeat url(${Imglist['other_bg']}) #11122d;
    width: 1105px;
    padding: 150px 0 26px;
    text-align: center;
    .char {
        position: absolute;
        top: 25%;
        left: -12%;
    }
    .char2 {
        position: absolute;
        top: 6%;
        right: -18%;
    }
    .btngroup {
        position: absolute;
        bottom: 10px;
        left: 20%;
        display: block;
        width: 60%;
        &__btn {
            display: inline-block
            margin: 0px 10px;
        }
    }
    .souls {
        position: relative;
        // display: block;
        // top: 40px;
        height: 330px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-wrap: wrap;
        flex: 1;
        .soulanimation,
        .soulrareanimation {
            transform: scale(1.5);
            margin: 20px;
        }
    }
`;
