import React                    from 'react';
import Style                    from 'styled-components';
import F11Layout                from './../features/F11Layout/';
import Menu                     from './../features/menu';
import ScrollArea               from 'react-scrollbar';
import { connect }              from 'react-redux';
import { setValues }            from './../store/redux';
import {Imglist}                from './../constants/Import_Images';
import {Frame}                  from './../common/frame';
import {CustomBtn, GrayBtn}     from './../common/buttons';
import {Item}                   from './../common/item';
import { Link, Redirect }       from 'react-router-dom';
import ModalConfirm             from './../features/modals/ModalConfirm';
import ModalMessage             from './../features/modals/ModalMessage';
import {apiPost}                from './../constants/Api';

export class Combine extends React.Component {
    apiCombine(){
        if(this.props.modal_combin>0 && this.props.modal_combin<9){
            // this.props.setValues({ modal_open: "loading" });
            let dataSend = {type:'combine',package_id:this.props.modal_combin};
            let successCallback = (data) => {
                // if (data.status) {
                //     this.props.setValues({
                //         ...data.content.response,
                //         modal_open: '',
                //     });
                // }
                this.props.setValues({
                    ...data.content,
                    modal_open: "message",
                    modal_message: "ผสมชิ้นส่วน "+this.props.modal_item+" เรียบร้อย",
                })
            };
            const failCallback = (data) => {
                if (data.type == 'no_permission') {
                    this.props.setValues({
                        permission: false,
                        modal_open: "",
                    });
                }else {
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: data.message,
                    });
                }
            };
            apiPost("REACT_APP_API_POST_MERGE", this.props.jwtToken, dataSend, successCallback, failCallback);
        }else{
            this.props.setValues({
                modal_open:"message",
                modal_message: "เกิดข้อผิดพลาดในการผสมชิ้นส่วน",
            });
        }

    }

    apiGetMaterial(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'material'};
        let successCallback = (data) => {
                this.props.setValues({
                    ...data.content,
                    modal_open: "",
                });
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_MATERIAL", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    //==========================================================================
    actOpenModalConfirm(n,title){
        console.log(n);
        this.props.setValues({
            modal_open: "confirm",
            modal_message: "คุณต้องการผสมชิ้นส่วน "+title+" หรือไม่?",
            modal_combin: n,
            modal_item: title,
        })
    }
    actConfirm(){
        this.apiCombine();
    }
    //==========================================================================
    constructor(props){
        super(props);
        this.state = {
            list: [
                {id: 1, name: "icon_1", title:"ชุดเทพพิทักษ์" },
                {id: 2, name: "icon_2", title:"เครื่องประดับเทพพิทักษ์" },
                {id: 3, name: "icon_3", title:"ปีกเทพพิทักษ์" },
                {id: 4, name: "icon_4", title:"กล่องอาวุธลวงตาเทพพิทักษ์" },
                // {id: 5, name: "item_05", title:"ชุดอาทิตย์อัสดง" },
                // {id: 6, name: "item_06", title:"เซ็ทกุหลาบซันตามาเรีย" },
            ],
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiGetMaterial();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiGetMaterial();
        }
    }

    render() {
        let { list } = this.state;
        let { materials } = this.props;
        return (
            <F11Layout showUserName={true} showCharacterName={false}>
                <Menu page='combine'/>
                <CombineContent>
                    <Frame title="ผสมชิ้นส่วน">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:350, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#000", opacity:1}}
                        >
                            <div className="list">
                                {list.map((item,index)=>{
                                    let r = materials[index] ? materials[index][0] : {};
                                    let g = materials[index] ? materials[index][1] : {};
                                    let b = materials[index] ? materials[index][2] : {};
                                    let lock = (r.quantity && g.quantity && b.quantity )? false: true;
                                    return (
                                        <div className="list__slot" key={"combine_slot_"+index}>
                                            <Item name={item.name+"_R_get"} count={r.quantity} />
                                            <Item name={item.name+"_G_get"} count={g.quantity} />
                                            <Item name={item.name+"_B_get"} count={b.quantity} />
                                            <img src={Imglist["combine_arrow"]} />
                                            <Item name={item.name+"_Base"} count={""}/>
                                            <div className="list__btn" onClick={lock ? ()=>{} : ()=>this.actOpenModalConfirm(item.id,item.title)}>
                                                <GrayBtn lock={lock}>ผสมชิ้นส่วน</GrayBtn>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                        </ScrollArea>
                        <div className="btngroup">
                            {/* <Link className="btngroup__btn" to={process.env.REACT_APP_EVENT_PATH + '/redeem'}><CustomBtn>รับของรางวัล</CustomBtn></Link> */}
                            <Link className="btngroup__btn" to={process.env.REACT_APP_EVENT_PATH + '/buy-soul'}><CustomBtn>ซื้อโซลเพิ่ม</CustomBtn></Link>
                            {/* <Link className="btngroup__btn" to={process.env.REACT_APP_EVENT_PATH + '/send'}><CustomBtn>ส่งให้เพื่อน</CustomBtn></Link> */}
                        </div>
                    </Frame>
                </CombineContent>
                <ModalConfirm
                    actConfirm={()=>this.actConfirm()}
                />
                <ModalMessage />
            </F11Layout>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Combine);

const CombineContent = Style.div`
    background: top center no-repeat url(${Imglist['other_bg']}) #11122d;
    width: 1105px;
    padding: 150px 0 26px;
    text-align: center;
    .list {
        margin: 0px auto;
        display: block;
        width: 70%;
        height: 100%;
        &__slot {
            position: relative;
            display: inline-block;
            width: 100%;
            // height: 183px;
            height: 225px;
            border-radius: 7px;
            padding: 15px;
            box-sizing: border-box;
            background-color: #1d1812;
            margin-bottom: 25px;
            &>* {
                margin: 20px;
                vertical-align: middle;
            }
            &>div {
                display: inline-block;
                margin: 5px;
            }
        }
        &__btn {
            display: block;
            margin: 20px 5px 0 !important;
        }
    }
    .btngroup {
        position: absolute;
        bottom: 10px;
        left: 0px;
        display: block;
        width: 100%;
        &__btn {
            display: inline-block
            margin: 0px 10px;
        }
    }
`;
