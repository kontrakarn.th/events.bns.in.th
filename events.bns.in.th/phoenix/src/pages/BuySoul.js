import React                from 'react';
import styled               from 'styled-components';
import F11Layout            from './../features/F11Layout/';
import Menu                 from './../features/menu';
import SoulBall             from './../features/SoulBall/';
import { connect }          from 'react-redux';
import { setValues }        from './../store/redux';
import { Imglist }          from './../constants/Import_Images';
import { Frame }            from './../common/frame';
import { Link, Redirect }   from 'react-router-dom';
import ModalConfirm         from './../features/modals/ModalConfirm';
import {apiPost}              from './../constants/Api';

class BuySoul extends React.Component {
    api(){
        if(this.props.buy_soul_count!=1 && this.props.buy_soul_count!=11){
            this.props.setValues({
                modal_open:"message",
                modal_message: "โปรดเลือกจำนวนที่ต้องการเปิด",
            });
        }else{
            this.props.setValues({ modal_open: "loading",buy_soul_result:[] });
            let dataSend = {type:'buysoul',package_id:this.props.buy_soul_count == 1 ? 1 : 2 };
            let successCallback = (data) => {

                    this.props.setValues({
                        buy_soul_result: data.content,
                        modal_open: "",
                        modal_message: ""
                    })

                    this.setState({opensoul: true})
            };
            const failCallback = (data) => {
                if (data.type == 'no_permission') {
                    this.props.setValues({
                        permission: false,
                        modal_open: "",
                    });
                }else {
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: data.message,
                    });
                }
            };
            apiPost("REACT_APP_API_POST_BUY_SOUL", this.props.jwtToken, dataSend, successCallback, failCallback);


        }

    }
    //==========================================================================
    actBuySoul(n,c) {
        this.props.setValues({
            buy_soul_count: n,
            buy_soul_cost: c,
            modal_open: "confirm",
            modal_message: (n==1) ? "จำนวน 1 โซล ?":"จำนวน 10+1 โซล ?",
        })
    }
    actConfirmBuySoul() {
        this.api();
    }
    //==========================================================================
    constructor(props){
        super(props);
        this.state = {
            opensoul: false,
            soulCount: 0,
            soulCost: 0,
        }
    }
    render() {
        if(this.state.opensoul){ return (<Redirect to={process.env.REACT_APP_EVENT_PATH + '/open-soul'} />); };

        return (
            <F11Layout showUserName={true} showCharacterName={false}>
                <Menu page='soul'/>
                <BuySoulContent>
                    <Frame title="ซื้อโซล">
                        <div className="set">
                            <div className="set__name">ซื้อ 1 โซล</div>
                            <div className="set__soul set__soul--single">
                                <SoulBall break={false} itemImg="" click={()=>{}}/>
                            </div>
                            <a className={"set__btn"+(true ? "":" lock")} onClick={()=>this.actBuySoul(1,2500)}>2,500</a>
                        </div>
                        <div className="set">
                            <div className="set__name">ซื้อ 10 + 1 โซล</div>
                            <div className="set__soul set__soul--group">
                                <SoulBall break={false} itemImg="" click={()=>{}}/>
                                <SoulBall break={false} itemImg="" click={()=>{}}/>
                                <SoulBall break={false} itemImg="" click={()=>{}}/>
                                <SoulBall break={false} itemImg="" click={()=>{}}/>
                                <SoulBall break={false} itemImg="" click={()=>{}}/>
                                <SoulBall break={false} itemImg="" click={()=>{}}/>
                            </div>
                            <a className={"set__btn"+(true ? "":" lock")} onClick={()=>this.actBuySoul(11,25000)}>25,000</a>
                        </div>
                        {/* <img className="char_2" src={Imglist['buy_soul_char']} /> */}
                    </Frame>
                </BuySoulContent>
                <ModalConfirm
                    actConfirm={()=>this.actConfirmBuySoul()}
                />

            </F11Layout>
        )
    }
    //==========================================================================
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(BuySoul);

const BuySoulContent = styled.div`
    background: top center no-repeat url(${Imglist['other_bg']}) #11122d;
    width: 1105px;
    padding: 150px 0 26px;
    text-align: center;
    .char_2 {
        position: absolute;
        top: 20%;
        right: -1%;
    }
    .set {
        margin: 40px 50px;
        display: inline-block;
        width: 165px;
        height: 270px;
        text-align: center;
        color: #ffffff;
        font-weight: bolder;
        &__name {
            display: block;
            line-height: 1.8em;
            font-size: 1.4em;
        }
        &__soul {
            position: relative;
            display: block
            width: 131px;
            height: 138px;
            margin: 15px auto;
            background: center no-repeat url(${Imglist['buy_soul_border']});
            .soulanimation {
                position: absolute;
                // opacity: 0.5;
            }
            &--single {
                .soulanimation {
                    transform: translate3d(-50%, 25px, 0px) scale(1.5);
                }
            }
            &--group {
                .soulanimation {
                    &:nth-child(1) {
                        top: 7px;
                        left: 30px;
                        transform: scale(0.8);
                    }
                    &:nth-child(2) {
                        top: 17px;
                        left: 27px;
                        transform: scale(0.6);
                    }
                    &:nth-child(3) {
                        top: 19px;
                        left: 5px;
                        transform: scale(0.6);
                    }
                    &:nth-child(4) {
                        top: 19px;
                        left: 50px;
                        transform: scale(0.6);
                    }
                    &:nth-child(5) {
                        top: 37px;
                        left: 0px;
                        transform: scale(0.8);
                    }
                    &:nth-child(6) {
                        top: 40px;
                        left: 40px;
                        transform: scale(0.8);
                    }
                }
            }
        }
        &__btn {
            font-size: 1.2em;
            display: block
            width: 160px;
            height: 40px;
            line-height:30px;
            text-align: right;
            padding-right: 32px;
            box-sizing: border-box;
            background: top center no-repeat url(${Imglist['btn_buy_soul']});
            filter: grayscale(0.2);
            cursor: pointer;
            &:hover {
                filter: grayscale(0);
                background-position: bottom center;
            }
            &:after {
                content: "";
                display: inline-block;
                width: 30px;
                height: 33px;
                vertical-align: bottom;
                background: center no-repeat url(${Imglist['diamond']});
            }
            &.lock {
                pointer-events: none;
                filter: grayscale(1);
            }
        }
    }
`;
