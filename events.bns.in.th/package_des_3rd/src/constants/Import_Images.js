import content1					from '../static/images/content1.jpg'
import content2					from '../static/images/content2.jpg'
import content3					from '../static/images/content3.jpg'

import btn_buy					from '../static/images/btn_buy.png'
import frame					from '../static/images/frame.png'
import btn_next					from '../static/images/btn_next.png'
import btn_prev					from '../static/images/btn_prev.png'
import bg_modal					from '../static/images/bg_modal.png'
import btn_default				from '../static/images/btn_default.png'
import iconScroll 				from '../static/images/ico_scroll.png'


export const Imglist = {
	content1,
	content2,
	content3,
	iconScroll,		
	btn_buy,
	frame,
	btn_next,
	btn_prev,
	bg_modal,
	btn_default
}