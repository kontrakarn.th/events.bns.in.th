import React from "react";
import { connect } from "react-redux";
import styled  from 'styled-components';
import ModalCore from "./ModalCore.js";
// import './styles.scss';
import { setValues, actCloseModal } from "./redux";
import { apiPost } from './../../middlewares/Api';
import {Imglist} from './../../constants/Import_Images';


const ConfirmModal = (props) => {
  let {modal_message} = props;
    // console.log('Bello');
    /* const apiBuy = ()=>{
        props.setValues({
            modal_open: "loading",
        });
        let dataSend = {type:'buy', package_id: props.package_id};
        let successCallback = (data) => {
            if (data.status) {
                props.setValues({
                    ...data.data,
                    modal_message: "ซื้อสำเร็จ<br/><br/>กรุณาตรวจสอบจดหมายภายในเกม",
                    modal_open: "message",
                });
            }else{
                props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_message: data.message,
                    modal_open: "message_nochar",
                });
            }else {
                props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                });
            }
        };
        apiPost("REACT_APP_API_POST_BUY", props.jwtToken, dataSend, successCallback, failCallback);
    } */

	return (
    <ModalCore name="confirm" outSideClick={false}>
        <ModalMessageContent>
            <div className="contenttitle">ยืนยัน</div>
            <div className="contenttext" dangerouslySetInnerHTML={{__html: props.modal_message}} />

            <div>
                <Btns onClick={()=>props.actConfirm()}>
                    ตกลง
                </Btns>
                <Btns onClick={() => props.setValues({ modal_open: '',modal_message:'',package_id:0 })}>
                    ยกเลิก
                </Btns>
            </div>
        </ModalMessageContent>
    </ModalCore>
	)
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal,...state.Main});


const mapDispatchToProps = { setValues, actCloseModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmModal);


const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 440px;
    height: 312px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist['bg_modal']});
    box-sizing: border-box;
    padding: 9% 3%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttitle{
        font-size: 21px;
        color: #000;
        font-family: 'Kanit-Regular', tahoma;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 21px;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 155px;
        //height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;

const Btns = styled.div`
    background: url(${Imglist['btn_default']}) no-repeat top;
    width: 95px;
    height: 31px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #000;
    line-height: 31px;
    font-size: 20px;
    background-size: cover;
    &:hover{
        background-position: bottom;
    }
`
