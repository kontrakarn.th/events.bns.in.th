import React from 'react';
import  ModalCore from './ModalCore.js';
import { connect } from "react-redux";
import styled  from 'styled-components';
import {setValues , actCloseModal} from './redux';
import {Imglist} from './../../constants/Import_Images';

const LoadingModal = (props) => {
	return (
		<ModalCore name="loading" outSideClick={false}>
            <div className="page__loading">
            กำลังดำเนินการ...
        </div> 
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoadingModal);