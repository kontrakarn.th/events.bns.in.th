import React from 'react';
import  ModalCore from './ModalCore.js';
import { connect } from "react-redux";
import './styles.scss';
import {setValues , actCloseModal} from './redux';
import {Imglist} from './../../constants/Import_Images';
import styled  from 'styled-components';

const MessageModal = (props) => {
	return (
		<ModalCore name="message" outSideClick={false}>
			<ModalMessageContent>
				<div className="contenttitle">แจ้งเตือน</div>
				<div className="contenttext" dangerouslySetInnerHTML={{__html: props.modal_message}} />

				<div>
					<Btns onClick={() => props.setValues({ modal_open: '' })}>ตกลง</Btns>
				</div>
			</ModalMessageContent>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal,...state.Main
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageModal);


const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 440px;
    height: 312px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist['bg_modal']});
    box-sizing: border-box;
    padding: 9% 3%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttitle{
        font-size: 21px;
        color: #000;
        font-family: 'Kanit-Regular', tahoma;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 21px;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 155px;
        //height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;

const Btns = styled.div`
    background: url(${Imglist['btn_default']}) no-repeat top;
    width: 95px;
    height: 31px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #000;
    line-height: 31px;
    font-size: 20px;
    background-size: cover;
    &:hover{
        background-position: bottom;
    }
`