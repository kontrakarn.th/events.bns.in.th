import MessageModal 		from './MessageModal';
import LoadingModal 		from './LoadingModal';
import ConfirmModal 		from './ConfirmModal';
import ConfirmReceiveModal  from './ConfirmReceiveModal';
import PurchasedInfoModal from './PurchasedInfoModal';
export {
	MessageModal,
	LoadingModal,
	ConfirmModal,
	ConfirmReceiveModal,
	PurchasedInfoModal
}
