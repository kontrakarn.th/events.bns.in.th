const initialState = {
  modal_open: "",
  qrcode: "",
  username: "",
  modal_message: "",
  modal_message_color: "#000000",
  select_reward: "",
  select_item: { name: "", img: "", price: "" },
  mission_item: "",
  item_status: "",
  quest_select: ""
};

export const ModalReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return {
          ...state,
          [action.key]: action.value
        };
      } else {
        return state;
      }
    case "SET_VALUES":
      return { ...state, ...action.value };
    default:
      return state;
    case "CLOSE":
      return Object.assign({}, state, {
        modal_open: "",
        select_item: ""
      });
    case "QRCODE":
      return Object.assign({}, state, {
        qrcode: ""
      });
    case "USERNAME":
      return Object.assign({}, state, {
        username: ""
      });
    case "CONFIRM":
      return Object.assign({}, state, {
        modal_open: ""
      });
    case "SELECT_ITEM":
      return Object.assign({}, state, {
        select_item: action.value
      });
    case "PURCHASE_INFO":
      return Object.assign({}, state, {
        modal_open: ""
      });
  }
};

export const setValue = (key, value) => {
  return {
    type: "SET_VALUE",
    value,
    key
  };
};

export const setValues = value => {
  // console.log(value)
  return {
    type: "SET_VALUES",
    value
  };
};

export const actCloseModal = () => {
  return {
    type: "CLOSE"
  };
};
export const actQrcode = () => {
  return {
    type: "QRCODE"
  };
};
export const actUsername = () => {
  return {
    type: "USERNAME"
  };
};

export const actConfirmModal = () => {
  return {
    type: "CONFIRM"
  };
};

export const actSelectItem = value => {
  return {
    type: "SELECT_ITEM",
    value
  };
};

export const actPurchaseInfo = value => {
  return {
    type: "PURCHASE_INFO",
    value
  };
};
