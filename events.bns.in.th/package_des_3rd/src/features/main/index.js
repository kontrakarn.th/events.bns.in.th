import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import Slider from "react-slick";
import styled from 'styled-components';
import { apiPost } from './../../middlewares/Api';
// import { onAccountLogout } from './../../actions/AccountActions';

import { setValues, onAccountLogout } from './../../store/redux';
import { Imglist } from '../../constants/Import_Images';

import { ConfirmModal, MessageModal, LoadingModal, ConfirmReceiveModal } from "../../features/modal";

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className="btn_next"
        style={{ ...style, display: "block"}}
        onClick={onClick}>
        <img src={Imglist.btn_next}/>
      </div>
    );
}
function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className="btn_prev"
        style={{ ...style, display: "block"}}
        onClick={onClick}>
        <img src={Imglist.btn_prev}/>
      </div>
    );
}

class Main extends Component {
    constructor(props) {
        super(props)
        this.state = {
            
        }

        this.props.setValues({ modal_open: '',modal_message:'',package_id:0 })
    }

    ////////////   API   ////////////

    apiEventInfo(){
        this.props.setValues({
            modal_open: "loading",
        });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.data,
                    modal_open: "",
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_message: data.message,
                    modal_open: "message_nochar",
                });

            }else if(data.type=='no_game_info'){
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message_nochar",
                });

            }else {
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiAcceptPurchase(packageKey){
        this.props.setValues({
            modal_open: "loading",
        });
        let dataSend = {type:'purchase_package', package_key: packageKey};
        let successCallback = (data) => {
            if (data.status) {
                // console.log('data.content:',data.content);
                this.props.setValues({
                    ...data.data,
                    modal_message: data.message,
                    modal_open: "receive",
                });
            }else{
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_message: data.message,
                    modal_open: "message_nochar",
                });
            }else {
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: "message",
                });
            }
        };
        apiPost("REACT_APP_API_POST_PURCHASE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ////////////   API   ////////////

    componentDidMount() {
        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 800);
    }

    componentDidUpdate(prevProps, prevState) {

        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            setTimeout(() => {
                this.apiEventInfo()
            }, 800);
        }
    }

    handleConfirm(packageKey=0,packageName='',packagePrice='') {

        if(packageKey == 1 ||  packageKey == 2){
            this.props.setValues({
                package_key: packageKey,
                modal_open: "confirm",
                modal_message: 'ซื้อ '+packageName+'<br />ราคา '+packagePrice+' ไดมอนด์?',
            });
        }

    }
    handleBuy(){
        this.apiAcceptPurchase(this.props.package_key);
    }

    onLogout() {
        this.props.onAccountLogout();
    }

    render() {
        // let can_buy = this.props.can_buy ? this.props.can_buy : []
        // console.log("can_buy:",can_buy)
        const settings = {
            fade: true,
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        return (
            <div className="events-bns">
                <div className="preorder">
                    {/* <div className="preorder__btngroup">
                        <div className="username">xxx xxx</div>
                        <div className="logout">Logout</div>
                    </div> */}
                    <Wrapper>
                        <div className="container">
                            {/* <div className="preorder__scroll">
                                <img src={Imglist.iconScroll} alt=""/>
                            </div> */}
                            <div className="block block1">
                                <img src={Imglist.content1}/>
                            </div>
                            <div className="block block2">
                                <img src={Imglist.content2}/>
                                <div className="bottom">
                                    {/*<div className="btn_buy" onClick={()=>this.props.setValues({modal_open:'confirm', modal_message: 'ต้องการซื้อแพ็คเกจ?'})}></div>*/}
                                    {
                                        this.props.jwtToken && this.props.jwtToken !== "" && this.props.package ?
                                            (
                                                this.props.package.can_purchase == true && this.props.package.already_purchase == false 
                                                ?
                                                    <div className="btn_buy"  onClick={()=>this.handleConfirm(1,this.props.package.name,this.props.package.price)}></div>
                                                :
                                                    this.props.package.already_purchase == true
                                                    ?
                                                        <div className="btn_buy disabled"></div>
                                                    :
                                                        <div className="btn_buy disabled cantbuy"></div>
                                            )
                                        :
                                            <div className="btn_buy disabled cantbuy"></div>
                                    }
                                </div>
                            </div> {/* end block2 */}

                      
                            <div className="block block3">
                                <img src={Imglist.content3}/>
                            </div>

                            {/* <Block5 className="block block5">
                                <div>
                                    <img src={Imglist.preview_costume_header}/>
                                    <h1>{this.props.test}</h1>
                                </div>
                                <div className="preview_costume">
                                    <Slider {...settings}>
                                    {
                                        this.props.preview_costume.map((item,key) => {
                                            return(
                                                <div key={key}>
                                                    <div className="preview_costume--title">- {item.title} -</div>
                                                    <img src={item.image}/>
                                                </div>
                                            )
                                        })
                                    }
                                    </Slider>
                                </div>
                            </Block5>   */}
                            
                        </div>
                   </Wrapper>
                </div>
                <LoadingModal />
                <ConfirmModal actConfirm={this.handleBuy.bind(this)}/>
                <ConfirmReceiveModal/>
                <MessageModal />
                {/* <PurchasedInfoModal
                    msg={this.props.purchased_message}
                /> */}
            </div>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal,...state.Main});

const mapDispatchToProps = {
    onAccountLogout,
    setValues
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main)


const Wrapper = styled.div`
    .container{
        max-width: 875px;
        margin: 0 auto;
        padding: 0 15px;
        position: relative;
    }
    .block{
        position: relative;
        margin-bottom: 10%;
        & .bottom{
            position: absolute;
            bottom: -20px;
            left:0;
            right: 0;
            text-align: center;
        }
    }
    .btn_buy{
        background: url(${Imglist['btn_buy']}) top center no-repeat;
        width: 190px;
        height: 58px;
        cursor: pointer;
        display: inline-block;
        &:hover{
            background-position: center;
        }
        &.disabled{
            background-position: bottom center;
            pointer-events: none;
            cursor: default;
            &.cantbuy{
                background-position: top center;
                filter: grayscale(1);
            }
        }
    }
`
const Block5 = styled.div`
    text-align: center;
    .preview_costume{
        background: url(${Imglist['frame']}) top center / 100% 100% no-repeat;
        padding: 40px 20px 20px;
        &--title{
            color: #bababa;
            font-family: 'Kanit-Regular';
            font-size: 18px;
        }
    }
    .btn_prev,
    .btn_next{
        top: 50%;
        width: 19px;
        height: 36px;
        -webkit-transform: translate(0,-50%);
        -ms-transform: translate(0,-50%);
        transform: translate(0,-50%);
        cursor: pointer;
        color: transparent;
        border: none;
        outline: 0;
        position: absolute;
        z-index: 5;
        :hover{
            opacity: .7;
        }
    }
    .btn_prev{
        left: 0;
    }
    .btn_next{
        right: 0;
    }
`
