import React from "react";
import { connect } from "react-redux";
import ModalCore from "./ModalCore.js";
import './styles.scss';
import { setValues, actCloseModal } from "./redux";
import { apiPost } from './../../middlewares/Api';

const ConfirmModal = (props) => {
  let {modal_message} = props;

  const apiBuy = ()=>{
      props.setValues({
          modal_open: "loading",
      });
      let dataSend = {type:'buy', package_id: props.package_id};
      let successCallback = (data) => {
          if (data.status) {
              props.setValues({
                  ...data.data,
                  modal_message: "ซื้อสำเร็จ<br/><br/>กรุณาตรวจสอบจดหมายภายในเกม",
                  modal_open: "message",
              });
          }else{
              props.setValues({
                  modal_message: data.message,
                  modal_open: "message",
              });
          }
      };
      const failCallback = (data) => {
          if (data.type == 'no_permission') {
              props.setValues({
                  permission: false,
                  modal_message: data.message,
                  modal_open: "message_nochar",
              });
          }else {
              props.setValues({
                  modal_message: data.message,
                  modal_open: "message",
              });
          }
      };
      apiPost("REACT_APP_API_POST_BUY", props.jwtToken, dataSend, successCallback, failCallback);
  }

	return (
    <ModalCore name="confirm" outSideClick={false}>
			<div className="contentwrap">
				<div className="text text--head">ยืนยัน</div>
				<div className="inner" dangerouslySetInnerHTML={{ __html: props.modal_message }}></div>
				<div className="modal_btn">
					<div className="btn_ok" onClick={()=>apiBuy()}></div>
					<div className="btn_cancel" onClick={() => props.setValues({ modal_open: '',modal_message:'',package_id:0 })}></div>
				</div>
			</div>
    </ModalCore>
	)
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal,...state.Main});


const mapDispatchToProps = { setValues, actCloseModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmModal);
