import React from 'react';
import  ModalCore from './ModalCore.js';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';

const PurchasedInfoModal = (props) => {
	return (
		<ModalCore name="purchase_info" outSideClick={false}>
			<div className="contentwrap">
				<div className="text text--head">รายละเอียดสั่งซื้อ</div>
				<div className="inner" dangerouslySetInnerHTML={{ __html: props.msg }}></div>
				<div className="modal_btn">
					<div className="btn_ok" onClick={() => props.setValues({ modal_open: '' })}></div>
				</div>
			</div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PurchasedInfoModal);
