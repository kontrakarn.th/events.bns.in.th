import detail					from '../static/images/detail.png'
import block2					from '../static/images/block2.png'
import block3					from '../static/images/block3.png'
import block4					from '../static/images/block4.png'
import btn_buy					from '../static/images/btn_buy.png'
import preview_costume1			from '../static/images/preview_costume1.png'
import preview_costume2			from '../static/images/preview_costume2.png'
import preview_costume3			from '../static/images/preview_costume3.png'
import preview_costume_header	from '../static/images/preview_costume_header.png'
import frame					from '../static/images/frame.png'
import btn_next					from '../static/images/btn_next.png'
import btn_prev					from '../static/images/btn_prev.png'

const iconScroll 	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/f11/icons/icon_scroll.png'
const test_items 	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/f11/test_items.png'
const btn_zoom		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/f11/btn_zoom.png'
const character	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/f11/character.png'
const close		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/preorder_3rdspec_package/f11/close.png'

export const Imglist = {
	iconScroll,
	test_items,
	btn_zoom,
	character,
	close,
	detail,		
	block2,
	block3,
	block4,
	btn_buy,
	preview_costume1,
	preview_costume2,
	preview_costume3,
	preview_costume_header,
	frame,
	btn_next,
	btn_prev
}