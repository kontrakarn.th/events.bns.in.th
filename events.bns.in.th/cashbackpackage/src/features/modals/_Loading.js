import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';

const ModalLoading = props => {
  const name = "loading";
  const onClick = ()=>{};
  return (
    <ModalCore
        open={props.modal_open === name}
        onClick={()=>onClick()}
    >
      <ModalcontentStyle>
        <h3>กำลังดำเนินการ...</h3>
      </ModalcontentStyle>
    </ModalCore>
  )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalLoading);

const ModalcontentStyle = styled.div`
    text-align: center;
    color: #FFFFFF;
`;
