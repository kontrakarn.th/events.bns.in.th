import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import imgList from "../../constants/ImportImages";
import {apiPost} from '../../constants/Api';

const ModalConfirm = props => {
    const name = "confirm";
    const apiBuy = () => {
        props.setValues({
            modal_open:"loading"
        })
        let dataSend = {type: "unlock_passport"};
        let successCallback = (res) => {
            if(res.status) {
                props.setValues({
                    ...res.data,
                    modal_open:"reward",
                    modal_bonus: true,
                    modal_message: res.message,
                    load_reward: true
                })
            } 
            else {
                props.setValues({
                    modal_open:"message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }
            else {
                props.setValues({
                    modal_open:"message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_UNLOCK_PASSPORT", props.jwtToken, dataSend, successCallback, failCallback);
    }
    return (
        <ModalCore
            open={props.modal_open === name}
            onClick={()=>props.setValues({modal_open:''})}
        >
            <ModalcontentStyle>
                <div className="message">
                    <div>
                        ต้องการซื้อ<br/>
                        <span>แพ็คเกจกำราบมังกร</span><br/>
                        150,000 ไดมอนด์
                    </div>
                </div>
                <div className="buttons">
                    <a className="btn btn--confirm"onClick={()=>apiBuy()} />
                    <a className="btn btn--cancel"onClick={()=>props.setValues({modal_open:''})} />
                </div>
            </ModalcontentStyle>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalConfirm);

const ModalcontentStyle = styled.div`
    position: relative;
    display: block;
    width: 414px;
    height: 318px;
    padding: 105px 10px 0px;
    background-image: url(${imgList.bg_confirm});
    .message {
        color: #FFFFFF;
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 55%;
        text-align: center;
        font-family: "Kanit-Light", Tahoma;
        >div{
            color: #301616;
            line-height: 1.2;
            >span{
                color: #038800;
            }
        }
    }
    .buttons {
        position: absolute;
        left: 0px;
        bottom: 38px;
        display: block;
        width: 100%;
        text-align: center;
    }
    .btn {
        display: inline-block;
        width: 96px;
        height: 38px;
        margin: 0px 20px;
        background-position: top center;
        cursor: pointer;
        &--confirm {
            background-image: url(${imgList.btn_confirm});
        }
        &--cancel {
            background-image: url(${imgList.btn_cancel});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;
