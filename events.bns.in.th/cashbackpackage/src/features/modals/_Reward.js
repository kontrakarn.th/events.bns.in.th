import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import imgList from "../../constants/ImportImages";
import {apiPost} from '../../constants/Api';

const ModalReward = props => {
  const name = "reward";

    return (
        <ModalCore
            open={props.modal_open === name}
            onClick={()=>props.setValues({modal_open:''})}
        >
            <ModalcontentStyle>
                <div className="message">
                    {props.modal_bonus 
                    ?
                        <div>
                            ได้รับ <span>{props.modal_message}</span> x1<br/>
                            และ <span>กล่องคืนกำไรจากมังกร</span> x1<br/>
                            ไอเทมจะถูกส่งเข้ากล่องจดหมาย<br/>
                            กรุณาเข้าเกมเพื่อกดรับ
                        </div>
                    :
                        <div>
                            ได้รับ <span>{props.modal_message}</span> x1<br/>
                            ไอเทมจะถูกส่งเข้ากล่องจดหมาย<br/>
                            กรุณาเข้าเกมเพื่อกดรับ
                        </div>
                    }
                </div>
                <div className="buttons">
                    <a className="btn btn--confirm" onClick={()=>props.setValues({modal_open:''})} />
                </div>
            </ModalcontentStyle>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalReward);

const ModalcontentStyle = styled.div`
    position: relative;
    display: block;
    width: 414px;
    height: 319px;
    padding: 105px 10px 0px;
    background-image: url(${imgList.bg_message});
    .message {
        color: #FFFFFF;
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 55%;
        text-align: center;
        font-family: "Kanit-Light", Tahoma;
        >div{
            color: #301616;
            line-height: 1.2;
            >span{
                color: #038800;
            }
        }
    }
    .buttons {
        position: absolute;
        left: 0px;
        bottom: 38px;
        display: block;
        width: 100%;
        text-align: center;
    }
    .btn {
        display: inline-block;
        width: 96px;
        height: 38px;
        margin: 0px 20px;
        background-position: top center;
        cursor: pointer;
        &--confirm {
            background-image: url(${imgList.btn_confirm});
        }
        &--cancel {
            background-image: url(${imgList.btn_cancel});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;
