import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';

import OauthLogin from '../features/oauthLogin';
import Home from '../pages/Home';
import Promotion from '../pages/Promotion';
import RewardHistory from '../pages/RewardHistory';
import ResentHistory from '../pages/ResentHistory';

export default (props)=>{
  let eventPath = process.env.REACT_APP_EVENT_PATH;
  return (
    <BrowserRouter>
      <>
        <Route path={eventPath} component={OauthLogin}/>
        <Route path={eventPath} exact component={Home}/>
        <Route path={eventPath+"/promotion"} exact component={Promotion}/>
        <Route path={eventPath+"/reward"} exact component={RewardHistory}/>
        <Route path={eventPath+"/resent"} exact component={ResentHistory}/>
      </>
    </BrowserRouter>
  )
}
