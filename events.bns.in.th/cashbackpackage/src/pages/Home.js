import React,{useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {setValues} from './../store/redux';
import styled from 'styled-components';
import imgList from './../constants/ImportImages';
import F11Layout from './../features/f11Layout';
import Modals from './../features/modals';
import {apiPost} from './../constants/Api';

const Home = (props) => {
  	const apiEventInfo = () => {
		let dataSend = {type: "event_info"};
		let successCallback = (res) => {
			if(res.status) {
				props.setValues({
					...res.data,
					modal_open:""
				})
			} 
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		let failCallback = (res) => {
			if (res.type === 'no_permission') {
				props.setValues({
					permission: false,
					modal_open: "",
				});
			}
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
    	apiPost("REACT_APP_API_POST_EVENT_INFO", props.jwtToken, dataSend, successCallback, failCallback);
	}
	useEffect(()=>{
		if(props.jwtToken !== "" && !props.status) apiEventInfo();
	},[props.jwtToken])
	return (
		<F11Layout page={"main"}>
			<Modals />
			<HomeStyle>
				<section className="banner">
					<Link className="banner__btn" to={props.eventPath("/promotion")} />
				</section>
				<section className="detail">
					<img className="detail__board detail__board--1" src={imgList.content_1_home} alt=''/>
				</section>
				<section className="detail">
					<img className="detail__board detail__board--2" src={imgList.content_2_home} alt=''/>
				</section>
				<section className="detail">
					<img className="detail__board detail__board--3" src={imgList.content_3_home} alt=''/>
				</section>
				<section className="detail">
					<img className="detail__board detail__board--4" src={imgList.content_4_home} alt=''/>
				</section>
			</HomeStyle>
		</F11Layout>
	)
}
const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(Home);

const HomeStyle = styled.div`
	position: relative;
	display: block;
	width: 1105px;
	height: 3275px;
	background-image: url(${imgList.bg_home});
  	.banner {
		position: relative;
		display: block;
		height: 700px;
		&__btn {
			position: absolute;
			top: 410px;
    		left: 228px;
			display: block;
			width: 306px;
			height: 53px;
			background-image: url(${imgList.btn_join});
			background-position: top center;
			&:hover {
				background-position: bottom center;
			}
		}
  	}
	.detail {
		position: relative;
		display: block;
		&__board {
			display: block;
			&--1{
				width: 895px;
				height: 506px;
				margin: 39px auto 0px;
			}
			&--2{
				width: 981px;
				height: 603px;
				margin: 38px 0 0 45px;
			}
			&--3{
				width: 660px;
				height: 623px;
				margin: 100px auto 0;
			}
			&--4{
				width: 591px;
				height: 443px;
				margin: 54px 0 0 262px;
			}
		}
	}
`;
