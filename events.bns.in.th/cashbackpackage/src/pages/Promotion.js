import React,{useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {setValues} from '../store/redux';
import styled from 'styled-components';
import imgList from '../constants/ImportImages';
import F11Layout from '../features/f11Layout';
import Modals from './../features/modals';
import {apiPost} from './../constants/Api';

const Promotion = (props) => {

    const apiEventInfo = () => {
        let dataSend = {type: "event_info"};
        let successCallback = (res) => {
            if(res.status) {
                props.setValues({
                    ...res.data,
                    modal_open:"",
                })
            } 
            else {
                props.setValues({
                    modal_open:"message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }
            else {
                props.setValues({
                    modal_open:"message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_EVENT_INFO", props.jwtToken, dataSend, successCallback, failCallback);
    }
    const apiCheckin = (day) => {
        props.setValues({
            modal_open:"loading"
        })
        let dataSend = {type: "checkin",day: day};
        let successCallback = (res) => {
            if(res.status) {
                props.setValues({
                    ...res.data,
                    modal_open:"reward",
                    modal_bonus: false,
                    modal_message: res.message,
                    load_reward: true
                })
            } 
            else {
                props.setValues({
                    modal_open:"message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }
            else {
                props.setValues({
                    modal_open:"message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_CHECKIN", props.jwtToken, dataSend, successCallback, failCallback);
    }
    const apiClaimFinal = () => {
        props.setValues({
            modal_open:"loading"
        })
        let dataSend = {type: "claim_final_reward"};
        let successCallback = (res) => {
            if(res.status) {
                props.setValues({
                    ...res.data,
                    modal_open:"reward",
                    modal_bonus: false,
                    modal_message: res.message,
                    load_reward: true
                })
            } 
            else {
                props.setValues({
                    modal_open:"message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }
            else {
                props.setValues({
                    modal_open:"message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_CLAIM_FINAL", props.jwtToken, dataSend, successCallback, failCallback);
    }
    const actConfirm = () => {
        props.setValues({ modal_open: "confirm" });
    }
    useEffect(()=>{
        if(props.jwtToken !== "" && !props.status) apiEventInfo();
    },[props.jwtToken])
    return (
        <F11Layout page={"promotion"}>
            <PromotionStyle className="promotion">
                <section className="promotion__content promotion__content--1">
                    <img className="promotion__content-img" src={imgList.content_1_promotion} alt=''/>
                    <div className="promotion__package">
                        <img src={imgList.reward_package} alt=''/>
                        <div className="promotion__faq-package promotion__faq-package--1">
                            <img src={imgList.package_1} alt=''/>
                        </div>
                        <div className="promotion__faq-package promotion__faq-package--2">
                            <img src={imgList.package_2} alt=''/>
                        </div>
                    </div>
                    {props.unlock_package.can_buy
                    ?
                        props.unlock_package.already_buy
                        ?
                            <div className="promotion__btn-buy promotion__btn-buy--buyed"/>
                        :
                            <div className="promotion__btn-buy promotion__btn-buy--buy" onClick={()=>actConfirm()}/>
                    :
                        <div className="promotion__btn-buy promotion__btn-buy--buydiable"/>
                    }
                </section>
                <section className="promotion__content promotion__content--2">
                    <img className="promotion__content-img" src={imgList.content_2_promotion} alt=''/>
                    <div className="promotion__row">
                        {props.checkin_list && props.checkin_list.length>0 
                        ?
                            props.checkin_list.map((item, key) => {
                                return (
                                    <div className={"promotion__row-item promotion__row-item--" + (key+1)} key={"api_check_in"+key}>
                                        <div className="promotion__img">
                                            {item.is_checkin
                                            ?
                                                <img src={imgList.item_normal} alt=''/>
                                            :
                                                item.can_checkin
                                                ?
                                                    <img src={imgList.item_normal} alt=''/>
                                                :
                                                    <img src={imgList.item_normal_disable} alt=''/>
                                            }
                                            <div className={"promotion__num promotion__num--"+(key+1)}>
                                                {item.day}
                                            </div>
                                            <div className={"promotion__faq-normal promotion__faq-normal--" + (key+1)}>
                                                <img src={imgList.normal} alt=''/>
                                            </div>
                                        </div>
                                        {item.is_checkin
                                        ?
                                            <div className="promotion__btn-claim promotion__btn-claim--claimed"/>
                                        :
                                            item.can_checkin
                                            ?
                                                <div className="promotion__btn-claim promotion__btn-claim--claim" onClick={()=>apiCheckin(item.day)}/>
                                            :
                                                <div className="promotion__btn-claim promotion__btn-claim--claimdiable"/>
                                        }
                                    </div>
                                );
                            })
                        :
                            new Array(14).fill(0).map((item, key) => {
                                return (
                                    <div className={"promotion__row-item promotion__row-item--" + (key+1)} key={"check_in"+key}>
                                        <div className="promotion__img">
                                            <img src={imgList.item_normal_disable} alt=''/>
                                            <div className={"promotion__num promotion__num--"+(key+1)}>
                                                {key+1}
                                            </div>
                                            <div className={"promotion__faq-normal promotion__faq-normal--" + (key+1)}>
                                                <img src={imgList.normal} alt=''/>
                                            </div>
                                        </div>
                                        <div className="promotion__btn-claim promotion__btn-claim--claimdiable"/>
                                    </div>
                                );
                            })
                        }
                    </div>
                </section>
                <section className="promotion__content promotion__content--3">
                    <img className="promotion__content-img" src={imgList.content_3_promotion} alt=''/>
                    <div className="promotion__jackpot">
                        <img src={imgList.reward_jackpot} alt=''/>
                        <div className="promotion__faq-jackpot promotion__faq-jackpot--1">
                            <img src={imgList.jackpot_1} alt=''/>
                        </div>
                        <div className="promotion__faq-jackpot promotion__faq-jackpot--2">
                            <img src={imgList.jackpot_2} alt=''/>
                        </div>
                    </div>
                    {/* {props.final_reward.can_claim
                    ?
                        <div className="promotion__btn-claim-2 promotion__btn-claim-2--claim"/>
                    :
                    } */}
                    {props.final_reward.has_claimed
                    ?
                        <div className="promotion__btn-claim-2 promotion__btn-claim-2--claimed"/>
                    :
                        props.final_reward.can_claim
                        ?
                            <div className="promotion__btn-claim-2 promotion__btn-claim-2--claim" onClick={()=>apiClaimFinal()}/>
                        :
                            <div className="promotion__btn-claim-2 promotion__btn-claim-2--claimdiable"/>
                    }
                </section>
            </PromotionStyle>
            <Modals />
        </F11Layout>
    )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = {setValues};
export default connect( mapStateToProps, mapDispatchToProps )(Promotion);

const PromotionStyle = styled.div`
    position: relative;
    display: block;
    width: 1105px;
    height: 2118px;
    margin: 0px;
    background-image: url(${imgList.bg_promotion});
    padding-top: 120px;
    .promotion{
        &__content{
            position: relative;
            display: block;
            &--1{
                width: 660px;
				margin: 0 auto;
            }
            &--2{
                width: 738px;
				margin: 175px 0 0 180px;
            }
            &--3{
                width: 738px;
                margin: 50px 0 0 180px;
            }
        }
        &__content-img{
            width: 100%;
            display: block;
        }
        &__package{
            position: absolute;
            top: 25%;
            left: 50%;
            transform: translate(-50%, 0);
            width: 119px;
            >img{
                width: 100%;
                display: block;
            }
            &:hover{
                >div{
                    opacity: 1;
                }
            }
        }
        &__faq-package{
            position: absolute;
            opacity: 0;
            transition: .2s all;
            pointer-events: none;
            &--1{
                width: 302px;
                top: -140px;
                left: -408px;
            }
            &--2{
                width: 278px;
                top: -116px;
                right: -377px;
            }
            >img{
                width: 100%;
                display: block;
            }
        }
        &__btn-buy{
            position: absolute;
            top: 67%;
            left: 50%;
            transform: translate(-50%, 0);
            width: 124px;
            height: 47px;
            &--buy{
                background-image: url(${imgList.btn_buy});
                background-position: top center;
                cursor: pointer;
                &:hover {
                    background-position: bottom center;
                }
            }
            &--buyed{
                background-image: url(${imgList.btn_buyed});
                background-position: top center;
            }
            &--buydiable{
                background-image: url(${imgList.btn_buy_disable});
                background-position: top center;
            }
        }
        &__row{
            position: absolute;
            width: 750px;
            top: 152px;
            left: -4px;

            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
        }
        &__row-item{
            position: relative;
            margin: 0 22.5px;
            &--1,&--2,&--3,&--4,&--5{
                margin-bottom: 84px;
            }
            &--6,&--7,&--8,&--9,&--10{
                margin-bottom: 78px;
            }
            &:hover{
                z-index: 20;
            }
        }
        &__img{
            position: relative;
            width: 85px;
            margin: 0 auto;
            >img{
                width: 100%;
                display: block;
            }
            &:hover{
                z-index: 20;
                >div{
                    opacity: 1;
                }
            }
        }
        &__num{
            position: absolute;
            top: 5%;
            left: 51.5%;
            transform: translate(-50%, 0);
            font-size: 15px;
            font-family: "Kanit-Medium", Tahoma;
            color: #fff;
            &--10,&--11,&--12,&--13,&--14{
                top: 6.5%;
                font-size: 14px;
            }
        }
        &__btn-claim{
            width: 87px;
            height: 35px;
            margin-top: 13px;
            &--claim{
                background-image: url(${imgList.btn_claim});
                background-position: top center;
                cursor: pointer;
                &:hover {
                    background-position: bottom center;
                }
            }
            &--claimed{
                background-image: url(${imgList.btn_claimed});
                background-position: top center;
            }
            &--claimdiable{
                background-image: url(${imgList.btn_claim_diable});
                background-position: top center;
            }
        }
        &__faq-normal{
            position: absolute;
            opacity: 0;
            transition: .2s all;
            pointer-events: none;
            width: 302px;
            >img{
                width: 100%;
                display: block;
            }
            &--1,&--2,&--3,&--4,&--6,&--7,&--8,&--9,&--11,&--12,&--13{
                top: -117px;
                right: -323px;
            }
            &--5,&--10,&--14{
                top: -117px;
                left: -321px;
            }
        }
        &__jackpot{
            position: absolute;
            top: 37%;
            left: 50%;
            transform: translate(-50%, 0);
            width: 102px;
            >img{
                width: 100%;
                display: block;
            }
            &:hover{
                >div{
                    opacity: 1;
                }
            }
        }
        &__btn-claim-2{
            position: absolute;
            top: 79.2%;
            left: 50%;
            transform: translate(-50%, 0);
            width: 87px;
            height: 35px;
            &--claim{
                background-image: url(${imgList.btn_claim});
                background-position: top center;
                cursor: pointer;
                &:hover {
                    background-position: bottom center;
                }
            }
            &--claimed{
                background-image: url(${imgList.btn_claimed});
                background-position: top center;
            }
            &--claimdiable{
                background-image: url(${imgList.btn_claim_diable});
                background-position: top center;
            }
        }
        &__faq-jackpot{
            position: absolute;
            opacity: 0;
            transition: .2s all;
            pointer-events: none;
            &--1{
                width: 302px;
                top: -94px;
                left: -360px;
            }
            &--2{
                width: 302px;
                top: -47px;
                right: -365px;
            }
            >img{
                width: 100%;
                display: block;
            }
        }
    }
`;
