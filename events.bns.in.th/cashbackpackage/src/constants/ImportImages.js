const bg_home = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/bg_home.jpg';
const bg_promotion = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/bg_promotion.jpg';
const btn_join = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/btn_join.png';
const content_1_home = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/content_1_home.png';
const content_2_home = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/content_2_home.png';
const content_3_home = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/content_3_home.png';
const content_4_home = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/content_4_home.png';
const content_1_promotion = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/content_1_promotion.png';
const content_2_promotion = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/content_2_promotion.png';
const content_3_promotion = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/content_3_promotion.png';
const jackpot_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/jackpot_1.png';
const jackpot_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/jackpot_2.png';
const normal = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/normal.png';
const reward_package = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/reward_package.png';
const package_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/package_1.png';
const package_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/package_2.png';
const btn_buy = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/btn_buy.png';
const btn_buy_disable = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/btn_buy_disable.png';
const btn_buyed = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/btn_buyed.png';
const item_normal_disable = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/item_normal_disable.png';
const item_normal = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/item_normal.png';
const btn_claim = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/btn_claim.png';
const btn_claim_diable = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/btn_claim_diable.png';
const btn_claimed = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/btn_claimed.png';
const reward_jackpot = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/reward_jackpot.png';
const bg_menu = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/bg_menu.jpg';
const bg_history = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/bg_history.jpg';
const board_history = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/board_history.png';
const btn_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/btn_confirm.png';
const bg_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/bg_confirm.png';
const btn_cancel = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/btn_cancel.png';
const bg_message = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/bg_message.png';
const btn_home = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/btn_home.png';
const icon_scroll = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/icon_scroll.png';
const permission = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/permission.jpg';
const bg_resent = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/cashbackpackage/bg_resent.jpg';

export default {
	bg_home,
	bg_promotion,
	btn_join,
	content_1_home,
	content_2_home,
	content_3_home,
	content_4_home,
	content_1_promotion,
	content_2_promotion,
	jackpot_1,
	jackpot_2,
	content_3_promotion,
	normal,
	reward_package,
	package_2,
	package_1,
	btn_buy,
	btn_buy_disable,
	btn_buyed,
	item_normal_disable,
	item_normal,
	btn_claim,
	btn_claim_diable,
	btn_claimed,
	reward_jackpot,
	bg_menu,
	bg_history,
	board_history,
	btn_confirm,
	bg_confirm,
	btn_cancel,
	bg_message,
	btn_home,
	icon_scroll,
	permission,
	bg_resent
}
