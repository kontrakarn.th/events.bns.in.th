import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from "../../constants/ImportImages";

const ModalLoading = props => {
  	const name = "message";
	return (
		<ModalCore
			open={props.modal_open === name}
			onClick={()=>props.setValues({modal_open:''})}
		>
			<ModalcontentStyle>
				<div className="message">
					<div dangerouslySetInnerHTML={{__html: props.modal_message}}/>
				</div>
				<div className="buttons">
					<a className="btn btn--confirm" onClick={()=>props.setValues({modal_open:''})} />
				</div>
			</ModalcontentStyle>
		</ModalCore>
  	)
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalLoading);

const ModalcontentStyle = styled.div`
	position: relative;
	display: block;
	width: 461px;
	height: 334px;
	padding: 101px 10px 0px;
	background-image: url(${imgList.bg_message});
	.message {
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 55%;
        text-align: center;
        font-family: "Kanit-Light", Tahoma;
        >div{
            color: #ffffff;
            line-height: 1.2;
        }
    }
  	.buttons {
		position: absolute;
		left: 0px;
		bottom: 33px;
		display: block;
		width: 100%;
		text-align: center;
	}
	.btn {
		display: inline-block;
		width: 143px;
		height: 40px;
		margin: 0px 20px;
		background-position: top center;
		cursor: pointer;
		&--confirm {
			background-image: url(${imgList.btn_confirm});
		}
		&--cancel {
			background-image: url(${imgList.btn_cancel});
		}
		&:hover {
			background-position: bottom center;
		}
	}
`;
