const bg = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/bg.png';
const content_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/content_1.png';
const content_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/content_2_2.png';
const btn_buy = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/btn_buy.png';
const btn_buyed = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/btn_buyed.png';
const reward_hot_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/reward_hot_2.png';
const reward_hot_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/reward_hot_1.png';
const bg_message = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/bg_message.png';
const btn_cancel = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/btn_cancel.png';
const btn_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/btn_confirm.png';
const bg_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/bg_confirm.png';
const btn_home = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/btn_home.png';
const icon_scroll = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/icon_scroll.png';
const icon_down_arrow = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/icon_down_arrow.png';
const permission = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/permission.jpg';
const bg_menu = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/3rd_spec_assassin/bg_menu.jpg';

export default {
	bg,
	content_1,
	content_2,
	btn_buy,
	btn_buyed,
	reward_hot_2,
	reward_hot_1,
	bg_message,
	btn_cancel,
	btn_confirm,
	bg_confirm,
	btn_home,
	icon_scroll,
	icon_down_arrow,
	permission,
	bg_menu
}
