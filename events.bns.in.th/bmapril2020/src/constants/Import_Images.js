
const menu_bg 							= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/f11/menu_bg.jpg';

const btn_back 							= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/btn_back.png';
const btn_cancel 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/btn_cancel.png';
const btn_close 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/btn_close.png';
const btn_confirm 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/btn_confirm.png';
const btn_gacha_hover 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/btn_gacha_hover.png';
const btn_gacha 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/btn_gacha.png';
const btn_next 							= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/btn_next.png';
const btn_receive_hover 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/btn_receive_hover.png';
const btn_receive 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/btn_receive.png';
const btn_send_hover 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/btn_send_hover.png';
const btn_send 							= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/btn_send.png';
const gift_bg 							= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/gift_bg.jpg';
const history_bg 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/history_bg.jpg';
const home_all 							= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/home_all.jpg';
const home_banner 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/home_banner.jpg';
const home_gacha 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/home_gacha.jpg';
const home_item 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/home_item.jpg';
const home_rule 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/home_rule.jpg';
const icon 									= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/icon.png';
const item_pet 							= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/item_pet.png';
const item_set 							= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/item_set.png';
const item_pet_special 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/item_pet_special.png';
const popup_confirm 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/popup_confirm.png';
const popup_item 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/popup_item.png';
const popup_message 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/popup_message.png';
const item_weapon 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/item_weapon.png';
const item_1				 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/item_1.png';
const item_2				 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/item_2.png';
const item_3				 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/item_3.png';

const bg_permission 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/f11/permission.jpg';
const icon_scroll 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/f11/icon_scroll.png';
const btn_home 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/f11/btn_home.png';
const menu_special_btn 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/menu_special_btn.jpg';
const icon_dropdown 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/bmapril2020/images/icon_dropdown.png';

//==============================================================================================================

// modal
// const btn_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_confirm.png';
// const btn_cancel = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/btn_cancel.png';
// const popup_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/popup_confirm.png';
// const popup_message = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/popup_message.png';
// const popup_select = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/league/images/popup_select.png';

export default {
	// base Layout
	bg_permission,
	menu_bg,
	icon_scroll,
	btn_home,
	menu_special_btn,
	icon_dropdown,

	//Event
	btn_back,
	btn_cancel,
	btn_close,
	btn_confirm,
	btn_gacha_hover,
	btn_gacha,
	btn_next,
	btn_receive_hover,
	btn_receive,
	btn_send_hover,
	btn_send,
	gift_bg,
	history_bg,
	home_all,
	home_banner,
	home_gacha,
	home_item,
	home_rule,
	icon,
	item_pet,
	item_set,
	item_pet_special,
	item_weapon,
	popup_confirm,
	popup_item,
	popup_message,
	item_1,
	item_2,
	item_3,
}
