import styled from 'styled-components';
import imgList from './../../constants/Import_Images';

export default styled.div`
    position: absolute;
    top: 0px;
    left: 0px;
    display: block;
    width: 1105px;
    height: 700px;
    background: no-repeat center url(${imgList.bg_permission}) #c5c0ad /cover;
`;
