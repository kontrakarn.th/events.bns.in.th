import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from './../../constants/Import_Images';
import {apiPost} from './../../constants/Api';

const CPN = props => {
  let {item_list,item_select} = props;
  const apiSendReward = (id) => {
    if(props.jwtToken !== "") {

      props.setValues({ modal_open: "loading" });
      apiPost(
        "REACT_APP_API_POST_SEND_REWARD",
        props.jwtToken,
        {
          type: "send_rewards",
          package_key: id,
        },
        (data)=>{//function successCallback
          props.setValues({
              ...data.data,
              modal_open: "message",
              modal_message: data.message,
          });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message,
          });
        },
      );
    }
  }

  return (
    <ModalCore
        modalName="receive"
        actClickOutside={()=>props.setValues({modal_open:""})}
    >
        <ModalReceive className="mdreceive">
            <div
              className="mdreceive__text"
              dangerouslySetInnerHTML={{__html: props.modal_message}}
            />
            <div className="mdreceive__btns">
                <Btn className='confirm' onClick={()=>apiSendReward(item_list[item_select].id)} />
                <Btn className='cancel' onClick={()=>props.setValues({modal_open:''})} />
            </div>
        </ModalReceive>
    </ModalCore>
  )
}
const mstp = state => ({...state.Main});
const mdtp = { setValues };
export default connect( mstp, mdtp )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalReceive = styled.div`
  position: relative;
  display: block;
  width: 459px;
  height: 322px;
  font-size: 1em;
  color: #FFFFFF;
  background: no-repeat center url(${imgList.popup_confirm});
  padding: 65px 55px 0px;
  .mdreceive{
    &__text {
      width: 100%;
      height: 155px;
      display: flex;
      justify-content: center;
      align-items: center;
      align-items: center;
      color: #000000;
      font-size: 21px;
      line-height: 1.5em;
      word-break: break-word;
      text-align: center;
    }
    &__btns {
      position: relative;
      margin: 100px auto;
      display: flex;
      justify-content: center;
      align-items: center;
      margin: 30px auto 0px;
    }

  }
`;

const Btn = styled.div`
  width: 108px;
  height: 41px;
  display: inline-block;
  cursor: pointer;
  margin: 0 15px;
  color: #000;
  line-height: 36px;
  font-size: 20px;
  background-size: contain;
  opacity: 0.8;
  filter: grayscale(0.3);
  &.confirm{
    background: no-repeat 0 0 url(${imgList.btn_confirm});
  }
  &.cancel{
    background: no-repeat 0 0 url(${imgList.btn_cancel});
  }
  &:hover{
    filter: grayscale(0);
  }
`
