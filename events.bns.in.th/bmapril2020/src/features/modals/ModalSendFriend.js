import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from './../../constants/Import_Images';
import {apiPost} from './../../constants/Api';

const CPN = props => {
  let {item_list,item_select,friend_uid} = props;
  let package_name = item_list[item_select] ? item_list[item_select].package_name : "";

  const apiCheckUid = (package_name,friend_uid) => {
    if(props.jwtToken !== "") {
      props.setValues({ modal_open: "loading" });
      apiPost(
        "REACT_APP_API_POST_CHECK_UID",
        props.jwtToken,
        {
          type: "check_uid",
          sendto: friend_uid,
        },
        (data)=>{//function successCallback
          props.setValues({
              ...data.data,
              modal_open: "send_confirm",
              modal_message: "ท่านต้องการส่ง<br/>"+package_name+"<br/> ให้ UID:"+friend_uid+"<br /><br/>ค่าบริการ 10,000 ไดมอนด์",
          });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message,
          });
        },
      );
    }
  }

  return (
    <ModalCore
        modalName="send"
        actClickOutside={()=>props.setValues({modal_open:""})}
    >
        <ModalSendFriend className="mdsendfriend">
            <div className="mdsendfriend__text">
                <div>กรุณากรอก UID ของเพื่อน</div>
                <Input
                    id="f_uid"
                    type="text"
                    value={props.friend_uid}
                    onChange={(e)=>props.setValues({friend_uid:e.target.value})}
                />
            </div>
            <div className="mdsendfriend__btns">
                <Btn className='confirm' onClick={()=>apiCheckUid(package_name,friend_uid)} />
                <Btn className='cancel' onClick={()=>props.setValues({modal_open:''})} />
            </div>
        </ModalSendFriend>
    </ModalCore>
  )
}
const mstp = state => ({...state.Main});
const mdtp = { setValues };
export default connect( mstp, mdtp )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalSendFriend = styled.div`
  position: relative;
  display: block;
  width: 459px;
  height: 322px;
  font-size: 1em;
  color: #FFFFFF;
  background: no-repeat center url(${imgList.popup_confirm});
  padding: 65px 55px 0px;
  .mdsendfriend{
    &__text {
      flex-direction: column;
      width: 100%;
      height: 155px;
      display: flex;
      justify-content: center;
      align-items: center;
      align-items: center;
      color: #000000;
      font-size: 21px;
      line-height: 1.5em;
      word-break: break-word;
      text-align: center;
    }
    &__btns {
      position: relative;
      margin: 100px auto;
      display: flex;
      justify-content: center;
      align-items: center;
      margin: 30px auto 0px;
    }

  }
`;

const Btn = styled.div`
  width: 108px;
  height: 41px;
  display: inline-block;
  cursor: pointer;
  margin: 0 15px;
  color: #000;
  line-height: 36px;
  font-size: 20px;
  background-size: contain;
  opacity: 0.8;
  filter: grayscale(0.3);
  &.confirm{
    background: no-repeat 0 0 url(${imgList.btn_confirm});
  }
  &.cancel{
    background: no-repeat 0 0 url(${imgList.btn_cancel});
  }
  &:hover{
    filter: grayscale(0);
  }
`

const Input = styled.input`
    display: block;
    cursor: pointer;
    width: 90%;
    margin-top: 20px;
    text-align: center;
    font-size: 1em;
    border: 0;
    padding: 5px 10px;
`
