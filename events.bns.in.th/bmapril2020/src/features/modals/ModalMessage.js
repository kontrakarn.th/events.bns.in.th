import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from "../../constants/Import_Images";

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="message"
            // actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent className="mdmessage">
                <div
                  className="mdmessage__text"
                  dangerouslySetInnerHTML={{__html: modal_message}}
                />
                <div className="mdmessage__btns">
                  <Btn className='confirm' onClick={()=>props.setValues({modal_open: props.character === "" ?"selectcharacter" :""})} />
                </div>
            </ModalMessageContent>
        </ModalCore>
    )
}

const mstp = state => ({...state.Main});
const mdtp = { setValues };
export default connect( mstp, mdtp )(CPN);

const ModalMessageContent = styled.div`
  position: relative;
  display: block;
  width: 459px;
  height: 322px;
  font-size: 1em;
  color: #FFFFFF;
  background: no-repeat center url(${imgList.popup_message});
  padding: 65px 55px 0px;
  .mdmessage{
    &__text {
      width: 100%;
      height: 155px;
      display: flex;
      justify-content: center;
      align-items: center;
      text-align: center;
      color: #000000;
      font-size: 21px;
      line-height: 1.5em;
      word-break: break-word;
    }
    &__btns {
      position: relative;
      margin: 100px auto;
      display: flex;
      justify-content: center;
      align-items: center;
      margin: 30px auto 0px;
    }
  }
`;

const Btn = styled.div`
  width: 94px;
  height: 40px;
  display: inline-block;
  cursor: pointer;
  margin: 0 15px;
  color: #000;
  line-height: 36px;
  font-size: 20px;
  background-size: contain;
  opacity: 0.8;
  &.confirm{
    background: no-repeat 0 0 url(${imgList['btn_confirm']});
  }
  &.cancel{
    background: no-repeat 0 0 url(${imgList['btn_cancel']});
  }
  &:hover{
    filter: grayscale(0.9)
  }
`
