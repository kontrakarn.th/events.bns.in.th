import React,{useState,useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import styled from 'styled-components';
import imgList from './../constants/Import_Images';
import Slider from "react-slick";
import F11Layout from './../features/F11Layout';
import Btn from "./../features/Buttons";
import Pagination from './../features/Pagination';
import {apiPost} from './../constants/Api';
import {setValues} from './../store/redux';

const HomePage = props => {
  const [itemSlide,setItemSlide] = useState(null);
  const [itemPage,setItemPage] = useState(0);

  const itemSlideSettings = {
      dots: false,
      speed: 500,
      dots: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
      afterChange: (p)=>setItemPage(p),
      appendDots: dots => (
        <div>
          <ul style={{ margin: "0px" }}> {dots} </ul>
        </div>
      ),
      customPaging: i => (
        <div />
      )
  };

  const apiEventInfo = () => {
    if(props.jwtToken !== "" && !props.status) {
      props.setValues({ modal_open: "loading" });
      apiPost(
        "REACT_APP_API_POST_EVENT_INFO",
        props.jwtToken,
        {type: "event_info"},
        (data)=>{//function successCallback
          props.setValues({
              ...data.data,
              modal_open: "",
              // modal_open: "selectcharacter",
          });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message,
          });
        },
      );
    }
  }
  const actShowModalConfirm = () => {
    console.log("actShowModalConfirm");
    if(props.jwtToken && props.jwtToken !== "") {
      props.setValues({
          modal_open: "confirm",
          modal_message: "ต้องการเปิดกล่อง?",
      });

    }
  }
  const actShowModalItem = ()=>{
    props.setValues({
        modal_open: "reward",
    });
  }

  useEffect(()=>{ apiEventInfo() },[props.jwtToken]);

  return (
    <F11Layout showUserName={true} showCharacterName={true} page={""}>
      <HomeStyle className="home">
        <section className="home__banner" />
        <section className="home__rule">
          <a
            className="btn"
            onClick={()=>actShowModalItem()}
          />
        </section>
        <section className="home__gacha">
          <div className="home__gachabtn" >
            <Btn
              width="117px"
              height="49px"
              grayscale={props.can_open_gachapon ?"0":"1"}
              active={props.can_open_gachapon}
              src={imgList.btn_gacha}
              hover={imgList.btn_gacha_hover}
              onClick={()=>actShowModalConfirm()}
            />
          </div>
        </section>
        <section className="home__items">
          <div className="home__slide">
          <Slider key="pointslide" {...itemSlideSettings} ref={e=>{if(e)setItemSlide(e)}}>
            <div><img className="home__itemslot" src={imgList.item_pet} /></div>
            <div><img className="home__itemslot" src={imgList.item_weapon} /></div>
            <div><img className="home__itemslot" src={imgList.item_set} /></div>
            <div><img className="home__itemslot" src={imgList.item_pet_special} /></div>
          </Slider>
          </div>
        </section>
      </HomeStyle>
    </F11Layout>
  )
}
const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(HomePage);


const HomeStyle= styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: 3345px;
  background-image: url(${imgList.l_home});
  background-color: #000000;
  .home {
    /* l_send
    l_history
    l_home */
    &__banner {
      height: 700px;
      background-image: url(${imgList.home_banner});
    }
    &__rule {
      height: 1050px;
      background-image: url(${imgList.home_rule});
    }
    &__gacha {
      height: 600px;
      background-image: url(${imgList.home_gacha});
    }
    &__gachabtn {
      position: relative;
      top: 300px;
      display:block;
      width: 117px;
      height: 48px;
      margin: 0px auto;
    }
    &__items {
      height: 995px;
      background-image: url(${imgList.home_item});
    }
    &__itemslot {
      margin: 0px auto;
      display: block;
    }
    &__slide {
      position: relative;
      top: 145px;//32px;
      display:block;
      width: 890px;//906px;
      height: 865px;
      /* background-image: url(${imgList.home_item_frame}); */
      margin: 0px auto;
      .slick-dots {
        .slick-active {
          background-color: #e5e5e5;
        }
        li {
          background-color: #646464;
          border-radius: 50%;
          width: 10px;
          height: 10px;
        }
      }
    }
  }
  .btn {
    position: absolute;
    top: 543px;
    left: 305px;
    display: block;
    width: 33px;
    height: 33px;
  }
`;

const HideBeforeAfter = styled.div`

  display: block;
  width: 44px;
  height: 44px;
  z-index: 2;
  &.slick-next {
    right: 0px;
  }
  &.slick-prev {
    left: 0px;
  }
  &:before,&:after {
    content: "";
  }
`;
function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <HideBeforeAfter
        className={className}
        style={{ ...style, display: "block"}}
        onClick={onClick}
      >
        <img src={imgList.btn_next}/>
      </HideBeforeAfter>
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <HideBeforeAfter
          className={className}
          style={{ ...style, display: "block"}}
          onClick={onClick}
        >
          <img src={imgList.btn_back}/>
        </HideBeforeAfter>
    );
}
