import React,{useState,useEffect} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import imgList from './../constants/Import_Images';
import F11Layout from './../features/F11Layout';
import {apiPost} from './../constants/Api';
import {setValues} from './../store/redux';

const HistoryPage = props => {

  const apiHistory = () => {
    if(props.jwtToken !== "") {
      props.setValues({ modal_open: "loading" });
      apiPost(
        "REACT_APP_API_POST_HISTORY",
        props.jwtToken,
        {type: "history"},
        (data)=>{//function successCallback
          props.setValues({
            username:data.data.username,
            history:data.data.item_list,
            modal_open: "",
              // ...data.data,
              // modal_open: "selectcharacter",
          });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message,
          });
        },
      );
    }
  }

  useEffect(()=>{ apiHistory() },[props.jwtToken]);

  return (
    <F11Layout>
      <HistoryStyle>
        <ul className="list">
          {props.history.map((item,index)=>{
            return (
              <li className="list__slot">
                <div className="list__text">{item.title}</div>
                <div className="list__date">{item.datetime}</div>
              </li>
            )
          })}
        </ul>
      </HistoryStyle>
    </F11Layout>
  )
}

const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(HistoryPage);

const HistoryStyle= styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: 700px;
  background-image: url(${imgList.history_bg});

  .list {
    position: relative;
    top: 230px;
    display: block;
    width: 890px;
    height: 400px;
    margin: 0px auto;
    /* padding: 30px 80px; */
    padding: 0px 80px;
    overflow-y: auto;
    &__slot {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
    &__text {
      width: 500px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    &__date {

    }
  }
`;
