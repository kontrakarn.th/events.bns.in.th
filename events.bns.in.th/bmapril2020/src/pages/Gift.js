import React,{useState,useEffect} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import imgList from './../constants/Import_Images';
import F11Layout from './../features/F11Layout';
import {apiPost} from './../constants/Api';
import {setValues} from './../store/redux';
import Btn from "./../features/Buttons";

const GiftPage = props => {

  const actSelcet = (id,remain)=>{
    if(remain > 0) {
      props.setValues({
          item_select: id
      });
    }
  }
  const actOpenReceiveModal = ()=>{
    let {item_list,item_select} = props;
    props.setValues({
        modal_open: "receive",
        modal_message: "ท่านต้องการรับ<br/>"+item_list[item_select].package_name,
    });
  }
  const actOpenSendModal = ()=>{
    props.setValues({
        modal_open: "send",
        friend_uid: "",
    });
  }

  const apiExchangeInfo = () => {
    if(props.jwtToken !== "") {
      props.setValues({ modal_open: "loading" });
      apiPost(
        "REACT_APP_API_POST_EXCHANGE_INFO",
        props.jwtToken,
        {type: "exchange_info"},
        (data)=>{//function successCallback
          props.setValues({
              ...data.data,
              modal_open:"",
          });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message,
          });
        },
      );
    }
  }

  useEffect(()=>{ apiExchangeInfo() },[props.jwtToken]);

  let canReceive = (props.item_select !== -1) ? true: false;

  return (
    <F11Layout>
      <GiftStyle>
        {[1,2,3].map((item,index)=>{
          let {id,remain} = props.item_list[index];
          return (
            <SlotItem
              key={"key_"+index}
              className={"gift__area"}
              num={item}
              lock={remain<=0}
              active={(props.item_select === -1) ? true : (index === props.item_select)}
              onClick={()=>actSelcet(index,remain)}
            >
              <img className="image" src={imgList["item_"+item]} alt />
              <div className="count">จำนวนที่มี {remain}</div>
            </SlotItem>
          )
        })}
        <div className="gift__bottom">
          <Btn
            width="144px"
            height="49px"
            grayscale={canReceive? "0":"1"}
            active={canReceive}
            src={imgList.btn_receive}
            hover={imgList.btn_receive_hover}
            onClick={()=>actOpenReceiveModal()}
          />
          <Btn
            width="144px"
            height="49px"
            grayscale={(canReceive && props.can_send_gift)? "0": "1"}
            active={(canReceive && props.can_send_gift)}
            src={imgList.btn_send}
            hover={imgList.btn_send_hover}
            onClick={()=>actOpenSendModal()}
          />
        </div>
      </GiftStyle>
    </F11Layout>
  )
}

const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(GiftPage);

const GiftStyle = styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: 700px;
  background-image: url(${imgList.gift_bg});
  background-color: #000000;
  .gift {
    &__slot {

    }
    &__bottom {
      display: flex;
      justify-content: space-between;
      align-items: center;
      width: 335px;
      position: absolute;
      top: 550px;
      left: 50%;
      transform: translate(-50%, 0px);
    }
  }
`;
const SlotItem = styled.a`
  position: absolute;
  top: 280px;
  ${props=> props.num === 1 && "left: 396px;"}
  ${props=> props.num === 2 && "left: 553px;"}
  ${props=> props.num === 3 && "left: 709px;"}
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
  width: 150px;
  height: 220px;
  transform: translate(-50%,0);
  filter: grayscale( ${props=>props.active?"0":"1"});
  pointer-events: ${props=>props.lock ? "none":"all"};
  padding: 10px 0px;
  .image {
    opacity: ${props=>props.lock ? "0.3":"1"};
  }
  .count {
    background-color: #313334;
    border-radius: 2em;
    min-width: 120px;
    font-size: 16px
    color: #FFFFFF;
    text-align: center;
    height: 1.6em;
    line-height: 1.6em;
    padding: 2px 10px;

  }
`;
