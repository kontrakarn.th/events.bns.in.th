import React from 'react';
import Modal from './Modal';

export default class ModalReceiveExchange extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                title="ยืนยัน"
                confirmMode={false}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox">
                    <span className="inbox__head">ได้รับ</span>
                    <div>- {this.props.msg}</div>
                </div>
            </Modal>
        )
  }
}
