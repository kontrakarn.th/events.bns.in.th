import React, { Component } from 'react';
import {connect} from 'react-redux';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/airpay_dec2018.css';

// import ModalConfirmToRedeem from '../components/ModalConfirmToRedeem';

import ModalMessage from '../components/ModalMessage';
import ModalConfirmGacha from '../components/ModalConfirmGacha';
import ModalReceiveGacha from '../components/ModalReceiveGacha';
import ModalConfirmExchange from '../components/ModalConfirmExchange';
import ModalReceiveExchange from '../components/ModalReceiveExchange';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';

import imgGacha1 from './../static/images/airpay_dec2018/gacha_1.png';
import imgGacha2 from './../static/images/airpay_dec2018/gacha_2.png';
import imgGacha3 from './../static/images/airpay_dec2018/gacha_3.png';
import imgGacha5 from './../static/images/airpay_dec2018/gacha_5.png';
import imgGacha4 from './../static/images/airpay_dec2018/gacha_4.png';
import imgGachaEffect from './../static/images/airpay_dec2018/gacha_effect.png';
import imgExchange1 from './../static/images/airpay_dec2018/exchange_1.png';
import imgExchange2 from './../static/images/airpay_dec2018/exchange_2.png';
import imgExchange3 from './../static/images/airpay_dec2018/exchange_3.png';
import imgExchange4 from './../static/images/airpay_dec2018/exchange_4.png';
import imgExchange5 from './../static/images/airpay_dec2018/exchange_5.png';
import imgExchange6 from './../static/images/airpay_dec2018/exchange_6.png';

import {
    onAccountLogout
} from '../actions/AccountActions';

const gachaList = [ imgGacha1, imgGacha2, imgGacha3, imgGacha5, imgGacha4 ];
const exchangeList = [  imgExchange1, imgExchange2, imgExchange3, imgExchange4, imgExchange5, imgExchange6 ];
const gachaRewardTitle = [
    ["ชุดความเย้ายวน หน้าหนาว (1)","เหรียญ AirPay (2)","พลุแห่งการ เฉลิมฉลอง (5)"],
    ["ชุดเสื้อแจ็คเก็ต กลุ่มรักอิสระ (1)","เหรียญ AirPay (2)","พลุแห่งการ เฉลิมฉลอง (5)"],
    ["กล่องอัญมณี ห้าเหลี่ยมของฮงมุน (3)","เหรียญ AirPay (2)","พลุแห่งการ เฉลิมฉลอง (5)"],
    ["สัญลักษณ์ ฮงมุน (20)","เหรียญ AirPay (2)","พลุแห่งการ เฉลิมฉลอง (5)"],
    ["หินจันทรา (1)","เหรียญ AirPay (2)","พลุแห่งการ เฉลิมฉลอง (5)"],
]
class HomeContainer extends Component {
    actGachaRandom(){
        this.setState({
            gacha: -1
        })
        this.apiPlay();
    }
    actExchange(packet_id){

        this.apiExchange(packet_id);
    }
    genClassGacha(baseClass,index,gachaSelected,infinite=false){
        if(gachaSelected < 0) {
            return baseClass;
        }
        if(infinite){
            return baseClass + " " + baseClass + "--rnd"+index+"infinite";
        }
        if(gachaSelected === index) {
            return baseClass + " " + baseClass + "--selected"
        } else {
            return baseClass + " " + baseClass + "--rnd"+index
        }



    }
    numberWithCommas(x){
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiEventInfo(){
        this.setState({ showLoading: true});
        let self = this;
        fetch( process.env.REACT_APP_API_POST_EVENT_INFO, {
            method: 'POST',
            body: JSON.stringify({
                "type":"event_info"
            }),
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'
            }
        }).then(response => response.json()).then(response=>{
            if(response.status){
                this.setState({
                    permission: true,
                    showLoading: false,
                    ...response.data
                })
            }else{
                if(response.type=='no_permission'){
                    this.setState({
                        permission: false
                    })
                } else {
                    this.setState({
                        showModalMessage: response.message,
                        showLoading: false,
                    });
                }
            }

        }, error => {

        });
    }
    apiPlay(){
        this.setState({
            showModalConfirmGacha: false,
            // showLoading: true,
            rnd_gacha: true,
        });
        let self = this;
        // fetch(process.env.REACT_APP_API_SERVER_HOST + process.env.REACT_APP_API_POST_PLAY, {
        fetch( process.env.REACT_APP_API_POST_PLAY, {
            method: 'POST',
            body: JSON.stringify({
                "type":"play"
            }),
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'
            }
        }).then(response => response.json()).then(response=>{
            if(response.status){
                this.setState({
                    gacha: response.gacha,
                    // showLoading: false,
                    rnd_gacha: false,
                    ...response.data
                })
                setTimeout(()=>this.setState({showModalReceiveGacha:true}),4000);
            }else{
                if(response.type=='no_permission'){
                    this.setState({
                        permission: false
                    })
                } else {
                    this.setState({
                        showModalMessage: response.message,
                        // showLoading: false,
                        rnd_gacha: false,
                    });
                }
            }

        }, error => {

        });
    }
    apiExchange(packet_id){
        this.setState({
            showModalConfirmExchange: false,
            showLoading: true
        });
        let self = this;
        fetch( process.env.REACT_APP_API_POST_EXCHANGE, {
            method: 'POST',
            body: JSON.stringify({
                "type":"exchange",
                "id":packet_id,
            }),
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'
            }
        }).then(response => response.json()).then(response=>{
            if(response.status){
                this.setState({
                    permission: true,
                    // showModalReceiveExchange: true,
                    // showModalReceiveExchangeMessage: response.message,
                    showModalMessage: response.message,
                    canClickExchange: true,
                    showLoading: false,
                    ...response.data,
                })
            }else{
                if(response.type=='no_permission'){
                    this.setState({
                        permission: false,
                        showLoading: false,
                    })
                } else {
                    this.setState({
                        showModalMessage: response.message,
                        showLoading: false,
                    });
                }
            }

        }, error => {

        });
    }
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,

            name: "",
            canplay: false,
            coin_amount: 0,
            gachas:[],
            mypoint: 0,
            packages:[],
            remain_my_ticket: 0,
            remain_play: 0,
            remain_ticket: 0,

            package_select:{},
            gacha: -1,
            rnd_gacha: false,
            exchange_package: {},

            showModalMessage: "",
            showModalConfirmGacha: false,
            showModalReceiveGacha: false,
            showModalConfirmExchange: false,
            showModalReceiveExchange: false,
            showModalReceiveExchangeMessage: "",

            canClickRandom: true,
            canClickExchange: true,
            scollDown: false,
        }
    }
    componentDidMount(){
        setTimeout(()=>this.apiEventInfo(),600);
    }
    componentDidUpdate(prevProps, prevState) {
        // if(prevProps.userData != this.props.userData){
        //     this.getEventInfo()
        // }
    }

    render() {
        let username = (this.props.userData)? this.props.userData.username : "";
        return (
            <div >
                <div className="f11layout">
                    <div className="f11layout__name">{username}</div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                    <div className={"f11layout__scroll"+(this.state.scollDown? " hide":"")} ><img src={iconScroll} alt="" /></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">

                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="airpay_dec2018">
                                <section className="airpay_dec2018__section airpay_dec2018__section--header">
                                    <div className="header">
                                        <div className="header__inner">
                                            <div className="header__tag header__tag--date"></div>
                                            <div className="header__tag header__tag--promotion"></div>
                                        </div>
                                    </div>
                                </section>
                                <section className="airpay_dec2018__section airpay_dec2018__section--condition">
                                    <div className="condition">
                                        <div className="condition__inner">
                                            <div className="condition__head"></div>
                                            <ul className="condition__text">
                                                <li>ระยะเวลากิจกรรม 12 ธ.ค. 2561 (08:00 น.) - 9 ม.ค.. 2562 (23:59 น.)</li>
                                                <li>ผู้เล่นแลกไดมอนด์ผ่าน <span>AirPay</span> ทุกช่องทาง</li>
                                                <li>เมื่อครบ 30,000 ไดมอนด์ จะได้รับสิทธิ์ในการสุ่มกาชาของ <span>AirPay</span>  1 สิทธิ์ (5 สิทธิ์ต่อ UID)</li>
                                                <li>ผู้เล่นสามารถแลกไดมอนด์แบบสะสมได้</li>
                                                <li>จำกัดสิทธิ์ 1,500 สิทธิ์แรกที่สุ่มกาชาของ <span>AirPay</span> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                                <section className="airpay_dec2018__section airpay_dec2018__section--status">
                                    <div className="status">
                                        <div className="status__inner">
                                            <div className="status__head"></div>
                                            <div className="status__group" style={{width: "50%"}}>
                                                <div className="status__topic">จำนวนไดมอนด์ที่แลก</div>
                                                <div className="status__text">{this.numberWithCommas(this.state.mypoint)}</div>
                                            </div>
                                            <div className="status__group" style={{width: "50%"}}>
                                                <div className="status__topic">จำนวนสิทธิ์ที่คุณมี</div>
                                                <div className="status__text">{this.state.remain_my_ticket} / 5</div>

                                            </div>
                                            <div className="status__group" style={{width: "100%"}}>
                                                <div className="status__topic">จำนวนสิทธิ์ที่เหลือทั้งหมด</div>
                                                <div className="status__text">{this.numberWithCommas(this.state.remain_ticket)}</div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <section className="airpay_dec2018__section airpay_dec2018__section--gacha">
                                    <div className="gacha">
                                        <div className="gacha__head"></div>
                                        <div className="gacha__list">
                                            {this.state.gachas.map((item,index)=>{
                                                return (
                                                    <div className={this.genClassGacha("gacha__slot",index,this.state.gacha,this.state.rnd_gacha)} key={"gacha_slot_"+item.package_id}>
                                                        <img className="gacha__image" src={gachaList[index]} alt="" />
                                                        <img className="gacha__effect" src={imgGachaEffect} alt="" />
                                                    </div>
                                                )
                                            })}
                                        </div>
                                        <div className="gacha__random">
                                            {this.state.canplay && this.state.canClickRandom?
                                                <a className="gacha__btnrandom" onClick={()=>this.setState({showModalConfirmGacha: true,  canClickRandom: false})}>จำนวนสิทธิ์ในการกดสุ่มที่เหลือ {this.state.remain_play}</a>
                                                :
                                                <div className="gacha__btnrandom gacha__btnrandom--inactive" >
                                                    จำนวนสิทธิ์ในการกดสุ่มที่เหลือ {this.state.remain_play}
                                                </div>


                                            }
                                        </div>
                                    </div>
                                </section>
                                <section className="airpay_dec2018__section airpay_dec2018__section--exchange">
                                    <div className="exchange">
                                        <div className="exchange__inner">
                                            <div className="exchange__head"></div>
                                            <div className="exchange__amount">
                                                จำนวนเหรียญ<br />
                                                AirPay<br />
                                                ของคุณ<br />
                                                <span>{this.state.coin_amount}</span>
                                            </div>
                                            <div className="exchange__list">
                                                {this.state.packages.map((item,index)=>{
                                                    return (
                                                        <div className="exchange__slot">
                                                            <img className="exchange__image" src={exchangeList[index]} />
                                                            {item.can_receive && this.state.canClickExchange ?
                                                                <a className="exchange__btn" onClick={()=>this.setState({showModalConfirmExchange:true, exchange_package:item })} />
                                                                :
                                                                <div className="exchange__btn exchange__btn--inactive" />
                                                            }
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </ScrollArea>
                    </div>
                    :
                    <Permission />
                }


                <Loading
                    open={this.state.showLoading}
                />
                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
                <ModalConfirmGacha
                    open={this.state.showModalConfirmGacha}
                    actConfirm={()=>this.actGachaRandom()}
                    actClose={()=>this.setState({showModalConfirmGacha: false, canClickRandom: true})}
                />
                <ModalReceiveGacha
                    open={this.state.showModalReceiveGacha}
                    actClose={()=>this.setState({showModalReceiveGacha: false, canClickRandom: true})}
                    data={this.state.gacha >= 0? gachaRewardTitle[this.state.gacha] : []}
                />
                <ModalConfirmExchange
                    open={this.state.showModalConfirmExchange}
                    actConfirm={()=>this.actExchange(this.state.exchange_package.package_id)}
                    actClose={()=>this.setState({showModalConfirmExchange: false, canClickExchange: true})}
                    data={this.state.exchange_package}

                />
                <ModalReceiveExchange
                    open={this.state.showModalReceiveExchange}
                    actClose={()=>this.setState({showModalReceiveExchange: false, canClickExchange: true})}
                    msg={this.state.showModalReceiveExchangeMessage}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
    onAccountLogout: () => {
        dispatch(onAccountLogout());
    }
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
