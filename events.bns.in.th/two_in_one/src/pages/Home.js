import React,{useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {setValues} from './../store/redux';
import styled from 'styled-components';
import imgList from './../constants/ImportImages';
import F11Layout from './../features/f11Layout';
import Modals from './../features/modals';
import {apiPost} from './../constants/Api';

const Home = (props) => {
  	const apiEventInfo = () => {
		let dataSend = {type: "event_info"};
		let successCallback = (res) => {
			if (res.status) {
				props.setValues({
					...res.data,
				})
                if (res.data && !res.data.selected_char && res.data.characters.length === 0) {
                    props.setValues({
                        modal_open: "message",
                        modal_message: "ตัวละครไม่ตรงเงื่อนไข",
                    })
                } else if (res.data && !res.data.selected_char && res.data.characters.length != 0) {
                    props.setValues({
                        modal_open: "character"
                    })
                } else {
					props.setValues({
                        modal_open: ""
                    })
				}
            } else {
                props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
		}
		let failCallback = (res) => {
			if (res.type === 'no_permission') {
				props.setValues({
					permission: false,
					modal_open: "",
				});
			}
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
    	apiPost("REACT_APP_API_POST_EVENT_INFO", props.jwtToken, dataSend, successCallback, failCallback);
	}

	const apiReceiveCoin = (id) => {
		let dataSend = {type: "claim_quest_points",id};
		let successCallback = (res) => {
			if(res.status) {
				props.setValues({
					...res.data,
					modal_open:"message",
					modal_message: res.message,
				})
			} 
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		let failCallback = (res) => {
			if (res.type === 'no_permission') {
				props.setValues({
					permission: false,
					modal_open: "",
				});
			}
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		apiPost("REACT_APP_API_POST_CLAIM_QUEST_POINTS", props.jwtToken, dataSend, successCallback, failCallback);
	}

	const handleExchange = (title,id) => {
		props.setValues({
			modal_open: 'confirm',
			modal_message: 'ต้องการแลกไอเทม <br/><span>' + title + '</span> ?',
			modal_exchange_id: id
		})
	}

	useEffect(()=>{
		if(props.jwtToken !== "" && !props.status) apiEventInfo();
	},[props.jwtToken])

	return (
		<F11Layout page={"main"} showSidebar={true}>
			<Modals />
			<HomeStyle>
				<section className="banner">
					{/* <Link className="banner__btn" to={props.eventPath("/promotion")} /> */}
				</section>

				{/* ===== section daily ===== */}
				<section className="detail">
					<div className="wrapper">
						<img src={imgList.header_daily} alt=""/>
						<div className="quest__wrapper">
						{
							props.daily_quests.map((items, key)=>{	
								return(
									<div key={key}>
										{/* <div className="count">{items.completed_count}/{items.quest_limit}</div> */}
										<img src={imgList[items.img]}/>
										{
											items.status == 'not_in_condition' &&
											<div className="btn btn_coin_disabled"></div>
										}
										{
											items.status == 'claimed' &&
											<div className="btn btn_coin_received"></div>
										}
										{
											items.status == 'can_claim' &&
											<div className="btn btn_coin" onClick={()=>apiReceiveCoin(items.id)}></div>
										}
									</div>
								)
							})
						}
						</div>
						<img src={imgList.line} alt="" className="line"/>
					</div>
				</section>

				{/* ===== section mission ===== */}
				<section className="detail section_mission">
					<div className="wrapper">
						<img src={imgList.header_mission} alt=""/>
						<div className="quest__wrapper">
						{
							props.daily_challenge_quests.map((items, key)=>{
								return(
									<div key={key}>
										<img src={imgList[items.img]}/>
										{
											items.status == 'not_in_condition' &&
											<div className="btn btn_coin_disabled"></div>
										}
										{
											items.status == 'claimed' &&
											<div className="btn btn_coin_received"></div>
										}
										{
											items.status == 'can_claim' &&
											<div className="btn btn_coin" onClick={()=>apiReceiveCoin(items.id)}></div>
										}
									</div>
								)
							})
						}
						</div>
						<img src={imgList.line} alt="" className="line"/>
						<img src={imgList.character1} alt="" className="character1"/>
						<img src={imgList.character2} alt="" className="character2"/>
					</div>
				</section>

				{/* ===== section exchange ===== */}
				<section className="detail section_exchange">

					<div className="wrapper">
						<img src={imgList.header_exchange1} alt="" className="header"/>
						<div className="exchange__wrapper">
						{
							props.limit_pve_rewards.map((items, key)=>{
								return(
									<div key={key}>
										<div className="count">{items.limit_count}/{items.limit}</div>
										<img src={imgList[items.img]}/>
										{
											items.can_claim ?
											<div className="btn btn_exchange" onClick={()=>handleExchange(items.package_name,items.id)}></div>
											:
											<div className="btn btn_exchange_disabled"></div>
										}	
									</div>
								)
							})
						}
						</div>
					</div>

					<div className="wrapper">
						<img src={imgList.header_exchange2} alt="" className="header"/>
						<div className="exchange__wrapper">
						{
							props.pve_rewards.map((items, key)=>{
								return(
									<div key={key}>
										<img src={imgList[items.img]}/>
										{
											items.hover !== null &&
											<img src={imgList[items.hover]} className="hover"/>
										}
										{
											items.can_claim ?
											<div className="btn btn_exchange" onClick={()=>handleExchange(items.package_name,items.id)}></div>
											:
											<div className="btn btn_exchange_disabled"></div>
										}	
									</div>
								)
							})
						}
						</div>
						<img src={imgList.line} alt="" className="line"/>
					</div>

					<div className="wrapper">
						<img src={imgList.header_exchange3} alt="" className="header"/>
						<div className="exchange__wrapper">
						{
							props.limit_pvp_rewards.map((items, key)=>{
								return(
									<div key={key}>
										<div className="count">{items.limit_count}/{items.limit}</div>
										<img src={imgList[items.img]}/>
										{
											items.can_claim ?
											<div className="btn btn_exchange" onClick={()=>handleExchange(items.package_name,items.id)}></div>
											:
											<div className="btn btn_exchange_disabled"></div>
										}
									</div>
								)
							})
						}
						</div>
					</div>

					<div className="wrapper">
						<img src={imgList.header_exchange4} alt="" className="header"/>
						<div className="exchange__wrapper">
						{
							props.pvp_rewards.map((items, key)=>{
								return(
									<div key={key}>
										<img src={imgList[items.img]}/>
										{
											items.hover !== null &&
											<img src={imgList[items.hover]} className="hover"/>
										}
										{
											items.can_claim ?
											<div className="btn btn_exchange" onClick={()=>handleExchange(items.package_name,items.id)}></div>
											:
											<div className="btn btn_exchange_disabled"></div>
										}
									</div>
								)
							})
						}
						</div>
					</div>

				</section>
						
				{/* ===== section rule ===== */}
				<section>
					<img src={imgList.rule}/>
				</section>
			</HomeStyle>
		</F11Layout>
	)
}
const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(Home);

const HomeStyle = styled.div`
	position: relative;
	display: block;
	width: 1105px;
	height: 6399px;
	background-image: url(${imgList.bg_home});
  	.banner {
		position: relative;
		display: block;
		height: 700px;
		margin: 0 0 140px;
		&__btn {
			position: absolute;
			top: 410px;
    		left: 228px;
			display: block;
			width: 306px;
			height: 53px;
			background-image: url(${imgList.btn_join});
			background-position: top center;
			&:hover {
				background-position: bottom center;
			}
		}
  	}
	section{
		position: relative;
		display: block;
		margin: 80px 0;
		.wrapper{
			width: 750px;
			margin: 0 auto;
			text-align:center;
			.line{
				margin: 50px 0;
			}
		}
	
	}
	.quest__wrapper{
		display: flex;
		flex-wrap: wrap;
		align-content: center;
		justify-content: center;
		> div{
			width: 25%;
			position: relative;
			margin-bottom: 20px;
			img{
				margin-bottom: 5px;
				max-width: 100%;
				max-height: 100%;
			}
		}
		.count{
			font-size: 13px;
			color: #fff;
			position: absolute;
			left: 0;
			right: 0;
			margin: 0 auto;
			width: 35px;
    		height: 35px;
			line-height: 35px;
		}
	}
	.btn{
		width: 91px;
		height: 39px;
		margin: 0 auto;
		cursor: pointer;
		&:hover{
			background-position: bottom center;
		}
		&_coin{
			background-image: url(${imgList.btn_coin});
			background-position: top center;
		}
		&_coin_disabled{
			background-image: url(${imgList.btn_coin_disabled});
			background-position: top center;
			pointer-events: none;
		}
		&_coin_received{
			background-image: url(${imgList.btn_coin_received});
			background-position: top center;
			pointer-events: none;
		}
		&_exchange{
			background-image: url(${imgList.btn_exchange});
			background-position: top center;
		}
		&_exchange_disabled{
			background-image: url(${imgList.btn_exchange_disabled});
			background-position: top center;
			pointer-events: none;
		}
		
	}
	.exchange__wrapper{
		display: flex;
		flex-wrap: wrap;
		> div{
			width: 20%;
			position: relative;
			margin-bottom: 20px;
			img{
				margin-bottom: 5px;
				max-width: 100%;
				max-height: 100%;
				&:hover + .hover{
					opacity: 1;
					z-index: 1;
				}
			}
			.hover{
				max-width: 400px;
				max-height: unset !important;
				position: absolute;
				left: 85%;
				top: -20px;
				opacity: 0;
				//z-index: 0;
				transition: all 0.3s ease;
			}
			
		}
		
		.count{
			font-size: 13px;
			color: #fff;
			position: absolute;
			top: 106px;
			left: 0;
			right: 0;
			margin: 0 auto;
		}
	}
	.section_mission{
		.character1{
			position: absolute;
			left: 70px;
			top: -50px;
		}
		.character2{
			position: absolute;
			right: 100px;
			top: 0;
		}
	}
	.section_exchange{
		.header{
			margin: 35px 0;
		}
	}
`;
