import React,{useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {setValues} from '../store/redux';
import styled from 'styled-components';
import imgList from '../constants/ImportImages';
import F11Layout from '../features/f11Layout';
import ScrollArea from 'react-scrollbar';
import {apiPost} from './../constants/Api';

const ResentHistory = (props) => {
	const apiEventInfo = () => {
		let dataSend = {type: "event_info"};
		let successCallback = (res) => {
			if (res.status) {
				props.setValues({
					...res.data,
				})
				if (res.data && !res.data.selected_char && res.data.characters.length === 0) {
						props.setValues({
								modal_open: "message",
								modal_message: "ตัวละครไม่ตรงเงื่อนไข",
						})
				} else if (res.data && !res.data.selected_char && res.data.characters.length != 0) {
						props.setValues({
								modal_open: "character"
						})
				} else {
					props.setValues({
						modal_open: ""
					})
				}
            } else {
                props.setValues({
                    modal_open: "message",
                    modal_message: res.message,
                });
            }
		}
		let failCallback = (res) => {
			if (res.type === 'no_permission') {
				props.setValues({
					permission: false,
					modal_open: "",
				});
			}
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
    	apiPost("REACT_APP_API_POST_EVENT_INFO", props.jwtToken, dataSend, successCallback, failCallback);
	}
  const apiHistory = () => {
		props.setValues({
			modal_open:"loading"
		})
    let dataSend = {type: "compensate_history"};
    let successCallback = (res) => {
      if(res.status) {
        props.setValues({
            history_resent: res.data.points_histories,
            modal_open: "",
        })
      } else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    let failCallback = (res) => {
      if (res.type === 'no_permission') {
        props.setValues({
            permission: false,
            modal_open: "",
        });
      }else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    apiPost("REACT_APP_API_POST_HISTORY_RESENT", props.jwtToken, dataSend, successCallback, failCallback);
  }

  useEffect(()=>{
		if( props.jwtToken!=="" ) {
			apiEventInfo();
			apiHistory();
		}
  },[])

  	return (
		<F11Layout page={"resent"} hideScroll={true} showSidebar={false}>
			<HistoryStyle>
				<div className="table">
				<ScrollArea speed={0.8} className="table__list" contentClassName="" horizontal={false} >
					<ul>
						{props.history_resent && props.history_resent.length>0 && props.history_resent.map((item,index)=>{
							return (
								<li className="table__slot" key={"point_"+index}>
									<div className="table__title">{item.title}</div>
									<div className="table__date">{item.date}</div>
									<div className="table__time">{item.time}</div>
								</li>
							)
						})}
					</ul>
				</ScrollArea>
				</div>
			</HistoryStyle>
		</F11Layout>
  	)
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ResentHistory);

const HistoryStyle = styled.div`
	color: #FFFFFF;
	font-size: 18px;
	position: relative;
	display: block;
	width: 1105px;
	height: 700px;
	padding-top: 100px;
	margin: 0px;
	background-image: url(${imgList.bg_history});
	.table {
		position: relative;
		display: block;
		width: 953px;
		height: 601px;
		padding-top: 75px;
		margin: 0px auto;
		background-image: url(${imgList.history_resend_frame});
		&__list {
			width: 940px;
			height: 470px;
			margin: 0px auto;
		}
		&__slot {
			position: relative;
			display: flex;
			//justify-content: space-between;
			align-items: center;
			width: 764px;
			margin: 0px auto 1px;
			font-family: "Kanit-Light", Tahoma;
			font-size: 16px;
			color: #fff;
		}
		&__title {
			width: 68%;
    		margin-right: 1%;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}
		&__date {
			width: 14%;
			margin-right: 7%;
			text-align: left;
		}
		&__time {
			width: 8%;
    		text-align: left;
		}
	}

`;
