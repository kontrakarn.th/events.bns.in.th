const bg_home					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/bg_home.jpg';
const line						= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/line.png'
const character1				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/character1.png'
const character2				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/character2.png'
const coin_gold					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/coin_gold.png'
const coin_red					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/coin_red.png'
const rule						= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/rule.png'
const header_daily				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/header_daily.png' 
const header_exchange1			= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/header_exchange1.png'
const header_exchange2			= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/header_exchange2.png'
const header_exchange3			= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/header_exchange3.png'
const header_exchange4			= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/header_exchange4.png'
const header_mission			= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/header_mission.png'
const btn_coin_disabled			= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/btn_coin_disabled.png'
const btn_coin_received			= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/btn_coin_received.png'
const btn_coin					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/btn_coin.png'
const bg_menu					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/bg_menu.jpg'
const bg_confirm				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/bg_confirm.png'
const bg_message				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/bg_message.png'
const bg_character				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/bg_character.png'
const btn_confirm				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/btn_confirm.png'
const btn_cancel				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/btn_cancel.png'
const btn_exchange_disabled		= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/btn_exchange_disabled.png'
const btn_exchange				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/btn_exchange.png'
const history_point_frame		= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/history_point_frame.png'
const history_resend_frame		= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/history_resend_frame.png'
const history_reward_frame		= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/history_reward_frame.png'
const bg_history				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/bg_history.jpg'

const quest_1					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/quest/1.png'
const quest_2					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/quest/2.png'
const quest_3					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/quest/3.png'
const quest_4					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/quest/4.png'
const quest_5					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/quest/5.png'
const quest_6					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/quest/6.png'
const quest_7					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/quest/7.png'
const quest_8					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/quest/8.png'
const quest_9					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/quest/9.png'
const quest_10					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/quest/10.png'
const mission_1					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/mission/1.png'
const mission_2					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/mission/2.png'
const exchange1_1				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange1/1.png'
const exchange1_2				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange1/2.png'
const exchange1_3				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange1/3.png'
const exchange1_4				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange1/4.png'
const exchange1_5				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange1/5.png'
const exchange1_6				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange1/6.png'
const exchange1_7				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange1/7.png'
const exchange1_8				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange1/8.png'
const exchange1_9				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange1/9.png'
const exchange1_10				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange1/10.png'
const exchange1_11				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange1/11.png'
const exchange1_12				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange1/12.png'
const exchange2_1				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/1.png'
const exchange2_2				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/2.png'
const exchange2_3				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/3.png'
const exchange2_4				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/4.png'
const exchange2_5				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/5.png'
const exchange2_6				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/6.png'
const exchange2_7				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/7.png'
const exchange2_8				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/8.png'
const exchange2_9				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/9.png'
const exchange2_10				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/10.png'
const exchange3_1				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange3/1.png'
const exchange3_2				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange3/2.png'
const exchange3_3				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange3/3.png'
const exchange3_4				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange3/4.png'
const exchange3_5				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange3/5.png'
const exchange3_6				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange3/6.png'
const exchange3_7				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange3/7.png'
const exchange3_8				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange3/8.png'
const exchange3_9				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange3/9.png'
const exchange3_10				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange3/10.png'
const exchange3_11				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange3/11.png'
const exchange4_1				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/1.png'
const exchange4_2				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/2.png'
const exchange4_3				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/3.png'
const exchange4_4				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/4.png'
const exchange4_5				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/5.png'
const exchange4_6				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/6.png'
const exchange4_7				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/7.png'
const exchange4_8				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/8.png'
const exchange4_9				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/9.png'
const exchange4_10				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/10.png'
const exchange4_11				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/11.png'
const exchange4_12				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/12.png'
const exchange4_13				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange4/13.png'
const hover_1					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/hover_1.png'
const hover_2					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/hover_2.png'
const hover_3					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/exchange2/hover_3.png'
const icon_down_arrow			= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/icon_down_arrow.png'
const icon_close				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/icon_close.png'
const btn_home 					= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/btn_home.png';
const icon_scroll 				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/icon_scroll.png';
const permission 				= 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/two_in_one/permission.jpg';


export default {
	btn_home,
	icon_scroll,
	permission,


	bg_home,
	line,
	character1,
	character2,
	coin_gold,	
	coin_red,
	rule,
	header_daily,
	header_exchange1,
	header_exchange2,
	header_exchange3,
	header_exchange4,
	header_mission,
	btn_coin_disabled,
	btn_coin_received,
	btn_coin,
	bg_menu,
	bg_confirm,
	bg_message,
	bg_character,
	btn_confirm,
	btn_cancel,
	btn_exchange_disabled,
	btn_exchange,
	history_point_frame,
	history_resend_frame,
	history_reward_frame,
	bg_history,			
	quest_1,		
	quest_2,		
	quest_3,		
	quest_4,		
	quest_5,		
	quest_6,		
	quest_7,		
	quest_8,		
	quest_9,		
	quest_10,	
	mission_1,
	mission_2,
	exchange1_1,	
	exchange1_2,	
	exchange1_3,	
	exchange1_4,	
	exchange1_5,	
	exchange1_6,	
	exchange1_7,	
	exchange1_8,	
	exchange1_9,	
	exchange1_10,
	exchange1_11,
	exchange1_12,
	exchange2_1,	
	exchange2_2,	
	exchange2_3,	
	exchange2_4,	
	exchange2_5,	
	exchange2_6,	
	exchange2_7,	
	exchange2_8,	
	exchange2_9,	
	exchange2_10,
	exchange3_1,	
	exchange3_2,	
	exchange3_3,	
	exchange3_4,	
	exchange3_5,	
	exchange3_6,	
	exchange3_7,	
	exchange3_8,	
	exchange3_9,	
	exchange3_10,
	exchange3_11,
	exchange4_1,	
	exchange4_2,	
	exchange4_3,	
	exchange4_4,	
	exchange4_5,	
	exchange4_6,	
	exchange4_7,	
	exchange4_8,	
	exchange4_9,	
	exchange4_10,
	exchange4_11,
	exchange4_12,
	exchange4_13,
	hover_1,
	hover_2,
	hover_3,
	icon_down_arrow,
	icon_close		
}
