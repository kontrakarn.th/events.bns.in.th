import React, { useState } from 'react';
import { connect } from 'react-redux';
import ScrollArea from 'react-scrollbar';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from '../../constants/ImportImages';
import { apiPost } from './../../constants/Api';

const ModalLoading = (props) => {
    const name = 'character';
    const [slected, setSlected] = useState(-1);
    const [slectedId, setSlectedId] = useState(-1);
    const [show, setShow] = useState(false);

    const actSelected = (index, id) => {
        setSlected(index);
        setSlectedId(id)
    };
    const actSelectCharacter = (id) => {
        if (slected < 0) {
             props.setValues({
                 modal_open: 'message',
                 modal_message: 'โปรดเลือกตัวละคร',
                 selected_char: false,
             });
        } else {
            const selectedChar = props.characters.filter((c) => c.id == slectedId)[0];
            props.setValues({
                modal_open: 'confirm_character',
                modal_message: 'ต้องการเลือกตัวละคร<br/>'+ selectedChar.char_name + '<br/>อาชีพ ' + selectedChar.job_name + ' LV.'+ selectedChar.level + ' ฮงมุน LV.' + selectedChar.mastery_level,
                modal_character_id: slectedId,
            });
            //apiSelectCharacter(props.characters[slected].id);
        }
    };
    // const apiSelectCharacter = (id) => {
    //     let dataSend = { type: 'select_character', id };
    //     let successCallback = (res) => {
    //         if (res.status) {
    //             props.setValues({
    //                 ...res.data,
    //                 modal_open: 'confirm_character',
    //                 modal_message: 'ต้องการเลือกตัวละคร' + res.message,
    //                 modal_character_id: id
    //             });
    //         } else {
    //             props.setValues({
    //                 modal_open: 'message',
    //                 modal_message: res.message,
    //             });
    //         }
    //     };
    //     let failCallback = (res) => {
    //         if (res.type === 'no_permission') {
    //             props.setValues({
    //                 permission: false,
    //                 modal_open: '',
    //             });
    //         } else {
    //             props.setValues({
    //                 modal_open: 'message',
    //                 modal_message: res.message,
    //             });
    //         }
    //     };
    //     apiPost(
    //         'REACT_APP_API_POST_SELECT_CHAR',
    //         props.jwtToken,
    //         dataSend,
    //         successCallback,
    //         failCallback
    //     );
    // };
    let char_name = props.characters[slected]
        ? props.characters[slected].char_name
        : '';
    return (
        <ModalCore
            open={props.modal_open === name}
            onClick={() => props.setValues({ modal_open: '' })}
        >
            <ModalcontentStyle
                className='kanit_light_font'
                showList={show}
                selected={slected < 0}
            >
                <div className="close" onClick={()=>props.setValues({modal_open:''})}><img src={imgList.icon_close}/></div>
                <div className="content">
                    <div className='select' onClick={() => setShow(!show)}>
                        <div className='select__name'>
                            {slected < 0 ? 'โปรดเลือกตัวละคร' : char_name}
                        </div>
                        <img
                            className='select__icon'
                            src={imgList.icon_down_arrow}
                            alt=''
                        />
                        <ScrollArea
                            peed={0.8}
                            className='select__list'
                            contentClassName=''
                            horizontal={false}
                        >
                            <ul className=''>
                                {props.characters &&
                                    props.characters.map((item, index) => {
                                        return (
                                            <li
                                                key={index}
                                                className='select__slot'
                                                onClick={() =>
                                                    actSelected(index, item.id)
                                                }
                                            >
                                                {item.char_name}
                                            </li>
                                        );
                                    })}
                            </ul>
                        </ScrollArea>
                    </div>
                </div>
                <div className='buttons'>
                    <a
                        className='btn btn--confirm'
                        onClick={() => actSelectCharacter()}
                    />
                </div>
            </ModalcontentStyle>
        </ModalCore>
    );
};

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues };
export default connect(mapStateToProps, mapDispatchToProps)(ModalLoading);

const ModalcontentStyle = styled.div`
   position: relative;
    display: block;
    width: 511px;
    height: 340px;
    padding: 60px 10px 10px;
    background-image: url(${imgList.bg_character});
    .close{
        position: absolute;
        top: 40px;
        right: 20px;
        cursor:pointer;
    }
	.title{
        font-size: 19px;
        text-align: center;
        line-height: 1;
        color: #fff;
        margin-bottom: 25px;
    }
    .content {
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 75%;
        text-align: center;
        font-family: "Kanit-Light", Tahoma;
        &__text{
            line-height: 1.2;
            margin-bottom: 20px;
        }
    }
    .select {
        position: relative;
        color: #fff;
        font-size: 20px;
        display: flex;
        width: 350px;
        justify-content: space-between;
        align-items: center;
        background-color: #03141d;
        pointer-events: ${(props) => (props.showList ? 'none' : 'all')};
        margin: 0px auto;
        cursor: pointer;
        padding: 10px 5px;
        &__name {
            padding: 0px 22.5px;
        }
        &__icon {
        }
        &__list {
            z-index: 1;
            position: absolute;
            /*top: 75px;*/
            top: 55px;
            left: 0px;
            display: block;
            width: 100%;
            max-height: 300px;
            background-color: #03141d;

            opacity: ${(props) => (props.showList ? '1' : '0')};
            pointer-events: ${(props) => (props.showList ? 'all' : 'none')};
        }
        &__slot {
            /* padding: 22.5px;
            background-color: #000000;
            color: #ffffff; */
            padding: 10px;
            background-color: #03141d;
            color: #fff;
            &:hover {
                /* color: #000000;
                background-color: #ffffff; */
                color: #f5f5f5;
            }
        }
    }
    .buttons {
        /* position: absolute;
        left: 0px;
        bottom: 40px;
        display: block;
        width: 100%; */
        text-align: center;
    }
    .btn {
        display: inline-block;
        width: 91px;
        height: 39px;
        margin: 0px 20px;
        background-position: top center;
        &--confirm {
            background-image: url(${imgList.btn_confirm});
        }
        &--cancel {
            background-image: url(${imgList.btn_cancel});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;
