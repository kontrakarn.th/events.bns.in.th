import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from "../../constants/ImportImages";

const ModalLoading = props => {
  const name = "message";
  const actClose = () => {
    if(!props.selected_char && props.characters.length !== 0) {
      props.setValues({modal_open:'character'});
    } else {
      props.setValues({modal_open:''});
    }
  }
	return (
		<ModalCore
			open={props.modal_open === name}
			onClick={()=>props.setValues({modal_open:''})}
		>
			<ModalcontentStyle>
				<div className="message">
					<div dangerouslySetInnerHTML={{__html: props.modal_message}}/>
				</div>
				<div className="buttons">
					<a className="btn btn--confirm" onClick={()=>props.setValues({modal_open:''})} />
				</div>
			</ModalcontentStyle>
		</ModalCore>
  	)
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalLoading);

const ModalcontentStyle = styled.div`
	position: relative;
	display: block;
	width: 511px;
  height: 340px;
	padding: 60px 10px 10px;
	background-image: url(${imgList.bg_message});
	.message {
				color: #FFFFFF;
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 75%;
        text-align: center;
        font-family: "Kanit-Light", Tahoma;
				>div{
            //color: #301616;
            line-height: 1.2;
            >span{
                color: #ffc26b;
            }
        }
    }
  	.buttons {
		/* position: absolute;
		left: 0px;
		bottom: 38px;
		display: block;
		width: 100%; */
		text-align: center;
	}
	.btn {
		display: inline-block;
		width: 91px;
    height: 39px;
		margin: 0px 20px;
		background-position: top center;
		cursor: pointer;
		&--confirm {
			background-image: url(${imgList.btn_confirm});
		}
		&--cancel {
			background-image: url(${imgList.btn_cancel});
		}
		&:hover {
			background-position: bottom center;
		}
	}
`;
