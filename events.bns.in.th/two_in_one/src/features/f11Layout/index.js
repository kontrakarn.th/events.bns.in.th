import React,{useState} from 'react';
import {connect} from 'react-redux';
import { setValues } from './../../store/redux';
import ScrollArea from 'react-scrollbar';
import styled from 'styled-components';
import Permission from './_Permission';
import BtnHome from './_BtnHome';
import IconScroll from './_IconScroll';
import Menu from './_Menu';
import SlotText from './_SlotText';
import imgList from '../../constants/ImportImages';


const LayoutF11 = props => {
  const [showScroll,setShowScroll] = useState(true);
  let nameText = [];
  if(props.username !== "") nameText.push(props.username);
  if(props.character_name !== "") nameText.push(props.character_name);
  
  const handleScroll = (hide) => {
    if(props.hideScroll) {
      if(showScroll) setShowScroll(false);
    } else {
      if(showScroll !== !hide) setShowScroll(!hide);
    }
  }
  const handleSelectCharacter = () =>{
    props.setValues({
      modal_open: 'character'
    })
  }

  if(!props.permission) return <Permission />;

  return (
    <Layout background={props.background}>
      <OverAll>
          <BtnHome to="/" />
          {showScroll &&
            <IconScroll />
          }
          {props.showMenu && props.page!=='' &&
            <Menu
              list={props.menu}
              page={props.page}
              show_compensation_history={props.show_compensation_history}
            />
          }
          {nameText.length > 0 && props.selected_char &&
            <div className="topright" >
              <SlotText>{nameText.join(" : ")}</SlotText>
            </div>
          }
          {
            !props.selected_char &&
            <BtnCharacter onClick={()=>handleSelectCharacter()} className={ !props.selected_char && props.characters.length === 0 ? 'disabled': ''}>เลือกตัวละคร</BtnCharacter>
          }
        
         
          { props.showSidebar &&
            <Sidebar>
                <div className="coin coin__gold">
                  <div>เหรียญเจ้าสำนัก</div>
                  <div>{props.pve_tokens} <img src={imgList.coin_gold}/></div>
                </div>
                <div className="coin coin__red">
                  <div>เหรียญยอดนักสู้</div>
                  <div>{props.pvp_tokens} <img src={imgList.coin_red}/></div>
                </div> 
            </Sidebar>
          }
      </OverAll>

      <ScrollArea
          speed={0.8}
          horizontal={false}
          vertical={props.modal_open === ""}
          onScroll={(value) => handleScroll( value.topPosition > 10)}
          style={{ width: '100%', height:700, opacity:1}}
          verticalScrollbarStyle={{backgroundColor:"#000", opacity:1}}
      >
          {props.children && props.children}
      </ScrollArea>
    </Layout>
  )
}

const mstp = state => ({...state});
const mdtp = { setValues };
export default connect( mstp, mdtp )(LayoutF11);


const Layout = styled.div`
    position: relative;
    display: block;
    width: 1105px;
    height: 700px;
    max-width: 100vw;
    max-height: 100vh;
    overflow: hidden;
    background: no-repeat top center url(${props => props.background});
    font-family: "Kanit-Regular",tahoma;
    .topright {
      right: 22px;
      top: 18px;
      position: absolute;
    }
`;
const OverAll = styled.div`
    z-index: 100;
    position: absolute;
    top: 0px;
    left: 0px;
    display: block;
    width: 100%;
    height: 100%;
    pointer-events: none;
`;
// background: no-repeat center url(${props => props.src}) #c5c0ad;

const Sidebar = styled.div `
    position: absolute;
    bottom: 15%;
    right: -1px;
    display: block;
    color: #fff;
    font-size: 13px;
    z-index: 10;
    text-align: center;
    .coin{ 
        width: 120px;
        height: 65px;
        padding: 10px;
        margin-bottom: 10px; 
        &__gold{
          background-color: #0a2736;
          border: 1px solid #1d4181;
        }
        &__red{
          background-color: #281a1b;
          border: 1px solid #a12224;
        }
        & img{
            vertical-align: middle;
        }
    }   
`;

const BtnCharacter = styled.div `
    pointer-events: auto;
    display: inline-block;
    padding: 0px 25px;
    margin-left: 17px;
    text-align: center;
    height: 45px;
    line-height: 45px;
    background-color: rgba(0,0,0,0.7);
    border-radius: 15px;
    color: #FFFFFF;
    font-size: 14px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    right: 22px;
    top: 18px;
    position: absolute;
    cursor: pointer;
    &.disabled{
      pointer-events:none;
    }
`
