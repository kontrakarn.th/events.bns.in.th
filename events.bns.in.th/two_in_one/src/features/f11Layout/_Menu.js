import React,{useState,useEffect} from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import imgList from './../../constants/ImportImages';
import { setValues } from './../../store/redux';

const Menu = (props) => {
  const [showMenu,setShowMenu] = useState(false);
  return (
    <MenuLayout time={0.3} active={showMenu}>
      <a className="hamburger" onClick={()=>setShowMenu(!showMenu)}><div /><div /><div /></a>
      <div className="backdrop" />
      <ul className="list">
      {/* {props.list.map((item,index)=>{
          return (
            <li key={"menu"+index}>
              <Link
                className={"list__slot"+(props.page==item.page ? " active":"")}
                to={item.link}
                onClick={()=>setShowMenu(false)}
              >{item.title}</Link>
            </li>
          )
        })} */}
        <li>
            <Link
              className={"list__slot"+(props.page=='main' ? " active":"")}
              to={process.env.REACT_APP_EVENT_PATH+"/"}
              onClick={()=>setShowMenu(false)}
            >หน้าหลัก</Link>
        </li>
        { props.selected_char && 
        <>
          <li>
              <Link
                className={"list__slot"+(props.page=='point' ? " active":"")}
                to={process.env.REACT_APP_EVENT_PATH+"/point"}
                onClick={()=>setShowMenu(false)}
              >ประวัติการสำเร็จภารกิจ</Link>
          </li>
          <li>
              <Link
                className={"list__slot"+(props.page=='reward' ? " active":"")}
                to={process.env.REACT_APP_EVENT_PATH+"/reward"}
                onClick={()=>setShowMenu(false)}
              >ประวัติการแลกไอเทม</Link>
          </li>
        </>
        }

        {props.show_compensate_history &&  props.selected_char &&
          <li>
            <Link
              className={"list__slot"+(props.page=='resent' ? " active":"")}
              to={process.env.REACT_APP_EVENT_PATH+"/resent"}
              onClick={()=>setShowMenu(false)}
            >ประวัติการรับไอเทมชดเชย</Link>
          </li>
        }
      </ul>
    </MenuLayout>
  )
}
const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(Menu);

const MenuLayout = styled.div`
  position: fixed;
  top: 0px;
  left: 0px;
  display: block;
  width: 1105px;
  height: 700px;
  z-index: 100;
  pointer-events: none;
  .hamburger {
    z-index: 2;
    position: absolute;
    top: 25px;//15px;
    left: 25px;//15px;
    display: block;
    width: 30px;
    height: 20px;//25px;
    pointer-events: all;
    cursor: pointer;
    // filter: drop-shadow(1px 1px 2px #00000099);
    div{
      position: absolute;
      display: block;
      height: 20%;
      width: 100%;
      background-color: #fff;
      transition: all ${props=>props.time} ease-in-out;
      &:nth-child(1) {
        top: ${props=>props.active ? "50":"5"}%;
        transform: translate(0, -50%) ${props=>props.active ? "rotate(45deg)":""};
      }
      &:nth-child(2) {
        top: 50%;
        transform: translate(0, -50%);
        opacity: ${props=>props.active ? "0":"1"}
      }
      &:nth-child(3) {
        bottom: ${props=>props.active ? "50":"5"}%;
        bootom: 50%;
        transform: translate(0, 50%) ${props=>props.active ? "rotate(-45deg)":""};
      }
    }
  }
  .backdrop {
    display: block;
    width: 100%;
    height: 100%;
    background-color: ${props=>props.active ? "rgba(0,0,0,.9)":"#00000000"};
    transition: background ${props=>props.time} ease-in-out;
    pointer-events: ${props=>props.active ? "all":"none"};
  }
  .list {
    font-size: 20px;
    color: #8e8e8e;
    z-index: 1;
    position: absolute;
    top: 0px;
    left: 0px;
    display: block;
    height: 100%;
    width: 329px;
    padding-top: 150px;
    padding-left: 40px;
    background-image: url(${imgList.bg_menu});
    pointer-events: ${props=>props.active ? "all":"none"};
    transform: translate(${props=>props.active ? "0":"-100%"}, 0) ;
    transition: transform ${props=>props.time} ease-in-out;
    &__slot {
      font-size: 18px;
      line-height: 2em;
      font-weight: lighter;
      :hover{
        color: #fff;
      }
    &.active {
      color: #fff;
      //font-family: "Kanit-Bold", Tahoma;
    }
    &--reward {
      position: relative;
      left: -25px;
    }
  }
`
