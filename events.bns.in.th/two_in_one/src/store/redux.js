import imgList from './../constants/ImportImages';

const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    showUserName: true,
    showCharacterName: false,
    showMenu: true,
    username: "",
    character_name: "",
    characters: [{}],
    menu: [
      {title: "หน้าหลัก", page: "main", link: process.env.REACT_APP_EVENT_PATH+"/"},
      {title: "ประวัติการสำเร็จภารกิจ", page: "reward", link: process.env.REACT_APP_EVENT_PATH+"/reward"},
      {title: "ประวัติการแลกไอเทม", page: "point", link: process.env.REACT_APP_EVENT_PATH+"/point"}
    ],
    selected_char: false,
    loginStatus: false,
    permission: true,

    //===modal
    modal_open: "",//"loading","message","character",""confirm","turban","towelset","confirm_package","confirm_claim"
    modal_message: "",
    modal_bonus: false,

    is_unlocked: false,
    unlock_package: {
        can_buy: false,
        already_buy: false
    },
    checkin_list: [],
    show_compensation_history: false,
    load_reward: true,
    history_reward: [],
    history_resent: [],
    history_point: [],

    //===event base value
    eventPath: (path)=>(process.env.REACT_APP_EVENT_PATH+path),
    status: false,

    points: 0,
    tokens: 0,  
    daily_quests:[],
    daily_challenge_quests: [],
    modal_character_id: 0,
    pve_tokens: 0,
    pvp_tokens: 0,
    limit_pve_rewards:[],
    pve_rewards:[],
    limit_pvp_rewards: [],
    pvp_rewards:[]
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         :return ({...state, sessionKey: action.value});
        case 'ACCOUNT_AUTHED'       :return ({...state, userData: action.value});
        case 'RECEIVE_SESSION_KEY'  :return ({...state, sessionKey: action.value});
        case 'SET_LOGIN_URL'        :return ({...state, loginUrl: action.value});
        case 'SET_LOGOUT_URL'       :return ({...state, logoutUrl: action.value});
        case 'ACCOUNT_LOGIN'        :return ({...state, authState: "LOGGED_IN"});
        case 'ACCOUNT_LOGOUT'       :return ({...state, authState: "LOGGED_OUT"});
        case 'ACCOUNT_AUTH_CHECKED' :return ({...state, authed: true});
        case 'SET_JWT_TOKEN'        :return ({...state, jwtToken: action.value});

        case "SET_VALUE"            :
            return (Object.keys(defaultState).indexOf(action.key) >= 0) ? {
                ...state,
                [action.key]: action.value
            } : state;
        case "SET_VALUES"           :return {...state, ...action.value};
        default:
            return state;
    }
}

export const onAccountAuth = (sessionKey) => ({type: 'ACCOUNT_AUTH', value: sessionKey});
export const onAccountAuthed = (userData) => ({type: 'ACCOUNT_AUTHED', value: userData});
export const onAccountLogin = () => ({type: 'ACCOUNT_LOGIN', value: ""});
export const onAccountLogout = () => ({type: 'ACCOUNT_LOGOUT', value: ""});
export const receiveSessionKey = (sessionKey) => ({type: 'RECEIVE_SESSION_KEY', value: sessionKey});
export const setLoginUrl = (url) => ({type: 'SET_LOGIN_URL', value: url});
export const setLogoutUrl = (url) => ({type: 'SET_LOGOUT_URL', value: url});
export const onAccountAuthChecked = () => ({type: 'ACCOUNT_AUTH_CHECKE', value: true})
export const setJwtToken = (token) => ({type: 'SET_JWT_TOKEN', value: token});

export const setValue = (key, value) => ({type: "SET_VALUE", value, key});
export const setValues = (value) => ({type: "SET_VALUES", value});
