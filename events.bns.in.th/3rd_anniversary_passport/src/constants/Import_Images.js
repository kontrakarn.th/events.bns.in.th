import bg					from '../static/images/background.jpg'
import rule					from '../static/images/rule.png'
import rule_pic				from '../static/images/rule_pic.png'
import package_item_1		from '../static/images/package/1.png'
import package_item_2		from '../static/images/package/2.png'
import package_item_3		from '../static/images/package/3.png'
import package_item_4		from '../static/images/package/4.png'
import package_item_5		from '../static/images/package/5.png'
import package_item_6		from '../static/images/package/6.png'
import package_item_7		from '../static/images/package/7.png'
import package_item_8		from '../static/images/package/8.png'
import package_item_9		from '../static/images/package/9.png'
import package_popup1		from '../static/images/package/popup1.png'
import package_frame		from '../static/images/package_frame.png'
import checkin_frame		from '../static/images/checkin_frame.png'
import btn_diamond			from '../static/images/btn_diamond.png'
import btn_default			from '../static/images/btn_default.png'
import diamond				from '../static/images/diamond.png'
import checkin1_item_1		from '../static/images/checkin1/1.png'
import checkin1_item_2		from '../static/images/checkin1/2.png'
import checkin1_item_3		from '../static/images/checkin1/3.png'
import checkin1_item_4		from '../static/images/checkin1/4.png'
import checkin1_item_5		from '../static/images/checkin1/5.png'
import checkin1_item_6		from '../static/images/checkin1/6.png'
import checkin1_item_7		from '../static/images/checkin1/7.png'
import checkin1_item_8		from '../static/images/checkin1/8.png'
import checkin1_pic			from '../static/images/checkin1_pic.png'
import icon_check			from '../static/images/icon_check.png'
import btn_checkin_icon		from '../static/images/btn_checkin_icon.png'
import btn_checkin			from '../static/images/btn_checkin.png'
import btn_prev_checkin		from '../static/images/btn_prev_checkin.png'
import checkin2_item_1		from '../static/images/checkin2/1.png'
import checkin2_item_2		from '../static/images/checkin2/2.png'
import checkin2_item_3		from '../static/images/checkin2/3.png'
import checkin2_item_4		from '../static/images/checkin2/4.png'
import checkin2_item_5		from '../static/images/checkin2/5.png'
import checkin2_item_6		from '../static/images/checkin2/6.png'
import checkin2_item_7		from '../static/images/checkin2/7.png'
import checkin2_item_8		from '../static/images/checkin2/8.png'
import checkin3_item_1		from '../static/images/checkin3/1.png'
import checkin3_item_2		from '../static/images/checkin3/2.png'
import checkin3_item_3		from '../static/images/checkin3/3.png'
import checkin3_item_4		from '../static/images/checkin3/4.png'
import checkin3_item_5		from '../static/images/checkin3/5.png'
import checkin3_item_6		from '../static/images/checkin3/6.png'
import checkin3_item_7		from '../static/images/checkin3/7.png'
import checkin3_item_8		from '../static/images/checkin3/8.png'
import checkin3_pic			from '../static/images/checkin3_pic.png'
import checkin4_item_1		from '../static/images/checkin4/1.png'
import checkin4_item_2		from '../static/images/checkin4/2.png'
import checkin4_item_3		from '../static/images/checkin4/3.png'
import checkin4_item_4		from '../static/images/checkin4/4.png'
import checkin4_item_5		from '../static/images/checkin4/5.png'
import checkin4_item_6		from '../static/images/checkin4/6.png'
import checkin4_item_7		from '../static/images/checkin4/7.png'
import checkin4_item_8		from '../static/images/checkin4/8.png'
import exclusive_item_1		from '../static/images/exclusive/1.png'
import exclusive_item_2		from '../static/images/exclusive/2.png'
import exclusive_item_3		from '../static/images/exclusive/3.png'
import exclusive_frame		from '../static/images/exclusive_frame.png'
import exclusive_pic		from '../static/images/exclusive_pic.png'
import exclusive_popup1		from '../static/images/exclusive/popup1.png'
import exclusive_popup2		from '../static/images/exclusive/popup2.png'
import icon_plus			from '../static/images/icon_plus.png'
import modal_close			from '../static/images/close.png'
import modal_bg				from '../static/images/modal_bg.png'
import icon_dropdown 		from '../static/images/icon_dropdown.png'



//https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/

export const Imglist = {
	bg,
	rule,
	rule_pic,
	package_item_1,
	package_item_2,
	package_item_3,
	package_item_4,
	package_item_5,
	package_item_6,
	package_item_7,
	package_item_8,
	package_item_9,
	package_popup1,
	package_frame,
	checkin_frame,
	btn_diamond,
	btn_default,
	diamond,
	checkin1_item_1,
	checkin1_item_2,
	checkin1_item_3,
	checkin1_item_4,
	checkin1_item_5,
	checkin1_item_6,
	checkin1_item_7,
	checkin1_item_8,
	checkin1_pic,
	icon_check,	
	btn_checkin,
	btn_checkin_icon,
	btn_prev_checkin,
	checkin2_item_1,
	checkin2_item_2,
	checkin2_item_3,
	checkin2_item_4,
	checkin2_item_5,
	checkin2_item_6,
	checkin2_item_7,
	checkin2_item_8,
	checkin3_item_1,
	checkin3_item_2,
	checkin3_item_3,
	checkin3_item_4,
	checkin3_item_5,
	checkin3_item_6,
	checkin3_item_7,
	checkin3_item_8,
	checkin3_pic,
	checkin4_item_1,
	checkin4_item_2,
	checkin4_item_3,
	checkin4_item_4,
	checkin4_item_5,
	checkin4_item_6,
	checkin4_item_7,
	checkin4_item_8,
	exclusive_item_1,
	exclusive_item_2,
	exclusive_item_3,
	exclusive_popup1,
	exclusive_popup2,
	exclusive_frame,
	exclusive_pic,
	icon_plus,
	modal_bg,
	modal_close,
	icon_dropdown
	
}
