import React from 'react';
import {connect} from 'react-redux';

import ModalMessage from './ModalMessage';
import ModalLoading from './ModalLoading';
import ModalSelectCharacter from './ModalSelectCharacter';
import ModalCostume  from './ModalCostume'
import ModalConfirmCheckin from './ModalConfirmCheckin'

const Modals = (props)=> (
    <>
        <ModalMessage />
        <ModalLoading />
        <ModalSelectCharacter />
        <ModalCostume/>
        <ModalConfirmCheckin/>
    </>
)

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = {  };

export default connect( mapStateToProps, mapDispatchToProps )(Modals);
