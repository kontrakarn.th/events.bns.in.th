import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';
import {Imglist} from "../../constants/Import_Images";

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="message"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <div className="contenttitle">แจ้งเตือน</div>
                <div className="contenttext" dangerouslySetInnerHTML={{__html: modal_message}} />

                <div>
                    <Btns onClick={()=>props.setValues({modal_open:''})}>
                        ตกลง
                    </Btns>
                </div>
            </ModalMessageContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 492px;
    height: 286px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist['modal_bg']});
    box-sizing: border-box;
    padding: 4% 10%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    // h3 {
    //     font-size: 30px;
    //     line-height: 1.5em;
    //     color: #563e26;
    // }
    .contenttitle{
        font-size: 21px;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 15px;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 155px;
        //height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        h3{
            font-size: 21px;  
        }
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;

const Btns = styled.div`
    background: no-repeat top center url(${Imglist['btn_default']});
    width: 95px;
    height: 45px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #fff;
    line-height: 36px;
    font-size: 20px;
    background-size: cover;
    &:hover{
        background-position: bottom center;
    }
`
