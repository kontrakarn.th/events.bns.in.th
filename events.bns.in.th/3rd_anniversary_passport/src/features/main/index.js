import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import {Imglist} from './../../constants/Import_Images';
import ModalConfirmReceive from '../modals/ModalConfirmReceive';
import ModalSelectCharacter from "../modals/ModalSelectCharacter";
import ModalCostume from "../modals/ModalCostume";
import ModalConfirmCheckin from '../modals/ModalConfirmCheckin';


class Main extends React.Component {

    actConfirm(token){
        if (token === undefined || token === ""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไม่สามารถแลกของไอเทมได้",
            });
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'redeem',token:token};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                    coin:data.data.coin,
                    itemLists:data.data.itemLists,
                });
                this.setState({
                    itemLists : data.data.itemLists,
                    questLists : data.data.questLists,
                })
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);

    }

    apiSelectCharacter(id){
        if (id === undefined || id === ""){
            return;
        }
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'select_character',id:id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selectd_char: data.data.selectd_char,
                    characters: data.data.characters,
                    coin:data.data.coin,
                    free:data.data.free,
                    itemLists:data.data.itemLists,
                    mission_score:data.data.mission_score,
                    modal_open: "",
                });
                /* this.setState({
                    itemLists : data.data.rewards,
                    questLists : data.data.quests,
                }) */
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiClaimFree(){

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'claim_free'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    coin:data.data.coin,
                    free:data.data.free,
                    itemLists:data.data.itemLists,
                    modal_open: "",
                });
                /* this.setState({
                    itemLists : data.data.rewards,
                    questLists : data.data.quests,
                }) */
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_CLAIM_FREE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true){
                /* this.setState({
                    itemLists : data.data.rewards,
                    questLists : data.data.quests,
                }) */
                this.props.setValues({
                    ...data.data,
                    // username: data.data.username,
                    // character_name: data.data.character_name,
                    // selectd_char: data.data.selectd_char,
                    // characters: data.data.characters,
                    // coin:data.data.coin,
                    // free:data.data.free,
                    // itemLists:data.data.itemLists,
                    // mission_score:data.data.mission_score,
                    modal_open: "",
                });

                // if (data.data.selected_char === false){
                //     this.props.setValues({ modal_open: "selectcharacter" });
                // }
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            active: [],
            itemLists: [],
            final_rewards:true

        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    apiUnlockPassport(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'unlock_passport'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    ...data.data,
                    modal_open:'message', 
                    modal_message: 'ซื้อแพ็คเกจ 3 year anniversary เรียบร้อยแล้ว<br/>ไอเทมส่งเข้าไปในกล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อรับของรางวัล'
                });
            }
        }
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_UNLOCK_PASSORT", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    actUnlockPassport() {
        this.props.setValues({
            modal_open:'confirm', 
            modal_message: 'ต้องการซื้อแพ็คเกจ 3 Year Anniversary?'
        }); 
    }
    apiCheckinWeek(week,day,can_checkin,is_checkin){
        if(can_checkin && !is_checkin){     
            this.setState({ showLoading: true, showModalConfirmCheckin: false });
            console.log(week,day);
            let self = this;
            let dataSend = { type: "checkin", week, day };
            let successCallback = (data) => {
                console.log('call back', data);
                if (data.status) {
                    
                    this.props.setValues({
                        ...data.data,
                        modal_open:'message', 
                        modal_message: data.message || 'Check in เรียบร้อยแล้ว<br/>ได้รับ<br/>ไอเทมส่งเข้าไปในกล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อรับของรางวัล'
                    });
                }
            };
            const failCallback = (data) => {
                console.log('fail call back', data);
                if (data.type == 'no_permission') {
                 

                }else {
                    this.props.setValues({
                        modal_open:'message',
                        modal_message:data.message,
                    });
                  
                }
            };
            apiPost("REACT_APP_API_POST_CHECKIN", this.props.jwtToken, dataSend, successCallback, failCallback);
        }
    }
    actCheckinReward(week,can_claim, has_claimed){
        if(can_claim && !has_claimed){     
            this.setState({ showLoading: true, showModalConfirmCheckin: false });
         
            let self = this;
            let dataSend = { type: "claim_weekly_reward", week };
            let successCallback = (data) => {
                console.log('call back', data);
                if (data.status) {
                    
                    this.props.setValues({
                        ...data.data,
                        modal_open:'message', 
                        modal_message: data.message || 'Check in เรียบร้อยแล้ว<br/>ได้รับ<br/>ไอเทมส่งเข้าไปในกล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อรับของรางวัล'
                    });
                }
            };
            const failCallback = (data) => {
                console.log('fail call back', data);
                if (data.type == 'no_permission') {
                 

                }else {
                    this.props.setValues({
                        modal_open:'message',
                        modal_message:data.message,
                    });
                  
                }
            };
            apiPost("REACT_APP_API_POST_CLAIM_WEEKLY_REWARD", this.props.jwtToken, dataSend, successCallback, failCallback);
        }
    }
    actCheckin(week,day,can_checkin,is_checkin,previous_checkin){
        if(!is_checkin && can_checkin){

            if(previous_checkin){
                this.props.setValues({
                    modal_open: 'confirmCheckin',
                    modal_message: 'ต้องการ check in ย้อนหลัง?',
                    modal_checkin: {week,day,can_checkin,is_checkin}
                });
            }else{
                this.apiCheckinWeek(week,day,can_checkin,is_checkin);
            }
        }

    }
    actReceive(){
        this.props.setValues({
            modal_open: 'receive',
            modal_message: 'ต้องการรับรางวัล?',
           
        }); 
    }
    actConfirmFinalReward() {
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'claim_final_reward'};
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    ...data.data,
                    modal_open:'message', 
                    modal_message: data.message
                });
            }
        }
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_CLAIM_FINAL_REWARD", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    render() {
        return (
            <PageWrapper>
                <Content>
                    {/* rule */}
                    <div className="section rule">
                        <img src={Imglist.rule}/>  
                    </div>

                    {/* package */}
                    <div className="section package">
                        <div className="wrapper">
                            <div className="header">แพ็คเกจ <span className="text-yellow">3 YEAR ANNIVERSARY</span></div>
                            <div className="list">
                            {
                                this.props.package_list && this.props.package_list.length > 0 && this.props.package_list.map((items,key)=>{
                                    return(  
                                        <div key={key} className="item">
                                            {
                                                items.popup_image !== '' &&
                                                <div className="icon_plus" onClick={()=>this.props.setValues({modal_open:'costume', modal_image: items.popup_image})}><img src={Imglist.icon_plus}/></div>
                                            }
                                            <img src={items.item}/>
                                            <div className="name">{items.name}<br/>{items.count}</div>
                                        </div>
                                    )
                                })
                            } 
                            </div>
                            {
                                (!this.props.is_unlocked && this.props.unlock_package.can_buy && !this.props.unlock_package.already_buy) ?
                                <div className="btn_diamond" onClick={()=>this.actUnlockPassport()}>30,000 <img src={Imglist.diamond}/></div>
                                :
                                <div className="btn_diamond disabled">30,000 <img src={Imglist.diamond}/></div>
                            }
                        </div>
                        
                    </div>

                    {/* checkin 1 */}
                    <div className="section checkin checkin1">
                        <div className="wrapper">
                            <div className="header">Check-in <span className="text-yellow">รับของฟรีทุกวัน</span> (1)</div>
                            <div className="list">
                            
                            {
                                this.props.checkin_image[0].map((items,key)=>{
                                    
                                    if(key == 7){
                                        let reward  = this.props.weekly_rewards[0] ||{week: 0, can_claim: false , has_claimed : false};
                                        return(  
                                            <div key={key} className="item">
                                                <img src={items.item}/>
                                                <div className="name" dangerouslySetInnerHTML={{__html: items.name}}></div>
                                                <div onClick={()=>this.actCheckinReward(reward.week,reward.can_claim, reward.has_claimed)}>
                                                   {
                                                      !reward.can_claim &&
                                                      <div className="btn_check disabled">
                                                        ไม่ตรงเงื่อนไข
                                                      </div>
                                                   } 
                                                   {
                                                      reward.can_claim && !reward.has_claimed && 
                                                        <div className="btn_check">
                                                            รับรางวัล
                                                        </div>
                                                   }
                                                   {
                                                      reward.can_claim && reward.has_claimed  &&
                                                        <div className="btn_check disabled">
                                                            รับแล้ว
                                                        </div>
                                                   }
                                                   
    
                                                </div>
                                               
                                            </div>
                                        )
                                    }else{
                                        let daily = {can_checkin:false, is_checkin:false, previous_checkin:false}; 
                                        if(this.props.checkin_list && this.props.checkin_list[0] && this.props.checkin_list[0][key] ) {
                                            daily =  this.props.checkin_list[0][key];
                                        }
                                    return(  
                                        <div key={key} className="item">
                                            <img src={items.item}/>
                                            <div className="name" dangerouslySetInnerHTML={{__html: items.name}}></div>
                                            <div onClick={()=>this.actCheckin(daily.week,daily.day,daily.can_checkin, daily.is_checkin,daily.previous_checkin)}>
                                               {
                                                  !daily.can_checkin &&
                                                  <div className="btn_check disabled">
                                                    ไม่ตรงเงื่อนไข
                                                  </div>
                                               } 
                                               {
                                                  daily.can_checkin && !daily.is_checkin &&  !daily.previous_checkin &&
                                                    <div className="btn_checkin_icon">
                                                        {/* check in */}
                                                    </div>
                                               }
                                               {
                                                  daily.can_checkin && daily.is_checkin && !daily.previous_checkin &&
                                                    <div className="btn_checkin_icon checked">
                                                        {/* check in */}
                                                    </div>
                                               }
                                                {
                                                  daily.can_checkin && !daily.is_checkin &&  daily.previous_checkin &&
                                                    <div className="btn_prev_checkin">
                                                        {/* Check in ย้อนหลัง */}
                                                    </div>
                                               }
                                                {
                                                  daily.can_checkin && daily.is_checkin &&  daily.previous_checkin &&
                                                    <div className="btn_prev_checkin checked">
                                                       {/* Check in ย้อนหลัง */}
                                                    </div>
                                               }

                                            </div>
                                           
                                            
                                        </div>
                                    )
                                        }
                                })
                            
                            } 
                            </div>
                        </div>   
                    </div>

                    {/* checkin 2 */}
                    <div className="section checkin checkin2">
                        <div className="wrapper">
                            <div className="header">Check-in <span className="text-yellow">รับของฟรีทุกวัน</span> (2)</div>
                            <div className="list">
                            
                            {
                                this.props.checkin_image[1].map((items,key)=>{
                                    
                                    if(key == 7){
                                        let reward  = this.props.weekly_rewards[1] ||{week: 1, can_claim: false , has_claimed : false};
                                        return(  
                                            <div key={key} className="item">
                                                <img src={items.item}/>
                                                <div className="name" dangerouslySetInnerHTML={{__html: items.name}}></div>
                                                <div onClick={()=>this.actCheckinReward(reward.week,reward.can_claim, reward.has_claimed)}>
                                                   {
                                                      !reward.can_claim &&
                                                      <div className="btn_check disabled">
                                                        ไม่ตรงเงื่อนไข
                                                      </div>
                                                   } 
                                                   {
                                                      reward.can_claim && !reward.has_claimed && 
                                                        <div className="btn_check">
                                                            รับรางวัล
                                                        </div>
                                                   }
                                                   {
                                                      reward.can_claim && reward.has_claimed  &&
                                                        <div className="btn_check disabled">
                                                            รับแล้ว
                                                        </div>
                                                   }
                                                   
    
                                                </div>
                                               
                                            </div>
                                        )
                                    }else{

                                        let daily = {can_checkin:false, is_checkin:false, previous_checkin:false}; 
                                        if(this.props.checkin_list && this.props.checkin_list[1] && this.props.checkin_list[1][key] ) {
                                            daily =  this.props.checkin_list[1][key];
                                        }
                                    return(  
                                        <div key={key} className="item">
                                            <img src={items.item}/>
                                            <div className="name" dangerouslySetInnerHTML={{__html: items.name}}></div>
                                            <div onClick={()=>this.actCheckin(daily.week,daily.day,daily.can_checkin, daily.is_checkin,daily.previous_checkin)}>
                                               {
                                                  !daily.can_checkin &&
                                                  <div className="btn_check disabled">
                                                    ไม่ตรงเงื่อนไข
                                                  </div>
                                               } 
                                               {
                                                  daily.can_checkin && !daily.is_checkin &&  !daily.previous_checkin &&
                                                    <div className="btn_checkin_icon">
                                                        {/* check in */}
                                                    </div>
                                               }
                                               {
                                                  daily.can_checkin && daily.is_checkin && !daily.previous_checkin &&
                                                    <div className="btn_checkin_icon checked">
                                                        {/* check in */}
                                                    </div>
                                               }
                                                {
                                                  daily.can_checkin && !daily.is_checkin &&  daily.previous_checkin &&
                                                    <div className="btn_prev_checkin">
                                                        {/* Check in ย้อนหลัง */}
                                                    </div>
                                               }
                                                {
                                                  daily.can_checkin && daily.is_checkin &&  daily.previous_checkin &&
                                                    <div className="btn_prev_checkin checked">
                                                       {/* Check in ย้อนหลัง */}
                                                    </div>
                                               }

                                            </div>
                                           
                                            
                                        </div>
                                    )
                                        }
                                })
                            
                            } 
                            </div>
                        </div>   
                    </div>

                    {/* checkin 3 */}
                    <div className="section checkin checkin3">
                        <div className="wrapper">
                            <div className="header">Check-in <span className="text-yellow">รับของฟรีทุกวัน</span> (3)</div>
                            <div className="list">
                            
                            {
                                this.props.checkin_image[2].map((items,key)=>{
                                    
                                    if(key == 7){
                                        let reward  = this.props.weekly_rewards[2] ||{week: 2, can_claim: false , has_claimed : false};
                                        return(  
                                            <div key={key} className="item">
                                                <img src={items.item}/>
                                                <div className="name" dangerouslySetInnerHTML={{__html: items.name}}></div>
                                                <div onClick={()=>this.actCheckinReward(reward.week,reward.can_claim, reward.has_claimed)}>
                                                   {
                                                      !reward.can_claim &&
                                                      <div className="btn_check disabled">
                                                        ไม่ตรงเงื่อนไข
                                                      </div>
                                                   } 
                                                   {
                                                      reward.can_claim && !reward.has_claimed && 
                                                        <div className="btn_check">
                                                            รับรางวัล
                                                        </div>
                                                   }
                                                   {
                                                      reward.can_claim && reward.has_claimed  &&
                                                        <div className="btn_check disabled">
                                                            รับแล้ว
                                                        </div>
                                                   }
                                                   
    
                                                </div>
                                               
                                            </div>
                                        )
                                    }else{
                                        let daily = {can_checkin:false, is_checkin:false, previous_checkin:false}; 
                                        if(this.props.checkin_list && this.props.checkin_list[2] && this.props.checkin_list[2][key] ) {
                                            daily =  this.props.checkin_list[2][key];
                                        }
                                    return(  
                                        <div key={key} className="item">
                                            <img src={items.item}/>
                                            <div className="name" dangerouslySetInnerHTML={{__html: items.name}}></div>
                                            <div onClick={()=>this.actCheckin(daily.week,daily.day,daily.can_checkin, daily.is_checkin,daily.previous_checkin)}>
                                               {
                                                  !daily.can_checkin &&
                                                  <div className="btn_check disabled">
                                                    ไม่ตรงเงื่อนไข
                                                  </div>
                                               } 
                                               {
                                                  daily.can_checkin && !daily.is_checkin &&  !daily.previous_checkin &&
                                                    <div className="btn_checkin_icon">
                                                        {/* check in */}
                                                    </div>
                                               }
                                               {
                                                  daily.can_checkin && daily.is_checkin && !daily.previous_checkin &&
                                                    <div className="btn_checkin_icon checked">
                                                        {/* check in */}
                                                    </div>
                                               }
                                                {
                                                  daily.can_checkin && !daily.is_checkin &&  daily.previous_checkin &&
                                                    <div className="btn_prev_checkin">
                                                        {/* Check in ย้อนหลัง */}
                                                    </div>
                                               }
                                                {
                                                  daily.can_checkin && daily.is_checkin &&  daily.previous_checkin &&
                                                    <div className="btn_prev_checkin checked">
                                                       {/* Check in ย้อนหลัง */}
                                                    </div>
                                               }

                                            </div>
                                           
                                            
                                        </div>
                                    )
                                        }
                                })
                            
                            } 
                            </div>
                        </div>   
                    </div>

                    {/* checkin 4 */}
                    <div className="section checkin checkin4">
                        <div className="wrapper">
                            <div className="header">Check-in <span className="text-yellow">รับของฟรีทุกวัน</span> (4)</div>
                            <div className="list">
                            
                            {
                                this.props.checkin_image[3].map((items,key)=>{
                                    
                                    if(key == 7){
                                        let reward  = this.props.weekly_rewards[3] ||{week: 3, can_claim: false , has_claimed : false};
                                        return(  
                                            <div key={key} className="item">
                                                <img src={items.item}/>
                                                <div className="name" dangerouslySetInnerHTML={{__html: items.name}}></div>
                                                <div onClick={()=>this.actCheckinReward(reward.week,reward.can_claim, reward.has_claimed)}>
                                                   {
                                                      !reward.can_claim &&
                                                      <div className="btn_check disabled">
                                                        ไม่ตรงเงื่อนไข
                                                      </div>
                                                   } 
                                                   {
                                                      reward.can_claim && !reward.has_claimed && 
                                                        <div className="btn_check">
                                                            รับรางวัล
                                                        </div>
                                                   }
                                                   {
                                                      reward.can_claim && reward.has_claimed  &&
                                                        <div className="btn_check disabled">
                                                            รับแล้ว
                                                        </div>
                                                   }
                                                   
    
                                                </div>
                                               
                                            </div>
                                        )
                                    }else{

                                        let daily = {can_checkin:false, is_checkin:false, previous_checkin:false}; 
                                        if(this.props.checkin_list && this.props.checkin_list[3] && this.props.checkin_list[3][key] ) {
                                            daily =  this.props.checkin_list[3][key];
                                        }
                                    return(  
                                        <div key={key} className="item">
                                            <img src={items.item}/>
                                            <div className="name" dangerouslySetInnerHTML={{__html: items.name}}></div>
                                            <div onClick={()=>this.actCheckin(daily.week,daily.day,daily.can_checkin, daily.is_checkin,daily.previous_checkin)}>
                                               {
                                                  !daily.can_checkin &&
                                                  <div className="btn_check disabled">
                                                    ไม่ตรงเงื่อนไข
                                                  </div>
                                               } 
                                               {
                                                  daily.can_checkin && !daily.is_checkin &&  !daily.previous_checkin &&
                                                    <div className="btn_checkin_icon">
                                                        {/* check in */}
                                                    </div>
                                               }
                                               {
                                                  daily.can_checkin && daily.is_checkin && !daily.previous_checkin &&
                                                    <div className="btn_checkin_icon checked">
                                                        {/* check in */}
                                                    </div>
                                               }
                                                {
                                                  daily.can_checkin && !daily.is_checkin &&  daily.previous_checkin &&
                                                    <div className="btn_prev_checkin">
                                                        {/* Check in ย้อนหลัง */}
                                                    </div>
                                               }
                                                {
                                                  daily.can_checkin && daily.is_checkin &&  daily.previous_checkin &&
                                                    <div className="btn_prev_checkin checked">
                                                       {/* Check in ย้อนหลัง */}
                                                    </div>
                                               }

                                            </div>
                                           
                                            
                                        </div>
                                    )
                                        }
                                })
                            
                            } 
                            </div>
                        </div>   
                    </div>

                    {/* exclusive */}
                    <div className="section exclusive">
                        <div className="wrapper">
                            <div className="header">รางวัลเอ็กซ์คลูซีฟ <span className="text-yellow">สำหรับคุณ</span></div>
                            <div className="list">
                           
                            {
                                this.props.exclusive_list && this.props.exclusive_list.length > 0 && this.props.exclusive_list.map((items,key)=>{
                                    return(  
                                        <div key={key} className="item">
                                            {
                                                items.popup_image !== '' &&
                                                <div className="icon_plus" onClick={()=>this.props.setValues({modal_open:'costume', modal_image: items.popup_image})}><img src={Imglist.icon_plus}/></div>
                                            }
                                            <img src={items.item}/>
                                            <div className="name">{items.name}</div>
                                        </div>
                                    )
                                })
                            } 
                            </div>
                            <p>
                                ผู้เล่นสามารถกดรับ <span className="text-yellow">ของรางวัลเอ็กซ์คลูซีฟ</span> ได้ ก็ต่อเมื่อ <br/>
                                Check-in แบบปกติทุกวันครบ 28 วัน โดยไม่ใช้ Check-in ย้อนหลัง
                            </p>
                         
                            {
                                this.props.final_reward.has_claimed == true ?
                                    <div className="btn_default inactive">รับไปแล้ว</div>
                                :
                                    (
                                        this.props.final_reward.can_claim == true ?
                                            <div className="btn_default" onClick={()=>this.actReceive()}>รับรางวัล</div>
                                        :
                                            <div className="btn_default inactive">ไม่ตรงเงื่อนไข</div>
                                    )
                            }

                        </div>   
                    </div>
              
                </Content>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={()=>this.apiUnlockPassport()}/>
                <ModalConfirmReceive actConfirm={()=>this.actConfirmFinalReward()}/>
                {/* <ModalSelectCharacter actSelectCharacter={this.apiSelectCharacter.bind(this)}/> */}
                <ModalConfirmCheckin actConfirm={()=>this.apiCheckinWeek(this.props.modal_checkin.week,this.props.modal_checkin.day,this.props.modal_checkin.can_checkin,this.props.modal_checkin.is_checkin)}/>
                <ModalCostume/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #11122d;
    width: 1105px;
    padding-top: 730px;
    text-align: center;
    position: relative;

`

const Content = styled.div `  
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    img{
        max-width: 100%;
        max-height: 100%;
        vertical-align: middle;
    }
    .text-yellow{
        color: #eac001;
    }
    .text-center{
        text-align: center;
    }
    .btn_default{
        background: url(${Imglist['btn_default']}) top center no-repeat;
        width: 89px;
        height: 42px;
        line-height: 36px;
        margin: 0 auto; 
        cursor: pointer;
        font-size: 13px;
        &:hover{
            background-position: bottom center;
        }
        &.inactive,
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);  
            cursor:auto; 
        }
    }
    .btn_check{
        background: url(${Imglist['btn_checkin']}) top center  no-repeat ;   
        width: 98px;
        height: 46px;
        line-height: 45px;
        font-size: 14px;
        margin: 0 auto;
        cursor:pointer;
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);    
        }
    }
    .btn_checkin_icon{
        background: url(${Imglist['btn_checkin_icon']}) top center  no-repeat; 
        width: 98px;
        height: 46px; 
        margin: 0 auto;
        cursor:pointer;
        &:hover{
            background-position: 0 -46px;
        }
        &.checked{
            background-position: bottom center;
            cursor: default;
        }
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);   
        }
    }
    .btn_prev_checkin{
        background: url(${Imglist['btn_prev_checkin']}) top center  no-repeat ; 
        width: 98px;
        height: 46px; 
        line-height: 15px;
        padding: 6px;
        margin: 0 auto;
        cursor:pointer;
        &:hover{
            background-position: 0 -46px;
        }
        &.checked{
            background-position: bottom center;
            cursor: default;
        }
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);   
        }
    }
    .custom-checkbox {
        position: relative;
        padding-right: 25px;
        line-height: 15px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        font-size: 14px;

        background: url(${Imglist['btn_checkin']}) top center  no-repeat ; 
        justify-content: center;
        display: flex;
        align-items: center;
        width: 98px;
        height: 46px;
        margin: 0 auto;
        &:hover{
            background-position: bottom center;
        }
        &.prev-checkin{
            background: url(${Imglist['btn_prev_checkin']}) top center  no-repeat ;  
            &.disabled{
                pointer-events: none;
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%); 
            }
        }
        & input{
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }
        & input:checked + .checkmark::after {
            content: "";
            position: absolute;
            background: url(${Imglist['icon_check']}) top center  no-repeat ; 
            display: block;
            width: 23px;
            height: 20px;
            left: 3px;
        }
        & .checkmark {
            position: absolute;
            top: 50%;
            right: 8px;
            height: 20px;
            width: 20px;
            background-color: #fff;
            border-radius: 50%;
            margin-top: -10px;
            box-shadow: 1px 1px 4px #000 inset;
        }
    }
    .section{
        color:#fff;
        padding-top: 2.2%;
        padding-bottom: 2.2%;
        position: relative;
        &__header{
            font-size: 20px;
            font-weight: bold;
            margin-bottom: 30px;
            text-align: center;
        }
        &__step1{
            text-align:center;
            padding-top: 3%;
        }
        &__step2{
            text-align: center;
            padding-top: 4%;
        }
    }
    .header{
        font-size: 20px;
    }
    .btn_diamond{
        background: url(${Imglist['btn_diamond']}) no-repeat top center;  
        width: 129px;
        height: 52px;
        margin: 0 auto;
        font-size: 18px;
        line-height: 45px;
        font-style: italic;
        color: #fff;
        text-shadow: 1px 2px 1px #000;
        cursor: pointer;
        &:hover{
            background-position: bottom center;
        } 
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%); 
            cursor: default;
        } 
    }
    .rule{
        &::after{
            content: '';
            background: url(${Imglist['rule_pic']}) no-repeat top center; 
            width: 226px;
            height: 272px;
            position: absolute;
            right: 0;
            bottom: 40px;
        }
        img{
            margin: 0 auto;
            display: block;
        }

    }
    .package{
        text-align:center;
        .wrapper{
            background: url(${Imglist['package_frame']}) no-repeat top center;
            position: relative;
            width: 888px;
            height:656px;
            margin: 0 auto;
            padding: 7% 8%;
        }
        .icon_plus{
            position: absolute;
            right: 0;
            top: 0;
            cursor:pointer;
        }
        .item{
            position: relative;
        }
        .list{
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            justify-content: center;
            margin: 4% 0;
            > div{
                padding: 1%;
                flex: 0 0 19%; 
            }
            .name{
                font-size: 12px;
                line-height: 1;
                margin: 10px 0;
                min-height: 25px;
            }
        }
       
    }
    .items{
        width: 20%;
        display:inline-block;
        vertical-align: top;
        font-size: 14px;
        padding: 15px 10px;
        color: #939393;
        text-align: center;
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%); 
            &.active{
                -webkit-filter: none;
                filter: none;  
            }
        }
        &__img{
            position:relative;  
        }
        &__name{
            min-height: 35px;
            line-height: 1.2;
            margin: 5px 0;
        }
    }
    .checkin{
        .wrapper{
            background: url(${Imglist['checkin_frame']}) no-repeat top center;
            position: relative;
            width: 888px;
            height:677px;
            margin: 0 auto;
            padding: 6% 14%;
        }
        .list{
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            justify-content: center;
            margin: 2% 0;
           > div{
                padding: 2%;
                flex: 0 0 25%; 
           }
           .name{
                font-size: 12px;
                line-height: 1;
                margin: 10px 0;
                min-height: 25px;
            }
        }
    }
    .checkin1{
        color: #fff;
        text-align: center;
        position: relative;
        
        ::after{
            content: '';
            background: url(${Imglist['checkin1_pic']}) no-repeat top center; 
            width: 260px;
            height: 505px;
            position: absolute;
            right: 0;
            bottom: 0;
        }
    }
    .btn_exchange{
        background: url(${Imglist['btn_exchange']}) no-repeat top center; 
        width:99px;
        height: 40px;
        cursor:pointer;
        margin: 0 auto;
        :hover{
            background-position: 0 -40px;
        }
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);   
        }
        &.inactive{
            background-position: bottom center;
            cursor:auto;
        }
    }
    .checkin3{
        ::after{
            content: '';
            background: url(${Imglist['checkin3_pic']}) no-repeat top center; 
            width: 251px;
            height: 681px;
            position: absolute;
            left: 0;
            bottom: -40px;
        }
    }
    .checkin4{
        .list .item:last-child .name{
            font-size: 10px;
        }
        .list .name{
            min-height: 39px;
        }
    }
    .exclusive{
        padding-bottom: 10%;
        &::after{
            content: '';
            background: url(${Imglist['exclusive_pic']}) no-repeat top center; 
            width: 290px;
            height: 561px;
            position: absolute;
            right: 0;
            bottom: 0;   
        }
        .wrapper{
            background: url(${Imglist['exclusive_frame']}) no-repeat top center;
            position: relative;
            width: 881px;
            height: 466px;
            margin: 0 auto;
            padding: 7% 14%;
           
        }
        .list{
            display: flex;
            //align-items: center;
            justify-content: center;
            margin: 6% 0 2%;
            > div{
                padding: 2%;
                flex: 0 0 20%; 
            }
            .name{
                font-size: 12px;
                line-height: 1;
                min-height: 25px;
            }
            .item{
                position: relative;
            }
            p{
                margin: 5px 0;
            }
        }
        .icon_plus{
            position: absolute;
            right: 0;
            top:0;
            cursor:pointer;
        }

`

