import { Imglist } from "../constants/Import_Images";

const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    showCharacterName: false,
    characters: [],
    username: "",
    coin: 0,
    free: 'disabled',
    character: "",
    selected_char: false,
    loginStatus: false,
    showScroll: true,
    noScroll: true,
    loading: false,
    permission: true,
    status: false,
    can_play: false,

    points: 0,
    points_text: '',
    quests: [],
    rewards: [],


    //===modal
    modal_open: "",//"loading","detail","showreward",""showreward","send","modalInput"
    modal_message: "",
    modal_item_token: "",
    modal_item_name: "",
    modal_uid: "",
    modal_get_name: "",
    modal_checkin:{week:0,day:0,can_checkin:false,is_checkin:false},
  
  


    //===event base value
    status: false,
    checkin_list: [[{}]], 
    unlock_package:{
        already_buy: false,
    },
    final_reward:{
        can_claim: false,
        has_claimed: false,
        img_key: '',
        title: '',
    },
    is_unlocked: false,
    weekly_rewards: [],


    package_list:[
        {
            item: Imglist.package_item_1,
            name: 'ปีกผีเสื้อแสงดาว',
            count: 'x1',
            popup_image: Imglist.package_popup1
        },
        {
            item: Imglist.package_item_2,
            name: 'หินโซล',
            count: 'x500',
            popup_image: ''
        },
        {
            item: Imglist.package_item_3,
            name: 'หินจันทรา',
            count: 'x50',
            popup_image: ''
        },
        {
            item: Imglist.package_item_4,
            name: 'เกล็ดสีทอง',
            count: 'x3',
            popup_image: ''
        },
        {
            item: Imglist.package_item_5,
            name: 'ดาวมังกรแดง',
            count: 'x3',
            popup_image: ''
        },
        {
            item: Imglist.package_item_6,
            name: 'ลูกแก้วมังกรสมุทร',
            count: 'x3',
            popup_image: ''
        },
        {
            item: Imglist.package_item_7,
            name: 'ลูกแก้วดาวอังคาร ขั้น 3',
            count: 'x3',
            popup_image: ''
        },
        {
            item: Imglist.package_item_8,
            name: 'ลูกแก้วดาวพุธ ขั้น 3',
            count: 'x3',
            popup_image: ''
        },
        {
            item: Imglist.package_item_9,
            name: 'ลูกแก้วดาวพฤหัส ขั้น 3',
            count: 'x3',
            popup_image: ''
        },
    ],
    // checkin1_list:[
    //     {
    //         item: Imglist.checkin1_item_1,
    //         name: 'เหรียญสวนฟ้าร้อง <br/>รุ่น 2',
    //         has_checkin: true,
    //         previous_checkin: true,
    //         has_checkin_prev: true,
    //     },
    //     {
    //         item: Imglist.checkin1_item_2,
    //         name: 'เหรียญสวนฟ้าร้อง <br/>รุ่น 2',
    //         has_checkin: false ,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin1_item_3,
    //         name: 'เหรียญสวนฟ้าร้อง <br/>รุ่น 2',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin1_item_4,
    //         name: 'เหรียญมังกรสมุทร',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin1_item_5,
    //         name: 'เหรียญมังกรสมุทร',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin1_item_6,
    //         name: 'เหรียญมังกรสมุทร',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin1_item_7,
    //         name: 'เหรียญมังกรสมุทร',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin1_item_8,
    //         name: 'ปีกแทชอน<br/>เหล็กแทชอน',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    // ],
    // checkin2_list:[
    //     {
    //         item: Imglist.checkin2_item_1,
    //         name: 'เหรียญมังกรสมุทร',
    //         has_checkin: true,
    //         previous_checkin: true,
    //         has_checkin_prev: true,
    //     },
    //     {
    //         item: Imglist.checkin2_item_2,
    //         name: 'เหรียญมังกรสมุทร',
    //         has_checkin: false,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin2_item_3,
    //         name: 'เหรียญมังกรสมุทร',
    //         has_checkin: false,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin2_item_4,
    //         name: 'เศษเกล็ดสีทอง',
    //         has_checkin: false,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin2_item_5,
    //         name: 'เศษเกล็ดสีทอง',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin2_item_6,
    //         name: 'เศษเกล็ดสีทอง',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin2_item_7,
    //         name: 'เศษเกล็ดสีทอง',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin2_item_8,
    //         name: 'หญ้าซาฮวา<br/>วิญญาณซาฮวา',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    // ],
    // checkin3_list:[
    //     {
    //         item: Imglist.checkin3_item_1,
    //         name: 'หินโซล',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin3_item_2,
    //         name: 'หินโซล',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin3_item_3,
    //         name: 'หินโซล',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin3_item_4,
    //         name: 'หินโซล',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin3_item_5,
    //         name: 'หินโซล',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin3_item_6,
    //         name: 'หินโซล',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin3_item_7,
    //         name: 'หินโซล',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin3_item_8,
    //         name: 'หญ้าซาฮวา<br/>วิญญาณซาฮวา',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    // ],
    // checkin4_list:[
    //     {
    //         item: Imglist.checkin4_item_1,
    //         name: 'หินจันทรา',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin4_item_2,
    //         name: 'หินจันทรา',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin4_item_3,
    //         name: 'หินจันทรา',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin4_item_4,
    //         name: 'หินจันทรา',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin4_item_5,
    //         name: 'หินจันทรา',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin4_item_6,
    //         name: 'หินจันทรา',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin4_item_7,
    //         name: 'หินจันทรา',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    //     {
    //         item: Imglist.checkin4_item_8,
    //         name: 'กล่องหินสถานะ ระยิบระยับ x2 <br/>กล่องหินสกิล ระยิบระยับ x1 <br/>กล่องแผ่นผนึกค่าสถานะฮงมุน ระยิบระยับ x1',
    //         has_checkin: true,
    //         previous_checkin: false,
    //         has_checkin_prev: false,
    //     },
    // ],
    checkin_list:[
        [
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false,
                previous_checkin:false ,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
   
        ] ,
        [
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false,
                previous_checkin:false ,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
   
        ]  ,
        [
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false,
                previous_checkin:false ,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
   
        ]  ,
        [
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false,
                previous_checkin:false ,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
            {
                is_checkin: false ,
                previous_checkin:false,
                is_prev_checkin: false
            },
   
        ]   
          
    ],
    checkin_image:[
        [
            {
                item: Imglist.checkin1_item_1,
                name: 'เหรียญสวนฟ้าร้อง <br/>รุ่น 2',

            },
            {
                item: Imglist.checkin1_item_2,
                name: 'เหรียญสวนฟ้าร้อง <br/>รุ่น 2',
       
            },
            {
                item: Imglist.checkin1_item_3,
                name: 'เหรียญสวนฟ้าร้อง <br/>รุ่น 2',
             
            },
            {
                item: Imglist.checkin1_item_4,
                name: 'เหรียญมังกรสมุทร',
                
            },
            {
                item: Imglist.checkin1_item_5,
                name: 'เหรียญมังกรสมุทร',
                
            },
            {
                item: Imglist.checkin1_item_6,
                name: 'เหรียญมังกรสมุทร',
                
            },
            {
                item: Imglist.checkin1_item_7,
                name: 'เหรียญมังกรสมุทร',
                
            },
            {
                item: Imglist.checkin1_item_8,
                name: 'ปีกแทชอน<br/>เหล็กแทชอน',
                
            },
        ],
        [
            {
                item: Imglist.checkin2_item_1,
                name: 'เหรียญมังกรสมุทร',
                
            },
            {
                item: Imglist.checkin2_item_2,
                name: 'เหรียญมังกรสมุทร',
                
            },
            {
                item: Imglist.checkin2_item_3,
                name: 'เหรียญมังกรสมุทร',
                
            },
            {
                item: Imglist.checkin2_item_4,
                name: 'เศษเกล็ดสีทอง',
                
            },
            {
                item: Imglist.checkin2_item_5,
                name: 'เศษเกล็ดสีทอง',
               
            },
            {
                item: Imglist.checkin2_item_6,
                name: 'เศษเกล็ดสีทอง',
               
            },
            {
                item: Imglist.checkin2_item_7,
                name: 'เศษเกล็ดสีทอง',
               
            },
            {
                item: Imglist.checkin2_item_8,
                name: 'หญ้าซาฮวา<br/>วิญญาณซาฮวา',
               
            },
        ],
        [
            {
                item: Imglist.checkin3_item_1,
                name: 'หินโซล',
              
            },
            {
                item: Imglist.checkin3_item_2,
                name: 'หินโซล',
   
            },
            {
                item: Imglist.checkin3_item_3,
                name: 'หินโซล',
       
            },
            {
                item: Imglist.checkin3_item_4,
                name: 'หินโซล',
      
            },
            {
                item: Imglist.checkin3_item_5,
                name: 'หินโซล',
         
            },
            {
                item: Imglist.checkin3_item_6,
                name: 'หินโซล',
              
            },
            {
                item: Imglist.checkin3_item_7,
                name: 'หินโซล',
           
            },
            {
                item: Imglist.checkin3_item_8,
                name: 'หญ้าซาฮวา<br/>วิญญาณซาฮวา',
              
            },
        ],
        [
            {
                item: Imglist.checkin4_item_1,
                name: 'หินจันทรา',
                
            },
            {
                item: Imglist.checkin4_item_2,
                name: 'หินจันทรา',
                
            },
            {
                item: Imglist.checkin4_item_3,
                name: 'หินจันทรา',
                
            },
            {
                item: Imglist.checkin4_item_4,
                name: 'หินจันทรา',
                
            },
            {
                item: Imglist.checkin4_item_5,
                name: 'หินจันทรา',
                
            },
            {
                item: Imglist.checkin4_item_6,
                name: 'หินจันทรา',
                
            },
            {
                item: Imglist.checkin4_item_7,
                name: 'หินจันทรา',
                
            },
            {
                item: Imglist.checkin4_item_8,
                name: 'กล่องหินสถานะ ระยิบระยับ x2 <br/>กล่องหินสกิล ระยิบระยับ x1 <br/>กล่องแผ่นผนึกค่าสถานะฮงมุน ระยิบระยับ x1',
                
            }, 
        ]
    ],
    weekly_rewards: [
        {
            can_claim: false,
            has_claimed:false,
        },
        {
            can_claim: false,
            has_claimed:false,
        },
        {
            can_claim: false,
            has_claimed:false,
        },
        {
            can_claim: false,
            has_claimed:false,
        }
    ],
    exclusive_list: [
        {
            item: Imglist.exclusive_item_1,
            name: 'ชุดเทพมังกรทอง',
            popup_image: Imglist.exclusive_popup1
        },
        {
            item: Imglist.exclusive_item_2,
            name: 'เครื่องประดับเทพมังกรทอง',
            popup_image: Imglist.exclusive_popup2
        },
        {
            item: Imglist.exclusive_item_3,
            name: 'หินสถานะผืนดิน [เจาะเกราะ]',
            popup_image: ''
        },
    ],

    //combine
    materials: [],

    //===soul values
    buy_soul_result: [],
    buy_soul_count: 0,
    buy_soul_cost: 0,


    //===history values
    items_history: [],

    //===trade values
    exchangeinfo: {
        amt: 0,
        icon: "",
        id: 0,
        key: "",
        product_title: ""
    },

    //===send item
    target_uid: -1,
    target_username: "",
    my_bag: [
        {id: 1, title: "ชุดจันทราสีเงิน", icon: "S_Moon_01_Base", key: "S_Moon_01", quantity: 0},
        {id: 2, title: "เครื่องประดับจันทราสีเงิน", icon: "S_Moon_02_Base", key: "S_Moon_02", quantity: 0},
        {id: 3, title: "ต่างหูจันทราสีเงิน", icon: "S_Moon_04_Base", key: "S_Moon_04", quantity: 0},
        {id: 4, title: "ทรงผมจันทราสีเงิน", icon: "S_Moon_03_Base", key: "S_Moon_03", quantity: 0},
        {id: 5, title: "เครื่องประดับนักเรียนเกาหลียุค 80", icon: "S_Moon_07_Base", key: "S_Moon_07", quantity: 0},
        {id: 6, title: "หมวกนักเรียนเกาหลียุค 80", icon: "S_Moon_08_Base", key: "S_Moon_08", quantity: 0},
        {id: 7, title: "ชุดนักเรียนเกาหลียุค 80", icon: "S_Moon_06_Base", key: "S_Moon_06", quantity: 0},
        {id: 8, title: "หินสัตว์เลี้ยงสาวน้อยจากจันทรา", icon: "S_Moon_05_Base", key: "S_Moon_05", quantity: 0},
        {id: 9, title: "เซ็ทเหมียวจันทราสีเงิน", icon: "S_Moon_09_Base", key: "S_Moon_09", quantity: 0},
        {id: 10, title: "เซ็ทนักเรียนเกาหลียุค 80", icon: "S_Moon_10_Base", key: "S_Moon_10", quantity: 0},
        {id: 11, title: "กล่องอาวุธลวงตาจันทราสีเงิน", icon: "S_Moon_11_Base", key: "S_Moon_11", quantity: 0},
    ],
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         :
            return ({...state, sessionKey: action.value});
        case 'ACCOUNT_AUTHED'       :
            return ({...state, userData: action.value});
        case 'RECEIVE_SESSION_KEY'  :
            return ({...state, sessionKey: action.value});
        case 'SET_LOGIN_URL'        :
            return ({...state, loginUrl: action.value});
        case 'SET_LOGOUT_URL'       :
            return ({...state, logoutUrl: action.value});
        case 'ACCOUNT_LOGIN'        :
            return ({...state, authState: "LOGGED_IN"});
        case 'ACCOUNT_LOGOUT'       :
            return ({...state, authState: "LOGGED_OUT"});
        case 'ACCOUNT_AUTH_CHECKED' :
            return ({...state, authed: true});
        case 'SET_JWT_TOKEN'        :
            return ({...state, jwtToken: action.value});

        case "SET_VALUE"            :
            return (Object.keys(defaultState).indexOf(action.key) >= 0) ? {
                ...state,
                [action.key]: action.value
            } : state;
        case "SET_VALUES"           :
            return {...state, ...action.value};
        default:
            return state;
    }
}

export const onAccountAuth = (sessionKey) => ({type: 'ACCOUNT_AUTH', value: sessionKey});
export const onAccountAuthed = (userData) => ({type: 'ACCOUNT_AUTHED', value: userData});
export const onAccountLogin = () => ({type: 'ACCOUNT_LOGIN', value: ""});
export const onAccountLogout = () => ({type: 'ACCOUNT_LOGOUT', value: ""});
export const receiveSessionKey = (sessionKey) => ({type: 'RECEIVE_SESSION_KEY', value: sessionKey});
export const setLoginUrl = (url) => ({type: 'SET_LOGIN_URL', value: url});
export const setLogoutUrl = (url) => ({type: 'SET_LOGOUT_URL', value: url});
export const onAccountAuthChecked = () => ({type: 'ACCOUNT_AUTH_CHECKE', value: true})
export const setJwtToken = (token) => ({type: 'SET_JWT_TOKEN', value: token});

export const setValue = (key, value) => ({type: "SET_VALUE", value, key});
export const setValues = (value) => ({type: "SET_VALUES", value});

