import bg 			from '../static/images/background.png';

import modal_bg 	from '../static/images/modal_bg.png'
import btn_default 	from '../static/images/btn_default.png'

import content1 	from '../static/images/content1.png'
import content2 	from '../static/images/content2.png'
import content3 	from '../static/images/content3.png'
import content4		from '../static/images/content4.png'
import content5		from '../static/images/content5.png'
import btn_prev 	from '../static/images/btn_prev.png'
import btn_next 	from '../static/images/btn_next.png'

import btn_package	from '../static/images/btn_package.png'
import checkin1_1 	from  '../static/images/checkin1/1.png'
import checkin1_2 	from  '../static/images/checkin1/2.png'
import checkin1_3 	from  '../static/images/checkin1/3.png'
import checkin1_4 	from  '../static/images/checkin1/4.png'
import checkin1_5 	from  '../static/images/checkin1/5.png'
import checkin1_6	from  '../static/images/checkin1/6.png'
import checkin1_7 	from  '../static/images/checkin1/7.png'
import checkin1_special	from  '../static/images/checkin1/special.png'
import checkin2_1 	from  '../static/images/checkin2/1.png'
import checkin2_2 	from  '../static/images/checkin2/2.png'
import checkin2_3 	from  '../static/images/checkin2/3.png'
import checkin2_4 	from  '../static/images/checkin2/4.png'
import checkin2_5 	from  '../static/images/checkin2/5.png'
import checkin2_6	from  '../static/images/checkin2/6.png'
import checkin2_7 	from  '../static/images/checkin2/7.png'
import checkin2_special			from  '../static/images/checkin2/special.png'
import icon_plus				from '../static/images/icon_plus.png'
import btn_checkin				from '../static/images/btn_checkin.png'
import btn_checkin_past			from '../static/images/btn_checkin_past.png'
import chracter_checkin1		from '../static/images/chracter_checkin1.png'
import icon_check				from '../static/images/icon_check.png'
import special_reward1			from '../static/images/special_reward/1.png'
import special_reward2			from '../static/images/special_reward/2.png'
import character_special_reward	from '../static/images/character_special_reward.png'
import modal_bg_character		from '../static/images/modal_bg_character.png'
import btn_close				from '../static/images/btn_close.png'
import example_pet				from '../static/images/example_pet.png'
import example_special_reward	from '../static/images/example_special_reward.png'
import example_moon				from '../static/images/example_moon.png'
import weapon_reward				from '../static/images/weapon_reward.png'

//https://cdngarenanow-a.akamaihd.net/webth/bns/events/lantern_festival

export const Imglist = {
	bg,
	modal_bg,
	btn_default,
	content1,
	content2,
	content3,
	content4,
	content5,
	btn_prev,
	btn_next,

	btn_package,
	checkin1_1,
	checkin1_2,
	checkin1_3,
	checkin1_4,
	checkin1_5,
	checkin1_6,
	checkin1_7,
	checkin1_special,
	checkin2_1,
	checkin2_2,
	checkin2_3,
	checkin2_4,
	checkin2_5,
	checkin2_6,
	checkin2_7,
	checkin2_special,
	icon_plus,
	btn_checkin,
	btn_checkin_past,
	chracter_checkin1,
	icon_check,
	special_reward1,
	special_reward2,
	character_special_reward,
	modal_bg_character,
	btn_close,
	example_pet,
	example_special_reward,
	example_moon,
	weapon_reward,
}
