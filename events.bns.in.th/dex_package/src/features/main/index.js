import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import {Imglist} from './../../constants/Import_Images';
import ModalConfirmReceive from '../modals/ModalConfirmReceive';
import ModalCharacter from "../modals/ModalCharacter";

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div className={className} onClick={onClick}><img src={Imglist.btn_next}/></div>
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div className={className} onClick={onClick}><img src={Imglist.btn_prev}/></div>
    );
}

class Main extends React.Component {

    actConfirm(){

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'buy_dex_package'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content,
                    modal_open: "message",
                    modal_message: data.message,
                });
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_BUY_DEX_PACKAGE", this.props.jwtToken, dataSend, successCallback, failCallback);

    }

    actCheckIn(week,id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'checkin',week:week,checkin_id:id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content,
                    modal_open: "message",
                    modal_message: data.message,
                });
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_CHECKIN", this.props.jwtToken, dataSend, successCallback, failCallback);

    }

    actClaimSpecialReward(week){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'claim_special_reward',week:week};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content,
                    modal_open: "message",
                    modal_message: data.message,
                });
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_CLAIM_SPECIAL_REWARD", this.props.jwtToken, dataSend, successCallback, failCallback);

    }

    actClaimFinalReward(week){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'claim_final_reward'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content,
                    modal_open: "message",
                    modal_message: data.message,
                });
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_CLAIM_FINAL_REWARD", this.props.jwtToken, dataSend, successCallback, failCallback);

    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    getEventInfo(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true){

                this.props.setValues({
                    ...data.content,
                    modal_open: "",
                });

            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,

            active: [],
            showDataCharacter: [],
            unlock_package: true,
            checkinList1: [
                {
                    image: Imglist.checkin1_1,
                    name: 'เหล็กแทชอน',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin1_2,
                    name: 'กล่องหินสกิลที่ส่องสว่าง',
                    has_checkin: true,
                    previous_checkin: true,
                    has_checkin_prev: true,
                },
                {
                    image: Imglist.checkin1_3,
                    name: 'หินจันทรา',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin1_4,
                    name: 'หินจันทรา',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin1_5,
                    name: 'หินจันทรา',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin1_6,
                    name: 'หินจันทรา',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin1_7,
                    name: 'เกล็ดสีคราม',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin1_special,
                    name: 'กล่องอาวุธลวงตาเล่ห์มายา หินสัตว์เลี้ยงวิคตอเรีย',
                    type: 'special',
                    can_claim: true,
                    has_claim: false,
                    popup_character: [Imglist.example_pet,Imglist.weapon_reward],
                    popup_character_title: ['ตัวอย่างสัตว์เลี้ยงวิคตอเรีย',''],
                },
            ],
            checkinList2: [
                {
                    image: Imglist.checkin2_1,
                    name: 'เหล็กแทชอน',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin2_2,
                    name: 'กล่องหินสกิลที่ส่องสว่าง',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin2_3,
                    name: 'หินโซล',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin2_4,
                    name: 'หินโซล',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin2_5,
                    name: 'หินโซล',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin2_6,
                    name: 'หินโซล',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin2_7,
                    name: 'เกล็ดสีคราม',
                    has_checkin: true,
                    previous_checkin: false,
                    has_checkin_prev: false,
                },
                {
                    image: Imglist.checkin2_special,
                    name: 'ชุดจันทร์เสี้ยว เสน่ห์ฮวันดัน',
                    type: 'special',
                    can_claim: true,
                    has_claim: false,
                    popup_character: Imglist.example_moon,
                    popup_character_title: 'ตัวอย่างชุดจันทร์เสี้ยว',
                },
            ],
            special_reward:[
                {
                    image: Imglist.special_reward1,
                    name: 'หินสัตว์เลี้ยงวิคตอเรีย',
                    show_details: true,
                    popup_character: Imglist.example_pet,
                    popup_character_title: 'ตัวอย่างสัตว์เลี้ยงวิคตอเรีย',
                },
                {
                    image: Imglist.special_reward2,
                    name: 'ปีกทูตแห่งราคะ',
                    show_details: true,
                    popup_character: Imglist.example_special_reward,
                    popup_character_title: 'ตัวอย่างปีกทูตแห่งราคะ',

                }
            ],
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.getEventInfo();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.getEventInfo();
        }

    }
    showDetails(args){
        // console.log(args)
        this.setState({
            showDataCharacter: args
        })
        this.props.setValues({modal_open:'character', modal_character: args})
    }
    render() {

        let result_list_1 = this.props.checkin_list[0].list
        let result_list_2 = this.props.checkin_list[1].list
        const settings = {
            dots: true,
            infinite: true,
            fade: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        return (
            <PageWrapper>
                <Content>
                    {/* section 1 */}
                    <div className="section section1">
                      <img src={Imglist.content1}/>
                    </div>

                    {/* section 2 */}
                    <div className="section section2">
                        <img src={Imglist.content2}/>
                        <div className={"btn btn_package " + (this.props.can_buy_pacakge==false || this.props.unlock_package==true ? " inactive" : "")}
                        onClick={this.props.can_buy_pacakge ? ()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการซื้อแพ็คเกจ?'}) : ()=>{}}></div>
                    </div>

                    {/* section 3 */}
                    <div className="section section3">

                        <div className="checkinList">
                            {
                                this.state.checkinList1.map((items,key)=>{
                                    return(
                                        <div key={key} className="checkinList_items">

                                            <img src={items.image}/>
                                            {
                                                items.type == "special" &&
                                                <div className="btn_view_character" onClick={()=>this.props.setValues({modal_open:'character', modal_character: [{image:items.popup_character[0], title:items.popup_character_title[0]},{image:items.popup_character[1], title:items.popup_character_title[1]}] })}><img src={Imglist.icon_plus}/></div>
                                            }

                                            <div className="name">{result_list_1 && result_list_1[key] && result_list_1[key].title}</div>
                                            {
                                                this.props.unlock_package == true ?
                                                    (
                                                        result_list_1[key].type == "special" ?
                                                            result_list_1[key].has_claim == true ?
                                                                <div className="btn btn_checkin inactive">รับไปแล้ว</div>
                                                            :
                                                                (
                                                                    result_list_1[key].can_claim == true ?
                                                                        <div className="btn btn_checkin" onClick={()=>this.actClaimSpecialReward(1)}>รับรางวัล</div>
                                                                    :
                                                                        <div className="btn btn_checkin inactive">ไม่ตรงเงื่อนไข</div>
                                                                )
                                                        :
                                                            (
                                                                result_list_1[key].has_checkin == true ?
                                                                    result_list_1[key].previous_checkin == true && result_list_1[key].has_checkin_prev == true ?
                                                                    <div className="btn btn_checkin_past" >
                                                                        <label class="custom-checkbox">
                                                                            Checkin ย้อนหลัง
                                                                            <input type="checkbox" disabled="disabled" checked="checked" />
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                    </div>
                                                                    :
                                                                    <div className="btn btn_checkin">
                                                                        <label class="custom-checkbox">
                                                                            Checkin
                                                                            <input type="checkbox" disabled="disabled" checked="checked" />
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                    </div>
                                                                :
                                                                    (
                                                                        result_list_1[key].previous_checkin == true ?
                                                                            result_list_1[key].can_checkin == true ?
                                                                                <div className="btn btn_checkin_past">
                                                                                    <label class="custom-checkbox">
                                                                                        Checkin ย้อนหลัง
                                                                                        <input type="checkbox" onChange={()=>this.actCheckIn(1,(key+1))} />
                                                                                        <span class="checkmark"></span>
                                                                                    </label>
                                                                                </div>
                                                                            :
                                                                            <div className="btn btn_checkin inactive">ไม่ตรงเงื่อนไข</div>
                                                                        :
                                                                            (
                                                                                result_list_1[key].can_checkin == true ?
                                                                                    <div className="btn btn_checkin" >
                                                                                        <label class="custom-checkbox" >
                                                                                            Checkin
                                                                                            <input type="checkbox" onChange={()=>this.actCheckIn(1,(key+1))}/>
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                    </div>
                                                                                :
                                                                                <div className="btn btn_checkin inactive">ไม่ตรงเงื่อนไข</div>
                                                                            )
                                                                    )
                                                            )
                                                    )
                                                :
                                                    <div className="btn btn_checkin inactive">ไม่ตรงเงื่อนไข</div>
                                            }
                                        </div>
                                    )
                                })
                            }
                        </div>

                    </div>

                    {/* section 4 */}
                    <div className="section section4">
                        <div className="checkinList">
                            {
                                this.state.checkinList2.map((items,key)=>{
                                    return(
                                        <div key={key} className="checkinList_items">
                                            <img src={items.image}/>
                                            {
                                                items.type == "special" &&
                                                <div className="btn_view_character" onClick={()=>this.props.setValues({modal_open:'character', modal_character: [{image:items.popup_character, title:items.popup_character_title}] })}><img src={Imglist.icon_plus}/></div>
                                            }

                                            <div className="name">{result_list_2 && result_list_2[key] && result_list_2[key].title}</div>
                                            {
                                                this.props.unlock_package == true ?
                                                    (
                                                        result_list_2[key].type == "special" ?
                                                            result_list_2[key].has_claim == true ?
                                                                <div className="btn btn_checkin inactive">รับไปแล้ว</div>
                                                            :
                                                                (
                                                                    result_list_2[key].can_claim == true ?
                                                                        <div className="btn btn_checkin" onClick={()=>this.actClaimSpecialReward(2)}>รับรางวัล</div>
                                                                    :
                                                                        <div className="btn btn_checkin inactive">ไม่ตรงเงื่อนไข</div>
                                                                )
                                                        :
                                                            (
                                                                result_list_2[key].has_checkin == true ?
                                                                    result_list_2[key].previous_checkin == true && result_list_2[key].has_checkin_prev == true ?
                                                                    <div className="btn btn_checkin_past">
                                                                        <label class="custom-checkbox">
                                                                            Checkin ย้อนหลัง
                                                                            <input type="checkbox" disabled="disabled" checked="checked"/>
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                    </div>
                                                                    :
                                                                    <div className="btn btn_checkin">
                                                                        <label class="custom-checkbox">
                                                                            Checkin
                                                                            <input type="checkbox" disabled="disabled" checked="checked" />
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                    </div>
                                                                :
                                                                    (
                                                                        result_list_2[key].previous_checkin == true ?
                                                                            result_list_2[key].can_checkin == true ?
                                                                                <div className="btn btn_checkin_past">
                                                                                    <label class="custom-checkbox">
                                                                                        Checkin ย้อนหลัง
                                                                                        <input type="checkbox" onChange={()=>this.actCheckIn(2,(key+1))}/>
                                                                                        <span class="checkmark"></span>
                                                                                    </label>
                                                                                </div>
                                                                            :
                                                                            <div className="btn btn_checkin inactive">ไม่ตรงเงื่อนไข</div>
                                                                        :
                                                                            (
                                                                                result_list_2[key].can_checkin == true ?
                                                                                    <div className="btn btn_checkin">
                                                                                        <label class="custom-checkbox">
                                                                                            Checkin
                                                                                            <input type="checkbox" onChange={()=>this.actCheckIn(2,(key+1))}/>
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                    </div>
                                                                                :
                                                                                <div className="btn btn_checkin inactive">ไม่ตรงเงื่อนไข</div>
                                                                            )
                                                                    )
                                                            )
                                                    )
                                                :
                                                    <div className="btn btn_checkin inactive">ไม่ตรงเงื่อนไข</div>
                                            }
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>

                {/* section 5 */}
                <div className="section section5">
                        <div className="checkinList">
                            {
                                this.state.special_reward.map((items,key)=>{
                                    return(
                                        <div key={key} className="checkinList_items">
                                            <img src={items.image}/>
                                            {
                                                items.show_details &&
                                                <div className="btn_view_character" onClick={()=>this.props.setValues({modal_open:'character', modal_character: [{image:items.popup_character, title:items.popup_character_title}] })}><img src={Imglist.icon_plus}/></div>
                                            }
                                            <div className="name">{items.name}</div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        <p className="detail_box">
                            ผู้เล่นสามารถกดรับ <span className="text-yellow">ของรางวัลเอ็กซ์คลูซีฟ</span> ได้ ก็ต่อเมื่อ <br/>
                            Check-in แบบปกติทุกวันครบ 14 วัน โดยไม่ใช้ Check-in ย้อนหลัง
                        </p>
                        {
                            this.props.final_rewards.has_claim==false && this.props.final_rewards.can_claim==false
                            ?
                                <div className="btn btn_checkin inactive">ไม่ตรงเงื่อนไข</div>
                            :
                                this.props.final_rewards.can_claim==true && this.props.final_rewards.has_claim==false
                                ?
                                    <div className="btn btn_checkin" onClick={()=>this.actClaimFinalReward()}>รับรางวัล</div>
                                :
                                    <div className="btn btn_checkin inactive">รับรางวัลแล้ว</div>
                        }
                    </div>
                </Content>
                <ModalLoading />
                <ModalCharacter />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                <ModalConfirmReceive actConfirm={this.actConfirm.bind(this)}/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #11122d;
    width: 1105px;
    padding-top: 740px;
    text-align: center;
`

const Content = styled.div `
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    img{
        max-width: 100%;
        max-height: 100%;
    }
    .text-yellow{
        color: #ffd200;
    }
    .text-center{
        text-align: center;
    }
    .btn_checkin{
        background: url(${Imglist['btn_checkin']}) no-repeat top center;
        display: inline-block;
        width: 100px;
        height: 46px;
        line-height: 38px;
        &:hover{
            background-position: bottom center;
        }
        &.inactive{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
            cursor:default;

        }
    }
    .btn_checkin_past{
        background: url(${Imglist['btn_checkin_past']}) no-repeat top center;
        display: inline-block;
        width: 100px;
        height: 46px;
        &:hover{
            background-position: bottom center;
        }
    }
    .custom-checkbox {
        display: flex;
        position: relative;
        padding-right: 20px;
        cursor: pointer;
        line-height: 1;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        justify-content: center;
        align-items: center;
        height: 90%;
        font-size: 14px;
        input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            &.checked{
                content:'';
                background: url(${Imglist['icon_check']}) no-repeat top center;
                position: absolute;
                right: -5px;
                top: 0px;
                width: 24px;
                height: 20px;
            }
        }
        input:checked ~ .checkmark {
            &::after {
                content:'';
                background: url(${Imglist['icon_check']}) no-repeat top center;
                position: absolute;
                right: -5px;
                top: 0px;
                width: 24px;
                height: 20px;
            }
        }
        .checkmark {
            position: absolute;
            top: 10px;
            right: 7px;
            height: 20px;
            width: 20px;
            background-color: #fff;
            border-radius: 50%;
            box-shadow: 1px 1px 4px rgba(0,0,0,0.8) inset;
        }
    }

    & .section{
        color:#fff;
        padding-bottom: 6%;
        &__header{
            font-size: 20px;
            font-weight: bold;
            margin-bottom: 30px;
            text-align: center;
        }
        &__step1{
            text-align:center;
            padding-top: 3%;
        }
        &__step2{
            text-align: center;
            padding-top: 4%;
        }
    }
    & .section1{
        text-align: center;
        font-size: 15px;
        position: relative;
        padding-top:3%;
        &::after{
            content: '';
            background: url(${Imglist['character1']}) no-repeat top center;
            width: 270px;
            height: 680px;
            position: absolute;
            right: 0;
            top: 28%;
        }
        & .quest__title{
            margin: 50px 0 25px 0;
        }
        & .questItems{
            display: inline-block;
            vertical-align: top;
            text-align: center;
            padding: 10px 20px;
            position: relative;
            &__points{
                text-align: center;
                position: absolute;
                right: 10%;
                top: 50%;
                font-size: 18px;
                line-height: 1.5;
                min-width: 28px;
            }
            &.disabled{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);
            }
        }
        & ol{
            padding-left: 15px;
        }
    }
    & .section2{
        //background: url(${Imglist['content2']}) no-repeat top center;
        //padding: 10% 16% 20% 16%;
        z-index: 1;
        position: relative;
        text-align:center;
        &::after{
            content: '';
            background: url(${Imglist['character2']}) no-repeat top center;
            width: 365px;
            height: 870px;
            position: absolute;
            right: 0;
            top: 72%;
        }
    }
    & .items{
        width: 20%;
        display:inline-block;
        vertical-align: top;
        font-size: 14px;
        padding: 15px 10px;
        color: #939393;
        text-align: center;
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
            pointer-events:none;
            &.active{
                -webkit-filter: none;
                filter: none;
                pointer-events:auto;
            }
        }
        &__img{
            position:relative;
        }
        &__name{
            min-height: 35px;
            line-height: 1.2;
            margin: 5px 0;
        }
    }

    & .btn{
        cursor:pointer;
        &_package{
            background: no-repeat top center url(${Imglist['btn_package']});
            width: 130px;
            height: 52px;
            position: absolute;
            bottom: 18%;
            left:0;
            right: 0;
            margin: 0 auto;
            &:hover{
                background-position: bottom center;
            }
            &.inactive{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);
                pointer-events:none;
            }
        }
    }
    .section3{
        background: top center no-repeat url(${Imglist['content3']});
        color: #fff;
        text-align: center;
        padding: 11% 23% 7%;
        z-index: 1;
        position: relative;
        &::after{
            content: '';
            background: top center no-repeat url(${Imglist['chracter_checkin1']});
            position: absolute;
            left:0;
            bottom: 20%;
            width: 1040px;
            height: 465px;
        }
    }
    .checkinList{
        display: flex;
        flex-wrap: wrap;
        position: relative;
        justify-content: center;
        z-index: 1;
        .checkinList_items{
            width: 25%;
            margin-bottom: 6%;
            position: relative;
        }
        .btn_view_character{
            position: absolute;
            right: 18px;
            top: 10px;
            cursor:pointer;
        }
        .name{
            font-size: 13px;
            line-height: 1.3;
            color: #939393;
            min-height: 42px;
        }
    }
    .section4{
        background: top center no-repeat url(${Imglist['content4']});
        color: #fff;
        text-align: center;
        padding: 11% 23% 7%;
        z-index: 1;
        position: relative;
    }
    .section5{
        background: top center no-repeat url(${Imglist['content5']});
        color: #fff;
        text-align: center;
        padding: 13% 22% 12%;
        z-index: 1;
        position: relative;
        &::after{
            content: '';
            background: top center no-repeat url(${Imglist['character_special_reward']});
            width: 415px;
            height: 575px;
            position: absolute;
            bottom:0;
            left:0;
        }
        .detail_box{
            margin-top:-5mm;
            margin-bottom:2.5em;
        }
        .checkinList .btn_view_character{
            top: -12px;
        }
    }
`
