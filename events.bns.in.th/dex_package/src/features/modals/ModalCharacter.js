import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';
import iconDropdown from '../../static/images/icon_dropdown.png'
import { apiPost } from '../../middlewares/Api';
import {Imglist} from './../../constants/Import_Images';
import Slider from "react-slick";
const slider_image = [
    {
        image: Imglist.slide1,
        title: 'ชุดราชันสมรภูมิ และ เครื่องประดับราชันสมรภูมิ'
    },
    {
        image: Imglist.slide2,
        title: 'ชุดผู้พิพากษา และ สัญลักษณ์ผู้พิพากษา'
    }
]
const CPN = props => {
    const actSelectCharacter=(e)=>{
        e.preventDefault();
        let indexCharacter = props.value_select;
        if(indexCharacter !== "" ){
            props.apiSelectCharacter(indexCharacter)
        }else{
            props.setValues({
                modal_open:"message",
                modal_message: "โปรดเลือกตัวละคร",
                selected_char:false,
            });
        }
    }

    function handleChange(e) {
        props.setValues({
            value_select : e.target.value,
        })
    }

    function SampleNextArrow(props) {
        const { className, style, onClick } = props;
        return (
          <div className={className} onClick={onClick}><img src={Imglist.btn_next}/></div>
        );
    }

    function SamplePrevArrow(props) {
        const { className, style, onClick } = props;
        return (
            <div className={className} onClick={onClick}><img src={Imglist.btn_prev}/></div>
        );
    }
    const settings = {
        dots: false,
        infinite: true,
        fade: false,
        //initialSlide : 1,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />
    };


    return (

        <ModalCore
            modalName="character"
            actClickOutside={false}
        >
            <ModalMessageContent>
                <div className="btn_close" onClick={()=>props.setValues({modal_open:''})}>
                    <img src={Imglist.btn_close}/>
                </div>

                <Slider {...settings}>
                    {
                        props.modal_character && props.modal_character.map((items,key)=>{
                            return(
                                <div key={key} className="">
                                    <div>{items.title}</div>
                                    <img src={items.image}/>
                                </div>
                            )
                        })
                    }
                </Slider>
            </ModalMessageContent>
        </ModalCore>
    )
}


const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ModalMessageContent = styled.div`
font-family: 'Kanit-Light';
    position: relative;
    display: block;
    width: 900px;
    //height: 630px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist['modal_bg_character']});
    background-size: 100% 100%;
    box-sizing: border-box;
    margin: 0 auto;
    padding: 3%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .test{
        width: 40%;
        float:left;
        img{
            max-width: 100%;
        }
    }
    .contenttitle{
        font-size: 21px;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 21px;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 180px;
        //height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .btn_close{
        position: absolute;
        right: 0;
        top: -33px;
        cursor:pointer;
    }
    .titletext{
        position: absolute;
        font-family: 'Kanit-Light';
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
    .slick-slide img{
        margin: 0 auto;
    }
    .slick-dots li.slick-active button:before {
        opacity: 1;
        color: #fff;
    }
    .slick-dots li button:before{
        font-size: 11px;
        opacity: 1;
        color: #646464;
    }
    .slick-prev{
        left:30px;
    }
    .slick-next{
        right:30px;
    }
    .slick-prev,
    .slick-next{
        width: auto;
        height: auto;
        z-index: 1;
        &::before{
            font-size:0;
        }
    }
`;
