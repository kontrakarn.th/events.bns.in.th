import React from 'react';
import {connect} from 'react-redux';

import ModalMessage from './ModalMessage';
import ModalLoading from './ModalLoading';
import ModalCharacter from './ModalCharacter';

const Modals = (props)=> (
    <>
        <ModalMessage />
        <ModalLoading />
        <ModalCharacter />
    </>
)

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = {  };

export default connect( mapStateToProps, mapDispatchToProps )(Modals);
