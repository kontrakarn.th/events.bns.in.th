import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './redux';

import modal_bg from './images/modal_bg.png';
import title_text from './images/title_text.png';
import confirm_btn from './images/confirm_title.png';
import cancel_btn from './images/cancel_btn.png';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="confirm"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <img className="titletext" src={title_text} alt=""/>
                <div className="contenttext" dangerouslySetInnerHTML={{__html: modal_message}} />
            </ModalMessageContent>
            <ModalBottom>
                    <Btns src={confirm_btn} onClick={()=>{if(props.actConfirm) props.actConfirm()}}/>
                    <Btns src={cancel_btn} onClick={()=>props.setValues({modal_open:''})}/>
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.ModalReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 340px;
    height: 352px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${modal_bg});
    box-sizing: border-box;
    padding: 5%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttext{
        color: #ffcdae;
        font-family: 'Kanit-Medium';
        font-size: 24px;
        line-height: 1.5em;
        word-break: break-word;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        display: block;
        margin: -9% auto;
    }
`;
const ModalBottom = styled.div`
    display: block;
    margin: -30px auto 0;
    position: relative;
    z-index: 50;
`;

const Btns = styled.img`
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
`
