import React from 'react';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';

import { onAccountLogout } from './../../actions/AccountActions';
import { setValues } from './redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalShowItem from '../../features/modals/ModalShowItem';
import {Imglist} from './../../constants/Import_Images';

class Main extends React.Component {
    actConfirm(){
        this.apiRedeem(
            this.props.modal_confirmitem && this.props.modal_confirmitem !== ''
            && this.props.modal_confirmitem
        );
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.response,
                    modal_open: "",
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CHECKIN", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiRedeem(type_id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type_id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content.response,
                    modal_open:"message",
                    modal_message: "ส่งของรางวัลสำเร็จ<br/>กรุณาตรวจสอบจดหมายภายในเกม"
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            modalConfirmToRedeem:false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }

    render() {
        // 'item_1','item_2'
        let maglist = ['item_1','item_3','item_4','item_5','item_4','item_5','item_6','item_7','item_7','item_8'];
        return (
            <MainWrapper>
                <div className="board_1">
                        <img src={Imglist['board_1']} alt=""/>
                        <MagnifyIcon className={'board1__p1'} src={Imglist['magnify_icon']}  onClick={()=>this.props.setValues({modal_open: 'item',modal_item:'item_1'})}/>
                        <MagnifyIcon className={'board1__p2'} src={Imglist['magnify_icon']}  onClick={()=>this.props.setValues({modal_open: 'item',modal_item:'item_2'})}/>

                        <RewardBtn pass={this.props.condition_1_pass} received={this.props.condition_1_received} className='board1' onClick={()=>this.props.setValues({modal_open: 'confirm',modal_confirmitem: 1,modal_message:'ยืนยันการรับของขวัญ'})}/>
                </div>
                <div className="board_2">
                        <img src={Imglist['board_2']} alt="" onLoad={()=>this.setState({showBoard:true})}/>
                        {this.state.showBoard &&
                            <>
                            {
                                maglist && maglist.map((item,index) => {
                                    return (
                                        <MagnifyIcon
                                            key={`board_2_pos_${index+1}`}
                                            className={`board2__p${index+1}`}
                                            src={Imglist['magnify_icon']}
                                            onClick={()=>this.props.setValues({modal_open: 'item',modal_item:item})}
                                            />
                                     )
                                })
                            }
                        <RewardBtn pass={this.props.condition_2_pass} received={this.props.condition_2_received} className='board2' onClick={()=>this.props.setValues({modal_open: 'confirm',modal_confirmitem: 2,modal_message:'ยืนยันการรับของขวัญ'})}/>
                        </>
                        }
                </div>
                <div className="board_3">
                    <div className="board_3_character">
                        <img src={Imglist['board_3_character']} alt=""/>
                    </div>
                    <img src={Imglist['board_3']} alt=""/>
                    <RewardBtn pass={this.props.condition_3_pass} received={this.props.condition_3_received} className='board3' onClick={()=>this.props.setValues({modal_open: 'confirm',modal_confirmitem: 3,modal_message:'ยืนยันการรับของขวัญ'})}/>
                </div>
                <div className="board_4">
                    <img src={Imglist['board_4']} alt=""/>
                </div>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                <ModalShowItem/>
            </MainWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const MainWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']});
    width: 1105px;
    padding-top: 630px;
    text-align: center;
    .board_1{
        display: inline-block;
        position: relative;
        text-align: center;
    }
    .board_2{
        display: inline-block;
        position: relative;
        text-align: center;
        margin: 70px auto 0;
    }
    .board_3{
        display: inline-block;
        position: relative;
        text-align: center;
        margin: 40px auto;
    }
    .board_4{
        display: inline-block;
        position: relative;
        text-align: center;
        margin: 40px auto;
    }
    .board_3_character{
        position: absolute;
        bottom: -20%;
        left: -8%;
        z-index: 50;
        pointer-events:none;
    }

`
const RewardBtn = styled.div`
    width: 318px;
    height: 68px;
    cursor: pointer;
    background: top center no-repeat url(${Imglist['button_condition']});
    pointer-events: none;
    ${props=> props.pass === true && props.received !== true && `
        pointer-events: visible;
        background: top center no-repeat url(${Imglist['btn']});
    `}
    ${props=> props.received === true && `
        pointer-events: none;
        background: top center no-repeat url(${Imglist['btn_active']});
    `}
    &.board1{
        position: absolute;
        bottom: 70px;
        left: 50%;
        transform: translate3d(-50%,0,0);
    }
    &.board2{
        position: absolute;
        bottom: 48px;
        left: 50%;
        transform: translate3d(-50%,0,0);
    }
    &.board3{
        position: absolute;
        bottom: 100px;
        left: 50%;
        transform: translate3d(-50%,0,0);
    }
`

const MagnifyIcon = styled.img`
    position: absolute;
    cursor: pointer;
    &.board1{
        &__p1{
            bottom: 44%;
            left: 46%;
        }
        &__p2{
            bottom: 44%;
            right: 10%;
        }
    }
    &.board2{
        &__p1{
            top: 14.5%;
            left: 39.5%;
        }
        &__p2{
            top: 14.5%;
            right: 16%;
        }
        &__p3{
            top: 33.4%;
            right: 7.5%;
        }
        &__p4{
            top: 39.6%;
            left: 46%;
        }
        &__p5{
            top: 48%;
            right: 8%;
        }
        &__p6{
            top: 54.4%;
            left: 46%;
        }
        &__p7{
            bottom: 36%;
            right: 7.5%;
        }
        &__p8{
            bottom: 29.7%;
            left: 46%;
        }
        &__p9{
            bottom: 14.9%;
            left: 46%;
        }
        &__p10{
            bottom: 8.5%;
            left: 46%;
        }
    }
`
