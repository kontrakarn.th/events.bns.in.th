const bg 								= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/bg.png';
const board_1 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/board_1.png';
const board_2 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/board_2.png';
const board_3 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/board_3.png';
const board_4 					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/board_4.png';
const magnify_icon 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/magnify_icon.png';
const board_3_character	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/board_3_character.png'
const btn_active 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/btn_active.png';
const btn 							= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/btn.png';
const button_condition 	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/button_condition.png';
const item_1 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/item_1.png';
const item_2 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/item_2.png';
const item_3 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/item_3.png';
const item_4 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/item_4.png';
const item_5 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/item_5.png';
const item_6 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/item_6.png';
const item_7 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/item_7.png';
const item_8 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/item_8.png';

// https://cdngarenanow-a.akamaihd.net/webth/bns/events/revival_april2020/images/bg.png
export const Imglist = {
	bg,
	board_1,
	board_2,
	board_3,
	board_4,
	magnify_icon,
	board_3_character,
	btn_active,
	btn,
	button_condition,
	item_1,
	item_2,
	item_3,
	item_4,
	item_5,
	item_6,
	item_7,
	item_8,
}
