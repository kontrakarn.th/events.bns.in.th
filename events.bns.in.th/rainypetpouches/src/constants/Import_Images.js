const bg_home = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/bg_home.jpg';
const box1_frame = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/box1_frame.png';
const box2_frame = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/box2_frame.png';
const box3_frame = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/box3_frame.png';
const random_btn = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/random_btn.png';
const btn_gacha_hover = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/btn_gacha_hover.png';
const btn_gacha = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/btn_gacha.png';
const btn_back = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/btn_back.png';
const btn_next = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/btn_next.png';
const bg_modal_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/bg_modal_confirm.png';
const btn_cancel = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/btn_cancel.png';
const btn_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/btn_confirm.png';
const bg_modal_message = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/bg_modal_message.png';
const bg_menu = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/bg_menu.jpg';
const trade_bg = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/trade_bg.jpg';
const trade_frame = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/trade_frame.png';
const frame_active = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/frame_active.png';
const frame = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/frame.png';
const frame_disable = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/frame_disable.png';
const btn_receive_hover = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/btn_receive_hover.png';
const btn_receive = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/btn_receive.png';
const btn_send = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/btn_send.png';
const btn_send_hover = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/btn_send_hover.png';
const bg_modal_uid = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/bg_modal_uid.png';
const bg_history = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/bg_history.jpg';
const bg_permission = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/f11/permission.jpg';
const icon_scroll = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/f11/icon_scroll.png';
const btn_home = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/f11/btn_home.png';

const pet_1 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/pet/pet_1.png';
const pet_2 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/pet/pet_2.png';
const pet_3 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/pet/pet_3.png';
const pet_4 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/pet/pet_4.png';
const pet_5 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/pet/pet_5.png';
const pet_6 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/pet/pet_6.png';
const pet_7 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/pet/pet_7.png';
const pet_8 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/pet/pet_8.png';
const pet_9 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/pet/pet_9.png';
const pet_10 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/pet/pet_10.png';
const pet_11 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/pet/pet_11.png';

const MixPetGacha_IC1 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/Icon/MixPetGacha_IC01.png';
const MixPetGacha_IC2 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/Icon/MixPetGacha_IC02.png';
const MixPetGacha_IC3 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/Icon/MixPetGacha_IC03.png';
const MixPetGacha_IC4 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/Icon/MixPetGacha_IC04.png';
const MixPetGacha_IC5 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/Icon/MixPetGacha_IC05.png';
const MixPetGacha_IC6 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/Icon/MixPetGacha_IC06.png';
const MixPetGacha_IC7 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/Icon/MixPetGacha_IC07.png';
const MixPetGacha_IC8 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/Icon/MixPetGacha_IC08.png';
const MixPetGacha_IC9 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/Icon/MixPetGacha_IC09.png';
const MixPetGacha_IC10 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/Icon/MixPetGacha_IC10.png';
const MixPetGacha_IC11 = 'https://cdngarenanow-a.akamaihd.net/webth/cdn/bns/rainypetpouches/Icon/MixPetGacha_IC11.png';

export default {
	bg_home,
	box1_frame,
	box2_frame,
	box3_frame,
	random_btn,
	btn_gacha_hover,
	btn_gacha,
	btn_back,
	btn_next,
	bg_modal_confirm,
	btn_cancel,
	btn_confirm,
	bg_modal_message,
	bg_menu,
	frame_disable,
	frame_active,
	frame,
	trade_bg,
	trade_frame,
	btn_receive_hover,
	btn_receive,
	btn_send,
	btn_send_hover,
	bg_modal_uid,
	bg_history,
	bg_permission,
	icon_scroll,
	btn_home,
	pet_1,
	pet_2,
	pet_3,
	pet_4,
	pet_5,
	pet_6,
	pet_7,
	pet_8,
	pet_9,
	pet_10,
	pet_11,
	MixPetGacha_IC1,
	MixPetGacha_IC2,
	MixPetGacha_IC3,
	MixPetGacha_IC4,
	MixPetGacha_IC5,
	MixPetGacha_IC6,
	MixPetGacha_IC7,
	MixPetGacha_IC8,
	MixPetGacha_IC9,
	MixPetGacha_IC10,
	MixPetGacha_IC11
}
