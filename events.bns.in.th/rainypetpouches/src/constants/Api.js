import KJUR from 'jsrsasign';
// import $ from "jquery";

const createAuthorizationHeaders = (token) => {
    // Header
    let headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': (["B", "e", "a", "r", "e", "r"].join("")) + " " + token,
    };
    return headers;
}
export const apiPost = (
  api_name,
  token,
  api_data,
  successCallback=()=>{},
  failCallback=()=>{},
  responseCallback=()=>{},
  errorCallback=()=>{}
) => {
    fetch(process.env.REACT_APP_API_SERVER_HOST+process.env[api_name], {
        method: 'POST',
        credentials: 'same-origin',
        // ...result
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': (["B", "e", "a", "r", "e", "r"].join("")) + " " + token,
        },
        body: JSON.stringify(api_data)
    }).then(response => {
        if (response.status == 200) {
            response.json().then(data => {
                if (data.status) {
                    successCallback(data);
                } else {
                    failCallback(data);
                }
            });
        } else {
            responseCallback(response);
        }
    }, error => {
        errorCallback(error);
    });
}
