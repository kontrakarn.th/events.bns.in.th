import React,{useState,useEffect} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import imgList from '../constants/Import_Images';
import F11Layout from '../features/F11Layout';
import {apiPost} from '../constants/Api';
import {setValues} from '../store/redux';
import Btn from "../features/Buttons";

const GiftPage = props => {
	const actSelcet = (id,remain)=>{
		if(remain > 0) {
			props.setValues({
				item_select: id
			});
		}
	}
	const actOpenReceiveModal = ()=>{
		let {item_list,item_select} = props;
		props.setValues({
			modal_open: "receive",
			modal_message: "ต้องการรับไอเทม<br/><span>"+item_list[item_select].package_name+'</span> ?',
		});
	}
	const actOpenSendModal = ()=>{
		props.setValues({
			modal_open: "send",
			friend_uid: "",
		});
	}
	const apiExchangeInfo = () => {
		if(props.jwtToken !== "") {
			props.setValues({ modal_open: "loading" });
			apiPost(
				"REACT_APP_API_POST_EXCHANGE_INFO",
				props.jwtToken,
				{type: "exchange_info"},
				(data)=>{//function successCallback
				props.setValues({
					...data.data,
					modal_open:"",
				});
				},
				(data)=>{//function failCallback
				props.setValues({
					modal_open: "message",
					modal_message: data.message,
				});
				},
			);
		}
	}
  	useEffect(()=>{ apiExchangeInfo() },[props.jwtToken]);
	let canReceive = (props.item_select !== -1) ? true: false;
	let item_name = [
		"หินล้ำค่า<br/>ลูกเจี๊ยบ",
		"หินสัตว์เลี้ยง<br/>น้องหมาสิงโต",
		"หินสัตว์เลี้ยง<br/>อัลปาก้า",
		"หินสัตว์เลี้ยง<br/>ยูนิคอร์น",
		"หินสัตว์เลี้ยง<br/>แร็คคูน",
		"หินสัตว์เลี้ยง<br/>โชกี้",
		"หินสัตว์เลี้ยง<br/>นกฮูก",
		"หินสัตว์เลี้ยง<br/>นางเงือก",
		"หินสัตว์เลี้ยง<br/>สาวน้อยใต้ทะเล",
		"หินสัตว์เลี้ยง<br/>ผีเสื้อโคมไฟ",
		"หินสัตว์เลี้ยง<br/>ภูติหิมะตัวน้อย",
	]
  	return (
		<F11Layout page="gift">
			<GiftStyle>
				<GiftStyle_bg>
					{[1,2,3,4,5,6,7,8,9,10,11].map((item,index)=>{
					let {id,package_name,remain} = props.item_list[index];
						return (
							<SlotItem
								key={"key_"+index}
								className={"gift__area"}
								num={item}
								lock={remain<=0}
								active={(index === props.item_select)}
								onClick={()=>actSelcet(index,remain)}
							>
								<div className="frame">
									<img src={imgList["MixPetGacha_IC"+item]} alt='' />
								</div>
								<p className="txt_item_name" dangerouslySetInnerHTML={{__html: item_name[index]+'<br/>('+remain+')'}}></p>
							</SlotItem>
						)
					})}
					<div className="gift__bottom">
						<Btn
							width="296px"
							height="95px"
							grayscale={canReceive? "0":"1"}
							active={canReceive}
							src={imgList.btn_receive}
							hover={imgList.btn_receive_hover}
							onClick={()=>actOpenReceiveModal()}
						/>
						<Btn
							width="296px"
							height="95px"
							grayscale={(canReceive && props.can_send_gift)? "0": "1"}
							active={(canReceive && props.can_send_gift)}
							src={imgList.btn_send}
							hover={imgList.btn_send_hover}
							onClick={()=>actOpenSendModal()}
						/>
					</div>
				</GiftStyle_bg>
			</GiftStyle>
		</F11Layout>
  	)
}

const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(GiftPage);

const GiftStyle = styled.div`
	position: relative;
	display: block;
	width: 100%;
	height: 1387px;
	background-image: url(${imgList.trade_bg});
	background-color: #000000;
`;

const GiftStyle_bg = styled.div`
	position: absolute;
	display: block;
	width: 980px;
	height: 1189px;
	background-image: url(${imgList.trade_frame});
	background-position: center;
	background-color: transparent ;
	background-repeat: no-repeat;
	top: 100px;
	left: 50%;
    transform: translate(-50%, 0);
  	.gift {
		&__bottom {
			display: flex;
			justify-content: center;
			align-items: center;
			width: 100%;
			position: absolute;
			top: 1115px;
			left: 50%;
			transform: translate(-50%, 0px);
			>a{
				margin: 0 20px;
			}
		}
  	}
`;
const SlotItem = styled.a`
  	position: absolute;
	${props=> props.num === 1 && "top: 340px;left: 239px;"}
	${props=> props.num === 2 && "top: 340px;left: 406px;"}
	${props=> props.num === 3 && "top: 340px;left: 574px;"}
	${props=> props.num === 4 && "top: 340px;left: 741px;"}

	${props=> props.num === 5 && "top: 573px;left: 239px;"}
	${props=> props.num === 6 && "top: 573px;left: 406px;"}
	${props=> props.num === 7 && "top: 573px;left: 574px;"}
	${props=> props.num === 8 && "top: 573px;left: 741px;"}

	${props=> props.num === 9 && "top: 806px;left: 322px;"}
	${props=> props.num === 10 && "top: 806px;left: 489px;"}
	${props=> props.num === 11 && "top: 806px;left: 657px;"}
	display: flex;
	justify-content: space-between;
	align-items: center;
	flex-direction: column;
	width: 120px;
	transform: translate(-50%,0);
	pointer-events: ${props=>props.lock ? "none":"all"};
	padding: 10px 0px;
	.txt_item_name{
		text-align: center;
		margin-top: 16px;
		line-height: 1.2;
		color: #543c24;
		font-family: "Kanit-Light", tahoma;
	}
	.frame{
		position: relative;
		width: 100%;
		height: 121px;
		${props=>props.lock
		?
			`background: url(${imgList.frame_disable}) no-repeat center;`
		:
			props.active
			?
				`background: url(${imgList.frame_active}) no-repeat center;`
			:
				`background: url(${imgList.frame}) no-repeat center;`
		}
		background-size: cover;
		>img{
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			width: 64px;
			height: 64px;
			filter: grayscale(${props=>props.lock ? "1":"0"});
		}
	}
`;
