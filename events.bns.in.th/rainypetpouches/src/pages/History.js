import React,{useState,useEffect} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import imgList from '../constants/Import_Images';
import F11Layout from '../features/F11Layout';
import {apiPost} from '../constants/Api';
import {setValues} from '../store/redux';

const HistoryPage = props => {
	const apiHistory = () => {
		if(props.jwtToken !== "") {
			props.setValues({ modal_open: "loading" });
			apiPost(
				"REACT_APP_API_POST_HISTORY",
				props.jwtToken,
				{type: "history"},
				(data)=>{//function successCallback
					props.setValues({
						username:data.data.username,
						history:data.data.item_list,
						modal_open: ""
					});
				},
				(data)=>{//function failCallback
					props.setValues({
						modal_open: "message",
						modal_message: data.message,
					});
				},
			);
		}
	}
  	useEffect(()=>{ apiHistory() },[props.jwtToken]);
	return (
		<F11Layout page="history">
			<HistoryStyle>
				<ul className="list">
					{props.history.map((item,index)=>{
						return (
							<li className="list__slot" key={"history_"+index}>
								<div className="list__text">{item.title}</div>
								<div className="list__date">{item.date}</div>
								<div className="list__time">{item.time}</div>
							</li>
						)
					})}
				</ul>
			</HistoryStyle>
		</F11Layout>
	)
}

const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(HistoryPage);

const HistoryStyle= styled.div`
	position: relative;
	display: block;
	width: 100%;
	height: 700px;
	background-image: url(${imgList.bg_history});
	.list {
		position: relative;
		top: 331px;
		display: block;
		width: 833px;
		height: 322px;
		margin: 0px auto;
		padding: 0px 80px;
		overflow-y: auto;
		font-family: "Kanit-Light", tahoma;
		font-size: 16px;
		color: #000;
		&__slot {
			display: flex;
			align-items: center;
			line-height: 1.7;
		}
		&__text {
			width: 460px;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}
		&__date {
			margin-left: 6px;
			width: 130px;
		}
		&__time{
			margin-left: 9px;
			width: 68px;
		}
	}
`;
