import React,{useState,useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import styled from 'styled-components';
import imgList from '../constants/Import_Images';
import Slider from "react-slick";
import F11Layout from '../features/F11Layout';
import Btn from "../features/Buttons";
import Pagination from '../features/Pagination';
import {apiPost} from '../constants/Api';
import {setValues} from '../store/redux';

const HomePage = props => {
  const [itemSlide,setItemSlide] = useState(null);
  const [itemPage,setItemPage] = useState(0);

  const itemSlideSettings = {
      dots: false,
      speed: 500,
      dots: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
      afterChange: (p)=>setItemPage(p),
      appendDots: dots => (
        <div>
          <ul style={{ margin: "0px" }}> {dots} </ul>
        </div>
      ),
      customPaging: i => (
        <div />
      )
  };

  const apiEventInfo = () => {
    if(props.jwtToken !== "" && !props.status) {
      props.setValues({ modal_open: "loading" });
      apiPost(
        "REACT_APP_API_POST_EVENT_INFO",
        props.jwtToken,
        {type: "event_info"},
        (data)=>{//function successCallback
          props.setValues({
              ...data.data,
              modal_open: "",
              // modal_open: "selectcharacter",
          });
        },
        (data)=>{//function failCallback
          props.setValues({
            modal_open: "message",
            modal_message: data.message,
          });
        },
      );
    }
  }
  	const actShowModalConfirm = () => {
		console.log("actShowModalConfirm");
		if(props.jwtToken && props.jwtToken !== "") {
			props.setValues({
				modal_open: "confirm",
				modal_message: "ต้องการเปิดถุงสัตว์เลี้ยงในหน้าฝน ?",
			});
		}
  	}
  	useEffect(()=>{ apiEventInfo() },[props.jwtToken]);
  	return (
		<F11Layout showUserName={true} showCharacterName={true} page="home">
			<HomeStyle className="home">
				<section className="home__banner"/>
				<section className="home__rule">
					<img src={imgList.box1_frame} alt=''/>
				</section>
				<section className="home__gacha">
					<div>
						<img src={imgList.box2_frame} alt=''/>
						<div className="home__gachabtn">
							<Btn
								width="296px"
								height="95px"
								// grayscale={props.can_open_gachapon ?"0":"1"}
								// active={props.can_open_gachapon}
								grayscale={0}
								active={true}
								src={imgList.btn_gacha}
								hover={imgList.btn_gacha_hover}
								onClick={()=>actShowModalConfirm()}
							/> 
						</div>
					</div>
				</section>
			<section className="home__items">
			<div className="home__slide">
			<Slider key="pointslide" {...itemSlideSettings} ref={e=>{if(e)setItemSlide(e)}}>
				<div><img className="home__itemslot" src={imgList.pet_1} /></div>
				<div><img className="home__itemslot" src={imgList.pet_2} /></div>
				<div><img className="home__itemslot" src={imgList.pet_3} /></div>
				<div><img className="home__itemslot" src={imgList.pet_4} /></div>
				<div><img className="home__itemslot" src={imgList.pet_5} /></div>
				<div><img className="home__itemslot" src={imgList.pet_6} /></div>
				<div><img className="home__itemslot" src={imgList.pet_7} /></div>
				<div><img className="home__itemslot" src={imgList.pet_8} /></div>
				<div><img className="home__itemslot" src={imgList.pet_9} /></div>
				<div><img className="home__itemslot" src={imgList.pet_10} /></div>
				<div><img className="home__itemslot" src={imgList.pet_11} /></div>
			</Slider>
			</div>
			</section>
		</HomeStyle>
		</F11Layout>
  	)
}
const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(HomePage);


const HomeStyle= styled.div`
	position: relative;
	display: block;
	width: 100%;
	height: 2817px;
	background-image: url(${imgList.bg_home});
	background-color: #000000;
  	.home {
		&__banner {
			height: 686px;
		}
		&__rule {
			height: 596px;
			>img{
				height: 100%;
				margin-left: 60px;
				display: block;
			}
		}
		&__gacha {
			height: 580px;
			margin-top: 84px;
			>div{
				position: relative;
				height: 100%;
    			margin-left: -28px;
				>img{
					height: 100%;
					margin: 0 auto;
					display: block;
				}
			}
		}
		&__gachabtn {
			position: absolute;
			bottom: -17px;
			left: 51%;
			transform: translate(-50%, 0);
			width: 296px;
			height: 95px;
			pointer-events: all;
		}
		&__items {
			width: 980px;
			height: 759px;
			margin: 79px auto 0;
			background-image: url(${imgList.box3_frame});
		}
		&__itemslot {
			margin: 0px auto;
			display: block;
		}
		&__slide {
			position: relative;
			top: 240px;
			display: block;
			width: 830px;
			margin: 0px auto;
			.slick-dots {
				display: none;
				.slick-active {
					background-color: #e5e5e5;
				}
				li {
					background-color: #646464;
					border-radius: 50%;
					width: 10px;
					height: 10px;
				}
			}
		}
	}
	.btn {
		position: absolute;
		top: 543px;
		left: 305px;
		display: block;
		width: 33px;
		height: 33px;
	}
`;

const HideBeforeAfter = styled.div`
	display: block;
	width: 26px;
	height: 37px;
	z-index: 2;
	&.slick-next {
		right: 0px;
	}
	&.slick-prev {
		left: 0px;
	}
	&:before,&:after {
		content: "";
	}
`;
function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <HideBeforeAfter
        className={className}
        style={{ ...style, display: "block"}}
        onClick={onClick}
      >
        <img src={imgList.btn_next}/>
      </HideBeforeAfter>
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <HideBeforeAfter
          className={className}
          style={{ ...style, display: "block"}}
          onClick={onClick}
        >
          <img src={imgList.btn_back}/>
        </HideBeforeAfter>
    );
}
