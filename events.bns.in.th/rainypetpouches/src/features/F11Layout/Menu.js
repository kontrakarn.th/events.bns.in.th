import React,{useState,useEffect} from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import imgList from '../../constants/Import_Images';

const CPN = (props) => {
  const [showMenu,setShowMenu] = useState(false);

  useEffect(()=>{
    //delay close menu
    setTimeout( ()=> {setShowMenu(false);}, 100);
  },[props.page]);

  return (
    <MenuLayout claaNemr="menu" active={showMenu}>
      <a className="menu__hamburger" onClick={()=>setShowMenu(!showMenu)}><div /><div /><div /></a>
      <div className="menu__backdrop" />
      <ul className="menu__list">
        {props.menu.map((item,index)=>{
          return (
            <li key={"menu_"+index}>
              <Link
                className={"menu__slot"+(props.page === item.page ? " active":"")}
                to={item.link}
                onClick={()=>setShowMenu(false)}
              >{item.title}</Link>
            </li>
          )
        })}
      </ul>
    </MenuLayout>
  )
}
const mstp = state => ({...state.Main});
const mdtp = { };
export default connect( mstp, mdtp )(CPN);

//animation
const spd= "0.3s";

const MenuLayout = styled.div`
  position: fixed;
  top: 0px;
  left: 0px;
  display: block;
  width: 1105px;
  height: 700px;
  z-index: 100;
  pointer-events: none;
  .menu {
    &__hamburger {
      z-index: 2;
      position: absolute;
      top: 25px;//15px;
      left: 25px;//15px;
      display: block;
      width: 30px;
      height: 20px;//25px;
      pointer-events: all;
      // filter: drop-shadow(1px 1px 2px #00000099);
      div{
        position: absolute;
        display: block;
        height: 20%;
        width: 100%;
        background-color: #494949;
        transition: all ${spd} ease-in-out;
        &:nth-child(1) {
          top: ${props=>props.active ? "50":"5"}%;
          transform: translate(0, -50%) ${props=>props.active ? "rotate(45deg)":""};
        }
        &:nth-child(2) {
          top: 50%;
          transform: translate(0, -50%);
          opacity: ${props=>props.active ? "0":"1"}
        }
        &:nth-child(3) {
          bottom: ${props=>props.active ? "50":"5"}%;
          bootom: 50%;
          transform: translate(0, 50%) ${props=>props.active ? "rotate(-45deg)":""};
        }
      }
    }
    &__backdrop {
      display: block;
      width: 100%;
      height: 100%;
      background-color: ${props=>props.active ? "#00000099":"#00000000"};
      transition: background ${spd} ease-in-out;
      pointer-events: ${props=>props.active ? "all":"none"};
    }
    &__list {
      z-index: 1;
      position: absolute;
      top: 0px;
      left: 0px;
      display: block;
      height: 100%;
      width: 329px;
      padding-top: 150px;
      padding-left: 60px;
      background-image: url(${imgList.bg_menu});
      pointer-events: ${props=>props.active ? "all":"none"};
      transform: translate(${props=>props.active ? "0":"-100%"}, 0) ;
      transition: transform ${spd} ease-in-out;
    }
    &__slot {
      font-size: 20px;
      line-height: 2em;
      font-family: "Kanit-Regular", tahoma;
      color: #494949;
      &.active {
        font-family: "Kanit-Bold", tahoma;
      }
      &--reward {
        position: relative;
        left: -25px;
      }
    }
  }
`
