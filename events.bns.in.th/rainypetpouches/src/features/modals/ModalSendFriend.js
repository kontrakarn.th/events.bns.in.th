import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import imgList from '../../constants/Import_Images';
import {apiPost} from '../../constants/Api';

const CPN = props => {
  	let {item_list,item_select,friend_uid} = props;
  	let package_name = item_list[item_select] ? item_list[item_select].package_name : "";
  	const apiCheckUid = (package_name,friend_uid) => {
		if(props.jwtToken !== "") {
			props.setValues({ modal_open: "loading" });
			apiPost(
				"REACT_APP_API_POST_CHECK_UID",
				props.jwtToken,
				{
					type: "check_uid",
					sendto: friend_uid,
				},
				(data)=>{//function successCallback
					props.setValues({
						...data.data,
						modal_open: "send_confirm",
						modal_message: "ต้องการส่งไอเทม<br/><span>"+package_name+"</span><br/> ให้ <span>"+data.data.target_username+"</span><br/>โดยมีค่าธรรมเนียม 10,000 ไดมอนด์?",
					});
				},
				(data)=>{//function failCallback
					props.setValues({
						modal_open: "message",
						modal_message: data.message,
					});
				},
			);
		}
  	}

  	return (
		<ModalCore
			modalName="send"
			actClickOutside={()=>props.setValues({modal_open:""})}
		>
			<ModalSendFriend className="mdsendfriend">
				<div className="mdsendfriend__content">
					<Input
						id="f_uid"
						type="text"
						value={props.friend_uid}
						onChange={(e)=>props.setValues({friend_uid:e.target.value})}
					/>
				</div>
				<div className="mdsendfriend__btns">
					<Btn className='confirm' onClick={()=>apiCheckUid(package_name,friend_uid)} />
					<Btn className='cancel' onClick={()=>props.setValues({modal_open:''})} />
				</div>
			</ModalSendFriend>
		</ModalCore>
  	)
}
const mstp = state => ({...state.Main});
const mdtp = { setValues };
export default connect( mstp, mdtp )(CPN);

const ModalSendFriend = styled.div`
	position: relative;
	display: block;
	width: 475px;
	height: 323px;
	color: #FFFFFF;
	background: no-repeat center url(${imgList.bg_modal_uid});
	padding: 114px 55px 0px;
  	.mdsendfriend{
		&__content {
			width: 100%;
			height: 155px;
			display: flex;
			justify-content: center;
			align-items: center;
		}
		&__btns {
			position: relative;
			margin: 100px auto;
			display: flex;
			justify-content: center;
			align-items: center;
			margin: 0 auto;
    	}
  	}
`;

const Btn = styled.div`
	width: 126px;
	height: 41px;
	cursor: pointer;
	margin: 0 4px;
	color: #000;
	background-size: cover;
	&.confirm{
		background: no-repeat center top url(${imgList.btn_confirm});
	}
	&.cancel{
		background: no-repeat center top url(${imgList.btn_cancel});
	}
	&:hover{
		background-position: bottom center;
	}
`

const Input = styled.input`
    display: block;
    cursor: pointer;
    width: 90%;
    text-align: center;
	border: none;
    outline: none;
	padding: 9px 10px;
	background: #7bbbec;
	color: #040608;
	font-family: "Kanit-Light", tahoma;
	font-size: 20px;
	margin-top: -15px;
`
