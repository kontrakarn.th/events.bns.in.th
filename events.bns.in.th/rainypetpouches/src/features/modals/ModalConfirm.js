import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import imgList from '../../constants/Import_Images';
import {apiPost} from '../../constants/Api';

const CPN = props => {
  	const apiBuyGachapon = () => {
		if(props.jwtToken !== "") {
			props.setValues({ modal_open: "loading" });
			apiPost(
				"REACT_APP_API_POST_BUY_GACHAPON",
				props.jwtToken,
				{type: "buy_gachapon"},
				(data)=>{
					props.setValues({
						...data.data,
						modal_open: "message",
						modal_message: "ได้รับ<br/><span>"+data.data.result.title+"</span><br/>ไอเทมถูกจัดเก็บไปยังระบบส่งของขวัญ",

					});
				},
				(data)=>{
					props.setValues({
						modal_open: "message",
						modal_message: data.message,
					});
				},
			);
		}
	}
  	return (
		<ModalCore
			modalName="confirm"
			actClickOutside={()=>props.setValues({modal_open:""})}
		>
			<ModalConfirmContent className="mdconfirm">
				<div
					className="mdconfirm__text"
					dangerouslySetInnerHTML={{__html: props.modal_message}}
				/>
				<div className="mdconfirm__btns">
					<Btn className='confirm' onClick={()=>apiBuyGachapon()} />
					<Btn className='cancel' onClick={()=>props.setValues({modal_open:''})} />
				</div>
			</ModalConfirmContent>
		</ModalCore>
  	)
}

const mstp = state => ({...state.Main});
const mdtp = { setValues };
export default connect( mstp, mdtp )(CPN);

const ModalConfirmContent = styled.div`
	position: relative;
	display: block;
	width: 475px;
	height: 327px;
	color: #FFFFFF;
	background: no-repeat center url(${imgList.bg_modal_confirm});
	padding: 118px 55px 0px;
  	.mdconfirm{
		&__text {
			width: 100%;
			height: 155px;
			display: flex;
			justify-content: center;
			align-items: center;
			align-items: center;
			color: #040608;
			font-size: 20px;
			word-break: break-word;
			text-align: center;
			font-family: "Kanit-Light", tahoma;
		}
		&__btns {
			position: relative;
			margin: 100px auto;
			display: flex;
			justify-content: center;
			align-items: center;
			margin: 0 auto;
		}
	}
`;

const Btn = styled.div`
	width: 126px;
	height: 41px;
	cursor: pointer;
	margin: 0 4px;
	color: #000;
	background-size: cover;
	&.confirm{
		background: no-repeat center top url(${imgList.btn_confirm});
	}
	&.cancel{
		background: no-repeat center top url(${imgList.btn_cancel});
	}
	&:hover{
		background-position: bottom center;
	}
`
