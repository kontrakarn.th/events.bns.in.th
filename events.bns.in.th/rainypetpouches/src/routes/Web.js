import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import OauthLogin from '../middlewares/OauthLogin';
import Modals from '../features/modals';

import Home from '../pages/Home';
import Gift from '../pages/Gift';
import History from '../pages/History';

export const Web = (props) => {
  let dmp = process.env.REACT_APP_EVENT_PATH;
  return (
    <>
      <Route path={""} component={Modals} />
      <Route path={""} component={OauthLogin} />
      <Route path={dmp} exact component={Home} />
      <Route path={dmp+'/gift'} exact component={Gift} />
      <Route path={dmp+'/history'} exact component={History} />
    </>
  );
}
