import React from 'react';
import Modal from './Modal';

export default class ModalSendPackage extends React.Component {
    render() {
        let title = this.props.data && this.props.data || [];
        return (
            <Modal
                open={this.props.open}
                title="ยืนยัน"
                confirmMode={false}
                // actConfirm={()=>this.props.actConfirm()}
                // actClose={()=>this.props.actClose()}
            >
                <div className="inbox" dangerouslySetInnerHTML={{__html: this.props.msg}} />
                <div className="modal__bottom">
                    <a className="modal__button">ส่งให้ตัวเอง</a>
                    <a className="modal__button">ส่งให้เพื่อน</a>
                </div>
            </Modal>
        )
  }
}
