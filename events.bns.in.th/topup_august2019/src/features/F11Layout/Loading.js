import React from 'react';
import styled, { keyframes } from 'styled-components';
import bgPermission from './images/permission.jpg';

export default styled.div`
    position: absolute;
    top: 0px;
    left: 0px;
    display: block;
    width: 1105px;
    height: 700px;
    background: no-repeat center url(${bgPermission}) #c5c0ad /cover;
`;
