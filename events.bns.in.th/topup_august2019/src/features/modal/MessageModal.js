import React from 'react';
import  ModalCore from './ModalCore.js';
import styles from './styles.module.scss';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';

const MessageModal = (props) => {
	return (
		<ModalCore name="message" outSideClick={true}>
			<div className={[styles.contentwrap,styles.confirm].join(' ')}>
				<div className={styles.confirmcontent}>
					<div className={[styles.confirmcontent__text,styles.message].join(' ')} dangerouslySetInnerHTML={{__html: props.modal_message}}></div>
					<div className={[styles.btnbox,styles.message].join(' ')}>
						<div className={styles.btn} onClick={() => props.setValues({modal_open:''})}>ตกลง</div>
					</div>
				</div>
			</div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageModal);
