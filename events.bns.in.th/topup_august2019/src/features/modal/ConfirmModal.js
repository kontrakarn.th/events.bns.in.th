import React from "react";
import { connect } from "react-redux";
import ModalCore from "./ModalCore.js";
import styles from "./styles.module.scss";
import { setValues, actCloseModal } from "./redux";

const ConfirmModal = (props) => {
  let {modal_message} = props;
	return (
    <ModalCore name="confirm" outSideClick={true}>
      <div className={[styles.contentwrap].join(" ")}>
          <div className={[styles.contentTitle]}>ยืนยัน</div>
          <div className={[styles.contentInner]} dangerouslySetInnerHTML={{__html: modal_message}}></div>
          <div className={[styles.btnbox]}>
            <div className={styles.btn} onClick={()=>{if(props.actConfirm) props.actConfirm()}}>รับไอเทม</div>
          </div>
      </div>
    </ModalCore>
	)
}

const mapStateToProps = state => ({
  ...state.ModalReducer,
  ...state.Modal
});

const mapDispatchToProps = { setValues, actCloseModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmModal);
