import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import '../../styles/preorder.css';

import { apiPost } from './../../middlewares/Api';

import { onAccountLogout } from './../../actions/AccountActions';

import { setValues } from "./redux";

import { ConfirmModal, MessageModal, LoadingModal } from "../../features/modal";

import {Imglist} from './../../constants/Import_Images';

class Main extends Component {
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
            confirmRandom: false,
            confirmRandomMessage: "",
            confirmReceiveGacha: false,
            confirmReceiveGachaMessage: "",
            packageDetails: false,
            PackageDetailsMessage: "",
            has_claim: false,
            can_claim: true,
            collection1: [
                {
                    img: Imglist.collection1_1,
                    name: 'ประกายฉลามดำ',
                    count: '5'
                },
                {
                    img: Imglist.collection1_2,
                    name: ' หินโซล',
                    count: '250'
                },
                {
                    img: Imglist.collection1_3,
                    name: 'หินจันทรา',
                    count: '25'
                }, 
            ],
            collection2: [
                {
                    img: Imglist.collection2_1,
                    name: 'เกล็ดสีคราม',
                    count: '5'
                },
                {
                    img: Imglist.collection2_2,
                    name: ' ชิ้นส่วนผลึกฮงมุน',
                    count: '5'
                },
                {
                    img: Imglist.collection2_3,
                    name: 'กล่องอัญมณีหกเหลี่ยม ของฮงมุน พิเศษ',
                    count: '5'
                }, 
                {
                    img: Imglist.collection2_4,
                    name: 'ลูกแก้วคนทรง',
                    count: '5'
                }, 
            ],
            collection3: [
                {
                    img: Imglist.collection3_1,
                    name: 'ชุดเซเลปลายเสือ',
                    count: '1'
                },
                {
                    img: Imglist.collection3_2,
                    name: 'หมวกเซเลปลายเสือ',
                    count: '1'
                },
                {
                    img: Imglist.collection3_3,
                    name: 'ไดมอนด์ 50000',
                    count: '1'
                }, 
                {
                    img: Imglist.collection3_4,
                    name: 'ปีกแทชอน',
                    count: '5'
                }, 
            ]
        }
    }

    apiEventInfo(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status) {
                // console.log('data.content:',data.content);
                this.props.setValues({
                    ...data.content,
                    modal_open: "",
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiRedeemItem(id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'redeem',package_id:id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content,
                    modal_open:'message',
                    modal_message: data.message,
                    package_id: -1,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                    package_id: -1,
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                    modal_type: 'error',
                });
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 1000);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo()
            }, 1000);
        }
    }

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scollDown === false) this.setState({ scollDown: true });
        } else {
            if (this.state.scollDown === true) this.setState({ scollDown: false });
        }
    }

    actConfirmRedeem(){
        this.apiRedeemItem(
            this.props.package_id && this.props.package_id >= 1  && this.props.package_id <= 3
            && this.props.package_id
        );
    }

    render() {
        console.log(this.props.modal_open)
        return (
            <div className="events-bns">
                {this.state.permission ?
                    <div className="preorder">
                        <div className="preorder-inner">
                            <section className="preorder__section preorder__section--header">
                                <div className="header">
                                    <div className="header__scroll"></div>
                                </div>
                            </section>
                            <section className="preorder__section condition">
                                <div className="container">
                                    <div className="section__wrapper">
                                        <div className="section__head">
                                            <h3>รายละเอียดโปรโมชั่น</h3>
                                        </div>
                                        <div className="section__content">
                                            <ul className="condition__text">
                                                <li>ผู้เล่นเติมไดมอนด์เข้าเกมตามที่กำหนด สามารถกดรับไอเทมได้ฟรี!</li>
                                                <li>ผู้เล่นสามารถเติมไดมอนด์แบบสะสมได้</li>
                                                <li>ผู้เล่นสามารถเติมไดมอนด์ได้ทุกช่องทาง</li>
                                                <li>สามารถกดรับไอเทมได้ 1 ครั้ง ต่อขั้น / 1 UID</li>
                                                <li>เริ่มโปรโมชั่น วันที่ 28 สิงหาคม 2562 เวลา 12:00:00 น.</li>
                                                <li>สิ้นสุดโปรโมชั่น 4 กันยายน 2562 เวลา 23:59:59 น.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section className="preorder__section text-center">
                                <div className="container">
                                    <div className="section__wrapper">
                                        <div className="section__head">
                                            <h3>สะสมครบ 10,000 ไดมอนด์</h3>
                                        </div>
                                        <div className="section__content">
                                            {
                                                this.state.collection1.map((item,key)=>{
                                                    return(
                                                        <div key={key} className="itemsInline">
                                                            <div className="itemsInline__img">
                                                                <img src={item.img}/>
                                                            </div>
                                                            <div className="itemsInline__content">
                                                                <div>{item.name}</div>
                                                                <div>จำนวน {item.count} ชิ้น</div>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                        <div className="section__btn">
                                        {
                                            this.props.packages && this.props.packages[0].received == true ?
                                                <button type="button" className="btn btn-default disabled">รับไปแล้ว</button>
                                            :
                                                (
                                                    this.props.packages && this.props.packages[0].can_receive == true ?
                                                        <button type="button" className="btn btn-default" onClick={()=>this.props.setValues({modal_open:'confirm', package_id:1, modal_message:'รับไอเทมสะสมครบ <br/> 10,000 ไดมอนด์'})}>รับไอเทม</button>
                                                    :
                                                        <button type="button" className="btn btn-default disabled">ไม่ตรงเงื่อนไข</button>
                                                )
                                        }
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                            </section>
                            <section className="preorder__section text-center">
                                <div className="container">
                                    <div className="section__wrapper">
                                        <div className="section__head">
                                            <h3>สะสมครบ 20,000 ไดมอนด์</h3>
                                        </div>
                                        <div className="section__content">
                                            {
                                                this.state.collection2.map((item,key)=>{
                                                    return(
                                                        <div key={key} className="itemsInline">
                                                            <div className="itemsInline__img">
                                                                <img src={item.img}/>
                                                            </div>
                                                            <div className="itemsInline__content">
                                                                <div>{item.name}</div>
                                                                <div>จำนวน {item.count} ชิ้น</div>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                        <div className="section__btn">
                                            {
                                                this.props.packages && this.props.packages[1].received == true ?
                                                    <button type="button" className="btn btn-default disabled">รับไปแล้ว</button>
                                                :
                                                    (
                                                        this.props.packages && this.props.packages[1].can_receive == true ?
                                                            <button type="button" className="btn btn-default" onClick={()=>this.props.setValues({modal_open:'confirm', package_id:2 , modal_message:'รับไอเทมสะสมครบ <br/> 20,000 ไดมอนด์'})}>รับไอเทม</button>
                                                        :
                                                        <button type="button" className="btn btn-default disabled">ไม่ตรงเงื่อนไข</button>
                                                    )
                                            }
                                        </div>
                                    </div>
                                    
                                </div>
                            </section>
                            <section className="preorder__section text-center">
                                <div className="container">
                                    <div className="section__wrapper">
                                        <div className="section__head">
                                            <h3>สะสมครบ 150,000 ไดมอนด์</h3>
                                        </div>
                                        <div className="section__content">
                                            {
                                                this.state.collection3.map((item,key)=>{
                                                    return(
                                                        <div key={key} className="itemsInline">
                                                            <div className="itemsInline__img">
                                                                <img src={item.img}/>
                                                            </div>
                                                            <div className="itemsInline__content">
                                                                <div>{item.name}</div>
                                                                <div>จำนวน {item.count} ชิ้น</div>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                        <div className="section__btn">
                                            {
                                                this.props.packages && this.props.packages[2].received == true ?
                                                    <button type="button" className="btn btn-default disabled">รับไปแล้ว</button>
                                                :
                                                    (
                                                        this.props.packages && this.props.packages[2].can_receive == true ?
                                                            <button type="button" className="btn btn-default" onClick={()=>this.props.setValues({modal_open:'confirm', package_id:3, modal_message:'รับไอเทมสะสมครบ <br/> 150,000 ไดมอนด์'})}>รับไอเทม</button>
                                                        :
                                                        <button type="button" className="btn btn-default disabled">ไม่ตรงเงื่อนไข</button>
                                                    )
                                            }
                                        </div>
                                    </div>
                                    
                                </div>
                            </section>
                        </div>
                        <LoadingModal />
                        <MessageModal />
                        <ConfirmModal actConfirm={this.actConfirmRedeem.bind(this)}/>
                    </div>
                    :
                    null
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = {
    onAccountLogout,
    setValues
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main)
