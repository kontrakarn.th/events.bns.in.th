import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import {apiPost} from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';

import ModalMessage from '../components/ModalMessage';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';

import ImgFaminggo from './../static/images/faminggo.png';

//image for section costume
import costumeSet1_1 from './../static/images/costume1/1.png'
import costumeSet1_2 from './../static/images/costume1/2.png'
import costumeSet1_3 from './../static/images/costume1/3.png'
import costumeSet1_4 from './../static/images/costume1/4.png'
import costumeSet1_5 from './../static/images/costume1/5.png'
import costumeSet1_6 from './../static/images/costume1/6.png'
import costumeSet1_7 from './../static/images/costume1/7.png'

import costumeSet2_1 from './../static/images/costume2/1.png'
import costumeSet2_2 from './../static/images/costume2/2.png'
import costumeSet2_3 from './../static/images/costume2/3.png'
import costumeSet2_4 from './../static/images/costume2/4.png'
import costumeSet2_5 from './../static/images/costume2/5.png'
import costumeSet2_6 from './../static/images/costume2/6.png'
import costumeSet2_7 from './../static/images/costume2/7.png'

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
} from '../actions/AccountActions';

class CostumeContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
            costumeSet1: [
                {
                    img: costumeSet1_1,
                },
                {
                    img: costumeSet1_2,
                },
                {
                    img: costumeSet1_3,
                },
                {
                    img: costumeSet1_4,
                },
                {
                    img: costumeSet1_5,
                },
                {
                    img: costumeSet1_6,
                },
                {
                    img: costumeSet1_7,
                }
            ],
            costumeSet2: [
                {
                    img: costumeSet2_1,
                },
                {
                    img: costumeSet2_2,
                },
                {
                    img: costumeSet2_3,
                },
                {
                    img: costumeSet2_4,
                },
                {
                    img: costumeSet2_5,
                },
                {
                    img: costumeSet2_6,
                },
                {
                    img: costumeSet2_7,
                }
            ],

        }
    }

    apiEventInfo() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "event_info" };
        let successCallback = (data) => {
            
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    buy_soul_info: data.content
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }


    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 1000);

    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo()
            }, 1000);
        }
    }

    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }

    render() {
        return (
            <div>
            <div className="f11layout">
                <div className="f11layout__user">
                    <div className="f11layout__user--style">{this.props.username}</div>
                </div>
                <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
            </div>
            {this.state.permission ?
                <div className="events-bns">
                    <ScrollArea
                        speed={0.8}
                        className="area"
                        contentClassName=""
                        horizontal={false}
                        style={{ width: '100%', height:700, opacity:1}}
                        verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                        onScroll={this.handleScroll.bind(this)}

                    >
                        <div className="preorder">
                            <div className="preorder-inner">
                            <section className="preorder__section preorder__section--header">
                                <div className="header">
                                    <div className="header__scroll"><img src={iconScroll} alt="" /></div>
                                    <Link to="/soul_gachapon/soul" className="btn btn-promotion">เข้าร่วมโปรโมชั่น</Link>
                                </div>
                            </section>
                            <section className="preorder__section costume">
                                <div className="container">
                                    <div className="section__head">
                                        <h3>ตัวอย่างชุด</h3>
                                    </div>
                                    <div className="section__content text-center">
                                        <div className="costume__block">
                                            <div className="btn-costume">กล่องอาวุธลวงตาฟลามิงโก้</div>
                                            <img src={ImgFaminggo}/>
                                        </div>
                                        <div className="costume__block">
                                            <div className="btn-costume">ชุดเซ็ทม้าขาว</div>
                                            <div>
                                                {
                                                    this.state.costumeSet1.map((items, index) => {
                                                        return(
                                                            <div className="costume-items">
                                                                <img src={items.img}/>
                                                            </div>   
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                        <div className="costume__block">
                                            <div className="btn-costume">ชุดเซ็ทรักเราหวานชื่น</div>
                                            <div>
                                                {
                                                    this.state.costumeSet2.map((items, index) => {
                                                        return(
                                                            <div className="costume-items">
                                                                <img src={items.img}/>
                                                            </div>   
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </section>
                            <div className="bg-bottom"></div>
                            </div>
                        </div>
                    </ScrollArea>
                </div>
            :
                <Permission />
            }

            <Loading
                open={this.state.showLoading}
            />
        </div>   
                 
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CostumeContainer)