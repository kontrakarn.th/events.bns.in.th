import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import {apiPost} from '../middlewares/Api';
import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';

import ModalMessage from '../components/ModalMessage';
import ModalInput from '../components/ModalInput';
// import ModalConfirmReceiveGacha from '../components/ModalConfirmReceiveGacha';
import ModalNoReceive from '../components/ModalNoReceive';
import ModalConfirmSend from '../components/ModalConfirmSend';
import ModalConfirmSendGift from '../components/ModalConfirmSendGift';

import {Menu} from '../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
import ImgItems from './../static/images/items/set1no_num.png'

import white_horse_set from './../static/images/items/white_horse_set_nonumb.png';
import white_horse_wings from './../static/images/items/white_horse_wings_nonumb.png';
import pegasus_pet from './../static/images/items/pegasus_pet_nonumb.png';
import love_suit from './../static/images/items/love_suit_nonumb.png';
import love_hat from './../static/images/items/love_hat_nonumb.png';
import doflamingo_weapon_skin from './../static/images/items/doflamingo_weapon_skin_nonumb.png';

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
} from '../actions/AccountActions';

const item_list = [
    {
        'img' : white_horse_set
    },
    {
        'img' : white_horse_wings
    },
    {
        'img' : pegasus_pet
    },
    {
        'img' : love_suit
    },
    {
        'img' : love_hat
    },
    {
        'img' : doflamingo_weapon_skin
    },
];

class SendContainer extends Component {
   
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,

            //modal
            showModalCheckUid: false,
            showModalConfirmSendGift: false,
            showModalConfirmSendGiftMessage: "",
            // showModalConfirmReceiveGacha: false,
            // showModalNoReceive: false,

            showModalSend: false,
            showModalSendMessage: "",

            showModalMessage: "",

            username: '',
            my_bag: [],
            packageId: 0,
            packageName: "",

            targetUid: "",

            active: -1,

        }
    }

    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiMyBag(), 1000);

    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiMyBag();
            }, 1000);
        }
        if(prevState.packageId != this.state.packageId && this.state.packageId == 0){
            this.setState({
                active: -1,
            })
        }
    }

    apiMyBag() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "my_bag" };
        let successCallback = (data) => {
            
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    my_bag: data.content.my_bag,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_BAG", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSend(){
        this.setState({ showLoading: true, showModalSend: false, });
        let self = this;
        let dataSend = { type: "send_item", package_id: this.state.packageId };
        let successCallback = (data) => {
            
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    showModalMessage: data.message,
                    my_bag: data.content.my_bag,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_SEND_ITEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiCheckUid(targetUid){
        // console.log(targetUid);
        this.setState({ showLoading: true, showModalCheckUid: false, });
        let self = this;
        let dataSend = { type: "check_uid", uid: targetUid };
        let successCallback = (data) => {
            
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    showModalConfirmSendGift: true,
                    showModalConfirmSendGiftMessage: "ส่ง &quot;"+this.state.packageName+"&quot;<br />ให้กับ UID: &quot;"+data.content.target_uid+"&quot;<br />Username: &quot;"+data.content.target_username+"&quot;<br />หรือไม่?",
                    targetUid: targetUid,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CHECK_UID", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSendGift(){
        this.setState({ showLoading: true, showModalConfirmSendGift: false, });
        let self = this;
        let dataSend = { type: "check_uid", package_id: this.state.packageId, uid: this.state.targetUid };
        let successCallback = (data) => {
            
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    showModalMessage: data.message,
                    my_bag: data.content.my_bag,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_SEND_GIFT", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    checkUid(){
        if(this.state.packageId != 0){
            this.setState({
                showModalCheckUid:true,
            });
        }else{
            this.setState({
                showModalMessage:"กรุณาเลือกไอเทมก่อนส่งของขวัญ",
            });
        }
    }
    
    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }

    handleClickActive(index,packageId,packageName){
        this.setState({
            active:index,
            packageId: packageId,
            packageName: packageName,
        })
    }

    onConfirmSend(e){
        e.preventDefault();
        if(this.state.packageId != 0){
            this.setState({
                showModalSend: true,
                showModalSendMessage: "รับ "+this.state.packageName+"?",
            });
        }
    }

    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="preorder">
                                <div className="page__wrapper">
                                    <Menu />
                                    <section className="preorder__section send">
                                        <div className="container">
                                            <div className="section__head">
                                                <h3>ส่งของรางวัล</h3>
                                            </div>
                                            <div className="section__content items__wrapper">  
                                                <div className="content__scroll">
                                                    {
                                                        this.state.my_bag.map((items, index) => {
                                                            return(
                                                                <div className="items__box send-item">
                                                                    {
                                                                        items.quantity > 0 ?
                                                                            <div onClick={()=>this.handleClickActive(index,items.id,items.title)} className={"items__box--inner select-item " + (this.state.active === index ? 'active' : '')}>
                                                                                <img src={item_list[index].img}/>
                                                                                <div className="items__box--count">{items.quantity}</div>
                                                                            </div>
                                                                        :
                                                                            <div  className={"items__box--inner"}>
                                                                                <img src={item_list[index].img}/>
                                                                                <div className="items__box--count">{items.quantity}</div>
                                                                            </div>
                                                                    }
                                                                    
                                                                </div>
                                                            )
                                                        })
                                                    }
                                               </div>
                                            </div>
                                            <div className="section__btn">
                                                <div className="btn-default" onClick={this.onConfirmSend.bind(this)}>รับของรางวัล</div>
                                                <Link to="/soul_gachapon/soul" className="btn-default">ซื้อโซลเพิ่ม</Link>
                                                <div className="btn-default" onClick={this.checkUid.bind(this)}>ส่งให้เพื่อน</div>
                                            </div>
                                        </div>   
                                    </section>
                                </div>
                            </div>
                            
                        </ScrollArea>

                        <ModalInput
                            open={this.state.showModalCheckUid}
                            actConfirm={(uid)=>this.apiCheckUid(uid)}
                            actClose={()=>this.setState({
                                showModalCheckUid: false,
                                packageId: 0,
                                packageName: "",
                            })}
                        />

                        <ModalConfirmSendGift
                            open={this.state.showModalConfirmSendGift}
                            actConfirm={()=>this.apiSendGift()}
                            actClose={()=>this.setState({
                                showModalConfirmSendGift: false,
                                packageId: 0,
                                packageName: "",
                                targetUid: "",
                            })}
                            msg={this.state.showModalConfirmSendGiftMessage}
                        />

                        <ModalConfirmSend
                            open={this.state.showModalSend}
                            actConfirm={()=>this.apiSend()}
                            actClose={()=>this.setState({
                                showModalSend: false,
                                showModalSendMessage: "",
                                packageId: 0,
                                packageName: "",
                            })}
                            msg={this.state.showModalSendMessage}
                        />

                        <ModalMessage
                            open={this.state.showModalMessage.length > 0}
                            actClose={()=>this.setState({
                                showModalMessage: "",
                                packageId: 0,
                                packageName: "",
                                targetUid: "",
                            })}
                            msg={this.state.showModalMessage}
                        />
                    </div>
                :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />
            </div>          
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(SendContainer))
