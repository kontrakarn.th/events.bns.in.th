import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import {apiPost} from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import ModalMessage from '../components/ModalMessage';
import ModalConfirmCombine from '../components/ModalConfirmCombine';

import {Menu} from '../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
} from '../actions/AccountActions';

import white_horse_set from './../static/images/items/white_horse_set_nonumb.png';
import white_horse_wings from './../static/images/items/white_horse_wings_nonumb.png';
import pegasus_pet from './../static/images/items/pegasus_pet_nonumb.png';
import love_suit from './../static/images/items/love_suit_nonumb.png';
import love_hat from './../static/images/items/love_hat_nonumb.png';
import white_horse_set_part1 from './../static/images/items/white_horse_set_part1_nonumb.png';
import white_horse_set_part2 from './../static/images/items/white_horse_set_part2_nonumb.png';
import white_horse_wings_part1 from './../static/images/items/white_horse_wings_part1_nonumb.png';
import white_horse_wings_part2 from './../static/images/items/white_horse_wings_part2_nonumb.png';
import pegasus_pet_part1 from './../static/images/items/pegasus_pet_part1_nonumb.png';
import pegasus_pet_part2 from './../static/images/items/pegasus_pet_part2_nonumb.png';
import love_suit_part1 from './../static/images/items/love_suit_part1_nonumb.png';
import love_suit_part2 from './../static/images/items/love_suit_part2_nonumb.png';
import love_hat_part1 from './../static/images/items/love_hat_part1_nonumb.png';
import love_hat_part2 from './../static/images/items/love_hat_part2_nonumb.png';

const item_icons = {
    'MAT_1_1' : white_horse_set_part1,
    'MAT_1_2' : white_horse_set_part2,
    'MAT_2_1' : white_horse_wings_part1,
    'MAT_2_2' : white_horse_wings_part2,
    'MAT_3_1' : pegasus_pet_part1,
    'MAT_3_2' : pegasus_pet_part2,
    'MAT_4_1' : love_suit_part1,
    'MAT_4_2' : love_suit_part2,
    'MAT_5_1' : love_hat_part1,
    'MAT_5_2' : love_hat_part2,
};

const rare_item_icons = [
    {
        id: 1,
        title: "เซ็ตม้าขาว",
        img: white_horse_set,
    },
    {
        id: 2,
        title: "ปีกม้าขาว",
        img: white_horse_wings,
    },
    {
        id: 3,
        title: "หินล้ำค่าเปกาซัส",
        img: pegasus_pet,
    },
    {
        id: 4,
        title: "ชุดรักเราหวานชื่น",
        img: love_suit,
    },
    {
        id: 5,
        title: "หมวกรักกันไม่เสื่อมคลาย",
        img: love_hat,
    },
];

class CombineContainer extends Component {
   
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
            showModalMessage: "",

            username: "",

            materials: [],

            confirmCombine: false,
            packageId: 0,
            confirmCombineMessage: "",
        }
    }

    handleClickMenu() {
        // console.log(this.state.showMenu)
        this.setState({
            showMenu:!this.state.showMenu
        })
    }

    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }

    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiMaterials(), 800);

    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiMaterials();
            }, 800);
        }
    }

    apiMaterials() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "my_material" };
        let successCallback = (data) => {
            
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    materials: data.content.materials,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_MATERIAL", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    onConfirmCombine(e,packageId,packageName){
        e.preventDefault();
        this.setState({
            confirmCombine:true,
            packageId: packageId,
            confirmCombineMessage:'ผสมชิ้นส่วน <br/> '+packageName+'?',
        });
    }

    apiConfirmCombine(){
        this.setState({ showLoading: true, confirmCombine: false, });
        let self = this;
        let dataSend = { type: "combine", package_id: this.state.packageId };
        let successCallback = (data) => {
            
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    showModalMessage: data.message,
                    materials: data.content.materials,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                    confirmCombine: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    confirmCombine: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_MERGE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
   
    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="preorder">
                                <div className="page__wrapper">
                                    <Menu />
                                    <section className="preorder__section combine">
                                        <div className="container">
                                            <div className="section__head">
                                                <h3>ผสมชิ้นส่วน</h3>
                                            </div>
                                            <div className="section__content items__wrapper text-center">  
                                                <div className="content__scroll clearfix">
                                                {
                                                    this.state.materials && this.state.materials.length > 0 ?
                                                        this.state.materials.map((items, index) => {
                                                            return(
                                                                <div className="items__wrapper--left">
                                                                    <div className="items__wrapper--leftCombine">
                                                                    {
                                                                        items.map((items2, index2) => {
                                                                            return(
                                                                                <div className="items__box">
                                                                                    <div className="items__box--inner">
                                                                                        <img src={item_icons[items2.id]}/>
                                                                                        <div className="items__box--count">
                                                                                        {
                                                                                            items2.quantity
                                                                                        }
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            )
                                                                        })
                                                                    }
                                                                        <div className="items__box">
                                                                            <div className="items__box--inner">
                                                                                <img src={rare_item_icons[index].img}/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    {
                                                                        items[0].quantity > 0 && items[1].quantity > 0 ?
                                                                            <div className="btn-soul" onClick={(event,packageId,packageName)=>this.onConfirmCombine(event,rare_item_icons[index].id,rare_item_icons[index].title)}>ผสมชิ้นส่วน</div>
                                                                        :
                                                                            <div className="btn-soul disabled">ชิ้นส่วนไม่พอ</div>
                                                                    }
                                                                </div>
                                                            )
                                                        })
                                                    :
                                                        null
                                                }
                                                </div>
                                            </div>
                                            <div className="section__btn">
                                                <div className="btn-default">รับของรางวัล</div>
                                                <Link to="/soul_gachapon/soul" className="btn btn-default">ซื้อโซลเพิ่ม</Link>
                                            </div>
                                        </div>   
                                    </section>
                                
                                </div>
                            </div>
                        </ScrollArea>

                        <ModalConfirmCombine
                            open={this.state.confirmCombine}
                            actConfirm={() => this.apiConfirmCombine()}
                            actClose={() => this.setState({
                                confirmCombine: false
                            })}
                            msg={this.state.confirmCombineMessage}
                        />

                        <ModalMessage
                            open={this.state.showModalMessage.length > 0}
                            actClose={()=>this.setState({showModalMessage: ""})}
                            msg={this.state.showModalMessage}
                        />
                    </div>
                :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />
            </div>          
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(CombineContainer))
