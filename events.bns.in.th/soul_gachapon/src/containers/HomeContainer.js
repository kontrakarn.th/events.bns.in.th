import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from './../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';

import ModalMessage from '../components/ModalMessage';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';

import {
    onAccountLogout,
    setUsername,
    setSoulPermission
} from '../actions/AccountActions';

class HomeContainer extends Component {

    apiEventInfo() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "event_info" };
        let successCallback = (data) => {
            
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    buy_soul_info: data.content
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
        }
    }

    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 1000);

    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo()
            }, 1000);
        }
    }

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scollDown === false) this.setState({ scollDown: true });
        } else {
            if (this.state.scollDown === true) this.setState({ scollDown: false });
        }
    }

    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: '700px', opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                        >
                            <div className="preorder">
                                <div class="preorder-inner">
                                    <section className="preorder__section preorder__section--header">
                                        <div className="header">
                                            <div className="header__scroll"><img src={iconScroll} alt="" /></div>
                                            <Link to="/soul_gachapon/soul" className="btn btn-promotion">เข้าร่วมโปรโมชั่น</Link>
                                        </div>
                                    </section>
                                    <section className="preorder__section condition">
                                        <div className="container">
                                            <div className="section__head">
                                                <h3>เงื่อนไขโปรโมชั่น</h3>
                                            </div>
                                            <div className="section__content">
                                                <ul className="condition__text">
                                                    <li>ระยะเวลาโปรโมชั่นที่สามารถซื้อลูกแก้วโซลได้  14 กุมภาพันธ์ - 28 กุมภาพันธ์ 2562 (23:59 น.)</li>
                                                    <li>สามารถแลกไอเทมได้ถึงวันที่ 7 มีนาคม (23:59 น.)</li>
                                                    <li>ราคา 25 บาท ต่อ 1 ลูกแก้วโซล</li>
                                                    <li>โปรโมชั่น 10 ลูกแก้วโซล ฟรี 1 ลูก</li>
                                                    <li>สามารถเข้าร่วมโปรโมชั่นนี้ไม่จำกัด</li>
                                                    <li>ชุดสามารถส่งให้เพื่อนได้โดยเสียค่าบริการ 10,000 ไดมอนด์</li>
                                                </ul>
                                                <br />
                                                <h3>พิเศษสุดสำหรับลูกค้า <span className="text--blue">AirPay</span></h3>
                                                <ul className="condition__text">
                                                    <li>แลกไดมอนด์เข้าเกม 50,000 ไดมอนด์ รับ 2 สิทธิ์ (ไม่สามารถสะสมได้)</li>
                                                    <li>แลกไดมอนด์เข้าเกม 100,000 ไดมอนด์ รับ 5 สิทธิ์ (ไม่สามารถสะสมได้)</li>
                                                    <li>1UID สามารถรับสิทธิ์สูงสุดได้ 5 สิทธิ์</li>
                                                    <li>จำนวนสิทธิ์ทั้งหมด 8,000 สิทธิ์</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </section>
                                    <div className="bg-bottom"></div>
                                </div>
                            </div>
                        </ScrollArea>
                    </div>
                    :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    permission: state.AccountReducer.permission
})

const mapDispatchToProps = {
    onAccountLogout, setUsername, setSoulPermission
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
