import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from '../middlewares/Api';

import ModalConfirmCombine from '../components/ModalConfirmCombine';
import ModalMessage from '../components/ModalMessage';

import { Loading } from '../components/Loading';

import { Menu } from '../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
import ImgItems from './../static/images/items/bottle.png';

import ImgPieces from './../static/images/items/set2no_num.png';

import {
    onAccountLogout,
    setUsername,
    setSoulPermission
} from '../actions/AccountActions';

import white_horse_set_part1 from './../static/images/items/white_horse_set_part1_nonumb.png';
import white_horse_set_part2 from './../static/images/items/white_horse_set_part2_nonumb.png';
import white_horse_wings_part1 from './../static/images/items/white_horse_wings_part1_nonumb.png';
import white_horse_wings_part2 from './../static/images/items/white_horse_wings_part2_nonumb.png';
import pegasus_pet_part1 from './../static/images/items/pegasus_pet_part1_nonumb.png';
import pegasus_pet_part2 from './../static/images/items/pegasus_pet_part2_nonumb.png';
import love_suit_part1 from './../static/images/items/love_suit_part1_nonumb.png';
import love_suit_part2 from './../static/images/items/love_suit_part2_nonumb.png';
import love_hat_part1 from './../static/images/items/love_hat_part1_nonumb.png';
import love_hat_part2 from './../static/images/items/love_hat_part2_nonumb.png';
import { Permission } from '../components/Permission';

import * as _ from 'lodash';

const item_icons = {
    'MAT_1_1': white_horse_set_part1,
    'MAT_1_2': white_horse_set_part2,
    'MAT_2_1': white_horse_wings_part1,
    'MAT_2_2': white_horse_wings_part2,
    'MAT_3_1': pegasus_pet_part1,
    'MAT_3_2': pegasus_pet_part2,
    'MAT_4_1': love_suit_part1,
    'MAT_4_2': love_suit_part2,
    'MAT_5_1': love_hat_part1,
    'MAT_5_2': love_hat_part2,
};

// lodash
class TradeContainer extends Component {

    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
            showModal: false,
            showModalMessage: "",
            confirmCombine: false,
            confirmCombineMessage: 'ยืนยันการแลกยาโชคชะตา ขนาดใหญ่',
            ItemsList: [
                {
                    img: ImgItems,
                },

            ],
            pieces: [
                {
                    img: ImgPieces,
                },
                {
                    img: ImgPieces,
                },
                {
                    img: ImgPieces,
                },
                {
                    img: ImgPieces,
                }
            ],
            dataStore: [
                {
                    id: 'MAT_1_1',
                    counter: 0,
                    title: '',
                    use: 0
                },
                {
                    id: 'MAT_1_2',
                    counter: 0,
                    title: '',
                    use: 0
                },
                {
                    id: 'MAT_2_1',
                    counter: 0,
                    title: '',
                    use: 0
                },
                {
                    id: 'MAT_2_2',
                    counter: 0,
                    title: '',
                    use: 0
                },
                {
                    id: 'MAT_3_1',
                    counter: 0,
                    title: '',
                    use: 0
                },
                {
                    id: 'MAT_3_2',
                    counter: 0,
                    title: '',
                    use: 0
                },
                {
                    id: 'MAT_4_1',
                    counter: 0,
                    title: '',
                    use: 0
                },
                {
                    id: 'MAT_4_2',
                    counter: 0,
                    title: '',
                    use: 0
                },
                {
                    id: 'MAT_5_1',
                    counter: 0,
                    title: '',
                    use: 0
                },
                {
                    id: 'MAT_5_2',
                    counter: 0,
                    title: '',
                    use: 0
                }
            ]
        }
    }

    apiEventInfo() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "event_info" };
        let successCallback = (data) => {
            this.setState({
                permission: true,
                showLoading: false,
            })
            if (data.content) {
                this.props.setUsername(data.content.username);
                // this.props.setSoulPermission(data.content);
            }
            this.handleMaterialApi();
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            } else if (data.type === "no_char") {
                this.apiGetCharacter();
            } else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scollDown === false) this.setState({ scollDown: true });
        } else {
            if (this.state.scollDown === true) this.setState({ scollDown: false });
        }
    }

    componentDidMount() {
        if (this.props.jwtToken !== "") {
            setTimeout(() => this.apiEventInfo(), 1000);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo()
            }, 1000);
        }
    }

    handleIncrement(key) {
        const { dataStore } = this.state;
        const findex = _.findIndex(dataStore, item => item.id === key);
        if (dataStore[findex].use < dataStore[findex].counter) {
            dataStore[findex].use++;
            this.setState({
                dataStore: dataStore
            })
        }
    }

    handleDecrement(key) {
        const { dataStore } = this.state;
        const findex = _.findIndex(dataStore, item => item.id === key);
        if ((dataStore[findex].use) > 0) {
            dataStore[findex].use--;
            this.setState({
                dataStore: dataStore
            })
        }
    }

    handleMaterialApi() {
        apiPost("REACT_APP_API_POST_MATERIAL", this.props.jwtToken,
            {
                type: 'my_material'
            },
            (resp) => { // success response
                if (resp.status) {
                    if (resp.content) {
                        let materials = resp.content.materials;
                        let dataStore = [];
                        materials.forEach(element => {
                            element.forEach(resp => {
                                dataStore.push(resp)
                            })
                        });
                        this.setState({
                            dataStore: dataStore.map(resp => ({
                                id: resp.id,
                                counter: resp.quantity,
                                title: resp.title,
                                use: 0
                            }))
                        });
                    }
                } else {
                    this.setState({
                        showLoading: false,
                        showModal: true,
                        showModalMessage: resp.message,
                    });
                }
            },
            (data) => { // error response
                if (data.type == 'no_permission') {
                    this.setState({
                        showLoading: false,
                        permission: false,
                    });
                } else {
                    this.setState({
                        showLoading: false,
                        showModal: true,
                        showModalMessage: data.message,
                    });
                }
            });
    }

    hasExchangeItem() {
        const encounter = _.findIndex(this.state.dataStore, item => item.use > 0);
        if (encounter === -1) {
            return false
        } else {
            return true;
        }
    }

    handleSubmitReddem() {
        this.setState({
            confirmCombine: false
        });
        if (this.hasExchangeItem()) {
            this.setState({ showLoading: true });
            let self = this;
            let dataSend = {
                type: "exchange_potion", item_data: this.state.dataStore.map(item => ({
                    item_id: item.id,
                    number: item.use
                }))
            };
            let successCallback = (data) => {
                this.handleMaterialApi();
                this.setState({
                    permission: true,
                    showLoading: false,
                    showModal: true,
                    showModalMessage: data.message,
                })
            };
            const failCallback = (data) => {
                if (data.type == 'no_permission') {
                    this.setState({
                        showLoading: false,
                        permission: false,
                    });

                } else if (data.type === "no_char") {
                    this.apiGetCharacter();
                } else {
                    this.setState({
                        showLoading: false,
                        showModal: true,
                        showModalMessage: data.message,
                    });
                }
            };
            apiPost("REACT_APP_API_POST_REDEEM_POTION", this.props.jwtToken, dataSend, successCallback, failCallback);
        } else {
            this.setState({
                showModal: true,
                showModalMessage: 'กรุณาเลือกชิ้นส่วนสำหรับใช้ในการแลกยาโชคชะตาขนาดใหญ่'
            })
        }
    }

    mapImage(key_item) {
        if (item_icons[key_item]) {
            return item_icons[key_item];
        } else {
            return '';
        }
    }

    renderItemMaterial() {
        return this.state.dataStore.map(item => {
            return (
                <div>
                    <div className="items__pieces--left">
                        <div className="items__box--inner">
                            <img src={this.mapImage(item.id)} alt={item.title} />
                            <div className="items__box--count">{item.counter}</div>
                        </div>
                    </div>
                    <div className="items__pieces--right">
                        <div className="count">{item.use}</div>
                        <button type="button" className="btn btn-plus" onClick={() => this.handleIncrement(item.id)}>+</button>
                        <button type="button" className="btn btn-minus" onClick={() => this.handleDecrement(item.id)}>-</button>
                    </div>
                </div>
            );
        })
    }

    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: 700, opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                            onScroll={this.handleScroll.bind(this)}
                        >
                            <div className="preorder">
                                <div className="page__wrapper">
                                    <Menu />
                                    <section className="preorder__section trade">
                                        <div className="container">
                                            <div className="section__head">
                                                <h3>แลกยาโชคชะตา ขนาดใหญ่</h3>
                                            </div>
                                            <div className="section__content items__wrapper clearfix text-center">
                                                <div className="items__wrapper--left line">
                                                    <div>
                                                        <b>1. แลกยาโชคชะตา ขนาดใหญ่</b><br />
                                                        (1 เศษแลกได้ 10 ขวด)
                                                    </div>
                                                    <div className="items__wrapper--leftInner">
                                                        {
                                                            this.state.ItemsList.map((items, index) => {
                                                                return (
                                                                    <div className="items__box">
                                                                        <div className="items__box--inner">
                                                                            <img src={items.img} />
                                                                            {/* <div className="items__box--count">99</div> */}
                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                                <div className="items__wrapper--right">
                                                    <div>
                                                        <b>2. เลือกชิ้นส่วนที่ต้องการแลก</b><br />
                                                        (จำนวนชิ้นส่วนที่ใช้แลก 1 ชิ้น)
                                                    </div>
                                                    <div className="items__wrapper--rightInner">
                                                        {
                                                            this.renderItemMaterial()
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="section__btn">
                                                <div className="btn-default" onClick={() => { this.setState({ confirmCombine: true }) }}>รับของรางวัล</div>
                                                <Link to="/soul_gachapon/soul" className="btn btn-default">ซื้อโซลเพิ่ม</Link>
                                            </div>
                                        </div>
                                    </section>

                                </div>
                            </div>
                        </ScrollArea>
                    </div>
                    :
                    <Permission />
                }

                <ModalConfirmCombine
                    open={this.state.confirmCombine}
                    actConfirm={() => { this.handleSubmitReddem() }}
                    actClose={() => this.setState({
                        confirmCombine: false
                    })}
                    msg={this.state.confirmCombineMessage}
                />

                <ModalMessage
                    open={this.state.showModal}
                    actClose={() => this.setState({ showModal: false })}
                    msg={this.state.showModalMessage}
                />

                <Loading
                    open={this.state.showLoading}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    setSoulPermission
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TradeContainer)
