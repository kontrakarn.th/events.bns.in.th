import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import ModalMessage from '../components/ModalMessage';
import ModalConfirmOpenSoul from '../components/ModalConfirmOpenSoul';

import { Menu } from './../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import ball1 from './../static/images/ball1.png'
import ball2 from './../static/images/ball2.png'
import ball3 from './../static/images/ball3.png'

import SoulEffect from './../components/SoulEffect';

/* import iconScroll from './../static/images/icons/icon_scroll.png';
import iconMenu from './../static/images/icon_menu.png' */

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
} from '../actions/AccountActions';

class SoulContainer extends Component {

    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scrollDown: false,
            showModalConfirmOpenSoul: false,
            modalContent: {},
            soulItems: [
                {
                    id: 1,
                    text: 'เปิด 1 โซล',
                    text_confirm: 'ยืนยันซื้อโซล 1 ลูก?',
                    img: ball1,
                    count: '2,500',
                    img_h: "79px",
                    img_w: "140px",
                },
                {
                    id: 2,
                    text: 'เปิด 10+1 โซล',
                    text_confirm: 'ยืนยันซื้อโซล 10+1 ลูก?',
                    img: ball2,
                    count: '25,000',
                    img_h: "83px",
                    img_w: "140px",
                },
                {
                    id: 3,
                    text: 'โบนัส AirPay<br />จำนวนที่เหลือ ',
                    text_confirm: 'รับโบนัส AirPay?',
                    img: ball3,
                    count: '0/5',
                    img_h: "75px",
                    img_w: "165px",
                }
            ],

            showModalMessage: "",
            username: "",
            buy_soul_info: [],
            buy_soul_result : [],
        }
    }


    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 800);

    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo();
            }, 800);
        }
    }

    apiEventInfo() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "event_info" };
        let successCallback = (data) => {
            
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    // username: data.content.username,
                    buy_soul_info: data.content
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scrollDown === false) this.setState({ scrollDown: true });
        } else {
            if (this.state.scrollDown === true) this.setState({ scrollDown: false });
        }
    }

    ConfirmOpenSoul(id=1) {
        // console.log(id);
        this.setState({
            showModalConfirmOpenSoul: true,
            modalContent: this.state.soulItems[id-1]
        });
        /* switch (id) {
            case 1:
                this.setState({
                    showModalConfirmOpenSoul: true,
                    modalContent: this.state.soulItems.find(item => item.id === id)
                });
                break;
            case 2:
                this.setState({
                    showModalConfirmOpenSoul: true,
                    modalContent: this.state.soulItems.find(item => item.id === id)
                });
                break;
            case 3:
                this.setState({
                    showModalConfirmOpenSoul: true,
                    modalContent: this.state.soulItems.find(item => item.id === id)
                });
                break;
            default:
                break;
        } */
    }

    apiConfirmOpenSoul() {
        const package_id = this.state.modalContent.id;

        this.props.setBuySoulResult([]);

        if (package_id === 1 || package_id === 2) {
            this.setState({
                showLoading: true
            });
            apiPost(
                "REACT_APP_API_POST_BUY_SOUL",
                this.props.jwtToken,
                {
                    type: 'buysoul',
                    package_id: package_id
                },
                (resp) => { // success response
                    if (resp.status) {
                        this.props.setBuySoulResult(resp.content);
                        this.props.history.push(process.env.REACT_APP_EVENT_PATH + '/open-soul')
                    } else {
                        this.setState({
                            showLoading: false,
                            showModalMessage: resp.message,
                        });
                    }
                },
                (data) => { // error response
                    if (data.type == 'no_permission') {
                        this.setState({
                            showLoading: false,
                            permission: false,
                        });

                    }else {
                        this.setState({
                            showLoading: false,
                            showModalMessage: data.message,
                        });
                    }
                });
        } else if (package_id == 3) {
            // airpay logic
            this.setState({ showLoading: true });
            let dataSend = { type: "redeem_airpay_promotion" };
            let successCallback = (resp) => {
                if (resp.status) {
                    this.props.setBuySoulResult(resp.content);
                    this.props.history.push(process.env.REACT_APP_EVENT_PATH + '/open-soul')
                } else {
                    this.setState({
                        showLoading: false,
                        showModalMessage: resp.message,
                    });
                }
            };
            const failCallback = (data) => {
                if (data.type == 'no_permission') {
                    this.setState({
                        showLoading: false,
                        permission: false,
                    });

                }else {
                    this.setState({
                        showLoading: false,
                        showModalMessage: data.message,
                    });
                }
            };
            apiPost("REACT_APP_API_POST_REDEEM_AIRPAY_PROMOTION", this.props.jwtToken, dataSend, successCallback, failCallback);
        }
    }

    checkSoulPermission(items) {
        if (items.id === 1 && this.state.buy_soul_info.soul_1 == false) {
            return <div className="btn-soul disabled">ไดมอนต์ไม่พอ</div>;
        } else if (items.id === 2 && this.state.buy_soul_info.soul_10 == false) {
            return <div className="btn-soul disabled">ไดมอนต์ไม่พอ</div>;
        } else if (items.id === 3 && this.state.buy_soul_info.airpay_info && this.state.buy_soul_info.airpay_info.airpay_can_redeem == false) {
            if(this.state.buy_soul_info.airpay_info.airpay_used == this.state.buy_soul_info.airpay_info.airpay_total_rights){
                return <div className="btn-soul disabled">{this.state.buy_soul_info.airpay_info.airpay_used+"/"+this.state.buy_soul_info.airpay_info.airpay_total_rights}</div>;
            }else{
                if(this.state.buy_soul_info.airpay_info.airpay_remain_all_rights == 0){
                    return <div className="btn-soul disabled">สิทธิ์เต็มแล้ว</div>;
                }else{
                    return <div className="btn-soul disabled">ไม่ตรงเงื่อนไข</div>;
                }
            }
        } else {
            if(items.id === 3 && this.state.buy_soul_info.airpay_info && this.state.buy_soul_info.airpay_info.airpay_can_redeem == true){
                return <div className="btn-soul" onClick={(id)=>this.ConfirmOpenSoul(items.id)}>{this.state.buy_soul_info.airpay_info.airpay_used+"/"+this.state.buy_soul_info.airpay_info.airpay_total_rights}</div>
            }else{
                return <div className="btn-soul" onClick={(id)=>this.ConfirmOpenSoul(items.id)}>{items.count}</div>
            }
        }
    }

    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: 700, opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                            onScroll={this.handleScroll.bind(this)}
                        >
                            <div className="preorder">
                                <div className="page__wrapper">
                                    <Menu />
                                    <section className="preorder__section soul">

                                        <div className="container">
                                            <div className="section__head">
                                                <h3>ซื้อโซล</h3>
                                            </div>
                                            <div className="section__content">
                                                {
                                                    this.state.soulItems.map((items, index) => {
                                                        return (
                                                            <div className="soul__items">
                                                                <div className="soul__items--text" dangerouslySetInnerHTML={{__html: (this.state.buy_soul_info && this.state.buy_soul_info.airpay_info && items.id == 3) ? items.text + this.state.buy_soul_info.airpay_info.airpay_remain_all_rights_text : items.text}}></div>
                                                                <div className="soul__items--img">
                                                                    <div style={{height: items.img_h, width: items.img_w, margin: "0 auto"}}>
                                                                        <SoulEffect 
                                                                            open={false}
                                                                            active={false}
                                                                            soul={items.img}
                                                                            item=""
                                                                        />
                                                                    </div>
                                                                </div>
                                                                {this.checkSoulPermission(items)}
                                                            </div>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>

                                    </section>

                                </div>
                            </div>
                        </ScrollArea>

                        <ModalConfirmOpenSoul
                            open={this.state.showModalConfirmOpenSoul}
                            actConfirm={() => this.apiConfirmOpenSoul()}
                            actClose={() => this.setState({
                                showModalConfirmOpenSoul: false
                            })}
                            msg={this.state.modalContent.text_confirm}
                        />

                        <ModalMessage
                            open={this.state.showModalMessage.length > 0}
                            actClose={()=>this.setState({showModalMessage: ""})}
                            msg={this.state.showModalMessage}
                        />
                    </div>
                    :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    // permission: state.AccountReducer.permission,
    buy_soul_result: state.AccountReducer.buy_soul_result
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(SoulContainer))
