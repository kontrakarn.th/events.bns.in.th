import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import {apiPost} from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import ModalMessage from '../components/ModalMessage';
import ModalConfirmRedeem from '../components/ModalConfirmRedeem';

import {Menu} from '../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
import ImgItems from './../static/images/items/set1no_num.png'
import ImgPieces from './../static/images/items/set2no_num.png'

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
} from '../actions/AccountActions';

import white_horse_set from './../static/images/items/white_horse_set_nonumb.png';
import white_horse_wings from './../static/images/items/white_horse_wings_nonumb.png';
import pegasus_pet from './../static/images/items/pegasus_pet_nonumb.png';
import love_suit from './../static/images/items/love_suit_nonumb.png';
import love_hat from './../static/images/items/love_hat_nonumb.png';
import doflamingo_weapon_skin from './../static/images/items/doflamingo_weapon_skin_nonumb.png';
import white_horse_set_part1 from './../static/images/items/white_horse_set_part1_nonumb.png';
import white_horse_set_part2 from './../static/images/items/white_horse_set_part2_nonumb.png';
import white_horse_wings_part1 from './../static/images/items/white_horse_wings_part1_nonumb.png';
import white_horse_wings_part2 from './../static/images/items/white_horse_wings_part2_nonumb.png';
import pegasus_pet_part1 from './../static/images/items/pegasus_pet_part1_nonumb.png';
import pegasus_pet_part2 from './../static/images/items/pegasus_pet_part2_nonumb.png';
import love_suit_part1 from './../static/images/items/love_suit_part1_nonumb.png';
import love_suit_part2 from './../static/images/items/love_suit_part2_nonumb.png';
import love_hat_part1 from './../static/images/items/love_hat_part1_nonumb.png';
import love_hat_part2 from './../static/images/items/love_hat_part2_nonumb.png';

const item_icons = {
    'MAT_1_1' : white_horse_set_part1,
    'MAT_1_2' : white_horse_set_part2,
    'MAT_2_1' : white_horse_wings_part1,
    'MAT_2_2' : white_horse_wings_part2,
    'MAT_3_1' : pegasus_pet_part1,
    'MAT_3_2' : pegasus_pet_part2,
    'MAT_4_1' : love_suit_part1,
    'MAT_4_2' : love_suit_part2,
    'MAT_5_1' : love_hat_part1,
    'MAT_5_2' : love_hat_part2,
};

class RedeemContainer extends Component {
   
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
            showModalMessage: "",
            
            confirmRedeem: false,
            confirmRedeemMessage: "",
            itemData: [],

            username: "",

            materials: [],

            showMenu: false,

            ItemsList: [
                {
                    id: 1,
                    title: "เซ็ตม้าขาว",
                    img: white_horse_set,
                },
                {
                    id: 2,
                    title: "ปีกม้าขาว",
                    img: white_horse_wings,
                },
                {
                    id: 3,
                    title: "หินล้ำค่าเปกาซัส",
                    img: pegasus_pet,
                },
                {
                    id: 4,
                    title: "ชุดรักหวานชื่น",
                    img: love_suit,
                },
                {
                    id: 5,
                    title: "หมวกรักกันไม่เสื่อมคลาย",
                    img: love_hat,
                },
                {
                    id: 6,
                    title: "กล่องอาวุธลวงตาฟลามิงโก้",
                    img: doflamingo_weapon_skin,
                },
                
            ],
            materials_mock: [
                {
                    img: white_horse_set_part1,
                },
                {
                    img: white_horse_set_part2,
                },
                {
                    img: white_horse_wings_part1,
                },
                {
                    img: white_horse_wings_part2,
                },
                {
                    img: pegasus_pet_part1,
                },
                {
                    img: pegasus_pet_part2,
                },
                {
                    img: love_suit_part1,
                },
                {
                    img: love_suit_part2,
                },
                {
                    img: love_hat_part1,
                },
                {
                    img: love_hat_part2,
                },
            ],


            packageId: 0,
            packageName: '',
            require_quantity: 0,
            materials_total_quantity: 0,
            MAT_1_1: 0,
            MAT_1_2: 0,

            MAT_2_1: 0,
            MAT_2_2: 0,

            MAT_3_1: 0,
            MAT_3_2: 0,

            MAT_4_1: 0,
            MAT_4_2: 0,

            MAT_5_1: 0,
            MAT_5_2: 0,

        }
    }

    resetState(){
        this.setState({
            packageId: 0,
            packageName: '',
            require_quantity: 0,
            materials_total_quantity: 0,
            MAT_1_1: 0,
            MAT_1_2: 0,

            MAT_2_1: 0,
            MAT_2_2: 0,

            MAT_3_1: 0,
            MAT_3_2: 0,

            MAT_4_1: 0,
            MAT_4_2: 0,

            MAT_5_1: 0,
            MAT_5_2: 0,
        });
    }

    handleClickMenu() {
        // console.log(this.state.showMenu)
        this.setState({
            showMenu:!this.state.showMenu
        })
    }

    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiMaterials(), 1000);

    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiMaterials();
            }, 1000);
        }
    }

    apiMaterials() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "my_material" };
        let successCallback = (data) => {
            
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    materials: data.content.materials,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_MATERIAL", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }

    handleClickActive(index,packageId,packageName){
        this.resetState();
        this.setState({
            active:index,
            packageId: packageId,
            packageName: packageName,
            require_quantity: this.checkMaterialRequireByPackage(packageId),
        })
    }

    checkMaterialRequireByPackage(packageId){
        switch(packageId){

            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return 5;
                break;

            default:
                return false;
                break;
        }

        return false;
    }

    increaseMaterialCount(e, materialId, quantity){
        e.preventDefault();
        
        if(this.state.require_quantity && this.state.require_quantity !== 0){
            if(quantity > 0){

                let materialTotalQuantity = this.state.materials_total_quantity;
                let materialQuantity = this.state[materialId];

                if(materialQuantity < quantity){

                    if(materialTotalQuantity < this.state.require_quantity){

                        this.setState({
                            [materialId]: materialQuantity+1,
                            materials_total_quantity: materialTotalQuantity+1,
                        });

                    }else{
                        this.setState({
                            showModalMessage: 'สามารถรวมชิ้นส่วนทั้งหมดได้ครังละ<br />'+this.state.require_quantity+' ชิ้นเท่านั้น',
                        });
                    }

                }

            }
        }else{
            this.setState({
                showModalMessage: 'กรุณาเลือกไอเทมที่ต้องการแลก',
            });
        }
    }

    decreaseMaterialCount(e, materialId, quantity){
        e.preventDefault();
        
        if(this.state.require_quantity && this.state.require_quantity !== 0){
            if(quantity > 0){

                let materialTotalQuantity = this.state.materials_total_quantity;
                let materialQuantity = this.state[materialId];

                if(materialQuantity > 0 && materialQuantity <= this.state.require_quantity && this.state.require_quantity > 0){

                    if(materialTotalQuantity <= this.state.require_quantity){

                        this.setState({
                            [materialId]: materialQuantity-1,
                            materials_total_quantity: materialTotalQuantity-1,
                        });

                    }

                }

            }
        }else{
            this.setState({
                showModalMessage: 'กรุณาเลือกไอเทมที่ต้องการแลก',
            });
        }
    }

    onConfirmToExchange(){
        if(this.state.packageId > 0 && this.state.require_quantity > 0){
            if(this.state.materials_total_quantity == this.state.require_quantity && this.state.require_quantity > 0){

                let item_data = [];
                
                this.state.materials.map((items, index) => {
                    items.map((items2, index2) => {
                        item_data.push({
                            item_id: items2.id,
                            number: this.state[items2.id]
                        })
                    })
                });

                console.log(item_data);
                this.setState({
                    itemData: item_data,
                    confirmRedeem: true,
                    confirmRedeemMessage: "ยืนยันแลก "+this.state.packageName+" ?"
                });

            }else{
                this.setState({
                    showModalMessage: 'ชิ้นส่วนที่คุณเลือกยังไม่ครบ '+this.state.require_quantity+' ชิ้น',
                });
            }
        }else{
            this.setState({
                showModalMessage: 'กรุณาเลือกไอเทมที่ต้องการแลก',
            });
        }
    }

    apiConfirmRedeem(){
        this.setState({ showLoading: true, confirmRedeem: false, confirmRedeemMessage: "" });
        let self = this;
        let dataSend = { type: "redeem", package_id: this.state.packageId, item_data: this.state.itemData };
        let successCallback = (data) => {
            this.resetState();
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    showModalMessage: data.message,
                    materials: data.content.materials,
                })
            }
        };
        const failCallback = (data) => {
            this.resetState();
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                    confirmRedeem: false,
                    confirmRedeemMessage: "",
                });

            }else {
                this.setState({
                    showLoading: false,
                    confirmRedeem: false,
                    confirmRedeemMessage: "",
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
   
    render() {
        let _i = 1;
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="preorder">
                                <div className="page__wrapper">
                                    <Menu />
                                    <section className="preorder__section redeem">
                                        <div className="container">
                                            <div className="section__head">
                                                <h3>แลกชิ้นส่วน</h3>
                                            </div>
                                            <div className="section__content items__wrapper clearfix text-center">  
                                                <div className="items__wrapper--left line">
                                                    <div>
                                                        <b>1. เลือกไอเท็มที่ต้องการแลก</b><br/>
                                                        (เลือกได้ครั้งละ 1 ชิ้น)
                                                    </div>
                                                    <div className="items__wrapper--leftInner">
                                                        {
                                                            this.state.ItemsList.map((items, index) => {
                                                                return(
                                                                    <div className="items__box send-item">
                                                                        <div onClick={()=>this.handleClickActive(index,items.id,items.title)} className={"items__box--inner select-item " + (this.state.active === index ? 'active' : '')}>
                                                                            <img src={items.img}/>
                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                                <div className="items__wrapper--right">
                                                    <div>
                                                        <b>2. เลือกชิ้นส่วนที่ต้องการแลก</b><br/>
                                                        (จำนวนชิ้นส่วนที่ต้องการแลก 5 ชิ้น)
                                                    </div>
                                                    <div className="items__wrapper--rightInner">
                                                    {
                                                        this.state.materials && this.state.materials.length > 0 ?
                                                            this.state.materials.map((items, index) => {
                                                                return(
                                                                    items.map((items2, index2) => {
                                                                        return(
                                                                            <div>
                                                                                <div className="items__pieces--left">
                                                                                    <div className="items__box--inner">
                                                                                        <img src={item_icons[items2.id]}/>
                                                                                        <div className="items__box--count">
                                                                                        {
                                                                                            items2.quantity
                                                                                        }
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="items__pieces--right">
                                                                                    <div className="count">
                                                                                    {
                                                                                        this.state[items2.id] || 0
                                                                                    }
                                                                                    </div>
                                                                                    <button type="button" className="btn btn-plus" onClick={(event,materialId,quantity)=>this.increaseMaterialCount(event, items2.id,items2.quantity)}>+</button>
                                                                                    <button type="button" className="btn btn-minus" onClick={(event,materialId,quantity)=>this.decreaseMaterialCount(event, items2.id,items2.quantity)}>-</button>
                                                                                </div>
                                                                            </div>
                                                                        )
                                                                        _i++
                                                                    })
                                                                )
                                                            })
                                                        :
                                                            this.state.materials_mock.map((items, index) => {
                                                                return(
                                                                    <div>
                                                                        <div className="items__pieces--left">
                                                                            <div className="items__box--inner">
                                                                                <img src={items.img}/>
                                                                                <div className="items__box--count">0</div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="items__pieces--right">
                                                                            <div className="count">0</div>
                                                                            <button type="button" className="btn btn-plus">+</button>
                                                                            <button type="button" className="btn btn-minus">-</button>
                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                    }
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="section__btn">
                                                <div className="btn-default" onClick={this.onConfirmToExchange.bind(this)}>แลกของรางวัล</div>
                                                <Link to="/soul_gachapon/soul" className="btn-default">ซื้อโซลเพิ่ม</Link>
                                            </div>
                                        </div>   
                                    </section>
                                
                                </div>
                            </div>
                        </ScrollArea>

                        <ModalConfirmRedeem
                            open={this.state.confirmRedeem}
                            actConfirm={() => this.apiConfirmRedeem()}
                            actClose={() => this.setState({
                                confirmRedeem: false
                            })}
                            msg={this.state.confirmRedeemMessage}
                        />

                        <ModalMessage
                            open={this.state.showModalMessage.length > 0}
                            actClose={()=>this.setState({showModalMessage: ""})}
                            msg={this.state.showModalMessage}
                        />
                    </div>
                :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />
            </div>          
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}
export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(RedeemContainer))
