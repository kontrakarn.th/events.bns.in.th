import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import {
    withRouter
} from 'react-router-dom'
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import {apiPost} from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';

import {Menu} from './../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
import ImgSoul from './../static/images/ball1.png'

import white_horse_set from './../static/images/items/white_horse_set_nonumb.png';
import white_horse_wings from './../static/images/items/white_horse_wings_nonumb.png';
import pegasus_pet from './../static/images/items/pegasus_pet_nonumb.png';
import love_suit from './../static/images/items/love_suit_nonumb.png';
import love_hat from './../static/images/items/love_hat_nonumb.png';
import doflamingo_weapon_skin from './../static/images/items/doflamingo_weapon_skin_nonumb.png';
import white_horse_set_part1 from './../static/images/items/white_horse_set_part1_nonumb.png';
import white_horse_set_part2 from './../static/images/items/white_horse_set_part2_nonumb.png';
import white_horse_wings_part1 from './../static/images/items/white_horse_wings_part1_nonumb.png';
import white_horse_wings_part2 from './../static/images/items/white_horse_wings_part2_nonumb.png';
import pegasus_pet_part1 from './../static/images/items/pegasus_pet_part1_nonumb.png';
import pegasus_pet_part2 from './../static/images/items/pegasus_pet_part2_nonumb.png';
import love_suit_part1 from './../static/images/items/love_suit_part1_nonumb.png';
import love_suit_part2 from './../static/images/items/love_suit_part2_nonumb.png';
import love_hat_part1 from './../static/images/items/love_hat_part1_nonumb.png';
import love_hat_part2 from './../static/images/items/love_hat_part2_nonumb.png';
import hive_queen_wing from './../static/images/items/hive_queen_wing.png';
import moonstone from './../static/images/items/moonstone.png';
import soulstone from './../static/images/items/soulstone.png';
import hongmoon_medal from './../static/images/items/hongmoon_medal.png';
import achievement_medal from './../static/images/items/achievement_medal.png';
import blackshark_scale from './../static/images/items/blackshark_scale.png';
import bluesky_scale from './../static/images/items/bluesky_scale.png';
import firedragon_scale from './../static/images/items/firedragon_scale.png';
import encourage_stone from './../static/images/items/encourage_stone.png';
import advanced_transmute_stone from './../static/images/items/advanced_transmute_stone.png';
import hongmoon_soul_crystal from './../static/images/items/hongmoon_soul_crystal.png';

import SoulEffect from './../components/SoulEffect';

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
} from '../actions/AccountActions';

const item_icons = {
    'white_horse_set' : white_horse_set,
    'white_horse_wings' : white_horse_wings,
    'pegasus_pet' : pegasus_pet,
    'love_suit' : love_suit,
    'love_hat' : love_hat,
    'doflamingo_weapon_skin' : doflamingo_weapon_skin,
    'white_horse_set_part1' : white_horse_set_part1,
    'white_horse_set_part2' : white_horse_set_part2,
    'white_horse_wings_part1' : white_horse_wings_part1,
    'white_horse_wings_part2' : white_horse_wings_part2,
    'pegasus_pet_part1' : pegasus_pet_part1,
    'pegasus_pet_part2' : pegasus_pet_part2,
    'love_suit_part1' : love_suit_part1,
    'love_suit_part2' : love_suit_part2,
    'love_hat_part1' : love_hat_part1,
    'love_hat_part2' : love_hat_part2,
    'hive_queen_wing' : hive_queen_wing,
    'moonstone' : moonstone,
    'soulstone' : soulstone,
    'hongmoon_medal' : hongmoon_medal,
    'achievement_medal' : achievement_medal,
    'blackshark_scale' : blackshark_scale,
    'bluesky_scale' : bluesky_scale,
    'firedragon_scale' : firedragon_scale,
    'encourage_stone' : encourage_stone,
    'advanced_transmute_stone' : advanced_transmute_stone,
    'hongmoon_soul_crystal' : hongmoon_soul_crystal,
};

class OpenSoulContainer extends Component {
   
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
            showModalMessage: "",

            showModalConfirmRandomGachapon: false,
            gacha: -1,
            rnd_gacha: false,

            open_all: false,

            soulItems: [
                {
                    img: ImgSoul,
                },
                {
                    img: ImgSoul,
                },
                {
                    img: ImgSoul,
                },
                {
                    img: ImgSoul,
                },
                {
                    img: ImgSoul,
                },
                {
                    img: ImgSoul,
                },
                {
                    img: ImgSoul,
                },
                {
                    img: ImgSoul,
                },
                {
                    img: ImgSoul,
                },
                {
                    img: ImgSoul,
                },
                {
                    img: ImgSoul,
                },
            ],

        }
    }

    /* apiEventInfo() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "event_info" };
        let successCallback = (data) => {
            
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    // username: data.content.username,
                    buy_soul_info: data.content
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    } */

    handleClickMenu() {
        // console.log(this.state.showMenu)
        this.setState({
            showMenu:!this.state.showMenu
        })
    }

    componentDidMount(){
        // console.log(this.props.jwtToken);
        // if(this.props.jwtToken !== "") setTimeout(()=>this.apiEventInfo(),800);
        
    }
    componentDidUpdate(prevProps, prevState) {
        /* if(this.props.jwtToken !== prevProps.jwtToken){
            setTimeout(()=>{
                this.apiEventInfo()
            },600);
        } */
        // console.log(this.props.buy_soul_result);
        if(this.props.buy_soul_result.length == 0){
            this.props.history.push(process.env.REACT_APP_EVENT_PATH + '/soul')
        }
    }

    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }

   
    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="preorder">
                                <div className="page__wrapper">
                                    <Menu />
                                    <section className="preorder__section opensoul">
                                        <div className="container">
                                            <div className="section__head">
                                                <h3>เปิดโซล</h3>
                                            </div>
                                            <div className="section__content items__wrapper"> 
                                                <div className="content__scroll">
                                                    {
                                                        this.props.buy_soul_result && this.props.buy_soul_result.length > 0 ?
                                                            this.props.buy_soul_result.map((items, index) => {
                                                                return(
                                                                    <div className={"items__box "+(this.props.buy_soul_result.length == 1 ? "one_item" : "")}>
                                                                        <SoulEffect 
                                                                            open={this.state.open_all}
                                                                            active={true}
                                                                            soul={ImgSoul}
                                                                            item={item_icons[items.key]}
                                                                        />
                                                                    </div>
                                                                )
                                                            })
                                                        :
                                                            null
                                                    }
                                                </div> 
                                            </div>
                                            <div className="section__btn">
                                                <Link to="/soul_gachapon/soul" className="btn btn-default">ซื้อโซลเพิ่ม</Link>
                                                <div className="btn-default" onClick={()=>this.setState({open_all:true})}>เปิดทั้งหมด</div>
                                            </div>
                                        </div>   
                                    </section>
                                
                                </div>
                            </div>
                        </ScrollArea>
                    </div>
                :
                    <Permission />
                }
            </div>          
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    buy_soul_result: state.AccountReducer.buy_soul_result
})
const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}
export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(OpenSoulContainer))

/* export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OpenSoulContainer) */
