import React from "react";
import { connect } from "react-redux";
import { setValue } from "./redux";

const Component = props => (
    <div>
        Home
    </div>
)

const mapStateToProps = state => state.form;
const mapDispatchToProps = { setValue };

export default connect( mapStateToProps, mapDispatchToProps )(Component);
