import React from 'react';

import './../styles/index.css';
import './../styles/modal.css';
import './../styles/f11layout.css';
// import HomeComponent from './../features/home/' ;
import HistoryListContainer from '../containers/HistoryListContainer';

export class HistoryList extends React.Component {
    render() {
        return (<HistoryListContainer />)
    }
}
