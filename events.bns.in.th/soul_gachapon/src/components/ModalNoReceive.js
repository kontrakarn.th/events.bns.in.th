import React from 'react';
import Modal from './Modal';

export default class ModalNoReceive extends React.Component {
    render() {
        let msg = this.props.data && this.props.data.package_title || "";
        return (
            <Modal
                open={this.props.open}
                title="แจ้งเตือน"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox">
                    ไม่สามารถส่งได้ <br/>
                    เนื่องจากไดมอนด์ <br/>
                    ไม่เพียงพอ
                </div>
            </Modal>
        )
  }
}
