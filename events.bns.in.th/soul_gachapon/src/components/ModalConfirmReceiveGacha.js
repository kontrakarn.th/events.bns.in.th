import React from 'react';
import Modal from './Modal';

export default class ModalConfirmReceiveGacha extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                title="ยืนยัน"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox">
                    ส่งเซ็ทท้าขาว <br/>
                    จำนวน 1 ชิ้น ไปยัง <br/>
                    ID aaa <br/>
                    หรือไม่?
                    <p>
                        ค่าบริการ <br/>
                        10,000 ไดมอนด์
                    </p>

                </div>
            </Modal>
        )
  }
}
