import React from 'react';
import { Link } from 'react-router-dom';
import iconMenu from './../static/images/icon_menu.png';
import iconMenuClose from './../static/images/menu_close.png';
export class Menu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showMenu:false

        }
    }

    handleClickMenu() {
        this.setState({
            showMenu:!this.state.showMenu
        })
    }

    render() {
        return (
            <div className="left__menu">
                <div className={"bg-overlay " + (this.state.showMenu ? "show" : " ") }></div>
                <a onClick={this.handleClickMenu.bind(this)} className="menu-icon"><img alt="icon" src={iconMenu} /></a>
                <ul className={"left__menu--list " + (this.state.showMenu ? "show" : " ") }>
                    <li className="menu-close"><a onClick={this.handleClickMenu.bind(this)}></a></li>
                    <li><Link to="/soul_gachapon/soul">ซื้อโซล</Link></li>
                    <li><Link to="/soul_gachapon/history">ประวัติการแลก</Link></li>
                    <li><Link to="/soul_gachapon/send">ส่งของรางวัล</Link></li>
                    <li><Link to="/soul_gachapon/combine">ผสมชิ้นส่วน</Link></li>
                    <li><Link to="/soul_gachapon/redeem">แลกชิ้นส่วน</Link></li>
                    <li><Link to="/soul_gachapon/trade">แลกยาโชคชะตา ขนาดใหญ่</Link></li>
                    <li><Link to="/soul_gachapon">เงื่อนไขโปรโมชั่น</Link></li>
                    <li><Link to="/soul_gachapon/costume">ตัวอย่างชุด</Link></li>
                </ul>

            </div>
        )
    }
}
