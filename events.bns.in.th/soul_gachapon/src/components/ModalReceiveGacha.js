import React from 'react';
import Modal from './Modal';

export default class ModalReceiveGacha extends React.Component {
    render() {
        let title = this.props.data && this.props.data || [];
        return (
            <Modal
                open={this.props.open}
                title="กรุณากรอก UID"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox">
                    <input type="text" className="input_uid"/>
                    {title.map((item,index)=>{
                        return (<div>- {item}</div>)
                    })}
                </div>
            </Modal>
        )
  }
}
