import React, { Component } from 'react'
import iconDropdown from './../static/images/icon_dropdown.png'
export default class ModalMessage extends Component {

    actSelectWeapon(e){
        // e.preventDefault();
        // let indexCharacter = document.getElementById("charater_form").value;
        // let idCharacter = this.props.listCharater[indexCharacter].id;
        // let nameCharacter = this.props.listCharater[indexCharacter].name;
        // this.props.actSelectCharacter(idCharacter,nameCharacter)
    }
    render() {
        return (
            <div className={"modal "+(this.props.open ? "":"close")} >
                <div className="modal__backdrop" />
                <div className={"modal__content"}>
                    <div className={"modal__incontent modal__incontent--selectcharacter"} dangerouslySetInnerHTML={{__html: this.props.msg}}></div>
                    <div className={"modal__bottom"}>
                        <a className="modal__button" onClick={this.props.closeModal}>
                            ปิด
                        </a>
                    </div>
                </div>
            </div>
        )
  }
}
