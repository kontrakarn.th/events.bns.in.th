import frame_decor from './../static/images/frame_decor.png';
import item_box2 from './../static/images/item_box2.png';
import item_box3 from './../static/images/item_box3.png';
import item_box4 from './../static/images/item_box4.png';
import title_box1 from './../static/images/title_box1.png';
import title_box2 from './../static/images/title_box2.png';
import title_box3 from './../static/images/title_box3.png';
import title_box4 from './../static/images/title_box4.png';

export const Imglist = {
	frame_decor,
	item_box2,
	item_box3,
	item_box4,
	title_box1,
	title_box2,
	title_box3,
	title_box4,
}