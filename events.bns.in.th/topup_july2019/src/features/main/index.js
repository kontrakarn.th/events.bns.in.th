import React from 'react';
import {connect} from 'react-redux';
import Slider from 'react-slick';
import { apiPost } from './../../middlewares/Api';
import styled, { keyframes } from 'styled-components';

import { onAccountLogout } from './../../actions/AccountActions';
import { setValues } from './redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';

import './css/style.css';
import {Imglist} from './../../constants/Import_Images';

class Main extends React.Component {
    actConfirm(){
        this.apiRedeemItem(
            this.props.modal_confirmitem && this.props.modal_confirmitem !== ''
            && this.props.modal_confirmitem
        );
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content,
                    modal_open: "",
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiRedeemItem(id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'redeem',package_id:id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content,
                    modal_open:'message',
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                    modal_type: 'error',
                });
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: ''
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }



    render() {
        return (
            <div className="main">
                <div className="main__contentwrap">
                        <div className="main__box1">
                                <div className="main__box1--titlebox">
                                        <img src={Imglist['title_box1']} alt=""/>
                                        <ul className="main__box1--list">
                                                <li>1. ผู้เล่นเติมไดมอนด์เข้าเกมตามที่กำหนด สามารถกดรับไอเทมได้ฟรี!</li>
                                                <li>2. ผู้เล่นสามารถเติมไดมอนด์แบบสะสมได้</li>
                                                <li>3. ผู้เล่นสามารถเติมไดมอนด์ได้ทุกช่องทาง</li>
                                                <li>4. สามารถกดรับไอเทมได้ 1 ครั้ง ต่อขั้น / 1 UID</li>
                                                <li>5. โปรโมชั่นเริ่ม 31 กรกฎาคม 2562 12:00:00 น.</li>
                                                <li>6. สิ้นสุดโปรโมชั่นการเติมเงิน วันที่ 7 สิงหาคม 2562 23:59:59 น.</li>
                                                <li>7. ปิดระบบการรับไอเทม วันที่ 14 สิงหาคม 2562 23:59:59 น.</li>
                                        </ul>
                                        <img className="main__box1--decor" src={Imglist['frame_decor']} alt=""/>
                                </div>
                        </div>
                        <div className="main__box2">
                                <div className="main__box2--titlebox">
                                        <img src={Imglist['title_box2']} alt=""/>
                                </div>
                                <img className="main__box2--itembox" src={Imglist['item_box2']} alt=""/>
                                <div className={"main__box2--btn "+(this.props.packages && (this.props.packages[0].can_receive === false || this.props.packages[0].received === true) && ' disabled')} onClick={()=>this.props.setValues({modal_open:'confirm',modal_confirmitem:1,modal_message:'รับไอเทมสะสมครบ<br/>30,000 ไดมอนด์'})}/>
                        </div>
                        <div className="main__box2">
                                <div className="main__box2--titlebox">
                                        <img src={Imglist['title_box3']} alt=""/>
                                </div>
                                <img className="main__box2--itembox" src={Imglist['item_box3']} alt=""/>
                                <div className={"main__box2--btn "+(this.props.packages && (this.props.packages[1].can_receive === false || this.props.packages[1].received === true) && ' disabled')} onClick={()=>this.props.setValues({modal_open:'confirm',modal_confirmitem:2,modal_message:'รับไอเทมสะสมครบ<br/>50,000 ไดมอนด์'})}/>
                        </div>
                        <div className="main__box2">
                                <div className="main__box2--titlebox">
                                        <img src={Imglist['title_box4']} alt=""/>
                                </div>
                                <img className="main__box2--itembox" src={Imglist['item_box4']} alt=""/>
                                <div className={"main__box2--btn "+(this.props.packages && (this.props.packages[2].can_receive === false || this.props.packages[2].received === true) && ' disabled')} onClick={()=>this.props.setValues({modal_open:'confirm',modal_confirmitem:3,modal_message:'รับไอเทมสะสมครบ<br/>100,000 ไดมอนด์'})}/>
                        </div>
                </div>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
            </div>
        )
    }
}
const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);
