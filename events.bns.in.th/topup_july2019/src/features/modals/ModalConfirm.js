import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './redux';

import modal_bg from '../../static/images/modal/modal_bg.png';
import title_text from '../../static/images/modal/title_text.png';
import btns from '../../static/images/btn.png';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="confirm"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <img className="titletext" src={title_text} alt=""/>
                <div dangerouslySetInnerHTML={{__html: modal_message}} />
            </ModalMessageContent>
            <ModalBottom>
                <Btns onClick={()=>{if(props.actConfirm) props.actConfirm()}}/>
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.ModalReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 369px;
    height: 363px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${modal_bg});
    box-sizing: border-box;
    padding: 5%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    div{
        font-size: 30px;
        line-height: 1.5em;
        color: #ffffff;
        word-break: break-word;
        margin-top: 30px;
    }
    .titletext{
        display: block;
        margin: 50px auto 0;
    }
`;
const ModalBottom = styled.div`
    display: block;
    margin: -50px auto 0;
    position: relative;
    z-index: 50;
`;
const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 341px;
    height: 91px;
    background:top center no-repeat url(${btns});
    color: #fdf2e0;
    position:relative;
    text-align: center;
    box-sizing: border-box;
    padding-top: 3%;
    font-size: 24px;
    font-family: DBXtypeX;
    &:hover{
        background-position: bottom center;
    }
`;
