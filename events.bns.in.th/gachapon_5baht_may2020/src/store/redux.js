import { Imglist } from "../constants/Import_Images";

const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    // showCharacterName: true,
    characters: [],
    username: "",
    setCanPlay: '',
    can_play: [false,false,false],
    character: "",
    selected_char: false,
    loginStatus: false,
    showScroll: false,
    noScroll: true,
    loading: false,
    permission: true,
    status: false,

    points: 0,
    points_text: '',
    quests: [],
    rewards: [],


    //===modal
    modal_open: "",//"loading","detail","showreward",""showreward","send","modalInput"
    modal_message: "",
    modal_item_token: "",
    modal_item_name: "",
    modal_uid: "",
    modal_get_name: "",
    modal_combin: -1,
    modal_trade: -1,
    setMsg: 1,
    

    history_get : false,
    received:[],
    redeem:[],
    

    //===soul values
    package_id:0,
    buy_soul_count: 0,
    buy_soul_cost: 0,
    setBuySoulResult:[],
    //buy_soul_result: [],
    // buy_soul_result: [
    //     {
    //         id:1,
    //         srcFrontCard: Imglist.cardFront1,
    //         srcBackCard: Imglist.card_back,
    //         cardBack:Imglist.card_back
    //     },
    //     {
    //         id:2,
    //         srcFrontCard: Imglist.cardFront2,
    //         srcBackCard: Imglist.card_back,
    //         cardBack:Imglist.card_back
    //     },
    //     {
    //         id: 3,
    //         srcFrontCard: Imglist.cardFront3,
    //         srcBackCard: Imglist.card_back,
    //         cardBack:Imglist.card_back
    //     },
    //     {
    //         id: 4,
    //         srcFrontCard: Imglist.cardFront4,
    //         srcBackCard: Imglist.card_back,
    //         cardBack:Imglist.card_back
    //     },
    //     {
    //         id: 5,
    //         srcFrontCard: Imglist.cardFront5,
    //         srcBackCard: Imglist.card_back,
    //         cardBack:Imglist.card_back
    //     },
    //     {
    //         id: 6,
    //         srcFrontCard: Imglist.cardFront6,
    //         srcBackCard: Imglist.card_back,
    //         cardBack:Imglist.card_back
    //     },
    //     {
    //         id: 7,
    //         srcFrontCard: Imglist.cardFront7,
    //         srcBackCard: Imglist.card_back,
    //         cardBack:Imglist.card_back
    //     },
    //     {
    //         id: 8,
    //         srcFrontCard: Imglist.cardFront8,
    //         srcBackCard: Imglist.card_back,
    //         cardBack:Imglist.card_back
    //     },
    //     {
    //         id: 9,
    //         srcFrontCard: Imglist.cardFront9,
    //         srcBackCard: Imglist.card_back,
    //         cardBack:Imglist.card_back
    //     },
    //     {
    //         id: 10,
    //         srcFrontCard: Imglist.cardFront10,
    //         srcBackCard: Imglist.card_back,
    //         cardBack:Imglist.card_back
    //     },
    // ],

    //===soul values
    soulItems: [
        {
            id: 1,
            img: Imglist.card1,
            count: '500 ไดมอนด์',
            img_h: '157px',
            img_w: '216px'
        },
        {
            id: 2,
            img: Imglist.card5,
            count: '2,500 ไดมอนด์',
            img_h: '157px',
            img_w: '216px'
        },
        {
            id: 3,
            img: Imglist.card10,
            count: '5,000 ไดมอนด์',
            img_h: '157px',
            img_w: '216px'
        }
    ],
    buy_func:()=>console.log(""),
    //===history values
    historyList: [],

}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         :
            return ({...state, sessionKey: action.value});
        case 'ACCOUNT_AUTHED'       :
            return ({...state, userData: action.value});
        case 'RECEIVE_SESSION_KEY'  :
            return ({...state, sessionKey: action.value});
        case 'SET_LOGIN_URL'        :
            return ({...state, loginUrl: action.value});
        case 'SET_LOGOUT_URL'       :
            return ({...state, logoutUrl: action.value});
        case 'ACCOUNT_LOGIN'        :
            return ({...state, authState: "LOGGED_IN"});
        case 'ACCOUNT_LOGOUT'       :
            return ({...state, authState: "LOGGED_OUT"});
        case 'ACCOUNT_AUTH_CHECKED' :
            return ({...state, authed: true});
        case 'SET_JWT_TOKEN'        :
            return ({...state, jwtToken: action.value});

        case "SET_VALUE"            :
            return (Object.keys(defaultState).indexOf(action.key) >= 0) ? {
                ...state,
                [action.key]: action.value
            } : state;
        case "SET_VALUES"           :
            return {...state, ...action.value};
        default:
            return state;
    }
}

export const onAccountAuth = (sessionKey) => ({type: 'ACCOUNT_AUTH', value: sessionKey});
export const onAccountAuthed = (userData) => ({type: 'ACCOUNT_AUTHED', value: userData});
export const onAccountLogin = () => ({type: 'ACCOUNT_LOGIN', value: ""});
export const onAccountLogout = () => ({type: 'ACCOUNT_LOGOUT', value: ""});
export const receiveSessionKey = (sessionKey) => ({type: 'RECEIVE_SESSION_KEY', value: sessionKey});
export const setLoginUrl = (url) => ({type: 'SET_LOGIN_URL', value: url});
export const setLogoutUrl = (url) => ({type: 'SET_LOGOUT_URL', value: url});
export const onAccountAuthChecked = () => ({type: 'ACCOUNT_AUTH_CHECKE', value: true})
export const setJwtToken = (token) => ({type: 'SET_JWT_TOKEN', value: token});

export const setValue = (key, value) => ({type: "SET_VALUE", value, key});
export const setValues = (value) => ({type: "SET_VALUES", value});

