import React from 'react';

import F11Layout from './../features/F11Layout/';
import History from './../features/history';
export class HistoryPage extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={false}
                showSidebar={false}
            >
                <History/>
            </F11Layout>
        )
    }
}
