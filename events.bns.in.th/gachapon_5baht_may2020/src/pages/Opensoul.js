import React from 'react';
import F11Layout    from '../features/F11Layout';
import Opensoul     from '../features/opensoul';

export class OpensoulPage extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={false}
                showSidebar={false}
            >
                <Opensoul/>
            </F11Layout>
        )
    }
}
