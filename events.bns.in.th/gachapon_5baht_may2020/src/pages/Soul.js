import React from 'react';
import F11Layout    from '../features/F11Layout';
import Soul         from '../features/soul';

export class SoulPage extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={false}
                showSidebar={false}
            >
                <Soul/>
            </F11Layout>
        )
    }
}
