import React from 'react';
import './style.css';
import {Imglist} from './../../constants/Import_Images';
export default (props) => (
    <div onClick={props.onClick.bind(this)} className={"card "+(props.open? "filpopen": "filpclose")}>
    	<img className={"card__back"} src={props.srcBackCard || Imglist.card_back} />
    	<img className={"card__front"} src={props.srcFrontCard || Imglist.card_01} />
        <img className={"card__size"} src={props.srcBackCard || Imglist.card_back} />
    </div>
)
