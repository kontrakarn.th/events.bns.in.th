import React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
//import Logo from '../main/BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu             from '../menu/';
import ModalLoading     from '../modals/ModalLoading';
import ModalMessage     from '../modals/ModalMessage';
import ModalConfirmBuy  from '../modals/ModalConfirmBuy'
import {Imglist}        from './../../constants/Import_Images';
import ModalConfirm     from '../modals/ModalConfirm';

class Soul extends React.Component {

    apiEventInfo() {
        this.props.setValues({ modal_open: "loading" });
        //this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "event_info" };
        let successCallback = (data) => {
            if (data.status) {
                // this.props.setUsername(data.data.nickname);
                // this.props.setCanPlay(data.data.can_play);

                this.props.setValues({ 
                    username: data.data.nickname,
                    setCanPlay: data.data.can_play,
                    modal_open: "" ,
                    permission: true,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiConfirmOpenSoul() {
        const package_id = this.props.package_id;
        // let history = useHistory();

        this.props.setValues({
            modal_open: 'loading',
            setBuySoulResult:[],
        });
        apiPost(
            "REACT_APP_API_POST_BUY_CARD",
            this.props.jwtToken,
            {
                type: 'buy_card',
                package_id: package_id
            },
            (resp) => { // success response
                if (resp.status) {
                    this.props.setValues({ 
                        setCanPlay: resp.data.can_play,
                        setBuySoulResult: resp.content,
                        modal_open: ''
                    });
                    //this.props.setBuySoulResult(resp.content);
                    //this.props.setCanPlay(resp.data.can_play);
                    this.props.history.push(process.env.REACT_APP_EVENT_PATH + '/open-soul')      
                } else {
                    this.props.setValues({
                        modal_open: '',
                        modal_message: resp.message
                    });
                }
            },
            (data) => { // error response
                if (data.type == 'no_permission') {
                    this.props.setValues({
                        permission: false,
                        modal_open: "",
                    });
                }else {
                    this.props.setValues({
                        modal_open:"",
                        modal_message: data.message,
                    });
                }
        });
    }


    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: false,
            scrollDown: false,
            modalContent: {},
            
            showModalMessage: "",
            username: "",
            buy_soul_info: [],
            buy_soul_result : [],
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiEventInfo()
            this.props.setValues({buy_func:()=>this.apiConfirmOpenSoul()})
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiEventInfo()
            this.props.setValues({buy_func:()=>this.apiConfirmOpenSoul()})
        }
    }

    render() {
        return (
            <PageWrapper>
                <Menu page='soul'/>
                <SoulFrame>    
                    <div className="header">ซื้อการ์ด</div>
                    <div className="wrapper clearfix">
                        {this.props.soulItems.map((items, index) => {
                            return (
                                <div key={index} className="soul__items">
                                    <div className="soul__items--img">
                                        <img src={items.img}/>
                                        {this.props.setCanPlay["card_"+(index+1)] ?
                                            <a
                                                className="btn_default"
                                                onClick={()=>this.props.setValues({
                                                    package_id:items.id,
                                                    modal_open: 'confirmbuy'
                                                })}
                                                style={{
                                                    fontSize: "20px",
                                                    lineHeight: "47px",
                                                }}
                                            >
                                                {items.count}
                                            </a>

                                            :
                                            <div
                                                className="btn_default disabled" 
                                            >
                                                ไดมอนด์ไม่พอ
                                            </div>
                                        }

                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </SoulFrame>
                <ModalLoading />
                <ModalMessage />
        
                <ModalConfirm/>
                <ModalConfirmBuy
                    actConfirm_Buy_Soul={()=>this.apiConfirmOpenSoul()}
                    // actClose={() => this.setState({
                    //     showModalConfirmOpenSoul: false
                    // })}
                    //setMsg={this.props.package_id}
                />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(withRouter(Soul));

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_buy_soul']});
    width: 1105px;
    padding-top:172px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const SoulFrame = styled.div`
    background: top center no-repeat url(${Imglist['bg_content']});
    width: 940px;
    height: 525px;
    margin: 0 auto;
    box-sizing: border-box;
    padding: 30px 140px 0;
    color: #fff;
    img{
        max-width: 100%;
        max-height: 100%;
    }
    .header{
        font-family: 'Kanit-Medium', san-sarif;
        font-size: 20px;
        line-height: 1;
    }
    .wrapper{
        display: flex;
        align-items: center;
        justify-content: center;
        height: 95%;
    }
    .soul{
        &__items{
            flex: 0 0 20%;
            padding: 20px;
        } 
    }
    .btn_default{
        background: top center no-repeat url(${Imglist['btn_default']}); 
        width: 150px;
        height: 47px;
        line-height: 47px;
        color: #1e0d00;
        margin: 0 auto;
        font-family: 'DBWittayuX-Bold',san-sarif;
        font-size: 20px;
        cursor:pointer;
        display: inline-block;
        &:hover{
            background-position: bottom center;
        }
        &.disabled{
            pointer-events: none;
            -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
            filter: grayscale(100%);
        }
    
        // &.gray-btn{
        //     filter: grayscale(1);
        //     opacity: 0.7;
        // }
    }
`
