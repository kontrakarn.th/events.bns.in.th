import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
//import Logo from '../main/BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import {Imglist} from './../../constants/Import_Images';

class History extends React.Component {

    apiGetHistory(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'item_history'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open: "",
                    //...data.data,
                    historyList: data.content
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_ITEM_HISTORY", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage:'',
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiGetHistory(this.props);
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            if (!this.props.uid) {
                this.apiGetHistory(this.props);
            }
        }

    }

    render() {
        return (
            <PageWrapper>
                {/* <Logo/> */}
                <Menu page='history'/>
               
                <HistroyFrame>
                    <div className="header">ประวัติ</div>
                    <ul className="list">
                        {
                            this.props.historyList && this.props.historyList.length > 0 ?
                                this.props.historyList.map((items, index) => {
                                    return(
                                        <li>
                                            <div>{items.product_title}</div>
                                            <div>
                                                {items.log_time_string}
                                            </div>
                                        </li>
                                    )
                                })
                            :
                                null
                        }
                    </ul>
                    <Link to={`${process.env.REACT_APP_EVENT_PATH}/soul`} className="btn_default">ซื้อการ์ด</Link>
                </HistroyFrame>
                <ModalLoading />
                <ModalMessage />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(History);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_history']}) #050506;
    width: 1105px;
    padding-top:175px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const HistroyFrame = styled.div`
    background: top center no-repeat url(${Imglist['bg_content']});
    width: 940px;
    height: 525px;
    margin: 0 auto;
    box-sizing: border-box;
    padding: 33px 120px 0;
    color: #fff;
    .list{
        width: 700x;
        height: 340px;
        margin: 50px auto 20px;
        padding: 0;
        overflow: auto;
        overflow-x: hidden;
        color: #fff;
        >li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 0 2%;
            box-sizing: border-box;
            >div:first-child{
                width: 70%;
                text-align: left;
            }
        }
        &::-webkit-scrollbar {
            width: 8px;
        }
        &::-webkit-scrollbar-track {
          background: rgba(173,173,173,0.05);
        }
        &::-webkit-scrollbar-thumb {
            background: #463028;
            border-radius: 6px;
        }
    }
    .header{
        font-family: 'Kanit-Medium', san-sarif;
        font-size: 20px;
        line-height: 1;
    }
    .btn_default{
        background: top center no-repeat url(${Imglist['btn_default']}); 
        width: 150px;
        height: 47px;
        line-height: 47px;
        color: #1e0d00;
        margin: 0 auto;
        font-family: 'DBWittayuX-Bold',san-sarif;
        font-size: 20px;
        cursor:pointer;
        display: inline-block;
        &:hover{
            background-position: bottom center;
        }
    }
`
