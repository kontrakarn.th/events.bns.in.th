import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';
import {Imglist} from './../../constants/Import_Images';

const CPN = props => {
    let {modal_message,modal_item_token} = props;
    return (
        <ModalCore
            modalName="confirm"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <div className="contenttitle">ยืนยัน</div>
                <div className="contenttext" dangerouslySetInnerHTML={{__html: modal_message}} />

                <div>
                    <Btns onClick={()=>props.actConfirm(modal_item_token)}>
                        ตกลง
                    </Btns>
                    <Btns onClick={()=>props.setValues({modal_open:''})}>
                        ยกเลิก
                    </Btns>
                </div>
            </ModalMessageContent>
  
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 325px;
    height: 390px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist['modal_bg']});
    box-sizing: border-box;
    padding: 25px 10px;
    font-family: 'Kanit-Medium';
    .text--brown{
        color: #1c0908;
    }
    .contenttitle{
        font-size: 22px;
        color: #000;
    }
    .contenttext{
        font-size: 18px;
        height: 93%;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 190px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;
const Btns = styled.div`
    background: no-repeat top center url(${Imglist['btn_default']});
    width: 135px;
    height: 43px;
    display: inline-block;
    cursor: pointer;
    margin: 0 5px;
    color: #1e0d00;
    line-height: 42px;
    font-size: 22px;
    background-size: cover;
    font-family: 'DBWittayuX-Bold',san-sarif;
    &:hover{
        background-position: bottom center;
    }
`
