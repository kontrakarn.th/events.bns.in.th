import React from 'react';
import {connect} from 'react-redux';

import ModalMessage from './ModalMessage';
import ModalLoading from './ModalLoading';
import ModalSelectCharacter from './ModalSelectCharacter';
import ModalConfirmbuy from './ModalConfirmBuy'

const Modals = (props)=> (
    <>
        <ModalMessage />
        <ModalLoading />
        <ModalSelectCharacter />
        <ModalConfirmbuy/>
    </>
)

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = {  };

export default connect( mapStateToProps, mapDispatchToProps )(Modals);
