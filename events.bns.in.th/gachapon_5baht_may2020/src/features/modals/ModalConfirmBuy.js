import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';

import {Imglist} from '../../constants/Import_Images';

const CPN = props => {
    let {modal_message,modal_item_token} = props;
    console.log(props)
    return (
        <ModalCore
            modalName="confirmbuy"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <div className="contenttitle">แจ้งเตือน</div>
                <div className="contenttext">
                    {props.package_id === 1 &&
                        <div>
                            <span className="text--brown">
                                ต้องการที่จะซื้อการ์ด<br/>
                                จำนวน 1 ใบ<br/>
                            </span>
                            ในราคา 500 ไดมอนด์<br/>
                            ใช่หรือไม่
                        </div>
                    }
                    {props.package_id === 2 &&
                        <div>
                            <span className="text--brown">
                                ต้องการที่จะซื้อการ์ด<br/>
                                จำนวน 5 ใบ<br/>
                            </span>
                            ในราคา 2,500 ไดมอนด์<br/>
                            ใช่หรือไม่
                        </div>
                    }
                    {props.package_id === 3 &&
                        <div>
                            <span className="text--brown">
                                ต้องการที่จะซื้อการ์ด<br/>
                                จำนวน 10 ใบ<br/>
                            </span>
                            ในราคา 5,000 ไดมอนด์<br/>
                            ใช่หรือไม่
                        </div>
                    }
                </div>
                <div>
                    <Btns onClick={()=>props.buy_func()}>
                    {/* <Btns onClick={()=>console.log("test")}> */}
                        ตกลง
                    </Btns>
                    <Btns onClick={()=>props.setValues({modal_open:''})}>
                        ยกเลิก
                    </Btns>
                </div>
            </ModalMessageContent>
        </ModalCore>
        
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 325px;
    height: 390px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist['modal_bg']});
    box-sizing: border-box;
    padding: 25px 10px;
    font-family: 'Kanit-Medium';
    .text--brown{
        color: #1c0908;
    }
    .contenttitle{
        font-size: 22px;
        color: #000;
    }
    .contenttext{
        font-size: 18px;
        height: 93%;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 190px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;
const Btns = styled.div`
    background: no-repeat top center url(${Imglist['btn_default']});
    width: 135px;
    height: 43px;
    display: inline-block;
    cursor: pointer;
    margin: 0 5px;
    color: #1e0d00;
    line-height: 42px;
    font-size: 22px;
    background-size: cover;
    font-family: 'DBWittayuX-Bold',san-sarif;
    &:hover{
        background-position: bottom center;
    }
`
