import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
//import Logo from '../main/BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import {Imglist} from './../../constants/Import_Images';
import FlipCard from '../FlipCard'

const cardFrontImage=[
    Imglist.cardFront1,
    Imglist.cardFront2,
    Imglist.cardFront3,
    Imglist.cardFront4,
    Imglist.cardFront5,
    Imglist.cardFront6,
    Imglist.cardFront7,
    Imglist.cardFront8,
    Imglist.cardFront9,
    Imglist.cardFront10,
    Imglist.cardFront11,
    Imglist.cardFront12,
    Imglist.cardFront13,
    Imglist.cardFront14,
    Imglist.cardFront15,
    Imglist.cardFront16,
]

class Opensoul extends React.Component {

   
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: false,
            modalContent: {},
            package_id:0,
            showModalMessage: "",
            username: "",
            openCard: [false,false,false,false,false,false,false,false,false,false],
        }
    }

    actOpenCard(n){
        let {openCard} = this.state;
        openCard[n] = true;
        this.setState({openCard});
    }
    actOpenCardAll(){
        this.setState({openCard:[true,true,true,true,true,true,true,true,true,true]});
    }
  

    render() {
        return (
            <PageWrapper>
                <Menu page='opensoul'/>
                <OpenSoulFrame>    
                    <div className="header">เปิดการ์ด</div>
                    <div className="wrapper clearfix">
                        <div className="buy_soul_wrapper">
                        {
                            this.props.setBuySoulResult.map((items, index) => {
                                let onlyOne = (this.props.setBuySoulResult.length === 1) ? true: false;
                            
                                return(
                                    <div className="items__box" key={index} style={{
                                        //display: onlyOne? "block": "inline-block"
                                    }}>
                                        <FlipCard
                                            open={this.state.openCard[index]}
                                            onClick={()=>this.actOpenCard(index)}
                                            srcBackCard={items.cardBack}
                                            srcFrontCard={cardFrontImage[items.id-1]}
                                        />
                                    </div>
                                )
                            })
                        }
                        </div>
                        <div className="wrapper__btn">
                            <Link to={`${process.env.REACT_APP_EVENT_PATH}/soul`} className="btn btn_default">ซื้อการ์ด</Link>
                            <div className="btn_default" onClick={()=>this.actOpenCardAll()}>เปิดทั้งหมด</div>
                        </div>
                    </div>
                </OpenSoulFrame>
                <ModalLoading />
                <ModalMessage />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Opensoul);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_history']});
    width: 1105px;
    padding-top:172px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const OpenSoulFrame = styled.div`
    background: top center no-repeat url(${Imglist['bg_content']});
    width: 940px;
    height: 525px;
    margin: 0 auto;
    box-sizing: border-box;
    padding: 30px 140px 0;
    color: #fff;
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    img{
        max-width: 100%;
        max-height: 100%;
    }
    .header{
        font-family: 'Kanit-Medium', san-sarif;
        font-size: 20px;
        line-height: 1;
    }
    .wrapper{
      margin-top: 40px;
      height: 85%;
    }
    .buy_soul_wrapper{
        height: 87%;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;
    }
    .items__box{
        width: 20%
    }
    .soul{
        &__items{
            flex: 0 0 20%;
            padding: 20px;
        }
    }
    .btn_default{
        background: top center no-repeat url(${Imglist['btn_default']}); 
        width: 150px;
        height: 47px;
        line-height: 47px;
        color: #1e0d00;
        margin: 0 auto;
        font-family: 'DBWittayuX-Bold',san-sarif;
        font-size: 20px;
        cursor:pointer;
        display:inline-block;
        margin: 5px 50px;
        &:hover{
            background-position: bottom center;
        }
        &.disabled{
            pointer-events: none;
            -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
            filter: grayscale(100%);
        }
    
        // &.gray-btn{
        //     filter: grayscale(1);
        //     opacity: 0.7;
        // }
    }
`
