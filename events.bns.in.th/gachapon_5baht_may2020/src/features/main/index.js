import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import {Imglist} from './../../constants/Import_Images';
import ModalConfirmBuy from '../modals/ModalConfirmBuy';
import ModalSelectCharacter from "../modals/ModalSelectCharacter";

import Menu from '../menu';


class Main extends React.Component {

    actConfirm(token){
        if (token === undefined || token === ""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไม่สามารถแลกของไอเทมได้",
            });
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'redeem',token:token};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                    coin:data.data.coin,
                    itemLists:data.data.itemLists,
                });
                this.setState({
                    itemLists : data.data.itemLists,
                    questLists : data.data.questLists,
                })
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);

    }

    // apiSelectCharacter(id){
    //     if (id === undefined || id === ""){
    //         return;
    //     }
    //     this.props.setValues({ modal_open: "loading" });
    //     let dataSend = {type:'select_character',id:id};
    //     let successCallback = (data) => {
    //         if (data.status) {
    //             this.props.setValues({
    //                 username: data.data.username,
    //                 character_name: data.data.character_name,
    //                 selectd_char: data.data.selectd_char,
    //                 characters: data.data.characters,
    //                 coin:data.data.coin,
    //                 free:data.data.free,
    //                 itemLists:data.data.itemLists,
    //                 mission_score:data.data.mission_score,
    //                 modal_open: "",
    //             });
    //             /* this.setState({
    //                 itemLists : data.data.rewards,
    //                 questLists : data.data.quests,
    //             }) */
    //         }else{
    //             this.props.setValues({
    //                 modal_open:"message",
    //                 modal_message: data.message,
    //             });
    //         }
    //     };
    //     const failCallback = (data) => {
    //         this.props.setValues({
    //             modal_open:"message",
    //             modal_message: data.message,
    //         });
    //     };
    //     apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    // }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true){
                // this.props.setUsername(data.data.nickname);
                // this.props.setCanPlay(data.data.can_play);
                this.props.setValues({
                    username: data.data.nickname,
                    setCanPlay: data.data.can_play,
                    modal_open: "",
                });
                // if (data.data.selected_char === false){
                //     this.props.setValues({ modal_open: "selectcharacter" });
                // }
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }

    render() {
        return (
            <PageWrapper>
                {/* <Menu page="home"/> */}
                <Content>         
                    <div className="wrapper">
                        <Link to={`${process.env.REACT_APP_EVENT_PATH}/soul`}className="btn btn-promotion">เข้าร่วมโปรโมชั่น</Link>
                    </div>
                </Content>
                <ModalLoading />
                <ModalMessage />
                {/* <ModalConfirm actConfirm={this.actConfirm.bind(this)}/> */}
                {/* <ModalConfirmReceive actConfirm={this.actConfirm.bind(this)}/> */}
                {/* <ModalSelectCharacter actSelectCharacter={this.apiSelectCharacter.bind(this)}/> */}
    
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    width: 1105px;
    height: 700px;
    // padding-top: 740px;
    text-align: center;
    position: relative;
`

const Content = styled.div ` 
    width: 100%;
    height: 100%; 
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }

    .wrapper {
        background: top center no-repeat url(${Imglist['bg_main']}) #11122d;
        position: relative;
        display: block;
        width: 100%;
        height: 100%;
        background-color: #171312;

        & .btn-promotion{
            background: url(${Imglist['btn_promotion']}) no-repeat 0 0;
            padding: 60px 25px;
            color: #fff;
            border-radius: 50px;
            font-size: 24px;
            position: absolute;
            bottom: -25px;
            right: 150px;
            text-decoration: none;
            z-index: 99;
            text-align: center;
            min-width: 300px;
            cursor:pointer;
            &:hover{
                background-position: 0 -138px;
            }
        }
    }

`

