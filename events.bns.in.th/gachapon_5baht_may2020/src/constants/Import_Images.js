import icon_menu			from '../static/images/icon_menu.png'

import bg_main				from '../static/images/bg_main.jpg'
import btn_promotion		from '../static/images/btn_promotion.png'
import menu_bg				from '../static/images/menu_bg.png'
import bg_history			from '../static/images/bg_history.jpg'
import bg_content			from '../static/images/bg_content.png'
import bg_buy_soul			from '../static/images/bg_buy_soul.jpg'
import btn_default			from '../static/images/btn_default.png'
import card1				from '../static/images/card1.png'
import card5				from '../static/images/card5.png'
import card10				from '../static/images/card10.png'

import cardFront1			from '../static/images/card/1.png'
import cardFront2			from '../static/images/card/2.png'
import cardFront3			from '../static/images/card/3.png'
import cardFront4			from '../static/images/card/4.png'
import cardFront5			from '../static/images/card/5.png'
import cardFront6			from '../static/images/card/6.png'
import cardFront7			from '../static/images/card/7.png'
import cardFront8			from '../static/images/card/8.png'
import cardFront9			from '../static/images/card/9.png'
import cardFront10			from '../static/images/card/10.png'
import cardFront11			from '../static/images/card/11.png'
import cardFront12			from '../static/images/card/12.png'
import cardFront13			from '../static/images/card/13.png'
import cardFront14			from '../static/images/card/14.png'
import cardFront15			from '../static/images/card/15.png'
import cardFront16			from '../static/images/card/16.png'
import card_back			from '../static/images/card/card_back.png'
import card_01				from '../static/images/card/card_01.png'
import modal_bg				from '../static/images/modal_bg.png'



//https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/

export const Imglist = {
	icon_menu,

	bg_main,
	btn_promotion,
	menu_bg,
	bg_history,
	bg_content,
	bg_buy_soul,
	btn_default,
	card1,
	card5,
	card10,
	cardFront1,	
	cardFront2,	
	cardFront3,	
	cardFront4,	
	cardFront5,	
	cardFront6,	
	cardFront7,	
	cardFront8,	
	cardFront9,	
	cardFront10,
	cardFront11,
	cardFront12,
	cardFront13,
	cardFront14,
	cardFront15,
	cardFront16,
	card_back,
	card_01,
	modal_bg,



}
