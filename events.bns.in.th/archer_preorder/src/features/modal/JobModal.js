import React from 'react';
import  ModalCore from './ModalCore.js';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';

const JobModal = (props) => {
	return (
		<ModalCore name="job" outSideClick={false}>
			<div className="contentwrap contentwrap--boardjob">
				<div className="inner inner--board">
					<div className="text text--head">
						กรุณาเลือกอาชีพ
					</div>
                    <select className="option weapon-class" onChange={props.changeSelectedWeapon} value={props.selectedWeapon}>
                        <option value="-1"> -- อาวุธ --</option>
                        <option value="12">อาร์เชอร์</option>
                        <option value="1">เบลดมาสเตอร์</option>
                        <option value="2">กังฟูมาสเตอร์</option>
                        <option value="3">ฟอร์ซมาสเตอร์</option>
                        <option value="4">โซลกันเนอร์</option>
                        <option value="5">เดสทรอยเยอร์</option>
                        <option value="6">ซัมมอนเนอร์</option>
                        <option value="7">แอสซาซิน</option>
                        <option value="8">เบลดแดนเซอร์</option>
                        <option value="9">วอร์ลอค</option>
                        <option value="10">โซลไฟต์เตอร์</option>
                        <option value="11">วอร์เดน</option>
                    </select>
                    <div className="btn__ok" onClick={()=>{if(props.actJob) props.actJob()}}></div>
                    <div className="btn__no" onClick={() => props.setValues({ modal_open: '' })}></div>
				</div>
			</div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(JobModal);
