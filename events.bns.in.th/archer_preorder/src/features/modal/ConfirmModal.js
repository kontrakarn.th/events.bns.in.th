import React from "react";
import { connect } from "react-redux";
import ModalCore from "./ModalCore.js";
import './styles.scss';
import { setValues, actCloseModal } from "./redux";

const ConfirmModal = (props) => {
  let {modal_message} = props;
	return (
    <ModalCore name="confirm" outSideClick={false}>
      	<div className="contentwrap contentwrap--board">
		  	<div className="inner inner--boardmaxwidth">
				<div className="text text--head">
					คุณแน่ใจว่าจะซื้อ
				</div>
				<div className="text text--detail" dangerouslySetInnerHTML={{ __html: props.modal_message }}>
				</div>
				<div className="btn__ok" onClick={()=>{if(props.actConfirm) props.actConfirm()}}></div>
				<div className="btn__no" onClick={() => props.setValues({ modal_open: '' })}></div>
			</div>
		</div>
    </ModalCore>
	)
}

const mapStateToProps = state => ({
  ...state.ModalReducer,
  ...state.Modal
});

const mapDispatchToProps = { setValues, actCloseModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmModal);
