import React from 'react';
import  ModalCore from './ModalCore.js';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';

const PurchasedInfoModal = (props) => {
	return (
		<ModalCore name="purchase_info" outSideClick={false}>
			<div className="contentwrap contentwrap--boardjob">
				<div className="inner inner--board">
					<div className="text text--purchase-h">
						รายละเอียดแพ็คเกจที่สั่งซื้อ
					</div>
                    <div className="text text--purchase-info" dangerouslySetInnerHTML={{ __html: props.msg }}>
					</div>
                    <div className="btn__ok" onClick={() => props.setValues({ modal_open: '' })}></div>
				</div>
			</div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PurchasedInfoModal);
