import React        from 'react';
import background   from './../static/images/permission.jpg';

export class Permission extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            open: true,
            wait: true
        }
        this.intervalCheck = setInterval(()=>{
            if(this.state.open === false){
                this.setState({wait: false})
                clearInterval(this.intervalCheck);
            } else {
            }
        }, 2000);
    }
    componentWillMount(){
        this.setState({open: this.props.open});
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.open !== prevProps.open){
            this.setState({open: this.props.open});
        }
    }
    render() {
        return (
            <div className={"page "+(this.state.wait? "":"close")}>
                <img className="loading__background" src={background} />
            </div>
        )
    }
}
