import React from 'react';
import './index.css';
import img_dice from './images/img_dice.png';

const CPN = (props)=>(
    <div className="dice">
        <div className="dice__jump">
            <div className="dice__rotate">
                <div className={"dice__image dice__image--"+props.number}  style={{backgroundImage:"url("+img_dice+")"}}/>
            </div>
        </div>
    </div>


)
export default CPN;
