import React from 'react';
import Modal from './Modal';

export default class extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                title=""
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div>
                    ท่านต้องการยกเลินกระดานนี้หรือไม่
                </div>
            </Modal>
        )
  }
}
