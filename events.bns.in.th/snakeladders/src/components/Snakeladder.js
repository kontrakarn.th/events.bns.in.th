import React, { Component } from 'react'
import Dice from './Dice';
import Modal from './Modal';

import img_snakeladder from './../static/images/img_snakeladder.png';
import charactor_snakeladder from './../static/images/charactor_snakeladder.png';

export default class extends Component {
    actRollDice(){
        if(this.state.animation === false){
            this.setState({confirmPlayDice: true})
        }
    }
    actDiamondRollDice(){
        if(this.state.animation === false){
            this.setState({confirmPlayDiamondDice: true})
        }
    }
    actRejectBoard(){
        if(this.state.animation === false){
            if(this.props.actRejectBoard) this.props.actRejectBoard();
        }
    }

    actConfirmRolldice(){
        if(this.state.animation === false){
            this.setState({confirmPlayDice: false})
            if(this.props.actPlayFreeDice) this.props.actPlayFreeDice();
        }
    }
    actConfirmDiamondRolldice(){
        if(this.state.animation === false){
            this.setState({confirmPlayDiamondDice: false})
            if(this.props.actPlayDiamondDice) this.props.actPlayDiamondDice();
        }
    }
    fncSetStep(current,next){
        if( next > current ) {
            setTimeout( ()=>this.fncNextStep(current+1,next),1000);
            this.setState({
                current_step: current,
                next_step: next,
                animation: true
            })
        } else {
            this.setState({
                current_step: current,
                next_step: next,
                animation: false
            })
        }
    }
    fncNextStep(current,next){
        if( next >= current ) {
            this.setState({
                current_step: current,
                animation: true
            })
            setTimeout( ()=>this.fncNextStep(current+1,next),1000);
        } else {
            this.setState({
                animation: false
            })
        }
    }
    funcGenCharacterMove(){

        let { animation, current_step } = this.state;
        if(animation) {
             return {
                 ...charPosition[current_step],
                 transition:" top 1s linear, left 1s linear"
             }
        } else {
             return {
                 ...charPosition[current_step]
             }
        }
    }
    //==============================================================================
    //api
    //==============================================================================
    constructor(props){
        super(props);
        this.state = {
            confirmPlayDice: false,
            confirmPlayDiamondDice: false,
            playDice: false,
            free_rights: true,
            free_rights_used: false,
            animation: false,
            current_step: 0,
            next_step: 0,
            showDetail: -1,
        }
    }
    componentDidMount(){
        let { current_step, next_step } = this.props;
        this.fncSetStep(current_step, next_step);
    }
    componentDidUpdate(prevProps, prevState){
        let { current_step, next_step } = this.props;
        if(current_step !== prevProps.current_step || next_step !== prevProps.next_step){
            this.fncSetStep(current_step, next_step);
        }
    }
    render(){
        let board_key = this.props.board_key || 0;
        let solt_key = Math.max(0,this.state.showDetail);
        let popupDetail = this.props.items[solt_key].img;
        if(this.props.items[solt_key]["img"+board_key])
            popupDetail = this.props.items[solt_key]["img"+board_key];
        return (
            <div className="snakeladder">
                <img src={img_snakeladder} />
                <div className="snakeladder__charactor" style={this.funcGenCharacterMove()}>
                    <img
                        className={"charactor"+(this.state.animation ? " animation":"")}
                        src={charactor_snakeladder}
                    />
                </div>
                <div className="snakeladder__rewardboxs" >
                    {this.props.items && this.props.items.map((item, index) => { return (
                        <div
                            key={index}
                            className={"snakeladder__rewardbox snakeladder__rewardbox--" + (index+1)}
                            onMouseOver={()=>this.setState({showDetail:index})}
                            onMouseOut={()=>this.setState({showDetail:-1})}
                        />
                    )})}
                    {this.state.showDetail >= 0 &&
                        <div className={"snakeladder__rewardbox snakeladder__rewardbox--detail snakeladder__rewardbox--" + (this.state.showDetail+1)} >
                            {/*<img src={this.props.items[this.state.showDetail].img} />*/}
                            <img src={popupDetail} />

                        </div>
                    }
                </div>

                <div className={"snakeladder__wrapbtnrolldice" + (this.props.lockbtn ? "--fixed":"")}>
                    {
                        this.props.btnGroup ?
                            <div>
                                <a className="snakeladder__btnrolldice snakeladder__btnrolldice--diamond" onClick={this.actDiamondRollDice.bind(this)} />
                                <a className="snakeladder__btnrolldice snakeladder__btnrolldice--cancel" onClick={this.actRejectBoard.bind(this)} />
                            </div>
                        :
                            <a className="snakeladder__btnrolldice" onClick={this.actRollDice.bind(this)} />
                    }
                </div>
                <Modal
                    open={this.state.confirmPlayDice}
                    confirmMode={true}
                    actConfirm={this.actConfirmRolldice.bind(this)}
                    actClose={() => this.setState({ confirmPlayDice: false})}
                >
                    ต้องการทอยเต๋าหรือไม่
                </Modal>
                <Modal
                    open={this.state.confirmPlayDiamondDice}
                    confirmMode={true}
                    actConfirm={this.actConfirmDiamondRolldice.bind(this)}
                    actClose={() => this.setState({ confirmPlayDiamondDice: false})}
                >
                    ต้องการทอยเต๋าหรือไม่<br/>
                    ( 5000 ไดมอนด์ )
                </Modal>

            </div>
        )
    }
}

const charPosition = [

    {top: "10px",   left: "30px"},//0

    {top: "10px",   left: "167px"},//1
    {top: "10px",   left: "286px"},//2
    {top: "10px",   left: "407px"},//3
    {top: "10px",   left: "528px"},//4
    {top: "10px",   left: "649px"},//5

    {top: "155px",  left: "649px"},//6
    {top: "295px",  left: "649px"},//7
    {top: "435px",  left: "649px"},//8
    {top: "575px",  left: "649px"},//9
    {top: "720px",  left: "649px"},//10
    {top: "860px",  left: "649px"},//11
    {top: "1000px", left: "649px"},//12
    {top: "1140px", left: "649px"},//13
    {top: "1280px", left: "649px"},//14
    {top: "1420px", left: "649px"},//15
    {top: "1560px", left: "649px"},//16
    {top: "1700px", left: "649px"},//17
    {top: "1840px", left: "649px"},//18
    {top: "1985px", left: "649px"},//19
    {top: "2125px", left: "649px"},//20
    {top: "2265px", left: "649px"},//21

    {top: "2265px", left: "528px"},//22
    {top: "2125px", left: "528px"},//23
    {top: "1985px", left: "528px"},//24
    {top: "1840px", left: "528px"},//25
    {top: "1700px", left: "528px"},//26
    {top: "1560px", left: "528px"},//27
    {top: "1420px", left: "528px"},//28
    {top: "1280px", left: "528px"},//29
    {top: "1140px", left: "528px"},//30
    {top: "1000px", left: "528px"},//31
    {top: "860px",  left: "528px"},//32
    {top: "720px",  left: "528px"},//33
    {top: "575px",  left: "528px"},//34
    {top: "435px",  left: "528px"},//35
    {top: "295px",  left: "528px"},//36
    {top: "155px",  left: "528px"},//37

    {top: "155px",  left: "407px"},//38
    {top: "295px",  left: "407px"},//39
    {top: "435px",  left: "407px"},//40
    {top: "575px",  left: "407px"},//41
    {top: "720px",  left: "407px"},//42
    {top: "860px",  left: "407px"},//43
    {top: "1000px", left: "407px"},//44
    {top: "1140px", left: "407px"},//45
    {top: "1280px", left: "407px"},//46
    {top: "1420px", left: "407px"},//47
    {top: "1560px", left: "407px"},//48
    {top: "1700px", left: "407px"},//49
    {top: "1840px", left: "407px"},//50
    {top: "1985px", left: "407px"},//51
    {top: "2125px", left: "407px"},//52
    {top: "2265px", left: "407px"},//53

    {top: "2265px", left: "286px"},//54
    {top: "2125px", left: "286px"},//55
    {top: "1985px", left: "286px"},//56
    {top: "1840px", left: "286px"},//57
    {top: "1700px", left: "286px"},//58
    {top: "1560px", left: "286px"},//59
    {top: "1420px", left: "286px"},//60
    {top: "1280px", left: "286px"},//61
    {top: "1140px", left: "286px"},//62
    {top: "1000px", left: "286px"},//63
    {top: "860px",  left: "286px"},//64
    {top: "720px",  left: "286px"},//65
    {top: "575px",  left: "286px"},//66
    {top: "435px",  left: "286px"},//67
    {top: "295px",  left: "286px"},//68
    {top: "155px",  left: "286px"},//69

    {top: "155px",  left: "167px"},//70
    {top: "295px",  left: "167px"},//71
    {top: "435px",  left: "167px"},//72
    {top: "575px",  left: "167px"},//73
    {top: "720px",  left: "167px"},//74
    {top: "860px",  left: "167px"},//75
    {top: "1000px", left: "167px"},//76
    {top: "1140px", left: "167px"},//77
    {top: "1280px", left: "167px"},//78
    {top: "1420px", left: "167px"},//79
    {top: "1560px", left: "167px"},//80
    {top: "1700px", left: "167px"},//81
    {top: "1840px", left: "167px"},//82
    {top: "1985px", left: "167px"},//83
    {top: "2125px", left: "167px"},//84
    {top: "2265px", left: "167px"},//85

    {top: "2265px", left: "50px"},//86
    {top: "2125px", left: "50px"},//87
    {top: "1985px", left: "50px"},//88
    {top: "1840px", left: "50px"},//89
    {top: "1700px", left: "50px"},//90
    {top: "1560px", left: "50px"},//91
    {top: "1420px", left: "50px"},//92
    {top: "1280px", left: "50px"},//93
    {top: "1140px", left: "50px"},//94
    {top: "1000px", left: "50px"},//95
    {top: "860px",  left: "50px"},//96
    {top: "720px",  left: "50px"},//97
    {top: "575px",  left: "50px"},//98
    {top: "435px",  left: "50px"},//99
    {top: "295px",  left: "50px"},//100

]
