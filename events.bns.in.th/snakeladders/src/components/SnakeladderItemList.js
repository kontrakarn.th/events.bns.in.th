import popupFree1       from './../static/images/items/free1.png';
import popupFree10       from './../static/images/items/free10.png';
import popupFree11       from './../static/images/items/free11.png';
import popupFree20       from './../static/images/items/free20.png';
import popupFree21       from './../static/images/items/free21.png';
import popupFree30       from './../static/images/items/free30.png';
import popupFree31       from './../static/images/items/free31.png';
import popupFree40       from './../static/images/items/free40.png';
import popupFree41       from './../static/images/items/free41.png';
import popupFree50       from './../static/images/items/free50.png';
import popupFree51       from './../static/images/items/free51.png';
import popupFree60       from './../static/images/items/free60.png';
import popupFree61       from './../static/images/items/free61.png';
import popupFree70       from './../static/images/items/free70.png';
import popupFree71       from './../static/images/items/free71.png';
import popupFree80       from './../static/images/items/free80.png';
import popupFree81       from './../static/images/items/free81.png';
import popupFree90       from './../static/images/items/free90.png';
import popupFree91       from './../static/images/items/free91.png';
import popupFree100       from './../static/images/items/free100.png';

import popupPaid1       from './../static/images/items/paid1.png';
import popupPaid10      from './../static/images/items/paid10.png';
import popupPaid11      from './../static/images/items/paid11.png';
import popupPaid20      from './../static/images/items/paid20.png';
import popupPaid21      from './../static/images/items/paid21.png';
import popupPaid30      from './../static/images/items/paid30.png';
import popupPaid31      from './../static/images/items/paid31.png';
import popupPaid40      from './../static/images/items/paid40.png';
import popupPaid41      from './../static/images/items/paid41.png';
import popupPaid50      from './../static/images/items/paid50.png';
import popupPaid51      from './../static/images/items/paid51.png';
import popupPaid60_6    from './../static/images/items/paid60_6.png';
import popupPaid60_7    from './../static/images/items/paid60_7.png';
import popupPaid60_8    from './../static/images/items/paid60_8.png';
import popupPaid60_9    from './../static/images/items/paid60_9.png';
import popupPaid60      from './../static/images/items/paid60.png';
import popupPaid61      from './../static/images/items/paid61.png';
import popupPaid70_6    from './../static/images/items/paid70_6.png';
import popupPaid70_7    from './../static/images/items/paid70_7.png';
import popupPaid70_8    from './../static/images/items/paid70_8.png';
import popupPaid70_9    from './../static/images/items/paid70_9.png';
import popupPaid70      from './../static/images/items/paid70.png';
import popupPaid71      from './../static/images/items/paid71.png';
import popupPaid80_6    from './../static/images/items/paid80_6.png';
import popupPaid80_7    from './../static/images/items/paid80_7.png';
import popupPaid80_8    from './../static/images/items/paid80_8.png';
import popupPaid80_9    from './../static/images/items/paid80_9.png';
import popupPaid80      from './../static/images/items/paid80.png';
import popupPaid81      from './../static/images/items/paid81.png';
import popupPaid90_1    from './../static/images/items/paid90_1.png';
import popupPaid90_2    from './../static/images/items/paid90_2.png';
import popupPaid90_3    from './../static/images/items/paid90_3.png';
import popupPaid90_4    from './../static/images/items/paid90_4.png';
import popupPaid90_5    from './../static/images/items/paid90_5.png';
import popupPaid90_6    from './../static/images/items/paid90_6.png';
import popupPaid90_7    from './../static/images/items/paid90_7.png';
import popupPaid90_8    from './../static/images/items/paid90_8.png';
import popupPaid90_9    from './../static/images/items/paid90_9.png';
import popupPaid91      from './../static/images/items/paid91.png';
import popupPaid100_1   from './../static/images/items/paid100_1.png';
import popupPaid100_2   from './../static/images/items/paid100_2.png';
import popupPaid100_3   from './../static/images/items/paid100_3.png';
import popupPaid100_4   from './../static/images/items/paid100_4.png';
import popupPaid100_5   from './../static/images/items/paid100_5.png';
import popupPaid100_6   from './../static/images/items/paid100_6.png';
import popupPaid100_7   from './../static/images/items/paid100_7.png';
import popupPaid100_8   from './../static/images/items/paid100_8.png';
import popupPaid100_9   from './../static/images/items/paid100_9.png';

export const snakeladderPaid = [
    { img: popupPaid1},//1
    { img: popupPaid1},//2
    { img: popupPaid1},//3
    { img: popupPaid1},//4
    { img: popupPaid1},//5
    { img: popupPaid1},//6
    { img: popupPaid1},//7
    { img: popupPaid1},//8
    { img: popupPaid1},//9
    { img: popupPaid10},//10
    { img: popupPaid11},//11
    { img: popupPaid11},//12
    { img: popupPaid11},//13
    { img: popupPaid11},//14
    { img: popupPaid11},//15
    { img: popupPaid11},//16
    { img: popupPaid11},//17
    { img: popupPaid11},//18
    { img: popupPaid11},//19
    { img: popupPaid20},//20
    { img: popupPaid21},//21
    { img: popupPaid21},//22
    { img: popupPaid21},//23
    { img: popupPaid21},//24
    { img: popupPaid21},//25
    { img: popupPaid21},//26
    { img: popupPaid21},//27
    { img: popupPaid21},//28
    { img: popupPaid21},//29
    { img: popupPaid30},//30
    { img: popupPaid31},//31
    { img: popupPaid31},//32
    { img: popupPaid31},//33
    { img: popupPaid31},//34
    { img: popupPaid31},//35
    { img: popupPaid31},//36
    { img: popupPaid31},//37
    { img: popupPaid31},//38
    { img: popupPaid31},//39
    { img: popupPaid40},//40
    { img: popupPaid41},//41
    { img: popupPaid41},//42
    { img: popupPaid41},//43
    { img: popupPaid41},//44
    { img: popupPaid41},//45
    { img: popupPaid41},//46
    { img: popupPaid41},//47
    { img: popupPaid41},//48
    { img: popupPaid41},//49
    { img: popupPaid50},//50
    { img: popupPaid51},//51
    { img: popupPaid51},//52
    { img: popupPaid51},//53
    { img: popupPaid51},//54
    { img: popupPaid51},//55
    { img: popupPaid51},//56
    { img: popupPaid51},//57
    { img: popupPaid51},//58
    { img: popupPaid51},//59
    { img: popupPaid60, img6: popupPaid60_6, img7: popupPaid60_7, img8: popupPaid60_8, img9: popupPaid60_9},//60
    { img: popupPaid61},//61
    { img: popupPaid61},//62
    { img: popupPaid61},//63
    { img: popupPaid61},//64
    { img: popupPaid61},//65
    { img: popupPaid61},//66
    { img: popupPaid61},//67
    { img: popupPaid61},//68
    { img: popupPaid61},//69
    { img: popupPaid70, img6: popupPaid70_6, img7: popupPaid70_7, img8: popupPaid70_8, img9: popupPaid70_9},//70
    { img: popupPaid71},//71
    { img: popupPaid71},//72
    { img: popupPaid71},//73
    { img: popupPaid71},//74
    { img: popupPaid71},//75
    { img: popupPaid71},//76
    { img: popupPaid71},//77
    { img: popupPaid71},//78
    { img: popupPaid71},//79
    { img: popupPaid80, img6: popupPaid80_6, img7: popupPaid80_7, img8: popupPaid80_8, img9: popupPaid80_9},//80
    { img: popupPaid81},//81
    { img: popupPaid81},//82
    { img: popupPaid81},//83
    { img: popupPaid81},//84
    { img: popupPaid81},//85
    { img: popupPaid81},//86
    { img: popupPaid81},//87
    { img: popupPaid81},//88
    { img: popupPaid81},//89
    { img: popupPaid90_1, img2: popupPaid90_2, img3: popupPaid90_3, img4: popupPaid90_4, img5: popupPaid90_5, img6: popupPaid90_6, img7: popupPaid90_7, img8: popupPaid90_8, img9: popupPaid90_9},//90
    { img: popupPaid91},//91
    { img: popupPaid91},//92
    { img: popupPaid91},//93
    { img: popupPaid91},//94
    { img: popupPaid91},//95
    { img: popupPaid91},//96
    { img: popupPaid91},//97
    { img: popupPaid91},//98
    { img: popupPaid91},//99
    { img: popupPaid100_1, img2: popupPaid100_2, img3: popupPaid100_3, img4: popupPaid100_4, img5: popupPaid100_5, img6: popupPaid100_6, img7: popupPaid100_7, img8: popupPaid100_8, img9: popupPaid100_9},//100
]

export const snakeladder = [
    { img: popupFree1,title:"คริสตัลหินโซล",    amount:50 },//1
    { img: popupFree1,title:"คริสตัลหินโซล",    amount:50 },//2
    { img: popupFree1,title:"คริสตัลหินโซล",    amount:50 },//3
    { img: popupFree1,title:"คริสตัลหินโซล",    amount:50 },//4
    { img: popupFree1,title:"คริสตัลหินโซล",    amount:50 },//5
    { img: popupFree1,title:"คริสตัลหินโซล",    amount:50 },//6
    { img: popupFree1,title:"คริสตัลหินโซล",    amount:50 },//7
    { img: popupFree1,title:"คริสตัลหินโซล",    amount:50 },//8
    { img: popupFree1,title:"คริสตัลหินโซล",    amount:50 },//9
    { img: popupFree10,title:"หินโซล",           amount:50 },//10
    { img: popupFree11,title:"คริสตัลหินจันทรา",  amount:15 },//11
    { img: popupFree11,title:"คริสตัลหินจันทรา",  amount:15 },//12
    { img: popupFree11,title:"คริสตัลหินจันทรา",  amount:15 },//13
    { img: popupFree11,title:"คริสตัลหินจันทรา",  amount:15 },//14
    { img: popupFree11,title:"คริสตัลหินจันทรา",  amount:15 },//15
    { img: popupFree11,title:"คริสตัลหินจันทรา",  amount:15 },//16
    { img: popupFree11,title:"คริสตัลหินจันทรา",  amount:15 },//17
    { img: popupFree11,title:"คริสตัลหินจันทรา",  amount:15 },//18
    { img: popupFree11,title:"คริสตัลหินจันทรา",  amount:15 },//19
    { img: popupFree20,title:"หินจันทรา",         amount:50 },//20
    { img: popupFree21,title:"สัญลักษณ์ฮงมุน",    amount:15 },//21
    { img: popupFree21,title:"สัญลักษณ์ฮงมุน",    amount:15 },//22
    { img: popupFree21,title:"สัญลักษณ์ฮงมุน",    amount:15 },//23
    { img: popupFree21,title:"สัญลักษณ์ฮงมุน",    amount:15 },//24
    { img: popupFree21,title:"สัญลักษณ์ฮงมุน",    amount:15 },//25
    { img: popupFree21,title:"สัญลักษณ์ฮงมุน",    amount:15 },//26
    { img: popupFree21,title:"สัญลักษณ์ฮงมุน",    amount:15 },//27
    { img: popupFree21,title:"สัญลักษณ์ฮงมุน",    amount:15 },//28
    { img: popupFree21,title:"สัญลักษณ์ฮงมุน",    amount:15 },//29
    { img: popupFree30,title:"สัญลักษณ์ฮงมุน",    amount:50 },//30
    { img: popupFree31,title:"สัญลักษณ์กลุ่มโจรสลัดแมว",    amount:2 },//31
    { img: popupFree31,title:"สัญลักษณ์กลุ่มโจรสลัดแมว",    amount:2 },//32
    { img: popupFree31,title:"สัญลักษณ์กลุ่มโจรสลัดแมว",    amount:2 },//33
    { img: popupFree31,title:"สัญลักษณ์กลุ่มโจรสลัดแมว",    amount:2 },//34
    { img: popupFree31,title:"สัญลักษณ์กลุ่มโจรสลัดแมว",    amount:2 },//35
    { img: popupFree31,title:"สัญลักษณ์กลุ่มโจรสลัดแมว",    amount:2 },//36
    { img: popupFree31,title:"สัญลักษณ์กลุ่มโจรสลัดแมว",    amount:2 },//37
    { img: popupFree31,title:"สัญลักษณ์กลุ่มโจรสลัดแมว",    amount:2 },//38
    { img: popupFree31,title:"สัญลักษณ์กลุ่มโจรสลัดแมว",    amount:2 },//39
    { img: popupFree40,title:"สัญลักษณ์กลุ่มโจรสลัดแมว",    amount:25 },//40
    { img: popupFree41,title:"ประกายฉลามดำ",    amount:2 },//41
    { img: popupFree41,title:"ประกายฉลามดำ",    amount:2 },//42
    { img: popupFree41,title:"ประกายฉลามดำ",    amount:2 },//43
    { img: popupFree41,title:"ประกายฉลามดำ",    amount:2 },//44
    { img: popupFree41,title:"ประกายฉลามดำ",    amount:2 },//45
    { img: popupFree41,title:"ประกายฉลามดำ",    amount:2 },//46
    { img: popupFree41,title:"ประกายฉลามดำ",    amount:2 },//47
    { img: popupFree41,title:"ประกายฉลามดำ",    amount:2 },//48
    { img: popupFree41,title:"ประกายฉลามดำ",    amount:2 },//49
    { img: popupFree50,title:"อัญมณีกรุฮงมุน",    amount:1 },//50
    { img: popupFree51,title:"ล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส)",    amount:1 },//51
    { img: popupFree51,title:"ล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส)",    amount:1 },//52
    { img: popupFree51,title:"ล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส)",    amount:1 },//53
    { img: popupFree51,title:"ล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส)",    amount:1 },//54
    { img: popupFree51,title:"ล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส)",    amount:1 },//55
    { img: popupFree51,title:"ล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส)",    amount:1 },//56
    { img: popupFree51,title:"ล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส)",    amount:1 },//57
    { img: popupFree51,title:"ล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส)",    amount:1 },//58
    { img: popupFree51,title:"ล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส)",    amount:1 },//59
    { img: popupFree60,title:"อัญมณีหินพิทักษณ์ทะเลสาป",    amount:1 },//60
    { img: popupFree61,title:"กล่องลูกแก้ว ขั้น 1 (ศุกร์ เสาร์)",    amount:1 },//61
    { img: popupFree61,title:"กล่องลูกแก้ว ขั้น 1 (ศุกร์ เสาร์)",    amount:1 },//62
    { img: popupFree61,title:"กล่องลูกแก้ว ขั้น 1 (ศุกร์ เสาร์)",    amount:1 },//63
    { img: popupFree61,title:"กล่องลูกแก้ว ขั้น 1 (ศุกร์ เสาร์)",    amount:1 },//64
    { img: popupFree61,title:"กล่องลูกแก้ว ขั้น 1 (ศุกร์ เสาร์)",    amount:1 },//65
    { img: popupFree61,title:"กล่องลูกแก้ว ขั้น 1 (ศุกร์ เสาร์)",    amount:1 },//66
    { img: popupFree61,title:"กล่องลูกแก้ว ขั้น 1 (ศุกร์ เสาร์)",    amount:1 },//67
    { img: popupFree61,title:"กล่องลูกแก้ว ขั้น 1 (ศุกร์ เสาร์)",    amount:1 },//68
    { img: popupFree61,title:"กล่องลูกแก้ว ขั้น 1 (ศุกร์ เสาร์)",    amount:1 },//69
    { img: popupFree70,title:"กล่องอัญมณีเจ็ดเหลี่ยมของฮงมุน",    amount:1 },//70
    { img: popupFree71,title:"ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง",    amount:1 },//71
    { img: popupFree71,title:"ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง",    amount:1 },//72
    { img: popupFree71,title:"ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง",    amount:1 },//73
    { img: popupFree71,title:"ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง",    amount:1 },//74
    { img: popupFree71,title:"ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง",    amount:1 },//75
    { img: popupFree71,title:"ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง",    amount:1 },//76
    { img: popupFree71,title:"ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง",    amount:1 },//77
    { img: popupFree71,title:"ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง",    amount:1 },//78
    { img: popupFree71,title:"ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง",    amount:1 },//79
    { img: popupFree80,title:"กรุพิศวง",    amount:1 },//80
    { img: popupFree81,title:"กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส)",    amount:1 },//81
    { img: popupFree81,title:"กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส)",    amount:1 },//82
    { img: popupFree81,title:"กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส)",    amount:1 },//83
    { img: popupFree81,title:"กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส)",    amount:1 },//84
    { img: popupFree81,title:"กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส)",    amount:1 },//85
    { img: popupFree81,title:"กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส)",    amount:1 },//86
    { img: popupFree81,title:"กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส)",    amount:1 },//87
    { img: popupFree81,title:"กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส)",    amount:1 },//88
    { img: popupFree81,title:"กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส)",    amount:1 },//89
    { img: popupFree90,title:"คริสตัลวิญญาณฮงมุน",    amount:1 },//90
    { img: popupFree91,title:"กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์)",    amount:1 },//91
    { img: popupFree91,title:"กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์)",    amount:1 },//92
    { img: popupFree91,title:"กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์)",    amount:1 },//93
    { img: popupFree91,title:"กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์)",    amount:1 },//94
    { img: popupFree91,title:"กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์)",    amount:1 },//95
    { img: popupFree91,title:"กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์)",    amount:1 },//96
    { img: popupFree91,title:"กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์)",    amount:1 },//97
    { img: popupFree91,title:"กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์)",    amount:1 },//98
    { img: popupFree91,title:"กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์)",    amount:1 },//99
    { img: popupFree100,title:"ชุดอักขระทองคำ",    amount:1 },//100
]
