import React from 'react';
import { Link } from 'react-router-dom';
import iconMenu from './../static/images/icon_menu.png';
import iconMenuClose from './../static/images/menu_close.png';
export class Menu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showMenu:false

        }
    }

    handleClickMenu() {
        this.setState({
            showMenu:!this.state.showMenu
        })
    }

    render() {
        return (
            <div className="left__menu">
                <div className={"bg-overlay " + (this.state.showMenu ? "show" : " ") } onClick={this.handleClickMenu.bind(this)}></div>
                <a onClick={this.handleClickMenu.bind(this)} className="menu-icon"><img alt="icon" src={iconMenu} /></a>
                <ul className={"left__menu--list " + (this.state.showMenu ? "show" : " ") }>
                    <a className="menu-close" onClick={this.handleClickMenu.bind(this)} ><span className="menu-close__icon" /></a>
                    <li><Link onClick={this.handleClickMenu.bind(this)} to="/snakeladders/soul/" >เข้าร่วมกิจกรรมและโปรโมชั่น</Link></li>
                    <li><Link onClick={this.handleClickMenu.bind(this)} to="/snakeladders/" >เงื่อนไขโปรโมชั่น</Link></li>
                    <li><Link onClick={this.handleClickMenu.bind(this)} to="/snakeladders/send/" >ส่งของรางวัล</Link></li>
                    <li><Link onClick={this.handleClickMenu.bind(this)} to="/snakeladders/history/" >ประวัติ</Link></li>
                </ul>
            </div>
        )
    }
}
