import React from 'react';
import Modal from './Modal';

export default class ModalInput extends React.Component {

    actConfirm(){
        let target_uid = document.getElementById("target_uid").value;
        this.props.actConfirm(target_uid)
    }

    render() {
        // let title = this.props.data && this.props.data || [];
        return (
            <Modal
                open={this.props.open}
                title="กรุณากรอก UID"
                confirmMode={true}
                actConfirm={this.actConfirm.bind(this)}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox">
                    <input type="text" className="input_uid" name="target_uid" id="target_uid" />
                </div>
            </Modal>
        )
  }
}
