import React from 'react';
import Modal from './Modal';

export default class extends React.Component {
    render() {
        let title = this.props.data && this.props.data || [];
        return (
            <Modal
                open={this.props.open}
                title="Special"
                confirmMode={false}
                actClose={()=>this.props.actClose()}
            >
                <div>
                    ท่านได้ไปต่อช่องที่ {this.props.number}
                </div>
            </Modal>
        )
  }
}
