import React, { Component } from 'react';
import Dice from './Dice';

export default class extends Component {
    actOnClick(){
        clearTimeout(this.timeout);
        this.timeout=null;
        if(this.props.onClick) this.props.onClick();
    }
    createTimeout(){
        if(this.timeout !== null){
            clearTimeout(this.timeout);
            this.timeout=null;
        }
        this.timeout = setTimeout(()=>{
            //close modal
            this.actOnClick();
        },2000+3000);
    }
    constructor(props){
        super(props);
        this.timeout = null;
    }
    componentDidMount(){
        console.log("componentDidMount");
        if(this.props.open){
            this.createTimeout();
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.open !== prevProps.open){
            if(this.props.open){
                this.createTimeout();
            }
        }
    }

    render() {
        return (
            <div className={"modal "+(this.props.open ? "":"close")} onClick={this.actOnClick.bind(this)}>
                <div className="modal__backdrop" />
                {this.props.open &&
                    <Dice number={this.props.number || 1}/>
                }
            </div>
        )
  }
}
// <div className={"modal__content"}>
//     <div className={"modal__head"}>{this.props.title}</div>
//     {this.props.children}
//     <div className="modal__bottom">
//
//         {this.props.confirmMode &&
//             <a className="modal__button" onClick={()=>this.props.actConfirm()}>
//                 ตกลง
//             </a>
//         }
//         {this.props.confirmMode &&
//             <a className="modal__button" onClick={()=>this.props.actClose()}>
//                 ยกเลิก
//             </a>
//
//         }
//         {!this.props.confirmMode &&
//             <a className="modal__button" onClick={()=>this.props.actClose()}>
//                 ตกลง
//             </a>
//         }
//     </div>
// </div>
