import React from 'react';
import Modal from './Modal';

export default class extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                title="ได้รับ"
                confirmMode={false}
                actClose={()=>this.props.actClose()}
            >
                {this.props.itemList.map((item,index)=>(
                    <div>{item.title}</div>
                ))}
            </Modal>
        )
  }
}
// <div>{item.title} x {item.amount}</div>
