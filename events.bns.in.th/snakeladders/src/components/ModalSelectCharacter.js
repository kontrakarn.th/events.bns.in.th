import React, { Component } from 'react'
import Modal from './Modal';
import iconDropdown from './../static/images/icon_dropdown.png'
export default class ModalSelectCharacter extends Component {
    actSelectCharacter(){
        let indexCharacter = document.getElementById("charater_form").value;
        let idCharacter = this.props.charaterList[indexCharacter].id;
        let nameCharacter = this.props.charaterList[indexCharacter].char_name;
        this.props.actSelectCharacter(idCharacter,nameCharacter)
    }
    render() {
        return (

            <Modal
                open={this.props.open}
                title="เลือกตัวละคร"
                confirmMode={false}
                actClose={()=>this.actSelectCharacter()}
            >
                <form className="formselect">
                    <label >
                        <select id="charater_form" className="fromselect__list">
                            {this.props.open && this.props.charaterList && this.props.charaterList.map((item,index)=>{
                                return (
                                    <option key={index} value={index}>{item.char_name}</option>
                                )
                            })}
                        </select>
                        <img src={iconDropdown} alt="" />
                    </label>
                </form>
            </Modal>
        )
  }
}
