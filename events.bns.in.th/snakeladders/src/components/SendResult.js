import React from 'react';
import ReactModal from 'react-modal';
import { connect } from 'react-redux';
import {
    setValues,
    setLoading
} from './../../store/actions';
import { apiPost, apiPostFormData } from './../../constants/api';


const customStyles = {
    overlay: {
        backgroundColor : 'rgba(0, 0, 0, 0.5)',
        zIndex: '10',
        top: '0',
        maxWidth: "100vw",
        maxHeight: "100vh",
    },
    content: {
        top: "50%",
        left: "50%",
        transform: 'translate(-50%, -50%)',
        width: '90%',
        height: "90%",
        height: "min-content",
        maxWidth: "700px",
        maxHeight: "90vh",
        padding: '0',
        border: 'none',
        backgroundColor: 'rgba(0,0,0,0.6)'
    }
}

class SendResultModal extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            images: [],
            imagesLoaded: [],
            processing: false,
        }
        this.loading = [];
    }
    handleChange(e){
        console.log("handleChange 1 ",e.target.name,e.target.name);

        // this.setState({
        //     images: Object.values(e.target.files).map(item=>item.name),
        //     imagesFile: e.target.files,
        // })
        // console.log(e.target.files)

    }
    handleChangeImage(e){
        // console.log("handleChange",);
        let list = Object.values(e.target.files).map(item=>item).slice(0, 9)
        this.setState({
            images: Object.values(e.target.files).map(item=>item).slice(0, 9),
            imagesLoaded: list.map(()=>"")
        })
        for(let i = 0 ; i < list.length; i++){
            if(this.loading[i]){
                this.loading[i].onload = ()=>{};
                this.loading[i] = null;
            }
            let reader = new FileReader();
            reader.onload = (event)=>{
                let img = new Image;

                img.onload = ()=>{
                    let canvas = document.createElement('canvas');
                    canvas.width = 1280;//img.naturalWidth;
                    canvas.height = 960;//img.naturalHeight;

                    let ratio = 1;
                    ratio = Math.min(1280 / img.naturalWidth  , 960 / img.naturalHeight);
                    let nw = img.naturalWidth * ratio;
                    let nh = img.naturalHeight * ratio;
                    let px = 0.5 * (1280 - nw);
                    let py = 0.5 * (960 - nh);
                    canvas.getContext('2d').drawImage(img, 0, 0, img.naturalWidth, img.naturalHeight, px, py, nw, nh);
                    // callback(canvas.toDataURL('image/jpeg').replace(/^data:image\/(png|jpg);base64,/, ''));
                    let myImage=canvas.toDataURL("image/jpeg");

                    let {imagesLoaded} = this.state;
                    imagesLoaded[i] = myImage;
                    this.setState({imagesLoaded});
                }
                img.src = reader.result;
            }
            reader.readAsDataURL(e.target.files[i]);
            this.loading[i] = reader;
        }

    }
    genCanvasForSave(){

    }
    actSend(){
        let { team_a, team_b, tournament_id, group, round} = this.props.sendResultData
        // let formData = new FormData(document.getElementById("sendresult_form"));
        let formData = new FormData();
        formData.append('tournament_id',tournament_id);
        formData.append('group',group);
        formData.append('round',round);
        formData.append('team_a_score',document.getElementById("team_a_score").value);
        formData.append('team_a_id',team_a.id);
        formData.append('team_b_score',document.getElementById("team_b_score").value);
        formData.append('team_b_id',team_b.id);
        formData.append('remark',document.getElementById("remark").value);

        let {imagesLoaded} = this.state;
        // let imagesBlob = []
        for(let i = 0 ; i < imagesLoaded.length; i++){

			let block = imagesLoaded[i].split(";");

			let contentType = block[0].split(":")[1];

			let realData = block[1].split(",")[1];

			// Convert it to a blob to upload
			let blob = this.b64toBlob(realData, contentType);
            // imagesBlob[i] = blob;
            // img_team_crop = blob;
            formData.append('submit_image_'+(i+1),blob);
        }
        // formData.append('submit_images',imagesBlob);
        if(this.props.actSendResult)this.props.actSendResult(formData)

    }
    actCancel(){
        this.props.setValues({sendResultModal:false})
    }

    b64toBlob(b64Data, contentType, sliceSize) {
	        contentType = contentType || '';
	        sliceSize = sliceSize || 512;

	        var byteCharacters = atob(b64Data);
	        var byteArrays = [];

	        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
	            var slice = byteCharacters.slice(offset, offset + sliceSize);

	            var byteNumbers = new Array(slice.length);
	            for (var i = 0; i < slice.length; i++) {
	                byteNumbers[i] = slice.charCodeAt(i);
	            }

	            var byteArray = new Uint8Array(byteNumbers);

	            byteArrays.push(byteArray);
	        }

	      var blob = new Blob(byteArrays, {type: contentType});
	      return blob;
	}
//
//
//
// apiPostFormData
    render() {
        let { images, remark, imagesLoaded } = this.state;
        let { team_a, team_b, tournament_id, group, round} = this.props.sendResultData
        return (
            <ReactModal
                isOpen={this.props.sendResultModal}
                onRequestClose={() => this.props.callbackStatus ? this.props.callbackAction() : this.props.cancelAction()}
                style={customStyles}
            >
                <div className="spdmodal">
                    <div className="spdmodal__header">
                        ส่งผลการแข่งขัน
                    </div>
                    <div className="spdmodal__content ">
                        <form className="sendresultmodal" id="sendresult_form">
                            <input type="hidden" name="tournament_id" value={tournament_id} />
                            <input type="hidden" name="group" value={group} />
                            <input type="hidden" name="round" value={round} />
                            <div className="sendresultmodal__section">
                                <div className="sendresultmodal__topic"></div>
                                <div className="sendresultmodal__detail">
                                    <div className="sendresultmodal__team sendresultmodal__team--a">
                                        <img src={team_a.logo} />
                                        <span>{team_a.name}</span>
                                        <label> ผลคะแนน <input type="text" id="team_a_score" /> </label>
                                        <input type="hidden" name="team_a_id" value={team_a.id} />
                                        {/*<label> ผลคะแนน <input type="text" name="team_a" name="score_a" value={team_a.score} onChange={this.handleChange.bind(this)}/> </label>*/}
                                    </div>
                                    <div className="sendresultmodal__team sendresultmodal__team--b">
                                        <img src={team_b.logo} />
                                        <span>{team_b.name}</span>
                                        <label> ผลคะแนน <input type="text" id="team_b_score" /> </label>
                                        <input type="hidden" name="team_b_id" value={team_b.id} />
                                    </div>
                                </div>
                            </div>
                            <div className="sendresultmodal__section">
                                <div className="sendresultmodal__topic">รูปคะแนนยืนยัน</div>
                                <div className="sendresultmodal__detail">
                                    <div>
                                        <input
                                            className="sendresultmodal__hide"
                                            accept="image/jpg, image/jpeg, image/png"
                                            type="file"
                                            max={9}
                                            id="img"
                                            name="img"
                                            multiple
                                            onChange={this.handleChangeImage.bind(this)}
                                        />
                                        <label className="sendresultmodal__btn" type="button" htmlFor="img" >อัพโหลดรูปภาพ</label>
                                        <span> (สูงสุด 9 รูป) </span>
                                    </div>
                                    <ul className="sendresultmodal__listimage">
                                        {images.map((item,index)=>(
                                            <li key={"listname_"+index}>
                                                {(index+1)+ " " +item.name} <i className={imagesLoaded[index]!=="" ? "fa fa-check":"fa fa-refresh" } />
                                            </li>
                                        ))}
                                    </ul>
                                </div>

                            </div>
                            <div className="sendresultmodal__section">
                                <div className="sendresultmodal__topic">ข้อความเพิ่มเติม</div>
                                <div className="sendresultmodal__detail">
                                    <textarea className="sendresultmodal__comment" type="textarea" id="remark" />
                                </div>

                            </div>
                        </form>
                    </div>
                    <div className="spdmodal__footer">
                        <a className="spdmodal__btn spdmodal__btn--gold" onClick={()=>this.actSend()}>ตกลง</a>
                        <a className="spdmodal__btn" onClick={() =>this.actCancel()}>ยกเลิก</a>
                    </div>
                </div>
            </ReactModal>
        );
    }
}
const mapStateToProps = (state) => ({
	userData: state.userData,
	profileData: state.profileData,
    sendResultModal: state.sendResultModal,
    sendResultData: state.sendResultData,
})
const mapDispatchToProps = {setValues,setLoading};

export default connect(mapStateToProps,mapDispatchToProps)(SendResultModal);
