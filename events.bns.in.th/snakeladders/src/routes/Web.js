import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import OauthMiddleware from './../middlewares/OauthMiddleware';

import { Home } from './../pages/Home';
import { Soul } from './../pages/Soul'
import { ActivitiesEvent } from './../pages/ActivitiesEvent'
import { PromotionEvent } from './../pages/PromotionEvent'
import { Send } from './../pages/Send'
import { Random } from './../pages/Random'
import { HistoryList } from './../pages/HistoryList'

export class Web extends Component {
	render() {
		return (
			<div>
			 	<Route path={process.env.REACT_APP_EVENT_PATH} component={OauthMiddleware} />
				<Route path={process.env.REACT_APP_EVENT_PATH} exact component={Home} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/soul'} exact component={Soul} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/activitiesevent'} exact component={ActivitiesEvent} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/promotionevent'} exact component={PromotionEvent} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/send'} exact component={Send} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/random'} exact component={Random} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/history'} exact component={HistoryList} />
			</div>
		);
	}
}
