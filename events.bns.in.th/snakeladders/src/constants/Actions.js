export const ACCOUNT_AUTHED = 'ACCOUNT_AUTHED';
export const ACCOUNT_LOGIN = 'ACCOUNT_LOGIN';
export const ACCOUNT_LOGOUT = 'ACCOUNT_LOGOUT';
export const RECEIVE_SESSION_KEY = 'RECEIVE_SESSION_KEY';
export const SET_LOGIN_URL = 'SET_LOGIN_URL';
export const SET_LOGOUT_URL = 'SET_LOGOUT_URL';
export const ACCOUNT_AUTH_CHECKED = 'ACCOUNT_AUTH_CHECKED';

//EVENT Action
// export const SET_MISSION_COMPLETE = 'SET_MISSION_COMPLETE';
// export const SET_RECIEVED_REWARD = 'SET_RECIEVED_REWARD';
