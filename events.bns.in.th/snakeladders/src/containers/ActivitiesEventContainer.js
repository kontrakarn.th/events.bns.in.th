import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';

import Modal from '../components/Modal';
import ModalDice from '../components/ModalDice';
import ModalMessage from '../components/ModalMessage';
import ModalReceiveItem from '../components/ModalReceiveItem';
import ModalSpecial from '../components/ModalSpecial';
import ModalSpecialReceiveItem from '../components/ModalSpecialReceiveItem';

import Snakeladder from '../components/Snakeladder';
import { snakeladder } from '../components/SnakeladderItemList';

import { Menu } from '../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
import iconMenu from './../static/images/icon_menu.png';
import arrowStatus from './../static/images/arrow.png';

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
} from '../actions/AccountActions';

const headClearItem = ['ดันเจี้ยน', 'รายชื่อเควส', 'สถานะ']
class ActivitiesEventContainer extends Component {

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scollDown === false) this.setState({ scollDown: true });
        } else {
            if (this.state.scollDown === true) this.setState({ scollDown: false });
        }
        if (e.topPosition > 572) {
            if (this.state.lockbtn === false) this.setState({ lockbtn: true });
        } else {
            if (this.state.lockbtn === true) this.setState({ lockbtn: false });
        }
    }
    actPlayFreeDice(){
        this.apiPlayFreeDice();
        // let newCurrentStep = this.state.next_step;
        // let newNextStep = newCurrentStep + 1 + Math.floor(Math.random()*6);
        // console.log("actPlayFreeDice",newCurrentStep,newNextStep);
        // this.setState({
        //     showModalDice: true,
        //     showDice: newNextStep-newCurrentStep,
        //     current_step_: newCurrentStep,
        //     next_step_: newNextStep,
        // });

    }
    actSetAnimation(){
        this.setState({
            showModalDice: false,
            showLoading: false,
            current_step_: this.state.current_step,
            next_step_: this.state.next_step,
        });
        setTimeout(()=>{
            this.setState({
                showModalReceiveItem: true
            });
        }, ((this.state.showDice + 1)*1000) + 200);
    }
    closeModalReceiveItem(){
        this.setState({
            showModalReceiveItem: false,
            current_step_: this.state.next_step_,
            current_step: this.state.next_step_,
            showModalSpecial: this.state.has_special_path
        })
    }
    actSetSpecialAnimation(){
        let {current_step, special_path_step} =  this.state;
        this.setState({
            showModalSpecial: false,
            showLoading: false,
            current_step_: current_step,
            next_step_: special_path_step,
        });

        setTimeout(()=>{
            this.setState({
                showModalSpecialReceiveItem: true
            });
        }, ((special_path_step - current_step + 1)*1000) + 200);
    }
    closeModalSpecialReceiveItem(){
        this.setState({
            showModalSpecialReceiveItem: false,
            current_step_: this.state.next_step_,
            current_step: this.state.next_step_,
        })
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiFreeBoardInfo() {
        this.setState({ showLoading: true });
        let dataSend = { type: "event_info" };
        let successCallback = (data) => {

            if (data.status) {
                this.props.setUsername(data.content.character);
                this.setState({
                    permission: true,
                    showLoading: false,
                    ...data.content,
                    current_step: parseInt(data.content.current_step),
                    current_step_: parseInt(data.content.current_step),
                    next_step_: parseInt(data.content.current_step),
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_FREE_BOARD_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiPlayFreeDice() {
        this.setState({ showLoading: true });
        let dataSend = { type: "play_free_dice" };
        let successCallback = (data) => {
            let newCurrentStep = this.state.current_step;
            let newNextStep = parseInt(data.next_step);

            let next_step_rewards = data.next_step_rewards;
            let special_path_rewards = data.special_path_rewards;
            if(next_step_rewards.length < 1){
                next_step_rewards = data.rare_reward;
            } else {
                if(data.has_special_path) {
                    special_path_rewards = [...data.special_path_rewards,...data.rare_reward];
                } else {
                    next_step_rewards = [...data.next_step_rewards,...data.rare_reward];
                }
            }

            this.setState({
                permission: true,
                showLoading: false,
                showModalDice: true,
                showDice: newNextStep-newCurrentStep,
                // ...data.content,
                // current_step: newCurrentStep,

                free_rights: data.content.free_rights,
                free_rights_used: data.content.free_rights_used,
                completed_status: data.content.completed_status,
                quest_rights_used: data.content.quest_rights_used,
                quests: data.content.quests,
                next_step: newNextStep,
                next_step_rewards: next_step_rewards,
                has_special_path: data.has_special_path,
                special_path_step: parseInt(data.special_path_step),
                special_path_rewards: special_path_rewards,
            });
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_PLAY_FREE_DICE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props);
        this.state = {
            scollDown: false,
            permission: true,
            showLoading: true,
            showModalDice: false,
            showDice: 0,
            showModalReceiveItem:false,
            showModalSpecial: false,
            showModalSpecialReceiveItem:false,
            current_step_: 0,
            next_step_: 0,

            current_step: 0,
            free_rights: true,
            free_rights_used: false,
            completed_status: false,
            quest_rights_used: false,
            quests: [
                {id: 1, title: "เสียงหอนของเตาหลอม",  completed: false},
                {id: 2, title: "พันธะที่ผูกเราไว้",       completed: false},
                {id: 3, title: "เสียงเพรียกของสายลม",  completed: false},
                {id: 4, title: "ผู้บุกรุกที่ศาลนักพรตนาริว", completed: false},
                {id: 5, title: "รุกฆาต",              completed: false},
            ],
            lockbtn: false,

            showModalMessage: "",
            // clearItem: [
            questLsit: [
                { dungeon: 'โรงหลอมอัคคี',       name: 'เสียงหอนของเตาหลอม' },
                { dungeon: 'สุสานที่ถูกลืม',        name: 'พันธะที่ผูกเราไว้' },
                { dungeon: 'โรงหลอมเครื่องจักร',  name: 'เสียงเพรียกของสายลม' },
                { dungeon: 'สถานศักดิ์สิทธิ์นาริว',   name: 'ผู้บุกรุกที่ศาลนักพรตนาริว' },
                { dungeon: 'วิหารลับเงามังกรดำ',  name: 'รุกฆาต' }
            ],
            next_step: 0,
            next_step_rewards: [],
            has_special_path: false,
            special_path_rewards: [],
            special_path_step: 0,

        }
    }
    componentDidMount(){
        this.apiFreeBoardInfo();
    }
    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                    {!this.state.scollDown && <div className="f11layout__scroll"><img src={iconScroll} alt="" /></div>}
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: 700, opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="preorder">
                                <div className="page__wrapper">
                                    <Menu />
                                    <section className="preorder__section">
                                        <div className="container">
                                            <div className="section__head section__head--clearitem">
                                                <h3>เคลียร์ดันเจี้ยนรับสิทธิ์ฟรี</h3>
                                            </div>
                                            <div className="section__content section__content--clearitem">
                                                <div className="clearitem">
                                                    <div className="clearitem__wrap">
                                                        <div className="clearitem__table">
                                                            {
                                                                headClearItem.map((head, indexHead) => {
                                                                    return <div key={indexHead} className="clearitem__txt">{head}</div>
                                                                })
                                                            }
                                                        </div>
                                                        <div className="clearitem__table clearitem__table--body">
                                                            <div>
                                                                {
                                                                    this.state.questLsit.map((data, indexData) => {
                                                                        return (
                                                                            <div key={indexData} className="clearitem__wraptxt">
                                                                                <div className="clearitem__txt">
                                                                                    <span>{data.dungeon}</span>
                                                                                </div>
                                                                                <div className="clearitem__txt">
                                                                                    <span>{data.name}</span>
                                                                                </div>
                                                                                <div className="clearitem__txt">
                                                                                    <div className="clearitem__status">
                                                                                        {this.state.quests[indexData].completed && <img src={arrowStatus} />}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        )
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="clearitem__wrap clearitem__wrap--right">
                                                        <div className="clearitem__boxnum">
                                                            <div>
                                                                <div className="clearitem__txtnum">สิทธิ์การทอยลูกเต๋าฟรี</div>
                                                                <div className={"clearitem__txtnum clearitem__txtnum--num"+(this.state.free_rights_used?" used":"")}>{this.state.free_rights? 1:0}/1</div>
                                                                <div>*รีเซ็ททุกวันเวลา 06:00:00 น.*</div>
                                                            </div>
                                                        </div>
                                                        <div className="clearitem__boxnum">
                                                            <div>
                                                                <div className="clearitem__txtnum">สิทธิ์เคลียร์ดันเจี้ยน</div>
                                                                <div className={"clearitem__txtnum clearitem__txtnum--num"+(this.state.quest_rights_used?" used":"")}>{this.state.completed_status? 1:0}/1</div>
                                                                <div>*รีเซ็ททุกวันเวลา 06:00:00 น.*</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section className="preorder__section">
                                        <div className="container">
                                            <div className="section__head section__head--clearitem">
                                                <h3>กิจกรรมบันไดงู</h3>
                                            </div>
                                            <div className="section__content">
                                                <Snakeladder
                                                    items={snakeladder}
                                                    actPlayFreeDice={this.actPlayFreeDice.bind(this)}
                                                    lockbtn={this.state.lockbtn}
                                                    current_step={this.state.current_step_}
                                                    next_step={this.state.next_step_}
                                                />
                                            </div>
                                        </div>
                                    </section>
                                    <div className="bg-bottom bg-bottom--snakeladders"></div>
                                </div>
                            </div>
                        </ScrollArea>

                    </div>
                    :
                    <Permission />
                }
                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    confirmMode={false}
                    actClose={() => this.setState({ showModalMessage: "" })}
                    msg={this.state.showModalMessage}
                />
                <ModalDice
                    open={this.state.showModalDice}
                    number={this.state.showDice}
                    onClick={this.actSetAnimation.bind(this)}
                />
                <ModalReceiveItem
                    open={this.state.showModalReceiveItem}
                    itemList={this.state.next_step_rewards}
                    actClose={this.closeModalReceiveItem.bind(this)}
                />
                <ModalSpecial
                    open={this.state.showModalSpecial}
                    number={this.state.special_path_step}
                    actClose={this.actSetSpecialAnimation.bind(this)}
                />
                <ModalSpecialReceiveItem
                    open={this.state.showModalSpecialReceiveItem}
                    itemList={this.state.special_path_rewards}
                    actClose={this.closeModalSpecialReceiveItem.bind(this)}
                />
                <Loading
                    open={this.state.showLoading}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    // item_history: state.AccountReducer.item_history
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ActivitiesEventContainer)
