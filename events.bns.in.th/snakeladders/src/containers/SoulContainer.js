import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import ModalMessage from '../components/ModalMessage';

import { Menu } from './../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import snake1 from './../static/images/snake1.png'
import snake2 from './../static/images/snake2.png'

import SoulEffect from './../components/SoulEffect';

/* import iconScroll from './../static/images/icons/icon_scroll.png';
import iconMenu from './../static/images/icon_menu.png' */

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
} from '../actions/AccountActions';

const btnGroup = [
    {
        name: 'กิจกรรมบันไดงู',
        img: snake1,
        txtBtn: 'เข้าร่วมกิจกรรม',
        url: 'activitiesevent'
    },
    {
        name: 'โปรโมชั่นบันไดงู',
        img: snake2,
        txtBtn: 'เข้าร่วมโปรโมชั่น',
        url: 'random'
    }
]
class SoulContainer extends Component {


    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scrollDown === false) this.setState({ scrollDown: true });
        } else {
            if (this.state.scrollDown === true) this.setState({ scrollDown: false });
        }
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: false,
            scrollDown: false,
            showModalConfirmOpenSoul: false,
            modalContent: {},

            showModalMessage: "",
            username: "",
            buy_soul_info: [],
            buy_soul_result : [],
        }
    }


    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: 700, opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                            onScroll={this.handleScroll.bind(this)}
                        >
                            <div className="preorder">
                                <div className="page__wrapper">
                                    <Menu />
                                    <section className="preorder__section soul">

                                        <div className="container container--soul">
                                            <div className="section__head">
                                                <h3>เข้าร่วมกิจกรรมหรือโปรโมชั่น</h3>
                                            </div>
                                            <div className="section__content section__content--soul">
                                                {
                                                    btnGroup.map((btns, index) => {
                                                        return (
                                                            <div key={index} className="soul__items">
                                                                <div className="soul__items--img">
                                                                    <div className="soul__items--text">
                                                                        {btns.name}
                                                                    </div>
                                                                    {/* <div style={{height: items.img_h, width: items.img_w, margin: "0 auto"}}>
                                                                        <SoulEffect
                                                                            open={false}
                                                                            active={false}
                                                                            soul={items.img}
                                                                            item=""
                                                                        />
                                                                    </div> */}
                                                                    <div className="soul__img">
                                                                        <img src={btns.img} />
                                                                    </div>
                                                                    <Link to={"/snakeladders/" + btns.url+"/"} className="soul__btn">
                                                                        <div>{btns.txtBtn}</div>
                                                                    </Link>
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>

                                    </section>

                                </div>
                            </div>
                        </ScrollArea>

                    </div>
                    :
                    <Permission />
                }

                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
                <Loading
                    open={this.state.showLoading}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    // permission: state.AccountReducer.permission,
    buy_soul_result: state.AccountReducer.buy_soul_result
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(SoulContainer))
