import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import {apiPost} from '../middlewares/Api';
import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';

import ModalMessage from '../components/ModalMessage';
import ModalInput from '../components/ModalInput';
// import ModalConfirmReceiveGacha from '../components/ModalConfirmReceiveGacha';
import ModalNoReceive from '../components/ModalNoReceive';
import ModalConfirmSend from '../components/ModalConfirmSend';
import ModalConfirmSendGift from '../components/ModalConfirmSendGift';

import {Menu} from '../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
// import ImgItems from './../static/images/items/set1no_num.png'

import item1 from './../static/images/sendItem/item1.png'
import item2 from './../static/images/sendItem/item2.png'
import item3 from './../static/images/sendItem/item3.png'
import item4 from './../static/images/sendItem/item4.png'
import item5 from './../static/images/sendItem/item5.png'
import item6 from './../static/images/sendItem/item6.png'
import item7 from './../static/images/sendItem/item7.png'
import item8 from './../static/images/sendItem/item8.png'
import item9 from './../static/images/sendItem/item9.png'
import item10 from './../static/images/sendItem/item10.png'
import item11 from './../static/images/sendItem/item11.png'
import item12 from './../static/images/sendItem/item12.png'
import item13 from './../static/images/sendItem/item13.png'
import item14 from './../static/images/sendItem/item14.png'
import item15 from './../static/images/sendItem/item15.png'
import item16 from './../static/images/sendItem/item16.png'
import item17 from './../static/images/sendItem/item17.png'
import item18 from './../static/images/sendItem/item18.png'
import item19 from './../static/images/sendItem/item19.png'
import item20 from './../static/images/sendItem/item20.png'
import item21 from './../static/images/sendItem/item21.png'
import item22 from './../static/images/sendItem/item22.png'
import item23 from './../static/images/sendItem/item23.png'
import item24 from './../static/images/sendItem/item24.png'
import item25 from './../static/images/sendItem/item25.png'
import item26 from './../static/images/sendItem/item26.png'
import item27 from './../static/images/sendItem/item27.png'
import item28 from './../static/images/sendItem/item28.png'
import item29 from './../static/images/sendItem/item29.png'
import item30 from './../static/images/sendItem/item30.png'
import item31 from './../static/images/sendItem/item31.png'
import item32 from './../static/images/sendItem/item32.png'
import item33 from './../static/images/sendItem/item33.png'
import item34 from './../static/images/sendItem/item34.png'
import item35 from './../static/images/sendItem/item35.png'
import item36 from './../static/images/sendItem/item36.png'

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
} from '../actions/AccountActions';

const item_list = [
    {
        img: item1,
        name: 'กล่องอาวุธลวงตาเทพพิทักษ์'
    },
    {
        img: item2,
        name: 'กล่องอาวุธกุหลาบพิษ'
    },
    {
        img: item3,
        name: 'กล่องอาวุธลวงตาพยัคฆ์ขาว'
    },
    {
        img: item4,
        name: 'กล่องอาวุธลวงตาแสงแห่งความยินดี'
    },
    {
        img: item5,
        name: 'หินล้ำค่าเทพพิทักษ์'
    },
    {
        img: item6,
        name: 'หินล้ำค่าปลากัด'
    },
    {
        img: item7,
        name: 'หินล้ำค่าพยัคฆ์ขาว'
    },
    {
        img: item8,
        name: 'หินสัตว์เลี้ยงโดรน'
    },
    {
        img: item9,
        name: 'ที่คาดผมทรงหูแมว'
    },
    {
        img: item10,
        name: 'ชุดขาวของผู้บริสุทธิ์'
    },
    {
        img: item11,
        name: 'ชุดแสงที่เจิดจ้า'
    },
    {
        img: item12,
        name: 'ที่คาดผมที่เจิดจ้า'
    },
    {
        img: item13,
        name: 'ปีกเจิดจ้า'
    },
    {
        img: item14,
        name: 'ต่างหูที่เจิดจ้า'
    },
    {
        img: item15,
        name: 'เซ็ทแมวเหมียวที่เจิดจ้า'
    },
    {
        img: item16,
        name: 'ชุดกุหลาบพิษ'
    },
    {
        img: item17,
        name: 'มงกุฎหนามกุหลาบพิษ'
    },
    {
        img: item18,
        name: 'ปีกกุหลาบพิษ'
    },
    {
        img: item19,
        name: 'ผ้าปิดตากุหลาบพิษ'
    },
    {
        img: item20,
        name: 'เซ็ทกุหลาบพิษเมี๊ยว'
    },
    {
        img: item21,
        name: 'ชุดพยัคฆ์ขาว'
    },
    {
        img: item22,
        name: 'หมวกพยัคฆ์ขาว'
    },
    {
        img: item23,
        name: 'ปีกพยัคฆ์ขาว'
    },
    {
        img: item24,
        name: 'หน้ากากพยัคฆ์ขาว'
    },
    {
        img: item25,
        name: 'เซ็ทเหมียวพยัคฆ์ขาว'
    },
    {
        img: item26,
        name: 'ชุดเทพพิทักษ์'
    },
    {
        img: item27,
        name: 'เครื่องประดับเทพพิทักษ์'
    },
    {
        img: item28,
        name: 'ปีกเทพพิทักษ์'
    },
    {
        img: item29,
        name: 'เซ็ทเทพพิทักษ์เมี๊ยว'
    },
    {
        img: item30,
        name: 'ผีเสื้อสีชาด'
    },
    {
        img: item31,
        name: 'ชุดท่วงทำนองเสียงประสาน'
    },
    {
        img: item32,
        name: 'เสียงประสานกระดิ่งดำ'
    },
    {
        img: item33,
        name: 'ชุดท่วงทำนองบริสุทธิ์'
    },
    {
        img: item34,
        name: 'ท่วงทำนองกระดิ่งขาว'
    },
    {
        img: item35,
        name: 'ชุดสง่างาม'
    },
    {
        img: item36,
        name: 'สัญลักษณ์สุภาพชน'
    }
];

class SendContainer extends Component {

// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,

            //modal
            showModalCheckUid: false,
            showModalConfirmSendGift: false,
            showModalConfirmSendGiftMessage: "",
            // showModalConfirmReceiveGacha: false,
            // showModalNoReceive: false,

            showModalSend: false,
            showModalSendMessage: "",

            showModalMessage: "",

            username: '',
            my_bag: [],
            packageId: 0,
            packageName: "",

            box_item: [
                false, false, false, false, false,
                false, false, false, false, false,
                false, false, false, false, false,
                false, false, false, false, false,
                false, false, false, false, false,
                false, false, false, false, false,
                false, false, false, false, false,
                false
            ],

            targetUid: "",

            active: -1,

        }
    }

    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiMyBag(), 1000);

    }

    componentDidUpdate(prevProps, prevState) {
        // if (this.props.jwtToken !== prevProps.jwtToken) {
        //     setTimeout(() => {
        //         this.apiMyBag();
        //     }, 1000);
        // }
        if(prevState.packageId != this.state.packageId && this.state.packageId == 0){
            this.setState({
                active: -1,
            })
        }
    }
    // REACT_APP_API_POST_SEND_GIFT
    // REACT_APP_API_POST_CHECK_UID
    apiMyBag() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "my_bag" };
        let successCallback = (data) => {

            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    my_bag: data.content.my_bag,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_MY_BAG", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSend(){
        this.setState({ showLoading: true, showModalSend: false, });
        let self = this;
        let dataSend = { type: "send_item", id: this.state.packageId };
        let successCallback = (data) => {

            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    showModalMessage: data.message,
                    my_bag: data.content.my_bag,

                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_SEND_ITEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiCheckUid(targetUid){
        // console.log(targetUid);
        this.setState({ showLoading: true, showModalCheckUid: false, });
        let self = this;
        let dataSend = { type: "check_uid", uid: targetUid };
        let successCallback = (data) => {

            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    showModalConfirmSendGift: true,
                    showModalConfirmSendGiftMessage: "ส่ง &quot;"+this.state.packageName+"&quot;<br />ให้กับ UID: &quot;"+data.content.target_uid+"&quot;<br />Username: &quot;"+data.content.target_username+"&quot;<br />หรือไม่?",
                    targetUid: targetUid,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CHECK_UID", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSendGift(){
        this.setState({ showLoading: true, showModalConfirmSendGift: false, });
        let self = this;
        let dataSend = { type: "shend_gift", id: this.state.packageId, uid: this.state.targetUid };
        let successCallback = (data) => {

            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    showModalMessage: data.message,
                    my_bag: data.content.my_bag,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_SEND_GIFT", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    checkUid(){
        if(this.state.packageId != 0){
            this.setState({
                showModalCheckUid:true,
            });
        }else{
            this.setState({
                showModalMessage:"กรุณาเลือกไอเทมก่อนส่งของขวัญ",
            });
        }
    }

    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }

    handleClickActive(index,packageId,packageName){
        this.setState({
            active:index,
            packageId: packageId,
            packageName: packageName,
        })
    }

    onConfirmSend(e){
        console.log("onConfirmSend");
        e.preventDefault();
        if(this.state.packageId != 0){
            console.log("packageId",this.state.packageId);
            this.setState({
                showModalSend: true,
                showModalSendMessage: "รับ "+this.state.packageName+"?",
            });
        }
    }

    selectItem(numItem,count){
        // const itemSelect = this.state.box_item;
        // itemSelect[numItem] = itemSelect[numItem] ? false : true;
        // this.setState({ box_item: itemSelect });
        if(count >= 0){
            let {my_bag} = this.state;
            let packageId = my_bag[numItem]? my_bag[numItem].id : 0;
            let packageName = my_bag[numItem]? my_bag[numItem].title : "";
            this.setState({
                packageId,
                packageName,
            })
        }
    }

    render() {
        let {my_bag} = this.state;
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="preorder">
                                <div className="page__wrapper">
                                    <Menu />
                                    <section className="preorder__section send">
                                        <div className="container container--senditem">
                                            <div className="section__head section__head--senditem">
                                                <h3>ส่งของรางวัล</h3>
                                            </div>
                                            <div className="section__content items__wrapper">
                                                <div className="senditem">
                                                    {
                                                        item_list.map((item, indexItem) => {
                                                            let count = my_bag[indexItem]? my_bag[indexItem].quantity : 0;
                                                            let id = my_bag[indexItem]? my_bag[indexItem].id : -2;
                                                            return (
                                                                <a key={indexItem} className={"senditem__box" + (this.state.packageId === id ? " select" : "") + (count > 0 ? "" : " inactive")} onClick={() => this.selectItem(indexItem,count)}>
                                                                    <div className="senditem__img">
                                                                        <img src={item.img} />
                                                                        <span>{count}</span>

                                                                    </div>
                                                                    <div>{item.name}</div>
                                                                </a>
                                                            )
                                                        })
                                                    }
                                                </div>
                                            </div>
                                            <div className="section__btn section__btn--senditem">
                                                <div className={"btn-default"+(this.state.packageId > 0 ? "" : " inactive")} onClick={this.onConfirmSend.bind(this)}>
                                                    <span>รับของรางวัล</span>
                                                </div>
                                                <div className={"btn-default"+(this.state.packageId > 0 ? "" : " inactive")} onClick={this.checkUid.bind(this)}>
                                                    <span>ส่งให้เพื่อน</span>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>

                        </ScrollArea>

                        <ModalInput
                            open={this.state.showModalCheckUid}
                            actConfirm={(uid)=>this.apiCheckUid(uid)}
                            actClose={()=>this.setState({
                                showModalCheckUid: false,
                                packageId: 0,
                                packageName: "",
                                targetUid:"",
                            })}
                        />

                        <ModalConfirmSendGift
                            open={this.state.showModalConfirmSendGift}
                            actConfirm={()=>this.apiSendGift()}
                            actClose={()=>this.setState({
                                showModalConfirmSendGift: false,
                                packageId: 0,
                                packageName: "",
                                targetUid: "",
                            })}
                            msg={this.state.showModalConfirmSendGiftMessage}
                        />

                        <ModalConfirmSend
                            open={this.state.showModalSend}
                            actConfirm={()=>this.apiSend()}
                            actClose={()=>this.setState({
                                showModalSend: false,
                                showModalSendMessage: "",
                                packageId: 0,
                                packageName: "",

                            })}
                            msg={this.state.showModalSendMessage}
                        />

                        <ModalMessage
                            open={this.state.showModalMessage.length > 0}
                            actClose={()=>this.setState({
                                showModalMessage: "",
                                packageId: 0,
                                packageName: "",
                                targetUid: "",

                            })}
                            msg={this.state.showModalMessage}
                        />
                    </div>
                :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(SendContainer))
