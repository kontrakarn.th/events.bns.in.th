import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter, Redirect } from 'react-router-dom';

import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import ModalMessage from '../components/ModalMessage';
import ModalConfirmRandom from '../components/ModalConfirmRandom';
import ModalConfirmAcceptBoard from '../components/ModalConfirmAcceptBoard';

import { Menu } from './../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';

import board1 from './../static/images/bgBoard/board1.png'
import board2 from './../static/images/bgBoard/board2.png'
import board3 from './../static/images/bgBoard/board3.png'
import board4 from './../static/images/bgBoard/board4.png'
import board5 from './../static/images/bgBoard/board5.png'
import board6 from './../static/images/bgBoard/board6.png'
import board7 from './../static/images/bgBoard/board7.png'
import board8 from './../static/images/bgBoard/board8.png'
import board9 from './../static/images/bgBoard/board9.png'

/* import iconScroll from './../static/images/icons/icon_scroll.png';
import iconMenu from './../static/images/icon_menu.png' */

import {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult,
    setValues
} from '../actions/AccountActions';

class RandomContainer extends Component {
    actRanddomBoard(){
        this.apiRandomPaidBoard();
    }
    actSelectBoard(){
        if(this.state.board_key >= 0){
            this.apiAcceptBoard();
        }
    }
    genClassGacha(baseClass,index,gachaSelected,randomBoard=false){
        if(gachaSelected < 0) {
            return baseClass;
        } else {
            if(randomBoard) {
                if(gachaSelected === index) {
                    return baseClass + " " + baseClass + "--selected";
                } else {
                    return baseClass + " " + baseClass + "--rnd"+index;
                }
            } else {
                if(gachaSelected === index) {
                    return baseClass + " " + baseClass + "--active";
                } else {
                    return baseClass + " " + baseClass + "--inactive";
                }
            }
        }

    }
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiPaidBoardInfo() {
        this.setState({ showLoading: true });
        let dataSend = { type: "paid_board_info" };
        let successCallback = (data) => {
            let newCurrentStep = this.state.current_step;
            let newNextStep = parseInt(data.next_step);
            this.setState({
                permission: true,
                showLoading: false,
                board_key:  data.content.boards ? data.content.boards.board_key : -1,
                boards: data.content.boards||{},
                can_random_board: data.content.can_random_board,
            });
            if(data.content.boards) this.props.setValues({board_key:data.content.boards.board_key});
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_PAID_BOARD_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiRandomPaidBoard() {
        this.setState({ showLoading: true ,randomBoard: true, board_key: -1});
        let dataSend = { type: "random_paid_board" };
        let successCallback = (data) => {
            this.setState({
                permission: true,
                showLoading: false,
                showModalConfirmRandom: false,
                board_key: data.board_key,
            });
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_RANDOM_PAID_BOARD", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiAcceptBoard() {
        this.setState({ showLoading: true });
        let dataSend = { type: "accepted_board", id: this.state.board_key};
        let successCallback = (data) => {
            this.setState({
                permission: true,
                showLoading: false,
                randomBoard: false,
                showModalConfirmAccept: false,
                showModalMessage: data.message,
            });
            if(data.content.boards) this.props.setValues({board_key:data.board_key});
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_ACCEPT_BOARD", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    componentDidMount(){
        setTimeout(()=>{this.apiPaidBoardInfo()},1000);
    }
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: false,
            scrollDown: false,
            showModalMessage: "",
            showModalConfirmRandom: false,
            showModalConfirmAccept: false,

            boards: {},
            can_dice: false,
            can_random_board: false,
            character: "",
            username: "",

            board_key: -1,
            randomBoard: false,

            boardList: [
                { img: board1, name:"ชุดสง่างาม", select: true },
                { img: board2, name:"ชุดขาวบริสุทธิ์", select: false },
                { img: board3, name:"ผีเสื้อสีชาด", select: false },
                { img: board4, name:"ท่วงทำนองบริสุทธิ์", select: false },
                { img: board5, name:"ท่วงทำนองเสียงประสาน", select: false },
                { img: board6, name:"เทพพิทักษ์", select: false },
                { img: board7, name:"เซตกุหลาบพิษ", select: false },
                { img: board8, name:"เสือขาว", select: false },
                { img: board9, name:"เซตแสงที่เจิดจ้า", select: false }
            ]
        }
    }

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scrollDown === false) this.setState({ scrollDown: true });
        } else {
            if (this.state.scrollDown === true) this.setState({ scrollDown: false });
        }
    }
    // --rnd0infinite
    // --rnd0
    // --selected

    render() {
        let {randomBoard, board_key } = this.state;
        if(this.props.board_key > 0) return <Redirect to="/snakeladders/promotionevent"/>
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: 700, opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                            onScroll={this.handleScroll.bind(this)}
                        >
                            <div className="preorder">
                                <div className="page__wrapper page__wrapper--random">
                                    <Menu />
                                    <section className="preorder__section">

                                        <div className="container container--random">
                                            <div className="section__head section__head--random">
                                                <h3>สุ่มกระดาน</h3>
                                            </div>
                                            <div className="section__content">
                                                <div className="board">
                                                    {this.state.boardList.map((board, index) => {
                                                        return (
                                                            <img
                                                                key={index}
                                                                className={this.genClassGacha("board__img",index,board_key-1,randomBoard)}
                                                                src={board.img}
                                                            />
                                                        )
                                                    })}

                                                </div>
                                            </div>
                                            <div className="section__btn section__btn--random">
                                                {this.state.randomBoard ?
                                                    <a className="btn-default" onClick={()=>this.setState({showModalConfirmAccept: true})}>
                                                        <span>เลือกกระดาน</span>
                                                    </a>
                                                    :
                                                    <div className="btn-default inactive">
                                                        <span>เลือกกระดาน</span>
                                                    </div>
                                                }
                                                {this.state.can_random_board ?
                                                    <a className="btn-default" onClick={()=>this.setState({showModalConfirmRandom: true})}>
                                                        <span>สุ่ม (500 ไดมอนด์)</span>
                                                    </a>
                                                    :
                                                    <div className="btn-default inactive">
                                                        <span>สุ่ม (500 ไดมอนด์)</span>
                                                    </div>
                                                }
                                            </div>
                                        </div>

                                    </section>

                                </div>
                            </div>
                        </ScrollArea>

                    </div>
                    :
                    <Permission />
                }

                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={() => this.setState({ showModalMessage: "" })}
                    msg={this.state.showModalMessage}
                />
                <ModalConfirmRandom
                    open={this.state.showModalConfirmRandom}
                    actConfirm={this.actRanddomBoard.bind(this)}
                    actClose={()=>this.setState({showModalConfirmRandom: false})}
                />
                <ModalConfirmAcceptBoard
                    open={this.state.showModalConfirmAccept}
                    boardname={this.state.boardList[Math.max(0,this.state.board_key-1)].name}
                    actConfirm={this.actSelectBoard.bind(this)}
                    actClose={()=>this.setState({showModalConfirmAccept: false})}
                />
                <Loading
                    open={this.state.showLoading}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    board_key: state.AccountReducer.board_key,
    // permission: state.AccountReducer.permission,
    buy_soul_result: state.AccountReducer.buy_soul_result
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult,
    setValues
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(RandomContainer))
