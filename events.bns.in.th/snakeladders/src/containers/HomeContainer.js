import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from './../middlewares/Api';

import { Permission } from './../components/Permission';
import { Loading } from './../components/Loading';

import ModalMessage from './../components/ModalMessage';
import ModalSelectCharacter from './../components/ModalSelectCharacter';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';

import {
    onAccountLogout,
    setUsername,
    setSoulPermission,
    setValues
} from '../actions/AccountActions';

class HomeContainer extends Component {

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scollDown === false) this.setState({ scollDown: true });
        } else {
            if (this.state.scollDown === true) this.setState({ scollDown: false });
        }
    }
    actSelectCharacter(idCharacter,nameCharacter){
        this.props.setValues({username: nameCharacter})
        this.apiAcceptCharacter(idCharacter,nameCharacter);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiEventInfo() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "event_info" };
        let successCallback = (data) => {
            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            } else if (data.type == 'no_char') {
                this.setState({
                    showLoading: true,
                });
                this.apiGetCharcter();
            } else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_LOBBY_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiGetCharcter() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "get_characters" };
        let successCallback = (data) => {
            if (data.status) {
                this.setState({
                    showLoading: false,
                    showSelectCharacterModal: true,
                    characterList: data.content
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            } else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_GET_CHARACTER", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiAcceptCharacter(id,charname){
        let dataSend={
            type : "accept_character",
            id: id,
        };
        let successCallback = (data)=>{
            if (data.status) {
                this.setState({
                    showLoading: false,
                    showSelectCharacterModal: false,
                    showModalMessage: data.message,
                })
                this.props.setUsername(charname);
            }
        };
        let failCallback = (data)=>{
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });
            } else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_ACCEPT_CHARACTER",this.props.jwtToken,dataSend,successCallback,failCallback);
    };
    // ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,

            showSelectCharacterModal: false,
            characterList: [],
            showModalMessage: "",

        }
    }

    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 1000);

    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo();
            }, 1000);
        }
    }


    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: '700px', opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                        >
                            <div className="preorder">
                                <div class="preorder-inner">
                                    <section className="preorder__section preorder__section--header">
                                        <div className="header">
                                            <div className="header__scroll"><img src={iconScroll} alt="" /></div>
                                            <Link to="/snakeladders/soul/" className="btn btn-promotion">เข้าร่วมกิจกรรมและโปรโมชั่น</Link>
                                        </div>
                                    </section>
                                    <section className="preorder__section condition">
                                        <div className="container container--home">
                                            <div className="section__head">
                                                <h3>รายละเอียดกิจกรรมและโปรโมชั่น</h3>
                                            </div>
                                            <div className="section__content section__content--home">
                                                <h3>กิจกรรม</h3>
                                                <ul className="condition__text">
                                                    <li>ระยะเวลากิจกรรมเริ่ม 27 มีนาคม 2562 เวลา 12:00:00 น.</li>
                                                    <li>สิ้นสุด 17 เมษายน 2562 เวลา 05:59:59</li>
                                                    <li>ผู้เล่นสามารถทอยลูกเต๋าฟรีวันละ 1 ครั้ง</li>
                                                    <li>ผู้เล่นสามารถเลือกลงดันเจี้ยนเพื่อรับสิทธิ์เพิ่ม 1 สิทธิ์ในการทอยลูกเต๋า</li>
                                                    <li>รีเซ็ทสิทธิ์ทุกวันเวลา 06:00:00 น.</li>
                                                </ul>
                                                <br />
                                                <h3>โปรโมชั่น</h3>
                                                <ul className="condition__text">
                                                    <li>ระยะเวลาร่วมโปรโมชั่นเริ่ม 27 มีนาคม 2562 เวลา 12:00:00 น.</li>
                                                    <li>สิ้นสุด 17 เมษายน 2562 เวลา 05:59:59</li>
                                                    <li>ปิดระบบการแลกของ 23 เมษายน 2562 เวลา 23:59:59</li>
                                                    <li>ผู้เล่นสุ่มกระดานบันไดงูครั้งละ 500 ไดมอนด์ เพื่อเลือกกระดาน<br/>ที่ผู้เล่นอยากร่วมโปรโมชั่น</li>
                                                    <li>กระดานทั้งหมด 9 กระดาน คือ สง่างาม, ขาวบริสุทธิ์, ผีเสื้อสีชาด, เทพพิทักษ์,<br /> ท่วงทำนองบริสุทธิ์, ท่วงทำนองเสียงประสาน, กุหลาบพิษ, เสือขาว, แสงที่เจิดจ้า</li>
                                                    <li>ผู้เล่นเลือกกระดานบันไดงูได้แล้ว สามารถทอยลูกเต๋าครั้งละ 5000 ไดมอนด์</li>
                                                    <li>เมื่อทอยตกอยู่ช่องไหนจะได้รับของรางวัลช่องนั้น</li>
                                                    <li>ยกเว้นช่องที่ 10 20 30 40 50 60 70 80 90 100<br/>แค่เดินผ่านไม่จำเป็นต้องอยู่ช่องนั้น ก็สามารถได้รับของรางวัล</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </section>
                                    <div className="bg-bottom"></div>
                                </div>
                            </div>
                        </ScrollArea>
                    </div>
                    :
                    <Permission />
                }
                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
                <ModalSelectCharacter
                    open={this.state.showSelectCharacterModal}
                    charaterList={this.state.characterList}
                    actSelectCharacter={this.actSelectCharacter.bind(this)}
                />
                <Loading
                    open={this.state.showLoading}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    username: state.AccountReducer.username,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    permission: state.AccountReducer.permission
})

const mapDispatchToProps = {
    onAccountLogout, setUsername, setSoulPermission, setValues
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
