import React, {Component} from 'react';
import {Route} from 'react-router-dom';
// import OauthMiddleware from './../middlewares/OauthMiddleware';
import OauthMiddleware from './../features/oauthLogin';
import Modals from './../features/modals';

import {Home} from './../pages/Home';
import {HistoryPage} from './../pages/History'
import {HistoryItemPage} from './../pages/HistoryItem'

export class Web extends Component {
    render() {
        return (
            <>
                <Route path={process.env.REACT_APP_EVENT_PATH} component={OauthMiddleware}/>
                <Route path={process.env.REACT_APP_EVENT_PATH} component={Modals}/>
                <Route path={process.env.REACT_APP_EVENT_PATH} exact component={Home}/>
                <Route path={process.env.REACT_APP_EVENT_PATH+'/history'} exact component={HistoryPage}/>
                <Route path={process.env.REACT_APP_EVENT_PATH+'/history-item'} exact component={HistoryItemPage}/>      
            </>
        );
    }
}
