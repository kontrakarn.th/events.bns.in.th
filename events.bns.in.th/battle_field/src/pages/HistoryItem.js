import React from 'react';

import F11Layout from '../features/F11Layout';
import HistoryItem from '../features/historyItem';

export class HistoryItemPage extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
                showSidebar={false}
            >
                <HistoryItem/>
            </F11Layout>
        )
    }
}
