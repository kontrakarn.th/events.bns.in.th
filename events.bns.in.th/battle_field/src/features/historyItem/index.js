import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';
//import Logo from '../main/BNSLogo';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import {Imglist} from './../../constants/Import_Images';

class HistoryItem extends React.Component {

    apiGetHistory(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'getHistory'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open: '',
                    received: data.data.history.received,
                    redeem: data.data.history.redeem
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_HISTORY", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiGetHistory(this.props);
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            if (!this.props.uid) {
                this.apiGetHistory(this.props);
            }
        }

    }

    render() {
        return (
            <PageWrapper>
                {/* <Logo/> */}
                <Menu page='historyItem'/>
                <img src={Imglist.title_history_items}/>
                <HistroyFrame>
                    <ul className="list">
                        {
                            this.props.redeem !== '' && this.props.redeem.length > 0 && this.props.redeem.map((items,key)=>{
                                return(
                                    <li key={key}>
                                        <div>
                                            ได้รับ{items.item} x1
                                        </div>
                                        <div>
                                            {items.date}
                                        </div>
                                        <div>
                                            {items.time}
                                        </div>
                                    </li>
                            )})
                        }
                    </ul>
                </HistroyFrame>
                <ModalLoading />
                <ModalMessage />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(HistoryItem);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_other']}) #050506;
    width: 1105px;
    padding-top:70px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const HistroyFrame = styled.div`
    background: top center no-repeat url(${Imglist['history_section']});
    width:  850px;
    height: 550px;
    margin: 0 auto;
    box-sizing: border-box;
    padding: 40px;
    .list{
        width: 700x;
        height: 430px;
        margin: 0 auto;
        padding: 0;
        overflow: auto;
        overflow-x: hidden;
        color: #ffffff;
        >li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 0 2%;
            box-sizing: border-box;
            >div:first-child{
                width: 70%;
                text-align: left;
            }
            >div:nth-child(2){
                text-align: center;
                width: 15%;
            }
            >div:nth-child(3){
                text-align: center;
                width: 15%;
            }
           
        }
        &::-webkit-scrollbar {
            width: 8px;
        }
        &::-webkit-scrollbar-track {
          background: rgba(173,173,173,0.05);
        }
        &::-webkit-scrollbar-thumb {
            background: #000000;
        }
    }
`
