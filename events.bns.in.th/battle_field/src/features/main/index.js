import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import {Imglist} from './../../constants/Import_Images';
import ModalConfirmReceive from '../modals/ModalConfirmReceive';
import ModalSelectCharacter from "../modals/ModalSelectCharacter";
import ModalCostume from "../modals/ModalCostume";

import Menu from '../menu';

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div className={className} onClick={onClick}><img src={Imglist.btn_next}/></div>
    );
}
  
function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div className={className} onClick={onClick}><img src={Imglist.btn_prev}/></div>
    );
}

class Main extends React.Component {

    actConfirm(token){
        if (token === undefined || token === ""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไม่สามารถแลกของไอเทมได้",
            });
        }

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'redeem',token:token};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                    coin:data.data.coin,
                    itemLists:data.data.itemLists,
                });
                this.setState({
                    itemLists : data.data.itemLists,
                    questLists : data.data.questLists,
                })
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);

    }

    apiSelectCharacter(id){
        if (id === undefined || id === ""){
            return;
        }
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'select_character',id:id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selectd_char: data.data.selectd_char,
                    characters: data.data.characters,
                    coin:data.data.coin,
                    free:data.data.free,
                    itemLists:data.data.itemLists,
                    mission_score:data.data.mission_score,
                    modal_open: "",
                });
                /* this.setState({
                    itemLists : data.data.rewards,
                    questLists : data.data.quests,
                }) */
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_SELECT_CHAR", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiClaimFree(){

        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'claim_free'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    coin:data.data.coin,
                    free:data.data.free,
                    itemLists:data.data.itemLists,
                    modal_open: "",
                });
                /* this.setState({
                    itemLists : data.data.rewards,
                    questLists : data.data.quests,
                }) */
            }else{
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            this.props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        };
        apiPost("REACT_APP_API_POST_CLAIM_FREE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status === true){
                /* this.setState({
                    itemLists : data.data.rewards,
                    questLists : data.data.quests,
                }) */
                this.props.setValues({
                    username: data.data.username,
                    character_name: data.data.character_name,
                    selectd_char: data.data.selectd_char,
                    characters: data.data.characters,
                    coin:data.data.coin,
                    free:data.data.free,
                    itemLists:data.data.itemLists,
                    mission_score:data.data.mission_score,
                    modal_open: "",
                });

                if (data.data.selected_char === false){
                    this.props.setValues({ modal_open: "selectcharacter" });
                }
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,

            active: [],
            itemLists: [],
            questLists : [],
           
            slider_image: [
                {
                    image: Imglist.slide1,
                    title: 'ชุดราชันสมรภูมิ และ เครื่องประดับราชันสมรภูมิ'
                },
                {
                    image: Imglist.slide2,
                    title: 'ชุดผู้พิพากษา และ สัญลักษณ์ผู้พิพากษา'
                }
            ],

            exchange_unlimit: [
                {
                    image: Imglist.items1,
                    popup: ''
                },
                {
                    image: Imglist.items2,
                    popup: ''
                },
                {
                    image: Imglist.items3,
                    popup: Imglist.costume4
                },
                {
                    image: Imglist.items4,
                    popup: ''
                },
                {
                    image: Imglist.items5,
                    popup: ''
                },
                {
                    image: Imglist.items6,
                    popup: ''
                },
                {
                    image: Imglist.items7,
                    popup: ''
                },
                {
                    image: Imglist.items8,
                    popup: ''
                },
                {
                    image: Imglist.items9,
                    popup: ''
                },
                {
                    image: Imglist.items10,
                    popup: ''
                },
                {
                    image: Imglist.items11,
                    popup: ''
                },
                {
                    image: Imglist.items12,
                    popup: ''
                },
                {
                    image: Imglist.items13,
                    popup: ''
                },
                {
                    image: Imglist.items14,
                    popup: ''
                },
                {
                    image: Imglist.items15,
                    popup: ''
                },
                {
                    image: Imglist.items16,
                    popup: ''
                },
                {
                    image: Imglist.items17,
                    popup: ''
                },
                {
                    image: Imglist.items18,
                    popup: ''
                },
                {
                    image: Imglist.items19,
                    popup: ''
                },
                {
                    image: Imglist.items20,
                    popup: ''
                },
                {
                    image: Imglist.items21,
                    popup: ''
                },
                {
                    image: Imglist.items22,
                    popup: ''
                },
                {
                    image: Imglist.items23,
                    popup: ''
                },
                {
                    image: Imglist.items24,
                    popup: ''
                },
                {
                    image: Imglist.items25,
                    popup: ''
                },
                {
                    image: Imglist.items26,
                    popup: ''
                },
                {
                    image: Imglist.items27,
                    popup: ''
                },
                {
                    image: Imglist.items28,
                    popup: ''
                },
                {
                    image: Imglist.items29,
                    popup: ''
                },
                {
                    image: Imglist.items30,
                    popup: ''
                },
                {
                    image: Imglist.items31,
                    popup: ''
                },
                {
                    image: Imglist.items32,
                    popup: ''
                },
                {
                    image: Imglist.items33,
                    popup: ''
                },
                {
                    image: Imglist.items34,
                    popup: ''
                },
                {
                    image: Imglist.items35,
                    popup: ''
                },
                {
                    image: Imglist.items36,
                    popup: ''
                },
                {
                    image: Imglist.items37,
                    popup: ''
                },
            ],
            mission1: [
                {
                    image: Imglist.mission1_1,
                    score: '200'
                },
                {
                    image: Imglist.mission1_2,
                    score: '400'
                },
                {
                    image: Imglist.mission1_3,
                    score: '1100'
                },
            ],
            mission2: [
                {
                    image: Imglist.mission2_1,
                    score: '200'
                },
                {
                    image: Imglist.mission2_2,
                    score: '400'
                },
                {
                    image: Imglist.mission2_3,
                    score: '1100'
                },
            ],
            mission3: [
                {
                    image: Imglist.mission3_1,
                    score: '200'
                },
                {
                    image: Imglist.mission3_2,
                    score: '400'
                },
                {
                    image: Imglist.mission3_3,
                    score: '1100'
                },
            ],
            exchange: [
                {
                    items: Imglist.content3_item1,
                    popup: Imglist.costume1,
                    items_name: '',
                },
                {
                    items: Imglist.content3_item2,
                    popup: Imglist.costume2,
                    items_name: '',
                },
                {
                    items: Imglist.content3_item3,
                    popup: Imglist.costume3,
                    items_name: '',
                },
            ],
            exchange_limit: [
                {
                    items: Imglist.exchange_limit_item1,
                    score: '1111'
                },
                {
                    items: Imglist.exchange_limit_item2,
                    score: '200'
                },
                {
                    items: Imglist.exchange_limit_item3,
                    score: '40000'
                },
                {
                    items: Imglist.exchange_limit_item4,
                    score: '5000'
                },
                {
                    items: Imglist.exchange_limit_item5,
                    score: '8999'
                },
            ],
            item_per_event: [
                {
                    items: Imglist.reward,
                }
            ]
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }

    render() {
        const settings = {
            dots: true,
            infinite: true,
            fade: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        return (
            <PageWrapper>
                <Menu page="home"/>
                <Content>
                    {/* section 1 */}
                    <div className="section section1">
                        <img src={Imglist.content1}/>
                       
                        {/* <div className="quest text-center">
                            <div className="quest__title text-yellow">รายละเอียดเควสต์ และ การสะสมเหรียญกิจกรรม</div>

                            {
                                this.props.quests && this.props.quests.length > 0 && this.props.quests.map((items,key)=>{
                                    return(  
                                        <div key={key} className="questItems">
                                            <img src={items.image}/>
                                            <div className="questItems__points">{items.completed_count}</div>
                                        </div>
                                    )
                                })
                            } 
                        </div> */}
                         
                    </div>

                    {/* section 2 */}
                    <div className="section section2">
                        <div>
                            <img src={Imglist.content2_title}/>
                        </div>
                        {
                            this.props.free == "disabled" ?
                                <>
                                    <div className="btn_daily disabled"></div>
                                </>
                                :
                                this.props.free == "received" ?
                                    <>
                                        <div className="btn_daily received"></div>
                                    </>
                                    :
                                    <>
                                        <div className="btn_daily" onClick={()=>this.apiClaimFree()}></div>
                                    </>
                        }

                        {/* item.received == true ?
                            <div className="btn_daily disabled">รับไปแล้ว</div>
                        :
                            (
                                item.pending == true ?
                                    <div className="btn_daily" onClick={()=>this.actConfirm(1)}></div>
                                :
                                    <div className="btn_daily received"></div>
                            ) */}

                      
                        <img src={Imglist.content2_text}/>

               
                        <div className="mission1 score_wrapper">

                            <div className="score_box">
                            {
                                this.state.mission1.map((item, key)=>{
                                return(
                                    <div className="score_col">
                                        <img src={item.image}/>
                                        <div className="score_inner">
                                            <div>คะแนนที่ได้รับในวันนี้</div>
                                            <div className="score">{this.props.mission_score.mission1 && this.props.mission_score.mission1[key]}</div>
                                        </div>
                                    </div>
                                    )
                                })
                            }
                            </div>
                        </div>      
                     

                 
                        <div className="mission2 score_wrapper">
                            <div className="score_box">
                            {
                                this.state.mission2.map((item, key)=>{
                                return(
                                    <div className="score_col">
                                        <img src={item.image}/>
                                        <div className="score_inner">
                                            <div>คะแนนที่ได้รับในวันนี้</div>
                                            <div className="score">{this.props.mission_score.mission2 && this.props.mission_score.mission2[key]}</div>
                                        </div>
                                    </div>
                                    )
                                })
                            }
                            </div>
                        </div>      
                  

                    
                        <div className="mission3 score_wrapper">
                            <div className="score_box">
                            {
                                this.state.mission3.map((item, key)=>{
                                return(
                                    <div className="score_col">
                                        <img src={item.image}/>
                                        <div className="score_inner">
                                            <div>คะแนนที่ได้รับในวันนี้</div>
                                            <div className="score">{this.props.mission_score.mission3 && this.props.mission_score.mission3[key]}</div>
                                        </div>
                                    </div>
                                    )
                                })
                            }
                            </div>
                        </div>      
                       


                        {/* {
                            this.props.rewards && this.props.rewards.length > 0 && this.props.rewards.map((items,key)=>{
                                return(  
                                    <div key={key} className="items">
                                        <div className="items__img">
                                            <img src={items.image}/>
                                        </div>
                                        <div className="items__name">{items.title}</div>
                                        {
                                            items.can_exchange === false ?
                                                <>
                                                    <div className={"btn btn-exchange inactive"}>แลกไอเทม</div>
                                                </>
                                                :
                                                <>
                                                    <div className={"btn btn-exchange"} onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการแลกไอเทม '+items.title + ' ?',exchange_key: items.id})}>แลกไอเทม</div>
                                                </>
                                        }
                                        <small>ใช้ {items.required_points} เหรียญ</small> <br/>
                                        {
                                            items.limit && parseInt(items.limit) !== 0 ?
                                                <small className="limit">จำกัด {items.limit} ชิ้น</small>
                                            : null
                                        }
                                    </div>
                                )
                            })
                        }        */}
                    </div>

                    {/* section 3 */}
                    <div className="section section3">
                        <div>
                            <img src={Imglist.content3_title}/>
                        </div>

                        {/* รางวัลที่แลกเปลี่ยนได้  */}
                        <div className="exchange_wrapper">
                            {
                                this.state.exchange.map((item, key)=>{
                                return(
                                    <div className="">
                                        <img src={item.items}/>
                                        <div className="icon_plus" onClick={()=>this.props.setValues({modal_open:'costume', modal_image: item.popup})}><img src={Imglist.icon_plus}/></div>
                                        {
                                            this.props.itemLists.new_items[key] && this.props.itemLists.new_items[key].canRedeem === true ?
                                                <>
                                                    <div className="btn_exchange" onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการแลก<br/>'+ this.props.itemLists.new_items[key].name +'?',modal_item_token: this.props.itemLists.new_items[key].token})}></div>
                                                </>
                                                :
                                                <>
                                                    <div className="btn_exchange disabled"></div>
                                                </>
                                        }
                                    </div>
                                    )
                                })
                            }
                        </div>

                        {/* รางวัลที่แลกเปลี่ยนได้  จำกัดจำนวนครั้ง/กิจกรรม */}
                        <div>
                            <div className="header">รางวัลที่แลกเปลี่ยนได้ <span>จำกัดจำนวนครั้ง/กิจกรรม</span></div>
                            {
                                this.state.item_per_event.map((item, key)=>{
                                return(
                                    <div className="exchange_limit_event">
                                        <img src={item.items}/> 
                                        <div className="details">
                                            <div className="score_inner">
                                                <div>จำนวนครั้ง</div>
                                                <div className="score">{this.props.itemLists.item_per_event[key].limit}</div> 
                                            </div>
                                            {
                                                this.props.itemLists.item_per_event[key] && this.props.itemLists.item_per_event[key].canRedeem === true ?
                                                    <>
                                                        <div className="btn_exchange" onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการแลก<br/>' + this.props.itemLists.item_per_event[key].name+'?',modal_item_token: this.props.itemLists.item_per_event[key].token})}></div>
                                                    </>
                                                    :
                                                    <>
                                                        <div className="btn_exchange disabled"></div>
                                                    </>
                                            }
                                        </div>
                                    </div>
                                    )
                                })
                            }  
                        </div>

                        <div className="header">รางวัลที่แลกเปลี่ยนได้ <span>จำกัดจำนวนครั้ง/วัน</span> (รีเซ็ตเวลา 23:59:59 น. ของทุกวัน)</div>
                        <div className="exchange_limit_wrapper">
                            {
                                this.state.exchange_limit.map((item, key)=>{
                                return(
                                    <div>
                                        <img src={item.items}/>
                                        <div className="details">
                                            <div className="score_inner">
                                                <div>จำนวนครั้ง</div>
                                                <div className="score">{this.props.itemLists.item_per_day[key].limit}</div>
                                            </div>
                                            {
                                                this.props.itemLists.item_per_day[key] && this.props.itemLists.item_per_day[key].canRedeem === true ?
                                                    <>
                                                        <div className="btn_exchange" onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการแลก<br/>' + this.props.itemLists.item_per_day[key].name+'?',modal_item_token: this.props.itemLists.item_per_day[key].token})}></div>
                                                    </>
                                                    :
                                                    <>
                                                        <div className="btn_exchange disabled"></div>
                                                    </>
                                            }
                                        </div> 
                                    </div>
                                    )
                                })
                            }
                        </div>


                        <div className="header">รางวัลที่แลกเปลี่ยนได้ <span>ไม่จำกัดจำนวนครั้ง</span></div>
                        <div className="exchange_unlimit_wrapper">
                            {
                                this.state.exchange_unlimit.map((item, key)=>{
                                return(
                                    <div className="">
                                        <img src={item.image}/> 
                                        {
                                            item.popup !== '' &&
                                                <div className="icon_plus" onClick={()=>this.props.setValues({modal_open:'costume', modal_image: item.popup})}><img src={Imglist.icon_plus}/></div>
                                        }
                                        {
                                            this.props.itemLists.item_unlimited[key] && this.props.itemLists.item_unlimited[key].canRedeem === true ?
                                                <>
                                                    <div className="btn_exchange" onClick={()=>this.props.setValues({modal_open:'confirm',modal_message: 'ต้องการแลก<br/>' + this.props.itemLists.item_unlimited[key].name+'?',modal_item_token: this.props.itemLists.item_unlimited[key].token})}></div>
                                                </>
                                                :
                                                <>
                                                    <div className="btn_exchange disabled"></div>
                                                </>
                                        }
                                    </div>
                                    )
                                })
                            }
                        </div>

                        
                    </div>
              
                </Content>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                <ModalConfirmReceive actConfirm={this.actConfirm.bind(this)}/>
                <ModalSelectCharacter actSelectCharacter={this.apiSelectCharacter.bind(this)}/>
                <ModalCostume/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #11122d;
    width: 1105px;
    padding-top: 740px;
    text-align: center;
    position: relative;
    ::after{
        content: '';
        background: url(${Imglist['character']}) top center  no-repeat ; 
        width: 500px;
        height: 398px;            
        position: absolute;
        right: 0;
        bottom: 0;
        z-index: 2;

    }
`

const Content = styled.div `  
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }
    & .text-yellow{
        color: #f2ff67;
    }
    & .text-center{
        text-align: center;
    }
    & .section{
        color:#fff;
        padding-top: 6%;
        padding-bottom: 6%;
        &__header{
            font-size: 20px;
            font-weight: bold;
            margin-bottom: 30px;
            text-align: center;
        }
        &__step1{
            text-align:center;
            padding-top: 3%;
        }
        &__step2{
            text-align: center;
            padding-top: 4%;
        }
    }
    & small{
        font-size: 80%;
        line-height: 1;
    }
    & .limit{
        color: #e4e0e0;
    }
    & .section1{
        img{
            margin: 0 auto;
            display: block;
        }
       
        & ol{
            padding-left: 15px;
        }
    }
    & .section2{
        position: relative;
        text-align:center;
        .btn_daily{
            background: url(${Imglist['btn_daily']}) no-repeat top center; 
            width:277px;
            height: 58px;
            cursor:pointer;
            margin: 0 auto;
            :hover{
                background-position: 0 -58px;
            }
            &.disabled{
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);  
                cursor:auto; 
            }
            &.received{
                background-position: bottom center;
                cursor:auto;
            }
        }
        .score_inner{
            background: url(${Imglist['score']}) top center / cover  no-repeat ;
            padding: 5px; 
            font-size: 14px;
            color: #8588a5;
            width: 90%;
            margin: 0 auto;
            .score{
                color: #72c8ff;
                font-size: 16px;
            }
        }
        .score_box{
            display: flex;
            justify-content: center;
            padding: 40px 60px;
            align-items: center;
            height: 100%;
        }
        .score_col{
            padding: 0 40px;
            flex: 1; 
        }
        .mission1{
            margin: 0 auto 15px;
            background: url(${Imglist['mission1_bg']}) top center / 100% 100% no-repeat ;
            width: 920px;
            height: 385px;
        }
        .mission2{
            margin: 0 auto 15px;
            background: url(${Imglist['mission2_bg']}) top center / 100% 100% no-repeat;
            width: 920px;
            height: 385px;
        }
        .mission3{
            margin: 0 auto 15px;
            background: url(${Imglist['mission3_bg']}) top center / 100% 100% no-repeat;
            width: 920px;
            height: 385px;
        }
        
 
    }
    & .items{
        width: 20%;
        display:inline-block;
        vertical-align: top;
        font-size: 14px;
        padding: 15px 10px;
        color: #939393;
        text-align: center;
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%); 
            &.active{
                -webkit-filter: none;
                filter: none;  
            }
        }
        &__img{
            position:relative;  
        }
        &__name{
            min-height: 35px;
            line-height: 1.2;
            margin: 5px 0;
        }
    }
    & .section3{
        color: #fff;
        text-align: center;
        padding-bottom: 14%;
        z-index: 1;
        position: relative;
        & .slick-slide img{
            margin: 0 auto;
        }
        & .slick-dots li.slick-active button:before {
            opacity: 1;
            color: #fff;
        }
        & .slick-dots li button:before{
            font-size: 11px;
            opacity: 1;
            color: #646464;
        }
        & .slick-prev{
            left:120px;
        }
        & .slick-next{
            right:120px;
        }
        & .slick-prev,
        & .slick-next{
            width: auto;
            height: auto;
            z-index: 1;
            &::before{
                font-size:0;
            }
        }

        .score_inner{
            background: url(${Imglist['score']}) top center / cover  no-repeat ;
            padding: 5px; 
            font-size: 12px;
            color: #8588a5;
            width: 90px;
            margin: 0 auto 10px;
            .score{
                color: #72c8ff;
                font-size: 16px;
            }
        }
        .details{
            position: absolute;
            left: 0;
            right: 0;
            margin: 0 auto;
            bottom: 30px;
        }
        .exchange_wrapper{       
            > div{
               display: inline-block;
               padding: 0 10px;
               position: relative;
               max-width: 33.33%;
            }
            .btn_exchange{ 
                position: absolute;
                left: 0;
                right: 0;
                bottom: 30px;
            }
        }
        .icon_plus{
            position: absolute;
            right: 90px;
            top: 60px;
            cursor:pointer;
        }
        .header{
            margin: 40px 0 20px 0;
            > span{
                color: #ffee90;
            }
        }
        .exchange_limit_event{
            position: relative;
        }
        .exchange_limit_wrapper{
            > div{
                display: inline-block;
                padding: 0 8px;
                position: relative;
            }
        }
        .exchange_unlimit_wrapper{
            background: url(${Imglist['exchange_unlimit_bg']}) top center /100% 100%  no-repeat ; 
            width: 895px;
            margin: 0 auto;
            padding: 40px 60px;
            text-align: left;
            >div{
                display: inline-block;
                width: 25%;
                padding: 0 15px 30px;
                position: relative;
            }
           .icon_plus{
            right: 35px;
            top: 35px;
           }
        }
    }
    .btn_exchange{
        background: url(${Imglist['btn_exchange']}) no-repeat top center; 
        width:99px;
        height: 40px;
        cursor:pointer;
        margin: 0 auto;
        :hover{
            background-position: 0 -40px;
        }
        &.disabled{
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);   
        }
        &.inactive{
            background-position: bottom center;
            cursor:auto;
        }
    }
    

`

