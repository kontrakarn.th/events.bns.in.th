const bg 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/background.jpg';
const icon_menu 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/icon_menu.png'
const menu_bg			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/menu_bg.png'

const modal_bg 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/modal_bg.png'
const btn_default 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/btn_default.png'

const content1 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/content1.png'
const content2 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/content2.png'
const content3 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/content3.png'
const btn_prev 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/btn_prev.png'
const btn_next 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/btn_next.png'
const sidebar_bg 		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/sidebar_bg.png'
const remark 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/remark.png'
const icon_dropdown 	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/icon_dropdown.png'


const exchange_unlimit_bg = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/bg.png'
const character		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/character.png'
const items1 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/1.png'
const items2 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/2.png'
const items3 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/3.png'
const items4 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/4.png'
const items5 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/5.png'
const items6 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/6.png'
const items7 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/7.png'
const items8 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/8.png'
const items9 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/9.png'
const items10 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/10.png'
const items11 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/11.png'
const items12 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/12.png'
const items13 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/13.png'
const items14 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/14.png'
const items15 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/15.png'
const items16 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/16.png'
const items17 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/17.png'
const items18 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/18.png'
const items19 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/19.png'
const items20 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/20.png'
const items21 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/21.png'
const items22 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/22.png'
const items23 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/23.png'
const items24 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/24.png'
const items25 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/25.png'
const items26 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/26.png'
const items27 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/27.png'
const items28 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/28.png'
const items29 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/29.png'
const items30 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/30.png'
const items31 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/31.png'
const items32 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/32.png'
const items33 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/33.png'
const items34 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/34.png'
const items35 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/35.png'
const items36 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/36.png'
const items37 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_unlimit/37.png'

const content2_title	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/content2_title.png'
const content2_text	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/content2_text.png'
const btn_daily		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/btn_daily.png'

const score			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/score.png'
const content3_title	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/content3_title.png'
const content3_item1	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/content3_item1.png'
const content3_item2	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/content3_item2.png'
const content3_item3	= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/content3_item3.png'
const btn_exchange		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/btn_exchange.png'
const icon_plus		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/icon_plus.png'
const reward			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/reward.png'
const exchange_limit_item1		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_limit_item1.png'
const exchange_limit_item2		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_limit_item2.png'
const exchange_limit_item3		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_limit_item3.png'
const exchange_limit_item4		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_limit_item4.png'
const exchange_limit_item5		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/exchange_limit_item5.png'
const bg_other					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/bg_other.jpg'
const title_history_items		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/title_history_items.png'
const title_history_score		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/title_history_score.png'
const history_section			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/history_section.png'

const mission1_bg				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/mission1/bg.png'
const mission2_bg				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/mission2/bg.png'
const mission1_1				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/mission1/1.png'
const mission1_2				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/mission1/2.png'
const mission1_3				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/mission1/3.png'
const mission2_1				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/mission2/1.png'
const mission2_2				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/mission2/2.png'
const mission2_3				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/mission2/3.png'
const mission3_1				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/mission3/1.png'
const mission3_2				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/mission3/2.png'
const mission3_3				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/mission3/3.png'
const mission3_bg				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/mission3/bg.png'
const modal_character_bg		= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/modal_character_bg.png'
const costume1					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/costume/1.png'
const costume2					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/costume/2.png'
const costume3					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/costume/3.png'
const costume4					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/costume/4.png'
const modal_close				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/modal_close.png'

//https://cdngarenanow-a.akamaihd.net/webth/bns/events/battle_field/

export const Imglist = {
	bg,
	icon_menu,
	menu_bg,
	modal_bg,
	btn_default,
	content1,
	content2,
	content3,
	btn_prev,
	btn_next,
	sidebar_bg,
	remark,
	icon_dropdown,

	exchange_unlimit_bg,
	character,
	items1 ,
	items2 ,
	items3 ,
	items4 ,
	items5 ,
	items6 ,
	items7 ,
	items8 ,
	items9 ,
	items10,
	items11,
	items12,
	items13,
	items14,
	items15,
	items16,
	items17,
	items18,
	items19,
	items20,
	items21,
	items22,
	items23,
	items24,
	items25,
	items26,
	items27,
	items28,
	items29,
	items30,
	items31,
	items32,
	items33,
	items34,
	items35,
	items36,
	items37,


	content2_title,
	content2_text,
	btn_daily,
	score,
	content3_title,
	content3_item1,
	content3_item2,
	content3_item3,
	btn_exchange,
	icon_plus,
	reward,
	exchange_limit_item1,
	exchange_limit_item2,
	exchange_limit_item3,
	exchange_limit_item4,
	exchange_limit_item5,
	bg_other,
	title_history_items,
	title_history_score,
	history_section,

	mission1_1,
	mission1_2,
	mission1_3,
	mission1_bg,
	mission2_1,
	mission2_2,
	mission2_3,
	mission2_bg,
	mission3_1,
	mission3_2,
	mission3_3,
	mission3_bg,
	modal_character_bg,
	costume1,
	costume2,
	costume3,
	costume4,
	modal_close,
}
