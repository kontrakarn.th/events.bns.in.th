import React from 'react';
import ModalCore from './ModalCore.js';
import { connect } from 'react-redux';
import { setValues, actCloseModal } from './redux';
import styled from 'styled-components';
import { Imglist } from '../../constants/Import_Images';

const MessageModal = (props) => {
    return (
        <ModalCore name='message' outSideClick={false}>
            <ModalcontentStyle>
                <div
                    className='message'
                    dangerouslySetInnerHTML={{ __html: props.modal_message }}
                />
                <div className='buttons'>
                    <a
                        className='btn btn--confirm'
                        onClick={() => props.setValues({ modal_open: '' })}
                    />
                </div>
            </ModalcontentStyle>
            {/* <div className='contentwrap contentwrap--board'>
                <div className='inner inner--boardmaxwidth'>
                    <div
                        className='text text--text'
                        dangerouslySetInnerHTML={{
                            __html: props.modal_message,
                        }}
                    ></div>
                    <div
                        className='btn__ok'
                        onClick={() => props.setValues({ modal_open: '' })}
                    ></div>
                </div>
            </div> */}
        </ModalCore>
    );
};

const mapStateToProps = (state) => ({
    ...state.Modal,
});

const mapDispatchToProps = { setValues, actCloseModal };

export default connect(mapStateToProps, mapDispatchToProps)(MessageModal);

// modal_message
const ModalcontentStyle = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    width: 95%;
    max-width: 470px;
    max-height: calc(600px * (513 / 914));
    height: calc(95vw * (513 / 914));
    /* padding: 0px 0px 24px; */
    background: url(${Imglist.modal_message}) top center no-repeat;
    justify-content: space-evenly;

    @media (max-width: 640px) {
        background-size: cover;
        max-height: unset;
        width: 100%;
        max-width: unset;
        height: 72vw;
    }

    .message {
        color: #fff;
        font-size: 20px;
        display: block;
        display: block;
        text-align: center;
        font-family: 'Kanit-Light', Tahoma;
        line-height: 1.2;
        padding-top: 15%;
    }
    .buttons {
        pointer-events: auto;
        /* position: absolute;
        left: 50%;
        bottom: 67px;
        display: flex;
        width: 202px;
        transform: translate(-50%, 0px);
        text-align: center;
        justify-content: center;
        align-items: center; */
    }
    .btn {
        display: inline-block;
        width: 138px;
        height: 41px;
        background-position: top center;
        cursor: pointer;
        &--confirm {
            background-image: url(${Imglist.btn_confirm});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;
