import React, { Component } from 'react';
import { connect } from 'react-redux';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import styled from 'styled-components';

import { Imglist } from '../../constants/Import_Images';

import { apiPost } from './../../middlewares/Api';

import { onAccountLogout } from './../../actions/AccountActions';

import { setValues } from './redux';

import { MessageModal, LoadingModal } from '../../features/modal';
import iconScroll from './../../static/images/icons/icon_scroll.png';
import btnReceive from './../../static/images/btn_receive.png';
import btn_enter from './../../static/images/btn_enter.png';
import btn_patch from './../../static/images/btn_patch.png';
import btn_login from './../../static/images/btn_login.png';
import btn_logout from './../../static/images/btn_logout.png';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            registered: false,
        };
    }

    ////////////   API   ////////////

    apiEventInfo() {
        this.props.setValues({
            modal_open: 'loading',
        });
        let dataSend = { type: 'event_info' };
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.data,
                    modal_open: '',
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_message: data.message,
                    modal_open: 'message_nochar',
                });
            } else if (data.type === 'no_game_info') {
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: 'message_nochar',
                });
            } else {
                this.props.setValues({
                    modal_message: data.message,
                    modal_open: 'message',
                });
            }
        };
        apiPost(
            'REACT_APP_API_POST_EVENT_INFO',
            this.props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );
    }

    getItem = () => {
        let dataSend = { type: '' };
        let successCallback = (res) => {
            if (res.status) {
                this.props.setValues({
                    ...res.data,
                    modal_open: 'message',
                    modal_message: res.message,
                });
            } else {
                this.props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                });
            } else {
                this.props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        apiPost(
            'REACT_APP_API_POST_GET_ITEM',
            this.props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );
    };

    ////////////   API   ////////////

    componentDidMount() {
        if (this.props.jwtToken !== '')
            setTimeout(() => this.apiEventInfo(), 1000);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo();
            }, 1000);
        }
    }

    handleReceived = () => {
        this.getItem();
    };

    onLogout() {
        this.props.onAccountLogout();
    }

    render() {
        // console.log('already_purchase', this.props.already_purchase)
        return (
            <div className='events-bns'>
                <NewBie>
                    <div className='newbie__btngroup'>
                        {this.props.jwtToken && this.props.jwtToken !== '' ? (
                            <React.Fragment>
                                {/* <a onClick={this.onLogout.bind(this)}>
                                    <img src={btn_logout} />
                                </a> */}
                                <div className='newbie__btngroup--username'>
                                    {this.props.username}
                                </div>
                                <div
                                    onClick={this.onLogout.bind(this)}
                                    className='newbie__btngroup--username'
                                >
                                    logout
                                </div>
                            </React.Fragment>
                        ) : (
                            <a href={this.props.loginUrl}>
                                <img src={btn_login} />
                            </a>
                        )}
                        {/* <a href="">
                            <img src={btn_patch} alt=''/>
                        </a> */}
                    </div>
                    {/* <div className='newbie__scroll'>
                        <img src={iconScroll} alt='' />
                    </div> */}

                    <div className='newbie__board-buy'>
                        {this.props.jwtToken && this.props.jwtToken !== '' ? (
                            this.props.can_get_package === 1 ? (
                                <div
                                    onClick={this.handleReceived}
                                    className='newbie__btn_receive'
                                ></div>
                            ) : (
                                <div className='newbie__btn_received'></div>
                            )
                        ) : (
                            <div className='newbie__btn--inactive'></div>
                        )}
                    </div>
                </NewBie>
                <LoadingModal />
                <MessageModal />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    ...state.layout,
    ...state.AccountReducer,
    ...state.EventReducer,
    ...state.Modal,
});

const mapDispatchToProps = {
    onAccountLogout,
    setValues,
};
export default connect(mapStateToProps, mapDispatchToProps)(Main);

const NewBie = styled.div`
    background: url(${Imglist.bg}) top center no-repeat #5f3e1c;
    background-size: cover;
    padding: 980px 0 2%;
    max-width: 1920px;
    margin: 0 auto;

    @media (max-width: 1060px) {
        padding: 89% 0 2%;
        max-width: 1920px;
    }

    @media (max-width: 640px) {
        background: url(${Imglist.bg_mb}) top center no-repeat;
        background-size: 100%;
        padding: 207% 0 0;
        max-width: 1920px;
    }

    .newbie__btn_received {
        background: url(${Imglist.btn_received}) top center no-repeat;
        width: 31%;
        height: 9.3%;
        position: absolute;
        top: 35%;
        pointer-events: none;
        background-size: 100%;
        transform: translateX(-7px);

        @media (max-width: 640px) {
            width: 65%;
            top: 32%;
            height: 7.8%;
            transform: translateX(0);
        }
    }

    .newbie__btn_receive {
        background: url(${Imglist.btn_receive}) top center no-repeat;
        width: 31%;
        height: 9.3%;
        cursor: pointer;
        position: absolute;
        top: 35%;
        background-size: 100%;
        transform: translateX(-7px);

        &:hover {
            background-position: bottom center;
        }

        @media (max-width: 640px) {
            width: 65%;
            top: 32%;
            height: 7.8%;
            transform: translateX(0);
        }
    }

    .newbie__btn--inactive {
        background: url(${Imglist.btn_receive_inactive}) top center no-repeat;
        width: 31%;
        height: 9.3%;
        cursor: pointer;
        position: absolute;
        top: 35%;
        background-size: 100%;
        transform: translateX(-7px);
        pointer-events: none;

        @media (max-width: 640px) {
            width: 65%;
            top: 32%;
            height: 7.8%;
            transform: translateX(0);
        }

        /* @media (min-width: 1320px) {
            top: calc(50% * (1924  /  2034));
        } */
    }

    .newbie__btngroup {
        position: absolute;
        top: 2vw;
        /* right: 2%; */
        width: 100%;
        max-width: 1920px;
        text-align: right;
        display: flex;
        justify-content: flex-end;

        &--username {
            text-align: center;
            line-height: 45px;
            width: 84px;
            height: 45px;
            color: #fff;
            background-color: #000;
            border-radius: 15px;
            -webkit-border-radius: 15px;
            -moz-border-radius: 15px;
            -ms-border-radius: 15px;
            -o-border-radius: 15px;

            &:first-child {
                padding: 0 1.5%;
            }
        }

        > a,
        > div {
            width: 5%;
            display: inline-block;
            margin: 0 0.5%;
            min-width: 80px;
            cursor: pointer;

            @media (max-width: 640px) {
                min-width: 75px;
            }
        }

        img {
            display: block;
        }
    }

    &__scroll {
        text-align: center;
        width: 100%;
        position: absolute;
        top: 860px;

        > img {
            animation: mover 1s infinite alternate;
        }

        @media (max-width: 1060px) {
            top: 78vw;
        }

        @media (max-width: 640px) {
            top: 129vw;
        }
    }

    .newbie__board-buy {
        position: relative;
        display: flex;
        justify-content: center;
        width: 95%;
        max-width: 1060px;
        max-height: calc(1060px * (970 / 1050));
        height: calc(95vw * (970 / 1050));
        margin: 5% auto 0;
        position: relative;

        @media (max-width: 640px) {
            width: 100%;
            height: calc(100vw * (1503 / 640));
            max-height: unset;
            margin: -12% auto 0;
        }

        /* @media (min-width: 1320px) {
            width: 100%;
            height: calc(100vw * (1924  /  2034));
            max-height: unset;
            margin: -12% auto 0;
        } */
    }
`;
