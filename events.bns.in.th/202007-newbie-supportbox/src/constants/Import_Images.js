import iconHome from '../static/images/buttons/btn_home.png';
import iconScroll from '../static/images/icons/icon_scroll.png';

//home
const bg =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/newbie_free_support_package/bg.png';
const bg_mb =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/newbie_free_support_package/bg_mb.png';
const btn_received =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/newbie_free_support_package/btn_received.png';
const btn_receive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/newbie_free_support_package/btn_receive.png';
const btn_receive_inactive =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/newbie_free_support_package/btn_receive_inactive.png';
const modal_message =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/newbie_free_support_package/modal_message.png';
const btn_confirm =
    'https://cdngarenanow-a.akamaihd.net/webth/bns/events/newbie_free_support_package/btn_confirm.png';

export const Imglist = {
    iconHome,
    iconScroll,

    bg,
    bg_mb,
    btn_received,
    btn_receive,
    btn_receive_inactive,
    modal_message,
    btn_confirm,
};
