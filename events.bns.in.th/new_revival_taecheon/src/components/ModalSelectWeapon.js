import React, { Component } from 'react'
import iconDropdown from './../static/images/icon_dropdown.png'
export default class ModalSelectWeapon extends Component {

    actSelectWeapon(e){
        // e.preventDefault();
        // let indexCharacter = document.getElementById("charater_form").value;
        // let idCharacter = this.props.listCharater[indexCharacter].id;
        // let nameCharacter = this.props.listCharater[indexCharacter].name;
        // this.props.actSelectCharacter(idCharacter,nameCharacter)
    }
    render() {
        return (
            <div className={"modal "+(this.props.open ? "":"close")} >
                <div className="modal__backdrop" />
                <div className={"modal__content"}>
                    <div className={"modal__incontent modal__incontent--selectcharacter"}>
                        เลือกอาวุธ<br />
                        <form className="formselect">
                            <label >
                                <select id="charater_form" className="fromselect__list">
                                    {this.props.open && this.props.listCharater && this.props.listCharater.map((item,index)=>{
                                        return (
                                            <option key={index} value={index}>{item.name}</option>
                                        )
                                    })}
                                </select>
                                <img src={iconDropdown} alt="" />
                            </label>
                        </form>
                    </div>
                    <div className={"modal__bottom"}>
                        <a
                            onClick={this.actSelectWeapon.bind(this)}
                            className="modal__button"
                        >
                            ตกลง
                        </a>
                        <a className="modal__button" onClick={this.props.closeModal}>
                            ยกเลิก
                        </a>
                    </div>
                </div>
            </div>
        )
  }
}
