import React, { Component } from 'react'
import iconDropdown from './../static/images/icon_dropdown.png'
export default class ModalConfirmRedeem extends Component {

    actSelectWeapon(e) {
        // e.preventDefault();
        // let indexCharacter = document.getElementById("charater_form").value;
        // let idCharacter = this.props.listCharater[indexCharacter].id;
        // let nameCharacter = this.props.listCharater[indexCharacter].name;
        // this.props.actSelectCharacter(idCharacter,nameCharacter)
    }

    getTextHeader(type_id) {
        if (type_id == 1) {
            return 'ของขวัญต้อนรับผู้ฝึกยุทธหน้าใหม่';
        } else if (type_id == 2) {
            return 'ของขวัญต้อนรับศิษย์เก่าสู่อ้อมกอด';
        } else {
            return 'คริสตัลหินโซล x500, คริสตัลหินจันทรา x50';
        }
    }

    render() {
        return (
            <div className={"modal " + (this.props.open ? "" : "close")} >
                <div className="modal__backdrop" />
                <div className="modal__content">
                    <div className={"modal__incontent modal__incontent--selectcharacter"}>
                        ยืนยันรับ<br /><br />
                        {this.getTextHeader(this.props.typeId)}
                    </div>
                    <div className={"modal__bottom"}>
                        <a
                            onClick={() => {
                                this.props.confirm(this.props.typeId);
                            }}
                            className="modal__button"
                        >
                            ตกลง
                        </a>
                        <a className="modal__button" onClick={this.props.closeModal}>
                            ยกเลิก
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}
