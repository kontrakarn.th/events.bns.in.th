import React from 'react';
import Modal from './Modal';

export default class ModalConfirmClaimReward extends React.Component {
    render() {
        let msg = this.props.data || "";
        return (
            <Modal
                open={this.props.open}
                title="ยืนยัน"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox">
                    <span className="inbox__head">รับของรางวัล</span>
                    <div dangerouslySetInnerHTML={{__html: msg}}></div>
                </div>
            </Modal>
        )
  }
}
