import React from 'react';
import Modal from './Modal';

export default class ModalConfirmSelectChar extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                title={this.props.title}
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div dangerouslySetInnerHTML={{__html: this.props.msg}} />
            </Modal>
        )
  }
}
