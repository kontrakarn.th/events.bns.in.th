import React from 'react';
import Modal from './Modal';

export default class ModalConfirmExchange extends React.Component {
    render() {
        let msg = this.props.data || "";
        return (
            <Modal
                open={this.props.open}
                title="ยืนยัน"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox">
                    <span className="inbox__head">แลก</span>
                    <div>- {msg}</div>
                </div>
            </Modal>
        )
  }
}
