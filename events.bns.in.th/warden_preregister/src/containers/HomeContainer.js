import React, { Component } from 'react';
import {connect} from 'react-redux';
import 'whatwg-fetch';

import $ from "jquery";

import {apiPost} from './../middlewares/Api';

import ModalMessage from '../components/ModalMessage';
import ModalConfirmAnswer from '../components/ModalConfirmAnswer';
import ModalReceiveGacha from '../components/ModalReceiveGacha';
import ModalConfirmExchange from '../components/ModalConfirmExchange';
import ModalReceiveExchange from '../components/ModalReceiveExchange';
import ModalConfirmClaimReward from '../components/ModalConfirmClaimReward';
import ModalSelectCharacter from '../components/ModalSelectCharacter';
import ModalConfirmSelectChar from '../components/ModalConfirmSelectChar';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
import logo from './../static/images/logo_bns.png'
import headText from './../static/images/head_text.png'
import headTextMobile from './../static/images/head_text_mobile.png'
import step1 from './../static/images/step1.png'
import step2 from './../static/images/step2.png'
import question1 from './../static/images/question_step1.png'
import question2 from './../static/images/question_step2.png'
import titleItem from './../static/images/title_item.png'
import item1 from './../static/images/item01.png'
import item2 from './../static/images/item02.png'
import item3 from './../static/images/item03.png'
import item4 from './../static/images/item04.png'
import item5 from './../static/images/item05.png'
import char1 from './../static/images/char01.png'
import char2 from './../static/images/char02.png'
import char3 from './../static/images/char03.png'
import notcheck from './../static/images/notchecked.png'
import checked from './../static/images/checked.png'


import {
    onAccountLogout
} from '../actions/AccountActions';

class HomeContainer extends Component {
    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }
// ========= ========= ========= ========= ========= ========= ========= ========= =========

apiEventInfo(){
    this.setState({ showLoading: true});
    let self = this;
    let dataSend={ type : "event_info" };
    let successCallback = (data)=>{
        // console.log("data",data);
        this.setState({
           permission: true,
           showLoading: false,
           ...data.content
       })
    };
    let failCallback = (data)=>{
        if(data.type=='no_permission'){
            this.setState({
                permission: false,
                showLoading: false,
            });
        }
        if(data.type=="no_game_info"){
            this.setState({
                showModalMessage: data.message,
                showLoading: false,
            });
        }
        if(data.type === "no_char"){
            // this.apiGetCharacter();
        }
    };
    apiPost("REACT_APP_API_POST_EVENT_INFO",this.props.jwtToken,dataSend,successCallback,failCallback);
}

apiGetCharacter(){
    this.setState({ showLoading: true});
    let self = this;
    let dataSend={ type : "get_character" };
    let successCallback = (data)=>{
        this.setState({
           permission: true,
           showLoading: false,
           listCharater: data.content,
           showModalSelectCharacter: true
       })
    };
    let failCallback = (data)=>{
        if(data.type=='no_permission'){
            this.setState({
                permission: false,
                showLoading: false,
            });
        }
    };
    apiPost("REACT_APP_API_POST_GET_CHARACTER",this.props.jwtToken,dataSend,successCallback,failCallback);
}
apiAcceptCharacter(character_id){
    this.setState({ showLoading: true,showModalConfirmCharacter: false});
    let self = this;
    let dataSend={
        type : "accept_character",
        id: character_id,
    };
    let successCallback = (data)=>{
        // console.log("data",data);
        this.setState({
           permission: true,
           showLoading: false,
           showModalMessage: data.message,
           ...data.content
       })
    };
    let failCallback = (data)=>{
        if(data.type=='no_permission'){
            this.setState({
                permission: false,
                showLoading: false,
            });
        } else {
            this.setState({
                showModalMessage: data.message,
                showModalSelectCharacter: true,
                showLoading: false,
            });
        }
    };
    apiPost("REACT_APP_API_POST_ACCEPT_CHARACTER",this.props.jwtToken,dataSend,successCallback,failCallback);
}

apiSendAnswer(answer){
    this.setState({ showLoading: true,showModalConfirmAnswer: false});
    let self = this;
    let dataSend={ type : "send_answer", id: answer};
    let successCallback = (data)=>{
        // console.log("data",data);
        this.setState({
            permission: true,
            showLoading: false,
            showModalMessage: data.message,
            ...data.content
       })
    };
    let failCallback = (data)=>{
        if(data.type=='no_permission'){
            this.setState({
                permission: false,
                showLoading: false,
            });
        }
        if(data.type === "no_char"){
            // this.apiGetCharacter();
        }
    };
    apiPost("REACT_APP_API_POST_SEND_ANSWER",this.props.jwtToken,dataSend,successCallback,failCallback);
}

apiRedeemReward(packageId){
    this.setState({ showLoading: true,showModalConfirmClaimReward: false});
    let self = this;
    let dataSend={ type : "redeem_reward", package_id: packageId};
    let successCallback = (data)=>{
        // console.log("data",data);
        this.setState({
            packageId: 0,
            packageInfo: "",
            permission: true,
            showLoading: false,
            showModalMessage: data.message,
            ...data.content
       })
    };
    let failCallback = (data)=>{
        if(data.type=='no_permission'){
            this.setState({
                permission: false,
                showLoading: false,
            });
        }
        if(data.type === "no_char"){
            // this.apiGetCharacter();
        }
    };
    apiPost("REACT_APP_API_POST_REDEEM",this.props.jwtToken,dataSend,successCallback,failCallback);
}
        
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: false,

            name: "",
            canplay: false,
            check1: false,
            check2: true,
            check3: false,

            character_id: -1,
            character_name: "",
            listCharater: [],
            showModalSelectCharacter: false,

            showModalMessage: "",
            showModalConfirmAnswer: false,
            showModalReceiveGacha: false,
            showModalConfirmExchange: false,
            showModalReceiveExchange: false,
            showModalReceiveExchangeMessage: "",

            // canClickRandom: true,
            canClickExchange: true,
            scollDown: false,

            username: "",
            character: "",
            can_select_char: false,
            selected_char: false,
            can_preregis: false,
            is_registered: false,
            preregis_can_receive: false,
            preregis_received: false,
            quest_1: false,
            quest_2: false,
            quest_3: false,
            warden_can_receive: false,
            warden_received: false,

            answer: 0,

            packageId: 0,
            showModalConfirmClaimReward: false,

            packageInfo: "",
        }
    }
    componentDidMount(){
        if(this.props.jwtToken !== "") setTimeout(()=>this.apiEventInfo(),300);
        
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken){
            setTimeout(()=>{
                this.apiEventInfo()
            },300);
        }

        if(prevState.answer != this.state.answer && this.state.answer != 0){
            // remove diabled
            $('.pre-register').removeClass('disabled');
        }

        if(this.state.can_select_char !== prevState.can_select_char && this.state.can_select_char == true && this.state.selected_char == false){
            setTimeout(()=>{
                this.apiGetCharacter();
            },600);
        }

    }

    onAnswerChanged(e){
        this.setState({
            answer: e.currentTarget.value
        });
    }

    onPreRegister(){

        if(this.state.answer != 0){

            this.setState({
                showModalConfirmAnswer: true
            });

        }

    }

    actSendAnswer(){

        if(this.state.answer != 0){
            this.apiSendAnswer(this.state.answer);
        }
    }

    onClaimReward(e,package_id){
        if(this.state.preregis_can_receive == true && this.state.is_registered == true){

            let packageRewardText = "- คูปองเพิ่มช่องตัวละคร<br/>- คูปองอัพเกรดด่วน<br />- คูปองลดราคาแพ็คเกจวอร์เดน 200 บาท";
            
            this.setState({
                showModalConfirmClaimReward: true,
                packageId:package_id,
                packageInfo: packageRewardText,
            });
        }
    }

    onClaimWardenReward(e,package_id){
        if(this.state.warden_can_receive == true && this.state.is_registered == true && this.state.selected_char == true){

            let packageRewardText =  "- ชุดผู้ปกปักษ์<br/>- ทรงผมผู้ปกปักษ์";

            this.setState({
                showModalConfirmClaimReward: true,
                packageId:package_id,
                packageInfo: packageRewardText,
            });
        }
    }

    actRedeemReward(){
        if((this.state.preregis_can_receive == true && this.state.is_registered == true && this.state.packageId != 0) || (this.state.warden_can_receive == true && this.state.is_registered == true && this.state.selected_char == true)){
            this.apiRedeemReward(this.state.packageId);
        }
    }

    actApiSelectCharacter(character_id,character_name){
        this.apiAcceptCharacter(character_id)
    }

    onLogout(e) {
        e.preventDefault();
        console.log("Logout!");
        const {dispatch} = this.props;
        dispatch(onAccountLogout());
    }

    render() {
        return (
            <div className="">
                {this.state.permission ?
                    <div className="events-bns">
                        <div className="warden">
                            <div className="events-head">
                                <div className="events-head__title">
                                    <div className="events-head__logo"><img src={logo} alt="bns" /></div>
                                    <picture>
                                        <source media="(max-width: 640px)" srcSet={headTextMobile} />
                                        <source srcSet={headText} />
                                        <img className="char" src={headText} alt="ลงทะเบียนล่วงหน้า" />
                                    </picture>
                                    {
                                    Object.keys(this.state.username).length > 0 ?
                                            <div className="events-head__account">{this.state.username}<br /><a href="#" onClick={this.onLogout.bind(this)}>Logout</a></div>

                                        :
                                            null
                                            // <div className="events-head__account"><a href={this.props.loginUrl}>Login</a></div>
                                    }
                                </div>
                                <div className={"events-head__scroll" +(this.state.scollDown? " hide":"")}><img src={iconScroll} alt="" /></div>
                            </div>
                            <section className="events-content">
                                <div className="warden__blog">
                                    <div className="warden__step"><img src={step1} alt="step1" /></div>
                                    <div className="warden__inner">
                                        <p>กรอกแบบสอบถามและลงทะเบียนสนใจเล่นดาบใหญ่ในวันที่ <span className="warden__text--yellow">26 ธันวาคม - 8 มกราคม</span></p>
                                        <div className="warden__content">
                                            <div><img src={question1} alt="question1" /></div>
                                            <ul className="warden__answer-list">
                                                <li><input type="radio" name="step1" id="fashion" value="1" disabled={this.state.is_registered || !this.state.can_preregis} checked={this.state.answer == 1} onChange={this.onAnswerChanged.bind(this)} /><label for="fashion"> ชุดแฟชั่น</label></li>
                                                <li><input type="radio" name="step1" id="pet" value="2" disabled={this.state.is_registered || !this.state.can_preregis} checked={this.state.answer == 2} onChange={this.onAnswerChanged.bind(this)} /><label for="pet"> สัตว์เลี้ยง</label></li>
                                                <li><input type="radio" name="step1" id="weapon" value="3" disabled={this.state.is_registered || !this.state.can_preregis} checked={this.state.answer == 3} onChange={this.onAnswerChanged.bind(this)} /><label for="weapon"> อาวุธลวงตา</label></li>
                                                <li><input type="radio" name="step1" id="item" value="4" disabled={this.state.is_registered || !this.state.can_preregis} checked={this.state.answer == 4} onChange={this.onAnswerChanged.bind(this)} /><label for="item"> ไอเท็มเสริมแกร่ง (อาวุธ, จิตของฮงมุน ฯลฯ)</label></li>
                                            </ul>
                                            {
                                                this.state.can_preregis == true && this.state.is_registered == false ?
                                                    <a className="warden-btns__normal pre-register disabled" onClick={this.onPreRegister.bind(this)}>ลงทะเบียน</a>
                                                :
                                                    (
                                                        this.state.is_registered == true ?
                                                            <a className="warden-btns__normal disabled">ลงทะเบียนแล้ว</a>
                                                        :
                                                            <a className="warden-btns__normal disabled">ไม่ตรงเงื่อนไข</a>
                                                    )
                                            }
                                            
                                        </div>
                                        <p>สามารถรับไอเทมได้ฟรี</p>
                                        <div className="warden__content">
                                            <div><img src={titleItem} alt="Free Item" /></div>
                                            <ul className="warden__item-list">
                                                <li>
                                                    <img src={item1} alt="คูปองเพิ่มชื่อตัวละคร" />
                                                    คูปองเพิ่มช่องตัวละคร (1)
                                                </li>
                                                <li>
                                                    <img src={item2} alt="คูปองอัพเกรดด่วน" />
                                                    คูปองอัพเกรดด่วน (1)
                                                </li>
                                                <li>
                                                    <img src={item3} alt="คูปองลดราคาแพ็คเกจวอร์เดน" />
                                                    คูปองลดราคา <br/>แพ็คเกจวอร์เดน 200 บาท
                                                </li>
                                            </ul>
                                            {
                                                this.state.preregis_received == true ?
                                                    <a className="warden-btns__normal disabled">รับไปแล้ว</a>
                                                :
                                                    (
                                                        this.state.preregis_can_receive == true && this.state.is_registered == true ?
                                                            <a className="warden-btns__normal" onClick={(event,packageId)=>this.onClaimReward(event,1)}>รับไอเทมฟรี</a>
                                                        :
                                                            <a className="warden-btns__normal disabled">ไม่ตรงเงื่อนไข</a>
                                                    )
                                            }
                                        </div>
                                    </div>
                                    <img src={char1} alt="character" className="warden__character--01" />
                                </div>
                            </section>
                                <div className="warden__blog">
                                    <img src={char2} alt="character" className="warden__character--02" />
                                    <div className="warden__step"><img src={step2} alt="step2" /></div>
                                    <div className="warden__inner">
                                        <p>ผู้เล่นลงทะเบียนสนใจและเล่นวอร์เดนโดยทำตามเงื่อนไข</p>
                                        <p className="warden__text--yellow">วันที่ 16 มกราคม - 13 กุมภาพันธ์  2562</p>
                                        <div className="warden__content">
                                            {
                                                this.state.character != "" ?
                                                    <div className="character-name">{this.state.character}</div>
                                                :
                                                    null
                                            }
                                            <div><img src={question2} alt="question2" /></div>
                                            <ul className="warden__answer-list">
                                                <li>{this.state.quest_1 ? <img src={checked} alt="" /> : <img src={notcheck} alt="" />} เปลวไฟแห่งความลำบาก</li>
                                                <li>{this.state.quest_2 ? <img src={checked} alt="" /> : <img src={notcheck} alt="" />} เข้าร่วมสงครามเกาะแห่งพันธนาการ 1 ครั้ง</li>
                                                <li>{this.state.quest_3 ? <img src={checked} alt="" /> : <img src={notcheck} alt="" />} ผ่านเควสเนื้อเรื่องบทที่ 8 ตอนที่ 20 ชีวิตใหม่ของซอยอน</li>
                                            </ul>
                                        </div>
                                        <div className="warden__content">
                                            <div><img src={titleItem} alt="Free Item" /></div>
                                            <ul className="warden__item-list">
                                                <li>
                                                    <img src={item4} alt="คูปองเพิ่มชื่อตัวละคร" />
                                                    ชุดผู้ปกปักษ์ (1)
                                                </li>
                                                <li>
                                                    <img src={item5} alt="คูปองอัพเกรดด่วน" />
                                                    ทรงผมผู้ปกปักษ์ (1)
                                                </li>
                                            </ul>
                                            {
                                                this.state.selected_char == true ?
                                                    (
                                                        this.state.warden_received == true ?
                                                            <a className="warden-btns__normal disabled">รับไปแล้ว</a>
                                                        :
                                                            (
                                                                this.state.warden_can_receive == true && this.state.is_registered == true && this.state.selected_char == true ?
                                                                    <a className="warden-btns__normal" onClick={(event,packageId)=>this.onClaimWardenReward(event,2)}>รับไอเทมฟรี</a>
                                                                :
                                                                    <a className="warden-btns__normal disabled">ไม่ตรงเงื่อนไข</a>
                                                            )
                                                    )
                                                :
                                                    null
                                            }
                                            
                                        </div>
                                    </div>
                                    <img src={char3} alt="character" className="warden__character--03" />
                                </div>
                            </div>
                    </div>
                    :
                    <Permission />
                }


                <Loading
                    open={this.state.showLoading}
                />
                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
                <ModalConfirmAnswer
                    open={this.state.showModalConfirmAnswer}
                    actConfirm={this.actSendAnswer.bind(this)}
                    actClose={()=>this.setState({showModalConfirmAnswer: false})}
                />
                <ModalConfirmClaimReward
                    open={this.state.showModalConfirmClaimReward}
                    actConfirm={this.actRedeemReward.bind(this)}
                    actClose={()=>this.setState({showModalConfirmClaimReward: false, packageId:0})}
                    data={this.state.packageInfo !="" ? this.state.packageInfo : ""}
                />

                <ModalSelectCharacter
                    open={this.state.showModalSelectCharacter}
                    listCharater={this.state.listCharater|| []}
                    actSelectCharacter={(character_id,character_name)=>this.setState({
                        showModalSelectCharacter: false,
                        showModalConfirmCharacter: true,
                        character_id: character_id,
                        character_name: character_name
                    })}
                />
                <ModalConfirmSelectChar
                    open={this.state.showModalConfirmCharacter}
                    actConfirm={()=>this.actApiSelectCharacter(this.state.character_id,this.state.character_name)}
                    actClose={()=>this.setState({
                        showModalSelectCharacter: true,
                        showModalConfirmCharacter: false
                    })}
                    title={"ยืนยัน"}
                    msg={'เลือก <span>"'+this.state.character_name +'"</span> ?'}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
    /* onAccountLogout: () => {
        dispatch(onAccountLogout());
    } */
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
