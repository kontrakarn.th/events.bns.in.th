import detail_title from './../static/images/pet/detail_title.jpg';
import random_title from './../static/images/pet/random_title.jpg';
import example_title from './../static/images/pet/example_title.jpg';
import frame2_item from './../static/images/pet/frame2_item.png';

//pet example
import pet_1 from './../static/images/pet/example/pet_1.png';
import pet_2 from './../static/images/pet/example/pet_2.png';
import pet_3 from './../static/images/pet/example/pet_3.png';
import pet_4 from './../static/images/pet/example/pet_4.png';
import pet_5 from './../static/images/pet/example/pet_5.png';
import pet_6 from './../static/images/pet/example/pet_6.png';
import pet_7 from './../static/images/pet/example/pet_7.png';
import pet_8 from './../static/images/pet/example/pet_8.png';
import pet_9 from './../static/images/pet/example/pet_9.png';
import pet_10 from './../static/images/pet/example/pet_10.png';
import pet_11 from './../static/images/pet/example/pet_11.png';
import box2_imgbtn from './../static/images/pet/box2_imgbtn.png';

//item
import gacha_1 from "./../static/images/pet/item/gacha_1.png";
import gacha_2 from "./../static/images/pet/item/gacha_2.png";
import gacha_3 from "./../static/images/pet/item/gacha_3.png";
import gacha_4 from "./../static/images/pet/item/gacha_4.png";
import gacha_5 from "./../static/images/pet/item/gacha_5.png";
import gacha_6 from "./../static/images/pet/item/gacha_6.png";
import gacha_7 from "./../static/images/pet/item/gacha_7.png";
import gacha_8 from "./../static/images/pet/item/gacha_8.png";
import gacha_9 from "./../static/images/pet/item/gacha_9.png";
import gacha_10 from "./../static/images/pet/item/gacha_10.png";
import gacha_11 from "./../static/images/pet/item/gacha_11.png";
export const Imglist = {
	detail_title,
	random_title,
	example_title,
	frame2_item,
	pet_1,
	pet_2,
	pet_3,
	pet_4,
	pet_5,
	pet_6,
	pet_7,
	pet_8,
	pet_9,
	pet_10,
	pet_11,
	box2_imgbtn,
	gacha_1,
	gacha_2,
	gacha_3,
	gacha_4,
	gacha_5,
	gacha_6,
	gacha_7,
	gacha_8,
	gacha_9,
	gacha_10,
	gacha_11,
}