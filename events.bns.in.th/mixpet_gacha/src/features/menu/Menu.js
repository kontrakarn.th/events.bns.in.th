import React from 'react';
import { Link } from 'react-router-dom';
import iconMenu from './../../static/images/pet/icon_menu.png';
import iconMenuClose from './../../static/images/pet/menu_close.png';
import './styles.scss';
export default class Menu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showMenu:false
        }
    }

    handleClickMenu() {
        this.setState({
            showMenu:!this.state.showMenu
        })
    }

    render() {
        return (
            <div className="left__menu">
                <div className={"bg-overlay " + (this.state.showMenu ? "show" : " ") }></div>
                <a onClick={this.handleClickMenu.bind(this)} className="menu-icon"><img alt="icon" src={iconMenu} /></a>
                <ul className={"left__menu--list " + (this.state.showMenu ? "show" : " ") }>
                    <li className="menu-close"><a onClick={this.handleClickMenu.bind(this)}></a></li>
                    <li><Link to="/mixpet_gacha/" className={this.props.page === 'main' ? 'underline' : null}>สุ่มสัตว์เลี้ยงลํ้าค่า</Link></li>
                    <li><Link to="/mixpet_gacha/trade" className={this.props.page === 'trade' ? 'underline' : null}>ระบบแลกเปลี่ยน</Link></li>
                    <li><Link to="/mixpet_gacha/history" className={this.props.page === 'history' ? 'underline' : null}>ประวัติ</Link></li>
                </ul>
            </div>
        )
    }
}
