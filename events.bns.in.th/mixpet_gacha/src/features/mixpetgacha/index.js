import React from 'react';
import {connect} from 'react-redux';
import Slider from 'react-slick';
import { apiPost } from './../../middlewares/Api';
import styled, { keyframes } from 'styled-components';

import { onAccountLogout } from './../../actions/AccountActions';
import { setValues } from './redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalDetail from './../../features/modals/ModalDetail';
import ModalShowItem from './../../features/modals/ModalShowItem';
import ModalItemResult from './../../features/modals/ModalItemResult';

import './css/style.css';
import Menu from '../menu/Menu.js';
import {Imglist} from './../../constants/Import_Images';

const ArrowNext =(props)=>{
    return(
            <div
                  className="arrow--next"
                  onClick={props.onClick}
            />
    )
}

const ArrowPrev =(props)=>{
    return(
         <div
          className="arrow--prev"
          onClick={props.onClick}
        />
    )
}
class MixPetGacha extends React.Component {
    actConfirm(){
        this.apiRandomPet();
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.response,
                    modal_open: "",
                    username: data.data.nickname,
                    can_play: data.data.can_play,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CHECKIN", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiRandomPet(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'randompet'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    modal_open:'itemresult',
                    modal_message:'ยินดีด้วยคุณได้รับ',
                    modal_item: data.content,
                    can_play: data.data.can_play,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_RANDOM_PET", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: ''
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }



    render() {
        let settings = {
          dots: false,
          infinite: true,
          speed: 500,
          slidesToShow: 1,
          slidesToScroll: 1,
          nextArrow: <ArrowNext/>,
          prevArrow: <ArrowPrev/>
        };
        let arrimg = Array.from(Array(11),( x , index)=> index+1);
        return (
            <div className="main">
                <Menu page={'main'}/>
                <div className="main__contentwrap">
                        <div className="main__box1">
                                <div className="main__box1--titlebox">
                                        <img src={Imglist['detail_title']} alt=""/>
                                        <ul className="main__box1--list">
                                                <li>1. ผู้เล่นสามารถกดเปิดถุงสัตว์เลี้ยงล้ำค่ามูลค่า 30,000 ไดมอนด์</li>
                                                <li>2. เมื่อกดสุ่มผู้เล่นจะมีโอกาสได้รับหินสัตว์เลี้ยงสุดคูล หินสัตว์เลี้ยงจะถูกเก็บไว้ในระบบเว็บไซต์</li>
                                                <li>3. ผู้เล่นสามารถเลือกที่จะกดรับหินสัตว์เลี้ยงให้ตัวเองได้ โดยไม่เสียค่าใช้จ่าย</li>
                                                <li>4. ผู้เล่นสามารถเลือกกดส่งให้เพื่อนได้ จะเสียค่าใช้จ่ายมูลค่า 10,000 ไดมอนด์ ต่อไอเทม 1 ชิ้น</li>
                                                <li>5. โปรโมชั่นเริ่ม 12 มิถุนายน 2562 12:00:00 น.</li>
                                                <li>6. โปรโมชั่นสิ้นสุดการเปิดกาชาปอง 26 มิถุนายน 2562 เวลา 23:59:59 น.</li>
                                                <li>7. ปิดระบบการรับไอเทม และ ส่งไอเทม 3 กรกฎาคม 2562 เวลา 23:59:59 น.</li>
                                        </ul>
                                </div>
                        </div>
                        <div className="main__box2">
                                <div className="main__box2--titlebox">
                                        <img src={Imglist['random_title']} alt=""/>
                                        <img src={Imglist['frame2_item']} alt=""/>
                                        <div className={"main__box2--btn " + (!this.props.can_play && ' disable' )} onClick={()=>this.props.setValues({modal_open:'confirm',modal_message:`ยืนยันการเปิดถุงสัตว์เลี้ยง<br/>มูลค่า 30,000 ไดมอนด์`})}/>
                                        {/*onClick={()=>this.props.setValues({modal_open:'itemresult',modal_message:'ยินดีด้วยคุณได้รับ'})}*/}
                                        <img className="main__box2--imgbtn" src={Imglist['box2_imgbtn']} onClick={()=>this.props.setValues({modal_open:'showitem'})}/>
                                </div>
                        </div>
                         <div className="main__box3">
                                <div className="main__box3--titlebox">
                                        <img src={Imglist['example_title']} alt=""/>
                                        <Slider {...settings}>
                                                {
                                                    arrimg && arrimg.length > 0 &&
                                                    arrimg.map((pet,index) => {
                                                        return(
                                                                  <div className="main__box3--petbox" key={`pet_${pet}`}>
                                                                        <img className="main__box3--img" src={Imglist[`pet_${pet}`]} alt=""/>
                                                                </div>
                                                        )
                                                    })
                                                }
                                        </Slider>
                                </div>
                        </div>
                </div>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                <ModalShowItem/>
                <ModalItemResult/>
                <ModalDetail />
            </div>
        )
    }
}
const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(MixPetGacha);
