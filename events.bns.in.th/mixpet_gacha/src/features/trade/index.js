import React from 'react';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled, { keyframes } from 'styled-components';

import { onAccountLogout } from './../../actions/AccountActions';
import { setValues } from './redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalConfirmTrade from './../../features/modals/ModalConfirmTrade';
import ModalSend from './../../features/modals/​ModalSend';
import ModalConfirmSend from './../../features/modals/ModalConfirmSend';
import './css/style.css';
import Menu from '../menu/Menu.js';
import {Imglist} from '../../constants/Import_Images';

class Trade extends React.Component {
    actConfirmRedeem(){
        this.apiExchangeItem(this.props.modal_send_item.id);
    }
    actConfirmSendItem(){
        this.apiExchangeItem(this.props.modal_send_item.id,(this.props.input_message && this.props.input_message));
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiExchangeInfo(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'getexchangeinfo'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.response,
                    modal_open: "",
                    itemlist: data.content,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_GETEXCHANGEINFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiExchangeItem(id,uid){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'exchangeitem',id:id};
        if(uid){
            dataSend = {type:'exchangeitem',id:id,sendto: uid};
        }
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content.response,
                    modal_open:"message",
                    modal_message: data.message,
                    itemlist: data.content,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message.replace(' ',"<br/>"),
                });
            }
        };
        apiPost("REACT_APP_API_POST_EXCHANGEITEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: ''
        }
    }

    componentDidMount() {
        this.apiExchangeInfo();
    }



    render() {
        return (
            <div className="trade">
                <Menu page={'trade'} />
                <div className="trade__content">
                    <div className="trade__content--inner">
                        {
                            this.props.itemlist && this.props.itemlist.length > 0 &&
                            this.props.itemlist.map((item,index) => {
                                return(
                                        <div className="trade__box" key={item.product_title}>
                                             <div className={"trade__box--itembox " + (index < 3 && "hot") + (item.amt < 1 ? " disable" : '')} onClick={()=>this.props.setValues({modal_open:"confirmtrade",modal_send_item:{...item},input_message:''})}>
                                                    <img src={Imglist[item.icon]} alt=""/>
                                            </div>
                                            <div className="trade__box--itemname" 
                                                dangerouslySetInnerHTML={{ __html: item.product_title.replace(" ","<br/>") + ` (${item.amt}) ` }}/>
                                        </div>
                                )
                            })
                        }
                    </div>
                </div>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirmTrade actConfirm={this.actConfirmRedeem.bind(this)}/>
                <ModalSend/>
                <ModalConfirmSend actConfirm={this.actConfirmSendItem.bind(this)}/>
            </div>
        )
    }
}
const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.TradeReducer,...state.ModalReducer});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Trade);