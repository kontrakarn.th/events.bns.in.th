
const initialState = {
    "itemlist" : [
        {
            "id": 1,
            "product_title": "หินสัตว์เลี้ยง ภูติหิมะตัวน้อย",
            "icon": "gacha_1",
            "key": "gacha_1",
            "amt": 0
        },
        {
            "id": 2,
            "product_title": "หินสัตว์เลี้ยง เมโลดี้",
            "icon": "gacha_2",
            "key": "gacha_2",
            "amt": 0
        },
        {
            "id": 3,
            "product_title": "หินสัตว์เลี้ยง นกฮูก",
            "icon": "gacha_3",
            "key": "gacha_3",
            "amt": 0
        },
        {
            "id": 4,
            "product_title": "หินล้ำค่า ลูกเจี๊ยบ",
            "icon": "gacha_4",
            "key": "gacha_4",
            "amt": 0
        },
        {
            "id": 5,
            "product_title": "หินสัตว์เลี้ยง น้องหมาสิงโต",
            "icon": "gacha_5",
            "key": "gacha_5",
            "amt": 0
        },
        {
            "id": 6,
            "product_title": "หินสัตว์เลี้ยง อัลปาก้า",
            "icon": "gacha_6",
            "key": "gacha_6",
            "amt": 0
        },
        {
            "id": 7,
            "product_title": "หินสัตว์เลี้ยง วิญญาณมูซา",
            "icon": "gacha_7",
            "key": "gacha_7",
            "amt": 0
        },
        {
            "id": 8,
            "product_title": "หินสัตว์เลี้ยง วิคตอเรีย",
            "icon": "gacha_8",
            "key": "gacha_8",
            "amt": 0
        },
        {
            "id": 9,
            "product_title": "หินสัตว์เลี้ยง งูขาว",
            "icon": "gacha_9",
            "key": "gacha_9",
            "amt": 0
        },
        {
            "id": 10,
            "product_title": "หินล้ำค่า จินซอยอน",
            "icon": "gacha_10",
            "key": "gacha_10",
            "amt": 0
        },
        {
            "id": 11,
            "product_title": "หินล้ำค่า ซอยอนเด็ก",
            "icon": "gacha_11",
            "key": "gacha_11",
            "amt": 0
        },
    ]
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return {
          ...state,
          [action.key]: action.value
        };
      } else {
        return state;
      }
    case "SET_VALUES": return { ...state, ...action.value };
    default:
      return state;
  }
};

export const setValue  = (key, value) =>{ return ({ type: "SET_VALUE", value, key }) };
export const setValues = (value) =>{ return ({ type: "SET_VALUES", value }) };
