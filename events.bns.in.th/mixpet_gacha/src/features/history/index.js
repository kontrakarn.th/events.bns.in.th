import React ,{Fragment}from 'react';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled, { keyframes } from 'styled-components';

import { onAccountLogout } from './../../actions/AccountActions';
import { setValues } from './redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalDetail from './../../features/modals/ModalDetail';
import ScrollArea from 'react-scrollbar';

import './css/style.css';
import Menu from '../menu/Menu.js';

class History extends React.Component {
     apiGetHistory(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'historylist'};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content.response,
                    modal_open: '',
                    history_list: data.content
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_HISTORYLIST", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    componentDidMount() {
        this.apiGetHistory()
    }
    render() {
        // let hisarry = Array.from(Array(35),( x , index)=> index+1);
        return (
            <div className="history">
                <Menu page={'history'}/>
                <div className="history__content">
                          <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: "70%",height: "95%",opacity: "1",margin: "0 auto",padding: "0 3%"}}
                            verticalScrollbarStyle={{backgroundColor:"#000", opacity:1}}
                        >
                        <ul className="history__list">
                        {
                            this.props.history_list && this.props.history_list.length > 0 && 
                            this.props.history_list.map((item,index) => {
                                return(
                                        <li className={"history__list--list" + (item.send_to ? ' sent' : ' receive')} key={'history_' + (index+1)}>
                                            {
                                                item.send_to 
                                                ?
                                                    <Fragment>
                                                        <div>{item.product_title}</div>
                                                        <div>(UID {item.send_to})</div>
                                                        <div>{item.updated_at}</div>
                                                    </Fragment>
                                                :
                                                    <Fragment>
                                                        <div>{item.product_title}</div>
                                                        <div>{item.updated_at}</div>
                                                    </Fragment>
                                            }
                                        </li>           
                                )
                            })
                            // hisarry.map((item,index) => {
                            //     return (
                            //             <li class="history__list--list receive"><div>หินสัตว์เลี้ยง น้องหมาสิงโต</div><div>2019-06-10 13:32:23</div></li>
                            //     )
                            // })
                        }
                        </ul>
                        </ScrollArea>
                </div>
                <ModalLoading />
                <ModalMessage />
                <ModalDetail />
            </div>
        )
    }
}
const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.HistoryReducer});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(History);