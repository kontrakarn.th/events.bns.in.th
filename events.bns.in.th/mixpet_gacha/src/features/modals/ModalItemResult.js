import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './redux';

import modal_bg from './../../static/images/pet/modal/modal_bg.png';
import imgBtnConfirm from './images/btn_confirm.png';
import imgBtnCancel from './images/btn_cancel.png';
import imgBtn from './../../static/images/pet/modal/btns.png';
import {Imglist} from './../../constants/Import_Images';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="itemresult"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <h2>แจ้งเตือน</h2>
                <Itembox>
                 <div className="trade__box--itembox ">
                        <img src={Imglist[props.modal_item[0].icon]} alt=""/>
                </div>
                </Itembox>
                <h3 dangerouslySetInnerHTML={{__html: modal_message}} />
                <ItemName>
                    <div className="trade__box--itemname" 
                        dangerouslySetInnerHTML={{ __html: typeof props.modal_item[0].product_title === 'string' && props.modal_item[0].product_title.replace(" ","")}}/>
                </ItemName>
            </ModalMessageContent>
            <ModalBottom>
                <Btns onClick={()=>props.setValues({modal_open:""})} >
                        ตกลง
                </Btns>
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.ModalReducer,...state.TradeReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 396px;
    height: 415px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${modal_bg});
    h2 {
        font-family: DBXtypeX;
        position: relative;
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-family: DBXtypeX;
        position: relative;
        top: 100px;
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
`;
const ModalBottom = styled.div`
    position: relative;
    display: inline-block;
    left: 50%;
    transform: translate3d(-50%,-50%,0);

`;
const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 144px;
    height: 50px;
    background:top center no-repeat url(${imgBtn});
    color: #fdf2e0;
    position:relative;
    text-align: center;
    box-sizing: border-box;
    padding-top: 3%;
    font-size: 24px;
    font-family: DBXtypeX;
    &:hover{
        background-position: bottom center;
    }
`;
const Itembox = styled.div `
    position: relative;
    top: 90px;
`;
const ItemName = styled.div`
    position: relative;
    top: 80px;
    >div{
        color: #8d2d00 !important;
    }
`
