
const initialState = {
    modal_open: "none",
    modal_message: "",
    modal_detail: "",
    modal_item: [
      {
            "id": 0,
            "item_type": "",
            "product_title": "",
            "icon": "",
            "key": ""
        }
    ],
    input_message: '',
    modal_send_item:{},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return {
          ...state,
          [action.key]: action.value
        };
      } else {
        return state;
      }
    case "SET_VALUES": return { ...state, ...action.value };
    default:
      return state;
  }
};

export const setValue = (key, value) =>{
    // console.log("setValue",key, value);
    return ({
        type: "SET_VALUE",
        value,
        key
    })
};
export const setValues = (value) =>{
    // console.log("setValue", value);
    return ({
        type: "SET_VALUES",
        value
    })
};
