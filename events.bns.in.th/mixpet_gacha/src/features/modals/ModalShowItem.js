import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './redux';

import imgAlert from './images/modal_alert.png';
import imgBtnConfirm from './images/btn_confirm.png';
import imgBtnCancel from './images/btn_cancel.png';
import modal_bg_long from '../../static/images/pet/modal/modal_bg_long.png';
import closeBtn from '../../static/images/pet/modal/close_btn.png';
import {Imglist} from '../../constants/Import_Images';
const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="showitem"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >  
            
            <ModalMessageContent>
                <Closebtn>
                    <img src={closeBtn} alt="" onClick={()=>props.setValues({modal_open:''})}/>
                </Closebtn>
                <Itemwrapper>
                        {
                            props.itemlist && props.itemlist.length > 0 &&
                            props.itemlist.map((item,index) => {
                                return(
                                        <Itembox key={item.product_title}>
                                             <div className={"trade__box--itembox " + (index < 3 && "hot")}>
                                                    <img src={Imglist[item.icon]} alt=""/>
                                            </div>
                                            <div className="trade__box--itemname" 
                                                dangerouslySetInnerHTML={{ __html: item.product_title.replace(" ","<br/>")}}/>
                                        </Itembox>
                                )
                            })
                        }
                </Itemwrapper>
            </ModalMessageContent>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.ModalReducer,...state.TradeReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 813px;
    height: 645px;
    color: #FFFFFF;
    background: no-repeat center url(${modal_bg_long});
    background-size: 100% 100%;
    padding: 7%;
    box-sizing: border-box;
`;
const ModalBottom = styled.div`
    position: relative;
    display: inline-block;
    left: 50%;
    transform: translate3d(-50%,-50%,0);

`;
const Itemwrapper = styled.div `
    display: block;
    width: 85%;
    margin: 0 auto;
`
const Itembox = styled.div`
    display: inline-block;
    width: 25%;
`
const Closebtn = styled.div`
    position: absolute;
    display: block;
    top: 3%;
    right: -2%;
    >img{
        display: block;
        cursor: pointer;
    }
`;
const BtnConfirm = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 124px;
    height: 52px;
    background: no-repeat center url(${imgBtnConfirm});
`;
const BtnCancel = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 124px;
    height: 52px;
    background: no-repeat center url(${imgBtnCancel});
`;
