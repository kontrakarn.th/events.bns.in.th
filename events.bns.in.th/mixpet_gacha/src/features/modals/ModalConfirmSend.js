import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './redux';

import modal_bg from './../../static/images/pet/modal/modal_bg.png';
import imgBtnConfirm from './images/btn_confirm.png';
import imgBtnCancel from './images/btn_cancel.png';
import imgBtn from './../../static/images/pet/modal/btns.png';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="confirmsend"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <h2>ยินยัน</h2>
                <h3 dangerouslySetInnerHTML={{__html: modal_message}} />
            </ModalMessageContent>
            <ModalBottom>
                <Btns onClick={()=>{if(props.actConfirm) props.actConfirm()}} >
                        ตกลง
                </Btns>
                <Btns onClick={()=>props.setValues({modal_open:""})} >
                        ยกเลิก
                </Btns>
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.ModalReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 396px;
    height: 415px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${modal_bg});
    h2 {
        font-family: DBXtypeX;
        position: relative;
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-family: DBXtypeX;
        position: relative;
        top: 100px;
        font-size: 30px;
        line-height: 1.3em;
        color: #563e26;
    }
`;
const ModalBottom = styled.div`
    position: relative;
    display: inline-block;
    left: 50%;
    transform: translate3d(-50%,-50%,0);

`;
const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 144px;
    height: 50px;
    background:top center no-repeat url(${imgBtn});
    color: #fdf2e0;
    position:relative;
    text-align: center;
    box-sizing: border-box;
    padding-top: 3%;
    font-size: 24px;
    font-family: DBXtypeX;
    &:hover{
        background-position: bottom center;
    }
`;
