import React from 'react';
import {connect} from 'react-redux';

import F11Layout from './../features/F11Layout/';
import HistoryContent from './../features/history';

export class History extends React.Component {
    render() {
        return (
        	<F11Layout
                showUserName={true}
                showCharacterName={false}
                noScroll={true}
            >
                <HistoryContent />
            </F11Layout>
        )
    }
}
