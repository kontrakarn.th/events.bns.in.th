import React from 'react';
import {connect} from 'react-redux';

import F11Layout from './../features/F11Layout/';
import TradeContent from './../features/trade';

export class Trade extends React.Component {
    render() {
        return (
        	<F11Layout
                showUserName={true}
                showCharacterName={false}
                noScroll={true}
            >
                <TradeContent />
            </F11Layout>
        	)
    }
}
