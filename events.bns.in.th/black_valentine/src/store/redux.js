const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    // showCharacterName: true,
    characters:[],
    username: "",
    character: "",
    loginStatus: false,
    showScroll: true,
    noScroll: true,
    loading: false,
    permission: true,
    status: false,
    can_play: false,


    //===modal
    modal_open: "",//"loading","detail","showreward",""showreward","send","modalInput"
    modal_message: "",
    modal_item: "",
    modal_uid: "",
    modal_get_name: "",
    modal_combin: -1,
    modal_trade: -1,

    //===event base value
    list_item: [
        {id: 1, name:"item_01_R", count: 0},
        {id: 2, name:"item_01_G", count: 0},
        {id: 3, name:"item_01_B", count: 0},
        {id: 4, name:"item_02_R", count: 0},
        {id: 5, name:"item_02_G", count: 0},
        {id: 6, name:"item_02_B", count: 0},
        {id: 7, name:"item_03_R", count: 0},
        {id: 8, name:"item_03_G", count: 0},
        {id: 9, name:"item_03_B", count: 0},
        {id: 10, name:"item_04_R", count: 0},
        {id: 11, name:"item_04_G", count: 0},
        {id: 12, name:"item_04_B", count: 0},
        {id: 13, name:"item_05_R", count: 0},
        {id: 14, name:"item_05_G", count: 0},
        {id: 15, name:"item_05_B", count: 0},
        {id: 16, name:"item_06_R", count: 0},
        {id: 17, name:"item_06_G", count: 0},
        {id: 18, name:"item_06_B", count: 0},
    ],

    list_redeem: [
        {package_id:1,name:"ชุดจุมพิตอสูร",icon:"item_01_Base"},
        {package_id:2,name:"หมวกจุมพิตอสูร",icon:"item_02_Base"},
        {package_id:3,name:"ปีกจุมพิตอสูร",icon:"item_03_Base"},
        {package_id:4,name:"กล่องอาวุธลวงตาจุมพิตอสูร",icon:"item_04_Base"},
        {package_id:5,name:"ชุดอาทิตย์อัสดง",icon:"item_05_Base"},
        {package_id:6,name:"เซ็ทกุหลาบซันตามาเรีย",icon:"item_06_Base"},
    ],
    img2name: {
        "item_01_Base":"ชุดจุมพิตอสูร",
        "item_02_Base":"หมวกจุมพิตอสูร",
        "item_03_Base":"ปีกจุมพิตอสูร",
        "item_04_Base":"กล่องอาวุธลวงตาจุมพิตอสูร",
        "item_05_Base":"ชุดอาทิตย์อัสดง",
        "item_06_Base":"เซ็ทกุหลาบซันตามาเรีย",
    },
    //combine
    materials: [],

    //===soul values
    buy_soul_result: [],
    buy_soul_count: 0,
    buy_soul_cost: 0,


    //===history values
    items_history: [],

    //===trade values
    exchangeinfo: {
        amt: 0,
        icon: "",
        id: 0,
        key: "",
        product_title: ""
    },

    //===send item
    target_uid:-1,
    target_username: "",
    my_bag: [
        {id:1,title:"ชุดจุมพิตอสูร",icon:"item_01_Base",key:"item_01",quantity:0},
        {id:2,title:"หมวกจุมพิตอสูร",icon:"item_02_Base",key:"item_02",quantity:0},
        {id:3,title:"ปีกจุมพิตอสูร",icon:"item_03_Base",key:"item_03",quantity:0},
        {id:4,title:"กล่องอาวุธลวงตา|จุมพิตอสูร",icon:"item_04_Base",key:"item_04",quantity:0},
        {id:5,title:"ชุดอาทิตย์อัสดง",icon:"item_05_Base",key:"item_05",quantity:0},
        {id:6,title:"เซ็ทกุหลาบ|ซันตามาเรีย",icon:"item_06_Base",key:"item_06",quantity:0},
        // {id:7,title:"ชุดนักเรียนเกาหลียุค 80",icon:"S_Moon_06_Base",key:"S_Moon_06",quantity:0},
        // {id:8,title:"หินสัตว์เลี้ยงสาวน้อยจากจันทรา",icon:"S_Moon_05_Base",key:"S_Moon_05",quantity:0},
        // {id:9,title:"เซ็ทเหมียวจันทราสีเงิน",icon:"S_Moon_09_Base",key:"S_Moon_09",quantity:0},
        // {id:10,title:"เซ็ทนักเรียนเกาหลียุค 80",icon:"S_Moon_10_Base",key:"S_Moon_10",quantity:0},
        // {id:11,title:"กล่องอาวุธลวงตาจันทราสีเงิน",icon:"S_Moon_11_Base",key:"S_Moon_11",quantity:0},
    ],
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         : return ({ ...state, sessionKey: action.value });
        case 'ACCOUNT_AUTHED'       : return ({ ...state, userData: action.value });
        case 'RECEIVE_SESSION_KEY'  : return ({ ...state, sessionKey: action.value });
        case 'SET_LOGIN_URL'        : return ({ ...state, loginUrl: action.value });
        case 'SET_LOGOUT_URL'       : return ({ ...state, logoutUrl: action.value });
        case 'ACCOUNT_LOGIN'        : return ({ ...state, authState: "LOGGED_IN" });
        case 'ACCOUNT_LOGOUT'       : return ({ ...state, authState: "LOGGED_OUT" });
        case 'ACCOUNT_AUTH_CHECKED' : return ({ ...state, authed: true });
        case 'SET_JWT_TOKEN'        : return ({ ...state, jwtToken: action.value });

        case "SET_VALUE"            : return (Object.keys(defaultState).indexOf(action.key) >= 0) ? { ...state, [action.key]: action.value } : state;
        case "SET_VALUES"           : return { ...state, ...action.value };
        default:
            return state;
    }
}

export const onAccountAuth          = (sessionKey)  => ({ type: 'ACCOUNT_AUTH',         value: sessionKey });
export const onAccountAuthed        = (userData)    => ({ type: 'ACCOUNT_AUTHED',       value: userData });
export const onAccountLogin         = ()            => ({ type: 'ACCOUNT_LOGIN',        value: "" });
export const onAccountLogout        = ()            => ({ type: 'ACCOUNT_LOGOUT',       value: "" });
export const receiveSessionKey      = (sessionKey)  => ({ type: 'RECEIVE_SESSION_KEY',  value: sessionKey });
export const setLoginUrl            = (url)         => ({ type: 'SET_LOGIN_URL',        value: url });
export const setLogoutUrl           = (url)         => ({ type: 'SET_LOGOUT_URL',       value: url });
export const onAccountAuthChecked   = ()            => ({ type: 'ACCOUNT_AUTH_CHECKE',  value: true })
export const setJwtToken            = (token)       => ({ type: 'SET_JWT_TOKEN',        value: token });

export const setValue               = (key, value) => ({ type: "SET_VALUE", value, key });
export const setValues              = (value) => ({ type: "SET_VALUES", value });
