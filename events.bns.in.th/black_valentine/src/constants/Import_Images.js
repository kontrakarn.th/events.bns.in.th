import btn_enter from '../static/images/btn_enter.png'
import condition_frame from '../static/images/condition_frame.png';
import other_bg from '../static/images/other_bg.jpg';
import bg_main from '../static/images/bg_main.jpg';
import icon_menu from '../static/images/icon_menu.png';
import menu_close from '../static/images/menu_close.png';
import menu_bg from '../static/images/menu_bg.jpg';
import title_frame from '../static/images/title_frame.png';
import frame from '../static/images/frame.png';
import btn from '../static/images/btn.png';
import frame_costume from '../static/images/frame_costume.png';
import diamond from './../static/images/diamond.png';
import buy_soul_button from './../static/images/buy_soul_button.png';
import buy_soul_border from './../static/images/buy_soul_border.png';
import buy_soul_char from '../static/images/buy_soul_char.png';



import combine_arrow from './../static/images/combine_arrow.png';

import redeem_plus_btn from './../static/images/redeem_plus_btn.png';
import redeem_line_btn from './../static/images/redeem_line_btn.png';

import preview_1 from '../static/images/preview_1.png'
import preview_2 from '../static/images/preview_2.png'
import preview_3 from '../static/images/preview_3.png'
import preview_4 from '../static/images/preview_4.png'

import btn_next from '../static/images/btn_next.png';
import btn_prev from '../static/images/btn_prev.png';

import item_01 from '../static/images/items/item_01.png';
import item_01_Base from '../static/images/items/item_01_Base.png';
import item_01_get from '../static/images/items/item_01_get.png';
import item_01_R from '../static/images/items/item_01_R.png';
import item_01_G from '../static/images/items/item_01_G.png';
import item_01_B from '../static/images/items/item_01_B.png';
import item_01_R_get from '../static/images/items/item_01_R_get.png';
import item_01_G_get from '../static/images/items/item_01_G_get.png';
import item_01_B_get from '../static/images/items/item_01_B_get.png';
import item_02_B_get from '../static/images/items/item_02_B_get.png';
import item_02 from '../static/images/items/item_02.png';
import item_02_Base from '../static/images/items/item_02_Base.png';
import item_02_get from '../static/images/items/item_02_get.png';
import item_02_R from '../static/images/items/item_02_R.png';
import item_02_G from '../static/images/items/item_02_G.png';
import item_02_B from '../static/images/items/item_02_B.png';
import item_02_R_get from '../static/images/items/item_02_R_get.png';
import item_02_G_get from '../static/images/items/item_02_G_get.png';
import item_03 from '../static/images/items/item_03.png';
import item_03_Base from '../static/images/items/item_03_Base.png';
import item_03_get from '../static/images/items/item_03_get.png';
import item_03_R from '../static/images/items/item_03_R.png';
import item_03_G from '../static/images/items/item_03_G.png';
import item_03_B from '../static/images/items/item_03_B.png';
import item_03_R_get from '../static/images/items/item_03_R_get.png';
import item_03_G_get from '../static/images/items/item_03_G_get.png';
import item_03_B_get from '../static/images/items/item_03_B_get.png';
import item_04 from '../static/images/items/item_04.png';
import item_04_Base from '../static/images/items/item_04_Base.png';
import item_04_get from '../static/images/items/item_04_get.png';
import item_04_R from '../static/images/items/item_04_R.png';
import item_04_G from '../static/images/items/item_04_G.png';
import item_04_B from '../static/images/items/item_04_B.png';
import item_04_R_get from '../static/images/items/item_04_R_get.png';
import item_04_G_get from '../static/images/items/item_04_G_get.png';
import item_04_B_get from '../static/images/items/item_04_B_get.png';
import item_05 from '../static/images/items/item_05.png';
import item_05_Base from '../static/images/items/item_05_Base.png';
import item_05_get from '../static/images/items/item_05_get.png';
import item_05_R from '../static/images/items/item_05_R.png';
import item_05_G from '../static/images/items/item_05_G.png';
import item_05_B from '../static/images/items/item_05_B.png';
import item_05_R_get from '../static/images/items/item_05_R_get.png';
import item_05_G_get from '../static/images/items/item_05_G_get.png';
import item_05_B_get from '../static/images/items/item_05_B_get.png';
import item_06 from '../static/images/items/item_06.png';
import item_06_Base from '../static/images/items/item_06_Base.png';
import item_06_get from '../static/images/items/item_06_get.png';
import item_06_R from '../static/images/items/item_06_R.png';
import item_06_G from '../static/images/items/item_06_G.png';
import item_06_B from '../static/images/items/item_06_B.png';
import item_06_R_get from '../static/images/items/item_06_R_get.png';
import item_06_G_get from '../static/images/items/item_06_G_get.png';
import item_06_B_get from '../static/images/items/item_06_B_get.png';
import item_07 from '../static/images/items/item_07.png';
import item_08 from '../static/images/items/item_08.png';
import item_09 from '../static/images/items/item_09.png';
import item_10 from '../static/images/items/item_10.png';
import item_11 from '../static/images/items/item_11.png';
import item_12 from '../static/images/items/item_12.png';
import item_13 from '../static/images/items/item_13.png';
import item_14 from '../static/images/items/item_14.png';
import item_15 from '../static/images/items/item_15.png';
import item_16 from '../static/images/items/item_16.png';
import item_17 from '../static/images/items/item_17.png';
import item_18 from '../static/images/items/item_18.png';
import item_19 from '../static/images/items/item_19.png';
import item_20 from '../static/images/items/item_20.png';
import item_21 from '../static/images/items/item_21.png';
import item_22 from '../static/images/items/item_22.png';

export const Imglist = {
	btn,
	icon_menu,
	menu_close,
	menu_bg,
	btn_enter,
	condition_frame,
	other_bg,
	title_frame,
	frame,
	bg_main,
	frame_costume,

	preview_1,
	preview_2,
	preview_3,
	preview_4,

	diamond,
	buy_soul_button,
	buy_soul_border,
	buy_soul_char,


	combine_arrow,
	redeem_plus_btn,
	redeem_line_btn,
	btn_next,
	btn_prev,

	item_01_B_get,
	item_01_B,
	item_01_Base,
	item_01_G_get,
	item_01_G,
	item_01_get,
	item_01_R_get,
	item_01_R,
	item_01,
	item_02_B_get,
	item_02_B,
	item_02_Base,
	item_02_G_get,
	item_02_G,
	item_02_get,
	item_02_R_get,
	item_02_R,
	item_02,
	item_03_B_get,
	item_03_B,
	item_03_Base,
	item_03_G_get,
	item_03_G,
	item_03_get,
	item_03_R_get,
	item_03_R,
	item_03,
	item_04_B_get,
	item_04_B,
	item_04_Base,
	item_04_G_get,
	item_04_G,
	item_04_get,
	item_04_R_get,
	item_04_R,
	item_04,
	item_05_B_get,
	item_05_B,
	item_05_Base,
	item_05_G_get,
	item_05_G,
	item_05_get,
	item_05_R_get,
	item_05_R,
	item_05,
	item_06_B_get,
	item_06_B,
	item_06_Base,
	item_06_G_get,
	item_06_G,
	item_06_get,
	item_06_R_get,
	item_06_R,
	item_06,
	item_07,
	item_08,
	item_09,
	item_10,
	item_11,
	item_12,
	item_13,
	item_14,
	item_15,
	item_16,
	item_17,
	item_18,
	item_19,
	item_20,
	item_21,
	item_22,
}
