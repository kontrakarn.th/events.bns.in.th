import React from 'react';
import {connect} from 'react-redux';

import ModalMessage from './ModalMessage';
import ModalLoading from './ModalLoading';

const Modals = (props)=> (
    <>
        <ModalMessage />
        <ModalLoading />
    </>
)

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = {  };

export default connect( mapStateToProps, mapDispatchToProps )(Modals);
