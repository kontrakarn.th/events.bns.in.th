import React from 'react';
import styled from 'styled-components';
import {Imglist} from './../../constants/Import_Images';

export const Frame = props => {
    return (
        <ContentFrame className="\" cusPadding={props.cusPadding}>
            {props.title !== "" &&
                <div className="frame__title">{props.title}</div>
            }
            <div className="frame__content">
                {props.children && props.children}
            </div>
        </ContentFrame>
    )
}

const ContentFrame = styled.div`
    background: top center no-repeat url(${Imglist['frame']});
    width: 931px;
    height: 503px;
    display: block;
    margin: 0 auto;
    position: relative;
    box-sizing: border-box;
    padding: ${props=>props.cusPadding ? props.cusPadding : '7% 6%'}
    // padding: 7% 6%;
    .frame {
        &__title {
            background: top center no-repeat url(${Imglist['title_frame']});
            width: 313px;
            height: 61px;
            font-size: 1.8em;
            line-height: 1.8em;
            color: #ffffff;
            text-align: center;
            font-weight: 600;
            margin: 0 auto;
            position: absolute;
            top: 0;
            left: 50%;
            transform: translate3d(-50%,-30%,0);
            font-family: 'Kanit-SemiBold';
        }
        &__content {

        }
    }
`;
