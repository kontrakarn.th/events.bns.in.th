const  bg 						= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/vip_benefit/bg.jpg';
const  btn_home 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/vip_benefit/btn_home.png';
const  ico_scroll 				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/vip_benefit/ico_scroll.png';
const  icon_dropdown 			= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/vip_benefit/icon_dropdown.png';
const  header					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/vip_benefit/header.png'
const  item1					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/vip_benefit/item1.png'
const  item2					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/vip_benefit/item2.png'
const  item3					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/vip_benefit/item3.png'
const  btn_receive				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/vip_benefit/btn_receive.png'
const  btn_cancel				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/vip_benefit/btn_cancel.png'
const  btn_confirm				= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/vip_benefit/btn_confirm.png'
const  bg_modal					= 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/vip_benefit/bg_modal.png'

export const Imglist = {
	bg,
	btn_home,
	ico_scroll,
	icon_dropdown,
	header,
	item1,	
	item2,
	item3,
	btn_receive,
	btn_cancel,
	btn_confirm,
	bg_modal	
}
