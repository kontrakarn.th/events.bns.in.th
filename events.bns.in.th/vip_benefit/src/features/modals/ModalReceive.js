import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';
import {Imglist} from '../../constants/Import_Images';

const CPN = props => {
    return (
        <ModalCore
            modalName="receive"
            actClickOutside={()=>props.setValues({modal_open: "message"})}
        >
            <ModalMessageContent>
                <div className="contenttitle">ได้รับ</div>
                <div className="contenttext">
                    {
                        props.receive_items.map((item,key) => {
                            return(
                                <div key={key} className="items">
                                    <img src={item.key} className="gen__items" alt="" />
                                    <div className="itemsList__name">
                                        {item.product_title + ' x ' + item.amount} <br/>
                                    </div>
                                </div>
                            )})
                    }
                </div>

                <div>
                    <Btns className='confirm' onClick={() => props.setValues({modal_open: "message"})}></Btns>
                </div>
            </ModalMessageContent>

        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    // width: 476px;
    // height: 336px;
    width: 750px;
    //height: 357px;
    text-align: center;
    background: url(${Imglist['bg_modal_receive_items']}) top center / 100% 100% no-repeat;
    box-sizing: border-box;
    padding: 4% 8%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttitle{
        font-size: 21px;
        color: #fff;
        text-shadow: 0 0 10px #8d92e8;
        -webkit-text-fill-color: #fff;
        -webkit-text-stroke-width: 1px;
        -webkit-text-stroke-color: #b3b7fb;
        margin-bottom: 40px;
        font-family: "Kanit-Medium",tahoma;
    }
    .contenttext{
        //background: url(${Imglist['bg_modal_inner']}) top center / 100% 100% no-repeat;
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 21px;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 180px;
        //height: 100%;
        display: flex;
        justify-content: center;
        //align-items: center;
        margin-bottom: 10px;
        .items{
            padding: 0 10px;
            flex: 1;
        }
        font-family: "Kanit-Medium",tahoma;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
    .itemsList__name{
        color: #fff;
        text-shadow: 0 0 3px #8d92e8;
        -webkit-text-fill-color: #fff;
        -webkit-text-stroke-width: 1px;
        -webkit-text-stroke-color: #b3b7fb;
        font-size: 13px;
        line-height: 1.5;
    }
`;

const Btns = styled.div`
    width: 99px;
    height: 31px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #000;
    line-height: 36px;
    font-size: 20px;
    background-size: contain;
    &.confirm{
        background: no-repeat 0 0 url(${Imglist['btn_confirm']});
    }
    &.cancel{
        background: no-repeat 0 0 url(${Imglist['btn_cancel']});
    }
    &:hover{
        background-position: bottom center;
    }
`
