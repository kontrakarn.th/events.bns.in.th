import { apiPost } from '../middlewares/Api';
import { Imglist } from "../constants/Import_Images";

const initialState = {
    //--- event_info
    modal_open: "none",
    modal_message: "",
    modal_item_token: "",
    modal_confirmitem: '',
    modal_type: '',
    modal_item: '',
    modal_count: '',
};

export default (state = initialState, action) => {
    switch (action.type) {
        case "SET_VALUE":
            if (Object.keys(initialState).indexOf(action.key) >= 0) {
                return {
                    ...state,
                    [action.key]: action.value
                };
            } else {
                return state;
            }
        case "SET_VALUES": return { ...state, ...action.value };
        default:
            return state;
    }
};

export const setValue  = (key, value) =>{ return ({ type: "SET_VALUE", value, key }) };
export const setValues = (value) =>{ return ({ type: "SET_VALUES", value }) };

export const apiCheckin = (props) => {
    props.setValues({ modal_open: "loading" });
    let dataSend = {type:'event_info'};
    let successCallback = (data) => {
        if (data.status === true){
            props.setValues({
                ...data.data,
                modal_open: "",
                
            });
        }
    };
    const failCallback = (data) => {
        if (data.type === 'no_permission') {
            props.setValues({
                permission: false,
                modal_open: "",
            });
        }else {
            props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        }
    };
    apiPost("REACT_APP_API_POST_EVENT_INFO", props.jwtToken, dataSend, successCallback, failCallback);
}

export const actConfirm = (props) => {
    console.log(props.product_key)
    if (props.product_key === undefined || props.product_key === ""){
        props.setValues({
            modal_open:"message",
            modal_message: "ไม่สามารถแลกของไอเทมได้",
        });
    }

    props.setValues({ modal_open: "loading" });
    let dataSend = {type: "claim_vip_reward", key: props.product_key};
    let successCallback = (data) => {
        if (data.status) {

            props.setValues({
                ...data.data,
                modal_open: "message",
                modal_message: data.message,
                
            });
        }else{
            props.setValues({
                modal_open:"message",
                modal_message: data.message,
            });
        }
    };
    const failCallback = (data) => {
        props.setValues({
            modal_open:"message",
            modal_message: data.message,
        });
    };
    apiPost("REACT_APP_API_POST_CLAIM_REWARD", props.jwtToken, dataSend, successCallback, failCallback);
    // this.apiRedeem(
    //     this.props.modal_confirmitem && this.props.modal_confirmitem !== ''
    //     && this.props.modal_confirmitem
    // );
}
