import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import { apiPost } from '../../middlewares/Api';
import styled from 'styled-components';
import Logo from './BNSLogo';
import { setValues, onAccountLogout } from '../../store/redux';
import ModalLoading from '../modals/ModalLoading';
import ModalMessage from '../modals/ModalMessage';
import ModalConfirm from '../modals/ModalConfirm';
import {Imglist} from '../../constants/Import_Images';
import { apiCheckin, actConfirm } from '../redux';
import CountDown from '../countdown'

class Main extends React.Component {

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false,
        }
    }

    confirmOpen = (title, count, _key) => {
        this.props.setValues({
            modal_open: 'confirm',
            modal_message:'ต้องการรับของรางวัล '+ title + ' x'+count+'?',
            product_key: _key
        });
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            if (!this.props.uid) {
                apiCheckin(this.props);
            }
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            if (!this.props.uid) {
                apiCheckin(this.props);
            }
        }
    }

    render() {
        // let quests = this.props.quests || [];
        return (
            <PageWrapper>
                <Logo/>
                <Content>
                    <div className="vip_status">
                        สถานะ VIP: &nbsp; 
                        { 
                            this.props.vip_active == true ?
                                <span className="vip__status active">Active</span>
                            :
                                <span className="vip__status inactive">Inactive</span>
                        } &nbsp; 
                        VIP Level: { this.props.vip_level } &nbsp; 
                        ระยะเวลาคงเหลือ: &nbsp; 
                        {
                            this.props.vip_remain_date != "" ?
                                <CountDown deadline={this.props.vip_remain_date}/>
                            :
                                ""
                        }
                        
                    </div>
                    <img src={Imglist.header}/>
                    <div className="wrapper"> 
                        {
                            this.props.vip_run_effect && <div key={this.props.vip_run_effect.key} className="box">
                                <img src={this.props.vip_run_effect.image}/>
                                <div className="item_countdown">
                                    {
                                        this.props.vip_run_effect.next_receive_date != "" ?
                                            <CountDown deadline={this.props.vip_run_effect.next_receive_date}/>
                                        :
                                            ""
                                    }
                                </div>
                                {
                                    this.props.vip_run_effect.received == true ?
                                        <Btn className="disabled"></Btn>
                                    :
                                        (
                                            this.props.vip_run_effect.can_receive == true ?
                                                <Btn onClick={()=>this.confirmOpen(this.props.vip_run_effect.title, 1, this.props.vip_run_effect.key)}></Btn>
                                            :
                                                <Btn className="disabled"></Btn>
                                        )
                                }
                            </div>
                        }    
                        {
                            this.props.cold_storage_reset && <div key={this.props.cold_storage_reset.key} className="box">
                                <img src={this.props.cold_storage_reset.image}/>
                                <div className="item_countdown">
                                    {
                                        this.props.cold_storage_reset.next_receive_date != "" ?
                                            <CountDown deadline={this.props.cold_storage_reset.next_receive_date}/>
                                        :
                                            ""
                                    }
                                </div>
                                {
                                    this.props.cold_storage_reset.received == true ?
                                        <Btn className="disabled"></Btn>
                                    :
                                        (
                                            this.props.cold_storage_reset.can_receive == true ?
                                                <Btn onClick={()=>this.confirmOpen(this.props.cold_storage_reset.title, 1, this.props.cold_storage_reset.key)}></Btn>
                                            :
                                                <Btn className="disabled"></Btn>
                                        )
                                }
                            </div>
                        }
                        {
                            this.props.vip_gachapon_box && <div key={this.props.vip_gachapon_box.key} className="box">
                                <img src={this.props.vip_gachapon_box.image}/>
                                <div className="item_countdown">
                                    {
                                        this.props.vip_gachapon_box.next_receive_date != "" ?
                                            <CountDown deadline={this.props.vip_gachapon_box.next_receive_date}/>
                                        :
                                            ""
                                    }
                                </div>
                                {
                                    this.props.vip_gachapon_box.received == true ?
                                        <Btn className="disabled"></Btn>
                                    :
                                        (
                                            this.props.vip_gachapon_box.can_receive == true ?
                                                <Btn onClick={()=>this.confirmOpen(this.props.vip_gachapon_box.title, this.props.vip_gachapon_box.amount, this.props.vip_gachapon_box.key)}></Btn>
                                            :
                                                <Btn className="disabled"></Btn>
                                        )
                                }
                            </div>
                        }
                            
                    </div>
                </Content>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm
                  actConfirm={actConfirm.bind(this)}
                />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg']}) #050506;
    width: 1105px;
    // padding-top: 740px;
    padding-top: 85px;
    padding-bottom: 75px;
    text-align: center;
    position: relative;
    z-index: 20;
`

const Content = styled.div `
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    & img{
        max-width: 100%;
        max-height: 100%;
    }
    position: relative;
    .wrapper{
        display: flex;
        align-items: center;
        justify-content: center;
        > .box{
            width: 25%;
            position: relative;
        }
    }
    .vip_status{
        color: #fff;
        text-align: right;
        margin-top: 10px;
        margin-right: 20px;
    }
    .item_countdown{
        width: 100%;
        text-align: center;
        display: block;
        color: #fff;
        margin: 0 auto;
        position: absolute;
        top: 15px;
    }
    .vip__status{
        display: inline;

        &.active{
            color: green;
        }
        &.inactive{
            color: red;
        }
    }
`

// const Box = styled.div`
//         width: 33px;
//         height: 33px;
//         margin: 0 auto;
//         position: absolute;
//         left: 170px;
//         top: ${props=>props.start}px;
//         color: #ffffff;
//         line-height: 33px;
//         &:nth-child(2){
//             left: 416px;
//         }
//         &:nth-child(3n){
//             left: 662px;
//         }
//         &:nth-child(3n+4){
//             top: calc(${props=>props.start}px + ${props=>props.pos}px);
//             left: 170px;
//         }
//         &:nth-child(3n+5){
//              top: calc(${props=>props.start}px + ${props=>props.pos}px);
//              left: 416px;
//         }
//         &:nth-child(3n+6){
//              top: calc(${props=>props.start}px + ${props=>props.pos}px);
//         }
// `

const Btn = styled.div `
    background: top center no-repeat url(${Imglist['btn_receive']});
    width: 127px;
    height: 35px;
    cursor: pointer;
    margin: 0 auto;
    position: absolute;
    bottom: 12%;
    left: 0;
    right: 0;
    &:hover{
        background-position: center;
    }
    &.disabled{
        background-position: bottom center;
        cursor: none;
        pointer-events: none;
    }
`
