// import axios from "axios";
const initialState = {
    message: "",
    diamond : 0,
    remain_ticket: 0,
    remain_play: 0,
    remain_my_ticket: 0,
    canplay: false,
    gachas: [
        { id: 0, items: { img: "", name: "name0", desc: "desc0" } },
        { id: 1, items: { img: "", name: "name1", desc: "desc1" } },
        { id: 2, items: { img: "", name: "name2", desc: "desc2" } },
        { id: 3, items: { img: "", name: "name3", desc: "desc3" } },
        { id: 4, items: { img: "", name: "name4", desc: "desc4" } }
    ],
    coin_amount: 0,
    packages: [
        { id: 0, coin: 0, title: "title0", desc: "desc0", can_receive: false, receive: false },
        { id: 1, coin: 0, title: "title1", desc: "desc1", can_receive: false, receive: false },
        { id: 2, coin: 0, title: "title2", desc: "desc2", can_receive: false, receive: false },
        { id: 3, coin: 0, title: "title3", desc: "desc3", can_receive: false, receive: false },
        { id: 4, coin: 0, title: "title4", desc: "desc4", can_receive: false, receive: false },
        { id: 5, coin: 0, title: "title5", desc: "desc5", can_receive: false, receive: false },
    ]
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE":
      if (Object.keys(initialState).indexOf(action.key) >= 0) {
        return { ...state, ...action.value };
      } else {
        return state;
      }
    default:
      return state;
  }
};

export const setValue = (key, value) => ({ type: "SET_VALUE", value, key });
// export const submit = (fullName, email) => ({
//   type: "SUBMIT",
//   // payload: axios.post("http://www.mocky.io/v2/5bfbb9c23100002b0039b9c1")
//   payload: axios.post("http://www.mocky.io/v2/5bfbc91a310000730039ba6c")
// });
