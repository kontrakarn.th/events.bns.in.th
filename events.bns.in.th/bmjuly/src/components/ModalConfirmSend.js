import React from 'react';
import Modal from './Modal';

export default class ModalConfirmSend extends React.Component {


    render() {
        // console.log(this.props)
        return (
            <Modal
                open={this.props.open}
                title="ยืนยัน"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox">
                    <h3>ยืนยันการส่งแพ็คเกจ<br/>ค่าธรรมเนียมการส่ง 10,000 ไดมอนด์ <br/>ไปยัง UID {this.props.uidsend} ({this.props.namesend})</h3>
                </div>
            </Modal>
        )
  }
}
