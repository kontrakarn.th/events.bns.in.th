import React from 'react';
import Modal from './Modal';
import pack_1 from '../static/images/items/olympia_package1.png'
import pack_2 from '../static/images/items/olympia_package2.png'
import pack_3 from '../static/images/items/olympia_package3.png'

export default class ModalPackageDetails extends React.Component {
    render() {
        let title = this.props.data && this.props.data || [];
        return (
                <Modal
                    open={this.props.open}
                    title="แพ็คเกจโอลิมเปียแห่งอิสรภาพฯ"
                    confirmMode={false}
                    removeConfirmBtn={true}
                    actConfirm={()=>this.props.actConfirm()}
                    actClose={()=>this.props.actClose()}
                    page={'packageDetail'}
                >
                    <div onClick={()=>this.props.actClose()} className="icon-close">x</div>
                    <div className="modalPackage__wrapper">

                        <div className="itemsInline">
                            <div className="itemsInline__img">
                                <img src={pack_1}/>
                            </div>
                            <div className="itemsInline__content">
                                <div>ชุดโอลิมเปีย</div>
                                <div>(1)</div>
                            </div>
                        </div>
                        <div className="itemsInline">
                            <div className="itemsInline__img">
                                <img src={pack_2}/>
                            </div>
                            <div className="itemsInline__content">
                                <div>เครื่องประดับศีรษะโอลิมเปีย</div>
                                <div>(1)</div>
                            </div>
                        </div>
                        <div className="itemsInline">
                            <div className="itemsInline__img">
                                <img src={pack_3}/>
                            </div>
                            <div className="itemsInline__content">
                                <div>ปีกแห่งอิสรภาพที่เปล่งประกาย</div>
                                <div>(1)</div>
                            </div>
                        </div>
                    </div>
                    <div className="inbox" dangerouslySetInnerHTML={{__html: this.props.msg}} />
                </Modal>
        )
  }
}
