import bg from '../static/images/bg.jpg';
import board_1 from '../static/images/board_1.png';
import board_2 from '../static/images/board_2.png';
import board_3 from '../static/images/board_3.png';
import magnify_icon from '../static/images/magnify_icon.png';
import character_1 from '../static/images/character_1.png';
import character_2 from '../static/images/character_2.png';
import character_3 from '../static/images/character_3.png';
import btn_active from '../static/images/btn_active.png';
import btn from '../static/images/btn.png';
import button_condition from '../static/images/button_condition.png';
import item_1 from '../static/images/item_1.png';
import item_2 from '../static/images/item_2.png';
import item_3 from '../static/images/item_3.png';
import item_4 from '../static/images/item_4.png';
import item_5 from '../static/images/item_5.png';
import item_6 from '../static/images/item_6.png';
import item_7 from '../static/images/item_7.png';
import item_8 from '../static/images/item_8.png';
export const Imglist = {
	bg,
	board_1,
	board_2,
	board_3,
	magnify_icon,
	character_1,
	character_2,
	character_3,
	btn_active,
	btn,
	button_condition,
	item_1,
	item_2,
	item_3,
	item_4,
	item_5,
	item_6,
	item_7,
	item_8,
}