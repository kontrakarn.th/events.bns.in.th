import React from 'react';
import './../styles/index.css';
import './../styles/modal.css';
import './../styles/f11layout.css';
import F11Layout from './../features/F11Layout/';
import RankContent from './../features/rank/' ;

export class Rank extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={false}
                noScroll={true}
            >
                <RankContent/>
            </F11Layout>
        )
    }
}
