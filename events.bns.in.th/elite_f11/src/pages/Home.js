import React from 'react';
import './../styles/index.css';
import './../styles/modal.css';
import './../styles/f11layout.css';
import F11Layout from './../features/F11Layout/';
import Main from './../features/main/' ;

export class Home extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={false}
            >
                <Main/>
            </F11Layout>
        )
    }
}
