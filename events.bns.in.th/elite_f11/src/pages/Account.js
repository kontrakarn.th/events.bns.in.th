import React from 'react';
import './../styles/index.css';
import './../styles/modal.css';
import './../styles/f11layout.css';
import F11Layout from './../features/F11Layout/';
import AccountContent from './../features/account/' ;

export class Account extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={false}
                noScroll={true}
            >
                <AccountContent/>
            </F11Layout>
        )
    }
}
