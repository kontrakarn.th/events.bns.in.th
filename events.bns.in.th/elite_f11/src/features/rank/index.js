import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import 'whatwg-fetch';
import { apiPost } from './../../middlewares/Api';
import { setValues } from "../../features/redux";
import Navbar from '../../features/navbar/' ;
import { imgList } from '../../constants/Import_Images';

class Rank extends Component {
    constructor(props) {
        super(props)
    }

    getRanking(){
        this.props.setValues({'modal_open':'loading'})

        let dataSend = {type:'ranking'};
        let successCallback = (data) => {
            this.props.setValues({...data.data,'modal_open':'','modal_message':''})
        };
        const failCallback = (data) => {
            this.props.setValues({'modal_open':'message','modal_message':data.message})
        };
        setTimeout(()=>{
            apiPost("REACT_APP_API_POST_RANKING", this.props.jwtToken, dataSend, successCallback, failCallback);
        },800)
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.getRanking();
        }
    }

    componentDidMount() {
        if(this.props.jwtToken && this.props.jwtToken !== ""){
            this.getRanking();
        }
    }

    render() {
        return (
            <div className="events-bns">
                <RankContent className="rank">
                    <Navbar/>
                    <div className="rank__inner">
                        <div className="rank__board">
                            <div className="rank__box rank__box--1">
                                <img src={imgList.rank_1}/>
                                <div className="rank__text">{this.props.ranking && this.props.ranking.length>0 && this.props.ranking[0].nickname}</div>
                            </div>
                            <div className="rank__box rank__box--2">
                                <img src={imgList.rank_2}/>
                                <div className="rank__text">{this.props.ranking && this.props.ranking.length>1 && this.props.ranking[1].nickname}</div>
                            </div>
                            <div className="rank__box rank__box--2">
                                <img src={imgList.rank_3}/>
                                <div className="rank__text">{this.props.ranking && this.props.ranking.length>2 && this.props.ranking[2].nickname}</div>
                            </div>
                            <img className="rank__label" src={imgList.label_rank}/>
                            <div className="rank__table">
                                <div className="rank__head">
                                    <div>อันดับ</div>
                                    <div>ชื่อ</div>
                                    <div>ระดับ Elite</div>
                                </div>
                                <div className="rank__body">
                                {
                                    this.props.ranking && this.props.ranking.length>0 && this.props.ranking.map((item,key)=>{
                                        return (
                                            <div className="rank__col">
                                            <div>{key+1}</div>
                                            <div>{item.nickname}</div>
                                            <div>{this.props.vip_list[item.vip]}</div>
                                        </div>)
                                    })
                                }
                                </div>
                            </div>
                        </div>
                    </div>
                </RankContent>
            </div>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = {
    setValues
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Rank)

const RankContent = styled.div`
    width: 100%;
    background: url(${imgList.bg_rank}) no-repeat top center;
    background-size: cover;
    padding-bottom: 8%;
    @media(max-width: 640px) {
        background: url(${imgList.bg_rank_mb}) no-repeat top center;
        background-size: cover;
    }
    .rank{
        &__inner{
            padding-top: 13%;
            @media(max-width: 640px) {
                padding-top: 80px;
            }
        }
        &__board{
            border: 1px solid #84611e;
            border-radius: 5px;
            width: 90%;
            max-width: 803px;
            margin: 0 auto;
            text-align: center;
            padding-bottom: 7%;
            background: #000;
            @media(max-width: 640px) {
                border: none;
                width: 100%;
                background: none;
            }
        }
        &__box{
            position: relative;
            width: 40%;
            margin: 0 auto;
            >img{
                width: 100%;
                display: block;
            }
            &--1{
                margin-bottom: -20%;
            }
            &--2{
                display: inline-block;
                margin: 0 5%;
            }
        }
        &__text{
            width: 43%;
            position: absolute;
            top: 64%;
            left: 47%;
            transform: translate(-50%,0);
            color: #fff;
            @media(max-width: 1024px) {
                font-size: 1.6vw;
            }
        }
        &__label{
            width: 36%;
            margin-top: 5%;
        }
        &__table{
            width: 70%;
            margin: 6% auto 0;
            @media(max-width: 640px) {
                width: 85%;
            }
        }
        &__head{
            background: rgba(243,251,191,1);
            background: -moz-linear-gradient(left, rgba(243,251,191,1) 0%, rgba(255,241,160,1) 87%, rgba(255,241,160,1) 100%);
            background: -webkit-gradient(left top, right top, color-stop(0%, rgba(243,251,191,1)), color-stop(87%, rgba(255,241,160,1)), color-stop(100%, rgba(255,241,160,1)));
            background: -webkit-linear-gradient(left, rgba(243,251,191,1) 0%, rgba(255,241,160,1) 87%, rgba(255,241,160,1) 100%);
            background: -o-linear-gradient(left, rgba(243,251,191,1) 0%, rgba(255,241,160,1) 87%, rgba(255,241,160,1) 100%);
            background: -ms-linear-gradient(left, rgba(243,251,191,1) 0%, rgba(255,241,160,1) 87%, rgba(255,241,160,1) 100%);
            background: linear-gradient(to right, rgba(243,251,191,1) 0%, rgba(255,241,160,1) 87%, rgba(255,241,160,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f3fbbf', endColorstr='#fff1a0', GradientType=1 );
            padding: 2% 0;
            font-family: 'Kanit-SemiBold','tahoma';
            >div{
                display: inline-block;
                width: 25%;
                &:nth-child(2){
                    width: 50%;
                }
            }
        }
        &__body{
            padding: 2% 0;
            background: #1d1e1e;
        }
        &__col{
            padding: .5% 0;
            color: #fff;
            >div{
                display: inline-block;
                width: 25%;
                &:nth-child(2){
                    width: 50%;
                }
            }
        }
    }
`;
