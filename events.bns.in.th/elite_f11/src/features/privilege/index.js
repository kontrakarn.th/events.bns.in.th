import React from 'react';
import styled from 'styled-components';
import { imgList } from '../../constants/Import_Images';
export class Privilege extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <PrivilegeContent className="privilege1">
                <div className="privilege1__label">
                    <img src={imgList.label}/>
                    <span>สิทธิประโยชน์</span>
                </div>
                <img className="privilege1__image-privilege" src={imgList.privilege}/>
            </PrivilegeContent>
        )
    }
}

const PrivilegeContent = styled.div`
    position: relative;
    box-sizing: border-box;
    width: 90%;
    max-width: 802px;
    margin: 0 auto;
    padding: 70px 100px 50px;
    border: 1px solid #84611e;
    border-radius: 5px;
    background: #000;
    text-align: center;
    @media(max-width: 640px) {
        width: 100%;
        border-right: unset;
        border-left: unset;
        border-radius: unset;
        padding: 7% 4% 5%;
    }
    .privilege1{
        &__label{
            width: 34%;
            position: absolute;
            left: 50%;
            transform: translate(-50%,0);
            top: -23px;
            >img{
                display: block;
                margin: 0 auto;
            }
            >span{
                color: #30190a;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                width: 100%;
                text-align: center;
                font-family: 'Kanit-SemiBold','tahoma';
                font-size: 1.2em;
            }
            @media(max-width: 802px) {
                top: -5.5%;
            }
            @media(max-width: 640px) {
                width: 50%;
                font-size: 2.3vw;
                top: -4vw;
            }
        }
        &__image-privilege{
            margin-top: -10%;
            width: 100%;
        }
    }
`;