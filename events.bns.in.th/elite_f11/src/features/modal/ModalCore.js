import React from 'react'
import styles from './styles.module.scss';
import {setValues , actCloseModal} from './redux';
import { connect } from "react-redux";

const ModalCore =(props)=>{
	//input
	let {name,outSideClick} = props;
	//redux
	return(
			<div className={[styles.modal ,props.modal_open === name ? styles.open : styles.close].join(' ')}>
				<div className={[styles.modal__backdrop, props.modal_open === name ? styles.bopen : styles.bclose].join(' ')} onClick={()=>{outSideClick && props.actCloseModal() }} />
				<div className={styles.modal__content}>
					{props.children && props.children }
				</div>
			</div>
	)
}

const mapStateToProps = state => ({
    ...state.Modal
});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect( 
    mapStateToProps, 
    mapDispatchToProps 
)(ModalCore);