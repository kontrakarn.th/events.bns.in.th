import MessageModal from './MessageModal';
import LoadingModal from './LoadingModal';
import ConfirmModal from './ConfirmModal';
import EditModal from './EditModal';
import ReceiveModal from './ReceiveModal';
import SentModal from './SentModal';
import UidModal from './UidModal';

export {
	MessageModal,
	LoadingModal,
	ConfirmModal,
	EditModal,
	ReceiveModal,
	SentModal,
	UidModal
}
