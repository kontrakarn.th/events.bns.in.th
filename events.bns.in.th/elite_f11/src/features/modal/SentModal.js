import React from 'react';
import  ModalCore from './ModalCore.js';
import { connect } from "react-redux";
import {setValues , actCloseModal} from './redux';
import { imgList } from '../../constants/Import_Images';
import { apiPost } from './../../middlewares/Api';

const SentModal = (props) => {

	const buyOwner = ()=>{
		props.setValues({'modal_open':'loading'})
		let dataSend = {
            type:'buy_excusive_owner',
            id:props.select_shop_id,
        };
		let successCallback = (data) => {
			props.setValues({...data.data,item_get:data.item_get,'modal_open':'receive','modal_message':data.message})
		};
		const failCallback = (data) => {
			props.setValues({'modal_open':'message','modal_message':data.message})
		};

        apiPost("REACT_APP_API_POST_BUY_OWNER", props.jwtToken, dataSend, successCallback, failCallback);
	}

	return (
		<ModalCore name="sent" outSideClick={true}>
			<div className="contentwrap contentwrap--message">
				<img className="label-top label-top--message" src={imgList.label_reward} alt=''/>
				<div className="inner inner--message">
					<div className="text text--message">
                        คุณได้เลือกซื้อ {props.excusive_item_prev && props.excusive_item_prev.length>0 && props.select_shop_index>=0 &&  props.excusive_item_prev[props.select_shop_index].product_title}
					</div>
				</div>
				<div className="btn-group btn-group--big">
                    <div className="receive receive--big" onClick={() => buyOwner()}/>
                    <div className="sent sent--big" onClick={() => props.setValues({ modal_open: 'uid' })}/>
                </div>
			</div>
		</ModalCore>
	)
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = { setValues ,actCloseModal};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SentModal);
