import React from "react";
import { connect } from "react-redux";
import ModalCore from "./ModalCore.js";
import './styles.scss';
import { setValues, actCloseModal } from "./redux";
import { imgList } from '../../constants/Import_Images';
const ConfirmModal = (props) => {
	return (
    <ModalCore name="confirm" outSideClick={true}>
      	<div className="contentwrap contentwrap--message">
				<img className="label-top label-top--message" src={imgList.label_confirm} alt=''/>
				<div className="inner inner--message">
					<div className="text text--message" dangerouslySetInnerHTML={{ __html: props.modal_message }}>
					</div>
				</div>
				<div className="btn-group btn-group--big">
                    <div className="agree agree--big" onClick={() => props.handleConfrim()}/>
					<div className="notok notok--big" onClick={() => props.setValues({ modal_open: '' })}/>
                </div>
			</div>
    </ModalCore>
	)
}

const mapStateToProps = state => ({
  ...state.ModalReducer,
  ...state.Modal
});

const mapDispatchToProps = { setValues, actCloseModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmModal);
