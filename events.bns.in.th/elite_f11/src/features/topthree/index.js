import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { imgList } from '../../constants/Import_Images';
import { setValues } from "../../features/redux";

class TopThree extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <TopThreeContent className="topthree">
                <div className="topthree__label-top">
                    <img className="topthree__label" src={imgList.label_topthree} alt=''/>
                </div>
                <div className="topthree__text">
                    ได้รับ
                </div>
                <img className="topthree__imgitem" src={this.props.ranking_3_item.length>0 && this.props.ranking_3_item[0].icon} alt=''/>
                <div className="topthree__text">
                    {this.props.ranking_3_item.length>0 && this.props.ranking_3_item[0].product_title}
                </div>
                <img className="topthree__line-end" src={imgList.line_end} alt=''/>
                <div className="topthree__text topthree__text--big">
                    ตัวอย่าง {this.props.ranking_3_item.length>0 && this.props.ranking_3_item[0].product_title}
                </div>
                <img src={this.props.ranking_3_item.length>0 && this.props.ranking_3_item[0].example_img} alt=''/>
            </TopThreeContent>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = {
    setValues
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TopThree)

const TopThreeContent = styled.div`
    position: relative;
    box-sizing: border-box;
    width: 100%;
    margin: 24% auto 0;
    padding: 70px 30px 0;
    border: 1px solid #84611e;
    border-radius: 5px;
    background: #000;
    text-align: center;
    @media(max-width: 640px) {
        width: 100%;
        border-right: unset;
        border-left: unset;
        border-radius: unset;
        padding: 7% 4% 5%;
    }
    .topthree{
        &__label-top{
            width: 50%;
            position: absolute;
            left: 50%;
            transform: translate(-50%,0);
            top: -93px;
            >img{
                display: block;
                margin: 0 auto;
            }
            >span{
                color: #30190a;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                width: 100%;
                text-align: center;
                font-family: 'Kanit-SemiBold','tahoma';
                font-size: 1.2em;
            }
            @media(max-width: 640px) {
                width: 50%;
                font-size: 2.3vw;
                top: -13vw;
            }
        }
        &__list{
            width: 100%;
        }
        &__line-end{
            margin: 0 0 6% 0;
        }
        &__text{
            color: #aa9b7c;
            font-size: .8em;
            margin: 0 0 7% 0;
            &--big{
                font-size: 1em;
            }
            @media(max-width: 1024px) {
                font-size: .7em;
            }
            @media(max-width: 750px) {
                font-size: 1.3vw;
            }
            @media(max-width: 640px) {
                font-size: 2vw;
            }
        }
        &__imgitem{
            width: 14%;
            margin: 2% 0;
        }
    }
`;
