import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { imgList } from '../../constants/Import_Images';
import ItemShop from '../../features/itemshop';
import { setValues } from "../../features/redux";

class ShopShow extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <ShopContent className="shop">
                <div className="shop__label-top">
                    <img className="shop__label" src={imgList.label} alt=''/>
                    <span>ประจำเดือน{this.props.month_list[this.props.month]}</span>
                </div>
                <div className="shop__list">
                    {this.props.excusive_item && this.props.excusive_item.length>0 && this.props.excusive_item.map((item, key) => {
                        return (
                            <ItemShop
                                key={"excusive_shop_"+key}
                                itemId={item.id}
                                item_key={key}
                                text={item.product_title}
                                icon={item.icon}
                                price={item.price}
                                buy={()=>{}}
                                show_only={this.props.show_only}
                                page="shop"
                            />
                        );
                    })}
                </div>
                <img className="shop__line-end" src={imgList.line_end} alt=''/>
            </ShopContent>
        )
    }
}

const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer,...state.Modal});

const mapDispatchToProps = {
    setValues
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ShopShow)

const ShopContent = styled.div`
    position: relative;
    box-sizing: border-box;
    width: 100%;
    margin: 3% auto 0;
    padding: 70px 100px 50px;
    border: 1px solid #84611e;
    border-radius: 5px;
    background: #000;
    text-align: center;
    @media(max-width: 640px) {
        width: 100%;
        border-right: unset;
        border-left: unset;
        border-radius: unset;
        padding: 7% 4% 5%;
    }
    .shop{
        &__label-top{
            width: 34%;
            position: absolute;
            left: 50%;
            transform: translate(-50%,0);
            top: -23px;
            >img{
                display: block;
                margin: 0 auto;
            }
            >span{
                color: #30190a;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                width: 100%;
                text-align: center;
                font-family: 'Kanit-SemiBold','tahoma';
                font-size: 1.2em;
            }
            @media(max-width: 640px) {
                width: 50%;
                font-size: 2.3vw;
                top: -4vw;
            }
        }
        &__diamonds{
            color: #fff;
        }
        &__list{
            width: 100%;
        }
        &__line-end{
            margin-top: 9%;
        }
    }
`;
