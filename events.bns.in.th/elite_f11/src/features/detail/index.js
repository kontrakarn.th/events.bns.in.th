import React from 'react';
import styled from 'styled-components';
import { imgList } from '../../constants/Import_Images';
export class Detail extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <DetailContent className="detail">
                <div className="detail__label">
                    <img src={imgList.label}/>
                    <span>รายละเอียดโปรโมชั่น</span>
                </div>
                <div className="detail__list">
                    1. ผู้เล่นแลกไดมอนด์เข้าเกมเพื่อที่จะสะสมคะแนน ใช้ในการเลื่อนยศ ซึ่งมี 3 ยศ (Guardian, Legend, Divine)
                </div>
                <div className="detail__list">
                    2. ระบบจะนับตั้งแต่วันที่ 1 - 30 ของทุกเดือน แต่สิทธิ์ Elite HM Master จะใช้ได้ในเดือนถัดไป
                </div>
                <div className="detail__list">
                    3. ก่อนเริ่มโปรโมชั่นนี้ ผู้เล่นจะต้องลงทะเบียน ใส่วัน เดือน ปี เกิด เพศ อีเมล เพื่อของขวัญในวันเกิด
                </div>
                <div className="detail__list">
                    4. Special item  และ Exclusive item จะมีอายุ 30 วัน นับจากวันที่กดรับไอเทม
                </div>
                <div className="detail__list">
                    5 ยศทั้งหมดจะรีเซ็ททุกเดือน
                </div>
            </DetailContent>
        )
    }
}

const DetailContent = styled.div`
    position: relative;
    box-sizing: border-box;
    width: 90%;
    max-width: 802px;
    margin: 0 auto;
    padding: 70px 100px 50px;
    border: 1px solid #84611e;
    border-radius: 5px;
    background: #000;
    @media(max-width: 640px) {
        padding: 7% 4% 5%;
    }
    .detail{
        &__label{
            width: 34%;
            position: absolute;
            left: 50%;
            transform: translate(-50%,0);
            top: -23px;
            >img{
                display: block;
            }
            >span{
                color: #30190a;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                width: 100%;
                text-align: center;
                font-family: 'Kanit-SemiBold','tahoma';
                font-size: 1.2em;
            }
            @media(max-width: 802px) {
                top: -8%;
            }
            @media(max-width: 640px) {
                width: 50%;
                font-size: 2.3vw;
                top: -4vw;
            }
        }
        &__list{
            font-size: .8em;
        }
    }
`;