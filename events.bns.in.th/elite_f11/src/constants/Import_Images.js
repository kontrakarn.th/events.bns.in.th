const iconHome = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/buttons/btn_home.png';
const iconScroll = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/icons/icon_scroll.png';
const logo = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/logo.png';
const btn_menu = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/btn_menu.png';
const btn_close = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/btn_close.png';
const banner = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/banner.jpg';
const banner_mb = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/banner_mb.jpg';
const label = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label.jpg';
const privilege = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/privilege.png';
const label_daliy = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label_daliy.png';
const label_birthday = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label_birthday.png';
const label_weekly = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label_weekly.png';
const rewards_daliy = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/rewards_daliy.png';
const rewards_birthday = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/rewards_birthday.png';
const rewards_weekly = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/rewards_weekly.png';
const line_end = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/line_end.png';
const label_shop = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label_shop.png';
const item_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/item_1.png';
const item_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/item_2.png';
const item_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/item_3.png';
const item_4 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/item_4.png';
const item_5 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/item_5.png';
const item_6 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/item_6.png';
const item_7 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/item_7.png';
const item_mock = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/item_mock.png';
const circle = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/circle.png';
const btn_claim = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/btn_claim.png';
const btn_buy = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/btn_buy.png';
const diamond = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/diamond.png';
const label_divine = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label_divine.png';
const label_topten = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label_topten.png';
const label_topthree = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label_topthree.png';
const ex_topten = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/ex_topten.png';
const ex_topthree = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/ex_topthree.png';
const dot_active = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/dot_active.png';
const dot = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/dot.png';
const line_float = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/line_float.png';
const rank_1 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/rank_1.png';
const rank_2 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/rank_2.png';
const rank_3 = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/rank_3.png';
const label_rank = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label_rank.png';
const bg_rank = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/bg_rank.jpg';
const bg_rank_mb = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/bg_rank_mb.jpg';
const divine = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/divine.png';
const guardian = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/guardian.png';
const legend = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/legend.png';
const icon_loading = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/icon_loading.png';
const line_progress = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/line_progress.png';
const line_account = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/line_account.png';
const btn_edit = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/btn_edit.png';
const bg_account = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/bg_account.png';
const label_error = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label_error.png';
const loading_lyn = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/loading_lyn.png';
const label_confirm = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label_confirm.png';
const label_reward = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label_reward.png';
const btn_receive = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/btn_receive.png';
const btn_sent = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/btn_sent.png';
const label_sent = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/label_sent.png';
const bg_reward = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/bg_reward.jpg';
const bg_exclusive = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/bg_exclusive.jpg';
const frame_empty = 'https://cdngarenanow-a.akamaihd.net/webth/bns/events/hmelite_2019_f11/images/elite/frame_empty.png';

export const imgList = {
	iconHome,
	iconScroll,
	logo,
	btn_menu,
	btn_close,
	banner,
	banner_mb,
	label,
	privilege,
	label_daliy,
	label_birthday,
	label_weekly,
	rewards_daliy,
	rewards_birthday,
	rewards_weekly,
	line_end,
	label_shop,
	item_1,
	item_2,
	item_3,
	item_4,
	item_5,
	item_6,
	item_7,
	item_mock,
	circle,
	btn_claim,
	btn_buy,
	diamond,
	label_divine,
	label_topten,
	label_topthree,
	ex_topten,
	ex_topthree,
	dot_active,
	dot,
	line_float,
	rank_1,
	rank_2,
	rank_3,
	label_rank,
	bg_rank,
	bg_rank_mb,
	divine,
	guardian,
	legend,
	icon_loading,
	line_progress,
	line_account,
	btn_edit,
	bg_account,
	label_error,
	label_confirm,
	label_reward,
	btn_receive,
	btn_sent,
	label_sent,
	bg_reward,
	bg_exclusive,
	frame_empty,
	loading_lyn,
}

export const vip_img_list = {
	1:guardian,
	2:legend,
	3:divine
}
