import coupon_ribbon from '../static/images/event/coupon_ribbon.png';
import detail_ribbon from '../static/images/event/detail_ribbon.png';
import exchange_character from '../static/images/event/exchange_character.png';
import item_ribbon from '../static/images/event/item_ribbon.png';

//item
import gacha_1 from '../static/images/event/item/gacha_1.png';
import gacha_2 from '../static/images/event/item/gacha_2.png';
import gacha_3 from '../static/images/event/item/gacha_3.png';
import gacha_4 from '../static/images/event/item/gacha_4.png';
import gacha_5 from '../static/images/event/item/gacha_5.png';
import gacha_6 from '../static/images/event/item/gacha_6.png';
import gacha_7 from '../static/images/event/item/gacha_7.png';
import gacha_8 from '../static/images/event/item/gacha_8.png';
import gacha_9 from '../static/images/event/item/gacha_9.png';
import gacha_10 from '../static/images/event/item/gacha_10.png';
import gacha_11 from '../static/images/event/item/gacha_11.png';
import gacha_12 from '../static/images/event/item/gacha_12.png';
import gacha_13 from '../static/images/event/item/gacha_13.png';
import preview_1 from '../static/images/event/item/preview_1.png';
import preview_2 from '../static/images/event/item/preview_2.png';
import preview_3 from '../static/images/event/item/preview_3.png';
import preview_4 from '../static/images/event/item/preview_4.png';

import modal_fastfood from '../features/modals/images/modal_fastfood.jpg';
import modal_dessert from '../features/modals/images/modal_dessert.jpg';
import modal_drinks from '../features/modals/images/modal_drinks.jpg';

export const Imglist = {
	coupon_ribbon,
	detail_ribbon,
	exchange_character,
	item_ribbon,
	gacha_1,
	gacha_2,
	gacha_3,
	gacha_4,
	gacha_5,
	gacha_6,
	gacha_7,
	gacha_8,
	gacha_9,
	gacha_10,
	gacha_11,
	gacha_12,
	gacha_13,
	preview_1,
	preview_2,
	preview_3,
	preview_4,
	modal_fastfood,
	modal_dessert,
	modal_drinks,
}