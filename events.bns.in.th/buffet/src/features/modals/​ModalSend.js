import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../main/redux';

import imgBtnConfirm from './images/btn_confirm.png';
import imgBtnCancel from './images/btn_cancel.png';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="send"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <h2>กรอก UID</h2>
                <InputSend  name='input_message'
                type="text"
                ref={input_message => input_message && input_message.focus()}
                onChange={(e) =>props.setValues({input_message:e.target.value})}/>
            </ModalMessageContent>
            <ModalBottom>
                <Btns className={props.input_message === '' && 'disable'} onClick={()=>props.setValues({modal_open:"confirmsend",
                    modal_message:`ยืนยันการส่งไอเทม<br/>ค่าธรรมเนียมการส่ง<br/>
                    10,000 ไดมอนด์<br/>
                    ไปยัง UID ${props.input_message}`})}>
                        ตกลง
                </Btns>
                <Btns onClick={()=>props.setValues({modal_open:""})} >
                        ยกเลิก
                </Btns>
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.EventReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 396px;
    height: 415px;
    text-align: center;
    color: #FFFFFF;
    h2 {
        font-family: DBXtypeX;
        position: relative;
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-family: DBXtypeX;
        position: relative;
        top: 140px;
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
`;
const ModalBottom = styled.div`
    position: relative;
    display: inline-block;
    left: 50%;
    transform: translate3d(-50%,-50%,0);

`;
const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 144px;
    height: 50px;
    color: #fdf2e0;
    position:relative;
    text-align: center;
    box-sizing: border-box;
    padding-top: 3%;
    font-size: 24px;
    font-family: DBXtypeX;
    &:hover{
        background-position: bottom center;
    }
`;
const InputSend = styled.input`
        display: block;
        background: rgba(0,0,0,0);
        border: 0;
        position: relative;
        top: 45%;
        width: 70%;
        left: 50%;
        color: #543c24;
        height: fit-content;
        box-sizing: border-box;
        padding: 2% 0;
        transform: translate3d(-50%,-50%,0);
        font-size: 30px;
        text-align:center;
        overflow:hidden;
        &:focus{
            outline: none;
        }
`
