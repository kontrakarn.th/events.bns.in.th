import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../main/redux';

import modal_bg from './images/modal_bg.jpg';
import btn_confirm from './images/btn_confirm.png';
import btn_cancel from './images/btn_cancel.png';
import confirm_fastfood from './images/confirm_fastfood.png';
import confirm_dessert from './images/confirm_dessert.png';
import confirm_drinks from './images/confirm_drinks.png';

const CPN = props => {
    let {modal_message} = props;
    let confirmImg ={confirm_fastfood,confirm_dessert,confirm_drinks};
    return (
        <ModalCore
            modalName="confirm"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                    <img key={'confirm'+props.modal_preview} src={confirmImg[`confirm_${props.modal_preview}`]} alt=""/>
            </ModalMessageContent>
            <ModalBottom>
                <Btns onClick={()=>{if(props.actConfirm) props.actConfirm()}} />
                <Btns className={'cancel'} onClick={()=>props.setValues({modal_open:""})} />
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.EventReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 628px;
    height: 472px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${modal_bg});
    display:flex;
    justify-content: center;
    align-items: center;
    h2 {
        font-family: DBXtypeX;
        position: relative;
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-family: DBXtypeX;
        position: relative;
        top: 140px;
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
`;
const ModalBottom = styled.div`
    position: relative;
    display: inline-block;
    left: 50%;
    transform: translate3d(-50%,-50%,0);

`;
const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 214px;
    height: 67px;
    background:top center no-repeat url(${btn_confirm});
    position:relative;
    text-align: center;
    box-sizing: border-box;
    padding-top: 3%;
    &:hover{
        background-position: bottom center;
    }
    &.cancel{
        background:top center no-repeat url(${btn_cancel});
    }
`;
