import React from 'react';

import F11Layout from './../features/F11Layout/';
import Ranking from './../features/ranking';

export class RankingPage extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
            >
                <Ranking/>
            </F11Layout>
        )
    }
}
