import React from 'react';

import F11Layout from './../features/F11Layout/';
import Score from './../features/score';

export class ScorePage extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
            >
                <Score/>
            </F11Layout>
        )
    }
}
