import React from 'react';

import F11Layout from './../features/F11Layout/';
import Preview from './../features/preview';

export class PreviewPage extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={true}
            >
                <Preview/>
            </F11Layout>
        )
    }
}
