import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';
import iconDropdown from '../../static/images/icon_dropdown.png';
import {Imglist} from '../../constants/Import_Images';

const CPN = (props) => {
    const actSelectCharacter = (e)=>{
        // e.preventDefault();
        let indexCharacter = props.value_select;
        if(indexCharacter !== "" ){
            // console.log("actSelectCharacter oong",props);
            props.actSelectCharacter(indexCharacter);
        }else{
            props.setValues({
                modal_open:"message",
                modal_message: "โปรดเลือกตัวละคร",
                selected_char:false,
            });
        }
    }

    const handleChange = (e)=>{
        props.setValues({
            value_select : e.target.value,
        })
    }
    return (
        <ModalCore
            modalName="selectcharacter"
            actClickOutside={false}
        >
             <ModalContent>
                    <div className="contenttitle">
                        กรุณาเลือกตัวละครที่ต้องการเข้าร่วมกิจกรรม<br />
                    </div>
                    <div className="contenttext">
                        <FormSelect>
                            <label >
                                <select id="charater_form" className="fromselect__list" onChange={(e) => handleChange(e)}>
                                    <option value="" disabled selected>เลือกตัวละคร</option>
                                    {props.characters && props.characters.map((item,index)=>{
                                        return (
                                            <option key={index} value={item.id}>{item.char_name}</option>
                                        )
                                    })}
                                </select>
                                <img src={iconDropdown} alt="" />
                            </label>
                        </FormSelect>
                    </div>
                    <Btns className='confirm' onClick={()=>actSelectCharacter()}>

                    </Btns>

            </ModalContent>
        </ModalCore>
    )
}
// <Btns className='cancel' onClick={()=>props.setValues({modal_open:''})}>
//
// </Btns>
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ModalContent = styled.div`
     position: relative;
    display: block;
    width: 476px;
    height: 336px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${Imglist['modal_bg']});
    box-sizing: border-box;
    padding: 10% 10% 8%;
    font-family: 'Kanit-Light';
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttitle{
        font-size: 21px;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Light';
        font-size: 21px;
        line-height: 1.5em;
        word-break: break-word;
        min-height: 180px;
        //height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`
const FormSelect  = styled.form`
        margin: 0px auto;
        label {
            position: relative;
            color:#fff;
            background-color: #070707;
            width: 382px;
            height: 45px;
            display: block;
            box-sizing: border-box;
            overflow: hidden;
            display: flex;
            justify-content: center;
            align-items: center;
            &:focus {
                outline: 0 none;
            }
        }
        select {
            font-size: 20px;
            border: 0px;
            background: #000;
            vertical-align: middle;
            width: 110%;
            left: 0;
            position: absolute;
            text-indent: 10px;
            z-index: 1;
            color: #ffffff;
            font-family: 'Kanit-Light';
            &:focus {
                outline: 0 none;
            }
            option{
                color: #ffffff;
            }
        }
        img {
            position: absolute;
            display: block;
            top: 50%;
            width: 15px;
            height: 8px;
            right: 10px;
            transform: translate( -50% , -50% );
            user-select: none;
        }
    }
`
const Btns = styled.div`
    width: 104px;
    height: 44px;
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
    color: #000;
    line-height: 36px;
    font-size: 20px;
    background-size: contain;
    opacity: 0.8;
    &.confirm{
        background: no-repeat 0 0 url(${Imglist['btn_confirm']});
    }
    &.cancel{
        background: no-repeat 0 0 url(${Imglist['btn_cancel']});
    }
    &:hover{
        background-position: bottom center;
    }
`
