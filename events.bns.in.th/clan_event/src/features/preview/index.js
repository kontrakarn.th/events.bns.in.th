import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Slider from "react-slick";
import styled from 'styled-components';
import Logo from '../main/BNSLogo';

import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import {Imglist} from './../../constants/Import_Images';

  const SampleNextArrow =props=> {
        const { className, style, onClick } = props;
        return (
          <div className={className} onClick={onClick}><img src={Imglist.right_arrow}/></div>
        );
    }
      
    const SamplePrevArrow =props=>{
        const { className, style, onClick } = props;
        return (
            <div className={className} onClick={onClick}><img src={Imglist.left_arrow}/></div>
        );
    }
class Preview extends React.Component {

    render() {
          const settings = {
            dots: true,
            infinite: true,
            fade: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        return (
            <PageWrapper>
                <Logo/>
                <Menu page='preview'/>
                <Content>
                    <Slider {...settings}>
                            <div>
                                    <img src={Imglist['slide_1']} alt=""/>
                            </div>
                            <div>
                                    <img src={Imglist['slide_2']} alt=""/>
                            </div>
                    </Slider>
                </Content>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Preview);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_2']}) #050506;
    width: 1105px;
    padding-top: 120px;
    padding-bottom: 80px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const Content = styled.div`
    background: top center no-repeat url(${Imglist['frame_slide']});
    width: 922px;
    height: 891px;
    margin: 0 auto;
    box-sizing: border-box;
    padding: 120px 100px;
    img{
        display: block;
        margin: 0 auto;
    }
    & .slick-dots li.slick-active button:before {
        opacity: 1;
        color: #fff;
    }
    & .slick-dots li button:before{
        font-size: 11px;
        opacity: 1;
        color: #646464;
    }
    & .slick-prev{
        left:0;
    }
    & .slick-next{
        right:0;
    }
    & .slick-prev,
    & .slick-next{
        width: auto;
        height: auto;
        z-index: 1;
        &::before{
            font-size:0;
        }
    }
`