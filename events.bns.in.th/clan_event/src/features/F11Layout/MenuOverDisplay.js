import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import { setValue, setValues,onAccountLogout } from './../../store/redux';
import iconHome from './images/btn_home.png';
import iconScroll from './images/icon_scroll.png';
import {Imglist} from './../../constants/Import_Images';

const CPN = (props) => {
    let nameText = "";
    let coin = 0;
    if(props.showUserName && props.username !== "" && props.character != ""){
        nameText = props.username;
    }
    if(props.character !== ""){
        nameText = (nameText !== "")? nameText+" : "+props.character : props.character;
    }

    if (props.coin){
        coin = props.coin;
    }
    return (
        <>
            <TopData>
                {nameText !== "" &&
                    <SlotText>{nameText}</SlotText>
                }
                {props.showLogin &&
                    <SlotBtn onClick={()=>onAccountLogout()}>Logout</SlotBtn>
                }
            </TopData>
            <ButtomHome href="/" />
            {props.showScroll && <IconScroll /> }
        </>
    )
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValue, setValues };
export default connect( mapStateToProps, mapDispatchToProps )(CPN);


const TopData = styled.div`
    /* opacity: 0; */
    position: absolute;
    top: 0px;
    right: 0px;
    display: block;
    margin-top: 17px;
    margin-right: 17px;
`;
const SlotText = styled.div`
    pointer-events: auto;
    display: inline-block;
    padding: 0px 25px;
    margin-left: 17px;
    /* width: fit-content; */
    /* max-width: 170px; */
    text-align: center;
    height: 45px;
    line-height: 45px;
    background-color: rgba(0,0,0,0.7);
    border-radius: 15px;
    color: #FFFFFF;
    font-size: 14px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    /* transition: width 0.3s ease-in-out; */
`;
const SlotBtn = styled.a`
    pointer-events: auto;
    cursor: pointer;
    display: inline-block;
    padding: 0px 10px;
    margin-left: 17px;
    width: fit-content;
    max-width: 170px;
    text-align: center;
    height: 45px;
    line-height: 45px;
    background-color: rgba(0,0,0,0.7);
    border-radius: 15px;
    color: #FFFFFF;
    font-size: 14px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;
const ButtomHome = styled.a`
    position: absolute;
    top: calc(50% - 32px);
    right: 14px;
    display: block;
    width: 63px;
    height: 63px;
    background: no-repeat center url(${iconHome});
    user-select: auto;
    pointer-events:  all;
`;
const IconScrollAnimation = keyframes`
    0% { transform: translate3d(-50%,0,0); }
    100% { transform: translate3d(-50%,-10%,0); }
`;
const IconScroll = styled.div`
    position: absolute;
    bottom: 10px;
    left: 50%;
    transform: translate3d( -50%, 0, 0);
    display: block;
    width: 52px;
    height: 71px;
    background: no-repeat center url(${iconScroll});
    animation: ${IconScrollAnimation} 1s infinite alternate;
`;
