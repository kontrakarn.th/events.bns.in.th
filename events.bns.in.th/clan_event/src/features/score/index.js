import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import styled from 'styled-components';
import Logo from '../main/BNSLogo';
import { apiPost } from './../../middlewares/Api';
import { setValues, onAccountLogout } from './../../store/redux';
import Menu from '../menu/';
import {Imglist} from './../../constants/Import_Images';
import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';

class Score extends React.Component {
    numberWithCommas=(x)=> {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    confirmRedeemItem=(step)=>{
        this.props.setValues({
            modal_open: 'confirm',
            modal_message:'คุณยืนยันที่จะรับไอเทม ?',
            modal_item: this.props.clan_rewards[step].id,
        });
    }
    checkBtn(step) {
        let received = this.props.clan_rewards[step].is_claimed || false;
        let can = this.props.clan_rewards[step].can_claim || false;
        if(received) {
            return "disable"
        } else if(can) {
            return "pending"
        } else {
            return "disable"
        }
    }
    actRedeemItem(){
        this.apiClanRedeem();
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiClanRedeem(id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {
            type:'redeem_clan_reward',
            package_key: this.props.modal_item
        };
        let successCallback = (data) => {
            if (data.status === true){
                this.props.setValues({
                    ...data.data,

                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CLAN_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    render() {
        let {rank_rewards, clan_rewards, score, character} = this.props;
        let clan_name = rank_rewards.clan_name || "";
        let clan_score = rank_rewards.clan_score || "0";

        console.log("props",this.props);
        return (
            <PageWrapper>
                <Logo/>
                <Menu page='score'/>
                <Content>
                    <Textbox>
                        <div>แคลน {clan_name} มีคะแนน {clan_score}</div>
                        <div>ตัวละคร {character} มีคะแนน {this.numberWithCommas(score)}</div>
                    </Textbox>
                    <RedeemBtn className='prize1' status={this.checkBtn("step1")} onClick={()=>this.confirmRedeemItem("step1")}/>
                    <RedeemBtn className='prize2' status={this.checkBtn("step2")} onClick={()=>this.confirmRedeemItem("step2")}/>
                    <RedeemBtn className='prize3' status={this.checkBtn("step3")} onClick={()=>this.confirmRedeemItem("step3")}/>
                </Content>
                <ModalLoading/>
                <ModalMessage/>
                <ModalConfirm actConfirm={()=>this.actRedeemItem()} modal_item_token={"use redux"}/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Score);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_2']}) #050506;
    width: 1105px;
    padding-top: 122px;
    padding-bottom: 60px;
    text-align: center;
    position: relative;
    z-index: 20;
`
const Content = styled.div`
    background: top center no-repeat url(${Imglist['clan_score']});
    width: 922px;
    height: 2008px;
    margin: 0 auto;
    box-sizing: border-box;
    color: #ffffff;
    padding-top: 190px;
    position: relative;
    img{
        display: block;
        margin: 0 auto;
    }
`
const Textbox = styled.div`
    color: #ffffff;
    >div{
        width: 420px
        height: 38px;
        margin: 0 auto 33px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
`

const RedeemBtn = styled.div`
    background: top center no-repeat url(${Imglist['btn_redeem']});
    width: 104px;
    height: 44px;
    cursor: pointer;
    position: absolute;
    left: 50%;
    transform: translate3d(-50%,0,0);
    ${props=>props.status === 'disable' &&
        `
            pointer-events: none;
            background-position: bottom center;
        `
    }
    &:hover{
        background-position: center;
    }
    &.prize1{
        top: 783px;
    }
    &.prize2{
        top: 1212px;
    }
    &.prize3{
        bottom: 98px;
    }
`
