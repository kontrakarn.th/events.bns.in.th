import bg_2 from '../static/images/bg_2.jpg';
import bg from '../static/images/bg.jpg';
import btn_cancel from '../static/images/btn_cancel.png';
import btn_confirm from '../static/images/btn_confirm.png';
import btn_home from '../static/images/btn_home.png';
import btn_inactive from '../static/images/btn_inactive.png';
import btn_redeem from '../static/images/btn_redeem.png';
import clan_score from '../static/images/clan_score.png';
import ranking from '../static/images/ranking.png';
import detail from '../static/images/detail.png';
import history_section from '../static/images/history_section.png';
import ico_scroll from '../static/images/ico_scroll.png';
import icon_dropdown from '../static/images/icon_dropdown.png';
import icon_menu from '../static/images/icon_menu.png';
import slide_2 from '../static/images/slide_2.png';
import main_content from '../static/images/main_content.png';
import main_content1 from '../static/images/main_content1.png';
import main_content2 from '../static/images/main_content2.png';
import menu_bg from '../static/images/menu_bg.jpg';
import modal_bg from '../static/images/modal_bg.png';
import slide_1 from '../static/images/slide_1.png';
import frame_slide from '../static/images/frame_slide.png';
import left_arrow from '../static/images/left_arrow.png';
import right_arrow from '../static/images/right_arrow.png';

export const Imglist = {
	bg_2,
	bg,
	btn_cancel,
	btn_confirm,
	btn_home,
	btn_inactive,
	btn_redeem,
	clan_score,
	ranking,
	detail,
	history_section,
	ico_scroll,
	icon_dropdown,
	icon_menu,
	slide_2,
	main_content1,
	main_content2,
	menu_bg,
	modal_bg,
	slide_1,
	frame_slide,
	left_arrow,
	right_arrow,
}
