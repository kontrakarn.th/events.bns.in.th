import imgList from '../constants/ImportImages'

const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    showUserName: true,
    showCharacterName: false,
    showMenu: true,
    username: "",
    character_name: "",
    characters:[],
    menu: [
      {title: "หน้าหลัก", page: "main", link: process.env.REACT_APP_EVENT_PATH+"/"},
      {title: "โปรโมชั่น", page: "promotion", link: process.env.REACT_APP_EVENT_PATH+"/promotion"},
      {title: "ประวัติการได้รับไอเทม", page: "reward", link: process.env.REACT_APP_EVENT_PATH+"/reward"}
    ],
    selected_char: false,
    loginStatus: false,
    permission: true,

    //===modal
    //modal_open: "loading",//"loading","message","character",""confirm","turban","towelset","confirm_package","confirm_claim"
    modal_open: "",
    modal_message: "",
    modal_bonus: false,
    modal_character_id: 0,

    is_unlocked: false,
    unlock_package: {
        can_buy: false,
        already_buy: false
    },
    final_reward: {
        title: "กล่องมังกรแห่งความพยายาม",
        img_key: null,
        can_claim: false,
        has_claimed: false,
        package_key: 3
    },
    show_compensation_history: false,
    load_reward: true,
    history_reward: [],
    history_resent: [],

    //===event base value
    eventPath: (path)=>(process.env.REACT_APP_EVENT_PATH+path),
    status: false,

    points: 0,
    tokens: 0,


    costume_slider:[
        {
            img: imgList.costume_1
        },
        {
            img: imgList.costume_2
        }
    ],
    charactor:[],
    checkin_list: [
        {
            id: 1,
            img: imgList.items_1,
            hover:imgList.hover_1,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 2,
            img: imgList.items_2,
            hover:imgList.hover_2,
            is_checkin: false,
            can_checkin: false,
        },
        {
            id: 3,
            img: imgList.items_3,
            hover:imgList.hover_3,
            is_checkin: false,
            can_checkin: true,
        },
        {
            id: 4,
            img: imgList.items_4,
            hover:imgList.hover_4,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 5,
            img: imgList.items_5,
            hover:imgList.hover_5,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 6,
            img: imgList.items_6,
            hover:imgList.hover_6,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 7,
            img: imgList.items_7,
            hover:imgList.hover_7,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 8,
            img: imgList.items_8,
            hover:imgList.hover_8,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 9,
            img: imgList.items_9,
            hover:imgList.hover_9,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 10,
            img: imgList.items_10,
            hover:imgList.hover_10,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 11,
            img: imgList.items_11,
            hover:imgList.hover_11,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 12,
            img: imgList.items_12,
            hover:imgList.hover_12,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 13,
            img: imgList.items_13,
            hover:imgList.hover_13,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 14,
            img: imgList.items_14,
            hover:imgList.hover_14,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 15,
            img: imgList.items_15,
            hover:imgList.hover_15,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 16,
            img: imgList.items_16,
            hover:imgList.hover_16,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 17,
            img: imgList.items_17,
            hover:imgList.hover_17,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 18,
            img: imgList.items_18,
            hover:imgList.hover_18,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 19,
            img: imgList.items_19,
            hover:imgList.hover_19,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 20,
            img: imgList.items_20,
            hover:imgList.hover_20,
            is_checkin: true,
            can_checkin: true,
        },
        {
            id: 21,
            img: imgList.items_21,
            hover:imgList.hover_21,
            is_checkin: true,
            can_checkin: true,
        },

    ],
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         :return ({...state, sessionKey: action.value});
        case 'ACCOUNT_AUTHED'       :return ({...state, userData: action.value});
        case 'RECEIVE_SESSION_KEY'  :return ({...state, sessionKey: action.value});
        case 'SET_LOGIN_URL'        :return ({...state, loginUrl: action.value});
        case 'SET_LOGOUT_URL'       :return ({...state, logoutUrl: action.value});
        case 'ACCOUNT_LOGIN'        :return ({...state, authState: "LOGGED_IN"});
        case 'ACCOUNT_LOGOUT'       :return ({...state, authState: "LOGGED_OUT"});
        case 'ACCOUNT_AUTH_CHECKED' :return ({...state, authed: true});
        case 'SET_JWT_TOKEN'        :return ({...state, jwtToken: action.value});

        case "SET_VALUE"            :
            return (Object.keys(defaultState).indexOf(action.key) >= 0) ? {
                ...state,
                [action.key]: action.value
            } : state;
        case "SET_VALUES"           :return {...state, ...action.value};
        default:
            return state;
    }
}

export const onAccountAuth = (sessionKey) => ({type: 'ACCOUNT_AUTH', value: sessionKey});
export const onAccountAuthed = (userData) => ({type: 'ACCOUNT_AUTHED', value: userData});
export const onAccountLogin = () => ({type: 'ACCOUNT_LOGIN', value: ""});
export const onAccountLogout = () => ({type: 'ACCOUNT_LOGOUT', value: ""});
export const receiveSessionKey = (sessionKey) => ({type: 'RECEIVE_SESSION_KEY', value: sessionKey});
export const setLoginUrl = (url) => ({type: 'SET_LOGIN_URL', value: url});
export const setLogoutUrl = (url) => ({type: 'SET_LOGOUT_URL', value: url});
export const onAccountAuthChecked = () => ({type: 'ACCOUNT_AUTH_CHECKE', value: true})
export const setJwtToken = (token) => ({type: 'SET_JWT_TOKEN', value: token});

export const setValue = (key, value) => ({type: "SET_VALUE", value, key});
export const setValues = (value) => ({type: "SET_VALUES", value});
