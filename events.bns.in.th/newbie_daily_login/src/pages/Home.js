import React,{useEffect} 	from 'react';
import {Link} 				from 'react-router-dom';
import {connect} 			from 'react-redux';
import {setValues} 			from './../store/redux';
import styled 				from 'styled-components';
import imgList 				from './../constants/ImportImages';
import F11Layout 			from './../features/f11Layout';
import Modals 				from './../features/modals';
import {apiPost} 			from './../constants/Api';
import Slider 				from 'react-slick';

const ArrowNext = (props) => {
    return (
        <div
            className="arrow--next"
            onClick={props.onClick}
        />
    )
}
const ArrowPrev = (props) => {
    return (
        <div
            className="arrow--prev"
            onClick={props.onClick}
        />
    )
}

const Home = (props) => {

	//==== Slick slider ====//
	const settings = {
		dots: true,
		infinite: true,
		speed: 500,
		fade: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow: <ArrowNext />,
		prevArrow: <ArrowPrev />
	};


	//============ event info ===========//
  	const apiEventInfo = () => {
		let dataSend = {type: "event_info"};
		let successCallback = (res) => {
			if(res.status) {
				props.setValues({
					...res.data,
					modal_open:""
				})
			} 
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}

		let failCallback = (res) => {
			if (res.type === 'no_permission') {
				props.setValues({
					permission: false,
					modal_open: "",
				});
			}
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.data.message,
				});
			}
		}
    	apiPost("REACT_APP_API_POST_EVENT_INFO", props.jwtToken, dataSend, successCallback, failCallback);
	}


	//=========== get item ============//
	const apiReceiveItem = (date_number) => {
		let dataSend = {type: "getitem", date_number: date_number};
		props.setValues({
			modal_open: 'loading'
		});
		let successCallback = (res) => {
			if(res.status) {
				props.setValues({
					...res.data,
					modal_open:"message",
					modal_message: res.message,
				})
			} 
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		let failCallback = (res) => {
			if (res.type === 'no_permission') {
				props.setValues({
					permission: false,
					modal_open: "",
				});
			}
			else {
				props.setValues({
					modal_open:"message",
					modal_message: res.message,
				});
			}
		}
		apiPost("REACT_APP_API_POST_GETITEM", props.jwtToken, dataSend, successCallback, failCallback);
	}

	useEffect(()=>{
		if(props.jwtToken !== "" && !props.status) apiEventInfo();
	},[props.jwtToken])

	return (	
		<F11Layout page={"main"}>
			<Modals />
			<HomeStyle>
				<section className="banner"></section>
				<section className="detail detail__1">
					<img className="detail__board" src={imgList.rule} alt=''/>
					{props.can_get_reward 
                    ?
                        props.coupon_reward
                        ?
							<div className="btn_receive_item received"></div>
                        :
							<div className="btn_receive_item" onClick={()=>apiReceiveItem(100)}></div>
                    :
						<div className="btn_receive_item not_condition"></div>
					}
				</section>
				<section className="detail detail__2">
					<div className="checkin__wrapper">
					{
						props.checkin_list.map((item, key) => {
							return (
								<div className={"checkin__items items_" + (key+1)} key={key}>
									<div className={"checkin__items--img"}>
										
			

										{
											props.can_get_reward 
											?
												props.checkin_list[key].is_checkin ?
													<div className="checked">
														<img src={imgList[item.img]} alt=''/>
													</div>	
												:
													props.checkin_list[key].can_checkin ?
														props.characters.length > 0 
														?
														<img src={imgList[item.img]} alt='' onClick={()=>props.setValues({modal_open: 'character'})}/>	
														:	
														<img src={imgList[item.img]} alt='' onClick={()=>apiReceiveItem(item.id)}/>	
													
													:
													<img src={imgList[item.img]} alt='' className="disabled"/>
											:
											<img src={imgList[item.img]} alt='' className="disabled"/>
										}

										<div className="hover">
											<img src={imgList[item.hover]}/>
										</div>
									</div>
								</div>
							);
						})
					}
					</div>
				</section>
				<section className="detail detail__3">
					<div className="slidebox">
						<Slider {...settings}>
							{
								props.costume_slider.map((item, key) => {
									return (
										<div className="slider" key={key}>
											<img className="slider__img" src={item.img} alt="" />
										</div>
									)
								})
							}
						</Slider>
					</div>
				</section>
				
			</HomeStyle>
		</F11Layout>
	)
}
const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(Home);

const HomeStyle = styled.div`
	position: relative;
	display: block;
	width: 1105px;
	height: 3275px;
	background-image: url(${imgList.bg_home});
	section:not(:first-child){
		margin-bottom: 150px;
	}
  	.banner {
		position: relative;
		display: block;
		height: 760px;
		&__btn {
			position: absolute;
			top: 410px;
    		left: 228px;
			display: block;
			width: 306px;
			height: 53px;
			background-image: url(${imgList.btn_join});
			background-position: top center;
			&:hover {
				background-position: bottom center;
			}
		}
  	}
	.detail {
		position: relative;
		display: block;
		&__1{
			img{
				margin: 0 auto;
				display: block;
			}
			.btn_receive_item{
				position: absolute;
				left: 0;
				right: 0;
				bottom: 60px;
				margin: 0 auto;
				width: 178px;
				height: 56px;
				background-image: url(${imgList.btn_receive_item});
				background-position: top center;
				cursor:pointer;	
				&:hover{
					background-position: bottom center;	
				}
				&.received{
					background-image: url(${imgList.btn_received_item});
					background-position: top center;
					pointer-events: none;
					cursor: auto;
				}
				&.not_condition{
					background-image: url(${imgList.btn_not_condition});
					background-position: top center;
					pointer-events: none;
					cursor: auto;
				}
			}
		}
		&__2{	
			width: 875px;
			height: 608px;
			background-image: url(${imgList.login_bg});
			background-position: top center;
			margin: 0 auto;
			box-sizing: border-box;
			padding: 100px 50px 50px;
			.checkin{
				&__wrapper{
					display: flex;
					flex-wrap: wrap;
				}
				&__items{
					width: 14%;
					padding: 1px 1px 30px;
					box-sizing: border-box;
					position: relative;
					img{
						display: block;
						max-width: 100%;
						max-height: 100%;
						
						&.disabled{
							filter: grayscale(100%);
						}
					}
					&--img{
						position: relative;
						&:hover .hover{
							opacity: 1;
							visibility: visible;
						}
					}
					.checked{
						&:before{
							content: '';
							background-color: rgba(0,0,0,0.75);
							position: absolute;
							width: 100%;
							height: 100%;
							border-radius: 22px;
						}
						&:after{
							content: '';
							background: url(${imgList.icon_check}) top center no-repeat;
							width: 38px;
							height: 31px;
							position: absolute;
							left: 0;
							right: 0;
							top: 50%;
							transform: translateY(-50%);
							-webkit-transform: translateY(-50%);
							margin: 0 auto;
						}
					}
				
					.hover{
						position: absolute;
					    left: 70px;
   					 	bottom: 40px;
						width: 250px;
						opacity: 0;
						transition: all .3s ease;
						visibility: hidden;
						z-index: 5;
					}
					&.items_7 .hover,
					&.items_14 .hover,
					&.items_21 .hover{
						left: auto;
						right: 70px;
					}
					&.items_8 .hover{
						width: 470px;
					}
				}
			}
		
		}
		&__3{
			width: 875px;
			height: 679px;
			background-image: url(${imgList.costume_bg});
			background-position: top center;
			margin: 0 auto;
			box-sizing: border-box;
			padding: 50px 15px 15px;
			.slider{
				&__img{
					display: block;
					margin: 0 auto;
					max-width: 100%;
					max-height: 100%;
				}
			}
		
			.arrow{
				&--next{
					display: block;
					width: 36px;
					height: 54px;
					background: url(${imgList['btn_next']}) top center no-repeat;
					top: 50%;
					transform: translate3d(0,-50%,0);
					position: absolute;
					right: -90px;
				}
				&--prev{
					display: block;
					width: 36px;
					height: 54px;
					background: url('${imgList['btn_prev']}') top center no-repeat;
					top: 50%;
					transform: translate3d(0,-50%,0);
					left: -90px;
					position: absolute;
				}
			}
			.slick-dots li {
				width: 30px;
				height: 5px;
				& button{
					padding: 2px;
					width: 100%;
					height: 100%;
					&::before{
						content: '';
						background-color: #ff8bb4;
						width: 100%;
						height: 100%;
						opacity:1;
					}
				}
				&.slick-active button:before{
					background-color: #ff5c6d;
				}
			}
			.slick-dots{
				bottom: 0;
			}
			
		}
		
	}
`;
