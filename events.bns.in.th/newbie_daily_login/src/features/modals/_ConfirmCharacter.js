import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import imgList from "../../constants/ImportImages";
import {apiPost} from '../../constants/Api';

const ModalConfirm = props => {
    const name = "confirm_character";
    const apiconfirm = (id) => {
        props.setValues({
            modal_open:"loading"
        })
        let dataSend = {type: "save_charactor",id};
        let successCallback = (res) => {
            if(res.status) {
                props.setValues({
                    ...res.data,
                    modal_open:"message",
                    modal_message: res.message,
                })
            } 
            else {
                props.setValues({
                    modal_open:"message",
                    modal_message: res.message,
                });
            }
        }
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }
            else {
                props.setValues({
                    modal_open:"message",
                    modal_message: res.message,
                });
            }
        }
        apiPost("REACT_APP_API_POST_SAVE_CHARACTER", props.jwtToken, dataSend, successCallback, failCallback);
    }
    return (
        <ModalCore
            open={props.modal_open === name}
            onClick={()=>props.setValues({modal_open:''})}
        >
            <ModalcontentStyle>
                <div className="title">ยืนยัน</div>
                <div className="message">
                <div dangerouslySetInnerHTML={{__html: props.modal_message}}/>

                </div>
                <div className="buttons">
                    <a className="btn btn--confirm"onClick={()=>apiconfirm(props.modal_character_id)} />
                    <a className="btn btn--cancel"onClick={()=>props.setValues({modal_open:''})} />
                </div>
            </ModalcontentStyle>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalConfirm);

const ModalcontentStyle = styled.div`
    position: relative;
    display: block;
	width: 493px;
	height: 345px;
	padding: 10px;
    background-image: url(${imgList.modal_bg});
    .title{
        font-size: 19px;
        text-align: center;
        line-height: 1;
        color: #fff;
        margin-bottom: 25px;
    }
    .message {
        color: #FFFFFF;
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 55%;
        text-align: center;
        font-family: "Kanit-Light", Tahoma;
        >div{
            color: #301616;
            line-height: 1.2;
        }
    }
    .buttons {
        position: absolute;
        left: 0px;
        bottom: 38px;
        display: block;
        width: 100%;
        text-align: center;
    }
    .btn {
        display: inline-block;
		width: 178px;
		height: 56px;
        margin: 0px 20px;
        background-position: top center;
        cursor: pointer;
        &--confirm {
            background-image: url(${imgList.btn_confirm});
        }
        &--cancel {
            background-image: url(${imgList.btn_cancel});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;
