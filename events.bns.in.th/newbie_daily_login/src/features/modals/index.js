import React from 'react';
import ModalMessage                 from './_Message';
import ModalLoading                 from './_Loading';
import ModalConfirm                 from './_Confirm';
import ModalSelectCharacter         from './_SelectCharacter'
import ModalConfirmCharacter        from './_ConfirmCharacter'

export default (props)=> (
  <>
    <ModalLoading />
    <ModalMessage />
    <ModalConfirm/>
    <ModalSelectCharacter/>
    <ModalConfirmCharacter/>
  </>
)
