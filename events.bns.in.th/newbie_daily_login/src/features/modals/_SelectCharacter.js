import React, { useState } from 'react';
import { connect } from 'react-redux';
import ScrollArea from 'react-scrollbar';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from '../../constants/ImportImages';
import { apiPost } from './../../constants/Api';

const ModalLoading = (props) => {
    const name = 'character';
    const [slected, setSlected] = useState(-1);
    const [show, setShow] = useState(false);

    const actSelected = (index, id) => {
        setSlected(index);
    };
    const actSelectCharacter = (id) => {

        if (slected < 0) {
             props.setValues({
                 modal_open: 'message',
                 modal_message: 'โปรดเลือกตัวละคร',
                 selected_char: false,
             });
        } else {
            apiSelectCharacter(props.characters[slected].id);
        }
    };
    const apiSelectCharacter = (id) => {
        let dataSend = { type: 'select_character', id };
        let successCallback = (res) => {
            if (res.status) {
                props.setValues({
                    ...res.data,
                    modal_open: 'confirm_character',
                    modal_message: res.message,
                    modal_character_id: id
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        let failCallback = (res) => {
            if (res.type === 'no_permission') {
                props.setValues({
                    permission: false,
                    modal_open: '',
                });
            } else {
                props.setValues({
                    modal_open: 'message',
                    modal_message: res.message,
                });
            }
        };
        apiPost(
            'REACT_APP_API_POST_SELECT_CHAR',
            props.jwtToken,
            dataSend,
            successCallback,
            failCallback
        );
    };
    let char_name = props.characters[slected]
        ? props.characters[slected].char_name
        : '';
    return (
        <ModalCore
            open={props.modal_open === name}
            onClick={() => props.setValues({ modal_open: '' })}
        >
            <ModalcontentStyle
                className='kanit_light_font'
                showList={show}
                selected={slected < 0}
            >
                <div className="close" onClick={()=>props.setValues({modal_open:''})}><img src={imgList.modal_close}/></div>
                <div className="title">กรุณาเลือกตัวละคร</div>
                <div className="content">
                    <div className="content__text">ไอเทมในรายการที่ 3 "อาวุธคุนลุน ขั้น 3"<br/>จะได้รับตามอาชีพของตัวละครที่ผู้เล่นได้เลือกไว้</div>
                    <div className='select' onClick={() => setShow(!show)}>
                        <div className='select__name'>
                            {slected < 0 ? 'โปรดเลือกตัวละคร' : char_name}
                        </div>
                        <img
                            className='select__icon'
                            src={imgList.icon_down_arrow}
                            alt=''
                        />
                        <ScrollArea
                            peed={0.8}
                            className='select__list'
                            contentClassName=''
                            horizontal={false}
                        >
                            <ul className=''>
                                {props.characters &&
                                    props.characters.map((item, index) => {
                                        return (
                                            <li
                                                key={index}
                                                className='select__slot'
                                                onClick={() =>
                                                    actSelected(index, item.char_id)
                                                }
                                            >
                                                {item.char_name}
                                            </li>
                                        );
                                    })}
                            </ul>
                        </ScrollArea>
                    </div>
                </div>
                <div className='buttons'>
                    <a
                        className='btn btn--confirm'
                        onClick={() => actSelectCharacter()}
                    />
                </div>
            </ModalcontentStyle>
        </ModalCore>
    );
};

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = { setValues };
export default connect(mapStateToProps, mapDispatchToProps)(ModalLoading);

const ModalcontentStyle = styled.div`
    position: relative;
    display: block;
	width: 493px;
	height: 345px;
	padding: 10px;
    background-image: url(${imgList.modal_bg});
    .close{
        position: absolute;
        right: 0;
        top: -35px;
        cursor:pointer;
    }
	.title{
        font-size: 19px;
        text-align: center;
        line-height: 1;
        color: #fff;
        margin-bottom: 25px;
    }
    .content {
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 55%;
        text-align: center;
        font-family: "Kanit-Light", Tahoma;
        &__text{
            line-height: 1.2;
            margin-bottom: 20px;
        }
    }
    .select {
        position: relative;
        color: #000;
        font-size: 20px;
        display: flex;
        width: 350px;
        justify-content: space-between;
        align-items: center;
        background-color: #ffb3c6;
        pointer-events: ${(props) => (props.showList ? 'none' : 'all')};
        margin: 0px auto;
        cursor: pointer;
        padding: 5px;
        &__name {
            padding: 0px 22.5px;
        }
        &__icon {
        }
        &__list {
            z-index: 1;
            position: absolute;
            /*top: 75px;*/
            top: 50px;
            left: 0px;
            display: block;
            width: 100%;
            max-height: 300px;
            background-color: #000000;

            opacity: ${(props) => (props.showList ? '1' : '0')};
            pointer-events: ${(props) => (props.showList ? 'all' : 'none')};
        }
        &__slot {
            /* padding: 22.5px;
            background-color: #000000;
            color: #ffffff; */
            padding: 10px;
            background-color: #ffb4c6;
            color: #000;
            &:hover {
                /* color: #000000;
                background-color: #ffffff; */
                color: #535353;
            }
        }
    }
    .buttons {
        position: absolute;
        left: 0px;
        bottom: 40px;
        display: block;
        width: 100%;
        text-align: center;
    }
    .btn {
        display: inline-block;
        width: 178px;
		height: 56px;
        margin: 0px 20px;
        background-position: top center;
        &--confirm {
            background-image: url(${imgList.btn_confirm});
        }
        &--cancel {
            background-image: url(${imgList.btn_cancel});
        }
        &:hover {
            background-position: bottom center;
        }
    }
`;
