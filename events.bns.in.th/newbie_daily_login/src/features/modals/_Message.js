import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import imgList from "../../constants/ImportImages";

const ModalLoading = props => {
  const name = "message";
  const actClose = () => {
    if(!props.selected_char && props.characters.length !== 0) {
      props.setValues({modal_open:'character'});
    } else {
      props.setValues({modal_open:''});
    }
  }
	return (
		<ModalCore
			open={props.modal_open === name}
			onClick={()=>props.setValues({modal_open:''})}
		>
			<ModalcontentStyle>
				<div className="title">แจ้งเตือน</div>
				<div className="message">
					<div dangerouslySetInnerHTML={{__html: props.modal_message}}/>
				</div>
				<div className="buttons">
					<a className="btn btn--confirm" onClick={()=>props.setValues({modal_open:''})} />
				</div>
			</ModalcontentStyle>
		</ModalCore>
  	)
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(ModalLoading);

const ModalcontentStyle = styled.div`
	position: relative;
	display: block;
	width: 493px;
	height: 345px;
	padding: 10px;
	background-image: url(${imgList.modal_bg});
	.title{
    font-size: 19px;
    text-align: center;
    line-height: 1;
		color: #fff;
		margin-bottom: 25px;
	}
	.message {
        font-size: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        height: 55%;
        text-align: center;
        font-family: "Kanit-Light", Tahoma;
        >div{
            color: #301616;
            line-height: 1.2;
        }
    }
  	.buttons {
		position: absolute;
		left: 0px;
		bottom: 38px;
		display: block;
		width: 100%;
		text-align: center;
	}
	.btn {
		display: inline-block;
		width: 178px;
		height: 56px;
		margin: 0px 20px;
		background-position: top center;
		cursor: pointer;
		&--confirm {
			background-image: url(${imgList.btn_confirm});
		}
		&--cancel {
			background-image: url(${imgList.btn_cancel});
		}
		&:hover {
			background-position: bottom center;
		}
	}
`;
