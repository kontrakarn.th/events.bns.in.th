import styled, { keyframes } from 'styled-components';
import imgList from './../../constants/ImportImages';

const IconScrollAnimation = keyframes`
    0% { transform: translateY(0); }
    100% { transform: translateY(-10px); }
`;
const IconScroll = styled.div`
    position: absolute;
    bottom: 10px;
    left: 48%;
    transform: translate3d( -50%, 0, 0);
    display: block;
    width: 52px;
    height: 71px;
    background: no-repeat center url(${imgList.icon_scroll});
    animation: ${IconScrollAnimation} 1s infinite alternate;
`;

export default IconScroll;