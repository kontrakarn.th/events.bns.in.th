import React, { Component } from 'react'
import fb from './../static/images/btn_fb.png'
import cs from './../static/images/btn_cs.png'
import login from './../static/images/btn_login.png'
import logout from './../static/images/btn_logout.png'
export default class Menu extends Component {
    render() {
        return (
            <div className="events-bns__bottom">
                <ul className="events-bns__bottom-list">
                    <li>
                        <a href="">
                            {this.props.login ?
                                <img src={logout} />
                                :
                                <img src={login} />
                            }
                            
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/th.bns/"><img src={fb} /></a>
                    </li>
                    <li>
                        <a href=""><img src={cs} /></a>
                    </li>
                </ul>
            </div>
        )
  }
}
