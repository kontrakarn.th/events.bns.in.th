import React, { Component } from 'react'
import {connect} from 'react-redux'
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch'
import './../styles/index.css'

import {apiPost} from './../middlewares/Api';
import Head from '../components/Head';
import Promotion from '../components/Promotion'
import Redeem from '../components/Redeem'
import ModalSelectCharacter from '../components/ModalSelectCharacter'
import ModalSelectWeapon from '../components/ModalSelectWeapon'
import ModalConfirmRedeem from '../components/ModalConfirmRedeem'
import ModalMessage from '../components/ModalMessage'
import ModalLoading from '../components/ModalLoading';

import {Permission} from './../components/Permission';

import home from './../static/images/btn_home.png'

import leaf_top from './../static/images/leaf_top.png'
import leaf_mid from './../static/images/leaf_mid.png'
import part3_box from './../static/images/part3_box.png'

class HomeContainer extends Component {
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: ''
        }
    }
    openModal(){
        this.setState({
            modalConfirmToRedeem:true
        })
    }
    closeModal(){
        this.setState({
            // modalSelectCharacter: false,
            // modalConfirmSelectCharacter: false,
            modalLoading: false,
            modalConfirmToRedeem: false,
            modalShowMessage: false,
            modalMessage: '',
            // modalSelectWeapon:false,
            characters: [],
            weapon:[],
        });
    }

    componentWillMount() {

        this.apiPostEventInfo()
    }

    apiPostEventInfo(){
        let apiName="REACT_APP_API_POST_EVENT_INFO";
        let dataSend={ type : "event_info" };
        let successCallback = (data)=>{
            this.setState({
                eventInfo: data.content
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    permission: true,
                });
            }
        };
        apiPost(apiName,dataSend,successCallback,failCallback);
    }

    apiRedeem(){
        this.setState({
            modalLoading: true,
            modalConfirmToRedeem: false
        });
        let apiName="REACT_APP_API_POST_REDEEM";
        let dataSend={ type : "redeem_reward" };
        let successCallback = (data)=>{
            this.setState({
                modalLoading: false,
                eventInfo: data.content,
                modalShowMessage: true,
                modalMessage: data.message
            })
        };
        let failCallback = (data)=>{
            if(data.type=='no_permission'){
                this.setState({
                    modalLoading: false,
                    permission: true,
                });
            }
        };
        apiPost(apiName,dataSend,successCallback,failCallback);
    }

    render() {
        return (
            <div className="events-bns">
                {
                    this.state.permission && this.state.permission == true ?
                        <Permission open={this.state.permission} />
                    :
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#fc4b00", opacity:1}}
                        >
                            <div className="newrevival">
                                <div className="newrevival__leaftop">
                                    <img src={leaf_top} alt=""/>
                                </div>
                                <div className="newrevival__leafmid">
                                    <img src={leaf_mid} alt=""/>
                                </div>

                                <Head
                                    userData={this.props.userData}
                                />
                                <Promotion />
                                <Redeem
                                    eventInfo={this.state.eventInfo}
                                    openModal={this.openModal.bind(this)}
                                    // redeem={this.state.redeem}
                                />
                                <div className="newrevival__fashion">
                                    <img src={part3_box} />
                                </div>
                                <ModalConfirmRedeem
                                    open={this.state.modalConfirmToRedeem}
                                    confirm={this.apiRedeem.bind(this)}
                                    closeModal={this.closeModal.bind(this)}
                                />
                                <ModalMessage
                                    open={this.state.modalShowMessage}
                                    msg={this.state.modalMessage}
                                    closeModal={this.closeModal.bind(this)}
                                />
                                <ModalLoading
                                    open={this.state.modalLoading}
                                />
                               {/*  <ModalSelectCharacter
                                    open={this.state.modalSelectCharacter}
                                />
                                <ModalSelectWeapon
                                    open={this.state.modalSelectWeapon}
                                    closeModal={this.closeModal.bind(this)}
                                /> */}
                                <div className="newrevival__home">
                                    <a href={process.env.REACT_APP_API_SERVER_HOST}>
                                        <img src={home} />
                                    </a>
                                </div>
                            </div>
                        </ScrollArea>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
	loginUrl: state.AccountReducer.loginUrl,
	logoutUrl: state.AccountReducer.logoutUrl,
})

const mapDispatchToProps = (dispatch) => ({
    dispatch:dispatch,
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
