import React from 'react';
import {connect} from 'react-redux';

import HomeContainer from './../containers/HomeContainer';

import F11Layout from './../features/f11layout';
import RevivalMay2019 from './../features/revivalMay2019';

export class Home extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={false}
            >
                <RevivalMay2019 />
            </F11Layout>
        )
    }
}


    // <Permission />
//<HomeContainer />
