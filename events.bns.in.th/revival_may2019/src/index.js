import 'core-js/es6/map';
import 'core-js/es6/set';
import 'raf/polyfill';
import React from 'react';

import { hydrate, render }  from 'react-dom';

import ReactDOM from 'react-dom';
// import {render} from 'react-snapshot';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import {combineReducers, createStore} from 'redux';
import {Web} from './routes/Web';
import serviceWorker from './serviceWorker';
import AccountReducer from './reducers/AccountReducer';
import layout from './features/f11layout/redux';
import EventReducer from './features/revivalMay2019/redux';
import ModalReducer from './features/modals/redux';

const store = createStore(
	combineReducers({
		AccountReducer,
		layout,
		EventReducer,
		ModalReducer,
	})
);
const rootElement = document.getElementById('root');
ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<Web />
		</BrowserRouter>
 	</Provider>,
	document.getElementById('root')
)
