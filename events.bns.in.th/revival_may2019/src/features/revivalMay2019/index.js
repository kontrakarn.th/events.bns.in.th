import React from 'react';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled, { keyframes } from 'styled-components';

import { onAccountLogout } from './../../actions/AccountActions';
import { setValues } from './redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalDetail from './../../features/modals/ModalDetail';

import imgSection_sample        from './images/section_sample.png';
import imgSection_reward        from './images/section_reward.png';
import imgSection_condition     from './images/section_condition.png';
import imgIcon_magnifying_glass from './images/icon_magnifying_glass.png';
import imgButton_inactive       from './images/button_inactive.png';
import imgButton_condition      from './images/button_condition.png';
import imgButton_active         from './images/button_active.png';
import imgBackground            from './images/background.jpg';
import imgChar1                 from './images/char_left_bottom.png';
import imgChar2                 from './images/char_right.png';

import './css/style.css';

class RevivalMay2019 extends React.Component {
    actClick(type_id){
        // this.apiRedeem(type_id);
        let message = "กล่องน้องเล็กหน้าใหม่";
        if(type_id === 1)message = "กล่องน้องเล็กหน้าใหม่";
        if(type_id === 2)message = "กล่องน้องเล็กที่กลับมา";

        this.props.setValues({
            type_id,
            modal_open: "confirm",
            modal_message: message,
        });

    }
    actConfirm(){
        this.apiRedeem(this.props.type_id);
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.response,
                    modal_open: "",
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CHECKIN", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiRedeem(type_id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type_id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content.response,
                    modal_open:"message",
                    // modal_message: data.message,
                    modal_message: "ส่งของรางวัลสำเร็จ<br/>กรุณาตรวจสอบจดหมายภายในเกม"
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: ''
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }



    render() {
        let { condition_1_pass, condition_1_received, condition_2_pass, condition_2_received } = this.props;

        let status_btn_1 = condition_1_received ? "inactive" : (condition_1_pass ? "active" : "");
        let image_btn_1  = condition_1_received ? imgButton_inactive : (condition_1_pass ? imgButton_active : imgButton_condition);
        let status_btn_2 = condition_2_received ? "inactive" : (condition_2_pass ? "active" : "");
        let image_btn_2  = condition_2_received ? imgButton_inactive : (condition_2_pass ? imgButton_active : imgButton_condition);

        return (
            <div className="revivalmay2019">
                <SectionCondition>
                    <a className={"btn btn--mg0 "} onClick={()=>this.props.setValues({modal_open:"detail",modal_detail:"items"})}><img src={imgIcon_magnifying_glass} /></a>
                    <a className={"btn btn--receive "+status_btn_1} onClick={()=>{if(status_btn_1==="active")this.actClick(1)}}><img src={image_btn_1} /></a>
                </SectionCondition>
                <SectionReward>
                    <a className={"btn btn--mg1 "} onClick={()=>this.props.setValues({modal_open:"detail",modal_detail:"items"})}><img src={imgIcon_magnifying_glass} /></a>
                    <a className={"btn btn--mg2 "} onClick={()=>this.props.setValues({modal_open:"detail",modal_detail:"hongmoon"})}><img src={imgIcon_magnifying_glass} /></a>
                    <a className={"btn btn--mg3 "} onClick={()=>this.props.setValues({modal_open:"detail",modal_detail:"cytal1_t_w_th"})}><img src={imgIcon_magnifying_glass} /></a>
                    <a className={"btn btn--mg4 "} onClick={()=>this.props.setValues({modal_open:"detail",modal_detail:"hongmoon"})}><img src={imgIcon_magnifying_glass} /></a>
                    <a className={"btn btn--mg5 "} onClick={()=>this.props.setValues({modal_open:"detail",modal_detail:"cytal1_t_w_th"})}><img src={imgIcon_magnifying_glass} /></a>
                    <a className={"btn btn--mg6 "} onClick={()=>this.props.setValues({modal_open:"detail",modal_detail:"hongmoon_s"})}><img src={imgIcon_magnifying_glass} /></a>
                    <a className={"btn btn--mg7 "} onClick={()=>this.props.setValues({modal_open:"detail",modal_detail:"cytal1_f_sa"})}><img src={imgIcon_magnifying_glass} /></a>
                    <a className={"btn btn--mg8 "} onClick={()=>this.props.setValues({modal_open:"detail",modal_detail:"hongmoon_s"})}><img src={imgIcon_magnifying_glass} /></a>
                    <a className={"btn btn--mg9 "} onClick={()=>this.props.setValues({modal_open:"detail",modal_detail:"cytal1_f_sa"})}><img src={imgIcon_magnifying_glass} /></a>
                    <img className={"char char--set1"}src={imgChar1} />
                    <img className={"char char--set2"}src={imgChar2} />
                    <a className={"btn btn--receive "+status_btn_2} onClick={()=>{if(status_btn_2==="active")this.actClick(2)}}><img src={image_btn_2} /></a>
                </SectionReward>
                <SectionSample>
                </SectionSample>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                <ModalDetail />
            </div>
        )
    }
}
const mapStateToProps = state => ({...state.layout,...state.AccountReducer,...state.EventReducer});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(RevivalMay2019);

const SectionCondition = styled.div`
    position: relative;
    display: bloack;
    width: 949px;
    height: 898px;
    margin: 0px auto 0px;
    background: no-repeat top center url(${imgSection_condition});
`;

const SectionReward = styled.div`
    position: relative;
    display: bloack;
    width: 949px;
    height: 3076px;
    margin: 50px auto 0px;
    background: no-repeat top center url(${imgSection_reward});
`;

const SectionSample = styled.div`
    position: relative;
    display: bloack;
    width: 949px;
    height: 898px;
    margin: 50px auto 0px;
    background: no-repeat top center url(${imgSection_sample});
`;
