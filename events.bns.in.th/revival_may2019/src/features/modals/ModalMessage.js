import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './redux';

import imgAlert from './images/modal_alert.png';
import imgBtnConfirm from './images/btn_confirm.png';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="message"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                {/*<h2>ยืนยันการรับ</h2>*/}
                <h3 dangerouslySetInnerHTML={{__html: modal_message}} />
            </ModalMessageContent>
            <ModalBottom>
                <BtnConfirm onClick={()=>props.setValues({modal_open:""})} />
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.ModalReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 340px;
    height: 372px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${imgAlert});
    h2 {
        font-family: Kanit-Medium;
        position: relative;
        top: 130px;
        font-size: 22px;
        color: #f7c7a8
    }
    h3 {
        font-family: Kanit-Regular
        position: relative;
        top: 145px;
        font-size: 20px;
        line-height: 1.5em;
        color: #d69973;
    }
`;
const ModalBottom = styled.div`
    position: relative;
    display: block;
    transform: translate3d(0,-50%,0);
    width: 340px;
`;
const BtnConfirm = styled.div`
    display: block;
    margin: 0px auto;
    width: 124px;
    height: 52px;
    background: no-repeat center url(${imgBtnConfirm});
`;
