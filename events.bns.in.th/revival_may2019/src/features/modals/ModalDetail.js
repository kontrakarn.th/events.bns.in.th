import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './redux';

import img_cytal1_f_sa from './images/popup_cytal1_f_sa.png';
import img_cytal1_t_w_th from './images/popup_cytal1_t_w_th.png';
import img_hongmoon_s from './images/popup_hongmoon_s.png';
import img_hongmoon from './images/popup_hongmoon.png';
import img_items from './images/popup_items.png';

const CPN = props => {
    let {modal_detail} = props;
    return (
        <ModalCore
            modalName="detail"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            {modal_detail==="items"&&
                <img src={img_items} />
            }
            {modal_detail==="cytal1_f_sa"&&
                <img src={img_cytal1_f_sa} />
            }
            {modal_detail==="cytal1_t_w_th"&&
                <img src={img_cytal1_t_w_th} />
            }
            {modal_detail==="hongmoon_s"&&
                <img src={img_hongmoon_s} />
            }
            {modal_detail==="hongmoon"&&
                <img src={img_hongmoon} />
            }
        </ModalCore>
    )
}


// <ModalBottom>
//     <BtnConfirm onClick={()=>props.setValues({modal_open:""})} />
// </ModalBottom>
const mapStateToProps = state => ({...state.ModalReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
