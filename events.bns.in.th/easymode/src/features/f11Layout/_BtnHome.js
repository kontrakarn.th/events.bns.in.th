import React from 'react';
import styled from 'styled-components';
import imgList from './../../constants/ImportImages';

export default (props) => {
  return (
    <HomeButtom href={props.to|| "/"} />
  )
}

const HomeButtom = styled.a`
  position: absolute;
  top: calc(50% - 32px);
  right: 14px;
  display: block;
  width: 63px;
  height: 63px;
  background: no-repeat center url(${imgList.btn_home});
  user-select: auto;
  pointer-events:  all;
`;
