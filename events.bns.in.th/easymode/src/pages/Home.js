import React,{useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {setValues} from './../store/redux';
import styled from 'styled-components';
import imgList from './../constants/ImportImages';
import F11Layout from './../features/f11Layout';
import Modals from './../features/modals';
import {apiPost} from './../constants/Api';

const Home = (props) => {

  const apiEventInfo = () => {
    let dataSend = {type: "event_info"};
    let successCallback = (res) => {
      if(res.status) {
        if(res.data && !res.data.selected_char && res.data.characters.length === 0 ) {
          props.setValues({
            ...res.data,
            modal_open: "message",
            modal_message: "ไม่มีตัวละครตรงตามเงื่อนไข",
            claim_reward_list: props.claim_reward_list,
            quest_daily_list: props.quest_daily_list,
          })
        } else {
          props.setValues({
            ...res.data,
            modal_open: res.data.selected_char ? "":"character",
          })
        }
      } else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    let failCallback = (res) => {
      if (res.type === 'no_permission') {
        props.setValues({
            permission: false,
            modal_open: "",
        });
      }else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    apiPost("REACT_APP_API_POST_EVENT_INFO", props.jwtToken, dataSend, successCallback, failCallback);
  }

  useEffect(()=>{
    if(props.jwtToken !== "" && !props.status) apiEventInfo();
  },[props.jwtToken])
  
  return (
    <F11Layout page={"main"}>
      <Modals />
      <HomeStyle>
        <section className="banner">
          <Link className="banner__btn" to={props.eventPath("/package")} />
          <img className="banner__time" src={imgList.time} />
        </section>
        <section className="detail">
          <img className="detail__board" src={imgList.detail} />
        </section>
      </HomeStyle>
    </F11Layout>
  )
}
const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(Home);

const HomeStyle = styled.div`
  position: relative;
  display: block;
  width: 1105px;
  height: 1385px;
  height: 1385px;
  background-image: url(${imgList.home_background});
  .banner {
    position: relative;
    display: block;
    height: 700px;
    &__btn {
      position: absolute;
      top: 495px;
      left: 50%;
      display: block;
      width: 349px;
      height: 54px;
      transform: translate(-50%, 0px);
      background-image: url(${imgList.btn_join});
      background-position: top center;
      &:hover {
        background-position: bottom center;
      }
    }
    &__time {
      position: absolute;
      top: 565px;
      left: 50%;
      display: block;
      transform: translate(-50%, 0px);
    }
  }
  .detail {
    position: relative;
    display: block;
    &__board {
      display block;
      width: 963px;
      height: 535px
      margin: 80px auto 0px;
    }
  }
`;
// home_background
// detail
