import React,{useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {setValues} from '../store/redux';
import styled from 'styled-components';
import imgList from '../constants/ImportImages';
import F11Layout from '../features/f11Layout';
import Modals from './../features/modals';
import {apiPost} from './../constants/Api';

const Package = (props) => {

  const apiEventInfo = () => {
    let dataSend = {type: "event_info"};
    let successCallback = (res) => {
      if(res.status) {
        props.setValues({
          ...res.data,
          modal_open: res.data.selected_char ? "":"character",
        })
      } else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    let failCallback = (res) => {
      if (res.type === 'no_permission') {
        props.setValues({
            permission: false,
            modal_open: "",
        });
      }else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    apiPost("REACT_APP_API_POST_EVENT_INFO", props.jwtToken, dataSend, successCallback, failCallback);
  }
  // const apiPurchasePackage = () => {
  //   props.setValues({modal_open: "loading"});
  //   let dataSend = {type: "purchase_package"};
  //   let successCallback = (res) => {
  //     if(res.status) {
  //       props.setValues({
  //         ...res.data,
  //         modal_open:"message",
  //         modal_message: res.message,
  //       })
  //     } else {
  //       props.setValues({
  //           modal_open:"message",
  //           modal_message: res.message,
  //       });
  //     }
  //   }
  //   let failCallback = (res) => {
  //     if (res.type === 'no_permission') {
  //       props.setValues({
  //           permission: false,
  //           modal_open: "",
  //       });
  //     }else {
  //       props.setValues({
  //           modal_open:"message",
  //           modal_message: res.message,
  //       });
  //     }
  //   }
  //   apiPost("REACT_APP_API_POST_PURCHASE_PACKAGE", props.jwtToken, dataSend, successCallback, failCallback);
  // };
  const apiCliamFreePoint = () => {
    props.setValues({modal_open: "loading"});
    let dataSend = {type: "claim_free_points"};
    let successCallback = (res) => {
      if(res.status) {
        props.setValues({
          ...res.data,
          modal_open:"message",
          modal_message: res.message,
          history_point: [],
        })
      } else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    let failCallback = (res) => {
      if (res.type === 'no_permission') {
        props.setValues({
            permission: false,
            modal_open: "",
        });
      }else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    apiPost("REACT_APP_API_POST_CLAIM_FREE_POINTS", props.jwtToken, dataSend, successCallback, failCallback);
  };
  // const apiCliamPurchasePoint = () => {
  //   props.setValues({modal_open: "loading"});
  //   let dataSend = {type: "claim_purchase_points"};
  //   let successCallback = (res) => {
  //     if(res.status) {
  //       props.setValues({
  //         ...res.data,
  //         modal_open:"message",
  //         modal_message: res.message,
  //       })
  //     } else {
  //       props.setValues({
  //           modal_open:"message",
  //           modal_message: res.message,
  //       });
  //     }
  //   }
  //   let failCallback = (res) => {
  //     if (res.type === 'no_permission') {
  //       props.setValues({
  //           permission: false,
  //           modal_open: "",
  //       });
  //     }else {
  //       props.setValues({
  //           modal_open:"message",
  //           modal_message: res.message,
  //       });
  //     }
  //   }
  //   apiPost("REACT_APP_API_POST_CLAIM_PURCHASE_POINTS", props.jwtToken, dataSend, successCallback, failCallback);
  // };
  const apiCliamSpecialQuestPoint = (id) => {
    props.setValues({modal_open: "loading"});
    let dataSend = {type: "claim_special_quest_points",id};
    let successCallback = (res) => {
      if(res.status) {
        props.setValues({
          ...res.data,
          modal_open:"message",
          modal_message: res.message,
          history_point: [],
        })
      } else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    let failCallback = (res) => {
      if (res.type === 'no_permission') {
        props.setValues({
            permission: false,
            modal_open: "",
        });
      }else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    apiPost("REACT_APP_API_POST_CLAIM_SPECIAL_QUEST_POINTS", props.jwtToken, dataSend, successCallback, failCallback);
  };
  // const apiOpenBox = () => {
  //   props.setValues({modal_open: "loading"});
  //   let dataSend = {type: "open_box"};
  //   let successCallback = (res) => {
  //     if(res.status) {
  //       props.setValues({
  //         ...res.data,
  //         modal_open:"message",
  //         modal_message: res.message,
  //         history_reward: [],
  //         history_testure: [],
  //       })
  //     } else {
  //       props.setValues({
  //           modal_open:"message",
  //           modal_message: res.message,
  //       });
  //     }
  //   }
  //   let failCallback = (res) => {
  //     if (res.type === 'no_permission') {
  //       props.setValues({
  //           permission: false,
  //           modal_open: "",
  //       });
  //     }else {
  //       props.setValues({
  //           modal_open:"message",
  //           modal_message: res.message,
  //       });
  //     }
  //   }
  //   apiPost("REACT_APP_API_POST_OPEN_BOX", props.jwtToken, dataSend, successCallback, failCallback);
  // };
  // const apiCliamReward = (id) => {
  //   props.setValues({modal_open: "loading"});
  //   let dataSend = {type: "claim_reward",id};
  //   let successCallback = (res) => {
  //     if(res.status) {
  //       props.setValues({
  //         ...res.data,
  //         modal_open: "",
  //         history_reward: [],
  //         history_testure: [],
  //       })
  //     } else {
  //       props.setValues({
  //           modal_open:"message",
  //           modal_message: res.message,
  //       });
  //     }
  //   }
  //   let failCallback = (res) => {
  //     if (res.type === 'no_permission') {
  //       props.setValues({
  //           permission: false,
  //           modal_open: "",
  //       });
  //     }else {
  //       props.setValues({
  //           modal_open:"message",
  //           modal_message: res.message,
  //       });
  //     }
  //   }
  //   apiPost("REACT_APP_API_POST_CLIAM_REWARD", props.jwtToken, dataSend, successCallback, failCallback);
  // };

  const actPurchasePackage = () => {
    props.setValues({ modal_open: "confirm_package" });
  }
  const actCliamOpenBox = () => {
    props.setValues({ modal_open: "confirm_open" })
  }
  const actCliamPurchasePoint = () => {
    props.setValues({ modal_open: "confirm_claim" })
  }
  const actCliamReward = (data) => {
    props.setValues({ modal_open: "confirm_reward", modal_reward_data: data })
  }

  useEffect(()=>{
    if(props.jwtToken !== "" && !props.status) apiEventInfo();
  },[props.jwtToken])

  return (
    <F11Layout page={"package"}>
      <Modals />
      <PackageStyle>
        <div className="towels">
          ปีกผีเสื้อปัจจุบัน<br/>
          {props.tokens}<img  className="towels__icon" src={imgList.towels_icon} alt="" />
         </div>
        <section className="purchase">
          <div className="purchase__content">
             <a 
              className={"purchase__btn"+(props.already_purchased ? " purchased":"")} 
              onClick={()=>actPurchasePackage()}
            ></a>
          </div>
        </section>
        <section className="quest">
          <div className="quest__content">
            <a 
              className={"quest__btn quest__btn--3point "+props.claim_daily_free_points_status} 
              onClick={()=>apiCliamFreePoint()}
            ></a>
            <a 
              className={"quest__btn quest__btn--10point "+props.claim_daily_purchase_points} 
              onClick={()=>actCliamPurchasePoint()}
            ></a>
            <ul className="quest__list">
              {[1,2,3,4,5,6,0,7,0].map((item,index)=>{
                if(index === 6 || index === 8) {
                  return  <li key={"qs_"+index} className={"quest__slot"} />
                } else {
                  let {id,status} = props.quest_daily_list[item-1];
                  return (
                    <li key={"qs_"+index} className={"quest__slot"}>
                      <img src={imgList["boss"+item]} alt=""/>
                      <a 
                        className={"quest__slotbtn "+status}
                        onClick={()=>apiCliamSpecialQuestPoint(id)}
                      ></a>
                    </li>
                  )
                }
              })}
            </ul>
          </div>
        </section>
        <section className="treasure">
        <div className="treasure__content">
        <span className="treasure__title">คะแนนปัจจุบัน {props.points} คะแนน</span>
          <div className="treasure__help">
            <img className="treasure__100" src={imgList.popup_treasure_100}/>
            <img className="treasure__maybe" src={imgList.popup_treasure_maybe}/>
          </div>
          <a 
            className={"treasure__btn"+(props.can_open_box? "":" lock")}
            onClick={()=>actCliamOpenBox()}
          ></a>
        </div>
        </section>
        <section className="exchange">
          <div className="exchange__content">
            <a className="exchange__plus exchange__plus--1" onClick={()=>props.setValues({modal_open: "turban"})} />
            <a className="exchange__plus exchange__plus--2" onClick={()=>props.setValues({modal_open: "towelset"})} />

            {props.claim_reward_list.claim_start.map((item,index)=>{
              let cn = "exchange__btn exchange__btn--day"+(index+1);
              return (
                <a 
                  key={"day"+index}
                  className={cn+(item.can_claim?" ":" lock")}
                  // onClick={()=>apiCliamReward(item.reward_id)}
                  onClick={()=>actCliamReward(item)}
                ></a>
               )
            })}

            {[10,10,5,5].map((item,index)=>{
              let cn = "exchange__count exchange__count--fix"+(index+1);
              return (
                <div className={cn}>
                  {(props.claim_reward_list.claim_limit[index].limit_count || 0)+"/"+item}
                </div>
              )
            })}
            {props.claim_reward_list.claim_limit.map((item,index)=>{
              let cn = "exchange__btn exchange__btn--fix"+(index+1);
              return (
                <a 
                  key={"fix"+index}
                  className={cn+(item.can_claim?" ":" lock")}
                  onClick={()=>actCliamReward(item)}
                />
              )
            })}

            {props.claim_reward_list.claim_unlimit.map((item,index)=>{
              let colum = index % 4;
              let row = Math.floor(index / 4);
              let cn = "exchange__btn c"+colum+" r"+row;
              return (
                  <a 
                    key={"inf"+index}
                    className={cn+( item.can_claim?" ":" lock")}
                    // onClick={()=>apiCliamReward(reward_id)}
                    onClick={()=>actCliamReward(item)}
                    
                  ></a>
              )
            })}
          </div>
        </section>
      </PackageStyle>
    </F11Layout>
  )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = {setValues};
export default connect( mapStateToProps, mapDispatchToProps )(Package);

const PackageStyle = styled.div`
  position: relative;
  display: block;
  width: 1105px;
  height: 5794px;
  margin: 0px;
  background-image: url(${imgList.package_background});
  .towels {
    z-index: 1;
    color: #FFFFFf;
    text-align: right;
    padding-top: 13px;
    padding-right: 15px;
    line-height: 1.9em;
    font-size: 14px;
    position: fixed;
    top: 500px;
    right: 0px;
    display: block;
    width: 150px;
    height: 72px;
    background-image: url(${imgList.towels_count});
    &__icon {
      vertical-align: middle;
      margin-left: 5px;
    }
  }
  .purchase {
    position: relative;
    display: block;
    height: 840px;
    padding-top: 140px;
    &__content {
      display: block;
      width: 963px;
      height: 652px;
      margin: 0px auto;
      background-image: url(${imgList.package_section});
    }
    &__btn {
      position: absolute;
      top: 633px;
      left: 455px;
      display: block;
      width: 192px;
      height: 40px;
      background-image: url(${imgList.btn_package});
      background-position: top center;
      &:hover {
        background-position: bottom center;
      }
      &.purchased {
        user-select: none;
        pointer-events: none; 
        background-image: url(${imgList.btn_got_package});
      }
    }
  }
  .quest {
    position: relative;
    display: block;
    height: 1630px;
    padding-top: 40px;
    &__content {
      display: block;
      width: 963px;
      height: 1514px;
      margin: 0px auto;
      background-image: url(${imgList.quest_section});
    }
    &__btn {
      position: absolute;
      display: block;
      width: 277px;
      height: 58px;
      background-position: top center;
      &--3point {
        top: 247px;
        left: 416px;
        background-image: url(${imgList.btn_3points});
        &.not_in_condition {
          filter: grayscale(1);
          user-select: none;
          pointer-events: none; 
        }
        &.claimed {
          user-select: none;
          pointer-events: none; 
          background-image: url(${imgList.btn_got_3points});
        }
        &.can_claim {
        }
      }
      &--10point {
        top: 331px;
        left: 416px;
        background-image: url(${imgList.btn_10points});
        &.not_in_condition {
          filter: grayscale(1);
          user-select: none;
          pointer-events: none;
        }
        &.claimed {
          user-select: none;
          pointer-events: none; 
          background-image: url(${imgList.btn_got_10points});
        }
        &.can_claim {
        }
      }
      &:hover {
        background-position: bottom center;
      }
      // btn_got_package.png
    }
    &__list {
      position: absolute;
      top: 543px;
      left: 204px;
      display: flex;
      width: 695px;
      align-items: flex-start;
      justify-content: space-between;
      flex-wrap: wrap;
    }
    &__slot {
      position: relative;
      display: block;
      width: 217px;
      height: 276px;
      margin-bottom: 34px;
    }
    &__slotbtn {
      position: absolute;
      bottom: 33px;
      left: 37px;
      display: block;
      width: 143px;
      height: 40px;
      background-image: url(${imgList.btn_point});
      background-position: top center;
      &:hover {
        background-position: bottom center;
      }
      &.not_in_condition {
        filter: grayscale(1);
        user-select: none;
        pointer-events: none; 
      }
      &.claimed {
        user-select: none;
        pointer-events: none; 
        background-image: url(${imgList.btn_got_point});
      }
      &.can_claim {
      }
    }
  }
  .treasure {
    display: block;
    padding-top: 40px;
    &__content {
      position: relative;
      display: block;
      height: 1027px;
      background-image: url(${imgList.treasure_section});
    }
    &__title {
      color: #FFFFFF;
      font-size: 20px;
      text-align: center;
      position: absolute;
      top: 79px;
      left: 315px;
      display: block;
      width: 475px;
    }
    &__help {
      z-index: 1;
      position: absolute;
      top: 144px;
      left: 495px;
      display: block;
      width: 115px;
      height: 113px;
      opacity: 0;
      transition: opacity 0.3s linear;
      &:hover {
        opacity: 1;
      }
    }
    &__100 {
      position: absolute;
      top: 0px;
      left: 0px;
      transform: translate(-340px, -205px);
      user-select: none;
      pointer-events: none;
    }
    &__maybe {
      position: absolute;
      top: 0px;
      right: 0px;
      transform: translate(482px, -205px);
      user-select: none;
      pointer-events: none;
    }
    &__popup {
      position: absolute;
      display: block;
      &--100 {
        // top: -60px;
        // left: 155px;
      }
      &--maybe {
        // top: -60px;
        // left: 610px;
      }
    }
    &__btn {
      position: absolute;
      top: 295px;
      left: 455px;
      display: block;
      width: 192px;
      height: 40px;
      background-position: top center;
      background-image: url(${imgList.btn_treasure});
      cursor: pointer;
      &:hover {
        background-position: bottom center;
      }
      &.lock {
        background-image: url(${imgList.btn_got_treasure});
        user-select: none;
        pointer-events: none;
      }
    }
  }
  .exchange {
    position: absolute;
    top: 3040px;
    display: block;
    width: 100%;
    height: 2754px;
    padding-top: 40px;
    &__content {
      position: relative;
      display: block;
      height: 2603px;
      background-position: 50% 0px;
      background-image: url(${imgList.exchange_section});
      background-repeat: no-repeat;
    }
    &__count {
      position: absolute;
      line-height: 39px;
      color: #FFFFFF;
      font-size: 15px;
      text-align: center;
      display: block;
      width: 39px;
      height: 39px;
      background-image: url(${imgList.exchange_count});
      &--fix1 {
        top: 545px;
        left: 290px;
      }
      &--fix2 {
        top: 545px;
        left: 453px;
      }
      &--fix3 {
        top: 545px;
        left: 615px;
      }
      &--fix4 {
        top: 545px;
        left: 778px;
      }
    }
    &__btn {
      position: absolute;
      display: block;
      width: 84px;
      height: 36px;
      background-position: top center;
      background-image: url(${imgList.btn_exchange});
      cursor: pointer;
      &:hover {
        background-position: bottom center;
      }
      &.lock {
        background-image: url(${imgList.btn_exchange_inactive});
        user-select: none;
        pointer-events: none;
      }
      &--day1 {
        top: 335px;
        left: 430px;
      }
      &--day2 {
        top: 335px;
        left: 587px;
      }

      &--fix1 {
        top: 776px;
        left: 269px;
      }
      &--fix2 {
        top: 776px;
        left: 432px;
      }
      &--fix3 {
        top: 776px;
        left: 594px;
      }
      &--fix4 {
        top: 776px;
        left: 757px;
      }


      &.c0 {
        left: 269px;
      }
      &.c1 {
        left: 432px;
      }
      &.c2 {
        left: 594px;
      }
      &.c3 {
        left: 757px;
      }

      &.r0 {
        top: 1212px;
      }
      &.r1 {
        top: 1488px;
      }
      &.r2 {
        top: 1811px;
      }
      &.r3 {
        top: 2109px;
      }
      &.r4 {
        top: 2409px;
      }

    }

    &__plus {
      position: absolute;
      display: block;
      width: 32px;
      height: 32px;
      background-image: url(${imgList.btn_plus});
      cursor: pointer;
      &--1 {
        top: 159px;
        left: 499px;
      }
      &--2 {
        top: 161px;
        left: 657px;
      }
    }
  }

`;
