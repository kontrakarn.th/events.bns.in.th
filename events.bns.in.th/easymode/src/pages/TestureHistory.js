import React,{useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {setValues} from '../store/redux';
import styled from 'styled-components';
import imgList from '../constants/ImportImages';
import F11Layout from '../features/f11Layout';
import ScrollArea from 'react-scrollbar';
import {apiPost} from './../constants/Api';

const Package = (props) => {

  const apiHistory = () => {
    let dataSend = {type: "open_box"};
    let successCallback = (res) => {
      if(res.status) {
        props.setValues({
          history_testure: res.data.histories,
          modal_open: "",
        })
      } else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    let failCallback = (res) => {
      if (res.type === 'no_permission') {
        props.setValues({
            permission: false,
            modal_open: "",
        });
      }else {
        props.setValues({
            modal_open:"message",
            modal_message: res.message,
        });
      }
    }
    apiPost("REACT_APP_API_POST_HISTORY", props.jwtToken, dataSend, successCallback, failCallback);
  }

  useEffect(()=>{
    if( props.jwtToken!=="" && props.history_testure.length <= 0 ) apiHistory();
  },[props.jwtToken])

  return (
    <F11Layout page={"testure"} hideScroll={true}>
      <HistoryStyle>
        <div className="table">
          <ScrollArea speed={0.8} className="table__list" contentClassName="" horizontal={false} >
            <ul>
            {props.history_testure.map((item,index)=>{
              return (
                  <li className="table__slot" key={"point_"+index}>
                    <div className="table__title">{item.reward_name}</div>
                    <div className="table__date">{item.date}</div>
                    <div className="table__time">{item.time}</div>
                  </li>
              )
            })}
            </ul>
          </ScrollArea>
        </div>
      </HistoryStyle>
    </F11Layout>
  )
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(Package);

const HistoryStyle = styled.div`
  color: #FFFFFF;
  font-size: 18px;
  position: relative;
  display: block;
  width: 1105px;
  height: 700px;
  padding-top: 80px;
  margin: 0px;
  background-image: url(${imgList.history_background});
  .table {
    position: relative;
    display: block;
    width: 963px;
    height: 592px;
    padding-top: 125px;
    margin: 0px auto;
    background-image: url(${imgList.testure_history_frame});
    &__list {
      width: 940px;
      height: 400px;
      margin: 0px auto;
    }
    &__slot {
      position: relative;
      display: flex;
      justify-content: space-between;
      align-items: center;
      width: 690px;
      margin: 0px auto;
    }
    &__title {
      width: 68%;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    &__date {

    }
    &__time {

    }
  }

`;
