import React,{useState,useEffect} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import styled from 'styled-components';
import {imgList} from './../constants/ImageList';
import Slider from "react-slick";
import F11Layout from './../features/F11Layout';
import Pagination from './../features/Pagination';
import {apiPost} from './../constants/Api';
import {setValues} from './../store/redux';

const HomePage = props => {

  const apiEventInfo = () => {
    apiPost(
      "REACT_APP_API_POST_EVENT_INFO",
      props.jwtToken,
      {type: "event_info"},
      (data)=>{//function successCallback
        props.setValues({
            ...data.data,
            // modal_open: (data.data.character_name === ""? "selectcharacter":""),
        });
      },
      (data)=>{//function failCallback
        props.setValues({
          modal_open: "message",
          modal_message: data.message,
        });
      },
    );
  }
  const apiClaimCoupon = () => {
    apiPost(
      "REACT_APP_API_POST_CLAIM_COUPON",
      props.jwtToken,
      {type: "claim_coupon"},
      (data)=>{//function successCallback
        props.setValues({
            ...data.data,
            modal_open: "message",
            modal_message: "ไอเทมถูกส่งเข้าไปในกล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อกดรับ",
        });
      },
      (data)=>{//function failCallback
        props.setValues({
          modal_open: "message",
          modal_message: data.message,
        });
      },
    );
  }
  const apiCheckin = () => {
    apiPost(
      "REACT_APP_API_POST_CHECKIN",
      props.jwtToken,
      {type: "checkin"},
      (data)=>{//function successCallback
        props.setValues({
            ...data.data,
            modal_open: "message",
            modal_message: data.message,
        });
      },
      (data)=>{//function failCallback
        props.setValues({
          modal_open: "message",
          modal_message: data.message,
        });
      },
    );
  }
  useEffect(()=>{
    if(props.jwtToken && props.jwtToken !== ""  && !props.status) {
      apiEventInfo();
    }
  },[props.jwtToken]);
  return (
    <F11Layout showUserName={true} showCharacterName={false} page={""} showMenu={false}>
      <HomeStyle className="home">
        <section className="home__banner" />
        <section className="home__detail">
          <div className="detail">
            <div className="detail__set">
              <img className="detail__image" src={imgList.item} />
              <Btn
                className="detail__btn"
                canClick={props.coupon.can_claim || false}
                ckicked={props.coupon.is_claimed || false}
                onClick={()=>apiClaimCoupon()}
              />
            </div>
          </div>
        </section>
        <section className="home__daily">
          <div className="daily">
            <div className="daily__set">
              {props.check_list.map((item,index)=>{
                let active = (item.can_checkin && !item.is_claimed);
                return (
                  <DailySlot
                    className="dailyslot"
                    key={"daily_"+item.days}
                    // active={active}
                    active={!item.is_claimed}
                    checked={item.is_claimed}
                    image={""}
                    onClick={()=>{if(active)apiCheckin()}}
                  >
                    {item.is_claimed &&
                      <img className="dailyslot__check" src={imgList.icon_check} />
                    }
                    <img className="dailyslot__image" src={imgList["day_"+item.days]} />
                    <img className={"dailyslot__detail dailyslot__detail--"+item.days} src={imgList["detail_"+item.days]} />
                  </DailySlot>
                )
              })}
            </div>
          </div>
        </section>
        <section className="home__reward">
          <img src={imgList.content_reward} />
        </section>
      </HomeStyle>
    </F11Layout>
  )
}

const mstp = state => ({...state.Main});
const mdpt = {setValues};
export default connect( mstp, mdpt )(HomePage);

const HomeStyle= styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: 2860px;
  background-image: url(${imgList.bg});
  background-color: #000000;
  .home {
    &__banner {
      height: 700px;
    }
    &__detail {
      height: 610px;
      padding: 65px 115px 0px;
    }
    &__daily {
      height: 760px;
      padding: 35px 115px
    }
    &__reward {
      height: 790px;
      padding: 35px 115px
    }
    section {
      position: relative;
      display:block;
      width: 100%;
    }
  }
  section {
    position: relative;
    display: block;
    width: 100%;

  }

  .detail {
    position: relative;
    display: block;
    width: 875px;
    height: 519px;
    background-image: url(${imgList.content_detail});
    &__set {
      position: relative;
      top: 335px;
      display:block;
    }
    &__image {
      display: block;
      margin: 0px auto;
    }
    &__btn {
      margin: 5px auto;
    }
  }

  .daily {
    position: relative;
    display: block;
    width: 875px;
    height: 702px;
    background-image: url(${imgList.content_daily});
    &__set {
      position: relative;
      top: 140px;
      margin: 0px auto;
      display: block;
      width: 784px;
    }
  }
`;
const Btn = styled.a`
  display: block;
  width: 94px;
  height: 35px;
  filter: grayscale(${props=>props.canClick? "0":"1"});
  pointer-events: ${props=>props.canClick ? "all":"none"};
  background-image: url(${props=>props.ckicked ? imgList.btn_received: imgList.btn_receive});
`;

const DailySlot = styled.a`
  position: relative;
  display: inline-block;
  width: 92px;
  height: 123px;
  margin: 10px;
  /* pointer-events: ${props=>props.active ? "all":"none"}; */
  /* cursor:  ${props=>props.active ? "pointer":"none"}; */
  .dailyslot {
    &__check {
      z-index: 1;
      position: absolute;
      top: 37%;
      left: 50%;
      display: ${props=>props.checked ? "block": "none"}
      transform: translate(-50%, -50%);
    }
    &__image {
      filter: grayscale(${props=>props.active? "0":"1"});
    }
    &__detail {
      z-index: 2;
      position: absolute;
      top: 0%;
      left: 50%;
      display: none;
      /* transform: translate(-50%, -80%); */
      /* width 303 */
      transform: translate(-152px, -80%);
      pointer-events: ${props=>props.canClick ? "all":"none"};
    }
  }
  &:hover .dailyslot__detail {
      display: block;
  }
`;
