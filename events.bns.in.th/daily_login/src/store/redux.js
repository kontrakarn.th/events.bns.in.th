const defaultState = {
    //===login values
    sessionKey: "",
    userData: {},
    authState: "",
    loginUrl: "",
    logoutUrl: "",
    authed: false,
    jwtToken: "",

    //===layout values
    characters:[],
    username: "",
    character_name: "",
    coin: 0,
    selected_char: false,
    loginStatus: false,
    showScroll: true,
    noScroll: true,
    loading: false,
    permission: true,
    status: false,
    menu: [
      {title: "หน้สหลัก",          link:"",             value: ""},
      {title: "การแข่งขัน",        link:"competition",  value: ""},
      {title: "ภารกิจสะสมคะแนน",  link:"quest",        value: ""},
      {title: "อันดับปัจจุบัน",       link:"ranking",      value: ""},
      {title: "ประวัติทำภารกิจ",    link:"history",      value: ""},
      // {title: "รับของรางวัลพิเศษ",  link:"reward",      value: "check"},
    ],
    //===modal
    modal_open: "",//"loading","selectcharacter","confirm"
    modal_message: "ไอเทมถูกส่งเข้าไปในกล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อกดรับ",
    modal_item: "",
    modal_select: "",

    //=== EVENT ===//
    current_day: 1,
    coupon: {
      can_claim: false,
      is_claimed: false
    },
    check_list: [
      {days: 1, can_checkin: false, is_claimed: false},
      {days: 2, can_checkin: false, is_claimed: false},
      {days: 3, can_checkin: false, is_claimed: false},
      {days: 4, can_checkin: false, is_claimed: false},
      {days: 5, can_checkin: false, is_claimed: false},
      {days: 6, can_checkin: false, is_claimed: false},
      {days: 7, can_checkin: false, is_claimed: false},
      {days: 8, can_checkin: false, is_claimed: false},
      {days: 9, can_checkin: false, is_claimed: false},
      {days:10, can_checkin: false, is_claimed: false},
      {days:11, can_checkin: false, is_claimed: false},
      {days:12, can_checkin: false, is_claimed: false},
      {days:13, can_checkin: false, is_claimed: false},
      {days:14, can_checkin: false, is_claimed: false},
      {days:15, can_checkin: false, is_claimed: false},
      {days:16, can_checkin: false, is_claimed: false},
      {days:17, can_checkin: false, is_claimed: false},
      {days:18, can_checkin: false, is_claimed: false},
      {days:19, can_checkin: false, is_claimed: false},
      {days:20, can_checkin: false, is_claimed: false},
      {days:21, can_checkin: false, is_claimed: false},
    ],
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ACCOUNT_AUTH'         : return ({ ...state, sessionKey: action.value });
        case 'ACCOUNT_AUTHED'       : return ({ ...state, userData: action.value });
        case 'RECEIVE_SESSION_KEY'  : return ({ ...state, sessionKey: action.value });
        case 'SET_LOGIN_URL'        : return ({ ...state, loginUrl: action.value });
        case 'SET_LOGOUT_URL'       : return ({ ...state, logoutUrl: action.value });
        case 'ACCOUNT_LOGIN'        : return ({ ...state, authState: "LOGGED_IN" });
        case 'ACCOUNT_LOGOUT'       : return ({ ...state, authState: "LOGGED_OUT" });
        case 'ACCOUNT_AUTH_CHECKED' : return ({ ...state, authed: true });
        case 'SET_JWT_TOKEN'        : return ({ ...state, jwtToken: action.value });

        case "SET_VALUE"            : return (Object.keys(defaultState).indexOf(action.key) >= 0) ? { ...state, [action.key]: action.value } : state;
        case "SET_VALUES"           : return ({ ...state, ...action.value });
        default                     : return state;
    }
}

export const onAccountAuth          = (sessionKey)  => ({ type: 'ACCOUNT_AUTH',         value: sessionKey });
export const onAccountAuthed        = (userData)    => ({ type: 'ACCOUNT_AUTHED',       value: userData });
export const onAccountLogin         = ()            => ({ type: 'ACCOUNT_LOGIN',        value: "" });
export const onAccountLogout        = ()            => ({ type: 'ACCOUNT_LOGOUT',       value: "" });
export const receiveSessionKey      = (sessionKey)  => ({ type: 'RECEIVE_SESSION_KEY',  value: sessionKey });
export const setLoginUrl            = (url)         => ({ type: 'SET_LOGIN_URL',        value: url });
export const setLogoutUrl           = (url)         => ({ type: 'SET_LOGOUT_URL',       value: url });
export const onAccountAuthChecked   = ()            => ({ type: 'ACCOUNT_AUTH_CHECKE',  value: true })
export const setJwtToken            = (token)       => ({ type: 'SET_JWT_TOKEN',        value: token });

export const setValue               = (key, value)  => ({ type: "SET_VALUE", value, key });
export const setValues              = (value)       => ({ type: "SET_VALUES", value });
