import React from 'react';
import {connect} from 'react-redux';

import ModalMessage from './ModalMessage';
import ModalLoading from './ModalLoading';
import ModalSelectCharacter from './ModalSelectCharacter';
import ModalConfirm from './ModalConfirm';

const Modals = (props)=> (
    <>
        <ModalMessage />
        <ModalLoading />
    </>
)

const mstp = state => ({...state.Main});
const mdtp = {  };

export default connect( mstp, mdtp )(Modals);
