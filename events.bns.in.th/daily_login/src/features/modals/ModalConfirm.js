import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import {imgList} from './../../constants/ImageList';

const CPN = props => {
  const apiSelectQuest = () => {
    console.log("apiSelectQuest",props.modal_select);
    // apiPost(
    //   "REACT_APP_API_POST_EVENT_INFO",
    //   props.jwtToken,
    //   ()=>{},//function successCallback
    //   ()=>{},//function failCallback
    // );

    props.setValues({
        modal_open: "message",
        modal_message: "สำเร็จ",
    });
  }

  return (
    <ModalCore
        modalName="confirm"
        actClickOutside={()=>props.setValues({modal_open:""})}
    >
        <ModalConfirmContent className="mdconfirm">
            <div
              className="mdconfirm__text"
              dangerouslySetInnerHTML={{__html: props.modal_message}}
            />
            <div className="mdconfirm__btns">
                <Btn className='confirm' onClick={()=>apiSelectQuest()} />
                <Btn className='cancel' onClick={()=>props.setValues({modal_open:''})} />
            </div>
        </ModalConfirmContent>

    </ModalCore>
  )
}

const mstp = state => ({...state.Main});
const mdtp = { setValues };

export default connect( mstp, mdtp )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalConfirmContent = styled.div`
  position: relative;
  display: block;
  width: 503px;
  height: 345px;
  font-family: 'Kanit-Light';
  font-size: 1em;
  color: #FFFFFF;
  background: no-repeat center url(${imgList.popup_confirm});
  padding: 65px 55px 0px;
  .mdconfirm{
    &__text {
      width: 100%;
      height: 155px;
      display: flex;
      justify-content: center;
      align-items: center;
      color: #000000;
      font-family: 'Kanit-Light';
      font-size: 21px;
      line-height: 1.5em;
      word-break: break-word;
    }
    &__btns {
      position: relative;
      margin: 100px auto;
      display: flex;
      justify-content: center;
      align-items: center;
      margin: 30px auto 0px;
    }

  }
`;

const Btn = styled.div`
  width: 108px;
  height: 41px;
  display: inline-block;
  cursor: pointer;
  margin: 0 15px;
  color: #000;
  line-height: 36px;
  font-size: 20px;
  background-size: contain;
  opacity: 0.8;
  &.confirm{
    background: no-repeat 0 0 url(${imgList['btn_confirm']});
  }
  &.cancel{
    background: no-repeat 0 0 url(${imgList['btn_cancel']});
  }
  &:hover{
    filter: grayscale(0.9)
  }
`
