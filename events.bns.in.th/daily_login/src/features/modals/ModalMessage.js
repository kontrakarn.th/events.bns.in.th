import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import {imgList} from "../../constants/ImageList";

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="message"
            // actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent className="mdmessage">
                <div
                  className="mdmessage__text"
                  dangerouslySetInnerHTML={{__html: modal_message}}
                />
                <div className="mdmessage__btns">
                  <Btn className='confirm' onClick={()=>props.setValues({modal_open: props.character === "" ?"selectcharacter" :""})} />
                </div>
            </ModalMessageContent>
        </ModalCore>
    )
}

const mstp = state => ({...state.Main});
const mdtp = { setValues };
export default connect( mstp, mdtp )(CPN);

const ModalMessageContent = styled.div`
  position: relative;
  display: block;
  width: 503px;
  height: 345px;
  font-family: 'Kanit-Light';
  font-size: 1em;
  color: #FFFFFF;
  background: no-repeat center url(${imgList.popup_message});
  .mdmessage{
    &__text {
      position: relative;
      top: 117px;
      left: 60px;
      width: 385px;
      height: 117px;
      display: flex;
      justify-content: center;
      align-items: center;
      color: #6b4e2d;
      text-align: center;
      font-family: 'Kanit-Light';
      font-size: 18px;
      line-height: 1.5em;
      word-break: break-word;
    }
    &__btns {
      position: absolute;
      left: 0px;
      bottom: 55px;
      display: flex;
      width: 100%;
      justify-content: center;
      align-items: center;
    }

  }
`;

const Btn = styled.div`
  width: 95px;
  height: 29px;
  display: inline-block;
  cursor: pointer;
  margin: 0 15px;
  color: #000;
  line-height: 36px;
  font-size: 20px;
  background-size: contain;
  opacity: 0.8;
  &.confirm{
    background: no-repeat 0 0 url(${imgList['btn_confirm']});
  }
  &.cancel{
    background: no-repeat 0 0 url(${imgList['btn_cancel']});
  }
  &:hover{
    filter: grayscale(0.9)
  }
`
