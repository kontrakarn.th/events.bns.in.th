import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import { setValues } from './../../store/redux';

const CPN = props => {
    let {modal_open} = props;
    return (
        <Modal className={modal_open===props.modalName ? "open":"close"}>
            <ModalBackdrop
                onClick={()=>{if(props.actClickOutside)props.actClickOutside()}}
            />
            <ModalContent>
                {props.children && props.children}
            </ModalContent>
        </Modal>
    )
}

const mstp = state => ({...state.Main});
const mdtp = { setValues };

export default connect( mstp, mdtp )(CPN);

const Modal = styled.div`
    position: fixed;
    top: 0px;
    left: 0px;
    display: block;
    width: 1105px;
    height: 700px;
    overflow: hidden;
    z-index: 7000;
    opacity: 0;
    pointer-events: none;
    &.close {
        opacity: 0;
        pointer-events: none;
    }
    &.open {
        opacity: 1;
        pointer-events: all;
    }
`;
const ModalBackdrop = styled.div`
    position: relative;
    display: block;
    width:100%;
    height: 100%;
    background-color: rgba(0,0,0,0.7);
`;
const ModalContent = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    display: block;
    max-width: 90%;
    max-height: 90%;
    transform: translate3d(-50%,-50%,0);
`;
