import 'core-js/es6/map';
import 'core-js/es6/set';
import 'raf/polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import {combineReducers, createStore} from 'redux';
import {Web} from './routes/Web';
import * as serviceWorker from './serviceWorker';
// import AccountReducer from './reducers/AccountReducer';
import MainRedux from './store/redux';
import layout from './features/F11Layout/redux';

const store = createStore(
	combineReducers({
		Main: MainRedux,
		// AccountReducer,
		// layout,
	})
);
ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<Web />
		</BrowserRouter>
 	</Provider>,
	document.getElementById('root')
)
serviceWorker.unregister();
