import React from 'react';
import {connect} from 'react-redux';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';
import iconDropdown from '../../static/images/icon_dropdown.png'
import { apiPost } from './../../middlewares/Api';


const CPN = props => {
     const actSelectCharacter=(e)=>{
        e.preventDefault();
        // console.log(props.characters)
        let indexCharacter = document.getElementById("charater_form").value;
        if(indexCharacter!=""){
            let idCharacter = props.characters[indexCharacter].char_id;
            let nameCharacter = props.characters[indexCharacter].char_name;

            props.apiSelectCharacter(idCharacter)
        }else{
            props.setValues({
                modal_open:"message",
                modal_message: "โปรดเลือกตัวละคร",
                to_select_char:true,
            });
        }

        // console.log(idCharacter,nameCharacter)
        // props.actSelectCharacter(idCharacter,nameCharacter)
    }
    return (
        <ModalCore
            modalName="selectcharacter"
            actClickOutside={false}
        >
                <div className={"modal__content"}>
                    <div className={"modal__incontent modal__incontent--selectcharacter"}>
                        เลือกตัวละคร<br />
                        <form className="formselect">
                            <label >
                                <select id="charater_form" className="fromselect__list">
                                    <option value="">เลือกตัวละคร</option>
                                    {props.characters && props.characters.map((item,index)=>{
                                        return (
                                            <option key={index} value={index}>{item.char_name}</option>
                                        )
                                    })}
                                </select>
                                <img src={iconDropdown} alt="" />
                            </label>
                        </form>
                    </div>
                    <div className={"modal__bottom"}>
                        <a
                            onClick={(e)=>actSelectCharacter(e)}
                            className="modal__button"
                        >
                            ตกลง
                        </a>
                    </div>
                </div>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.layout,...state.EventReducer});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
