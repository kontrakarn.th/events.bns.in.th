import React from 'react';

import ModalLoading from './ModalLoading';
import ModalDetail from './ModalDetail';
// import ModalConfirm from './ModalConfirm';
import ModalShowReward from './ModalShowReward';
import ModalMessage from './ModalMessage';

export default props => (
    <>
        <ModalLoading />
        <ModalDetail />

        <ModalShowReward />
        <ModalMessage />
    </>
)
