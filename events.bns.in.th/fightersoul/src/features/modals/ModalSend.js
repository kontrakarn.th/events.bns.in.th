import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';

import {imgList} from './../../constants/ImageList';

import bg from './../../static/images/popups/popup_confirm.png';
import btn_tome from './../../static/images/btn_tome.png';
import btn_toyou from './../../static/images/btn_toyou.png';

const CPN = props => {
    let { setValues, modalInput, modalGetName, exchangeinfo } = props;
    return (
        <ModalCore
            modalName="send"
            actClickOutside={()=>props.setValues({modalOpen:""})}
        >
            <ModalContent>
                <div className='itembox'>
                    <div className="mdtitle">การส่งไอเทม</div>
                    <img className="mditem" src={imgList[exchangeinfo.icon]} />
                    <div className="mdname">{exchangeinfo.product_title}</div>
                    <div className="mdname">นักรบ (1)</div>
                </div>
            </ModalContent>
            <ModalBottom>
                <Btns className={"me"}  onClick={()=>{if(props.actSendToMe)props.actSendToMe()}} />
                <Btns className={'you'} onClick={()=>{if(props.actSendToYou)props.actSendToYou()}} />
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}

const ModalContent = styled.div`
    position: relative;
    display: block;
    width: 458px;
    height: 484px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${bg});
    box-sizing: border-box;
    padding-top: 30%;
    .mdtitle {
        display: block;
        font-size: 30px;
        color: #ffffff;
    }
    .mdinput {
        display: block
        background: transparent;
        border: 0;
        position: relative;
        width: 70%;
        color: #FFFFFF;
        height: 1.2em;
        line-height: 1.2em;
        margin: 50px auto;
        font-size: 30px;
        text-align:center;
        overflow:hidden;
        border-bottom: 1px solid #4e4e4e;
        &:focus{
            outline: none;
            animation: none;
        }

    }
`;
const ModalBottom = styled.div`
    position: absolute;
    bottom: 0%;
    left: 50%;
    display: block;
    white-space: nowrap;
    transform: translate3d(-50%,0%,0);
`;
const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 141px;
    height: 48px;
    position:relative;
    text-align: center;
    box-sizing: border-box;
    padding-top: 3%;
    cursor: pointer;
    &:hover {
        background-position: bottom center;
    }
    &.me {
        background:top center no-repeat url(${btn_tome});
    }
    &.you {
        background:top center no-repeat url(${btn_toyou});
    }
    &.disable {
        pointer-events: none;
        filter: grayscale(1);
    }
`;
