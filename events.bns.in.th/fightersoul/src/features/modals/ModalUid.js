import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';

import bg from './../../static/images/popups/popup_alert.png';
import btn_confirm from './../../static/images/btn_confirm.png';
import btn_cancel from './../../static/images/btn_cancel.png';

const CPN = props => {
    let { setValues, modalInput, modalGetName } = props;
    return (
        <ModalCore
            modalName="uid"
            actClickOutside={()=>props.setValues({modalOpen:""})}
        >
            <ModalContent>
                <div className="mdtitle">กรอก UID</div>
                <input
                    autoFocus={true}
                    className="mdinput"
                    name='input_message'
                    type="text"
                    ref={input_message => input_message && input_message.focus()}
                    value={modalInput}
                    onChange={(e)=>setValues({modalInput:e.target.value})}
                />
            </ModalContent>
            <ModalBottom>
                <Btns
                    className={modalInput === '' && 'disable'}
                    onClick={()=>{if(props.actConfirm)props.actConfirm()}} />
                <Btns className={'cancel'} onClick={()=>setValues({modalOpen:""})} />
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}

const ModalContent = styled.div`
    position: relative;
    display: block;
    width: 458px;
    height: 484px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${bg});
    box-sizing: border-box;
    padding-top: 30%;
    .mdtitle {
        display: block;
        font-size: 30px;
        color: #ffffff;
    }
    .mdinput {
        display: block
        background: transparent;
        border: 0;
        position: relative;
        width: 70%;
        color: #FFFFFF;
        height: 1.2em;
        line-height: 1.2em;
        margin: 50px auto;
        font-size: 30px;
        text-align:center;
        overflow:hidden;
        border-bottom: 1px solid #4e4e4e;
        &:focus{
            outline: none;
            animation: none;
        }

    }
`;
const ModalBottom = styled.div`
    position: absolute;
    bottom: 0%;
    left: 50%;
    display: block;
    white-space: nowrap;
    transform: translate3d(-50%,0%,0);
`;
const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 141px;
    height: 48px;
    background:top center no-repeat url(${btn_confirm});
    position:relative;
    text-align: center;
    box-sizing: border-box;
    padding-top: 3%;
    cursor: pointer;
    &:hover {
        background-position: bottom center;
    }
    &.cancel {
        background:top center no-repeat url(${btn_cancel});
    }
    &.disable {
        pointer-events: none;
        filter: grayscale(1);
    }
`;
const InputSend = styled.input`
        display: block
        background: transparent;
        // background: rgba(0,0,0,0);
        border: 0;
        position: relative;
        // top: 45%;
        width: 70%;
        // left: 50%;
        color: #FFFFFF;
        height: 1.2em;
        // box-sizing: border-box;
        // padding: 2% 0;
        // transform: translate3d(-50%,-50%,0);
        font-size: 30px;
        text-align:center;
        overflow:hidden;
        &:focus{
            outline: none;
        }
`
