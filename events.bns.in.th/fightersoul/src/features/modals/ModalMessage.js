import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';

import bg from './../../static/images/popups/popup_alert.png';
import btn_confirm from './../../static/images/btn_confirm.png';
import btn_cancel from './../../static/images/btn_cancel.png';


const CPN = props => {
    let { setValues, modalMessage } = props;
    return (
        <ModalCore
            modalName="message"
            actClickOutside={()=>setValues({modalOpen:""})}
        >
            <ModalContent>
                <div className='mdtext' dangerouslySetInnerHTML={{ __html: modalMessage}}/>
            </ModalContent>
            <ModalBottom>
                <Btns onClick={()=>setValues({modalOpen:''})} />
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ModalContent = styled.div`
    position: relative;
    display: block;
    width: 458px;
    height: 484px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${bg});
    box-sizing: border-box;
    padding-top: 40%;
    .mdtext {
        display: block;
        font-size: 30px;
        color: #ffffff;
    }
    .mditem {
        display: block;
    }
    .mdname {
        display: block;
        font-size: 20px;
    }
`;

const ModalBottom = styled.div`
    position: absolute;
    bottom: 0%;
    left: 50%;
    display: block;
    white-space: nowrap;
    transform: translate3d(-50%,0%,0);
`;

const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 141px;
    height: 48px;
    background:top center no-repeat url(${btn_confirm});
    position:relative;
    text-align: center;
    box-sizing: border-box;
    padding-top: 3%;
    &:hover{
        background-position: bottom center;
    }
    &.cancel{
        background:top center no-repeat url(${btn_cancel});
    }
`;
