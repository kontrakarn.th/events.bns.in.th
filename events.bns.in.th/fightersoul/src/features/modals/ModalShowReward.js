import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';

import {imgList} from './../../constants/ImageList';

import bg from './../../static/images/popups/popup_alert.png';
import btn_confirm from './../../static/images/btn_confirm.png';
import btn_cancel from './../../static/images/btn_cancel.png';




const CPN = props => {
    let { setValues, modalOpen, modalMessage, modalItem} = props;
    return (
        <ModalCore
            modalName="showreward"
            actClickOutside={()=>setValues({modalOpen:"",modalItem:-1})}
        >
            <ModalContent>
                <div className='itembox'>
                    <div className="mdtext">ยินดีด้วย คุณได้รับ</div>
                    <img className="mditem" src={imgList[modalItem.icon]} />
                    <div className="mdname" dangerouslySetInnerHTML={{__html: modalItem.product_title}} />
                </div>
            </ModalContent>
            <ModalBottom>
                <Btns onClick={()=>setValues({modalOpen:"",modalItem:-1})} />
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(CPN);

const ModalContent = styled.div`
    position: relative;
    display: block;
    width: 458px;
    height: 484px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${bg});
    box-sizing: border-box;
    padding-top: 35%;
    .mdtext {
        display: block;
        font-size: 30px;
        color: #ffffff;
    }
    .mditem {
        margin: 10px auto;
        display: block;
    }
    .mdname {
        display: block;
        font-size: 20px;
    }
`;

const ModalBottom = styled.div`
    position: absolute;
    bottom: 0%;
    left: 50%;
    display: block;
    white-space: nowrap;
    transform: translate3d(-50%,0%,0);
`;

const Btns = styled.div`
    display: inline-block;
    margin: 0px 10px;
    width: 141px;
    height: 48px;
    background:top center no-repeat url(${btn_confirm});
    position:relative;
    text-align: center;
    box-sizing: border-box;
    padding-top: 3%;
    &:hover{
        background-position: bottom center;
    }
    &.cancel{
        background:top center no-repeat url(${btn_cancel});
    }
`;
