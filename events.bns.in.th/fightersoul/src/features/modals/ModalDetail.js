import React from 'react';
import {connect} from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from './../../store/redux';

import popup_detail from './../../static/images/popups/popup_detail.png';

const CPN = props => {
    return (
        <ModalCore modalName="detail" actClickOutside={()=>props.setValues({modalOpen: ""})}>
            <ModalDetailStyle />
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.EventReducer});
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalDetailStyle = styled.div`
    display: block;
    width: 993px;
    height: 484px;
    background: no-repeat top center url(${popup_detail});
`;
