import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from './../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import { Menu } from '../components/Menu';

import ModalConfirmReceiveGacha from '../components/ModalConfirmReceiveGacha';
import ModalConfirmRandomGachapon from '../components/ModalConfirmRandomGachapon';
import ModalPackageDetails from '../components/ModalPackageDetails'

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';
import diamon from './../static/images/diamon.png'
import items from './../static/images/items.png'

import items1 from './../static/images/items/1.png'
import items2 from './../static/images/items/2.png'
import items3 from './../static/images/items/3.png'
import items4 from './../static/images/items/4.png'
import items5 from './../static/images/items/5.png'
import items6 from './../static/images/items/6.png'
import items7 from './../static/images/items/7.png'
import items8 from './../static/images/items/8.png'
import items9 from './../static/images/items/9.png'
import items10 from './../static/images/items/10.png'
import {
    onAccountLogout,
    setUsername,
    setSoulPermission
} from '../actions/AccountActions';

class HomeContainer extends Component {

    apiEventInfo() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "event_info" };
        let successCallback = (data) => {

            if (data.status) {
                this.props.setUsername(data.data.nickname);
                this.setState({
                    permission: true,
                    showLoading: false,
                    can_play: data.data.can_play
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CHECKIN", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiRandomItem() {
        this.setState({
            confirmRandom: false,
            showLoading:true,
        });

        let self = this;
        let dataSend = { type: "randomitem" };
        let successCallback = (data) => {

            if (data.status) {
                this.setState({
                    item_get:data.content[0],
                    showLoading: false,
                    can_play: data.data.can_play,
                    confirmReceiveGacha:true,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_RANDOM_ITEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
            confirmRandom: false,
            confirmRandomMessage: "",
            confirmReceiveGacha: false,
            confirmReceiveGachaMessage: "",
            packageDetails: false,
            PackageDetailsMessage: "",
            can_play:false,
            item_get:{},
            items: [
                {
                    img: items1,
                    name: 'แพ็คเกจโอลิมเปียแห่งอิสรภาพฯ',
                    count: '1'
                },
                {
                    img: items2,
                    name: 'ชุดเงาจันทร์',
                    count: '1'
                },
                {
                    img: items3,
                    name: 'กรุฮงมุน ที่ส่องสว่าง',
                    count: '1'
                },
                {
                    img: items4,
                    name: 'ถุงเพชร แปดเหลี่ยม',
                    count: '1'
                },
                {
                    img: items5,
                    name: 'คริสตัลวิญญาณฮงมุน',
                    count: '1'
                },
                {
                    img: items6,
                    name: 'กรุพิศวง',
                    count: '3'
                },
                {
                    img: items7,
                    name: 'กรุฮงมุน',
                    count: '1'
                },
                {
                    img: items8,
                    name: 'หินเปลี่ยนรูป ชั้นสูง',
                    count: '3'
                },
                {
                    img: items9,
                    name: 'คริสตัลอัญมณี หยินหยาง',
                    count: '1'
                },
                {
                    img: items10,
                    name: 'คริสตัลอัญมณี หยินหยาง',
                    count: '2'
                },
            ],
            items_api: [
                {
                    img: items9,
                    name: 'คริสตัลอัญมณี หยินหยาง',
                    count: '1'
                },
                {
                    img: items10,
                    name: 'คริสตัลอัญมณี หยินหยาง',
                    count: '2'
                },
                {
                    img: items8,
                    name: 'หินเปลี่ยนรูป ชั้นสูง',
                    count: '3'
                },
                {
                    img: items7,
                    name: 'กรุฮงมุน',
                    count: '1'
                },
                {
                    img: items6,
                    name: 'กรุพิศวง',
                    count: '3'
                },
                {
                    img: items5,
                    name: 'คริสตัลวิญญาณฮงมุน',
                    count: '1'
                },
                {
                    img: items4,
                    name: 'ถุงเพชร แปดเหลี่ยม',
                    count: '1'
                },
                {
                    img: items3,
                    name: 'กรุฮงมุน ที่ส่องสว่าง',
                    count: '1'
                },
                {
                    img: items2,
                    name: 'ชุดเงาจันทร์',
                    count: '1'
                },
                {
                    img: items1,
                    name: 'แพ็คเกจโอลิมเปียแห่งอิสรภาพฯ',
                    count: '1'
                },
            ]
        }
    }

    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 1000);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo()
            }, 1000);
        }
    }

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scollDown === false) this.setState({ scollDown: true });
        } else {
            if (this.state.scollDown === true) this.setState({ scollDown: false });
        }
    }
    onConfirmRandom(){
        this.setState({
            //itemData: item_data,
            confirmRandom: true,
            confirmRandomMessage: "ยืนยันการสุ่มกาชา <br/> มูลค่า 10000 ไดมอนด์"
        });
    }
    apiConfirmReceiveGacha() {
        this.apiRandomItem();

    }
    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home"><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: '700px', opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                        >
                            <div className="preorder">
                                <div className="preorder-inner">
                                    <Menu />
                                    <section className="preorder__section preorder__section--header">
                                        <div className="header">
                                            <div className="header__scroll"><img src={iconScroll} alt="" /></div>
                                        </div>
                                    </section>
                                    <section className="preorder__section condition bg__blog">
                                        <div className="container">
                                            <div className="section__wrapper">
                                                <div className="section__head">
                                                    <h3>รายละเอียดโปรโมชั่น</h3>
                                                </div>
                                                <div className="section__content">
                                                    <ul className="condition__text">
                                                        <li>ผู้เล่นสามารถเปิดกล่องกาชาแห่งอิสรภาพฯ ในราคา 10,000 ไดมอนด์/ชิ้น ซึ่งจะทำการสุ่มไอเทม 1 ประเภท ตามรายการข้างล่าง</li>
                                                        <li>ไอเทมกล่องกาชาแห่งอิสรภาพฯ จะแสดงผลบนเว็บไซต์โปรโมชั่นนี้เท่านั้น</li>
                                                        <li>ไอเทมทุกชิ้นที่ได้รับจะถูกส่งเข้ากล่องสัมภาระในไอดีที่ใช้งานอัตโนมัติ ยกเว้น แพ็คเกจโอลิมเปียแห่งอิสรภาพ</li>
                                                        <li>หากผู้เล่นได้รับแพ็คเกจโอลิมเปียแห่งอิสรภาพ จะต้องกดรับด้วยตนเองใน 'ระบบแลกเปลี่ยน' หรือสามารถส่งไอเทมนี้ให้กับผู้อื่นได้ โดยมีค่าใช้จ่ายที่ 10,000 ไดมอนด์/ครั้ง</li>
                                                        <li>ระยะเวลาของโปรโมชั่น  ตั้งแต่วันที่ 31 กรกฎาคม - 14 สิงหาคม 2562 เวลา 23:59 น.</li>
                                                        <li>ปิดระบบส่งของขวัญ  วันที่ 21 สิงหาคม 2562 เวลา 23:59 น.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section className="preorder__section random__gacha bg__blog text-center">
                                        <div className="container">
                                            <div className="section__wrapper">
                                                <div className="items">
                                                    <div className="items__img">
                                                        <img src={items}/>
                                                    </div>

                                                    <div className="items__content">
                                                        <h3>กล่องกาชาแห่งอิสรภาพ ที่เปล่งประกาย</h3>
                                                        <div className="text--yellow text-diamon">10,000 <img src={diamon}/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="section__btn">
                                            {
                                                this.state.can_play==true
                                                ?
                                                    <button type="button" className="btn btn-default" onClick={this.onConfirmRandom.bind(this)}>สุ่มกาชา</button>
                                                :
                                                    <button type="button" className="btn btn-default disabled" >สุ่มกาชา</button>
                                            }
                                            </div>
                                        </div>
                                    </section>
                                    <section className="preorder__section bg__blog text-center">
                                        <div className="container">
                                            <div className="section__wrapper">
                                                <div className="section__head">
                                                    <h3>ไอเทมที่มีโอกาสได้รับ</h3>
                                                </div>
                                                {
                                                    this.state.items.map((item,key)=>{
                                                        return(
                                                            <div key={key} className="itemsInline">
                                                                <div className="itemsInline__img">
                                                                    <img src={item.img}/>
                                                                    {
                                                                        key==0
                                                                        ?
                                                                            <div className="icon-plus" onClick={() => { this.setState({ packageDetails: true }) }}></div>
                                                                        :
                                                                            null
                                                                    }

                                                                </div>
                                                                <div className="itemsInline__content">
                                                                    <div>{item.name}</div>
                                                                    <div>({item.count})</div>
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </ScrollArea>
                        <ModalConfirmRandomGachapon
                            open={this.state.confirmRandom}
                            actConfirm={() => this.apiConfirmReceiveGacha()}
                            actClose={() => this.setState({
                                confirmRandom: false
                            })}
                            msg={this.state.confirmRandomMessage}
                        />
                        <ModalConfirmReceiveGacha
                            open={this.state.confirmReceiveGacha}
                            actConfirm={() => this.setState({
                                confirmReceiveGacha: false
                            })}
                            actClose={() => this.setState({
                                confirmReceiveGacha: false
                            })}
                            msg={this.state.confirmReceiveGachaMessage}
                            item_get={this.state.items_api[this.state.item_get.id-1]}
                        />

                        <ModalPackageDetails
                            open={this.state.packageDetails}
                            actClose={() => this.setState({
                                packageDetails: false
                            })}
                            msg={this.state.PackageDetailsMessage}
                        />
                    </div>
                    :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />

            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    permission: state.AccountReducer.permission
})

const mapDispatchToProps = {
    onAccountLogout, setUsername, setSoulPermission
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
