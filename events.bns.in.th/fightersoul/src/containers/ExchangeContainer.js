import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import {apiPost} from '../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';
import ModalMessage from '../components/ModalMessage';

import {Menu} from '../components/Menu';

import iconHome from './../static/images/buttons/btn_home.png';
import items from './../static/images/items/1.png'

import {
    onAccountLogout,
    setUsername,
    setBuySoulResult
} from '../actions/AccountActions';
import ModalSendPackage from '../components/ModalSendPackage';
import ModalInput from '../components/ModalInput';
import ModalPackageDetails from '../components/ModalPackageDetails';
import ModalConfirmSend from '../components/ModalConfirmSend';

class ExchangeContainer extends Component {

// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)
        this.textInput = React.createRef();
        this.state = {
            permission: true,
            showLoading: true,
            scollDown: false,
            showModalMessage: "",

            modalConfirmSend: false,

            confirmRedeem: false,
            confirmRedeemMessage: "",
            packageDetails: false,
            PackageDetailsMessage: "",
            modalInput: false,
            itemData: [],

            exchangeinfo: [],
            uidSend:"",
            nameSend:"",
            showMenu: false,

        }
    }

    resetState(){
        this.setState({
            sendPackage:false,
            modalConfirmSend: false,
            confirmRedeem: false,
            packageDetails: false,
            modalInput: false,
        });
    }

    handleClickMenu() {
        // console.log(this.state.showMenu)
        this.setState({
            showMenu:!this.state.showMenu
        })
    }

    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiGetExchangeInfo(), 1000);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiGetExchangeInfo();
            }, 1000);
        }
    }

    apiGetExchangeInfo() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "getexchangeinfo" };
        let successCallback = (data) => {

            if (data.status) {
                this.setState({
                    permission: true,
                    showLoading: false,
                    exchangeinfo: data.content,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_GETEXCHANGEINFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiGetName(uid) {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "getname",sendto:uid };
        let successCallback = (data) => {

            if (data.status) {
                this.setState({
                    permission: true,
                    showLoading: false,
                    modalConfirmSend:true,
                    nameSend: data.content.name,
                })

            }else {
                this.setState({
                    modalConfirmSend:false,
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                    modalConfirmSend:false,
                });

            }else {
                this.setState({
                    modalConfirmSend:false,
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_GET_NAME", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSendExchangeToMe() {

        this.resetState()
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "exchangeitem" };
        let successCallback = (data) => {

            if (data.status) {
                this.setState({
                    permission: true,
                    showLoading: false,
                    exchangeinfo: data.content,
                    showModalMessage: "ส่งไอเท็มสำเร็จ",
                })
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EXCHANGEITEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSendExchangeToOther() {
        this.resetState()
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "exchangeitem",sendto:this.state.uidSend };
        let successCallback = (data) => {

            if (data.status) {
                this.setState({
                    permission: true,
                    showLoading: false,
                    exchangeinfo: data.content,
                    showModalMessage: "ส่งไอเท็มสำเร็จ",
                })
            }else{
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }

        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EXCHANGEITEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    handleScroll(e) {
        if(e.topPosition > 10){
            if(this.state.scollDown === false) this.setState({scollDown: true});
        } else {
            if(this.state.scollDown === true) this.setState({scollDown: false});
        }
    }

    onSelectSend(){
        this.setState({
            sendPackage: true
        });
    }

    onSendPackage(){
        this.setState({
            modalInput: true,
        },()=>{
            setTimeout(()=>{
                document.getElementById("target_uid").focus();
            }, 100);
        });
    }

    actConfirm(uid){
        this.setState({
            uidSend:uid
        },()=>{
            this.apiGetName(uid)
        });
    }

    render() {
        let _i = 1;
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {this.state.permission ?
                    <div className="events-bns">
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height:700, opacity:1}}
                            verticalScrollbarStyle={{backgroundColor:"#e79dee", opacity:1}}
                            onScroll={this.handleScroll.bind(this)}

                        >
                            <div className="preorder">
                                <div className="page__wrapper exchange">
                                    <Menu />
                                    <section className="preorder__section text-center">
                                        <div className="container">
                                            <div className="section__wrapper">
                                                <div className="section__head">
                                                    <h3>ระบบแลกเปลี่ยน</h3>
                                                </div>
                                                <div className="section__content">
                                                    <div className="items">
                                                        <div className="items__img">
                                                            <img src={items}/>
                                                            <div className="icon-plus" onClick={() => { this.setState({ packageDetails: true }) }}></div>
                                                        </div>
                                                        {
                                                            this.state.exchangeinfo && this.state.exchangeinfo.length>0
                                                            ?
                                                                <div className="items__content">
                                                                    <h3>แพ็คเกจโอลิมเปียแห่งอิสรภาพฯ</h3>
                                                                    <h3>จำนวนที่เหลือ {this.state.exchangeinfo[0].amt} ชิ้น</h3>
                                                                    <button type="button" className={"btn btn-default "+(this.state.exchangeinfo[0].amt > 0 ? "" : "disabled")} onClick={this.state.exchangeinfo[0].amt > 0 ? this.onSelectSend.bind(this) : ()=>{}}>ส่งไอเทม</button>
                                                                </div>
                                                            :
                                                                <div className="items__content">
                                                                    <h3>แพ็คเกจโอลิมเปียแห่งอิสรภาพฯ</h3>
                                                                    <h3>จำนวนที่เหลือ 0 ชิ้น</h3>
                                                                    <button type="button" className="btn btn-default disabled">ส่งไอเทม</button>
                                                                </div>
                                                        }

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>

                                </div>
                            </div>
                        </ScrollArea>


                        <ModalSendPackage
                            open={this.state.sendPackage}
                            // actConfirm={() => this.apiConfirmRedeem()}
                            actClose={() => this.setState({
                                sendPackage: false
                            })}
                            msg={this.state.sendPackageMessage}
                            amt={this.state.exchangeinfo && this.state.exchangeinfo.length>0 ? this.state.exchangeinfo[0].amt : 0}
                            actSendToMe={this.state.exchangeinfo && this.state.exchangeinfo.length>0 && this.state.exchangeinfo[0].amt ? this.apiSendExchangeToMe.bind(this) : ()=>{}}
                            actSendToFriend={this.onSendPackage.bind(this)}

                        />

                         <ModalInput
                            open={this.state.modalInput}
                            actConfirm={this.actConfirm.bind(this)}
                            actClose={() => this.setState({
                                modalInput: false
                            })}
                        />
                         <ModalConfirmSend
                            open={this.state.modalConfirmSend}
                            actConfirm={this.apiSendExchangeToOther.bind(this)}
                            uidsend={this.state.uidSend}
                            namesend={this.state.nameSend}
                            actClose={() => this.setState({
                                modalConfirmSend: false
                            })}
                        />
                        <ModalPackageDetails
                            open={this.state.packageDetails}
                            //actConfirm={() => this.apiConfirmRandom()}
                            actClose={() => this.setState({
                                packageDetails: false
                            })}
                            msg={this.state.PackageDetailsMessage}
                        />

                        <ModalMessage
                           open={this.state.showModalMessage.length>0 ? true : false}
                           actClose={() => this.setState({
                               showModalMessage: ""
                           })}
                           msg={this.state.showModalMessage}
                       />

                    </div>
                :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
})

const mapDispatchToProps = {
    onAccountLogout,
    setUsername,
    // setSoulPermission,
    setBuySoulResult
}
export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(ExchangeContainer))
