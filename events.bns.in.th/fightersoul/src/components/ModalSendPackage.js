import React from 'react';
import Modal from './Modal';
import items1 from './../static/images/items/1.png'


export default class ModalSendPackage extends React.Component {
    render() {
        let title = this.props.data && this.props.data || [];
        return (
            <Modal
                open={this.props.open}
                title="ยืนยัน"
                confirmMode={false}
                removeConfirmBtn={true}
            >
                <div onClick={()=>this.props.actClose()} className="icon-close">x</div>
                <div className="modalPackage__wrapper">
                    <div>
                        <div className="itemsInline__img">
                            <img src={items1}/>
                        </div>
                        <div className="itemsInline__content">
                            <div>การส่งแพ็คเกจ</div>
                            <div style={{color:"#c4a193"}}>แพ็คเกจโอลิมเปียแห่งอิสรภาพฯ ({this.props.amt})</div>
                        </div>
                    </div>
                </div>
                <div className="inbox" dangerouslySetInnerHTML={{__html: this.props.msg}} />
                <div className="modal__bottom">
                    <a className="modal__button" onClick={()=>this.props.actSendToMe()}>ส่งให้ตัวเอง</a>
                    <a className="modal__button" onClick={()=>this.props.actSendToFriend()}>ส่งให้เพื่อน</a>
                </div>
            </Modal>
        )
  }
}
