import React from 'react';
import Modal from './Modal';
import items from '../static/images/items.png'
export default class ModalConfirmReceiveGacha extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                title="แจ้งเตือน"
                confirmMode={false}
                removeConfirmBtn={false}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
            {
                this.props.item_get
                ?
                    <div className="inbox">
                        <img src={this.props.item_get.img}/>
                       <h2>ยินดีด้วย คุณได้รับ</h2>
                        <div className="text--brown">{this.props.item_get.name} ({this.props.item_get.count})</div>
                    </div>
                :
                    null
            }

            </Modal>
        )
  }
}
