import React from 'react';


export default props => {
    let { width, height, onClick, src } = props;
    return (
        <a onClick>
            <canvas
                width
                height
                style={{
                    background:  'no-repeat top center url('+src+");"
                }}
            />
        </a>
    )
}
