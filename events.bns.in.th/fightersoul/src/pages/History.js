import React from 'react';
import 'whatwg-fetch';
import { connect } from 'react-redux';
import styled from 'styled-components';
import F11Layout from './../features/F11Layout/';
import ScrollArea from 'react-scrollbar';
import { apiPost } from '../middlewares/Api';
import { setValues } from './../store/redux';

import bg from './../static/images/history_background.jpg';

class History extends React.Component {
    //==========================================================================
    // Api
    //==========================================================================
    apiItemHistory() {
        let { setValues, jwtToken } = this.props;
        setValues({ modalOpen: "loading" });

        let dataSend = { type: "historylist" };
        let successCallback = (data) => {
            if (data.status) {
                setValues({
                    permission: true,
                    modalOpen: "",
                    historyList: data.content
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                setValues({
                    modalOpen: "",
                    permission: false,
                });

            }else {
                this.setState({
                    modalOpen: "message",
                    modalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_HISTORYLIST", jwtToken, dataSend, successCallback, failCallback);
    }
    //==========================================================================
    // Function
    //==========================================================================

    //---

    //==========================================================================
    // React
    //==========================================================================
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    componentDidMount() {
        if (this.props.jwtToken !== "") this.apiItemHistory();
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== "" && this.props.jwtToken !== prevProps.jwtToken) this.apiItemHistory();
    }

    render() {
        let { historyList } = this.props;
        return (
            <F11Layout showUserName={true} showCharacterName={false} showLeftMenu={true} >
                <HistoryStyle>
                    <ScrollArea
                        speed={0.8}
                        className="area"
                        contentClassName=""
                        horizontal={false}
                        style={{ width: 720, height:350, opacity:1, margin: "0px auto 0px",color: "#FFFFFF"}}
                        verticalScrollbarStyle={{backgroundColor:"#000", opacity:1}}
                    >
                        <ul className="history__list">
                            {historyList.map((item,index)=>{
                                if(item.send_type=="owner"){
                                    if(item.product_title=="แพ็คเก็จโอลิมเปียแห่งอิสรภาพ"){
                                        return(
                                            <li className="history__slot" key={"history_"+index}>
                                                <div>ส่ง {item.product_title} ให้ตัวเอง</div>
                                                <div>{item.updated_at}</div>
                                            </li>
                                        )
                                    }else{
                                        return(
                                            <li className="history__slot" key={"history_"+index}>
                                                <div>ได้รับ {item.product_title}</div>
                                                <div>{item.updated_at}</div>
                                            </li>
                                        )
                                    }

                                }else if(item.send_type=="none"){
                                    return(
                                        <li className="history__slot" key={"history_"+index}>
                                            <div>จัดเก็บ {item.product_title} ในเว็ป</div>
                                            <div>{item.updated_at}</div>
                                        </li>
                                    )
                                }else{
                                    return(
                                        <li className="history__slot" key={"history_"+index}>
                                            <div>ส่ง {item.product_title} ให้(UID {item.send_to})</div>
                                            <div>{item.updated_at}</div>
                                        </li>
                                    )
                                }
                            })}
                        </ul>
                    </ScrollArea>
                </HistoryStyle>
            </F11Layout>
        )
    }
}

const mapStateToProps = (state) => ({ ...state.Main })

const mapDispatchToProps = { setValues }
export default connect( mapStateToProps, mapDispatchToProps )(History)

const HistoryStyle = styled.div`
    position: relative;
    display: block;
    width: 1105px;
    height: 700px;
    background: no-repeat top center url(${bg});
    padding-top: 330px;
    box-sizing: border-box;
    .history__slot {
        display: flex;
        justify-content: space-between;
        align-items: center;
        width: 90%;
        overflow: visible;
        margin: 0px auto;
    }
`;
