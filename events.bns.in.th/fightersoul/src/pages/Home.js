import React from 'react';
import 'whatwg-fetch';
import { connect } from 'react-redux';
import styled from 'styled-components';
import './../styles/preorder.css';

import { apiPost } from './../middlewares/Api';

// import { Permission } from '../components/Permission';

import F11Layout from './../features/F11Layout/';
import ModalConfirm from './../features/modals/ModalConfirm';


import bg from './../static/images/home_background.jpg';
import blog_rule from './../static/images/blog_rule.png';
import blog_gacha from './../static/images/blog_gacha.png';
import blog_detail from './../static/images/blog_detail.png';
import btn_gacha from './../static/images/btn_gacha.png';
import icon_plus from './../static/images/icon_plus.png';

import { setValues } from './../store/redux';

import './../styles/index.css';
import './../styles/modal.css';

class Home extends React.Component {

    //==========================================================================
    // Api
    //==========================================================================
    apiEventInfo() {
        let { setValues } = this.props;
        setValues({ modalOpen: "loading" });

        let dataSend = { type: "event_info" };
        let successCallback = (data) => {
            setValues({
                permission: true,
                modalOpen: "",
                username: data.data.nickname,
                can_play: data.data.can_play
            })
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                setValues({
                    modalOpen: "",
                    permission: false,
                });

            }else {
                setValues({
                    modalOpen: "message",
                    modalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CHECKIN", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiRandomItem() {
        let { setValues } = this.props;
        setValues({ modalOpen: "loading" });

        let dataSend = { type: "randomitem" };
        let successCallback = (data) => {
            setValues({
                permission: true,
                modalOpen: "showreward",
                modalItem: data.content[0],
                can_play: data.data.can_play,
            })
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                setValues({
                    modalOpen: "",
                    permission: false,
                });
            }else {
                setValues({
                    modalOpen: "message",
                    modalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_RANDOM_ITEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    //==========================================================================
    // Function
    //==========================================================================

    onConfirmRandom(){
        this.setState({
            //itemData: item_data,
            confirmRandom: true,
            confirmRandomMessage: "ยืนยันการสุ่มกาชา <br/> มูลค่า 10000 ไดมอนด์"
        });
    }
    apiConfirmReceiveGacha() {
        this.apiRandomItem();
    }

    //==========================================================================
    // React
    //==========================================================================
    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            can_play: true,
        }
    }

    componentDidMount() {
        if (this.props.jwtToken !== "") this.apiEventInfo();
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== "" && this.props.jwtToken !== prevProps.jwtToken) {
            this.apiEventInfo();
        }
    }

    render() {
        let { setValues, can_play } = this.props;

        return (
            <F11Layout showUserName={true} showCharacterName={false} showLeftMenu={true} >
                <HomeStyle>
                    <section className="section section--free">
                    </section>
                    <section className="section section--rule">
                    </section>
                    <section className="section section--gacha">
                        <a
                            className={"section__btn section__btn--gacha"+(can_play? "":" disable")}
                            onClick={()=>{if(can_play) setValues({modalOpen: "confirm"})}}
                        />
                    </section>
                    <section className="section section--detail">
                        <a
                            className={"section__btn section__btn--plus"}
                            onClick={()=>setValues({ modalOpen: "detail" }) }
                        />
                    </section>
                </HomeStyle>

                <ModalConfirm actConfirm={()=>this.apiConfirmReceiveGacha()}/>
            </F11Layout>
        )
    }
}

const mapStateToProps = (state) => ({ ...state.Main });
const mapDispatchToProps = {  setValues };
export default connect( mapStateToProps, mapDispatchToProps )(Home);

const HomeStyle = styled.div`
    position: relative;
    display: block;
    width: 1105px;
    height: 2817px;
    background: no-repeat top center url(${bg});
    .section {
        position: relative;
        display: block;
        width: 993px;
        margin: 0px auto;
        &--free {
            height: 660px;
        }
        &--rule {
            height: 563px;
            background: no-repeat top center url(${blog_rule});
        }
        &--gacha {
            margin-top: 100px;
            height: 563px;
            background: no-repeat top center url(${blog_gacha});
        }
        &--detail {
            margin-top: 100px;
            height: 726px;
            background: no-repeat top center url(${blog_detail});
        }
        &__btn {
            position: absolute;
            &--gacha {
                bottom: 0%;
                left: 50%;
                display: block;
                width: 300px;
                height: 96px;
                transform: translate3d(-50%,30%,0);
                background: no-repeat bottom center url(${btn_gacha});
                &:hover {
                    background-position: top center;
                }
                &.disable {
                    pointer-events: none;
                    filter: grayscale(1);
                }
            }
            &--plus {
                top: 35%;
                left: 19%;
                display: block;
                width: 39px;
                height: 39px;
                background: no-repeat center url(${icon_plus});
                filter: grayscale(0.4);
                &:hover {
                    filter: grayscale(0);
                }
            }
        }
    }

`;
