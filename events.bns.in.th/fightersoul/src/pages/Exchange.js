import React from 'react';
import 'whatwg-fetch';
import {connect} from 'react-redux';
import styled from 'styled-components';
import F11Layout from './../features/F11Layout/';
import {imgList} from './../constants/ImageList';

import {apiPost} from '../middlewares/Api';
import { setValues } from '../store/redux';
//
// import ModalSendPackage from '../components/ModalSendPackage';
// import ModalInput from '../components/ModalInput';
// import ModalPackageDetails from '../components/ModalPackageDetails';
// import ModalConfirmSend from '../components/ModalConfirmSend';

import ModalSend from './../features/modals/ModalSend';
import ModalUid from './../features/modals/ModalUid';
import ModalConfirmTrade from './../features/modals/ModalConfirmTrade';

import bg from './../static/images/exchange_background.jpg';
import btn_send from './../static/images/btn_send.png';
import icon_plus from './../static/images/icon_plus.png';
import FTS_002 from './../static/images/items/FTS_002.png';
import FTS_001 from './../static/images/items/FTS_001.png';

class Exchange extends React.Component {

    //==========================================================================
    // Api
    //==========================================================================
    apiGetExchangeInfo() {
        let { setValues } = this.props;
        setValues({ modalOpen: "loading" });

        let dataSend = { type: "getexchangeinfo" };
        let successCallback = (data) => {
            if (data.status) {
                setValues({
                    permission: true,
                    modalOpen: "",
                    exchangeinfo: data.content[0],
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                setValues({
                    modalOpen: "",
                    permission: false,
                });

            }else {
                setValues({
                    modalOpen: "message",
                    modalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_GETEXCHANGEINFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiGetName() {
        let { setValues, modalInput } = this.props;
        setValues({ modalOpen: "loading" });

        let dataSend = { type: "getname", sendto:modalInput };
        let successCallback = (data) => {
            if (data.status) {
                setValues({
                    permission: true,
                    modalOpen: "confirmtrade",
                    modalConfirmSend:true,
                    modalMessage:`ยืนยันการส่งไอเทม<br/>ค่าธรรมเนียมการส่ง<br/>10,000 ไดมอนด์<br/>ไปยัง UID ${modalInput}<br/>(${ data.content.name})`
                })
            }else {
                setValues({
                    modalOpen: "message",
                    modalMessage: data.message,
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                setValues({
                    modalOpen: "",
                    permission: false,
                });

            }else {
                this.setState({
                    modalOpen: "message",
                    modalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_GET_NAME", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiSendExchange(other) {
        let { setValues, modalInput, exchangeinfo } = this.props;
        setValues({ modalOpen: "loading" });

        let dataSend = { type: "exchangeitem" };
        if(other === "other"){
            dataSend.sendto = modalInput;
        }

        let successCallback = (data) => {
            setValues({
                permission: true,
                modalOpen: "message",
                exchangeinfo: data.content[0],
                modalMessage: data.message,
            });
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                setValues({
                    permission: false,
                    modalOpen: "",
                });
            }else {
                setValues({
                    modalOpen: "message",
                    modalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EXCHANGEITEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    //==========================================================================
    // Function
    //==========================================================================

    handleClickMenu() {
        // console.log(this.state.showMenu)
        this.setState({
            showMenu:!this.state.showMenu
        })
    }
    onSelectSend(b){
        if(b){
            this.setState({
                sendPackage: true
            });
        }
    }

    onSendPackage(){
        this.setState({
            modalInput: true,
        },()=>{
            setTimeout(()=>{
                document.getElementById("target_uid").focus();
            }, 100);
        });
    }
    //==========================================================================
    // React
    //==========================================================================
// ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    componentDidMount() {
        if (this.props.jwtToken !== "") this.apiGetExchangeInfo();
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== "" && this.props.jwtToken !== prevProps.jwtToken) this.apiGetExchangeInfo();
    }
    render() {
        let { setValues, modalOpen, exchangeinfo } = this.props;
        let canTrade = (exchangeinfo && exchangeinfo.amt>0)? true: false;

        return (
            <F11Layout showUserName={true} showCharacterName={false} showLeftMenu={true} >
                <ExchangeStyle>
                    {exchangeinfo.icon !== "" &&
                        <div className="exchange__set">
                            <div className="exchange__image">
                                <img src={imgList[exchangeinfo.icon]} />
                                <a
                                    className={"exchange__btn exchange__btn--plus"}
                                    onClick={()=>setValues({ modalOpen: "detail" }) }
                                />
                            </div>
                            <div className="exchange__name">{exchangeinfo.product_title}</div>
                            <div className="exchange__count">จำนวนที่เหลือ {exchangeinfo.amt}</div>
                        </div>
                    }
                    <a
                        className={"exchange__btn exchange__btn--send"+(canTrade? "":" disable")}
                        onClick={()=>setValues({ modalOpen: "send" }) }
                    />
                </ExchangeStyle>

                <ModalSend
                    actSendToMe={()=>this.apiSendExchange("")}
                    actSendToYou={()=>setValues({modalOpen:"uid"})}
                />
                <ModalUid
                    actConfirm={()=>this.apiGetName()}
                />
                <ModalConfirmTrade
                    actConfirm={()=>this.apiSendExchange("other")}/>
            </F11Layout>
        )
    }
}

const mapStateToProps = (state) => ({ ...state.Main });
const mapDispatchToProps = { setValues };
export default connect( mapStateToProps, mapDispatchToProps)(Exchange);


const ExchangeStyle = styled.div`
    position: relative;
    display: block;
    width: 1105px;
    height: 700px;
    background: no-repeat top center url(${bg});

    .exchange {
        &__set {
            position: absolute;
            top: 50%;
            left: 50%;
            display: block
            // width: 200px;
            // height: 300px;
            transform: translate3d(-50%,0,0);
            text-align: center;
            color: #FFFFFF;
            font-size: 20px;
            line-height: 1.5em;
        }
        &__image {
            position: relative;
            display: block;
        }
        &__name {
            margin-top: 20px;
        }
        &__count { }
        &__btn {
            position: absolute;
            &--send {
                bottom: 3%;
                left: 50%;
                display: block;
                width: 300px;
                height: 96px;
                transform: translate3d(-50%,0,0);
                background: no-repeat bottom center url(${btn_send});
                &:hover {
                    background-position: top center;
                }
                &.disable {
                    pointer-events: none;
                    filter: grayscale(1);
                }
            }
            &--plus {
                top: 65%;
                left: 65%;
                display: block;
                width: 39px;
                height: 39px;
                background: no-repeat center url(${icon_plus});
                filter: grayscale(0.4);
                &:hover {
                    filter: grayscale(0);
                }
            }

        }
    }

`;
