import FTS_014 from './../static/images/items/FTS_014.png';
import FTS_011 from './../static/images/items/FTS_011.png';
import FTS_012 from './../static/images/items/FTS_012.png';
import FTS_013 from './../static/images/items/FTS_013.png';
import FTS_010 from './../static/images/items/FTS_010.png';
import FTS_008 from './../static/images/items/FTS_008.png';
import FTS_009 from './../static/images/items/FTS_009.png';
import FTS_007 from './../static/images/items/FTS_007.png';
import FTS_006 from './../static/images/items/FTS_006.png';
import FTS_005 from './../static/images/items/FTS_005.png';
import FTS_004 from './../static/images/items/FTS_004.png';
import FTS_003 from './../static/images/items/FTS_003.png';
import FTS_002 from './../static/images/items/FTS_002.png';
import FTS_001 from './../static/images/items/FTS_001.png';

export const imgList = {
    // gacha_14 :FTS_014,
    // gacha_11 :FTS_011,
    // gacha_12 :FTS_012,
    // gacha_13 :FTS_013,
    // gacha_10 :FTS_010,
    // gacha_9 :FTS_009,
    // gacha_8 :FTS_008,
    // gacha_7 :FTS_007,
    // gacha_6 :FTS_006,
    // gacha_5 :FTS_005,
    // gacha_4 :FTS_004,
    // gacha_3 :FTS_003,
    // gacha_2 :FTS_002,
    // gacha_1 :FTS_001,
    //
    gacha_11 :FTS_001,
    gacha_12 :FTS_012,
    gacha_13 :FTS_013,

    gacha_10 :FTS_002,
    gacha_9 :FTS_003,
    gacha_8 :FTS_004,
    gacha_7 :FTS_005,
    gacha_6 :FTS_006,
    gacha_5 :FTS_007,
    gacha_4 :FTS_008,
    gacha_3 :FTS_009,
    gacha_2 :FTS_010,
    gacha_1 :FTS_011,
}
