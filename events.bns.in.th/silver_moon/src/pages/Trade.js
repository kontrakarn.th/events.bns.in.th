import React                    from 'react';
import Style                    from 'styled-components';
import F11Layout                from './../features/F11Layout/';
import Menu                     from './../features/menu';
import ScrollArea               from 'react-scrollbar';
import { connect }              from 'react-redux';
import { setValues }            from './../store/redux';
import { Imglist }              from './../constants/Import_Images';
import { Frame }                from './../common/frame';
import { CustomBtn, MiniBtn }   from './../common/buttons';
import {Item}                   from './../common/item';
import { Link, Redirect }       from 'react-router-dom';
import ModalConfirm             from './../features/modals/ModalConfirm';
import {apiPost}                  from './../constants/Api';

export class Trade extends React.Component {
    apiTrade(){
        const item_data = []
        if(!this.props.materials){
            this.props.setValues({
                modal_open:"message",
                modal_message: "เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง",
            });
        }else{
            this.props.setValues({ modal_open: "loading" });
            var key_itr=0
            var total_mat=0
            for(var i=0;i<this.props.materials.length;i++){
                for(var k=0;k<this.props.materials[i].length;k++){
                    var mat_data={item_id: this.props.materials[i][k].id, number:this.state.list_counts[key_itr] }
                    item_data.push(mat_data)
                    key_itr=key_itr+1
                    total_mat=total_mat+this.state.list_counts[key_itr]
                }

            }

            if(total_mat<1){
                this.props.setValues({
                    modal_open:"message",
                    modal_message: "จำนวนชิ้นส่วนไม่ถูกต้อง",
                });
            }else{
                let dataSend = {type:'exchange_potion',item_data:item_data};
                let successCallback = (data) => {

                        const list_item = []
                        if(data.content && data.content.materials){
                            for(var i=0;i<data.content.materials.length;i++){
                                for(var k=0;k<data.content.materials[i].length;k++){
                                    var mat_data={id: data.content.materials[i][k].id, name:data.content.materials[i][k].icon, count:data.content.materials[i][k].quantity }
                                    list_item.push(mat_data)
                                }

                            }
                        }
                        this.props.setValues({
                            ...data.content,
                            list_item:list_item,
                            modal_open: "message",
                            modal_message: "แลก ปีกราชาแทชอน สำเร็จ",
                        })
                        this.setState({
                            select_icon:"",
                            sum_count: 0,
                            list_counts: [
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                            ]
                        });
                };
                const failCallback = (data) => {
                    if (data.type == 'no_permission') {
                        this.props.setValues({
                            permission: false,
                            modal_open: "",
                        });
                    }else {
                        this.props.setValues({
                            modal_open:"message",
                            modal_message: data.message,
                        });
                    }
                };
                apiPost("REACT_APP_API_POST_REDEEM_POTION", this.props.jwtToken, dataSend, successCallback, failCallback);
            }

        }

    }

    apiGetMaterial(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'material'};
        let successCallback = (data) => {
                const list_item = []
                if(data.content && data.content.materials){
                    for(var i=0;i<data.content.materials.length;i++){
                        for(var k=0;k<data.content.materials[i].length;k++){
                            var mat_data={id: data.content.materials[i][k].id, name:data.content.materials[i][k].icon, count:data.content.materials[i][k].quantity }
                            list_item.push(mat_data)
                        }

                    }
                }

                this.props.setValues({
                    ...data.content,
                    list_item:list_item,
                    modal_open: "",
                });
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_MATERIAL", this.props.jwtToken, dataSend, successCallback, failCallback);
    }


    //==========================================================================
    actOpenModalConfirm(){
        this.props.setValues({
            modal_open: "confirm",
            modal_message: "ต้องแลกปีกราชาแทชอน ?",
            modal_trade: 0,
            modal_item:-1,
        })
    }
    actConfirm(){
        this.apiTrade();
    }
    actIncrease(n){
        let ary = [...this.state.list_counts];
        let sum = this.state.sum_count;
        ary[n]++;
        sum++;
        this.setState({
            list_counts:ary,
            sum_count:sum
        });
    }
    actDecrease(n){
        let ary = [...this.state.list_counts];
        let sum = this.state.sum_count;
        ary[n]--;
        sum--;
        this.setState({
            list_counts:ary,
            sum_count:sum
        });
    }
    //==========================================================================
    constructor(props){
        super(props);
        this.state = {
            select_icon: "",
            select_name: "",
            sum_count: 0,
            list_counts: [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
            ]
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiGetMaterial();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiGetMaterial();
        }
    }

    render() {
        let { list_counts, sum_count } = this.state;
        let { list_item } = this.props;
        let tradeLock = (sum_count === 1)? "": " lock";
        return (
            <F11Layout showUserName={true} showCharacterName={false}>
                <Menu page='trade'/>
                <TradeContent>
                    <Frame title="แลกปีกราชาแทชอน">
                        <div className="trade">
                            <div className="trade__section">
                                <div className="trade__title">1. เลือกไอเทมที่ต้องการแลก</div>
                                <div className="trade__comment">(เลือกได้ครั้งละ 1 ชิ้น)</div>
                                <div className="trade__item"><Item name={"S_Moon_27"} count={""}/>
                                </div>
                            </div>
                            <div className="trade__section">
                                <div className="trade__title">2. เลือกชิ้นส่วนที่ต้องการแลก</div>
                                <div className="trade__comment">(จำนวนชิ้นส่วนที่ใช้แลก 1 ชิ้น)</div>
                                <ul className="trade__list">
                                    <ScrollArea
                                        speed={0.8}
                                        className="area"
                                        contentClassName=""
                                        horizontal={false}
                                        style={{ width: '100%', height:280, opacity:1}}
                                        verticalScrollbarStyle={{backgroundColor:"#000", opacity:1}}
                                    >
                                        {list_item.map((item,index)=>{
                                            let lockDown = list_counts[index] > 0 ? false: true;
                                            let lockUp = (sum_count < 1 &&  list_counts[index] < item.count)? false: true;
                                            return (
                                                <li className="trade__slotr" key={"sub_item_"+index}>
                                                    <div className="trade__imgr">
                                                        <Item name={item.name} count={item.count}/>
                                                    </div>
                                                    <MiniBtn type={"line"} lock={lockDown} onClick={()=>this.actDecrease(index)}/>
                                                    <MiniBtn type={"plus"} lock={lockUp} onClick={()=>this.actIncrease(index)}/>
                                                    <div className="trade__inputr">
                                                        {list_counts[index]}
                                                    </div>
                                                </li>
                                            )
                                        })}
                                    </ScrollArea>
                                </ul>
                            </div>
                        </div>
                        <div className="btngroup">
                            <a
                                className={"btngroup__btn"+tradeLock}
                                onClick={()=>this.actOpenModalConfirm()}
                            >
                                <CustomBtn>ผสมชิ้นส่วน</CustomBtn>
                            </a>
                            <Link className="btngroup__btn" to={process.env.REACT_APP_EVENT_PATH + '/buy-soul'}>
                                <CustomBtn>ซื้อโซลเพิ่ม</CustomBtn>
                            </Link>
                        </div>
                    </Frame>
                </TradeContent>
                <ModalConfirm
                    actConfirm={()=>this.actConfirm()}
                />
            </F11Layout>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Trade);

const TradeContent = Style.div`
    background: top center no-repeat url(${Imglist['other_bg']}) #11122d;
    width: 1105px;
    padding: 170px 0 26px;
    text-align: center;

    .trade {
        position: relative;
        display: block;
        color: #FFFFFF;
        &__section {
            position: relative;
            display: inline-block;
            box-sizing: border-box;
            vertical-align: top;
            width: 50%;
            min-height: 350px;
            &:first-child  {
                border-right: 1px solid #FFFFFF;
            }
        }
        &__title {
            text-align: center;
            font-size: 1.5em;
            inline-height: 1em;
        }
        &__comment {}
        &__item {
            display: block;
            width: 350px;
            margin: 50px auto 0px;
            &>* {
                margin: 0px auto;
            }
        }
        &__slotl {
            display: inline-block;
            margin: 15px;
        }
        &__slotr {
            display: inline-block;
            margin: 15px;
            &>* {
                display: inline-block;
                vertical-align: middle;
                margin: 2px;
            }
        }
        &__imgr {
            margin-right: 20px;
        }
        &__inputr {
            display: inline-block;
            width: 50px;
            text-align: center;
            margin-left: 35px;
            background-color: #000000;
            color: #FFFFFF;
            font-size: 1.2em;
            border: 1px solid;
        }
    }
    .btngroup {
        position: absolute;
        bottom: 12px;
        left: 0px;
        display: block;
        width: 100%;
        &__btn {
            display: inline-block
            margin: 0px 10px;
            &.lock {
                pointer-events: none;
                filter: grayscale(1);
            }
        }
    }
`;
