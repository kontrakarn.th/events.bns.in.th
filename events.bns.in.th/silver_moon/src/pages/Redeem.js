import React                    from 'react';
import Style                    from 'styled-components';
import F11Layout                from './../features/F11Layout/';
import Menu                     from './../features/menu';
import ScrollArea               from 'react-scrollbar';
import { connect }              from 'react-redux';
import { setValues }            from './../store/redux';
import { Imglist }              from './../constants/Import_Images';
import { Frame }                from './../common/frame';
import { CustomBtn, MiniBtn }   from './../common/buttons';
import {Item}                   from './../common/item';
import { Link, Redirect }       from 'react-router-dom';
import ModalConfirm             from './../features/modals/ModalConfirm';
import {apiPost}                  from './../constants/Api';

class Redeem extends React.Component {
    apiRedeem() {
        if(this.state.package_id<1 || this.state.package_id>8){
            this.props.setValues({
                modal_open:"message",
                modal_message: "ไอเท็มที่ต้องการแลกไม่ถูกต้อง",
            });
        }else{
            const item_data = []
            if(!this.props.materials){
                this.props.setValues({
                    modal_open:"message",
                    modal_message: "เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง",
                });
            }else{
                this.props.setValues({ modal_open: "loading" });
                var key_itr=0
                var total_mat=0
                for(var i=0;i<this.props.materials.length;i++){
                    for(var k=0;k<this.props.materials[i].length;k++){
                        var mat_data={item_id: this.props.materials[i][k].id, number:this.state.list_counts[key_itr] }
                        item_data.push(mat_data)
                        key_itr=key_itr+1
                        total_mat=total_mat+this.state.list_counts[key_itr]
                    }

                }

                if(total_mat<5){
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: "จำนวนชิ้นส่วนไม่ถูกต้อง",
                    });
                }else{
                    let dataSend = {type:'redeem',package_id:this.state.package_id,item_data:item_data};
                    let successCallback = (data) => {

                            const list_item = []
                            if(data.content && data.content.materials){
                                for(var i=0;i<data.content.materials.length;i++){
                                    for(var k=0;k<data.content.materials[i].length;k++){
                                        var mat_data={id: data.content.materials[i][k].id, name:data.content.materials[i][k].icon, count:data.content.materials[i][k].quantity }
                                        list_item.push(mat_data)
                                    }

                                }
                            }
                            this.props.setValues({
                                ...data.content,
                                list_item:list_item,
                                modal_open: "message",
                                modal_message: "แลกชิ้นส่วน "+this.state.select_name+" สำเร็จ",
                            })
                            this.setState({
                                package_id:"",
                                select_icon:"",
                                sum_count: 0,
                                list_counts: [
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                ]
                            });
                    };
                    const failCallback = (data) => {
                        if (data.type == 'no_permission') {
                            this.props.setValues({
                                permission: false,
                                modal_open: "",
                            });
                        }else {
                            this.props.setValues({
                                modal_open:"message",
                                modal_message: data.message,
                            });
                        }
                    };
                    apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
                }

            }
        }
    }

    apiGetMaterial(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'material'};
        let successCallback = (data) => {
                const list_item = []
                if(data.content && data.content.materials){
                    for(var i=0;i<data.content.materials.length;i++){
                        for(var k=0;k<data.content.materials[i].length;k++){
                            var mat_data={id: data.content.materials[i][k].id, name:data.content.materials[i][k].icon, count:data.content.materials[i][k].quantity }
                            list_item.push(mat_data)
                        }

                    }
                }

                this.props.setValues({
                    ...data.content,
                    list_item:list_item,
                    modal_open: "",
                });
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_MATERIAL", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    //==========================================================================
    actOpenModalConfirm(){
        this.props.setValues({
            modal_open: "confirm",
            modal_message: "ต้องการแลก "+this.state.select_name+" ใช่หรือไม่?",
            modal_redeem: this.state.package_id,
            modal_item: this.state.package_id,
        })
    }
    actConfirm(){
        this.apiRedeem();
    }
    actIncrease(n){
        let ary = [...this.state.list_counts];
        let sum = this.state.sum_count;
        ary[n]++;
        sum++;
        this.setState({
            list_counts:ary,
            sum_count:sum
        });
    }
    actDecrease(n){
        let ary = [...this.state.list_counts];
        let sum = this.state.sum_count;
        ary[n]--;
        sum--;
        this.setState({
            list_counts:ary,
            sum_count:sum
        });
    }
    //==========================================================================
    constructor(props){
        super(props);
        this.state = {
            package_id: "",
            select_icon: "",
            select_name: "",

            sum_count: 0,
            list_counts: [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
            ]
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiGetMaterial();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiGetMaterial();
        }
    }


    render() {
        let { package_id, list_counts, sum_count } = this.state;
        let { list_item } = this.props;
        let redeemLock = (package_id != "" && sum_count === 5)? "": " lock";
        return (
            <F11Layout showUserName={true} showCharacterName={false}>
                <Menu page='redeem'/>
                <RedeemContent>
                    <Frame title="แลกชิ้นส่วน">
                        <div className="redeem">
                            <div className="redeem__section">
                                <div className="redeem__title">1. เลือกไอเทมที่ต้องการแลก</div>
                                <div className="redeem__comment">(เลือกได้ครั้งละ 1 ชิ้น)</div>
                                <ul className="redeem__list">
                                    {this.props.list_redeem.map((item,index)=>{
                                        let lock = (package_id !== "" && package_id !== item.package_id )? " lock": "";
                                        return (
                                            <li
                                                className={"redeem__slotl"+lock}
                                                key={"redeem_slot_"+index}
                                                onClick={()=>this.setState({package_id:item.package_id,select_icon:item.icon,select_name:item.name})}
                                            >
                                                <Item name={item.icon} count={""}/>
                                            </li>
                                        )
                                    })}
                                </ul>
                            </div>
                            <div className="redeem__section">
                                <div className="redeem__title">2. เลือกชิ้นส่วนที่ต้องการแลก</div>
                                <div className="redeem__comment">(จำนวนชิ้นส่วนที่ใช้แลก 5 ชิ้น)</div>
                                <ul className="redeem__list">
                                    <ScrollArea
                                        speed={0.8}
                                        className="area"
                                        contentClassName=""
                                        horizontal={false}
                                        style={{ width: '100%', height:280, opacity:1}}
                                        verticalScrollbarStyle={{backgroundColor:"#000", opacity:1}}
                                    >
                                        {list_item.map((item,index)=>{
                                            let lockDown = list_counts[index] > 0 ? false: true;
                                            let lockUp = (sum_count < 5 &&  list_counts[index] < item.count)? false: true;
                                            return (
                                                <li className="redeem__slotr" key={"redeem_item_"+index}>
                                                    <div className="redeem__imgr"><Item name={item.name} count={item.count}/></div>
                                                    <MiniBtn type={"line"} lock={lockDown} onClick={()=>this.actDecrease(index)}/>
                                                    <MiniBtn type={"plus"} lock={lockUp} onClick={()=>this.actIncrease(index)}/>
                                                    <div className="redeem__inputr">
                                                        {list_counts[index]}
                                                    </div>
                                                </li>
                                            )
                                        })}
                                    </ScrollArea>
                                </ul>
                            </div>
                        </div>
                        <div className="btngroup">
                            <a className={"btngroup__btn"+redeemLock} onClick={()=>this.actOpenModalConfirm()}>
                                <CustomBtn>ผสมชิ้นส่วน</CustomBtn>
                            </a>
                            <Link className="btngroup__btn" to={process.env.REACT_APP_EVENT_PATH + '/buy-soul'}>
                                <CustomBtn>ซื้อโซลเพิ่ม</CustomBtn>
                            </Link>
                        </div>
                    </Frame>
                </RedeemContent>
                <ModalConfirm
                    actConfirm={()=>this.actConfirm()}
                />
            </F11Layout>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Redeem);

const RedeemContent = Style.div`
    background: top center no-repeat url(${Imglist['other_bg']}) #11122d;
    width: 1105px;
    padding: 170px 0 26px;
    text-align: center;

    .redeem {
        position: relative;
        display: block;
        color: #FFFFFF;
        &__section {
            position: relative;
            display: inline-block;
            box-sizing: border-box;
            vertical-align: top;
            width: 50%;
            min-height: 350px;
            &:first-child  {
                border-right: 1px solid #FFFFFF;
            }
        }
        &__title {
            text-align: center;
            font-size: 1.5em;
            inline-height: 1em;
        }
        &__comment {

        }
        &__list {
            display: block;
            width: 400px;
            margin: 10px auto 0px;
        }
        &__slotl {
            display: inline-block;
            margin: 5px;
            &.lock {
                filter: grayscale(1);
            }
        }
        &__slotr {
            display: inline-block;
            margin: 15px;
            &>* {
                display: inline-block;
                vertical-align: middle;
                margin: 2px;
            }
        }
        &__imgr {
            margin-right: 20px;
        }
        &__inputr {
            display: inline-block;
            width: 50px;
            text-align: center;
            margin-left: 35px;
            background-color: #000000;
            color: #FFFFFF;
            font-size: 1.2em;
            border: 1px solid;
        }
    }
    .btngroup {
        position: absolute;
        bottom: 12px;
        left: 0px;
        display: block;
        width: 100%;
        &__btn {
            display: inline-block
            margin: 0px 10px;
            &.lock {
                pointer-events: none;
                filter: grayscale(1);
            }
        }
    }
`;
