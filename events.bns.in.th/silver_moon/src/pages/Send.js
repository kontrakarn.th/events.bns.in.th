import React                from 'react';
import Style                from 'styled-components';
import F11Layout            from './../features/F11Layout/';
import Menu                 from './../features/menu';
import { connect }          from 'react-redux';
import { setValues }        from './../store/redux';
import {Imglist}            from './../constants/Import_Images';
import {Frame}              from './../common/frame';
import {CustomBtn}          from './../common/buttons';
import {Item}               from './../common/item';
import { Link, Redirect }   from 'react-router-dom';
import ModalConfirmReceive  from './../features/modals/ModalConfirmReceive';
import ModalConfirmSend     from './../features/modals/ModalConfirmSend';
import ModalConfirmUid      from './../features/modals/ModalConfirmUid';
import {apiPost}              from './../constants/Api';

export class Send extends React.Component {
    apiGetMyBag(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'my_bag'};
        let successCallback = (data) => {
                this.props.setValues({
                    ...data.content,
                    modal_open: "",
                });
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_BAG", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiReceive(){
        if(this.state.select_id<1 || this.state.select_id>11){
            this.props.setValues({
                modal_open:"message",
                modal_message: "โปรดเลือกของที่ต้องการรับ",
            });
        }else{
            this.props.setValues({ modal_open: "loading" });
            let dataSend = {type:'send_item',package_id:this.state.select_id};
            let successCallback = (data) => {
                    this.props.setValues({
                        ...data.content,
                        modal_open: "message",
                        modal_message: data.message,
                    })

                    this.setState({
                        select_id: -1
                    });

            };
            const failCallback = (data) => {
                if (data.type == 'no_permission') {
                    this.props.setValues({
                        permission: false,
                        modal_open: "",
                    });
                }else {
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: data.message,
                    });
                }
            };
            apiPost("REACT_APP_API_POST_SEND_ITEM", this.props.jwtToken, dataSend, successCallback, failCallback);
        }

    }

    apiGetName(){
        var send_id=document.getElementById('f_uid').value
        if(!send_id || send_id==""){
            this.props.setValues({
                modal_open:"message",
                modal_message: "โปรดกรอก UID ที่ต้องการส่ง",
            });
        }else{
            this.props.setValues({ modal_open: "loading" });
            let dataSend = {type:'check_uid',uid:send_id};
            let successCallback = (data) => {
                    this.props.setValues({
                        ...data.content,
                        modal_open: "send",
                        modal_message: "ต้องการส่ง <br/>"+this.props.my_bag[this.state.select_id-1].title+" <br/>ให้ "+data.content.target_username+" ใช่หรือไม่?",
                    })
            };
            const failCallback = (data) => {
                if (data.type == 'no_permission') {
                    this.props.setValues({
                        permission: false,
                        modal_open: "",
                    });
                }else {
                    this.props.setValues({
                        modal_open:"message",
                        modal_message: data.message,
                    });
                }
            };
            apiPost("REACT_APP_API_POST_CHECK_UID", this.props.jwtToken, dataSend, successCallback, failCallback);
        }

    }

    apiSend(){
        if(this.state.select_id<1 || this.state.select_id>11){
            this.props.setValues({
                modal_open:"message",
                modal_message: "โปรดเลือกของที่ต้องการรับ",
            });
        }else{
            if(this.props.target_uid<0){
                this.props.setValues({
                    modal_open:"message",
                    modal_message: "ไม่พบ UID ที่ต้องการส่ง",
                });
            }else{
                this.props.setValues({ modal_open: "loading" });
                let dataSend = {type:'send_gift',package_id:this.state.select_id,uid:this.props.target_uid};
                let successCallback = (data) => {
                        this.props.setValues({
                            ...data.content,
                            target_uid:-1,
                            target_username:"",
                            modal_open: "message",
                            modal_message: data.message,
                        })

                        this.setState({
                            select_id: -1
                        });

                };
                const failCallback = (data) => {
                    if (data.type == 'no_permission') {
                        this.props.setValues({
                            permission: false,
                            modal_open: "",
                        });
                    }else {
                        this.props.setValues({
                            modal_open:"message",
                            modal_message: data.message,
                        });
                    }
                };
                apiPost("REACT_APP_API_POST_SEND_GIFT", this.props.jwtToken, dataSend, successCallback, failCallback);
            }

        }

    }
    //==========================================================================
    actSelectItem (id) {
        this.setState({
            select_id: id
        })
    }

    actOpenModalReceive() {
        let { select_id, list } = this.state;
        this.props.setValues({
            modal_open: "receive",
            modal_message: "ต้องการรับ<br/>"+this.props.my_bag[select_id-1].title+" ?",
            modal_item: this.props.my_bag[select_id-1],
        })
    }
    actConfirmReceive(){
        this.apiReceive();
    }
    actOpenModalUid() {
        this.props.setValues({
            modal_open: "uid",
            modal_message: "",
            modal_uid: "",
        })
    }
    actConfirmUid(){
        this.apiGetName();
    }
    actConfirmSend(){
        this.apiSend();
    }
    //==========================================================================
    constructor(props){
        super(props);
        this.state = {
            select_id: -1,
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiGetMyBag();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiGetMyBag();
        }
    }

    render() {
        let { select_id, list } = this.state;
        return (
            <F11Layout showUserName={true} showCharacterName={false}>
                <Menu page='send'/>
                <OpenSoulContent>
                    <Frame title="ส่งของรางวัล">
                        <div className="list">
                         {this.props.my_bag && this.props.my_bag.map((item,index)=>{
                             let disable = item.quantity > 0 ? "": " disable";
                             let lock = (select_id !== item.id && select_id >= 0)? " lock": "";
                             return (
                                <div
                                    className={"list__slot"+lock+disable}
                                    key={"item_"+index}
                                    onClick={()=>this.actSelectItem(item.id)}
                                >
                                    <Item name={item.key} count={item.quantity}/>
                                </div>
                             )
                         })}
                        </div>
                        <div className="btngroup">
                            <a
                                className={"btngroup__btn" +(select_id !== -1? "": " lock")}
                                onClick={()=>this.actOpenModalReceive()}
                            >
                                <CustomBtn>รับของรางวัล</CustomBtn>
                            </a>
                            <Link className="btngroup__btn" to={process.env.REACT_APP_EVENT_PATH + '/buy-soul'}>
                                <CustomBtn>ซื้อโซลเพิ่ม</CustomBtn>
                            </Link>
                            <a
                                className={"btngroup__btn"+(select_id !== -1? "": " lock")}
                                onClick={()=>this.actOpenModalUid()}
                            >
                                <CustomBtn>ส่งให้เพื่อน</CustomBtn>
                            </a>
                        </div>
                        <img className="char" src={Imglist['send_char']} />
                        <img className="char2" src={Imglist['send_char2']} />
                    </Frame>
                </OpenSoulContent>
                <ModalConfirmReceive
                    actConfirm={()=>this.actConfirmReceive()}
                />
                <ModalConfirmUid
                    actConfirm={()=>this.actConfirmUid()}
                />
                <ModalConfirmSend
                    actConfirm={()=>this.actConfirmSend()}
                />
            </F11Layout>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Send);

const OpenSoulContent = Style.div`
    background: top center no-repeat url(${Imglist['other_bg']}) #11122d;
    width: 1105px;
    padding: 170px 0 26px;
    text-align: center;
    .char {
        position: absolute;
        top: 31%;
        left: -12%;
    }
    .char2 {
        position: absolute;
        top: 23%;
        right: -10%;
    }
    .list {
        margin: 0 auto;
        display: block;
        width: 60%;
        height: 100%;
        &__slot {
            display: inline-block;
            margin: 15px;

            &.lock {
                filter: grayscale(1);
            }
            &.disable {
                pointer-events: none;
                filter: grayscale(1);
                opacity: 0.6;
            }
        }
    }
    .btngroup {
        position: absolute;
        bottom: 12px;
        left: 0px;
        display: block;
        width: 100%;
        &__btn {
            display: inline-block
            margin: 0px 10px;
            &.lock {
                pointer-events: none;
                filter: grayscale(1);
            }
        }
    }
`;
