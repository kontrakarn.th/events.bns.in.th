import React from 'react';

import F11Layout from './../features/F11Layout/';
import CostumePage from './../features/costume';

export class Costume extends React.Component {
    render() {
        return (
            <F11Layout
                showUserName={true}
                showCharacterName={false}
            >
                <CostumePage />
            </F11Layout>
        )
    }
}
