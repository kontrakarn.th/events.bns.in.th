import React from 'react';
import {connect} from 'react-redux';
import styled  from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../redux';

import modal_bg from './images/modal_bg.png';
import title_text from './images/title_text.png';
import confirm_btn from './images/confirm_btn.png';
import cancel_btn from './images/cancel_btn.png';

const CPN = props => {
    let {modal_message} = props;
    return (
        <ModalCore
            modalName="receive"
            actClickOutside={()=>props.setValues({modal_open:""})}
        >
            <ModalMessageContent>
                <img className="titletext" src={title_text} alt=""/>
                <div className="contenttext" dangerouslySetInnerHTML={{__html: modal_message}} />
            </ModalMessageContent>
            <ModalBottom>
                    <Btns src={confirm_btn} onClick={()=>{if(props.actConfirm) props.actConfirm()}}/>
                    <Btns src={cancel_btn} onClick={()=>props.setValues({modal_open:''})}/>
            </ModalBottom>
        </ModalCore>
    )
}

const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(CPN);
//className={"modal__incontent modal__incontent--loading"}
const ModalMessageContent = styled.div`
    position: relative;
    display: block;
    width: 410px;
    height: 314px;
    text-align: center;
    color: #FFFFFF;
    background: no-repeat center url(${modal_bg});
    box-sizing: border-box;
    padding: 10% 5% 15%;
    h2 {
        top: 20px;
        font-size: 40px;
        color: #ffffff;
    }
    h3 {
        font-size: 30px;
        line-height: 1.5em;
        color: #563e26;
    }
    .contenttext{
        color: #FFFFFF;
        font-family: 'Kanit-Medium';
        font-size: 24px;
        line-height: 1.5em;
        word-break: break-word;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .titletext{
        position: absolute;
        display: block;
        top: 3%;
        left: 50%;
        transform: translate3d(-50%,0px,0px);
    }
`;
const ModalBottom = styled.div`
    display: block;
    position: absolute;
    bottom: 0px;
    z-index: 50;
`;

const Btns = styled.img`
    display: inline-block;
    cursor: pointer;
    margin: 0 15px;
`
