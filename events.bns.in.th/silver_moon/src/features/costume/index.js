import React from 'react';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalShowItem from '../../features/modals/ModalShowItem';
import {Imglist} from './../../constants/Import_Images';
import Menu from '../../features/menu';

class Costume extends React.Component {


    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        return (
            <PageWrapper>
                <Menu page='costume'/>
                <EnterBtn/>
                <Board src={Imglist['preview_frame']} alt=''/>
                <FooterCharacter src={Imglist['footer_char']} alt=''/>
                <ModalLoading />
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Costume);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_costume']}) #11122d;
    width: 1105px;
    padding-top: 630px;
    text-align: center;
`

const EnterBtn = styled.div`
    background: top center no-repeat url(${Imglist['btn_enter']});
    width: 305px;
    height: 53px;
    cursor: pointer;
    position: absolute;
    top: 490px;
    left: 400px;
    &:hover{
        background-position: bottom center;
    }
`
const Board = styled.img`
    display: block;
    margin: 0 auto;
    padding: 140px 0 60px;
`
const FooterCharacter = styled.img`
    position: absolute;
    bottom: 0;
    left: 0;
`
