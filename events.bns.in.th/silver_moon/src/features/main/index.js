import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import styled from 'styled-components';

import { setValues, onAccountLogout } from './../../store/redux';

import ModalLoading from './../../features/modals/ModalLoading';
import ModalMessage from './../../features/modals/ModalMessage';
import ModalConfirm from './../../features/modals/ModalConfirm';
import ModalShowItem from '../../features/modals/ModalShowItem';
import {Imglist} from './../../constants/Import_Images';
import Menu from '../../features/menu';

class Main extends React.Component {
    actConfirm(){
        this.apiRedeem(
            this.props.modal_confirmitem && this.props.modal_confirmitem !== ''
            && this.props.modal_confirmitem
        );
    }

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiCheckin(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'event_info'};
        let successCallback = (data) => {
                this.props.setValues({
                    ...data.content,
                    modal_open: "",
                });
        };
        const failCallback = (data) => {
            if (data.type === 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    apiRedeem(type_id){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type_id};
        let successCallback = (data) => {
            if (data.status) {
                this.props.setValues({
                    ...data.content.response,
                    modal_open:"message",
                    modal_message: "ส่งของรางวัลสำเร็จ<br/>กรุณาตรวจสอบจดหมายภายในเกม"
                });
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_REDEEM", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    constructor(props) {
        super(props)

        this.state = {
            eventInfo: {},
            // modalSelectCharacter:false,
            modalConfirmToRedeem:false,
            // modalSelectWeapon: false,
            redeem:false,
            permission: false,
            modalLoading: false,
            modalShowMessage: false,
            modalMessage: '',
            showBoard: false
        }
    }

    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiCheckin();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiCheckin();
        }
    }

    render() {
        return (
            <PageWrapper>
                <Menu page='main'/>
                <Link to={process.env.REACT_APP_EVENT_PATH + '/buy-soul'}><EnterBtn/></Link>
                <ConditionBoard src={Imglist['condition_frame']} alt=''/>
                <FooterCharacter src={Imglist['footer_char']} alt=''/>
                <ModalLoading />
                <ModalMessage />
                <ModalConfirm actConfirm={this.actConfirm.bind(this)}/>
                <ModalShowItem/>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { onAccountLogout, setValues };

export default connect( mapStateToProps, mapDispatchToProps )(Main);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['bg_costume']}) #11122d;
    width: 1105px;
    padding-top: 630px;
    text-align: center;

`

const EnterBtn = styled.div`
    background: top center no-repeat url(${Imglist['btn_enter']});
    width: 305px;
    height: 53px;
    cursor: pointer;
    position: absolute;
    top: 490px;
    left: 400px;
    &:hover{
        background-position: bottom center;
    }
`
const ConditionBoard = styled.img`
    display: block;
    margin: 0 auto;
    padding: 140px 0 60px;
`
const FooterCharacter = styled.img`
    position: absolute;
    bottom: 0;
    left: 0;
`
