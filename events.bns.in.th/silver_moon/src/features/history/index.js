import React from 'react';
import {connect} from 'react-redux';
import { apiPost } from './../../middlewares/Api';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { setValues } from './../../store/redux';

import {Imglist} from './../../constants/Import_Images';
import Menu from '../../features/menu';
import ScrollArea from 'react-scrollbar';

class History extends React.Component {

    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    apiGetHistory(){
        this.props.setValues({ modal_open: "loading" });
        let dataSend = {type:'item_history'};
        let successCallback = (data) => {
            this.props.setValues({
                ...data.content,
                modal_open: ''
            });
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.props.setValues({
                    permission: false,
                    modal_open: "",
                });
            }else {
                this.props.setValues({
                    modal_open:"message",
                    modal_message: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_ITEM_HISTORY", this.props.jwtToken, dataSend, successCallback, failCallback);
    }
    // ========= ========= ========= ========= ========= ========= ========= ========= ========= =========
    componentWillMount() {
        if(this.props.jwtToken !== ""){
            this.apiGetHistory();
        }

    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.jwtToken !== prevProps.jwtToken && this.props.jwtToken !== ""){
            this.apiGetHistory();
        }
    }
    render() {
        return (
            <PageWrapper>
                <Menu page='history'/>
                <ContentFrame>
                        <TitleBox>ประวัติการแลก</TitleBox>
                        <ScrollArea
                            speed={0.8}
                            className="area"
                            contentClassName=""
                            horizontal={false}
                            style={{ width: '100%', height: '100%', opacity: 1 }}
                            verticalScrollbarStyle={{ backgroundColor: "#000000", opacity: 1 }}
                        >
                            <ul className="list">
                                {
                                    this.props.items_history && this.props.items_history.length > 0 &&
                                    this.props.items_history.map((item,index) => {
                                        return(
                                                <li key={`history_${index+1}`}>
                                                {
                                                    item.send_to_uid!=""
                                                    ?
                                                        <div>ส่ง {item.item_title} ให้กับ UID: {item.send_to_uid}</div>
                                                    :
                                                        item.status=='used'
                                                        ?
                                                            (item.send_type=="owner" && item.type=='rare_item')
                                                            ?
                                                                <div>ส่ง {item.item_title} ให้ตัวเอง</div>
                                                            :
                                                                <div>ได้รับ {item.item_title}</div>
                                                        :
                                                            <div>จัดเก็บ {item.item_title} ในเว็ป</div>

                                                }
                                                    <div>{item.created_at}</div>
                                                </li>
                                        )
                                    })
                                }
                            </ul>
                        </ScrollArea>
                        <Link to="/silver_moon/buy-soul"><CustomBtn>ซื้อโซลเพิ่ม</CustomBtn></Link>
                </ContentFrame>
            </PageWrapper>
        )
    }
}
const mapStateToProps = state => ({...state.Main});
const mapDispatchToProps = { setValues };

export default connect( mapStateToProps, mapDispatchToProps )(History);

const PageWrapper = styled.div`
    background: top center no-repeat url(${Imglist['other_bg']}) #11122d;
    width: 1105px;
    padding: 170px 0 26px;
    text-align: center;
`

const ContentFrame = styled.div`
    background: top center no-repeat url(${Imglist['frame']});
    width: 931px;
    height: 503px;
    display: block;
    margin: 0 auto;
    position: relative;
    box-sizing: border-box;
    padding: 7% 10%;
    .list{
        margin: 0;
        padding: 0;
        list-style-type: none;
        height: 100%;
        overflow: auto;
        >li{
            color: #ffffff;
            display: flex;
            justify-content: space-between;
            align-items: center;
            white-space: nowrap;
            >div:first-child{
                width: 60%;
                text-align: left;
            }
            >div:nth-child(2){
                width: 30%;
                text-align: center;
            }
            /* >div:last-child{
                width: 15%;
                text-align: center;
            } */
        }
    }
`
const TitleBox = styled.div`
    background: top center no-repeat url(${Imglist['title_frame']});
    width: 313px;
    height: 61px;
    font-size: 2em;
    line-height: 1.8em;
    color: #ffffff;
    text-align: center;
    font-weight: 600;
    margin: 0 auto;
    position: absolute;
    top: 0;
    left: 50%;
    transform: translate3d(-50%,-30%,0);
`
const CustomBtn = styled.div`
    width: 225px;
    height: 51px;
    background: top center no-repeat url(${Imglist['btn']});
    color: #ffffff;
    cursor: pointer;
    font-size: 1.5em;
    line-height: 2em;
    font-weight: 600;
    margin: 17px auto 0;
    &:hover{
        background-position: bottom center;
    }
`
