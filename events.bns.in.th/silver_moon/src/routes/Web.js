import React, {Component} from 'react';
import {Route} from 'react-router-dom';
// import OauthMiddleware from './../middlewares/OauthMiddleware';
import OauthMiddleware from './../features/oauthLogin';
import Modals from './../features/Modals';

import { Home } 	from './../pages/Home';
import { Costume } 	from './../pages/Costume';
import { History } 	from './../pages/History';
import BuySoul	 	from './../pages/BuySoul';
import OpenSoul		from './../pages/OpenSoul';
import Send 		from './../pages/Send';
import Combine 		from './../pages/Combine';
import Redeem 		from './../pages/Redeem';
import Trade 		from './../pages/Trade';

export class Web extends Component {
	render() {
		return (
			<>
				<Route path={process.env.REACT_APP_EVENT_PATH} component={OauthMiddleware} />
				<Route path={process.env.REACT_APP_EVENT_PATH} component={Modals} />

				<Route path={process.env.REACT_APP_EVENT_PATH} exact component={Home} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/costume'} exact component={Costume} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/history'} exact component={History} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/buy-soul'} exact component={BuySoul} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/open-soul'} exact component={OpenSoul} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/send'} exact component={Send} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/combine'} exact component={Combine} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/redeem'} exact component={Redeem} />
				<Route path={process.env.REACT_APP_EVENT_PATH + '/trade'} exact component={Trade} />
			</>
		);
	}
}
