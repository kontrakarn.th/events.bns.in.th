import btn_enter from '../static/images/btn_enter.png';
import footer_char from '../static/images/footer_char.png';
import condition_frame from '../static/images/condition_frame.png';
import other_bg from '../static/images/other_bg.jpg';
import p1_bg from '../static/images/p1_bg.jpg';
import preview_frame from '../static/images/preview_frame.png';

import bg_costume from '../static/images/bg_costume.jpg';
import icon_menu from '../static/images/icon_menu.png';
import menu_close from '../static/images/menu_close.png';
import menu_bg from '../static/images/menu_bg.jpg';
import title_frame from '../static/images/title_frame.png';
import frame from '../static/images/frame.png';
import btn from '../static/images/btn.png';

import diamond from './../static/images/diamond.png';
import buy_soul_button from './../static/images/buy_soul_button.png';
import buy_soul_border from './../static/images/buy_soul_border.png';
import buy_soul_char from '../static/images/buy_soul_char.png';

import open_soul_char from './../static/images/open_soul_char.png';
import open_soul_char2 from './../static/images/open_soul_char2.png';

import send_char from './../static/images/send_char.png';
import send_char2 from './../static/images/send_char2.png';

import combine_arrow from './../static/images/combine_arrow.png';

import redeem_plus_btn from './../static/images/redeem_plus_btn.png';
import redeem_line_btn from './../static/images/redeem_line_btn.png';

import S_Moon_01_B from './../static/images/items/S_Moon_01_B.png';
import S_Moon_01_B_get from './../static/images/items/S_Moon_01_B_get.png';
import S_Moon_01_Base from './../static/images/items/S_Moon_01_Base.png';
import S_Moon_01_G from './../static/images/items/S_Moon_01_G.png';
import S_Moon_01_G_get from './../static/images/items/S_Moon_01_G_get.png';
import S_Moon_01_R from './../static/images/items/S_Moon_01_R.png';
import S_Moon_01_R_get from './../static/images/items/S_Moon_01_R_get.png';
import S_Moon_01 from './../static/images/items/S_Moon_01.png';
import S_Moon_01_get from './../static/images/items/S_Moon_01_get.png';
import S_Moon_02_B from './../static/images/items/S_Moon_02_B.png';
import S_Moon_02_B_get from './../static/images/items/S_Moon_02_B_get.png';
import S_Moon_02_Base from './../static/images/items/S_Moon_02_Base.png';
import S_Moon_02_G from './../static/images/items/S_Moon_02_G.png';
import S_Moon_02_G_get from './../static/images/items/S_Moon_02_G_get.png';
import S_Moon_02_R from './../static/images/items/S_Moon_02_R.png';
import S_Moon_02_R_get from './../static/images/items/S_Moon_02_R_get.png';
import S_Moon_02 from './../static/images/items/S_Moon_02.png';
import S_Moon_02_get from './../static/images/items/S_Moon_02_get.png';
import S_Moon_03_B from './../static/images/items/S_Moon_03_B.png';
import S_Moon_03_B_get from './../static/images/items/S_Moon_03_B_get.png';
import S_Moon_03_Base from './../static/images/items/S_Moon_03_Base.png';
import S_Moon_03_G from './../static/images/items/S_Moon_03_G.png';
import S_Moon_03_G_get from './../static/images/items/S_Moon_03_G_get.png';
import S_Moon_03_R from './../static/images/items/S_Moon_03_R.png';
import S_Moon_03_R_get from './../static/images/items/S_Moon_03_R_get.png';
import S_Moon_03 from './../static/images/items/S_Moon_03.png';
import S_Moon_03_get from './../static/images/items/S_Moon_03_get.png';
import S_Moon_04_B from './../static/images/items/S_Moon_04_B.png';
import S_Moon_04_B_get from './../static/images/items/S_Moon_04_B_get.png';
import S_Moon_04_Base from './../static/images/items/S_Moon_04_Base.png';
import S_Moon_04_G from './../static/images/items/S_Moon_04_G.png';
import S_Moon_04_G_get from './../static/images/items/S_Moon_04_G_get.png';
import S_Moon_04_R from './../static/images/items/S_Moon_04_R.png';
import S_Moon_04_R_get from './../static/images/items/S_Moon_04_R_get.png';
import S_Moon_04 from './../static/images/items/S_Moon_04.png';
import S_Moon_04_get from './../static/images/items/S_Moon_04_get.png';
import S_Moon_05_B from './../static/images/items/S_Moon_05_B.png';
import S_Moon_05_B_get from './../static/images/items/S_Moon_05_B_get.png';
import S_Moon_05_Base from './../static/images/items/S_Moon_05_Base.png';
import S_Moon_05_G from './../static/images/items/S_Moon_05_G.png';
import S_Moon_05_G_get from './../static/images/items/S_Moon_05_G_get.png';
import S_Moon_05_R from './../static/images/items/S_Moon_05_R.png';
import S_Moon_05_R_get from './../static/images/items/S_Moon_05_R_get.png';
import S_Moon_05 from './../static/images/items/S_Moon_05.png';
import S_Moon_05_get from './../static/images/items/S_Moon_05_get.png';
import S_Moon_06_B from './../static/images/items/S_Moon_06_B.png';
import S_Moon_06_B_get from './../static/images/items/S_Moon_06_B_get.png';
import S_Moon_06_Base from './../static/images/items/S_Moon_06_Base.png';
import S_Moon_06_G from './../static/images/items/S_Moon_06_G.png';
import S_Moon_06_G_get from './../static/images/items/S_Moon_06_G_get.png';
import S_Moon_06_R from './../static/images/items/S_Moon_06_R.png';
import S_Moon_06_R_get from './../static/images/items/S_Moon_06_R_get.png';
import S_Moon_06 from './../static/images/items/S_Moon_06.png';
import S_Moon_06_get from './../static/images/items/S_Moon_06_get.png';
import S_Moon_07_B from './../static/images/items/S_Moon_07_B.png';
import S_Moon_07_B_get from './../static/images/items/S_Moon_07_B_get.png';
import S_Moon_07_Base from './../static/images/items/S_Moon_07_Base.png';
import S_Moon_07_G from './../static/images/items/S_Moon_07_G.png';
import S_Moon_07_G_get from './../static/images/items/S_Moon_07_G_get.png';
import S_Moon_07_R from './../static/images/items/S_Moon_07_R.png';
import S_Moon_07_R_get from './../static/images/items/S_Moon_07_R_get.png';
import S_Moon_07 from './../static/images/items/S_Moon_07.png';
import S_Moon_07_get from './../static/images/items/S_Moon_07_get.png';
import S_Moon_08_B from './../static/images/items/S_Moon_08_B.png';
import S_Moon_08_B_get from './../static/images/items/S_Moon_08_B_get.png';
import S_Moon_08_Base from './../static/images/items/S_Moon_08_Base.png';
import S_Moon_08_G from './../static/images/items/S_Moon_08_G.png';
import S_Moon_08_G_get from './../static/images/items/S_Moon_08_G_get.png';
import S_Moon_08_R from './../static/images/items/S_Moon_08_R.png';
import S_Moon_08_R_get from './../static/images/items/S_Moon_08_R_get.png';
import S_Moon_08 from './../static/images/items/S_Moon_08.png';
import S_Moon_08_get from './../static/images/items/S_Moon_08_get.png';
import S_Moon_09_Base from './../static/images/items/S_Moon_09_Base.png';
import S_Moon_09 from './../static/images/items/S_Moon_09.png';
import S_Moon_09_get from './../static/images/items/S_Moon_09_get.png';
import S_Moon_10_Base from './../static/images/items/S_Moon_10_Base.png';
import S_Moon_10 from './../static/images/items/S_Moon_10.png';
import S_Moon_10_get from './../static/images/items/S_Moon_10_get.png';
import S_Moon_11_Base from './../static/images/items/S_Moon_11_Base.png';
import S_Moon_11 from './../static/images/items/S_Moon_11.png';
import S_Moon_11_get from './../static/images/items/S_Moon_11_get.png';
import S_Moon_12 from './../static/images/items/S_Moon_12.png';
import S_Moon_13 from './../static/images/items/S_Moon_13.png';
import S_Moon_14 from './../static/images/items/S_Moon_14.png';
import S_Moon_15 from './../static/images/items/S_Moon_15.png';
import S_Moon_16 from './../static/images/items/S_Moon_16.png';
import S_Moon_17 from './../static/images/items/S_Moon_17.png';
import S_Moon_18 from './../static/images/items/S_Moon_18.png';
import S_Moon_19 from './../static/images/items/S_Moon_19.png';
import S_Moon_20 from './../static/images/items/S_Moon_20.png';
import S_Moon_21 from './../static/images/items/S_Moon_21.png';
import S_Moon_22 from './../static/images/items/S_Moon_22.png';
import S_Moon_23 from './../static/images/items/S_Moon_23.png';
import S_Moon_24 from './../static/images/items/S_Moon_24.png';
import S_Moon_25 from './../static/images/items/S_Moon_25.png';
import S_Moon_26 from './../static/images/items/S_Moon_26.png';
import S_Moon_27 from './../static/images/items/S_Moon_27.png';

export const Imglist = {
	btn,
	icon_menu,
	menu_close,
	menu_bg,
	btn_enter,
	footer_char,
	condition_frame,
	other_bg,
	p1_bg,
	preview_frame,
	bg_costume,
	title_frame,
	frame,

	diamond,
	buy_soul_button,
	buy_soul_border,
	buy_soul_char,
	open_soul_char,
	open_soul_char2,
	send_char,
	send_char2,
	combine_arrow,
	redeem_plus_btn,
	redeem_line_btn,

	S_Moon_01_B,
	S_Moon_01_B_get,
	S_Moon_01_Base,
	S_Moon_01_G,
	S_Moon_01_G_get,
	S_Moon_01_R,
	S_Moon_01_R_get,
	S_Moon_01,
	S_Moon_01_get,
	S_Moon_02_B,
	S_Moon_02_B_get,
	S_Moon_02_Base,
	S_Moon_02_G,
	S_Moon_02_G_get,
	S_Moon_02_R,
	S_Moon_02_R_get,
	S_Moon_02,
	S_Moon_02_get,
	S_Moon_03_B,
	S_Moon_03_B_get,
	S_Moon_03_Base,
	S_Moon_03_G,
	S_Moon_03_G_get,
	S_Moon_03_R,
	S_Moon_03_R_get,
	S_Moon_03,
	S_Moon_03_get,
	S_Moon_04_B,
	S_Moon_04_B_get,
	S_Moon_04_Base,
	S_Moon_04_G,
	S_Moon_04_G_get,
	S_Moon_04_R,
	S_Moon_04_R_get,
	S_Moon_04,
	S_Moon_04_get,
	S_Moon_05_B,
	S_Moon_05_B_get,
	S_Moon_05_Base,
	S_Moon_05_G,
	S_Moon_05_G_get,
	S_Moon_05_R,
	S_Moon_05_R_get,
	S_Moon_05,
	S_Moon_05_get,
	S_Moon_06_B,
	S_Moon_06_B_get,
	S_Moon_06_Base,
	S_Moon_06_G,
	S_Moon_06_G_get,
	S_Moon_06_R,
	S_Moon_06_R_get,
	S_Moon_06,
	S_Moon_06_get,
	S_Moon_07_B,
	S_Moon_07_B_get,
	S_Moon_07_Base,
	S_Moon_07_G,
	S_Moon_07_G_get,
	S_Moon_07_R,
	S_Moon_07_R_get,
	S_Moon_07,
	S_Moon_07_get,
	S_Moon_08_B,
	S_Moon_08_B_get,
	S_Moon_08_Base,
	S_Moon_08_G,
	S_Moon_08_G_get,
	S_Moon_08_R,
	S_Moon_08_R_get,
	S_Moon_08,
	S_Moon_08_get,
	S_Moon_09_Base,
	S_Moon_09,
	S_Moon_09_get,
	S_Moon_10_Base,
	S_Moon_10,
	S_Moon_10_get,
	S_Moon_11_Base,
	S_Moon_11,
	S_Moon_11_get,
	S_Moon_12,
	S_Moon_13,
	S_Moon_14,
	S_Moon_15,
	S_Moon_16,
	S_Moon_17,
	S_Moon_18,
	S_Moon_19,
	S_Moon_20,
	S_Moon_21,
	S_Moon_22,
	S_Moon_23,
	S_Moon_24,
	S_Moon_25,
	S_Moon_26,
	S_Moon_27,
}
