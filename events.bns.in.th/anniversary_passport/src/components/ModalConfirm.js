import React from 'react';
import Modal from './Modal';

export default class ModalConfirm extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                title="แจ้งเตือน"
                confirmMode={true}
                actConfirm={()=>this.props.actConfirm()}
                actClose={()=>this.props.actClose()}
            >
                <div className="inbox">
                    ซื้อแพ็จเกจ <br/>
                    มูลค่า 22,200 ไดมอนด์?
                </div>
                {/* <div className="modal__content--inner" dangerouslySetInnerHTML={{__html: this.props.msg}}/> */}
            </Modal>
        )
  }
}
