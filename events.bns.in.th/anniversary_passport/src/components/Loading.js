import React        from 'react';

export class Loading extends React.Component {
    render() {
        return (
            <div className={"page "+(this.props.open? "":"close")}>
                 <div className="page__loading">
                    กำลังดำเนินการ...
                </div> 
            </div>
        )
    }
}
