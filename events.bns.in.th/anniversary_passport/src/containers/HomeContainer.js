import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import 'whatwg-fetch';
import './../styles/preorder.css';

import { apiPost } from './../middlewares/Api';

import { Permission } from '../components/Permission';
import { Loading } from '../components/Loading';

import ModalMessage from '../components/ModalMessage';
import ModalConfirm from '../components/ModalConfirm';
import ModalConfirmCheckin from '../components/ModalConfirmCheckin';
import ModalConfirmClaimReward from '../components/ModalConfirmClaimReward';

import iconHome from './../static/images/buttons/btn_home.png';
import iconScroll from './../static/images/icons/icon_scroll.png';

//package
import items1 from './../static/images/items/1.png'
import items2 from './../static/images/items/2.png'
import items3 from './../static/images/items/3.png'
import items4 from './../static/images/items/4.png'
import items5 from './../static/images/items/5.png'
import items6 from './../static/images/items/6.png'

//special items
import special1 from './../static/images/special-items/1.png'
import special2 from './../static/images/special-items/2.png'

//check-in
import checkIn1_1 from './../static/images/checkin_items/1-1.png'
import checkIn1_2 from './../static/images/checkin_items/1-2.png'
import checkIn1_3 from './../static/images/checkin_items/1-3.png'

import checkIn2_1 from './../static/images/checkin_items/2-1.png'
import checkIn2_2 from './../static/images/checkin_items/2-2.png'
import checkIn2_3 from './../static/images/checkin_items/2-3.png'

import checkIn3_1 from './../static/images/checkin_items/3-1.png'
import checkIn3_2 from './../static/images/checkin_items/3-2.png'

import checkIn4_1 from './../static/images/checkin_items/4-1.png'
import checkIn4_2 from './../static/images/checkin_items/4-2.png'

import {
    onAccountLogout,
    setUsername,
    setSoulPermission
} from '../actions/AccountActions';

const imgList = {
    "reset_charm": checkIn1_1,
    "sakura_coin": checkIn1_2,
    "taecheon_iron": checkIn1_3,
    "blue_scale": checkIn2_1,
    "inferno_heart": checkIn2_2,
    "inari_headwear": checkIn2_3,
    "soulstone": checkIn3_1,
    "inari_okami_pet": checkIn3_2,
    "moonstone": checkIn4_1,
    "inari_outfit": checkIn4_2,
}

class HomeContainer extends Component {
    apiEventInfo() {
        this.setState({ showLoading: true });
        let self = this;
        let dataSend = { type: "event_info" };
        let successCallback = (data) => {

            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    final_rewards: data.content.final_rewards,
                    can_buy_pacakge: data.content.can_buy_pacakge,
                    unlock_package: data.content.unlock_package,
                    checkInList1: data.content.checkin_list[0].list,
                    checkInList2: data.content.checkin_list[1].list,
                    checkInList3: data.content.checkin_list[2].list,
                    checkInList4: data.content.checkin_list[3].list,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_EVENT_INFO", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiBuyPackage(){
        this.setState({ showLoading: true, showModalConfirm: false });
        let self = this;
        let dataSend = { type: "buy_anniversary_package" };
        let successCallback = (data) => {

            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    showModalMessage: data.message,
                    final_rewards: data.content.final_rewards,
                    can_buy_pacakge: data.content.can_buy_pacakge,
                    unlock_package: data.content.unlock_package,
                    checkInList1: data.content.checkin_list[0].list,
                    checkInList2: data.content.checkin_list[1].list,
                    checkInList3: data.content.checkin_list[2].list,
                    checkInList4: data.content.checkin_list[3].list,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_BUY_ANNIVERSARY_PACKAGE", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiCheckin(){
        // console.log("Week : "+this.state.week);
        // console.log("checkinId : "+this.state.checkinId);
        // console.log("Message : "+this.state.modalConfirmCheckinMessage);

        this.setState({ showLoading: true, showModalConfirmCheckin: false });
        let self = this;
        let dataSend = { type: "checkin", week: this.state.week, checkin_id: this.state.checkinId };
        let successCallback = (data) => {

            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    week: 0,
                    checkinId: 0,
                    showModalMessage: data.message,
                    final_rewards: data.content.final_rewards,
                    can_buy_pacakge: data.content.can_buy_pacakge,
                    unlock_package: data.content.unlock_package,
                    checkInList1: data.content.checkin_list[0].list,
                    checkInList2: data.content.checkin_list[1].list,
                    checkInList3: data.content.checkin_list[2].list,
                    checkInList4: data.content.checkin_list[3].list,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                    week: 0,
                    checkinId: 0,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                    week: 0,
                    checkinId: 0,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CHECKIN", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiClaimSpecialReward(){
        // console.log(this.state.week);
        this.setState({ showLoading: true, showModalConfirmSpecialReward: false });
        let self = this;
        let dataSend = { type: "claim_special_reward", week: this.state.week };
        let successCallback = (data) => {

            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    week: 0,
                    showModalMessage: data.message,
                    final_rewards: data.content.final_rewards,
                    can_buy_pacakge: data.content.can_buy_pacakge,
                    unlock_package: data.content.unlock_package,
                    checkInList1: data.content.checkin_list[0].list,
                    checkInList2: data.content.checkin_list[1].list,
                    checkInList3: data.content.checkin_list[2].list,
                    checkInList4: data.content.checkin_list[3].list,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                    week: 0,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                    week: 0,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CLAIM_SPECIAL_REWARD", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    apiClaimFinalReward(){
        // console.log(this.state.week);
        this.setState({ showLoading: true, showModalConfirmFinalReward: false });
        let self = this;
        let dataSend = { type: "claim_final_reward" };
        let successCallback = (data) => {

            if (data.status) {
                this.props.setUsername(data.content.username);
                this.setState({
                    permission: true,
                    showLoading: false,
                    showModalMessage: data.message,
                    final_rewards: data.content.final_rewards,
                    can_buy_pacakge: data.content.can_buy_pacakge,
                    unlock_package: data.content.unlock_package,
                    checkInList1: data.content.checkin_list[0].list,
                    checkInList2: data.content.checkin_list[1].list,
                    checkInList3: data.content.checkin_list[2].list,
                    checkInList4: data.content.checkin_list[3].list,
                })
            }
        };
        const failCallback = (data) => {
            if (data.type == 'no_permission') {
                this.setState({
                    showLoading: false,
                    permission: false,
                });

            }else {
                this.setState({
                    showLoading: false,
                    showModalMessage: data.message,
                });
            }
        };
        apiPost("REACT_APP_API_POST_CLAIM_FINAL_REWARD", this.props.jwtToken, dataSend, successCallback, failCallback);
    }

    constructor(props) {
        super(props)

        this.state = {
            permission: true,
            showLoading: true,
            showModalConfirm: false,
            showModalConfirmCheckin: false,
            showModalConfirmSpecialReward: false,
            showModalConfirmFinalReward: false,
            scollDown: false,
            showModalMessage:"",
            modalConfirmCheckinMessage: "",
            modalConfirmSpecialRewardMessage: "",
            modalConfirmFinalRewardMessage: "",
            week: 0,
            checkinId: 0,
            can_buy_pacakge: false,
            unlock_package: false,
            final_rewards: {},
            packageList: [
                {
                    img: items1,
                    name: 'หินโซล <br/>(500)',
                },
                {
                    img: items2,
                    name: 'หินจันทรา <br/>(50)',
                },
                {
                    img: items3,
                    name: 'เกล็ดสีคราม <br/>(5)',
                },
                {
                    img: items4,
                    name: 'ลูกแก้วดาวศุกร์ <br/>ขั้น2 (2)',
                },
                {
                    img: items5,
                    name: 'ลูกแก้วดาวเสาร์ <br/>ขั้น2 (2)',
                },
                {
                    img: items6,
                    name: 'เหรียญฝึกฝน <br/>ฮงซอกกึน(20)',
                }
            ],
            specialList:[
                {
                    img: special1,
                    name:'หินสัตว์เลี้ยงจิ้งจอกอินาริ',
                    rareItems: '',
                },
                {
                    img: special2,
                    name:'ปีกผีเสื้อแสงดาวประกายชมพู',
                    rareItems: 'หายาก',
                }
            ],
            checkInList1: [],
            checkInList2: [],
            checkInList3: [],
            checkInList4: [],

        }
    }

    componentDidMount() {
        // console.log(this.props.jwtToken);
        if (this.props.jwtToken !== "") setTimeout(() => this.apiEventInfo(), 1000);

    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.jwtToken !== prevProps.jwtToken) {
            setTimeout(() => {
                this.apiEventInfo()
            }, 1000);
        }
    }
    confirmOpen() {
        this.setState({
            showModalConfirm: true,
        });
    }
    onConfirmCheckin(e,week,checkinId,prevCheckin){
        e.preventDefault();

        let msg = "";
        if(prevCheckin == true){
            msg = "<h3>เช็คอินย้อนหลัง</h3>มูลค่า 1,000 ไดมอนด์";
        }else{
            msg = "<h3>ยืนยันเช็คอิน?</h3>";
        }

        this.setState({
            showModalConfirmCheckin: true,
            week: week,
            checkinId: checkinId,
            modalConfirmCheckinMessage: msg,
        });
    }

    onConfirmSpecialReward(e,week,itemTitle){
        e.preventDefault();
        // console.log("Week : "+ week);
        this.setState({
            showModalConfirmSpecialReward: true,
            week: week,
            modalConfirmSpecialRewardMessage: "<h3>ยืนยันรับ</h3>"+itemTitle+"?",
        });

    }

    onConfirmFinalReward(){
        this.setState({
            showModalConfirmFinalReward: true,
            modalConfirmFinalRewardMessage: "<h3>ยืนยันรับ</h3> หินสัตว์เลี้ยงจิ้งจอกอินาริ<br />ปีกผีเสื้อแสงดาวประกายชมพู ?",
        });
    }

    handleScroll(e) {
        if (e.topPosition > 10) {
            if (this.state.scollDown === false) this.setState({ scollDown: true });
        } else {
            if (this.state.scollDown === true) this.setState({ scollDown: false });
        }
    }

    render() {
        return (
            <div>
                <div className="f11layout">
                    <div className="f11layout__user">
                        <div className="f11layout__user--style">{this.props.username}</div>
                    </div>
                    <div className="f11layout__home" ><a href="/"><img src={iconHome} alt="" /></a></div>
                </div>
                {
                    this.state.permission ?
                        <div className="events-bns">
                            <ScrollArea
                                speed={0.8}
                                className="area"
                                contentClassName=""
                                horizontal={false}
                                style={{ width: '100%', height: '700px', opacity: 1 }}
                                verticalScrollbarStyle={{ backgroundColor: "#e79dee", opacity: 1 }}
                            >
                                <div className="preorder">
                                    <div class="preorder-inner">
                                        <section className="preorder__section preorder__section--header">
                                            <div className="header">
                                                <div className="header__scroll"><img src={iconScroll} alt="" /></div>
                                            </div>
                                        </section>
                                        <section className="preorder__section condition">
                                            <div className="container">
                                                <div className="section__wrapper">
                                                    <div className="section__head">
                                                        <h3>รายละเอียดโปรโมชั่น</h3>
                                                    </div>
                                                    <div className="section__content">
                                                        <ol className="condition__text">
                                                            <li>ผู้เล่นต้องจ่ายเงินซื้อแพ็คเกจมูลค่า 22,200 ไดมอนด์  ก่อน เพื่อปลดการเล่นกิจกรรม Check in</li>
                                                            <li>ผู้เล่นสามารถกดปุ่ม Check in และรับของรางวัลได้ทุกวัน</li>
                                                            <li>ผู้เล่นสามารถกดปุ่ม Check in  และรับของรางวัลย้อนหลังได้ แต่เสียค่าใช้จ่ายเป็นไดมอนด์มูลค่า 1,000 ไดมอนด์ ทุกปุ่มที่ตัวเองพลาด</li>
                                                            <li>ผู้เล่นไม่สามารถกดปุ่ม Check in ข้ามวันได้ ต้องกดปุ่ม Check in ตามลำดับที่เท่านั้น</li>
                                                            <li>ผู้เล่นสามารถกดปุ่ม รับของรางวัล Special Reward ในอาทิตย์นั้นได้ ก็ต่อเมื่อผู้เล่น กดปุ่ม Check in ทุกปุ่มก่อนหน้านั้นสำเร็จ</li>
                                                            <li>ผู้เล่นไม่สามารถรับ Special Reward ของอาทิตย์หลัง ๆ ได้ ถ้า Special Reward อาทิตย์แรก ๆ ยังไม่ได้กดรับ</li>
                                                            <li>ผู้เล่นสามารถกดรับ Exclusive Reward ได้ ก็ต่อเมื่อ Check in แบบปกติทุกวันครบ 28 วัน โดยไม่ใช้ Check in ย้อนหลัง</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section className="preorder__section anniversary">
                                            <div className="container">
                                                <div className="section__wrapper">
                                                    <div className="section__head">
                                                        <h3><span className="section__step">1</span>2 Years Anniversary Passport</h3>
                                                    </div>
                                                    <div className="section__content">
                                                        {
                                                            this.state.packageList.map((items, index) => {
                                                                return(
                                                                    <div className="items__box">
                                                                        <div className="items__boxInner">
                                                                            <div><img src={items.img}/></div>
                                                                            <div className="items__box--content">
                                                                                <div dangerouslySetInnerHTML={{__html: items.name}} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                    <div className="section__btn">
                                                        {
                                                            this.state.unlock_package ?
                                                                <div className="btn btn--default inactive" >ซื้อไปแล้ว</div>
                                                            :
                                                                (
                                                                    this.state.can_buy_pacakge ?
                                                                        <div className="btn btn--default" onClick={()=>this.confirmOpen()}>เปิดใช้งาน</div>
                                                                    :
                                                                        <div className="btn btn--default inactive" >ไดมอนด์ไม่เพียงพอ</div>
                                                                )

                                                        }


                                                    </div>
                                                </div>
                                            </div>
                                        </section> {/*  end section anniversary */}
                                        <section className="preorder__section checkIn">
                                            <div className="container">
                                                <div className="section__wrapper">
                                                    <div className="section__head">
                                                        <h3><span className="section__step">2</span>Check in รับของฟรีทุกวัน(1)</h3>
                                                    </div>
                                                    <div className="section__content">
                                                        {
                                                            this.state.checkInList1.map((items, index) => {
                                                                return(
                                                                    <div className="items__box">
                                                                        <div className="items__boxInner">
                                                                            <div className="items__box--contentTop">
                                                                                {
                                                                                    items.type == "special" ?
                                                                                        <div className="items__box--date">พิเศษ</div>
                                                                                    :
                                                                                        <div className="items__box--date">{items.checkin_date}</div>
                                                                                }
                                                                                <div>{items.title}</div>
                                                                            </div>
                                                                            <div className="items__box--img"><img src={imgList[items.img_key]}/></div>
                                                                            <div className="items__box--checkbox">
                                                                            {
                                                                                this.state.unlock_package == true ?
                                                                                    (
                                                                                        items.type == "special" ?
                                                                                            items.has_claim == true ?
                                                                                                <div className="btn btn--check disabled">รับไปแล้ว</div>
                                                                                            :
                                                                                                (
                                                                                                    items.can_claim == true ?
                                                                                                        <div className="btn btn--check" onClick={(event,week,itemTitle)=>this.onConfirmSpecialReward(event,items.week,items.title)}>รับรางวัล</div>
                                                                                                    :
                                                                                                        <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                                                )
                                                                                        :
                                                                                            (
                                                                                                items.has_checkin == true ?
                                                                                                    items.previous_checkin == true && items.has_checkin_prev == true ?
                                                                                                        <label className="custom-checkbox btn btn--check checked">Check in ย้อนหลัง
                                                                                                            <input type="checkbox" checked />
                                                                                                            <span className="checkmark checked"></span>
                                                                                                        </label>
                                                                                                    :
                                                                                                        <label className="custom-checkbox btn btn--check checked">
                                                                                                                Check in
                                                                                                                <input type="checkbox" checked />
                                                                                                                <span className="checkmark checked"></span>
                                                                                                        </label>
                                                                                                :
                                                                                                    (
                                                                                                        items.previous_checkin == true ?
                                                                                                            items.can_checkin == true ?
                                                                                                                <label className="custom-checkbox btn btn--check prev-checkin" onClick={(event,week,checkinId,prevCheckin)=>this.onConfirmCheckin(event,items.week,items.checkin_id,items.previous_checkin)}>Check in ย้อนหลัง
                                                                                                                    <input type="checkbox"/>
                                                                                                                    <span className="checkmark prev-checkin"></span>
                                                                                                                </label>
                                                                                                            :
                                                                                                                <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                                                        :
                                                                                                            (
                                                                                                                items.can_checkin == true ?
                                                                                                                    <label className="custom-checkbox btn btn--check" onClick={(event,week,checkinId,prevCheckin)=>this.onConfirmCheckin(event,items.week,items.checkin_id,items.previous_checkin)}>
                                                                                                                            Check in
                                                                                                                            <input type="checkbox"/>
                                                                                                                            <span className="checkmark"></span>
                                                                                                                    </label>
                                                                                                                :
                                                                                                                    <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                                                            )
                                                                                                    )
                                                                                            )
                                                                                    )
                                                                                :
                                                                                    <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                            }

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </section>  {/*  end section check in 1 */}
                                        <section className="preorder__section checkIn">
                                            <div className="container">
                                                <div className="section__wrapper">
                                                    <div className="section__head">
                                                        <h3><span className="section__step">2</span>Check in รับของฟรีทุกวัน(2)</h3>
                                                    </div>
                                                    <div className="section__content">
                                                        {
                                                            this.state.checkInList2.map((items, index) => {
                                                                return(
                                                                    <div className="items__box">
                                                                        <div className="items__boxInner">
                                                                            <div className="items__box--contentTop">
                                                                                {
                                                                                    items.type == "special" ?
                                                                                        <div className="items__box--date">พิเศษ</div>
                                                                                    :
                                                                                        <div className="items__box--date">{items.checkin_date}</div>
                                                                                }
                                                                                <div>{items.title}</div>
                                                                            </div>
                                                                            <div className="items__box--img"><img src={imgList[items.img_key]}/></div>
                                                                            <div className="items__box--checkbox">
                                                                            {
                                                                                this.state.unlock_package == true ?
                                                                                    (
                                                                                        items.type == "special" ?
                                                                                            items.has_claim == true ?
                                                                                                <div className="btn btn--check disabled">รับไปแล้ว</div>
                                                                                            :
                                                                                                (
                                                                                                    items.can_claim == true ?
                                                                                                        <div className="btn btn--check" onClick={(event,week,itemTitle)=>this.onConfirmSpecialReward(event,items.week,items.title)}>รับรางวัล</div>
                                                                                                    :
                                                                                                        <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                                                )
                                                                                        :
                                                                                            (
                                                                                                items.has_checkin == true ?
                                                                                                    items.previous_checkin == true && items.has_checkin_prev == true ?
                                                                                                        <label className="custom-checkbox btn btn--check checked">Check in ย้อนหลัง
                                                                                                            <input type="checkbox" checked />
                                                                                                            <span className="checkmark checked"></span>
                                                                                                        </label>
                                                                                                    :
                                                                                                        <label className="custom-checkbox btn btn--check checked">
                                                                                                                Check in
                                                                                                                <input type="checkbox" checked />
                                                                                                                <span className="checkmark checked"></span>
                                                                                                        </label>
                                                                                                :
                                                                                                    (
                                                                                                        items.previous_checkin == true ?
                                                                                                            items.can_checkin == true ?
                                                                                                                <label className="custom-checkbox btn btn--check prev-checkin" onClick={(event,week,checkinId,prevCheckin)=>this.onConfirmCheckin(event,items.week,items.checkin_id,items.previous_checkin)}>Check in ย้อนหลัง
                                                                                                                    <input type="checkbox"/>
                                                                                                                    <span className="checkmark prev-checkin"></span>
                                                                                                                </label>
                                                                                                            :
                                                                                                                <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                                                        :
                                                                                                            (
                                                                                                                items.can_checkin == true ?
                                                                                                                    <label className="custom-checkbox btn btn--check" onClick={(event,week,checkinId,prevCheckin)=>this.onConfirmCheckin(event,items.week,items.checkin_id,items.previous_checkin)}>
                                                                                                                            Check in
                                                                                                                            <input type="checkbox"/>
                                                                                                                            <span className="checkmark"></span>
                                                                                                                    </label>
                                                                                                                :
                                                                                                                    <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                                                            )
                                                                                                    )
                                                                                            )
                                                                                    )
                                                                                :
                                                                                    <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                            }

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </section>  {/*  end section check in 2 */}
                                        <section className="preorder__section checkIn checkIn3">
                                            <div className="container">
                                                <div className="section__wrapper">
                                                    <div className="section__head">
                                                        <h3><span className="section__step">2</span>Check in รับของฟรีทุกวัน(3)</h3>
                                                    </div>
                                                    <div className="section__content">
                                                        {
                                                            this.state.checkInList3.map((items, index) => {
                                                                return(
                                                                    <div className="items__box">
                                                                        <div className="items__boxInner">
                                                                            <div className="items__box--contentTop">
                                                                                {
                                                                                    items.type == "special" ?
                                                                                        <div className="items__box--date">พิเศษ</div>
                                                                                    :
                                                                                        <div className="items__box--date">{items.checkin_date}</div>
                                                                                }
                                                                                <div>{items.title}</div>
                                                                            </div>
                                                                            <div className="items__box--img"><img src={imgList[items.img_key]}/></div>
                                                                            <div className="items__box--checkbox">
                                                                            {
                                                                                this.state.unlock_package == true ?
                                                                                    (
                                                                                        items.type == "special" ?
                                                                                            items.has_claim == true ?
                                                                                                <div className="btn btn--check disabled">รับไปแล้ว</div>
                                                                                            :
                                                                                                (
                                                                                                    items.can_claim == true ?
                                                                                                        <div className="btn btn--check" onClick={(event,week,itemTitle)=>this.onConfirmSpecialReward(event,items.week,items.title)}>รับรางวัล</div>
                                                                                                    :
                                                                                                        <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                                                )
                                                                                        :
                                                                                            (
                                                                                                items.has_checkin == true ?
                                                                                                    items.previous_checkin == true && items.has_checkin_prev == true ?
                                                                                                        <label className="custom-checkbox btn btn--check checked">Check in ย้อนหลัง
                                                                                                            <input type="checkbox" checked />
                                                                                                            <span className="checkmark checked"></span>
                                                                                                        </label>
                                                                                                    :
                                                                                                        <label className="custom-checkbox btn btn--check checked">
                                                                                                                Check in
                                                                                                                <input type="checkbox" checked />
                                                                                                                <span className="checkmark checked"></span>
                                                                                                        </label>
                                                                                                :
                                                                                                    (
                                                                                                        items.previous_checkin == true ?
                                                                                                            items.can_checkin == true ?
                                                                                                                <label className="custom-checkbox btn btn--check prev-checkin" onClick={(event,week,checkinId,prevCheckin)=>this.onConfirmCheckin(event,items.week,items.checkin_id,items.previous_checkin)}>Check in ย้อนหลัง
                                                                                                                    <input type="checkbox"/>
                                                                                                                    <span className="checkmark prev-checkin"></span>
                                                                                                                </label>
                                                                                                            :
                                                                                                                <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                                                        :
                                                                                                            (
                                                                                                                items.can_checkin == true ?
                                                                                                                    <label className="custom-checkbox btn btn--check" onClick={(event,week,checkinId,prevCheckin)=>this.onConfirmCheckin(event,items.week,items.checkin_id,items.previous_checkin)}>
                                                                                                                            Check in
                                                                                                                            <input type="checkbox"/>
                                                                                                                            <span className="checkmark"></span>
                                                                                                                    </label>
                                                                                                                :
                                                                                                                    <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                                                            )
                                                                                                    )
                                                                                            )
                                                                                    )
                                                                                :
                                                                                    <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                            }

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </section>  {/*  end section check in 3 */}
                                        <section className="preorder__section checkIn">
                                            <div className="container">
                                                <div className="section__wrapper">
                                                    <div className="section__head">
                                                        <h3><span className="section__step">2</span>Check in รับของฟรีทุกวัน(4)</h3>
                                                    </div>
                                                    <div className="section__content">
                                                        {
                                                            this.state.checkInList4.map((items, index) => {
                                                                return(
                                                                    <div className="items__box">
                                                                        <div className="items__boxInner">
                                                                            <div className="items__box--contentTop">
                                                                                {
                                                                                    items.type == "special" ?
                                                                                        <div className="items__box--date">พิเศษ</div>
                                                                                    :
                                                                                        <div className="items__box--date">{items.checkin_date}</div>
                                                                                }
                                                                                <div>{items.title}</div>
                                                                            </div>
                                                                            <div className="items__box--img"><img src={imgList[items.img_key]}/></div>
                                                                            <div className="items__box--checkbox">
                                                                            {
                                                                                this.state.unlock_package == true ?
                                                                                    (
                                                                                        items.type == "special" ?
                                                                                            items.has_claim == true ?
                                                                                                <div className="btn btn--check disabled">รับไปแล้ว</div>
                                                                                            :
                                                                                                (
                                                                                                    items.can_claim == true ?
                                                                                                        <div className="btn btn--check" onClick={(event,week,itemTitle)=>this.onConfirmSpecialReward(event,items.week,items.title)}>รับรางวัล</div>
                                                                                                    :
                                                                                                        <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                                                )
                                                                                        :
                                                                                            (
                                                                                                items.has_checkin == true ?
                                                                                                    items.previous_checkin == true && items.has_checkin_prev == true ?
                                                                                                        <label className="custom-checkbox btn btn--check checked">Check in ย้อนหลัง
                                                                                                            <input type="checkbox" checked />
                                                                                                            <span className="checkmark checked"></span>
                                                                                                        </label>
                                                                                                    :
                                                                                                        <label className="custom-checkbox btn btn--check checked">
                                                                                                                Check in
                                                                                                                <input type="checkbox" checked />
                                                                                                                <span className="checkmark checked"></span>
                                                                                                        </label>
                                                                                                :
                                                                                                    (
                                                                                                        items.previous_checkin == true ?
                                                                                                            items.can_checkin == true ?
                                                                                                                <label className="custom-checkbox btn btn--check prev-checkin" onClick={(event,week,checkinId,prevCheckin)=>this.onConfirmCheckin(event,items.week,items.checkin_id,items.previous_checkin)}>Check in ย้อนหลัง
                                                                                                                    <input type="checkbox"/>
                                                                                                                    <span className="checkmark prev-checkin"></span>
                                                                                                                </label>
                                                                                                            :
                                                                                                                <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                                                        :
                                                                                                            (
                                                                                                                items.can_checkin == true ?
                                                                                                                    <label className="custom-checkbox btn btn--check" onClick={(event,week,checkinId,prevCheckin)=>this.onConfirmCheckin(event,items.week,items.checkin_id,items.previous_checkin)}>
                                                                                                                            Check in
                                                                                                                            <input type="checkbox"/>
                                                                                                                            <span className="checkmark"></span>
                                                                                                                    </label>
                                                                                                                :
                                                                                                                    <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                                                            )
                                                                                                    )
                                                                                            )
                                                                                    )
                                                                                :
                                                                                    <div className="btn btn--check disabled">ไม่ตรงเงื่อนไข</div>
                                                                            }

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </section>   {/*  end section check in 4 */}
                                        <section className="preorder__section special">
                                            <div className="container">
                                                <div className="section__wrapper">
                                                    <div className="section__head">
                                                        <h3><span className="section__step">3</span>รางวัลสุดเอ็กซ์คลูซีฟสำหรับคุณ</h3>
                                                    </div>
                                                    <div className="section__content">
                                                        {
                                                            this.state.specialList.map((items, index) => {
                                                                return(
                                                                    <div className="items__box">
                                                                        <div className="items__boxInner">
                                                                            <div>
                                                                                {
                                                                                    items.rareItems !== '' ?
                                                                                    <div className="items__box--rareItems">{items.rareItems}</div>
                                                                                    :
                                                                                    null
                                                                                }
                                                                                <img src={items.img}/>
                                                                            </div>
                                                                            <div className="items__box--content">
                                                                                <div>{items.name}</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                        <p className="text--purple">
                                                            **ผู้เล่นสามารถกดรับ Exclusive Reward ได้ ก็ต่อเมื่อ <br/>
                                                            Check in แบบปกติทุกวันครบ 28 วัน โดยไม่ใช้ Check in ย้อนหลัง**
                                                        </p>
                                                    </div>
                                                    <div className="section__btn">
                                                        {
                                                            this.state.final_rewards.has_claim == true ?
                                                                <div className="btn btn--default inactive">รับไปแล้ว</div>
                                                            :
                                                                (
                                                                    this.state.final_rewards.can_claim == true ?
                                                                        <div className="btn btn--default" onClick={this.onConfirmFinalReward.bind(this)}>รับของรางวัลพิเศษ</div>
                                                                    :
                                                                        <div className="btn btn--default inactive">ไม่ตรงเงื่อนไข</div>
                                                                )
                                                        }

                                                    </div>
                                                </div>
                                            </div>
                                        </section> {/*  end section special */}
                                    </div>
                                </div>
                            </ScrollArea>
                        </div>
                    :
                    <Permission />
                }

                <Loading
                    open={this.state.showLoading}
                />

                <ModalConfirm
                    open={this.state.showModalConfirm}
                    actConfirm={() => this.apiBuyPackage()}
                    actClose={() => this.setState({
                        showModalConfirm: false
                    })}
                    msg={this.state.modalConfirmMessage}
                />

                <ModalConfirmCheckin
                    open={this.state.showModalConfirmCheckin}
                    actConfirm={() => this.apiCheckin()}
                    actClose={() => this.setState({
                        showModalConfirmCheckin: false,
                        modalConfirmCheckinMessage: "",
                    })}
                    msg={this.state.modalConfirmCheckinMessage}
                />

                <ModalConfirmClaimReward
                    open={this.state.showModalConfirmSpecialReward}
                    actConfirm={() => this.apiClaimSpecialReward()}
                    actClose={() => this.setState({
                        showModalConfirmSpecialReward: false,
                        modalConfirmSpecialRewardMessage: "",
                    })}
                    msg={this.state.modalConfirmSpecialRewardMessage}
                />

                <ModalConfirmClaimReward
                    open={this.state.showModalConfirmFinalReward}
                    actConfirm={() => this.apiClaimFinalReward()}
                    actClose={() => this.setState({
                        showModalConfirmFinalReward: false,
                        modalConfirmFinalRewardMessage: "",
                    })}
                    msg={this.state.modalConfirmFinalRewardMessage}
                />

                <ModalMessage
                    open={this.state.showModalMessage.length > 0}
                    actClose={()=>this.setState({showModalMessage: ""})}
                    msg={this.state.showModalMessage}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    userData: state.AccountReducer.userData,
    loginUrl: state.AccountReducer.loginUrl,
    logoutUrl: state.AccountReducer.logoutUrl,
    jwtToken: state.AccountReducer.jwtToken,
    username: state.AccountReducer.username,
    permission: state.AccountReducer.permission
})

const mapDispatchToProps = {
    onAccountLogout, setUsername, setSoulPermission
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeContainer)
