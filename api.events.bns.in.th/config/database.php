<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    // 'default' => env('DB_CONNECTION','events_bns_test'),
    'default' => 'events_bns_test',

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        # events_bns
        'events_bns' => [
            'driver'    => env('EVENTS_BNS_DRIVER', 'mysql'),
            'host'      => env('EVENTS_BNS_HOST'),
            'database'  => env('EVENTS_BNS_DATABASE'),
            'username'  => env('EVENTS_BNS_USERNAME'),
            'password'  => env('EVENTS_BNS_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        # events_bns_test
        'events_bns_test' => [
            'driver'    => env('EVENTS_BNS_TEST_DRIVER', 'mysql'),
            'host'      => env('EVENTS_BNS_TEST_HOST'),
            'database'  => env('EVENTS_BNS_TEST_DATABASE'),
            'username'  => env('EVENTS_BNS_TEST_USERNAME'),
            'password'  => env('EVENTS_BNS_TEST_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        # esports_bns_test
        'esports_bns_test' => [
            'driver'    => env('ESPORTS_BNS_TEST_DRIVER', 'mysql'),
            'host'      => env('ESPORTS_BNS_TEST_HOST'),
            'database'  => env('ESPORTS_BNS_TEST_DATABASE'),
            'username'  => env('ESPORTS_BNS_TEST_USERNAME'),
            'password'  => env('ESPORTS_BNS_TEST_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        # esports_bns
        'esports_bns' => [
            'driver'    => env('ESPORTS_BNS_DRIVER', 'mysql'),
            'host'      => env('ESPORTS_BNS_HOST'),
            'database'  => env('ESPORTS_BNS_DATABASE'),
            'username'  => env('ESPORTS_BNS_USERNAME'),
            'password'  => env('ESPORTS_BNS_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'garena_bns' => [
            'driver'    => env('GARENA_BNS_DRIVER', 'mysql'),
            'host'      => env('GARENA_BNS_HOST'),
            'port'      => env('GARENA_BNS_PORT'),
            'database'  => env('GARENA_BNS_DATABASE'),
            'username'  => env('GARENA_BNS_USERNAME'),
            'password'  => env('GARENA_BNS_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mongodb_bns' => [
            'driver' => 'mongodb',
            'host' => env('MONGODB_HOST', '103.80.219.46'),
            'port' => '27017',
            'database' => 'events_bns',
            //'username'  => 'events_bns',
            //'password'  => 'db0dii,[ugvHogvl'
        ],

        'mongodb_bns_test' => [
            'driver' => 'mongodb',
            'host' => env('MONGODB_HOST', '103.80.219.46'),
            'port' => '27017',
            'database' => 'events_bns_test',
            //'username'  => 'events_bns',
            //'password'  => 'db0dii,[ugvHogvl'
        ],

        // CODM
        # events_codm
        'events_codm' => [
            'driver'    => 'mysql',
            'host'      => '111.223.35.24',
            'port'      => '3306',
            'database'  => 'garena_events_codm',
            'username'  => 'garena_codm',
            'password'  => 'dkiuojk:uFvfugvH,',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        'events_codm_test' => [
            'driver'    => 'mysql',
            'host'      => '111.223.35.24',
            'port'      => '3306',
            'database'  => 'garena_events_codm_test',
            'username'  => 'garena_codm',
            'password'  => 'dkiuojk:uFvfugvH,',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        // 'client' => 'predis',

        'cluster' => false,
        'default' => [
            'host'     => env('REDIS_HOST', '111.223.35.176'),
            'port'     => 6379,
            'password' => 'kaj2fgGkRT5YQw84',
            'database' => 7,
        ],
        'cache' => [
            'host'     => env('REDIS_HOST', '111.223.35.176'),
            'port'     => 6379,
            'password' => 'kaj2fgGkRT5YQw84',
            'database' => env('REDIS_DB', 7),
        ],
        'event_requests_limit' => [
            'host'     => env('REDIS_HOST', '111.223.35.176'),
            'port'     => 6379,
            'password' => 'kaj2fgGkRT5YQw84',
            'database' => 7,
        ],
        'queue' => [
            'host'     => env('REDIS_HOST', '111.223.35.176'),
            'port'     => 6379,
            'password' => 'kaj2fgGkRT5YQw84',
            'database' => 7,
        ],
        'session' => [
            'host'     => env('REDIS_HOST', '111.223.35.176'),
            'port'     => 6379,
            'password' => 'kaj2fgGkRT5YQw84',
            'database' => 0,
        ],

    ],

];
