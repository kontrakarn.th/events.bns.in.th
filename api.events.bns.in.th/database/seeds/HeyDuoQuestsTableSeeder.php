<?php

use Illuminate\Database\Seeder;
use App\Models\Test\hey_duo\Quest;

class HeyDuoQuestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Quest::truncate();

        Quest::insert([
            [
                'quest_dungeon' => 'วิหารทรายวายุ',
                'quest_title' => 'กุลา',
                'quest_code' => 'ME_WA_sandTemple_Boss_0002',
                'quest_id' => 1,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'เรือโจรสลัดต้องสาป',
                'quest_title' => 'มุกชอลวัน',
                'quest_code' => 'ME_SB_BlackFist_0001',
                'quest_id' => 2,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'กลุ่มหน้ากากปีศาจ',
                'quest_title' => 'ราชาหน้ากากปีศาจ',
                'quest_code' => 'ME_WC_PekingOperaWarrior_0001',
                'quest_id' => 3,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'แหล่งกำเนิดทัพปีศาจ',
                'quest_title' => 'ราชาปีศาจชอนอึม',
                'quest_code' => 'ME_SB_Gargoyle_0001',
                'quest_id' => 4,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'วังลีลาสวรรค์ที่บิดเบี้ยว',
                'quest_title' => 'ซอบมู',
                'quest_code' => 'ME_SC_SoulHammer_0001',
                'quest_id' => 5,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'หุบเขาวิหคสายฟ้า',
                'quest_title' => 'อายันคา',
                'quest_code' => 'ME_WB_IronFeatherBoss_0001',
                'quest_id' => 6,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'โรงเก็บหุ่นยนต์ หมายเลข 0',
                'quest_title' => 'ซอลยอง หมายเลข 0',
                'quest_code' => 'ME_SC_BattleMachine_0001',
                'quest_id' => 7,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'Daily Free Points x3',
                'quest_title' => 'Daily Free Points x3',
                'quest_code' => null,
                'quest_id' => 8,
                'quest_type' => 'daily_free_points',
                'quest_limit' => 1,
                'quest_points' => 3,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'จักรพรรดิราตรีบรรพกาลบ้าคลั่ง (ระดับที่ 2)',
                'quest_title' => 'จักรพรรดิราตรีบรรพกาลบ้าคลั่ง (ระดับที่ 2)',
                'quest_code' => 25134,
                'quest_id' => 9,
                'quest_type' => 'daily_area',
                'quest_limit' => 1,
                'quest_points' => 2,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'จักรพรรดิราตรีบรรพกาลบ้าคลั่ง (ระดับที่ 4)',
                'quest_title' => 'จักรพรรดิราตรีบรรพกาลบ้าคลั่ง (ระดับที่ 4)',
                'quest_code' => 25135,
                'quest_id' => 10,
                'quest_type' => 'daily_area',
                'quest_limit' => 1,
                'quest_points' => 3,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'จักรพรรดิราตรีบรรพกาลบ้าคลั่ง (ระดับที่ 6)',
                'quest_title' => 'จักรพรรดิราตรีบรรพกาลบ้าคลั่ง (ระดับที่ 6)',
                'quest_code' => 25136,
                'quest_id' => 11,
                'quest_type' => 'daily_area',
                'quest_limit' => 1,
                'quest_points' => 15,
                'image' => null,
            ],
        ]);
    }
}
