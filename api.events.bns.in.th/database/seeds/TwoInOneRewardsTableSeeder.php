<?php

use Flynsarmy\CsvSeeder\CsvSeeder;

class TwoInOneRewardsTableSeeder extends CsvSeeder {

	public function __construct()
	{
        $this->table = 'two_in_one_rewards';
        $this->connection = 'events_bns_test';
        $this->filename = base_path().'/database/seeds/csvs/two_in_one_rewards.csv';
        $this->offset_rows = 1;
        $this->mapping = [
            0 => 'package_name',
            1 => 'package_id',
            2 => 'package_quantity',
            3 => 'package_amount',
            4 => 'package_type',
            5 => 'required_tokens',
            6 => 'limit',
            7 => 'img',
            8 => 'hover',
        ];
	}

	public function run()
	{
		// Recommended when importing larger CSVs
		DB::disableQueryLog();

		// Uncomment the below to wipe the table clean before populating
		DB::table($this->table)->truncate();

		parent::run();
	}
}