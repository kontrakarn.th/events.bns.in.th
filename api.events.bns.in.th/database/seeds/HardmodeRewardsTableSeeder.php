<?php

use App\Models\Test\hardmode\Reward;
use Illuminate\Database\Seeder;

class HardmodeRewardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Reward::truncate();

        Reward::insert([
            [
                'package_name' => 'แพ็คเกจความสวยงามของผ้าขนหนู x1',
                'package_id' => 3392,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'package',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'คริสตัลหินโซล x50',
                'package_id' => 3393,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'fix',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'คริสตัลหินจันทรา x25',
                'package_id' => 3393,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'fix',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ประกายฉลามดำ x5',
                'package_id' => 3393,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'fix',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'หินจันทรา x5',
                'package_id' => 3393,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'fix',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'หินโซล x5',
                'package_id' => 3393,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'fix',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'เกล็ดสีคราม x1',
                'package_id' => 2071,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 15,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ดาวมังกรฟ้า x1',
                'package_id' => 2648,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 14.65,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ลูกแก้วยอดนักรบ x1',
                'package_id' => 2647,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 10,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ลูกแก้วดาวอังคาร ขั้น 3 x1',
                'package_id' => 3394,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 10,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ลูกแก้วดาวพุธ ขั้น 3 x1',
                'package_id' => 3395,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 10,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ลูกแก้วดาวพฤหัส ขั้น 3 x1',
                'package_id' => 3396,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 10,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ปีกแทชอน x1',
                'package_id' => 2242,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 10,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ยันต์ปริศนาแห่งฮงมุน x1',
                'package_id' => 2192,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 10,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'หญ้าซาฮวา x1',
                'package_id' => 3064,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 10,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'เหรียญมังกรสมุทร x1',
                'package_id' => 3351,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 0.05,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'เกล็ดสีทอง x1',
                'package_id' => 3146,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 0.05,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ดาวมังกรแดง x1',
                'package_id' => 3224,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 0.05,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'วิญญาณซาฮวา x1',
                'package_id' => 3141,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 0.05,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ลูกแก้วมังกรสมุทร x1',
                'package_id' => 3353,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 0.05,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ผ้าโพกหัวออนเซ็น x1',
                'package_id' => 1424,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 0.05,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ชุดผ้าขนหนูออนเซ็น x1',
                'package_id' => 1423,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'random',
                'rate' => 0.05,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => null,
                'limit' => null,
            ],
            [
                'package_name' => 'ผ้าโพกหัวออนเซ็น x1',
                'package_id' => 1424,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim_start',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => '2020-06-09 00:00:00',
                'required_tokens' => 50,
                'limit' => null,
            ],
            [
                'package_name' => 'ชุดผ้าขนหนูออนเซ็น x1',
                'package_id' => 1423,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim_start',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => '2020-06-09 00:00:00',
                'required_tokens' => 150,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องหินสถานะ ท้าทายรายวัน x1',
                'package_id' => 2718,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim_limit',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 10,
                'limit' => 10,
            ],
            [
                'package_name' => 'กล่องหินสกิล ท้าทายรายวัน x1',
                'package_id' => 3223,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim_limit',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 15,
                'limit' => 10,
            ],
            [
                'package_name' => 'ดาวมังกรแดง x1',
                'package_id' => 3224,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim_limit',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 30,
                'limit' => 5,
            ],
            [
                'package_name' => 'เกล็ดสีทอง x1',
                'package_id' => 3146,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim_limit',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 25,
                'limit' => 5,
            ],
            [
                'package_name' => 'เกล็ดสีคราม x1',
                'package_id' => 2071,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 5,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส) x1',
                'package_id' => 2134,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 5,
                'limit' => null,
            ],
            [
                'package_name' => 'ประกายฉลามดำ x1',
                'package_id' => 1150,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 5,
                'limit' => null,
            ],
            [
                'package_name' => 'วิญญาณวายุทมิฬ x1',
                'package_id' => 2000,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 5,
                'limit' => null,
            ],
            [
                'package_name' => 'ปีกวายุทมิฬ x1',
                'package_id' => 1328,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 5,
                'limit' => null,
            ],
            [
                'package_name' => 'สัญลักษณ์ลานฝึกเทวา x1',
                'package_id' => 2719,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 5,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องแหวนราชาทมิฬ ขั้น 1 x1',
                'package_id' => 2688,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 10,
                'limit' => null,
            ],
            [
                'package_name' => 'ถุงมือราชันย์ ขั้น 1 x1',
                'package_id' => 2683,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 10,
                'limit' => null,
            ],
            [
                'package_name' => 'เข็มขัดราชันย์พยัคฆ์ ขั้น 1 x1',
                'package_id' => 2684,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 10,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องสร้อยข้อมือเทพมังกร ขั้น 1 x1',
                'package_id' => 2685,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 10,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องสร้อยคอราชินีทมิฬ ขั้น 1 x1',
                'package_id' => 2686,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 10,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องต่างหูราชาทมิฬ ขั้น 1 x1',
                'package_id' => 2687,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 10,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องโซลชิลด์พายุดำหมายเลข1 x1',
                'package_id' => 1716,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 5,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องโซลชิลด์พายุดำหมายเลข2 x1',
                'package_id' => 1717,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 5,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องโซลชิลด์พายุดำหมายเลข3 x1',
                'package_id' => 1718,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 5,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องโซลชิลด์พายุดำหมายเลข4 x1',
                'package_id' => 1719,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 5,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องโซลชิลด์พายุดำหมายเลข5 x1',
                'package_id' => 1720,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 10,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องโซลชิลด์พายุดำหมายเลข6 x1',
                'package_id' => 1721,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 10,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องโซลชิลด์พายุดำหมายเลข7 x1',
                'package_id' => 1722,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 15,
                'limit' => null,
            ],
            [
                'package_name' => 'กล่องโซลชิลด์พายุดำหมายเลข8 x1',
                'package_id' => 1723,
                'package_quantity' => 1,
                'package_amount' => 1,
                'package_type' => 'claim',
                'rate' => 0,
                'package_key' => null,
                'claim_start' => null,
                'required_tokens' => 20,
                'limit' => null,
            ],
        ]);
    }
}
