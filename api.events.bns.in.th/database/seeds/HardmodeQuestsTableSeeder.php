<?php

use App\Models\Test\hardmode\Quest;
use Illuminate\Database\Seeder;

class HardmodeQuestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Quest::truncate();

        Quest::insert([
            [
                'quest_dungeon' => 'วิหารทรายวายุ',
                'quest_title' => 'กุลา',
                'quest_code' => 'ME_WA_sandTemple_Boss_0002',
                'quest_id' => 1,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'required_diamonds' => 0,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'เรือโจรสลัดต้องสาป',
                'quest_title' => 'มุกชอลวัน',
                'quest_code' => 'ME_SB_BlackFist_0001',
                'quest_id' => 2,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'required_diamonds' => 0,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'กลุ่มหน้ากากปีศาจ',
                'quest_title' => 'ราชาหน้ากากปีศาจ',
                'quest_code' => 'ME_WC_PekingOperaWarrior_0001',
                'quest_id' => 3,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'required_diamonds' => 0,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'แหล่งกำเนิดทัพปีศาจ',
                'quest_title' => 'ราชาปีศาจชอนอึม',
                'quest_code' => 'ME_SB_Gargoyle_0001',
                'quest_id' => 4,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'required_diamonds' => 0,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'วังลีลาสวรรค์ที่บิดเบี้ยว',
                'quest_title' => 'ซอบมู',
                'quest_code' => 'ME_SC_SoulHammer_0001',
                'quest_id' => 5,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'required_diamonds' => 0,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'หุบเขาวิหคสายฟ้า',
                'quest_title' => 'อายันคา',
                'quest_code' => 'ME_WB_IronFeatherBoss_0001',
                'quest_id' => 6,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'required_diamonds' => 0,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'โรงเก็บหุ่นยนต์ หมายเลข 0',
                'quest_title' => 'ซอลยอง หมายเลข 0',
                'quest_code' => 'ME_SC_BattleMachine_0001',
                'quest_id' => 7,
                'quest_type' => 'daily_bosskill',
                'quest_limit' => 1,
                'quest_points' => 1,
                'required_diamonds' => 0,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'Daily Free Points x3',
                'quest_title' => 'Daily Free Points x3',
                'quest_code' => null,
                'quest_id' => 8,
                'quest_type' => 'daily_free_points',
                'quest_limit' => 1,
                'quest_points' => 3,
                'required_diamonds' => 0,
                'image' => null,
            ],
            [
                'quest_dungeon' => 'Daily Purchase Points x10',
                'quest_title' => 'Daily Purchase Points x10',
                'quest_code' => null,
                'quest_id' => 9,
                'quest_type' => 'daily_purchase_points',
                'quest_limit' => 1,
                'quest_points' => 10,
                'required_diamonds' => 5000,
                'image' => null,
            ],
        ]);
    }
}
