<?php

use Flynsarmy\CsvSeeder\CsvSeeder;

class HmPassSs2RewardsTableSeeder extends CsvSeeder
{
    public function __construct()
	{
        $this->table = 'hm_pass_ss2_rewards';
        $this->connection = 'events_bns_test';
		$this->filename = base_path().'/database/seeds/csvs/hm_pass_ss2_rewards.csv';
		$this->offset_rows = 1;
		$this->mapping = [
			0 => 'package_name',
			1 => 'package_id',
			2 => 'package_quantity',
			3 => 'package_amount',
			4 => 'package_type',
			5 => 'required_points',
			6 => 'required_lvl',
			7 => 'is_big_reward',
			8 => 'icon',
			9 => 'hover',
		];
	}

	public function run()
	{
		// Recommended when importing larger CSVs
		DB::disableQueryLog();

		// Uncomment the below to wipe the table clean before populating
		DB::table($this->table)->truncate();

		parent::run();
	}
}
