<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopupMayMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topup_may_members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid')->index();
            $table->string('username')->nullable();
            $table->string('ncid')->index();
            $table->unsignedInteger('total_diamonds')->default(0);
            $table->string('last_ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topup_may_members');
    }
}
