<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Carbon;

class CreateSgvSendItemLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('events_bns_test')->create('sgv_send_item_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid');
            $table->string('ncid');
            $table->string('status');
            $table->boolean('send_item_status')->default(false);
            $table->string('send_item_purchase_id');
            $table->string('send_item_purchase_status');
            $table->text('goods_data');
            $table->string('last_ip')->default('0.0.0.0');
            $table->string('type');
            $table->date('log_date')->default(Carbon::now()->toDateString());
            $table->integer('log_date_timestamp')->default(Carbon::now()->timestamp);
            // new
            $table->string('send_gift_from')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sgv_send_item_logs');
    }
}
