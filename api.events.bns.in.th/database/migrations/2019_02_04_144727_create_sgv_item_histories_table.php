<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Carbon;

class CreateSgvItemHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('events_bns_test')->create('sgv_item_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->nullable();
            $table->string('product_title')->nullable();
            $table->unsignedInteger('product_quantity')->default(0);
            $table->string('uid');
            $table->string('ncid')->default('')->nullable();
            $table->string('last_ip')->default('0.0.0.0');
            $table->string('icon');
            $table->string('gachapon_type');
            $table->date('log_date')->default(Carbon::now()->toDateString());
            $table->integer('log_date_timestamp')->default(Carbon::now()->timestamp);
            $table->string('status');
            $table->string('send_type');
            $table->string('item_type');
            // new field
            $table->string('product_set')->nullable();
            $table->text('product_pack_data')->nullable();
            $table->string('send_to_uid')->nullable();
            $table->string('send_to_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sgv_item_histories');
    }
}
