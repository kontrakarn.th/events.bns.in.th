<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Carbon;

class CreateSgvDeductLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('events_bns_test')->create('sgv_deduct_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->default('pending');
            $table->unsignedInteger('diamond')->default(0);
            $table->string('uid')->nullable();
            $table->string('ncid')->nullable();
            $table->string('last_ip')->default('0.0.0.0');
            $table->string('deduct_type')->nullable();
            $table->date('log_date')->default(Carbon::now()->toDateString());
            $table->integer('log_date_timestamp')->default(Carbon::now()->timestamp);
            $table->unsignedInteger('before_deduct_diamond')->default(0);
            $table->unsignedInteger('after_deduct_diamond')->default(0);
            $table->boolean('deduct_status')->default(false);
            $table->string('deduct_purchase_id')->nullable();
            $table->string('deduct_purchase_status')->nullable();
            $table->text('deduct_data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sgv_deduct_logs');
    }
}
