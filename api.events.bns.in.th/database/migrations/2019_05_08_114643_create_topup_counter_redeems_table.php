<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopupCounterRedeemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topup_may_counter_redeems', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bonus_1')->default(0);
            $table->unsignedInteger('bonus_2')->default(0);
            $table->unsignedInteger('bonus_3')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topup_may_counter_redeems');
    }
}
