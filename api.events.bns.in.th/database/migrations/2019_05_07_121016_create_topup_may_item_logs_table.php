<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopupMayItemLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topup_may_item_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid')->index();
            $table->string('ncid')->index();
            $table->unsignedInteger('require_diamonds')->default(0);
            $table->unsignedInteger('package_id')->index();
            $table->string('package_title');
            $table->text('product_set');
            $table->boolean('send_item_status')->default(false);
            $table->string('send_item_purchase_id')->nullable()->index();
            $table->string('send_item_purchase_status')->nullable();
            $table->text('goods_data')->nullable();
            $table->enum('status', [
                'pending',
                'success',
                'unsuccess',
                'processing'
            ])->default('pending');
            $table->string('last_ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topup_may_item_logs');
    }
}
