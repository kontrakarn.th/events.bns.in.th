<?php

namespace App\Traits;

use Error;

trait ApiServiceTrait
{
    private $baseApi = 'http://api.apps.garena.in.th';

    public function getResponseByQuestType($questLog, $startDateTime, $endDateTime)
    {
        switch ($questLog->quest->quest_type) {
            case 'xp_gain':
                return $this->apiCharExpGain($questLog, $startDateTime, $endDateTime);

            case 'bosskill':
                $newbieDifficultyId = 13;
                return $this->apiBossKillByDifficulty($questLog, $startDateTime, $endDateTime, $newbieDifficultyId);

            case 'bosskill_hard':
                $hardDifficultyId = 12;
                return $this->apiBossKillByDifficulty($questLog, $startDateTime, $endDateTime, $hardDifficultyId);

            case 'area':
                return $this->apiQuestCompleted($questLog, $startDateTime, $endDateTime);

            case 'battlefield_1':
            case 'battlefield_2':
            case 'battlefield_3':
                return $this->apiBattleFieldResults($questLog, $startDateTime, $endDateTime);

            case '1v1':
                return $this->api1v1History($questLog, $startDateTime, $endDateTime);

            case '3v3':
                return $this->api3v3History($questLog, $startDateTime, $endDateTime);

            case 'daily_challenge':
                return $this->apiDailyChallenge($questLog, $startDateTime, $endDateTime);

            default:
                throw new Error('getResponseByQuestType : Quest type not found');
        }
    }

    public function getCompletedCountByQuestType($questLog, $response)
    {
        switch ($questLog->quest->quest_type) {
            case 'xp_gain':
                if ($questLog->member->mastery_level == 30) {
                    return 1;
                }
                $xpGain = (int) $response->response->exp_gain;
                return $xpGain >= $questLog->quest->xp ? 1 : 0;

            case 'bosskill':
            case 'bosskill_hard':
                return (int) $response->response->kill_count;

            case 'area':
                return (int) $response->response->quest_completed_count;

            case 'battlefield_1':
                $type1 = collect($response->response)->firstWhere('type', '1');
                return $type1 ? (int) $type1->total_count : 0;

            case 'battlefield_2':
                $type2 = collect($response->response)->firstWhere('type', '2');
                return $type2 ? (int) $type2->total_count : 0;

            case 'battlefield_3':
                $type3 = collect($response->response)->firstWhere('type', '3');
                return $type3 ? (int) $type3->total_count : 0;

            case '1v1':
                return collect($response->response)->filter(function ($value, $key) {
                    return $value->type == '1';
                })->count();

            case '3v3':
                return collect($response->response)->filter(function ($value, $key) {
                    return $value->type == '4';
                })->count();

            case 'daily_challenge':
                return (int) $response->response->count;

            default:
                throw new Error('getCompletedCountByQuestType : Quest type not found');
        }
    }

    private function apiCharExpGain($questLog, $startDateTime, $endDateTime)
    {
        if (empty($questLog) || empty($startDateTime) || empty($endDateTime)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'get_char_exp_gain',
            'char_id'    => $questLog->member->char_id,
            'start_time' => $startDateTime,
            'end_time'   => $endDateTime,
        ]);

        return json_decode($data);
    }

    private function apiBossKillByDifficulty($questLog, $startDateTime, $endDateTime, $difficultyId)
    {
        if (empty($questLog) || empty($startDateTime) || empty($endDateTime) || empty($difficultyId)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'get_boss_killed_by_difficulty',
            'char_id'    => $questLog->member->char_id,
            'boss_id'   => $questLog->quest->quest_code,
            'difficulty_id' => $difficultyId, // 11 = ง่าย, 12 = ยาก, 13 = มือใหม่
            'start_time' => $startDateTime,
            'end_time'   => $endDateTime,
        ]);

        return json_decode($data);
    }

    private function apiQuestCompleted($questLog, $startDateTime, $endDateTime)
    {
        if (empty($questLog) || empty($startDateTime) || empty($endDateTime)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $questLog->member->char_id,
            'quest_id'   => $questLog->quest->quest_code,
            'start_time' => $startDateTime,
            'end_time'   => $endDateTime,
        ]);

        return json_decode($data);
    }

    private function apiBattleFieldResults($questLog, $startDateTime, $endDateTime)
    {
        if (empty($questLog) || empty($startDateTime) || empty($endDateTime)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'get_battlefield_results',
            'char_id'    => $questLog->member->char_id,
            'start_time' => $startDateTime,
            'end_time'   => $endDateTime,
            // 'char_id'    => '487684',
            // 'start_time' => '2020-07-08 06:00:00',
            // 'end_time' => '2020-07-15 05:59:59',
        ]);

        return json_decode($data);
    }

    private function api1v1History($questLog, $startDateTime, $endDateTime)
    {
        if (empty($questLog) || empty($startDateTime) || empty($endDateTime)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'events',
            'subservice' => 'get_1v1_history',
            'char_id'    => $questLog->member->char_id,
            'start_time' => $startDateTime,
            'end_time'   => $endDateTime,
            'user_id' => $questLog->member->ncid,
            // 'char_id'    => '2221267',
            // 'start_time' => '2020-06-23 00:00:00',
            // 'end_time' => '2020-06-23 23:59:59',
            // 'user_id' => '799B1331-949C-4B7E-9210-D03583029973',
        ]);

        return json_decode($data);
    }

    private function api3v3History($questLog, $startDateTime, $endDateTime)
    {
        if (empty($questLog) || empty($startDateTime) || empty($endDateTime)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'events',
            'subservice' => 'get_3v3_history',
            'char_id'    => $questLog->member->char_id,
            'start_time' => $startDateTime,
            'end_time'   => $endDateTime,
            'user_id' => $questLog->member->ncid,
            // 'char_id'    => '2221267',
            // 'start_time' => '2020-06-23 00:00:00',
            // 'end_time' => '2020-06-23 23:59:59',
            // 'user_id' => '799B1331-949C-4B7E-9210-D03583029973',
        ]);

        return json_decode($data);
    }

    private function apiDailyChallenge($questLog, $startDateTime, $endDateTime)
    {
        if (empty($questLog) || empty($startDateTime) || empty($endDateTime)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'get_daily_challenge_completed_count',
            'char_id'    => $questLog->member->char_id,
            'start_time' => $startDateTime,
            'end_time'   => $endDateTime,
        ]);

        return json_decode($data);
    }

    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }
}