<?php

    namespace App\Models\fightersoul;

    use Moloquent;

    /**
     * Description of Member
     *
     * @author naruebaetbouhom
     */
    class Member extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'fightersoul_member';
        protected $fillable = ['uid', 'username', 'ncid', 'last_ip', 'log_timestamp'];

    }
