<?php

    namespace App\Models\league;

    use App\Models\BaseModel;

    class Quest extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'league_quests';
        protected $fillable = [
            'quest_dungeon',
            'quest_title',
            'quest_code',
            'quest_id',
            'quest_type',
            'quest_limit',
            'quest_points',
            'image',
        ];

    }