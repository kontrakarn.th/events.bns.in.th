<?php

    namespace App\Models\league;

    use App\Models\BaseModel;

    class Color extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'league_colors';
        protected $fillable = [
            'color_id',
            'color_title',
            'color_npc',
            'member_count',
            'total_points',
            'color_rank',
            'member_top_rank',
            'member_top_rank_points',
        ];

    }