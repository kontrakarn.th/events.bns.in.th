<?php

    namespace App\Models\league;

    use App\Models\BaseModel;

    class Group extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'league_groups';
        protected $fillable = [
            'group_id',
            'member_count',
        ];

    }