<?php

    namespace App\Models\hm_welfare_package;

    use App\Models\BaseModel;

    class Deductlog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'hm_welfare_package_deductlogs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'discount', // 0 = no, 1 = yes
            'diamonds',
            'package_id',
            'package_title',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];
    }