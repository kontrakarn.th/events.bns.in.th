<?php

    namespace App\Models\hm_welfare_package;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'hm_welfare_package_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'diamonds',
            'user_type',
            'booking_type',
            'purchased',
            'last_ip',
            'created_at',
            'updated_at'
        ];
    }