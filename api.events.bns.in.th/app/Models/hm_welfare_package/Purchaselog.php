<?php

    namespace App\Models\hm_welfare_package;

    use App\Models\BaseModel;

    class Purchaselog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'hm_welfare_package_purchaselogs';
        protected $fillable = [
            'uid',
            'ncid',
            'deduct_id',
            'require_diamonds',
            'package_id',
            'package_title',
            'product_set',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status', // pending, success, unsuccess
            'last_ip',
            'log_date',
            'log_date_timestamp'
        ];
    }