<?php

    namespace App\Models\hm_welfare_package;

    use App\Models\BaseModel;

    class Bookinglog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'hm_welfare_package_bookinglogs';
    }