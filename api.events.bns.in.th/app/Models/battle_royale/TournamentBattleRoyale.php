<?php

namespace App\Models\battle_royale;

use App\Models\BaseModel;

class TournamentBattleRoyale extends BaseModel
{

    protected $connection = 'esports_bns_test';
    // protected $connection = 'esports_bns';
    protected $table = 'esports_tournament_battle_royale';
}
