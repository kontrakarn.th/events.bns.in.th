<?php

namespace App\Models\bns_word;

use Illuminate\Database\Eloquent\Model;

class BnsWording extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'bns_wording';
}
