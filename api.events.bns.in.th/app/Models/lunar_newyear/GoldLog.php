<?php

    namespace App\Models\lunar_newyear;

    use App\Models\BaseModel;

    class GoldLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'lunar_newyear_gold_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'buns_log_id',
            'play_key',
            'gold',
            'gold_type',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }