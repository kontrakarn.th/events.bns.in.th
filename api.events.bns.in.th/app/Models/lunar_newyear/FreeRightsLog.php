<?php

    namespace App\Models\lunar_newyear;

    use App\Models\BaseModel;

    class FreeRightsLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'lunar_newyear_free_rights_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'used_status',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }