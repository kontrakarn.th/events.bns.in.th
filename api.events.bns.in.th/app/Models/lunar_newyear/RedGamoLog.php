<?php

    namespace App\Models\lunar_newyear;

    use App\Models\BaseModel;

    class RedGamoLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'lunar_newyear_red_gamo_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'buns_log_id',
            // 'used_status', // not_use, used
            'status', // success, unsuccess
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }