<?php

    namespace App\Models\lunar_newyear;

    use App\Models\BaseModel;

    class BunsPlayLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'lunar_newyear_buns_play_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'buns_key',
            'buns_title',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }