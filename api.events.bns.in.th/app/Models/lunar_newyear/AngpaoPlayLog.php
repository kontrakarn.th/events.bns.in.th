<?php

    namespace App\Models\lunar_newyear;

    use App\Models\BaseModel;

    class AngpaoPlayLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'lunar_newyear_angpao_play_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'deduct_red_gamo_id',
            'play_key', // not_use, used
            'score',
            'status', // success, unsuccess
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }