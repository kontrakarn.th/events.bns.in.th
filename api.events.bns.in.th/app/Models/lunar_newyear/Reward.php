<?php

    namespace App\Models\lunar_newyear;

    class Reward {

        // Exchange Rewards
        public function setExchangeByPackageId($packageId=null){
            if(empty($packageId)){
                return false;
            }

            $rewards = $this->exchangeRewardList();

            return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
        }

        public function exchangeRewardList(){
            return [
                [
                    'id' => 1,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ชุดเซ็ตความหวังใหม่',
                    'product_id' => 2069,
                    'product_quantity' => 1,
                    'require_points' => 500,
                    'exchange_limit' => 1,
                    'amount' => 1,
                ],
                [
                    'id' => 2,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ชุดตะวันฉายประกายแดง',
                    'product_id' => 2093,
                    'product_quantity' => 1,
                    'require_points' => 200,
                    'exchange_limit' => 1,
                    'amount' => 1,
                ],
                [
                    'id' => 3,
                    'item_type' => 'exchange_points',
                    'product_title' => 'กล่องอาวุธลวงตาราชันย์',
                    'product_id' => 2070,
                    'product_quantity' => 1,
                    'require_points' => 200,
                    'exchange_limit' => 1,
                    'amount' => 1,
                ],
                [
                    'id' => 4,
                    'item_type' => 'exchange_points',
                    'product_title' => 'เกล็ดสีคราม',
                    'product_id' => 2071,
                    'product_quantity' => 1,
                    'require_points' => 50,
                    'exchange_limit' => 4,
                    'amount' => 1,
                ],
                [
                    'id' => 5,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ประกายฉลามดำ',
                    'product_id' => 1150,
                    'product_quantity' => 1,
                    'require_points' => 25,
                    'exchange_limit' => 9999,
                    'amount' => 1,
                ],
                [
                    'id' => 6,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ลูกแก้วเบลูก้า',
                    'product_id' => 873,
                    'product_quantity' => 1,
                    'require_points' => 25,
                    'exchange_limit' => 9999,
                    'amount' => 1,
                ],
                [
                    'id' => 7,
                    'item_type' => 'exchange_points',
                    'product_title' => 'หินล้ำค่าหายาก',
                    'product_id' => 2072,
                    'product_quantity' => 1,
                    'require_points' => 25,
                    'exchange_limit' => 9999,
                    'amount' => 20,
                ],
                [
                    'id' => 8,
                    'item_type' => 'exchange_points',
                    'product_title' => 'คริสตัลหินจันทรา',
                    'product_id' => 442,
                    'product_quantity' => 1,
                    'require_points' => 25,
                    'exchange_limit' => 9999,
                    'amount' => 25,
                ],
                [
                    'id' => 9,
                    'item_type' => 'exchange_points',
                    'product_title' => 'คริสตัลหินโซล',
                    'product_id' => 1067,
                    'product_quantity' => 1,
                    'require_points' => 15,
                    'exchange_limit' => 9999,
                    'amount' => 100,
                ],
                [
                    'id' => 10,
                    'item_type' => 'exchange_points',
                    'product_title' => 'สัญลักษณ์ฮงมุน',
                    'product_id' => 1884,
                    'product_quantity' => 1,
                    'require_points' => 10,
                    'exchange_limit' => 9999,
                    'amount' => 100,
                ],
                [
                    'id' => 11,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ยาโชคชะตา',
                    'product_id' => 1674,
                    'product_quantity' => 1,
                    'require_points' => 1,
                    'exchange_limit' => 9999,
                    'amount' => 1,
                ],
            ];
        }

        // Ranking Rewards
        public function setRankByPackageId($packageId=null){
            if(empty($packageId)){
                return false;
            }

            $rewards = $this->rankRewardList();

            return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
        }

        public function rankRewardList(){
            return [
                [
                    'id' => 1,
                    'item_type' => 'rank_reward',
                    'product_title' => 'รางวัลอันดับที่ 1',
                    'product_id' => 2073,
                    'product_quantity' => 1,
                    'amount' => 1,
                ],
                [
                    'id' => 2,
                    'item_type' => 'rank_reward',
                    'product_title' => 'รางวัลอันดับที่ 2',
                    'product_id' => 2074,
                    'product_quantity' => 1,
                    'amount' => 1,
                ],
                [
                    'id' => 3,
                    'item_type' => 'rank_reward',
                    'product_title' => 'รางวัลอันดับที่ 3',
                    'product_id' => 2075,
                    'product_quantity' => 1,
                    'amount' => 1,
                ],
                [
                    'id' => 4,
                    'item_type' => 'rank_reward',
                    'product_title' => 'รางวัลอันดับที่ 4',
                    'product_id' => 2076,
                    'product_quantity' => 1,
                    'amount' => 1,
                ],
                [
                    'id' => 5,
                    'item_type' => 'rank_reward',
                    'product_title' => 'รางวัลอันดับที่ 5',
                    'product_id' => 1671,
                    'product_quantity' => 1,
                    'amount' => 1,
                ],
                [
                    'id' => 6,
                    'item_type' => 'rank_reward',
                    'product_title' => 'รางวัลอันดับที่ 6',
                    'product_id' => 2077,
                    'product_quantity' => 1,
                    'amount' => 1,
                ],
            ];
        }
    }