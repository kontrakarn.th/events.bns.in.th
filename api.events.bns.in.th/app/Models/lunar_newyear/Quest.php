<?php

    namespace App\Models\lunar_newyear;

    class Quest {

        // Dungeon Quests
        public function getQuestList(){
            return [
                [
                    'id' => 1,
                    'quest_code' => 1558,
                    'quest_title' => 'ภัยคุกคามปริศนาที่ตื่นขึ้น',
                    'quest_dungeon' => 'วิหารทรายวายุ',
                    'blue_gamo' => 2,
                    'blue_gamo_wd' => 4,
                ],
                [
                    'id' => 2,
                    'quest_code' => 1735,
                    'quest_title' => 'ฝันร้ายแห่งกรุสมบัติ',
                    'quest_dungeon' => 'กรุสมบัตินาริว',
                    'blue_gamo' => 2,
                    'blue_gamo_wd' => 4,
                ],
                [
                    'id' => 3,
                    'quest_code' => 1697,
                    'quest_title' => 'เสียงเพลงอันโหยหวน',
                    'quest_dungeon' => 'ถ้ำใต้น้ำเสียงสะท้อน',
                    'blue_gamo' => 2,
                    'blue_gamo_wd' => 4,
                ],
                [
                    'id' => 4,
                    'quest_code' => 1672,
                    'quest_title' => 'ผู้พลัดถิ่นแห่งเมืองโบราณพรรณไม้',
                    'quest_dungeon' => 'เมืองโบราณพรรณไม้',
                    'blue_gamo' => 2,
                    'blue_gamo_wd' => 4,
                ],
                [
                    'id' => 5,
                    'quest_code' => 1614,
                    'quest_title' => 'เสียงเพรียกของสายลม',
                    'quest_dungeon' => 'โรงหลอมเครื่องจักร',
                    'blue_gamo' => 2,
                    'blue_gamo_wd' => 4,
                ],
                [
                    'id' => 6,
                    'quest_code' => 1674,
                    'quest_title' => 'หุบเขาเหมันต์',
                    'quest_dungeon' => 'โรงหลอมเครื่องจักร',
                    'blue_gamo' => 1,
                    'blue_gamo_wd' => 2,
                ],
                [
                    'id' => 7,
                    'quest_code' => 1623,
                    'quest_title' => 'รุกฆาต',
                    'quest_dungeon' => 'วิหารลับเงามังกรดำ',
                    'blue_gamo' => 1,
                    'blue_gamo_wd' => 2,
                ],
                [
                    'id' => 8,
                    'quest_code' => 1528,
                    'quest_title' => 'ผู้บุกรุกที่ศาลนักพรตนาริว',
                    'quest_dungeon' => 'สถานศักดิ์สิทธิ์นาริว',
                    'blue_gamo' => 1,
                    'blue_gamo_wd' => 2,
                ],
                [
                    'id' => 9,
                    'quest_code' => 1478,
                    'quest_title' => 'เสียงหอนของเตาหลอม',
                    'quest_dungeon' => 'โรงหลอมอัคคี',
                    'blue_gamo' => 1,
                    'blue_gamo_wd' => 2,
                ],
                [
                    'id' => 10,
                    'quest_code' => 1365,
                    'quest_title' => 'พันธะที่ผูกเราไว้',
                    'quest_dungeon' => 'สุสานที่ถูกลืม',
                    'blue_gamo' => 1,
                    'blue_gamo_wd' => 2,
                ],
            ];
        }

        // Buns Gachapon Rate
        public function getRandomBuns(){

            $buns = $this->getBunsRate();

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($buns as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    // $value['color'] = $this->randomColor();
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            $reward['timestamp'] = time();

            // now we have the reward
            return $reward;
        }

        private function getBunsRate(){
            return [
                [
                    'id' => 1,
                    'title' => 'ซาลาเปาหน้ายิ้ม',
                    'chance' => 33.33,
                ],
                [
                    'id' => 2,
                    'title' => 'ซาลาเปาหน้าโกรธ',
                    'chance' => 33.33,
                ],
                [
                    'id' => 3,
                    'title' => 'ซาลาเปาหน้าร้องไห้',
                    'chance' => 33.34,
                ],
            ];
        }

    }