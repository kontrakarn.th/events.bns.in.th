<?php

    namespace App\Models\lunar_newyear;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {

        protected $connection = 'events_bns';
        protected $table = 'lunar_newyear_send_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'deduct_gold_id',
            'reward_id',
            'product_id',
            'product_title',
            'product_quantity',
            'item_type', // 'exchange_points','rank_reward'
            'amount',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    