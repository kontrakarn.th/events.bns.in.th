<?php

    namespace App\Models\topup_may2020;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'topup_may2020_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'already_claimed',
            'selected_package', // refer: package_key
            'last_ip',
        ];

    }