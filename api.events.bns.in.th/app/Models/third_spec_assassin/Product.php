<?php

namespace App\Models\third_spec_assassin;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'third_spec_assassin_product';
    protected $fillable = [
        'id',
        'product_group_id',
        'th_name',
        'en_name',
        'amount',
        'rate',
        'product_img',
    ];

}
