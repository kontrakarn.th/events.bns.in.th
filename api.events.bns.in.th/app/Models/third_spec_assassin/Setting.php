<?php

namespace App\Models\third_spec_assassin;

use App\Models\BaseModel;

class Setting extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'third_spec_assassin_settings';
    protected $fillable = [
        'event_start',
        'event_end',
        'attend_start',
        'attend_end',
        'exchange_start',
        'exchange_end',
        'active',
    ];
}
