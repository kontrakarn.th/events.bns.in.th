<?php

namespace App\Models\songkran2020;

use Illuminate\Database\Eloquent\Model;

class Cron extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'songkran2020_crons';
}
