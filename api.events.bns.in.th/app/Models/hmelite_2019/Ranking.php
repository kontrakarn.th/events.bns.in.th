<?php

namespace App\Models\hmelite_2019;

use Illuminate\Database\Eloquent\Model;

class Ranking extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'hmelite_2019_ranking';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'nickname',
        'no',
        'vip',
        'diamond',
        'ranking_at',
    ];

}
