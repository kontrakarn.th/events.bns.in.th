<?php

namespace App\Models\hmelite_2019;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //add this line


class ItemList extends Model
{
    use SoftDeletes; //add this line

    protected $connection = 'events_bns';
    protected $table = 'hmelite_2019_item_list';
    protected $fillable = [
        'product_id',
        'product_title',
        'amt',
        'weight',
        'item_price',
        'icon',
        'example_img',
    ];
    protected $dates = ['deleted_at'];


    public static function getItemByProductId($product_id){
        $data=self::where('product_id',$product_id)->first();
        return $data;
    }
}
