<?php

namespace App\Models\hmelite_2019;

use Illuminate\Database\Eloquent\Model;

class MemberLogs extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'hmelite_2019_member_logs';
    protected $fillable = [
        'no_edit',
        'uid',
        'username',
        'ncid',
        'firstname',
        'lastname',
        'nickname',
        'tel',
        'email',
        'birthday',
        'vip',
        'last_ip',
    ];
}
