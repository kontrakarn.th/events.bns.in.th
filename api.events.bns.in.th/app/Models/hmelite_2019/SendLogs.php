<?php

namespace App\Models\hmelite_2019;

use Illuminate\Database\Eloquent\Model;

class SendLogs extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'hmelite_2019_sendlogs';
    protected $fillable = [
        'transaction_id',
        'uid',
        'ncid',
        'nickname',
        'item_type',
        'item_no',
        'product_title',
        'product_id',
        'product_amt',
        'item_price',
        'icon',
        'item_date',
        'status',
        'send_item_purchase_id',
        'send_item_purchase_status',
        'goods_data',
        'item_status',
        'send_to_deduct_id',
        'send_to_uid',
        'send_to_name',
        'last_ip',
    ];
}
