<?php

namespace App\Models\hmelite_2019;

use Illuminate\Database\Eloquent\Model;

class DiamondLogs extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'hmelite_2019_diamond_logs';
    protected $fillable = [
        'uid',
        'ncid',
        'username',
        'nickname',
        'diamond',
        'last_redeem',
        'vip',
        'ranking_at',
        'created_at',
        'updated_at',
    ];

}
