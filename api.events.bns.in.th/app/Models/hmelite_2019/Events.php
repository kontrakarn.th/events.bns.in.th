<?php

namespace App\Models\hmelite_2019;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'hmelite_2019_events';


    public static function getEventByID($id){
        $data =self::find($id);
        return $data;
    }
}
