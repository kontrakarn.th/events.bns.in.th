<?php

namespace App\Models\hmelite_2019;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //add this line
use DB;

class ItemSelect extends Model
{
    use SoftDeletes; //add this line

    protected $connection = 'events_bns';
    protected $table = 'hmelite_2019_item_select';
    protected $fillable = [
        'item_id',
        'type',
        'item_date',
        'show_only',
        'active',
        'active_test',
    ];

    protected $dates = ['deleted_at'];

    public function item()
    {
        return $this->belongsTo('App\Models\hmelite_2019\ItemList','item_id','id');
    }

    public static function getItemSelectByType($type,$month,$year){
        $model= new ItemSelect;
        $data=DB::connection($model->getConnectionName())->select("SELECT l.*,s.item_date,s.show_only,s.active,s.active_test
FROM hmelite_2019_item_select as s INNER JOIN hmelite_2019_item_list as l ON s.item_id=l.id
WHERE (s.active=1 OR s.active_test=1) AND s.type='".$type."' AND MONTH(s.item_date)=".$month." AND YEAR(s.item_date)=".$year." AND s.deleted_at IS NULL AND l.deleted_at IS NULL" );
        return $data;
    }

    public static function getItemSelectByTypeForRanking($type,$month,$year){
        $model= new ItemSelect;
        $data=DB::connection($model->getConnectionName())->select("SELECT l.*,s.item_date,s.show_only,s.active,s.active_test
FROM hmelite_2019_item_select as s INNER JOIN hmelite_2019_item_list as l ON s.item_id=l.id
WHERE s.active=1 AND s.type='".$type."' AND MONTH(s.item_date)=".$month." AND YEAR(s.item_date)=".$year." AND s.deleted_at IS NULL AND l.deleted_at IS NULL" );
        return $data;
    }
}
