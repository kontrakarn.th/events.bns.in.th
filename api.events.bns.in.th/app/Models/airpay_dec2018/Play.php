<?php

    namespace App\Models\airpay_dec2018;

    use App\Models\BaseModel;

    class Play extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'airpay_december_2018_play';
        protected $fillable = [
            'id',
            'ncid',
            'uid',
            'package_id',
            'status',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'created_at',
            'updated_at',
        ];

        public static function getAmtTicketUsed(){
            $data = self::count();
            return $data;
        }
        public static function getAmtTicketUsedByUid($uid){
            $data = self::where('uid',$uid)->count();
            return $data;
        }
    }
