<?php

    namespace App\Models\archer_preorder;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'archer_preorder_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'discount_used',
            'last_ip',
            'created_at',
            'updated_at'
        ];

    }