<?php

namespace App\Models\soul_gachapon_valentine_2019;

class sgv_reward
{

    public function getRandomRewardInfo()
    {

        $rewards = $this->getRewardsList();

        $random = rand(0, 100000);
        $accumulatedChance = 0;
        $reward = false;

        foreach ($rewards as $value) {
            $chance = $value['chance'] * 1000;
            $currentChance = $accumulatedChance + $chance;

                // acquired reward
            if ($random >= $accumulatedChance && $random <= $currentChance) {
                    // $value['color'] = $this->randomColor();
                $reward = $value;
                break;
            }
            $accumulatedChance = $currentChance;
        }

        $reward['random'] = $random;

            // now we have the reward
        return $reward;
    }

    protected function getRewardsList()
    {
        return [
            [
                'id' => 1,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ตม้าขาว',
                'product_id' => 2079,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'white_horse_set.png',
                'key' => 'white_horse_set',
                'color' => 'gold'
            ],
            [
                'id' => 2,
                'item_type' => 'rare_item',
                'product_title' => 'ปีกม้าขาว',
                'product_id' => 2080,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'white_horse_wings.png',
                'key' => 'white_horse_wings',
                'color' => 'gold'
            ],
            [
                'id' => 3,
                'item_type' => 'rare_item',
                'product_title' => 'หินล้ำค่าเปกาซัส',
                'product_id' => 2081,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'pegasus_pet.png',
                'key' => 'pegasus_pet',
                'color' => 'gold'
            ],
            [
                'id' => 4,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดรักหวานชื่น',
                'product_id' => 2083,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'love_suit.png',
                'key' => 'love_suit',
                'color' => 'gold'
            ],
            [
                'id' => 5,
                'item_type' => 'rare_item',
                'product_title' => 'หมวกรักกันไม่เสื่อมคลาย',
                'product_id' => 2082,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'love_hat.png',
                'key' => 'love_hat',
                'color' => 'gold'
            ],
            [
                'id' => 6,
                'item_type' => 'rare_item',
                'product_title' => 'กล่องอาวุธลวงตาฟลามิงโก้',
                'product_id' => 2078,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'doflamingo_weapon_skin.png',
                'key' => 'doflamingo_weapon_skin',
                'color' => 'gold'
            ],
            // Items parts set A
            [
                'id' => 7,
                'item_type' => 'material',
                'product_title' => 'เซตม้าขาว Part.1',
                'product_id_as' => 'MAT_1_1',
                'product_id' => 20791,
                'product_quantity' => 1,
                'chance' => 1.500,
                'icon' => 'white_horse_set_part1.png',
                'key' => 'white_horse_set_part1',
                'color' => 'purple'
            ],
            [
                'id' => 8,
                'item_type' => 'material',
                'product_title' => 'เซตม้าขาว Part.2',
                'product_id_as' => 'MAT_1_2',
                'product_id' => 20792,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'white_horse_set_part2.png',
                'key' => 'white_horse_set_part2',
                'color' => 'purple'
            ],
                // Items parts set B
            [
                'id' => 9,
                'item_type' => 'material',
                'product_title' => 'ปีกม้าขาว Part.1',
                'product_id_as' => 'MAT_2_1',
                'product_id' => 20801,
                'product_quantity' => 1,
                'chance' => 1.500,
                'icon' => 'white_horse_wings_part1.png',
                'key' => 'white_horse_wings_part1',
                'color' => 'purple'
            ],
            [
                'id' => 10,
                'item_type' => 'material',
                'product_title' => 'ปีกม้าขาว Part.2',
                'product_id_as' => 'MAT_2_2',
                'product_id' => 20802,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'white_horse_wings_part2.png',
                'key' => 'white_horse_wings_part2',
                'color' => 'purple'
            ],
                // Items parts set C
            [
                'id' => 11,
                'item_type' => 'material',
                'product_title' => 'หินล้ำค่าเปกาซัส Part.1',
                'product_id_as' => 'MAT_3_1',
                'product_id' => 20811,
                'product_quantity' => 1,
                'chance' => 1.500,
                'icon' => 'pegasus_pet_part1.png',
                'key' => 'pegasus_pet_part1',
                'color' => 'purple'
            ],
            [
                'id' => 12,
                'item_type' => 'material',
                'product_title' => 'หินล้ำค่าเปกาซัส Part.2',
                'product_id_as' => 'MAT_3_2',
                'product_id' => 20812,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'pegasus_pet_part2.png',
                'key' => 'pegasus_pet_part2',
                'color' => 'purple'
            ],

            // Items parts set D
            [
                'id' => 13,
                'item_type' => 'material',
                'product_title' => 'ชุดรักหวานชื่น Part.1',
                'product_id_as' => 'MAT_4_1',
                'product_id' => 20831,
                'product_quantity' => 1,
                'chance' => 1.500,
                'icon' => 'love_suit_part1.png',
                'key' => 'love_suit_part1',
                'color' => 'purple'
            ],
            [
                'id' => 14,
                'item_type' => 'material',
                'product_title' => 'ชุดรักหวานชื่น Part.2',
                'product_id_as' => 'MAT_4_2',
                'product_id' => 20832,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'love_suit_part2.png',
                'key' => 'love_suit_part2',
                'color' => 'purple'
            ],

            // Items parts set E
            [
                'id' => 15,
                'item_type' => 'material',
                'product_title' => 'หมวกรักกันไม่เสื่อมคลาย Part.1',
                'product_id_as' => 'MAT_5_1',
                'product_id' => 20821,
                'product_quantity' => 1,
                'chance' => 1.500,
                'icon' => 'love_hat_part1.png',
                'key' => 'love_hat_part1',
                'color' => 'purple'
            ],
            [
                'id' => 16,
                'item_type' => 'material',
                'product_title' => 'หมวกรักกันไม่เสื่อมคลาย Part.2',
                'product_id_as' => 'MAT_5_2',
                'product_id' => 20822,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'love_hat_part2.png',
                'key' => 'love_hat_part2',
                'color' => 'purple'
            ],

            // Object item
            [
                'id' => 17,
                'item_type' => 'object',
                'product_title' => 'ปีกวายุทมิฬ x1',
                'product_id' => 1328,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'hive_queen_wing.png',
                'key' => 'hive_queen_wing',
                'color' => 'red'
            ],
            [
                'id' => 18,
                'item_type' => 'object',
                'product_title' => 'หินจันทรา x10',
                'product_id' => 571,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'moonstone.png',
                'key' => 'moonstone',
                'color' => 'red'
            ],
            [
                'id' => 19,
                'item_type' => 'object',
                'product_title' => 'หินโซล x100',
                'product_id' => 1918,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'soulstone.png',
                'key' => 'soulstone',
                'color' => 'red'
            ],
            [
                'id' => 20,
                'item_type' => 'object',
                'product_title' => 'สัญลักษณ์ฮงมุน x50',
                'product_id' => 1881,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'hongmoon_medal.png',
                'key' => 'hongmoon_medal',
                'color' => 'red'
            ],
            [
                'id' => 21,
                'item_type' => 'object',
                'product_title' => 'เหรียญสัมฤทธิ์ x1',
                'product_id' => 2084,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'achievement_medal.png',
                'key' => 'achievement_medal',
                'color' => 'red'
            ],
            [
                'id' => 22,
                'item_type' => 'object',
                'product_title' => 'ประกายฉลามดำ x2',
                'product_id' => 1852,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'blackshark_scale.png',
                'key' => 'blackshark_scale',
                'color' => 'red'
            ],
            [
                'id' => 23,
                'item_type' => 'object',
                'product_title' => 'เศษเกล็ดสีคราม x5',
                'product_id' => 2085,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'bluesky_scale.png',
                'key' => 'bluesky_scale',
                'color' => 'red'
            ],
            [
                'id' => 24,
                'item_type' => 'object',
                'product_title' => 'เกล็ดมังกรไฟ x5',
                'product_id' => 1987,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'firedragon_scale.png',
                'key' => 'firedragon_scale',
                'color' => 'red'
            ],
            [
                'id' => 25,
                'item_type' => 'object',
                'product_title' => 'ศิลาปรนิมมิต x1',
                'product_id' => 2086,
                'product_quantity' => 1,
                'chance' => 3.000,
                'icon' => 'encourage_stone.png',
                'key' => 'encourage_stone',
                'color' => 'red'
            ],
            [
                'id' => 26,
                'item_type' => 'object',
                'product_title' => 'หินเปลี่ยนรูปขั้นสูง x1',
                'product_id' => 911,
                'product_quantity' => 1,
                'chance' => 3.000,
                'icon' => 'advanced_transmute_stone.png',
                'key' => 'advanced_transmute_stone',
                'color' => 'red'
            ],
            [
                'id' => 27,
                'item_type' => 'object',
                'product_title' => 'คริสตัลวิญญาณฮงมุน x1',
                'product_id' => 1896,
                'product_quantity' => 1,
                'chance' => 2.500,
                'icon' => 'hongmoon_soul_crystal.png',
                'key' => 'hongmoon_soul_crystal',
                'color' => 'red'
            ],
        ];
    }

    // Rare Items
    public function setRareItemByPackageId($packageId=null){
        if(empty($packageId)){
            return false;
        }

        $rewards = $this->rareItemList();

        return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
    }

    private function rareItemList(){
        return [
            [
                'id' => 1,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ตม้าขาว',
                'product_id' => 2079,
                'product_quantity' => 1,
                'icon' => 'white_horse_set.png',
                'key' => 'white_horse_set',
                'color' => 'red'
            ],
            [
                'id' => 2,
                'item_type' => 'rare_item',
                'product_title' => 'ปีกม้าขาว',
                'product_id' => 2080,
                'product_quantity' => 1,
                'icon' => 'white_horse_wings.png',
                'key' => 'white_horse_wings',
                'color' => 'red'
            ],
            [
                'id' => 3,
                'item_type' => 'rare_item',
                'product_title' => 'หินล้ำค่าเปกาซัส',
                'product_id' => 2081,
                'product_quantity' => 1,
                'icon' => 'pegasus_pet.png',
                'key' => 'pegasus_pet',
                'color' => 'red'
            ],
            [
                'id' => 4,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดรักเราหวานชื่น',
                'product_id' => 2083,
                'product_quantity' => 1,
                'icon' => 'love_suit.png',
                'key' => 'love_suit',
                'color' => 'red'
            ],
            [
                'id' => 5,
                'item_type' => 'rare_item',
                'product_title' => 'หมวกรักกันไม่เสื่อมคลาย',
                'product_id' => 2082,
                'product_quantity' => 1,
                'icon' => 'love_hat.png',
                'key' => 'love_hat',
                'color' => 'red'
            ],
            [
                'id' => 6,
                'item_type' => 'rare_item',
                'product_title' => 'กล่องอาวุธลวงตาฟลามิงโก้',
                'product_id' => 2078,
                'product_quantity' => 1,
                'icon' => 'doflamingo_weapon_skin.png',
                'key' => 'doflamingo_weapon_skin',
                'color' => 'red'
            ],
        ];
    }

}
