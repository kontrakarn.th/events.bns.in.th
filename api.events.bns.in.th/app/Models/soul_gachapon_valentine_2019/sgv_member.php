<?php

namespace App\Models\soul_gachapon_valentine_2019;

use Illuminate\Database\Eloquent\Model;

class sgv_member extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'sgv_members';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'last_ip'
    ];
}
