<?php

namespace App\Models\battle_field;

use Illuminate\Database\Eloquent\Model;

class QuestLog extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'battle_field_quest_logs';

    protected $fillable = [
        'quest_success_1',
        'quest_success_2',
        'quest_success_3',
        'quest_success_4',
        'quest_success_5',
        'quest_success_6',
        'quest_success_7',
        'quest_success_8',
        'quest_success_9',
        'quest_free',
        'coin',
        'last_update',
        'response',
        'is_play',
        'peak_time_coin',
        'peak_3',
        'peak_6',
        'peak_9',
        'api_check_time',
    ];

}
