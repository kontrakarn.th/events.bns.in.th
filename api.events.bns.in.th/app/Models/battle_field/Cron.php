<?php

namespace App\Models\battle_field;

use Illuminate\Database\Eloquent\Model;

class Cron extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'battle_field_crons';
}
