<?php

namespace App\Models\battle_field;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'battle_field_rewards';

}
