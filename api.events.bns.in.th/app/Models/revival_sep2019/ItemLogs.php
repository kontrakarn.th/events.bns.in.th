<?php

namespace App\Models\revival_sep2019;

use Illuminate\Database\Eloquent\Model;

class ItemLogs extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'revival_sep2019_item_logs';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'package_id',
        'package_title',
        'status',
        'send_item_status',
        'send_item_purchase_id',
        'send_item_purchase_status',
        'goods_data',
        'log_date',
        'log_date_timestamp',
        'last_ip'
    ];
    protected $dates = ['log_date'];

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->last_ip = \Request::ip();
        });
    }
}
