<?php

namespace App\Models\revival_sep2019;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClaimLogs extends Model
{
    use SoftDeletes;

    protected $connection = 'events_bns';
    protected $table = 'revival_sep2019_claim_logs';
    protected $fillable = [
        'member_id',
        'claim_id',
        'status', // unuse,used
        'last_ip',
    ];
    protected $dates = ['deleted_at'];

    protected static function boot(){
        parent::boot();

        static::saving(function($model){
            $model->last_ip = \Request::ip();
        });
    }
}
