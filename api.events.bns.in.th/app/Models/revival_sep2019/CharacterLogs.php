<?php

namespace App\Models\revival_sep2019;

use Illuminate\Database\Eloquent\Model;

class CharacterLogs extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'revival_sep2019_character_logs';
    protected $fillable = [
        'member_id',
        'name',
        'world_id',
        'mastery_level',
        'level',
        'last_play_start',
        'last_play_end',
        'last_ip',
        'job',
        'exp',
        'creation_time',
        'remark'
    ];
}
