<?php

    namespace App\Models\anniversary_passport;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'anniversary_passport_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'unlocked_passport',
            'last_ip',
        ];

    }