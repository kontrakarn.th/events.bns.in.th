<?php

    namespace App\Models\anniversary_passport;

    class Reward {

        public function getUnlockPackage(){
            return [
                'item_type' => 'unlock_package',
                'product_title' => '2nd Year Anniversary Package',
                'product_id' => 2269,
                'product_quantity' => 1,
                'amount' => 1,
            ];
        }

        public function getFinalReward(){
            return [
                [
                    'item_type' => 'final_reward',
                    'product_title' => 'หินสัตว์เลี้ยงจิ้งจอกอินาริ',
                    'product_id' => 2276,
                    'product_quantity' => 1,
                    'amount' => 1,
                ],
                [
                    'item_type' => 'final_reward',
                    'product_title' => 'ปีกผีเสื้อแสงดาวประกายชมพู',
                    'product_id' => 2279,
                    'product_quantity' => 1,
                    'amount' => 1,
                ],
            ];
        }

    }