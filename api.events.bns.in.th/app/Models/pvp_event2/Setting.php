<?php

namespace App\Models\pvp_event2;

use App\Models\BaseModel;

class Setting extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'pvp_event2_setting';

    public static function getSetting(){
        return self::orderBy("id","desc")->first();
    }
}
