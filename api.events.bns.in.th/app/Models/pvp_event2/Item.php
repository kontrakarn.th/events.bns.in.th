<?php

namespace App\Models\pvp_event2;

use App\Models\BaseModel;

class Item extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'pvp_event2_item';
}
