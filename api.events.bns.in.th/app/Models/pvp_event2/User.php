<?php

namespace App\Models\pvp_event2;

use App\Models\BaseModel;

class User extends BaseModel
{
    protected $connection   = 'events_bns';
    protected $table        = 'pvp_event2_user';
    public $timestamps      = true;
    const STATUS_FLAG_NONE      = 'NONE';
    const STATUS_FLAG_OK        = 'OK';
    const STATUS_FLAG_PENDING   = 'PENDING';
}
