<?php

    namespace App\Models\clan_event;

    use App\Models\BaseModel;

    class QuestDailyLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'clan_event_quest_daily_log';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'world_id',
            'clan_id',
            'quest_title',
            'quest_code',
            'quest_points',
            'quest_type',
            'completed_count',
            'total_points',
            'log_date',
            'log_date_timestamp',
            'last_ip'
        ];
    }

