<?php

    namespace App\Models\clan_event;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'clan_event_items_log';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'clan_id',
            'world_id',
            'package_id',
            'package_key',
            'package_name',
            'package_quantity',
            'package_amount',
            'package_type',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];
    }

