<?php

    namespace App\Models\clan_event;

    use App\Models\BaseModel;

    class QuestMemberLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'clan_event_quest_member_log';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'world_id',
            'clan_id',
            'quest_title',
            'quest_code',
            'quest_points',
            'quest_type', // quest, boss_kill
            'completed_count',
            'total_points',
            'last_ip'
        ];
    }

