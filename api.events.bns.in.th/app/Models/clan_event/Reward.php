<?php

    namespace App\Models\clan_event;

    use App\Models\BaseModel;

    class Reward extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'clan_event_rewards';
        protected $fillable = [
            'package_name',
            'package_id',
            'package_quantity',
            'package_amount',
            'package_type',
            'package_key',
        ];
    }

