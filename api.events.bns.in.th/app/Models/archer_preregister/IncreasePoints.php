<?php

    namespace App\Models\archer_preregister;

    use App\Models\BaseModel;

    class IncreasePoints extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'archer_preregister_increase_points';
        protected $fillable = [
            'points',
            'date_increase',
            'date_increase_timestamp',
        ];

    }