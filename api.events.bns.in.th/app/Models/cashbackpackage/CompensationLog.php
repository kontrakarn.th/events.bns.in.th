<?php

namespace App\Models\cashbackpackage;

use App\Models\BaseModel;

class CompensationLog extends BaseModel
{

    protected $connection = 'events_bns';
    protected $table = 'cashbackpackage_compensation_item_logs';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'week',
        'day',
        'package_name',
        'package_id',
        'package_quantity',
        'package_amount',
        'package_type',
        'package_key',
        'image',
        'send_item_status',
        'send_item_purchase_id',
        'send_item_purchase_status',
        'goods_data',
        'status',
        'log_date',
        'log_date_timestamp',
        'last_ip',
    ];
}
