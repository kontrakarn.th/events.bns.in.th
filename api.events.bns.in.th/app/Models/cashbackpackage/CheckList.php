<?php

namespace App\Models\cashbackpackage;

use App\Models\BaseModel;

class CheckList extends BaseModel
{

    protected $connection = 'events_bns';
    protected $table = 'cashbackpackage_checklist';
    protected $fillable = [
        'checkin_date',
        'week',
        'day',
        'package_key',
    ];
}
