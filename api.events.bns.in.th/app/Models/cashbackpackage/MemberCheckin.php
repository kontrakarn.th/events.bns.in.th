<?php

namespace App\Models\cashbackpackage;

use App\Models\BaseModel;

class MemberCheckin extends BaseModel
{

    protected $connection = 'events_bns';
    protected $table = 'cashbackpackage_member_checkin';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'checkin_date',
        'week',
        'day',
        'package_key',
        'is_checkin',
        'is_prev_checkin',
        'last_ip',
        'checkin_at'
    ];
}
