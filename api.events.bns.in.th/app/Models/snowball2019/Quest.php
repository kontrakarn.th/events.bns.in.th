<?php

    namespace App\Models\snowball2019;

    use App\Models\BaseModel;

    class Quest extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'snowball2019_quests';
        protected $fillable = [
            'quest_dungeon',
            'quest_title',
            'quest_code',
            'quest_points',
            'quest_type',
            'quest_limit',
            'image',
        ];
    }

