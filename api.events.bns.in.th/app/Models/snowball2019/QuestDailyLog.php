<?php

    namespace App\Models\snowball2019;

    use App\Models\BaseModel;

    class QuestDailyLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'snowball2019_quest_daily_log';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'quest_id',
            'quest_dungeon',
            'quest_title',
            'quest_code',
            'quest_points',
            'quest_type',
            'quest_limit',
            'image',
            'claimed_count',
            'completed_count',
            'log_date',
            'log_date_timestamp',
            'last_ip'
        ];
    }

