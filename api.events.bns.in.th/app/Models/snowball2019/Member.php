<?php

    namespace App\Models\snowball2019;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'snowball2019_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'world_id',
            'total_balls',
            'used_balls',
            'boss_kill_count',
            'exchange_count',
            'last_ip'
        ];
    }

