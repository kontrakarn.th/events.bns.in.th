<?php

    namespace App\Models\exchange_gift;

    use Moloquent;

    /**
     * Description of ItemHistory
     *
     * @author naruebaetbouhom
     */
    class Gift extends Moloquent {

        protected $connection = 'mongodb_bns';
        protected $table = 'exchange_gift_gift';

    }
