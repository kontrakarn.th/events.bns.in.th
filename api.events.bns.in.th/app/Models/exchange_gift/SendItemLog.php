<?php

    namespace App\Models\exchange_gift;

    use Moloquent;

    /**
     * Description of SendItemLog
     *
     * @author naruebaetbouhom
     */
    class SendItemLog extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'exchange_gift_send_item_log';
        protected $fillable = [
            'uid',
            'ncid',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'last_ip',
            'transaction_id',
            'log_timestamp'
        ];

    }
