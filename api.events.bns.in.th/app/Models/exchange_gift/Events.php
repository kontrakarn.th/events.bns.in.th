<?php

    namespace App\Models\exchange_gift;

    use Moloquent;

    /**
     * Description of ItemHistory
     *
     * @author naruebaetbouhom
     */
    class Events extends Moloquent {

        protected $connection = 'mongodb_bns';
        protected $table = 'bns_events';

        public static function getEventByKey($key_name){
            $data = self::where('event_key',$key_name)->first();
            return $data;
        }
    }
