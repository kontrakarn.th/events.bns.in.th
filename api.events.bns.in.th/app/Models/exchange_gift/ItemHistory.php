<?php

    namespace App\Models\exchange_gift;

    use Moloquent;

    /**
     * Description of ItemHistory
     *
     * @author naruebaetbouhom
     */
    class ItemHistory extends Moloquent {

        protected $connection = 'mongodb_bns';
        protected $table = 'exchange_gift_item_history';
        protected $fillable = [
            'product_id',
            'product_title',
            'product_quantity',
            'uid',
            'ncid',
            'status',
            'last_ip',
            'transaction_id',
            // 'product_pack_data',
            'log_timestamp',
            'log_time_string'
        ];

    }
