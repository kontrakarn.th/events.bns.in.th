<?php

    namespace App\Models\pvp_event;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'pvp_event_items_log';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'package_id',
            'package_key',
            'package_name',
            'package_quantity',
            'package_amount',
            'package_type',
            'points',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];
    }

