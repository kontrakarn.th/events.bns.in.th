<?php

    namespace App\Models\pvp_event;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'pvp_event_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'world_id',
            'total_points',
            'used_points',
            'last_ip'
        ];
    }

