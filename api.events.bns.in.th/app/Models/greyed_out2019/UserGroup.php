<?php

    namespace App\Models\greyed_out2019;

    use Moloquent;

    class UserGroup extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'greyed_out2019_user_groups';
        protected $fillable = [
            'uid',
            'group_id'
        ];
    }