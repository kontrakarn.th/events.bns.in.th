<?php

    namespace App\Models\greyed_out2019;

    use Moloquent;

    class Member extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'greyed_out2019_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'group_id',
            'selected_pool', // true, false => for check user hsa selected 2 items
            'pool_list', // ref #1
            'pool_round', // current pool round
            'pool_status', // true = completed, false = pending
            'created_at_timestamp',
            'created_at_string',
            'last_ip'
        ];
    }

#ref #1
/* array[
    [
        'product_key' => 1,
        'product_id'=> 1234,
        'product_title'=> 'Item A',
        'weight' => 1,
        'status' => false, // false = not_use, true = used
    ],
    [
        'product_key' => 2,
        'product_id'=> 123456,
        'product_title'=> 'Item B',
        'weight' => 10,
        'status' => false, // false = not_use, true = used
    ],
]; */