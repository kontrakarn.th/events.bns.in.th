<?php

    namespace App\Models\greyed_out2019;

    use Moloquent;

    class BnsEvent extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'bns_events';
        protected $fillable = [
            'event_key',
            'start_time',
            'end_time',
            'price_pool',
            'rewards_pool',
            'rewards_fixed',
            'groups_weight'
        ];
    }