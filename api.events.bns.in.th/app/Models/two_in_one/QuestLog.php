<?php

namespace App\Models\two_in_one;

use App\Models\BaseModel;

class QuestLog extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'two_in_one_quest_logs';
    protected $fillable = [
        "uid",
        "char_id",
        "quest_id",
        "completed_count",
        "claimed_count",
        "status",
        "claim_status",
        "log_date",
        "log_date_timestamp",
        "quest_complete_datetime",
        "last_ip",
    ];

    public function quest()
    {
        return $this->belongsTo(Quest::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'uid', 'uid');
    }
}
