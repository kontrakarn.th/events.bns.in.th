<?php

namespace App\Models\two_in_one;

use App\Models\BaseModel;

class Quest extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'two_in_one_quests';
    protected $fillable = [
        "quest_title",
        "quest_dungeon",
        "quest_code",
        "quest_type", // 'area','daily_challenge'
        "quest_limit",
        "xp",
        "quest_points",
        "img",
    ];
}
