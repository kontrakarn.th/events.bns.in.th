<?php

namespace App\Models\two_in_one;

use App\Models\BaseModel;

class Member extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'two_in_one_members';
    protected $fillable = [
        "uid",
        "username",
        "ncid",
        "char_id",
        "char_name",
        "world_id",
        "level",
        "mastery_level",
        "job",
        "job_name",
        "total_pvp_tokens",
        "used_pvp_tokens",
        "total_pve_tokens",
        "used_pve_tokens",
        "last_ip",
    ];

    public function getRemainingPvpTokensAttribute()
    {
        $totalPvpTokens = $this->total_pvp_tokens;
        $usedPvpTokens = $this->used_pvp_tokens;

        if ($totalPvpTokens < $usedPvpTokens) {
            return 0;
        }

        return $totalPvpTokens - $usedPvpTokens;
    }

    public function getRemainingPveTokensAttribute()
    {
        $totalPveTokens = $this->total_pve_tokens;
        $usedPveTokens = $this->used_pve_tokens;

        if ($totalPveTokens < $usedPveTokens) {
            return 0;
        }

        return $totalPveTokens - $usedPveTokens;
    }
}
