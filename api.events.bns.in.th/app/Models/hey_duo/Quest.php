<?php

namespace App\Models\hey_duo;

use App\Models\BaseModel;

class Quest extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'hey_duo_quests';
    protected $fillable = [
        'quest_dungeon',
        'quest_boss',
        'quest_code',
        'quest_id',
        'quest_type',
        'quest_limit',
        'quest_points',
        'image',
    ];
}
