<?php

namespace App\Models\hey_duo;

use App\Models\BaseModel;

class Reward extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'hey_duo_rewards';
    protected $fillable = [
        'package_name',
        'package_id',
        'package_quantity',
        'package_amount',
        'package_type', // diamonds, materials
        'rate',
        'available_date',
        'limit',
        'required_points',
        'required_diamonds',
    ];
}
