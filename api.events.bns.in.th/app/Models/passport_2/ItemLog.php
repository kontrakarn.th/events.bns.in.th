<?php

    namespace App\Models\passport_2;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {

        protected $connection = 'events_bns';
        protected $table = 'passport_2_send_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'week',
            'checkin_id',
            'item_type', // unlock_reward, checkin_reward, special_reward, final_reward
            'previous_checkin', // 0, 1
            'product_id',
            'product_title',
            'product_quantity',
            'amount',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
