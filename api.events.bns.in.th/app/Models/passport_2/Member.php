<?php

    namespace App\Models\passport_2;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'passport_2_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'unlocked_passport',
            'last_ip',
        ];

    }
