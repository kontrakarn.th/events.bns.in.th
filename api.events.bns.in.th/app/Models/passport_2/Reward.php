<?php

    namespace App\Models\passport_2;

    class Reward {

        public function getUnlockPackage(){
            return [
                'item_type' => 'unlock_package',
                'product_title' => 'แพ็คเกจหนังสือเดินทางต้อนรับปีใหม่',
                'product_id' => 2959,
                'product_quantity' => 1,
                'amount' => 1,
            ];
        }

        public function getFinalReward(){
            return [
                [
                    'item_type' => 'final_reward',
                    'product_title' => 'หินสัตว์เลี้ยงวิคตอเรีย และ ปีกทูตแห่งราคะ',
                    'product_id' => 2964,
                    'product_quantity' => 1,
                    'amount' => 1,
                ],
            ];
        }

    }
