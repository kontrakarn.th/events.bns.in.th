<?php

    namespace App\Models\package_des3rd;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'package_des3rd_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'discount_used',
            'last_ip',
            'created_at',
            'updated_at'
        ];

    }