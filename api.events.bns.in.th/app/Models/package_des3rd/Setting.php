<?php

    namespace App\Models\package_des3rd;

    use App\Models\BaseModel;

    class Setting extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'package_des3rd_setting';
        protected $fillable = [
            'event_start',
            'event_end',
            'active',
        ];

    }