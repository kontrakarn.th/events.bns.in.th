<?php

    namespace App\Models\hm_battlepass;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'hm_battlepass_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'quest_log_id',
            'package_id',
            'package_title',
            'amount',
            'reward_key',
            'package_type', // paid, free
            'status',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'log_date',
            'log_date_timestamp',
            'last_ip'
        ];

    }