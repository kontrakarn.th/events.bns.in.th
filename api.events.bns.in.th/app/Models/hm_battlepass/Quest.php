<?php

    namespace App\Models\hm_battlepass;

    class Quest {

        public function setQuestByStep($step=0){

            if(empty($step)){
                return false;
            }

            $specialQuests = $this->specialQuestsList();

            $stepKey = array_search($step, array_column($specialQuests, 'step'));

            return (isset($specialQuests[$stepKey]) && !empty($specialQuests[$stepKey])) ? $specialQuests[$stepKey] : false;

        }

        public function setSpecialQuestById($questId=0){

            if(empty($questId)){
                return false;
            }

            $quests = $this->specialQuestsList();

            $questKey = array_search($questId, array_column($quests, 'quest_id'));

            return (isset($quests[$questKey]) && !empty($quests[$questKey])) ? $quests[$questKey] : false;

        }

        private function specialQuestsList(){
            return [
                [
                    'quest_id' => 61,
                    'step' => 5,
                    'quest_code' => 38,
                    'quest_title' => 'โจมตีชูบาโร่จอมพลังด้วยบอลหิมะขนาดเล็ก',
                    'count_required' => 300,
                    'max_count' => 300,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'หมวกนักปราชญ์ผู้ปราดเปรียว',
                            'product_id' => 1894,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'sage_cap',
                        ],
                        'free' => [
                            'product_title' => 'แว่นกันแดดทุ่งหิมะ',
                            'product_id' => 1901,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'snowfield_sunglasses',
                        ]
                    ]
                ],
                [
                    'quest_id' => 62,
                    'step' => 10,
                    'quest_code' => 0,
                    'quest_title' => 'ทำ Weekly challenge',
                    'count_required' => 1, // completed 3 sub quests
                    'max_count' => 0,
                    'quest_type' => 'weekly',
                    'has_sub_quest' => true,
                    'sub_quest' => [
                        [
                            'quest_code' => 1530,
                            'quest_title' => 'การเจอกันของคู่ต่อสู้ของมังกรเงินหลับใหล',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1662,
                            'quest_title' => 'สถานที่ศักดิ์สิทธิ์ที่ไม่หลับใหล',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1664,
                            'quest_title' => 'ไฟที่ไม่มอดดับ',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1607,
                            'quest_title' => 'สามมงกุฎแห่งหุบเขาลมหวน',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1628,
                            'quest_title' => 'มงกุฏ 3 แฉกของเบลูก้าลากูน',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                    ],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'แว่นนักปราชญ์ผู้ปราดเปรียว',
                            'product_id' => 1895,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'sage_glasses',
                        ],
                        'free' => [
                            'product_title' => 'ที่คาดผมขวานทองแดง',
                            'product_id' => 1907,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'copper_axe_headband',
                        ]
                    ]
                ],
                [
                    'quest_id' => 63,
                    'step' => 15,
                    'quest_code' => 25172,
                    'quest_title' => 'ผ่านดันเจี้ยน เมืองโบราณพรรณไม้',
                    'count_required' => 10,
                    'max_count' => 0,
                    'quest_type' => 'quest_completed_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'คริสตัลวิญญาณฮงมุน',
                            'product_id' => 1896,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'hongmoon_crystal',
                        ],
                        'free' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1902,
                            'product_quantity' => 1,
                            'amount' => 500,
                            'reward_key' => 'hongmoon_medal',
                        ]
                    ]
                ],
                [
                    'quest_id' => 64,
                    'step' => 20,
                    'quest_code' => 0,
                    'quest_title' => 'ทำ daily challenge ',
                    'count_required' => 3, // completed 3 sub quests for 3 days
                    'max_count' => 0,
                    'quest_type' => 'daily',
                    'has_sub_quest' => true,
                    'sub_quest' => [
                        [
                            'quest_code' => 1497,
                            'quest_title' => 'กุหลาบจากอดีตกาล',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1252,
                            'quest_title' => 'ยามเหมันต์จบลง',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1219,
                            'quest_title' => 'สยบทะเลคลั่ง',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1064,
                            'quest_title' => 'ดาบของจอมดาบเดี่ยวมรณะ',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1478,
                            'quest_title' => 'เสียงหอนของเตาหลอม',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1614,
                            'quest_title' => 'เสียงเพรียกของสายลม',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1674,
                            'quest_title' => 'หุบเขาเหมันต์',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1350,
                            'quest_title' => 'ชนะแท็กแมทช์',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 414,
                            'quest_title' => 'ตัวต่อตัว',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 483,
                            'quest_title' => 'ภาพหลอนอันเหน็บหนาว',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1212,
                            'quest_title' => 'บ่อเกิดแห่งหายนะ',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 299,
                            'quest_title' => 'พื้นที่ปิดผนึก',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1365,
                            'quest_title' => 'พันธะที่ผูกเราไว้',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1528,
                            'quest_title' => 'ผู้บุกรุกที่ศาลนักพรตนาริว',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1528,
                            'quest_title' => 'ผู้บุกรุกที่ศาลนักพรตนาริว',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1623,
                            'quest_title' => 'รุกฆาต',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1672,
                            'quest_title' => 'ผู้พลัดถิ่นแห่งเมืองโบราณพรรณไม้',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1697,
                            'quest_title' => 'เสียงเพลงอันโหยหวน',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1735,
                            'quest_title' => 'ฝันร้ายแห่งกรุสมบัติ',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                        [
                            'quest_code' => 1784,
                            'quest_title' => 'เปลวไฟแห่งความลำบาก',
                            'count_required' => 1,
                            'max_count' => 0,
                        ],
                    ],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'อาวุธลวงตานาฬิกาจักรกล',
                            'product_id' => 1897,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'clock_weapon_skin',
                        ],
                        'free' => [
                            'product_title' => 'กล่องอาวุธลวงตาเขี้ยวพยัคฆ์',
                            'product_id' => 1908,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'tiger_fang_weapon_skin',
                        ]
                    ]
                ],
                [
                    'quest_id' => 65,
                    'step' => 25,
                    'quest_code' => 0,
                    'quest_title' => 'สังหาร (ลิน, จิน,กอน ยุน)',
                    'count_required' => 0,
                    'max_count' => 100,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => true,
                    'sub_quest' => [
                        [
                            'quest_code' => 226,
                            'quest_title' => 'สังหาร จิน',
                            'count_required' => 10,
                            'max_count' => 100,
                        ],
                        [
                            'quest_code' => 227,
                            'quest_title' => 'สังหาร กอน',
                            'count_required' => 10,
                            'max_count' => 100,
                        ],
                        [
                            'quest_code' => 228,
                            'quest_title' => 'สังหาร ลิน',
                            'count_required' => 10,
                            'max_count' => 100,
                        ],
                        [
                            'quest_code' => 229,
                            'quest_title' => 'สังหาร ยุน',
                            'count_required' => 10,
                            'max_count' => 100,
                        ],
                    ],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'หินสัตว์เลี้ยงนกเค้าแมว',
                            'product_id' => 1898,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'owl_pet',
                        ],
                        'free' => [
                            'product_title' => 'ลูกแก้วดาวพุธ ขั้น 1',
                            'product_id' => 1903,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'mercury_orb_stage1',
                        ]
                    ]
                ],
                [
                    'quest_id' => 66,
                    'step' => 30,
                    'quest_code' => 174,
                    'quest_title' => 'ใช้มังกรสวรรค์',
                    'count_required' => 100,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ลูกแก้วดาวศุกร์ ขั้น 1',
                            'product_id' => 1899,
                            'product_quantity' => 1,
                            'amount' => 4,
                            'reward_key' => 'venus_orb_stage1',
                        ],
                        'free' => [
                            'product_title' => 'ลูกแก้วดาวพฤหัส ขั้น 1',
                            'product_id' => 1904,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'jupiter_orb_stage1',
                        ]
                    ]
                ],
                [
                    'quest_id' => 67,
                    'step' => 35,
                    'quest_code' => 289,
                    'quest_title' => 'ฆ่าเยติ',
                    'count_required' => 50,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ชุดนักปราชญ์ผู้ปราดเปรียว',
                            'product_id' => 1893,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'sage_costume',
                        ],
                        'free' => [
                            'product_title' => 'ชุดสุดระยิบระยับ',
                            'product_id' => 1905,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'glittering_costume',
                        ]
                    ]
                ],
                [
                    'quest_id' => 68,
                    'step' => 40,
                    'quest_code' => 0,
                    'quest_title' => 'ชนะ PVP 1v1 และ 3v3',
                    'count_required' => 0,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => true,
                    'sub_quest' => [
                        [
                            'quest_code' => 260,
                            'quest_title' => 'ชนะ PVP 1v1',
                            'count_required' => 5,
                            'max_count' => 5000,
                        ],
                        [
                            'quest_code' => 261,
                            'quest_title' => 'ชนะ PVP 3v3',
                            'count_required' => 5,
                            'max_count' => 5000,
                        ],
                    ],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'คริสตัลอัญมณีหยินหยาง',
                            'product_id' => 1900,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'yin_yang_crystal',
                        ],
                        'free' => [
                            'product_title' => 'ถุงหินสัตว์เลี้ยงสุภาพบุรุษ',
                            'product_id' => 1906,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'gentleman_pet_bag',
                        ]
                    ]
                ],
            ];
        }


        // random quest
        public function getRandomQuest(){

            $quests = $this->questsList();

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $quest = false;

            foreach ($quests as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired quest
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    // $value['color'] = $this->randomColor();
                    $quest = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $quest['random'] = $random;

            $quest['timestamp'] = time();

            // now we have the quest
            return $quest;
        }

        public function setQuestById($questId=0){

            if(empty($questId)){
                return false;
            }

            $quests = $this->questsList();

            $questKey = array_search($questId, array_column($quests, 'quest_id'));

            return (isset($quests[$questKey]) && !empty($quests[$questKey])) ? $quests[$questKey] : false;

        }

        private function questsList(){
            return [
                [
                    'chance' => 2.000,
                    'quest_id' => 1,
                    'quest_code' => 275,
                    'quest_title' => 'กินยา 200',
                    'count_required' => 200,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'คริสตัลหินจันทรา',
                            'product_id' => 1847,
                            'product_quantity' => 1,
                            'amount' => 30,
                            'reward_key' => 'moonstone_crystal',
                        ],
                        'free' => [
                            'product_title' => 'คริสตัลหินจันทรา',
                            'product_id' => 1845,
                            'product_quantity' => 1,
                            'amount' => 15,
                            'reward_key' => 'moonstone_crystal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 2,
                    'quest_code' => 275,
                    'quest_title' => 'กินยา 100',
                    'count_required' => 100,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'คริสตัลหินจันทรา',
                            'product_id' => 1846,
                            'product_quantity' => 1,
                            'amount' => 20,
                            'reward_key' => 'moonstone_crystal',
                        ],
                        'free' => [
                            'product_title' => 'คริสตัลหินจันทรา',
                            'product_id' => 477,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'moonstone_crystal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 3,
                    'quest_code' => 275,
                    'quest_title' => 'กินยา 200',
                    'count_required' => 200,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1854,
                            'product_quantity' => 1,
                            'amount' => 4,
                            'reward_key' => 'blackshark',
                        ],
                        'free' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1852,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'blackshark',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 4,
                    'quest_code' => 275,
                    'quest_title' => 'กินยา 100',
                    'count_required' => 100,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1852,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'blackshark',
                        ],
                        'free' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1150,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'blackshark',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'achievement_step' => 1,
                    'quest_id' => 5,
                    'quest_code' => 8,
                    'quest_title' => 'ว่ายน้ำ 3 นาที',
                    'count_required' => 0,
                    'max_count' => 0,
                    'quest_type' => 'achievement_completed',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'คริสตัลหินจันทรา',
                            'product_id' => 477,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'moonstone_crystal',
                        ],
                        'free' => [
                            'product_title' => 'คริสตัลหินจันทรา',
                            'product_id' => 1116,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'moonstone_crystal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'achievement_step' => 1,
                    'quest_id' => 6,
                    'quest_code' => 5,
                    'quest_title' => 'ถึงฮงมุน ขั้น 10',
                    'count_required' => 0,
                    'max_count' => 0,
                    'quest_type' => 'achievement_completed',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'คริสตัลหินจันทรา',
                            'product_id' => 477,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'moonstone_crystal',
                        ],
                        'free' => [
                            'product_title' => 'คริสตัลหินจันทรา',
                            'product_id' => 1116,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'moonstone_crystal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 7,
                    'quest_code' => 266,
                    'quest_title' => 'ชุบชีวิตเพื่อน',
                    'count_required' => 50,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'คริสตัลหินโซล',
                            'product_id' => 586,
                            'product_quantity' => 1,
                            'amount' => 30,
                            'reward_key' => 'soulstone_crystal',
                        ],
                        'free' => [
                            'product_title' => 'คริสตัลหินโซล',
                            'product_id' => 1115,
                            'product_quantity' => 1,
                            'amount' => 15,
                            'reward_key' => 'soulstone_crystal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 8,
                    'quest_code' => 266,
                    'quest_title' => 'ชุบชีวิตเพื่อน',
                    'count_required' => 20,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'คริสตัลหินโซล',
                            'product_id' => 1849,
                            'product_quantity' => 1,
                            'amount' => 20,
                            'reward_key' => 'soulstone_crystal',
                        ],
                        'free' => [
                            'product_title' => 'คริสตัลหินโซล',
                            'product_id' => 1848,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'soulstone_crystal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 9,
                    'quest_code' => 195,
                    'quest_title' => 'บวกโซลชิลด์ให้ได้ค่ามากสุด',
                    'count_required' => 10,
                    'max_count' => 500,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1880,
                            'product_quantity' => 1,
                            'amount' => 40,
                            'reward_key' => 'hongmoon_medal',
                        ],
                        'free' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1878,
                            'product_quantity' => 1,
                            'amount' => 20,
                            'reward_key' => 'hongmoon_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 10,
                    'quest_code' => 195,
                    'quest_title' => 'บวกโซลชิลด์ให้ได้ค่ามากสุด',
                    'count_required' => 20,
                    'max_count' => 500,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1882,
                            'product_quantity' => 1,
                            'amount' => 60,
                            'reward_key' => 'hongmoon_medal',
                        ],
                        'free' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1879,
                            'product_quantity' => 1,
                            'amount' => 30,
                            'reward_key' => 'hongmoon_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 11,
                    'quest_code' => 5,
                    'quest_title' => 'ตกปลาที่ฟาร์มเวหา',
                    'count_required' => 10,
                    'max_count' => 10000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1880,
                            'product_quantity' => 1,
                            'amount' => 40,
                            'reward_key' => 'hongmoon_medal',
                        ],
                        'free' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1878,
                            'product_quantity' => 1,
                            'amount' => 20,
                            'reward_key' => 'hongmoon_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 12,
                    'quest_code' => 5,
                    'quest_title' => 'ตกปลาที่ฟาร์มเวหา',
                    'count_required' => 20,
                    'max_count' => 10000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1882,
                            'product_quantity' => 1,
                            'amount' => 60,
                            'reward_key' => 'hongmoon_medal',
                        ],
                        'free' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1879,
                            'product_quantity' => 1,
                            'amount' => 30,
                            'reward_key' => 'hongmoon_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 13,
                    'quest_code' => 5,
                    'quest_title' => 'ตกปลาที่ฟาร์มเวหา',
                    'count_required' => 30,
                    'max_count' => 10000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1883,
                            'product_quantity' => 1,
                            'amount' => 80,
                            'reward_key' => 'hongmoon_medal',
                        ],
                        'free' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1880,
                            'product_quantity' => 1,
                            'amount' => 40,
                            'reward_key' => 'hongmoon_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 14,
                    'quest_code' => 5,
                    'quest_title' => 'ตกปลาที่ฟาร์มเวหา',
                    'count_required' => 40,
                    'max_count' => 10000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1884,
                            'product_quantity' => 1,
                            'amount' => 100,
                            'reward_key' => 'hongmoon_medal',
                        ],
                        'free' => [
                            'product_title' => 'เหรียญสัญลักษณ์ฮงมุน',
                            'product_id' => 1881,
                            'product_quantity' => 1,
                            'amount' => 50,
                            'reward_key' => 'hongmoon_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 15,
                    'quest_code' => 265,
                    'quest_title' => 'ช่วยเพื่อนฟื้นฟูโซล',
                    'count_required' => 10,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ผงธาตุ',
                            'product_id' => 1860,
                            'product_quantity' => 1,
                            'amount' => 20,
                            'reward_key' => 'element_powder',
                        ],
                        'free' => [
                            'product_title' => 'ผงธาตุ',
                            'product_id' => 1648,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'element_powder',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 16,
                    'quest_code' => 265,
                    'quest_title' => 'ช่วยเพื่อนฟื้นฟูโซล',
                    'count_required' => 5,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ผงธาตุ',
                            'product_id' => 1648,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'element_powder',
                        ],
                        'free' => [
                            'product_title' => 'ผงธาตุ',
                            'product_id' => 1861,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'element_powder',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 17,
                    'quest_code' => 194,
                    'quest_title' => 'บวกโซลชิลด์',
                    'count_required' => 10,
                    'max_count' => 500,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ยาโชคชะตาขนาดใหญ่',
                            'product_id' => 1863,
                            'product_quantity' => 1,
                            'amount' => 30,
                            'reward_key' => 'greatest_lucky_potion',
                        ],
                        'free' => [
                            'product_title' => 'ยาโชคชะตาขนาดใหญ่',
                            'product_id' => 1862,
                            'product_quantity' => 1,
                            'amount' => 15,
                            'reward_key' => 'greatest_lucky_potion',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 18,
                    'quest_code' => 194,
                    'quest_title' => 'บวกโซลชิลด์',
                    'count_required' => 20,
                    'max_count' => 500,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ยาโชคชะตาขนาดใหญ่',
                            'product_id' => 1864,
                            'product_quantity' => 1,
                            'amount' => 60,
                            'reward_key' => 'greatest_lucky_potion',
                        ],
                        'free' => [
                            'product_title' => 'ยาโชคชะตาขนาดใหญ่',
                            'product_id' => 1863,
                            'product_quantity' => 1,
                            'amount' => 30,
                            'reward_key' => 'greatest_lucky_potion',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 19,
                    'quest_code' => 222,
                    'quest_title' => 'สังหารผู้เล่นอื่น',
                    'count_required' => 10,
                    'max_count' => 10000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1852,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'blackshark',
                        ],
                        'free' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1150,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'blackshark',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 20,
                    'quest_code' => 222,
                    'quest_title' => 'สังหารผู้เล่นอื่น',
                    'count_required' => 20,
                    'max_count' => 10000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1853,
                            'product_quantity' => 1,
                            'amount' => 3,
                            'reward_key' => 'blackshark',
                        ],
                        'free' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1852,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'blackshark',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 21,
                    'quest_code' => 195,
                    'quest_title' => 'บวกโซลชิลด์ให้ได้ค่ามากสุด',
                    'count_required' => 10,
                    'max_count' => 500,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1874,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'cat_pirate_medal',
                        ],
                        'free' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1873,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'cat_pirate_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 22,
                    'quest_code' => 195,
                    'quest_title' => 'บวกโซลชิลด์ให้ได้ค่ามากสุด',
                    'count_required' => 15,
                    'max_count' => 500,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1875,
                            'product_quantity' => 1,
                            'amount' => 3,
                            'reward_key' => 'cat_pirate_medal',
                        ],
                        'free' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1874,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'cat_pirate_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 23,
                    'quest_code' => 260,
                    'quest_title' => 'ชนะ PVP 1vs1',
                    'count_required' => 20,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1876,
                            'product_quantity' => 1,
                            'amount' => 4,
                            'reward_key' => 'cat_pirate_medal',
                        ],
                        'free' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1874,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'cat_pirate_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 24,
                    'quest_code' => 260,
                    'quest_title' => 'ชนะ PVP 1vs1',
                    'count_required' => 10,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1874,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'cat_pirate_medal',
                        ],
                        'free' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1873,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'cat_pirate_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 25,
                    'quest_code' => 193,
                    'quest_title' => 'ซ่อมอาวุธ',
                    'count_required' => 20,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ขนอีกา',
                            'product_id' => 1844,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'raven_feather',
                        ],
                        'free' => [
                            'product_title' => 'ขนอีกา',
                            'product_id' => 1842,
                            'product_quantity' => 1,
                            'amount' => 6,
                            'reward_key' => 'raven_feather',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 26,
                    'quest_code' => 193,
                    'quest_title' => 'ซ่อมอาวุธ',
                    'count_required' => 10,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ขนอีกา',
                            'product_id' => 1842,
                            'product_quantity' => 1,
                            'amount' => 6,
                            'reward_key' => 'raven_feather',
                        ],
                        'free' => [
                            'product_title' => 'ขนอีกา',
                            'product_id' => 1840,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'raven_feather',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 27,
                    'quest_code' => 193,
                    'quest_title' => 'ซ่อมอาวุธ',
                    'count_required' => 5,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ขนอีกา',
                            'product_id' => 1841,
                            'product_quantity' => 1,
                            'amount' => 4,
                            'reward_key' => 'raven_feather',
                        ],
                        'free' => [
                            'product_title' => 'ขนอีกา',
                            'product_id' => 1840,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'raven_feather',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 28,
                    'quest_code' => 193,
                    'quest_title' => 'ซ่อมอาวุธ',
                    'count_required' => 15,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ขนอีกา',
                            'product_id' => 1843,
                            'product_quantity' => 1,
                            'amount' => 8,
                            'reward_key' => 'raven_feather',
                        ],
                        'free' => [
                            'product_title' => 'ขนอีกา',
                            'product_id' => 1465,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'raven_feather',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 29,
                    'quest_code' => 192,
                    'quest_title' => 'อาวุธแตกเหลือ 0 แต้ม',
                    'count_required' => 5,
                    'max_count' => 500,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'หินล้ำค่าหายาก',
                            'product_id' => 1885,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'precious_stones',
                        ],
                        'free' => [
                            'product_title' => 'หินล้ำค่าหายาก x5',
                            'product_id' => 1887,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'precious_stones',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 30,
                    'quest_code' => 192,
                    'quest_title' => 'อาวุธแตกเหลือ 0 แต้ม',
                    'count_required' => 10,
                    'max_count' => 500,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'หินล้ำค่าหายาก',
                            'product_id' => 1886,
                            'product_quantity' => 1,
                            'amount' => 15,
                            'reward_key' => 'precious_stones',
                        ],
                        'free' => [
                            'product_title' => 'หินล้ำค่าหายาก',
                            'product_id' => 1888,
                            'product_quantity' => 1,
                            'amount' => 8,
                            'reward_key' => 'precious_stones',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'achievement_step' => 1,
                    'quest_id' => 31,
                    'quest_code' => 6,
                    'quest_title' => 'ลอยตัว',
                    'count_required' => 0,
                    'max_count' => 0,
                    'quest_type' => 'achievement_completed',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'หินล้ำค่าหายาก',
                            'product_id' => 1890,
                            'product_quantity' => 1,
                            'amount' => 6,
                            'reward_key' => 'precious_stones',
                        ],
                        'free' => [
                            'product_title' => 'หินล้ำค่าหายาก',
                            'product_id' => 1889,
                            'product_quantity' => 1,
                            'amount' => 3,
                            'reward_key' => 'precious_stones',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'achievement_step' => 1,
                    'quest_id' => 32,
                    'quest_code' => 7,
                    'quest_title' => 'ดิ่งลงพื้น',
                    'count_required' => 0,
                    'max_count' => 0,
                    'quest_type' => 'achievement_completed',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'หินล้ำค่าหายาก',
                            'product_id' => 1890,
                            'product_quantity' => 1,
                            'amount' => 6,
                            'reward_key' => 'precious_stones',
                        ],
                        'free' => [
                            'product_title' => 'หินล้ำค่าหายาก',
                            'product_id' => 1889,
                            'product_quantity' => 1,
                            'amount' => 3,
                            'reward_key' => 'precious_stones',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'achievement_step' => 6,
                    'quest_id' => 33,
                    'quest_code' => 4,
                    'quest_title' => 'ถึงเวล 55',
                    'count_required' => 0,
                    'max_count' => 0,
                    'quest_type' => 'achievement_completed',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1603,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'blackshark',
                        ],
                        'free' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1855,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'blackshark',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 34,
                    'quest_code' => 189,
                    'quest_title' => 'ทำลายโซลชิลด์ที่ปลดผนึกแล้ว',
                    'count_required' => 10,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1856,
                            'product_quantity' => 1,
                            'amount' => 6,
                            'reward_key' => 'blackshark',
                        ],
                        'free' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1853,
                            'product_quantity' => 1,
                            'amount' => 3,
                            'reward_key' => 'blackshark',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 35,
                    'quest_code' => 189,
                    'quest_title' => 'ทำลายโซลชิลด์ที่ปลดผนึกแล้ว',
                    'count_required' => 15,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1603,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'blackshark',
                        ],
                        'free' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1855,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'blackshark',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 36,
                    'quest_code' => 0,
                    'quest_title' => 'ฆ่ามอนเตอร์',
                    'count_required' => 5000,
                    'max_count' => 0,
                    'quest_type' => 'monster_kill',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'วิญญาณพญากาดำ',
                            'product_id' => 1869,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'raven_king_soul',
                        ],
                        'free' => [
                            'product_title' => 'วิญญาณพญากาดำ',
                            'product_id' => 1867,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'raven_king_soul',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 37,
                    'quest_code' => 0,
                    'quest_title' => 'ฆ่ามอนเตอร์',
                    'count_required' => 3000,
                    'max_count' => 0,
                    'quest_type' => 'monster_kill',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'วิญญาณพญากาดำ',
                            'product_id' => 1868,
                            'product_quantity' => 1,
                            'amount' => 8,
                            'reward_key' => 'raven_king_soul',
                        ],
                        'free' => [
                            'product_title' => 'วิญญาณพญากาดำ',
                            'product_id' => 1866,
                            'product_quantity' => 1,
                            'amount' => 4,
                            'reward_key' => 'raven_king_soul',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 38,
                    'quest_code' => 0,
                    'quest_title' => 'ฆ่ามอนเตอร์',
                    'count_required' => 2000,
                    'max_count' => 0,
                    'quest_type' => 'monster_kill',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'วิญญาณพญากาดำ',
                            'product_id' => 1866,
                            'product_quantity' => 1,
                            'amount' => 4,
                            'reward_key' => 'raven_king_soul',
                        ],
                        'free' => [
                            'product_title' => 'วิญญาณพญากาดำ',
                            'product_id' => 1865,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'raven_king_soul',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 39,
                    'quest_code' => 242,
                    'quest_title' => 'ถูกสังหาร',
                    'count_required' => 10,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1874,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'cat_pirate_medal',
                        ],
                        'free' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1873,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'cat_pirate_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 2.000,
                    'quest_id' => 40,
                    'quest_code' => 242,
                    'quest_title' => 'ถูกสังหาร',
                    'count_required' => 20,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1876,
                            'product_quantity' => 1,
                            'amount' => 4,
                            'reward_key' => 'cat_pirate_medal',
                        ],
                        'free' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1874,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'cat_pirate_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 1.440,
                    'quest_id' => 41,
                    'quest_code' => 222,
                    'quest_title' => 'สังหารผู้เล่นอื่น',
                    'count_required' => 50,
                    'max_count' => 10000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ปีกวายุทมิฬ',
                            'product_id' => 1859,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'hive_queen_wing',
                        ],
                        'free' => [
                            'product_title' => 'ปีกวายุทมิฬ',
                            'product_id' => 1601,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'hive_queen_wing',
                        ]
                    ]
                ],
                [
                    'chance' => 1.440,
                    'quest_id' => 42,
                    'quest_code' => 222,
                    'quest_title' => 'สังหารผู้เล่นอื่น',
                    'count_required' => 30,
                    'max_count' => 10000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ปีกวายุทมิฬ',
                            'product_id' => 1858,
                            'product_quantity' => 1,
                            'amount' => 8,
                            'reward_key' => 'hive_queen_wing',
                        ],
                        'free' => [
                            'product_title' => 'ปีกวายุทมิฬ',
                            'product_id' => 1725,
                            'product_quantity' => 1,
                            'amount' => 3,
                            'reward_key' => 'hive_queen_wing',
                        ]
                    ]
                ],
                [
                    'chance' => 1.440,
                    'quest_id' => 43,
                    'quest_code' => 222,
                    'quest_title' => 'สังหารผู้เล่นอื่น',
                    'count_required' => 20,
                    'max_count' => 10000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ปีกวายุทมิฬ',
                            'product_id' => 1601,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'hive_queen_wing',
                        ],
                        'free' => [
                            'product_title' => 'ปีกวายุทมิฬ',
                            'product_id' => 1617,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'hive_queen_wing',
                        ]
                    ]
                ],
                [
                    'chance' => 1.420,
                    'quest_id' => 44,
                    'quest_code' => 0,
                    'quest_title' => 'สังหารผู้เล่นฝ่ายตรงข้าม',
                    'count_required' => 50,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => true,
                    'sub_quest' => [
                        [
                            'quest_code' => 223,
                            'quest_title' => 'สังหารผู้เล่นฝ่ายตรงข้าม(1)',
                            'count_required' => 50,
                            'max_count' => 1000,
                        ],
                        [
                            'quest_code' => 224,
                            'quest_title' => 'สังหารผู้เล่นฝ่ายตรงข้าม(2)',
                            'count_required' => 50,
                            'max_count' => 1000,
                        ],
                    ],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'วิญญาณพายุทมิฬ',
                            'product_id' => 1872,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'hive_queen_soul',
                        ],
                        'free' => [
                            'product_title' => 'วิญญาณพายุทมิฬ',
                            'product_id' => 1609,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'hive_queen_soul',
                        ]
                    ]
                ],
                [
                    'chance' => 1.420,
                    'quest_id' => 45,
                    'quest_code' => 0,
                    'quest_title' => 'สังหารผู้เล่นฝ่ายตรงข้าม',
                    'count_required' => 30,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => true,
                    'sub_quest' => [
                        [
                            'quest_code' => 223,
                            'quest_title' => 'สังหารผู้เล่นฝ่ายตรงข้าม(1)',
                            'count_required' => 30,
                            'max_count' => 1000,
                        ],
                        [
                            'quest_code' => 224,
                            'quest_title' => 'สังหารผู้เล่นฝ่ายตรงข้าม(2)',
                            'count_required' => 30,
                            'max_count' => 1000,
                        ],
                    ],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'วิญญาณพายุทมิฬ',
                            'product_id' => 1871,
                            'product_quantity' => 1,
                            'amount' => 8,
                            'reward_key' => 'hive_queen_soul',
                        ],
                        'free' => [
                            'product_title' => 'วิญญาณพายุทมิฬ',
                            'product_id' => 1870,
                            'product_quantity' => 1,
                            'amount' => 3,
                            'reward_key' => 'hive_queen_soul',
                        ]
                    ]
                ],
                [
                    'chance' => 1.420,
                    'quest_id' => 46,
                    'quest_code' => 0,
                    'quest_title' => 'ฆ่ามอนเตอร์',
                    'count_required' => 7000,
                    'max_count' => 0,
                    'quest_type' => 'monster_kill',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1876,
                            'product_quantity' => 1,
                            'amount' => 4,
                            'reward_key' => 'cat_pirate_medal',
                        ],
                        'free' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1874,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'cat_pirate_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 1.420,
                    'quest_id' => 47,
                    'quest_code' => 0,
                    'quest_title' => 'ฆ่ามอนเตอร์',
                    'count_required' => 8000,
                    'max_count' => 0,
                    'quest_type' => 'monster_kill',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1877,
                            'product_quantity' => 1,
                            'amount' => 6,
                            'reward_key' => 'cat_pirate_medal',
                        ],
                        'free' => [
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                            'product_id' => 1875,
                            'product_quantity' => 1,
                            'amount' => 3,
                            'reward_key' => 'cat_pirate_medal',
                        ]
                    ]
                ],
                [
                    'chance' => 1.440,
                    'quest_id' => 48,
                    'quest_code' => 242,
                    'quest_title' => 'ถูกสังหาร',
                    'count_required' => 30,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 1',
                            'product_id' => 1837,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'hive_queen_soulshield_box_no1',
                        ],
                        'free' => [
                            'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 1',
                            'product_id' => 1716,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'hive_queen_soulshield_box_no1',
                        ]
                    ]
                ],
                [
                    'chance' => 1.440,
                    'quest_id' => 49,
                    'quest_code' => 242,
                    'quest_title' => 'ถูกสังหาร',
                    'count_required' => 40,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 2',
                            'product_id' => 1838,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'hive_queen_soulshield_box_no2',
                        ],
                        'free' => [
                            'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 2',
                            'product_id' => 1717,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'hive_queen_soulshield_box_no2',
                        ]
                    ]
                ],
                [
                    'chance' => 1.440,
                    'quest_id' => 50,
                    'quest_code' => 242,
                    'quest_title' => 'ถูกสังหาร',
                    'count_required' => 50,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 3',
                            'product_id' => 1839,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'hive_queen_soulshield_box_no3',
                        ],
                        'free' => [
                            'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 3',
                            'product_id' => 1718,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'hive_queen_soulshield_box_no3',
                        ]
                    ]
                ],
                [
                    'chance' => 1.420,
                    'quest_id' => 51,
                    'quest_code' => 193,
                    'quest_title' => 'ซ่อมอาวุธ',
                    'count_required' => 30,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ผงธาตุ',
                            'product_id' => 1860,
                            'product_quantity' => 1,
                            'amount' => 20,
                            'reward_key' => 'element_powder',
                        ],
                        'free' => [
                            'product_title' => 'ผงธาตุ',
                            'product_id' => 1648,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'element_powder',
                        ]
                    ]
                ],
                [
                    'chance' => 1.420,
                    'quest_id' => 52,
                    'quest_code' => 193,
                    'quest_title' => 'ซ่อมอาวุธ',
                    'count_required' => 40,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ผงธาตุ',
                            'product_id' => 1648,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'element_powder',
                        ],
                        'free' => [
                            'product_title' => 'ผงธาตุ x5',
                            'product_id' => 1861,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'element_powder',
                        ]
                    ]
                ],
                [
                    'chance' => 1.420,
                    'quest_id' => 53,
                    'quest_code' => 192,
                    'quest_title' => 'อาวุธแตกเหลือ 0 แต้ม',
                    'count_required' => 5,
                    'max_count' => 500,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ผงธาตุ',
                            'product_id' => 1860,
                            'product_quantity' => 1,
                            'amount' => 20,
                            'reward_key' => 'element_powder',
                        ],
                        'free' => [
                            'product_title' => 'ผงธาตุ',
                            'product_id' => 1648,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'element_powder',
                        ]
                    ]
                ],
                [
                    'chance' => 1.420,
                    'quest_id' => 54,
                    'quest_code' => 192,
                    'quest_title' => 'อาวุธแตกเหลือ 0 แต้ม',
                    'count_required' => 10,
                    'max_count' => 500,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ผงธาตุ',
                            'product_id' => 1648,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'element_powder',
                        ],
                        'free' => [
                            'product_title' => 'ผงธาตุ',
                            'product_id' => 1861,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'element_powder',
                        ]
                    ]
                ],
                [
                    'chance' => 1.680,
                    'quest_id' => 55,
                    'quest_code' => 265,
                    'quest_title' => 'ช่วยเพื่อนฟื้นฟูโซล',
                    'count_required' => 20,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1857,
                            'product_quantity' => 1,
                            'amount' => 8,
                            'reward_key' => 'blackshark',
                        ],
                        'free' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1853,
                            'product_quantity' => 1,
                            'amount' => 3,
                            'reward_key' => 'blackshark',
                        ]
                    ]
                ],
                [
                    'chance' => 1.680,
                    'quest_id' => 56,
                    'quest_code' => 265,
                    'quest_title' => 'ช่วยเพื่อนฟื้นฟูโซล',
                    'count_required' => 30,
                    'max_count' => 1000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1603,
                            'product_quantity' => 1,
                            'amount' => 10,
                            'reward_key' => 'blackshark',
                        ],
                        'free' => [
                            'product_title' => 'ประกายฉลามดำ',
                            'product_id' => 1855,
                            'product_quantity' => 1,
                            'amount' => 5,
                            'reward_key' => 'blackshark',
                        ]
                    ]
                ],
                [
                    'chance' => 1.660,
                    'achievement_step' => 2,
                    'quest_id' => 57,
                    'quest_code' => 9,
                    'quest_title' => 'ใช้มังกรสวรรค์ 100 ครั้ง',
                    'count_required' => 0,
                    'max_count' => 0,
                    'quest_type' => 'achievement_completed',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'อัญมณีหินพิทักษ์ทะเลสาบ',
                            'product_id' => 1892,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'pet_pod_crystal',
                        ],
                        'free' => [
                            'product_title' => 'อัญมณี หินพิทักษ์ทะเลสาบ',
                            'product_id' => 1672,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'pet_pod_crystal',
                        ]
                    ]
                ],
                [
                    'chance' => 1.660,
                    'achievement_step' => 1,
                    'quest_id' => 58,
                    'quest_code' => 120,
                    'quest_title' => 'เควสท์ประจำวัน 100 ครั้ง',
                    'count_required' => 0,
                    'max_count' => 0,
                    'quest_type' => 'achievement_completed',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'อัญมณีกรุฮงมุน',
                            'product_id' => 1891,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'sacred_vial',
                        ],
                        'free' => [
                            'product_title' => 'อัญมณีกรุฮงมุน',
                            'product_id' => 1671,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'sacred_vial',
                        ]
                    ]
                ],
                [
                    'chance' => 1.660,
                    'achievement_step' => 2,
                    'quest_id' => 59,
                    'quest_code' => 120,
                    'quest_title' => 'เควสท์ประจำวัน 1,000 ครั้ง',
                    'count_required' => 0,
                    'max_count' => 0,
                    'quest_type' => 'achievement_completed',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'อัญมณีกรุฮงมุน',
                            'product_id' => 1891,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'sacred_vial',
                        ],
                        'free' => [
                            'product_title' => 'อัญมณีกรุฮงมุน',
                            'product_id' => 1671,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'sacred_vial',
                        ]
                    ]
                ],
                [
                    'chance' => 1.660,
                    'quest_id' => 60,
                    'quest_code' => 189,
                    'quest_title' => 'ทำลายโซลชิลด์ที่ปลดผนึกแล้ว',
                    'count_required' => 50,
                    'max_count' => 5000,
                    'quest_type' => 'achievement_count',
                    'has_sub_quest' => false,
                    'sub_quest' => [],
                    'rewards' =>[
                        'paid' => [
                            'product_title' => 'ถุงมือยอนฮวาริน',
                            'product_id' => 1851,
                            'product_quantity' => 1,
                            'amount' => 2,
                            'reward_key' => 'yeonhwarin_glove',
                        ],
                        'free' => [
                            'product_title' => 'ถุงมือยอนฮวาริน',
                            'product_id' => 1850,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'reward_key' => 'yeonhwarin_glove',
                        ]
                    ]
                ],

            ];
        }

    }