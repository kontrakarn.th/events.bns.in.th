<?php

    namespace App\Models\hm_battlepass;

    use App\Models\BaseModel;

    class DeductLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'hm_battlepass_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            // 'quest_log_id', // default 0
            'step',
            'deduct_type', // ticket_5_pass, ticket_pass, random_mission, multiplied_reward, unlock_paid_reward
            'diamonds',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }