<?php

    namespace App\Models\hm_battlepass;

    use App\Models\BaseModel;

    class SubQuestLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'hm_battlepass_sub_quest_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'step',
            'quest_log_id', // Quest Log ID
            'sub_quest_code',
            'sub_quest_title',
            'sub_quest_start', // datetime for quest start
            'sub_count_required', // quest required count
            'sub_max_count', // for achievement_count
            'sub_count_before', // old achievement count before start, only for "achievement_count" type.
            'sub_count_after', // latest achievement count for completed quest
            'status', // pending, completed, reject (for buy new random quest.)
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }