<?php

namespace App\Models\revival_jan2020;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Members extends Model
{
    use SoftDeletes;

    protected $connection = 'events_bns';
    protected $table = 'revival_jan2020_members';
    protected $fillable = [
        'uid',
        'ncid',
        'username',
        'last_ip',
    ];
    protected $dates = ['deleted_at'];

    protected static function boot(){
        parent::boot();

        static::saving(function($model){
            $model->last_ip = \Request::ip();
        });
    }
}
