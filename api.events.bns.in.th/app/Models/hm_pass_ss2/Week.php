<?php

namespace App\Models\hm_pass_ss2;

use App\Models\BaseModel;

class Week extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'hm_pass_ss2_weeks';
    protected $fillable = [
        'week_start',
        'week_end',
    ];
}
