<?php

namespace App\Models\hm_pass_ss2;

use App\Models\BaseModel;

class ItemLog extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'hm_pass_ss2_item_logs';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'reward_id',
        'package_name',
        'package_quantity',
        'package_amount',
        'package_type',
        'send_item_status',
        'send_item_purchase_id',
        'send_item_purchase_status',
        'goods_data',
        'status',
        'log_date',
        'log_date_timestamp',
        'last_ip'
    ];
}
