<?php

namespace App\Models\hm_pass_ss2;

use App\Models\BaseModel;

class Member extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'hm_pass_ss2_members';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'char_id',
        'char_name',
        'world_id',
        'total_points',
        'level',
        'mastery_level',
        'job',
        'job_name',
        'hm_pass_lvl',
        'unlocked_hm_pass_lvl',
        'is_unlocked',
        'is_adv_unlocked',
        'claimed_special_gift',
        'purchased_lvl',
        'processing_claim_all',
        'last_ip',
    ];
}
