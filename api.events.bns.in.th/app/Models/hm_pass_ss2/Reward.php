<?php

namespace App\Models\hm_pass_ss2;

use App\Models\BaseModel;
use App\Models\hm_pass_ss2\ItemLog;

class Reward extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'hm_pass_ss2_rewards';
    protected $fillable = [
        'package_name',
        'package_id',
        'package_quantity',
        'package_amount',
        'package_type', // 'free','unlocked','special_gift'
        'required_points',
        'required_lvl',
        'is_big_reward',
        'icon',
        'hover',
    ];

    public function itemLogs()
    {
        return $this->hasMany(ItemLog::class);
    }
}
