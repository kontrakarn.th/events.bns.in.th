<?php

    namespace App\Models\preorder_warden;

    use App\Models\BaseModel;

    class ItemCode extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'preorder_warden_item_code';
        protected $fillable = [
            'code',
            'status',
            'uid',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }