<?php

namespace App\Models\gacha5baht_may;

use Moloquent;

/**
 * Description of SendItemLog
 *
 * @author naruebaetbouhom
 */
class SendItemLog extends Moloquent
{
    protected $connection = 'mongodb_bns';
    protected $table = 'gacha5baht_may_send_item_log';
    protected $fillable = [
        'uid',
        'ncid',
        'send_item_status',
        'send_item_purchase_id',
        'send_item_purchase_status',
        'goods_data',
        'status',
        'last_ip',
        'type',
        'send_gift_from',
        'log_timestamp'
    ];
}
