<?php

    namespace App\Models\wardrobe_package_2019;

    use App\Models\BaseModel;

    class DeductLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'wardrobe_package_2019_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'diamonds',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip'
        ];

    }
    