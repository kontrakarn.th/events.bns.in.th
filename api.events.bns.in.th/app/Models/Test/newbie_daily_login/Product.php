<?php

namespace App\Models\Test\newbie_daily_login;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'newbie_daily_login_product';
    protected $fillable = [
        'id',
        'product_group_id',
        'th_name',
        'en_name',
        'amount',
        'rate',
        'product_img',
    ];

}
