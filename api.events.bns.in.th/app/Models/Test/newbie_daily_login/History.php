<?php

namespace App\Models\Test\newbie_daily_login;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'newbie_daily_login_history';
    protected $fillable = [
        'history_type_id',
        'product_id',
        'get_amount',
        'get_date_string',
        'get_time_string',
        'key_id',
        'last_ip',
        'created_at'


    ];


    public function Product(){
        return $this->belongsTo('App\Models\Test\newbie_daily_login\Product','product_id');
    }
}
