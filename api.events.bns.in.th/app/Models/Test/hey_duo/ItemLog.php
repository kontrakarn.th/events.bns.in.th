<?php

namespace App\Models\Test\hey_duo;

use App\Models\BaseModel;

class ItemLog extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'hey_duo_item_logs';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'reward_id',
        'package_name',
        'package_quantity',
        'package_amount',
        'package_type',
        'send_item_status',
        'send_item_purchase_id',
        'send_item_purchase_status',
        'goods_data',
        'status',
        'log_date',
        'log_date_timestamp',
        'last_ip'
    ];
}
