<?php

namespace App\Models\Test\hey_duo;

use App\Models\BaseModel;

class Member extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'hey_duo_members';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'char_id',
        'char_name',
        'level',
        'mastery_level',
        'world_id',
        'total_points',
        'used_points',
        'duo_uid',
        'code',
        'last_ip',
    ];

    public function getRemainingPointsAttribute()
    {
        $totalPoints = $this->total_points;
        $usedPoints = $this->used_points;

        if ($totalPoints < $usedPoints) {
            return 0;
        }

        return $totalPoints - $usedPoints;
    }
}
