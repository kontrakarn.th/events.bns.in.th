<?php

namespace App\Models\Test\hey_duo;

use App\Models\BaseModel;

class Setting extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'hey_duo_settings';
    protected $fillable = [
        'event_start',
        'event_end',
        'attend_start',
        'attend_end',
        'exchange_start',
        'exchange_end',
        'active',
    ];
}
