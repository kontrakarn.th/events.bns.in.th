<?php

namespace App\Models\Test\hey_duo;

use App\Models\BaseModel;

class QuestDailyLog extends BaseModel
{
    const QUEST_TYPE_DAILY_FREE_POINTS = 'daily_free_points';
    const QUEST_TYPE_DAILY_BOSSKILL = 'daily_bosskill';
    const QUEST_TYPE_DAILY_AREA = 'daily_area';
    
    protected $connection = 'events_bns_test';
    protected $table = 'hey_duo_quest_daily_logs';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'char_id',
        'char_name',
        'quest_id',
        'quest_dungeon',
        'quest_title',
        'quest_code',
        'quest_type',
        'quest_limit',
        'quest_points',
        'image',
        'completed_count',
        'claimed_count',
        'duo_claimed_count',
        'status', // pending, success
        'claim_status', // pending, claimed
        'duo_claim_status',
        'log_date',
        'log_date_timestamp',
        'quest_complete_datetime',
        'last_ip',
    ];
}
