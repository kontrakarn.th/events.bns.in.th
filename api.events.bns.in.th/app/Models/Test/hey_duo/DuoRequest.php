<?php

namespace App\Models\Test\hey_duo;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DuoRequest extends BaseModel
{
    use SoftDeletes;
    
    const STATUS_PENDING = 'pending';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_REJECTED = 'rejected';
    
    protected $connection = 'events_bns_test';
    protected $table = 'hey_duo_duo_requests';
    protected $fillable = [
        'sender_uid',
        'sender_code',
        'sender_name',
        'sender_level',
        'sender_mastery_level',
        'sender_world_id',
        'receiver_uid',
        'receiver_code',
        'receiver_name',
        'receiver_level',
        'receiver_mastery_level',
        'receiver_world_id',
        'status',
    ];
}
