<?php

    namespace App\Models\Test\hm_pad;

    use App\Models\BaseModel;

    class HmPadBg extends BaseModel {

    	protected $connection = 'garena_bns';

        protected $table = 'events_lobby_bg';
        
        protected $fillable = [];

    }
    