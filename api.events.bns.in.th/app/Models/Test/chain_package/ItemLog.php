<?php

    namespace App\Models\Test\chain_package;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'chain_package_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'deduct_id',
            'package_id',
            'package_key',
            'package_name',
            'diamonds',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip'
        ];
    }




