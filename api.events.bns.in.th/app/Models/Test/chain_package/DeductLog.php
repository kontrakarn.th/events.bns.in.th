<?php

    namespace App\Models\Test\chain_package;

    use App\Models\BaseModel;

    class DeductLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'chain_package_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'package_id',
            'package_key',
            'package_name',
            'diamonds',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip'
        ];
    }




