<?php

    namespace App\Models\Test\chain_package;

    use App\Models\BaseModel;

    class Reward extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'chain_package_rewards';
        protected $fillable = [
            'package_name',
            'package_id',
            'package_quantity',
            'package_amount',
            'package_type',
            'package_key',
            'diamonds_required',
        ];
    }

