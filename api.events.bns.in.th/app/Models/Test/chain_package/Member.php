<?php

    namespace App\Models\Test\chain_package;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'chain_package_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'last_ip'
        ];
    }

