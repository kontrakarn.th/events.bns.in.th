<?php

namespace App\Models\Test\songkran2020;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'songkran2020_members';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'last_ip',
        'quest_success_1',
        'quest_success_2',
        'quest_success_3',
        'quest_success_4',
        'quest_success_5',
        'quest_success_6',
        'quest_success_7',
        'coin',
        'coin_used',
        'coin_all',
        'last_updated',
    ];
}
