<?php

namespace App\Models\Test\songkran2020;

use Illuminate\Database\Eloquent\Model;

class QuestLog extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'songkran2020_quest_logs';

    protected $fillable = [
        'quest_success_1',
        'quest_success_2',
        'quest_success_3',
        'quest_success_4',
        'quest_success_5',
        'quest_success_6',
        'quest_success_7',
        'coin',
    ];

}
