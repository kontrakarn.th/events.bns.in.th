<?php

namespace App\Models\Test\songkran2020;

use Illuminate\Database\Eloquent\Model;

class Cron extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'songkran2020_crons';
}
