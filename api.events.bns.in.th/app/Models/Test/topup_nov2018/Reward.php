<?php
    // Mission for disciple
    namespace App\Models\Test\topup_nov2018;

    class Reward {

        public function airpayPackage(){
            return [
                'package_id' => 8,
                'package_title' => 'AirPay Promotion Package',
                'package_desc' => 'กล่องขอบคุณพระเจ้า 2 กล่องจาก AirPay',
                'require_diamonds' => 99990000,
                'product_set' => [
                    [
                        'id' => 1,
                        'product_title' => 'กล่องขอบคุณพระเจ้า 2 กล่อง',
                        'product_id' => 1639,
                        'product_quantity' => 2
                    ]
                ]
            ];
        }

        public function setRewardByPackage($packageKey=''){
            if(empty($packageKey)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageKey-1]) && !empty($rewards[$packageKey-1])) ? $rewards[$packageKey-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'Package 1',
                    'package_desc' => 'โบนัส 500 ไดมอนด์',
                    'require_diamonds' => 5000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'โบนัส 500 ไดมอนด์',
                            'product_id' => 1804,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 2,
                    'package_title' => 'Package 2',
                    'package_desc' => 'โบนัส 1,000 ไดมอนด์',
                    'require_diamonds' => 10000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'โบนัส 1,000 ไดมอนด์',
                            'product_id' => 1805,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 3,
                    'package_title' => 'Package 3',
                    'package_desc' => 'โบนัส 3,000 ไดมอนด์',
                    'require_diamonds' => 30000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'โบนัส 3,000 ไดมอนด์',
                            'product_id' => 1806,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 4,
                    'package_title' => 'Package 4',
                    'package_desc' => 'โบนัส 10,000 ไดมอนด์',
                    'require_diamonds' => 50000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'โบนัส 10,000 ไดมอนด์',
                            'product_id' => 1807,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 5,
                    'package_title' => 'Package 5',
                    'package_desc' => 'โบนัส 20,000 ไดมอนด์|กล่องอาวุธลวงตา เอื้องอินทนิล',
                    'require_diamonds' => 100000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'โบนัส 20,000 ไดมอนด์ และ กล่องอาวุธลวงตา เอื้องอินทนิล',
                            'product_id' => 1801,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 6,
                    'package_title' => 'Package 6',
                    'package_desc' => 'โบนัส 30,000 ไดมอนด์|หมวกเอื้องอินทนิล',
                    'require_diamonds' => 150000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'โบนัส 30,000 ไดมอนด์ และ หมวกเอื้องอินทนิล',
                            'product_id' => 1802,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 7,
                    'package_title' => 'Package 7',
                    'package_desc' => 'โบนัส 60,000 ไดมอนด์|ชุดเอื้องอินทนิล',
                    'require_diamonds' => 300000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'โบนัส 60,000 ไดมอนด์ และ ชุดเอื้องอินทนิล',
                            'product_id' => 1803,
                            'product_quantity' => 1
                        ]
                    ]
                ],
            ];
        }
    }
