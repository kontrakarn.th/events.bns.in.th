<?php

namespace App\Models\Test\revival_sep2019;

use Illuminate\Database\Eloquent\Model;

class LastLoginLists extends Model
{

    protected $connection = 'events_bns_test';
    protected $table = 'revival_sep2019_last_login_list';
    protected $fillable = [
        'uid'
    ];
}
