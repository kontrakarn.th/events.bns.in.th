<?php

namespace App\Models\Test\pvp_pk;

use App\Models\BaseModel;

class DeductLog extends BaseModel
{
    protected $connection   = 'events_bns_test';
    protected $table        = 'pvp_pk_deduct_log';
    public $timestamps      = true;

    const STATUS_FLAG_FAIL = 'FAIL';
    const STATUS_FLAG_OK = 'OK';
}
