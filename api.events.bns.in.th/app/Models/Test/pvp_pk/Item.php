<?php

namespace App\Models\Test\pvp_pk;

use App\Models\BaseModel;

class Item extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'pvp_pk_item';
}
