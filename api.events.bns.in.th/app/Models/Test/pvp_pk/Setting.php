<?php

namespace App\Models\Test\pvp_pk;

use App\Models\BaseModel;

class Setting extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'pvp_pk_setting';

    public static function getSetting(){
        return self::orderBy("id","desc")->first();
    }
}
