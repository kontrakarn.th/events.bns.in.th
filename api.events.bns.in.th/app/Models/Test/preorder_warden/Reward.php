<?php

    namespace App\Models\Test\preorder_warden;

    class Reward {

        public function setRewardByPackageId($packageId=null){
            if(empty($packageId)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'Pre-Order Warden Package',
                    'amount' => 1,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินสัตว์เลี้ยงสาวน้อยชุดขาว',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'จิตแห่งฮงมุน',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 3,
                            'product_title' => 'วิญญาณฮงมุน',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 4,
                            'product_title' => 'ชุดหัวหน้าเผ่า',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 5,
                            'product_title' => 'เครื่องประดับหัวหน้าเผ่า',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 6,
                            'product_title' => 'คริสตัลสัตว์เลี้ยงฮงมุน ขั้น 1',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 7,
                            'product_title' => 'หินออบซิเดียนหกเหลี่ยม',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 8,
                            'product_title' => 'อะเมธีสหกเหลี่ยมของฮงมุน',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 9,
                            'product_title' => 'ค้อนอัญมณีที่ส่องสว่าง x3',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 10,
                            'product_title' => 'เพชรห้าเหลี่ยม',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 11,
                            'product_title' => 'กล่องแหวนราชาทมิฬ ขั้น 1',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 12,
                            'product_title' => 'กล่องต่างหูราชาทมิฬ ขั้น 1',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 13,
                            'product_title' => 'สร้อยคอจักรพรรดิมังกร ขั้น 1',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 14,
                            'product_title' => 'เข้มขัดจักรพรรดิมังกร ขั้น 1',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 15,
                            'product_title' => 'พละกำลังราชา',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 16,
                            'product_title' => 'ยาที่สกัดจากแมลง',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 17,
                            'product_title' => 'พายุหิมะซอลซัม',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 18,
                            'product_title' => 'สมุนไพรดอกไม้หิมะ',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 19,
                            'product_title' => 'รากพันปี',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 20,
                            'product_title' => 'เกล็ดมังกรไฟ x50',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 21,
                            'product_title' => 'เหรียญแห่งพันธนาการ x50',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 22,
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว x40',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 23,
                            'product_title' => 'สัญลักษณ์ฮงมุน x100',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 24,
                            'product_title' => 'กล่องอาวุธดาวตก ขั้น 6',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                        [
                            'id' => 25,
                            'product_title' => 'กล่องโซลชิลด์อีกา เบอร์ 1-8',
                            'product_id' => 1745,
                            'product_quantity' => 1,
                        ],
                    ]
                ],
            ];
        }

    }