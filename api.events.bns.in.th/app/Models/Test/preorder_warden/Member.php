<?php

    namespace App\Models\Test\preorder_warden;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'preorder_warden_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'last_ip',
        ];

    }