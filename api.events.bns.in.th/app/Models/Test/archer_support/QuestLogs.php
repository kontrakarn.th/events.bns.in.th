<?php

    namespace App\Models\Test\archer_support;

    use Moloquent;

    class QuestLogs extends Moloquent {
        protected $connection = 'mongodb_bns_test';
        protected $table = 'archer_support_quest_logs';
        protected $fillable = [
            'uid',
            'character_id',
            'quest_step',
            'quest_id',
            'quest_name',
            'created_at_string',
            'last_ip'
        ];
    }
