<?php

    namespace App\Models\Test\archer_support;

    use Moloquent;

    class Member extends Moloquent {
        protected $connection = 'mongodb_bns_test';
        protected $table = 'archer_support_member';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'character_id',
            'character_name',
            'item_group_1',
            'item_group_2',
            'item_group_3',
            'created_at_string',
            'last_ip'
        ];
    }
