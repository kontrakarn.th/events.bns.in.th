<?php

    namespace App\Models\Test\archer_support;

    use Moloquent;

    class ItemLog extends Moloquent {

        protected $connection = 'mongodb_bns_test';
        protected $table = 'archer_support_item_logs';
        protected $fillable = [
            'uid',
            'ncid',
            'item_no',
            'item_type',
            'package_title',
            'product_set',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status', // pending, success, unsuccess
            'last_ip',
            'created_at_string',
            'created_at_string_time',
            'log_date',
            'log_date_timestamp'
        ];

    }
