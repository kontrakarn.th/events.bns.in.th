<?php

namespace App\Models\Test\bns_settings;

use Illuminate\Database\Eloquent\Model;

class BnsSettings extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'bns_settings';

}
