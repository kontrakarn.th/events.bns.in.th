<?php
    // Mission for disciple
    namespace App\Models\Test\package_des3rd;

    class Reward {

        public function setRewardByPackage($packageKey=''){
            if(empty($packageKey)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageKey-1]) && !empty($rewards[$packageKey-1])) ? $rewards[$packageKey-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_key' => 1,
                    'package_title' => 'แพ็คเกจทลายจันทรา',
                    'package_desc' => 'แพ็คเกจทลายจันทรา',
                    'require_diamonds' => 150000, // 150000
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'แพ็คเกจทลายจันทรา SET 1',
                            'product_id' => 3235,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'แพ็คเกจทลายจันทรา SET 2',
                            'product_id' => 3236,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 3,
                            'product_title' => 'แพ็คเกจทลายจันทรา SET 3',
                            'product_id' => 3257,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 4,
                            'product_title' => 'แพ็คเกจทลายจันทรา SET 4',
                            'product_id' => 3237,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 5,
                            'product_title' => 'แพ็คเกจทลายจันทรา SET 5',
                            'product_id' => 3261,
                            'product_quantity' => 1
                        ],
                    ]
                ],
            ];
        }

    }
