<?php

    namespace App\Models\Test\package_des3rd;

    use App\Models\BaseModel;

    class DeductLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'package_des3rd_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'require_diamonds',
            'package_key',
            'package_title',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }