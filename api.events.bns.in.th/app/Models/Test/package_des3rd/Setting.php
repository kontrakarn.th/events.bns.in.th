<?php

    namespace App\Models\Test\package_des3rd;

    use App\Models\BaseModel;

    class Setting extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'package_des3rd_setting';
        protected $fillable = [
            'event_start',
            'event_end',
            'active',
        ];

    }