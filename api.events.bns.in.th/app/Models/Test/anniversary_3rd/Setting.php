<?php

    namespace App\Models\Test\anniversary_3rd;

    use App\Models\BaseModel;

    class Setting extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'anniversary_3rd_setting';
        protected $fillable = [
            'event_start',
            'event_end',
            'active',
            'require_level',
            'require_hm',
        ];

    }