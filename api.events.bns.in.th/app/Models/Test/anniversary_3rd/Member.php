<?php

    namespace App\Models\Test\anniversary_3rd;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'anniversary_3rd_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'last_ip',
        ];

    }