<?php

namespace App\Models\Test\anniversary_3rd;

use App\Models\BaseModel;

class Milestone extends BaseModel
{

    protected $connection = 'events_bns_test';
    protected $table = 'anniversary_3rd_milestone';
    protected $fillable = [
        'points',
        'percentage',
        'unlock_date',
        'unlock_datetime',
        'created_at',
        'updated_at',
    ];
}
