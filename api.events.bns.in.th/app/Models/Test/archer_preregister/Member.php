<?php

    namespace App\Models\Test\archer_preregister;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'archer_preregister_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'is_registered',
            'last_ip',
            'created_at',
            'updated_at'
        ];

    }