<?php

    namespace App\Models\Test\archer_preregister;

    use App\Models\BaseModel;

    class IncreasePoints extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'archer_preregister_increase_points';
        protected $fillable = [
            'points',
            'date_increase',
            'date_increase_timestamp',
        ];

    }