<?php

    namespace App\Models\Test\moonstone_festival;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'moonstone_festival_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'world_id',
            'last_ip',
        ];

    }