<?php

    namespace App\Models\Test\preorder_3rdspec;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'preorder_3rdspec_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'last_ip',
            'created_at',
            'updated_at'
        ];

    }