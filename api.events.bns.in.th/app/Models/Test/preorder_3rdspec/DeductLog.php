<?php

    namespace App\Models\Test\preorder_3rdspec;

    use App\Models\BaseModel;

    class DeductLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'preorder_3rdspec_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'discount', // 0 = no, 1 = yes
            'diamonds',
            'package_id',
            'package_title',
            'weapon_id',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }