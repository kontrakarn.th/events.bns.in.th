<?php

namespace App\Models\Test\gacha5baht_may;

use Moloquent;

/**
 * Description of Member
 *
 * @author naruebaetbouhom
 */
class Member extends Moloquent
{
    protected $connection = 'mongodb_bns_test';
    protected $table = 'gacha5baht_may_member';
    protected $fillable = ['uid', 'username', 'ncid', 'last_ip', 'log_timestamp'];
}
