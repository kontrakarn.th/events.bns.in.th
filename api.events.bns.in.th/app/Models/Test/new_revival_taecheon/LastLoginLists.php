<?php

namespace App\Models\Test\new_revival_taecheon;

use Illuminate\Database\Eloquent\Model;

class LastLoginLists extends Model
{

    protected $connection = 'events_bns_test';
    protected $table = 'new_revival_last_login_list';
    protected $fillable = [
        'uid'
    ];
}
