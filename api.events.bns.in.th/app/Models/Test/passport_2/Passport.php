<?php

    namespace App\Models\Test\passport_2;

    class Passport {

        public function getCheckinInfo($week=null,$checkinId){
            if(empty($week) || empty($checkinId)){
                return false;
            }

            $passport = $this->getPassportList();

            $weekPassport = (isset($passport[$week-1]) && !empty($passport[$week-1])) ? $passport[$week-1] : false;

            $checkInList = $weekPassport['checkin_list'];

            return (isset($checkInList[$checkinId-1]) && !empty($checkInList[$checkinId-1])) ? $checkInList[$checkinId-1] : false;

        }

        public function getCheckinSpecialReward($week=null){
            if(empty($week)){
                return false;
            }

            $passport = $this->getPassportList();

            $weekPassport = (isset($passport[$week-1]) && !empty($passport[$week-1])) ? $passport[$week-1] : false;

            return $weekPassport['checkin_special'];

        }

        public function getPassportList(){

            return [
                [
                    'week' => 1,
                    'checkin_special' => [
                        'item_type' => 'checkin_special',
                        'product_title' => 'กล่องอาวุธลวงตาเล่ห์มายา และ หินสัตว์เลี้ยงวิคตอเรีย',
                        'product_id' => 2962,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'img_key' => '',
                    ],
                    'checkin_list' => [
                        [
                            'checkin_id' => 1,
                            'date' => '2020-01-02', // 2020-05-08
                            'date_start' => '2020-01-02 00:00:00', // 2020-05-08 00:00:00
                            'date_end' => '2020-01-02 23:59:59', // 2020-05-08 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'เหล็กแทชอน',
                                'product_id' => 2272,
                                'product_quantity' => 1,
                                'amount' => 5,
                                'img_key' => '',
                            ],
                        ],
                        [
                            'checkin_id' => 2,
                            'date' => '2020-01-02', // 2020-05-09
                            'date_start' => '2020-01-02 00:00:00', // 2020-05-09 00:00:00
                            'date_end' => '2020-01-02 23:59:59', // 2020-05-09 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'กล่องหินสกิล ที่ส่องสว่าง',
                                'product_id' => 2960,
                                'product_quantity' => 1,
                                'amount' => 1,
                                'img_key' => '',
                            ],
                        ],
                        [
                            'checkin_id' => 3,
                            'date' => '2020-01-02', // 2020-01-10
                            'date_start' => '2020-01-02 00:00:00', // 2020-01-10 00:00:00
                            'date_end' => '2020-01-02 23:59:59', // 2020-01-10 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินจันทรา',
                                'product_id' => 2277,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => '',
                            ],
                        ],
                        [
                            'checkin_id' => 4,
                            'date' => '2020-01-01', // 2020-01-11
                            'date_start' => '2020-01-01 00:00:00', // 2020-01-11 00:00:00
                            'date_end' => '2020-01-01 23:59:59', // 2020-01-11 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินจันทรา',
                                'product_id' => 2277,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => '',
                            ],
                        ],
                        [
                            'checkin_id' => 5,
                            'date' => '2020-01-01', // 2020-01-12
                            'date_start' => '2020-01-01 00:00:00', // 2020-01-12 00:00:00
                            'date_end' => '2020-01-01 23:59:59', // 2020-01-12 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินจันทรา',
                                'product_id' => 2277,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => '',
                            ],
                        ],
                        [
                            'checkin_id' => 6,
                            'date' => '2020-01-01', // 2020-01-13
                            'date_start' => '2020-01-01 00:00:00', // 2020-01-13 00:00:00
                            'date_end' => '2020-01-01 23:59:59', // 2020-01-13 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินจันทรา',
                                'product_id' => 2277,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => '',
                            ],
                        ],
                        [
                            'checkin_id' => 7,
                            'date' => '2020-01-02', // 2020-01-14
                            'date_start' => '2020-01-02 00:00:00', // 2020-01-14 00:00:00
                            'date_end' => '2020-01-02 23:59:59', // 2020-01-14 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'เกล็ดสีคราม',
                                'product_id' => 2961,
                                'product_quantity' => 1,
                                'amount' => 10,
                                'img_key' => '',
                            ],
                        ],
                    ],
                ],
                [
                    'week' => 2,
                    'checkin_special' => [
                        'item_type' => 'checkin_special',
                        'product_title' => 'ชุดจันทร์เสี้ยว และ เสน่ห์ฮวันดัน',
                        'product_id' => 2963,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'img_key' => '',
                    ],
                    'checkin_list' => [
                        [
                            'checkin_id' => 1,
                            'date' => '2020-01-02', // 2020-01-15
                            'date_start' => '2020-01-02 00:00:00', // 2020-01-15 00:00:00
                            'date_end' => '2020-01-02 23:59:59', // 2020-01-15 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'เหล็กแทชอน',
                                'product_id' => 2272,
                                'product_quantity' => 1,
                                'amount' => 5,
                                'img_key' => '',
                            ],
                        ],
                        [
                            'checkin_id' => 2,
                            'date' => '2020-01-02', // 2020-01-16
                            'date_start' => '2020-01-02 00:00:00', // 2020-01-16 00:00:00
                            'date_end' => '2020-01-02 23:59:59', // 2020-01-16 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'กล่องหินสถานะ ที่ส่องสว่าง',
                                'product_id' => 2965,
                                'product_quantity' => 1,
                                'amount' => 1,
                                'img_key' => '',
                            ],
                        ],
                        [
                            'checkin_id' => 3,
                            'date' => '2020-01-02', // 2020-01-17
                            'date_start' => '2020-01-02 00:00:00', // 2020-01-17 00:00:00
                            'date_end' => '2020-01-02 23:59:59', // 2020-01-17 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินโซล',
                                'product_id' => 2275,
                                'product_quantity' => 1,
                                'amount' => 25,
                                'img_key' => '',
                            ],
                        ],
                        [
                            'checkin_id' => 4,
                            'date' => '2020-01-02', // 2020-01-18
                            'date_start' => '2020-01-02 00:00:00', // 2020-01-18 00:00:00
                            'date_end' => '2020-01-02 23:59:59', // 2020-01-18 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินโซล',
                                'product_id' => 2275,
                                'product_quantity' => 1,
                                'amount' => 25,
                                'img_key' => '',
                            ],
                        ],
                        [
                            'checkin_id' => 5,
                            'date' => '2020-01-02', // 2020-01-19
                            'date_start' => '2020-01-02 00:00:00', // 2020-01-19 00:00:00
                            'date_end' => '2020-01-02 23:59:59', // 2020-01-19 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินโซล',
                                'product_id' => 2275,
                                'product_quantity' => 1,
                                'amount' => 25,
                                'img_key' => '',
                            ],
                        ],
                        [
                            'checkin_id' => 6,
                            'date' => '2020-01-01', // 2020-01-20
                            'date_start' => '2020-01-01 00:00:00', // 2020-01-20 00:00:00
                            'date_end' => '2020-01-01 23:59:59', // 2020-01-20 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินโซล',
                                'product_id' => 2275,
                                'product_quantity' => 1,
                                'amount' => 25,
                                'img_key' => '',
                            ],
                        ],
                        [
                            'checkin_id' => 7,
                            'date' => '2020-01-01', // 2020-01-21
                            'date_start' => '2020-01-01 00:00:00', // 2020-01-21 00:00:00
                            'date_end' => '2020-01-01 23:59:59', // 2020-01-21 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'เกล็ดสีคราม',
                                'product_id' => 2961,
                                'product_quantity' => 1,
                                'amount' => 10,
                                'img_key' => '',
                            ],
                        ],
                    ],
                ],
            ];

        }

    }
