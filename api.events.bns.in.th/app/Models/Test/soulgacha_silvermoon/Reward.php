<?php

namespace App\Models\Test\soulgacha_silvermoon;
use Carbon\Carbon;

class Reward
{

    public function getRandomRewardInfo()
    {
        // $date_rate=Carbon::parse("2019-10-05 00:00:00");
        // $date_now=Carbon::now();
        // if($date_now<$date_rate){
        //     $rewards = $this->getRewardsListFirstDay();
        // }else{
            $rewards = $this->getRewardsList();
        // }

        $random = rand(0, 100000);
        $accumulatedChance = 0;
        $reward = false;

        foreach ($rewards as $value) {
            $chance = $value['chance'] * 1000;
            $currentChance = $accumulatedChance + $chance;

                // acquired reward
            if ($random >= $accumulatedChance && $random <= $currentChance) {
                    // $value['color'] = $this->randomColor();
                $reward = $value;
                break;
            }
            $accumulatedChance = $currentChance;
        }

        $reward['random'] = $random;

            // now we have the reward
        return $reward;
    }

    protected function getRewardsListFirstDay()
    {
        return [
            [
                'id' => 1,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ทเหมียวจันทราสีเงิน',
                'product_id' => 2635,
                'product_quantity' => 1,
                'chance' => 0.550,
                'icon' => 'S_Moon_09_Base',
                'key' => 'S_Moon_09_get',
                'color' => 'gold'
            ],
            [
                'id' => 2,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ทนักเรียนเกาหลียุค 80',
                'product_id' => 2636,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_10_Base',
                'key' => 'S_Moon_10_get',
                'color' => 'gold'
            ],
            [
                'id' => 3,
                'item_type' => 'rare_item',
                'product_title' => 'กล่องอาวุธลวงตาจันทราสีเงิน',
                'product_id' => 2637,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'S_Moon_11_Base',
                'key' => 'S_Moon_11_get',
                'color' => 'gold'
            ],
            [
                'id' => 4,
                'item_type' => 'rare_item',
                'product_title' => 'ทรงผมจันทราสีเงิน',
                'product_id' => 2639,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'S_Moon_03_Base',
                'key' => 'S_Moon_03_get',
                'color' => 'gold'
            ],
            [
                'id' => 5,
                'item_type' => 'rare_item',
                'product_title' => 'ต่างหูจันทราสีเงิน',
                'product_id' => 2640,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'S_Moon_04_Base',
                'key' => 'S_Moon_04_get',
                'color' => 'gold'
            ],
            [
                'id' => 6,
                'item_type' => 'rare_item',
                'product_title' => 'เครื่องประดับจันทราสีเงิน',
                'product_id' => 2641,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'S_Moon_02_Base',
                'key' => 'S_Moon_02_get',
                'color' => 'gold'
            ],
            [
                'id' => 7,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดจันทราสีเงิน',
                'product_id' => 2638,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'S_Moon_01_Base',
                'key' => 'S_Moon_01_get',
                'color' => 'gold'
            ],
            // Items parts set A
            [
                'id' => 8,
                'item_type' => 'material',
                'product_title' => 'ชุดจันทราสีเงิน (Part 1)',
                'product_id_as' => 'MAT_1_1',
                'product_id' => 20791,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_01_R',
                'key' => 'S_Moon_01_R_get',
                'color' => 'red'
            ],
            [
                'id' => 9,
                'item_type' => 'material',
                'product_title' => 'ชุดจันทราสีเงิน (Part 2)',
                'product_id_as' => 'MAT_1_2',
                'product_id' => 20792,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_01_G',
                'key' => 'S_Moon_01_G_get',
                'color' => 'green'
            ],
            [
                'id' => 10,
                'item_type' => 'material',
                'product_title' => 'ชุดจันทราสีเงิน (Part 3)',
                'product_id_as' => 'MAT_1_3',
                'product_id' => 20793,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'S_Moon_01_B',
                'key' => 'S_Moon_01_B_get',
                'color' => 'blue'
            ],
                // Items parts set B
            [
                'id' => 11,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับจันทราสีเงิน (Part 1)',
                'product_id_as' => 'MAT_2_1',
                'product_id' => 20801,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_02_R',
                'key' => 'S_Moon_02_R_get',
                'color' => 'red'
            ],
            [
                'id' => 12,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับจันทราสีเงิน (Part 2)',
                'product_id_as' => 'MAT_2_2',
                'product_id' => 20802,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_02_G',
                'key' => 'S_Moon_02_G_get',
                'color' => 'green'
            ],
            [
                'id' => 13,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับจันทราสีเงิน (Part 3)',
                'product_id_as' => 'MAT_2_3',
                'product_id' => 20803,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_02_B',
                'key' => 'S_Moon_02_B_get',
                'color' => 'blue'
            ],

                // Items parts set C
            [
                'id' => 14,
                'item_type' => 'material',
                'product_title' => 'ต่างหูจันทราสีเงิน (Part 1)',
                'product_id_as' => 'MAT_3_1',
                'product_id' => 20811,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_04_R',
                'key' => 'S_Moon_04_R_get',
                'color' => 'red'
            ],
            [
                'id' => 15,
                'item_type' => 'material',
                'product_title' => 'ต่างหูจันทราสีเงิน (Part 2)',
                'product_id_as' => 'MAT_3_2',
                'product_id' => 20812,
                'product_quantity' => 1,
                'chance' => 0.700,
                'icon' => 'S_Moon_04_G',
                'key' => 'S_Moon_04_G_get',
                'color' => 'green'
            ],
            [
                'id' => 16,
                'item_type' => 'material',
                'product_title' => 'ต่างหูจันทราสีเงิน (Part 3)',
                'product_id_as' => 'MAT_3_3',
                'product_id' => 20813,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_04_B',
                'key' => 'S_Moon_04_B_get',
                'color' => 'blue'
            ],


            // Items parts set D
            [
                'id' => 17,
                'item_type' => 'material',
                'product_title' => 'ทรงผมจันทราสีเงิน (Part 1)',
                'product_id_as' => 'MAT_4_1',
                'product_id' => 20821,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_03_R',
                'key' => 'S_Moon_03_R_get',
                'color' => 'red'
            ],
            [
                'id' => 18,
                'item_type' => 'material',
                'product_title' => 'ทรงผมจันทราสีเงิน (Part 2)',
                'product_id_as' => 'MAT_4_2',
                'product_id' => 20822,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_03_G',
                'key' => 'S_Moon_03_G_get',
                'color' => 'green'
            ],
            [
                'id' => 19,
                'item_type' => 'material',
                'product_title' => 'ทรงผมจันทราสีเงิน (Part 3)',
                'product_id_as' => 'MAT_4_3',
                'product_id' => 20823,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_03_B',
                'key' => 'S_Moon_03_B_get',
                'color' => 'blue'
            ],


            // Items parts set E
            [
                'id' => 20,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับนักเรียนเกาหลียุค 80 (Part 1)',
                'product_id_as' => 'MAT_5_1',
                'product_id' => 20831,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_07_R',
                'key' => 'S_Moon_07_R_get',
                'color' => 'red'
            ],
            [
                'id' => 21,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับนักเรียนเกาหลียุค 80 (Part 2)',
                'product_id_as' => 'MAT_5_2',
                'product_id' => 20832,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_07_G',
                'key' => 'S_Moon_07_G_get',
                'color' => 'green'
            ],
            [
                'id' => 22,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับนักเรียนเกาหลียุค 80 (Part 3)',
                'product_id_as' => 'MAT_5_3',
                'product_id' => 20833,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_07_B',
                'key' => 'S_Moon_07_B_get',
                'color' => 'blue'
            ],

            // Items parts set F
            [
                'id' => 23,
                'item_type' => 'material',
                'product_title' => 'หมวกนักเรียนเกาหลียุค 80 (Part 1)',
                'product_id_as' => 'MAT_6_1',
                'product_id' => 20841,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_08_R',
                'key' => 'S_Moon_08_R_get',
                'color' => 'red'
            ],
            [
                'id' => 24,
                'item_type' => 'material',
                'product_title' => 'หมวกนักเรียนเกาหลียุค 80 (Part 2)',
                'product_id_as' => 'MAT_6_2',
                'product_id' => 20842,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_08_G',
                'key' => 'S_Moon_08_G_get',
                'color' => 'green'
            ],
            [
                'id' => 25,
                'item_type' => 'material',
                'product_title' => 'หมวกนักเรียนเกาหลียุค 80 (Part 3)',
                'product_id_as' => 'MAT_6_3',
                'product_id' => 20843,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_08_B',
                'key' => 'S_Moon_08_B_get',
                'color' => 'blue'
            ],
            // Items parts set G
            [
                'id' => 26,
                'item_type' => 'material',
                'product_title' => 'ชุดนักเรียนเกาหลียุค 80 (Part 1)',
                'product_id_as' => 'MAT_7_1',
                'product_id' => 20851,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_06_R',
                'key' => 'S_Moon_06_R_get',
                'color' => 'red'
            ],
            [
                'id' => 27,
                'item_type' => 'material',
                'product_title' => 'ชุดนักเรียนเกาหลียุค 80 (Part 2)',
                'product_id_as' => 'MAT_7_2',
                'product_id' => 20852,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_06_G',
                'key' => 'S_Moon_06_G_get',
                'color' => 'green'
            ],
            [
                'id' => 28,
                'item_type' => 'material',
                'product_title' => 'ชุดนักเรียนเกาหลียุค 80 (Part 3)',
                'product_id_as' => 'MAT_7_3',
                'product_id' => 20853,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_06_B',
                'key' => 'S_Moon_06_B_get',
                'color' => 'blue'
            ],
            // Items parts set H
            [
                'id' => 29,
                'item_type' => 'material',
                'product_title' => 'หินสัตว์เลี้ยงสาวน้อยจากจันทรา (Part 1)',
                'product_id_as' => 'MAT_8_1',
                'product_id' => 20861,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_05_R',
                'key' => 'S_Moon_05_R_get',
                'color' => 'red'
            ],
            [
                'id' => 30,
                'item_type' => 'material',
                'product_title' => 'หินสัตว์เลี้ยงสาวน้อยจากจันทรา (Part 2)',
                'product_id_as' => 'MAT_8_2',
                'product_id' => 20862,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_05_G',
                'key' => 'S_Moon_05_G_get',
                'color' => 'green'
            ],
            [
                'id' => 31,
                'item_type' => 'material',
                'product_title' => 'หินสัตว์เลี้ยงสาวน้อยจากจันทรา (Part 3)',
                'product_id_as' => 'MAT_8_3',
                'product_id' => 20863,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'S_Moon_05_B',
                'key' => 'S_Moon_05_B_get',
                'color' => 'blue'
            ],

            // Object item
            [
                'id' => 32,
                'item_type' => 'object',
                'product_title' => 'สัญลักษณ์ฮงมุน x50',
                'product_id' => 1881,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_12',
                'key' => 'S_Moon_12',
                'color' => 'red'
            ],
            [
                'id' => 33,
                'item_type' => 'object',
                'product_title' => 'กิก้าโฟน x10',
                'product_id' => 1062,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_13',
                'key' => 'S_Moon_13',
                'color' => 'red'
            ],
            [
                'id' => 34,
                'item_type' => 'object',
                'product_title' => 'ยาโชคชะตา ขนาดใหญ่ x50',
                'product_id' => 2642,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'S_Moon_14',
                'key' => 'S_Moon_14',
                'color' => 'red'
            ],
            [
                'id' => 35,
                'item_type' => 'object',
                'product_title' => 'คริสตัลหินโซล x10',
                'product_id' => 1848,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_15',
                'key' => 'S_Moon_15',
                'color' => 'red'
            ],
            [
                'id' => 36,
                'item_type' => 'object',
                'product_title' => 'เกล็ดมังกรไฟ x10',
                'product_id' => 2643,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_16',
                'key' => 'S_Moon_16',
                'color' => 'red'
            ],
            [
                'id' => 37,
                'item_type' => 'object',
                'product_title' => 'คริสตัลหินจันทรา x5',
                'product_id' => 1116,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_17',
                'key' => 'S_Moon_17',
                'color' => 'red'
            ],
            [
                'id' => 38,
                'item_type' => 'object',
                'product_title' => 'เหรียญฝึกฝน ฮงซอกกึน x1',
                'product_id' => 2644,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_18',
                'key' => 'S_Moon_18',
                'color' => 'red'
            ],
            [
                'id' => 39,
                'item_type' => 'object',
                'product_title' => 'คริสตัลอัญมณีฮงมุน x1',
                'product_id' => 2645,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'S_Moon_19',
                'key' => 'S_Moon_19',
                'color' => 'red'
            ],
            [
                'id' => 40,
                'item_type' => 'object',
                'product_title' => 'ไหมห้าสี x1',
                'product_id' => 2646,
                'product_quantity' => 1,
                'chance' => 3.000,
                'icon' => 'S_Moon_20',
                'key' => 'S_Moon_20',
                'color' => 'red'
            ],
            [
                'id' => 41,
                'item_type' => 'object',
                'product_title' => 'ค้อนอัญมณีที่ส่องสว่าง x1',
                'product_id' => 569,
                'product_quantity' => 1,
                'chance' => 3.000,
                'icon' => 'S_Moon_21',
                'key' => 'S_Moon_21',
                'color' => 'red'
            ],
            [
                'id' => 42,
                'item_type' => 'object',
                'product_title' => 'ลูกแก้วยอดนักรบ x1',
                'product_id' => 2647,
                'product_quantity' => 1,
                'chance' => 3.000,
                'icon' => 'S_Moon_22',
                'key' => 'S_Moon_22',
                'color' => 'red'
            ],
            [
                'id' => 43,
                'item_type' => 'object',
                'product_title' => 'ดาวมังกรฟ้า x1',
                'product_id' => 2648,
                'product_quantity' => 1,
                'chance' => 2.000,
                'icon' => 'S_Moon_23',
                'key' => 'S_Moon_23',
                'color' => 'red'
            ],
            [
                'id' => 44,
                'item_type' => 'object',
                'product_title' => 'กรุพิศวง x1',
                'product_id' => 1986,
                'product_quantity' => 1,
                'chance' => 1.000,
                'icon' => 'S_Moon_24',
                'key' => 'S_Moon_24',
                'color' => 'red'
            ],
            [
                'id' => 45,
                'item_type' => 'object',
                'product_title' => 'หินเปลี่ยนรูปชั้นสูง x1',
                'product_id' => 911,
                'product_quantity' => 1,
                'chance' => 1.000,
                'icon' => 'S_Moon_25',
                'key' => 'S_Moon_25',
                'color' => 'red'
            ],
            [
                'id' => 46,
                'item_type' => 'object',
                'product_title' => 'คริสตัลวิญญาณฮงมุน x1',
                'product_id' => 1896,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_26',
                'key' => 'S_Moon_26',
                'color' => 'red'
            ],
        ];
    }

    protected function getRewardsList()
    {
        return [
            [
                'id' => 1,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ทเหมียวจันทราสีเงิน',
                'product_id' => 2635,
                'product_quantity' => 1,
                'chance' => 0.550,
                'icon' => 'S_Moon_09_Base',
                'key' => 'S_Moon_09_get',
                'color' => 'gold'
            ],
            [
                'id' => 2,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ทนักเรียนเกาหลียุค 80',
                'product_id' => 2636,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_10_Base',
                'key' => 'S_Moon_10_get',
                'color' => 'gold'
            ],
            [
                'id' => 3,
                'item_type' => 'rare_item',
                'product_title' => 'กล่องอาวุธลวงตาจันทราสีเงิน',
                'product_id' => 2637,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'S_Moon_11_Base',
                'key' => 'S_Moon_11_get',
                'color' => 'gold'
            ],
            [
                'id' => 4,
                'item_type' => 'rare_item',
                'product_title' => 'ทรงผมจันทราสีเงิน',
                'product_id' => 2639,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'S_Moon_03_Base',
                'key' => 'S_Moon_03_get',
                'color' => 'gold'
            ],
            [
                'id' => 5,
                'item_type' => 'rare_item',
                'product_title' => 'ต่างหูจันทราสีเงิน',
                'product_id' => 2640,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'S_Moon_04_Base',
                'key' => 'S_Moon_04_get',
                'color' => 'gold'
            ],
            [
                'id' => 6,
                'item_type' => 'rare_item',
                'product_title' => 'เครื่องประดับจันทราสีเงิน',
                'product_id' => 2641,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'S_Moon_02_Base',
                'key' => 'S_Moon_02_get',
                'color' => 'gold'
            ],
            [
                'id' => 7,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดจันทราสีเงิน',
                'product_id' => 2638,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'S_Moon_01_Base',
                'key' => 'S_Moon_01_get',
                'color' => 'gold'
            ],
            // Items parts set A
            [
                'id' => 8,
                'item_type' => 'material',
                'product_title' => 'ชุดจันทราสีเงิน (Part 1)',
                'product_id_as' => 'MAT_1_1',
                'product_id' => 20791,
                'product_quantity' => 1,
                'chance' => 0.300,
                'icon' => 'S_Moon_01_R',
                'key' => 'S_Moon_01_R_get',
                'color' => 'red'
            ],
            [
                'id' => 9,
                'item_type' => 'material',
                'product_title' => 'ชุดจันทราสีเงิน (Part 2)',
                'product_id_as' => 'MAT_1_2',
                'product_id' => 20792,
                'product_quantity' => 1,
                'chance' => 0.150,
                'icon' => 'S_Moon_01_G',
                'key' => 'S_Moon_01_G_get',
                'color' => 'green'
            ],
            [
                'id' => 10,
                'item_type' => 'material',
                'product_title' => 'ชุดจันทราสีเงิน (Part 3)',
                'product_id_as' => 'MAT_1_3',
                'product_id' => 20793,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'S_Moon_01_B',
                'key' => 'S_Moon_01_B_get',
                'color' => 'blue'
            ],
                // Items parts set B
            [
                'id' => 11,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับจันทราสีเงิน (Part 1)',
                'product_id_as' => 'MAT_2_1',
                'product_id' => 20801,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_02_R',
                'key' => 'S_Moon_02_R_get',
                'color' => 'red'
            ],
            [
                'id' => 12,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับจันทราสีเงิน (Part 2)',
                'product_id_as' => 'MAT_2_2',
                'product_id' => 20802,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_02_G',
                'key' => 'S_Moon_02_G_get',
                'color' => 'green'
            ],
            [
                'id' => 13,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับจันทราสีเงิน (Part 3)',
                'product_id_as' => 'MAT_2_3',
                'product_id' => 20803,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'S_Moon_02_B',
                'key' => 'S_Moon_02_B_get',
                'color' => 'blue'
            ],

                // Items parts set C
            [
                'id' => 14,
                'item_type' => 'material',
                'product_title' => 'ต่างหูจันทราสีเงิน (Part 1)',
                'product_id_as' => 'MAT_3_1',
                'product_id' => 20811,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_04_R',
                'key' => 'S_Moon_04_R_get',
                'color' => 'red'
            ],
            [
                'id' => 15,
                'item_type' => 'material',
                'product_title' => 'ต่างหูจันทราสีเงิน (Part 2)',
                'product_id_as' => 'MAT_3_2',
                'product_id' => 20812,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_04_G',
                'key' => 'S_Moon_04_G_get',
                'color' => 'green'
            ],
            [
                'id' => 16,
                'item_type' => 'material',
                'product_title' => 'ต่างหูจันทราสีเงิน (Part 3)',
                'product_id_as' => 'MAT_3_3',
                'product_id' => 20813,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'S_Moon_04_B',
                'key' => 'S_Moon_04_B_get',
                'color' => 'blue'
            ],


            // Items parts set D
            [
                'id' => 17,
                'item_type' => 'material',
                'product_title' => 'ทรงผมจันทราสีเงิน (Part 1)',
                'product_id_as' => 'MAT_4_1',
                'product_id' => 20821,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_03_R',
                'key' => 'S_Moon_03_R_get',
                'color' => 'red'
            ],
            [
                'id' => 18,
                'item_type' => 'material',
                'product_title' => 'ทรงผมจันทราสีเงิน (Part 2)',
                'product_id_as' => 'MAT_4_2',
                'product_id' => 20822,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_03_G',
                'key' => 'S_Moon_03_G_get',
                'color' => 'green'
            ],
            [
                'id' => 19,
                'item_type' => 'material',
                'product_title' => 'ทรงผมจันทราสีเงิน (Part 3)',
                'product_id_as' => 'MAT_4_3',
                'product_id' => 20823,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'S_Moon_03_B',
                'key' => 'S_Moon_03_B_get',
                'color' => 'blue'
            ],


            // Items parts set E
            [
                'id' => 20,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับนักเรียนเกาหลียุค 80 (Part 1)',
                'product_id_as' => 'MAT_5_1',
                'product_id' => 20831,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_07_R',
                'key' => 'S_Moon_07_R_get',
                'color' => 'red'
            ],
            [
                'id' => 21,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับนักเรียนเกาหลียุค 80 (Part 2)',
                'product_id_as' => 'MAT_5_2',
                'product_id' => 20832,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_07_G',
                'key' => 'S_Moon_07_G_get',
                'color' => 'green'
            ],
            [
                'id' => 22,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับนักเรียนเกาหลียุค 80 (Part 3)',
                'product_id_as' => 'MAT_5_3',
                'product_id' => 20833,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'S_Moon_07_B',
                'key' => 'S_Moon_07_B_get',
                'color' => 'blue'
            ],

            // Items parts set F
            [
                'id' => 23,
                'item_type' => 'material',
                'product_title' => 'หมวกนักเรียนเกาหลียุค 80 (Part 1)',
                'product_id_as' => 'MAT_6_1',
                'product_id' => 20841,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_08_R',
                'key' => 'S_Moon_08_R_get',
                'color' => 'red'
            ],
            [
                'id' => 24,
                'item_type' => 'material',
                'product_title' => 'หมวกนักเรียนเกาหลียุค 80 (Part 2)',
                'product_id_as' => 'MAT_6_2',
                'product_id' => 20842,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_08_G',
                'key' => 'S_Moon_08_G_get',
                'color' => 'green'
            ],
            [
                'id' => 25,
                'item_type' => 'material',
                'product_title' => 'หมวกนักเรียนเกาหลียุค 80 (Part 3)',
                'product_id_as' => 'MAT_6_3',
                'product_id' => 20843,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'S_Moon_08_B',
                'key' => 'S_Moon_08_B_get',
                'color' => 'blue'
            ],
            // Items parts set G
            [
                'id' => 26,
                'item_type' => 'material',
                'product_title' => 'ชุดนักเรียนเกาหลียุค 80 (Part 1)',
                'product_id_as' => 'MAT_7_1',
                'product_id' => 20851,
                'product_quantity' => 1,
                'chance' => 0.750,
                'icon' => 'S_Moon_06_R',
                'key' => 'S_Moon_06_R_get',
                'color' => 'red'
            ],
            [
                'id' => 27,
                'item_type' => 'material',
                'product_title' => 'ชุดนักเรียนเกาหลียุค 80 (Part 2)',
                'product_id_as' => 'MAT_7_2',
                'product_id' => 20852,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_06_G',
                'key' => 'S_Moon_06_G_get',
                'color' => 'green'
            ],
            [
                'id' => 28,
                'item_type' => 'material',
                'product_title' => 'ชุดนักเรียนเกาหลียุค 80 (Part 3)',
                'product_id_as' => 'MAT_7_3',
                'product_id' => 20853,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'S_Moon_06_B',
                'key' => 'S_Moon_06_B_get',
                'color' => 'blue'
            ],
            // Items parts set H
            [
                'id' => 29,
                'item_type' => 'material',
                'product_title' => 'หินสัตว์เลี้ยงสาวน้อยจากจันทรา (Part 1)',
                'product_id_as' => 'MAT_8_1',
                'product_id' => 20861,
                'product_quantity' => 1,
                'chance' => 0.300,
                'icon' => 'S_Moon_05_R',
                'key' => 'S_Moon_05_R_get',
                'color' => 'red'
            ],
            [
                'id' => 30,
                'item_type' => 'material',
                'product_title' => 'หินสัตว์เลี้ยงสาวน้อยจากจันทรา (Part 2)',
                'product_id_as' => 'MAT_8_2',
                'product_id' => 20862,
                'product_quantity' => 1,
                'chance' => 0.150,
                'icon' => 'S_Moon_05_G',
                'key' => 'S_Moon_05_G_get',
                'color' => 'green'
            ],
            [
                'id' => 31,
                'item_type' => 'material',
                'product_title' => 'หินสัตว์เลี้ยงสาวน้อยจากจันทรา (Part 3)',
                'product_id_as' => 'MAT_8_3',
                'product_id' => 20863,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'S_Moon_05_B',
                'key' => 'S_Moon_05_B_get',
                'color' => 'blue'
            ],

            // Object item
            [
                'id' => 32,
                'item_type' => 'object',
                'product_title' => 'สัญลักษณ์ฮงมุน x50',
                'product_id' => 1881,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_12',
                'key' => 'S_Moon_12',
                'color' => 'red'
            ],
            [
                'id' => 33,
                'item_type' => 'object',
                'product_title' => 'กิก้าโฟน x10',
                'product_id' => 1062,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_13',
                'key' => 'S_Moon_13',
                'color' => 'red'
            ],
            [
                'id' => 34,
                'item_type' => 'object',
                'product_title' => 'ยาโชคชะตา ขนาดใหญ่ x50',
                'product_id' => 2642,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_14',
                'key' => 'S_Moon_14',
                'color' => 'red'
            ],
            [
                'id' => 35,
                'item_type' => 'object',
                'product_title' => 'คริสตัลหินโซล x10',
                'product_id' => 1848,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_15',
                'key' => 'S_Moon_15',
                'color' => 'red'
            ],
            [
                'id' => 36,
                'item_type' => 'object',
                'product_title' => 'เกล็ดมังกรไฟ x10',
                'product_id' => 2643,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_16',
                'key' => 'S_Moon_16',
                'color' => 'red'
            ],
            [
                'id' => 37,
                'item_type' => 'object',
                'product_title' => 'คริสตัลหินจันทรา x5',
                'product_id' => 1116,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_17',
                'key' => 'S_Moon_17',
                'color' => 'red'
            ],
            [
                'id' => 38,
                'item_type' => 'object',
                'product_title' => 'เหรียญฝึกฝน ฮงซอกกึน x1',
                'product_id' => 2644,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'S_Moon_18',
                'key' => 'S_Moon_18',
                'color' => 'red'
            ],
            [
                'id' => 39,
                'item_type' => 'object',
                'product_title' => 'คริสตัลอัญมณีฮงมุน x1',
                'product_id' => 2645,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'S_Moon_19',
                'key' => 'S_Moon_19',
                'color' => 'red'
            ],
            [
                'id' => 40,
                'item_type' => 'object',
                'product_title' => 'ไหมห้าสี x1',
                'product_id' => 2646,
                'product_quantity' => 1,
                'chance' => 3.000,
                'icon' => 'S_Moon_20',
                'key' => 'S_Moon_20',
                'color' => 'red'
            ],
            [
                'id' => 41,
                'item_type' => 'object',
                'product_title' => 'ค้อนอัญมณีที่ส่องสว่าง x1',
                'product_id' => 569,
                'product_quantity' => 1,
                'chance' => 3.000,
                'icon' => 'S_Moon_21',
                'key' => 'S_Moon_21',
                'color' => 'red'
            ],
            [
                'id' => 42,
                'item_type' => 'object',
                'product_title' => 'ลูกแก้วยอดนักรบ x1',
                'product_id' => 2647,
                'product_quantity' => 1,
                'chance' => 3.000,
                'icon' => 'S_Moon_22',
                'key' => 'S_Moon_22',
                'color' => 'red'
            ],
            [
                'id' => 43,
                'item_type' => 'object',
                'product_title' => 'ดาวมังกรฟ้า x1',
                'product_id' => 2648,
                'product_quantity' => 1,
                'chance' => 2.000,
                'icon' => 'S_Moon_23',
                'key' => 'S_Moon_23',
                'color' => 'red'
            ],
            [
                'id' => 44,
                'item_type' => 'object',
                'product_title' => 'กรุพิศวง x1',
                'product_id' => 1986,
                'product_quantity' => 1,
                'chance' => 1.000,
                'icon' => 'S_Moon_24',
                'key' => 'S_Moon_24',
                'color' => 'red'
            ],
            [
                'id' => 45,
                'item_type' => 'object',
                'product_title' => 'หินเปลี่ยนรูปชั้นสูง x1',
                'product_id' => 911,
                'product_quantity' => 1,
                'chance' => 1.000,
                'icon' => 'S_Moon_25',
                'key' => 'S_Moon_25',
                'color' => 'red'
            ],
            [
                'id' => 46,
                'item_type' => 'object',
                'product_title' => 'คริสตัลวิญญาณฮงมุน x1',
                'product_id' => 1896,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'S_Moon_26',
                'key' => 'S_Moon_26',
                'color' => 'red'
            ],
        ];
    }

    // Rare Items
    public function setRareItemByPackageId($packageId=null){
        if(empty($packageId)){
            return false;
        }

        $rewards = $this->rareItemList();

        return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
    }

    private function rareItemList(){
        return [
            [
                'id' => 1,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดจันทราสีเงิน',
                'product_id' => 2638,
                'product_quantity' => 1,
                'icon' => 'S_Moon_01_Base',
                'key' => 'S_Moon_01',
                'color' => 'red'
            ],
            [
                'id' => 2,
                'item_type' => 'rare_item',
                'product_title' => 'เครื่องประดับจันทราสีเงิน',
                'product_id' => 2641,
                'product_quantity' => 1,
                'icon' => 'S_Moon_02_Base',
                'key' => 'S_Moon_02',
                'color' => 'red'

            ],
            [
                'id' => 3,
                'item_type' => 'rare_item',
                'product_title' => 'ต่างหูจันทราสีเงิน',
                'product_id' => 2640,
                'product_quantity' => 1,
                'icon' => 'S_Moon_04_Base',
                'key' => 'S_Moon_04',
                'color' => 'red'
            ],
            [
                'id' => 4,
                'item_type' => 'rare_item',
                'product_title' => 'ทรงผมจันทราสีเงิน',
                'product_id' => 2639,
                'product_quantity' => 1,
                'icon' => 'S_Moon_03_Base',
                'key' => 'S_Moon_03',
                'color' => 'red'
            ],
            [
                'id' => 5,
                'item_type' => 'rare_item',
                'product_title' => 'เครื่องประดับนักเรียนเกาหลียุค 80',
                'product_id' => 2651,
                'product_quantity' => 1,
                'icon' => 'S_Moon_07_Base',
                'key' => 'S_Moon_07',
                'color' => 'red'
            ],
            [
                'id' => 6,
                'item_type' => 'rare_item',
                'product_title' => 'หมวกนักเรียนเกาหลียุค 80',
                'product_id' => 2650,
                'product_quantity' => 1,
                'icon' => 'S_Moon_08_Base',
                'key' => 'S_Moon_08',
                'color' => 'red'
            ],
            [
                'id' => 7,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดนักเรียนเกาหลียุค 80',
                'product_id' => 2649,
                'product_quantity' => 1,
                'icon' => 'S_Moon_06_Base',
                'key' => 'S_Moon_06',
                'color' => 'red'
            ],
            [
                'id' => 8,
                'item_type' => 'rare_item',
                'product_title' => 'หินสัตว์เลี้ยงสาวน้อยจากจันทรา',
                'product_id' => 2652,
                'product_quantity' => 1,
                'icon' => 'S_Moon_05_Base',
                'key' => 'S_Moon_05',
                'color' => 'red'
            ],
            [
                'id' => 9,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ทเหมียวจันทราสีเงิน',
                'product_id' => 2635,
                'product_quantity' => 1,
                'icon' => 'S_Moon_09_Base',
                'key' => 'S_Moon_09',
                'color' => 'red'
            ],
            [
                'id' => 10,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ทนักเรียนเกาหลียุค 80',
                'product_id' => 2636,
                'product_quantity' => 1,
                'icon' => 'S_Moon_10_Base',
                'key' => 'S_Moon_10',
                'color' => 'red'
            ],
            [
                'id' => 11,
                'item_type' => 'rare_item',
                'product_title' => 'กล่องอาวุธลวงตาจันทราสีเงิน',
                'product_id' => 2637,
                'product_quantity' => 1,
                'icon' => 'S_Moon_11_Base',
                'key' => 'S_Moon_11',
                'color' => 'red'
            ],
        ];
    }

}
