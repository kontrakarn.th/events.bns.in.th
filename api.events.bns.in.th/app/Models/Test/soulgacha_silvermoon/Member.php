<?php

namespace App\Models\Test\soulgacha_silvermoon;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'silvermoon_members';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'last_ip'
    ];
}
