<?php

    namespace App\Models\Test\buffet;

    use Moloquent;

    class PlayLogs extends Moloquent {
        protected $connection = 'mongodb_bns_test';
        protected $table = 'buffet_play_logs';
        protected $fillable = [
            'uid',
            'character_id',
            'play_type',
            'created_at_string',
        ];
    }
