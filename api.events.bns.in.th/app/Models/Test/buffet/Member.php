<?php

    namespace App\Models\Test\buffet;

    use Moloquent;

    class Member extends Moloquent {
        protected $connection = 'mongodb_bns_test';
        protected $table = 'buffet_member';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'character_id',
            'character_name',
            'ticket_group_1',
            'ticket_group_1_used',
            'ticket_group_2',
            'ticket_group_2_used',
            'ticket_group_3',
            'ticket_group_3_used',
            'token',
            'token_used',
            'created_at_string',
            'last_ip'
        ];
    }
