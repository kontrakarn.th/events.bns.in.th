<?php
    // Mission for disciple
    namespace App\Models\Test\buffet;

    class Reward {

        public function getRandomGachapon($group){

            $rewards = $this->getRewardsGachapon($group);

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($rewards as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            $reward['timestamp'] = time();

            // now we have the reward
            return $reward;
        }

        private function getRewardsGachapon($group){
            if($group==1){
                return [
                    [
                        'id' => 1,
                        'item_type' => 'events',
                        'group'=>1,
                        'icon'=>1,
                        'product_title' => 'คูปองกินบุฟเฟ่ต์ฟรี 3 ชิ้น',
                        'product_id' => -1,
                        'product_quantity' => 3,
                        'amount' => 3,
                        'chance' => 25.000,
                    ],
                    [
                        'id' => 2,
                        'item_type' => 'gachapon',
                        'group'=>1,
                        'icon'=>2,
                        'product_title' => 'หินโซล 2 ชิ้น',
                        'product_id' => 2468,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 20.000,
                    ],
                    [
                        'id' => 3,
                        'item_type' => 'gachapon',
                        'group'=>1,
                        'icon'=>3,
                        'product_title' => 'หินจันทรา 1 ชิ้น',
                        'product_id' => 1924,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 15.000,
                    ],
                    [
                        'id' => 4,
                        'item_type' => 'gachapon',
                        'group'=>1,
                        'icon'=>4,
                        'product_title' => 'เกล็ดสีคราม 1 ชิ้น',
                        'product_id' => 2071,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 15.000,
                    ],
                    [
                        'id' => 5,
                        'item_type' => 'gachapon',
                        'group'=>1,
                        'icon'=>2,
                        'product_title' => 'หินโซล 20 ชิ้น',
                        'product_id' => 2469,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 10.000,
                    ],
                    [
                        'id' => 6,
                        'item_type' => 'gachapon',
                        'group'=>1,
                        'icon'=>3,
                        'product_title' => 'หินจันทรา 10 ชิ้น',
                        'product_id' => 571,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 5.000,
                    ],
                    [
                        'id' => 7,
                        'item_type' => 'gachapon',
                        'group'=>1,
                        'icon'=>5,
                        'product_title' => 'ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด 1 ชิ้น',
                        'product_id' => 2470,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 5.000,
                    ],
                    [
                        'id' => 8,
                        'item_type' => 'gachapon',
                        'group'=>1,
                        'icon'=>2,
                        'product_title' => 'หินโซล 200 ชิ้น',
                        'product_id' => 2471,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 2.000,
                    ],
                    [
                        'id' => 9,
                        'item_type' => 'gachapon',
                        'group'=>1,
                        'icon'=>3,
                        'product_title' => 'หินจันทรา 100 ชิ้น',
                        'product_id' => 2137,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 1.000,
                    ],
                    [
                        'id' => 10,
                        'item_type' => 'gachapon',
                        'group'=>1,
                        'icon'=>2,
                        'product_title' => 'หินโซล 2000 ชิ้น',
                        'product_id' => 2472,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 1.500,
                    ],
                    [
                        'id' => 11,
                        'item_type' => 'gachapon',
                        'group'=>1,
                        'icon'=>3,
                        'product_title' => 'หินจันทรา 1000 ชิ้น',
                        'product_id' => 2473,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 0.500,
                    ],
                ];
            }elseif($group==2){
                return [
                    [
                        'id' => 1,
                        'item_type' => 'events',
                        'group'=>2,
                        'icon'=>1,
                        'product_title' => 'คูปองกินบุฟเฟ่ต์ฟรี 2 ชิ้น',
                        'product_id' => -1,
                        'product_quantity' => 2,
                        'amount' => 2,
                        'chance' => 20.000,
                    ],
                    [
                        'id' => 2,
                        'item_type' => 'gachapon',
                        'group'=>2,
                        'icon'=>2,
                        'product_title' => 'หินโซล 2 ชิ้น',
                        'product_id' => 2468,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 25.000,
                    ],
                    [
                        'id' => 3,
                        'item_type' => 'gachapon',
                        'group'=>2,
                        'icon'=>3,
                        'product_title' => 'หินจันทรา 1 ชิ้น',
                        'product_id' => 1924,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 15.000,
                    ],
                    [
                        'id' => 4,
                        'item_type' => 'gachapon',
                        'group'=>2,
                        'icon'=>4,
                        'product_title' => 'เกล็ดสีคราม 1 ชิ้น',
                        'product_id' => 2071,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 15.000,
                    ],
                    [
                        'id' => 5,
                        'item_type' => 'gachapon',
                        'group'=>2,
                        'icon'=>2,
                        'product_title' => 'หินโซล 20 ชิ้น',
                        'product_id' => 2469,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 10.000,
                    ],
                    [
                        'id' => 6,
                        'item_type' => 'gachapon',
                        'group'=>2,
                        'icon'=>3,
                        'product_title' => 'หินจันทรา 10 ชิ้น',
                        'product_id' => 571,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 5.000,
                    ],
                    [
                        'id' => 7,
                        'item_type' => 'gachapon',
                        'group'=>2,
                        'icon'=>5,
                        'product_title' => 'ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด 1 ชิ้น',
                        'product_id' => 2470,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 5.000,
                    ],
                    [
                        'id' => 8,
                        'item_type' => 'gachapon',
                        'group'=>2,
                        'icon'=>2,
                        'product_title' => 'หินโซล 200 ชิ้น',
                        'product_id' => 2471,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 2.000,
                    ],
                    [
                        'id' => 9,
                        'item_type' => 'gachapon',
                        'group'=>2,
                        'icon'=>3,
                        'product_title' => 'หินจันทรา 100 ชิ้น',
                        'product_id' => 2137,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 1.000,
                    ],
                    [
                        'id' => 10,
                        'item_type' => 'gachapon',
                        'group'=>2,
                        'icon'=>2,
                        'product_title' => 'หินโซล 2000 ชิ้น',
                        'product_id' => 2472,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 1.500,
                    ],
                    [
                        'id' => 11,
                        'item_type' => 'gachapon',
                        'group'=>2,
                        'icon'=>3,
                        'product_title' => 'หินจันทรา 1000 ชิ้น',
                        'product_id' => 2473,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 0.500,
                    ],
                ];
            }else{
                return [
                    [
                        'id' => 1,
                        'item_type' => 'events',
                        'group'=>3,
                        'icon'=>1,
                        'product_title' => 'คูปองกินบุฟเฟ่ต์ฟรี 1 ชิ้น',
                        'product_id' => -1,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 15.000,
                    ],
                    [
                        'id' => 2,
                        'item_type' => 'gachapon',
                        'group'=>3,
                        'icon'=>2,
                        'product_title' => 'หินโซล 2 ชิ้น',
                        'product_id' => 2468,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 30.000,
                    ],
                    [
                        'id' => 3,
                        'item_type' => 'gachapon',
                        'group'=>3,
                        'icon'=>3,
                        'product_title' => 'หินจันทรา 1 ชิ้น',
                        'product_id' => 1924,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 15.000,
                    ],
                    [
                        'id' => 4,
                        'item_type' => 'gachapon',
                        'group'=>4,
                        'icon'=>4,
                        'product_title' => 'เกล็ดสีคราม 1 ชิ้น',
                        'product_id' => 2071,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 15.000,
                    ],
                    [
                        'id' => 5,
                        'item_type' => 'gachapon',
                        'group'=>3,
                        'icon'=>2,
                        'product_title' => 'หินโซล 20 ชิ้น',
                        'product_id' => 2469,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 10.000,
                    ],
                    [
                        'id' => 6,
                        'item_type' => 'gachapon',
                        'group'=>3,
                        'icon'=>3,
                        'product_title' => 'หินจันทรา 10 ชิ้น',
                        'product_id' => 571,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 5.000,
                    ],
                    [
                        'id' => 7,
                        'item_type' => 'gachapon',
                        'group'=>3,
                        'icon'=>5,
                        'product_title' => 'ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด 1 ชิ้น',
                        'product_id' => 2470,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 5.000,
                    ],
                    [
                        'id' => 8,
                        'item_type' => 'gachapon',
                        'group'=>3,
                        'icon'=>2,
                        'product_title' => 'หินโซล 200 ชิ้น',
                        'product_id' => 2471,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 2.000,
                    ],
                    [
                        'id' => 9,
                        'item_type' => 'gachapon',
                        'group'=>3,
                        'icon'=>3,
                        'product_title' => 'หินจันทรา 100 ชิ้น',
                        'product_id' => 2137,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 1.000,
                    ],
                    [
                        'id' => 10,
                        'item_type' => 'gachapon',
                        'group'=>3,
                        'icon'=>2,
                        'product_title' => 'หินโซล 2000 ชิ้น',
                        'product_id' => 2472,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 1.500,
                    ],
                    [
                        'id' => 11,
                        'item_type' => 'gachapon',
                        'group'=>3,
                        'icon'=>3,
                        'product_title' => 'หินจันทรา 1000 ชิ้น',
                        'product_id' => 2473,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'chance' => 0.500,
                    ],
                ];
            }

        }

        public function setRewardByPackage($packageKey=''){
            if(empty($packageKey)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageKey-1]) && !empty($rewards[$packageKey-1])) ? $rewards[$packageKey-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'ชุดอสูรครามทอง',
                    'package_desc' => 'ชุดอสูรครามทอง',
                    'icon'=>6,
                    'require_token' => 75,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชุดอสูรครามทอง',
                            'product_id' => 2474,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 2,
                    'package_title' => 'เครื่องประดับอสูรครามทอง',
                    'package_desc' => 'เครื่องประดับอสูรครามทอง',
                    'icon'=>7,
                    'require_token' => 35,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'เครื่องประดับอสูรครามทอง',
                            'product_id' => 2475,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 3,
                    'package_title' => 'กล่องอาวุธลวงตาบุปผาอินทนิล',
                    'package_desc' => 'กล่องอาวุธลวงตาบุปผาอินทนิล',
                    'icon'=>8,
                    'require_token' => 20,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'กล่องอาวุธลวงตาบุปผาอินทนิล',
                            'product_id' => 2476,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 4,
                    'package_title' => 'ชุดบุปผาสีคราม',
                    'package_desc' => 'ชุดบุปผาสีคราม',
                    'icon'=>9,
                    'require_token' => 50,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชุดบุปผาสีคราม',
                            'product_id' => 2477,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 5,
                    'package_title' => 'เครื่องประดับผมบุปผาสีคราม',
                    'package_desc' => 'เครื่องประดับผมบุปผาสีคราม',
                    'icon'=>10,
                    'require_token' => 20,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'เครื่องประดับผมบุปผาสีคราม',
                            'product_id' => 2478,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 6,
                    'package_title' => 'เครื่องประดับผมบุปผาราตรี',
                    'package_desc' => 'เครื่องประดับผมบุปผาราตรี',
                    'icon'=>11,
                    'require_token' => 20,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'เครื่องประดับผมบุปผาราตรี',
                            'product_id' => 2479,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 7,
                    'package_title' => 'ชุดกมลพรรณอันทรงเกียรติ',
                    'package_desc' => 'ชุดกมลพรรณอันทรงเกียรติ',
                    'icon'=>12,
                    'require_token' => 50,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชุดกมลพรรณอันทรงเกียรติ',
                            'product_id' => 1631,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 8,
                    'package_title' => 'เครื่องประดับกมลพรรณอันทรงเกียรติ',
                    'package_desc' => 'เครื่องประดับกมลพรรณอันทรงเกียรติ',
                    'icon'=>13,
                    'require_token' => 20,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'เครื่องประดับกมลพรรณอันทรงเกียรติ',
                            'product_id' => 1632,
                            'product_quantity' => 1
                        ]
                    ]
                ],

            ];
        }
    }
