<?php

    namespace App\Models\Test\anniversary_passport;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'anniversary_passport_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'unlocked_passport',
            'last_ip',
        ];

    }