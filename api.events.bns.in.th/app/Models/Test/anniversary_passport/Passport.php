<?php

    namespace App\Models\Test\anniversary_passport;

    class Passport {

        public function getCheckinInfo($week=null,$checkinId){
            if(empty($week) || empty($checkinId)){
                return false;
            }

            $passport = $this->getPassportList();

            $weekPassport = (isset($passport[$week-1]) && !empty($passport[$week-1])) ? $passport[$week-1] : false;

            $checkInList = $weekPassport['checkin_list'];

            return (isset($checkInList[$checkinId-1]) && !empty($checkInList[$checkinId-1])) ? $checkInList[$checkinId-1] : false;

        }

        public function getCheckinSpecialReward($week=null){
            if(empty($week)){
                return false;
            }

            $passport = $this->getPassportList();

            $weekPassport = (isset($passport[$week-1]) && !empty($passport[$week-1])) ? $passport[$week-1] : false;

            return $weekPassport['checkin_special'];

        }

        public function getPassportList(){

            return [
                [
                    'week' => 1,
                    'checkin_special' => [
                        'item_type' => 'checkin_special',
                        'product_title' => 'เหล็กแทชอน',
                        'product_id' => 2272,
                        'product_quantity' => 1,
                        'amount' => 5,
                        'img_key' => 'taecheon_iron',
                    ],
                    'checkin_list' => [
                        [
                            'checkin_id' => 1,
                            'date' => '2019-04-06', // 2019-05-08
                            'date_start' => '2019-04-06 00:00:00', // 2019-05-08 00:00:00
                            'date_end' => '2019-04-06 23:59:59', // 2019-05-08 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'ยันต์รีเซ็ตใช้อีกครั้ง',
                                'product_id' => 2270,
                                'product_quantity' => 1,
                                'amount' => 5,
                                'img_key' => 'reset_charm',
                            ],
                        ],
                        [
                            'checkin_id' => 2,
                            'date' => '2019-04-07', // 2019-05-09
                            'date_start' => '2019-04-07 00:00:00', // 2019-05-09 00:00:00
                            'date_end' => '2019-04-07 23:59:59', // 2019-05-09 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'ยันต์รีเซ็ตใช้อีกครั้ง',
                                'product_id' => 2270,
                                'product_quantity' => 1,
                                'amount' => 5,
                                'img_key' => 'reset_charm',
                            ],
                        ],
                        [
                            'checkin_id' => 3,
                            'date' => '2019-04-08', // 2019-05-10
                            'date_start' => '2019-04-08 00:00:00', // 2019-05-10 00:00:00
                            'date_end' => '2019-04-08 23:59:59', // 2019-05-10 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'ยันต์รีเซ็ตใช้อีกครั้ง',
                                'product_id' => 2270,
                                'product_quantity' => 1,
                                'amount' => 5,
                                'img_key' => 'reset_charm',
                            ],
                        ],
                        [
                            'checkin_id' => 4,
                            'date' => '2019-04-09', // 2019-05-11
                            'date_start' => '2019-04-09 00:00:00', // 2019-05-11 00:00:00
                            'date_end' => '2019-04-09 23:59:59', // 2019-05-11 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'เหรียญซากุระ',
                                'product_id' => 2271,
                                'product_quantity' => 1,
                                'amount' => 5,
                                'img_key' => 'sakura_coin',
                            ],
                        ],
                        [
                            'checkin_id' => 5,
                            'date' => '2019-04-10', // 2019-05-12
                            'date_start' => '2019-04-10 00:00:00', // 2019-05-12 00:00:00
                            'date_end' => '2019-04-10 23:59:59', // 2019-05-12 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'เหรียญซากุระ',
                                'product_id' => 2271,
                                'product_quantity' => 1,
                                'amount' => 5,
                                'img_key' => 'sakura_coin',
                            ],
                        ],
                        [
                            'checkin_id' => 6,
                            'date' => '2019-04-11', // 2019-05-13
                            'date_start' => '2019-04-11 00:00:00', // 2019-05-13 00:00:00
                            'date_end' => '2019-04-11 23:59:59', // 2019-05-13 23:59:59 
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'เหรียญซากุระ',
                                'product_id' => 2271,
                                'product_quantity' => 1,
                                'amount' => 5,
                                'img_key' => 'sakura_coin',
                            ],
                        ],
                        [
                            'checkin_id' => 7,
                            'date' => '2019-04-12', // 2019-05-14
                            'date_start' => '2019-04-12 00:00:00', // 2019-05-14 00:00:00
                            'date_end' => '2019-04-12 23:59:59', // 2019-05-14 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'เหรียญซากุระ',
                                'product_id' => 2271,
                                'product_quantity' => 1,
                                'amount' => 5,
                                'img_key' => 'sakura_coin',
                            ],
                        ],
                    ],
                ],
                [
                    'week' => 2,
                    'checkin_special' => [
                        'item_type' => 'checkin_special',
                        'product_title' => 'เครื่องประดับอินาริ',
                        'product_id' => 2274,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'img_key' => 'inari_headwear',
                    ],
                    'checkin_list' => [
                        [
                            'checkin_id' => 1,
                            'date' => '2019-04-13', // 2019-05-15
                            'date_start' => '2019-04-13 00:00:00', // 2019-05-15 00:00:00
                            'date_end' => '2019-04-13 23:59:59', // 2019-05-15 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'ดวงวิญญาณจ้าวโลกันต์',
                                'product_id' => 2273,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => 'inferno_heart',
                            ],
                        ],
                        [
                            'checkin_id' => 2,
                            'date' => '2019-04-14', // 2019-05-16
                            'date_start' => '2019-04-14 00:00:00', // 2019-05-16 00:00:00
                            'date_end' => '2019-04-14 23:59:59', // 2019-05-16 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'ดวงวิญญาณจ้าวโลกันต์',
                                'product_id' => 2273,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => 'inferno_heart',
                            ],
                        ],
                        [
                            'checkin_id' => 3,
                            'date' => '2019-04-15', // 2019-05-17
                            'date_start' => '2019-04-15 00:00:00', // 2019-05-17 00:00:00
                            'date_end' => '2019-04-15 23:59:59', // 2019-05-17 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'ดวงวิญญาณจ้าวโลกันต์',
                                'product_id' => 2273,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => 'inferno_heart',
                            ],
                        ],
                        [
                            'checkin_id' => 4,
                            'date' => '2019-04-16', // 2019-05-18
                            'date_start' => '2019-04-16 00:00:00', // 2019-05-18 00:00:00
                            'date_end' => '2019-04-16 23:59:59', // 2019-05-18 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'ดวงวิญญาณจ้าวโลกันต์',
                                'product_id' => 2273,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => 'inferno_heart',
                            ],
                        ],
                        [
                            'checkin_id' => 5,
                            'date' => '2019-04-17', // 2019-05-19
                            'date_start' => '2019-04-17 00:00:00', // 2019-05-19 00:00:00
                            'date_end' => '2019-04-17 23:59:59', // 2019-05-19 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'เกล็ดสีคราม',
                                'product_id' => 2071,
                                'product_quantity' => 1,
                                'amount' => 1,
                                'img_key' => 'blue_scale',
                            ],
                        ],
                        [
                            'checkin_id' => 6,
                            'date' => '2019-04-18', // 2019-05-20
                            'date_start' => '2019-04-18 00:00:00', // 2019-05-20 00:00:00
                            'date_end' => '2019-04-18 23:59:59', // 2019-05-20 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'เกล็ดสีคราม',
                                'product_id' => 2071,
                                'product_quantity' => 1,
                                'amount' => 1,
                                'img_key' => 'blue_scale',
                            ],
                        ],
                        [
                            'checkin_id' => 7,
                            'date' => '2019-04-19', // 2019-05-21
                            'date_start' => '2019-04-19 00:00:00', // 2019-05-21 00:00:00
                            'date_end' => '2019-04-19 23:59:59', // 2019-05-21 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'เกล็ดสีคราม',
                                'product_id' => 2071,
                                'product_quantity' => 1,
                                'amount' => 1,
                                'img_key' => 'blue_scale',
                            ],
                        ],
                    ],
                ],
                [
                    'week' => 3,
                    'checkin_special' => [
                        'item_type' => 'checkin_special',
                        'product_title' => 'หินสัตว์เลี้ยง จิ้งจอกอินาริ',
                        'product_id' => 2276,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'img_key' => 'inari_okami_pet',
                    ],
                    'checkin_list' => [
                        [
                            'checkin_id' => 1,
                            'date' => '2019-04-20', // 2019-05-22
                            'date_start' => '2019-04-20 00:00:00', // 2019-05-22 00:00:00
                            'date_end' => '2019-04-20 23:59:59', // 2019-05-22 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินโซล',
                                'product_id' => 2275,
                                'product_quantity' => 1,
                                'amount' => 25,
                                'img_key' => 'soulstone',
                            ],
                        ],
                        [
                            'checkin_id' => 2,
                            'date' => '2019-04-21', // 2019-05-23
                            'date_start' => '2019-04-21 00:00:00', // 2019-05-23 00:00:00
                            'date_end' => '2019-04-21 23:59:59', // 2019-05-23 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินโซล',
                                'product_id' => 2275,
                                'product_quantity' => 1,
                                'amount' => 25,
                                'img_key' => 'soulstone',
                            ],
                        ],
                        [
                            'checkin_id' => 3,
                            'date' => '2019-04-22', // 2019-05-24
                            'date_start' => '2019-04-22 00:00:00', // 2019-05-24 00:00:00
                            'date_end' => '2019-04-22 23:59:59', // 2019-05-24 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินโซล',
                                'product_id' => 2275,
                                'product_quantity' => 1,
                                'amount' => 25,
                                'img_key' => 'soulstone',
                            ],
                        ],
                        [
                            'checkin_id' => 4,
                            'date' => '2019-04-23', // 2019-05-25
                            'date_start' => '2019-04-23 00:00:00', // 2019-05-25 00:00:00
                            'date_end' => '2019-04-23 23:59:59', // 2019-05-25 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินโซล',
                                'product_id' => 2275,
                                'product_quantity' => 1,
                                'amount' => 25,
                                'img_key' => 'soulstone',
                            ],
                        ],
                        [
                            'checkin_id' => 5,
                            'date' => '2019-04-24', // 2019-05-26
                            'date_start' => '2019-04-24 00:00:00', // 2019-05-26 00:00:00
                            'date_end' => '2019-04-24 23:59:59', // 2019-05-26 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินโซล',
                                'product_id' => 2275,
                                'product_quantity' => 1,
                                'amount' => 25,
                                'img_key' => 'soulstone',
                            ],
                        ],
                        [
                            'checkin_id' => 6,
                            'date' => '2019-04-25', // 2019-05-27
                            'date_start' => '2019-04-25 00:00:00', // 2019-05-27 00:00:00
                            'date_end' => '2019-04-25 23:59:59', // 2019-05-27 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินโซล',
                                'product_id' => 2275,
                                'product_quantity' => 1,
                                'amount' => 25,
                                'img_key' => 'soulstone',
                            ],
                        ],
                        [
                            'checkin_id' => 7,
                            'date' => '2019-04-26', // 2019-05-28
                            'date_start' => '2019-04-26 00:00:00', // 2019-05-28 00:00:00
                            'date_end' => '2019-04-26 23:59:59', // 2019-05-28 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินโซล',
                                'product_id' => 2275,
                                'product_quantity' => 1,
                                'amount' => 25,
                                'img_key' => 'soulstone',
                            ],
                        ],
                    ],
                ],
                [
                    'week' => 4,
                    'checkin_special' => [
                        'item_type' => 'checkin_special',
                        'product_title' => 'ชุดอินาริ',
                        'product_id' => 2278,
                        'product_quantity' => 1,
                        'amount' => 1,
                        'img_key' => 'inari_outfit',
                    ],
                    'checkin_list' => [
                        [
                            'checkin_id' => 1,
                            'date' => '2019-04-27', // 2019-05-29
                            'date_start' => '2019-04-27 00:00:00', // 2019-05-29 00:00:00
                            'date_end' => '2019-04-27 23:59:59', // 019-05-29 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินจันทรา',
                                'product_id' => 2277,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => 'moonstone',
                            ],
                        ],
                        [
                            'checkin_id' => 2,
                            'date' => '2019-04-28', // 2019-05-30
                            'date_start' => '2019-04-28 00:00:00', // 2019-05-30 00:00:00
                            'date_end' => '2019-04-28 23:59:59', // 2019-05-30 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินจันทรา',
                                'product_id' => 2277,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => 'moonstone',
                            ],
                        ],
                        [
                            'checkin_id' => 3,
                            'date' => '2019-04-29', // 2019-05-31
                            'date_start' => '2019-04-29 00:00:00', // 2019-05-31 00:00:00
                            'date_end' => '2019-04-29 23:59:59', // 2019-05-31 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินจันทรา',
                                'product_id' => 2277,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => 'moonstone',
                            ],
                        ],
                        [
                            'checkin_id' => 4,
                            'date' => '2019-04-30', // 2019-06-01
                            'date_start' => '2019-04-30 00:00:00', // 2019-06-01 00:00:00
                            'date_end' => '2019-04-30 23:59:59', // 2019-06-01 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินจันทรา',
                                'product_id' => 2277,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => 'moonstone',
                            ],
                        ],
                        [
                            'checkin_id' => 5,
                            'date' => '2019-05-01', // 2019-06-02
                            'date_start' => '2019-05-01 00:00:00', // 2019-06-02 00:00:00
                            'date_end' => '2019-05-01 23:59:59', // 2019-06-02 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินจันทรา',
                                'product_id' => 2277,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => 'moonstone',
                            ],
                        ],
                        [
                            'checkin_id' => 6,
                            'date' => '2019-05-02', // 2019-06-03
                            'date_start' => '2019-05-02 00:00:00', // 2019-06-03 00:00:00
                            'date_end' => '2019-05-02 23:59:59', // 2019-06-03 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินจันทรา',
                                'product_id' => 2277,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => 'moonstone',
                            ],
                        ],
                        [
                            'checkin_id' => 7,
                            'date' => '2019-05-03', // 2019-06-04
                            'date_start' => '2019-05-03 00:00:00', // 2019-06-04 00:00:00
                            'date_end' => '2019-05-03 23:59:59', // 2019-06-04 23:59:59
                            'reward' => [
                                'item_type' => 'checkin',
                                'product_title' => 'หินจันทรา',
                                'product_id' => 2277,
                                'product_quantity' => 1,
                                'amount' => 2,
                                'img_key' => 'moonstone',
                            ],
                        ],
                    ],
                ],
            ];

        }

    }