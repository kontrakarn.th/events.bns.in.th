<?php

    namespace App\Models\Test\anniversary_passport;

    use App\Models\BaseModel;

    class DeductLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'anniversary_passport_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'week',
            'checkin_id',
            'deduct_type', // checkin, unlock_package
            'diamonds',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip'
        ];

    }
    