<?php

    namespace App\Models\Test\lunar_newyear;

    use App\Models\BaseModel;

    class GoldDeductLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'lunar_newyear_gold_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'gold',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }