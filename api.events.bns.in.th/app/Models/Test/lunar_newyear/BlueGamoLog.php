<?php

    namespace App\Models\Test\lunar_newyear;

    use App\Models\BaseModel;

    class BlueGamoLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'lunar_newyear_blue_gamo_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'world_id',
            'job',
            'quest_id',
            'quest_code',
            'quest_title',
            'blue_gamo', // blue gamo quantity
            'status', // success, unsuccess
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }