<?php

    namespace App\Models\Test\lunar_newyear;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'lunar_newyear_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'world_id',
            'job',
            'last_ip',
        ];

    }