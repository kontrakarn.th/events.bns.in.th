<?php

    namespace App\Models\Test\lunar_newyear;

    use App\Models\BaseModel;

    class RedGamoDeductLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'lunar_newyear_red_gamo_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'status', // success, unsuccess
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }