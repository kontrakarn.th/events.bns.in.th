<?php

    namespace App\Models\Test\lunar_newyear;

    use App\Models\BaseModel;

    class TestScore extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'lunar_newyear_2019_score_test';
        protected $fillable = [
            'uid',
            'username',
            'score',
        ];

    }