<?php

namespace App\Models\Test\battle_royale;

use App\Models\BaseModel;

class TournamentAppliedCharacters extends BaseModel
{

    protected $connection = 'esports_bns_test';
    protected $table = 'esports_tournament_applied_characters';
}
