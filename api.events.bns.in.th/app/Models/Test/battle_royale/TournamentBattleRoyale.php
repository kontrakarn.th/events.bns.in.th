<?php

namespace App\Models\Test\battle_royale;

use App\Models\BaseModel;

class TournamentBattleRoyale extends BaseModel
{

    protected $connection = 'esports_bns_test';
    protected $table = 'esports_tournament_battle_royale';
}
