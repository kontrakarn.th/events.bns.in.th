<?php

namespace App\Models\Test\blue_jeans;

use App\Models\BaseModel;

class Reward extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'blue_jeans_rewards';
    protected $fillable = [
        'reward_type',
        'package_name',
        'package_id',
        'package_quantity',
        'package_amount',
        'package_type',
        'package_key',
        'chance',
        'prop',
        'image',
        'order',
        'available_date',
    ];
}
