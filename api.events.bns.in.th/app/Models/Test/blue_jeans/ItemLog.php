<?php

namespace App\Models\Test\blue_jeans;

use App\Models\BaseModel;

class ItemLog extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'blue_jeans_item_logs';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'history_id',
        'send_item_status',
        'send_item_purchase_id',
        'send_item_purchase_status',
        'goods_data',
        'status',
        'log_date',
        'log_date_timestamp',
        'last_ip',
    ];
}
