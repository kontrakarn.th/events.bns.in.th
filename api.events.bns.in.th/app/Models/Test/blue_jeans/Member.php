<?php

namespace App\Models\Test\blue_jeans;

use App\Models\BaseModel;

class Member extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'blue_jeans_members';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'last_ip',
        'group',
    ];
}
