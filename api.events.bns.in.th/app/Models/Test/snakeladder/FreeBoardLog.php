<?php

    namespace App\Models\Test\snakeladder;

    use App\Models\BaseModel;

    class FreeBoardLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'snakeladder_free_board_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'points',
            'points_title',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }