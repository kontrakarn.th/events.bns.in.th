<?php

    namespace App\Models\Test\snakeladder;

    class PaidBoard {

        // Random Dice
        public function randomBoard(){

            $boardRate = $this->getBoardRate();

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $board = false;

            foreach ($boardRate as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired board
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    // $value['color'] = $this->randomColor();
                    $board = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $board['random'] = $random;

            $board['timestamp'] = time();

            // now we have the board
            return $board;
        }

        private function getBoardRate(){
            return [
                [
                    'id' => 1,
                    'points_title' => 'ชุดสง่างาม',
                    'chance' => 25.000,
                ],
                [
                    'id' => 2,
                    'points_title' => 'ชุดขาวบริสุทธิ์',
                    'chance' => 15.000,
                ],
                [
                    'id' => 3,
                    'points_title' => 'ผีเสื้อสีชาด',
                    'chance' => 25.000,
                ],
                [
                    'id' => 4,
                    'points_title' => 'ท่วงทำนองบริสุทธิ์',
                    'chance' => 17.075,
                ],
                [
                    'id' => 5,
                    'points_title' => 'ท่วงทำนองเสียงประสาน',
                    'chance' => 17.075,
                ],
                [
                    'id' => 6,
                    'points_title' => 'เทพพิทักษ์',
                    'chance' => 0.250,
                ],
                [
                    'id' => 7,
                    'points_title' => 'เซตกุหลาบพิษ',
                    'chance' => 0.250,
                ],
                [
                    'id' => 8,
                    'points_title' => 'เสือขาว',
                    'chance' => 0.250,
                ],
                [
                    'id' => 9,
                    'points_title' => 'เซตแสงที่เจิดจ้า',
                    'chance' => 0.100,
                ],
            ];
        }

    }