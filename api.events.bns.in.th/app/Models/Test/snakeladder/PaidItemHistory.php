<?php

    namespace App\Models\Test\snakeladder;

    use App\Models\BaseModel;

    class PaidItemHistory extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'snakeladder_paid_item_history';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'board_paid_id',
            'board_step',
            'product_id',
            'product_title',
            'product_quantity',
            'amount',
            'path_type',
            'item_type',
            'item_group',
            'send_type',
            'send_to_uid',
            'send_to_name',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }