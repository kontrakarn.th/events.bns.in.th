<?php

    namespace App\Models\Test\snakeladder;

    use App\Models\BaseModel;

    class PaidBoardLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'snakeladder_paid_board_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'deduct_id',
            'board_id',
            'board_title',
            'selected', // 0 = no, 1 = yes
            'play_status', // 'pending', 'in_progress','completed','rejected'
            'status', // 'pending','success','reject'
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }