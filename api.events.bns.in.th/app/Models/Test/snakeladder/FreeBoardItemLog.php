<?php

    namespace App\Models\Test\snakeladder;

    use App\Models\BaseModel;

    class FreeBoardItemLog extends BaseModel {

        protected $connection = 'events_bns_test';
        protected $table = 'snakeladder_free_board_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'board_step',
            'product_title',
            'product_set',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'type',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    