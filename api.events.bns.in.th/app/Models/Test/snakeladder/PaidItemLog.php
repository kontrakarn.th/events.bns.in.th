<?php

    namespace App\Models\Test\snakeladder;

    use App\Models\BaseModel;

    class PaidItemLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'snakeladder_paid_send_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'send_gift_from',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }