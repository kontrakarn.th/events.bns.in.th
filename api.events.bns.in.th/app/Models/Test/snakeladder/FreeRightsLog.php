<?php

    namespace App\Models\Test\snakeladder;

    use App\Models\BaseModel;

    class FreeRightsLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'snakeladder_free_rights_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'used_status',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }