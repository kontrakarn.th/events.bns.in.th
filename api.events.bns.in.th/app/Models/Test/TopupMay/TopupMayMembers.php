<?php

namespace App\Models\Test\TopupMay;

use Illuminate\Database\Eloquent\Model;

class TopupMayMembers extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'topup_may_members';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'total_diamonds',
        'last_ip'
    ];
}
