<?php

namespace App\Models\Test\TopupMay;

use Illuminate\Database\Eloquent\Model;

class TopupMayRedeemLogs extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'topup_may_redeem_logs';
    protected $fillable = [
        'member_id',
        'bonus_type'
    ];


}
