<?php

    namespace App\Models\Test\bmnov;

    use Moloquent;

    /**
     * Description of SendItemLog
     *
     * @author naruebaetbouhom
     */
    class SendItemLog extends Moloquent {
        protected $connection = 'mongodb_bns_test';
        protected $table = 'bmnov_send_item_log';
        protected $fillable = [
            'uid',
            'ncid',
            'log_id',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'last_ip',
            'type',
            'send_gift_from',
            'log_timestamp'
        ];

    }
