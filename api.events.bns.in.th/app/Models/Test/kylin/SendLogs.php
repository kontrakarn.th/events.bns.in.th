<?php

namespace App\Models\Test\kylin;

use Illuminate\Database\Eloquent\Model;

class SendLogs extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'kylin_sendlogs';
    protected $fillable = [
        'transaction_id',
        'uid',
        'ncid',
        'username',
        'item_type',
        'item_no',
        'product_title',
        'product_id',
        'product_quantity',
        'item_price',
        'status',
        'send_item_purchase_id',
        'send_item_purchase_status',
        'goods_data',
        'item_status',
        'last_ip',
    ];
}
