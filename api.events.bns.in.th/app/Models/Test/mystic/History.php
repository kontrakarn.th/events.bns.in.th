<?php

namespace App\Models\Test\mystic;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'mystic_history';
    protected $fillable = [
        'history_type_id',
        'product_id',
        'get_amount',
        'get_date_string',
        'get_time_string',
        'key_id',
        'last_ip'

    ];

    public function SaveHistory($product_or_key_id,$history_type_id,$amount){
        $save = New History;
        $save->history_type_id = $history_type_id;
        $save->product_or_key_id = $product_or_key_id;
        $save->amount = $amount;
        $save->save();
    }

    public function HistoryType(){
        return $this->belongsTo('App\Models\Test\mystic\HistoryType','history_type_id');
    }
    public function KeyPackage(){
        return $this->belongsTo('App\Models\Test\mystic\KeyPackage','key_id');
    }
    public function Product(){
        return $this->belongsTo('App\Models\Test\mystic\Product','product_id');
    }
}
