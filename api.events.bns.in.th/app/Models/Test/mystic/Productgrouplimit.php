<?php

namespace App\Models\Test\mystic;

use Illuminate\Database\Eloquent\Model;

class Productgrouplimit extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'mystic_product_group_limit';
    protected $fillable = [
        'id',
        'uid',
        'product_group_id',
        'product_id',
    ];
    public function Product(){
        return $this->belongsTo('App\Models\Test\mystic\Product','product_id');
    }
}
