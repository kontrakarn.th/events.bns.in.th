<?php

namespace App\Models\Test\mystic;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'mystic_users';
    protected $fillable = [
        'id',
        'uid',
        'username',
        'ncid',
        'char_id',
        'char_name',
        'world_id',
        'job',
        'last_ip',
        'hongmoon_key',
        'user_type',

    ];

}
