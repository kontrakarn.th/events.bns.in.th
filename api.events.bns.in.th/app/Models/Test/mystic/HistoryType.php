<?php

namespace App\Models\Test\mystic;

use Illuminate\Database\Eloquent\Model;

class HistoryType extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'mystic_history_type';
    protected $fillable = [
        'id',
        'name',
        'table_name',
        'model_name',
    ];
}
