<?php

namespace App\Models\Test\mystic;

use Illuminate\Database\Eloquent\Model;

class KeyPackage extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'mystic_key_package';
    protected $fillable = [
        'id',
        'name',
        'amount',
        'bonus',
        'daimonds_price',
    ];

}
