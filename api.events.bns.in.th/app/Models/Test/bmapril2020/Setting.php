<?php

    namespace App\Models\Test\bmapril2020;

    use App\Models\BaseModel;

    class Setting extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'bmapril2020_setting';
        protected $fillable = [
            'event_start',
            'event_end',
            'gachapon_start',
            'gachapon_end',
            'exchange_start',
            'exchange_end',
            'diamonds_require',
            'service_charge',
            'active',
        ];

    }