<?php

    namespace App\Models\Test\bmapril2020;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'bmapril2020_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'last_ip',
        ];

    }