<?php

    namespace App\Models\Test\bmapril2020;

    use App\Models\BaseModel;

    class Reward extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'bmapril2020_rewards';
        protected $fillable = [
            'package_name',
            'package_id',
            'package_quantity',
            'package_amount',
            'package_type', // materials, rare_items
            'package_key',
            'chance',
            'image',
            'rare_item_order',
            'available_date',
        ];
    }