<?php

    namespace App\Models\Test\airpay_jan2019;

    use App\Models\BaseModel;

    class RightsLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'airpay_jan2019_rights_log';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'used_status', // not_use, used
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }