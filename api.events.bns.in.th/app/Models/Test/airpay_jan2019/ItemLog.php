<?php

    namespace App\Models\Test\airpay_jan2019;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {

        protected $connection = 'events_bns_test';
        protected $table = 'airpay_jan2019_send_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'reward_id',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'type', // 'airpay_gachapon','exchange_points'
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    