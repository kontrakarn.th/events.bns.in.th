<?php

    namespace App\Models\Test\airpay_jan2019;

    use App\Models\BaseModel;

    class ItemHistory extends BaseModel {

        protected $connection = 'events_bns_test';
        protected $table = 'airpay_jan2019_item_history';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'reward_id',
            'product_id',
            'product_title',
            'product_quantity',
            'item_type', // 'airpay_gachapon','z','exchange_points'
            'status', // pending, success, unsuccess
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    