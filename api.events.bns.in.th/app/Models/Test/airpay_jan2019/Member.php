<?php

    namespace App\Models\Test\airpay_jan2019;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'airpay_jan2019_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'total_diamonds',
            'last_ip',
        ];

    }