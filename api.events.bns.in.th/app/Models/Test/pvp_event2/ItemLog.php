<?php

namespace App\Models\Test\pvp_event2;

use App\Models\BaseModel;

class ItemLog extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'pvp_event2_item_log';
    public $timestamps      = true;
    const STATUS_FLAG_OK        = 'OK';
    const STATUS_FLAG_FAIL   = 'FAIL';
}
