<?php

namespace App\Models\Test\pvp_event2;

use App\Models\BaseModel;

class Item extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'pvp_event2_item';
}
