<?php

namespace App\Models\Test\pvp_event2;

use App\Models\BaseModel;

class User extends BaseModel
{
    protected $connection   = 'events_bns_test';
    protected $table        = 'pvp_event2_user';
    public $timestamps      = true;
    const STATUS_FLAG_NONE      = 'NONE';
    const STATUS_FLAG_OK        = 'OK';
    const STATUS_FLAG_PENDING   = 'PENDING';
}
