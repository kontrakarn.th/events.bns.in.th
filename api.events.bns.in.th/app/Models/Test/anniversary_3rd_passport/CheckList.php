<?php

    namespace App\Models\Test\anniversary_3rd_passport;

    use App\Models\BaseModel;

    class CheckList extends BaseModel {

        protected $connection = 'events_bns_test';
        protected $table = 'anniversary_3rd_passport_checklist';
        protected $fillable = [
            'checkin_date',
            'week',
            'day',
            'package_key',
        ];

    }
    