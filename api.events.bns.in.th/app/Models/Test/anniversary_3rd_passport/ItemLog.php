<?php

    namespace App\Models\Test\anniversary_3rd_passport;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {

        protected $connection = 'events_bns_test';
        protected $table = 'anniversary_3rd_passport_send_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'week',
            'day',
            'package_name',
            'package_id',
            'package_quantity',
            'package_amount',
            'package_type',
            'package_key',
            'image',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    