<?php

    namespace App\Models\Test\anniversary_3rd_passport;

    use App\Models\BaseModel;

    class Setting extends BaseModel {

        protected $connection = 'events_bns_test';
        protected $table = 'anniversary_3rd_passport_setting';
        protected $fillable = [
            'event_start',
            'event_end',
            'checkin_start',
            'checkin_end',
            'num_week',
            'num_days',
            'diamonds_unlock',
            'diamonds_checkin',
            'available_date',
            'active',
        ];

    }
    