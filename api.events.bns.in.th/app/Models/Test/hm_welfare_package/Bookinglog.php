<?php

    namespace App\Models\Test\hm_welfare_package;

    use App\Models\BaseModel;

    class Bookinglog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'hm_welfare_package_bookinglogs';
    }