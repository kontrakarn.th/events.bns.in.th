<?php

    namespace App\Models\Test\daily_login;

    use App\Models\BaseModel;

    class Setting extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'daily_login_setting';
        protected $fillable = [
            'event_start',
            'event_end',
            'min_hm_level_required',
            'min_create_char_date',
            'login_days',
            'active',
        ];

    }