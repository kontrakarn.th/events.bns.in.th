<?php

namespace App\Models\Test\daily_login;

use App\Models\BaseModel;

class LastLoginList extends BaseModel
{

    protected $connection = 'events_bns_test';
    protected $table = 'daily_login_last_login_list';
    protected $fillable = [
        'uid'
    ];
}
