<?php

namespace App\Models\Test\daily_login;

use App\Models\BaseModel;

class CharacterLog extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'daily_login_character_logs';
    protected $fillable = [
        'member_id',
        'name',
        'world_id',
        'mastery_level',
        'level',
        'last_play_start',
        'last_play_end',
        'last_ip',
        'job',
        'exp',
        'creation_time',
        'remark'
    ];
}
