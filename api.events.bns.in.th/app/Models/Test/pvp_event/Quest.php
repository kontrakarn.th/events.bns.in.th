<?php

    namespace App\Models\Test\pvp_event;

    use App\Models\BaseModel;

    class Quest extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'pvp_event_quests';
        protected $fillable = [
            'quest_title',
            'quest_code',
            'quest_points',
            'quest_type',
            'quest_limit',
            'image',
        ];
    }

