<?php

    namespace App\Models\Test\pvp_event;

    use App\Models\BaseModel;

    class QuestDailyLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'pvp_event_quest_daily_log';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'quest_title',
            'quest_code',
            'quest_points',
            'quest_type',
            'image',
            'completed_count',
            'total_points',
            'log_date',
            'log_date_timestamp',
            'last_ip'
        ];
    }

