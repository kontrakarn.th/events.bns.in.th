<?php

    namespace App\Models\Test\pvp_event;

    use App\Models\BaseModel;

    class QuestMemberLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'pvp_event_quest_member_log';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'quest_title',
            'quest_code',
            'quest_points',
            'quest_type', // quest, boss_kill
            'image',
            'completed_count',
            'total_points',
            'last_ip'
        ];
    }

