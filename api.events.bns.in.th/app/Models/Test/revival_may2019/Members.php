<?php

namespace App\Models\Test\revival_may2019;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Members extends Model
{
    use SoftDeletes;

    protected $connection = 'events_bns_test';
    protected $table = 'revival_may2019_members';
    protected $fillable = [
        'uid',
        'ncid',
        'username',
        'last_ip',
    ];
    protected $dates = ['deleted_at'];

    protected static function boot(){
        parent::boot();

        static::saving(function($model){
            $model->last_ip = \Request::ip();
        });
    }
}
