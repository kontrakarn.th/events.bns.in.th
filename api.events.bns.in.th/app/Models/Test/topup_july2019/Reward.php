<?php
    // Mission for disciple
    namespace App\Models\Test\topup_july2019;

    class Reward {

        public function setRewardByPackage($packageKey=''){
            if(empty($packageKey)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageKey-1]) && !empty($rewards[$packageKey-1])) ? $rewards[$packageKey-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'Topup 30,000 Diamonds',
                    'package_desc' => 'อัญมณี หินพิทักษ์ทะเลสาบ,หินโซล,หินจันทรา',
                    'require_diamonds' => 30000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'Topup 30,000 Diamonds',
                            'product_id' => 2422,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 2,
                    'package_title' => 'Topup 50,000 Diamonds',
                    'package_desc' => 'ไหมห้าสี,เกล็ดสีคราม,ลูกแก้วยอดนักรบ,กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์)',
                    'require_diamonds' => 50000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'Topup 50,000 Diamonds',
                            'product_id' => 2423,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 3,
                    'package_title' => 'Topup 100,000 Diamonds',
                    'package_desc' => 'ชุดท้องฟ้าไร้เมฆา,เครื่องประดับเมฆาลิขิต',
                    'require_diamonds' => 100000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'Topup 100,000 Diamonds',
                            'product_id' => 2424,
                            'product_quantity' => 1
                        ]
                    ]
                ],
            ];
        }
    }
