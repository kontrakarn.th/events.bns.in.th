<?php

namespace App\Models\Test\hm_pass_ss2;

use App\Models\BaseModel;

class Quest extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'hm_pass_ss2_quests';
    protected $fillable = [
        'quest_period', // 'daily','weekly'
        'day',
        'quest_title',
        'quest_dungeon',
        'quest_code',
        'quest_type', // 'xp_gain','bosskill','area','battlefield','1v1','3v3'
        'quest_limit',
        'xp',
        'quest_points',
    ];
}
