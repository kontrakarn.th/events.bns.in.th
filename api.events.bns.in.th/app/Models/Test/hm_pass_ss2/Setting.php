<?php

namespace App\Models\Test\hm_pass_ss2;

use App\Models\BaseModel;

class Setting extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'hm_pass_ss2_settings';
    protected $fillable = [
        'event_start',
        'event_end',
        'special_gift_start',
        'special_gift_end',
        'unlocked_diamonds',
        'unlocked_adv_diamonds',
        'purchased_lvl_diamonds',
        'active',
    ];
}
