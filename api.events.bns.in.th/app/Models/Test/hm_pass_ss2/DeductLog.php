<?php

namespace App\Models\Test\hm_pass_ss2;

use App\Models\BaseModel;

class DeductLog extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'hm_pass_ss2_deduct_logs';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'deduct_type',
        'diamonds',
        'before_deduct_diamond',
        'after_deduct_diamond',
        'deduct_status',
        'deduct_purchase_id',
        'deduct_purchase_status',
        'deduct_data',
        'status',
        'last_ip',
        'log_date',
        'log_date_timestamp',
        'purchased_lvl',
        'purchased_at_lvl',
    ];
}
