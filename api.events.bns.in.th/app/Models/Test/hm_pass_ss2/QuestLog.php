<?php

namespace App\Models\Test\hm_pass_ss2;

use App\Models\BaseModel;
use App\Models\Test\hm_pass_ss2\Quest;
use App\Models\Test\hm_pass_ss2\Member;

class QuestLog extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'hm_pass_ss2_quest_logs';
    protected $fillable = [
        'uid',
        'char_id',
        'quest_id',
        'completed_count',
        'status', // pending, success
        'log_date',
        'log_date_timestamp',
        'quest_complete_datetime',
        'last_ip',
    ];

    public function quest()
    {
        return $this->belongsTo(Quest::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'uid', 'uid');
    }
}
