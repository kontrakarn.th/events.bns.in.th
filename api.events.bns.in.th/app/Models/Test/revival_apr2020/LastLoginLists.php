<?php

namespace App\Models\Test\revival_apr2020;

use Illuminate\Database\Eloquent\Model;

class LastLoginLists extends Model
{

    protected $connection = 'events_bns_test';
    protected $table = 'revival_apr2020_last_login_list';
    protected $fillable = [
        'uid'
    ];
}
