<?php

namespace App\Models\Test\soul_gachapon_valentine_2019;

use Illuminate\Database\Eloquent\Model;

class sgv_send_item_log extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'sgv_send_item_logs';
    protected $fillable = [
        'uid',
        'ncid',
        'status',
        'send_item_status',
        'send_item_purchase_id',
        'send_item_purchase_status',
        'goods_data',
        'last_ip',
        'type',
        'log_date',
        'log_date_timestamp',
        'send_gift_from'
    ];
}
