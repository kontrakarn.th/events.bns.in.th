<?php

namespace App\Models\Test\soul_gachapon_valentine_2019;

use Illuminate\Database\Eloquent\Model;

class sgv_deduct_log extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'sgv_deduct_logs';
    protected $fillable = [
        'status',
        'diamond',
        'uid',
        'ncid',
        'last_ip',
        'deduct_type',
        'log_date',
        'log_date_timestamp',
        'before_deduct_diamond',
        'after_deduct_diamond',
        'deduct_status',
        'deduct_purchase_id',
        'deduct_purchase_status',
        'deduct_data'
    ];

    protected $dates = [
        'log_date'
    ];
}
