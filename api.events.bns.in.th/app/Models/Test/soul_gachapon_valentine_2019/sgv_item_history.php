<?php

namespace App\Models\Test\soul_gachapon_valentine_2019;

use Illuminate\Database\Eloquent\Model;

class sgv_item_history extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'sgv_item_histories';
    protected $fillable = [
        'product_id',
        'product_title',
        'product_quantity',
        'uid',
        'ncid',
        'item_type', // 'material','object','rare_item'
        'status',
        'last_ip',
        'icon',
        'gachapon_type', // 'ten','one','merge','redeem','airpay'
        'product_set',
        'product_pack_data',
        'send_type', // 'owner','gift','not_send','feather'
        'send_to_uid',
        'send_to_name',
        'log_date',
        'log_date_timestamp'
    ];
    protected $dates = ['log_date'];
}
