<?php

namespace App\Models\Test\easymode;

use App\Models\BaseModel;

class DeductLog extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'easymode_deduct_logs';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'week',
        'day',
        'deduct_type', // daily_purchase_points
        'diamonds',
        'before_deduct_diamond',
        'after_deduct_diamond',
        'deduct_status',
        'deduct_purchase_id',
        'deduct_purchase_status',
        'deduct_data',
        'status',
        'last_ip',
        'log_date',
        'log_date_timestamp',
    ];
}
