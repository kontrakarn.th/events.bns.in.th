<?php

namespace App\Models\Test\easymode;

use App\Models\BaseModel;

class Setting extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'easymode_settings';
    protected $fillable = [
        'event_start',
        'event_end',
        'open_box_start',
        'open_box_end',
        'claim_reward_start',
        'claim_reward_end',
        'active',
    ];
}
