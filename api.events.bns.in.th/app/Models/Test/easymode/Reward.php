<?php

namespace App\Models\Test\easymode;

use App\Models\BaseModel;

class Reward extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'easymode_rewards';
    protected $fillable = [
        'package_name',
        'package_id',
        'package_quantity',
        'package_amount',
        'package_type', // diamonds, materials
        'package_key',
        'image',
        'available_date',
    ];
}
