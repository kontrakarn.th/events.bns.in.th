<?php

namespace App\Models\Test\easymode;

use App\Models\BaseModel;

class ItemLog extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'easymode_item_logs';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'package_key',
        'package_name',
        'package_quantity',
        'package_amount',
        'package_type',
        'send_item_status',
        'send_item_purchase_id',
        'send_item_purchase_status',
        'goods_data',
        'status',
        'log_date',
        'log_date_timestamp',
        'last_ip'
    ];
}
