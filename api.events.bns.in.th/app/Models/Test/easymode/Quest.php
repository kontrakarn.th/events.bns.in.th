<?php

namespace App\Models\Test\easymode;

use App\Models\BaseModel;

class Quest extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'easymode_quests';
    protected $fillable = [
        'quest_dungeon',
        'quest_boss',
        'quest_code',
        'quest_id',
        'quest_type',
        'quest_limit',
        'quest_points',
        'image',
    ];
}
