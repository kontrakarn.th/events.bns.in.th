<?php

namespace App\Models\Test\easymode;

use App\Models\BaseModel;

class Member extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'easymode_members';
    protected $casts = [
        'already_purchased' => 'boolean',
    ];
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'char_id',
        'char_name',
        'world_id',
        'total_points',
        'used_points',
        'total_tokens',
        'used_tokens',
        'already_purchased',
        'last_ip',
    ];

    public function getRemainingPointsAttribute()
    {
        $totalPoints = $this->total_points;
        $usedPoints = $this->used_points;

        if ($totalPoints < $usedPoints) {
            return 0;
        }

        return $totalPoints - $usedPoints;
    }

    public function getRemainingTokensAttribute()
    {
        $totalTokens = $this->total_tokens;
        $usedTokens = $this->used_tokens;

        if ($totalTokens < $usedTokens) {
            return 0;
        }

        return $totalTokens - $usedTokens;
    }
}
