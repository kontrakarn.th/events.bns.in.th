<?php

    namespace App\Models\Test\buffet_v2;

    use Moloquent;

    class Gachapon extends Moloquent {
        protected $connection = 'mongodb_bns_test';
        protected $table = 'buffet_v2_gachapon';
        protected $fillable = [
            'id',
            'item_type',
            'group',
            'icon',
            'product_title',
            'product_id',
            'product_quantity',
            'amount',
            'chance'
        ];
    }
