<?php

    namespace App\Models\Test\buffet_v2;

    use Moloquent;

    class Package extends Moloquent {
        protected $connection = 'mongodb_bns_test';
        protected $table = 'buffet_v2_package';
        protected $fillable = [
            'package_id',
            'package_title',
            'package_desc',
            'icon',
            'require_token',
            'product_set',
        ];
    }
