<?php

namespace App\Models\Test\third_spec_assassin;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'third_spec_assassin_history';
    protected $fillable = [
        'history_type_id',
        'product_id',
        'get_amount',
        'get_date_string',
        'get_time_string',
        'key_id',
        'last_ip'

    ];


    public function Product(){
        return $this->belongsTo('App\Models\Test\third_spec_assassin\Product','product_id');
    }
}
