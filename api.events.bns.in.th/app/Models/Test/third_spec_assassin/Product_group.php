<?php

namespace App\Models\Test\third_spec_assassin;

use Illuminate\Database\Eloquent\Model;

class Product_group extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'third_spec_assassin_product_group';
    protected $fillable = [
        'id',
        'name',
        'rate_group',
        'get_amount',
        'report_column',
    ];
    public function Product(){
        return $this->Hasmany('App\Models\Test\third_spec_assassin\Product','product_group_id');
    }
}
