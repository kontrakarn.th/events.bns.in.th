<?php

namespace App\Models\Test\third_spec_assassin;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'third_spec_assassin_users';
    protected $fillable = [
        'id',
        'uid',
        'username',
        'ncid',
        'char_id',
        'char_name',
        'world_id',
        'job',
        'last_ip',
        'can_get_package',
    ];

}
