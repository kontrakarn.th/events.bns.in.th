<?php

namespace App\Models\Test\hardmode;

use App\Models\BaseModel;

class History extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'hardmode_histories';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'item_log_id',
        'reward_id',
        'reward_name',
        'reward_type',
        'type',
        'reward_limit',
        'log_date',
        'log_date_timestamp',
        'last_ip'
    ];
}
