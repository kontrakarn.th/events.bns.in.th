<?php

namespace App\Models\Test\hardmode;

use App\Models\BaseModel;

class QuestDailyLog extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'hardmode_quest_daily_logs';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'char_id',
        'char_name',
        'quest_id',
        'quest_dungeon',
        'quest_title',
        'quest_code',
        'quest_type',
        'quest_limit',
        'quest_points',
        'image',
        'completed_count',
        'claimed_count',
        'status', // pending, success
        'claim_status', // pending, claimed
        'log_date',
        'log_date_timestamp',
        'quest_complete_datetime',
        'last_ip',
        'created_at',
        'updated_at',
    ];
}
