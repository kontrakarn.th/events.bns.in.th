<?php

    namespace App\Models\Test\hello_summer;

    use App\Models\BaseModel;

    class WaterLogs extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'hello_summer_water_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'water_type', // water_drop, water_bowl
            'title',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }