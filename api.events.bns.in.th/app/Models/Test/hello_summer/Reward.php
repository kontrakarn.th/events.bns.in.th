<?php

    namespace App\Models\Test\hello_summer;

    class Reward {

        public function getGaranteeReward(){
            return [
                'item_type' => 'water_bowl',
                'title' => 'ขันน้ำ',
                'quantity' => 1,
            ];
        }

        public function getRandomGachapon(){

            $rewards = $this->getRewardsGachapon();

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($rewards as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            $reward['timestamp'] = time();

            // now we have the reward
            return $reward;
        }

        private function getRewardsGachapon(){
            return [
                [
                    'id' => 1,
                    'item_type' => 'gachapon',
                    'product_title' => 'แพ็คเกจหินโซล 20 ชิ้น',
                    'product_id' => 2225,
                    'product_quantity' => 1,
                    'amount' => 20,
                    'chance' => 50.000,
                ],
                [
                    'id' => 2,
                    'item_type' => 'gachapon',
                    'product_title' => 'แพ็คเกจหินจันทรา 3 ชิ้น',
                    'product_id' => 2226,
                    'product_quantity' => 1,
                    'amount' => 3,
                    'chance' => 45.000,
                ],
                [
                    'id' => 3,
                    'item_type' => 'gachapon',
                    'product_title' => 'เศษเกล็ดสีคราม 1 ชิ้น',
                    'product_id' => 2200,
                    'product_quantity' => 1,
                    'amount' => 1,
                    'chance' => 5.000,
                ],
            ];
        }

        // Exchange Rewards
        public function setExchangeByPackageId($packageId=null){
            if(empty($packageId)){
                return false;
            }

            $rewards = $this->exchangeRewardList();

            return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
        }

        public function exchangeRewardList(){
            return [
                [
                    'id' => 1,
                    'item_type' => 'exchange',
                    'product_title' => 'ชุดทะเลฤดูร้อน',
                    'product_id' => 2201,
                    'product_quantity' => 1,
                    'require_water_bowl' => 20,
                    'amount' => 1,
                ],
                [
                    'id' => 2,
                    'item_type' => 'exchange',
                    'product_title' => 'สน็อกเกิ้ล',
                    'product_id' => 2202,
                    'product_quantity' => 1,
                    'require_water_bowl' => 15,
                    'amount' => 1,
                ],
                [
                    'id' => 3,
                    'item_type' => 'exchange',
                    'product_title' => 'หมวกคลุมผม',
                    'product_id' => 2203,
                    'product_quantity' => 1,
                    'require_water_bowl' => 15,
                    'amount' => 1,
                ],
                [
                    'id' => 4,
                    'item_type' => 'exchange',
                    'product_title' => 'หมวกกุหลาบบาน',
                    'product_id' => 2204,
                    'product_quantity' => 1,
                    'require_water_bowl' => 15,
                    'amount' => 1,
                ],
                [
                    'id' => 5,
                    'item_type' => 'exchange',
                    'product_title' => 'หมวกท่องเที่ยวหน้าร้อน',
                    'product_id' => 2205,
                    'product_quantity' => 1,
                    'require_water_bowl' => 15,
                    'amount' => 1,
                ],
            ];
        }

    }