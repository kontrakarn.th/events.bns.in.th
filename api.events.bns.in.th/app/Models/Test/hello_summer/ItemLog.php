<?php

    namespace App\Models\Test\hello_summer;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {

        protected $connection = 'events_bns_test';
        protected $table = 'hello_summer_send_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'quest_id',
            'product_id',
            'product_title',
            'product_quantity',
            'item_type', // quest, gachapon , exchange
            'amount',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    