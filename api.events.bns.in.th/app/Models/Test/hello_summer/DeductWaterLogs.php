<?php

    namespace App\Models\Test\hello_summer;

    use App\Models\BaseModel;

    class DeductWaterLogs extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'hello_summer_deduct_water_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'deduct_type', // water_drop, water_bowl
            'before_deduct',
            'after_deduct',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }