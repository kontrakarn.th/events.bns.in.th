<?php

    namespace App\Models\Test\hello_summer;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'hello_summer_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'world_id',
            'job',
            'total_water_drop',
            'used_water_drop',
            'total_water_bowl',
            'used_water_bowl',
            'last_ip',
        ];

    }