<?php

    namespace App\Models\Test\rainypetpouches;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'rainypetpouches_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'last_ip',
            'group',
        ];

    }