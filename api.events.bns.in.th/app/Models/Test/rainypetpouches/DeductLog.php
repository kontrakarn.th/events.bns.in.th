<?php

    namespace App\Models\Test\rainypetpouches;

    use App\Models\BaseModel;

    class DeductLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'rainypetpouches_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'diamonds',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'deduct_type',
            'send_to',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }