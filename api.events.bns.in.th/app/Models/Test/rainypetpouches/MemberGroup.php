<?php

    namespace App\Models\Test\rainypetpouches;

    use App\Models\BaseModel;

    class MemberGroup extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'rainypetpouches_members_group';
        protected $fillable = [
            'uid',
            'group',
        ];

    }