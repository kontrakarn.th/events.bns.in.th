<?php

    namespace App\Models\Test\preregis_warden;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'preregis_warden_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'answer',
            'char_id',
            'char_name',
            'world_id',
            'quest_01',
            'quest_02',
            'quest_03',
            'package_discount',
            'last_ip',
        ];

    }