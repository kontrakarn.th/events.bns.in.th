<?php

    namespace App\Models\Test\preregis_warden;

    class Reward {

        public function setRewardByPackageId($packageId=null){
            if(empty($packageId)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'Pre-Register Warden Package',
                    'amount' => 1,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'คูปองเพิ่มช่องตัวละคร',
                            // 'product_id' => 1938,
                            'product_id' => 1745,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'คูปองอัพเกรดด่วน',
                            'product_id' => 1078,
                            'product_id' => 1745,
                            'product_quantity' => 1
                        ],
                    ]
                ],
                [
                    'package_id' => 2,
                    'package_title' => 'Warden Mission Package',
                    'amount' => 1,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชุดผู้ปกปักษ์',
                            // 'product_id' => 1939,
                            'product_id' => 1745,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 1,
                            'product_title' => 'ทรงผมผู้ปกปักษ์',
                            // 'product_id' => 1940,
                            'product_id' => 1745,
                            'product_quantity' => 1
                        ],
                    ]
                ],

            ];
        }

    }