<?php

    namespace App\Models\Test\piggy;

    use App\Models\BaseModel;

    class Round extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'piggy_rounds';
        protected $fillable = [
            'start_datetime',
            'end_datetime',
            'status',
        ];
    }

