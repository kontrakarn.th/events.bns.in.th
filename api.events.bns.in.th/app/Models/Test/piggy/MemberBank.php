<?php

    namespace App\Models\Test\piggy;

    use App\Models\BaseModel;

    class MemberBank extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'piggy_member_bank';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'round_id',
            'diamonds_2000_count',
            'diamonds_1000_count',
            'diamonds_500_count',
            'lootboxes',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];
    }

