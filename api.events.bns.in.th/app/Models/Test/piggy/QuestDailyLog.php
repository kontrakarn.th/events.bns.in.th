<?php

    namespace App\Models\Test\piggy;

    use App\Models\BaseModel;

    class QuestDailyLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'piggy_quest_daily_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'round_id',
            'quest_id',
            'quest_dungeon',
            'quest_title',
            'quest_code',
            'quest_diamonds',
            'quest_lootboxed',
            'image',
            'status', // pending, success
            'claim_status', // pending, claimed
            'log_date',
            'log_date_timestamp',
            'quest_complete_datetime',
            'last_ip',
        ];

    }