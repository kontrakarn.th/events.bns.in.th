<?php

    namespace App\Models\Test\piggy;

    use App\Models\BaseModel;

    class Quest extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'piggy_quests';
        protected $fillable = [
            'quest_dungeon',
            'quest_title',
            'quest_code',
            'quest_id',
            'quest_diamonds',
            'quest_lootboxed',
            'image',
        ];
    }

