<?php

    namespace App\Models\Test\piggy;

    use App\Models\BaseModel;

    class Material extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'piggy_materials';
        protected $fillable = [
            'title',
            'quantity',
            'limit',
            'image',
        ];
    }

