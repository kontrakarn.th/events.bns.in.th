<?php

namespace App\Models\Test\bns_word;

use Illuminate\Database\Eloquent\Model;

class BnsWording extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'bns_wording';
}
