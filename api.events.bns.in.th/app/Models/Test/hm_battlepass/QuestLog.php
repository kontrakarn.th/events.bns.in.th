<?php

    namespace App\Models\Test\hm_battlepass;

    use App\Models\BaseModel;

    class QuestLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'hm_battlepass_quest_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'step',
            'quest_id',
            'quest_code', // api quest id, api achievement id
            'quest_title',
            'quest_type', // quest_completed_count, achievement_completed, achievement_count, monster_kill, daily, weekly
            'quest_start', // datetime for quest start for quest_completed_count, achievement_count, monster_kill
            'count_required', // quest required count
            'max_count',
            'count_before', // old achievement count before start, only for "achievement_count" type.
            'count_after', // latest achievement count for completed quest
            'achievement_step', // for "achievement_completed" type
            'has_sub_quest', // 0 : false, 1 : true
            // 'sub_quest', // json endcode for subquest array list [quest_code,quest_title,count_required,]
            'status', // pending, completed, reject (for buy new random quest.)
            'completed_type', // pending, quest_completed, ticket_pass, ticket_5_pass, reject
            'reward_claimed', // claim reward status, 0 : false, 1 : true
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }