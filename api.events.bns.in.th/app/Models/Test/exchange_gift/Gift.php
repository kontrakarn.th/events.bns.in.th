<?php

    namespace App\Models\Test\exchange_gift;

    use Moloquent;

    /**
     * Description of ItemHistory
     *
     * @author naruebaetbouhom
     */
    class Gift extends Moloquent {

        protected $connection = 'mongodb_bns_test';
        protected $table = 'exchange_gift_gift';

    }
