<?php

    namespace App\Models\Test\exchange_gift;

    use Moloquent;

    /**
     * Description of DeductLog
     *
     * @author naruebaetbouhom
     */
    class DeductLog extends Moloquent {

        protected $connection = 'mongodb_bns_test';
        protected $table = 'exchange_gift_deduct_log';
        protected $fillable = [
            'diamond',
            'uid',
            'ncid',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'status',
            'last_ip',
            'item_list',
            'log_timestamp'
        ];

    }
