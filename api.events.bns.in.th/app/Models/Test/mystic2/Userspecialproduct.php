<?php

namespace App\Models\Test\mystic2;

use Illuminate\Database\Eloquent\Model;

class Userspecialproduct extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'mystic2_user_special_item';
    protected $fillable = [
        'uid',
        'special_item_id',
        'type_select',

    ];
    public function Product(){
        return $this->belongsTo('App\Models\Test\mystic2\Product','special_item_id');
    }

}
