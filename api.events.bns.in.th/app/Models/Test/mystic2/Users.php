<?php

namespace App\Models\Test\mystic2;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'mystic2_users';
    protected $fillable = [
        'id',
        'uid',
        'username',
        'ncid',
        'last_ip',
        'hongmoon_key',
        'user_type',
        'group',
    ];

}
