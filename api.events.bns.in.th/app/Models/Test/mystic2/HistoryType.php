<?php

namespace App\Models\Test\mystic2;

use Illuminate\Database\Eloquent\Model;

class HistoryType extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'mystic2_history_type';
    protected $fillable = [
        'id',
        'name',
        'table_name',
        'model_name',
    ];
}
