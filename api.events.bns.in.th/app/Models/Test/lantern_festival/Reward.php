<?php

namespace App\Models\Test\lantern_festival;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'lantern_festival_rewards';

}
