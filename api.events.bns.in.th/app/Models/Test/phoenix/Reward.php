<?php

namespace App\Models\Test\phoenix;

use Carbon\Carbon;

class Reward
{

    public function getRandomRewardInfo()
    {
        // $date_rate=Carbon::parse("2019-10-05 00:00:00");
        // $date_now=Carbon::now();
        // if($date_now<$date_rate){
        //     $rewards = $this->getRewardsListFirstDay();
        // }else{
        $rewards = $this->getRewardsList();
        // }

        $random = rand(0, 100000);
        $accumulatedChance = 0;
        $reward = false;

        foreach ($rewards as $value) {
            $chance = $value['chance'] * 1000;
            $currentChance = $accumulatedChance + $chance;

            // acquired reward
            if ($random >= $accumulatedChance && $random <= $currentChance) {
                // $value['color'] = $this->randomColor();
                $reward = $value;
                break;
            }
            $accumulatedChance = $currentChance;
        }

        $reward['random'] = $random;

        // now we have the reward
        return $reward;
    }

    protected function getRewardsList()
    {
        return [
            [
                'id' => 1,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดเทพพิทักษ์',
                'product_id' => 560,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'icon_1',
                'key' => 'icon_1_get',
                'color' => 'gold'
            ],
            [
                'id' => 2,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ตเทพพิทักษ์',
                'product_id' => 2443,
                'product_quantity' => 1,
                'chance' => 0.025,
                'icon' => 'icon_1_set',
                'key' => 'icon_1_set_get',
                'color' => 'gold'
            ],
            [
                'id' => 3,
                'item_type' => 'rare_item',
                'product_title' => 'เครื่องประดับเทพพิทักษ์',
                'product_id' => 561,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'icon_2',
                'key' => 'icon_2_get',
                'color' => 'gold'
            ],
            [
                'id' => 4,
                'item_type' => 'rare_item',
                'product_title' => 'ปีกเทพพิทักษ์',
                'product_id' => 562,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'icon_3',
                'key' => 'icon_3_get',
                'color' => 'gold'
            ],
            [
                'id' => 5,
                'item_type' => 'rare_item',
                'product_title' => 'หินสัตว์เลี้ยง เทพพิทักษ์',
                'product_id' => 563,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'icon_5',
                'key' => 'icon_5_get',
                'color' => 'gold'
            ],
            [
                'id' => 6,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ตเทพพิทักษ์เมี๊ยว',
                'product_id' => 2149,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'icon_6',
                'key' => 'icon_6_get',
                'color' => 'gold'
            ],
            [
                'id' => 7,
                'item_type' => 'rare_item',
                'product_title' => 'กล่องอาวุธลวงตาเทพพิทักษ์',
                'product_id' => 564,
                'product_quantity' => 1,
                'chance' => 0.075,
                'icon' => 'icon_4',
                'key' => 'icon_4_get',
                'color' => 'gold'
            ],
            // Items parts set A
            [
                'id' => 8,
                'item_type' => 'material',
                'product_title' => 'ชุดเทพพิทักษ์ (Part 1)',
                'product_id_as' => 'MAT_1_1',
                'product_id' => 20791,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'icon_1_R',
                'key' => 'icon_1_R_get',
                'color' => 'red'
            ],
            [
                'id' => 9,
                'item_type' => 'material',
                'product_title' => 'ชุดเทพพิทักษ์ (Part 2)',
                'product_id_as' => 'MAT_1_2',
                'product_id' => 20792,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'icon_1_G',
                'key' => 'icon_1_G_get',
                'color' => 'green'
            ],
            [
                'id' => 10,
                'item_type' => 'material',
                'product_title' => 'ชุดเทพพิทักษ์ (Part 3)',
                'product_id_as' => 'MAT_1_3',
                'product_id' => 20793,
                'product_quantity' => 1,
                'chance' => 0.125,
                'icon' => 'icon_1_B',
                'key' => 'icon_1_B_get',
                'color' => 'blue'
            ],
            // Items parts set B
            [
                'id' => 11,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับเทพพิทักษ์ (Part 1)',
                'product_id_as' => 'MAT_2_1',
                'product_id' => 20801,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'icon_2_R',
                'key' => 'icon_2_R_get',
                'color' => 'red'
            ],
            [
                'id' => 12,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับเทพพิทักษ์ (Part 2)',
                'product_id_as' => 'MAT_2_2',
                'product_id' => 20802,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'icon_2_G',
                'key' => 'icon_2_G_get',
                'color' => 'green'
            ],
            [
                'id' => 13,
                'item_type' => 'material',
                'product_title' => 'เครื่องประดับเทพพิทักษ์ (Part 3)',
                'product_id_as' => 'MAT_2_3',
                'product_id' => 20803,
                'product_quantity' => 1,
                'chance' => 0.125,
                'icon' => 'icon_2_B',
                'key' => 'icon_2_B_get',
                'color' => 'blue'
            ],

            // Items parts set C
            [
                'id' => 14,
                'item_type' => 'material',
                'product_title' => 'ปีกเทพพิทักษ์ (Part 1)',
                'product_id_as' => 'MAT_3_1',
                'product_id' => 20811,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'icon_3_R',
                'key' => 'icon_3_R_get',
                'color' => 'red'
            ],
            [
                'id' => 15,
                'item_type' => 'material',
                'product_title' => 'ปีกเทพพิทักษ์ (Part 2)',
                'product_id_as' => 'MAT_3_2',
                'product_id' => 20812,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'icon_3_G',
                'key' => 'icon_3_G_get',
                'color' => 'green'
            ],
            [
                'id' => 16,
                'item_type' => 'material',
                'product_title' => 'ปีกเทพพิทักษ์ (Part 3)',
                'product_id_as' => 'MAT_3_3',
                'product_id' => 20813,
                'product_quantity' => 1,
                'chance' => 0.125,
                'icon' => 'icon_3_B',
                'key' => 'icon_3_B_get',
                'color' => 'blue'
            ],


            // Items parts set D
            [
                'id' => 17,
                'item_type' => 'material',
                'product_title' => 'กล่องอาวุธลวงตาเทพพิทักษ์ (Part 1)',
                'product_id_as' => 'MAT_4_1',
                'product_id' => 20821,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'icon_4_R',
                'key' => 'icon_4_R_get',
                'color' => 'red'
            ],
            [
                'id' => 18,
                'item_type' => 'material',
                'product_title' => 'กล่องอาวุธลวงตาเทพพิทักษ์ (Part 2)',
                'product_id_as' => 'MAT_4_2',
                'product_id' => 20822,
                'product_quantity' => 1,
                'chance' => 0.250,
                'icon' => 'icon_4_G',
                'key' => 'icon_4_G_get',
                'color' => 'green'
            ],
            [
                'id' => 19,
                'item_type' => 'material',
                'product_title' => 'กล่องอาวุธลวงตาเทพพิทักษ์ (Part 3)',
                'product_id_as' => 'MAT_4_3',
                'product_id' => 20823,
                'product_quantity' => 1,
                'chance' => 0.125,
                'icon' => 'icon_4_B',
                'key' => 'icon_4_B_get',
                'color' => 'blue'
            ],

            // Object item
            [
                'id' => 20,
                'item_type' => 'object',
                'product_title' => 'หญ้าเขี้ยวจันทร์ x5',
                'product_id' => 3417,
                'product_quantity' => 1,
                'chance' => 5.150,
                'icon' => 'icon_7',
                'key' => 'icon_7',
                'color' => 'red'
            ],
            [
                'id' => 21,
                'item_type' => 'object',
                'product_title' => 'คริสตัลหินจันทรา x100',
                'product_id' => 943,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'icon_8',
                'key' => 'icon_8',
                'color' => 'red'
            ],
            [
                'id' => 22,
                'item_type' => 'object',
                'product_title' => 'หินจันทรา x100',
                'product_id' => 2137,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'icon_9',
                'key' => 'icon_9',
                'color' => 'red'
            ],
            [
                'id' => 23,
                'item_type' => 'object',
                'product_title' => 'กล่องแหวนราชาทมิฬ ขั้น 1 x1',
                'product_id' => 2688,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'icon_10',
                'key' => 'icon_10',
                'color' => 'red'
            ],
            [
                'id' => 24,
                'item_type' => 'object',
                'product_title' => 'คริสตัลหินโซล x200',
                'product_id' => 3260,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'icon_11',
                'key' => 'icon_11',
                'color' => 'red'
            ],
            [
                'id' => 25,
                'item_type' => 'object',
                'product_title' => 'หินโซล x200',
                'product_id' => 2471,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'icon_12',
                'key' => 'icon_12',
                'color' => 'red'
            ],
            [
                'id' => 26,
                'item_type' => 'object',
                'product_title' => 'กล่องแผ่นผนึกค่าสถานะฮงมุน ที่ส่องสว่าง x1',
                'product_id' => 3412,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'icon_13',
                'key' => 'icon_13',
                'color' => 'red'
            ],
            [
                'id' => 27,
                'item_type' => 'object',
                'product_title' => 'กล่องแผ่นผนึกค่าสถานะฮงมุน ระยิบระยับ x1',
                'product_id' => 3414,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'icon_14',
                'key' => 'icon_14',
                'color' => 'red'
            ],
            [
                'id' => 28,
                'item_type' => 'object',
                'product_title' => 'อัญมณี กรุฮงมุน x1',
                'product_id' => 1671,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'icon_15',
                'key' => 'icon_15',
                'color' => 'red'
            ],
            [
                'id' => 29,
                'item_type' => 'object',
                'product_title' => 'คริสตัลอัญมณีหยินหยาง x1',
                'product_id' => 1900,
                'product_quantity' => 1,
                'chance' => 3.500,
                'icon' => 'icon_16',
                'key' => 'icon_16',
                'color' => 'red'
            ],
            [
                'id' => 30,
                'item_type' => 'object',
                'product_title' => 'ยันต์ปริศนาแห่งฮงมุน x1',
                'product_id' => 2192,
                'product_quantity' => 1,
                'chance' => 3.500,
                'icon' => 'icon_17',
                'key' => 'icon_17',
                'color' => 'red'
            ],
            [
                'id' => 31,
                'item_type' => 'object',
                'product_title' => 'หินเปลี่ยนรูปชั้นสูง x1',
                'product_id' => 911,
                'product_quantity' => 1,
                'chance' => 3.500,
                'icon' => 'icon_18',
                'key' => 'icon_18',
                'color' => 'red'
            ],
            [
                'id' => 32,
                'item_type' => 'object',
                'product_title' => 'เกล็ดสีทอง x1',
                'product_id' => 3146,
                'product_quantity' => 1,
                'chance' => 3.500,
                'icon' => 'icon_19',
                'key' => 'icon_19',
                'color' => 'red'
            ],
            [
                'id' => 33,
                'item_type' => 'object',
                'product_title' => 'ดาวมังกรแดง x1',
                'product_id' => 3224,
                'product_quantity' => 1,
                'chance' => 3.500,
                'icon' => 'icon_20',
                'key' => 'icon_20',
                'color' => 'red'
            ],
            [
                'id' => 34,
                'item_type' => 'object',
                'product_title' => 'ถุงมือราชันย์ ขั้น 1 x1',
                'product_id' => 2683,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'icon_21',
                'key' => 'icon_21',
                'color' => 'red'
            ],
            [
                'id' => 35,
                'item_type' => 'object',
                'product_title' => 'เข็มขัดราชันย์พยัคฆ์ ขั้น 1 x1',
                'product_id' => 2684,
                'product_quantity' => 1,
                'chance' => 3.500,
                'icon' => 'icon_22',
                'key' => 'icon_22',
                'color' => 'red'
            ],
            [
                'id' => 36,
                'item_type' => 'object',
                'product_title' => 'กล่องสร้อยข้อมือเทพมังกร ขั้น 1 x1',
                'product_id' => 2685,
                'product_quantity' => 1,
                'chance' => 3.500,
                'icon' => 'icon_23',
                'key' => 'icon_23',
                'color' => 'red'
            ],
            [
                'id' => 37,
                'item_type' => 'object',
                'product_title' => 'กล่องสร้อยคอราชินีทมิฬ ขั้น 1 x1',
                'product_id' => 2686,
                'product_quantity' => 1,
                'chance' => 3.500,
                'icon' => 'icon_24',
                'key' => 'icon_24',
                'color' => 'red'
            ],
            [
                'id' => 38,
                'item_type' => 'object',
                'product_title' => 'กล่องต่างหูราชาทมิฬ ขั้น 1 x1',
                'product_id' => 2687,
                'product_quantity' => 1,
                'chance' => 3.500,
                'icon' => 'icon_25',
                'key' => 'icon_25',
                'color' => 'red'
            ],
            [
                'id' => 39,
                'item_type' => 'object',
                'product_title' => 'ดาวมังกรฟ้า x1',
                'product_id' => 2648,
                'product_quantity' => 1,
                'chance' => 3.500,
                'icon' => 'icon_26',
                'key' => 'icon_26',
                'color' => 'red'
            ],
            [
                'id' => 40,
                'item_type' => 'object',
                'product_title' => 'วิญญาณซาฮวา x1',
                'product_id' => 3141,
                'product_quantity' => 1,
                'chance' => 3.500,
                'icon' => 'icon_27',
                'key' => 'icon_27',
                'color' => 'red'
            ],
            [
                'id' => 41,
                'item_type' => 'object',
                'product_title' => 'หญ้าซาฮวา x1',
                'product_id' => 3064,
                'product_quantity' => 1,
                'chance' => 3.500,
                'icon' => 'icon_28',
                'key' => 'icon_28',
                'color' => 'red'
            ],
            [
                'id' => 42,
                'item_type' => 'object',
                'product_title' => 'ชิ้นส่วนผลึกฮงมุน x1',
                'product_id' => 3415,
                'product_quantity' => 1,
                'chance' => 4.000,
                'icon' => 'icon_29',
                'key' => 'icon_29',
                'color' => 'red'
            ],
        ];
        // return [
        //     [
        //         'id' => 1,
        //         'item_type' => 'rare_item',
        //         'product_title' => 'ชุดเทพพิทักษ์',
        //         'product_id' => 560,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.050,
        //         'chance' => 4.000,
        //         'icon' => 'icon_1',
        //         'key' => 'icon_1_get',
        //         'color' => 'gold'
        //     ],
        //     [
        //         'id' => 2,
        //         'item_type' => 'rare_item',
        //         'product_title' => 'เซ็ตเทพพิทักษ์',
        //         'product_id' => 2443,
        //         'product_quantity' => 1,
        //         'chance' => 0.025,
        //         'icon' => 'icon_1_set',
        //         'key' => 'icon_1_set_get',
        //         'color' => 'gold'
        //     ],
        //     [
        //         'id' => 3,
        //         'item_type' => 'rare_item',
        //         'product_title' => 'เครื่องประดับเทพพิทักษ์',
        //         'product_id' => 561,
        //         'product_quantity' => 1,
        //         'chance' => 0.050,
        //         'icon' => 'icon_2',
        //         'key' => 'icon_2_get',
        //         'color' => 'gold'
        //     ],
        //     [
        //         'id' => 4,
        //         'item_type' => 'rare_item',
        //         'product_title' => 'ปีกเทพพิทักษ์',
        //         'product_id' => 562,
        //         'product_quantity' => 1,
        //         'chance' => 0.050,
        //         'icon' => 'icon_3',
        //         'key' => 'icon_3_get',
        //         'color' => 'gold'
        //     ],
        //     [
        //         'id' => 5,
        //         'item_type' => 'rare_item',
        //         'product_title' => 'หินสัตว์เลี้ยง เทพพิทักษ์',
        //         'product_id' => 563,
        //         'product_quantity' => 1,
        //         'chance' => 5.000,
        //         'icon' => 'icon_5',
        //         'key' => 'icon_5_get',
        //         'color' => 'gold'
        //     ],
        //     [
        //         'id' => 6,
        //         'item_type' => 'rare_item',
        //         'product_title' => 'เซ็ตเทพพิทักษ์เมี๊ยว',
        //         'product_id' => 2149,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.050,
        //         'chance' => 3.500,
        //         'icon' => 'icon_6',
        //         'key' => 'icon_6_get',
        //         'color' => 'gold'
        //     ],
        //     [
        //         'id' => 7,
        //         'item_type' => 'rare_item',
        //         'product_title' => 'กล่องอาวุธลวงตาเทพพิทักษ์',
        //         'product_id' => 564,
        //         'product_quantity' => 1,
        //         'chance' => 0.075,
        //         'icon' => 'icon_4',
        //         'key' => 'icon_4_get',
        //         'color' => 'gold'
        //     ],
        //     // Items parts set A
        //     [
        //         'id' => 8,
        //         'item_type' => 'material',
        //         'product_title' => 'ชุดเทพพิทักษ์ (Part 1)',
        //         'product_id_as' => 'MAT_1_1',
        //         'product_id' => 20791,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.500,
        //         'chance' => 5.150,
        //         'icon' => 'icon_1_R',
        //         'key' => 'icon_1_R_get',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 9,
        //         'item_type' => 'material',
        //         'product_title' => 'ชุดเทพพิทักษ์ (Part 2)',
        //         'product_id_as' => 'MAT_1_2',
        //         'product_id' => 20792,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.250,
        //         'chance' => 5.000,
        //         'icon' => 'icon_1_G',
        //         'key' => 'icon_1_G_get',
        //         'color' => 'green'
        //     ],
        //     [
        //         'id' => 10,
        //         'item_type' => 'material',
        //         'product_title' => 'ชุดเทพพิทักษ์ (Part 3)',
        //         'product_id_as' => 'MAT_1_3',
        //         'product_id' => 20793,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.125,
        //         'chance' => 5.000,
        //         'icon' => 'icon_1_B',
        //         'key' => 'icon_1_B_get',
        //         'color' => 'blue'
        //     ],
        //     // Items parts set B
        //     [
        //         'id' => 11,
        //         'item_type' => 'material',
        //         'product_title' => 'เครื่องประดับเทพพิทักษ์ (Part 1)',
        //         'product_id_as' => 'MAT_2_1',
        //         'product_id' => 20801,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.500,
        //         'chance' => 5.000,
        //         'icon' => 'icon_2_R',
        //         'key' => 'icon_2_R_get',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 12,
        //         'item_type' => 'material',
        //         'product_title' => 'เครื่องประดับเทพพิทักษ์ (Part 2)',
        //         'product_id_as' => 'MAT_2_2',
        //         'product_id' => 20802,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.250,
        //         'chance' => 5.000,
        //         'icon' => 'icon_2_G',
        //         'key' => 'icon_2_G_get',
        //         'color' => 'green'
        //     ],
        //     [
        //         'id' => 13,
        //         'item_type' => 'material',
        //         'product_title' => 'เครื่องประดับเทพพิทักษ์ (Part 3)',
        //         'product_id_as' => 'MAT_2_3',
        //         'product_id' => 20803,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.125,
        //         'chance' => 5.000,
        //         'icon' => 'icon_2_B',
        //         'key' => 'icon_2_B_get',
        //         'color' => 'blue'
        //     ],

        //     // Items parts set C
        //     [
        //         'id' => 14,
        //         'item_type' => 'material',
        //         'product_title' => 'ปีกเทพพิทักษ์ (Part 1)',
        //         'product_id_as' => 'MAT_3_1',
        //         'product_id' => 20811,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.500,
        //         'chance' => 5.000,
        //         'icon' => 'icon_3_R',
        //         'key' => 'icon_3_R_get',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 15,
        //         'item_type' => 'material',
        //         'product_title' => 'ปีกเทพพิทักษ์ (Part 2)',
        //         'product_id_as' => 'MAT_3_2',
        //         'product_id' => 20812,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.250,
        //         'chance' => 5.000,
        //         'icon' => 'icon_3_G',
        //         'key' => 'icon_3_G_get',
        //         'color' => 'green'
        //     ],
        //     [
        //         'id' => 16,
        //         'item_type' => 'material',
        //         'product_title' => 'ปีกเทพพิทักษ์ (Part 3)',
        //         'product_id_as' => 'MAT_3_3',
        //         'product_id' => 20813,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.125,
        //         'chance' => 5.000,
        //         'icon' => 'icon_3_B',
        //         'key' => 'icon_3_B_get',
        //         'color' => 'blue'
        //     ],


        //     // Items parts set D
        //     [
        //         'id' => 17,
        //         'item_type' => 'material',
        //         'product_title' => 'กล่องอาวุธลวงตาเทพพิทักษ์ (Part 1)',
        //         'product_id_as' => 'MAT_4_1',
        //         'product_id' => 20821,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.500,
        //         'chance' => 3.500,
        //         'icon' => 'icon_4_R',
        //         'key' => 'icon_4_R_get',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 18,
        //         'item_type' => 'material',
        //         'product_title' => 'กล่องอาวุธลวงตาเทพพิทักษ์ (Part 2)',
        //         'product_id_as' => 'MAT_4_2',
        //         'product_id' => 20822,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.250,
        //         'chance' => 3.500,
        //         'icon' => 'icon_4_G',
        //         'key' => 'icon_4_G_get',
        //         'color' => 'green'
        //     ],
        //     [
        //         'id' => 19,
        //         'item_type' => 'material',
        //         'product_title' => 'กล่องอาวุธลวงตาเทพพิทักษ์ (Part 3)',
        //         'product_id_as' => 'MAT_4_3',
        //         'product_id' => 20823,
        //         'product_quantity' => 1,
        //         // 'chance' => 0.125,
        //         'chance' => 3.500,
        //         'icon' => 'icon_4_B',
        //         'key' => 'icon_4_B_get',
        //         'color' => 'blue'
        //     ],

        //     // Object item
        //     [
        //         'id' => 20,
        //         'item_type' => 'object',
        //         'product_title' => 'หญ้าเขี้ยวจันทร์ x5',
        //         'product_id' => 3417,
        //         'product_quantity' => 1,
        //         // 'chance' => 5.150,
        //         'chance' => 0.500,
        //         'icon' => 'icon_7',
        //         'key' => 'icon_7',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 21,
        //         'item_type' => 'object',
        //         'product_title' => 'คริสตัลหินจันทรา x100',
        //         'product_id' => 943,
        //         'product_quantity' => 1,
        //         // 'chance' => 5.000,
        //         'chance' => 0.250,
        //         'icon' => 'icon_8',
        //         'key' => 'icon_8',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 22,
        //         'item_type' => 'object',
        //         'product_title' => 'หินจันทรา x100',
        //         'product_id' => 2137,
        //         'product_quantity' => 1,
        //         // 'chance' => 5.000,
        //         'chance' => 0.125,
        //         'icon' => 'icon_9',
        //         'key' => 'icon_9',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 23,
        //         'item_type' => 'object',
        //         'product_title' => 'กล่องแหวนราชาทมิฬ ขั้น 1 x1',
        //         'product_id' => 2688,
        //         'product_quantity' => 1,
        //         // 'chance' => 5.000,
        //         'chance' => 0.500,
        //         'icon' => 'icon_10',
        //         'key' => 'icon_10',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 24,
        //         'item_type' => 'object',
        //         'product_title' => 'คริสตัลหินโซล x200',
        //         'product_id' => 3260,
        //         'product_quantity' => 1,
        //         // 'chance' => 5.000,
        //         'chance' => 0.250,
        //         'icon' => 'icon_11',
        //         'key' => 'icon_11',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 25,
        //         'item_type' => 'object',
        //         'product_title' => 'หินโซล x200',
        //         'product_id' => 2471,
        //         'product_quantity' => 1,
        //         // 'chance' => 5.000,
        //         'chance' => 0.125,
        //         'icon' => 'icon_12',
        //         'key' => 'icon_12',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 26,
        //         'item_type' => 'object',
        //         'product_title' => 'กล่องแผ่นผนึกค่าสถานะฮงมุน ที่ส่องสว่าง x1',
        //         'product_id' => 3412,
        //         'product_quantity' => 1,
        //         // 'chance' => 5.000,
        //         'chance' => 0.500,
        //         'icon' => 'icon_13',
        //         'key' => 'icon_13',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 27,
        //         'item_type' => 'object',
        //         'product_title' => 'กล่องแผ่นผนึกค่าสถานะฮงมุน ระยิบระยับ x1',
        //         'product_id' => 3414,
        //         'product_quantity' => 1,
        //         // 'chance' => 5.000,
        //         'chance' => 0.250,
        //         'icon' => 'icon_14',
        //         'key' => 'icon_14',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 28,
        //         'item_type' => 'object',
        //         'product_title' => 'อัญมณี กรุฮงมุน x1',
        //         'product_id' => 1671,
        //         'product_quantity' => 1,
        //         // 'chance' => 5.000,
        //         'chance' => 0.125,
        //         'icon' => 'icon_15',
        //         'key' => 'icon_15',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 29,
        //         'item_type' => 'object',
        //         'product_title' => 'คริสตัลอัญมณีหยินหยาง x1',
        //         'product_id' => 1900,
        //         'product_quantity' => 1,
        //         // 'chance' => 3.500,
        //         'chance' => 0.500,
        //         'icon' => 'icon_16',
        //         'key' => 'icon_16',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 30,
        //         'item_type' => 'object',
        //         'product_title' => 'ยันต์ปริศนาแห่งฮงมุน x1',
        //         'product_id' => 2192,
        //         'product_quantity' => 1,
        //         // 'chance' => 3.500,
        //         'chance' => 0.250,
        //         'icon' => 'icon_17',
        //         'key' => 'icon_17',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 31,
        //         'item_type' => 'object',
        //         'product_title' => 'หินเปลี่ยนรูปชั้นสูง x1',
        //         'product_id' => 911,
        //         'product_quantity' => 1,
        //         // 'chance' => 3.500,
        //         'chance' => 0.125,
        //         'icon' => 'icon_18',
        //         'key' => 'icon_18',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 32,
        //         'item_type' => 'object',
        //         'product_title' => 'เกล็ดสีทอง x1',
        //         'product_id' => 3146,
        //         'product_quantity' => 1,
        //         'chance' => 3.500,
        //         'icon' => 'icon_19',
        //         'key' => 'icon_19',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 33,
        //         'item_type' => 'object',
        //         'product_title' => 'ดาวมังกรแดง x1',
        //         'product_id' => 3224,
        //         'product_quantity' => 1,
        //         'chance' => 3.500,
        //         'icon' => 'icon_20',
        //         'key' => 'icon_20',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 34,
        //         'item_type' => 'object',
        //         'product_title' => 'ถุงมือราชันย์ ขั้น 1 x1',
        //         'product_id' => 2683,
        //         'product_quantity' => 1,
        //         'chance' => 0.050,
        //         // 'chance' => 5.000,
        //         'icon' => 'icon_21',
        //         'key' => 'icon_21',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 35,
        //         'item_type' => 'object',
        //         'product_title' => 'เข็มขัดราชันย์พยัคฆ์ ขั้น 1 x1',
        //         'product_id' => 2684,
        //         'product_quantity' => 1,
        //         'chance' => 3.500,
        //         'icon' => 'icon_22',
        //         'key' => 'icon_22',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 36,
        //         'item_type' => 'object',
        //         'product_title' => 'กล่องสร้อยข้อมือเทพมังกร ขั้น 1 x1',
        //         'product_id' => 2685,
        //         'product_quantity' => 1,
        //         'chance' => 3.500,
        //         'icon' => 'icon_23',
        //         'key' => 'icon_23',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 37,
        //         'item_type' => 'object',
        //         'product_title' => 'กล่องสร้อยคอราชินีทมิฬ ขั้น 1 x1',
        //         'product_id' => 2686,
        //         'product_quantity' => 1,
        //         'chance' => 3.500,
        //         'icon' => 'icon_24',
        //         'key' => 'icon_24',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 38,
        //         'item_type' => 'object',
        //         'product_title' => 'กล่องต่างหูราชาทมิฬ ขั้น 1 x1',
        //         'product_id' => 2687,
        //         'product_quantity' => 1,
        //         'chance' => 3.500,
        //         'icon' => 'icon_25',
        //         'key' => 'icon_25',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 39,
        //         'item_type' => 'object',
        //         'product_title' => 'ดาวมังกรฟ้า x1',
        //         'product_id' => 2648,
        //         'product_quantity' => 1,
        //         'chance' => 3.500,
        //         'icon' => 'icon_26',
        //         'key' => 'icon_26',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 40,
        //         'item_type' => 'object',
        //         'product_title' => 'วิญญาณซาฮวา x1',
        //         'product_id' => 3141,
        //         'product_quantity' => 1,
        //         'chance' => 3.500,
        //         'icon' => 'icon_27',
        //         'key' => 'icon_27',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 41,
        //         'item_type' => 'object',
        //         'product_title' => 'หญ้าซาฮวา x1',
        //         'product_id' => 3064,
        //         'product_quantity' => 1,
        //         // 'chance' => 3.500,
        //         'chance' => 0.050,
        //         'icon' => 'icon_28',
        //         'key' => 'icon_28',
        //         'color' => 'red'
        //     ],
        //     [
        //         'id' => 42,
        //         'item_type' => 'object',
        //         'product_title' => 'ชิ้นส่วนผลึกฮงมุน x1',
        //         'product_id' => 3415,
        //         'product_quantity' => 1,
        //         // 'chance' => 4.000,
        //         'chance' => 0.050,
        //         'icon' => 'icon_29',
        //         'key' => 'icon_29',
        //         'color' => 'red'
        //     ],
        // ];
    }

    // Rare Items
    public function setRareItemByPackageId($packageId = null)
    {
        if (empty($packageId)) {
            return false;
        }

        $rewards = $this->rareItemList();

        return (isset($rewards[$packageId - 1]) && !empty($rewards[$packageId - 1])) ? $rewards[$packageId - 1] : false;
    }

    public function rareItemList()
    {
        return [
            [
                'id' => 1,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดเทพพิทักษ์',
                'product_id' => 560,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'icon_1',
                'key' => 'icon_1_get',
                'color' => 'gold'
            ],
            [
                'id' => 2,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ตเทพพิทักษ์',
                'product_id' => 2443,
                'product_quantity' => 1,
                'chance' => 0.025,
                'icon' => 'icon_1_set',
                'key' => 'icon_1_set_get',
                'color' => 'gold'
            ],
            [
                'id' => 3,
                'item_type' => 'rare_item',
                'product_title' => 'เครื่องประดับเทพพิทักษ์',
                'product_id' => 561,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'icon_2',
                'key' => 'icon_2_get',
                'color' => 'gold'
            ],
            [
                'id' => 4,
                'item_type' => 'rare_item',
                'product_title' => 'ปีกเทพพิทักษ์',
                'product_id' => 562,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'icon_3',
                'key' => 'icon_3_get',
                'color' => 'gold'
            ],
            [
                'id' => 5,
                'item_type' => 'rare_item',
                'product_title' => 'หินสัตว์เลี้ยง เทพพิทักษ์',
                'product_id' => 563,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'icon_5',
                'key' => 'icon_5_get',
                'color' => 'gold'
            ],
            [
                'id' => 6,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ตเทพพิทักษ์เมี๊ยว',
                'product_id' => 2149,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'icon_6',
                'key' => 'icon_6_get',
                'color' => 'gold'
            ],
            [
                'id' => 7,
                'item_type' => 'rare_item',
                'product_title' => 'กล่องอาวุธลวงตาเทพพิทักษ์',
                'product_id' => 564,
                'product_quantity' => 1,
                'chance' => 0.075,
                'icon' => 'icon_4',
                'key' => 'icon_4_get',
                'color' => 'gold'
            ]
        ];
    }

    // Rare Items
    public function setMergeRareItemByPackageId($packageId = null)
    {
        if (empty($packageId)) {
            return false;
        }

        $rewards = $this->mergeRareItemList();

        return (isset($rewards[$packageId - 1]) && !empty($rewards[$packageId - 1])) ? $rewards[$packageId - 1] : false;
    }

    public function mergeRareItemList()
    {
        return [
            [
                'id' => 1,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดเทพพิทักษ์',
                'product_id' => 560,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'icon_1',
                'key' => 'icon_1_get',
                'color' => 'gold'
            ],
            [
                'id' => 2,
                'item_type' => 'rare_item',
                'product_title' => 'เครื่องประดับเทพพิทักษ์',
                'product_id' => 561,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'icon_2',
                'key' => 'icon_2_get',
                'color' => 'gold'
            ],
            [
                'id' => 3,
                'item_type' => 'rare_item',
                'product_title' => 'ปีกเทพพิทักษ์',
                'product_id' => 562,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'icon_3',
                'key' => 'icon_3_get',
                'color' => 'gold'
            ],
          
            [
                'id' => 4,
                'item_type' => 'rare_item',
                'product_title' => 'กล่องอาวุธลวงตาเทพพิทักษ์',
                'product_id' => 564,
                'product_quantity' => 1,
                'chance' => 0.075,
                'icon' => 'icon_4',
                'key' => 'icon_4_get',
                'color' => 'gold'
            ]
        ];
    }
}
