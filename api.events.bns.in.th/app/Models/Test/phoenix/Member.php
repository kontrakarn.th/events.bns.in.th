<?php

namespace App\Models\Test\phoenix;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'phoenix_members';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'last_ip'
    ];
}
