<?php

namespace App\Models\Test\two_in_one;

use App\Models\BaseModel;

class Setting extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'two_in_one_settings';
    protected $fillable = [
        'event_start',
        'event_end',
        'attend_start',
        'attend_end',
        'active',
    ];
}
