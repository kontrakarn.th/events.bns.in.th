<?php

namespace App\Models\Test\two_in_one;

use App\Models\BaseModel;

class Reward extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'two_in_one_rewards';
    protected $fillable = [
        "package_name",
        "package_id",
        "package_quantity",
        "package_amount",
        "package_type", // 'pve','pvp','limit_pve','limit_pvp'
        "required_tokens",
        "limit",
        "img",
        "hover",
    ];

    public function itemLogs()
    {
        return $this->hasMany(ItemLog::class);
    }
}
