<?php

namespace App\Models\Test\battle_field;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'battle_field_rewards';

}
