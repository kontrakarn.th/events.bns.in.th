<?php
    // Mission for disciple
    namespace App\Models\Test\topup_august2019;

    class Reward {

        public function setRewardByPackage($packageKey=''){
            if(empty($packageKey)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageKey-1]) && !empty($rewards[$packageKey-1])) ? $rewards[$packageKey-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'Topup 10,000 Diamonds',
                    'package_desc' => 'ประกายฉลามดำ,หินโซล,หินจันทรา',
                    'require_diamonds' => 10000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'Topup 10,000 Diamonds',
                            'product_id' => 2495,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 2,
                    'package_title' => 'Topup 20,000 Diamonds',
                    'package_desc' => 'เกล็ดสีคราม,ชิ้นส่วนผลึกฮงมุน,กล่องอัญมณีหกเหลี่ยมของฮงมุน พิเศษ,ลูกแก้วคนทรง',
                    'require_diamonds' => 20000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'Topup 20,000 Diamonds',
                            'product_id' => 2496,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 3,
                    'package_title' => 'Topup 150,000 Diamonds',
                    'package_desc' => 'ชุดปรบมือให้เกียรติ/ชุดเซเลปลายเสือ,เครื่องประดับแห่งความนับถือ/ หมวกเซเลปลายเสือ,ไดมอนด์ 50000,ปีกแทชอน',
                    'require_diamonds' => 150000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'Topup 150,000 Diamonds',
                            'product_id' => 2497,
                            'product_quantity' => 1
                        ]
                    ]
                ],
            ];
        }
    }
