<?php

    namespace App\Models\Test\topup_august2019;

    use Moloquent;

    class ItemLog extends Moloquent {

        protected $connection = 'mongodb_bns_test';
        protected $table = 'topup_august2019_item_logs';
        protected $fillable = [
            'uid',
            'ncid',
            'require_diamonds',
            'package_id',
            'package_title',
            'product_set',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status', // pending, success, unsuccess
            'last_ip',
            'log_date',
            'log_date_timestamp'
        ];

    }
