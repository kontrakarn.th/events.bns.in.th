<?php

    namespace App\Models\Test\happy_xmas2018;

    use App\Models\BaseModel;

    class PlayData extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'happy_xmas_2018_play_data';

        protected $fillable = [
            'id',
            'play_id',
            'ncid',
            'uid',
            'package_id',
            'item_id',
            'item_name',
            'item_amount',
            'item_type',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'status',
            'good_data',
            'log_date',
            'log_date_timestamp',
            'created_at',
            'updated_at',
        ];

    }
