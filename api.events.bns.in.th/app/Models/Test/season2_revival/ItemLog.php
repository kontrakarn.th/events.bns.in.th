<?php

    namespace App\Models\Test\season2_revival;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'new_revival_2018_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'package_id',
            'package_title',
            'status',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'log_date',
            'log_date_timestamp',
            'last_ip'
        ];

    }