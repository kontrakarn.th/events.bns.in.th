<?php

    namespace App\Models\Test\clan_event;

    use App\Models\BaseModel;

    class ClanRank extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'clan_event_score_rank';
        protected $fillable = [
            'clan_id',
            'clan_type',
            'clan_name',
            'clan_faction',
            'world_id',
            'total_points',
            'rank',
            'last_ip'
        ];
    }

