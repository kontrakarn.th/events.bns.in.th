<?php

    namespace App\Models\Test\clan_event;

    use App\Models\BaseModel;

    class Quest extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'clan_event_quests';
        protected $fillable = [
            'quest_title',
            'quest_code',
            'quest_points',
            'quest_type',
            'quest_limit',
        ];
    }

