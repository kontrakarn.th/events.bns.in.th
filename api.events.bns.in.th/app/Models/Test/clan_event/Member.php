<?php

    namespace App\Models\Test\clan_event;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'clan_event_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'world_id',
            'clan_id',
            'clan_type',
            'clan_name',
            'clan_faction',
            'joined_at',
            'joined_at_timestamp',
            'leaved_at',
            'leaved_at_timestamp',
            'current_member',
            'total_points',
            'last_ip'
        ];
    }

