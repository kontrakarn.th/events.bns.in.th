<?php

    namespace App\Models\Test\vip_benefit;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'vip_benefit_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'last_ip',
        ];

    }