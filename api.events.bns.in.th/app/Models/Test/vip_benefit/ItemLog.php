<?php

    namespace App\Models\Test\vip_benefit;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'vip_benefit_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'vip_level',
            'product_key',
            'product_id',
            'product_code',
            'product_title',
            'product_quantity',
            'product_set',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'log_date',
            'log_datetime',
            'log_date_timestamp',
            'last_ip',
        ];

    }