<?php

    namespace App\Models\Test\vip_benefit;

    use App\Models\BaseModel;

    class Reward extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'vip_benefit_rewards';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'product_key',
            'product_id',
            'product_code',
            'product_title',
            'product_quantity',
            'vip_level',
            'period',
            'available_date',
            'image',
        ];

    }