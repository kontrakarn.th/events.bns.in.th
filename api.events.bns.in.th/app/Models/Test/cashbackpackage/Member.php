<?php

namespace App\Models\Test\cashbackpackage;

use App\Models\BaseModel;

class Member extends BaseModel
{
    protected $connection = 'events_bns_test';
    protected $table = 'cashbackpackage_members';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'is_unlocked',
        'last_ip',
    ];
}
