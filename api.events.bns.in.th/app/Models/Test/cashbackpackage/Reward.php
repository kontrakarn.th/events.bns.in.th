<?php

namespace App\Models\Test\cashbackpackage;

use App\Models\BaseModel;

class Reward extends BaseModel
{

    protected $connection = 'events_bns_test';
    protected $table = 'cashbackpackage_rewards';
    protected $fillable = [
        'package_name',
        'package_id',
        'package_quantity',
        'package_amount',
        'package_key',
        'package_type',
        'week',
        'image',
        'available_date',
    ];
}
