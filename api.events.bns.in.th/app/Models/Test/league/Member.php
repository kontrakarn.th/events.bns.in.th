<?php

    namespace App\Models\Test\league;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'league_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'world_id',
            'is_register',
            'group_id',
            'color_id',
            'total_points',
            'rank',
            'rank_previous',
            'rank_latest_updated',
            'last_ip',
        ];

    }