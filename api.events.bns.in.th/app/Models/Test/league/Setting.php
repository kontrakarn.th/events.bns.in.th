<?php

    namespace App\Models\Test\league;

    use App\Models\BaseModel;

    class Setting extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'league_setting';
        protected $fillable = [
            'event_start',
            'event_end',
            'register_start',
            'register_end',
            'competition_start',
            'competition_end',
            'claim_reward_start',
            'claim_reward_end',
            'max_group_member',
            'color_max_score',
            'active',
        ];

    }