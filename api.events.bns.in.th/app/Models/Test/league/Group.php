<?php

    namespace App\Models\Test\league;

    use App\Models\BaseModel;

    class Group extends BaseModel {
        protected $connection = 'events_bns_test';
        protected $table = 'league_groups';
        protected $fillable = [
            'group_id',
            'member_count',
        ];

    }