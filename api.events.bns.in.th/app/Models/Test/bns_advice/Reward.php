<?php

    namespace App\Models\Test\bns_advice;

    class Reward {

        ////////////  Advice Gachapon  /////////////

        public function getRandomAdviceReward(){

            $rewards = $this->getAdviceRewardsList();

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($rewards as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    // $value['color'] = $this->randomColor();
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            $reward['timestamp'] = time();

            // now we have the reward
            return $reward;
        }

        private function getAdviceRewardsList(){
            return [
                [
                    'id' => 1,
                    'item_type' => 'advice_gachapon',
                    'product_title' => 'ชุดศิลปแจกันโบราณ',
                    'product_id' => 1992,
                    'product_quantity' => 1,
                    'chance' => 10.000,
                    'amount' => 1,
                ],
                [
                    'id' => 2,
                    'item_type' => 'advice_gachapon',
                    'product_title' => 'หมวกศิลปพัดโปราณ',
                    'product_id' => 1993,
                    'product_quantity' => 1,
                    'chance' => 5.000,
                    'amount' => 1,
                ],
                [
                    'id' => 3,
                    'item_type' => 'advice_gachapon',
                    'product_title' => 'หินเปลี่ยนรูปขั้นสูง',
                    'product_id' => 771,
                    'product_quantity' => 1,
                    'chance' => 15.000,
                    'amount' => 1,
                ],
                [
                    'id' => 4,
                    'item_type' => 'advice_gachapon',
                    'product_title' => 'ยันต์ผนึกบุปผาน้ำแข็งที่ส่องสว่าง',
                    'product_id' => 1991,
                    'product_quantity' => 1,
                    'chance' => 5.000,
                    'amount' => 1,
                ],
                [
                    'id' => 5,
                    'item_type' => 'advice_gachapon',
                    'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 8',
                    'product_id' => 1723,
                    'product_quantity' => 1,
                    'chance' => 5.000,
                    'amount' => 1,
                ],
                [
                    'id' => 6,
                    'item_type' => 'advice_gachapon',
                    'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 7',
                    'product_id' => 1722,
                    'product_quantity' => 1,
                    'chance' => 10.000,
                    'amount' => 1,
                ],
                [
                    'id' => 7,
                    'item_type' => 'advice_gachapon',
                    'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 6',
                    'product_id' => 1721,
                    'product_quantity' => 1,
                    'chance' => 10.000,
                    'amount' => 1,
                ],
                [
                    'id' => 8,
                    'item_type' => 'advice_gachapon',
                    'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 5',
                    'product_id' => 1720,
                    'product_quantity' => 1,
                    'chance' => 10.000,
                    'amount' => 1,
                ],
                [
                    'id' => 9,
                    'item_type' => 'advice_gachapon',
                    'product_title' => 'เกล็ดมังกรไฟ',
                    'product_id' => 1987,
                    'product_quantity' => 1,
                    'chance' => 15.000,
                    'amount' => 5,
                ],
                [
                    'id' => 10,
                    'item_type' => 'advice_gachapon',
                    'product_title' => 'หินจันทรา',
                    'product_id' => 571,
                    'product_quantity' => 1,
                    'chance' => 15.000,
                    'amount' => 10,
                ],
            ];
        }

        public function adviceGaranteeReward(){
            return [
                'item_type' => 'advice_gachapon',
                'product_title' => 'สัญลักษณ์ฮงมุน x5',
                'product_id' => 1990,
                'product_quantity' => 1,
                'amount' => 5,
            ];
        }

        public function adviceGaranteePoints(){
            return [
                'point_title' => 'เหรียญ Advice x10',
                'point_quantity' => 10,
            ];
        }

        ////////////  Advice Gachapon  /////////////

        ////////////  Free Gachapon  /////////////

        public function getRandomFreeReward(){

            $rewards = $this->getFreeRewardsList();

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($rewards as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    // $value['color'] = $this->randomColor();
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            $reward['timestamp'] = time();

            // now we have the reward
            return $reward;
        }

        private function getFreeRewardsList(){
            return [
                [
                    'id' => 1,
                    'item_type' => 'free_gachapon',
                    'product_title' => 'หินจันทรา',
                    'product_id' => 779,
                    'product_quantity' => 1,
                    'chance' => 5.000,
                    'amount' => 5,
                ],
                [
                    'id' => 2,
                    'item_type' => 'free_gachapon',
                    'product_title' => 'ยันต์ท้าทายรวดเร็ววิถีฮงมุน',
                    'product_id' => 1999,
                    'product_quantity' => 1,
                    'chance' => 5.000,
                    'amount' => 2,
                ],
                [
                    'id' => 3,
                    'item_type' => 'free_gachapon',
                    'product_title' => 'หินโซล',
                    'product_id' => 1996,
                    'product_quantity' => 1,
                    'chance' => 10.000,
                    'amount' => 10,
                ],
                [
                    'id' => 4,
                    'item_type' => 'free_gachapon',
                    'product_title' => 'ชิ้นส่วนสัญลักษณ์กลุ่มโจรสลัดแมว',
                    'product_id' => 1995,
                    'product_quantity' => 1,
                    'chance' => 10.000,
                    'amount' => 1,
                ],
                [
                    'id' => 5,
                    'item_type' => 'free_gachapon',
                    'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 4',
                    'product_id' => 1719,
                    'product_quantity' => 1,
                    'chance' => 10.000,
                    'amount' => 1,
                ],
                [
                    'id' => 6,
                    'item_type' => 'free_gachapon',
                    'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 3',
                    'product_id' => 1718,
                    'product_quantity' => 1,
                    'chance' => 10.000,
                    'amount' => 1,
                ],
                [
                    'id' => 7,
                    'item_type' => 'free_gachapon',
                    'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 2',
                    'product_id' => 1717,
                    'product_quantity' => 1,
                    'chance' => 15.000,
                    'amount' => 1,
                ],
                [
                    'id' => 8,
                    'item_type' => 'free_gachapon',
                    'product_title' => 'กล่องโซลชิลด์พายุดำหมายเลข 1',
                    'product_id' => 1716,
                    'product_quantity' => 1,
                    'chance' => 15.000,
                    'amount' => 1,
                ],
                [
                    'id' => 9,
                    'item_type' => 'free_gachapon',
                    'product_title' => 'เกล็ดมังกรไฟ',
                    'product_id' => 1998,
                    'product_quantity' => 1,
                    'chance' => 10.000,
                    'amount' => 2,
                ],
                [
                    'id' => 10,
                    'item_type' => 'free_gachapon',
                    'product_title' => 'ชิ้นส่วนฉลามดำ',
                    'product_id' => 1997,
                    'product_quantity' => 1,
                    'chance' => 10.000,
                    'amount' => 1,
                ],
            ];

        }

        public function freeGaranteeReward(){
            return [
                'item_type' => 'free_gachapon',
                'product_title' => 'สัญลักษณ์ฮงมุน x5',
                'product_id' => 1990,
                'product_quantity' => 1,
                'amount' => 5,
            ];
        }

        public function freeGaranteePoints(){
            return [
                'point_title' => 'เหรียญ Advice x1',
                'point_quantity' => 1,
            ];
        }

        ////////////  Free Gachapon  /////////////

        ////////////  Exchange  /////////////

        public function setExchangeByPackageId($packageId=null){
            if(empty($packageId)){
                return false;
            }

            $rewards = $this->exchangeRewardList();

            return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
        }

        public function exchangeRewardList(){
            return [
                [
                    'id' => 1,
                    'item_type' => 'exchange_points',
                    'product_title' => 'หินเปลี่ยนรูปขั้นสูง',
                    'product_id' => 771,
                    'product_quantity' => 1,
                    'require_points' => 50,
                    'amount' => 1,
                ],
                [
                    'id' => 2,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ชุดศิลปแจกันโบราณ',
                    'product_id' => 1992,
                    'product_quantity' => 1,
                    'require_points' => 50,
                    'amount' => 1,
                ],
                [
                    'id' => 3,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ถุงเพชรแปดเหลี่ยม',
                    'product_id' => 1731,
                    'product_quantity' => 1,
                    'require_points' => 50,
                    'amount' => 1,
                ],
                [
                    'id' => 4,
                    'item_type' => 'exchange_points',
                    'product_title' => 'อาวุธลวงตาโพไซดอน',
                    'product_id' => 1994,
                    'product_quantity' => 1,
                    'require_points' => 50,
                    'amount' => 1,
                ],
                [
                    'id' => 5,
                    'item_type' => 'exchange_points',
                    'product_title' => 'หมวกศิลปพัดโบราณ',
                    'product_id' => 1993,
                    'product_quantity' => 1,
                    'require_points' => 30,
                    'amount' => 1,
                ],
                [
                    'id' => 6,
                    'item_type' => 'exchange_points',
                    'product_title' => 'วิญญาณวายุทมิฬ',
                    'product_id' => 2000,
                    'product_quantity' => 1,
                    'require_points' => 25,
                    'amount' => 1,
                ],
                [
                    'id' => 7,
                    'item_type' => 'exchange_points',
                    'product_title' => 'กล่องอัญมณีหกเหลี่ยมของฮงมุนพิเศษ',
                    'product_id' => 2043,
                    'product_quantity' => 1,
                    'require_points' => 10,
                    'amount' => 1,
                ],
                [
                    'id' => 8,
                    'item_type' => 'exchange_points',
                    'product_title' => 'หินจันทรา',
                    'product_id' => 571,
                    'product_quantity' => 1,
                    'require_points' => 5,
                    'amount' => 10,
                ],
                [
                    'id' => 9,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ประกายฉลามดำ',
                    'product_id' => 1855,
                    'product_quantity' => 1,
                    'require_points' => 5,
                    'amount' => 5,
                ],
                [
                    'id' => 10,
                    'item_type' => 'exchange_points',
                    'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว',
                    'product_id' => 1873,
                    'product_quantity' => 1,
                    'require_points' => 5,
                    'amount' => 1,
                ],
                [
                    'id' => 11,
                    'item_type' => 'exchange_points',
                    'product_title' => 'สัญลักษณ์ฮงมุน',
                    'product_id' => 2002,
                    'product_quantity' => 1,
                    'require_points' => 5,
                    'amount' => 1,
                ],
                [
                    'id' => 12,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ยันต์ท้าทายรวดเร็ววิถีฮงมุน',
                    'product_id' => 2003,
                    'product_quantity' => 1,
                    'require_points' => 3,
                    'amount' => 1,
                ],
                [
                    'id' => 13,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ยาโชคชะตา',
                    'product_id' => 1674,
                    'product_quantity' => 1,
                    'require_points' => 1,
                    'amount' => 1,
                ],
                [
                    'id' => 14,
                    'item_type' => 'exchange_points',
                    'product_title' => 'พลุขนาดเล็ก',
                    'product_id' => 2004,
                    'product_quantity' => 1,
                    'require_points' => 1,
                    'amount' => 1,
                ],
                [
                    'id' => 15,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ยันต์มิตรภาพ',
                    'product_id' => 2005,
                    'product_quantity' => 1,
                    'require_points' => 1,
                    'amount' => 1,
                ],
            ];
        }

        ////////////  Exchange  /////////////

    }