<?php

    namespace App\Models\Test\bns_advice;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {

        protected $connection = 'events_bns_test';
        protected $table = 'bns_advice_send_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'reward_id',
            'deduct_points_id',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'type', // 'advice_gachapon','free_gachapon','exchange_points'
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    