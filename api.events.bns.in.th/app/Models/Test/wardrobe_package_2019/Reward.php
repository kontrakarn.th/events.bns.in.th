<?php

    namespace App\Models\Test\wardrobe_package_2019;

    class Reward {

        public function setRewardByPackageId($packageId=null){
            if(empty($packageId)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'Wardrobe Package 1',
                    'require_diamonds' => 90000,
                    'purchase_limit' => 1,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'Wardrobe',
                            'product_id' => 381,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ชุดนักเรียนแลกเปลี่ยน',
                            'product_id' => 2087,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 3,
                            'product_title' => 'หินสัตว์เลี้ยง จิ้งจอก',
                            'product_id' => 2088,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 4,
                            'product_title' => 'VIP 9,000 Points',
                            'product_id' => 380,
                            'product_quantity' => 9000
                        ],
                    ]
                ],
                [
                    'package_id' => 2,
                    'package_title' => 'Wardrobe Package 2',
                    'require_diamonds' => 40000,
                    'purchase_limit' => 2,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชุดนักเรียนแลกเปลี่ยน',
                            'product_id' => 2087,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'หินสัตว์เลี้ยง จิ้งจอก',
                            'product_id' => 2088,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 3,
                            'product_title' => 'VIP 4,000 Points',
                            'product_id' => 380,
                            'product_quantity' => 4000
                        ],
                    ]
                ],

            ];
        }

    }