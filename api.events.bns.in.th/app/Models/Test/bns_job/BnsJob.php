<?php

namespace App\Models\Test\bns_job;

use Illuminate\Database\Eloquent\Model;

class BnsJob extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'bns_job';
    protected $fillable = [
        'job_id',
        'job_th_name',
        'job_en_name',
    ];
}
