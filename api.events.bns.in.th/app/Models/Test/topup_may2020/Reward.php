<?php

    namespace App\Models\Test\topup_may2020;

    use App\Models\BaseModel;

    class Reward extends BaseModel {

        protected $connection = 'events_bns_test';
        protected $table = 'topup_may2020_rewards';
        protected $fillable = [
            'package_name',
            'package_id',
            'package_quantity',
            'package_amount',
            'package_key',
            'topup_bonus',
            'total_rights',
            'used_rights',
            'topup_require',
            'diamonds_require',
            'image',
            'available_date',
        ];

    }
    