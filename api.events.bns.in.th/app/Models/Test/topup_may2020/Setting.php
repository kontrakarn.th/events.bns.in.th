<?php

    namespace App\Models\Test\topup_may2020;

    use App\Models\BaseModel;

    class Setting extends BaseModel {

        protected $connection = 'events_bns_test';
        protected $table = 'topup_may2020_setting';
        protected $fillable = [
            'event_start',
            'event_end',
            'topup_start',
            'topup_end',
            'active',
        ];

    }
    