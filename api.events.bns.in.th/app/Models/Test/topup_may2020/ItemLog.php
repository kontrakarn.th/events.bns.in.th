<?php

    namespace App\Models\Test\topup_may2020;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {

        protected $connection = 'events_bns_test';
        protected $table = 'topup_may2020_send_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'package_name',
            'package_id',
            'package_quantity',
            'package_amount',
            'package_key',
            'image',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    