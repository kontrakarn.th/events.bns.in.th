<?php

    namespace App\Models\Test\airpay_dec2018;

    use App\Models\BaseModel;

    class Member extends BaseModel {

        protected $connection = 'events_bns_test';
        protected $table = 'airpay_december_2018_members';
        protected $fillable = [
            'id',
            'uid',
            'ncid',
            'username',
            'char_id',
            'char_name',
            'world_id',
            'last_ip',
            'total_diamonds',
            'ticket_amount',
            'coin_amount',
            'created_at',
            'updated_at',
        ];
    }
