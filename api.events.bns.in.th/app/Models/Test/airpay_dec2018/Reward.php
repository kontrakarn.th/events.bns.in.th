<?php
    // Mission for disciple
    namespace App\Models\Test\airpay_dec2018;

    class Reward {

        public function setRewardByPackage($packageKey=''){
            if(empty($packageKey)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageKey-1]) && !empty($rewards[$packageKey-1])) ? $rewards[$packageKey-1] : false;
        }

        public function setGachaByKey($packageKey=''){
            if(empty($packageKey)){
                return false;
            }

            $rewards = $this->gachaList();

            return (isset($rewards[$packageKey-1]) && !empty($rewards[$packageKey-1])) ? $rewards[$packageKey-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'ชุดความเย้ายวนหน้าหนาว',
                    'package_desc' => 'ชุดความเย้ายวนหน้าหนาว',
                    'require_coins' => 5,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชุดความเย้ายวนหน้าหนาว',
                            'product_id' => 1921,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 2,
                    'package_title' => 'ชุดเสื้อแจ็คเก็ตกลุ่มรักอิสระ',
                    'package_desc' => 'ชุดเสื้อแจ็คเก็ตกลุ่มรักอิสระ',
                    'require_coins' => 3,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชุดเสื้อแจ็คเก็ตกลุ่มรักอิสระ',
                            'product_id' => 1922,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 3,
                    'package_title' => 'หินจันทรา 50 ชิ้น',
                    'package_desc' => 'หินจันทรา 50 ชิ้น',
                    'require_coins' => 1,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินจันทรา 50 ชิ้น',
                            'product_id' => 1925,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 4,
                    'package_title' => 'ประกายฉลามดำ 5 ชิ้น',
                    'package_desc' => 'ประกายฉลามดำ 5 ชิ้น',
                    'require_coins' => 1,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ประกายฉลามดำ 5 ชิ้น',
                            'product_id' => 1855,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 5,
                    'package_title' => 'หินโซล 100 ชิ้น',
                    'package_desc' => 'หินโซล 100 ชิ้น',
                    'require_coins' => 1,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินโซล 100 ชิ้น',
                            'product_id' => 1918,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 6,
                    'package_title' => 'ยันต์อัพเกรด วิถีฮงมุนที่ส่องสว่าง',
                    'package_desc' => 'ยันต์อัพเกรด วิถีฮงมุนที่ส่องสว่าง',
                    'require_coins' => 2,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ยันต์อัพเกรด วิถีฮงมุนที่ส่องสว่าง',
                            'product_id' => 1926,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
            ];
        }

        public static function gachaList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'ชุดความเย้ายวนหน้าหนาว (1)',
                    'package_desc' => 'ชุดความเย้ายวนหน้าหนาว (1)',
                    'rate_min' => 1,
                    'rate_max' => 15000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชุดความเย้ายวนหน้าหนาว',
                            'product_id' => 1921,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 2,
                    'package_title' => 'ชุดเสื้อแจ็คเก็ตกลุ่มรักอิสระ (1)',
                    'package_desc' => 'ชุดเสื้อแจ็คเก็ตกลุ่มรักอิสระ (1)',
                    'rate_min' => 15001,
                    'rate_max' => 35000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชุดเสื้อแจ็คเก็ตกลุ่มรักอิสระ',
                            'product_id' => 1922,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 3,
                    'package_title' => 'กล่องอัญมณีห้าเหลี่ยมของฮงมุน (3)',
                    'package_desc' => 'กล่องอัญมณีห้าเหลี่ยมของฮงมุน (3)',
                    'rate_min' => 35001,
                    'rate_max' => 60000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'กล่องอัญมณีห้าเหลี่ยมของฮงมุน 3 กล่อง',
                            'product_id' => 1923,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 4,
                    'package_title' => 'สัญลักษณ์ฮงมุน (20)',
                    'package_desc' => 'สัญลักษณ์ฮงมุน (20)',
                    'rate_min' => 60001,
                    'rate_max' => 80000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'สัญลักษณ์ฮงมุน (20)',
                            'product_id' => 1878,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 5,
                    'package_title' => 'หินจันทรา (1)',
                    'package_desc' => 'หินจันทรา (1)',
                    'rate_min' => 80001,
                    'rate_max' => 100000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินจันทรา (1)',
                            'product_id' => 1924,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
            ];
        }

        public static function fixList(){
            return [
                [
                    'id' => 2,
                    'product_title' => 'พลุแห่งการเฉลิมฉลอง 5 ชิ้น',
                    'product_id' => 1920,
                    'product_quantity' => 1,
                    'product_type'=>'ingame'
                ],
                [
                    'id' => 3,
                    'product_title' => 'เหรียญ AirPay',
                    'product_id' => 0,
                    'product_quantity' => 2,
                    'product_type'=>'event'
                ],

            ];
        }
    }
