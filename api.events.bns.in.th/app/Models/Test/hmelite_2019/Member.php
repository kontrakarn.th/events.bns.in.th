<?php

namespace App\Models\Test\hmelite_2019;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'hmelite_2019_member';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'firstname',
        'lastname',
        'nickname',
        'tel',
        'email',
        'birthday',
        'vip',
        'last_ip',
    ];

    public static function getMemberByUid($uid){
        $data=self::where('uid',$uid)->first();
        return $data;
    }

    public static function getMemberByNickname($nickname){
        $data=self::where('nickname',$nickname)->first();
        return $data;
    }
}
