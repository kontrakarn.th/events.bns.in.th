<?php

namespace App\Models\Test\hmelite_2019;
use Carbon\Carbon;

class Reward
{

    public static function getRandomRewardInfo($item)
    {
        // dd(collect($item)->sum('weight'));
        $sum_weight=collect($item)->sum('weight');
        if($sum_weight==0){
            return false;
        }

        $random = rand(1, $sum_weight);
        $accumulatedChance = 0;
        $reward = false;

        foreach ($item as $value) {
            // $chance = $value['chance'] * 1000;
            $currentChance = $accumulatedChance + $value->weight;

                // acquired reward
            if ($random > $accumulatedChance && $random <= $currentChance) {
                $reward = $value;
                break;
            }
            $accumulatedChance = $currentChance;
        }

        $reward->random = $random;

            // now we have the reward
        return $reward;
    }

}
