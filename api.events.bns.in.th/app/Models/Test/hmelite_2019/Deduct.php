<?php

namespace App\Models\Test\hmelite_2019;

use Illuminate\Database\Eloquent\Model;

class Deduct extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'hmelite_2019_deduct';
    protected $fillable = [
        'uid',
        'ncid',
        'username',
        'type',
        'item_no',
        'diamond',
        'before_deduct_diamond',
        'after_deduct_diamond',
        'status',
        'deduct_data',
        'deduct_purchase_id',
        'deduct_purchase_status',
        'last_ip',
    ];

}
