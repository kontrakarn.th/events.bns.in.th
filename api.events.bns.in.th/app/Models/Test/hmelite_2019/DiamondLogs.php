<?php

namespace App\Models\Test\hmelite_2019;

use Illuminate\Database\Eloquent\Model;

class DiamondLogs extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'hmelite_2019_diamond_logs';
    protected $fillable = [
        'uid',
        'ncid',
        'username',
        'nickname',
        'diamond',
        'ranking_at',
    ];

}
