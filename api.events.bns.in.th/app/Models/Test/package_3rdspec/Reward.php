<?php
    // Mission for disciple
    namespace App\Models\Test\package_3rdspec;

    class Reward {

        public function setRewardByPackage($packageKey=''){
            if(empty($packageKey)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageKey-1]) && !empty($rewards[$packageKey-1])) ? $rewards[$packageKey-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'แพ็คเกจคมดาบเทวาพิทักษ์',
                    'package_desc' => 'แพ็คเกจคมดาบเทวาพิทักษ์',
                    'require_diamonds' => 150000, // 150000
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'แพ็คเกจคมดาบเทวาพิทักษ์ SET 1',
                            'product_id' => 2978,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'แพ็คเกจคมดาบเทวาพิทักษ์ SET 2',
                            'product_id' => 2980,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 2,
                    'package_title' => 'แพ็คเกจเขี้ยวเล็บของหมาป่าทมิฬ',
                    'package_desc' => 'แพ็คเกจเขี้ยวเล็บของหมาป่าทมิฬ',
                    'require_diamonds' => 150000, // 150000
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'แพ็คเกจเขี้ยวเล็บของหมาป่าทมิฬ SET 1',
                            'product_id' => 2979,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'แพ็คเกจเขี้ยวเล็บของหมาป่าทมิฬ SET 2',
                            'product_id' => 2980,
                            'product_quantity' => 1
                        ]
                    ]
                ],
            ];
        }

    }
