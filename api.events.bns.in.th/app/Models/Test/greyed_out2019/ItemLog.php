<?php

    namespace App\Models\Test\greyed_out2019;

    use Moloquent;

    class ItemLog extends Moloquent {

        protected $connection = 'mongodb_bns_test';
        protected $table = 'greyed_out2019_item_logs';
        protected $fillable = [
            'uid',
            'ncid',
            'deduct_id',
            'group_id',
            'pool_round',
            'pool_price',
            'product_id',
            'product_title',
            'product_quantity',
            "amount",
            'product_image',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status', // pending, success, unsuccess
            'last_ip',
            'created_at_string',
            'created_at_string_time',
            'log_date',
            'log_date_timestamp'
        ];

    }
