<?php

    namespace App\Models\Test\greyed_out2019;

    use Moloquent;

    class DeductLog extends Moloquent {

        protected $connection = 'mongodb_bns_test';
        protected $table = 'greyed_out2019_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'group_id',
            'pool_round',
            'pool_price',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }