<?php

    namespace App\Models\Test\greyed_out2019;

    use Moloquent;

    class UserGroup extends Moloquent {
        protected $connection = 'mongodb_bns_test';
        protected $table = 'greyed_out2019_user_groups';
        protected $fillable = [
            'uid',
            'group_id'
        ];
    }