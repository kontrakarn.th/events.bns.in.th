<?php

namespace App\Models\Test\halloween2019;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'halloween2019_rewards';

}
