<?php

namespace App\Models\Test\halloween2019;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'halloween2019_members';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'last_ip',
        'quest_success_1',
        'quest_success_2',
        'quest_success_3',
        'quest_success_4',
        'quest_success_5',
        'quest_success_6',
        'quest_success_7',
        'quest_success_8',
        'quest_success_9',
        'quest_success_10',
        'quest_success_11',
        'quest_success_12',
        'quest_success_13',
        'quest_success_14',
        'coin',
        'coin_used',
    ];
}
