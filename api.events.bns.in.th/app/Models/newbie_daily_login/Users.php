<?php

namespace App\Models\newbie_daily_login;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'newbie_daily_login_users';
    protected $fillable = [
        'id',
        'uid',
        'username',
        'ncid',
        'char_id',
        'char_name',
        'world_id',
        'job',
        'last_ip',
        'can_get_package',
    ];

}
