<?php

namespace App\Models\kylin;

use Illuminate\Database\Eloquent\Model;

class DeductLog extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'kylin_deduct';
    protected $fillable = [
        'uid',
        'ncid',
        'username',
        'type',
        'item_no',
        'diamond',
        'before_deduct_diamond',
        'after_deduct_diamond',
        'status',
        'deduct_data',
        'deduct_purchase_id',
        'deduct_purchase_status',
        'last_ip',
    ];

}
