<?php

namespace App\Models\kylin;

use Illuminate\Database\Eloquent\Model;

class ItemHistory extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'kylin_item_history';
    protected $fillable = [
        'uid',
        'ncid',
        'item_type',
        'item_no',
        'product_title',
        'product_id',
        'product_quantity',
        'last_ip',
    ];
}
