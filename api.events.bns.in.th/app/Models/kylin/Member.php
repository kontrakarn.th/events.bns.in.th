<?php

namespace App\Models\kylin;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'kylin_members';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'last_ip',
    ];

    public static function getMemberByUid($uid){
        $data=self::where('uid',$uid)->first();
        return $data;
    }

    public static function getMemberByNickname($nickname){
        $data=self::where('nickname',$nickname)->first();
        return $data;
    }
}
