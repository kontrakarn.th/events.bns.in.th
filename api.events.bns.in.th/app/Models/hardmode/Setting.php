<?php

namespace App\Models\hardmode;

use App\Models\BaseModel;

class Setting extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'hardmode_settings';
    protected $fillable = [
        'event_start',
        'event_end',
        'open_box_start',
        'open_box_end',
        'claim_reward_start',
        'claim_reward_end',
        'active',
    ];
}
