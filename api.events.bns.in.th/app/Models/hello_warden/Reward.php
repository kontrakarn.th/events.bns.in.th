<?php

    namespace App\Models\hello_warden;

    class Reward {

        public function setRewardByPackageId($packageId=null){
            if(empty($packageId)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'Hello Warden',
                    'amount' => 1,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'กล่องต้อนรับผู้ฝึกยุทธหน้าใหม่',
                            'product_id' => 2007,
                            'product_quantity' => 1
                        ]
                    ]
                ],

            ];
        }

    }
