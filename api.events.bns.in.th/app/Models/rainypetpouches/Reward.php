<?php

    namespace App\Models\rainypetpouches;

    use App\Models\BaseModel;

    class Reward extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'rainypetpouches_rewards';
        protected $fillable = [
            'reward_type',
            'package_name',
            'package_id',
            'package_quantity',
            'package_amount',
            'package_type', 
            'package_key',
            'chance',
            'prop',
            'image',
            'order',
            'available_date',
        ];
    }