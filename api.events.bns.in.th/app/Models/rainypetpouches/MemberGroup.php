<?php

    namespace App\Models\rainypetpouches;

    use App\Models\BaseModel;

    class MemberGroup extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'rainypetpouches_members_group';
        protected $fillable = [
            'uid',
            'group',
        ];

    }