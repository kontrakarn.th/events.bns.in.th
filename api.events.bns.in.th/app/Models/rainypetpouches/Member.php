<?php

    namespace App\Models\rainypetpouches;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'rainypetpouches_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'last_ip',
            'group',
        ];

    }