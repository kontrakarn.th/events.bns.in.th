<?php

    namespace App\Models\topup_nov2018;

    use Moloquent;

    class Member extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'topup_nov2018_member';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'total_diamonds',
            'last_ip'
        ];
    }
