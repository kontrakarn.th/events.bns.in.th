<?php

namespace App\Models\halloween2019;

use Illuminate\Database\Eloquent\Model;

class SendItemLog extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'halloween2019_send_item_logs';

    protected $fillable = [
        'uid',
        'ncid',
        'product_id',
        'product_title',
        'product_quantity',
        'coin',
        'send_item_status',
        'send_item_purchase_id',
        'send_item_purchase_status',
        'send_item_respone',
        'goods_data',
        'last_ip',
        'log_date',
        'log_date_timestamp',
        'send_gift_from'
    ];
}
