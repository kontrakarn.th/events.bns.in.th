<?php

namespace App\Models\halloween2019;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'halloween2019_rewards';

}
