<?php

    namespace App\Models\snakeladder;

    use App\Models\BaseModel;

    class PaidDeductLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'snakeladder_paid_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'diamonds',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'deduct_type', // 'send_gift','random_board','board_dice'
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip'
        ];

    }
    