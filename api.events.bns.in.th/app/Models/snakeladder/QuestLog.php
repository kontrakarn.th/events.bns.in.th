<?php

    namespace App\Models\snakeladder;

    use App\Models\BaseModel;

    class QuestLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'snakeladder_free_quest_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'world_id',
            'job',
            'quest_id',
            'quest_code',
            'quest_title',
            'status', // success, unsuccess
            'used_status', // not_use, used
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }