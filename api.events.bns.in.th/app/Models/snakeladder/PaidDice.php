<?php

    namespace App\Models\snakeladder;

    class PaidDice {

        // Random Dice
        public function randomDice(){

            $diceRate = $this->getDiceRate();

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $dice = false;

            foreach ($diceRate as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired dice
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    // $value['color'] = $this->randomColor();
                    $dice = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $dice['random'] = $random;

            $dice['timestamp'] = time();

            // now we have the dice
            return $dice;
        }

        private function getDiceRate(){
            return [
                [
                    'points_title' => '1 แต้ม',
                    'points' => 1,
                    'chance' => 50.000,
                ],
                [
                    'points_title' => '2 แต้ม',
                    'points' => 2,
                    'chance' => 25.000,
                ],
                [
                    'points_title' => '3 แต้ม',
                    'points' => 3,
                    'chance' => 20.000,
                ],
                [
                    'points_title' => '4 แต้ม',
                    'points' => 4,
                    'chance' => 3.000,
                ],
                [
                    'points_title' => '5 แต้ม',
                    'points' => 5,
                    'chance' => 1.000,
                ],
                [
                    'points_title' => '6 แต้ม',
                    'points' => 6,
                    'chance' => 1.000,
                ],
            ];
        }

    }
