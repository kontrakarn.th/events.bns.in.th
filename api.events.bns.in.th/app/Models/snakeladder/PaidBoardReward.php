<?php

    namespace App\Models\snakeladder;

    class PaidBoardReward {

        public function setPaidBoardRewardInfo($boardId, $step){
            // reward list
            switch(true){
                case ($step >= 1 && $step <= 9):
                    return [
                        'product_title' => 'คริสตัลหินโซล x100',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'คริสตัลหินโซล x100',
                                'product_id' => 1067,
                                'product_quantity' => 1,
                                'amount' => 100,
                            ],
                        ],
                    ];
                    break;

                case ($step == 10):
                    return [
                        'product_title' => 'หินโซล x100',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'หินโซล x100',
                                'product_id' => 1918,
                                'product_quantity' => 1,
                                'amount' => 100,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 11 && $step <= 19):
                    return [
                        'product_title' => 'คริสตัลหินจันทรา x30',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'คริสตัลหินจันทรา x30',
                                'product_id' => 1847,
                                'product_quantity' => 1,
                                'amount' => 30,
                            ],
                        ],
                    ];
                    break;

                case ($step == 20):
                    return [
                        'product_title' => 'หินจันทรา x100',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'หินจันทรา x100',
                                'product_id' => 2137,
                                'product_quantity' => 1,
                                'amount' => 100,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 21 && $step <= 29):
                    return [
                        'product_title' => 'สัญลักษณ์ฮงมุน x50',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'สัญลักษณ์ฮงมุน x50',
                                'product_id' => 1881,
                                'product_quantity' => 1,
                                'amount' => 50, 
                            ],
                        ],
                    ];
                    break;

                case ($step == 30):
                    return [
                        'product_title' => 'สัญลักษณ์ฮงมุน x100',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'สัญลักษณ์ฮงมุน x100',
                                'product_id' => 1884,
                                'product_quantity' => 1,
                                'amount' => 100,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 31 && $step <= 39):
                    return [
                        'product_title' => 'สัญลักษณ์โจรสลัดแมว x5',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'สัญลักษณ์โจรสลัดแมว x5',
                                'product_id' => 2138,
                                'product_quantity' => 1,
                                'amount' => 5,
                            ],
                        ],
                    ];
                    break;

                case ($step == 40):
                    return [
                        'product_title' => 'สัญลักษณ์โจรสลัดแมว x50',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'สัญลักษณ์โจรสลัดแมว x50',
                                'product_id' => 2139,
                                'product_quantity' => 1,
                                'amount' => 50,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 41 && $step <= 49):
                    return [
                        'product_title' => 'ประกายฉลามดำ x5',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'ประกายฉลามดำ x5',
                                'product_id' => 1855,
                                'product_quantity' => 1,
                                'amount' => 5,
                            ],
                        ],
                    ];
                    break;

                case ($step == 50):
                    return [
                        'product_title' => 'อัญมณีกรุฮงมุน x2',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'อัญมณีกรุฮงมุน x2',
                                'product_id' => 1891,
                                'product_quantity' => 1,
                                'amount' => 2,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 51 && $step <= 59):
                    return [
                        'product_title' => 'กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส) x2',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส) x2',
                                'product_id' => 2140,
                                'product_quantity' => 1,
                                'amount' => 2,
                            ],
                        ],
                    ];
                    break;

                case ($step == 60):
                    switch($boardId){
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            return [
                                'product_title' => 'อัญมณีหินพิทักษณ์ทะเลสาป x2',
                                'product_set' => [
                                    [
                                        'item_type' => 'normal',
                                        'product_title' => 'อัญมณีหินพิทักษณ์ทะเลสาป x2',
                                        'product_id' => 1892,
                                        'product_quantity' => 1,
                                        'amount' => 2,
                                    ],
                                ],
                            ];
                            break;

                        case 6:
                            return [
                                'product_title' => 'กล่องอาวุธลวงตาเทพพิทักษ์ x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 1,
                                        'product_title' => 'กล่องอาวุธลวงตาเทพพิทักษ์ x1',
                                        'product_id' => 564,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 7:
                            return [
                                'product_title' => 'กล่องอาวุธกุหลาบพิษ x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 2,
                                        'product_title' => 'กล่องอาวุธกุหลาบพิษ x1',
                                        'product_id' => 924,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 8:
                            return [
                                'product_title' => 'กล่องอาวุธลวงตาพยัคฆ์ขาว x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 3,
                                        'product_title' => 'กล่องอาวุธลวงตาพยัคฆ์ขาว x1',
                                        'product_id' => 1157,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 9:
                            return [
                                'product_title' => 'กล่องอาวุธลวงตาแสงแห่งความยินดี x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 4,
                                        'product_title' => 'กล่องอาวุธลวงตาแสงแห่งความยินดี x1',
                                        'product_id' => 2142,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                    }
                    break;
                    
                case ($step >= 61 && $step <= 69):
                    return [
                        'product_title' => 'กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์) x2',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์) x2',
                                'product_id' => 2141,
                                'product_quantity' => 1,
                                'amount' => 2,
                            ],
                        ],
                    ];
                    break;

                case ($step == 70):
                    switch($boardId){
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            return [
                                'product_title' => 'กรุพิศวง x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'normal',
                                        'product_title' => 'กรุพิศวง x1',
                                        'product_id' => 1986,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 6:
                            return [
                                'product_title' => 'หินล้ำค่าเทพพิทักษ์ x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 5,
                                        'product_title' => 'หินล้ำค่าเทพพิทักษ์ x1',
                                        'product_id' => 563,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 7:
                            return [
                                'product_title' => 'หินล้ำค่าปลากัด x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 6,
                                        'product_title' => 'หินล้ำค่าปลากัด x1',
                                        'product_id' => 932,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 8:
                            return [
                                'product_title' => 'หินล้ำค่าพยัคฆ์ขาว x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 7,
                                        'product_title' => 'หินล้ำค่าพยัคฆ์ขาว x1',
                                        'product_id' => 1166,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 9:
                            return [
                                'product_title' => 'หินสัตว์เลี้ยงโดรน x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 8,
                                        'product_title' => 'หินสัตว์เลี้ยงโดรน x1',
                                        'product_id' => 2148,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                    }
                    break;

                case ($step >= 71 && $step <= 79):
                    return [
                        'product_title' => 'ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง x1',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง x1',
                                'product_id' => 2133,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step == 80):
                    switch($boardId){
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            return [
                                'product_title' => 'คริสตัลวิญญาณฮงมุน x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'normal',
                                        'product_title' => 'คริสตัลวิญญาณฮงมุน x1',
                                        'product_id' => 1896,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 6:
                            return [
                                'product_title' => 'เครื่องประดับเทพพิทักษ์ x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 27,
                                        'product_title' => 'เครื่องประดับเทพพิทักษ์ x1',
                                        'product_id' => 561,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 7:
                            return [
                                'product_title' => 'มงกุฎหนามกุหลาบพิษ x1, ผ้าปิดตากุหลาบพิษ x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 17,
                                        'product_title' => 'มงกุฎหนามกุหลาบพิษ x1',
                                        'product_id' => 921,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 19,
                                        'product_title' => 'ผ้าปิดตากุหลาบพิษ x1',
                                        'product_id' => 922,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 8:
                            return [
                                'product_title' => 'หมวกพยัคฆ์ขาว x1, หน้ากากพยัคฆ์ขาว x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 22,
                                        'product_title' => 'หมวกพยัคฆ์ขาว x1',
                                        'product_id' => 1138,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 24,
                                        'product_title' => 'หน้ากากพยัคฆ์ขาว x1',
                                        'product_id' => 1140,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 9:
                            return [
                                'product_title' => 'ที่คาดผมที่เจิดจ้า x1, ต่างหูที่เจิดจ้า x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 12,
                                        'product_title' => 'ที่คาดผมที่เจิดจ้า x1',
                                        'product_id' => 2145,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 14,
                                        'product_title' => 'ต่างหูที่เจิดจ้า x1',
                                        'product_id' => 2146,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                    }
                    break;

                case ($step >= 81 && $step <= 89):
                    return [
                        'product_title' => 'กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส) x1',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส) x1',
                                'product_id' => 2134,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step == 90):
                    switch($boardId){
                        case 1:
                            return [
                                'product_title' => 'สัญลักษณ์สุภาพชน x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 36,
                                        'product_title' => 'สัญลักษณ์สุภาพชน x1',
                                        'product_id' => 342,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 2:
                            return [
                                'product_title' => 'ที่คาดผมหูแมว x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 9,
                                        'product_title' => 'ที่คาดผมหูแมว x1',
                                        'product_id' => 766,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 3:
                            return [
                                'product_title' => 'คริสตัลอัญมณีหยินหยาง x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'normal',
                                        'product_title' => 'คริสตัลอัญมณีหยินหยาง x1',
                                        'product_id' => 1900,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 4:
                            return [
                                'product_title' => 'ท่วงทำนองกระดิ่งขาว x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 34,
                                        'product_title' => 'ท่วงทำนองกระดิ่งขาว x1',
                                        'product_id' => 339,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 5:
                            return [
                                'product_title' => 'เสียงประสานกระดิ่งดำ x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 32,
                                        'product_title' => 'เสียงประสานกระดิ่งดำ x1',
                                        'product_id' => 770,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 6:
                            return [
                                'product_title' => 'ปีกเทพพิทักษ์ x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 28,
                                        'product_title' => 'ปีกเทพพิทักษ์ x1',
                                        'product_id' => 562,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 7:
                            return [
                                'product_title' => 'ปีกกุหลาบพิษ x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 18,
                                        'product_title' => 'ปีกกุหลาบพิษ x1',
                                        'product_id' => 923,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 8:
                            return [
                                'product_title' => 'ปีกพยัคฆ์ขาว x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 23,
                                        'product_title' => 'ปีกพยัคฆ์ขาว x1',
                                        'product_id' => 1139,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;    

                        case 9:
                            return [
                                'product_title' => 'ปีกเจิดจ้า x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 13,
                                        'product_title' => 'ปีกเจิดจ้า x1',
                                        'product_id' => 2144,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                    }
                    break;

                case ($step >= 91 && $step <= 99):
                    return [
                        'product_title' => 'กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์) x1',
                        'product_set' => [
                            [
                                'item_type' => 'normal',
                                'product_title' => 'กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์) x1',
                                'product_id' => 2135,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step == 100):
                    switch($boardId){
                        case 1:
                            return [
                                'product_title' => 'ชุดสง่างาม x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 35,
                                        'product_title' => 'ชุดสง่างาม x1',
                                        'product_id' => 341,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 2:
                            return [
                                'product_title' => 'ชุดขาวของผู้บริสุทธิ์ x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 10,
                                        'product_title' => 'ชุดขาวของผู้บริสุทธิ์ x1',
                                        'product_id' => 767,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 3:
                            return [
                                'product_title' => 'ผีเสื้อสีชาด x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 30,
                                        'product_title' => 'ผีเสื้อสีชาด x1',
                                        'product_id' => 338,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 4:
                            return [
                                'product_title' => 'ชุดท่วงทำนองบริสุทธิ์ x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 33,
                                        'product_title' => 'ชุดท่วงทำนองบริสุทธิ์ x1',
                                        'product_id' => 340,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 5:
                            return [
                                'product_title' => 'ชุดท่วงทำนองเสียงประสาน x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 31,
                                        'product_title' => 'ชุดท่วงทำนองเสียงประสาน x1',
                                        'product_id' => 769,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 6:
                            return [
                                'product_title' => 'ชุดเทพพิทักษ์ x1, เซ็ทเทพพิทักษ์เมี้ยว x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 26,
                                        'product_title' => 'ชุดเทพพิทักษ์ x1',
                                        'product_id' => 560,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 29,
                                        'product_title' => 'เซ็ทเทพพิทักษ์เมี้ยว x1',
                                        'product_id' => 2149,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 7:
                            return [
                                'product_title' => 'ชุดกุหลาบพิษ x1, เซ็ทเหมียวกุหลาบพิษ x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 16,
                                        'product_title' => 'ชุดกุหลาบพิษ x1',
                                        'product_id' => 920,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 20,
                                        'product_title' => 'เซ็ทเหมียวกุหลาบพิษ x1',
                                        'product_id' => 934,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 8:
                            return [
                                'product_title' => 'ชุดพยัคฆ์ขาว x1, เซ็ทเหมียวพยัคฆ์ขาว x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 21,
                                        'product_title' => 'ชุดพยัคฆ์ขาว x1',
                                        'product_id' => 1137,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 25,
                                        'product_title' => 'เซ็ทเหมียวพยัคฆ์ขาว x1',
                                        'product_id' => 2150,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                        case 9:
                            return [
                                'product_title' => 'ชุดแสงที่เจิดจ้า x1, เซ็ทแมวเหมียวที่เจิดจ้า x1',
                                'product_set' => [
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 11,
                                        'product_title' => 'ชุดแสงที่เจิดจ้า x1',
                                        'product_id' => 2143,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                    [
                                        'item_type' => 'rare',
                                        'item_group' => 15,
                                        'product_title' => 'เซ็ทแมวเหมียวที่เจิดจ้า x1',
                                        'product_id' => 2147,
                                        'product_quantity' => 1,
                                        'amount' => 1,
                                    ],
                                ],
                            ];
                            break;

                    }
                    break;

                default:
                    return false;
                    break;

            }

            return false;
        }

        public function rareItemList(){
            return [
                [
                    'id' => 1, // group id
                    'title' => 'กล่องอาวุธลวงตาเทพพิทักษ์',
                ],
                [
                    'id' => 2, // group id
                    'title' => 'กล่องอาวุธกุหลาบพิษ',
                ],
                [
                    'id' => 3, // group id
                    'title' => 'กล่องอาวุธลวงตาพยัคฆ์ขาว',
                ],
                [
                    'id' => 4, // group id
                    'title' => 'กล่องอาวุธลวงตาแสงแห่งความยินดี',
                ],
                [
                    'id' => 5, // group id
                    'title' => 'หินล้ำค่าเทพพิทักษ์',
                ],
                [
                    'id' => 6, // group id
                    'title' => 'หินล้ำค่าปลากัด',
                ],
                [
                    'id' => 7, // group id
                    'title' => 'หินล้ำค่าพยัคฆ์ขาว',
                ],
                [
                    'id' => 8, // group id
                    'title' => 'หินสัตว์เลี้ยงโดรน',
                ],
                [
                    'id' => 9, // group id
                    'title' => 'ที่คาดผมหูแมว',
                ],
                [
                    'id' => 10, // group id
                    'title' => 'ชุดขาวของผู้บริสุทธิ์',
                ],
                [
                    'id' => 11, // group id
                    'title' => 'ชุดแสงที่เจิดจ้า',
                ],
                [
                    'id' => 12, // group id
                    'title' => 'ที่คาดผมที่เจิดจ้า',
                ],
                [
                    'id' => 13, // group id
                    'title' => 'ปีกเจิดจ้า',
                ],
                [
                    'id' => 14, // group id
                    'title' => 'ต่างหูที่เจิดจ้า',
                ],
                [
                    'id' => 15, // group id
                    'title' => 'เซ็ทแมวเหมียวที่เจิดจ้า',
                ],
                [
                    'id' => 16, // group id
                    'title' => 'ชุดกุหลาบพิษ',
                ],
                [
                    'id' => 17, // group id
                    'title' => 'มงกุฎหนามกุหลาบพิษ',
                ],
                [
                    'id' => 18, // group id
                    'title' => 'ปีกกุหลาบพิษ',
                ],
                [
                    'id' => 19, // group id
                    'title' => 'ผ้าปิดตากุหลาบพิษ',
                ],
                [
                    'id' => 20, // group id
                    'title' => 'เซ็ทเหมียวกุหลาบพิษ',
                ],
                [
                    'id' => 21, // group id
                    'title' => 'ชุดพยัคฆ์ขาว',
                ],
                [
                    'id' => 22, // group id
                    'title' => 'หมวกพยัคฆ์ขาว',
                ],
                [
                    'id' => 23, // group id
                    'title' => 'ปีกพยัคฆ์ขาว',
                ],
                [
                    'id' => 24, // group id
                    'title' => 'หน้ากากพยัคฆ์ขาว',
                ],
                [
                    'id' => 25, // group id
                    'title' => 'เซ็ทเหมียวพยัคฆ์ขาว',
                ],
                [
                    'id' => 26, // group id
                    'title' => 'ชุดเทพพิทักษ์',
                ],
                [
                    'id' => 27, // group id
                    'title' => 'เครื่องประดับเทพพิทักษ์',
                ],
                [
                    'id' => 28, // group id
                    'title' => 'ปีกเทพพิทักษ์',
                ],
                [
                    'id' => 29, // group id
                    'title' => 'เซ็ทเทพพิทักษ์เมี้ยว',
                ],
                [
                    'id' => 30, // group id
                    'title' => 'ผีเสื้อสีชาด',
                ],
                [
                    'id' => 31, // group id
                    'title' => 'ชุดท่วงทำนองเสียงประสาน',
                ],
                [
                    'id' => 32, // group id
                    'title' => 'เสียงประสานกระดิ่งดำ',
                ],
                [
                    'id' => 33, // group id
                    'title' => 'ชุดท่วงทำนองบริสุทธิ์',
                ],
                [
                    'id' => 34, // group id
                    'title' => 'ท่วงทำนองกระดิ่งขาว',
                ],
                [
                    'id' => 35, // group id
                    'title' => 'ชุดสง่างาม',
                ],
                [
                    'id' => 36, // group id
                    'title' => 'สัญลักษณ์สุภาพชน',
                ],
            ];
        }

    }