<?php

    namespace App\Models\snakeladder;

    use App\Models\BaseModel;

    class PaidBoardDiceLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'snakeladder_paid_board_dice_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'deduct_id',
            'board_paid_id',
            'points',
            'points_title',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }