<?php

    namespace App\Models\snakeladder;

    class FreeDice {

        // Random Dice
        public function randomDice(){

            $diceRate = $this->getDiceRate();

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $dice = false;

            foreach ($diceRate as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired dice
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    // $value['color'] = $this->randomColor();
                    $dice = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $dice['random'] = $random;

            $dice['timestamp'] = time();

            // now we have the dice
            return $dice;
        }

        private function getDiceRate(){
            return [
                [
                    'points_title' => '1 แต้ม',
                    'points' => 1,
                    'chance' => 20.000,
                ],
                [
                    'points_title' => '2 แต้ม',
                    'points' => 2,
                    'chance' => 20.000,
                ],
                [
                    'points_title' => '3 แต้ม',
                    'points' => 3,
                    'chance' => 15.000,
                ],
                [
                    'points_title' => '4 แต้ม',
                    'points' => 4,
                    'chance' => 15.000,
                ],
                [
                    'points_title' => '5 แต้ม',
                    'points' => 5,
                    'chance' => 15.000,
                ],
                [
                    'points_title' => '6 แต้ม',
                    'points' => 6,
                    'chance' => 15.000,
                ],
            ];
        }

        public function getFreeDiceReward($step){

            switch(true){
                case ($step >= 1 && $step <= 9):
                    return [
                        'product_title' => 'คริสตัลหินโซล x50',
                        'product_set' => [
                            [
                                'product_title' => 'คริสตัลหินโซล x50',
                                'product_id' => 274,
                                'product_quantity' => 1,
                                'amount' => 50,
                            ],
                        ],
                    ];
                    break;

                case ($step == 10):
                    return [
                        'product_title' => 'หินโซล x50',
                        'product_set' => [
                            [
                                'product_title' => 'หินโซล x50',
                                'product_id' => 1151,
                                'product_quantity' => 1,
                                'amount' => 50,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 11 && $step <= 19):
                    return [
                        'product_title' => 'คริสตัลหินจันทรา x15',
                        'product_set' => [
                            [
                                'product_title' => 'คริสตัลหินจันทรา x15',
                                'product_id' => 1845,
                                'product_quantity' => 1,
                                'amount' => 15,
                            ],
                        ],
                    ];
                    break;

                case ($step == 20):
                    return [
                        'product_title' => 'หินจันทรา x50',
                        'product_set' => [
                            [
                                'product_title' => 'หินจันทรา x50',
                                'product_id' => 1925,
                                'product_quantity' => 1,
                                'amount' => 50,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 21 && $step <= 29):
                    return [
                        'product_title' => 'สัญลักษณ์ฮงมุน x25',
                        'product_set' => [
                            [
                                'product_title' => 'สัญลักษณ์ฮงมุน x25',
                                'product_id' => 2129,
                                'product_quantity' => 1,
                                'amount' => 25,
                            ],
                        ],
                    ];
                    break;

                case ($step == 30):
                    return [
                        'product_title' => 'สัญลักษณ์ฮงมุน x50',
                        'product_set' => [
                            [
                                'product_title' => 'สัญลักษณ์ฮงมุน x50',
                                'product_id' => 1881,
                                'product_quantity' => 1,
                                'amount' => 50,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 31 && $step <= 39):
                    return [
                        'product_title' => 'สัญลักษณ์โจรสลัดแมว x2',
                        'product_set' => [
                            [
                                'product_title' => 'สัญลักษณ์โจรสลัดแมว x2',
                                'product_id' => 1874,
                                'product_quantity' => 1,
                                'amount' => 2,
                            ],
                        ],
                    ];
                    break;

                case ($step == 40):
                    return [
                        'product_title' => 'สัญลักษณ์โจรสลัดแมว x25',
                        'product_set' => [
                            [
                                'product_title' => 'สัญลักษณ์โจรสลัดแมว x25',
                                'product_id' => 2130,
                                'product_quantity' => 1,
                                'amount' => 25,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 41 && $step <= 49):
                    return [
                        'product_title' => 'ประกายฉลามดำ x2',
                        'product_set' => [
                            [
                                'product_title' => 'ประกายฉลามดำ x2',
                                'product_id' => 1852,
                                'product_quantity' => 1,
                                'amount' => 2,
                            ],
                        ],
                    ];
                    break;

                case ($step == 50):
                    return [
                        'product_title' => 'อัญมณีกรุฮงมุน 1',
                        'product_set' => [
                            [
                                'product_title' => 'อัญมณีกรุฮงมุน 1',
                                'product_id' => 1671,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 51 && $step <= 59):
                    return [
                        'product_title' => 'กล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส) x1',
                        'product_set' => [
                            [
                                'product_title' => 'กล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส) x1',
                                'product_id' => 1935,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step == 60):
                    return [
                        'product_title' => 'อัญมณีหินพิทักษณ์ทะเลสาป x1',
                        'product_set' => [
                            [
                                'product_title' => 'อัญมณีหินพิทักษณ์ทะเลสาป x1',
                                'product_id' => 1672,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 61 && $step <= 69):
                    return [
                        'product_title' => 'กล่องลูกแก้ว ขั้น 1 (ศุกร์ เสาร์) x1',
                        'product_set' => [
                            [
                                'product_title' => 'กล่องลูกแก้ว ขั้น 1 (ศุกร์ เสาร์) x1',
                                'product_id' => 2131,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step == 70):
                    return [
                        'product_title' => 'กล่องอัญมณีเจ็ดเหลี่ยมของฮงมุน x1',
                        'product_set' => [
                            [
                                'product_title' => 'กล่องอัญมณีเจ็ดเหลี่ยมของฮงมุน x1',
                                'product_id' => 2132,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 71 && $step <= 79):
                    return [
                        'product_title' => 'ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง x1',
                        'product_set' => [
                            [
                                'product_title' => 'ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด ที่ส่องสว่าง x1',
                                'product_id' => 2133,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step == 80):
                    return [
                        'product_title' => 'กรุพิศวง x1',
                        'product_set' => [
                            [
                                'product_title' => 'กรุพิศวง x1',
                                'product_id' => 1986,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 81 && $step <= 89):
                    return [
                        'product_title' => 'กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส) x1',
                        'product_set' => [
                            [
                                'product_title' => 'กล่องลูกแก้ว ขั้น 2 (อังคาร พุธ พฤหัส) x1',
                                'product_id' => 2134,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step == 90):
                    return [
                        'product_title' => 'คริสตัลวิญญาณฮงมุน x1',
                        'product_set' => [
                            [
                                'product_title' => 'คริสตัลวิญญาณฮงมุน x1',
                                'product_id' => 1896,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step >= 91 && $step <= 99):
                    return [
                        'product_title' => 'กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์) x1',
                        'product_set' => [
                            [
                                'product_title' => 'กล่องลูกแก้ว ขั้น 2 (ศุกร์ เสาร์) x1',
                                'product_id' => 2135,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                case ($step == 100):
                    return [
                        'product_title' => 'ชุดอักขระทองคำ x1',
                        'product_set' => [
                            [
                                'product_title' => 'ชุดอักขระทองคำ x1',
                                'product_id' => 2136,
                                'product_quantity' => 1,
                                'amount' => 1,
                            ],
                        ],
                    ];
                    break;

                default:
                    return false;
                    break;
            }

            return false;
        }

    }
