<?php

    namespace App\Models\snakeladder;

    class Quest {

        // Dungeon Quests
        public function getQuestList(){
            return [
                [
                    'id' => 1,
                    'quest_code' => 1478,
                    'quest_title' => 'เสียงหอนของเตาหลอม',
                    'quest_dungeon' => 'โรงหลอมอัคคี',
                ],
                [
                    'id' => 2,
                    'quest_code' => 1365,
                    'quest_title' => 'พันธะที่ผูกเราไว้',
                    'quest_dungeon' => 'สุสานที่ถูกลืม',
                ],
                [
                    'id' => 3,
                    'quest_code' => 1614,
                    'quest_title' => 'เสียงเพรียกของสายลม',
                    'quest_dungeon' => 'โรงหลอมเครื่องจักร',
                ],
                [
                    'id' => 4,
                    'quest_code' => 1528,
                    'quest_title' => 'ผู้บุกรุกที่ศาลนักพรตนาริว',
                    'quest_dungeon' => 'สถานศักดิ์สิทธิ์นาริว',
                ],
                [
                    'id' => 5,
                    'quest_code' => 1623,
                    'quest_title' => 'รุกฆาต',
                    'quest_dungeon' => 'วิหารลับเงามังกรดำ',
                ],
            ];
        }

    }