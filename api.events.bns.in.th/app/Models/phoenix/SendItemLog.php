<?php

namespace App\Models\phoenix;

use Illuminate\Database\Eloquent\Model;

class SendItemLog extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'phoenix_send_item_logs';
    protected $fillable = [
        'uid',
        'ncid',
        'status',
        'send_item_status',
        'send_item_purchase_id',
        'send_item_purchase_status',
        'goods_data',
        'last_ip',
        'type',
        'log_date',
        'log_date_timestamp',
        'send_gift_from'
    ];
}
