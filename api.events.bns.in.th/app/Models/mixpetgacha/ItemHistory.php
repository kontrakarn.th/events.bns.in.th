<?php

    namespace App\Models\mixpetgacha;

    use Moloquent;

    /**
     * Description of ItemHistory
     *
     * @author naruebaetbouhom
     */
    class ItemHistory extends Moloquent {

        protected $connection = 'mongodb_bns';
        protected $table = 'mixpetgacha_item_history';
        protected $fillable = [
            'item_id',
            'product_id',
            'product_title',
            'product_quantity',
            'uid',
            'ncid',
            'item_type',
            'status',
            'last_ip',
            'icon',
            'gachapon_type',
            'product_set',
            'product_pack_data',
            'send_type',
            'send_to_uid',
            'send_to_name',
            'log_timestamp',
            'log_time_string'
        ];

    }
