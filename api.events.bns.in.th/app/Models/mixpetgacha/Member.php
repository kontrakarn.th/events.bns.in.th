<?php

    namespace App\Models\mixpetgacha;

    use Moloquent;

    /**
     * Description of Member
     *
     * @author naruebaetbouhom
     */
    class Member extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'mixpetgacha_member';
        protected $fillable = ['uid', 'username', 'ncid', 'last_ip', 'log_timestamp'];

    }
