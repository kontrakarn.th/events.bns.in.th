<?php

    namespace App\Models\topup_july2019;

    use Moloquent;

    class Member extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'topup_july2019_member';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'total_diamonds',
            'last_ip'
        ];
    }
