<?php

namespace App\Models\bstc2019;

use Moloquent;

/**
 * Description of DeductLog
 *
 * @author naruebaetbouhom
 */
class User extends Moloquent
{

    protected $connection = 'mongodb_bns';
    protected $table = 'bstc_2019_users';
}
