<?php

    namespace App\Models\daily_stamp;

    use App\Models\BaseModel;

    class Events extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'daily_stamp_events';
        protected $fillable = [
            'eventname',
            'start_datetime',
            'end_datetime',
            'topup_start_datetime',
            'topup_end_datetime',
            'status',
            'test',
            'current_date',
            'claim_diamond',
            'buy_quest'
        ];

    }
