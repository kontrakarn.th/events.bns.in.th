<?php

    namespace App\Models\daily_stamp;

    use App\Models\BaseModel;

    class ItemHistory extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'daily_stamp_item_histories';
        protected $fillable = [
            'uid',
            'ncid',
            'product_id',
            'product_title',
            'product_quantity',
            'gachapon_type',
            'log_date',
            'log_date_timestamp',
            'item_type',
            'member_quest_id',
            'last_ip'
        ];

    }
