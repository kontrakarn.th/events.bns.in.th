<?php

    namespace App\Models\daily_stamp;

    use App\Models\BaseModel;

    class Quest extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'daily_stamp_quests';
        protected $fillable = [
            'quest_dungeon',
            'quest_title',
            'quest_code',
            'quest_id',
            'quest_amount',
            'image',
        ];
    }
