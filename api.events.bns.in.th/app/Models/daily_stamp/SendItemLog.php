<?php

    namespace App\Models\daily_stamp;

    use App\Models\BaseModel;

    class SendItemLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'daily_stamp_send_item_logs';
        protected $fillable = [
            'uid',
            'ncid',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'last_ip',
            'type',
            'log_date',
            'log_date_timestamp',
            'member_quest_id',
            'deduct_id',
            'name'
        ];

    }
