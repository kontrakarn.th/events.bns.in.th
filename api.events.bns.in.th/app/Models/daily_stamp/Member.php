<?php

    namespace App\Models\daily_stamp;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'daily_stamp_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'world_id',
            'last_ip',
            'total_stamp'
        ];

    }
