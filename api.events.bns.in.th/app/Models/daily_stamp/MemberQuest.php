<?php

    namespace App\Models\daily_stamp;

    use App\Models\BaseModel;

    class MemberQuest extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'daily_stamp_member_quest';
        protected $fillable = [
            'uid',
            'quest_index',
            'date',
            'status',
            'claim_free_status',
            'claim_diamond_status',
            'deduct_id',
            'last_ip',
            'start_quest',
            'end_quest'
        ];

    }
