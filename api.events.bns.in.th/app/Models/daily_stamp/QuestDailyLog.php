<?php

    namespace App\Models\daily_stamp;

    use App\Models\BaseModel;

    class QuestDailyLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'daily_stamp_quest_daily_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'quest_id',
            'quest_dungeon',
            'quest_title',
            'quest_code',
            'quest_amount',
            'quest_type',
            'image',
            'total_quest',
            'status', // pending, success
            'claim_status', // pending, claimed
            'log_date',
            'log_date_timestamp',
            'start_quest',
            'end_quest',
            'member_quest_id',
            'quest_complete_datetime',
            'last_ip',
        ];

    }
