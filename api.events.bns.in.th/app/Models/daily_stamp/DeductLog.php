<?php

    namespace App\Models\daily_stamp;

    use App\Models\BaseModel;

    class DeductLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'daily_stamp_deduct_logs';
        protected $fillable = [
            'uid',
            'ncid',
            'member_quest_id',
            'diamond',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'status',
            'deduct_type',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
