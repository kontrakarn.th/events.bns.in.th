<?php

namespace App\Models\daily_stamp;

use Cache;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Storage;

class Utils
{
    public const QUEST_DETAIL = [
        '2020-03-01' => [
            'index' => 1,
            'quest' => [0],
            'type' => 'normal',
            'rewards' => [5, 6, 7],
            'claim_free' => 'ไอเทมของวันที่ 1',
            'claim_diamond' => 'ไอเทมของวันที่ 1 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-02' => [
            'index' => 2,
            'quest' => [1],
            'type' => 'normal',
            'rewards' => [9, 11, 10],
            'claim_free' => 'ไอเทมของวันที่ 2',
            'claim_diamond' => 'ไอเทมของวันที่ 2 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-03' => [
            'index' => 3,
            'quest' => [2],
            'type' => 'normal',
            'rewards' => [5, 6, 17],
            'claim_free' => 'ไอเทมของวันที่ 3',
            'claim_diamond' => 'ไอเทมของวันที่ 3 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-04' => [
            'index' => 4,
            'quest' => [3],
            'type' => 'normal',
            'rewards' => [5, 6, 19],
            'claim_free' => 'ไอเทมของวันที่ 4',
            'claim_diamond' => 'ไอเทมของวันที่ 4 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-05' => [
            'index' => 5,
            'quest' => [4],
            'type' => 'normal',
            'rewards' => [18, 13, 14],
            'claim_free' => 'ไอเทมของวันที่ 5',
            'claim_diamond' => 'ไอเทมของวันที่ 5 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-06' => [
            'index' => 6,
            'quest' => [5],
            'type' => 'normal',
            'rewards' => [5, 6, 12],
            'claim_free' => 'ไอเทมของวันที่ 6',
            'claim_diamond' => 'ไอเทมของวันที่ 6 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-07' => [
            'index' => 7,
            'quest' => [6],
            'type' => 'normal',
            'rewards' => [9, 11, 10],
            'claim_free' => 'ไอเทมของวันที่ 7',
            'claim_diamond' => 'ไอเทมของวันที่ 7 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-08' => [
            'index' => 8,
            'quest' => [7],
            'type' => 'normal',
            'rewards' => [9, 11, 4],
            'claim_free' => 'ไอเทมของวันที่ 8',
            'claim_diamond' => 'ไอเทมของวันที่ 8 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-09' => [
            'index' => 9,
            'quest' => [8],
            'type' => 'normal',
            'rewards' => [9, 11, 10],
            'claim_free' => 'ไอเทมของวันที่ 9',
            'claim_diamond' => 'ไอเทมของวันที่ 9 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-10' => [
            'index' => 10,
            'quest' => [9],
            'type' => 'normal',
            'rewards' => [13, 15, 8],
            'claim_free' => 'ไอเทมของวันที่ 10',
            'claim_diamond' => 'ไอเทมของวันที่ 10 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-11' => [
            'index' => 11,
            'quest' => [10],
            'type' => 'normal',
            'rewards' => [5, 6, 19],
            'claim_free' => 'ไอเทมของวันที่ 11',
            'claim_diamond' => 'ไอเทมของวันที่ 11 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-12' => [
            'index' => 12,
            'quest' => [11],
            'type' => 'normal',
            'rewards' => [13, 15, 2],
            'claim_free' => 'ไอเทมของวันที่ 12',
            'claim_diamond' => 'ไอเทมของวันที่ 12 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-13' => [
            'index' => 13,
            'quest' => [12],
            'type' => 'normal',
            'rewards' => [13, 15, 11],
            'claim_free' => 'ไอเทมของวันที่ 13',
            'claim_diamond' => 'ไอเทมของวันที่ 13 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-14' => [
            'index' => 14,
            'quest' => [13],
            'type' => 'normal',
            'rewards' => [5, 6, 7],
            'claim_free' => 'ไอเทมของวันที่ 14',
            'claim_diamond' => 'ไอเทมของวันที่ 14 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-15' => [
            'index' => 15,
            'quest' => [14],
            'type' => 'normal',
            'rewards' => [13, 15, 0],
            'claim_free' => 'ไอเทมของวันที่ 15',
            'claim_diamond' => 'ไอเทมของวันที่ 15 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-16' => [
            'index' => 16,
            'quest' => [15],
            'type' => 'normal',
            'rewards' => [13, 15, 19],
            'claim_free' => 'ไอเทมของวันที่ 16',
            'claim_diamond' => 'ไอเทมของวันที่ 16 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-17' => [
            'index' => 17,
            'quest' => [16],
            'type' => 'normal',
            'rewards' => [18, 13, 14],
            'claim_free' => 'ไอเทมของวันที่ 17',
            'claim_diamond' => 'ไอเทมของวันที่ 17 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-18' => [
            'index' => 18,
            'quest' => [17],
            'type' => 'normal',
            'rewards' => [5, 6, 19],
            'claim_free' => 'ไอเทมของวันที่ 18',
            'claim_diamond' => 'ไอเทมของวันที่ 18 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-19' => [
            'index' => 19,
            'quest' => [18],
            'type' => 'normal',
            'rewards' => [5, 6, 8],
            'claim_free' => 'ไอเทมของวันที่ 19',
            'claim_diamond' => 'ไอเทมของวันที่ 19 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-20' => [
            'index' => 20,
            'quest' => [19],
            'type' => 'normal',
            'rewards' => [9, 11, 19],
            'claim_free' => 'ไอเทมของวันที่ 20',
            'claim_diamond' => 'ไอเทมของวันที่ 20 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-21' => [
            'index' => 21,
            'quest' => [20],
            'type' => 'normal',
            'rewards' => [3, 1, 16],
            'claim_free' => 'ไอเทมของวันที่ 21',
            'claim_diamond' => 'ไอเทมของวันที่ 21 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-22' => [
            'index' => 22,
            'quest' => [21],
            'type' => 'normal',
            'rewards' => [3, 1, 16],
            'claim_free' => 'ไอเทมของวันที่ 22',
            'claim_diamond' => 'ไอเทมของวันที่ 22 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-23' => [
            'index' => 23,
            'quest' => [22],
            'type' => 'normal',
            'rewards' => [3, 1, 16],
            'claim_free' => 'ไอเทมของวันที่ 23',
            'claim_diamond' => 'ไอเทมของวันที่ 23 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-24' => [
            'index' => 24,
            'quest' => [23, 24],
            'type' => 'special',
            'rewards' => [3, 1, 16],
            'claim_free' => 'ไอเทมของวันที่ 24',
            'claim_diamond' => 'ไอเทมของวันที่ 24 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-25' => [
            'index' => 25,
            'quest' => [25],
            'type' => 'normal',
            'rewards' => [5, 6, 19],
            'claim_free' => 'ไอเทมของวันที่ 25',
            'claim_diamond' => 'ไอเทมของวันที่ 25 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-26' => [
            'index' => 26,
            'quest' => [26, 27, 28, 29, 30, 31, 32, 33, 34, 35],
            'type' => 'normal',
            'rewards' => [20],
            'claim_free' => 'ไอเทมของวันที่ 26',
            'claim_diamond' => 'ไอเทมของวันที่ 26 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-27' => [
            'index' => 27,
            'quest' => [26, 27, 28, 29, 30, 31, 32, 33, 34, 35],
            'type' => 'normal',
            'rewards' => [20],
            'claim_free' => 'ไอเทมของวันที่ 27',
            'claim_diamond' => 'ไอเทมของวันที่ 27 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-28' => [
            'index' => 28,
            'quest' => [26, 27, 28, 29, 30, 31, 32, 33, 34, 35],
            'type' => 'normal',
            'rewards' => [20],
            'claim_free' => 'ไอเทมของวันที่ 28',
            'claim_diamond' => 'ไอเทมของวันที่ 28 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-29' => [
            'index' => 29,
            'quest' => [26, 27, 28, 29, 30, 31, 32, 33, 34, 35],
            'type' => 'normal',
            'rewards' => [20],
            'claim_free' => 'ไอเทมของวันที่ 29',
            'claim_diamond' => 'ไอเทมของวันที่ 29 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ],
        '2020-03-30' => [
            'index' => 30,
            'quest' => [26, 27, 28, 29, 30, 31, 32, 33, 34, 35],
            'type' => 'normal',
            'rewards' => [20],
            'claim_free' => 'ไอเทมของวันที่ 30',
            'claim_diamond' => 'ไอเทมของวันที่ 30 รับเพิ่ม (ค่าธรรมเนียม 1,000 ไดมอนด์)'
        ]
    ];

    public const ARRAY_DATE = [
        '2020-03-01', '2020-03-02', '2020-03-03', '2020-03-04', '2020-03-05',
        '2020-03-06', '2020-03-07', '2020-03-08', '2020-03-09', '2020-03-10',
        '2020-03-11', '2020-03-12', '2020-03-13', '2020-03-14', '2020-03-15',
        '2020-03-16', '2020-03-17', '2020-03-18', '2020-03-19', '2020-03-20',
        '2020-03-21', '2020-03-22', '2020-03-23', '2020-03-24', '2020-03-25',
        '2020-03-26', '2020-03-27', '2020-03-28', '2020-03-29', '2020-03-30'
    ];

    public const ARRAY_UNLOCK = [
        '2020-03-01', '2020-03-02', '2020-03-03', '2020-03-04', '2020-03-05',
        '2020-03-06', '2020-03-07', '2020-03-08', '2020-03-09', '2020-03-10',
        '2020-03-11', '2020-03-12', '2020-03-13', '2020-03-14', '2020-03-15',
        '2020-03-16', '2020-03-17', '2020-03-18', '2020-03-19', '2020-03-20',
        '2020-03-21', '2020-03-22', '2020-03-23', '2020-03-24', '2020-03-25'
    ];

    public const ARRAY_STAMP = [
        [
            'stamp' => 3,
            'rewards' => [13, 15],
            'message' => 'ไอเทมพิเศษ สะสมแสตมป์ครบ 3 ดวง',
        ],
        [
            'stamp' => 7,
            'rewards' => [23, 24],
            'message' => 'ไอเทมพิเศษ สะสมแสตมป์ครบ 7 ดวง',
        ],
        [
            'stamp' => 11,
            'rewards' => [25, 26],
            'message' => 'ไอเทมพิเศษ สะสมแสตมป์ครบ 11 ดวง',
        ],
        [
            'stamp' => 14,
            'rewards' => [27, 28],
            'message' => 'ไอเทมพิเศษ สะสมแสตมป์ครบ 14 ดวง',
        ],
        [
            'stamp' => 18,
            'rewards' => [29, 30, 31],
            'message' => 'ไอเทมพิเศษ สะสมแสตมป์ครบ 18 ดวง',
        ],
        [
            'stamp' => 25,
            'rewards' => [32],
            'message' => 'ไอเทมพิเศษ สะสมแสตมป์ครบ 25 ดวง',
        ],
    ];

    public const KEY_EVENT = 'daily_stamp';

}
