<?php

    namespace App\Models\vip_benefit;

    use Moloquent;

    /**
     * Description of SendItemLog
     *
     * @author naruebaetbouhom
     */
    class SendItemLog extends Moloquent {

        protected $connection = 'mongodb_bns';
        protected $table = 'vip_shop_send_item_log';
        protected $fillable = [
            'uid',
            'ncid',
            'package_id',
            'package_key', 
            'package_title',
            'product_set',
            'deduct_id', // from deduct diamonds collection
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'last_ip',
            'log_date',
            'log_date_timestamp'
        ];

    }
    