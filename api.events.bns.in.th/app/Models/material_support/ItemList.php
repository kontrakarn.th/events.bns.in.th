<?php

    namespace App\Models\material_support;

    use Moloquent;

    /**
     * Description of ItemList
     *
     * @author naruebaetbouhom
     */
    class ItemList extends Moloquent {

        protected $connection = 'mongodb_bns';
        protected $table = 'material_support_item_list';
        protected $fillable = [
            'poolname',
            'eventname',
            'value'
        ];

    }
