<?php

    namespace App\Models\material_support;

    use Moloquent;

    /**
     * Description of Member
     *
     * @author naruebaetbouhom
     */
    class Member extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'material_support_member';
        protected $fillable = ['uid', 'username', 'ncid', 'last_ip', 'log_timestamp', 'log_time_string'];

    }
