<?php

    namespace App\Models\material_support;

    use Moloquent;

    /**
     * Description of Event
     *
     * @author naruebaetbouhom
     */
    class Event extends Moloquent {

        protected $connection = 'mongodb_bns';
        protected $table = 'material_support_event';
        protected $fillable = [
            'eventname',
            'start_time',
            'end_time',
            'status'
        ];

    }
