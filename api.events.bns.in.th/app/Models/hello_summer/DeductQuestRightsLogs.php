<?php

    namespace App\Models\hello_summer;

    use App\Models\BaseModel;

    class DeductQuestRightsLogs extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'hello_summer_deduct_quest_rights_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'quest_id',
            'quest_code',
            'quest_title',
            'before_deduct',
            'after_deduct',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }