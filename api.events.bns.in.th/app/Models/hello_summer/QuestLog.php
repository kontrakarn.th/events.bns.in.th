<?php

    namespace App\Models\hello_summer;

    use App\Models\BaseModel;

    class QuestLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'hello_summer_quest_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'world_id',
            'job',
            'quest_id',
            'quest_code',
            'quest_title',
            'total_completed',
            'total_used',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }