<?php

    namespace App\Models\hello_summer;

    class Quest {

        // get quest info buy id
        public function setQuestInfoByQuestId($questId=null){
            if(empty($questId)){
                return false;
            }

            $quest = $this->getQuestList();

            return (isset($quest[$questId-1]) && !empty($quest[$questId-1])) ? $quest[$questId-1] : false;
        }

        public function getRandomQuestRewards($questId=null){
            if(empty($questId)){
                return false;
            }

            $questInfo = $this->setQuestInfoByQuestId($questId);
            $questRewards = $questInfo['rewards'];

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($questRewards as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            $reward['timestamp'] = time();

            // now we have the reward
            return $reward;
        }

        // Dungeon Quests
        public function getQuestList(){
            return [
                [
                    'quest_id' => 1,
                    'quest_code' => 25197,
                    'quest_title' => 'เรือโจรสลัดต้องสาป',
                    'quest_dungeon' => 'เรือโจรสลัดต้องสาป',
                    'rewards' => [
                        [
                            'item_type' => 'water_drop',
                            'product_title' => 'หยดน้ำ',
                            'amount' => 1,
                            'chance' => 20.000,
                        ],
                        [
                            'item_type' => 'water_bowl',
                            'product_title' => 'ขันน้ำ',
                            'amount' => 1,
                            'chance' => 50.000,
                        ],
                        [
                            'item_type' => 'quest',
                            'product_title' => 'สัญลักษณ์ฮงมุน',
                            'product_id' => 2002,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'chance' => 30.000,
                        ],
                    ],
                ],
                [
                    'quest_id' => 2,
                    'quest_code' => 25188,
                    'quest_title' => 'คำสาปแห่งความโลภ',
                    'quest_dungeon' => 'วิหารทรายวายุ',
                    'rewards' => [
                        [
                            'item_type' => 'water_drop',
                            'product_title' => 'หยดน้ำ',
                            'amount' => 1,
                            'chance' => 20.000,
                        ],
                        [
                            'item_type' => 'water_bowl',
                            'product_title' => 'ขันน้ำ',
                            'amount' => 1,
                            'chance' => 50.000,
                        ],
                        [
                            'item_type' => 'quest',
                            'product_title' => 'สัญลักษณ์ฮงมุน',
                            'product_id' => 2002,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'chance' => 30.000,
                        ],
                    ],
                ],
                [
                    'quest_id' => 3,
                    'quest_code' => 25185,
                    'quest_title' => 'ลึกลงไปในกรุสมบัติ',
                    'quest_dungeon' => 'กรุสมบัตินาริว',
                    'rewards' => [
                        [
                            'item_type' => 'water_drop',
                            'product_title' => 'หยดน้ำ',
                            'amount' => 1,
                            'chance' => 15.000,
                        ],
                        [
                            'item_type' => 'water_bowl',
                            'product_title' => 'ขันน้ำ',
                            'amount' => 1,
                            'chance' => 50.000,
                        ],
                        [
                            'item_type' => 'quest',
                            'product_title' => 'สัญลักษณ์ฮงมุน',
                            'product_id' => 2002,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'chance' => 35.000,
                        ],
                    ],
                ],
                [
                    'quest_id' => 4,
                    'quest_code' => 25178,
                    'quest_title' => 'ลำนำแห่งชาวเรือ',
                    'quest_dungeon' => 'ถ้ำใต้น้ำเสียงสะท้อน',
                    'rewards' => [
                        [
                            'item_type' => 'water_drop',
                            'product_title' => 'หยดน้ำ',
                            'amount' => 1,
                            'chance' => 15.000,
                        ],
                        [
                            'item_type' => 'water_bowl',
                            'product_title' => 'ขันน้ำ',
                            'amount' => 1,
                            'chance' => 50.000,
                        ],
                        [
                            'item_type' => 'quest',
                            'product_title' => 'สัญลักษณ์ฮงมุน',
                            'product_id' => 2002,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'chance' => 35.000,
                        ],
                    ],
                ],
                [
                    'quest_id' => 5,
                    'quest_code' => 25172,
                    'quest_title' => 'บั้นปลายของผู้พิทักษ์',
                    'quest_dungeon' => 'เมืองโบราณพรรณไม้',
                    'rewards' => [
                        [
                            'item_type' => 'water_drop',
                            'product_title' => 'หยดน้ำ',
                            'amount' => 1,
                            'chance' => 15.000,
                        ],
                        [
                            'item_type' => 'water_bowl',
                            'product_title' => 'ขันน้ำ',
                            'amount' => 1,
                            'chance' => 50.000,
                        ],
                        [
                            'item_type' => 'quest',
                            'product_title' => 'สัญลักษณ์ฮงมุน',
                            'product_id' => 2002,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'chance' => 35.000,
                        ],
                    ],
                ],
                [
                    'quest_id' => 6,
                    'quest_code' => 25152,
                    'quest_title' => 'ตัวตนของหัวหน้าช่างแห่งโรงหลอมเครื่องจักร',
                    'quest_dungeon' => 'โรงหลอมเครื่องจักร',
                    'rewards' => [
                        [
                            'item_type' => 'water_drop',
                            'product_title' => 'หยดน้ำ',
                            'amount' => 1,
                            'chance' => 15.000,
                        ],
                        [
                            'item_type' => 'water_bowl',
                            'product_title' => 'ขันน้ำ',
                            'amount' => 1,
                            'chance' => 50.000,
                        ],
                        [
                            'item_type' => 'quest',
                            'product_title' => 'สัญลักษณ์ฮงมุน',
                            'product_id' => 2002,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'chance' => 35.000,
                        ],
                    ],
                ],
                [
                    'quest_id' => 7,
                    'quest_code' => 25158,
                    'quest_title' => 'การพักผ่อนของผู้ตาย',
                    'quest_dungeon' => 'วิหารลับเงามังกรดำ',
                    'rewards' => [
                        [
                            'item_type' => 'water_drop',
                            'product_title' => 'หยดน้ำ',
                            'amount' => 1,
                            'chance' => 15.000,
                        ],
                        [
                            'item_type' => 'water_bowl',
                            'product_title' => 'ขันน้ำ',
                            'amount' => 1,
                            'chance' => 50.000,
                        ],
                        [
                            'item_type' => 'quest',
                            'product_title' => 'สัญลักษณ์ฮงมุน',
                            'product_id' => 2002,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'chance' => 35.000,
                        ],
                    ],
                ],
                [
                    'quest_id' => 8,
                    'quest_code' => 25176,
                    'quest_title' => 'ผู้ปกครองแห่งหุบเขาหิวกระหาย',
                    'quest_dungeon' => 'หุบเขาอาถรรพ์',
                    'rewards' => [
                        [
                            'item_type' => 'water_drop',
                            'product_title' => 'หยดน้ำ',
                            'amount' => 1,
                            'chance' => 5.000,
                        ],
                        [
                            'item_type' => 'water_bowl',
                            'product_title' => 'ขันน้ำ',
                            'amount' => 1,
                            'chance' => 50.000,
                        ],
                        [
                            'item_type' => 'quest',
                            'product_title' => 'สัญลักษณ์ฮงมุน',
                            'product_id' => 2002,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'chance' => 45.000,
                        ],
                    ],
                ],
                [
                    'quest_id' => 9,
                    'quest_code' => 25147,
                    'quest_title' => 'ผู้เฝ้ามองอมตะ',
                    'quest_dungeon' => 'สถานศักดิ์สิทธิ์นาริว',
                    'rewards' => [
                        [
                            'item_type' => 'water_drop',
                            'product_title' => 'หยดน้ำ',
                            'amount' => 1,
                            'chance' => 2.000,
                        ],
                        [
                            'item_type' => 'water_bowl',
                            'product_title' => 'ขันน้ำ',
                            'amount' => 1,
                            'chance' => 50.000,
                        ],
                        [
                            'item_type' => 'quest',
                            'product_title' => 'สัญลักษณ์ฮงมุน',
                            'product_id' => 2002,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'chance' => 48.000,
                        ],
                    ],
                ],
                [
                    'quest_id' => 10,
                    'quest_code' => 25144,
                    'quest_title' => 'เปลวเพลิงที่ไม่มีวันดับ',
                    'quest_dungeon' => 'โรงหลอมอัคคี',
                    'rewards' => [
                        [
                            'item_type' => 'water_drop',
                            'product_title' => 'หยดน้ำ',
                            'amount' => 1,
                            'chance' => 2.000,
                        ],
                        [
                            'item_type' => 'water_bowl',
                            'product_title' => 'ขันน้ำ',
                            'amount' => 1,
                            'chance' => 50.000,
                        ],
                        [
                            'item_type' => 'quest',
                            'product_title' => 'สัญลักษณ์ฮงมุน',
                            'product_id' => 2002,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'chance' => 48.000,
                        ],
                    ],
                ],
                [
                    'quest_id' => 11,
                    'quest_code' => 25014,
                    'quest_title' => 'ตกสู่เปลวเพลิง',
                    'quest_dungeon' => 'สุสานที่ถูกลืม',
                    'rewards' => [
                        [
                            'item_type' => 'water_drop',
                            'product_title' => 'หยดน้ำ',
                            'amount' => 1,
                            'chance' => 2.000,
                        ],
                        [
                            'item_type' => 'water_bowl',
                            'product_title' => 'ขันน้ำ',
                            'amount' => 1,
                            'chance' => 50.000,
                        ],
                        [
                            'item_type' => 'quest',
                            'product_title' => 'สัญลักษณ์ฮงมุน',
                            'product_id' => 2002,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'chance' => 48.000,
                        ],
                    ],
                ],
                [
                    'quest_id' => 12,
                    'quest_code' => 25103,
                    'quest_title' => 'หัวหน้าอสูรแห่งลัทธิเงามังกรดำ',
                    'quest_dungeon' => 'คุกใต้ดินมังกรดำ',
                    'rewards' => [
                        [
                            'item_type' => 'water_drop',
                            'product_title' => 'หยดน้ำ',
                            'amount' => 1,
                            'chance' => 2.000,
                        ],
                        [
                            'item_type' => 'water_bowl',
                            'product_title' => 'ขันน้ำ',
                            'amount' => 1,
                            'chance' => 50.000,
                        ],
                        [
                            'item_type' => 'quest',
                            'product_title' => 'สัญลักษณ์ฮงมุน',
                            'product_id' => 2002,
                            'product_quantity' => 1,
                            'amount' => 1,
                            'chance' => 48.000,
                        ],
                    ],
                ],
            ];
        }

    }