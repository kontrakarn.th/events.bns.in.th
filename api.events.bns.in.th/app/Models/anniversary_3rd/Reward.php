<?php

    namespace App\Models\anniversary_3rd;

    use App\Models\BaseModel;

    class Reward extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'anniversary_3rd_rewards';
        protected $fillable = [
            'package_name',
            'package_id',
            'package_quantity',
            'package_amount',
            'package_key',
            'points',
            'image',
            'available_date',
        ];
    }