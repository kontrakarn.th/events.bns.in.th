<?php

    namespace App\Models\anniversary_3rd;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'anniversary_3rd_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'points',
            'package_key',
            'package_id',
            'package_name',
            'package_quantity',
            'package_amount',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }