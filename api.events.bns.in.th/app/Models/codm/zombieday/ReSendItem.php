<?php

    namespace App\Models\codm\zombieday;

    use App\Models\BaseModel;

    class ReSendItem extends BaseModel {
        protected $connection = 'events_codm';
        protected $table = 'codm_zombieday_resend_item';
        protected $fillable = [
            'open_id',
            'tencent_id',
            'item_name',
            'package_id',
            'status',
            'response',
            'tid',
            'iegams_key',
        ];
    }

