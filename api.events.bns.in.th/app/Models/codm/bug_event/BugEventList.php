<?php

    namespace App\Models\codm\bug_event;

    use App\Models\BaseModel;

    class BugEventList extends BaseModel {
        protected $connection = 'events_codm_test';
        protected $table = 'bug_event_id';
        protected $fillable = [
            'tencent_id',
            'item_id',
            'gid',
        ];
    }

