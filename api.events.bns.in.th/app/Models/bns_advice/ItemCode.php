<?php

    namespace App\Models\bns_advice;

    use App\Models\BaseModel;

    class ItemCode extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'bns_advice_item_code';
        protected $fillable = [
            'code',
            'status',
            'uid',
            'gahcapon_used',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }