<?php

    namespace App\Models\bns_advice;

    use App\Models\BaseModel;

    class DeductPointLog extends BaseModel {

        protected $connection = 'events_bns';
        protected $table = 'bns_advice_deduct_point_log';
        protected $fillable = [
            'uid',
        	'username',
        	'ncid',
        	'status',
            'points',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    