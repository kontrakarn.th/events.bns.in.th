<?php

    namespace App\Models\bns_advice;

    use App\Models\BaseModel;

    class ItemHistory extends BaseModel {

        protected $connection = 'events_bns';
        protected $table = 'bns_advice_item_history';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'reward_id',
            'product_id',
            'product_title',
            'product_quantity',
            'deduct_points_id', // deduct id from deduct points
            'item_type', // 'advice_gachapon','free_gachapon','exchange_points'
            'amount',
            'status', // pending, success, unsuccess
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    