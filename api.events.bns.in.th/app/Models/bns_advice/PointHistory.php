<?php

    namespace App\Models\bns_advice;

    use App\Models\BaseModel;

    class PointHistory extends BaseModel {

        protected $connection = 'events_bns';
        protected $table = 'bns_advice_point_history';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'title',
            'points',
            'status', // pending, success
            'type', // 'advice_gachapon','free_gachapon'
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    