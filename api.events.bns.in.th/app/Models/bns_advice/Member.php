<?php

    namespace App\Models\bns_advice;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'bns_advice_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'world_id',
            'points_used',
            'last_ip',
        ];

    }