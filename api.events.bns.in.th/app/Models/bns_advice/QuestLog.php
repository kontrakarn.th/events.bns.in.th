<?php

    namespace App\Models\bns_advice;

    use App\Models\BaseModel;

    class QuestLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'bns_advice_quest_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'char_id',
            'char_name',
            'gahcapon_used',
            'quest_id',
            'quest_code',
            'quest_title',
            'status', // pending, success, unsuccess
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }