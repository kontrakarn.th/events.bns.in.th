<?php

    namespace App\Models\anniversary_3rd_passport;

    use App\Models\BaseModel;

    class Reward extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'anniversary_3rd_passport_rewards';
        protected $fillable = [
            'package_name',
            'package_id',
            'package_quantity',
            'package_amount',
            'package_key',
            'package_type',
            'week',
            'image',
            'available_date',
        ];

    }
    