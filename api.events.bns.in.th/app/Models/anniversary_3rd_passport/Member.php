<?php

    namespace App\Models\anniversary_3rd_passport;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'anniversary_3rd_passport_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'is_unlocked',
            'last_ip',
        ];

    }