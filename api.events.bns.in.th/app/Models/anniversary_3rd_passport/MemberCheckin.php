<?php

    namespace App\Models\anniversary_3rd_passport;

    use App\Models\BaseModel;

    class MemberCheckin extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'anniversary_3rd_passport_member_checkin';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'checkin_date',
            'week',
            'day',
            'package_key',
            'is_checkin',
            'is_prev_checkin',
            'last_ip',
        ];

    }
    