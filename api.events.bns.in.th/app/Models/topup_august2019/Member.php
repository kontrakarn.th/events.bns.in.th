<?php

    namespace App\Models\topup_august2019;

    use Moloquent;

    class Member extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'topup_august2019_member';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'total_diamonds',
            'last_ip'
        ];
    }
