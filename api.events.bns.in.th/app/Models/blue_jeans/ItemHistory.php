<?php

namespace App\Models\blue_jeans;

use App\Models\BaseModel;

class ItemHistory extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'blue_jeans_item_history';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'deduct_id',
        'package_key',
        'package_id',
        'package_name',
        'package_quantity',
        'package_amount',
        'package_type',
        'image',
        'send_type', // owner, gift, none
        'send_to',
        'send_status',
        'log_date',
        'log_date_timestamp',
        'last_ip',
        'reward_type',
    ];
}
