<?php

namespace App\Models\blue_jeans;

use App\Models\BaseModel;

class MemberGroup extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'blue_jeans_members_group';
    protected $fillable = [
        'uid',
        'group',
    ];
}
