<?php

namespace App\Models\blue_jeans;

use App\Models\BaseModel;

class Setting extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'blue_jeans_setting';
    protected $fillable = [
        'event_start',
        'event_end',
        'gachapon_start',
        'gachapon_end',
        'exchange_start',
        'exchange_end',
        'diamonds_require',
        'service_charge',
        'active',
    ];
}
