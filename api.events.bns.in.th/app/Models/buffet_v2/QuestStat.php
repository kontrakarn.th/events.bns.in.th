<?php

    namespace App\Models\buffet_v2;

    use Moloquent;

    class QuestStat extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'buffet_v2_quest_stat';
        protected $fillable = [
            'quest_id',
            'type',
            'bns_quest_id',
            'quest_name',
            'group_no',
            'uid',
            'character_id',
            'character_name',
            'completed_count'
        ];
    }
