<?php

    namespace App\Models\buffet_v2;

    use Moloquent;

    class Quest extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'buffet_v2_quest';
        protected $fillable = [
            'quest_id',
            'type',
            'bns_quest_id',
            'quest_name',
            'group_no',
        ];
    }
