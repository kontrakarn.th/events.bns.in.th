<?php

    namespace App\Models\buffet_v2;

    use Moloquent;

    class PlayLogs extends Moloquent {
        protected $connection = 'mongodb_bns';
        protected $table = 'buffet_v2_play_logs';
        protected $fillable = [
            'uid',
            'character_id',
            'play_type',
            'created_at_string',
        ];
    }
