<?php
// Mission for disciple
namespace App\Models\buffet_v2;

use App\Models\buffet_v2\Gachapon;
use App\Models\buffet_v2\Package;

class Reward
{

    public function getRandomGachapon($group)
    {
        $rewards = $this->getRewardsGachapon($group);
        $random = rand(0, 100000);
        $accumulatedChance = 0;
        $reward = [];
        $reward['status']       = false;
        foreach ($rewards as $value) {
            $chance = $value['chance'] * 1000;
            $currentChance = $accumulatedChance + $chance;

            // acquired reward
            if ($random >= $accumulatedChance && $random <= $currentChance) {
                $reward             = $value;
                $reward['status']   = true;
                break;
            }
            $accumulatedChance = $currentChance;
        }
        $reward['random']       = $random;
        $reward['timestamp']    = time();

        // now we have the reward
        return $reward;
    }

    private function getRewardsGachapon($group)
    {
        $data = Gachapon::where('group',(int)$group)->get()->makeHidden('_id')->toArray();
        foreach($data as $k => $v){
            if($v['amount'] > 0){
                $data[$k]['product_title'] .= ' ('.$v['amount'].')';
            }
        }
        return $data;
        /* if ($group == 1) {
            return [
                [
                    'id' => 1,
                    'item_type' => 'events',
                    'group' => 1,
                    'icon' => 0,
                    'product_title' => 'คูปองกินบุฟเฟ่ต์ฟรี',
                    'product_id' => -1,
                    'product_quantity' => 3,
                    'amount' => 3,
                    'chance' => 25.000
                ],
                [
                    'id' => 2,
                    'item_type' => 'gachapon',
                    'group' => 1,
                    'icon' => 0,
                    'product_title' => 'เหยื่อตกปลา ชั้นต้น',
                    'product_id' => 3589,
                    'product_quantity' => 1,
                    'amount' => 5,
                    'chance' => 25.000
                ],
                [
                    'id' => 3,
                    'item_type' => 'gachapon',
                    'group' => 1,
                    'icon' => 0,
                    'product_title' => 'หินโซล',
                    'product_id' => 2468,
                    'product_quantity' => 1,
                    'amount' => 2,
                    'chance' => 15.000
                ],
                [
                    'id' => 4,
                    'item_type' => 'gachapon',
                    'group' => 1,
                    'icon' => 0,
                    'product_title' => 'หินจันทรา',
                    'product_id' => 1924,
                    'product_quantity' => 1,
                    'amount' => 1,
                    'chance' => 15.000
                ],
                [
                    'id' => 5,
                    'item_type' => 'gachapon',
                    'group' => 1,
                    'icon' => 0,
                    'product_title' => 'หินโซล',
                    'product_id' => 2469,
                    'product_quantity' => 1,
                    'amount' => 20,
                    'chance' => 13.250
                ],
                [
                    'id' => 6,
                    'item_type' => 'gachapon',
                    'group' => 1,
                    'icon' => 0,
                    'product_title' => 'หินจันทรา',
                    'product_id' => 571,
                    'product_quantity' => 1,
                    'amount' => 10,
                    'chance' => 2.500
                ],
                [
                    'id' => 7,
                    'item_type' => 'gachapon',
                    'group' => 1,
                    'icon' => 0,
                    'product_title' => 'ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด',
                    'product_id' => 2470,
                    'product_quantity' => 1,
                    'amount' => 1,
                    'chance' => 2.500
                ],
                [
                    'id' => 8,
                    'item_type' => 'gachapon',
                    'group' => 1,
                    'icon' => 0,
                    'product_title' => 'หินโซล',
                    'product_id' => 2471,
                    'product_quantity' => 1,
                    'amount' => 200,
                    'chance' => 0.500
                ],
                [
                    'id' => 9,
                    'item_type' => 'gachapon',
                    'group' => 1,
                    'icon' => 0,
                    'product_title' => 'หินโซล',
                    'product_id' => 2472,
                    'product_quantity' => 1,
                    'amount' => 2000,
                    'chance' => 0.500
                ],
                [
                    'id' => 10,
                    'item_type' => 'gachapon',
                    'group' => 1,
                    'icon' => 0,
                    'product_title' => 'หินจันทรา',
                    'product_id' => 2137,
                    'product_quantity' => 1,
                    'amount' => 100,
                    'chance' => 0.500
                ],
                [
                    'id' => 11,
                    'item_type' => 'gachapon',
                    'group' => 1,
                    'icon' => 0,
                    'product_title' => 'หินจันทรา',
                    'product_id' => 2473,
                    'product_quantity' => 1,
                    'amount' => 1000,
                    'chance' => 0.250
                ],
            ];
        } else if ($group == 2) {
            return [
                [
                    'id' => 1,
                    'item_type' => 'events',
                    'group' => 2,
                    'icon' => 0,
                    'product_title' => 'คูปองกินบุฟเฟ่ต์ฟรี',
                    'product_id' => -1,
                    'product_quantity' => 3,
                    'amount' => 3,
                    'chance' => 25.000
                ],
                [
                    'id' => 2,
                    'item_type' => 'gachapon',
                    'group' => 2,
                    'icon' => 0,
                    'product_title' => 'เหยื่อตกปลา ชั้นต้น',
                    'product_id' => 3589,
                    'product_quantity' => 1,
                    'amount' => 5,
                    'chance' => 25.000
                ],
                [
                    'id' => 3,
                    'item_type' => 'gachapon',
                    'group' => 2,
                    'icon' => 0,
                    'product_title' => 'หินโซล',
                    'product_id' => 2468,
                    'product_quantity' => 1,
                    'amount' => 2,
                    'chance' => 15.000
                ],
                [
                    'id' => 4,
                    'item_type' => 'gachapon',
                    'group' => 2,
                    'icon' => 0,
                    'product_title' => 'หินจันทรา',
                    'product_id' => 1924,
                    'product_quantity' => 1,
                    'amount' => 1,
                    'chance' => 15.000
                ],
                [
                    'id' => 5,
                    'item_type' => 'gachapon',
                    'group' => 2,
                    'icon' => 0,
                    'product_title' => 'หินโซล',
                    'product_id' => 2469,
                    'product_quantity' => 1,
                    'amount' => 20,
                    'chance' => 13.250
                ],
                [
                    'id' => 6,
                    'item_type' => 'gachapon',
                    'group' => 2,
                    'icon' => 0,
                    'product_title' => 'หินจันทรา',
                    'product_id' => 571,
                    'product_quantity' => 1,
                    'amount' => 10,
                    'chance' => 2.500
                ],
                [
                    'id' => 7,
                    'item_type' => 'gachapon',
                    'group' => 2,
                    'icon' => 0,
                    'product_title' => 'ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด',
                    'product_id' => 2470,
                    'product_quantity' => 1,
                    'amount' => 1,
                    'chance' => 2.500
                ],
                [
                    'id' => 8,
                    'item_type' => 'gachapon',
                    'group' => 2,
                    'icon' => 0,
                    'product_title' => 'หินโซล',
                    'product_id' => 2471,
                    'product_quantity' => 1,
                    'amount' => 200,
                    'chance' => 0.500
                ],
                [
                    'id' => 9,
                    'item_type' => 'gachapon',
                    'group' => 2,
                    'icon' => 0,
                    'product_title' => 'หินโซล',
                    'product_id' => 2472,
                    'product_quantity' => 1,
                    'amount' => 2000,
                    'chance' => 0.500
                ],
                [
                    'id' => 10,
                    'item_type' => 'gachapon',
                    'group' => 2,
                    'icon' => 0,
                    'product_title' => 'หินจันทรา',
                    'product_id' => 2137,
                    'product_quantity' => 1,
                    'amount' => 100,
                    'chance' => 0.500
                ],
                [
                    'id' => 11,
                    'item_type' => 'gachapon',
                    'group' => 2,
                    'icon' => 0,
                    'product_title' => 'หินจันทรา',
                    'product_id' => 2473,
                    'product_quantity' => 1,
                    'amount' => 1000,
                    'chance' => 0.250
                ],
            ];
        } else {
            return [
                [
                    'id' => 1,
                    'item_type' => 'events',
                    'group' => 3,
                    'icon' => 0,
                    'product_title' => 'คูปองกินบุฟเฟ่ต์ฟรี',
                    'product_id' => -1,
                    'product_quantity' => 1,
                    'amount' => 1,
                    'chance' => 24.070
                ],
                [
                    'id' => 2,
                    'item_type' => 'gachapon',
                    'group' => 3,
                    'icon' => 0,
                    'product_title' => 'เหยื่อตกปลา ชั้นต้น',
                    'product_id' => 3590,
                    'product_quantity' => 1,
                    'amount' => 2,
                    'chance' => 28.890
                ],
                [
                    'id' => 3,
                    'item_type' => 'gachapon',
                    'group' => 3,
                    'icon' => 0,
                    'product_title' => 'หินโซล',
                    'product_id' => 2468,
                    'product_quantity' => 1,
                    'amount' => 2,
                    'chance' => 14.440
                ],
                [
                    'id' => 4,
                    'item_type' => 'gachapon',
                    'group' => 3,
                    'icon' => 0,
                    'product_title' => 'หินจันทรา',
                    'product_id' => 1924,
                    'product_quantity' => 1,
                    'amount' => 1,
                    'chance' => 14.440
                ],
                [
                    'id' => 5,
                    'item_type' => 'gachapon',
                    'group' => 3,
                    'icon' => 0,
                    'product_title' => 'หินโซล',
                    'product_id' => 2469,
                    'product_quantity' => 1,
                    'amount' => 20,
                    'chance' => 12.760
                ],
                [
                    'id' => 6,
                    'item_type' => 'gachapon',
                    'group' => 3,
                    'icon' => 0,
                    'product_title' => 'หินจันทรา',
                    'product_id' => 571,
                    'product_quantity' => 1,
                    'amount' => 10,
                    'chance' => 2.410
                ],
                [
                    'id' => 7,
                    'item_type' => 'gachapon',
                    'group' => 3,
                    'icon' => 0,
                    'product_title' => 'ยันต์เพิ่มเลเวลฮงมุน แบบเร่งรัด',
                    'product_id' => 2470,
                    'product_quantity' => 1,
                    'amount' => 1,
                    'chance' => 2.410
                ],
                [
                    'id' => 8,
                    'item_type' => 'gachapon',
                    'group' => 3,
                    'icon' => 0,
                    'product_title' => 'หินโซล',
                    'product_id' => 2471,
                    'product_quantity' => 1,
                    'amount' => 200,
                    'chance' => 0.140
                ],
                [
                    'id' => 9,
                    'item_type' => 'gachapon',
                    'group' => 3,
                    'icon' => 0,
                    'product_title' => 'หินโซล',
                    'product_id' => 2472,
                    'product_quantity' => 1,
                    'amount' => 2000,
                    'chance' => 0.140
                ],
                [
                    'id' => 10,
                    'item_type' => 'gachapon',
                    'group' => 3,
                    'icon' => 0,
                    'product_title' => 'หินจันทรา',
                    'product_id' => 2137,
                    'product_quantity' => 1,
                    'amount' => 100,
                    'chance' => 0.140
                ],
                [
                    'id' => 11,
                    'item_type' => 'gachapon',
                    'group' => 3,
                    'icon' => 0,
                    'product_title' => 'หินจันทรา',
                    'product_id' => 2473,
                    'product_quantity' => 1,
                    'amount' => 1000,
                    'chance' => 0.140
                ],
            ];
        } */
    }

    public function setRewardByPackage($packageKey = '')
    {
        if (empty($packageKey)) {
            return false;
        }

        $rewards = $this->packageList();

        return (isset($rewards[$packageKey - 1]) && !empty($rewards[$packageKey - 1])) ? $rewards[$packageKey - 1] : false;
    }

    private function packageList()
    {
        $data = Package::get()->makeHidden('_id')->toArray();
        return $data;
        /* return [
            [
                'package_id' => 1,
                'package_title' => 'ชุดอสูรครามทอง',
                'package_desc' => 'ชุดอสูรครามทอง',
                'icon' => 0,
                'require_token' => 75,
                'product_set' => [
                    'id' => 1,
                    'product_title' => 'ชุดอสูรครามทอง',
                    'product_id' => 2474,
                    'product_quantity' => 1
                ]
            ],
            [
                'package_id' => 2,
                'package_title' => 'เครื่องประดับอสูรครามทอง',
                'package_desc' => 'เครื่องประดับอสูรครามทอง',
                'icon' => 0,
                'require_token' => 35,
                'product_set' => [
                    'id' => 2,
                    'product_title' => 'เครื่องประดับอสูรครามทอง',
                    'product_id' => 2475,
                    'product_quantity' => 1
                ]
            ],
            [
                'package_id' => 3,
                'package_title' => 'กล่องอาวุธลวงตากุหลาบซันตามาเรีย',
                'package_desc' => 'กล่องอาวุธลวงตากุหลาบซันตามาเรีย',
                'icon' => 0,
                'require_token' => 20,
                'product_set' => [
                    'id' => 3,
                    'product_title' => 'กล่องอาวุธลวงตากุหลาบซันตามาเรีย',
                    'product_id' => 3591,
                    'product_quantity' => 1
                ]
            ],
            [
                'package_id' => 4,
                'package_title' => 'ชุดนิมิตราชันย์เหมันต์',
                'package_desc' => 'ชุดนิมิตราชันย์เหมันต์',
                'icon' => 0,
                'require_token' => 75,
                'product_set' => [
                    'id' => 4,
                    'product_title' => 'ชุดนิมิตราชันย์เหมันต์',
                    'product_id' => 475,
                    'product_quantity' => 1
                ]
            ],
            [
                'package_id' => 5,
                'package_title' => 'หมวกกุ๊กไก่',
                'package_desc' => 'หมวกกุ๊กไก่',
                'icon' => 0,
                'require_token' => 20,
                'product_set' => [
                    'id' => 5,
                    'product_title' => 'หมวกกุ๊กไก่',
                    'product_id' => 1032,
                    'product_quantity' => 1
                ]
            ],
            [
                'package_id' => 6,
                'package_title' => 'กิ๊บติดผมกุหลาบชมพู',
                'package_desc' => 'กิ๊บติดผมกุหลาบชมพู',
                'icon' => 0,
                'require_token' => 20,
                'product_set' => [
                    'id' => 6,
                    'product_title' => 'กิ๊บติดผมกุหลาบชมพู',
                    'product_id' => 3592,
                    'product_quantity' => 1
                ]
            ],
            [
                'package_id' => 7,
                'package_title' => 'ชุดกมลพรรณอันทรงเกียรติ',
                'package_desc' => 'ชุดกมลพรรณอันทรงเกียรติ',
                'icon' => 0,
                'require_token' => 50,
                'product_set' => [
                    'id' => 7,
                    'product_title' => 'ชุดกมลพรรณอันทรงเกียรติ',
                    'product_id' => 1631,
                    'product_quantity' => 1
                ]
            ],
            [
                'package_id' => 8,
                'package_title' => 'เครื่องประดับกมลพรรณอันทรงเกียรติ',
                'package_desc' => 'เครื่องประดับกมลพรรณอันทรงเกียรติ',
                'icon' => 0,
                'require_token' => 20,
                'product_set' => [
                    'id' => 8,
                    'product_title' => 'เครื่องประดับกมลพรรณอันทรงเกียรติ',
                    'product_id' => 1632,
                    'product_quantity' => 1
                ]
            ],
        ]; */
    }
}
