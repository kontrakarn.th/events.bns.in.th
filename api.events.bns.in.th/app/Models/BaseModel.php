<?php

namespace App\Models;

use Cache;
use Illuminate\Database\Eloquent\Model;

/**
 * This model contains custom methods that can be used by all models
 *
 * @author ggadv2 (Nithiwat Maneesint) <nithiwat@garena.co.th>
 */
class BaseModel extends Model
{

    public static function getone($sql, $bindings = array())
    {
        $instance = new static;
        $connection = $instance->getConnection();

        $result = $connection->select($sql, $bindings)->toArray();
        if (count($result)) {
            return $result[0];
        }

        return array();
    }

    public static function getall($sql, $bindings = array())
    {
        $instance = new static;
        $connection = $instance->getConnection();

        $result = $connection->select($sql, $bindings)->toArray();
        if (count($result)) {
            return $result;
        }

        return array();
    }

    /**
     * Execute a select query then cache the result for the specified time
     * return an associative array of the sql result
     *
     * @author ggadv2 (Nithiwat Maneesint) <nithiwat@garena.co.th>
     *
     * @param string $sql
     * @param array $bindings
     * @param int $time
     * @return mixed
     */
    public static function cachegetone($sql, $bindings = array(), $time = 15)
    {
        $instance = new static;
        $connection = $instance->getConnection();

        if (strpos(strtolower($sql), 'limit') === FALSE) {
            if (strpos(strtolower($sql), 'count') === FALSE) {
                $sql = $sql . ' LIMIT 1';
            }
        }

        $key = vsprintf(str_replace('?', '%s', $sql), $bindings);
        $key = md5($key);
        if (Cache::has($key)) {
            return Cache::get($key);
        }

        $result = $connection->select($sql, $bindings)->toArray();
        if (count($result)) {
            Cache::put($key, $result[0], $time);
            return $result[0];
        }

        return array();
    }

    /**
     * Execute a select query then cache the result for the specified time
     * return array containing rows of associative array of the sql result
     *
     * @author ggadv2 (Nithiwat Maneesint) <nithiwat@garena.co.th>
     *
     * @param string $sql
     * @param array $bindings
     * @param int $time
     * @return mixed
     */
    public static function cachegetall($sql, $bindings = array(), $time = 15)
    {
        $instance = new static;
        $connection = $instance->getConnection();

        $key = vsprintf(str_replace('?', '%s', $sql), $bindings);
        $key = md5($key);
        if (Cache::has($key)) {
            return Cache::get($key);
        }

        $result = $connection->select($sql, $bindings)->toArray();
        if (count($result)) {
            foreach ($result as $key => $value) {
                $result[$key] = $value->to_array();
            }
            Cache::put($key, $result, $time);
            return $result;
        }

        return array();
    }

}
