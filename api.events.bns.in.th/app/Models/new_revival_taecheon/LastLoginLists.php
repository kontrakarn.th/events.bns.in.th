<?php

namespace App\Models\new_revival_taecheon;

use Illuminate\Database\Eloquent\Model;

class LastLoginLists extends Model
{

    protected $connection = 'events_bns';
    protected $table = 'new_revival_last_login_list';
    protected $fillable = [
        'uid'
    ];
}
