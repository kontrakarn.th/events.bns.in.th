<?php

namespace App\Models\bns_settings;

use Illuminate\Database\Eloquent\Model;

class BnsSettings extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'bns_settings';

}
