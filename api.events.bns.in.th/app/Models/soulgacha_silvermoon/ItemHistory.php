<?php

namespace App\Models\soulgacha_silvermoon;

use Illuminate\Database\Eloquent\Model;

class ItemHistory extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'silvermoon_item_histories';
    protected $fillable = [
        'product_id',
        'product_title',
        'product_quantity',
        'uid',
        'ncid',
        'item_type', // 'material','object','rare_item'
        'status',
        'last_ip',
        'icon',
        'gachapon_type', // 'ten','one','merge','redeem','airpay'
        'product_set',
        'product_pack_data',
        'send_type', // 'owner','gift','not_send','feather'
        'send_to_uid',
        'send_to_name',
        'log_date',
        'log_date_timestamp'
    ];
    protected $dates = ['log_date'];
}
