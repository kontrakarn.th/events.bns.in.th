<?php

    namespace App\Models\package_3rdspec;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'package_3rdspec_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'discount_used',
            'last_ip',
            'created_at',
            'updated_at'
        ];

    }