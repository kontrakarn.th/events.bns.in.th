<?php

    namespace App\Models\package_3rdspec;

    use App\Models\BaseModel;

    class DeductLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'package_3rdspec_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'diamonds',
            'package_id',
            'package_title',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }