<?php

    namespace App\Models\happy_xmas2018;

    use App\Models\BaseModel;

    class Exchange extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'happy_xmas_2018_exchange';

        protected $fillable = [
            'id',
            'ncid',
            'uid',
            'item_id',
            'item_name',
            'item_amount',
            'good_data',
            'status',
            'send_itme_status',
            'send_itme_datetime',
            'send_itme_timestamp',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'created_at',
            'updated_at',
        ];
    }
