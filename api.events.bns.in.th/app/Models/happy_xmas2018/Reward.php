<?php
    // Mission for disciple
    namespace App\Models\happy_xmas2018;

    class Reward {

        public function setGachaByKey($packageKey=''){
            if(empty($packageKey)){
                return false;
            }

            $rewards = $this->gachaList();

            return (isset($rewards[$packageKey-1]) && !empty($rewards[$packageKey-1])) ? $rewards[$packageKey-1] : false;
        }

        public static function gachaList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'แพ็คเกจคริสตัลหินโซล 10 ชิ้น',
                    'package_desc' => 'แพ็คเกจคริสตัลหินโซล 10 ชิ้น',
                    'rate_min' => 1,
                    'rate_max' => 12000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'แพ็คเกจคริสตัลหินโซล 10 ชิ้น',
                            'product_id' => 1928,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 2,
                    'package_title' => 'แพ็คเกจคริสตัลหินจันทรา 10 ชิ้น',
                    'package_desc' => 'แพ็คเกจคริสตัลหินจันทรา 10 ชิ้น',
                    'rate_min' => 12001,
                    'rate_max' => 24000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'แพ็คเกจคริสตัลหินจันทรา 10 ชิ้น',
                            'product_id' => 1929,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 3,
                    'package_title' => 'แพ็คหินโซล 10 ชิ้น',
                    'package_desc' => 'แพ็คหินโซล 10 ชิ้น',
                    'rate_min' => 24001,
                    'rate_max' => 27000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'แพ็คหินโซล 10 ชิ้น',
                            'product_id' => 1930,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 4,
                    'package_title' => 'แพ็คเกจหินจันทรา 10 ชิ้น',
                    'package_desc' => 'แพ็คเกจหินจันทรา 10 ชิ้น',
                    'rate_min' => 27001,
                    'rate_max' => 30000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'แพ็คเกจหินจันทรา 10 ชิ้น',
                            'product_id' => 1931,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 5,
                    'package_title' => 'สัญลักษณ์ฮงมุน x100',
                    'package_desc' => 'สัญลักษณ์ฮงมุน x100',
                    'rate_min' => 30001,
                    'rate_max' => 45000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'สัญลักษณ์ฮงมุน x100',
                            'product_id' => 1884,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 6,
                    'package_title' => 'เหรียญแห่งพันธนาการ x10',
                    'package_desc' => 'เหรียญแห่งพันธนาการ x10',
                    'rate_min' => 45001,
                    'rate_max' => 60000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'เหรียญแห่งพันธนาการ x10',
                            'product_id' => 1932,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 7,
                    'package_title' => 'ประกายฉลามดำ x2',
                    'package_desc' => 'ประกายฉลามดำ x2',
                    'rate_min' => 60001,
                    'rate_max' => 65000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ประกายฉลามดำ x2',
                            'product_id' => 1852,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 8,
                    'package_title' => 'ชิ้นส่วนฉลามดำ x5',
                    'package_desc' => 'ชิ้นส่วนฉลามดำ x5',
                    'rate_min' => 65001,
                    'rate_max' => 73000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชิ้นส่วนฉลามดำ x5',
                            'product_id' => 1933,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 9,
                    'package_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว x2',
                    'package_desc' => 'สัญลักษณ์กลุ่มโจรสลัดแมว x2',
                    'rate_min' => 73001,
                    'rate_max' => 78000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'สัญลักษณ์กลุ่มโจรสลัดแมว x2',
                            'product_id' => 1874,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 10,
                    'package_title' => 'ชิ้นส่วน สัญลักษณ์กลุ่มโจรสลัดแมว x5',
                    'package_desc' => 'ชิ้นส่วน สัญลักษณ์กลุ่มโจรสลัดแมว x5',
                    'rate_min' => 78001,
                    'rate_max' => 83000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชิ้นส่วน สัญลักษณ์กลุ่มโจรสลัดแมว x5',
                            'product_id' => 1934,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 11,
                    'package_title' => 'หินล้ำค่าหายาก x10',
                    'package_desc' => 'หินล้ำค่าหายาก x10',
                    'rate_min' => 83001,
                    'rate_max' => 90000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินล้ำค่าหายาก x10',
                            'product_id' => 1885,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 12,
                    'package_title' => 'หินล้ำค่าหายาก x5',
                    'package_desc' => 'หินล้ำค่าหายาก x5',
                    'rate_min' => 90001,
                    'rate_max' => 94000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินล้ำค่าหายาก x5',
                            'product_id' => 1887,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 13,
                    'package_title' => 'กล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส)',
                    'package_desc' => 'กล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส)',
                    'rate_min' => 94001,
                    'rate_max' => 97000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'กล่องลูกแก้ว ขั้น 1 (อังคาร พุธ พฤหัส)',
                            'product_id' => 1935,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
                [
                    'package_id' => 14,
                    'package_title' => 'กล่องอัญมณีห้าเหลี่ยมของฮงมุน',
                    'package_desc' => 'กล่องอัญมณีห้าเหลี่ยมของฮงมุน',
                    'rate_min' => 97001,
                    'rate_max' => 100000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'กล่องอัญมณีห้าเหลี่ยมของฮงมุน',
                            'product_id' => 1936,
                            'product_quantity' => 1,
                            'product_type'=>'ingame'
                        ]
                    ]
                ],
            ];
        }

        public static function fixList(){
            return [
                [
                    'id' => 1,
                    'product_title' => 'รูดอล์ฟ',
                    'product_id' => 1944,
                    'product_quantity' => 1,
                    'product_type'=>'ingame'
                ],
            ];
        }
    }
