<?php

    namespace App\Models\happy_xmas2018;

    use App\Models\BaseModel;

    class Play extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'happy_xmas_2018_play';
        protected $fillable = [
            'id',
            'ncid',
            'uid',
            'package_id',
            'status',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'created_at',
            'updated_at',
        ];

        public static function getPlayToday($uid){
            $data = self::where('uid',$uid)->whereRaw('DATE(created_at)=CURDATE()')->count();
            return $data;
        }

    }
