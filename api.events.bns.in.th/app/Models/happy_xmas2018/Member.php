<?php

    namespace App\Models\happy_xmas2018;

    use App\Models\BaseModel;

    class Member extends BaseModel {

        protected $connection = 'events_bns';
        protected $table = 'happy_xmas_2018_members';
        protected $fillable = [
            'id',
            'uid',
            'ncid',
            'username',
            'char_id',
            'char_name',
            'world_id',
            'last_ip',
            'total_play',
            'created_at',
            'updated_at',
        ];
    }
