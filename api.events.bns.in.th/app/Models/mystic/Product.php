<?php

namespace App\Models\mystic;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'mystic_product';
    protected $fillable = [
        'id',
        'product_group_id',
        'th_name',
        'en_name',
        'amount',
        'rate',
        'product_img',
    ];

}
