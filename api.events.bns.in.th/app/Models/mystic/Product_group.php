<?php

namespace App\Models\mystic;

use Illuminate\Database\Eloquent\Model;

class Product_group extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'mystic_product_group';
    protected $fillable = [
        'id',
        'name',
        'rate_group',
        'get_amount',
        'report_column',

    ];
    public function Product(){
        return $this->Hasmany('App\Models\mystic\Product','product_group_id');
    }
}
