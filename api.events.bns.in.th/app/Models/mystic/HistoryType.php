<?php

namespace App\Models\mystic;

use Illuminate\Database\Eloquent\Model;

class HistoryType extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'mystic_history_type';
    protected $fillable = [
        'id',
        'name',
        'table_name',
        'model_name',
    ];
}
