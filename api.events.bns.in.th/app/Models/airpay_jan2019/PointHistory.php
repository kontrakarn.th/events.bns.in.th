<?php

    namespace App\Models\airpay_jan2019;

    use App\Models\BaseModel;

    class PointHistory extends BaseModel {

        protected $connection = 'events_bns';
        protected $table = 'airpay_jan2019_point_history';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'title',
            'points',
            'status', // pending, success
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    