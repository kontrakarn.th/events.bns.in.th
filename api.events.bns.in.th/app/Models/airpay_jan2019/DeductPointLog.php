<?php

    namespace App\Models\airpay_jan2019;

    use App\Models\BaseModel;

    class DeductPointLog extends BaseModel {

        protected $connection = 'events_bns';
        protected $table = 'airpay_jan2019_deduct_point_log';
        protected $fillable = [
            'uid',
        	'username',
        	'ncid',
        	'status',
            'points',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }
    