<?php

    namespace App\Models\airpay_jan2019;

    use App\Models\BaseModel;

    class Member extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'airpay_jan2019_members';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'total_diamonds',
            'last_ip',
        ];

    }