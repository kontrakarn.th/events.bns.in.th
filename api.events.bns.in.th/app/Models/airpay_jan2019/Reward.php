<?php

    namespace App\Models\airpay_jan2019;

    class Reward {

        ////////////  Airpay Gachapon  /////////////

        public function getRandomAirpayReward(){

            $rewards = $this->getAirpayRewardsList();

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($rewards as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    // $value['color'] = $this->randomColor();
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            $reward['timestamp'] = time();

            // now we have the reward
            return $reward;
        }

        private function getAirpayRewardsList(){
            return [
                [
                    'id' => 1,
                    'item_type' => 'airpay_gachapon',
                    'product_title' => 'ชุดระบำสิงโต',
                    'chance' => 1.000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชุดระบำสิงโต',
                            'product_id' => 1984,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 2,
                    'item_type' => 'airpay_gachapon',
                    'product_title' => 'หมวกระบำสิงโต',
                    'chance' => 2.000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชุดระบำสิงโต',
                            'product_id' => 1985,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 3,
                    'item_type' => 'airpay_gachapon',
                    'product_title' => 'คริสตัลวิญญาณฮงมุน',
                    'chance' => 5.000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'คริสตัลวิญญาณฮงมุน',
                            'product_id' => 1896,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 4,
                    'item_type' => 'airpay_gachapon',
                    'product_title' => 'กรุพิศวง',
                    'chance' => 5.000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'กรุพิศวง',
                            'product_id' => 1986,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 5,
                    'item_type' => 'airpay_gachapon',
                    'product_title' => 'เกล็ดมังกรไฟ x5',
                    'chance' => 10.000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'เกล็ดมังกรไฟ',
                            'product_id' => 1987,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 6,
                    'item_type' => 'airpay_gachapon',
                    'product_title' => 'คริสตัลอัญมณีฮงมุน x5',
                    'chance' => 10.000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'คริสตัลอัญมณีฮงมุน',
                            'product_id' => 1988,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 7,
                    'item_type' => 'airpay_gachapon',
                    'product_title' => 'หินเปลี่ยนรูปชั้นสูง',
                    'chance' => 5.000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินเปลี่ยนรูปชั้นสูง',
                            'product_id' => 771,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 8,
                    'item_type' => 'airpay_gachapon',
                    'product_title' => 'หินจันทรา x5 หินโซล x15',
                    'chance' => 62.000,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินจันทรา x5',
                            'product_id' => 779,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'หินโซล x15',
                            'product_id' => 1989,
                            'product_quantity' => 1
                        ],
                    ],
                ],
            ];
        }

        public function airpayGaranteeReward(){
            return [
                'id' => 1,
                'item_type' => 'airpay_gachapon',
                'product_title' => 'สัญลักษณ์ฮงมุน x50',
                'product_set' => [
                    [
                        'id' => 1,
                        'product_title' => 'สัญลักษณ์ฮงมุน x50',
                        'product_id' => 1881,
                        'product_quantity' => 1
                    ],
                ],
            ];
        }

        public function airpayGaranteePoints(){
            return [
                'point_title' => 'เหรียญ Airpay x2',
                'point_quantity' => 2,
            ];
        }

        ////////////  Airpay Gachapon  /////////////

        ////////////  Exchange  /////////////

        public function setExchangeByPackageId($packageId=null){
            if(empty($packageId)){
                return false;
            }

            $rewards = $this->exchangeRewardList();

            return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
        }

        public function exchangeRewardList(){
            return [
                [
                    'id' => 1,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ชุดระบำสิงโต',
                    'require_points' => 5,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ชุดระบำสิงโต',
                            'product_id' => 1984,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 2,
                    'item_type' => 'exchange_points',
                    'product_title' => 'หมวกระบำสิงโต',
                    'require_points' => 3,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หมวกระบำสิงโต',
                            'product_id' => 1985,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 3,
                    'item_type' => 'exchange_points',
                    'product_title' => 'หินเปลี่ยนรูปชั้นสูง',
                    'require_points' => 3,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินเปลี่ยนรูปชั้นสูง',
                            'product_id' => 771,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 4,
                    'item_type' => 'exchange_points',
                    'product_title' => 'หินจันทรา x50',
                    'require_points' => 2,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินจันทรา x50',
                            'product_id' => 1925,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 5,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ยันต์อัพเกรด วิถีฮงมุนที่ส่องสว่าง',
                    'require_points' => 2,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ยันต์อัพเกรด วิถีฮงมุนที่ส่องสว่าง',
                            'product_id' => 1926,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 6,
                    'item_type' => 'exchange_points',
                    'product_title' => 'ประกายฉลามดำ x5',
                    'require_points' => 1,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ประกายฉลามดำ x5',
                            'product_id' => 1855,
                            'product_quantity' => 1
                        ],
                    ],
                ],
                [
                    'id' => 7,
                    'item_type' => 'exchange_points',
                    'product_title' => 'หินโซล x100',
                    'require_points' => 1,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินโซล x100',
                            'product_id' => 1918,
                            'product_quantity' => 1
                        ],
                    ],
                ],
            ];
        }

        ////////////  Exchange  /////////////

    }