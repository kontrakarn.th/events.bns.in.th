<?php

namespace App\Models\TopupMay;

use Illuminate\Database\Eloquent\Model;

class TopupMayCounterRedeem extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'topup_may_counter_redeems';
    protected $fillable = [
        'bonus_1', // 1500
        'bonus_2', // 1000
        'bonus_3' // 500
    ];
}
