<?php

namespace App\Models\TopupMay;

use Illuminate\Database\Eloquent\Model;

class TopupMayRedeemLogs extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'topup_may_redeem_logs';
    protected $fillable = [
        'member_id',
        'bonus_type'
    ];


}
