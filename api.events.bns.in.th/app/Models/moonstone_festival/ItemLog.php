<?php

    namespace App\Models\moonstone_festival;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'moonstone_festival_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'package_id',
            'package_title',
            'amount',
            'status',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'log_date',
            'log_date_timestamp',
            'last_ip'
        ];

    }