<?php

    namespace App\Models\moonstone_festival;

    class Reward {

        public function setRewardByPackageId($packageId=null){
            if(empty($packageId)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'Moonstone Package',
                    'amount' => 24,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินจันทรา',
                            'product_id' => 1919,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 2,
                    'package_title' => 'Soulstone Package',
                    'amount' => 100,
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'หินโซล',
                            'product_id' => 1918,
                            'product_quantity' => 1
                        ]
                    ]
                ],

            ];
        }

    }