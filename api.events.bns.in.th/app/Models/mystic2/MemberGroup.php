<?php

namespace App\Models\mystic2;

use App\Models\BaseModel;

class MemberGroup extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'mystic2_members_group';
    protected $fillable = [
        'uid',
        'group',
    ];
}
