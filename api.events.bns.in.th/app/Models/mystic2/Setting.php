<?php

namespace App\Models\mystic2;

use App\Models\BaseModel;

class Setting extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'mystic2_settings';
    protected $fillable = [
        'event_start',
        'event_end',
        'attend_start',
        'attend_end',
        'exchange_start',
        'exchange_end',
        'active',
    ];
}
