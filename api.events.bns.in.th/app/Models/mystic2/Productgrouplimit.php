<?php

namespace App\Models\mystic2;

use Illuminate\Database\Eloquent\Model;

class Productgrouplimit extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'mystic2_product_group_limit';
    protected $fillable = [
        'id',
        'uid',
        'product_group_id',
        'product_id',
    ];
    public function Product(){
        return $this->belongsTo('App\Models\mystic2\Product','product_id');
    }
}
