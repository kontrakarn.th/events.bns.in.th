<?php

namespace App\Models\mystic2;

use Illuminate\Database\Eloquent\Model;

class Product_group extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'mystic2_product_group';
    protected $fillable = [
        'id',
        'name',
        'rate_group',
        'get_amount',
        'report_column',

    ];
    public function Product(){
        return $this->hasMany('App\Models\mystic2\Product','product_group_id', 'product_group_id');
    }
}
