<?php

namespace App\Models\mystic2;

use App\Models\BaseModel;

class Report extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'mystic2_report';
    protected $fillable = [
        'unique_visit',
        'total_buy_key',
        'total_get_item',
        'total_get_special_item',
        'total_get_diamond',
        'total_get_soulsheild_monster',
        'total_get_soulsheild_person',
        'total_get_red_dragon',
        'total_get_gold',
        'total_get_strength',
        'total_get_support',
        'total_get_honorable',
        'total_get_value_stamp',
    ];
}
