<?php

namespace App\Models\mystic2;

use Illuminate\Database\Eloquent\Model;

class KeyPackage extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'mystic2_key_package';
    protected $fillable = [
        'id',
        'name',
        'amount',
        'bonus',
        'daimonds_price',
    ];

}
