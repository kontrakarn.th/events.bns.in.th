<?php

namespace App\Models\mystic2;

use Illuminate\Database\Eloquent\Model;

class Userspecialproduct extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'mystic2_user_special_item';
    protected $fillable = [
        'uid',
        'special_item_id',
        'type_select',

    ];
    public function Product(){
        return $this->belongsTo('App\Models\mystic2\Product','special_item_id');
    }

}
