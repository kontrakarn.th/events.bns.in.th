<?php

    namespace App\Models\piggy;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'piggy_item_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'deduct_id',
            'round_id',
            'product_set',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip'
        ];

    }