<?php

    namespace App\Models\piggy;

    use App\Models\BaseModel;

    class Reward extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'piggy_rewards';
        protected $fillable = [
            'package_name',
            'package_id',
            'package_quantity',
            'package_amount',
            'package_type', // diamonds, materials
            'package_key',
            'image',
            'available_date',
        ];
    }

