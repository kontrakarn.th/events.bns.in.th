<?php

    namespace App\Models\piggy;

    use App\Models\BaseModel;

    class Round extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'piggy_rounds';
        protected $fillable = [
            'start_datetime',
            'end_datetime',
            'status',
        ];
    }

