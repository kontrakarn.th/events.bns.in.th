<?php

    namespace App\Models\piggy;

    use App\Models\BaseModel;

    class Material extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'piggy_materials';
        protected $fillable = [
            'title',
            'quantity',
            'limit',
            'image',
        ];
    }

