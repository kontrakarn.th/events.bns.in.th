<?php

    namespace App\Models\season2_revival;

    use App\Models\BaseModel;

    class LastLoginList extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'new_revival_2018_last_login_list';
        protected $fillable = [
            'uid',
        ];

    }