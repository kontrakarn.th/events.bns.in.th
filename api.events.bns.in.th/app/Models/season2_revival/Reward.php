<?php

    namespace App\Models\season2_revival;

    class Reward {

        public function setReward(){
            return [
                'package_id' => 1,
                'package_title' => 'กล่องต้อนรับศิษย์น้องที่กลับมา',
                'product_set' => [
                    [
                        'id' => 1,
                        'product_title' => 'กล่องต้อนรับศิษย์น้องที่กลับมา',
                        'product_id' => 1823,
                        'product_quantity' => 1
                    ],
                ]
            ];
        }

    }