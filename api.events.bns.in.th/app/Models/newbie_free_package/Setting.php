<?php

namespace App\Models\newbie_free_package;

use App\Models\BaseModel;

class Setting extends BaseModel
{
    protected $connection = 'events_bns';
    protected $table = 'newbie_free_package_settings';
    protected $fillable = [
        'event_start',
        'event_end',
        'attend_start',
        'attend_end',
        'exchange_start',
        'exchange_end',
        'active',
    ];
}
