<?php

namespace App\Models\newbie_free_package;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'newbie_free_package_users';
    protected $fillable = [
        'id',
        'uid',
        'username',
        'ncid',
        'char_id',
        'char_name',
        'world_id',
        'job',
        'last_ip',
        'can_get_package',
    ];

}
