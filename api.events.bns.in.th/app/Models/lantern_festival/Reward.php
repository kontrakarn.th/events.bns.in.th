<?php

namespace App\Models\lantern_festival;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'lantern_festival_rewards';

}
