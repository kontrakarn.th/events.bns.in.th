<?php

namespace App\Models\lantern_festival;

use Illuminate\Database\Eloquent\Model;

class Cron extends Model
{
    protected $connection = 'events_bns_test';
    protected $table = 'lantern_festival_crons';

}
