<?php

    namespace App\Models\daily_login;

    use App\Models\BaseModel;

    class Reward extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'daily_login_rewards';
        protected $fillable = [
            'package_name',
            'package_id',
            'package_quantity',
            'package_amount',
            'package_type', // diamonds, materials
            'package_key',
            'days',
            'image',
            'available_date',
        ];
    }