<?php

    namespace App\Models\daily_login;

    use App\Models\BaseModel;

    class Checkin extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'daily_login_checkin';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'days',
            'is_checkin',
            'is_claimed',
            'claimed_log_date',
            'claimed_log_date_timestamp',
            'last_ip',
            'created_at',
            'updated_at',
        ];

    }