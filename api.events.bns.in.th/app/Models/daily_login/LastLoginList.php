<?php

namespace App\Models\daily_login;

use App\Models\BaseModel;

class LastLoginList extends BaseModel
{

    protected $connection = 'events_bns';
    protected $table = 'daily_login_last_login_list';
    protected $fillable = [
        'uid'
    ];
}
