<?php

    namespace App\Models\archer_package;

    use App\Models\BaseModel;

    class ItemLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'archer_package_item_logs';
        protected $fillable = [
            'uid',
            'ncid',
            'deduct_id',
            'require_diamonds',
            'package_id',
            'package_title',
            'product_set',
            'send_item_status',
            'send_item_purchase_id',
            'send_item_purchase_status',
            'goods_data',
            'status', // pending, success, unsuccess
            'last_ip',
            'log_date',
            'log_date_timestamp'
        ];

    }
