<?php

    namespace App\Models\archer_package;

    use App\Models\BaseModel;

    class DeductLog extends BaseModel {
        protected $connection = 'events_bns';
        protected $table = 'archer_package_deduct_logs';
        protected $fillable = [
            'uid',
            'username',
            'ncid',
            'diamonds',
            'package_id',
            'package_title',
            'weapon_id',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'status',
            'log_date',
            'log_date_timestamp',
            'last_ip',
        ];

    }