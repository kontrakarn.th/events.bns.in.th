<?php
    // Mission for disciple
    namespace App\Models\archer_package;

    class Reward {

        public function setRewardByPackage($packageKey=''){
            if(empty($packageKey)){
                return false;
            }

            $rewards = $this->packageList();

            return (isset($rewards[$packageKey-1]) && !empty($rewards[$packageKey-1])) ? $rewards[$packageKey-1] : false;
        }

        private function packageList(){
            return [
                [
                    'package_id' => 1,
                    'package_title' => 'แพ็คเกจสนับสนุนน้องใหม่ ขั้นเทพ',
                    'package_desc' => 'แพ็คเกจสนับสนุนน้องใหม่ ขั้นเทพ',
                    'require_diamonds' => 250000, // 250000
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'แพ็คเกจสนับสนุนน้องใหม่ ขั้นเทพ SET 1',
                            'product_id' => 2519,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'แพ็คเกจสนับสนุนน้องใหม่ ขั้นเทพ SET 2',
                            'product_id' => 2520,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'package_id' => 2,
                    'package_title' => 'แพ็คเกจสนับสนุนน้องใหม่',
                    'package_desc' => 'แพ็คเกจสนับสนุนน้องใหม่',
                    'require_diamonds' => 60000, // 60000
                    'product_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'แพ็คเกจสนับสนุนน้องใหม่ SET 1',
                            'product_id' => 2521,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'แพ็คเกจสนับสนุนน้องใหม่ SET 2',
                            'product_id' => 2522,
                            'product_quantity' => 1
                        ]
                    ]
                ],
            ];
        }

        public function setWeaponsByPackageId($packageId=0,$weaponId=0){
            if($packageId == 0 || $weaponId == 0){
                return false;
            }

            $rewards = [];
            if($packageId == 1){
                $rewards = $this->weaponsPackage1List();
            }elseif($packageId == 2){
                $rewards = $this->weaponsPackage2List();
            }
            
            if(empty($rewards)){
                return false;
            }

            return (isset($rewards[$weaponId-1]) && !empty($rewards[$weaponId-1])) ? $rewards[$weaponId-1] : false;
        }

        private function weaponsPackage1List(){
            return [
                [
                    'weapon_id' => 1,
                    'weapon_title' => 'ดาบคุนลุน ขั้น 6, ดาบอรุณ ขั้น 6',
                    'weapon_class' => 'เบลด มาสเตอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ดาบคุนลุน ขั้น 6',
                            'product_id' => 2533,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ดาบอรุณ ขั้น 6',
                            'product_id' => 2564,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 2,
                    'weapon_title' => 'สนับมือคุนลุน ขั้น 6, สนับมืออรุณ ขั้น 6',
                    'weapon_class' => 'กังฟูมาสเตอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'สนับมือคุนลุน ขั้น 6',
                            'product_id' => 2526,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'สนับมืออรุณ ขั้น 6',
                            'product_id' => 2565,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 3,
                    'weapon_title' => 'กำไลเวทย์คุนลุน ขั้น 6, กำไลเวทย์อรุณ ขั้น 6',
                    'weapon_class' => 'ฟอร์ซมาสเตอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'กำไลเวทย์คุนลุน ขั้น 6',
                            'product_id' => 2523,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'กำไลเวทย์อรุณ ขั้น 6',
                            'product_id' => 2567,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 4,
                    'weapon_title' => 'ปืนคู่คุนลุน ขั้น 6, ปืนคู่อรุณ ขั้น 6',
                    'weapon_class' => 'โซลกันเนอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ปืนคู่คุนลุน ขั้น 6',
                            'product_id' => 2528,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ปืนคู่อรุณ ขั้น 6',
                            'product_id' => 2573,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 5,
                    'weapon_title' => 'ขวานคุนลุน ขั้น 6, ขวานอรุณ ขั้น 6',
                    'weapon_class' => 'เดสทรอยเยอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ขวานคุนลุน ขั้น 6',
                            'product_id' => 2524,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ขวานอรุณ ขั้น 6',
                            'product_id' => 2568,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 6,
                    'weapon_title' => 'คทาคุนลุน ขั้น 6, คทาอรุณ ขั้น 6',
                    'weapon_class' => 'ซัมมอนเนอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'คทาคุนลุน ขั้น 6',
                            'product_id' => 2532,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'คทาอรุณ ขั้น 6',
                            'product_id' => 2566,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 7,
                    'weapon_title' => 'มีดคุนลุน ขั้น 6, มีดสั้นอรุณ ขั้น 6',
                    'weapon_class' => 'แอสแซสซิน',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'มีดคุนลุน ขั้น 6',
                            'product_id' => 2525,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'มีดสั้นอรุณ ขั้น 6',
                            'product_id' => 2569,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 8,
                    'weapon_title' => 'ดาบลินคุนลุน ขั้น 6, ดาบลินอรุณ ขั้น 6',
                    'weapon_class' => 'เบลดแดนเซอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ดาบลินคุนลุน ขั้น 6',
                            'product_id' => 2530,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ดาบลินอรุณ ขั้น 6',
                            'product_id' => 2570,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 9,
                    'weapon_title' => 'กริชคุนลุน ขั้น 6, กริชอรุณ ขั้น 6',
                    'weapon_class' => 'วอร์ลอค',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'กริชคุนลุน ขั้น 6',
                            'product_id' => 2534,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'กริชอรุณ ขั้น 6',
                            'product_id' => 2571,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 10,
                    'weapon_title' => 'สนับเวทย์คุนลุน ขั้น 6, สนับเวทย์อรุณ ขั้น 6',
                    'weapon_class' => 'โซลไฟเตอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'สนับเวทย์คุนลุน ขั้น 6',
                            'product_id' => 2531,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'สนับเวทย์อรุณ ขั้น 6',
                            'product_id' => 2572,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 11,
                    'weapon_title' => 'ดาบใหญ่คุนลุน ขั้น 6, ดาบใหญ่อรุณ ขั้น 6',
                    'weapon_class' => 'วอร์เดน',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ดาบใหญ่คุนลุน ขั้น 6',
                            'product_id' => 2527,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ดาบใหญ่อรุณ ขั้น 6',
                            'product_id' => 2574,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 12,
                    'weapon_title' => 'ธนูคุนลุน ขั้น 6, ธนูอรุณ ขั้น 6',
                    'weapon_class' => 'อาเชอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ธนูคุนลุน ขั้น 6',
                            'product_id' => 2529,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ธนูอรุณ ขั้น 6',
                            'product_id' => 2575,
                            'product_quantity' => 1
                        ]
                    ]
                ],
            ];
        }

        private function weaponsPackage2List(){
            return [
                [
                    'weapon_id' => 1,
                    'weapon_title' => 'ดาบคุนลุน ขั้น 1, ดาบอรุณ ขั้น 1',
                    'weapon_class' => 'เบลด มาสเตอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ดาบคุนลุน ขั้น 1',
                            'product_id' => 2557,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ดาบอรุณ ขั้น 1',
                            'product_id' => 2545,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 2,
                    'weapon_title' => 'สนับมือคุนลุน ขั้น 1, สนับมืออรุณ ขั้น 1',
                    'weapon_class' => 'กังฟูมาสเตอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'สนับมือคุนลุน ขั้น 1',
                            'product_id' => 2550,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'สนับมืออรุณ ขั้น 1',
                            'product_id' => 2538,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 3,
                    'weapon_title' => 'กำไลเวทย์คุนลุน ขั้น 1, กำไลเวทย์อรุณ ขั้น 1',
                    'weapon_class' => 'ฟอร์ซมาสเตอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'กำไลเวทย์คุนลุน ขั้น 1',
                            'product_id' => 2547,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'กำไลเวทย์อรุณ ขั้น 1',
                            'product_id' => 2535,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 4,
                    'weapon_title' => 'ปืนคู่คุนลุน ขั้น 1, ปืนคู่อรุณ ขั้น 1',
                    'weapon_class' => 'โซลกันเนอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ปืนคู่คุนลุน ขั้น 1',
                            'product_id' => 2552,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ปืนคู่อรุณ ขั้น 1',
                            'product_id' => 2540,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 5,
                    'weapon_title' => 'ขวานคุนลุน ขั้น 1, ขวานอรุณ ขั้น 1',
                    'weapon_class' => 'เดสทรอยเยอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ขวานคุนลุน ขั้น 1',
                            'product_id' => 2548,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ขวานอรุณ ขั้น 1',
                            'product_id' => 2536,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 6,
                    'weapon_title' => 'คทาคุนลุน ขั้น 1, คทาอรุณ ขั้น 1',
                    'weapon_class' => 'ซัมมอนเนอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'คทาคุนลุน ขั้น 1',
                            'product_id' => 2556,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'คทาอรุณ ขั้น 1',
                            'product_id' => 2544,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 7,
                    'weapon_title' => 'มีดคุนลุน ขั้น 1, มีดสั้นอรุณ ขั้น 1',
                    'weapon_class' => 'แอสแซสซิน',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'มีดคุนลุน ขั้น 1',
                            'product_id' => 2549,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'มีดสั้นอรุณ ขั้น 1',
                            'product_id' => 2537,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 8,
                    'weapon_title' => 'ดาบลินคุนลุน ขั้น 1, ดาบลินอรุณ ขั้น 1',
                    'weapon_class' => 'เบลดแดนเซอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ดาบลินคุนลุน ขั้น 1',
                            'product_id' => 2554,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ดาบลินอรุณ ขั้น 1',
                            'product_id' => 2542,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 9,
                    'weapon_title' => 'กริชคุนลุน ขั้น 1, กริชอรุณ ขั้น 1',
                    'weapon_class' => 'วอร์ลอค',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'กริชคุนลุน ขั้น 1',
                            'product_id' => 2558,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'กริชอรุณ ขั้น 1',
                            'product_id' => 2546,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 10,
                    'weapon_title' => 'สนับเวทย์คุนลุน ขั้น 1, สนับเวทย์อรุณ ขั้น 1',
                    'weapon_class' => 'โซลไฟเตอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'สนับเวทย์คุนลุน ขั้น 1',
                            'product_id' => 2555,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'สนับเวทย์อรุณ ขั้น 1',
                            'product_id' => 2543,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 11,
                    'weapon_title' => 'ดาบใหญ่คุนลุน ขั้น 1, ดาบใหญ่อรุณ ขั้น 1',
                    'weapon_class' => 'วอร์เดน',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ดาบใหญ่คุนลุน ขั้น 1',
                            'product_id' => 2551,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ดาบใหญ่อรุณ ขั้น 1',
                            'product_id' => 2539,
                            'product_quantity' => 1
                        ]
                    ]
                ],
                [
                    'weapon_id' => 12,
                    'weapon_title' => 'ธนูคุนลุน ขั้น 1, ธนูอรุณ ขั้น 1',
                    'weapon_class' => 'อาเชอร์',
                    'weapon_set' => [
                        [
                            'id' => 1,
                            'product_title' => 'ธนูคุนลุน ขั้น 1',
                            'product_id' => 2553,
                            'product_quantity' => 1
                        ],
                        [
                            'id' => 2,
                            'product_title' => 'ธนูอรุณ ขั้น 1',
                            'product_id' => 2541,
                            'product_quantity' => 1
                        ]
                    ]
                ],
            ];
        }

    }
