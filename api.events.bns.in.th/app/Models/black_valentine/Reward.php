<?php

namespace App\Models\black_valentine;
use Carbon\Carbon;

class Reward
{

    public function getRandomRewardInfo()
    {
        // $date_rate=Carbon::parse("2019-10-05 00:00:00");
        // $date_now=Carbon::now();
        // if($date_now<$date_rate){
        //     $rewards = $this->getRewardsListFirstDay();
        // }else{
            $rewards = $this->getRewardsList();
        // }

        $random = rand(0, 100000);
        $accumulatedChance = 0;
        $reward = false;

        foreach ($rewards as $value) {
            $chance = $value['chance'] * 1000;
            $currentChance = $accumulatedChance + $chance;

                // acquired reward
            if ($random >= $accumulatedChance && $random <= $currentChance) {
                    // $value['color'] = $this->randomColor();
                $reward = $value;
                break;
            }
            $accumulatedChance = $currentChance;
        }

        $reward['random'] = $random;

            // now we have the reward
        return $reward;
    }

    protected function getRewardsList()
    {
        return [
            [
                'id' => 1,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดอาทิตย์อัสดง',
                'product_id' => 3055,
                'product_quantity' => 1,
                'chance' => 0.100,
                'icon' => 'item_05_Base',
                'key' => 'item_05_get',
                'color' => 'gold'
            ],
            [
                'id' => 2,
                'item_type' => 'rare_item',
                'product_title' => 'กล่องอาวุธลวงตาจุมพิตอสูร',
                'product_id' => 3056,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'item_04_Base',
                'key' => 'item_04_get',
                'color' => 'gold'
            ],
            [
                'id' => 3,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดจุมพิตอสูร',
                'product_id' => 3057,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'item_01_Base',
                'key' => 'item_01_get',
                'color' => 'gold'
            ],
            [
                'id' => 4,
                'item_type' => 'rare_item',
                'product_title' => 'หมวกจุมพิตอสูร',
                'product_id' => 3058,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'item_02_Base',
                'key' => 'item_02_get',
                'color' => 'gold'
            ],
            [
                'id' => 5,
                'item_type' => 'rare_item',
                'product_title' => 'ปีกจุมพิตอสูร',
                'product_id' => 3059,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'item_03_Base',
                'key' => 'item_03_get',
                'color' => 'gold'
            ],
            [
                'id' => 6,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ทกุหลาบซันตามาเรีย',
                'product_id' => 3060,
                'product_quantity' => 1,
                'chance' => 0.050,
                'icon' => 'item_06_Base',
                'key' => 'item_06_get',
                'color' => 'gold'
            ],
            // Items parts set A
            [
                'id' => 7,
                'item_type' => 'material',
                'product_title' => 'ชุดจุมพิตอสูร (Part 1)',
                'product_id_as' => 'MAT_1_1',
                'product_id' => 20791,
                'product_quantity' => 1,
                'chance' => 0.400,
                'icon' => 'item_01_R',
                'key' => 'item_01_R_get',
                'color' => 'red'
            ],
            [
                'id' => 8,
                'item_type' => 'material',
                'product_title' => 'ชุดจุมพิตอสูร (Part 2)',
                'product_id_as' => 'MAT_1_2',
                'product_id' => 20792,
                'product_quantity' => 1,
                'chance' => 0.200,
                'icon' => 'item_01_G',
                'key' => 'item_01_G_get',
                'color' => 'green'
            ],
            [
                'id' => 9,
                'item_type' => 'material',
                'product_title' => 'ชุดจุมพิตอสูร (Part 3)',
                'product_id_as' => 'MAT_1_3',
                'product_id' => 20793,
                'product_quantity' => 1,
                'chance' => 0.100,
                'icon' => 'item_01_B',
                'key' => 'item_01_B_get',
                'color' => 'blue'
            ],
                // Items parts set B
            [
                'id' => 10,
                'item_type' => 'material',
                'product_title' => 'หมวกจุมพิตอสูร (Part1)',
                'product_id_as' => 'MAT_2_1',
                'product_id' => 20801,
                'product_quantity' => 1,
                'chance' => 0.400,
                'icon' => 'item_02_R',
                'key' => 'item_02_R_get',
                'color' => 'red'
            ],
            [
                'id' => 11,
                'item_type' => 'material',
                'product_title' => 'หมวกจุมพิตอสูร (Part2)',
                'product_id_as' => 'MAT_2_2',
                'product_id' => 20802,
                'product_quantity' => 1,
                'chance' => 0.200,
                'icon' => 'item_02_G',
                'key' => 'item_02_G_get',
                'color' => 'green'
            ],
            [
                'id' => 12,
                'item_type' => 'material',
                'product_title' => 'หมวกจุมพิตอสูร (Part3)',
                'product_id_as' => 'MAT_2_3',
                'product_id' => 20803,
                'product_quantity' => 1,
                'chance' => 0.100,
                'icon' => 'item_02_B',
                'key' => 'item_02_B_get',
                'color' => 'blue'
            ],

                // Items parts set C
            [
                'id' => 13,
                'item_type' => 'material',
                'product_title' => 'ปีกจุมพิตอสูร (Part1)',
                'product_id_as' => 'MAT_3_1',
                'product_id' => 20811,
                'product_quantity' => 1,
                'chance' => 0.400,
                'icon' => 'item_03_R',
                'key' => 'item_03_R_get',
                'color' => 'red'
            ],
            [
                'id' => 14,
                'item_type' => 'material',
                'product_title' => 'ปีกจุมพิตอสูร (Part2)',
                'product_id_as' => 'MAT_3_2',
                'product_id' => 20812,
                'product_quantity' => 1,
                'chance' => 0.200,
                'icon' => 'item_03_G',
                'key' => 'item_03_G_get',
                'color' => 'green'
            ],
            [
                'id' => 15,
                'item_type' => 'material',
                'product_title' => 'ปีกจุมพิตอสูร (Part3)',
                'product_id_as' => 'MAT_3_3',
                'product_id' => 20813,
                'product_quantity' => 1,
                'chance' => 0.100,
                'icon' => 'item_03_B',
                'key' => 'item_03_B_get',
                'color' => 'blue'
            ],


            // Items parts set D
            [
                'id' => 16,
                'item_type' => 'material',
                'product_title' => 'กล่องอาวุธลวงตาจุมพิตอสูร (Part1)',
                'product_id_as' => 'MAT_4_1',
                'product_id' => 20821,
                'product_quantity' => 1,
                'chance' => 0.400,
                'icon' => 'item_04_R',
                'key' => 'item_04_R_get',
                'color' => 'red'
            ],
            [
                'id' => 17,
                'item_type' => 'material',
                'product_title' => 'กล่องอาวุธลวงตาจุมพิตอสูร (Part2)',
                'product_id_as' => 'MAT_4_2',
                'product_id' => 20822,
                'product_quantity' => 1,
                'chance' => 0.200,
                'icon' => 'item_04_G',
                'key' => 'item_04_G_get',
                'color' => 'green'
            ],
            [
                'id' => 18,
                'item_type' => 'material',
                'product_title' => 'กล่องอาวุธลวงตาจุมพิตอสูร (Part3)',
                'product_id_as' => 'MAT_4_3',
                'product_id' => 20823,
                'product_quantity' => 1,
                'chance' => 0.100,
                'icon' => 'item_04_B',
                'key' => 'item_04_B_get',
                'color' => 'blue'
            ],


            // Items parts set E
            [
                'id' => 19,
                'item_type' => 'material',
                'product_title' => 'ชุดอาทิตย์อัสดง (Part1)',
                'product_id_as' => 'MAT_5_1',
                'product_id' => 20831,
                'product_quantity' => 1,
                'chance' => 0.400,
                'icon' => 'item_05_R',
                'key' => 'item_05_R_get',
                'color' => 'red'
            ],
            [
                'id' => 20,
                'item_type' => 'material',
                'product_title' => 'ชุดอาทิตย์อัสดง (Part2)',
                'product_id_as' => 'MAT_5_2',
                'product_id' => 20832,
                'product_quantity' => 1,
                'chance' => 0.200,
                'icon' => 'item_05_G',
                'key' => 'item_05_G_get',
                'color' => 'green'
            ],
            [
                'id' => 21,
                'item_type' => 'material',
                'product_title' => 'ชุดอาทิตย์อัสดง (Part3)',
                'product_id_as' => 'MAT_5_3',
                'product_id' => 20833,
                'product_quantity' => 1,
                'chance' => 0.100,
                'icon' => 'item_05_B',
                'key' => 'item_05_B_get',
                'color' => 'blue'
            ],

            // Items parts set F
            [
                'id' => 22,
                'item_type' => 'material',
                'product_title' => 'เซ็ทกุหลาบซันตามาเรีย (Part1)',
                'product_id_as' => 'MAT_6_1',
                'product_id' => 20841,
                'product_quantity' => 1,
                'chance' => 0.400,
                'icon' => 'item_06_R',
                'key' => 'item_06_R_get',
                'color' => 'red'
            ],
            [
                'id' => 23,
                'item_type' => 'material',
                'product_title' => 'เซ็ทกุหลาบซันตามาเรีย (Part2)',
                'product_id_as' => 'MAT_6_2',
                'product_id' => 20842,
                'product_quantity' => 1,
                'chance' => 0.200,
                'icon' => 'item_06_G',
                'key' => 'item_06_G_get',
                'color' => 'green'
            ],
            [
                'id' => 24,
                'item_type' => 'material',
                'product_title' => 'เซ็ทกุหลาบซันตามาเรีย (Part3)',
                'product_id_as' => 'MAT_6_3',
                'product_id' => 20843,
                'product_quantity' => 1,
                'chance' => 0.100,
                'icon' => 'item_06_B',
                'key' => 'item_06_B_get',
                'color' => 'blue'
            ],

            // Object item
            [
                'id' => 25,
                'item_type' => 'object',
                'product_title' => 'สัญลักษณ์ฮงมุน x50',
                'product_id' => 1881,
                'product_quantity' => 1,
                'chance' => 15.950,
                'icon' => 'item_07',
                'key' => 'item_07',
                'color' => 'red'
            ],
            [
                'id' => 26,
                'item_type' => 'object',
                'product_title' => 'กิก้าโฟน x10',
                'product_id' => 1062,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'item_08',
                'key' => 'item_08',
                'color' => 'red'
            ],
            [
                'id' => 27,
                'item_type' => 'object',
                'product_title' => 'ยาโชคชะตา ขนาดใหญ่ x50',
                'product_id' => 2642,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'item_09',
                'key' => 'item_09',
                'color' => 'red'
            ],
            [
                'id' => 28,
                'item_type' => 'object',
                'product_title' => 'คริสตัลหินโซล x10',
                'product_id' => 1848,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'item_10',
                'key' => 'item_10',
                'color' => 'red'
            ],
            [
                'id' => 29,
                'item_type' => 'object',
                'product_title' => 'เกล็ดมังกรไฟ x10',
                'product_id' => 2643,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'item_11',
                'key' => 'item_11',
                'color' => 'red'
            ],
            [
                'id' => 30,
                'item_type' => 'object',
                'product_title' => 'คริสตัลหินจันทรา x5',
                'product_id' => 1116,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'item_12',
                'key' => 'item_12',
                'color' => 'red'
            ],
            [
                'id' => 31,
                'item_type' => 'object',
                'product_title' => 'เหรียญฝึกฝน ฮงซอกกึน x1',
                'product_id' => 2644,
                'product_quantity' => 1,
                'chance' => 10.000,
                'icon' => 'item_13',
                'key' => 'item_13',
                'color' => 'red'
            ],
            [
                'id' => 32,
                'item_type' => 'object',
                'product_title' => 'ยันต์ท้าทายรวดเร็ว สมาพันธ์ผู้ทรงเกียรติ x1',
                'product_id' => 3054,
                'product_quantity' => 1,
                'chance' => 5.000,
                'icon' => 'item_14',
                'key' => 'item_14',
                'color' => 'red'
            ],
            [
                'id' => 33,
                'item_type' => 'object',
                'product_title' => 'ไหมห้าสี x1',
                'product_id' => 2646,
                'product_quantity' => 1,
                'chance' => 4.000,
                'icon' => 'item_15',
                'key' => 'item_15',
                'color' => 'red'
            ],
            [
                'id' => 34,
                'item_type' => 'object',
                'product_title' => 'ค้อนอัญมณีที่ส่องสว่าง x1',
                'product_id' => 569,
                'product_quantity' => 1,
                'chance' => 3.000,
                'icon' => 'item_16',
                'key' => 'item_16',
                'color' => 'red'
            ],
            [
                'id' => 35,
                'item_type' => 'object',
                'product_title' => 'ลูกแก้วยอดนักรบ x1',
                'product_id' => 2647,
                'product_quantity' => 1,
                'chance' => 3.000,
                'icon' => 'item_17',
                'key' => 'item_17',
                'color' => 'red'
            ],
            [
                'id' => 36,
                'item_type' => 'object',
                'product_title' => 'ดาวมังกรฟ้า x1',
                'product_id' => 2648,
                'product_quantity' => 1,
                'chance' => 2.000,
                'icon' => 'item_18',
                'key' => 'item_18',
                'color' => 'red'
            ],
            [
                'id' => 37,
                'item_type' => 'object',
                'product_title' => 'กรุพิศวง x1',
                'product_id' => 1986,
                'product_quantity' => 1,
                'chance' => 1.000,
                'icon' => 'item_20',
                'key' => 'item_20',
                'color' => 'red'
            ],
            [
                'id' => 38,
                'item_type' => 'object',
                'product_title' => 'หินเปลี่ยนรูปชั้นสูง x1',
                'product_id' => 911,
                'product_quantity' => 1,
                'chance' => 1.000,
                'icon' => 'item_19',
                'key' => 'item_19',
                'color' => 'red'
            ],
            [
                'id' => 39,
                'item_type' => 'object',
                'product_title' => 'คริสตัลวิญญาณฮงมุน x1',
                'product_id' => 1896,
                'product_quantity' => 1,
                'chance' => 0.500,
                'icon' => 'item_21',
                'key' => 'item_21',
                'color' => 'red'
            ],
        ];
    }

    // Rare Items
    public function setRareItemByPackageId($packageId=null){
        if(empty($packageId)){
            return false;
        }

        $rewards = $this->rareItemList();

        return (isset($rewards[$packageId-1]) && !empty($rewards[$packageId-1])) ? $rewards[$packageId-1] : false;
    }

    private function rareItemList(){
        return [
            [
                'id' => 1,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดจุมพิตอสูร',
                'product_id' => 3057,
                'product_quantity' => 1,
                'icon' => 'item_01_Base',
                'key' => 'item_01',
                'color' => 'red'
            ],
            [
                'id' => 2,
                'item_type' => 'rare_item',
                'product_title' => 'หมวกจุมพิตอสูร',
                'product_id' => 3058,
                'product_quantity' => 1,
                'icon' => 'item_02_Base',
                'key' => 'item_02',
                'color' => 'red'

            ],
            [
                'id' => 3,
                'item_type' => 'rare_item',
                'product_title' => 'ปีกจุมพิตอสูร',
                'product_id' => 3059,
                'product_quantity' => 1,
                'icon' => 'item_03_Base',
                'key' => 'item_03',
                'color' => 'red'
            ],
            [
                'id' => 4,
                'item_type' => 'rare_item',
                'product_title' => 'กล่องอาวุธลวงตาจุมพิตอสูร',
                'product_id' => 3056,
                'product_quantity' => 1,
                'icon' => 'item_04_Base',
                'key' => 'item_04',
                'color' => 'red'
            ],
            [
                'id' => 5,
                'item_type' => 'rare_item',
                'product_title' => 'ชุดอาทิตย์อัสดง',
                'product_id' => 3055,
                'product_quantity' => 1,
                'icon' => 'item_05_Base',
                'key' => 'item_05',
                'color' => 'red'
            ],
            [
                'id' => 6,
                'item_type' => 'rare_item',
                'product_title' => 'เซ็ทกุหลาบซันตามาเรีย',
                'product_id' => 3060,
                'product_quantity' => 1,
                'icon' => 'item_06_Base',
                'key' => 'item_06',
                'color' => 'red'
            ],
        ];
    }

}
