<?php

namespace App\Models\black_valentine;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'black_valentine_members';
    protected $fillable = [
        'uid',
        'username',
        'ncid',
        'last_ip'
    ];
}
