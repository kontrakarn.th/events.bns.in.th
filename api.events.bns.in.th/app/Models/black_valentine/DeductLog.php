<?php

namespace App\Models\black_valentine;

use Illuminate\Database\Eloquent\Model;

class DeductLog extends Model
{
    protected $connection = 'events_bns';
    protected $table = 'black_valentine_deduct_logs';
    protected $fillable = [
        'status',
        'diamond',
        'uid',
        'ncid',
        'last_ip',
        'deduct_type',
        'log_date',
        'log_date_timestamp',
        'before_deduct_diamond',
        'after_deduct_diamond',
        'deduct_status',
        'deduct_purchase_id',
        'deduct_purchase_status',
        'deduct_data'
    ];

    protected $dates = [
        'log_date'
    ];
}
