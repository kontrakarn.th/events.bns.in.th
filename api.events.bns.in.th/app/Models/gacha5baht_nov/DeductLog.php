<?php

    namespace App\Models\gacha5baht_nov;

    use Moloquent;

    /**
     * Description of DeductLog
     *
     * @author naruebaetbouhom
     */
    class DeductLog extends Moloquent {

        protected $connection = 'mongodb_bns';
        protected $table = 'gacha5baht_nov_deduct_log';
        protected $fillable = [
            'diamond',
            'uid',
            'ncid',
            'deduct_status',
            'deduct_purchase_id',
            'deduct_purchase_status',
            'deduct_data',
            'before_deduct_diamond',
            'after_deduct_diamond',
            'status',
            'last_ip',
            'deduct_type',
            'log_timestamp'
        ];

    }
