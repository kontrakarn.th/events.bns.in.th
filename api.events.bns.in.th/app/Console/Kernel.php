<?php

namespace App\Console;

use Illuminate\Console\Command;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\MoonstoneFestival::class,
        Commands\BattleRoyale::class,

        // Clan Event
        // Commands\ClanEventCron::class,
        // Commands\ClanEventCronMemberScore::class,
        // Commands\ClanEventClanScore::class,
        // Commands\ClanEventTopRank::class,
        // Commands\ClanEventRecheckDailyQuest::class,
        // Commands\ClanEventRecheckMemberQuest::class,
        // Commands\ClanEventRecheckMemberScore::class,

        Commands\pvp_event\UpdateQuestLogTest::class,
        Commands\pvp_event\UpdateQuestLog::class,
        Commands\pvp_event\UpdateYesterdayQuestLog::class,

        // Snow Ball 2019
        Commands\snowball2019\SnowBall2019UpdateQuestLog::class,

        // Commands\exchange_gift\ImportItem::class,
        Commands\elite\GetUserDiamondLogs::class,
        Commands\elite\SaveLogsReport::class,


        // Piggy
        Commands\piggy\PiggyUpdateQuestLogTest::class,
        Commands\piggy\PiggyUpdateQuestLog::class,

        // Daily Stamp
        Commands\daily_stamp\AddQuestDaily::class,
        Commands\daily_stamp\CheckMemberQuestNotSuccess::class,
        Commands\daily_stamp\CheckMemberQuestSuccess::class,
        Commands\daily_stamp\UpdateQuestLog::class,

        /*  League   */
        //test
        Commands\league\test\CreateDailyQuestLog::class,
        Commands\league\test\CheckDailyQuestLog::class,
        Commands\league\test\CheckSpecialDailyQuestLog::class,
        Commands\league\test\UpdateColorRanking::class,
        Commands\league\test\UpdateGroupRanking::class,
        Commands\league\test\UpdateUnsuccessDailyQuestLog::class,

        // Commands\league\test\RegisterMember::class,

        // live
        Commands\league\live\CreateDailyQuestLog::class,
        Commands\league\live\CheckDailyQuestLog::class,
        Commands\league\live\CheckSpecialDailyQuestLog::class,
        Commands\league\live\UpdateColorRanking::class,
        Commands\league\live\UpdateGroupRanking::class,
        Commands\league\live\UpdateUnsuccessDailyQuestLog::class,

        // Commands\league\live\RandomGroup::class,
        // Commands\league\live\RegisterMember::class,

        /*  League   */

        // CODM
        // Commands\codm\ZombieDaySendItem::class,
        // Commands\codm\bug_event\CheckItem::class,
        // Commands\codm\bug_event\CheckItem2::class,

        // Hardmode
        // Commands\hardmode\live\CreateDailyQuestLog::class,
        // Commands\hardmode\live\CheckDailyQuestLog::class,

        // Hey Duo
        // Commands\hey_duo\CreateDailyQuestLog::class,
        // Commands\hey_duo\CheckBossKill::class,
        // Commands\hey_duo\CheckAreaQuest::class,

        // Hongmoon Pass Season 2
        // Commands\hm_pass_ss2\CreateDailyQuestLog::class,
        // Commands\hm_pass_ss2\CreateWeeklyQuestLog::class,
        // Commands\hm_pass_ss2\CheckDailyQuestLog::class,
        // Commands\hm_pass_ss2\CheckWeeklyQuestLog::class,

        // Two in One
        // Commands\two_in_one\CreateDailyQuestLog::class,
        // Commands\two_in_one\CheckDailyQuestLog::class,

        // buffet v2
        //Commands\buffet_v2\TestCountQuestCompleted::class,
        Commands\buffet_v2\CountQuestCompleted::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        // $schedule->command('bns_save_battle_royale')->dailyAt('17:00'); #1

        // $schedule->command('bns_clan_event 0 500')->dailyAt('12:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCron_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event 500 500')->dailyAt('12:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCron_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event 1000 500')->dailyAt('12:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCron_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event 1500 500')->dailyAt('12:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCron_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event 2000 500')->dailyAt('12:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCron_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event 2500 500')->dailyAt('12:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCron_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event 3000 500')->dailyAt('12:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCron_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event 3500 500')->dailyAt('12:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCron_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event 4000 500')->dailyAt('12:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCron_9_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event 4500 500')->dailyAt('12:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCron_10_' . date('YmdHis') . '.log'));


        // $schedule->command('bns_clan_event_member_score 0 500')->dailyAt('16:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCronMemberScore_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event_member_score 500 500')->dailyAt('16:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCronMemberScore_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event_member_score 1000 500')->dailyAt('16:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCronMemberScore_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event_member_score 1500 500')->dailyAt('16:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCronMemberScore_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event_member_score 2000 500')->dailyAt('16:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCronMemberScore_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event_member_score 2500 500')->dailyAt('16:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCronMemberScore_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event_member_score 3000 500')->dailyAt('16:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCronMemberScore_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event_member_score 3500 500')->dailyAt('16:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCronMemberScore_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event_member_score 4000 500')->dailyAt('16:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCronMemberScore_9_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event_member_score 4500 500')->dailyAt('16:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventCronMemberScore_10_' . date('YmdHis') . '.log'));

        // $schedule->command('bns_clan_event_clan_score')->dailyAt('17:00')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventClanScore_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_clan_event_top_rank')->dailyAt('17:15')->sendOutputTo(storage_path('logs/schedulers/BnsClanEventTopRank_' . date('YmdHis') . '.log'));

        // START BNS PVP EVENT

        // every hour in 59 minutes
        // $schedule->command('bns_pvp_event_quests_update 0 1000')->hourlyAt(59)->sendOutputTo(storage_path('logs/schedulers/pvp_event/BnsPvPEventCron_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_quests_update 1000 1000')->hourlyAt(59)->sendOutputTo(storage_path('logs/schedulers/pvp_event/BnsPvPEventCron_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_quests_update 2000 1000')->hourlyAt(59)->sendOutputTo(storage_path('logs/schedulers/pvp_event/BnsPvPEventCron_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_quests_update 3000 1000')->hourlyAt(59)->sendOutputTo(storage_path('logs/schedulers/pvp_event/BnsPvPEventCron_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_quests_update 4000 1000')->hourlyAt(59)->sendOutputTo(storage_path('logs/schedulers/pvp_event/BnsPvPEventCron_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_quests_update 5000 1000')->hourlyAt(59)->sendOutputTo(storage_path('logs/schedulers/pvp_event/BnsPvPEventCron_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_quests_update 6000 1000')->hourlyAt(59)->sendOutputTo(storage_path('logs/schedulers/pvp_event/BnsPvPEventCron_7_' . date('YmdHis') . '.log'));

        // yesterday log
        // $schedule->command('bns_pvp_event_yesterday_quests_update 0 1000')->dailyAt('00:30')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_yesterday_quests_update 1000 1000')->dailyAt('00:30')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_yesterday_quests_update 2000 1000')->dailyAt('00:30')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_yesterday_quests_update 3000 1000')->dailyAt('00:30')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_yesterday_quests_update 4000 1000')->dailyAt('00:30')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_yesterday_quests_update 5000 1000')->dailyAt('00:30')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_yesterday_quests_update 6000 1000')->dailyAt('00:30')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_7_' . date('YmdHis') . '.log'));

        // $schedule->command('bns_pvp_event_yesterday_quests_update 0 1000')->dailyAt('03:00')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_yesterday_quests_update 1000 1000')->dailyAt('03:00')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_yesterday_quests_update 2000 1000')->dailyAt('03:00')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_yesterday_quests_update 3000 1000')->dailyAt('03:00')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_yesterday_quests_update 4000 1000')->dailyAt('03:00')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_yesterday_quests_update 5000 1000')->dailyAt('03:00')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_pvp_event_yesterday_quests_update 6000 1000')->dailyAt('03:00')->sendOutputTo(storage_path('logs/schedulers/pvp_event/UpdateYesterdayQuestLog_7_' . date('YmdHis') . '.log'));

        // END BNS PVP EVENT

        // Start Snow Ball 2019

        // every 1 hour in 58 minutes
        // $schedule->command('bns_snowball2019_quests_update 0 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 1000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 2000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 3000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 4000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 5000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 6000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 7000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 8000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_9_' . date('YmdHis') . '.log'));

        // daily at 23:30
        // $schedule->command('bns_snowball2019_quests_update 0 1000')->dailyAt('23:30')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 1000 1000')->dailyAt('23:30')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 2000 1000')->dailyAt('23:30')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 3000 1000')->dailyAt('23:30')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 4000 1000')->dailyAt('23:30')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 5000 1000')->dailyAt('23:30')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 6000 1000')->dailyAt('23:30')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 7000 1000')->dailyAt('23:30')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 8000 1000')->dailyAt('23:30')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_9_' . date('YmdHis') . '.log'));

        // daily at 23:45
        // $schedule->command('bns_snowball2019_quests_update 0 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 1000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 2000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 3000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 4000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 5000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 6000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 7000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 8000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_9_' . date('YmdHis') . '.log'));

        // daily at 23:55
        // $schedule->command('bns_snowball2019_quests_update 0 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 1000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 2000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 3000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 4000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 5000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 6000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 7000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_snowball2019_quests_update 8000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/snowball2019/snowball2019_9_' . date('YmdHis') . '.log'));

        //elite

        $schedule->command('bns:elite:get_user_diamond_logs')->monthlyOn(1, '00:01')->sendOutputTo(storage_path('logs/schedulers/elite/get_user_diamond_logs_' . date('YmdHis') . '.log'));

        $schedule->command('bns:elite:save_logs_report')->dailyAt('12:00')->sendOutputTo(storage_path('logs/schedulers/elite/elite_save_log_' . date('YmdHis') . '.log'));

        // End Snow Ball 2019


        // Piggy

        // every 30 minutes
        // $schedule->command('bns_piggy_quests_update 0 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 1000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 2000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 3000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 4000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 5000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 6000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 7000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 8000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_9_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 9000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_10_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 10000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_11_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 11000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_12_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 12000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_13_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 13000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_14_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 14000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_15_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 15000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_16_' . date('YmdHis') . '.log'));

        // daily at 23:45
        // $schedule->command('bns_piggy_quests_update 0 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 1000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 2000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 3000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 4000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 5000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 6000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 7000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 8000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_9_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 9000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_10_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 10000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_11_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 11000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_12_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 12000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_13_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 13000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_14_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 14000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_15_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 15000 1000')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2345_16_' . date('YmdHis') . '.log'));

        // daily at 23:55
        // $schedule->command('bns_piggy_quests_update 0 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 1000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 2000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 3000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 4000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 5000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 6000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 7000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 8000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_9_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 9000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_10_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 10000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_11_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 11000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_12_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 12000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_13_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 13000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_14_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 14000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_15_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_piggy_quests_update 15000 1000')->dailyAt('23:55')->sendOutputTo(storage_path('logs/schedulers/piggy/piggy_2355_16_' . date('YmdHis') . '.log'));

        // Daily Stamp
        // $schedule->command('bns_daily_stamp_add_quest_daily 0 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 1000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 2000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 3000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 4000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 5000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 6000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 7000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 8000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_9_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 9000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_10_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 10000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_11_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 11000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_12_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 12000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_13_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 13000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_14_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 14000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_15_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_add_quest_daily 15000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_add_quest_daily_16_' . date('YmdHis') . '.log'));

        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 0 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 1000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 2000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 3000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 4000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 5000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 6000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 7000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 8000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_9_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 9000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_10_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 10000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_11_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 11000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_12_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 12000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_13_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 13000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_14_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 14000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_15_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_not_success 15000 1000')->dailyAt('00:45')->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_not_success_16_' . date('YmdHis') . '.log'));

        // $schedule->command('bns_daily_stamp_check_member_quest_success 0 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 1000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 2000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 3000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 4000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 5000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 6000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 7000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 8000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_9_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 9000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_10_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 10000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_11_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 11000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_12_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 12000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_13_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 13000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_14_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 14000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_15_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_check_member_quest_success 15000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_check_member_quest_success_16_' . date('YmdHis') . '.log'));

        // $schedule->command('bns_daily_stamp_quest_update 0 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_1_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 1000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_2_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 2000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_3_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 3000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_4_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 4000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_5_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 5000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_6_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 6000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_7_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 7000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_8_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 8000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_9_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 9000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_10_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 10000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_11_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 11000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_12_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 12000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_13_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 13000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_14_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 14000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_15_' . date('YmdHis') . '.log'));
        // $schedule->command('bns_daily_stamp_quest_update 15000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/daily_stamp/daily_stamp_quest_update_16_' . date('YmdHis') . '.log'));

        // End Piggy


        /* Start League */

        // crete daily log
        // $schedule->command('bns_league_create_quest_daily_log_live 0 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_1_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 1000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_2_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 2000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_3_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 3000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_4_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 4000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_5_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 5000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_6_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 6000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_7_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 7000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_8_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 8000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_9_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 9000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_10_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 10000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_11_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 11000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_12_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 12000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_13_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 13000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_14_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 14000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_15_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 15000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_16_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 16000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_17_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 17000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_18_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 18000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_19_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 19000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_20_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_create_quest_daily_log_live 20000 1000')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_create_quest_daily_log_live_21_' . date('YmdH') . '.log'));

        // Daily Quest hourly at 30
        // $schedule->command('bns_league_check_quest_daily_log_live 0 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_1_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 1000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_2_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 2000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_3_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 3000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_4_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 4000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_5_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 5000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_6_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 6000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_7_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 7000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_8_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 8000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_9_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 9000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_10_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 10000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_11_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 11000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_12_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 12000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_13_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 13000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_14_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 14000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_15_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 15000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_16_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 16000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_17_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 17000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_18_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 18000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_19_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 19000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_20_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 20000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_21_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 21000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_22_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 22000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_23_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 23000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_24_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 24000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_25_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 25000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_26_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 26000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_27_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 27000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_28_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 28000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_29_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 29000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_30_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 30000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_31_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 31000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_32_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 32000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_33_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 33000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_34_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 34000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_35_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 35000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_36_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 36000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_37_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 37000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_38_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 38000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_39_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 39000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_40_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 40000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_41_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 41000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_42_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 42000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_43_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 43000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_44_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 44000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_45_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 45000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_46_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 46000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_47_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 47000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_48_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 48000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_49_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 49000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_50_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 50000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_51_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 51000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_52_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 52000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_53_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 53000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_54_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 54000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_55_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 55000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_56_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 56000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_57_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 57000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_58_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 58000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_59_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 59000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_60_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 60000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_61_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 61000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_62_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 62000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_63_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 63000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_64_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 64000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_65_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 65000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_66_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 66000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_67_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 67000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_68_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 68000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_69_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 69000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_70_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 70000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_71_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 71000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_72_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 72000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_73_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 73000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_74_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 74000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_75_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 75000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_76_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 76000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_77_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 77000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_78_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 78000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_79_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 79000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_80_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 80000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_81_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 81000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_82_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 82000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_83_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 83000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_84_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 84000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_85_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 85000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_86_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 86000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_87_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 87000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_88_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 88000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_89_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 89000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_90_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 90000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_91_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 91000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_92_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 92000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_93_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 93000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_94_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 94000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_95_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 95000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_96_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 96000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_97_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 97000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_98_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 98000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_99_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 99000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_100_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 100000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_101_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 110000 1000')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_102_' . date('YmdH') . '.log'));

        // Daily Quest hourly at 35
        // $schedule->command('bns_league_check_quest_daily_log_live 0 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_1_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 5000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_2_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 10000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_3_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 15000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_4_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 20000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_5_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 25000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_6_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 30000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_7_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 35000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_8_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 40000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_9_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 45000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_10_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 50000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_11_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 55000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_12_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 60000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_13_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 65000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_14_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 70000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_15_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 75000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_16_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 80000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_17_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 85000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_18_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 90000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_19_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 95000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_20_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 100000 5000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_21_' . date('YmdH') . '.log'));

        // Daily Quest hourly at 55
        // $schedule->command('bns_league_check_quest_daily_log_live 0 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_1_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 1000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_2_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 2000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_3_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 3000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_4_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 4000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_5_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 5000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_6_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 6000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_7_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 7000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_8_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 8000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_9_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 9000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_10_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 10000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_11_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 11000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_12_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 12000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_13_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 13000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_14_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 14000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_15_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 15000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_16_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 16000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_17_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 17000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_18_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 18000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_19_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 19000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_20_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 20000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_21_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 21000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_22_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 22000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_23_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 23000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_24_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 24000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_25_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 25000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_26_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 26000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_27_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 27000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_28_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 28000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_29_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 29000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_30_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 30000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_31_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 31000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_32_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 32000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_33_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 33000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_34_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 34000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_35_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 35000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_36_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 36000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_37_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 37000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_38_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 38000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_39_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 39000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_40_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 40000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_41_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 41000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_42_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 42000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_43_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 43000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_44_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 44000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_45_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 45000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_46_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 46000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_47_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 47000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_48_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 48000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_49_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 49000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_50_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 50000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_51_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 51000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_52_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 52000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_53_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 53000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_54_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 54000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_55_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 55000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_56_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 56000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_57_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 57000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_58_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 58000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_59_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 59000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_60_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 60000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_61_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 61000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_62_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 62000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_63_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 63000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_64_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 64000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_65_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 65000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_66_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 66000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_67_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 67000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_68_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 68000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_69_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 69000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_70_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 70000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_71_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 71000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_72_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 72000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_73_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 73000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_74_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 74000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_75_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 75000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_76_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 76000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_77_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 77000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_78_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 78000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_79_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 79000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_80_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 80000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_81_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 81000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_82_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 82000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_83_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 83000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_84_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 84000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_85_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 85000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_86_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 86000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_87_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 87000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_88_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 88000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_89_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 89000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_90_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 90000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_91_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 91000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_92_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 92000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_93_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 93000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_94_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 94000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_95_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 95000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_96_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 96000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_97_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 97000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_98_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 98000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_99_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 99000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_100_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 100000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_101_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_quest_daily_log_live 110000 1000')->hourlyAt(50)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_quest_daily_log_live_102_' . date('YmdH') . '.log'));

        // Special Daily Quest hourly at 15
        // $schedule->command('bns_league_check_special_quest_daily_log_live 0 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_1_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 1000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_2_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 2000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_3_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 3000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_4_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 4000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_5_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 5000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_6_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 6000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_7_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 7000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_8_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 8000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_9_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 9000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_10_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 10000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_11_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 11000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_12_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 12000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_13_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 13000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_14_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 14000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_15_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 15000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_16_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 16000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_17_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 17000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_18_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 18000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_19_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 19000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_20_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 20000 1000')->hourlyAt(15)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_21_' . date('YmdH') . '.log'));

        // Special Daily Quest hourly at 35
        // $schedule->command('bns_league_check_special_quest_daily_log_live 0 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_1_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 1000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_2_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 2000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_3_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 3000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_4_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 4000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_5_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 5000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_6_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 6000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_7_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 7000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_8_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 8000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_9_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 9000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_10_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 10000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_11_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 11000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_12_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 12000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_13_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 13000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_14_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 14000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_15_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 15000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_16_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 16000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_17_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 17000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_18_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 18000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_19_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 19000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_20_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 20000 1000')->hourlyAt(35)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_21_' . date('YmdH') . '.log'));

        // Special Daily Quest hourly at 55
        // $schedule->command('bns_league_check_special_quest_daily_log_live 0 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_1_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 1000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_2_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 2000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_3_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 3000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_4_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 4000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_5_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 5000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_6_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 6000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_7_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 7000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_8_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 8000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_9_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 9000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_10_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 10000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_11_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 11000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_12_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 12000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_13_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 13000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_14_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 14000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_15_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 15000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_16_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 16000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_17_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 17000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_18_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 18000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_19_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 19000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_20_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_check_special_quest_daily_log_live 20000 1000')->hourlyAt(55)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_check_special_quest_daily_log_live_21_' . date('YmdH') . '.log'));

        // // Update Daily Quest not Success at 01:00
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 0 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_1_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 1000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_2_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 2000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_3_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 3000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_4_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 4000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_5_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 5000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_6_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 6000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_7_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 7000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_8_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 8000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_9_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 9000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_10_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 10000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_11_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 11000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_12_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 12000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_13_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 13000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_14_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 14000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_15_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 15000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_16_' . date('YmdH') . '.log'));
        // $schedule->command('bns_league_update_unsuccess_quest_daily_log_live 16000 1000')->dailyAt('01:00')->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_unsuccess_quest_daily_log_live_17_' . date('YmdH') . '.log'));

        // update group score and ranking
        // $schedule->command('bns_league_update_group_ranking_live')->everyTenMinutes()->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_group_ranking_live_' . date('YmdH') . '.log'));

        // update color score and ranking
        // $schedule->command('bns_league_update_color_ranking_live')->hourlyAt(59)->sendOutputTo(storage_path('logs/schedulers/league/bns_league_update_color_ranking_live_' . date('YmdH') . '.log'));

        /* End League */

        //elite
        $schedule->command('bns:elite:save_logs_report')->dailyAt('00:05')->sendOutputTo(storage_path('logs/schedulers/elite/elite_save_log_' . date('YmdHis') . '.log'));

        // BNS Battle Field
        $schedule->command('bns_battle_field_quest_update 0 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/songkran2020/bns_battle_field_quest_update_1_' . date('YmdHis') . '.log'));
        $schedule->command('bns_battle_field_quest_update 1000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/songkran2020/bns_battle_field_quest_update_2_' . date('YmdHis') . '.log'));
        $schedule->command('bns_battle_field_quest_update 2000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/songkran2020/bns_battle_field_quest_update_3_' . date('YmdHis') . '.log'));
        $schedule->command('bns_battle_field_quest_update 3000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/songkran2020/bns_battle_field_quest_update_4_' . date('YmdHis') . '.log'));
        $schedule->command('bns_battle_field_quest_update 4000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/songkran2020/bns_battle_field_quest_update_5_' . date('YmdHis') . '.log'));
        $schedule->command('bns_battle_field_quest_update 5000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/songkran2020/bns_battle_field_quest_update_6_' . date('YmdHis') . '.log'));
        $schedule->command('bns_battle_field_quest_update 6000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/songkran2020/bns_battle_field_quest_update_7_' . date('YmdHis') . '.log'));
        $schedule->command('bns_battle_field_quest_update 7000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/songkran2020/bns_battle_field_quest_update_8_' . date('YmdHis') . '.log'));
        $schedule->command('bns_battle_field_quest_update 8000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/songkran2020/bns_battle_field_quest_update_9_' . date('YmdHis') . '.log'));
        $schedule->command('bns_battle_field_quest_update 9000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/songkran2020/bns_battle_field_quest_update_10_' . date('YmdHis') . '.log'));
        $schedule->command('bns_battle_field_quest_update 10000 1000')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/songkran2020/bns_battle_field_quest_update_11_' . date('YmdHis') . '.log'));

        // Hardmode
        // $schedule->command('bns_hardmode_create_quest_daily_log_live')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/hardmode/bns_hardmode_create_quest_daily_log_live' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hardmode_create_quest_daily_log_live')->everyFifteenMinutes()->sendOutputTo(storage_path('logs/schedulers/hardmode/bns_hardmode_create_quest_daily_log_live' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hardmode_check_quest_daily_log_live')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/hardmode/bns_hardmode_check_quest_daily_log_live' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hardmode_check_quest_daily_log_live')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/hardmode/bns_hardmode_check_quest_daily_log_live' . date('YmdHis') . '.log'));

        // Hey Duo
        // $schedule->command('bns_hey_duo_create_quest_daily_log')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/hey_duo/bns_hey_duo_create_quest_daily_log' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hey_duo_create_quest_daily_log')->everyFifteenMinutes()->sendOutputTo(storage_path('logs/schedulers/hey_duo/bns_hey_duo_create_quest_daily_log' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hey_duo_check_boss_kill')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/hey_duo/bns_hey_duo_check_boss_kill' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hey_duo_check_area_quest')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/hey_duo/bns_hey_duo_check_area_quest' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hey_duo_check_boss_kill')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/hey_duo/bns_hey_duo_check_boss_kill' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hey_duo_check_area_quest')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/hey_duo/bns_hey_duo_check_area_quest' . date('YmdHis') . '.log'));

        // Hongmoon Pass Season 2
        // $schedule->command('bns_hm_pass_ss2_create_daily_quest_log')->dailyAt('00:00')->sendOutputTo(storage_path('logs/schedulers/hm_pass_ss2/bns_hm_pass_ss2_create_daily_quest_log' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hm_pass_ss2_check_daily_quest_log')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/hm_pass_ss2/bns_hm_pass_ss2_check_daily_quest_log' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hm_pass_ss2_check_daily_quest_log')->dailyAt('23:45')->sendOutputTo(storage_path('logs/schedulers/hm_pass_ss2/bns_hm_pass_ss2_check_daily_quest_log' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hm_pass_ss2_create_weekly_quest_log')->weeklyOn(3, '06:00')->sendOutputTo(storage_path('logs/schedulers/hm_pass_ss2/bns_hm_pass_ss2_create_weekly_quest_log' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hm_pass_ss2_check_weekly_quest_log')->everyThirtyMinutes()->sendOutputTo(storage_path('logs/schedulers/hm_pass_ss2/bns_hm_pass_ss2_check_weekly_quest_log' . date('YmdHis') . '.log'));
        // $schedule->command('bns_hm_pass_ss2_check_weekly_quest_log')->weeklyOn(3, '05:45')->sendOutputTo(storage_path('logs/schedulers/hm_pass_ss2/bns_hm_pass_ss2_check_weekly_quest_log' . date('YmdHis') . '.log'));

        // Two in One
        // $schedule->command('bns_two_in_one_create_daily_quest_log')->dailyAt('06:00')->sendOutputTo(storage_path('logs/schedulers/two_in_one/bns_two_in_one_create_daily_quest_log' . date('YmdHis') . '.log'));
        // $schedule->command('bns_two_in_one_check_daily_quest_log')->hourlyAt(1)->sendOutputTo(storage_path('logs/schedulers/two_in_one/bns_two_in_one_check_daily_quest_log' . date('YmdHis') . '.log'));
        // $schedule->command('bns_two_in_one_check_daily_quest_log')->hourlyAt(30)->sendOutputTo(storage_path('logs/schedulers/two_in_one/bns_two_in_one_check_daily_quest_log' . date('YmdHis') . '.log'));
        // $schedule->command('bns_two_in_one_check_daily_quest_log')->dailyAt('05:45')->sendOutputTo(storage_path('logs/schedulers/two_in_one/bns_two_in_one_check_daily_quest_log' . date('YmdHis') . '.log'));

        // buffet
        // for test
        $buffet_command   = 'bns_test_buffet_v2_count_quest_completed';
        $schedule->command(''.$buffet_command.' 0 1000')
            ->everyMinute()
            ->withoutOverlapping() // default is 1440 min = 1 day
            ->sendOutputTo(storage_path('logs/schedulers/buffet_v2/'.$buffet_command.'' . date('YmdHis') . '.log'));
        // for live
        $buffet_command     = 'bns_buffet_v2_count_quest_completed';
        $schedule->command(''.$buffet_command.' 0 1000')
            ->everyMinute()
            ->withoutOverlapping() // default is 1440 min = 1 day
            ->sendOutputTo(storage_path('logs/schedulers/buffet_v2/'.$buffet_command.'' . date('YmdHis') . '.log'));
        $schedule->command(''.$buffet_command.' 1000 1000')
            ->everyMinute()
            ->withoutOverlapping() // default is 1440 min = 1 day
            ->sendOutputTo(storage_path('logs/schedulers/buffet_v2/'.$buffet_command.'' . date('YmdHis') . '.log'));
        $schedule->command(''.$buffet_command.' 2000 1000')
            ->everyMinute()
            ->withoutOverlapping() // default is 1440 min = 1 day
            ->sendOutputTo(storage_path('logs/schedulers/buffet_v2/'.$buffet_command.'' . date('YmdHis') . '.log'));
        $schedule->command(''.$buffet_command.' 3000 1000')
            ->everyMinute()
            ->withoutOverlapping() // default is 1440 min = 1 day
            ->sendOutputTo(storage_path('logs/schedulers/buffet_v2/'.$buffet_command.'' . date('YmdHis') . '.log'));
        $schedule->command(''.$buffet_command.' 4000 1000')
            ->everyMinute()
            ->withoutOverlapping() // default is 1440 min = 1 day
            ->sendOutputTo(storage_path('logs/schedulers/buffet_v2/'.$buffet_command.'' . date('YmdHis') . '.log'));
        $schedule->command(''.$buffet_command.' 5000 1000')
            ->everyMinute()
            ->withoutOverlapping() // default is 1440 min = 1 day
            ->sendOutputTo(storage_path('logs/schedulers/buffet_v2/'.$buffet_command.'' . date('YmdHis') . '.log'));
        $schedule->command(''.$buffet_command.' 6000 1000')
            ->everyMinute()
            ->withoutOverlapping() // default is 1440 min = 1 day
            ->sendOutputTo(storage_path('logs/schedulers/buffet_v2/'.$buffet_command.'' . date('YmdHis') . '.log'));
        $schedule->command(''.$buffet_command.' 7000 1000')
            ->everyMinute()
            ->withoutOverlapping() // default is 1440 min = 1 day
            ->sendOutputTo(storage_path('logs/schedulers/buffet_v2/'.$buffet_command.'' . date('YmdHis') . '.log'));
        $schedule->command(''.$buffet_command.' 8000 1000')
            ->everyMinute()
            ->withoutOverlapping() // default is 1440 min = 1 day
            ->sendOutputTo(storage_path('logs/schedulers/buffet_v2/'.$buffet_command.'' . date('YmdHis') . '.log'));
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
