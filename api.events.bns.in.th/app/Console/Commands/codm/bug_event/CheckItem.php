<?php

namespace App\Console\Commands\codm\bug_event;

use Illuminate\Console\Command;
use DB;
use Config;
use Carbon\Carbon;

use App\Models\codm\bug_event\BugEventList;

class CheckItem extends Command
{
    private $clientId = '6C74C47C-3EDC-45A9-BD92-66B5343B8850';

    private $areaId = 1;
    private $tips = "tha";

    private $eventName = 'test_api';
    private $apiUrl = "https://th.game.proxy.garenanow.com/game/codm/tidy/v1/inventory/";


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'codm_bug_event_check_item';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CODM bug event check item';

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        // $data = BugEventList::where("gid","=",null)->get();
        // dd(count($data));
        
        $users = BugEventList::where("gid","=",null)->get();
        // dd(count($users));

        if(count($users) > 0){
            foreach($users as $key=>$user){

                $this->info("--------------------------------------------------------------");
                $this->info("ID: ".$user->id);
                $this->info("Tencent ID: ".$user->tencent_id);
                $this->info("Item ID: ".$user->item_id);

                $keyList = null;
                $inventory = [];
                $inventory = $this->getInventory($user->tencent_id);
                // dd($inventory);
                $gid = 0;
                if(count($inventory) > 0){
                    foreach($inventory as $invent){
                        if($invent->ItemId == $user->item_id){
                            $gid = $invent->Gid;
                            break;
                        }
                    }
                }

                // dd($gid);
                BugEventList::where("id", $user->id)->where("tencent_id", $user->tencent_id)->update([
                    'gid' => $gid,
                ]);

                // $keyList = array_search($user->item_id, array_column($inventory, 'ItemId'));

                // $itemSelect = [];
                // $itemSelect = $inventory[$keyList];
                // dd($itemSelect);
                $this->info("GID: ".$gid);

                $this->info("--------------------------------------------------------------");

            }
        }

    }

    private function getInventory($tencentId=0){

        if($tencentId == 0){
            return [];
        }

        $headers = [
            'Client-ID: '.$this->clientId,
            'Content-Type: application/json'
        ];

        // dd($this->apiUrl.$tencentId.'?area='.$this->areaId);

        $response = $this->getApi($this->apiUrl.$tencentId.'?area='.$this->areaId,$headers);
        $resp=json_decode($response);

        return isset($resp->List) && count($resp->List) > 0 ? $resp->List : [];
    }

    private function getApi($url, $headers = array())
    {
        // if (is_array($params)) {
        //     $data = http_build_query($params);
        // } else {
        //     $data = $params;
        // }
        // if (empty($data) === FALSE) {
        //     $url = $url . '?' . $data;
        // }
        // dd($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === FALSE) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        // dd($ch);
        $resp = curl_exec($ch);
        curl_close($ch);
        return $resp;
    }

}