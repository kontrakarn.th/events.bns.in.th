<?php

namespace App\Console\Commands\codm;

use Illuminate\Console\Command;
use DB;
use Config;
use Carbon\Carbon;

use App\Models\codm\zombieday\ReSendItem;
// use App\Models\codm\Test\zombieday\ReSendItem;

use App\Jobs\codm\ZombieDaySendItemQueue as QUEUE;

class ZombieDaySendItem extends Command
{
    private $apiHost    = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'codm_zombieday_resend_item';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CODM Zombie Day re-send item';


    private $QUEUE_SENDITEM = 'codm_zombieday_resend_item_queue';
    // private $QUEUE_SENDITEM = 'codm_zombieday_resend_item_queue_test';

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        // ReSendItem::where('status', 'pending')
        ReSendItem::where('status', 'fail')
                    ->orderBy('id', 'ASC')
                    ->chunkById(5000, function ($items) {

                        if(count($items) > 0){
                            foreach($items as $key=>$item){

                                $this->info(($key+1).'. Send Item for TCID :'.$item->tencent_id.', Rewards: '.$item->item_name.', Record Id: '.$item->id);

                                // update status
                                ReSendItem::where('id', $item->id)->update([
                                    'status' => 'queue'
                                ]);

                                QUEUE::dispatch(
                                    $item->id
                                )->onQueue($this->QUEUE_SENDITEM);
                                
                            }
                        }

                    });

    }

}