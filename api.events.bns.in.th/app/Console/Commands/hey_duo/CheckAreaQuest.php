<?php

namespace App\Console\Commands\hey_duo;

use Illuminate\Console\Command;

use App\Models\hey_duo\Setting;
use App\Models\hey_duo\QuestDailyLog;
use App\Jobs\hey_duo\CheckQuestDailyLogQueue;

class CheckAreaQuest extends Command
{
    protected $description = 'BNS hey duo event';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_hey_duo_check_area_quest {--date=}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $date = $this->option('date');
        $logDate = $date ? $date : date('Y-m-d');

        QuestDailyLog::where('uid', '!=', 0)
            ->where('char_id', '!=', 0)
            ->whereNotNull('quest_code')
            ->where('quest_type', 'daily_area')
            ->where('status', 'pending')
            ->where('log_date', $logDate)
            ->chunkById(1000, function ($dailyLogs) use ($logDate) {
                foreach ($dailyLogs as $dailyLog) {
                    $this->info("BNS Hey Duo : Check Area Quest UID: " . $dailyLog->uid . " , Username : " . $dailyLog->username . " , Quest Code : " . $dailyLog->quest_code . " , Quest Title : " . $dailyLog->quest_title);

                    CheckQuestDailyLogQueue::dispatch($dailyLog, $logDate)->onQueue('bns_hey_duo_check_area_quest_queue');
                }
            });
    }
}
