<?php

namespace App\Console\Commands\hey_duo;

use Illuminate\Console\Command;

use App\Models\hey_duo\Setting;
use App\Models\hey_duo\Member;
use App\Jobs\hey_duo\CreateQuestDailyQueue;

class CreateDailyQuestLog extends Command
{
    protected $description = 'BNS hey duo event';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_hey_duo_create_quest_daily_log {--date=}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $date = $this->option('date');
        $logDate = $date ? $date : date('Y-m-d');

        Member::where('uid', '!=', 0)
            ->where('char_id', '!=', 0)
            ->whereNotNull('duo_uid')
            ->chunk(1000, function ($members) use ($logDate) {
                foreach ($members as $member) {
                    $this->info("BNS Hey Duo : Create Daily Quest Log UID: " . $member->uid . " , Username : " . $member->username . " , Character : " . $member->char_name);

                    CreateQuestDailyQueue::dispatch($member, $logDate)->onQueue('bns_hey_duo_create_quest_daily_log_queue');
                }
            });
    }
}
