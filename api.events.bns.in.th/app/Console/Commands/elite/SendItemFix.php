<?php

namespace App\Console\Commands\elite;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Storage;
use Cache;
use DB;

use App\Models\hmelite_2019\Events;
use App\Models\hmelite_2019\Member;
use App\Models\hmelite_2019\DiamondLogs;
use App\Models\hmelite_2019\Ranking;
use App\Models\hmelite_2019\ItemSelect;
use App\Models\hmelite_2019\SendLogs;

class SendItemFix extends Command
{
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns:elite:send_item_fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send item fix';

    /**
     * Query start end
     *
     * @var string
     */
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->sendFix();
    }

    private function sendFix(){
        // dd($data_raw);
        $getSend=SendLogs::where('status','!=','success')->get();
        if(isset($getSend)){
            foreach ($getSend as $key => $sendItemLog) {
                // code...
                // dd($sendItemLog);
                $goods_data=json_decode($sendItemLog->goods_data);
                // dd($goods_data);
                $send_result_raw = $this->dosendItem($sendItemLog->ncid, $goods_data);
                //
                $send_result = json_decode($send_result_raw);
                $sendlog_id=$sendItemLog->id;
                $sendup=SendLogs::find($sendlog_id);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                    $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
                    $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
                    $sendup->status='success';
                    $sendup->save();

                    $this->info('send_success');
                }else{
                    $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
                    $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
                    $sendup->status='fail';
                    $sendup->save();

                    $this->error('send_fail');
                }

                $this->info('send no.'.$key);
            }
        }else{
            $this->error("no item");
        }

        $this->info('send_finish');
    }

    private function doGetDiamond($ncid,$start,$stop) {
        if(empty($ncid)){
            return false;
        }

        return json_decode($this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'query_cash_txn',
                    'user_id' => $ncid,
                    'start_time' => $start,
                    'end_time' => $stop
                ]));
    }

    private function dosendItem(string $ncid, array $goodsData){
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'sending item from garena events bns elite 2019 ranking.',
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }


    /**
     * Simple POST request
     */
    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === FALSE) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === FALSE) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if(curl_errno($ch)){
            return null;
        }

        curl_close($ch);
        return $resp;
    }

}
