<?php

namespace App\Console\Commands\elite;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Storage;
use Cache;
use DB;

use App\Models\hmelite_2019\Events;
use App\Models\hmelite_2019\Member;
use App\Models\hmelite_2019\DiamondLogs;
use App\Models\hmelite_2019\Ranking;
use App\Models\hmelite_2019\ItemSelect;
use App\Models\hmelite_2019\SendLogs;

class GetUserDiamondLogs extends Command
{
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns:elite:get_user_diamond_logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get_user_diamond_logs';

    /**
     * Query start end
     *
     * @var string
     */
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // dd(Carbon::now()->addMonth(-1));
        $pre_date=Carbon::now()->startOfMonth()->subMonth();
        $start_date=$pre_date->toDateTimeString();
        $end_date=$pre_date->endofMonth()->toDateTimeString();
        // dd($start_date,$end_date);
        //get diamond log last month
        $this->reports($pre_date,$start_date,$end_date);

        //cal ranking
        $this->CalRanking($pre_date,$start_date,$end_date);

        //send_item ranking
        $this->sendItemRanking($pre_date);
        $this->sendItemVip3($pre_date);
    }

    private function reports($pre_date,$start,$end){

        // get all member info from package
        $num_member=Member::count();
        // dd($num_member);
        $datewhere=$pre_date->toDateString();
        $chk_diamond_logs=DiamondLogs::whereDate('ranking_at',$datewhere)->first();
        if(!isset($chk_diamond_logs)){
            if($num_member > 0){
                $events=Events::getEventByID(1);

                DB::connection('events_bns')->table('hmelite_2019_member')->update(['vip'=>0]);

                for($i=0;$i<$num_member;$i++){
                // foreach($memberFromReward as $member){
                    $member=Member::limit(1)->offset($i)->first();
                    $this->info($member->username);
                    $result_diamond = 0;
                    $last_redeem=0;
                    $result = null;
                    $result = $this->doGetDiamond($member->ncid,$start,$end);
                    if ($result->status == true) {
                        $content = [];
                        $content = $result->response;
                        $collection = [];
                        $collection = collect($content);
                        if (count($content) > 0) {
                            $result_diamond = $collection->sum('diamond');
                            $last_redeem=$collection->max('timestamp');
                        }
                    }
                    //
                    $chk_logs=DiamondLogs::where('uid',$member->uid)->whereDate('ranking_at',$datewhere)->first();
                    if(!isset($chk_logs)){
                        if($result_diamond>=$events->vip_3_diamond){
                            $vip=3;
                        }elseif($result_diamond>=$events->vip_2_diamond){
                            $vip=2;
                        }elseif($result_diamond>=$events->vip_1_diamond){
                            $vip=1;
                        }else{
                            $vip=0;
                        }
                        $diamond_logs = new DiamondLogs;
                        $diamond_logs->uid=$member->uid;
                        $diamond_logs->ncid=$member->ncid;
                        $diamond_logs->username=$member->username;
                        $diamond_logs->nickname=$member->nickname;
                        $diamond_logs->diamond=$result_diamond;
                        $diamond_logs->last_redeem=$last_redeem;
                        $diamond_logs->vip=$vip;
                        $diamond_logs->ranking_at=$pre_date->toDateTimeString();
                        $diamond_logs->save();

                        $member->vip=$vip;
                        $member->save();
                    }

                        echo "\n";
                        echo $i;
                        echo "\n";
                        echo "\n";
                        echo "----------------------------------------------";
                        echo "\n";
                        echo "UID : ".$member->uid;
                        echo "\n";
                        echo "Username : ".$member->username;
                        echo "\n";
                        echo "Total Diamonds : ".$result_diamond;
                        echo "\n";
                        echo "----------------------------------------------";
                        echo "\n";

                }

            }
        }
    }

    private function CalRanking($pre_date){
        $chk_ranking = Ranking::whereDate('ranking_at',$pre_date->toDateString())->first();
        if(!isset($chk_ranking)){
            $get_top_ten = DiamondLogs::whereDate('ranking_at',$pre_date->toDateString())->orderBy('diamond','DESC')->orderBy('last_redeem','DESC')->limit(10)->get();
            // dd($get_top_ten);
            foreach($get_top_ten as $key=>$value){
                $new_rank = new Ranking;
                $new_rank->uid = $value->uid;
                $new_rank->username = $value->username;
                $new_rank->ncid = $value->ncid;
                $new_rank->nickname = $value->nickname;
                $new_rank->no = $key+1;
                $new_rank->vip = $value->vip;
                $new_rank->ranking_at = $pre_date->toDateString();
                $new_rank->diamond = $value->diamond;
                $new_rank->last_redeem = $value->last_redeem;
                $new_rank->save();
                $this->info('ranking '.$key);
            }
        }

        $this->info('Ranking Success');
    }

    private function sendItemRanking($pre_date){
        $chk_ranking = Ranking::whereDate('ranking_at',$pre_date->toDateString())->where('vip',3)->first();
        $year=(int) $pre_date->format('Y');
        $month=(int) $pre_date->format('m');

        if(isset($chk_ranking)){
            $total_ranking=Ranking::whereDate('ranking_at',$pre_date->toDateString())->where('vip',3)->orderBy('no')->get();
            // dd($total_ranking);
            foreach($total_ranking as $key=>$value){
                if($value->vip==3){


                    $item_type="";
                    if($value->no>3 && $value->no<=10){
                        $item_type="ranking_10";
                    }elseif($value->no>0 && $value->no<=3){
                        $item_type="ranking_3";
                    }


                    if($item_type!=""){
                        $data_raw=ItemSelect::getItemSelectByTypeForRanking($item_type,$month,$year);
                        // dd($data_raw);
                        if(count($data_raw)>0){
                            $packageInfo=$data_raw[0];

                            $goods_data = []; //good data for send item group
                            $packageId = $packageInfo->product_id;
                            $packageTitle = $packageInfo->product_title;
                            $packageAmt = $packageInfo->amt;

                            $chk_get_item=SendLogs::where('uid',$value->uid)->where('item_type',$item_type)->where('item_date',$packageInfo->item_date)->first();
                            if(!isset($chk_get_item)){
                                $goods_data[] = [
                                   'goods_id' => $packageId,
                                   'purchase_quantity' => $packageAmt,
                                   'purchase_amount' => 0,
                                   'category_id' => 40
                                ];
                                // dd($packageInfo,$goods_data);

                                $sendItemLog= new SendLogs;
                                $sendItemLog->uid=$value->uid;
                                $sendItemLog->ncid=$value->ncid;
                                $sendItemLog->nickname=$value->nickname;
                                $sendItemLog->item_type=$item_type;
                                $sendItemLog->item_no=$packageInfo->id;
                                $sendItemLog->product_title=$packageTitle;
                                $sendItemLog->product_id=$packageId;
                                $sendItemLog->product_amt=$packageAmt;
                                $sendItemLog->icon=$packageInfo->icon;
                                $sendItemLog->item_date=$packageInfo->item_date;
                                $sendItemLog->status='pending';
                                $sendItemLog->send_item_purchase_id=0;
                                $sendItemLog->send_item_purchase_status=0;
                                $sendItemLog->send_item_purchase_status=0;
                                $sendItemLog->goods_data=json_encode($goods_data);
                                // dd($sendItemLog);
                                if(!$sendItemLog->save()){
                                    return response()->json([
                                                'status' => true,
                                                'message' => "เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (2)",
                                            ]);
                                }

                                $send_result_raw = $this->dosendItem($value->ncid, $goods_data);

                                $send_result = json_decode($send_result_raw);
                                $sendlog_id=$sendItemLog->id;
                                $sendup=SendLogs::find($sendlog_id);
                                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                                    $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
                                    $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
                                    $sendup->status='success';
                                    $sendup->save();

                                    $this->info('send_success');
                                }else{
                                    $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
                                    $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
                                    $sendup->status='fail';
                                    $sendup->save();

                                    $this->error('send_fail');
                                }
                            }
                        }else{
                            $this->error("no item");
                        }

                    }
                }
                $this->info("send_item : ".($key+1));
            }
        }
        $this->info('send_finish');
    }

    private function sendItemVip3($pre_date){

        $chk_member_vip3=Member::where('vip',3)->first();
        // dd($chk_member_vip3);
        $year=(int) $pre_date->format('Y');
        $month=(int) $pre_date->format('m');

        if(isset($chk_member_vip3)){
            $member_vip3=Member::where('vip',3)->get();
            // dd(count($member_vip3));
            foreach($member_vip3 as $key=>$value){
                if($value->vip==3){


                    // $chkGetItem=SendLogs::where('item_type','vip_3')->first();
                    // if(!isset($chkGetItem)){
                        $item_type="vip_3";
                        $data_raw=ItemSelect::getItemSelectByTypeForRanking($item_type,$month,$year);
                        // dd($data_raw);
                        if(count($data_raw)>0){
                            $packageInfo=$data_raw[0];

                            $goods_data = []; //good data for send item group
                            $packageId = $packageInfo->product_id;
                            $packageTitle = $packageInfo->product_title;
                            $packageAmt = $packageInfo->amt;

                            $chk_get_item=SendLogs::where('uid',$value->uid)->where('item_type',$item_type)->where('item_date',$packageInfo->item_date)->first();
                            if(!isset($chk_get_item)){
                                $goods_data[] = [
                                   'goods_id' => $packageId,
                                   'purchase_quantity' => $packageAmt,
                                   'purchase_amount' => 0,
                                   'category_id' => 40
                                ];
                                // dd($packageInfo,$goods_data);

                                $sendItemLog= new SendLogs;
                                $sendItemLog->uid=$value->uid;
                                $sendItemLog->ncid=$value->ncid;
                                $sendItemLog->nickname=$value->nickname;
                                $sendItemLog->item_type=$item_type;
                                $sendItemLog->item_no=$packageInfo->id;
                                $sendItemLog->product_title=$packageTitle;
                                $sendItemLog->product_id=$packageId;
                                $sendItemLog->product_amt=$packageAmt;
                                $sendItemLog->icon=$packageInfo->icon;
                                $sendItemLog->item_date=$packageInfo->item_date;
                                $sendItemLog->status='pending';
                                $sendItemLog->send_item_purchase_id=0;
                                $sendItemLog->send_item_purchase_status=0;
                                $sendItemLog->send_item_purchase_status=0;
                                $sendItemLog->goods_data=json_encode($goods_data);
                                // dd($sendItemLog);
                                if(!$sendItemLog->save()){
                                    return response()->json([
                                                'status' => true,
                                                'message' => "เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (2)",
                                            ]);
                                }

                                $send_result_raw = $this->dosendItem($value->ncid, $goods_data);

                                $send_result = json_decode($send_result_raw);
                                $sendlog_id=$sendItemLog->id;
                                $sendup=SendLogs::find($sendlog_id);
                                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                                    $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
                                    $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
                                    $sendup->status='success';
                                    $sendup->save();

                                    $this->info('send_success');
                                }else{
                                    $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
                                    $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
                                    $sendup->status='fail';
                                    $sendup->save();

                                    $this->error('send_fail');
                                }
                            }
                        }else{
                            $this->error("no item");
                        }

                    // }
                }
                $this->info("send_item : ".($key+1));
            }
        }
        $this->info('send_finish');
    }

    private function doGetDiamond($ncid,$start,$stop) {
        if(empty($ncid)){
            return false;
        }

        return json_decode($this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'query_cash_txn',
                    'user_id' => $ncid,
                    'start_time' => $start,
                    'end_time' => $stop
                ]));
    }

    private function dosendItem(string $ncid, array $goodsData){
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'sending item from garena events bns elite 2019 ranking.',
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }


    /**
     * Simple POST request
     */
    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === FALSE) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === FALSE) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if(curl_errno($ch)){
            return null;
        }

        curl_close($ch);
        return $resp;
    }

}
