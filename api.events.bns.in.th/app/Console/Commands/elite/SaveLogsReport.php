<?php

namespace App\Console\Commands\elite;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Storage;
use Cache;
use DB;

use App\Models\hmelite_2019\Events;
use App\Models\hmelite_2019\Member;
use App\Models\hmelite_2019\SaveLogs;
use App\Models\hmelite_2019\Ranking;
use App\Models\hmelite_2019\ItemSelect;
use App\Models\hmelite_2019\SendLogs;

class SaveLogsReport extends Command
{
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns:elite:save_logs_report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'save_logs_report';

    /**
     * Query start end
     *
     * @var string
     */
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // dd(Carbon::now()->addMonth(-1));
        $pre_date=Carbon::now()->startOfMonth();
        // $pre_date=Carbon::now()->startOfMonth()->subMonth();
        $start_date=$pre_date->toDateTimeString();
        $end_date=$pre_date->endofMonth()->toDateTimeString();
        // dd($start_date,$end_date);
        //get diamond log last month
        $this->reports($pre_date,$start_date,$end_date);

        //cal ranking
        // $this->CalRanking($pre_date,$start_date,$end_date);

        //send_item ranking
        // $this->sendItemRanking($pre_date);
        // $this->sendItemVip3($pre_date);
    }

    private function reports($pre_date,$start,$end){

        // get all member info from package
        $num_member=Member::count();
        // dd($num_member);
        $datewhere=$pre_date->toDateString();
        // $chk_diamond_logs=SaveLogs::whereDate('ranking_at',$datewhere)->first();
        // if(!isset($chk_diamond_logs)){
            if($num_member > 0){
                $events=Events::getEventByID(1);

                for($i=0;$i<$num_member;$i++){
                // foreach($memberFromReward as $member){
                    $member=Member::limit(1)->offset($i)->first();
                    $this->info($member->username);
                    $result_diamond = 0;
                    $last_redeem=0;
                    $result = null;
                    $result = $this->doGetDiamond($member->ncid,$start,$end);
                    if ($result->status == true) {
                        $content = [];
                        $content = $result->response;
                        $collection = [];
                        $collection = collect($content);
                        if (count($content) > 0) {
                            $result_diamond = $collection->sum('diamond');
                            $last_redeem=$collection->max('timestamp');
                        }
                    }
                    //
                    $save_logs=SaveLogs::where('uid',$member->uid)->whereDate('ranking_at',$datewhere)->first();
                    if(!isset($save_logs)){
                        $save_logs = new SaveLogs;
                    }
                        if($result_diamond>=$events->vip_3_diamond){
                            $vip=3;
                        }elseif($result_diamond>=$events->vip_2_diamond){
                            $vip=2;
                        }elseif($result_diamond>=$events->vip_1_diamond){
                            $vip=1;
                        }else{
                            $vip=0;
                        }
                        $save_logs->uid=$member->uid;
                        $save_logs->ncid=$member->ncid;
                        $save_logs->username=$member->username;
                        $save_logs->nickname=$member->nickname;
                        $save_logs->diamond=$result_diamond;
                        $save_logs->last_redeem=$last_redeem;
                        $save_logs->vip=$vip;
                        $save_logs->ranking_at=$pre_date->toDateTimeString();
                        $save_logs->save();


                        echo "\n";
                        echo $i;
                        echo "\n";
                        echo "\n";
                        echo "----------------------------------------------";
                        echo "\n";
                        echo "UID : ".$member->uid;
                        echo "\n";
                        echo "Username : ".$member->username;
                        echo "\n";
                        echo "Total Diamonds : ".$result_diamond;
                        echo "\n";
                        echo "----------------------------------------------";
                        echo "\n";

                }

            }
        // }
    }

    private function doGetDiamond($ncid,$start,$stop) {
        if(empty($ncid)){
            return false;
        }

        return json_decode($this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'query_cash_txn',
                    'user_id' => $ncid,
                    'start_time' => $start,
                    'end_time' => $stop
                ]));
    }

    private function dosendItem(string $ncid, array $goodsData){
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'sending item from garena events bns elite 2019 ranking.',
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }


    /**
     * Simple POST request
     */
    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === FALSE) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === FALSE) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if(curl_errno($ch)){
            return null;
        }

        curl_close($ch);
        return $resp;
    }

}
