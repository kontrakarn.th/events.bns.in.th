<?php

namespace App\Console\Commands\league\test;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Test\league\Setting;
use App\Models\Test\league\Member;
use App\Models\Test\league\Color;
use App\Models\Test\league\Group;
use App\Models\Test\league\ItemLog;
use App\Models\Test\league\Quest;
use App\Models\Test\league\QuestDailyLog;
use App\Models\Test\league\Reward;

class RegisterMember extends Command
{

    protected $description = 'BNS League event';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_league_register_members_test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {

        $memberNotRegister = Member::where('char_id', '!=', 0)->where('char_name', '!=', '')->where('is_register', 0)->inRandomOrder()->get();

        // dd(count($memberNotRegister));

        $eventSetting = Setting::where('active', 1)->first();

        $no = 1;
        if(count($memberNotRegister) > 0){
            foreach($memberNotRegister as $member){

                $this->info("No.".$no.", BNS League : Not Register UID: ".$member->uid.", Username:".$member->username.", char id:".$member->char_id.", is_register:".$member->is_register);


                $group = Group::orderBy('member_count', 'ASC')->orderBy('group_id', 'ASC')->first();
                if($group->member_count >= $eventSetting->max_group_member){
                    $groupCount = Group::count();
                    // create new group
                    Group::create([
                        'group_id' => $groupCount + 1,
                        'member_count' => 0,
                    ]);
    
                    $group = Group::orderBy('member_count', 'ASC')->orderBy('group_id', 'ASC')->first();
                }
    
                // get color
                $color = Color::orderBy('member_count', 'ASC')->first();

                $update = Member::where('uid', $member->uid)->update([
                    'group_id' => $group->group_id,
                    'color_id' => $color->color_id,
                    'is_register' => 1
                ]);

                if($update){

                    $group->member_count = $group->member_count + 1;
                    $group->save();

                    $color->member_count = $color->member_count + 1;
                    $color->save();

                }

                $no++;

            }
        }

    }

}

