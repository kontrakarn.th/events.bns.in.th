<?php

namespace App\Console\Commands\league\test;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Test\league\Setting;
use App\Models\Test\league\Member;
use App\Models\Test\league\Color;
use App\Models\Test\league\Group;
use App\Models\Test\league\ItemLog;
use App\Models\Test\league\Quest;
use App\Models\Test\league\QuestDailyLog;
use App\Models\Test\league\Reward;

use App\Jobs\league\test\UpdateUnsuccessDailyQuestLogQueue as Queue;

class UpdateUnsuccessDailyQuestLog extends Command
{

    protected $description = 'BNS League event';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_league_update_unsuccess_quest_daily_log_test {skip=0} {take=1}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $setting = Setting::where('active', 1)->first();
        if(isset($setting)){

            if (time() >= strtotime($setting->competition_start) && time() <= strtotime($setting->competition_end)) {

                $skip = $this->argument('skip');
                $take = $this->argument('take');

                $logDate = date('Y-m-d');

                $memberQuests = QuestDailyLog::where('uid', '!=', 0)
                                                ->where('char_id', '!=', 0)
                                                // ->where('quest_code', '!=', 0)
                                                ->whereIn('quest_type', ['daily','daily_special','daily_special_bosskill'])
                                                ->where('status', 'pending')
                                                ->where('log_date', '!=', $logDate)
                                                ->take((int)$take)
                                                ->skip((int)$skip)
                                                ->get();

                if (count($memberQuests) > 0) {
                    foreach($memberQuests as $quest){
                        if (empty($quest->id)) {
                            continue;
                        }

                        $this->info("BNS League : Update Unsuccess Daily Quest Log UID: ".$quest->uid." , Username : ".$quest->username." , Quest Code : ".$quest->quest_code." , Quest Title : ".$quest->quest_title);
                        
                        Queue::dispatch($quest->id)->onQueue('bns_league_update_unsuccess_quest_daily_log_queue_test');

                    }

                }

            }
        }
    }

}