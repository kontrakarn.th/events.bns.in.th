<?php

namespace App\Console\Commands\league\live;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\league\Setting;
use App\Models\league\Member;
use App\Models\league\Color;
use App\Models\league\Group;
use App\Models\league\ItemLog;
use App\Models\league\Quest;
use App\Models\league\QuestDailyLog;
use App\Models\league\Reward;

use App\Jobs\league\live\UpdateGroupRankingQueue as Queue;

class UpdateGroupRanking extends Command
{

    protected $description = 'BNS League event';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_league_update_group_ranking_live';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $setting = Setting::where('active', 1)->first();
        if(isset($setting)){

            if (time() >= strtotime($setting->competition_start) && time() <= strtotime($setting->competition_end)) {

                $groups = Group::orderBy('group_id', 'ASC')->get();

                if(count($groups) >0){

                    foreach($groups as $group){

                        $this->info("Update Group Ranking: ".$group->group_id);

                        Queue::dispatch($group->id)->onQueue('bns_league_update_group_ranking_queue_live');

                    }

                }

            }
        }
    }

}