<?php

namespace App\Console\Commands\league\live;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\league\Setting;
use App\Models\league\Member;
use App\Models\league\Color;
use App\Models\league\Group;
use App\Models\league\ItemLog;
use App\Models\league\Quest;
use App\Models\league\QuestDailyLog;
use App\Models\league\Reward;

use App\Jobs\league\live\CheckQuestDailyQueue as Queue;

class RandomGroup extends Command
{

    protected $description = 'BNS League event';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_league_random_group';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        
        // $members = Member::where('is_register', 1)->inRandomOrder()->get();

        // if(count($members) > 0){

        //     $newGroup =1;
        //     $order = 1;
        //     foreach($members as $member){

        //         $this->info("BNS League : Random UID: ".$member->uid." , Old Group : ".$member->group_id." , New Group : ".$newGroup.", No.".$order);

        //         Member::where('uid', $member->uid)->update([
        //             'group_id' => $newGroup,
        //         ]);

        //         $order++;

        //         if($order%100==1){
        //             $newGroup++;
        //         }

                
        //     }

        // }

    }

}