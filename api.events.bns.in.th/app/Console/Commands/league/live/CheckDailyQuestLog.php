<?php

namespace App\Console\Commands\league\live;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\league\Setting;
use App\Models\league\Member;
use App\Models\league\Color;
use App\Models\league\Group;
use App\Models\league\ItemLog;
use App\Models\league\Quest;
use App\Models\league\QuestDailyLog;
use App\Models\league\Reward;

use App\Jobs\league\live\CheckQuestDailyQueue as Queue;

class CheckDailyQuestLog extends Command
{

    protected $description = 'BNS League event';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_league_check_quest_daily_log_live {skip=0} {take=1}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $setting = Setting::where('active', 1)->first();
        if(isset($setting)){

            if (time() >= strtotime($setting->competition_start) && time() <= strtotime($setting->competition_end)) {

                $skip = $this->argument('skip');
                $take = $this->argument('take');

                $logDate = date('Y-m-d');

                $memberQuests = QuestDailyLog::where('uid', '!=', 0)
                                                ->where('char_id', '!=', 0)
                                                ->where('quest_code', '!=', 0)
                                                ->where('quest_type', 'daily')
                                                ->where('status', 'pending')
                                                ->where('log_date', $logDate)
                                                ->take((int)$take)
                                                ->skip((int)$skip)
                                                ->get();

                if (count($memberQuests) > 0) {
                    foreach($memberQuests as $quest){
                        if (empty($quest->id)) {
                            continue;
                        }

                        $this->info("BNS League : Check Daily Quest Log UID: ".$quest->uid." , Username : ".$quest->username." , Quest Code : ".$quest->quest_code." , Quest Title : ".$quest->quest_title.", Time:".date("Y-m-d H:i:s"));
                        
                        Queue::dispatch($quest->id)->onQueue('bns_league_check_quest_daily_log_queue_live');

                    }

                }

            }
        }
    }

}