<?php

namespace App\Console\Commands\league\live;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\league\Setting;
use App\Models\league\Member;
use App\Models\league\Color;
use App\Models\league\Group;
use App\Models\league\ItemLog;
use App\Models\league\Quest;
use App\Models\league\QuestDailyLog;
use App\Models\league\Reward;

use App\Jobs\league\live\CheckSpecialQuestDailyQueue as Queue;

class CheckSpecialDailyQuestLog extends Command
{

    protected $description = 'BNS League event';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_league_check_special_quest_daily_log_live {skip=0} {take=1}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $setting = Setting::where('active', 1)->first();
        if(isset($setting)){

            if (time() >= strtotime($setting->competition_start) && time() <= strtotime($setting->competition_end)) {

                $skip = $this->argument('skip');
                $take = $this->argument('take');

                $logDate = date('Y-m-d');

                $memberQuests = QuestDailyLog::where('uid', '!=', 0)
                                                ->where('char_id', '!=', 0)
                                                // ->where('quest_code', '!=', 0)
                                                ->whereIn('quest_type', ['daily_special','daily_special_bosskill'])
                                                ->where('log_date', $logDate)
                                                ->take((int)$take)
                                                ->skip((int)$skip)
                                                ->get();

                if (count($memberQuests) > 0) {
                    foreach($memberQuests as $quest){
                        if (empty($quest->id)) {
                            continue;
                        }

                        $this->info("BNS League : Create Daily Quest Log UID: ".$quest->uid." , Username : ".$quest->username." , Quest Code : ".$quest->quest_code." , Quest Title : ".$quest->quest_title);

                        /* if($quest->quest_type == 'daily_special_bosskill'){
                            $completed_count = 0;

                            $startDateTime = date('Y-m-d H:i:s', strtotime($quest->created_at));
                            $endDateTime = $quest->log_date.' 23:59:59';

                            $questApi = $this->apiBossKillsByAlias($quest->char_id, $quest->quest_code, $startDateTime, $endDateTime);

                            if ($questApi && $questApi->status === true) {
                                
                                $completed_count = (int)$questApi->response->kill_count;

                                if($quest->uid == 443691835){
                                    dd($completed_count);
                                }
                                
                            }
                            
                            $this->info('Quest Title:'.$quest->quest_title);
                            $this->info('Quest Compelte:'.$completed_count);
                        } */
                        
                        Queue::dispatch($quest->id)->onQueue('bns_league_check_special_quest_daily_log_queue_live');

                    }

                }

            }
        }
    }

    private function apiBossKillsByAlias($char_id = null, $alias_id = null, $start_date = null, $end_date = null)
    {
        if (empty($char_id) || empty($alias_id) || empty($start_date) || empty($end_date)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'get_mon_kills_by_mon_id',
            'char_id'    => $char_id,
            'mon_id'     => $alias_id,
            'start_time' => $start_date,
            'end_time'   => $end_date,
        ]);

        return json_decode($data);
    }

    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }

}