<?php

namespace App\Console\Commands\two_in_one;

use Illuminate\Console\Command;

use App\Models\two_in_one\Setting;
use App\Models\two_in_one\QuestLog;
use App\Jobs\two_in_one\CheckQuestQueue;

class CheckDailyQuestLog extends Command
{
    protected $description = 'BNS two in one event';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_two_in_one_check_daily_quest_log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $questDateTime = $this->setQuestDateTime();
        $logDate = $questDateTime['log_date'];
        $startDateTime = $questDateTime['start_date_time'];
        $endDateTime = $questDateTime['end_date_time'];

        QuestLog::with('quest', 'member')
            ->where('uid', '!=', 0)
            ->whereNotNull('quest_id')
            ->where('status', 'pending')
            ->where('log_date', $logDate)
            ->chunkById(1000, function ($questLogs) use ($startDateTime, $endDateTime) {
                foreach ($questLogs as $questLog) {
                    $this->info("BNS Two in One : Check Daily Quest Log UID: " . $questLog->uid . " , Username : " . $questLog->member->username . " , Quest Log ID : " . $questLog->id . " , Quest Title : " . $questLog->quest->quest_title);

                    CheckQuestQueue::dispatch($questLog, $startDateTime, $endDateTime)->onQueue('bns_two_in_one_check_daily_quest_log');
                }
            });
    }

    private function setQuestDateTime()
    {
        $currDate = date('Y-m-d');

        $startDatetime = '';
        $endDatetime = '';
        $logDate = '';

        if (time() >= strtotime($currDate . ' 00:00:00') && time() <= strtotime($currDate . ' 05:59:59')) { // check current time after 00:00:00 and not over 05:59:59
            $startDatetime = date('Y-m-d', strtotime(' -1 day')) . ' 06:00:00';
            $endDatetime = date('Y-m-d') . ' 05:59:59';
            $logDate = date('Y-m-d', strtotime(' -1 day'));
        } else {
            $startDatetime = date('Y-m-d') . ' 06:00:00';
            $endDatetime = date('Y-m-d', strtotime(' +1 day')) . ' 05:59:59';
            $logDate = date('Y-m-d');
        }

        return [
            'log_date' => $logDate,
            'start_date_time' => $startDatetime,
            'end_date_time' => $endDatetime,
        ];
    }
}
