<?php

namespace App\Console\Commands\two_in_one;

use Illuminate\Console\Command;

use App\Models\two_in_one\Setting;
use App\Models\two_in_one\Member;
use App\Jobs\two_in_one\CreateDailyQuestQueue;

class CreateDailyQuestLog extends Command
{
    protected $description = 'BNS two in one event';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_two_in_one_create_daily_quest_log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $questDateTime = $this->setQuestDateTime();
        $logDate = $questDateTime['log_date'];

        Member::where('uid', '!=', 0)
            ->where('char_id', '!=', 0)
            ->chunk(1000, function ($members) use ($logDate) {
                foreach ($members as $member) {
                    $this->info("BNS Two in One : Create Daily Quest Log UID: " . $member->uid . " , Username : " . $member->username . " , Character : " . $member->char_name);

                    CreateDailyQuestQueue::dispatch($member, $logDate)->onQueue('bns_two_in_one_create_daily_quest_log');
                }
            });
    }

    private function setQuestDateTime()
    {
        $currDate = date('Y-m-d');

        $startDatetime = '';
        $endDatetime = '';
        $logDate = '';

        if (time() >= strtotime($currDate . ' 00:00:00') && time() <= strtotime($currDate . ' 05:59:59')) { // check current time after 00:00:00 and not over 05:59:59
            $startDatetime = date('Y-m-d', strtotime(' -1 day')) . ' 06:00:00';
            $endDatetime = date('Y-m-d') . ' 05:59:59';
            $logDate = date('Y-m-d', strtotime(' -1 day'));
        } else {
            $startDatetime = date('Y-m-d') . ' 06:00:00';
            $endDatetime = date('Y-m-d', strtotime(' +1 day')) . ' 05:59:59';
            $logDate = date('Y-m-d');
        }

        return [
            'log_date' => $logDate,
            'start_date_time' => $startDatetime,
            'end_date_time' => $endDatetime,
        ];
    }
}
