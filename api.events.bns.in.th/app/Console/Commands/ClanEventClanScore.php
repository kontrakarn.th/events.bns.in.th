<?php

namespace App\Console\Commands;

use App\Jobs\ClanEventClanScoreJob;

use App\Models\clan_event\ClanRank;

use Illuminate\Console\Command;

class ClanEventClanScore extends Command
{
    protected $description = 'BNS Clan event cal score of all clans';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_clan_event_clan_score';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }


    private function init()
    {
        $clans = ClanRank::all();

        if (count($clans) > 0) {
            foreach ($clans as $clan) {
                ClanEventClanScoreJob::dispatch(
                    $clan
                )->onQueue('bns_clan_event_score_and_rank_update');
                // )->onQueue('BnsClanEvent');
                $this->info("ClanEvent : Summary Clan Point clan id :".$clan->clan_id);
            }
        }
    }


}
