<?php

namespace App\Console\Commands;

use App\Jobs\ClanEventTopRankJob;

use App\Models\clan_event\ClanRank;

use Illuminate\Console\Command;

class ClanEventTopRank extends Command
{
    protected $description = 'BNS Clan event cal top ranks';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_clan_event_top_rank';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }


    private function init()
    {
        $clans = ClanRank::where('total_points', '>', 0)->orderBy('total_points', 'DESC')->get();
        if (count($clans) > 0) {
            $rank      = 1;
            $tmp_rank  = null;
            $tmp_score = 0;
            foreach ($clans as $clan) {

                if ($tmp_score == $clan->total_points) { // คะแนนเท่ากัน
                    $rank = $tmp_rank;
                }

                $clan->rank = $rank;
                $clan->save();

                $tmp_rank  = $rank;
                $tmp_score = $clan->total_points;
                $rank++;
            }
        }

        $clans_empty = ClanRank::where('total_points', 0)->get();
        if (count($clans_empty) > 0) {
            foreach ($clans_empty as $clan) {
                $clan->rank = 0;
                $clan->save();
            }
        }
        $this->info("ClanEvent : Top Rank Calculate");

    }
}
