<?php

namespace App\Console\Commands\snowball2019;

use App\Jobs\snowball2019\SnowBall2019QuestLogQueue as Queue;

use App\Models\snowball2019\Member;
use App\Models\snowball2019\Quest;
use App\Models\snowball2019\QuestDailyLog;

use Carbon\Carbon;
use Illuminate\Console\Command;

class SnowBall2019UpdateQuestLog extends Command
{

    protected $description = 'BNS Snow Ball 2019 event';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_snowball2019_quests_update {skip=0} {take=1}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {

        $log_date = date('Y-m-d');

        $skip = $this->argument('skip');
        $take = $this->argument('take');

        $members = Member::where('uid', '!=', 0)
                                        ->where('char_id', '!=', 0)
                                        ->take((int)$take)
                                        ->skip((int)$skip)
                                        ->get();

        $quests = Quest::all();

        if(count($members) > 0){
            foreach($members as $member){

                if (empty($member->uid) || empty($member->char_id)) {
                    continue;
                }

                if(count($quests) > 0){
                    foreach($quests as $quest){

                        $this->info("Snow Ball 2019 : Check Quest UID : ".$member->uid." , Quest : ".$quest->quest_code." , Quest : ".$quest->quest_title);

                        Queue::dispatch(
                            $member->uid, $quest->quest_code, $log_date 
                        )->onQueue('bns_snowball2019_quests_update_queue');

                    }
                }

            }
        }

    }

}