<?php

namespace App\Console\Commands;

use App\Models\Test\halloween2019\QuestLog;
use App\Models\Test\halloween2019\Member;
use App\Models\Test\halloween2019\SendItemLog;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Run extends Command
{

    private $baseApi = 'http://api.apps.garena.in.th';

    private $questList = [
        [
            'id'   => 1794,
            'coin' => 15,
            'name' => "ความลับของทัพปีศาจ",
        ],
        [
            'id'   => 1790,
            'coin' => 15,
            'name' => "การแสดงแห่งค่ำคืนสีเลือด",
        ],
        [
            'id'   => 1744,
            'coin' => 15,
            'name' => "ความปรารถนาอันมืดมัว",
        ],
        [
            'id'   => 1558,
            'coin' => 15,
            'name' => "ภัยคุกคามปริศนาที่ตื่นขึ้น",
        ],
        [
            'id'   => 1735,
            'coin' => 10,
            'name' => "ฝันร้ายแห่งกรุสมบัติ",
        ],
        [
            'id'   => 1697,
            'coin' => 10,
            'name' => "เสียงเพลงอันโหยหวน",
        ],
        [
            'id'   => 1672,
            'coin' => 10,
            'name' => "ผู้พลัดถิ่นแห่งเมืองโบราณพรรณไม้",
        ],
        [
            'id'   => 1614,
            'coin' => 10,
            'name' => "เสียงเพรียกของสายลม",
        ],
        [
            'id'   => 1623,
            'coin' => 5,
            'name' => "รุกฆาต",
        ],
        [
            'id'   => 1674,
            'coin' => 5,
            'name' => "หุบเขาเหมันต์",
        ],
        [
            'id'   => 1528,
            'coin' => 5,
            'name' => "ผู้บุกรุกที่ศาลนักพรตนาริว",
        ],
        [
            'id'   => 1478,
            'coin' => 5,
            'name' => "เสียงหอนของเตาหลอม",
        ],
        [
            'id'   => 1365,
            'coin' => 5,
            'name' => "พันธะที่ผูกเราไว้",
        ],
        [
            'id'   => 1064,
            'coin' => 5,
            'name' => "ดาบของจอมดาบเดี่ยวมรณะ",
        ],
    ];


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_run_halloween';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Starting...\n";

        $date   = "2019-10-18";
        $start = "2019-10-18 06:00:00";
        $end   = "2019-10-19 05:59:59";

        $members = Member::where('character_id','!=',null)->where('created_at','<','2019-10-19 06:00:00')->take(1000)->get();

        $i = 0;
        foreach ($members as $member){

            $quest = QuestLog::whereUid($member->uid)->whereCharId($member->character_id)->whereDateLog('2019-10-18')->first();
            if ($quest){
                if ($quest->coin != 130){
//                    $quests_list = collect($this->questList);
//                    $arr    = [];
//                    $coin   = 0;
//                    foreach ($quests_list as $key => $quest_list) {
//
//                        $key     = $key + 1;
//                        $var     = "quest_success_".$key;
//                        $questId = $quest_list['id'];
//                        $check = $this->apiCheckQuestCompleted($member->uid, $member->character_id, $questId, $start, $end);
//                        if (is_null($check) == false && is_object($check) && $check->status) {
//                            $check_k =  $check->response->quest_completed_count;
//                        } else {
//                            $check_k = 0;
//                        }
//                        if ($check_k > 0) {
//                            $check_k = 1;
//                        }
//                        if ($check_k == 1) {
//                            $coin += $quest_list['coin'];
//                        }
//                        $arr[$var] = $check_k;
//                    }
//                    $arr['coin'] = $coin;
//                    $quest->update($arr);
//
//                    $sum_coin = (int) QuestLog::whereUid($member->uid)
//                        ->whereCharId($member->character_id)
//                        ->sum('coin');
//
//                    $sum_coin_used = (int) SendItemLog::whereUid($member->uid)
//                        ->whereStatus('success')
//                        ->sum('coin');
//
//                    $quest = QuestLog::whereUid($member->uid)
//                        ->whereCharId($member->character_id)
//                        ->get();
//
//                    $data = array(
//                        'quest_success_1'  => (int) $quest->sum('quest_success_1'),
//                        'quest_success_2'  => (int) $quest->sum('quest_success_2'),
//                        'quest_success_3'  => (int) $quest->sum('quest_success_3'),
//                        'quest_success_4'  => (int) $quest->sum('quest_success_4'),
//                        'quest_success_5'  => (int) $quest->sum('quest_success_5'),
//                        'quest_success_6'  => (int) $quest->sum('quest_success_6'),
//                        'quest_success_7'  => (int) $quest->sum('quest_success_7'),
//                        'quest_success_8'  => (int) $quest->sum('quest_success_8'),
//                        'quest_success_9'  => (int) $quest->sum('quest_success_9'),
//                        'quest_success_10' => (int) $quest->sum('quest_success_10'),
//                        'quest_success_11' => (int) $quest->sum('quest_success_11'),
//                        'quest_success_12' => (int) $quest->sum('quest_success_12'),
//                        'quest_success_13' => (int) $quest->sum('quest_success_13'),
//                        'quest_success_14' => (int) $quest->sum('quest_success_14'),
//                        'coin' => (int) $sum_coin,
//                        'coin_used' => (int) $sum_coin_used,
//                    );
//
//                    $member->update($data);
                    $status = "Yes";
                    $i ++;
                }else{
                    $status = "No";
                }
            }else{
                $status = "No";
            }

            echo $i." : ".$member->uid."  --> $status\n";
        }

        echo "$i/".$members->count();
    }


    private function apiCheckQuestCompleted($uid, $charId, $questId, $start, $end)
    {
        // $resp = Cache::remember('BNS:BUFFET:QUEST_COMPLETED_' . $questId . '_' . $uid, 10, function()use($charId,$questId,$start,$end) {
        $resp = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $charId,
            'quest_id'   => $questId,
            'start_time' => $start,
            'end_time'   => $end,
        ]);
        // });

        return json_decode($resp);
    }

    protected function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        // dd($url, $params, $headers);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === FALSE) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === FALSE) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }
}
