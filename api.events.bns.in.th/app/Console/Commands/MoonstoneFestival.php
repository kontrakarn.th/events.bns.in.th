<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use Carbon\Carbon;
use Storage;
use Cache;
use DB;

use App\Models\moonstone_festival\Member;
use App\Models\moonstone_festival\ItemLog;

class MoonstoneFestival extends Command
{
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_moonstone_festival';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Moonstone Festival';

    /**
     * Query start end
     *
     * @var string
     */
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkMemberKillMonstersOver3000();
    }

    private function checkMemberKillMonstersOver3000(){

        // dd('Bello');

        $startDate = '2018-12-18 00:00:00';
        $endDate = '2018-12-18 23:59:59';

        $startTimestamp = strtotime($startDate);
        $endTimestamp = strtotime($endDate);
        
        $members = Member::where('char_id', '!=', '')->get();

        /* echo "\n";
        echo "------------------------------------------";
        echo "\n";
        echo "\n";
        echo "Total members : ".count($members);
        echo "\n";
        echo "\n";
        echo "------------------------------------------";
        echo "\n"; */
        echo "UID,Username,NCID,CharId,CharName,MonsterKillCount";
        echo "\n";

        // $totalMemberCanClaimReward = 0;

        if(count($members) > 0){

            foreach($members as $member){

                // check item history
                $itemHistory = 0;
                $itemHistory = ItemLog::where('uid', $member->uid)
                                        ->whereBetween('log_date_timestamp', [$startTimestamp,$endTimestamp])
                                        ->count();

                if($itemHistory == 0){

                    // check api
                    $monstersKillCount = 0;
                    $monstersKillCount = $this->setMosterKillCount($member->uid,$member->ncid,$member->char_id,$startDate,$endDate);
                    if($monstersKillCount >= 3000){

                        echo $member->uid.",".$member->username.",".$member->ncid.",".$member->char_id.",".$member->char_name.",".$monstersKillCount;
                        echo "\n";

                        /* echo "\n";
                        echo "------------------------------------------";
                        echo "\n";
                        echo "\n";
                        echo "UID : ".$member->uid;
                        echo "\n";
                        echo "\n";
                        echo " Username : ".$member->username;
                        echo "\n";
                        echo "\n";
                        echo "Monsters Kill Count : ".$monstersKillCount;
                        echo "\n";
                        echo "\n";
                        echo "------------------------------------------";
                        echo "\n"; */

                        // $totalMemberCanClaimReward += 1;

                    }


                }

            }

        }

       /*  echo "\n";
        echo "------------------------------------------";
        echo "\n";
        echo "\n";
        echo "Total members kill monsters over 3000 : ".$totalMemberCanClaimReward;
        echo "\n";
        echo "\n";
        echo "------------------------------------------";
        echo "\n"; */

    }

    private function apiKillMoster($uid = null,$ncid = null,$char_id = null,$start_date = null,$end_date = null)
    {
        if (empty($uid) || empty($ncid) || empty($char_id) || empty($start_date) || empty($end_date))
            return false;

        $data = $this->post_api($this->baseApi, [
            'key_name' => 'bns',
            'service' => 'events',
            'user_id' => $ncid,
            'subservice' => 'get_mon_kills',
            'start_time' => $start_date,
            'end_time' => $end_date,
            'char_id' => $char_id
        ]);
        return json_decode($data);
    }

    private function setMosterKillCount($uid,$ncid,$char_id,$startDate,$endDate)
    {
        $monsterKillCount = 0;
        $monsterKill = $this->apiKillMoster($uid,$ncid,$char_id,$startDate,$endDate);
        if($monsterKill->status == true){
            $monsterKillCount = intval($monsterKill->response->kill_count);
        }

        return $monsterKillCount;
    }

     /**
     * Simple POST request
     */
    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === FALSE) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === FALSE) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);
        
        if(curl_errno($ch)){
            return null;
        }
        
        curl_close($ch);
        return $resp;
    }

}