<?php

namespace App\Console\Commands;

use App\Models\clan_event\QuestDailyLog;
use App\Models\clan_event\QuestMemberLog;

use Carbon\Carbon;
use Illuminate\Console\Command;

class ClanEventRecheckMemberQuest extends Command
{

    protected $description = 'BNS Clan event recheck member quest';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_clan_event_recheck_member_quest {quest_id}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $quest_id = $this->argument('quest_id');

        if(!isset($quest_id) || empty($quest_id)){
            $this->info("No quest id.");
            return false;
        }

        $questMemberList = QuestMemberLog::where('quest_code', $quest_id)->get();

        if(count($questMemberList) > 0){
            foreach($questMemberList as $questKey=>$questVal){

                $this->info(($questKey+1).". Start Check Member Quest UID : ".$questVal->uid." , Quest Id : ".$questVal->quest_code." , Quest Name : ".$questVal->quest_title);

                $questDailies = QuestDailyLog::where('uid', $questVal->uid)
                    ->where('char_id', $questVal->char_id)
                    ->where('quest_code', $questVal->quest_code)
                    ->get();

                if (count($questDailies) > 0) {

                    $questMemberLog = QuestMemberLog::where('uid', $questVal->uid)
                        ->where('char_id', $questVal->char_id)
                        ->where('quest_code', $questVal->quest_code)
                        ->first();

                    $complete_counts = $questDailies->sum('completed_count');

                    if ($questMemberLog) {
                        $questMemberLog->completed_count = $complete_counts;
                        $questMemberLog->total_points    = ($complete_counts * $questVal->quest_points);
                        $questMemberLog->save();

                        $this->info("Member Quest is updated!");
                    }else{
                        $this->info("Can not updated member quest!");
                    }

                }

                $this->info(($questKey+1).". End Check Member Quest UID : ".$questVal->uid." , Quest Id : ".$questVal->quest_code." , Quest Name : ".$questVal->quest_title);
                $this->info("----------------------------------------------------------------------");

            }
        }
    }

}