<?php

namespace App\Console\Commands\daily_stamp;

use App\Jobs\daily_stamp\CheckMemberQuestQueueSuccess as Queue;

use App\Models\daily_stamp\Events            as DB_EVENT;
use App\Models\daily_stamp\Member;
use App\Models\daily_stamp\MemberQuest;
use App\Models\daily_stamp\ItemHistory;
use App\Models\daily_stamp\SendItemLog;
use App\Models\daily_stamp\DeductLog;
use App\Models\daily_stamp\Quest;
use App\Models\daily_stamp\QuestDailyLog;
use App\Models\daily_stamp\Reward;
use App\Models\daily_stamp\Utils as Utils;

use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckMemberQuestSuccess extends Command
{

    protected $description = 'BNS Daily stamp event';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_daily_stamp_check_member_quest_success {skip=0} {take=1}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $event = DB_EVENT::where('eventname', Utils::KEY_EVENT)->first();

        if (isset($event) === true) {
            if (time() >= strtotime($event->topup_start_datetime) && time() <= strtotime($event->topup_end_datetime)) {

                $skip = $this->argument('skip');
                $take = $this->argument('take');

                // $memberQuests = MemberQuest::where('uid', 372177874)
                $memberQuests = MemberQuest::where('uid', '!=', 0)
                  ->where('status', 'pending')
                  ->take((int)$take)
                  ->skip((int)$skip)
                  ->get();

                if (count($memberQuests) > 0) {
                    foreach($memberQuests as $member) {
                        if (empty($member->id)) {
                            continue;
                        }

                        // $this->info($member->uid);

                        // $this->info("PiggyEvent : Check Quest UID : ".$quest->uid." , Quest ID : ".$quest->quest_id." , Quest : ".$quest->quest_code." , Quest : ".$quest->quest_title);

                        Queue::dispatch($member->id)->onQueue('bns_daily_stamp_check_member_quest_success');

                    }
                }
            }
        }

    }

}
