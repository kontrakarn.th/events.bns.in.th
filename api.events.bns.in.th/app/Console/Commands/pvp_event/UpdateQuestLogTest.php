<?php

namespace App\Console\Commands\pvp_event;

use App\Jobs\pvp_event\UpdateQuestLogQueueTest as Queue;

use App\Models\Test\pvp_event\Member;
use App\Models\Test\pvp_event\Quest;
use App\Models\Test\pvp_event\QuestMemberLog;
use App\Models\Test\pvp_event\QuestDailyLog;

use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateQuestLogTest extends Command
{

    protected $description = 'BNS PVP event';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_pvp_event_quests_update_test {skip=0} {take=1}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {

        $log_date = date('Y-m-d');

        $skip = $this->argument('skip');
        $take = $this->argument('take');

        $memberQuests = QuestMemberLog::where('uid', '!=', 0)
                                        ->where('char_id', '!=', 0)
                                        ->where('quest_code', '!=', 0)
                                        ->take((int)$take)
                                        ->skip((int)$skip)
                                        ->get();

        if(count($memberQuests) > 0){
            foreach($memberQuests as $quest){

                if (empty($quest->uid) || empty($quest->char_id) || empty($quest->quest_code)) {
                    continue;
                }

                $this->info("PvPEvent : Check Quest UID : ".$quest->uid." , Quest : ".$quest->quest_code." , Quest : ".$quest->quest_title);

                Queue::dispatch(
                    $quest->uid, $quest->quest_code, $log_date
                )->onQueue('bns_pvp_event_quests_update_queue_test');

            }
        }

    }

}