<?php

namespace App\Console\Commands\hardmode\test;

use Illuminate\Console\Command;

use App\Models\Test\hardmode\Setting;
use App\Models\Test\hardmode\QuestDailyLog;

use App\Jobs\hardmode\test\CheckQuestDailyQueue as Queue;

class CheckDailyQuestLog extends Command
{
    protected $description = 'BNS Hardmode event';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_hardmode_check_quest_daily_log_test {--date=}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $setting = Setting::where('active', 1)->first();

        if (isset($setting)) {

            if (time() >= strtotime($setting->open_box_start) && time() <= strtotime($setting->open_box_end)) {

                $date = $this->option('date');
                $logDate = $date ? $date : date('Y-m-d');

                $i = 0;

                QuestDailyLog::where('uid', '!=', 0)
                    ->where('char_id', '!=', 0)
                    ->whereNotNull('quest_code')
                    ->where('quest_type', 'daily_bosskill')
                    ->where('status', 'pending')
                    ->where('log_date', $logDate)
                    ->chunkById(1000, function ($memberQuests) use ($logDate, &$i) {
                        foreach ($memberQuests as $quest) {
                            if (empty($quest->id)) {
                                continue;
                            }

                            $this->info("BNS Hardmode : Create Daily Quest Log UID: " . $quest->uid . " , Username : " . $quest->username . " , Quest Code : " . $quest->quest_code . " , Quest Title : " . $quest->quest_title);

                            Queue::dispatch($quest->id, $logDate)->onQueue('bns_hardmode_check_quest_daily_log_queue_test');

                            $i++;
                        }
                    });

                $this->info("Success bns_hardmode_check_quest_daily_log_test total: " . $i);
            }
        }
    }
}
