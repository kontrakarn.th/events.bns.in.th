<?php

namespace App\Console\Commands\hardmode\live;

use Illuminate\Console\Command;

use App\Models\hardmode\Setting;
use App\Models\hardmode\Member;

use App\Jobs\hardmode\live\CreateQuestDailyQueue as Queue;

class CreateDailyQuestLog extends Command
{
    protected $description = 'BNS hardmode event';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_hardmode_create_quest_daily_log_live {--date=}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $setting = Setting::where('active', 1)->first();

        if (isset($setting)) {

            if (time() >= strtotime($setting->open_box_start) && time() <= strtotime($setting->open_box_end)) {

                $date = $this->option('date');
                $logDate = $date ? $date : date('Y-m-d');

                $i = 0;

                Member::where('uid', '!=', 0)
                    ->where('char_id', '!=', 0)
                    ->chunk(1000, function ($members) use ($logDate, &$i) {
                        foreach ($members as $member) {
                            if (empty($member->id)) {
                                continue;
                            }

                            $this->info("BNS Hardmode : Create Daily Quest Log UID: " . $member->uid . " , Username : " . $member->username . " , Character : " . $member->char_name);

                            Queue::dispatch($member->id, $logDate)->onQueue('bns_hardmode_create_quest_daily_log_queue_live');
                            
                            $i++;
                        }
                    });

                $this->info("Success bns_hardmode_create_quest_daily_log_live total: " . $i);
            }
        }
    }
}
