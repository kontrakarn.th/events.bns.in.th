<?php

namespace App\Console\Commands;

use App\Jobs\ClanEvent;
// use App\Jobs\ClanEventJob;
use App\Jobs\ClanEventMemberCalJob;

use App\Models\clan_event\Member;
use App\Models\clan_event\Quest;

use Carbon\Carbon;
use Illuminate\Console\Command;

class ClanEventCronMemberScore extends Command
{

    protected $description = 'BNS Clan event';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_clan_event_member_score {skip=0} {take=1}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function init()
    {

        $skip = $this->argument('skip');
        $take = $this->argument('take');

        // Get Yesterday
        $start = Carbon::now()->subDay(1)->startOfDay()->toDateTimeString();
        $end   = Carbon::now()->subDay(1)->endOfDay()->toDateTimeString();

        // Get members who pass condition
        $members = Member::where('char_id', '!=', 0)
            ->where('clan_id', '!=', 0)
            ->where('world_id', '!=', 0)
            ->where('current_member', 'yes')
            ->take((int)$take)
            ->skip((int)$skip)
            ->get();

        $this->info('Users (skip: ' . $skip . ', take: ' . $take . ') =' . count($members));

        // $questLists = Quest::all();

        if (count($members) > 0) {
            foreach ($members as $member) {

                $uid      = $member->uid;
                $char_id  = $member->char_id;
                $world_id = $member->world_id;

                if (empty($uid) || empty($char_id) || empty($world_id)) {
                    continue;
                }

                // Check member is current member of guild
                /* $joined = $this->apiJoinGuild($char_id, $world_id);
                if ($joined && $joined->status && $joined->status === false) {
                    $member->current_member = "no";
                    $member->save();
                    continue;

                } elseif ($joined->status === true) {
                    if (($member->clan_id != $joined->response->id) || ($member->joined_at_timestamp != strtotime($joined->response->joined_at))) {
                        // Guild ไม่ตรงกับตอนลงทะเบียน
                        $leave = $this->apiGuildLeave($char_id, $member->created_at, Carbon::now()->toDateTimeString());
                        if ($leave && $leave->status && $leave->status === true) {
                            $member->leaved_at           = Carbon::parse($leave->response->time_leave_guild)->toDateTimeString();
                            $member->leaved_at_timestamp = Carbon::parse($leave->response->time_leave_guild)->timestamp;
                        }
                        $member->current_member = "no";
                        $member->save();
                        continue;
                    }
                } */
                
                ClanEventMemberCalJob::dispatch(
                    $member
                )->onQueue('bns_clan_event_score_and_rank_update');
                // )->onQueue('BnsClanEvent');
                $this->info("ClanEvent : Summary Point UID : ".$member->uid);

            }
            return;
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    // --- APIs ---

    private function apiGuildLeave($char_id = null, $start_date = null, $end_date = null)
    {
        if (empty($char_id) || empty($start_date) || empty($end_date)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'guild_leave_by_char_id',
            'char_id'    => $char_id,
            'start_time' => $start_date,
            'end_time'   => $end_date,
        ]);

        return json_decode($data);
    }

    private function apiJoinGuild($char_id = null, $world_id = null)
    {
        if (empty($char_id) || empty($world_id)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name' => 'bns',
            'service'  => 'guild',
            'char_id'  => $char_id,
            'world_id' => $world_id,
        ]);

        return json_decode($data);
    }

    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }

}
