<?php

namespace App\Console\Commands\songkran2020;

use App\Models\songkran2020\Cron;
use App\Models\songkran2020\Member;
use App\Models\songkran2020\QuestLog;

use App\Jobs\songkran2020\UpdateQuestLogs;

use Carbon\Carbon;
use Illuminate\Console\Command;

class Sonkran2020UpdateQuest extends Command
{
    protected $description = 'BNS Songkran 2020 Update Quest';
    private $endTime = '2020-04-28 23:59:59';


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_songkran2020_quest_update {skip=0} {take=1}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        if (Carbon::now() > Carbon::parse($this->endTime)) {
            $this->info("End of event : do not run command.");
            return;
        }

        $this->info("Preparing to run command BNS Songkran 2020 Update quest.");

        // If Cron run before 00:30am will update yesterday.
        if (Carbon::now()->toDateTimeString() < Carbon::now()->format("Y-m-d 00:29:59")) {
            $date = Carbon::now()->subDay(1)->toDateString();
        } else {
            $date = Carbon::now()->toDateString();
        }

        $start = $date.' 00:00:00';
        $end   = $date.' 23:59:59';

        $skip = $this->argument('skip');
        $take = $this->argument('take');

        $members = Member::where('character_id', '!=', null)
            ->where('character_name', '!=', null)
            ->take((int) $take)
            ->skip((int) $skip)
            ->get();

        if (count($members) > 0) {
            foreach ($members as $member) {
                if (empty($member->id) || empty($member->character_id) ) {
                    continue;
                }

                // check created_at same date
                if (Carbon::parse($member->created_at)->toDateString() === $date){
                    $start = Carbon::parse($member->created_at)->toDateTimeString();
                }

                $member->last_update = Carbon::now()->toDateTimeString();
                $member->save();

                $this->info($member->uid." of ".$date." add to queue update quest.");
                UpdateQuestLogs::dispatch($member,$date,$start,$end)->onQueue('bns_songkran2020_quests_update_queue');
            }
        }
    }

}
