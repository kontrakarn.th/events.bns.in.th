<?php

namespace App\Console\Commands;

use App\Models\clan_event\QuestDailyLog;

use Carbon\Carbon;
use Illuminate\Console\Command;

class ClanEventRecheckDailyQuest extends Command
{

    protected $description = 'BNS Clan event recheck daily quest';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_clan_event_recheck_daily_quest {quest_id}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $quest_id = $this->argument('quest_id');

        if(!isset($quest_id) || empty($quest_id)){
            $this->info("No quest id.");
            return false;
        }
        
        $questDailyList = QuestDailyLog::where('quest_code', $quest_id)->get();

        $this->info("Total Logs : ".count($questDailyList));

        if(count($questDailyList) > 0){
            foreach($questDailyList as $questKey=>$questVal){

                $this->info(($questKey+1).". Start Check Daily Quest UID : ".$questVal->uid." , Quest Id : ".$questVal->quest_code." , Quest Name : ".$questVal->quest_title);

                $completed_count = 0;

                $startApi = '';
                $endApi = '';

                $startApi = $questVal->log_date.' 00:00:00';
                $endApi = $questVal->log_date.' 23:59:59';

                $questApiResp = $this->apiQuestCompleted($questVal->char_id, $questVal->quest_code, $startApi,
                    $endApi);
                if ($questApiResp->status === true) {
                    $completed_count = $questApiResp->response->quest_completed_count;
                } else {
                    $completed_count = 0;
                }

                // set data value for update
                $totalPoints = 0;
                $totalPoints = $completed_count > 0 ? $completed_count * $questVal->quest_points : 0;
                
                $updateDailyQuest = QuestDailyLog::where('id', $questVal->id)
                                                ->where('uid', $questVal->uid)
                                                ->where('quest_code', $quest_id)
                                                ->where('log_date', $questVal->log_date)
                                                ->update([
                                                    'completed_count' => $completed_count,
                                                    'total_points' => $totalPoints,
                                                ]);
                if($updateDailyQuest){
                    $this->info("Daily Quest is updated!");
                }else{
                    $this->info("Can not updated daaily quest!");
                }

                $this->info(($questKey+1).". End Check Daily Quest UID : ".$questVal->uid." , Quest Id : ".$questVal->quest_code." , Quest Name : ".$questVal->quest_title);
                $this->info("----------------------------------------------------------------------");
            }
        }

    }

    private function apiQuestCompleted($char_id = null, $quest_id = null, $start_date = null, $end_date = null)
    {
        if (empty($char_id) || empty($quest_id) || empty($start_date) || empty($end_date)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $char_id,
            'quest_id'   => $quest_id,
            'start_time' => $start_date,
            'end_time'   => $end_date,
        ]);

        return json_decode($data);
    }

    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }

}
