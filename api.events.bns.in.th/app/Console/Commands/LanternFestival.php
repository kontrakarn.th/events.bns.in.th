<?php

namespace App\Console\Commands;

use App\Models\lantern_festival\Cron;
use App\Models\lantern_festival\Member;
use App\Models\lantern_festival\QuestLog;
use App\Models\lantern_festival\SendItemLog;
use Carbon\Carbon;
use Illuminate\Console\Command;

class LanternFestival extends Command
{
    protected $total = 0;
    protected $baseApi = 'http://api.apps.garena.in.th';

    private $questList = [
        [
            'id'   => 1794,
            'coin' => 15,
            'name' => "ความลับของทัพปีศาจ",
        ],
        [
            'id'   => 1790,
            'coin' => 15,
            'name' => "การแสดงแห่งค่ำคืนสีเลือด",
        ],
        [
            'id'   => 1744,
            'coin' => 15,
            'name' => "ความปรารถนาอันมืดมัว",
        ],
        [
            'id'   => 1558,
            'coin' => 15,
            'name' => "ภัยคุกคามปริศนาที่ตื่นขึ้น",
        ],
        [
            'id'   => 1735,
            'coin' => 10,
            'name' => "ฝันร้ายแห่งกรุสมบัติ",
        ],
        [
            'id'   => 1697,
            'coin' => 10,
            'name' => "เสียงเพลงอันโหยหวน",
        ],
        [
            'id'   => 1672,
            'coin' => 10,
            'name' => "ผู้พลัดถิ่นแห่งเมืองโบราณพรรณไม้",
        ],
        [
            'id'   => 1614,
            'coin' => 10,
            'name' => "เสียงเพรียกของสายลม",
        ],
        [
            'id'   => 1623,
            'coin' => 5,
            'name' => "รุกฆาต",
        ],
        [
            'id'   => 1674,
            'coin' => 5,
            'name' => "หุบเขาเหมันต์",
        ],
        [
            'id'   => 1528,
            'coin' => 5,
            'name' => "ผู้บุกรุกที่ศาลนักพรตนาริว",
        ],
        [
            'id'   => 1478,
            'coin' => 5,
            'name' => "เสียงหอนของเตาหลอม",
        ],
        [
            'id'   => 1365,
            'coin' => 5,
            'name' => "พันธะที่ผูกเราไว้",
        ],
        [
            'id'   => 1064,
            'coin' => 5,
            'name' => "ดาบของจอมดาบเดี่ยวมรณะ",
        ],
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lantern_festival {skip=0} {take=100}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function init()
    {
        $skip = (int) $this->argument('skip');
        $take = (int) $this->argument('take');

        $users = Member::take((int) $take)
            ->skip((int) $skip)
            ->get();

        foreach ($users as $user) {

            if ($user->uid){

                $ql = QuestLog::whereUid($user->uid)->orderBy('id','desc')->first();
                if ($ql){
                    $date_last = $ql->date_log.' 00:00:00';

                    if (Carbon::parse($user->last_update) < Carbon::parse($date_last)){
                        $user->last_update = $date_last;
                        $user->save();
                        echo $user->uid." Set!!\n";
                    }
                }
            }

//            if ($user->created_at) {
//                $uid     = $user->uid;
//                $char_id = $user->character_id;
//                $dates   = $this->generateDateRange($user->created_at);
//                foreach ($dates as $date) {
//
//                    if (time() >= strtotime($date.' 00:00:00') && time() <= strtotime($date.' 05:59:59')) {
//                        $start = Carbon::parse($date)->subDay(1)->format("Y-m-d 06:00:00");
//                        $end   = Carbon::parse($date)->format("Y-m-d 05:59:59");
//                    } else {
//                        $start = Carbon::parse($date)->format("Y-m-d 06:00:00");
//                        $end   = Carbon::parse($date)->addDay(1)->format("Y-m-d 05:59:59");
//                    }
//
//                    $quests = collect($this->questList);
//                    $arr    = [];
//                    $coin   = 0;
//
//                    $questLogs = QuestLog::whereUid($uid)->whereCharId($char_id)->whereDateLog($date)->first();
//
//                    if ($questLogs){
//                        if ($questLogs->coin == 130){
//                            echo "$date pass\n";
//                            continue;
//                        }
//                    }else{
//                        echo "$date not found\n";
//                        continue;
//                    }
//
//                    echo "$uid -> $date \n";
//
//                    foreach ($quests as $key => $quest) {
//
//                        $key     = $key + 1;
//                        $var     = "quest_success_".$key;
//                        $questId = $quest['id'];
//                        $check   = $this->apiCheckQuestCompleted($uid, $char_id, $questId, $start, $end);
//                        if (is_null($check) == false && is_object($check) && $check->status) {
//                            $check_k = $check->response->quest_completed_count;
//                        } else {
//                            $check_k = 0;
//                        }
//                        if ($check_k > 0) {
//                            $check_k = 1;
//                        }
//                        if ($check_k == 1) {
//                            $coin += $quest['coin'];
//                        }
//                        $arr[$var] = $check_k;
//                    }
//
//                    $arr['coin']  = $coin;
//                    $arr['start'] = $start;
//                    $arr['end']   = $end;
//
//                    $questLogs = QuestLog::whereUid($uid)->whereCharId($char_id)->whereDateLog($date)->first();
//
//                    if ($questLogs){
//                        $questLogs->update($arr);
//                    }
//                }
//
//                $sum_coin = (int) QuestLog::whereUid($uid)
//                    ->whereCharId($char_id)
//                    ->sum('coin');
//
//                $sum_coin_used = (int) SendItemLog::whereUid($uid)
//                    ->whereStatus('success')
//                    ->sum('coin');
//
//                $quest = QuestLog::whereUid($uid)
//                    ->whereCharId($char_id)
//                    ->get();
//
//                $data = array(
//                    'quest_success_1'  => (int) $quest->sum('quest_success_1'),
//                    'quest_success_2'  => (int) $quest->sum('quest_success_2'),
//                    'quest_success_3'  => (int) $quest->sum('quest_success_3'),
//                    'quest_success_4'  => (int) $quest->sum('quest_success_4'),
//                    'quest_success_5'  => (int) $quest->sum('quest_success_5'),
//                    'quest_success_6'  => (int) $quest->sum('quest_success_6'),
//                    'quest_success_7'  => (int) $quest->sum('quest_success_7'),
//                    'quest_success_8'  => (int) $quest->sum('quest_success_8'),
//                    'quest_success_9'  => (int) $quest->sum('quest_success_9'),
//                    'quest_success_10' => (int) $quest->sum('quest_success_10'),
//                    'quest_success_11' => (int) $quest->sum('quest_success_11'),
//                    'quest_success_12' => (int) $quest->sum('quest_success_12'),
//                    'quest_success_13' => (int) $quest->sum('quest_success_13'),
//                    'quest_success_14' => (int) $quest->sum('quest_success_14'),
//                    'coin' => (int) $sum_coin,
//                    'coin_used' => (int) $sum_coin_used,
//                );
//
//                $member = Member::whereUid($uid) ->whereCharacterId($char_id)->first();
//                $member->update($data);


//                echo "$uid updated\n";
//            }
        }

        $this->total = $users->count();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Time['start'] = Carbon::now();

        $this->init();

        $Time['end']   = Carbon::now();
        $Time['diff']  = $Time['start']->diff($Time['end'])->format('%H:%I:%S');
        $Time['start'] = Carbon::parse($Time['start'])->format('H:I:s');
        $Time['end']   = Carbon::parse($Time['end'])->format('H:I:s');

        $cron             = new Cron();
        $cron->response   = json_encode($Time);
        $cron->total_user = $this->total;
        $cron->save();

        dd($Time);
    }

    private function generateDateRange($created_at)
    {
        $start_date = Carbon::parse($created_at);
        $end_date   = Carbon::today()->addDay(1);
        $dates      = [];

        for ($date = $start_date->copy(); $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
    }

    private function apiCheckQuestCompleted($uid, $charId, $questId, $start, $end)
    {
        $resp = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $charId,
            'quest_id'   => $questId,
            'start_time' => $start,
            'end_time'   => $end,
        ]);
        return json_decode($resp);
    }

    protected function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        // dd($url, $params, $headers);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }


}
