<?php

namespace App\Console\Commands\hm_pass_ss2;

use Illuminate\Console\Command;

use App\Models\hm_pass_ss2\Setting;
use App\Models\hm_pass_ss2\Member;
use App\Jobs\hm_pass_ss2\CreateDailyQuestQueue;

class CreateDailyQuestLog extends Command
{
    protected $description = 'BNS hongmoon pass ss2 event';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_hm_pass_ss2_create_daily_quest_log {--date=}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $date = $this->option('date');
        $logDate = $date ? $date : date('Y-m-d');

        Member::where('uid', '!=', 0)
            ->where('char_id', '!=', 0)
            ->chunk(1000, function ($members) use ($logDate) {
                foreach ($members as $member) {
                    $this->info("BNS Hongmoon pass season 2 : Create Daily Quest Log UID: " . $member->uid . " , Username : " . $member->username . " , Character : " . $member->char_name);

                    CreateDailyQuestQueue::dispatch($member, $logDate)->onQueue('bns_hm_pass_ss2_create_daily_quest_log');
                }
            });
    }
}
