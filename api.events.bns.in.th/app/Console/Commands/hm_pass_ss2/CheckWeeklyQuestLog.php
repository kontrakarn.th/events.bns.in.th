<?php

namespace App\Console\Commands\hm_pass_ss2;

use Illuminate\Console\Command;

use App\Models\hm_pass_ss2\Setting;
use App\Models\hm_pass_ss2\QuestLog;
use App\Models\hm_pass_ss2\Week;
use App\Jobs\hm_pass_ss2\CheckQuestQueue;
use Carbon\Carbon;

class CheckWeeklyQuestLog extends Command
{
    protected $description = 'BNS hongmoon pass ss2 event';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_hm_pass_ss2_check_weekly_quest_log {--week=}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $weekId = $this->option('week');

        if ($weekId) {
            $week = Week::find($weekId);
        } else {
            $now = Carbon::now()->toDateTimeString();
            $week = Week::where('week_start', '<=', $now)->where('week_end', '>=', $now)->first();
        }

        if (!$week) {
            return $this->error('Week not found');
        }

        $startDateTime = $week->week_start;
        $endDateTime = $week->week_end;

        QuestLog::with('quest', 'member')
            ->where('uid', '!=', 0)
            ->whereNotNull('quest_id')
            ->whereHas('quest', function ($query) use ($week) {
                $query->where('quest_period', 'weekly')
                    ->where('week', $week->id);
            })
            ->where('status', 'pending')
            ->chunkById(1000, function ($questLogs) use ($startDateTime, $endDateTime) {
                foreach ($questLogs as $questLog) {
                    $this->info("BNS Hongmoon pass season 2 : Check Weekly Quest Log UID: " . $questLog->uid . " , Username : " . $questLog->member->username . " , Quest Log ID : " . $questLog->id . " , Quest Title : " . $questLog->quest->quest_title);

                    CheckQuestQueue::dispatch($questLog, $startDateTime, $endDateTime)->onQueue('bns_hm_pass_ss2_check_weekly_quest_log');
                }
            });
    }
}
