<?php

namespace App\Console\Commands\hm_pass_ss2;

use Illuminate\Console\Command;

use Carbon\Carbon;
use App\Models\hm_pass_ss2\Setting;
use App\Models\hm_pass_ss2\QuestLog;
use App\Jobs\hm_pass_ss2\CheckQuestQueue;

class CheckDailyQuestLog extends Command
{
    protected $description = 'BNS hongmoon pass ss2 event';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_hm_pass_ss2_check_daily_quest_log {--date=}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $date = $this->option('date');
        $logDate = $date ? $date : date('Y-m-d');

        $dt = Carbon::parse($logDate);
        $day = strtolower($dt->shortEnglishDayOfWeek); // mon, tue, wed, thu, fri, sat, sun

        $startDateTime = $logDate . ' 00:00:00';
        $endDateTime = $logDate . ' 23:59:59';

        QuestLog::with('quest', 'member')
            ->where('uid', '!=', 0)
            ->whereNotNull('quest_id')
            ->whereHas('quest', function ($query) use ($day) {
                $query->where('quest_period', 'daily')
                    ->where('day', $day);
            })
            ->where('status', 'pending')
            ->where('log_date', $logDate)
            ->chunkById(1000, function ($questLogs) use ($startDateTime, $endDateTime) {
                foreach ($questLogs as $questLog) {
                    $this->info("BNS Hongmoon pass season 2 : Check Daily Quest Log UID: " . $questLog->uid . " , Username : " . $questLog->member->username . " , Quest Log ID : " . $questLog->id . " , Quest Title : " . $questLog->quest->quest_title);

                    CheckQuestQueue::dispatch($questLog, $startDateTime, $endDateTime)->onQueue('bns_hm_pass_ss2_check_daily_quest_log');
                }
            });
    }
}
