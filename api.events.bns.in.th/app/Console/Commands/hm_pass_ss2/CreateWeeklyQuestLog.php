<?php

namespace App\Console\Commands\hm_pass_ss2;

use Illuminate\Console\Command;

use App\Models\hm_pass_ss2\Setting;
use App\Models\hm_pass_ss2\Member;
use App\Models\hm_pass_ss2\Week;
use App\Jobs\hm_pass_ss2\CreateWeeklyQuestQueue;
use Carbon\Carbon;

class CreateWeeklyQuestLog extends Command
{
    protected $description = 'BNS hongmoon pass ss2 event';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_hm_pass_ss2_create_weekly_quest_log {--week=}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $weekId = $this->option('week');

        if ($weekId) {
            $week = Week::find($weekId);
        } else {
            $now = Carbon::now()->toDateTimeString();
            $week = Week::where('week_start', '<=', $now)->where('week_end', '>=', $now)->first();
        }

        if (!$week) {
            return $this->error('Week not found');
        }

        Member::where('uid', '!=', 0)
            ->where('char_id', '!=', 0)
            ->chunk(1000, function ($members) use ($week) {
                foreach ($members as $member) {
                    $this->info("BNS Hongmoon pass season 2 : Create Weekly Quest Log UID: " . $member->uid . " , Username : " . $member->username . " , Character : " . $member->char_name);

                    CreateWeeklyQuestQueue::dispatch($member, $week->id)->onQueue('bns_hm_pass_ss2_create_weekly_quest_log');
                }
            });
    }
}
