<?php

namespace App\Console\Commands\buffet_v2;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Test\buffet_v2\Quest as ModelQuest;
use App\Models\Test\buffet_v2\QuestStat as ModelQuestStat;
use App\Models\Test\buffet_v2\Member as ModelMember;

class TestCountQuestCompleted extends Command
{
    protected $description = 'BNS Buffet V2 Count Quest Completed';
    // event start - end
    // for test
    //private $eventStartDatetime = '2020-08-04 00:00:00';
    // for live
    private $eventStartDatetime = '2020-08-12 12:00:00';
    private $eventEndDatetime   = '2020-09-01 23:59:59';
    private $baseApi            = 'http://api.apps.garena.in.th';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_test_buffet_v2_count_quest_completed {skip=0} {take=1}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ts1 = time();
        $this->doJob();
        $ts2 = time();
        $this->info("script-time " . date('H:i:s', $ts1) . " - " . date('H:i:s', $ts2));
    }

    private function doJob()
    {
        $params = $this->arguments();

        // number of member check
        $skip = (int)($params['skip'] ?? 0);
        $take = (int)($params['take'] ?? 10);
        if (Carbon::now() > Carbon::parse($this->eventEndDatetime)) {
            $this->info("End of event : do not run command.");
            return;
        }
        if (Carbon::now() < Carbon::parse($this->eventStartDatetime)) {
            $this->info("Event not start yet : do not run command.");
            return;
        }
        $this->info("Preparing to run command BNS Buffet V2 Count Completing Quest.");
        $memberPool             = ModelMember::where('character_id', '!=', 0)->skip($skip)->take($take)->get()->toArray();
        $questPool              = ModelQuest::get()->toArray();
        $hashmq                 = [];
        foreach ($memberPool as $member) {
            foreach ($questPool as $quest) {
                $key    = $member['uid'] . ':' . $quest['quest_id'];
                $hashmq[$key] = [
                    'member' => $member,
                    'quest' => $quest
                ];
            }
        }
        $battleFieldApi = [];
        foreach ($hashmq as $key => $mq) {
            $hashmq[$key]['quest_completed_count'] = null;
            $member = $mq['member'];
            $quest  = $mq['quest'];
            // call api to get data quest id [1,2,3,4,10,11,12,13,14]
            if (in_array($quest['quest_id'], [1, 2, 3, 4, 10, 11, 12, 13, 14])) {
                $resp = json_decode($this->post_api($this->baseApi, [
                    'key_name'      => 'bns',
                    'service'       => 'quest_completed',
                    'char_id'       => $member['character_id'],
                    'quest_id'      => $quest['bns_quest_id'],
                    'start_time'    => $this->eventStartDatetime,
                    'end_time'      => $this->eventEndDatetime
                ]), true);
                if (isset($resp['response']['quest_completed_count'])) {
                    $hashmq[$key]['quest_completed_count'] = $resp['response']['quest_completed_count'];
                }
            } else if (in_array($quest['quest_id'], [5, 6, 7])) {
                // single api
                $battleFieldApi[$member['uid']] = $member['character_id'];
            } else if ($quest['quest_id'] == 8) {
                $resp = json_decode($this->post_api($this->baseApi, [
                    'key_name'      => 'bns',
                    'service'       => 'events',
                    'subservice'    => 'get_1v1_history',
                    'char_id'       => $member['character_id'],
                    'start_time'    => $this->eventStartDatetime,
                    'end_time'      => $this->eventEndDatetime,
                    'user_id'       => $member['ncid']
                ]), true);
                if (isset($resp['response'])) {
                    $hashmq[$key]['quest_completed_count'] = collect($resp['response'])->filter(function ($value, $key) {
                        return $value['type'] == '1';
                    })->count();
                }
            } else if ($quest['quest_id'] == 9) {
                $resp = json_decode($this->post_api($this->baseApi, [
                    'key_name'      => 'bns',
                    'service'       => 'events',
                    'subservice'   => 'get_3v3_history',
                    'char_id'       => $member['character_id'],
                    'start_time'    => $this->eventStartDatetime,
                    'end_time'      => $this->eventEndDatetime,
                    'user_id'       => $member['ncid']
                ]), true);
                if (isset($resp['response'])) {
                    $hashmq[$key]['quest_completed_count'] = collect($resp['response'])->filter(function ($value, $key) {
                        return $value['type'] == '4';
                    })->count();
                }
            }
        }
        // เฉพาะ quest 5,6,7 เป็น api แบบ รวม
        foreach ($battleFieldApi as $uid => $char_id) {
            $resp = json_decode($this->post_api($this->baseApi, [
                'key_name'      => 'bns',
                'service'       => 'get_battlefield_results',
                'char_id'       => $char_id,
                'start_time'    => $this->eventStartDatetime,
                'end_time'      => $this->eventEndDatetime
            ]), true);
            if (isset($resp['response']) && count($resp['response']) > 0) {
                foreach ($resp['response'] as $record) {
                    $key = null;
                    if ($record['type'] == 1) {
                        $key = $uid . ':5';
                    } else if ($record['type'] == 2) {
                        $key = $uid . ':6';
                    } else if ($record['type'] == 3) {
                        $key = $uid . ':7';
                    }
                    if ($key != null) {
                        $hashmq[$key]['quest_completed_count'] = $record['total_count'];
                    }
                }
            }
        }
        // save result to storage
        $saving_data_set = [];
        foreach ($hashmq as $key => $record) {
            if(!isset($record['quest_completed_count']) || (int)$record['quest_completed_count'] < 1){
                continue;
            }
            $saving_data_set[] = $this->buildBuffetV2QuestStatDataSet($record['member'], $record['quest'], $record['quest_completed_count']);
        }
        $this->saveQuestStat($saving_data_set);
        $this->info("End run command BNS Buffet V2 Count Completing Quest.");
    }
    private function saveQuestStat(array $payload)
    {
        $runing         = 0;
        $count_payload = count($payload);
        foreach ($payload as $record) {
            $wh = [
                ['uid', $record['uid']],
                ['quest_id', $record['quest_id']]
            ];
            try {
                if (count(ModelQuestStat::where($wh)->get()->toArray()) > 0) {
                    ModelQuestStat::where($wh)->update($record);
                } else {
                    ModelQuestStat::create($record);
                }
                $this->info('save completed (' . (++$runing) . '/' . $count_payload . ')');
            } catch (\Exception $e) {
                $this->info("run command BNS Buffet V2 Mongo Save some data Error!!!");
            }
        }
    }
    private function buildBuffetV2QuestStatDataSet($member, $quest, $countCompleted = 0): ?array
    {
        return [
            'quest_id'       => (int)$quest['quest_id'],
            'type'           => (int)$quest['type'],
            'bns_quest_id'   => (int)$quest['bns_quest_id'],
            'quest_name'     => (string)$quest['quest_name'],
            'group_no'       => (int)$quest['group_no'],
            'character_id'   => (int)$member['character_id'],
            'uid'            => (float)$member['uid'],
            'character_name' => (string)$member['character_name'],
            'completed_count' => (int)$countCompleted,
        ];
    }
    private function post_api($url, $params = [], $headers = [])
    {
        try {
            if (is_array($params)) {
                $data = http_build_query($params);
            } else {
                $data = $params;
            }
            // dd($url, $params, $headers);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url); // set url
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            if (empty($headers) === FALSE) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            }
            if (empty($data) === FALSE) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
            $resp = curl_exec($ch);
            if (curl_errno($ch)) {
                return null;
            }
            curl_close($ch);
            return $resp;
        } catch (\Exception $e) {
            $this->info('Post data error');
            return null;
        }
    }
}
