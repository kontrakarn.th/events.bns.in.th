<?php

namespace App\Console\Commands\battle_field;

use App\Models\battle_field\Cron;
use App\Models\battle_field\Member;
use App\Models\battle_field\QuestLog;

use App\Jobs\battle_field\UpdateQuestLogs;

use Carbon\Carbon;
use Illuminate\Console\Command;

class Check extends Command
{
    protected $description = 'BNS Battle Field 2020 Check';
    private $endTime = '2020-05-27 00:00:00';


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_battle_field_check';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $logs = QuestLog::whereDateLog('2020-05-06')
            ->where('coin', '>', 0)
            ->get();
        foreach ($logs as $log) {
            $start = Carbon::parse($log->created_at)->toDateTimeString();
            $end   = '2020-05-06 23:59:59';
            echo "$start - $end \n";
        }
    }

}
