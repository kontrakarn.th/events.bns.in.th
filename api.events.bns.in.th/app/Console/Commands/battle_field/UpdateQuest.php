<?php

namespace App\Console\Commands\battle_field;

use App\Models\battle_field\Cron;
use App\Models\battle_field\Member;
use App\Models\battle_field\QuestLog;

use App\Jobs\battle_field\UpdateQuestLogs;

use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateQuest extends Command
{
    protected $description = 'BNS Battle Field 2020 Update Quest';
    private $endTime = '2020-05-27 00:00:00';


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_battle_field_quest_update {skip=0} {take=1}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        if (Carbon::now() > Carbon::parse($this->endTime)) {
            $this->info("End of event : do not run command.");
            return;
        }

        $this->info("Preparing to run command BNS Battle Field 2020 Update quest.");

        // special if time is 0-2 am.
        $special = false;
        if (Carbon::now()->hour >= 0 && Carbon::now()->hour <= 1) {
            $special = true;
        }

        // If Cron run before 00:30am will update yesterday.
        if (Carbon::now()->toDateTimeString() < Carbon::now()->format("Y-m-d 00:29:59")) {
            $date    = Carbon::now()->subDay(1)->toDateString();
            $special = false;
        } else {
            $date = Carbon::now()->toDateString();
        }

        $start = $date.' 00:00:00';
        $end   = $date.' 23:59:59';

        $skip = $this->argument('skip');
        $take = $this->argument('take');

        $members = Member::where('character_id', '!=', null)
            ->where('character_name', '!=', null)
            ->take((int) $take)
            ->skip((int) $skip)
            ->get();

        if (count($members) > 0) {
            foreach ($members as $member) {
                if (empty($member->id) || empty($member->character_id)) {
                    continue;
                }

                // check created_at same date
                $start_api = $start;
                $end_api   = $end;
                if (Carbon::parse($member->created_at)->toDateString() === $date) {
                    $start_api = Carbon::parse($member->created_at)->toDateTimeString();
                }

                $member->last_update = Carbon::now()->toDateTimeString();
                $member->save();

                $this->info($member->uid." of ".$date." add to queue update quest. ($start_api, $end_api) special $special");
                UpdateQuestLogs::dispatch($member, $date, $start_api, $end_api,
                    $special)->onQueue('bns_battle_field_quests_update_queue');
            }
        }
    }

}
