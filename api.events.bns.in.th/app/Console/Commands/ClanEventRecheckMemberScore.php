<?php

namespace App\Console\Commands;

use App\Models\clan_event\Member;
use App\Models\clan_event\QuestMemberLog;

use Carbon\Carbon;
use Illuminate\Console\Command;

class ClanEventRecheckMemberScore extends Command
{

    protected $description = 'BNS Clan event recheck member score';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_clan_event_recheck_member_score';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $members = Member::where('char_id', '!=', 0)
            ->where('clan_id', '!=', 0)
            ->where('world_id', '!=', 0)
            ->where('current_member', 'yes')
            ->get();

        if (count($members) > 0) {
            foreach ($members as $memberKey=>$member) {

                $this->info(($memberKey+1).". Start Check Member Score UID : ".$member->uid);

                $questMemberLog = QuestMemberLog::where('uid', $member->uid)
                                                ->where('char_id', $member->char_id)
                                                ->get();

                $sum = 0;
                if (count($questMemberLog) > 0){
                    $sum = $questMemberLog->sum('total_points');
                }else{
                    $sum = 0;
                }

                $update = Member::where('uid', $member->uid)
                                ->update([
                                    'total_points' => $sum,
                                ]);
                if($update){
                    $this->info("Member score is updated!");
                }else{
                    $this->info("Can not updated member score!");
                }

                $this->info(($memberKey+1).". End Check Member Score UID : ".$member->uid);
                $this->info("----------------------------------------------------------------------");

            }
        }
    
        
    }

}