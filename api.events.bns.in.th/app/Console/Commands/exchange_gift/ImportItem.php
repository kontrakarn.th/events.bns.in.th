<?php

namespace App\Console\Commands\exchange_gift;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\exchange_gift\Items;

class ImportItem extends Command
{

    protected $description = 'Import Item Exchange Gift';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns:exchange_gift:import_item';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {
        $row=0;
        $handle = fopen(public_path("exchange_item.csv"),"r");
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            // dd($data);
            if($row==0){
                $row++;
                continue;
            }
            $item = new Items;
            $item->no=$row;
            $item->item_type="object";
            $item->product_id=(int)$data[1];
            $item->product_title=$data[0];
            $item->product_quantity=(int)$data[2];
            $item->price=(int)$data[3];
            $item->save();

            $this->info($row);
            $row++;

         }
        fclose($handle);
        $this->info("finish");
    }

}
