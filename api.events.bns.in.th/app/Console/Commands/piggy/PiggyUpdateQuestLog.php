<?php

namespace App\Console\Commands\piggy;

use App\Jobs\piggy\PiggyQuestLogQueue as Queue;

use App\Models\piggy\Member;
use App\Models\piggy\Quest;
use App\Models\piggy\QuestDailyLog;

use Carbon\Carbon;
use Illuminate\Console\Command;

class PiggyUpdateQuestLog extends Command
{

    protected $description = 'BNS Piggy event';
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_piggy_quests_update {skip=0} {take=1}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
    }

    private function init()
    {

        $log_date = date('Y-m-d');

        $skip = $this->argument('skip');
        $take = $this->argument('take');

        $memberQuests = QuestDailyLog::where('uid', '!=', 0)
                                        ->where('char_id', '!=', 0)
                                        ->where('quest_code', '!=', 0)
                                        ->where('log_date', $log_date)
                                        ->where('status', 'pending')
                                        ->take((int)$take)
                                        ->skip((int)$skip)
                                        ->get();

        if(count($memberQuests) > 0){
            foreach($memberQuests as $quest){

                if (empty($quest->uid) || empty($quest->char_id) || empty($quest->quest_id) || empty($quest->quest_code)) {
                    continue;
                }

                $this->info("PiggyEvent : Check Quest UID : ".$quest->uid." , Quest ID : ".$quest->quest_id." , Quest : ".$quest->quest_code." , Quest : ".$quest->quest_title);

                Queue::dispatch(
                    $quest->uid, $quest->quest_id, $quest->quest_code, $log_date
                )->onQueue('bns_piggy_quests_update_queue');

            }
        }

    }

}