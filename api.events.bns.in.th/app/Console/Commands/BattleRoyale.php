<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use Carbon\Carbon;
use Storage;
use Cache;
use DB;

use App\Models\battle_royale\TournamentBattleRoyale;
use App\Models\battle_royale\TournamentAppliedCharacters;
// use App\Models\battle_royale\TournamentBattleRoyale;

class BattleRoyale extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bns_save_battle_royale';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save data';

    /**
     * Query start end
     *
     * @var string
     */
    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $joinedDateCondition = '2017-06-20 23:59:59';
    private $levelCondition = 20;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $this->testSendItem();
        $this->savelog();
        // $this->startClanLevelUpSendItem();
    }
    private function savelog()
    {
        echo "\n";
        echo "-----------  Start ---------";
        echo "\n";
        $character = TournamentAppliedCharacters::all();
        $start_date = date("Y-m-d", strtotime("-1 day"));
        $date_now = date("Y-m-d", strtotime("-1 day"));
        foreach ($character as $ch) {
            $player_name = $ch->character_name;
            $player_class = $ch->character_class;
            $character_id = $ch->character_id;
            $team_id = $ch->team_id;
            $uid = $ch->uid;
            $tournament_id =  $ch->tournament_id;

            $body = [
                'key_name' => 'bns',
                'service' => 'get_battleroyale_results',
                'char_id' =>   $character_id,
                'start_time' => $start_date . ' 00:00:01',
                'end_time' => $date_now . ' 23:59:59',
            ];
            $header = [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Accept' => 'application/json'
            ];
            $battle_info =  $this->post_api($this->baseApi, $body, $header);
            $battle_info = json_decode($battle_info);
            if ($battle_info->status) {
                foreach ($battle_info->response as $bi) {
                    $model = TournamentBattleRoyale::where(['character_id' => $character_id, 'play_time' => $bi->time])->first();
                    if (!$model) {
                        $model = new TournamentBattleRoyale;
                        $model->tournament_id = $tournament_id;
                        $model->kill = $bi->kill_count;
                        $model->play_time = $bi->time;
                        $model->rank =  $bi->rank_at_end;
                        $model->team_id = $team_id;
                        $model->uid = $uid;
                        $model->character_id = $character_id;
                        $model->player_name = $player_name;
                        $model->player_class = $player_class;
                        $model->save();
                    }
                }
                echo $character_id . " " . $player_name . "\n";
            }
        }
        echo "\n";
        echo "-----------  end ---------";
        echo "\n";
    }

    private function testSendItem()
    {
        $clanMembers = DB::connection('events_bns')
            ->table('clan_levelup_members_all_server_log')
            ->select('*')
            ->where('uid', 321571544)
            ->groupBy('uid')
            ->orderBy('clan_level', 'DESC')
            ->orderBy('level', 'DESC')
            ->get();
        // dd($clanMembers);

        if (count($clanMembers) > 0) {

            echo "\n";
            echo "-----------  Start Send Items ----------";
            echo "\n";

            $no = 1;
            foreach ($clanMembers as $member) {
                // check get item history
                if ($this->checkGetItemHistory($member->uid, $member->ncid) === FALSE) {

                    $packageId = 0;
                    $packageId = $this->getPackageId($member->clan_level);

                    // set items
                    $packData = [];
                    $packData = $this->getPackage($packageId);

                    echo "\n";
                    echo "--------------------------------------------------";
                    echo "\n";
                    echo "\n";
                    echo "\n";
                    echo "no : " . $no;
                    echo "\n";
                    echo "\n";
                    echo "Send Item for UID : " . $member->uid;
                    echo "\n";
                    echo "\n";
                    echo "Username : " . $member->username;
                    echo "\n";
                    echo "\n";
                    echo "NCID : " . $member->ncid;
                    echo "\n";
                    echo "\n";
                    echo "Character : " . $member->character_name;
                    echo "\n";
                    echo "\n";
                    echo "Clan  : " . $member->clan_name;
                    echo "\n";
                    echo "\n";
                    echo "Clan Level  : " . $member->clan_level;
                    echo "\n";
                    echo "\n";

                    $SendItemLogArr = [
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'character_id' => $member->character_id,
                        'character_name' => $member->character_name,
                        'level' => $member->level,
                        'job' => $member->job,
                        'grade' => $member->grade,
                        'joined_at' => $member->joined_at,
                        'world_id' => $member->world_id,
                        'clan_id' => $member->clan_id,
                        'clan_name' => $member->clan_name,
                        'clan_level' => $member->clan_level,
                        'clan_type' => $member->clan_type,
                        'clan_faction' => $member->clan_faction,
                        'status' => 'pending',
                        'goods_data' => json_encode($packData)
                    ];
                    $SendItemLogData = $this->addSenditemLog($SendItemLogArr);

                    $send_result_raw = $this->dosendItem($member->ncid, $packData);
                    /*$send_result_raw = '{
                                            "status": true,
                                            "response": {
                                                "purchase_id": "3149503",
                                                "purchase_status": "4"
                                            }
                                        }';*/
                    $send_result = json_decode($send_result_raw);
                    if (is_null($send_result) == FALSE && is_object($send_result) && $send_result->status) {
                        $this->updateSendItemLog($SendItemLogData['id'], [
                            'send_item_status' => $send_result->status ?: FALSE,
                            'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                            'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                            'status' => 'success'
                        ]);
                    } else {
                        $this->updateSendItemLog($SendItemLogData['id'], [
                            'send_item_status' => isset($send_result->status) ? $send_result->status : FALSE,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                            'status' => 'unsuccess'
                        ]);
                    }

                    echo "\n";
                    echo "--------------------------------------------------";
                    echo "\n";
                    echo "\n";
                }

                if ($no % 100 == 0) {
                    echo "\n";
                    echo "\n";
                    echo "----------------  Sleep 1 sec  ----------------";
                    echo "\n";
                    echo "\n";
                    sleep(1);
                }

                $no++;
            }

            echo "\n";
            echo "-----------  End Send Items ----------";
            echo "\n";
        }
    }

    private function startClanLevelUpSendItem()
    {
        $allClanMembers = $this->getAllClanMembers();

        if (count($allClanMembers) > 0) {

            echo "\n";
            echo "-----------  Start Send Items ----------";
            echo "\n";

            $no = 1;
            foreach ($allClanMembers as $member) {
                // check get item history
                if ($this->checkGetItemHistory($member->uid, $member->ncid) === FALSE) {

                    $packageId = 0;
                    $packageId = $this->getPackageId($member->clan_level);

                    // set items
                    $packData = [];
                    $packData = $this->getPackage($packageId);

                    echo "\n";
                    echo "--------------------------------------------------";
                    echo "\n";
                    echo "\n";
                    echo "\n";
                    echo "no : " . $no;
                    echo "\n";
                    echo "\n";
                    echo "Send Item for UID : " . $member->uid;
                    echo "\n";
                    echo "\n";
                    echo "Username : " . $member->username;
                    echo "\n";
                    echo "\n";
                    echo "NCID : " . $member->ncid;
                    echo "\n";
                    echo "\n";
                    echo "Character : " . $member->character_name;
                    echo "\n";
                    echo "\n";
                    echo "Clan  : " . $member->clan_name;
                    echo "\n";
                    echo "\n";
                    echo "Clan Level  : " . $member->clan_level;
                    echo "\n";
                    echo "\n";

                    $SendItemLogArr = [
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'character_id' => $member->character_id,
                        'character_name' => $member->character_name,
                        'level' => $member->level,
                        'job' => $member->job,
                        'grade' => $member->grade,
                        'joined_at' => $member->joined_at,
                        'world_id' => $member->world_id,
                        'clan_id' => $member->clan_id,
                        'clan_name' => $member->clan_name,
                        'clan_level' => $member->clan_level,
                        'clan_type' => $member->clan_type,
                        'clan_faction' => $member->clan_faction,
                        'status' => 'pending',
                        'goods_data' => json_encode($packData)
                    ];
                    $SendItemLogData = $this->addSenditemLog($SendItemLogArr);

                    $send_result_raw = $this->dosendItem($member->ncid, $packData);
                    /* $send_result_raw = '{
                                            "status": true,
                                            "response": {
                                                "purchase_id": "3149503",
                                                "purchase_status": "4"
                                            }
                                        }';*/
                    $send_result = json_decode($send_result_raw);
                    if (is_null($send_result) == FALSE && is_object($send_result) && $send_result->status) {
                        $this->updateSendItemLog($SendItemLogData['id'], [
                            'send_item_status' => $send_result->status ?: FALSE,
                            'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                            'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                            'status' => 'success'
                        ]);
                    } else {
                        $this->updateSendItemLog($SendItemLogData['id'], [
                            'send_item_status' => isset($send_result->status) ? $send_result->status : FALSE,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                            'status' => 'unsuccess'
                        ]);
                    }

                    echo "\n";
                    echo "--------------------------------------------------";
                    echo "\n";
                    echo "\n";
                }

                if ($no % 100 == 0) {
                    echo "\n";
                    echo "\n";
                    echo "----------------  Sleep 1 sec  ----------------";
                    echo "\n";
                    echo "\n";
                    sleep(1);
                }

                $no++;
            }

            echo "\n";
            echo "-----------  End Send Items ----------";
            echo "\n";
        }
    }

    private function getAllClanMembers()
    {
        $clanMembers = DB::connection('events_bns')
            ->table('clan_levelup_members_all_server_log')
            ->select('*')
            ->groupBy('uid')
            ->orderBy('clan_level', 'DESC')
            ->orderBy('level', 'DESC')
            ->get();

        return $clanMembers;
    }

    // if true = this uid/ncid have received item before
    private function checkGetItemHistory($uid = null, $ncid = null)
    {
        if (empty($uid) && empty($ncid)) {
            return false;
        }

        $checkGetItem = DB::connection('events_bns')
            ->table('clan_levelup_send_item_log')
            // ->select('*')
            ->where('uid', '=', $uid)
            ->where('ncid', '=', $ncid)
            ->count();

        return $checkGetItem >= 1;
    }

    private function getLevel($uid)
    {
        $resp = (array) $this->doGetGameInfo($uid);
        if ($resp['status']) {
            $max_level = collect($resp['response'])->max('level') ?: 0;
            return $max_level;
        } else {
            return 0;
        }
    }

    private function getPackageId($clanLevel)
    {
        if (empty($clanLevel)) {
            return false;
        }

        if ($clanLevel >= 7 && $clanLevel <= 9) {
            return 1;
        } elseif ($clanLevel >= 10 && $clanLevel <= 12) {
            return 2;
        } elseif ($clanLevel >= 13 && $clanLevel <= 14) {
            return 3;
        } elseif ($clanLevel == 15) {
            return 4;
        }
    }

    private function addSenditemLog($arr)
    {
        return SendItemLog::create($arr);
    }

    protected function updateSendItemLog($log_id, $arr)
    {
        return SendItemLog::where('id', $log_id)->update($arr);
    }

    private function dosendItem(string $ncid, array $goodsData, string $txnId = '')
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'sending item from garena for clan level up event.',
            'goods' => json_encode($goodsData)
        ];
        if (empty($txnId) === FALSE) {
            $data['payment_completed_key'] = $txnId;
        }
        return $this->post_api($this->baseApi, $data);
    }

    private function getPackage($pack_id = 0)
    {
        $goods_data = null;
        // package 1 for clan level 7-9
        if ($pack_id == 1) {
            $goods_data = [
                [
                    'goods_id' => 291,
                    'purchase_quantity' => 1,
                    'purchase_amount' => 0,
                    'category_id' => 40
                ]
            ];
            // package 2 for clan level 10-12
        } elseif ($pack_id == 2) {
            $goods_data = [
                [
                    'goods_id' => 292,
                    'purchase_quantity' => 1,
                    'purchase_amount' => 0,
                    'category_id' => 40
                ]
            ];
            // package 3 for clan level 13-14
        } elseif ($pack_id == 3) {
            $goods_data = [
                [
                    'goods_id' => 292,
                    'purchase_quantity' => 2,
                    'purchase_amount' => 0,
                    'category_id' => 40
                ]
            ];
            // package 4 for clan level 15
        } elseif ($pack_id == 4) {
            $goods_data = [
                [
                    'goods_id' => 294,
                    'purchase_quantity' => 1,
                    'purchase_amount' => 0,
                    'category_id' => 40
                ],
                [
                    'goods_id' => 289,
                    'purchase_quantity' => 1,
                    'purchase_amount' => 0,
                    'category_id' => 40
                ]
            ];
        }

        return $goods_data;
    }

    /**
     * Simple POST request
     */
    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === FALSE) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === FALSE) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }
}
