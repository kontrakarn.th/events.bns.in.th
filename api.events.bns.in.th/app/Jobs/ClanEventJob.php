<?php

namespace App\Jobs;

use App\Models\clan_event\Member;
use App\Models\clan_event\Quest;
use App\Models\clan_event\QuestDailyLog;
use App\Models\clan_event\QuestMemberLog;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class ClanEventJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $member = null;
    protected $quest = null;
    protected $start = null;
    protected $end = null;

    /**
     * Create a new job instance.
     *
     * @param  Member  $member
     * @param  Quest  $quest
     */
    public function __construct(Member $member, Quest $quest, $start, $end)
    {
        $this->member = $member;
        $this->quest  = $quest;
        $this->start  = $start;
        $this->end    = $end;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->member != null && $this->quest != null && $this->start != null && $this->end != null) {

            $log_date  = Carbon::parse($this->start)->format("Y-m-d");
            $daily_log = QuestDailyLog::where('uid', $this->member->uid)
                ->where('char_id', $this->member->char_id)
                ->where('quest_code', $this->quest->quest_code)
                ->where('log_date', $log_date)
                ->first();

            if (!$daily_log) {
                $daily_log               = new QuestDailyLog();
                $daily_log->uid          = $this->member->uid;
                $daily_log->username     = $this->member->username;
                $daily_log->ncid         = $this->member->ncid;
                $daily_log->char_id      = $this->member->char_id;
                $daily_log->world_id     = $this->member->world_id;
                $daily_log->clan_id      = $this->member->clan_id;
                $daily_log->quest_title  = $this->quest->quest_title;
                $daily_log->quest_code   = $this->quest->quest_code;
                $daily_log->quest_points = $this->quest->quest_points;
                $daily_log->quest_type   = $this->quest->quest_type;
                $daily_log->save();
            }

            // API Get kill and calculate
            if ($this->quest->quest_type == "quest") {

                $quest = $this->apiQuestCompleted($this->member->char_id, $this->quest->quest_code, $this->start,
                    $this->end);
                if ($quest->status === true) {
                    $completed_count = $quest->response->quest_completed_count;
                } else {
                    $completed_count = 0;
                }

            } elseif ($this->quest->quest_type == "boss_kill") {

                $quest = $this->apiMonKills($this->member->char_id, $this->quest->quest_code, $this->start, $this->end);
                if ($quest->status === true) {
                    $completed_count = $quest->response->kill_count;
                } else {
                    $completed_count = 0;
                }
            } else {
                $completed_count = 0;
            }

            // check limit
            if ($this->quest->quest_limit != 0) {
                $questMemberLog = QuestMemberLog::where('uid', $this->member->uid)
                    ->where('char_id', $this->member->char_id)
                    ->where('quest_code', $this->quest->quest_code)
                    ->first();

                if (!$questMemberLog) {
                    $has = 0;
                } else {
                    $has = $questMemberLog->completed_count;
                }

                // มากกว่า limit แล้ว ไม่ให้เพิ่ม
                if ($has >= $this->quest->quest_limit) {
                    $completed_count = 0;
                }

                // หากทำเพิ่มแล้วมากกว่า limit ให้เอาจำนวนที่เหลือแค่นั้นพอ
                if (($has + $completed_count) > $this->quest->quest_limit) {
                    $completed_count = ($this->quest->quest_limit - $has);
                }
            }

            $daily_log->completed_count    = $completed_count;
            $daily_log->total_points       = ($completed_count * $this->quest->quest_points);
            $daily_log->log_date           = $log_date;
            $daily_log->log_date_timestamp = Carbon::now()->timestamp;
            $daily_log->last_ip            = $this->getIP();
            $daily_log->save();

            // Summary by Quest
            $this->summaryPoints();


        }
    }

    private function summaryPoints()
    {
//        echo "-----------------------------\n";
//        echo "UID : ".$this->member->uid."\n";
//        echo "Quest : ".$this->quest->quest_code."\n";

        $questDailies = QuestDailyLog::where('uid', $this->member->uid)
            ->where('char_id', $this->member->char_id)
            ->where('quest_code', $this->quest->quest_code)
            ->get();

        if (count($questDailies) > 0) {

            $questMemberLog = QuestMemberLog::where('uid', $this->member->uid)
                ->where('char_id', $this->member->char_id)
                ->where('quest_code', $this->quest->quest_code)
                ->first();

            $complete_counts = $questDailies->sum('completed_count');

            if ($questMemberLog) {
                $questMemberLog->completed_count = $complete_counts;
                $questMemberLog->total_points    = ($complete_counts * $this->quest->quest_points);
                $questMemberLog->last_ip         = $this->getIP();
                $questMemberLog->save();
            }

//            echo "Total Point : ".$questMemberLog->total_points."\n";
        }


    }

    // --- APIs ---

    private function apiQuestCompleted($char_id = null, $quest_id = null, $start_date = null, $end_date = null)
    {
        if (empty($char_id) || empty($quest_id) || empty($start_date) || empty($end_date)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $char_id,
            'quest_id'   => $quest_id,
            'start_time' => $start_date,
            'end_time'   => $end_date,
        ]);

        return json_decode($data);
    }

    private function apiMonKills($char_id = null, $mon_id = null, $start_date = null, $end_date = null)
    {
        if (empty($char_id) || empty($mon_id) || empty($start_date) || empty($end_date)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'get_mon_kills_by_mon_id',
            'char_id'    => $char_id,
            'mon_id'     => $mon_id,
            'start_time' => $start_date,
            'end_time'   => $end_date,
        ]);

        return json_decode($data);
    }

    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }


}
