<?php

namespace App\Jobs\newbie_free_package;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;

class SendItemToUserQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $ncid;
    protected $goods_data;
    protected $savehistoryid;
    protected $historycontroller;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;
    /**
     * Delete the job if its models no longer exist.
     *
     * @var bool
     */
    public $deleteWhenMissingModels = true;

    public function __construct($ncid,$goods_data,$savehistoryid,$historycontroller)
    {
        $this->ncid = $ncid;
        $this->goods_data = $goods_data;
        $this->savehistoryid = $savehistoryid;
        $this->historycontroller = $historycontroller;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $send_result_raw = $this->apiSendItem($this->ncid, $this->goods_data);
        $send_result = json_decode($send_result_raw);
        //$savereport = $this->reportcontroller->SaveReport($dist[$accum[$randomnum]]['report_column'],"Get_Product");
        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
         $savehistory = $this->historycontroller->UpdateHistory($this->savehistoryid,
                                        $send_result->status ?: false,
                                        $send_result->response->purchase_id ?: 0,
                                        $send_result->response->purchase_status ?: 0,
                                        'success'
                                        );
         return true;
     } else {
         $savehistory = $this->historycontroller->UpdateHistory($this->savehistoryid,
                                        $send_result->status ?: false,
                                        $send_result->response->purchase_id ?: 0,
                                        $send_result->response->purchase_status ?: 0,
                                        'unsuccess'
                                        );

         return false;
     }
    }

    private function apiSendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Sending item from garena events BNS Mystic.',
            'goods'                => json_encode($goodsData),
        ];
        return $this->post_api($this->baseApi, $data);
    }
    protected function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        // dd($url, $params, $headers);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === FALSE) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === FALSE) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }
}
