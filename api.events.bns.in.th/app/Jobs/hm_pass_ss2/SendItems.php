<?php

namespace App\Jobs\hm_pass_ss2;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\hm_pass_ss2\Member;
use App\Models\hm_pass_ss2\ItemLog;
use Illuminate\Support\Facades\Log;

class SendItems implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $member;
    protected $goodsData;
    protected $sendItemLogIds;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Delete the job if its models no longer exist.
     *
     * @var bool
     */
    public $deleteWhenMissingModels = true;

    public function __construct(Member $member, $goodsData, $sendItemLogIds)
    {
        $this->member = $member;
        $this->goodsData = $goodsData;
        $this->sendItemLogIds = $sendItemLogIds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $send_result_raw = $this->apiSendItem($this->member->ncid, $this->goodsData);
        $send_result = json_decode($send_result_raw);

        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            ItemLog::whereIn('id', $this->sendItemLogIds)
                ->where('uid', $this->member->uid)
                ->update([
                    'send_item_status' => $send_result->status ?: false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                    'status' => 'success'
                ]);

            Log::info('Send Items Successfully UID: ' . $this->member->uid . ' Username: ' . $this->member->username . ' NCID: ' . $this->member->ncid);
        } else {
            ItemLog::whereIn('id', $this->sendItemLogIds)
                ->where('uid', $this->member->uid)
                ->update([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                ]);

            Log::error('Send Items Unsuccess UID: ' . $this->member->uid . ' Username: ' . $this->member->username . ' NCID: ' . $this->member->ncid);
        }
    }

    private function apiSendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Sending item from garena events BNS Hongmoon Pass Season 2.',
            'goods'                => json_encode($goodsData),
        ];
        return $this->post_api($this->baseApi, $data);
    }

    protected function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        // dd($url, $params, $headers);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === FALSE) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === FALSE) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }
}
