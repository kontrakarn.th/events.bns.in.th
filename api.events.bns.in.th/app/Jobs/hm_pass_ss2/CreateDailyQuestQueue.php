<?php

namespace App\Jobs\hm_pass_ss2;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;

use App\Models\hm_pass_ss2\Member;
use App\Models\hm_pass_ss2\Quest;
use App\Models\hm_pass_ss2\QuestLog;

class CreateDailyQuestQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $member;
    protected $logDate;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Delete the job if its models no longer exist.
     *
     * @var bool
     */
    public $deleteWhenMissingModels = true;

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct(Member $member, $logDate)
    {
        $this->member = $member;
        $this->logDate = $logDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dt = Carbon::parse($this->logDate);
        $day = strtolower($dt->shortEnglishDayOfWeek); // mon, tue, wed, thu, fri, sat, sun
        $dailyQuests = Quest::where('quest_period', 'daily')
            ->where('day', $day)
            ->get();

        foreach ($dailyQuests as $quest) {
            QuestLog::firstOrCreate(
                [
                    'uid' => $this->member->uid,
                    'quest_id' => $quest->id,
                    'log_date' => $this->logDate
                ],
                [
                    'char_id' => $this->member->char_id,
                    'completed_count' => 0,
                    'status' => 'pending',
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]
            );
        }
    }

    protected function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }
}
