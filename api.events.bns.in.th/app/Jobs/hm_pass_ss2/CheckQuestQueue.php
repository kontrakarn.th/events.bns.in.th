<?php

namespace App\Jobs\hm_pass_ss2;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\hm_pass_ss2\QuestLog;
use App\Traits\ApiServiceTrait;
use Illuminate\Support\Facades\Log;
use App\Models\hm_pass_ss2\Member;
use Illuminate\Support\Facades\DB;

class CheckQuestQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ApiServiceTrait;

    protected $questLog;
    protected $startDateTime;
    protected $endDateTime;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Delete the job if its models no longer exist.
     *
     * @var bool
     */
    public $deleteWhenMissingModels = true;

    public function __construct(QuestLog $questLog, $startDateTime, $endDateTime)
    {
        $this->questLog = $questLog;
        $this->startDateTime = $startDateTime;
        $this->endDateTime = $endDateTime;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = $this->getResponseByQuestType($this->questLog, $this->startDateTime, $this->endDateTime);
        $this->handleApiResponse($this->questLog, $response);
    }

    private function handleApiResponse($questLog, $response)
    {
        if (!$response || $response->status === false) {
            Log::info('API unsuccess UID: ' . $questLog->uid . ' Quest Log ID: ' . $questLog->id . ' Log Date: ' . $questLog->log_date);
            return false;
        }

        $completedCount = $this->getCompletedCountByQuestType($questLog, $response);

        if ($completedCount >= $questLog->quest->quest_limit) {
            DB::transaction(function () use ($questLog) {
                $questLog->completed_count = $questLog->quest->quest_limit;
                $questLog->status = 'success';
                $questLog->quest_complete_datetime = date('Y-m-d H:i:s');
                $questLog->save();

                $member = Member::where('uid', $questLog->member->uid)->first();

                if ($member->hm_pass_lvl < 60 && $member->unlocked_hm_pass_lvl < 60) {
                    $member->total_points = $member->total_points + $questLog->quest->quest_points;
                    $increasedCurrentPoints = $member->current_points + $questLog->quest->quest_points;

                    if ($increasedCurrentPoints / 200 >= 1) {
                        $member->current_points = $increasedCurrentPoints % 200;
                        $member->hm_pass_lvl = $member->hm_pass_lvl + 1;
                        if ($member->is_unlocked == 1 || $member->is_adv_unlocked == 1) {
                            $member->unlocked_hm_pass_lvl = $member->unlocked_hm_pass_lvl + 1;
                        }
                    } else {
                        $member->current_points = $increasedCurrentPoints;
                    }
                }

                $member->save();
            });
        } else {
            $questLog->completed_count = $completedCount;
            $questLog->quest_complete_datetime = date('Y-m-d H:i:s');
            $questLog->save();
        }
    }
}
