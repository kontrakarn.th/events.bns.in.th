<?php

namespace App\Jobs\piggy;

use App\Models\piggy\Member;
use App\Models\piggy\Quest;
use App\Models\piggy\QuestDailyLog;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PiggyQuestLogQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $uid = null;
    protected $quest_id = null;
    protected $quest_code = null;
    protected $log_date = '';

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct($uid,$quest_id,$quest_code,$log_date)
    {
        $this->uid = $uid;
        $this->quest_id = $quest_id;
        $this->quest_code = $quest_code;
        $this->log_date = $log_date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->uid != null && $this->quest_id != null && $this->quest_code != null && $this->log_date != '') {

            $member = Member::where('uid', $this->uid)->first();

            $dailyLogCount = QuestDailyLog::where('uid', $this->uid)
                                        ->where('quest_id', $this->quest_id)
                                        ->where('quest_code', $this->quest_code)
                                        ->where('log_date', $this->log_date)
                                        ->where('status', 'pending')
                                        ->count();
            if($dailyLogCount > 0) {

                $dailyLog = QuestDailyLog::where('uid', $this->uid)
                                    ->where('quest_id', $this->quest_id)
                                    ->where('quest_code', $this->quest_code)
                                    ->where('log_date', $this->log_date)
                                    ->where('status', 'pending')
                                    ->first();

                $startDate = $this->log_date.' '.(date('H:i:s', strtotime($dailyLog->created_at)));
                $endDate = $this->log_date.' 23:59:59';
                $questApi = $this->apiQuestCompleted($member->char_id, $dailyLog->quest_code, $startDate, $endDate);
                if($questApi && $questApi->status === true) {

                    $completed_count = 0;
                    $completed_count = (int)$questApi->response->quest_completed_count;

                    if($completed_count > 0){

                        $dailyLog->status       = 'success';
                        $dailyLog->quest_complete_datetime = date('Y-m-d H:i:s');
                        $dailyLog->updated_at   = date('Y-m-d H:i:s');
                        $dailyLog->save();
                        
                    }

                }

            }

        }
    }

    private function apiQuestCompleted($char_id = null, $quest_id = null, $start_date = null, $end_date = null)
    {
        if (empty($char_id) || empty($quest_id) || empty($start_date) || empty($end_date)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $char_id,
            'quest_id'   => $quest_id,
            'start_time' => $start_date,
            'end_time'   => $end_date,
        ]);

        return json_decode($data);
    }

    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }


}