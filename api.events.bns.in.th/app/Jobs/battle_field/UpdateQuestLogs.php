<?php

namespace App\Jobs\battle_field;

use Carbon\Carbon;
use Log;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\battle_field\Cron;
use App\Models\battle_field\Member;
use App\Models\battle_field\QuestLog;
use App\Models\battle_field\SendItemLog;

class UpdateQuestLogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $member;
    protected $date;
    protected $start;
    protected $end;
    protected $special;

    protected $coin = [
        'quest_success_1' => 50,
        'quest_success_2' => 50,
        'quest_success_3' => 35,
        'quest_success_4' => 50,
        'quest_success_5' => 50,
        'quest_success_6' => 35,
        'quest_success_7' => 50,
        'quest_success_8' => 50,
        'quest_success_9' => 35,
        'peak'            => 25,
    ];

    /**
     * Create a new job instance.
     *
     * @param $member
     * @param $date
     * @param $start
     * @param $end
     * @param $special
     */
    public function __construct(Member $member, $date, $start, $end, $special)
    {
        $this->member  = $member;
        $this->date    = $date;
        $this->start   = $start;
        $this->end     = $end;
        $this->special = $special;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (empty($this->member->uid) || empty($this->member->character_id)) {
            return;
        }

        $logs = QuestLog::whereUid($this->member->uid)
            ->whereCharId($this->member->character_id)
            ->whereDateLog($this->date)
            ->first();

        if (!$logs) {
            $logs            = new QuestLog();
            $logs->uid       = $this->member->uid;
            $logs->username  = $this->member->username;
            $logs->ncid      = $this->member->ncid;
            $logs->char_id   = $this->member->character_id;
            $logs->char_name = $this->member->character_name;
            $logs->date_log  = $this->date;
            $logs->save();
        }

        $arr     = [];
        $is_play = false;

        $check = $this->apiGetBattleFieldResults((int) $logs->char_id, $this->start, $this->end);

        if (is_null($check) == false && is_object($check) && $check->status) {
            $response = json_encode($check->response);

            if (!empty($check->response[0])) {
                $type = (int) $check->response[0]->type;
                if ($type == 1) {
                    $arr['quest_success_1'] = (int) $check->response[0]->win_count;
                    $arr['quest_success_3'] = (int) $check->response[0]->total_count;
                } elseif ($type == 2) {
                    $arr['quest_success_4'] = (int) $check->response[0]->win_count;
                    $arr['quest_success_6'] = (int) $check->response[0]->total_count;
                } elseif ($type == 3) {
                    $arr['quest_success_7'] = (int) $check->response[0]->win_count;
                    $arr['quest_success_9'] = (int) $check->response[0]->total_count;
                }
            }

            if (!empty($check->response[1])) {
                $type = (int) $check->response[1]->type;
                if ($type == 1) {
                    $arr['quest_success_1'] = (int) $check->response[1]->win_count;
                    $arr['quest_success_3'] = (int) $check->response[1]->total_count;
                } elseif ($type == 2) {
                    $arr['quest_success_4'] = (int) $check->response[1]->win_count;
                    $arr['quest_success_6'] = (int) $check->response[1]->total_count;
                } elseif ($type == 3) {
                    $arr['quest_success_7'] = (int) $check->response[1]->win_count;
                    $arr['quest_success_9'] = (int) $check->response[1]->total_count;
                }
            }

            if (!empty($check->response[2])) {
                $type = (int) $check->response[2]->type;
                if ($type == 1) {
                    $arr['quest_success_1'] = (int) $check->response[2]->win_count;
                    $arr['quest_success_3'] = (int) $check->response[2]->total_count;
                } elseif ($type == 2) {
                    $arr['quest_success_4'] = (int) $check->response[2]->win_count;
                    $arr['quest_success_6'] = (int) $check->response[2]->total_count;
                } elseif ($type == 3) {
                    $arr['quest_success_7'] = (int) $check->response[2]->win_count;
                    $arr['quest_success_9'] = (int) $check->response[2]->total_count;
                }
            }
//
//            // ลมหวน
//            $arr['quest_success_1'] = (int) $check->response[0]->win_count;
//            $arr['quest_success_3'] = (int) $check->response[0]->total_count;
//
//            // เบลูก้า
//            $arr['quest_success_4'] = (int) $check->response[1]->win_count;
//            $arr['quest_success_6'] = (int) $check->response[1]->total_count;
//
//            // จักรกล
//            $arr['quest_success_7'] = (int) $check->response[2]->win_count;
//            $arr['quest_success_9'] = (int) $check->response[2]->total_count;

            // get kill
            $zones = [4850, 4852, 4851];
            foreach ($zones as $i => $zone) {
                $kill = $this->apiGetBattleFieldPlayerKilled((int) $logs->char_id, $zone, $this->start, $this->end);
                if (is_null($kill) == false && is_object($kill) && $kill->status) {
                    $kill = (int) $kill->response->kill_count;
                } else {
                    $kill = 0;
                }

                if ($i == 0) {
                    $arr['quest_success_2'] = (int) $kill;
                } elseif ($i == 1) {
                    $arr['quest_success_5'] = (int) $kill;
                } elseif ($i == 2) {
                    $arr['quest_success_8'] = (int) $kill;
                }
            }

            $is_play = true;

        } else {
            $response = "not_found";
        }

        $arr['is_play']        = $is_play;
        $arr['last_update']    = Carbon::now()->toDateTimeString();
        $arr['response']       = $response;
        $arr['api_check_time'] = $this->start.' - '.$this->end;
        $logs->update($arr);

        $this->summaryCoin($logs);
        $this->summaryMember();
    }

    public function summaryCoin($logs)
    {
        $arr  = [];
        $coin = 0;
        // calcurate coin

        $coin += ((int) $logs->quest_success_1 * (int) $this->coin['quest_success_1']);
        $coin += ((int) $logs->quest_success_2 * (int) $this->coin['quest_success_2']);
        $coin += ((int) $logs->quest_success_3 * (int) $this->coin['quest_success_3']);
        $coin += ((int) $logs->quest_success_4 * (int) $this->coin['quest_success_4']);
        $coin += ((int) $logs->quest_success_5 * (int) $this->coin['quest_success_5']);
        $coin += ((int) $logs->quest_success_6 * (int) $this->coin['quest_success_6']);
        $coin += ((int) $logs->quest_success_7 * (int) $this->coin['quest_success_7']);
        $coin += ((int) $logs->quest_success_8 * (int) $this->coin['quest_success_8']);
        $coin += ((int) $logs->quest_success_9 * (int) $this->coin['quest_success_9']);

        $peak_time_coin = 0;
        if ($this->special === true) {
            $peak_time_coin        += ((int) $logs->quest_success_3 * $this->coin['peak']);
            $peak_time_coin        += ((int) $logs->quest_success_6 * $this->coin['peak']);
            $peak_time_coin        += ((int) $logs->quest_success_9 * $this->coin['peak']);
            $coin                  += $peak_time_coin;
            $arr['peak_time_coin'] = $peak_time_coin;
            $arr['peak_3']         = $logs->quest_success_3;
            $arr['peak_6']         = $logs->quest_success_6;
            $arr['peak_9']         = $logs->quest_success_9;
        } else {
            $coin += $logs->peak_time_coin;
        }

        // free of the day
        $coin += (int) $logs->quest_free;

        $arr['coin']        = $coin;
        $arr['last_update'] = Carbon::now()->toDateTimeString();
        $logs->update($arr);
    }

    private function apiGetBattleFieldResults($charId, $start, $end)
    {
        $resp = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'get_battlefield_results',
            'char_id'    => $charId,
            'start_time' => $start,
            'end_time'   => $end,
        ]);
        return json_decode($resp);
    }

    private function apiGetBattleFieldPlayerKilled($charId, $zone_id, $start, $end)
    {
        $resp = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'get_battlefield_player_killed_count',
            'char_id'    => $charId,
            'zone_id'    => $zone_id,
            'start_time' => $start,
            'end_time'   => $end,
        ]);
        return json_decode($resp);
    }

    protected function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        // dd($url, $params, $headers);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }

    private function summaryMember()
    {
        if ($this->member && $this->member->character_id != 0) {

            $sum_coin = (int) QuestLog::whereUid($this->member->uid)
                ->whereCharId($this->member->character_id)
                ->sum('coin');

            $sum_coin_used = (int) SendItemLog::whereUid($this->member->uid)
                ->whereStatus('success')
                ->sum('coin');

            $this->member->quest_success_1 = (int) QuestLog::whereUid($this->member->uid)
                ->whereCharId($this->member->character_id)
                ->sum('quest_success_1');
            $this->member->quest_success_2 = (int) QuestLog::whereUid($this->member->uid)
                ->whereCharId($this->member->character_id)
                ->sum('quest_success_2');
            $this->member->quest_success_3 = (int) QuestLog::whereUid($this->member->uid)
                ->whereCharId($this->member->character_id)
                ->sum('quest_success_3');
            $this->member->quest_success_4 = (int) QuestLog::whereUid($this->member->uid)
                ->whereCharId($this->member->character_id)
                ->sum('quest_success_4');
            $this->member->quest_success_5 = (int) QuestLog::whereUid($this->member->uid)
                ->whereCharId($this->member->character_id)
                ->sum('quest_success_5');
            $this->member->quest_success_6 = (int) QuestLog::whereUid($this->member->uid)
                ->whereCharId($this->member->character_id)
                ->sum('quest_success_6');
            $this->member->quest_success_7 = (int) QuestLog::whereUid($this->member->uid)
                ->whereCharId($this->member->character_id)
                ->sum('quest_success_7');
            $this->member->quest_success_8 = (int) QuestLog::whereUid($this->member->uid)
                ->whereCharId($this->member->character_id)
                ->sum('quest_success_8');
            $this->member->quest_success_9 = (int) QuestLog::whereUid($this->member->uid)
                ->whereCharId($this->member->character_id)
                ->sum('quest_success_9');
            $this->member->quest_free      = (int) QuestLog::whereUid($this->member->uid)
                ->whereCharId($this->member->character_id)
                ->sum('quest_free');

            $this->member->coin        = $sum_coin;
            $this->member->coin_used   = $sum_coin_used;
            $this->member->last_update = Carbon::now()->toDateTimeString();
            $this->member->save();
        }
    }

}
