<?php

namespace App\Jobs\hardmode\test;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Test\hardmode\Member;
use App\Models\Test\hardmode\Quest;
use App\Models\Test\hardmode\QuestDailyLog;

class CreateQuestDailyQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $id = null, $logDate;

    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct($id, $logDate)
    {
        $this->id = $id;
        $this->logDate = $logDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->id) {

            $logDate = $this->logDate;

            $member = Member::where('id', $this->id)->first();

            $dailyQuest = Quest::all();

            if (count($dailyQuest) > 0) {

                foreach ($dailyQuest as $quest) {

                    // check daily quest exitst
                    $checkExist = 0;
                    $checkExist = QuestDailyLog::where('uid', $member->uid)->where('quest_id', $quest->quest_id)->where('log_date', $logDate)->count();
                    if ($checkExist <= 0) {
                        QuestDailyLog::create([
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'char_id' => $member->char_id,
                            'char_name' => $member->char_name,
                            'quest_id' => $quest->quest_id,
                            'quest_dungeon' => $quest->quest_dungeon,
                            'quest_title' => $quest->quest_title,
                            'quest_code' => $quest->quest_code,
                            'quest_type' => $quest->quest_type,
                            'quest_limit' => $quest->quest_limit,
                            'quest_points' => $quest->quest_points,
                            'image' => $quest->image,
                            'completed_count' => 0,
                            'claimed_count' => 0,
                            'status' => 'pending',
                            'claim_status' => 'pending',
                            'log_date' => date('Y-m-d'),
                            'log_date_timestamp' => time(),
                            'last_ip' => $this->getIP(),
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
                    }
                }
            }
        }
    }

    protected function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }
}
