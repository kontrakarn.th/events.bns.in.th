<?php

namespace App\Jobs\hardmode\test;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Test\hardmode\QuestDailyLog;
use Illuminate\Support\Facades\Log;

class CheckQuestDailyQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $id = null, $logDate;

    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct($id, $logDate)
    {
        $this->id = $id;
        $this->logDate = $logDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->id) {
            $dailyLog = QuestDailyLog::where('id', $this->id)->first();

            if (isset($dailyLog) === true) {

                $startDateTime = $this->logDate . ' 00:00:00';
                $endDateTime = $this->logDate . ' 23:59:59';

                $questApi = $this->apiBossKillByHardDifficulty($dailyLog->char_id, $dailyLog->quest_code, $startDateTime, $endDateTime);
               
                if ($questApi && $questApi->status === true) {
                    $completed_count = 0;
                    $completed_count = (int) $questApi->response->kill_count;

                    if ($completed_count >= $dailyLog->quest_limit) {
                        $dailyLog->completed_count = $dailyLog->quest_limit;
                        $dailyLog->status       = 'success';
                        $dailyLog->quest_complete_datetime = date('Y-m-d H:i:s');
                        $dailyLog->updated_at   = date('Y-m-d H:i:s');
                        $dailyLog->save();
                    } else {
                        $dailyLog->completed_count = $completed_count;
                        $dailyLog->quest_complete_datetime = date('Y-m-d H:i:s');
                        $dailyLog->updated_at   = date('Y-m-d H:i:s');
                        $dailyLog->save();
                    }
                } else {
                    Log::info('API unsuccess UID: ' . $dailyLog->uid . ' Quest Code: ' . $dailyLog->quest_code . ' Log Date: ' . $dailyLog->log_date);
                }
            }
        }
    }

    private function apiBossKillByHardDifficulty($char_id = null, $boss_id = null, $start_date = null, $end_date = null)
    {
        if (empty($char_id) || empty($boss_id) || empty($start_date) || empty($end_date)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'get_boss_killed_by_difficulty',
            'char_id'    => $char_id,
            'boss_id'   => $boss_id,
            'difficulty_id' => 12, // hard
            'start_time' => $start_date,
            'end_time'   => $end_date,
        ]);

        return json_decode($data);
    }

    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }
}
