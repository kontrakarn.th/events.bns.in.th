<?php

namespace App\Jobs\codm;

use Carbon\Carbon;
use Log;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\codm\zombieday\ReSendItem as SendLogs;
// use App\Models\codm\Test\zombieday\ReSendItem as SendLogs;

class ZombieDaySendItemQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;

    private $clientId = 'c0817e79-bb55-40a2-a55a-f234a1cb0480';
    private $iegams_key = 'IEGAMS-269101-317455';

    private $areaId = 1;
    private $tips = "tha";

    private $eventName = 'zombieday';
    private $apiUrl = "https://th.game.proxy.garenanow.com/game/codm/tidy/v1/senditem/";

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id=$id;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $send_data=SendLogs::find($this->id);
        $headers = [
            'Client-ID:'.$this->clientId,
            'Content-Type:application/json'
        ];

        $iegamsKey = $this->setIegame($this->iegams_key);
        if($send_data->status=='queue'){
        
            $package_id=$send_data->package_id;
            $randString = $this->generateRandomString(6);

            $dateTime = date('mdHis');
            $tid = 'AMS-' . $this->eventName . '-' . $dateTime . '-' . $randString . '-' . $iegamsKey;
            $payLoad = [
                'Package' => (int) $package_id,
                'Serial' => $tid,
            ];

            $api_send=$this->apiUrl.$send_data->tencent_id."?tips=".$this->tips."&area=".$this->areaId;
            $responseSendReward = $this->SendPost($api_send, $headers, json_encode($payLoad));

            if(isset($responseSendReward)){

                $response_decode=json_decode($responseSendReward);

                if($response_decode->Ret==0 && $response_decode->PackageIdCnt!=""){
                    //success
                    $send_data->response=$this->strConvert($response_decode->Msg);
                    $send_data->tid=$tid;
                    $send_data->iegams_key=$this->iegams_key;
                    $send_data->status='success';
                    $send_data->updated_at=Carbon::now();
                    $send_data->save();


                }else{
                    //unsuccess
                    $send_data->response=$response_decode->Msg." (2)";
                    $send_data->tid=$tid;
                    $send_data->iegams_key=$this->iegams_key;
                    $send_data->status='fail';
                    $send_data->updated_at=Carbon::now();
                    $send_data->save();
                }

            }else{
                //unsuccess
                $send_data->response="api error";
                $send_data->tid=$tid;
                $send_data->iegams_key=$this->iegams_key;
                $send_data->status='fail';
                $send_data->updated_at=Carbon::now();
                $send_data->save();

            }
        }
    }

    private function strConvert($str=""){
        $str = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
        }, $str);

        return $str;
    }

    private function setIegame($iegamsKey = ''){

        return str_replace("IEGAMS-","",$iegamsKey);

    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function SendPost($url, $headers, $payload) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        $res = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);

        // $res = json_decode($res);

        return $res;
    }

}