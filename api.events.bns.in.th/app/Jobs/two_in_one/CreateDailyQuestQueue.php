<?php

namespace App\Jobs\two_in_one;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\two_in_one\Member;
use App\Models\two_in_one\Quest;
use App\Models\two_in_one\QuestLog;

class CreateDailyQuestQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $member;
    protected $logDate;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Delete the job if its models no longer exist.
     *
     * @var bool
     */
    public $deleteWhenMissingModels = true;

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct(Member $member, $logDate)
    {
        $this->member = $member;
        $this->logDate = $logDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dailyQuests = Quest::whereIn('quest_type', ['area', 'daily_challenge'])->get();

        foreach ($dailyQuests as $quest) {
            QuestLog::firstOrCreate(
                [
                    'uid' => $this->member->uid,
                    'quest_id' => $quest->id,
                    'log_date' => $this->logDate
                ],
                [
                    'char_id' => $this->member->char_id,
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]
            );
        }
    }

    protected function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }
}
