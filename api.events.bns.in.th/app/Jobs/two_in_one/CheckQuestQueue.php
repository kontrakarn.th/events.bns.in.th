<?php

namespace App\Jobs\two_in_one;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\two_in_one\QuestLog;
use App\Traits\ApiServiceTrait;
use Illuminate\Support\Facades\Log;

class CheckQuestQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ApiServiceTrait;

    protected $questLog;
    protected $startDateTime;
    protected $endDateTime;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Delete the job if its models no longer exist.
     *
     * @var bool
     */
    public $deleteWhenMissingModels = true;

    public function __construct(QuestLog $questLog, $startDateTime, $endDateTime)
    {
        $this->questLog = $questLog;
        $this->startDateTime = $startDateTime;
        $this->endDateTime = $endDateTime;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = $this->getResponseByQuestType($this->questLog, $this->startDateTime, $this->endDateTime);
        $this->handleApiResponse($this->questLog, $response);
    }

    private function handleApiResponse($questLog, $response)
    {
        if (!$response || $response->status === false) {
            Log::info('API unsuccess UID: ' . $questLog->uid . ' Quest Log ID: ' . $questLog->id . ' Log Date: ' . $questLog->log_date);
            return false;
        }

        $completedCount = $this->getCompletedCountByQuestType($questLog, $response);

        if ($completedCount >= $questLog->quest->quest_limit) {
            $questLog->completed_count = $questLog->quest->quest_limit;
            $questLog->status = 'success';
            $questLog->quest_complete_datetime = date('Y-m-d H:i:s');
            $questLog->save();
        } else {
            $questLog->completed_count = $completedCount;
            $questLog->quest_complete_datetime = date('Y-m-d H:i:s');
            $questLog->save();
        }
    }
}
