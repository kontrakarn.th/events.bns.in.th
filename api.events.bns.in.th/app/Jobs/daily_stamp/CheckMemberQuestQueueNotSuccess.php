<?php

namespace App\Jobs\daily_stamp;

use App\Models\daily_stamp\Events            as DB_EVENT;
use App\Models\daily_stamp\Member;
use App\Models\daily_stamp\MemberQuest;
use App\Models\daily_stamp\ItemHistory;
use App\Models\daily_stamp\SendItemLog;
use App\Models\daily_stamp\DeductLog;
use App\Models\daily_stamp\Quest;
use App\Models\daily_stamp\QuestDailyLog;
use App\Models\daily_stamp\Reward;
use App\Models\daily_stamp\Utils as Utils;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;

class CheckMemberQuestQueueNotSuccess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $id = null;

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->id) {
            $memberQuest = MemberQuest::where('id', $this->id)
              ->first();

            $event = DB_EVENT::where('eventname', Utils::KEY_EVENT)->first();

            if (isset($memberQuest) === true) {
                $date = date('Y-m-d');
                $datetime = date('Y-m-d H:i:s');

                if ($event->test == true) {
                    $date = $event->current_date;
                    $datetime = $event->current_date . ' 00:00:00';
                }

                // echo date('Y-m-d H:i:s') . ' ' . $datetime . "\n";
                // echo (time() . ' ' . strtotime($datetime) . "\n");

                if (strtotime($datetime) > strtotime($memberQuest->end_quest)) {
                    $memberQuest->status = 'not_success';
                    $memberQuest->updated_at = date('Y-m-d H:i:s');
                    $memberQuest->save();

                    $questDailyLog = QuestDailyLog::where('uid', $memberQuest->uid)
                      ->where('member_quest_id', $memberQuest->id)
                      ->get();

                    foreach ($questDailyLog as $value) {
                        $value->status = 'not_success';
                        $value->updated_at = date('Y-m-d H:i:s');
                        $value->save();
                    }
                }

            }
        }
    }

    protected function getIP() {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }


}
