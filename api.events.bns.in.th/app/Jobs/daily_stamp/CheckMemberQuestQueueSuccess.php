<?php

namespace App\Jobs\daily_stamp;

use App\Models\daily_stamp\Events            as DB_EVENT;
use App\Models\daily_stamp\Member;
use App\Models\daily_stamp\MemberQuest;
use App\Models\daily_stamp\ItemHistory;
use App\Models\daily_stamp\SendItemLog;
use App\Models\daily_stamp\DeductLog;
use App\Models\daily_stamp\Quest;
use App\Models\daily_stamp\QuestDailyLog;
use App\Models\daily_stamp\Reward;
use App\Models\daily_stamp\Utils as Utils;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckMemberQuestQueueSuccess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $id = null;

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->id) {
            $memberQuest = MemberQuest::where('id', $this->id)
              ->first();

            if (isset($memberQuest) === true) {
                $questDailyLog = QuestDailyLog::where('uid', $memberQuest->uid)
                  ->where('member_quest_id', $memberQuest->id)
                  ->get();

                $totalQuestSuccess = 0;

                foreach ($questDailyLog as $value) {
                    if ($value->status === 'success') {
                        $totalQuestSuccess++;
                    }
                }

                if ($memberQuest->quest_index === 24) {
                    if ($totalQuestSuccess >= 1) {
                        $memberQuest->status = 'success';
                        $memberQuest->updated_at = date('Y-m-d H:i:s');
                        $memberQuest->save();
                    }
                } else {
                    if ($totalQuestSuccess == $questDailyLog->count()) {
                        $memberQuest->status = 'success';
                        $memberQuest->updated_at = date('Y-m-d H:i:s');
                        $memberQuest->save();
                    }
                }
            }
        }
    }

    protected function getIP() {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }


}
