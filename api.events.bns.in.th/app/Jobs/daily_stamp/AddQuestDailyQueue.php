<?php

namespace App\Jobs\daily_stamp;

use App\Models\daily_stamp\Events            as DB_EVENT;
use App\Models\daily_stamp\Member;
use App\Models\daily_stamp\MemberQuest;
use App\Models\daily_stamp\ItemHistory;
use App\Models\daily_stamp\SendItemLog;
use App\Models\daily_stamp\DeductLog;
use App\Models\daily_stamp\Quest;
use App\Models\daily_stamp\QuestDailyLog;
use App\Models\daily_stamp\Reward;
use App\Models\daily_stamp\Utils as Utils;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AddQuestDailyQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $id = null;

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->id) {
            $member = Member::where('id', $this->id)->first();
            $event = DB_EVENT::where('eventname', Utils::KEY_EVENT)->first();

            if (isset($member) === true && isset($event) === true) {
                  $date = date('Y-m-d');
                  $datetime = date('Y-m-d H:i:s');

                  if ($event->test == true) {
                      $date = $event->current_date;
                      $datetime = $event->current_date . ' ' . date('H:i:s');
                  }

                  $memberQuest = MemberQuest::where('uid', $member->uid)
                    ->where('date', $date)
                    ->first();

                  $questList = Quest::orderBy('id', 'ASC')->get();

                  if ($questList->count() > 0) {
                      if (isset($memberQuest) === false) {
                          $logMemberQuest = new MemberQuest;
                          $logMemberQuest->uid = $member->uid;
                          $logMemberQuest->quest_index = Utils::QUEST_DETAIL[$date]['index'];
                          $logMemberQuest->date = $date;
                          $logMemberQuest->status = 'pending';
                          $logMemberQuest->claim_free_status = 'pending';
                          $logMemberQuest->claim_diamond_status = 'pending';
                          $logMemberQuest->last_ip = $this->getIP();
                          $logMemberQuest->start_quest = $datetime;
                          $logMemberQuest->end_quest = $date . ' 23:59:59';
                          $saved = $logMemberQuest->save();

                          $questDailyLog = QuestDailyLog::where('uid', $member->uid)
                            ->where('log_date', $date)
                            ->orderBy('quest_id', 'ASC')
                            ->get();

                          if ($questDailyLog->count() == 0) {
                              $questDetail = Utils::QUEST_DETAIL[$date]['quest'];

                              if (isset($questDetail) === true) {
                                  for ($i = 0; $i < count($questDetail); $i++) {
                                      $questVal =  $questList[$questDetail[$i]];

                                      QuestDailyLog::create([
                                          'uid' => $member->uid,
                                          'username' => $member->username,
                                          'ncid' => $member->ncid,
                                          'char_id' => $member->char_id,
                                          'char_name' => $member->char_name,
                                          'quest_id' => $questVal->id,
                                          'quest_dungeon' => $questVal->quest_dungeon,
                                          'quest_title' => $questVal->quest_title,
                                          'quest_code' => $questVal->quest_code,
                                          'quest_amount' => $questVal->quest_amount,
                                          'quest_type' => 'daily',
                                          'image' => $questVal->image,
                                          'total_quest' => 0,
                                          'status' => 'pending',
                                          'claim_status' => 'pending',
                                          'log_date' => $date,
                                          'log_date_timestamp' => time(),
                                          'start_quest' => $datetime,
                                          'end_quest' => $date . ' 23:59:59',
                                          'member_quest_id' => $logMemberQuest->id,
                                          'last_ip' => $this->getIP(),
                                      ]);
                                  }
                              }
                          }
                      }
                  }
            }

            // $questApi = $this->apiQuestCompleted($dailyLog->char_id, $dailyLog->quest_code, $dailyLog->start_quest, $dailyLog->end_quest);
            // if ($questApi && $questApi->status === true) {
            //
            //     $completed_count = 0;
            //     $completed_count = (int)$questApi->response->quest_completed_count;
            //
            //     if ($completed_count >= $dailyLog->quest_amount) {
            //         $dailyLog->total_quest = $dailyLog->quest_amount;
            //         $dailyLog->status       = 'success';
            //         $dailyLog->quest_complete_datetime = date('Y-m-d H:i:s');
            //         $dailyLog->updated_at   = date('Y-m-d H:i:s');
            //         $dailyLog->save();
            //     } else {
            //         if ($dailyLog->total_quest < $completed_count) {
            //             $dailyLog->total_quest = $completed_count;
            //             $dailyLog->updated_at   = date('Y-m-d H:i:s');
            //             $dailyLog->save();
            //         }
            //     }
            // }
        }
    }

    protected function getIP() {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }


}
