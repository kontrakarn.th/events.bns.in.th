<?php

namespace App\Jobs\daily_stamp;

use App\Models\daily_stamp\Events            as DB_EVENT;
use App\Models\daily_stamp\Member;
use App\Models\daily_stamp\MemberQuest;
use App\Models\daily_stamp\ItemHistory;
use App\Models\daily_stamp\SendItemLog;
use App\Models\daily_stamp\DeductLog;
use App\Models\daily_stamp\Quest;
use App\Models\daily_stamp\QuestDailyLog;
use App\Models\daily_stamp\Reward;
use App\Models\daily_stamp\Utils as Utils;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Jobs\daily_stamp\CheckMemberQuestQueueSuccess;

class QuestLogQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $id = null;

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->id) {
            $dailyLog = QuestDailyLog::where('id', $this->id)->first();

            if (isset($dailyLog) === true) {
                $questApi = $this->apiQuestCompleted($dailyLog->char_id, $dailyLog->quest_code, $dailyLog->start_quest, $dailyLog->end_quest);
                if ($questApi && $questApi->status === true) {

                    $completed_count = 0;
                    $completed_count = (int)$questApi->response->quest_completed_count;

                    if ($completed_count >= $dailyLog->quest_amount) {
                        $dailyLog->total_quest = $dailyLog->quest_amount;
                        $dailyLog->status       = 'success';
                        $dailyLog->quest_complete_datetime = date('Y-m-d H:i:s');
                        $dailyLog->updated_at   = date('Y-m-d H:i:s');
                        $dailyLog->save();

                        CheckMemberQuestQueueSuccess::dispatch($dailyLog->member_quest_id)->onQueue('bns_daily_stamp_check_member_quest_success');
                    } else {
                        if ($dailyLog->total_quest < $completed_count) {
                            $dailyLog->total_quest = $completed_count;
                            $dailyLog->updated_at   = date('Y-m-d H:i:s');
                            $dailyLog->save();
                        }
                    }


                }
            }
        }
    }

    private function apiQuestCompleted($char_id = null, $quest_id = null, $start_date = null, $end_date = null)
    {
        if (empty($char_id) || empty($quest_id) || empty($start_date) || empty($end_date)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $char_id,
            'quest_id'   => $quest_id,
            'start_time' => $start_date,
            'end_time'   => $end_date,
        ]);

        return json_decode($data);
    }

    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }


}
