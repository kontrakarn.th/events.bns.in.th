<?php

namespace App\Jobs\pvp_event;

use App\Models\Test\pvp_event\Member;
use App\Models\Test\pvp_event\Quest;
use App\Models\Test\pvp_event\QuestMemberLog;
use App\Models\Test\pvp_event\QuestDailyLog;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateQuestLogQueueTest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $uid = null;
    protected $quest_code = null;
    protected $log_date = '';

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct($uid,$quest_code,$log_date)
    {
        $this->uid = $uid;
        $this->quest_code = $quest_code;
        $this->log_date = $log_date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if ($this->uid != null && $this->quest_code != null && $this->log_date != '') {

            $member = Member::where('uid', $this->uid)->first();

            $daily_log = QuestDailyLog::where('uid', $this->uid)
                                        ->where('quest_code', $this->quest_code)
                                        ->where('log_date', $this->log_date)
                                        ->first();
            if (!$daily_log) {

                $quest = QuestMemberLog::where('uid', $this->uid)->where('quest_code', $this->quest_code)->first();

                $daily_log                      = new QuestDailyLog;
                $daily_log->uid                 = $member->uid;
                $daily_log->username            = $member->username;
                $daily_log->ncid                = $member->ncid;
                $daily_log->char_id             = $member->char_id;
                $daily_log->quest_title         = $quest->quest_title;
                $daily_log->quest_code          = $quest->quest_code;
                $daily_log->quest_points        = $quest->quest_points;
                $daily_log->quest_type          = $quest->quest_type;
                $daily_log->image               = $quest->image;
                $daily_log->log_date            = $this->log_date;
                $daily_log->log_date_timestamp = strtotime($this->log_date);
                $daily_log->save();
            }

            $completed_count = 0;
            $startDate = $this->log_date.' 00:00:00';
            $endDate = $this->log_date.' 23:59:59';
            $questApi = $this->apiQuestCompleted($member->char_id, $daily_log->quest_code, $startDate, $endDate);
            if ($questApi->status === true) {
                $completed_count = $questApi->response->quest_completed_count;
            } else {
                $completed_count = 0;
            }

            $daily_log->completed_count    = $completed_count;
            $daily_log->total_points       = ($completed_count * $daily_log->quest_points);
            $daily_log->last_ip            = $this->getIP();
            $daily_log->save();

        }

    }

    private function apiQuestCompleted($char_id = null, $quest_id = null, $start_date = null, $end_date = null)
    {
        if (empty($char_id) || empty($quest_id) || empty($start_date) || empty($end_date)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $char_id,
            'quest_id'   => $quest_id,
            'start_time' => $start_date,
            'end_time'   => $end_date,
        ]);

        return json_decode($data);
    }

    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

}