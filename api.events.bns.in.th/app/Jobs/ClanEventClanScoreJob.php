<?php

namespace App\Jobs;

use App\Models\clan_event\ClanRank;
use App\Models\clan_event\Member;
use App\Models\clan_event\Quest;
use App\Models\clan_event\QuestMemberLog;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ClanEventClanScoreJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $clan = null;

    /**
     * Create a new job instance.
     *
     * @param  Member  $member
     * @param  Quest  $quest
     */
    public function __construct(ClanRank $clan)
    {
        $this->clan = $clan;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->clan != null){

            $questMemberLogs = QuestMemberLog::where('clan_id',$this->clan->clan_id)
                ->where('world_id',$this->clan->world_id)
                ->get();

            if (count($questMemberLogs) > 0){
                $sum_total = $questMemberLogs->sum('total_points');
            }else{
                $sum_total = 0;
            }

            $this->clan->total_points = $sum_total;
            $this->clan->save();

//            echo "Cal Point ! : ".$this->clan->clan_name." ".$sum_total."\n";


        }
    }
}
