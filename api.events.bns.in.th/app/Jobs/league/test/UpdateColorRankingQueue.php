<?php

namespace App\Jobs\league\test;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Test\league\Setting;
use App\Models\Test\league\Member;
use App\Models\Test\league\Color;
use App\Models\Test\league\Group;
use App\Models\Test\league\ItemLog;
use App\Models\Test\league\Quest;
use App\Models\Test\league\QuestDailyLog;
use App\Models\Test\league\Reward;

class UpdateColorRankingQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $colors = Color::orderBy('color_id', 'ASC')->get();
        if(count($colors) > 0){

            foreach($colors as $color){

                $total_points = 0;
                $total_points = Member::where('color_id', $color->color_id)->sum('total_points');

                // member top rank
                $playerId = '';
                $playerPoints = 0;
                $palyer = null;
                $palyer = Member::where('is_register', 1)->where('color_id', $color->color_id)->orderBy('rank', 'ASC')->first();
                if($palyer){
                    $playerId = $palyer->id;
                    $playerPoints = number_format($palyer->total_points);
                }

                // update color total points
                Color::where('color_id', $color->color_id)->update([
                    'total_points' => $total_points,
                    'member_top_rank' => $playerId,
                    'member_top_rank_points' => $playerPoints,
                ]);

            }

        }

        // update ranking
        $colorRanking = Color::orderBy('total_points', 'DESC')->orderBy('member_top_rank_points', 'DESC')->get();
        if(count($colorRanking) > 0){

            $rank = 1;
            foreach($colorRanking as $ranking){

                // update color total points
                Color::where('color_id', $ranking->color_id)->update([
                    'color_rank' => $rank,
                ]);

                $rank++;

            }

        }

    }


}