<?php

namespace App\Jobs\league\test;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Test\league\Setting;
use App\Models\Test\league\Member;
use App\Models\Test\league\Color;
use App\Models\Test\league\Group;
use App\Models\Test\league\ItemLog;
use App\Models\Test\league\Quest;
use App\Models\Test\league\QuestDailyLog;
use App\Models\Test\league\Reward;

class CheckSpecialQuestDailyQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $id = null;

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->id) {
            $dailyLog = QuestDailyLog::where('id', $this->id)->first();

            if (isset($dailyLog) === true) {

                $startDateTime = date('Y-m-d H:i:s', strtotime($dailyLog->created_at));
                $endDateTime = $dailyLog->log_date.' 23:59:59';

                $completed_count = 0;

                if($dailyLog->quest_type == 'daily_special_bosskill'){
                    $questApi = $this->apiBossKillsByAlias($dailyLog->char_id, $dailyLog->quest_code, $startDateTime, $endDateTime);

                    if ($questApi && $questApi->status === true) {
                        $completed_count = (int)$questApi->response->kill_count;
                    }
                }else{
                    $questApi = $this->apiQuestCompleted($dailyLog->char_id, $dailyLog->quest_code, $startDateTime, $endDateTime);

                    if ($questApi && $questApi->status === true) {
                        $completed_count = (int)$questApi->response->quest_completed_count;
                    }
                } 

                if ($completed_count >= $dailyLog->quest_limit) {
                    $dailyLog->completed_count = $dailyLog->quest_limit;
                    $dailyLog->status       = 'success';
                    $dailyLog->quest_complete_datetime = date('Y-m-d H:i:s');
                    $dailyLog->updated_at   = date('Y-m-d H:i:s');
                    $dailyLog->save();
                } else {
                    if ($dailyLog->completed_count < $completed_count) {
                        $dailyLog->completed_count = $completed_count;
                        $dailyLog->status       = 'success';
                        $dailyLog->quest_complete_datetime = date('Y-m-d H:i:s');
                        $dailyLog->updated_at   = date('Y-m-d H:i:s');
                        $dailyLog->save();
                    }
                }

            }

        }

    }

    private function apiQuestCompleted($char_id = null, $quest_id = null, $start_date = null, $end_date = null)
    {
        if (empty($char_id) || empty($quest_id) || empty($start_date) || empty($end_date)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $char_id,
            'quest_id'   => $quest_id,
            'start_time' => $start_date,
            'end_time'   => $end_date,
        ]);

        return json_decode($data);
    }

    private function apiBossKillsByAlias($char_id = null, $alias_id = null, $start_date = null, $end_date = null)
    {
        if (empty($char_id) || empty($alias_id) || empty($start_date) || empty($end_date)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'get_mon_kills_by_mon_id',
            'char_id'    => $char_id,
            'mon_id'     => $alias_id,
            'start_time' => $start_date,
            'end_time'   => $end_date,
        ]);

        return json_decode($data);
    }

    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }


}