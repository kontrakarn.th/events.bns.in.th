<?php

namespace App\Jobs\league\test;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Test\league\Setting;
use App\Models\Test\league\Member;
use App\Models\Test\league\Color;
use App\Models\Test\league\Group;
use App\Models\Test\league\ItemLog;
use App\Models\Test\league\Quest;
use App\Models\Test\league\QuestDailyLog;
use App\Models\Test\league\Reward;

class UpdateGroupRankingQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $id = null;

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        if ($this->id) {

            $groupInfo = Group::where('id', $this->id)->first();

            $groupMembers = Member::where('group_id', $groupInfo->group_id)->where('is_register', 1)->orderBy('total_points','DESC')->orderBy('rank_latest_updated', 'DESC')->get();
            if(count($groupMembers) > 0){
                $rank = 1;
                foreach($groupMembers as $member){

                    Member::where('uid', $member->uid)->where('is_register', 1)->update([
                        'rank' => $rank,
                        'rank_previous' => $member->rank,
                    ]);

                    $rank++;
                }
            }

        }

    }


}