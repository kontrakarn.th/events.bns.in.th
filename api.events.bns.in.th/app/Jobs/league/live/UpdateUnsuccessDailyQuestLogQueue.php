<?php

namespace App\Jobs\league\live;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\league\Setting;
use App\Models\league\Member;
use App\Models\league\Color;
use App\Models\league\Group;
use App\Models\league\ItemLog;
use App\Models\league\Quest;
use App\Models\league\QuestDailyLog;
use App\Models\league\Reward;

class UpdateUnsuccessDailyQuestLogQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $id = null;

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->id) {

            $memberQuest = QuestDailyLog::where('id', $this->id)->first();

            if (isset($memberQuest) === true) {

                $date = date('Y-m-d');
                $datetime = date('Y-m-d H:i:s');

                if (strtotime($date.' 00:00:00') > strtotime($memberQuest->log_date.' 23:59:59')) {
                    $memberQuest->status = 'unsuccess';
                    $memberQuest->updated_at = date('Y-m-d H:i:s');
                    $memberQuest->save();
                }

            }

        }

    }


}