<?php

namespace App\Jobs\hey_duo;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\hey_duo\QuestDailyLog;
use Illuminate\Support\Facades\Log;

class CheckQuestDailyLogQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $dailyLog;
    protected $logDate;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Delete the job if its models no longer exist.
     *
     * @var bool
     */
    public $deleteWhenMissingModels = true;

    public function __construct(QuestDailyLog $dailyLog, $logDate)
    {
        $this->dailyLog = $dailyLog;
        $this->logDate = $logDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dailyLog = $this->dailyLog;
        $startDateTime = $this->logDate . ' 00:00:00';
        $endDateTime = $this->logDate . ' 23:59:59';

        if ($dailyLog->quest_type === 'daily_bosskill') {
            $response = $this->apiBossKillByHardDifficulty($dailyLog, $startDateTime, $endDateTime);
        } else if ($dailyLog->quest_type === 'daily_area') {
            $response = $this->apiQuestCompleted($dailyLog, $startDateTime, $endDateTime);
        }

        $this->handleApiResponse($dailyLog, $response);
    }

    private function handleApiResponse($dailyLog, $response)
    {
        if (!$response || $response->status === false) {
            Log::info('API unsuccess UID: ' . $dailyLog->uid . ' Quest Code: ' . $dailyLog->quest_code . ' Log Date: ' . $dailyLog->log_date);
            return false;
        }

        $completed_count = 0;

        if ($dailyLog->quest_type === 'daily_bosskill') {
            $completed_count = (int) $response->response->kill_count;
        } else if ($dailyLog->quest_type === 'daily_area') {
            $completed_count = (int) $response->response->quest_completed_count;
        }

        if ($completed_count >= $dailyLog->quest_limit) {
            $dailyLog->completed_count = $dailyLog->quest_limit;
            $dailyLog->status = 'success';
            $dailyLog->quest_complete_datetime = date('Y-m-d H:i:s');
            $dailyLog->save();
        } else {
            $dailyLog->completed_count = $completed_count;
            $dailyLog->quest_complete_datetime = date('Y-m-d H:i:s');
            $dailyLog->save();
        }
    }

    private function apiBossKillByHardDifficulty($dailyLog, $startDateTime, $endDateTime)
    {
        if (empty($dailyLog) || empty($startDateTime) || empty($endDateTime)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'get_boss_killed_by_difficulty',
            'char_id'    => $dailyLog->char_id,
            'boss_id'   => $dailyLog->quest_code,
            'difficulty_id' => 12, // hard
            'start_time' => $startDateTime,
            'end_time'   => $endDateTime,
        ]);

        return json_decode($data);
    }

    private function apiQuestCompleted($dailyLog, $startDateTime, $endDateTime)
    {
        if (empty($dailyLog) || empty($startDateTime) || empty($endDateTime)) {
            return false;
        }

        $data = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $dailyLog->char_id,
            'quest_id'   => $dailyLog->quest_code,
            'start_time' => $startDateTime,
            'end_time'   => $endDateTime,
        ]);

        return json_decode($data);
    }

    private function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }
}
