<?php

namespace App\Jobs\hey_duo;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\hey_duo\Member;
use App\Models\hey_duo\Quest;
use App\Models\hey_duo\QuestDailyLog;

class CreateQuestDailyQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $member;
    protected $logDate;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Delete the job if its models no longer exist.
     *
     * @var bool
     */
    public $deleteWhenMissingModels = true;

    /**
     * Create a new job instance.
     *
     * @param  quest_code  $quest_code
     */
    public function __construct(Member $member, $logDate)
    {
        $this->member = $member;
        $this->logDate = $logDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dailyQuests = Quest::all();

        foreach ($dailyQuests as $quest) {
            QuestDailyLog::firstOrCreate(
                [
                    'uid' => $this->member->uid,
                    'quest_id' => $quest->quest_id,
                    'log_date' => $this->logDate
                ],
                [
                    'uid' => $this->member->uid,
                    'username' => $this->member->username,
                    'ncid' => $this->member->ncid,
                    'char_id' => $this->member->char_id,
                    'char_name' => $this->member->char_name,
                    'quest_id' => $quest->quest_id,
                    'quest_dungeon' => $quest->quest_dungeon,
                    'quest_title' => $quest->quest_title,
                    'quest_code' => $quest->quest_code,
                    'quest_type' => $quest->quest_type,
                    'quest_limit' => $quest->quest_limit,
                    'quest_points' => $quest->quest_points,
                    'image' => $quest->image,
                    'completed_count' => 0,
                    'claimed_count' => 0,
                    'status' => 'pending',
                    'claim_status' => 'pending',
                    'log_date' => $this->logDate,
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]
            );
        }
    }

    protected function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }
}
