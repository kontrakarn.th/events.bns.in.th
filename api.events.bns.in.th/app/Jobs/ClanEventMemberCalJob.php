<?php

namespace App\Jobs;

use App\Models\clan_event\Member;
use App\Models\clan_event\QuestMemberLog;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ClanEventMemberCalJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $member = null;

    /**
     * Create a new job instance.
     *
     * @param  Member  $member
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->member != null) {

            $questMemberLog = QuestMemberLog::where('uid', $this->member->uid)
                ->where('char_id', $this->member->char_id)
                ->get();

            if (count($questMemberLog) > 0){
                $sum = $questMemberLog->sum('total_points');
            }else{
                $sum = 0;
            }

            $this->member->total_points = $sum;
            $this->member->save();

//            echo "-----------------------------\n";
//            echo "UID Member : ".$this->member->uid."\n";
//            echo "Sum Point : ".$sum."\n";
//            echo "-----------------------------\n";

        }
    }
}
