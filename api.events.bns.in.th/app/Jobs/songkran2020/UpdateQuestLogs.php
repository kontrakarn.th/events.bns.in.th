<?php

namespace App\Jobs\songkran2020;

use Carbon\Carbon;
use Log;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\songkran2020\Cron;
use App\Models\songkran2020\Member;
use App\Models\songkran2020\QuestLog;

class UpdateQuestLogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseApi = 'http://api.apps.garena.in.th';

    protected $member;
    protected $date;
    protected $start;
    protected $end;

    protected $questList = [
        [
            'id'   => 25227,
            'coin' => 50,
            'name' => "อาวุธแห่งการทำลายล้างครั้งสุดท้าย",
        ],
        [
            'id'   => 25222,
            'coin' => 40,
            'name' => "ความทะนงแห่งสายฟ้าที่เหลืออยู่",
        ],
        [
            'id'   => 25215,
            'coin' => 40,
            'name' => "จุดจบของความปรารถนาที่ไม่เป็นจริง",
        ],
        [
            'id'   => 25185,
            'coin' => 35,
            'name' => "ลึกลงไปในกรุสมบัติ",
        ],
        [
            'id'   => 25178,
            'coin' => 35,
            'name' => "ลำนำแห่งชาวเรือ",
        ],
        [
            'id'   => 25147,
            'coin' => 25,
            'name' => "ผู้เฝ้ามองอมตะ",
        ],
        [
            'id'   => 25144,
            'coin' => 25,
            'name' => "เปลวเพลิงที่ไม่มีวันดับ",
        ],
    ];

    /**
     * Create a new job instance.
     *
     * @param $member
     * @param $date
     * @param $start
     * @param $end
     */
    public function __construct(Member $member, $date, $start, $end)
    {
        $this->member = $member;
        $this->date   = $date;
        $this->start  = $start;
        $this->end    = $end;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (empty($this->member->uid) || empty($this->member->character_id)) {
            return;
        }

        $logs = QuestLog::whereUid($this->member->uid)
            ->whereCharId($this->member->character_id)
            ->whereDateLog($this->date)
            ->first();

        if (!$logs) {
            $logs            = new QuestLog();
            $logs->uid       = $this->member->uid;
            $logs->username  = $this->member->username;
            $logs->ncid      = $this->member->ncid;
            $logs->char_id   = $this->member->character_id;
            $logs->char_name = $this->member->character_name;
            $logs->date_log  = $this->date;
            $logs->save();
        }

        $quests = collect($this->questList);
        $arr    = [];
        $coin   = 0;
        foreach ($quests as $key => $quest) {
            $key     = $key + 1;
            $var     = "quest_success_".$key;
            $questId = $quest['id'];
            $check   = $this->apiCheckQuestCompleted($this->member->uid, $this->member->character_id, $questId,
                $this->start, $this->end);
            if (is_null($check) == false && is_object($check) && $check->status) {
                $check = $check->response->quest_completed_count;
            } else {
                $check = 0;
            }

            if ($check >= 1) {
                $coin += (int) $quest['coin'] * $check;
            }
            $arr[$var] = $check;
        }

        $arr['coin'] = $coin;
        $logs->update($arr);

        $arr['start_api'] = $this->start;
        $arr['end_api']   = $this->end;
    }

    private function apiCheckQuestCompleted($uid, $charId, $questId, $start, $end)
    {
        $resp = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $charId,
            'quest_id'   => $questId,
            'start_time' => $start,
            'end_time'   => $end,
        ]);
        return json_decode($resp);
    }

    protected function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        // dd($url, $params, $headers);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === false) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === false) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }

}
