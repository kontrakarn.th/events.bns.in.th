<?php

namespace App\Http\Middleware;

use Closure;

use Response;

/*
    Author : Ekanan Prohsunthorn (First)
    
    Description: Here's a middleware made for SSO, can use in route with name garena.sso
    
    How it's work?
      just put it in the route then non-authen request will got
    
    How to use:
        In Route put 
            Route::group(['middleware' => 'garena.sso'], function() {
                // Your Authen Route
            }); 

        For Controller to retrieve uid and username. It will pass this data on request named userinfo
            $uid = $request->userinfo['uid'];
            $username = $request->userinfo['username'];
*/

class SSOMiddleware
{
    private $timeout = 3600;
    private $appid = 10046;
    private $appkey = '2c14892ba9489f4416a65349d6c46bb5f0a7ee3a8b02bcfc202339ece41f290b';
    private $host = 'https://sso.garena.com';
    private $locale = 'th';
    private $ref = '';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userdata = $this->getUserData();
        if(!isset($userdata['uid']) || !isset($userdata['username'])) {
            return Response::json([
                                'status' => false,
                                'message' => 'กรุณา login ก่อนเข้าใช้งาน'
                            ]);
        }

        // Passed _userinfo into $request in Controller
        $request->merge([
                            'userinfo' => $userdata
                        ]);
        return $next($request);
    }



    protected function getUserData()
    {
        if (isset($_COOKIE['sso_session'])) {
            $sessionKey = $_COOKIE['sso_session'];
            $userData = $this->decryptSessionKey($sessionKey);
            return [
                'uid' => $userData['uid'],
                'username' => $userData['username']
            ];
        }
        return null;
    }

    /*
     * Decrypt the session key
     *
     * char[3] random;
      uint8 platform: 1 - garena;
      uint32 timestamp;
      uint32 uid;
      char[16] username; (‘\0’ right padding)
      uint32 checksum, xor of previous dwords;
     * All fields are little endian
     */

    private function decryptSessionKey($sessionKey)
    {
        $appkey = hex2bin($this->appkey);
        $appkey = substr($appkey, 0, 32);
        $sessionKey = hex2bin($sessionKey);
        if (strlen($sessionKey) % 16 != 0) {
            return '';
        }
        $iv = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"; // \0 * 16
        $plain = $this->decrypt_data($sessionKey, $iv, $appkey);
        $data = unpack('Z3random/Cplatform/Vtimestamp/Vuid/Z16username', $plain);
        return $data;
    }

    /**
     * AES-256 CBC decrypt in PHP (for crying out loud)
     *
     * @param string $data
     * @param string $iv
     * @param string $key
     * @return boolean
     */
    private function decrypt_data($data, $iv, $key)
    {
        $cypher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

        // initialize encryption handle
        if (mcrypt_generic_init($cypher, $key, $iv) != -1) {
            // decrypt
            $decrypted = mdecrypt_generic($cypher, $data);

            // clean up
            mcrypt_generic_deinit($cypher);
            mcrypt_module_close($cypher);

            return $decrypted;
        }

        return false;
    }
}