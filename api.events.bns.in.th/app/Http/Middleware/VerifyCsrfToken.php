<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        '/sso/get_account_info*',
        '/oauth/get_account_info',


        // event path
        '/api/hm_pad/*',
        '/api/topup_nov2018/*',
        '/api/season2_revival/*',
        '/api/hm_battlepass/*',
        '/api/airpay_dec2018/*',
        '/api/moonstone_festival/*',
        '/api/hello_warden/*',
        '/api/happy_xmas2018/*',
        '/api/preregis_warden/*',
        '/api/preorder_warden/*',
        '/api/bns_advice/*',
        '/api/airpay_jan2019/*',
        '/api/lunar_newyear/*',
        '/api/soul_gachapon_valentine/*',
        '/api/wardrobe_package_2019/*',
        '/api/snakeladder/*',
        '/api/new_revival_taecheon/*',
        '/api/gacha5baht/*',
        '/api/hello_summer/*',
        '/api/anniversary_passport/*',
        '/api/topup_may2019/*',
        '/api/revival_may2019/*',
        '/api/mixpetgacha/*',
        '/api/topup_july2019/*',
        '/api/bmjuly/*',
        '/api/buffet/*',
        '/api/bstc2019/*',
        '/api/topup_august2019/*',
        '/api/fightersoul/*',
        '/api/archer_preregister/*',
        '/api/archer_preorder/*',
        '/api/soulgacha_silvermoon/*',
        '/api/archer_support/*',
        '/api/revival_sep2019/*',
        '/api/archer_package/*',
        '/api/greyed_out2019/*',
        '/api/halloween2019/*',
        '/api/hmelite_2019/*',
        '/api/hm_welfare_package/*',
        '/api/bmnov/*',
        '/api/lantern_festival/*',
        '/api/clan_event/*',
        '/api/gacha5baht_nov/*',
        '/api/chain_package/*',
        '/api/pvp_event/*',
        '/api/exchange_gift/*',
        '/api/dex_package/*',
        '/api/revival_jan2020/*',
        '/api/preorder_3rdspec/*',
        '/api/package_3rdspec/*',
        '/api/black_valentine/*',
        '/api/piggy/*',
        '/api/material_support/*',
        '/api/daily_stamp/*',
        '/api/kylin/*',
        '/api/vip_benefit/*',
        '/api/league/*',
        '/api/daily_login/*',
        '/api/revival_apr2020/*',
        '/api/songkran2020/*',
        '/api/bmapril2020/*',
        '/api/package_des3rd/*',
        '/api/anniversary_3rd/*',
        '/api/battle_field/*',
        '/api/anniversary_3rd_passport/*',
        '/api/topup_may2020/*',
        '/api/hardmode/*',
        '/api/gacha5baht_may/*',
        '/api/hey_duo/*',
        '/api/phoenix/*',
        '/api/cashbackpackage/*',
        '/api/mystic/*',
        '/api/hm_pass_ss2/*',
        '/api/newbie_free_package/*',
        '/api/newbie_daily_login/*',
        '/api/two_in_one/*',
        '/api/third_spec_assassin/*',
        '/api/buffet_v2/*',
        '/api/rainypetpouches/*',
        '/api/blue_jeans/*',
        '/api/mystic2/*',
        '/api/pvp_pk/*',
        '/api/pvp_event2/*',
        '/api/easymode/*',
    ];
}
