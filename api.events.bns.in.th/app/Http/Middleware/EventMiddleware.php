<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redis;

class EventMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // get IP
        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = array_shift($ips);
        }

        /**
         * ========================================
         * We do not want to allow spamming of requests. All POST requests coming from the same IP address with
         * the same payload will be limited to 1 request per second.
         * ========================================
         */
        if ($request->isMethod('post')) {
            // check payload
            $payload = $request->getContent();
            if (empty($payload) == FALSE) {
                // construct key
                $key = 'eventmiddleware:limitrequest:';
                $key .= $request->path() . ':';
                $key .= $ip;
                $redis = Redis::connection('event_requests_limit');
                $result = $redis->get($key);
                if ($result) {
                    if (strcasecmp($result, $payload) === 0) {
                        // this is likely a spam request, so we return "I'm a teapot"
                        // @see https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
                        abort(418, sprintf('I\'m a teapot: %s', $payload));
                    }
                } else {
                    $result = $redis->setex($key, 1, $payload); // 1 request per second
                    if ($result) {
                        // okay
                    } else {
                        abort(500, 'Error creating event request queue!');
                    }
                }
            }
        }

        return $next($request);
    }
}
