<?php

    namespace App\Http\Controllers\Api\material_support;

    class RewardController {

        public function getRandomRewardInfo($lootItem) {

            // $rewards = $this->getSpecialRewardsList();
            $rewards = $lootItem;

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($rewards as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            // now we have the reward
            return $reward;
        }

        public function getSpecialRewardsList() {
            return [
                [
                    'id' => 1,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลหินโซล x50',
                    'product_id' => 274,
                    'product_quantity' => 1,
                    'chance' => 28,
                    'icon' => 'Gacha5Baht_id01.png',
                    'key' => 'Gacha5Baht_id01'
                ],
                [
                    'id' => 2,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลหินจันทรา x5',
                    'product_id' => 1116,
                    'product_quantity' => 1,
                    'chance' => 15,
                    'icon' => 'Gacha5Baht_id02.png',
                    'key' => 'Gacha5Baht_id02'
                ],
                [
                    'id' => 3,
                    'item_type' => 'object',
                    'product_title' => 'หินเปลี่ยนรูป x5',
                    'product_id' => 690,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'Gacha5Baht_id03.png',
                    'key' => 'Gacha5Baht_id03'
                ],
                [
                    'id' => 4,
                    'item_type' => 'object',
                    'product_title' => 'ประกายฉลามดำ x5',
                    'product_id' => 1855,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'Gacha5Baht_id04.png',
                    'key' => 'Gacha5Baht_id04'
                ],
                [
                    'id' => 5,
                    'item_type' => 'object',
                    'product_title' => 'ลูกแก้วยอดนักรบ x1',
                    'product_id' => 2647,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'Gacha5Baht_id05.png',
                    'key' => 'Gacha5Baht_id05'
                ],
                [
                    'id' => 6,
                    'item_type' => 'object',
                    'product_title' => 'วิญญาณวายุทมิฬ x5',
                    'product_id' => 1609,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'Gacha5Baht_id06.png',
                    'key' => 'Gacha5Baht_id06'
                ],
                [
                    'id' => 7,
                    'item_type' => 'object',
                    'product_title' => 'ยันต์ปริศนาแห่งฮงมุน x1',
                    'product_id' => 2192,
                    'product_quantity' => 1,
                    'chance' => 5,
                    'icon' => 'Gacha5Baht_id07.png',
                    'key' => 'Gacha5Baht_id07'
                ],
                [
                    'id' => 8,
                    'item_type' => 'object',
                    'product_title' => 'เกล็ดสีคราม x1',
                    'product_id' => 2071,
                    'product_quantity' => 1,
                    'chance' => 5,
                    'icon' => 'Gacha5Baht_id08.png',
                    'key' => 'Gacha5Baht_id08'
                ],
                [
                    'id' => 9,
                    'item_type' => 'object',
                    'product_title' => 'ปีกแทชอน x1',
                    'product_id' => 2242,
                    'product_quantity' => 1,
                    'chance' => 5,
                    'icon' => 'Gacha5Baht_id09.png',
                    'key' => 'Gacha5Baht_id09'
                ],
                [
                    'id' => 10,
                    'item_type' => 'object',
                    'product_title' => 'ดาวมังกรฟ้า x1',
                    'product_id' => 2648,
                    'product_quantity' => 1,
                    'chance' => 0.5,
                    'icon' => 'Gacha5Baht_id010.png',
                    'key' => 'Gacha5Baht_id010'
                ],
                [
                    'id' => 11,
                    'item_type' => 'object',
                    'product_title' => 'ผ้าโพกหัวแห่งราตรี x1',
                    'product_id' => 2767,
                    'product_quantity' => 1,
                    'chance' => 0.5,
                    'icon' => 'Gacha5Baht_id011.png',
                    'key' => 'Gacha5Baht_id011'
                ],
                [
                    'id' => 12,
                    'item_type' => 'object',
                    'product_title' => 'กล่องลูกแก้ว ขั้น 3 (อังคาร พุธ พฤหัส) x1',
                    'product_id' => 2768,
                    'product_quantity' => 1,
                    'chance' => 0.4,
                    'icon' => 'Gacha5Baht_id012.png',
                    'key' => 'Gacha5Baht_id012'
                ],
                [
                    'id' => 13,
                    'item_type' => 'object',
                    'product_title' => 'กล่องหินสถานะ ระยิบระยับ x1',
                    'product_id' => 2769,
                    'product_quantity' => 1,
                    'chance' => 0.2,
                    'icon' => 'Gacha5Baht_id013.png',
                    'key' => 'Gacha5Baht_id013'
                ],
                [
                    'id' => 14,
                    'item_type' => 'object',
                    'product_title' => 'กล่องลูกแก้ว ขั้น 3 (ศุกร์ เสาร์) x1',
                    'product_id' => 2770,
                    'product_quantity' => 1,
                    'chance' => 0.2,
                    'icon' => 'Gacha5Baht_id014.png',
                    'key' => 'Gacha5Baht_id014'
                ],
                [
                    'id' => 15,
                    'item_type' => 'object',
                    'product_title' => 'กล่องหินสกิลระยิบระยับ x1',
                    'product_id' => 2771,
                    'product_quantity' => 1,
                    'chance' => 0.1,
                    'icon' => 'Gacha5Baht_id015.png',
                    'key' => 'Gacha5Baht_id015'
                ],
                [
                    'id' => 16,
                    'item_type' => 'object',
                    'product_title' => 'ชุดหนังแห่งราตรี x1',
                    'product_id' => 2772,
                    'product_quantity' => 1,
                    'chance' => 0.1,
                    'icon' => 'Gacha5Baht_id016.png',
                    'key' => 'Gacha5Baht_id016'
                ]
            ];
        }

    }
