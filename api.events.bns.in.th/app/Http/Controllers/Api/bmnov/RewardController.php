<?php

    namespace App\Http\Controllers\Api\bmnov;

    use Carbon\Carbon;

    class RewardController {

        public function getRandomRewardInfo() {

            // $date_rate=Carbon::parse("2019-11-13 16:00:00");
            // $date_now=Carbon::now();
            // if($date_now<$date_rate){
            //     $rewards = $this->getSpecialRewardsListBoots();
            // }else{
                $rewards = $this->getSpecialRewardsList();
            // }

            $random = rand(1, 100000);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($rewards as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            // now we have the reward
            return $reward;
        }

        public function getSpecialRewardsList() {
            return [
                [
                    'id' => 1,
                    'item_type' => 'object',
                    'product_title' => 'หินเปลี่ยนรูปชั้นสูง x1',
                    'product_id' => 911,
                    'product_quantity' => 1,
                    'chance' => 25,
                    'icon' => 'gacha_1',
                    'key' => 'gacha_1',
                    'stock'=>false
                ],
                [
                    'id' => 2,
                    'item_type' => 'object',
                    'product_title' => 'หินเปลี่ยนรูปชั้นสูง x2',
                    'product_id' => 2727,
                    'product_quantity' => 1,
                    'chance' => 15,
                    'icon' => 'gacha_2',
                    'key' => 'gacha_2',
                    'stock'=>false
                ],
                [
                    'id' => 3,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลอัญมณีหยินหยาง x1',
                    'product_id' => 1900,
                    'product_quantity' => 1,
                    'chance' => 15,
                    'icon' => 'gacha_3',
                    'key' => 'gacha_3',
                    'stock'=>false
                ],
                [
                    'id' => 4,
                    'item_type' => 'object',
                    'product_title' => 'ไหมห้าสี x10',
                    'product_id' => 2723,
                    'product_quantity' => 1,
                    'chance' => 15,
                    'icon' => 'gacha_4',
                    'key' => 'gacha_4',
                    'stock'=>false
                ],
                [
                    'id' => 5,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลอัญมณีฮงมุน x100',
                    'product_id' => 2728,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'gacha_5',
                    'key' => 'gacha_5',
                    'stock'=>false
                ],
                [
                    'id' => 6,
                    'item_type' => 'object',
                    'product_title' => 'ไหมห้าสี x15',
                    'product_id' => 2724,
                    'product_quantity' => 1,
                    'chance' => 6,
                    'icon' => 'gacha_6',
                    'key' => 'gacha_6',
                    'stock'=>false
                ],
                [
                    'id' => 7,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลอัญมณีหยินหยาง x2',
                    'product_id' => 2407,
                    'product_quantity' => 1,
                    'chance' => 5.5,
                    'icon' => 'gacha_7',
                    'key' => 'gacha_7',
                    'stock'=>false
                ],
                [
                    'id' => 8,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลอัญมณีหยินหยาง x3',
                    'product_id' => 2729,
                    'product_quantity' => 1,
                    'chance' => 5,
                    'icon' => 'gacha_8',
                    'key' => 'gacha_8',
                    'stock'=>false
                ],
                [
                    'id' => 9,
                    'item_type' => 'object',
                    'product_title' => 'ชุดวันที่หอมหวาน',
                    'product_id' => 2725,
                    'product_quantity' => 1,
                    'chance' => 2.5,
                    'icon' => 'gacha_9',
                    'key' => 'gacha_9',
                    'stock'=>false
                ],
                [
                    'id' => 10,
                    'item_type' => 'object',
                    'product_title' => 'เซ็ทชุดเกราะนักรบหมาป่า',
                    'product_id' => 2726,
                    'product_quantity' => 1,
                    'chance' => 1,
                    'icon' => 'gacha_10',
                    'key' => 'gacha_10',
                    'stock'=>true
                ],

            ];
        }

        public function getSpecialRewardsListBoots() {
            return [
                [
                    'id' => 1,
                    'item_type' => 'object',
                    'product_title' => 'หินเปลี่ยนรูปชั้นสูง x1',
                    'product_id' => 911,
                    'product_quantity' => 1,
                    'chance' => 20.5,
                    'icon' => 'gacha_1',
                    'key' => 'gacha_1',
                    'stock'=>false
                ],
                [
                    'id' => 2,
                    'item_type' => 'object',
                    'product_title' => 'หินเปลี่ยนรูปชั้นสูง x2',
                    'product_id' => 2727,
                    'product_quantity' => 1,
                    'chance' => 15,
                    'icon' => 'gacha_2',
                    'key' => 'gacha_2',
                    'stock'=>false
                ],
                [
                    'id' => 3,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลอัญมณีหยินหยาง x1',
                    'product_id' => 1900,
                    'product_quantity' => 1,
                    'chance' => 15,
                    'icon' => 'gacha_3',
                    'key' => 'gacha_3',
                    'stock'=>false
                ],
                [
                    'id' => 4,
                    'item_type' => 'object',
                    'product_title' => 'ไหมห้าสี x10',
                    'product_id' => 2723,
                    'product_quantity' => 1,
                    'chance' => 15,
                    'icon' => 'gacha_4',
                    'key' => 'gacha_4',
                    'stock'=>false
                ],
                [
                    'id' => 5,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลอัญมณีฮงมุน x100',
                    'product_id' => 2728,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'gacha_5',
                    'key' => 'gacha_5',
                    'stock'=>false
                ],
                [
                    'id' => 6,
                    'item_type' => 'object',
                    'product_title' => 'ไหมห้าสี x15',
                    'product_id' => 2724,
                    'product_quantity' => 1,
                    'chance' => 6,
                    'icon' => 'gacha_6',
                    'key' => 'gacha_6',
                    'stock'=>false
                ],
                [
                    'id' => 7,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลอัญมณีหยินหยาง x2',
                    'product_id' => 2407,
                    'product_quantity' => 1,
                    'chance' => 5.5,
                    'icon' => 'gacha_7',
                    'key' => 'gacha_7',
                    'stock'=>false
                ],
                [
                    'id' => 8,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลอัญมณีหยินหยาง x3',
                    'product_id' => 2729,
                    'product_quantity' => 1,
                    'chance' => 5,
                    'icon' => 'gacha_8',
                    'key' => 'gacha_8',
                    'stock'=>false
                ],
                [
                    'id' => 9,
                    'item_type' => 'object',
                    'product_title' => 'ชุดวันที่หอมหวาน',
                    'product_id' => 2725,
                    'product_quantity' => 1,
                    'chance' => 5,
                    'icon' => 'gacha_9',
                    'key' => 'gacha_9',
                    'stock'=>false
                ],
                [
                    'id' => 10,
                    'item_type' => 'object',
                    'product_title' => 'เซ็ทชุดเกราะนักรบหมาป่า',
                    'product_id' => 2726,
                    'product_quantity' => 1,
                    'chance' => 3,
                    'icon' => 'gacha_10',
                    'key' => 'gacha_10',
                    'stock'=>true
                ],

            ];
        }

    }
