<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;

class ApiController extends Controller
{
    protected $ip = '';

    public function __construct()
    {
        $this->setIp();
    }

    /**
     * Detect client's IP address
     */
    private function setIp()
    {
        // get IP
        try {
            $this->ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $this->ip = array_shift($ips);
            }
        } catch (Exception $e) {
            // ignore
        }
    }

    protected function injectCsrfToken()
    {
        // set csrf token
        $token = csrf_token();
        setcookie('csrf_token', $token, time() + 900, '/'); // 900 = 15 mins
    }

    private function strToAssoc($arr)
    {
        foreach ($arr as $key => $value) {
            preg_match('/(.*)\[(\d+)\]\[(.*)\]/', $key, $matches, PREG_OFFSET_CAPTURE);
            if (sizeof($matches) == 4) {
                $arr[$matches[1][0]][$matches[2][0]][$matches[3][0]] = $value;
                unset($arr[$key]);
            } else {
                return $arr;
            }
        }
        return $this->strToAssoc($arr);
    }

    private function extendArray($array1, $array2)
    {
        foreach ($array1 as $key => $value) {
            if (isset($array2[$key])) {
                if (is_array($array1[$key]) && is_array($array2[$key])) {
                    $array1[$key] = $this->extendArray($array1[$key], $array2[$key]);
                } else if (is_array($array2[$key])) {
                    $array1[$key] = $array2[$key];
                } else {
                    $array1[$key] = urldecode($array2[$key]);
                }
                unset($array2[$key]);
            } else {
                if (is_array($array1[$key])) { } else {
                    $array1[$key] = urldecode($array1[$key]);
                }
            }
        }
        return array_merge($array1, $array2);
    }

    protected function parseArrayQueryString($arr)
    {
        $arr2 = [];
        foreach ($arr as $key => $value) {
            $arr3 = $this->strToAssoc([$key => $value]);
            $arr2 = $this->extendArray($arr2, $arr3);
        }
        return $arr2;
    }

    /**
     * @param String $request
     * @param $secret
     * @return mixed
     */
    protected function decryptJwtToken(Request $request, $secret)
    {
        $auth = $request->header('Authorization');
        if (is_null($auth)) {
            return null;
        }

        $header_signature = base64_decode(str_replace('GarenaTH ', '', $auth));
        $header_signature = explode('.', $header_signature);
        $payload = base64_decode($request->getContent());

        if (sizeof($header_signature) !== 2) {
            return null;
        }

        $jwt = $header_signature[0] . '.' . $payload . '.' . $header_signature[1];

        try {
            $decoded = JWT::decode($jwt, $secret, ['HS256']); // we always use HS256
            $decoded = (array) $decoded;
            $decoded = $this->parseArrayQueryString($decoded);
            return $decoded;
        } catch (Exception $ex) {
            // verification failed
        }
        return null;
    }

    protected function decryptJwtBearer(Request $request, $secret)
    {
        $auth = $request->header('Authorization');
        if (is_null($auth)) {
            return null;
        }

        try {
            $decoded = JWT::decode($auth, $secret, ['HS256']); // we always use HS256
            $decoded = (array) $decoded;
            $decoded = $this->parseArrayQueryString($decoded);
            return $decoded;
        } catch (Exception $ex) {
            // verification failed
        }
        return null;
    }

    /**
     * Simple POST request
     */
    protected function post_api($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        // dd($url, $params, $headers);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === FALSE) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === FALSE) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }
}
