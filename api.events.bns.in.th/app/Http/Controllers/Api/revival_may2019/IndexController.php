<?php

namespace App\Http\Controllers\Api\revival_may2019;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Api\BnsEventController;

// Models
use App\Models\revival_may2019\Members;
use App\Models\revival_may2019\ClaimLogs;
use App\Models\revival_may2019\ItemLogs;
use App\Models\revival_may2019\CharacterLogs;
use App\Models\revival_may2019\LastLoginLists;

// cache
use Illuminate\Support\Facades\Cache;

class IndexController extends BnsEventController
{

    private $baseApi = 'http://api.apps.garena.in.th';

    private $event_start_time = '2019-05-22 12:00:00';
    private $event_end_time = '2019-06-26 23:59:59';
    private $event_claim_date = '2019-05-22 06:00:00';
    private $event_condition_2_start_date = '2019-04-01 00:00:00';
    private $event_condition_2_end_date = '2019-04-30 23:59:59';
    private $mastery_level = 10;

    private $awtSecret = 'LUtPEDS5M3P37RAwQZvQkFZvtwDXcrZX';
    private $userData;

    private $package_set_1 = [
        'name' => 'กล่องน้องเล็กหน้าใหม่',
        'package_id' => 2304,
        // 'package_id' => 1884,
        'quantity' => 1,
        'amount' => 1
    ];
    private $package_set_2 = [
        'name' => 'กล่องน้องเล็กที่กลับมา',
        'package_id' => 2305,
        // 'package_id' => 1674,
        'quantity' => 1,
        'amount' => 1
    ];

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->event_start_time = Carbon::parse($this->event_start_time);
        $this->event_end_time = Carbon::parse($this->event_end_time);
        $this->event_claim_date = Carbon::parse($this->event_claim_date);
        $this->event_condition_2_start_date = Carbon::parse($this->event_condition_2_start_date);
        $this->event_condition_2_end_date = Carbon::parse($this->event_condition_2_end_date);

        // JWT USER DATA
        $this->userData = $this->getJwtUserData($request);

        if (Carbon::now() < $this->event_start_time) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'กิจกรรมยังไม่เปิดใช้งาน'
            ]));
        }

        if (Carbon::now() > $this->event_end_time) {

            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'กิจกรรมปิดแล้ว '
            ]));
        }

        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
            die(json_encode([
                'status' => false,
                'type' => 'no_permission',
                'message' => 'Sorry, you do not have permission.'
            ]));
        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'cant_create_member',
                    'message' => 'ไม่พบตัวละครในเกม<br>กรุณาสร้างตัวละคร'
                ]));
            }
        } else {
            die(json_encode([
                'status' => false,
                'type' => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
            ]));
        }
    }

    public function checkin(Request $request) // checkin event
    {
        $decoded=$request->input();
        if (is_null($decoded)) {
            return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
        }
        $decoded = collect($decoded);

        $uid = $this->userData['uid'];
        $ncid = $this->getNcidByUid($uid);
        $member_id  = $this->getIdByUid($uid);

        // checking claim
        return response()->json($this->checkClaimData($uid, $ncid, $member_id));
    }

    private function checkClaimData($uid, $ncid, $member_id)
    {

        if ($this->countClaimLogs($member_id) < 2) {
            $response = json_decode($this->apiCharacters($uid, $ncid));
            if ($response->status) {
                $charData = collect($response->response);

                /*
                เงื่อนไขแรก
                ผู้เล่นที่สร้างตัวละครใหม่ตั้งแต่ 22 พฤษภาคม หลังปิดปรับปรุงเซิร์ฟเวอร์
                และเก็บเลเวลฮงมุน เลวเล 10
                จะได้รับกล่อง กล่องน้องเล็กหน้าใหม่
             */
                $condition_1 = $charData->map(function ($resp) {
                    if (Carbon::parse($resp->creation_time) >= $this->event_claim_date && $resp->mastery_level >= $this->mastery_level) {
                        return $resp;
                    } else {
                        return null;
                    }
                })->filter(function ($resp) {
                    return $resp != null;
                });

                /*
                เงื่อนไขสอง
                ผู้เล่นที่ไม่ได้ล็อกอินเข้าเกม ตั้งแต่วันที่ 1-30 เมษายน 2562
                จะได้รับกล่อง กล่องน้องเล็กที่กลับมา
             */
                $condition_2 = Cache::remember('BNS:revival_may2019:find_not_active_uid:' . $uid, 5, function () use ($uid) {
                    return LastLoginLists::where('uid', $uid)->count() > 0;
                });

                // Add Pass Logs (condition 1)
                if (count($condition_1) > 0) {
                    if ($this->hasClaimLogs($member_id, 1) == 0) {
                        // create claim logs 1
                        $this->createClaimLog($member_id, 1);
                        // add character to logs
                        $this->createCharacter([
                            'member_id' => $member_id,
                            'name' => $condition_1[0]->name,
                            'world_id' => $condition_1[0]->world_id,
                            'mastery_level' => $condition_1[0]->mastery_level,
                            'level' => $condition_1[0]->level,
                            'last_play_start' => $condition_1[0]->last_play_start,
                            'last_play_end' => $condition_1[0]->last_play_end,
                            'last_ip' => $condition_1[0]->last_ip,
                            'job' => $condition_1[0]->job,
                            'exp' => $condition_1[0]->exp,
                            'creation_time' => $condition_1[0]->creation_time,
                            'remark' => 1
                        ]);
                    }
                }
                // Add Pass Logs (condition 2)
                if ($condition_2) {
                    if ($this->hasClaimLogs($member_id, 2) == 0) {
                        // create claim logs 2
                        $this->createClaimLog($member_id, 2);
                    }
                }

                return [
                    'status' => true,
                    'response' => [
                        'username'=>$this->userData['username'],
                        'condition_1_pass' => count($condition_1) > 0 ? true : false,
                        'condition_1_received' => !empty($this->getClaimLogs($member_id, 1, 'used')),
                        'condition_2_pass' => $condition_2,
                        'condition_2_received' => !empty($this->getClaimLogs($member_id, 2, 'used')),
                    ]
                ];
            } else {
                return [
                    'status' => false,
                    'message' => 'เกิดความผิดพลาด ไม่พบข้อมูล'
                ];
            }
        } else {
            return [
                'status' => true,
                'response' => [
                    'username'=>$this->userData['username'],
                    'condition_1_pass' => true,
                    'condition_1_received' => !empty($this->getClaimLogs($member_id, 1, 'used')),
                    'condition_2_pass' => true,
                    'condition_2_received' => !empty($this->getClaimLogs($member_id, 2, 'used')),
                ]
            ];
        }
    }

    private function getEventInfo($member_id,$uid)
    {
        return [
            'status' => true,
            'response' => [
                'username'=>$this->userData['username'],
                'condition_1_pass' => $this->hasClaimLogs($member_id, 1) > 0,
                'condition_1_received' => !empty($this->getClaimLogs($member_id, 1, 'used')),
                'condition_2_pass' => $this->hasClaimLogs($member_id, 2) > 0,
                'condition_2_received' => !empty($this->getClaimLogs($member_id, 2, 'used')),
            ]
        ];
    }

    public function redeem(Request $request)
    {
        $decoded=$request->input();
        if (is_null($decoded)) {
            return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
        }
        $decoded = collect($decoded);

        if ($decoded->has('type_id') == false) {
            return response()->json([
                'status' => false,
                'message' => 'Invalid Parameter!'
            ]);
        } else {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            $ncid = $this->getNcidByUid($uid);
            $member_id = $this->getIdByUid($uid);

            if ($decoded['type_id'] == 1) {
                if ($this->hasClaimLogs($member_id, 1) > 0) {
                    $cl = $this->getClaimLogs($member_id, 1, 'unused');
                    if ($cl->status == 'used') {
                        return response()->json([
                            'status' => false,
                            'message' => 'คุณได้ทำการรับไอเทมของเงื่อนไขนี้ไปแล้วไม่สามารถรับไอเทมนี้ได้อีก'
                        ]);
                    }

                    // deduct claim logs
                    ClaimLogs::where('member_id', $member_id)->where('claim_id', 1)->update([
                        'status' => 'used'
                    ]);
                    // setup
                    $goods_data = [];
                    $goods_data[] = [
                        'goods_id' => $this->package_set_1['package_id'],
                        'purchase_quantity' => $this->package_set_1['quantity'],
                        'purchase_amount' => 0,
                        'category_id' => 40
                    ];
                    // add senditem logs
                    $itemLog = $this->addSendItemLog([
                        'uid' => $uid,
                        'username' => $username,
                        'ncid' => $ncid,
                        'log_date' => date('Y-m-d H:i:s'),
                        'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                        'package_id' => $this->package_set_1['package_id'],
                        'package_title' => $this->package_set_1['name'],
                        'status' => 'pending',
                        'send_item_status' => false,
                        'send_item_purchase_id' => '0',
                        'send_item_purchase_status' => '0',
                        'goods_data' => json_encode($goods_data),
                    ]);
                    // sent item..
                    $send_result_raw = $this->dosendItem($ncid, $goods_data);
                    $send_result = json_decode($send_result_raw);
                    // update logs
                    if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                        $this->updateSendItemLog([
                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                            'status' => 'success'
                        ], $itemLog['id'], $uid);

                        $event_info = $this->getEventInfo($member_id,$uid);

                        return response()->json([
                            'status' => true,
                            'message' => 'ส่งของรางวัลเสร็จเรียบร้อย<br />กรุณาตรวจสอบกล่องจดหมายภายในเกม',
                            'content' => $event_info
                        ]);
                    } else {
                        $this->updateSendItemLog([
                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                            'status' => 'unsuccess'
                        ], $itemLog['id'], $uid);

                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถส่งของรางวัลได้ กรุณาติดต่อฝ่ายบริการลูกค้า',
                        ]);
                    }
                }
            } else if ($decoded['type_id'] == 2) {
                if ($this->hasClaimLogs($member_id, 2) > 0) {
                    $cl = $this->getClaimLogs($member_id, 2, 'unused');
                    if ($cl->status == 'used') {
                        return response()->json([
                            'status' => false,
                            'message' => 'คุณได้ทำการรับไอเทมของเงื่อนไขนี้ไปแล้วไม่สามารถรับไอเทมนี้ได้อีก'
                        ]);
                    }
                    // deduct claim logs
                    ClaimLogs::where('member_id', $member_id)->where('claim_id', 2)->update([
                        'status' => 'used'
                    ]);
                    // setup
                    $goods_data = [];
                    $goods_data[] = [
                        'goods_id' => $this->package_set_2['package_id'],
                        'purchase_quantity' => $this->package_set_2['quantity'],
                        'purchase_amount' => 0,
                        'category_id' => 40
                    ];
                    // add senditem logs
                    $itemLog = $this->addSendItemLog([
                        'uid' => $uid,
                        'username' => $username,
                        'ncid' => $ncid,
                        'log_date' => date('Y-m-d H:i:s'),
                        'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                        'package_id' => $this->package_set_2['package_id'],
                        'package_title' => $this->package_set_2['name'],
                        'status' => 'pending',
                        'send_item_status' => false,
                        'send_item_purchase_id' => '0',
                        'send_item_purchase_status' => '0',
                        'goods_data' => json_encode($goods_data),
                    ]);
                    // sent item..
                    $send_result_raw = $this->dosendItem($ncid, $goods_data);
                    $send_result = json_decode($send_result_raw);
                    // update logs
                    if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                        $this->updateSendItemLog([
                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                            'status' => 'success'
                        ], $itemLog['id'], $uid);

                        $event_info = $this->getEventInfo($member_id,$uid);

                        return response()->json([
                            'status' => true,
                            'message' => 'ส่งของรางวัลเสร็จเรียบร้อย<br />กรุณาตรวจสอบกล่องจดหมายภายในเกม',
                            'content' => $event_info
                        ]);
                    } else {
                        $this->updateSendItemLog([
                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                            'status' => 'unsuccess'
                        ], $itemLog['id'], $uid);

                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถส่งของรางวัลได้ กรุณาติดต่อฝ่ายบริการลูกค้า',
                        ]);
                    }
                }
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid Parameter! [EX1]'
                ]);
            }

            return response()->json([
                'status' => false,
                'message' => 'คุณยังไม่ผ่านเงื่อนไขที่กำหนด กรุณาตรวจสอบใหม่อีกครั้ง'
            ]);
        }
    }

    private function createClaimLog($member_id, $claim_id)
    {
        return ClaimLogs::create([
            'member_id' => $member_id,
            'claim_id' => $claim_id,
            'status' => 'unused'
        ]);
    }

    private function createCharacter($arr)
    {
        return CharacterLogs::create($arr);
    }

    private function countClaimLogs($member_id)
    {
        return ClaimLogs::where('member_id', $member_id)->count();
    }

    private function getClaimLogs($member_id, $claim_id, $status = 'unused')
    {
        return ClaimLogs::where('member_id', $member_id)->where('claim_id', $claim_id)->where('status', $status)->first();
    }

    private function hasClaimLogs($member_id, $claim_id = 1)
    {
        return ClaimLogs::where('member_id', $member_id)->where('claim_id', $claim_id)->count();
    }

    private function apiCharacters($uid, $ncid)
    {
        return Cache::remember('BNS:revival_may2019:CHARACTERS_' . $uid, 10, function () use ($ncid) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'characters',
                'user_id' => $ncid
            ]);
        });
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        // $this->userData = $this->getUserData();
        if ($this->userData)
            return true;
        else
            return false;
    }

    private function is_accepted_ip()
    {
        $allow_ips = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function checkMember()
    {
        $uid = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid' => $uid,
                'username' => $username,
                'ncid' => $ncid,
                'last_ip' => $this->getIP(),
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
        } else {
            $member = Members::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Members::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Members::firstOrCreate($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id))
            return false;

        return Members::where('id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Members::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Members::where('ncid', $ncid)->value('uid');
    }

    protected function getIdByUid(int $uid)
    {
        return Members::where('uid', $uid)->value('id');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:revival_may2019:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'user_info',
                'uid' => $uid,
            ]);
            $userNcInfo = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }
        });
    }

    private function dosendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'sending item from garena events bns revival may 2019',
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function addSendItemLog(array $arr)
    {
        if ($arr) {
            $resp = ItemLogs::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return null;
            }
        }
        return null;
    }

    private function updateSendItemLog(array $arr, $logId, $uid)
    {
        if ($arr) {
            return ItemLogs::where('id', $logId)->where('uid', $uid)->update($arr);
        }
        return null;
    }
}
