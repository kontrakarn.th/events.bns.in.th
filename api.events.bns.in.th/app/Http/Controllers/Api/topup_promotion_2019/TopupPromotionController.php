<?php

namespace App\Http\Controllers\Api\topup_promotion_2019;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;

// Models
use App\Models\TopupMay\TopupMayMembers as Members;
use App\Models\TopupMay\TopupMayItemLogs as ItemLogs;
use App\Models\TopupMay\TopupMayCounterRedeem as RedeemCounter;
use App\Models\TopupMay\TopupMayRedeemLogs as RedeemLogs;

class TopupPromotionController extends BnsEventController
{
    private $userData = [];

    private $baseApi = 'http://api.apps.garena.in.th';

    private $startTime = '2019-05-12 00:00:00';
    private $endTime = '2019-06-04 05:59:59';

    private $diamondStartTime = '2019-05-12 00:00:00';
    private $diamondEndTime = '2019-06-04 05:59:59';

    private $bnsAppId = 32835;

    private $rewards = [
        [
            'product_id' => 2292,
            'product_title' => '12500 Diamond',
            'product_desc' => '12500 Diamond',
            'product_quantity' => 1,
            'product_amount' => 1,
            'product_diamond' => 12500
        ],
        [
            'product_id' => 2293,
            'product_title' => '50000 Diamond',
            'product_desc' => '50000 Diamond',
            'product_quantity' => 1,
            'product_amount' => 1,
            'product_diamond' => 50000
        ],
        [
            'product_id' => 2294,
            'product_title' => '300000 Diamond',
            'product_desc' => '300000 Diamond',
            'product_quantity' => 1,
            'product_amount' => 1,
            'product_diamond' => 300000
        ],
    ];

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->userData = $this->getJwtUserData($request);

        $this->checkEventStatus();
    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
            die(json_encode([
                'status' => false,
                'type' => 'no_permission',
                'message' => 'Sorry, you do not have permission.'
            ]));
        }

        if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status' => false,
                'type' => 'maintenance',
                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
            ]));
        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                ]));
            }
        } else {
            die(json_encode([
                'status' => false,
                'type' => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        $this->userData = $this->getUserData();
        if ($this->userData)
            return true;
        else
            return false;
    }


    private function is_accepted_ip()
    {
        $allow_ips = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    public function checkin(Request $request)
    {

        $data = [
            'diamond' => 0,
            'bonus' => [
                'bonus_1' => 0,
                'bonus_2' => 0,
                'bonus_3' => 0
            ],
            'bonus_counter' => null
        ];

        $this->userData = $this->getUserData();
        $memberData = $this->getMember($this->userData['uid']);
        $redeemLog  = $this->getRedeemLog($memberData->id);

        // when diamond reward loss
        $encounterRedeem = $this->canRedeem();
        if ($encounterRedeem['bonus_1'] == false) {
            $data['bonus']['bonus_1'] = 2;
        }
        if ($encounterRedeem['bonus_2'] == false) {
            $data['bonus']['bonus_2'] = 2;
        }
        if ($encounterRedeem['bonus_3'] == false) {
            $data['bonus']['bonus_3'] = 2;
        }

        $bonus_value = RedeemCounter::select('bonus_1', 'bonus_2', 'bonus_3')->first();
        $data['bonus_counter'] = $bonus_value;

        if (empty($redeemLog) == false) {
            switch ($redeemLog->bonus_type) {
                case 1:
                    $data['bonus']['bonus_1'] = 3;
                    break;
                case 2:
                    $data['bonus']['bonus_2'] = 3;
                    break;
                case 3:
                    $data['bonus']['bonus_3'] = 3;
                    break;
                default:
                    $data['bonus']['bonus_1'] = 3;
                    break;
            }
        } else {
            // check have redeem reward
            $raw_diamond = $this->doGetDiamondHistory($memberData->ncid);
            $diamond = collect($raw_diamond->response)->sum('diamond');

            if ($memberData->total_diamonds != $diamond) {
                $this->updateMember(['total_diamonds' => $diamond], $memberData->id);
            }

            $data['diamond'] = $diamond;

            // collection bonus
            $collection = collect($raw_diamond->response);
            $encounter_1 = $collection->where('diamond', 50000)->count();
            $encounter_2 = $collection->where('diamond', 100000)->count();
            $encounter_3 = $collection->where('diamond', 300000)->count();

            /*
            remark :
            0 = ไม่ตรงเงื่อนไข,
            1 = รับโบนัสไดมอนด์,
            2 = สิทธิ์หมดแล้ว,
            3 = รับไดมอนด์แล้ว
            */
            if ($encounter_3 > 0) {
                $data['bonus']['bonus_1'] = $data['bonus']['bonus_1'] != 2 ? 1 : 2;
                $data['bonus']['bonus_2'] = $data['bonus']['bonus_2'] != 2 ? 1 : 2;
                $data['bonus']['bonus_3'] = $data['bonus']['bonus_3'] != 2 ? 1 : 2;
            } else if ($encounter_2 > 0) {
                $data['bonus']['bonus_1'] = $data['bonus']['bonus_1'] != 2 ? 1 : 2;
                $data['bonus']['bonus_2'] = $data['bonus']['bonus_2'] != 2 ? 1 : 2;
            } else if ($encounter_1 > 0) {
                $data['bonus']['bonus_1'] = $data['bonus']['bonus_1'] != 2 ? 1 : 2;
            }
        }

        return response()->json([
            'status' => true,
            'message' => 'successfully',
            'data' => $data
        ]);
    }

    public function doRedeem(Request $request)
    {
        if ($request->has('bonus_id')) {
            $bonus_id = $request->bonus_id;

            $this->userData = $this->getUserData();
            $memberData = $this->getMember($this->userData['uid']);

            $redeemLog = $this->getRedeemLog($memberData->id);
            if (empty($redeemLog) == false) {
                return response()->json([
                    'status' => false,
                    'message' => 'คุณได้รับสิทธิ์ในการรับรางวัลเกินจำนวนแล้ว ไม่สามารถรับได้อีก'
                ]);
            }

            // check have redeem reward
            $raw_diamond = $this->doGetDiamondHistory($memberData->ncid);

            // collection bonus
            $collection = collect($raw_diamond->response);
            $encounter_1 = $collection->where('diamond', 50000)->count();
            $encounter_2 = $collection->where('diamond', 100000)->count();
            $encounter_3 = $collection->where('diamond', 300000)->count();

            $encounterRedeem = $this->canRedeem();
            $can_redeem_status = false;
            $diamond_can_redeem_status = false;
            switch ($bonus_id) {
                case 1:
                    if ($encounterRedeem['bonus_1'] > 0) {
                        $can_redeem_status = true;
                        if ($encounter_1 > 0 || $encounter_2 > 0 || $encounter_3 > 0) {
                            $diamond_can_redeem_status = true;
                        }
                    }
                    break;
                case 2:
                    if ($encounterRedeem['bonus_2'] > 0) {
                        $can_redeem_status = true;
                        if ($encounter_2 > 0 || $encounter_3 > 0) {
                            $diamond_can_redeem_status = true;
                        }
                    }
                    break;
                case 3:
                    if ($encounterRedeem['bonus_3'] > 0) {
                        $can_redeem_status = true;
                        if ($encounter_3 > 0) {
                            $diamond_can_redeem_status = true;
                        }
                    }
                    break;
                default:
                    return response()->json([
                        'status' => false,
                        'message' => 'เกิดความผิดพลาด กรุณาลองใหม่อีกครั้ง'
                    ]);
                    break;
            }

            if ($can_redeem_status) {
                if ($diamond_can_redeem_status) {
                    // deduct code value
                    RedeemCounter::decrement('bonus_' . $bonus_id, 1);

                    $redeem_logs = RedeemLogs::firstOrCreate(
                        ['member_id' => $memberData->id],
                        ['bonus_type' => $bonus_id]
                    );

                    $product_info = $this->rewards[$bonus_id - 1];
                    $goods_data = [];
                    $goods_data[] = [
                        'goods_id' => $product_info['product_id'],
                        'purchase_quantity' => $product_info['product_quantity'],
                        'purchase_amount' => 0,
                        'category_id' => 40
                    ];
                    // create send item logs
                    $sendItemLog = ItemLogs::firstOrCreate([
                        'uid' => $memberData->uid,
                        'ncid' => $memberData->ncid,
                    ], [
                        'package_id' => $product_info['product_id'],
                        'package_title' => $product_info['product_title'],
                        'require_diamonds' => $product_info['product_diamond'],
                        'product_set' => json_encode($product_info),
                        'status' => 'pending',
                        'send_item_status' => false,
                        'send_item_purchase_id' => 0,
                        'send_item_purchase_status' => 0,
                        'goods_data' => json_encode($goods_data),
                        'last_ip' => $this->getIP()
                    ]);

                    if ($sendItemLog->status == 'pending') {
                        ItemLogs::where('id', $sendItemLog->id)->update([
                            'status' => 'processing'
                        ]);

                        $send_result_raw = $this->dosendItem($memberData->ncid, $goods_data);
                        $send_result = json_decode($send_result_raw);
                        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                            ItemLogs::where('id', $sendItemLog->id)->update([
                                'send_item_status' => $send_result->status ?: false,
                                'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                                'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                                'status' => 'success'
                            ]);

                            return response()->json([
                                'status' => true,
                                'message' => "กรุณาตรวจสอบโบนัสไดมอนด์ภายในเกม",
                            ]);
                        } else {
                            ItemLogs::where('id', $sendItemLog->id)->update([
                                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                                'status' => 'unsuccess'
                            ]);

                            return response()->json([
                                'status' => false,
                                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                            ]);
                        }
                    } else {
                        ItemLogs::where('id', $sendItemLog->id)->update([
                            'status' => 'unsuccess'
                        ]);
                        return response()->json([
                            'status' => false,
                            'message' => 'เกิดความผิดพลาด กรุณาลองใหม่อีกครั้ง [99]'
                        ]);
                    }
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่ตรงเงื่อนไข'
                    ]);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'สิทธิ์นี้เต็มแล้ว'
                ]);
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'invalid parameter!'
            ]);
        }
    }

    private function getRedeemLog($member_id)
    {
        return RedeemLogs::where('member_id', $member_id)->first();
    }

    private function canRedeem()
    {
        $resp = RedeemCounter::first();
        return [
            'bonus_1' => $resp->bonus_1 > 0 ? true : false,
            'bonus_2' => $resp->bonus_2 > 0 ? true : false,
            'bonus_3' => $resp->bonus_3 > 0 ? true : false,
        ];
    }

    private function checkMember()
    {
        $uid = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid' => $uid,
                'username' => $username,
                'ncid' => $ncid,
                'total_diamonds' => 0,
                // 'diamond_points' => 0,
                // 'used_diamond_points' => 0,
                'last_ip' => $this->getIP()
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
        } else {
            $member = Members::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
        }
        return true;
    }

    private function getMember($uid)
    {
        return Members::where('uid', $uid)->first(); // fresh member diamonds
    }

    private function createMember($arr)
    {
        return Members::firstOrCreate(['uid' => $arr['uid']], $arr);
    }

    protected function hasMember($uid)
    {
        $counter = Members::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id))
            return false;

        return Members::where('id', $_id)->update($arr);
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:TOPUP_MAY2019:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'user_info',
                'uid' => $uid,
            ]);
            $userNcInfo = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }
        });
    }

    private function doGetDiamondHistory($ncid)
    {
        if (empty($ncid)) {
            return false;
        }
        return Cache::remember('BNS:TOPUP_MAY2019:DIAMONDS_HISTORY_' . $ncid, 5, function () use ($ncid) {
            return json_decode($this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'query_cash_txn',
                'user_id' => $ncid,
                'start_time' => ($this->diamondStartTime),
                'end_time' => ($this->diamondEndTime),
            ]));
        });
    }

    private function doGetTermgameDiamondHistory($uid)
    {
        if (empty($uid)) {
            return false;
        }
        return Cache::remember('BNS:TOPUP_MAY2019:TERMGAME_DIAMONDS_HISTORY_' . $uid, 5, function () use ($uid) {
            return json_decode($this->post_api($this->baseApi, [
                'key_name' => 'gws_test',
                'service' => 'get_user_topup',
                'uid' => $uid,
                'start_ts' => strtotime($this->diamondStartTime),
                'end_ts' => strtotime($this->diamondEndTime),
                'app_id' => $this->bnsAppId
            ]));
        });
    }

    private function dosendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Test sending item from garena events bns topup November 2018.',
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }
}
