<?php

    namespace App\Http\Controllers\Api\vip_benefit;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\vip_benefit\Member;
    use App\Models\vip_benefit\ItemLog;
    use App\Models\vip_benefit\SendItemLog as OldSendItemLog;
    use App\Models\vip_benefit\Reward;

    class IndexController extends BnsEventController{

        private $baseApi = 'http://api.apps.garena.in.th';
        private $startTime = '2020-03-18 00:00:00';
        private $endTime = '2021-01-31 23:59:59';

        private $startGetApiTime = '00:00:00';
        private $endGetApiTime = '23:59:59';

        private $userEvent = null;

        public function __construct(Request $request)
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getJwtUserData($request);
            $this->checkEventStatus();

        }

        private function checkEventStatus()
        {
            if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'no_permission',
                    'message' => 'Sorry, you do not have permission.',
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม',
                ]));
            }

            if ($this->checkIsMaintenance()) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์',
                ]));
            }

            if ($this->isLoggedIn()) {
                // check and create member
                if ($this->checkMember() == false) {
                    die(json_encode([
                        'status'  => false,
                        'type'    => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง',
                    ]));
                }
            } else {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'not_login',
                    'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน',
                ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip  = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData) {
                return true;
            } else {
                return false;
            }
        }

        private function is_accepted_ip()
        {
            $allow_ips   = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid      = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $arr = [
                    'uid'              => $uid,
                    'username'         => $username,
                    'ncid'             => $ncid,
                    'char_id'          => 0,
                    'char_name'        => '',
                    'world_id'         => 0,
                    'last_ip'          => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
                $this->userEvent = $resp;

            } else {
                $member = Member::where('uid', $uid)->first();
                if (empty($member->ncid)) {
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if (empty($ncid)) {
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
                $this->userEvent = $member;
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if (empty($_id)) {
                return false;
            }

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:VIPBENEFIT:ASSOCIATE_UID_WITH_NCID_'.$uid, 10, function () use ($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'user_info',
                    'uid'      => $uid,
                ]);
                $userNcInfo       = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name'             => 'bns',
                'service'              => 'send_item',
                'user_id'              => $ncid,
                'purchase_description' => 'Sending item from garena events BNS vip benefit event.',
                'goods'                => json_encode($goodsData),
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
        }

        private function vipInfo($uid = null)
        {
            if (empty($uid))
                return false;

            return Cache::remember('BNS:VIPBENEFIT:VIPINFO_' . $uid, 10, function() use($uid) {//GET VIP DATA
                        $data = $this->post_api($this->baseApi, [
                            'key_name' => 'bns',
                            'service' => 'vip',
                            'uid' => $uid
                        ]);
                        return json_decode($data);
                    });
        }

        public function getEventInfo(Request $request){
            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);
            if($eventInfo['status']==false){
                return response()->json([
                        'status' => false,
                        'message'=>$eventInfo['message'],
                    ]);
            }

            return response()->json([
                'status' => true,
                'data' => $eventInfo
            ]);
        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return [
                    'status' => false,
                    'message' => 'ไม่พอข้อมูล'
                ];
            }

            $member = Member::where('uid', $uid)->first();

            $logDate =  date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimestamp = time();
            $startDateTime = date('Y-m-d').' '.$this->startGetApiTime;
            $endDateTime = date('Y-m-d').' '.$this->endGetApiTime;

            $startDateTimeStamp = strtotime($startDateTime);
            $endDateTimeStamp = strtotime($endDateTime);

            // get member vip info
            $userVip = $this->vipInfo($member->uid);
            $vip_level = isset($userVip) && isset($userVip->response->level_id) ? (int)$userVip->response->level_id : 1;
            $vip_active = ($vip_level > 0 && $userVip->response->activation == 'true') ? true : false;
            $vip_remain_date =  $vip_active ? date('Y-m-d H:i:s', strtotime($userVip->response->total_effective_to)) : '';
            // dd($vip_remain_date);

            // vip run effect daterange to receive
            $vipRunEffectInfo = Reward::where('product_key', 'vip_run_effect')->first();
            $vipRunEffectDateRange = $this->checkItemDatediff($member->uid,'vip_run_effect',$vipRunEffectInfo->vip_level,$vipRunEffectInfo->period);
            if(isset($vipRunEffectDateRange) && $vipRunEffectDateRange != false){
                $vipEffectNextReceive = date('Y-m-d H:i:s',strtotime('+'.$vipRunEffectInfo->period.' days',strtotime($vipRunEffectDateRange['datetime'])));
                $vipEffectCanReceive = $logDateTimestamp >= strtotime($vipEffectNextReceive) ? true : false;
                $vipEffectReceived = $logDateTimestamp <  strtotime($vipEffectNextReceive) ? true : false;
                $vipEffectLatestReceived = ($vipEffectReceived) ? date('Y-m-d H:i:s', strtotime($vipRunEffectDateRange['datetime'])) : '';
            }else{
                $vipEffectCanReceive = true;
                $vipEffectReceived = false;
                $vipEffectLatestReceived = '';
                $vipEffectNextReceive = '';
            }

            // vip ice storage reset token
            $vipColdStorageInfo = Reward::where('product_key', 'cold_storage_reset')->first();
            $vipColdStorageResetDateRange = $this->checkItemDatediff($member->uid,'cold_storage_reset',$vipColdStorageInfo->vip_level,$vipColdStorageInfo->period);
            if(isset($vipColdStorageResetDateRange) && $vipColdStorageResetDateRange != false){
                $vipColdStorageNextReceive = date('Y-m-d H:i:s',strtotime('+'.$vipColdStorageInfo->period.' days',strtotime($vipColdStorageResetDateRange['datetime'])));
                $vipColdStorageCanReceive = $logDateTimestamp >= strtotime($vipColdStorageNextReceive) ? true : false;
                $vipColdStorageReceived = $logDateTimestamp < strtotime($vipColdStorageNextReceive) ? true : false;
                $vipColdStorageLatestReceived = ($vipColdStorageReceived) ? date('Y-m-d H:i:s', strtotime($vipColdStorageResetDateRange['datetime'])) : '';
            }else{
                $vipColdStorageCanReceive = true;
                $vipColdStorageReceived = false;
                $vipColdStorageLatestReceived = '';
                $vipColdStorageNextReceive = '';
            }

            // check daily vip gachapon
            $gachaponInfo = Reward::where('product_key', 'vip_gachapon_box')->where('vip_level', $vip_level)->first();
            $gachaponReceivedToday = ItemLog::where('uid', $member->uid)
                                                ->where('product_key', 'vip_gachapon_box')
                                                ->where('status', 'success')
                                                ->where('log_date', $logDate)
                                                ->count();
            $vipGachaponNextReceive = '';
            if($gachaponReceivedToday > 0){
                $vipGachaponNextReceive = date('Y-m-d',strtotime('+1 days')).' 00:00:00';
            }                                    

            return [
                'status' => true,
                'username' => $member->username,
                'uid' => $member->uid,
                'vip_level' => $vip_level,
                'vip_active' => $vip_active,
                "vip_remain_date" => $vip_remain_date,
                'vip_run_effect' => [
                    'key' => 'vip_run_effect',
                    'image' => $vipRunEffectInfo->image,
                    'title' => $vipRunEffectInfo->product_title,
                    'id' => $vipRunEffectInfo->product_id,
                    'can_receive' => $vipEffectCanReceive && $vip_active == true && $vip_level >= $vipRunEffectInfo->vip_level && $vipRunEffectInfo->vip_level > 0 ? true : false,
                    'received' => $vipEffectReceived,
                    'latest_receive_date' => $vipEffectLatestReceived,
                    'next_receive_date' => $vipEffectNextReceive,
                ],
                'cold_storage_reset' => [
                    'key' => 'cold_storage_reset',
                    'image' => $vipColdStorageInfo->image,
                    'title' => $vipColdStorageInfo->product_title,
                    'id' => $vipColdStorageInfo->product_id,
                    'can_receive' => $vipColdStorageCanReceive && $vip_active == true && $vip_level >= $vipColdStorageInfo->vip_level && $vipColdStorageInfo->vip_level > 0 ? true : false,
                    'received' => $vipColdStorageReceived,
                    'latest_receive_date' => $vipColdStorageLatestReceived,
                    'next_receive_date' => $vipColdStorageNextReceive,
                ],
                'vip_gachapon_box' => [
                    'key' => 'vip_gachapon_box',
                    'image' => $gachaponInfo->image,
                    'title' => $gachaponInfo->product_title,
                    'id' => $gachaponInfo->product_id,
                    'amount' => $gachaponInfo && isset($gachaponInfo->product_quantity) ? $gachaponInfo->product_quantity : 0,
                    'can_receive' => $gachaponReceivedToday == 0 && $vip_active == true,
                    'received' => $gachaponReceivedToday > 0,
                    'next_receive_date' => $vipGachaponNextReceive,
                ]
            ];
        }

        private function checkItemDatediff($uid='',$productKey='',$vipLevel=0,$period=0){

            if(empty($uid) || empty($productKey) || empty($vipLevel) || empty($period)){
                return false;
            }

            $currDateTime = date_create(date('Y-m-d H:i:s'));

            // Mongodb
            $oldData = OldSendItemLog::where('uid', (int)$uid)
                                    ->where('package_key', $productKey)
                                    ->whereIn('status', ['success','unsuccess'])
                                    ->orderBy('log_date_timestamp', 'DESC')
                                    ->first();

            
            if (empty($oldData) == false) {

                $receivedTimestamp = date_create(date('Y-m-d H:i:s', $oldData->log_date_timestamp));

                $intervalOld = date_diff($receivedTimestamp, $currDateTime);
                if($intervalOld->days < $period){
                    return [
                        'days' => (int)$intervalOld->days,
                        'datetime' => $receivedTimestamp->format('Y-m-d H:i:s')
                    ];
                }
            }

            // Current use MySQL
            $currentData = ItemLog::where('uid', $uid)
                                    ->where('product_key', $productKey)
                                    ->where('status', 'success')
                                    ->orderBy('id', 'DESC')
                                    ->first();
                            
            if (isset($currentData) && !empty($currentData->log_datetime)) {

                $currentTimestamp = date_create($currentData->log_datetime);
                
                $interval = date_diff($currentTimestamp, $currDateTime);
                // dd($interval->days);
                
                return [
                    'days' => $interval->days,
                    'datetime' => $currentTimestamp->format('Y-m-d H:i:s')
                ];
                 
            }
            
            return false;
        }

        public function getClaimVipReward(Request $request){
            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'claim_vip_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('key') == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (2)'
                ]);
            }

            $productKey = $decoded['key'];

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $member = Member::where('uid', $uid)->first();

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimestamp = time();

            // get member vip info
            $userVip = $this->vipInfo($member->uid);
            $vip_level = isset($userVip) && isset($userVip->response->level_id) ? (int)$userVip->response->level_id : 1;
            $vip_active = ($vip_level > 0 && $userVip->response->activation == 'true') ? true : false;

            // set receive status
            $canReceive = false;
            $received = false;
            $notEnoughVipLevel = false;
            $inactiveVipStatus = false;

            $product_key = '';
            $product_id = 0;
            $product_code = 0;
            $product_title = '';
            $product_quantity = 0;
            $item_vip_level = 1;

            switch($productKey){

                case "vip_run_effect":

                    // vip run effect daterange to receive
                    $vipRunEffectInfo = Reward::where('product_key', 'vip_run_effect')->first();
                    $vipRunEffectDateRange = $this->checkItemDatediff($member->uid,'vip_run_effect',$vipRunEffectInfo->vip_level,$vipRunEffectInfo->period);
                    if(isset($vipRunEffectDateRange) && $vipRunEffectDateRange != false){
                        $vipEffectNextReceive = date('Y-m-d H:i:s',strtotime('+'.$vipRunEffectInfo->period.' days',strtotime($vipRunEffectDateRange['datetime'])));
                        $vipEffectCanReceive = $logDateTimestamp >= strtotime($vipEffectNextReceive) ? true : false;
                        $vipEffectReceived = $logDateTimestamp <  strtotime($vipEffectNextReceive) ? true : false;
                    }else{
                        $vipEffectCanReceive = true;
                        $vipEffectReceived = false;
                    }

                    $canReceive = $vipEffectCanReceive && $vip_active == true && $vip_level >= $vipRunEffectInfo->vip_level && $vipRunEffectInfo->vip_level > 0 ? true : false;
                    $received = $vipEffectReceived;
                    $item_vip_level = $vipRunEffectInfo->vip_level;
                    $product_key = $vipRunEffectInfo->product_key;
                    $product_id = $vipRunEffectInfo->product_id;
                    $product_code = $vipRunEffectInfo->product_code;
                    $product_title = $vipRunEffectInfo->product_title;
                    $product_quantity = $vipRunEffectInfo->product_quantity;

                    break;

                case "cold_storage_reset":

                    // vip ice storage reset token
                    $vipColdStorageInfo = Reward::where('product_key', 'cold_storage_reset')->first();
                    $vipColdStorageResetDateRange = $this->checkItemDatediff($member->uid,'cold_storage_reset',$vipColdStorageInfo->vip_level,$vipColdStorageInfo->period);
                    if(isset($vipColdStorageResetDateRange) && $vipColdStorageResetDateRange != false){
                        $vipColdStorageNextReceive = date('Y-m-d H:i:s',strtotime('+'.$vipColdStorageInfo->period.' days',strtotime($vipColdStorageResetDateRange['datetime'])));
                        $vipColdStorageCanReceive = $logDateTimestamp >= strtotime($vipColdStorageNextReceive) ? true : false;
                        $vipColdStorageReceived = $logDateTimestamp < strtotime($vipColdStorageNextReceive) ? true : false;
                    }else{
                        $vipColdStorageCanReceive = true;
                        $vipColdStorageReceived = false;
                    }

                    $canReceive = $vipColdStorageCanReceive && $vip_active == true && $vip_level >= $vipColdStorageInfo->vip_level && $vipColdStorageInfo->vip_level > 0 ? true : false;
                    $received = $vipColdStorageReceived;
                    $item_vip_level = $vipColdStorageInfo->vip_level;
                    $product_key = $vipColdStorageInfo->product_key;
                    $product_id = $vipColdStorageInfo->product_id;
                    $product_code = $vipColdStorageInfo->product_code;
                    $product_title = $vipColdStorageInfo->product_title;
                    $product_quantity = $vipColdStorageInfo->product_quantity;

                    break;

                case "vip_gachapon_box":

                    // check daily vip gachapon
                    $gachaponInfo = Reward::where('product_key', 'vip_gachapon_box')->where('vip_level', $vip_level)->first();
                    $gachaponReceivedToday = ItemLog::where('uid', $member->uid)
                                                        ->where('product_key', 'vip_gachapon_box')
                                                        ->where('status', 'success')
                                                        ->where('log_date', $logDate)
                                                        ->count();
                                                        
                    $canReceive = $gachaponReceivedToday == 0 && $vip_active == true ? true : false;
                    $received = $gachaponReceivedToday > 0 ? true : false;
                    $item_vip_level = $gachaponInfo->vip_level;         
                    $product_key = $gachaponInfo->product_key;
                    $product_id = $gachaponInfo->product_id;
                    $product_code = $gachaponInfo->product_code;
                    $product_title = $gachaponInfo->product_title;
                    $product_quantity = $gachaponInfo->product_quantity;              

                    break;

            }

            if($received == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณได้รับของรางวัลไปก่อนหน้านี้แล้ว'
                ]);
            }

            if($canReceive == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย ไม่สามารถรับของรางวัลได้'
                ]);
            }

            if(empty($product_code) || empty($product_quantity) || $product_key == ''){
                return response()->json([
                    'status' => false,
                    'message' => 'No data required.'
                ]);
            }

            // set reward
            $goods_data[] = [
                'goods_id' => $product_code,
                'purchase_quantity' => $product_quantity,
                'purchase_amount' => 0,
                'category_id' => 40,
            ];

            $productList[] = [
                'title' => $product_title,
                'quantity' => $product_quantity,
            ];

            // create item log
            $sendItemLog = $this->addItemHistoryLog([
                'uid'                           => $member->uid,
                'username'                      => $member->username,
                'ncid'                          => $member->ncid,
                'vip_level'                     => $item_vip_level,
                'product_key'                   => $product_key,
                'product_id'                    => $product_id,
                'product_code'                  => $product_code,
                'product_title'                 => $product_title,
                'product_quantity'              => $product_quantity,
                'product_set'                   => json_encode($productList),
                'send_item_status'              => false,
                'send_item_purchase_id'         => 0,
                'send_item_purchase_status'     => 0,
                'goods_data'                    => json_encode($goods_data),
                'status'                        => 'pending',
                'log_date'                      => (string)date('Y-m-d'), // set to string
                'log_datetime'                  => (string)date('Y-m-d H:i:s'), // set to string
                'log_date_timestamp'            => time(),
                'last_ip'                       => $this->getIP(),
            ]);


            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);

            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                // update send item log
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ?: false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                    'status' => 'success'
                        ], $sendItemLog['id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                            'status' => true,
                            'message' => "ได้รับ ".$product_title." จำนวน x".$product_quantity."<br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                            'data' => $eventInfo
                        ]);
            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                        ], $sendItemLog['id'], $uid);

                // $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                            // 'data' => $eventInfo
                        ]);
            }

        }

    }