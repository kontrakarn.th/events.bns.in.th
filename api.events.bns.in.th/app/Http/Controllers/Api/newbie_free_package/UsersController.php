<?php

    namespace App\Http\Controllers\Api\newbie_free_package;

    use App\Http\Controllers\Api\BnsEventController;

    use App\Models\newbie_free_package\Users;

class UsersController extends BnsEventController{

        function userCanGetReward($uid){
            return Users::where('uid',$uid)->value('can_get_package');
        }
        function updateUserCanGetReward($uid){
            $finduser = Users::where('uid',$uid)->first();
            $updateuser = Users::find($finduser->id);
            $updateuser->can_get_package = 2;
            $updateuser->save();
            $returnuser = Users::where('uid',$updateuser->uid)->first();
            $canget = $returnuser->can_get_pcakage;
            return intval($canget);
        }
    }
