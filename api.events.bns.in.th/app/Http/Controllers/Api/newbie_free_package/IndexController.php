<?php

    namespace App\Http\Controllers\Api\newbie_free_package;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;
    use App\Models\newbie_free_package\Setting;
    use App\Models\newbie_free_package\Product;
    use App\Models\newbie_free_package\Users;

    use App\Http\Controllers\Api\newbie_free_package\UsersController;
    use App\Http\Controllers\Api\newbie_free_package\HistoryController;
    use App\Jobs\newbie_free_package\SendItemToUserQueue;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2018-12-14 00:00:00';
        private $endTime = '2018-12-20 23:59:59';

        private $startApiTime = '00:00:00';
        private $endApiTime = '23:59:59';

       public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);
            $this->checkEventStatus();
            $this->userscontroller = New UsersController;
           // $this->buykeycontroller = New BuyKeyController;
            $this->historycontroller = New HistoryController;

        }

        public function GetKey(Request $request){
            return $this->buykeycontroller->GetKey($request,$this->userData["uid"]);

        }
        public function GetHistory(Request $request){
            return $this->historycontroller->GetHistory($this->userData["uid"]);
        }
        private function getRegisterStatus($member)
        {
            if (is_null($member->code)) {
                return 'can_register';
            }

            if (!is_null($member->code) && is_null($member->duo_uid)) {
                return 'not_completed_register';
            }

            if (!is_null($member->code) && !is_null($member->duo_uid)) {
                return 'completed_register';
            }
        }
        private function apiSendItem(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name'             => 'bns',
                'service'              => 'send_item',
                'user_id'              => $ncid,
                'purchase_description' => 'Sending item from garena events BNS Mystic.',
                'goods'                => json_encode($goodsData),
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:MYSTIC:LIVE:CHARACTERS_' . $uid, 10, function () use ($ncid) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'characters',
                    'user_id'  => $ncid,
                ]);
            });
        }
        private function doGetChar($uid, $ncid)
        { //new fixed function
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i       = 1;
                    foreach ($resp->response as $key => $value) {
                        $content[] = [
                            'id' => $i,
                            'char_id'   => $value->id,
                            'char_name' => $value->name,
                            'world_id' => $value->world_id,
                            'job' => $value->job,
                            'level' => $value->level,
                            'creation_time' => $value->creation_time,
                            'last_play_start' => $value->last_play_start,
                            'last_play_end' => $value->last_play_end,
                            'last_ip' => $value->last_ip,
                            'exp' => $value->exp,
                            'mastery_level' => $value->mastery_level,
                        ];

                        $i++;
                    }
                    return $content;
                }
            }
            return null;
        }
        private function setEventInfo($uid)
        {
            $member = Users::where('uid', $uid)->first();

            if (!$member) {
                return [
                    'status' => false,
                    'message' => 'No member data.'
                ];
            }

            if ($member->char_id == 0) {

                $charData = $this->doGetChar($member->uid, $member->ncid);

                $content = [];

                $content = collect($charData)
                    ->filter(function ($obj) {
                        return $obj['level'] >= 60 && $obj['mastery_level'] >= 15;
                    })
                    ->map(function ($value) {
                        return [
                            'id' => $value['id'],
                            'char_name' => $value['char_name'],
                        ];
                    })
                    ->values();
                    $canget = $this->userscontroller->userCanGetReward($member->uid);

                return [
                    'status' => true,
                    'username' => $member->username,
                    //'register_status' => 'can_register',
                    'can_get_package' => intval($canget),
                    //'product_slot' => $this->userscontroller->getProductslot($member->uid),
                   // 'special_product' => $this->userscontroller->GetUserSpecialProduct($member->uid),
                    //'hongmoon_key' =>$this->userscontroller->GetAmountUserKeys($member->uid)
                ];
            }
            $canget = $this->userscontroller->userCanGetReward($member->uid);

            return [
                'status' => true,
                "message" => 'Event Info',
                'username' => $member->username,
                //'register_status' => $this->getRegisterStatus($member),
                'can_get_package' => intval($canget),
                //  'product_slot' => $this->userscontroller->getProductslot($member->uid),
              //  'special_product' => $this->userscontroller->GetUserSpecialProduct($member->uid),
              //  'hongmoon_key' =>$this->userscontroller->GetAmountUserKeys($member->uid)
            ];
        }

        public function getEventInfo(Request $request)
    {

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
         $checkAgent = $request->header('user-agent');
         $browser ="";
        if(strstr($checkAgent,"Mobile"))
        {
            $browser = "Mobile";
        }else if(strstr($checkAgent,"BnsIngameBrowser") ){
            $browser = "HongMoon Pad";
        }else if(strstr($checkAgent,"Postman")){
            $browser = "Postman";
        }else if(strstr($checkAgent,"Macintosh") || strstr($checkAgent,"Windows")){
            $browser = "PC";
        }
        if($this->isIngame() == true){
            $browser = "HongMoon Pad";
        }
        $member = Users::where('uid', $uid)->first();
        $updateAgent = Users::find($member->id);
        $updateAgent->user_agent = $checkAgent;
        $updateAgent->browser = $browser;
        $updateAgent->save();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($member->char_id !== 0 && !is_null($member->duo_uid)) {
            $this->createDailyQuestLog($member);
            $this->checkDailyQuestLog($member);
        }

        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] == false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message'],
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }
        private function getEventSetting()
        {
            return Setting::where('active', 1)->first();
        }
        private function checkEventStatus()
        {
            /*if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }*/

            // get events setting info
            $eventSetting = $this->getEventSetting();

            if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if ($this->checkIsMaintenance()) {
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if ($this->isLoggedIn()) {
                // check and create member
                if ($this->checkMember() == false) {
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            } else {
                die(json_encode([
                    'status' => false,
                    'type' => 'not_login',
                    'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                ]));
            }
        }

        private function getIP()
        {
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip  = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData) {
                return true;
            } else {
                return false;
            }
        }

        private function is_accepted_ip()
        {
            $allow_ips   = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid      = $this->userData['uid'];
            $username = $this->userData['username'];

            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }

                $arr = [
                    'uid'              => $uid,
                    'username'         => $username,
                    'ncid'             => $ncid,
                    'char_id'          => 0,
                    'char_name'        => '',
                    'world_id'         => 0,
                    'total_points'     => 0,
                    'used_points'      => 0,
                    'total_tokens'     => 0,
                    'used_tokens'      => 0,
                    'can_get_package'   =>  1, //Can = 1
                    'already_purchased' => 0,
                    'last_ip'          => $this->getIP(),

                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
                $this->userEvent = $resp;
            } else {
                $member = Users::where('uid', $uid)->first();
                if (empty($member->ncid)) {
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if (empty($ncid)) {
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
                $this->userEvent = $member;
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Users::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Users::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if (empty($_id)) {
                return false;
            }

            return Users::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Users::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Users::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:MYSTIC:LIVE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'user_info',
                    'uid'      => $uid,
                ]);
                $userNcInfo       = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }
            });
        }

        public function getItem(Request $request){
            $checkAgent = $request->header('user-agent');
            $browser ="";
            if(strstr($checkAgent,"Mobile"))
            {
                $browser = "Mobile";
            }else if(strstr($checkAgent,"BnsIngameBrowser") ){
                $browser = "HongMoon Pad";
            }else if(strstr($checkAgent,"Postman")){
                $browser = "Postman";
            }else if(strstr($checkAgent,"Macintosh") || strstr($checkAgent,"Windows")){
                $browser = "PC";
            }
            if($this->isIngame() == true){
                $browser = "HongMoon Pad";
            }
            $uid      = $this->userData['uid'];
            $member = Users::where('uid', $uid)->first();
            $canget = $this->userscontroller->userCanGetReward($uid);
            if ($canget == 2) {
                $data =[
                    'status' => true,
                    'username' => $member->username,
                    'message' => "<span>ได้รับของรางวัลแล้ว<br/>ไอเทมถูกส่งเข้ากล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อกดรับ</span>",
                    'can_get_package' => $this->userscontroller->userCanGetReward($uid),
                ];
                return response()->json([
                    'status' => false,
                    'message' => "รับของรางวัลไปแล้ว",
                ]);
            }
            $this->userscontroller->updateUserCanGetReward($uid);
            $eventInfo = $this->setEventInfo($uid);
            $ncid = $this->getNcidByUid($member->uid);
             $product = Product::all();
             foreach($product as $p){
            $goods_data = [];
            $goods_data[] = [
                'goods_id' => $p["product_id"],
                'purchase_quantity' => 1,
                'purchase_amount' => 0,
                'category_id' => 40,
            ];
            $savehistoryid=   $this->historycontroller->saveHistory($p->id,2,1,$uid,$checkAgent,$browser);
            SendItemToUserQueue::dispatch($ncid, $goods_data,$savehistoryid,$this->historycontroller)->onQueue('bns_newbie_free_support_package_senditem');
            }

            return response()->json([
                'status' => true,
                'data' => $eventInfo,
                'message' => "<span>ได้รับของรางวัลแล้ว<br/>ไอเทมถูกส่งเข้ากล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อกดรับ</span>",
            ]);

        }



    }
