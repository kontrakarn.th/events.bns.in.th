<?php

namespace App\Http\Controllers\Api\buffet_v2;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

use App\Models\buffet_v2\Member;
use App\Models\buffet_v2\Reward;
use App\Models\buffet_v2\ItemLog;
use App\Models\buffet_v2\PlayLogs;
use App\Models\buffet_v2\Quest;
use App\Models\buffet_v2\QuestStat;

class IndexController extends BnsEventController
{

    private $baseApi            = 'http://api.apps.garena.in.th';
    // for test
    //private $startTime          = '2020-08-04 00:00:00';
    // for live
    private $startTime          = '2020-08-12 12:00:00';
    private $endTime            = '2020-09-08 23:59:59';
    private $endOfRedeemTime    = '2020-09-08 23:59:59';
    private $questList          = [
        [
            'level' => 1,
            'quest' => []
        ],
        [
            'level' => 2,
            'quest' => []
        ],
        [
            'level' => 3,
            'quest' => []
        ],
    ];

    public function __construct(Request $request)
    {

        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);
        $this->checkInitEvent();
        // get questList
        $this->initQuestList();
    }
    private function initQuestList()
    {
        $questList = [
            '1' => ['level'=>1,'quest'=>[]],
            '2' => ['level'=>2,'quest'=>[]],
            '3' => ['level'=>3,'quest'=>[]]
        ];
        $tmp = Quest::get()->toArray();
        foreach($tmp as $quest){
            $key = (string)$quest['group_no'];
            if(isset($questList[$key]['quest'])){
                $questList[$key]['quest'][] = [
                    'id'    => $quest['quest_id'],
                    'name'  => $quest['quest_name'],
                ];
            }
        }
        // convert associative array to indexed array and replace new dataset to questList
        $this->questList = array_values($questList);
    }
    private function getEventTime() :array {
        $data_set = [
            'event_can_play' => false,
            'event_can_redeem' => false
        ];
        if( time() >= strtotime($this->startTime) && time() <= strtotime($this->endTime)){
            $data_set['event_can_play'] = true;
        }

        if( time() >= strtotime($this->startTime) && time() <= strtotime($this->endOfRedeemTime)){
            $data_set['event_can_redeem'] = true;
        }
        return $data_set;
    }
    private function checkRedeemStatus(){
        $event_rules = $this->getEventTime();
        if($event_rules['event_can_redeem'] === false){
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }
    }
    private function checkInitEvent(){
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status' => false,
                'type' => 'no_permission',
                'message' => 'Sorry, you do not have permission.'
            ]));
        }
        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status' => false,
                'type' => 'maintenance',
                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
            ]));
        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                ]));
            }
        } else {
            die(json_encode([
                'status' => false,
                'type' => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
            ]));
        }
    }
    private function checkEventStatus()
    {
        $event_rules = $this->getEventTime();
        if($event_rules['event_can_play'] === false){
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        if ($this->userData)
            return true;
        else
            return false;
    }

    private function is_accepted_ip()
    {
        $allow_ips = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function checkMember()
    {
        $uid = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid' => $uid,
                'username' => $username,
                'ncid' => $ncid,
                'character_id' => 0,
                'character_name' => "",
                'ticket_group_1' => 0,
                'ticket_group_1_used' => 0,
                'ticket_group_2' => 0,
                'ticket_group_2_used' => 0,
                'ticket_group_3' => 0,
                'ticket_group_3_used' => 0,
                'token' => 0,
                'token_used' => 0,
                'created_at_string' => Carbon::now()->toDateString(),
                'last_ip' => $this->getIP()
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['_id']) == false) {
                return false;
            }
        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id))
            return false;

        return Member::where('_id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:BUFFET_V2:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'user_info',
                'uid' => $uid,
            ]);
            $userNcInfo = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }
        });
    }

    private function dosendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Test sending item from garena events bns Buffet.',
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function doGetChar($uid, $ncid)
    { //new fixed function
        $resp_raw = $this->apiCharacters($uid, $ncid);

        $resp = json_decode($resp_raw);
        if (is_object($resp)) {
            if ($resp->status) {
                $content = [];
                $i = 1;
                foreach ($resp->response as $key => $value) {
                    $content[] = [
                        // 'id' => $i,
                        'char_id' => $value->id,
                        'char_name' => $value->name,
                        // 'world_id' => $value->world_id,
                        // 'job' => $value->job,
                        // 'level' => $value->level,
                        // 'creation_time' => $value->creation_time,
                        // 'last_play_start' => $value->last_play_start,
                        // 'last_play_end' => $value->last_play_end,
                        // 'last_ip' => $value->last_ip,
                        // 'exp' => $value->exp,
                        // 'mastery_level' => $value->mastery_level,
                    ];

                    $i++;
                }
                return $content;
            }
        }
        return null;
    }

    private function calQuest($uid, $charId)
    {
        $quest = $this->questList;
        $numpassquest = [
            0, // gacha group A
            0, // gacha group B
            0 // gacha group C
        ];
        $questStat = QuestStat::where(
            [
                ['uid', $uid],
                ['character_id', $charId]
            ]
        )->get()->toArray();
        $hashQuest = [];
        foreach($questStat as $v){
            $hashQuest[(int)$v['quest_id']] = (int)$v['completed_count'];
        }
        foreach ($quest as $key => $value) {
            $totalpass = 0;
            foreach ($value['quest'] as $key_q => $value_q) {
                if(isset($hashQuest[(int)$value_q['id']])){
                    // จำนวนที่ผ่าน quest
                    $totalpass += $hashQuest[(int)$value_q['id']];
                }
            }
            $numpassquest[$key] = $totalpass;
        }
        return $numpassquest;
    }

    private function addItemHistoryLog($arr)
    {
        return ItemLog::create($arr);
    }

    private function updateItemHistoryLog($arr, $log_id, $uid)
    {
        return ItemLog::where('_id', $log_id)->where('uid', $uid)->update($arr);
    }

    private function checkItemHistory($uid, $package_id)
    {
        $itemHistory = ItemLog::where('uid', $uid)
            ->where('package_id', $package_id)
            // ->where('status', 'success')
            ->count();
        return $itemHistory;
    }

    private function setEventInfo($uid)
    {
        if (empty($uid)) {
            return false;
        }
        $member = Member::where('uid', $uid)->first();
        if ($member->character_id == 0) {

            $mychar = $this->doGetChar($member->uid, $member->ncid);

            return [
                'username' => $member->username,
                'character_name' => '',
                'selectd_char' => false,
                'characters' => $mychar,
            ];
        }
        // dd($member->character_id);
        $cal_quest = $this->calQuest($member->uid, $member->character_id);
        $member->ticket_group_1 = $cal_quest[0];
        $member->ticket_group_2 = $cal_quest[1];
        $member->ticket_group_3 = $cal_quest[2];
        $member->save();

        $can_play_1 = false;
        $count_playable_1   = $member->ticket_group_1 - $member->ticket_group_1_used;
        $count_playable_2   = $member->ticket_group_2 - $member->ticket_group_2_used;
        $count_playable_3   = $member->ticket_group_3 - $member->ticket_group_3_used;
        $cumu_playable_1    = $member->ticket_group_1;
        $cumu_playable_2    = $member->ticket_group_2;
        $cumu_playable_3    = $member->ticket_group_3;
        if ($count_playable_1 > 0) {
            $can_play_1 = true;
        }
        // $can_play_1=true;

        $can_play_2 = false;
        if ($count_playable_2 > 0) {
            $can_play_2 = true;
        }
        // $can_play_2=true;

        $can_play_3 = false;
        if ($count_playable_3 > 0) {
            $can_play_3 = true;
        }
        // $can_play_3=true;
        $event_rules    = $this->getEventTime();
        return [
            'username' => $member->username,
            'character_name' => $member->character_name,
            'selectd_char' => true,
            'characters' => [],
            'can_play_1' => $can_play_1,
            'can_play_2' => $can_play_2,
            'can_play_3' => $can_play_3,
            'token' => $member->token - $member->token_used,
            'count_playable_1' => $count_playable_1,
            'count_playable_2' => $count_playable_2,
            'count_playable_3' => $count_playable_3,
            'cumu_playable_1' => $cumu_playable_1,
            'cumu_playable_2' => $cumu_playable_2,
            'cumu_playable_3' => $cumu_playable_3,
            'event_can_play' => $event_rules['event_can_play'],
            'event_can_redeem' => $event_rules['event_can_redeem'],
        ];
    }

    public function getEventInfo(Request $request)
    {
        $this->checkRedeemStatus();
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        // $ncid = $this->getNcidByUid($uid);
        // $uid=200062;
        // $ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
        // get member info
        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $eventInfo      = $this->setEventInfo($uid);
        return response()->json([
            'status' => true,
            'data' => $eventInfo,
        ]);
    }

    public function selectCharacter(Request $request)
    {
        $this->checkEventStatus();
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'select_character') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }
        if ($decoded->has('id') == false) { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (2)'
            ]);
        }

        $char_id = (int)$decoded['id'];

        $userData = $this->userData;
        $uid = $userData['uid'];

        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบข้อมูล'
            ]);
        }

        if ($member->character_id > 0) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถเปลี่ยนตัวละครได้'
            ]);
        }

        $mychar = $this->doGetChar($member->uid, $member->ncid);
        $myselect = collect($mychar)->where('char_id', $char_id);

        if (count($myselect) == 0) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบตัวละคร'
            ]);
        }
        // dd($myselect)->first();
        $member->character_id = (int) $myselect->first()['char_id'];
        $member->character_name = $myselect->first()['char_name'];
        $member->save();

        $eventInfo = $this->setEventInfo($uid);

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }

    public function eatBuffet(Request $request)
    {
        $this->checkEventStatus();
        $decoded = $request;

        if ($decoded->has('type') == false || $decoded['type'] != 'eat_buffet') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (1)'
            ]);
        }

        if ($decoded->has('id') == false) { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (2)'
            ]);
        } elseif ($decoded->id < 1 || $decoded->id > 3) {
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (3)'
            ]);
        }

        $id = $decoded->id;

        $userData = $this->userData;
        $uid = $userData['uid'];


        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบข้อมูล'
            ]);
        }

        if ($member->character_id == 0) {
            return response()->json([
                'status' => false,
                'message' => 'โปรดเลือกตัวละครก่อน'
            ]);
        }


        $ticket = 0;
        $ticket_used = 0;
        $remain_ticket = 0;

        if ($id == 1) {
            $ticket = $member->ticket_group_1;
            $ticket_used = $member->ticket_group_1_used;
            $member->ticket_group_1_used = $ticket_used + 1;
        } elseif ($id == 2) {
            $ticket = $member->ticket_group_2;
            $ticket_used = $member->ticket_group_2_used;
            $member->ticket_group_2_used = $ticket_used + 1;
        } elseif ($id == 3) {
            $ticket = $member->ticket_group_3;
            $ticket_used = $member->ticket_group_3_used;
            $member->ticket_group_3_used = $ticket_used + 1;
        }

        $remain_ticket = $ticket - $ticket_used;
        if ($remain_ticket <= 0) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถเล่นได้ คุณยังไม่สิทธิ์'
            ]);
        }

        //add play logs
        $playlogs = new PlayLogs;
        $playlogs->uid = $member->uid;
        $playlogs->character_id = $member->character_id;
        $playlogs->play_type = (int) $id;
        $playlogs->created_at_string = Carbon::now()->toDateString();
        if (!$playlogs->save()) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด ลองใหม่อีกครั้ง'
            ]);
        }

        $playlogs_id = $playlogs->id;
        //update ticket (set on above)
        $member->save();
        $reward = new Reward;
        //random item
        $item_get = $reward->getRandomGachapon($id);
        //send item
        // $goods_data = []; //good data for send item group
        // foreach ($productList as $key => $product) {
        $goods_data[] = [
            'goods_id' => $item_get['product_id'],
            'purchase_quantity' => $item_get['product_quantity'],
            'purchase_amount' => 0,
            'category_id' => 40
        ];
        // }
        if ($item_get['item_type'] == "events") {
            $sendItemLog = $this->addItemHistoryLog([
                'uid' => $member->uid,
                'ncid' => $member->ncid,
                'play_id' => $playlogs_id,
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
                'item_no' => $item_get['id'],
                'group' => $item_get['group'],
                'item_type' => $item_get['item_type'],
                'package_title' => $item_get['product_title'],
                'token' => 0,
                'product_set' => $item_get,
                'status' => 'success',
                'send_item_status' => true,
                'send_item_purchase_id' => 0,
                'send_item_purchase_status' => 0,
                'goods_data' => json_encode($goods_data),
                'created_at_string' => Carbon::now()->toDateString(),
                'created_at_string_time' => Carbon::now()->toDateTimeString(),
                'last_ip' => $this->getIP()
            ]);

            $member->token = $member->token + $item_get['product_quantity'];
            $member->save();
            $eventInfo = $this->setEventInfo($member->uid);

            return response()->json([
                'status' => true,
                'message' => "สำเร็จ",
                'data' => $eventInfo,
                'reward' => [
                    'icon' => $item_get['icon'],
                    'name' => $item_get['product_title'],
                ]
            ]);
        } else {

            $sendItemLog = $this->addItemHistoryLog([
                'uid' => $member->uid,
                'ncid' => $member->ncid,
                'play_id' => $playlogs_id,
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
                'item_no' => $item_get['id'],
                'group' => $item_get['group'],
                'item_type' => $item_get['item_type'],
                'package_title' => $item_get['product_title'],
                'token' => 0,
                'product_set' => $item_get,
                'status' => 'pending',
                'send_item_status' => false,
                'send_item_purchase_id' => 0,
                'send_item_purchase_status' => 0,
                'goods_data' => json_encode($goods_data),
                'created_at_string' => Carbon::now()->toDateString(),
                'created_at_string_time' => Carbon::now()->toDateTimeString(),
                'last_ip' => $this->getIP()
            ]);

            $send_result_raw = $this->dosendItem($member->ncid, $goods_data);

            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ? $send_result->status : false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ? $send_result->response->purchase_status : 0,
                    'status' => 'success'
                ], $sendItemLog['_id'], $member->uid);

                $eventInfo = $this->setEventInfo($member->uid);

                return response()->json([
                    'status' => true,
                    'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                    'data' => $eventInfo,
                    'reward' => [
                        'icon' => $item_get['icon'],
                        'name' => $item_get['product_title'],
                    ]
                ]);
            } else {
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                ], $sendItemLog['_id'], $member->uid);

                // $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                    'status' => false,
                    'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                    // 'data' => $eventInfo
                ]);
            }
        }
    }

    public function redeemReward(Request $request)
    {
        $this->checkRedeemStatus();
        $decoded = $request;

        if ($decoded->has('type') == false || $decoded['type'] != 'redeem') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (1)'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        // $uid=200062;
        // $ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
        if ($decoded->has('package_id') == false) { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (2)'
            ]);
        } elseif ($decoded->package_id < 1 || $decoded->package_id > 8) {
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (3)'
            ]);
        }

        $package_id = (int)$decoded['package_id'];
        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($member->character_id == 0) {
            return response()->json([
                'status' => false,
                'message' => 'โปรดเลือกตัวละครก่อน'
            ]);
        }
        // $member->uid=200062;
        // $member->ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
        // check item history

        $reward = new Reward;

        $packageInfo = $reward->setRewardByPackage($package_id);

        if ($packageInfo == false) {
            return response()->json([
                'status' => false,
                'message' => 'No data required.'
            ]);
        }

        $totalToken = $member->token;
        $remain_token = $member->token - $member->token_used;

        if ($remain_token < $packageInfo['require_token']) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />จำนวนคูปองกินบุฟเฟ่ต์ของคุณไม่พอ'
            ]);
        }


        // set reward packages
        $goods_data = []; //good data for send item group
        $packageId = $packageInfo['package_id'];
        $requireToken = $packageInfo['require_token'];
        $packageTitle = $packageInfo['package_title'];

        $productList = $packageInfo['product_set'];

        foreach ($productList as $key => $product) {
            $goods_data[] = [
                'goods_id' => $product['product_id'],
                'purchase_quantity' => $product['product_quantity'],
                'purchase_amount' => 0,
                'category_id' => 40
            ];
        }

        $sendItemLog = $this->addItemHistoryLog([
            'uid' => $member->uid,
            'ncid' => $member->ncid,
            'play_id' => "",
            'log_date' => (string)date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
            'item_no' => $packageId,
            'group' => 0,
            'item_type' => 'redeem',
            'package_title' => $packageTitle,
            'token' => $requireToken,
            'product_set' => $productList,
            'status' => 'pending',
            'send_item_status' => false,
            'send_item_purchase_id' => 0,
            'send_item_purchase_status' => 0,
            'goods_data' => json_encode($goods_data),
            'created_at_string' => Carbon::now()->toDateString(),
            'created_at_string_time' => Carbon::now()->toDateTimeString(),
            'last_ip' => $this->getIP()
        ]);
        $member->token_used = $member->token_used + $requireToken;
        $member->save();
        $send_result_raw = $this->dosendItem($member->ncid, $goods_data);

        $send_result = json_decode($send_result_raw);
        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            $this->updateItemHistoryLog([
                'send_item_status' => $send_result->status ? $send_result->status : false,
                'send_item_purchase_id' => $send_result->response->purchase_id ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ? $send_result->response->purchase_status : 0,
                'status' => 'success'
            ], $sendItemLog['_id'], $member->uid);

            // $eventInfo = $this->setEventInfo($member->uid);

            return response()->json([
                'status' => true,
                'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                'data' => [
                    'token' => $member->token - $member->token_used
                ],
                // 'data' => $eventInfo,
                'reward' => [
                    'icon' => $packageInfo['icon'],
                    'name' => $packageTitle
                ]
            ]);
        } else {
            $this->updateItemHistoryLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
            ], $sendItemLog['_id'], $member->uid);

            // $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                'status' => false,
                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                'data' => [
                    'token' => $member->token - $member->token_used
                ],
                // 'data' => $eventInfo
            ]);
        }
    }

    public function getHistory(Request $request)
    {
        $this->checkRedeemStatus();
        $decoded = $request;

        if ($decoded->has('type') == false || $decoded['type'] != 'history') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (1)'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $history = ItemLog::where('uid', $member->uid)->select('package_title', 'token', 'created_at_string_time')->orderBy('created_at', 'DESC')->get();

        return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => $history,
        ]);
    }

    private function apiCharacters($uid, $ncid)
    {
        return Cache::remember('BNS:BUFFET_V2:CHARACTERS_' . $uid, 10, function () use ($ncid) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'characters',
                'user_id' => $ncid
            ]);
        });
    }
}
