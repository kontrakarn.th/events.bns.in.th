<?php

    namespace App\Http\Controllers\Api\chain_package;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\chain_package\Member;
    use App\Models\chain_package\DeductLog;
    use App\Models\chain_package\ItemLog;
    use App\Models\chain_package\Reward;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2019-12-03 12:00:00';
        private $endTime = '2019-12-18 23:59:59';

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            if($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย<br />ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){

                // check member has game info
                $uid = $this->userData['uid'];
                $username = $this->userData['username'];
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    die(json_encode([
                        'status' => false,
                        'type' => 'no_game_info',
                        'message' => 'ขออภัย<br />กรุณาล็อคอินเข้าเกมเพื่อสร้างตัวละคร<br />ก่อนร่วมกิจกรรม'
                    ]));
                }

                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาด<br />ไม่สามารถบักทึกข้อมูลของคุณได้<br />กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        private function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:CHAIN_PACKAGE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function diamondBalance(int $uid) {
            return $this->post_api($this->baseApi, [
                        'key_name' => 'bns',
                        'service' => 'balance',
                        'uid' => $uid,
            ]);
        }

        private function setRemainDiamonds($uid){
            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return 0;
            }else{
                return intval($diamondsBalance->balance);
            }
        }

        private function setDiamondDeductData($deduct_diamond_amount=0)
        {
            if($deduct_diamond_amount==0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamomds for garena BNS events Chain Package.',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addDeductLog(array $arr) {
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductLog(array $arr, $logId) {
            if ($arr) {
                return DeductLog::where('id', $logId)->update($arr);
            }
            return null;
        }

        private function dosendItem(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Sending item from garena BNS events chain Package.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
        }

        public function getEventInfo(Request $request)
        {

            if ($request->has('type') == false && $request->type != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
            ]);

        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            // rewards list and status
            $rewards = Reward::orderBy('package_key', 'ASC')->get();
            $rewardList = [];
            if(count($rewards) > 0){
                foreach($rewards as $rewardKey=>$rewardVal){

                    $canPurchase = false;
                    $alreadyPurchased = false;
                    $packageHistory = 0;

                    $packageHistory = ItemLog::where('uid', $member->uid)->where('package_key', $rewardVal->package_key)->count();

                    $packageConditionStatus = $this->checkPackageconditionStatus($member->uid,$rewardVal->package_key);

                    $canPurchase = ($diamonds >= $rewardVal->diamonds_required && $packageHistory <= 0 && $packageConditionStatus == true) ? true : false;

                    $alreadyPurchased = ($packageHistory > 0) ? true : false;

                    $rewardList[] = [
                        'id' => $rewardVal->package_key,
                        'title' => $rewardVal->package_name,
                        'can_purchase' => $canPurchase,
                        'already_purchased' => $alreadyPurchased,
                        'required_diamonds' => $rewardVal->diamonds_required
                    ];

                }
            }


            return [
                'username' => $member->username,
                // 'diamonds' => number_format($diamonds),
                'rewards' => $rewardList,
            ];


        }

        private function checkPackageconditionStatus($uid,$packageKey = 0){
            if(!in_array($packageKey, [1,2,3,4])){
                return false;
            }

            switch ($packageKey) {
                case 1:
                    return true;
                    break;

                case 2:
                    $package1history = ItemLog::where('uid', $uid)->where('package_key', 1)->count();

                    if($package1history > 0){
                        return true;
                    }else{
                        return false;
                    }
                    break;
                
                case 3:
                    $package1history = ItemLog::where('uid', $uid)->where('package_key', 1)->count();
                    $package2history = ItemLog::where('uid', $uid)->where('package_key', 2)->count();

                    if($package1history > 0 && $package2history > 0){
                        return true;
                    }else{
                        return false;
                    }
                    break;

                case 4:
                    $package1history = ItemLog::where('uid', $uid)->where('package_key', 1)->count();
                    $package2history = ItemLog::where('uid', $uid)->where('package_key', 2)->count();
                    $package3history = ItemLog::where('uid', $uid)->where('package_key', 3)->count();

                    if($package1history > 0 && $package2history > 0 && $package3history > 0){
                        return true;
                    }else{
                        return false;
                    }
                    break;
                
                default:
                    return false;
                    break;
            }

        }

        public function purchasePackage(Request $request) {

            if ($request->has('type') == false && $request->type != 'purchase_package') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }

            if ($request->has('package_key') == false || !in_array($request->package_key, [1,2,3,4])) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (2)!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);

            $package_key = $request->package_key;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                        ]);
            }

            // get reward info
            $reward = Reward::where('package_key', $package_key)->first();
            if(!$reward){
                return response()->json([
                    'status' => false,
                    'message' => 'No data.'
                ]);
            }

            $canPurchase = false;
            $alreadyPurchased = false;
            $packageHistory = 0;

            $packageHistory = ItemLog::where('uid', $member->uid)->where('package_key', $reward->package_key)->count();
            if($packageHistory > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้ซื้อแพ็คเกจนี้ไปแล้ว'
                ]);
            }

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);
            if($diamonds < $reward->diamonds_required){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอซื้อแพ็คเกจ'
                ]);
            }

            $packageConditionStatus = $this->checkPackageconditionStatus($member->uid,$reward->package_key);
            if($packageConditionStatus == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ต้องซื้อแ็คเกจตามลำดับก่อน'
                ]);
            }

            $logDeduct = null;
            if(in_array($reward->package_key,[1,2,3])){
                // set deduct diamonds
                $logDeduct = $this->addDeductLog([
                    'uid'                       => $member->uid,
                    'username'                  => $member->username,
                    'ncid'                      => $member->ncid,
                    'package_id'                => $reward->package_id,
                    'package_key'               => $reward->package_key,
                    'package_name'              => $reward->package_name,
                    'diamonds'                  => $reward->diamonds_required,
                    'status'                    => 'pending',
                    'log_date'                  => date('Y-m-d H:i:s'), // set to string
                    'log_date_timestamp'        => time(),
                    'last_ip'                   => $this->getIP(),
                ]);

                $deductData = $this->setDiamondDeductData($reward->diamonds_required);
                $respDeductRaw = $this->deductDiamond($ncid, $deductData);
                $respDeduct = json_decode($respDeductRaw);
                if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){

                    // update deduct log
                    $arrDeductDiamond = [
                        'before_deduct_diamond' => $diamonds,
                        'after_deduct_diamond' => $diamonds - $reward->diamonds_required,
                        'deduct_status' => $respDeduct->status ?: 0,
                        'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
                        'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'success'
                    ];

                    $respDeduct = $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);
                }else{
                    $arrDeductDiamond['status'] = 'unsuccess';
                    $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);

                    return response()->json([
                        'status' => false,
                        'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }

            }


            
            $goods_data = []; //good data for send item group
            $goods_data[] = [
                'goods_id' => $reward->package_id,
                'purchase_quantity' => $reward->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40
            ];

            $sendItemLog = $this->addItemHistoryLog([
                'uid'                       => $uid,
                'username'                  => $member->username,
                'ncid'                      => $ncid,
                'deduct_id'                 => (isset($logDeduct['id']) && !empty($logDeduct['id'])) ? $logDeduct['id'] : 0,
                'package_id'                => $reward->package_id,
                'package_key'               => $reward->package_key,
                'package_name'              => $reward->package_name,
                'diamonds'                  => $reward->diamonds_required,
                'status'                    => 'pending',
                'send_item_status'          => false,
                'send_item_purchase_id'     => 0,
                'send_item_purchase_status' => 0,
                'goods_data'                => json_encode($goods_data),
                'last_ip'                   => $this->getIP(),
                'log_date'                  => (string)date('Y-m-d'), // set to string
                'log_date_timestamp'        => time(),
            ]);
                
            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ?: false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                    'status' => 'success'
                ], $sendItemLog['id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                            'status' => true,
                            'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                            'content' => $eventInfo
                        ]);
            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                ], $sendItemLog['id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                            'content' => $eventInfo
                        ]);
            }
            
        }


    }