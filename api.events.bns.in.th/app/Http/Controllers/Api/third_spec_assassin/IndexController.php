<?php

    namespace App\Http\Controllers\Api\third_spec_assassin;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;
    use App\Models\bns_settings\BnsSettings;
    use App\Models\third_spec_assassin\Product;
    use App\Models\third_spec_assassin\Users;

    use App\Http\Controllers\Api\third_spec_assassin\UsersController;
    use App\Http\Controllers\Api\third_spec_assassin\HistoryController;
    use App\Http\Controllers\Api\third_spec_assassin\BuyPackageController;
    use App\Jobs\third_spec_assassin\SendItemToUserQueue;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2018-12-14 00:00:00';
        private $endTime = '2018-12-20 23:59:59';

        private $startApiTime = '00:00:00';
        private $endApiTime = '23:59:59';

       public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);
            $this->checkEventStatus();
            $this->userscontroller = New UsersController;
            $this->buypacakgecontroller = New BuyPackageController;
            $this->historycontroller = New HistoryController;

        }

        public function BuyPackage(Request $request){

            return $this->buypacakgecontroller->BuyPackage($request,$this->userData["uid"]);
        }

        public function sendbackItem(Request $request){

            return $this->buypacakgecontroller->sendbackItem($request,$this->userData["uid"]);
        }

        private function getRegisterStatus($member)
        {
            if (is_null($member->code)) {
                return 'can_register';
            }

            if (!is_null($member->code) && is_null($member->duo_uid)) {
                return 'not_completed_register';
            }

            if (!is_null($member->code) && !is_null($member->duo_uid)) {
                return 'completed_register';
            }
        }
        private function apiSendItem(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name'             => 'bns',
                'service'              => 'send_item',
                'user_id'              => $ncid,
                'purchase_description' => 'Sending item from garena events BNS Mystic.',
                'goods'                => json_encode($goodsData),
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:MYSTIC:LIVE:CHARACTERS_' . $uid, 10, function () use ($ncid) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'characters',
                    'user_id'  => $ncid,
                ]);
            });
        }



        public function getEventInfo(Request $request)
    {
        return  $this->userscontroller->getEventInfo($request,$this->userData);
    }
        private function getEventSetting()
        {
            return BnsSettings::where("event_name","third_spec_assassin")->where('active', 1)->first();
        }
        private function checkEventStatus()
        {
            if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            $eventSetting = $this->getEventSetting();

            if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if ($this->checkIsMaintenance()) {
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if ($this->isLoggedIn()) {
                // check and create member
                if ($this->checkMember() == false) {
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            } else {
                die(json_encode([
                    'status' => false,
                    'type' => 'not_login',
                    'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                ]));
            }
        }

        private function getIP()
        {
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip  = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData) {
                return true;
            } else {
                return false;
            }
        }

        private function is_accepted_ip()
        {
            $allow_ips   = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid      = $this->userData['uid'];
            $username = $this->userData['username'];

            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }

                $arr = [
                    'uid'              => $uid,
                    'username'         => $username,
                    'ncid'             => $ncid,
                    'char_id'          => 0,
                    'char_name'        => '',
                    'world_id'         => 0,
                    'total_points'     => 0,
                    'used_points'      => 0,
                    'total_tokens'     => 0,
                    'used_tokens'      => 0,
                    'can_get_package'   =>  1, //Can = 1
                    'already_purchased' => 0,
                    'last_ip'          => $this->getIP(),

                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
                $this->userEvent = $resp;
            } else {
                $member = Users::where('uid', $uid)->first();
                if (empty($member->ncid)) {
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if (empty($ncid)) {
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
                $this->userEvent = $member;
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Users::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Users::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if (empty($_id)) {
                return false;
            }

            return Users::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Users::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Users::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:MYSTIC:LIVE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'user_info',
                    'uid'      => $uid,
                ]);
                $userNcInfo       = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }
            });
        }





    }
