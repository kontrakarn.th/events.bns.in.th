<?php

namespace App\Http\Controllers\Api\hm_pass_ss2;

use App\Http\Controllers\Api\BnsEventController;
use App\Jobs\hm_pass_ss2\SendItems;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

use App\Models\hm_pass_ss2\Member;
use App\Models\hm_pass_ss2\Setting;
use App\Models\hm_pass_ss2\DeductLog;
use App\Models\hm_pass_ss2\ItemLog;
use App\Models\hm_pass_ss2\Quest;
use App\Models\hm_pass_ss2\QuestLog;
use App\Models\hm_pass_ss2\QuestLogCompensate;
use App\Models\hm_pass_ss2\Reward;
use App\Models\hm_pass_ss2\Week;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class IndexController extends BnsEventController
{
    private $baseApi = 'http://api.apps.garena.in.th';

    private $userEvent = null;

    public function __construct(Request $request)
    {
        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);

        $this->checkEventStatus();
    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status' => false,
                'type' => 'no_permission',
                'message' => 'Sorry, you do not have permission.'
            ]));
        }

        // get events setting info
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status' => false,
                'type' => 'maintenance',
                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
            ]));
        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                ]));
            }
        } else {
            die(json_encode([
                'status' => false,
                'type' => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    private function is_accepted_ip()
    {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function isLoggedIn()
    {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'              => $uid,
                'username'         => $username,
                'ncid'             => $ncid,
                'char_id'          => 0,
                'char_name'        => '',
                'world_id'         => 0,
                'total_points'     => 0,
                'used_points'      => 0,
                'total_tokens'     => 0,
                'used_tokens'      => 0,
                'already_purchased' => 0,
                'last_ip'          => $this->getIP(),
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
            $this->userEvent = $resp;
        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id)) {
            return false;
        }

        return Member::where('id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function getEventSetting()
    {
        return Setting::where('active', 1)->first();
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:HM_PASS_SS2:LIVE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'user_info',
                'uid'      => $uid,
            ]);
            $userNcInfo       = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }
        });
    }

    private function doGetChar($uid, $ncid)
    { //new fixed function
        $resp_raw = $this->apiCharacters($uid, $ncid);

        $resp = json_decode($resp_raw);
        if (is_object($resp)) {
            if ($resp->status) {
                $content = [];
                $i       = 1;
                foreach ($resp->response as $key => $value) {
                    $content[] = [
                        'id' => $i,
                        'char_id'   => $value->id,
                        'char_name' => $value->name,
                        'world_id' => $value->world_id,
                        'level' => $value->level,
                        'job' => $value->job,
                        'job_name' => $this->getJobName($value->job),
                        'level' => $value->level,
                        'creation_time' => $value->creation_time,
                        'last_play_start' => $value->last_play_start,
                        'last_play_end' => $value->last_play_end,
                        'last_ip' => $value->last_ip,
                        'exp' => $value->exp,
                        'mastery_level' => $value->mastery_level,
                    ];

                    $i++;
                }
                return $content;
            }
        }
        return null;
    }

    private function apiCharacters($uid, $ncid)
    {
        return Cache::remember('BNS:HM_PASS_SS2:LIVE:CHARACTERS_' . $uid, 10, function () use ($ncid) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'characters',
                'user_id'  => $ncid,
            ]);
        });
    }

    protected function getJobName($classId)
    {
        $className = '';
        switch ($classId) {
            case 1:
                $className = 'เบลด มาสเตอร์';
                break;

            case 2:
                $className = 'กังฟู มาสเตอร์';
                break;

            case 3:
                $className = 'ฟอร์ซ มาสเตอร์';
                break;

            case 4:
                $className = 'โซล กันเนอร์';
                break;

            case 5:
                $className = 'เดสทรอยเยอร์';
                break;

            case 6:
                $className = 'ซัมมอนเนอร์';
                break;

            case 7:
                $className = 'แอสแซสซิน';
                break;

            case 8:
                $className = 'เบลด แดนเซอร์';
                break;

            case 9:
                $className = 'วอร์ล็อค';
                break;

            case 10:
                $className = 'โซล ไฟท์เตอร์';
                break;

            case 11:
                $className = 'วอร์เดน';
                break;

            case 12:
                $className = 'อาร์เชอร์';
                break;

            default:
                break;
        }

        return $className;
    }

    private function createDeductLog(array $arr)
    {
        if ($arr) {
            $resp = DeductLog::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return false; //fix
            }
        }
        return null;
    }

    private function updateDeductLog(array $arr, $log_id)
    {
        if ($arr) {
            return DeductLog::where('id', $log_id)->update($arr);
        }
        return null;
    }

    private function apiDiamondBalance(int $uid)
    {
        return $this->post_api($this->baseApi, [
            'key_name' => 'bns',
            'service' => 'balance',
            'uid' => $uid,
        ]);
    }

    private function setDiamondsBalance($uid)
    {

        $diamondBalanceRaw = $this->apiDiamondBalance($uid);
        $diamondBalance = json_decode($diamondBalanceRaw);

        $diamonds = 0;
        if ($diamondBalance->status == true) {
            $diamonds = intval($diamondBalance->balance);
        }

        return $diamonds;
    }

    private function apiDeductDiamond(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Deduct diamomds for garena events BNS Hongmoon Pass Season 2.',
            'currency_group_id' => 71,
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function setDiamondDeductData($deduct_diamond_amount = 0)
    {
        if ($deduct_diamond_amount == 0)
            return false;

        return [
            [
                'goods_id' => 741,
                'purchase_quantity' => $deduct_diamond_amount,
                'purchase_amount' => $deduct_diamond_amount,
                'category_id' => 40
            ]
        ];
    }

    private function apiSendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Sending item from garena events BNS Hey Duo.',
            'goods'                => json_encode($goodsData),
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function addItemHistoryLog($arr)
    {
        return ItemLog::create($arr);
    }

    private function updateItemHistoryLog($arr, $log_id, $uid)
    {
        return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
    }

    public function selectCharacter(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'select_character') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }
        if ($decoded->has('id') == false) { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (2)'
            ]);
        }

        $charKey = (int) $decoded['id'];

        $userData = $this->userData;
        $uid = $userData['uid'];

        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบข้อมูล'
            ]);
        }

        if ($member->char_id > 0) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถเปลี่ยนตัวละครได้'
            ]);
        }

        $mychar = $this->doGetChar($member->uid, $member->ncid);
        $myselect = collect($mychar)->where('id', $charKey)->first();
        if (count($myselect) == 0) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบตัวละคร'
            ]);
        }

        $member->char_id   = (int) $myselect['char_id'];
        $member->char_name = $myselect['char_name'];
        $member->world_id       = $myselect['world_id'];
        $member->level = $myselect['level'];
        $member->mastery_level = $myselect['mastery_level'];
        $member->job = $myselect['job'];
        $member->job_name = $myselect['job_name'];
        $member->save();

        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] == false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message'],
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }

    public function getEventInfo(Request $request)
    {

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] == false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message'],
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }

    private function setEventInfo($uid)
    {
        $member = Member::where('uid', $uid)->first();

        if (!$member) {
            return [
                'status' => false,
                'message' => 'No member data.'
            ];
        }

        if ($member->char_id == 0) {

            $charData = $this->doGetChar($member->uid, $member->ncid);

            $content = [];

            $content = collect($charData)
                ->map(function ($value) {
                    return [
                        'id' => $value['id'],
                        'char_name' => $value['char_name'],
                        'level' => $value['level'],
                        'job_name' => $value['job_name'],
                    ];
                })
                ->values();

            return [
                'status' => true,
                'message' => 'Event Info',
                'username' => $member->username,
                'character_name' => '',
                'selected_char' => false,
                'characters' => $content,
                'current_points' => 0,
                'hm_pass_lvl' => 0,
                'unlocked_hm_pass_lvl' => 0,
                'is_unlocked' => false,
                'is_adv_unlocked' => false,
                'can_purchase_level' => false,
                'can_claim_all' => false,
                'claim_special_gift_status' => 'not_in_condition',
                'daily_quests' => $this->getDailyQuests($member),
                'weekly_quests' => $this->getWeeklyQuests($member),
                'rewards' => [],
                'show_compensate_history' => false,
            ];
        }

        $this->createDailyQuestLogs($member);
        $this->createWeeklyQuestLogs($member);

        $eventSetting = $this->getEventSetting();

        return [
            'status' => true,
            'message' => 'Event Info',
            'username' => $member->username,
            'character_name' => $member->char_name,
            'selected_char' => true,
            'characters' => [],
            'current_points' => $member->current_points,
            'hm_pass_lvl' => $member->hm_pass_lvl,
            'unlocked_hm_pass_lvl' => $member->unlocked_hm_pass_lvl,
            'is_unlocked' => $member->is_unlocked,
            'is_adv_unlocked' => $member->is_adv_unlocked,
            'can_purchase_level' => $member->hm_pass_lvl < 60 && time() < $eventSetting->attend_end,
            'can_claim_all' => $this->getCanClaimAll($member),
            'claim_special_gift_status' => $this->getClaimSpecialGiftStatus($member),
            'daily_quests' => $this->getDailyQuests($member),
            'weekly_quests' => $this->getWeeklyQuests($member),
            'rewards' => $this->getRewards($member),
            'show_compensate_history' => $this->getShowCompensateHistory($member),
        ];
    }

    private function getShowCompensateHistory($member)
    {
        return QuestLogCompensate::where('uid', $member->uid)
            ->where('status', 'success')
            ->count() > 0;
    }

    private function createDailyQuestLogs($member)
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $logDate = date('Y-m-d');

        $dt = Carbon::parse($logDate);
        $day = strtolower($dt->shortEnglishDayOfWeek); // mon, tue, wed, thu, fri, sat, sun
        $dailyQuests = Quest::where('quest_period', 'daily')
            ->where('day', $day)
            ->get();

        foreach ($dailyQuests as $quest) {
            QuestLog::firstOrCreate(
                [
                    'uid' => $member->uid,
                    'quest_id' => $quest->id,
                    'log_date' => $logDate
                ],
                [
                    'char_id' => $member->char_id,
                    'completed_count' => 0,
                    'status' => 'pending',
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]
            );
        }
    }

    private function createWeeklyQuestLogs($member)
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $now = Carbon::now()->toDateTimeString();
        $week = Week::where('week_start', '<=', $now)->where('week_end', '>=', $now)->first();

        $dailyQuests = Quest::where('quest_period', 'weekly')
            ->where('week', $week->id)
            ->get();

        foreach ($dailyQuests as $quest) {
            QuestLog::firstOrCreate(
                [
                    'uid' => $member->uid,
                    'quest_id' => $quest->id,
                ],
                [
                    'char_id' => $member->char_id,
                    'completed_count' => 0,
                    'status' => 'pending',
                    'log_date' => date('Y-m-d'),
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]
            );
        }
    }

    private function getCanClaimRewardsQuery($member)
    {
        return Reward::whereDoesntHave('itemLogs', function ($query) use ($member) {
            $query->where('uid', $member->uid);
        })
            ->whereIn('package_type', ['free', 'unlocked'])
            ->where('required_lvl', '<=', $member->hm_pass_lvl)
            ->when($member->is_unlocked == 0 && $member->is_adv_unlocked == 0, function ($query) {
                return $query->where('package_type', 'free');
            });
    }

    private function getCanClaimAll($member)
    {
        $canClaimRewardsCount = $this->getCanClaimRewardsQuery($member)->count();

        return $canClaimRewardsCount > 0;
    }

    private function getRewards($member)
    {
        $freeRewards = Reward::select(
            'id',
            'required_lvl',
            'package_name',
            $this->getRewardStatusRawQuery($member),
            'is_big_reward',
            'icon',
            'hover'
        )
            ->where('package_type', 'free')
            ->get();

        $unlockedRewards = Reward::select(
            'id',
            'required_lvl',
            'package_name',
            $this->getRewardStatusRawQuery($member),
            'is_big_reward',
            'icon',
            'hover'
        )
            ->where('package_type', 'unlocked')
            ->get();

        $rewards = [];
        foreach ($unlockedRewards as $unlockedReward) {
            $reward = $unlockedReward;
            $findReward = $freeRewards->firstWhere('required_lvl', $unlockedReward->required_lvl);
            if ($findReward) {
                $reward['free_reward_id'] = $findReward->id;
                $reward['free_reward_is_big_reward'] = $findReward->is_big_reward;
                $reward['free_reward_package_name'] = $findReward->package_name;
                $reward['free_reward_status'] = $findReward->status;
                $reward['free_reward_icon'] = $findReward->icon;
                $reward['free_reward_hover'] = $findReward->hover;
            }
            $rewards[] = $reward;
        }

        return $rewards;
    }

    // private function getRewards($member, $packageType)
    // {
    //     return Reward::select(
    //         'id',
    //         'required_lvl',
    //         'package_name',
    //         $this->getRewardStatusRawQuery($member),
    //         'is_big_reward'
    //     )
    //         ->where('package_type', $packageType)
    //         ->get();
    // }

    private function getRewardStatusRawQuery($member)
    {
        return DB::raw("
            CASE 
                WHEN (SELECT COUNT(id) FROM hm_pass_ss2_item_logs WHERE uid = '" . $member->uid . "' AND reward_id = hm_pass_ss2_rewards.id) > 0 
                    THEN 'claimed'
                WHEN " . $member->hm_pass_lvl . " >= hm_pass_ss2_rewards.required_lvl
                    THEN
                        CASE 
                            WHEN hm_pass_ss2_rewards.package_type != 'unlocked' 
                                THEN 'can_claim'
                            WHEN " . $member->is_unlocked . " = 1 OR " . $member->is_adv_unlocked . " = 1 
                                THEN 'can_claim'
                            ELSE 'not_in_condition'
                        END
                ELSE 'locked'
            END AS status");
    }

    private function getClaimSpecialGiftStatus($member)
    {
        $eventSetting = $this->getEventSetting();
        if (time() < strtotime($eventSetting->special_gift_start) || time() > strtotime($eventSetting->special_gift_end)) {
            return 'not_in_condition';
        }

        if ($member->unlocked_hm_pass_lvl < 60) {
            return 'not_in_condition';
        }

        if ($member->claimed_special_gift == 1) {
            return 'claimed';
        }

        return 'can_claim';
    }

    private function getDailyQuests($member)
    {
        $logDate =  date('Y-m-d');
        $dt = Carbon::parse($logDate);
        $day = strtolower($dt->shortEnglishDayOfWeek); // mon, tue, wed, thu, fri, sat, sun

        $dailyQuestLogs = QuestLog::select(
            'hm_pass_ss2_quests.quest_title',
            'hm_pass_ss2_quests.quest_dungeon',
            'hm_pass_ss2_quests.quest_type',
            'hm_pass_ss2_quest_logs.status',
            'hm_pass_ss2_quest_logs.completed_count',
            'hm_pass_ss2_quests.quest_limit',
            'hm_pass_ss2_quests.xp',
            'hm_pass_ss2_quests.quest_points',
            'hm_pass_ss2_quests.img'
        )
            ->join('hm_pass_ss2_quests', 'hm_pass_ss2_quest_logs.quest_id', '=', 'hm_pass_ss2_quests.id')
            ->where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->whereHas('quest', function ($query) use ($day) {
                $query->where('quest_period', 'daily')
                    ->where('day', $day);
            })
            ->where('log_date', $logDate)
            ->get();

        if (count($dailyQuestLogs) == 0) {
            $dailyQuestLogs = Quest::select(
                'hm_pass_ss2_quests.quest_title',
                'hm_pass_ss2_quests.quest_dungeon',
                'hm_pass_ss2_quests.quest_type',
                DB::raw("'pending' AS status"),
                DB::raw("0 AS completed_count"),
                'hm_pass_ss2_quests.quest_limit',
                'hm_pass_ss2_quests.xp',
                'hm_pass_ss2_quests.quest_points',
                'hm_pass_ss2_quests.img'
            )
                ->where('quest_period', 'daily')
                ->where('day', $day)
                ->get();
        }

        return $dailyQuestLogs;
    }

    private function getWeeklyQuests($member)
    {
        $now = Carbon::now()->toDateTimeString();
        $week = Week::where('week_start', '<=', $now)->where('week_end', '>=', $now)->first();
        $weeklyQuestLogs = [];

        if ($week) {
            $weeklyQuestLogs = QuestLog::select(
                'hm_pass_ss2_quests.quest_title',
                'hm_pass_ss2_quests.quest_dungeon',
                'hm_pass_ss2_quests.quest_type',
                'hm_pass_ss2_quest_logs.status',
                'hm_pass_ss2_quest_logs.completed_count',
                'hm_pass_ss2_quests.quest_limit',
                'hm_pass_ss2_quests.xp',
                'hm_pass_ss2_quests.quest_points',
                'hm_pass_ss2_quests.img'
            )
                ->join('hm_pass_ss2_quests', 'hm_pass_ss2_quest_logs.quest_id', '=', 'hm_pass_ss2_quests.id')
                ->where('uid', $member->uid)
                ->where('char_id', $member->char_id)
                ->whereHas('quest', function ($query) use ($week) {
                    $query->where('quest_period', 'weekly')
                        ->where('week', $week->id);
                })
                ->get();
        }

        if (count($weeklyQuestLogs) == 0) {
            $weeklyQuestLogs = Quest::select(
                'hm_pass_ss2_quests.quest_title',
                'hm_pass_ss2_quests.quest_dungeon',
                'hm_pass_ss2_quests.quest_type',
                DB::raw("'pending' AS status"),
                DB::raw("0 AS completed_count"),
                'hm_pass_ss2_quests.quest_limit',
                'hm_pass_ss2_quests.xp',
                'hm_pass_ss2_quests.quest_points',
                'hm_pass_ss2_quests.img'
            )
                ->where('quest_period', 'weekly')
                ->where('week', 1)
                ->get();
        }

        return $weeklyQuestLogs;
    }

    public function unlockPackage(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'unlock_package') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($member->is_unlocked == 1 || $member->is_adv_unlocked == 1) {
            return response()->json([
                'status' => false,
                'message' => 'ปลดล็อกคัมภีร์ล้ำค่าไปแล้ว'
            ]);
        }

        $eventSetting = $this->getEventSetting();

        if (time() < strtotime($eventSetting->attend_start) || time() > strtotime($eventSetting->attend_end)) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่อยู่ในช่วงที่สามารถปลดล็อกได้'
            ]);
        }

        $diamondBalance = $this->setDiamondsBalance($member->uid);
        if ($diamondBalance < $eventSetting->unlocked_diamonds) {
            return response()->json([
                'status' => false,
                'message' => 'ไดมอนด์ไม่เพียงพอ'
            ]);
        }

        $logDeduct = $this->createDeductLog([
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'purchased_at_lvl' => $member->hm_pass_lvl,
            'deduct_type' => 'unlock_package',
            'diamonds' => $eventSetting->unlocked_diamonds,
            'status' => 'pending',
            'last_ip' => $this->getIP(),
            'log_date' => (string) date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
        ]);

        if (!$logDeduct) {
            return response()->json([
                'status' => false,
                'message' => 'Error creating deduct log'
            ]);
        }

        $deductData = $this->setDiamondDeductData($eventSetting->unlocked_diamonds);

        $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
        $resp_deduct = json_decode($resp_deduct_raw);

        if (!is_object($resp_deduct) || is_null($resp_deduct)) {
            $arr_log['status'] = 'unsuccess';
            $this->updateDeductLog($arr_log, $logDeduct['id']);

            return response()->json([
                'status' => false,
                'message' => 'Error deduct diamond'
            ]);
        }

        $this->updateDeductLog([
            'before_deduct_diamond' => $diamondBalance,
            'after_deduct_diamond' => $diamondBalance - $eventSetting->unlocked_diamonds,
            'deduct_status' => $resp_deduct->status ?: 0,
            'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
            'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
            'deduct_data' => json_encode($deductData),
            'status' => 'success'
        ], $logDeduct['id']);

        $member->hm_pass_lvl = $member->hm_pass_lvl + 1;
        if ($member->hm_pass_lvl > 60) {
            $member->hm_pass_lvl = 60;
        }
        $member->unlocked_hm_pass_lvl = $member->hm_pass_lvl;
        $member->is_unlocked = 1;

        if ($member->save()) {
            return response()->json([
                'status' => true,
                'message' => "ซื้อแพ็คเกจปลดล็อกคัมภีร์ล้ำค่าสำเร็จ<br/>ได้รับเพิ่ม 1 เลเวล",
                'data' => $this->setEventInfo($uid)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => "Error unlock package",
            ]);
        }
    }

    public function unlockAdvancedPackage(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'unlock_adv_package') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($member->is_unlocked == 1 || $member->is_adv_unlocked == 1) {
            return response()->json([
                'status' => false,
                'message' => 'ปลดล็อกคัมภีร์ล้ำค่าไปแล้ว'
            ]);
        }

        $eventSetting = $this->getEventSetting();

        if (time() < strtotime($eventSetting->attend_start) || time() > strtotime($eventSetting->attend_end)) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่อยู่ในช่วงที่สามารถปลดล็อกได้'
            ]);
        }

        $diamondBalance = $this->setDiamondsBalance($member->uid);
        if ($diamondBalance < $eventSetting->unlocked_adv_diamonds) {
            return response()->json([
                'status' => false,
                'message' => 'ไดมอนด์ไม่เพียงพอ'
            ]);
        }

        $logDeduct = $this->createDeductLog([
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'purchased_at_lvl' => $member->hm_pass_lvl,
            'deduct_type' => 'unlock_adv_package',
            'diamonds' => $eventSetting->unlocked_adv_diamonds,
            'status' => 'pending',
            'last_ip' => $this->getIP(),
            'log_date' => (string) date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
        ]);

        if (!$logDeduct) {
            return response()->json([
                'status' => false,
                'message' => 'Error creating deduct log'
            ]);
        }

        $deductData = $this->setDiamondDeductData($eventSetting->unlocked_adv_diamonds);

        $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
        $resp_deduct = json_decode($resp_deduct_raw);

        if (!is_object($resp_deduct) || is_null($resp_deduct)) {
            $arr_log['status'] = 'unsuccess';
            $this->updateDeductLog($arr_log, $logDeduct['id']);

            return response()->json([
                'status' => false,
                'message' => 'Error deduct diamond'
            ]);
        }

        $this->updateDeductLog([
            'before_deduct_diamond' => $diamondBalance,
            'after_deduct_diamond' => $diamondBalance - $eventSetting->unlocked_adv_diamonds,
            'deduct_status' => $resp_deduct->status ?: 0,
            'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
            'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
            'deduct_data' => json_encode($deductData),
            'status' => 'success'
        ], $logDeduct['id']);

        $member->hm_pass_lvl = $member->hm_pass_lvl + 20;
        if ($member->hm_pass_lvl > 60) {
            $member->hm_pass_lvl = 60;
        }
        $member->unlocked_hm_pass_lvl = $member->hm_pass_lvl;
        $member->is_adv_unlocked = 1;

        if ($member->save()) {
            return response()->json([
                'status' => true,
                'message' => "ซื้อแพ็คเกจปลดล็อกคัมภีร์ล้ำค่า (ขั้นสูง) สำเร็จ<br/>ได้รับเพิ่ม 20 เลเวล",
                'data' => $this->setEventInfo($uid)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => "Error unlock adv package",
            ]);
        }
    }

    public function purchaseLevel(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'purchase_level') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $level = $request->level;
        if (!$level) {
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (2)'
            ]);
        }

        if ($level > 60) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถซื้อเกิน 60 เลเวลได้'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($member->char_id == 0) {
            return response()->json([
                'status' => false,
                'message' => 'กรุณาเลือกตัวละคร'
            ]);
        }

        if ($member->hm_pass_lvl == 60 || $member->unlocked_hm_pass_lvl == 60) {
            return response()->json([
                'status' => false,
                'message' => 'เลเวล 60 แล้ว'
            ]);
        }

        $eventSetting = $this->getEventSetting();
        if (!$eventSetting) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด.'
            ]);
        }

        if (time() < strtotime($eventSetting->attend_start) || time() > strtotime($eventSetting->attend_end)) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่อยู่ในช่วงที่สามารซ้ือเลเวลได้'
            ]);
        }

        $requiredDiamonds = $eventSetting->purchased_lvl_diamonds * $level;

        $diamondBalance = $this->setDiamondsBalance($member->uid);
        if ($diamondBalance < $requiredDiamonds) {
            return response()->json([
                'status' => false,
                'message' => 'ไดมอนด์ไม่เพียงพอ'
            ]);
        }

        $logDeduct = $this->createDeductLog([
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'deduct_type' => 'purchase_level',
            'diamonds' => $requiredDiamonds,
            'status' => 'pending',
            'last_ip' => $this->getIP(),
            'log_date' => (string) date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
            'purchased_lvl' => $level,
        ]);

        if (!$logDeduct) {
            return response()->json([
                'status' => false,
                'message' => 'Error creating deduct log'
            ]);
        }

        $deductData = $this->setDiamondDeductData($requiredDiamonds);

        $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
        $resp_deduct = json_decode($resp_deduct_raw);

        if (!is_object($resp_deduct) || is_null($resp_deduct)) {
            $arr_log['status'] = 'unsuccess';
            $this->updateDeductLog($arr_log, $logDeduct['id']);

            return response()->json([
                'status' => false,
                'message' => 'Error deduct diamond'
            ]);
        }

        $this->updateDeductLog([
            'before_deduct_diamond' => $diamondBalance,
            'after_deduct_diamond' => $diamondBalance - $requiredDiamonds,
            'deduct_status' => $resp_deduct->status ?: 0,
            'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
            'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
            'deduct_data' => json_encode($deductData),
            'status' => 'success'
        ], $logDeduct['id']);

        $member->hm_pass_lvl = $member->hm_pass_lvl + $level;
        if ($member->hm_pass_lvl > 60) {
            $member->hm_pass_lvl = 60;
        }

        if ($member->is_unlocked == 1 || $member->is_adv_unlocked == 1) {
            $member->unlocked_hm_pass_lvl = $member->hm_pass_lvl;
        }

        $member->purchased_lvl = $member->purchased_lvl + $level;

        if ($member->save()) {
            return response()->json([
                'status' => true,
                'message' => "ปลดล็อก " . $level . " เลเวลสำเร็จ",
                'data' => $this->setEventInfo($uid)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => "Error purchase level",
            ]);
        }
    }

    public function claimSpecialGift(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'claim_special_gift') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $eventSetting = $this->getEventSetting();

        if ($member->claimed_special_gift == 1) {
            return response()->json([
                'status' => false,
                'message' => 'รับของขวัญพิเศษ สำหรับผู้มั่งคั่งไปแล้ว'
            ]);
        }

        if (time() < strtotime($eventSetting->special_gift_start) || time() > strtotime($eventSetting->special_gift_end)) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่อยู่ในช่วงที่สามารถรับของขวัญพิเศษ สำหรับผู้มั่งคั่งได้'
            ]);
        }

        if ($member->unlocked_hm_pass_lvl < 60) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่ตรงเงื่อนไข'
            ]);
        }

        $reward = Reward::where('package_type', 'special_gift')->first();

        if (!$reward) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด'
            ]);
        }

        $goods_data = [];

        $goods_data[] = [
            'goods_id' => $reward->package_id,
            'purchase_quantity' => $reward->package_quantity,
            'purchase_amount' => 0,
            'category_id' => 40,
        ];

        $sendItemLog = $this->addItemHistoryLog([
            'uid'                           => $member->uid,
            'username'                      => $member->username,
            'ncid'                          => $member->ncid,
            'reward_id'                     => $reward->id,
            'package_name'                  => $reward->package_name,
            'package_quantity'              => $reward->package_quantity,
            'package_amount'                => $reward->package_amount,
            'package_type'                  => $reward->package_type,
            'send_item_status'              => false,
            'send_item_purchase_id'         => 0,
            'send_item_purchase_status'     => 0,
            'goods_data'                    => json_encode($goods_data),
            'status'                        => 'pending',
            'log_date'                      => (string) date('Y-m-d'), // set to string
            'log_date_timestamp'            => time(),
            'last_ip'                       => $this->getIP(),
        ]);

        $ncid = $this->getNcidByUid($member->uid);
        $send_result_raw = $this->apiSendItem($ncid, $goods_data);
        $send_result = json_decode($send_result_raw);

        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            $this->updateItemHistoryLog([
                'send_item_status' => $send_result->status ?: false,
                'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                'status' => 'success'
            ], $sendItemLog['id'], $member->uid);

            $member->claimed_special_gift = 1;
            $member->save();

            return response()->json([
                'status' => true,
                'message' => "ได้รับ <span class='green'>" . $reward->package_name . " x" . $reward->package_amount . "</span> <br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                'data' => [
                    'claim_special_gift_status' => $this->getClaimSpecialGiftStatus($member),
                ],
            ]);
        } else {
            $this->updateItemHistoryLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
            ], $sendItemLog['id'], $member->uid);

            return response()->json([
                'status' => false,
                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า"
            ]);
        }
    }

    public function claimReward(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'claim_reward') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $id = $request->id;
        if (!$id) {
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (2)'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $reward = Reward::select(
            'id',
            'package_id',
            'package_name',
            'package_quantity',
            'package_amount',
            'package_type',
            $this->getRewardStatusRawQuery($member)
        )
            ->where('id', $id)
            ->first();

        if (!$reward) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด'
            ]);
        }

        if ($reward->status != 'can_claim') {
            return response()->json([
                'status' => false,
                'message' => 'ไม่ตรงเงื่อนไข ไม่สามารถรับไอเทมได้'
            ]);
        }

        $goods_data = [];

        $goods_data[] = [
            'goods_id' => $reward->package_id,
            'purchase_quantity' => $reward->package_quantity,
            'purchase_amount' => 0,
            'category_id' => 40,
        ];

        $sendItemLog = $this->addItemHistoryLog([
            'uid'                           => $member->uid,
            'username'                      => $member->username,
            'ncid'                          => $member->ncid,
            'reward_id'                     => $reward->id,
            'package_name'                  => $reward->package_name,
            'package_quantity'              => $reward->package_quantity,
            'package_amount'                => $reward->package_amount,
            'package_type'                  => $reward->package_type,
            'send_item_status'              => false,
            'send_item_purchase_id'         => 0,
            'send_item_purchase_status'     => 0,
            'goods_data'                    => json_encode($goods_data),
            'status'                        => 'pending',
            'log_date'                      => (string) date('Y-m-d'), // set to string
            'log_date_timestamp'            => time(),
            'last_ip'                       => $this->getIP(),
        ]);

        $ncid = $this->getNcidByUid($member->uid);
        $send_result_raw = $this->apiSendItem($ncid, $goods_data);
        $send_result = json_decode($send_result_raw);

        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            $this->updateItemHistoryLog([
                'send_item_status' => $send_result->status ?: false,
                'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                'status' => 'success'
            ], $sendItemLog['id'], $member->uid);

            return response()->json([
                'status' => true,
                'message' => 'ได้รับ <span class="green">' . $reward->package_name . " x" . $reward->package_amount . "</span> <br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                'data' => [
                    'can_claim_all' => $this->getCanClaimAll($member),
                    'rewards' => $this->getRewards($member),
                ]
            ]);
        } else {
            $this->updateItemHistoryLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
            ], $sendItemLog['id'], $member->uid);

            return response()->json([
                'status' => false,
                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า"
            ]);
        }
    }

    public function claimAll(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'claim_all') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($member->processing_claim_all == 1) {
            return response()->json([
                'status' => false,
                'message' => 'กำลังดำเนินการ'
            ]);
        }

        $member->update(['processing_claim_all' => 1]);

        $canClaimRewards = $this->getCanClaimRewardsQuery($member)->get();

        if (count($canClaimRewards) == 0) {

            $member->update(['processing_claim_all' => 0]);

            return response()->json([
                'status' => false,
                'message' => 'ไม่มีไอเทมที่สามารถรับได้'
            ]);
        }

        $goods_data = [];
        $sendItemLogIds = [];

        foreach ($canClaimRewards as $reward) {
            $goods_data[] = [
                'goods_id' => $reward->package_id,
                'purchase_quantity' => $reward->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40,
            ];

            $itemLogGoodsData = [];
            $itemLogGoodsData[] = [
                'goods_id' => $reward->package_id,
                'purchase_quantity' => $reward->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40,
            ];

            $sendItemLog = $this->addItemHistoryLog([
                'uid'                           => $member->uid,
                'username'                      => $member->username,
                'ncid'                          => $member->ncid,
                'reward_id'                     => $reward->id,
                'package_name'                  => $reward->package_name,
                'package_quantity'              => $reward->package_quantity,
                'package_amount'                => $reward->package_amount,
                'package_type'                  => $reward->package_type,
                'send_item_status'              => false,
                'send_item_purchase_id'         => 0,
                'send_item_purchase_status'     => 0,
                'goods_data'                    => json_encode($itemLogGoodsData),
                'status'                        => 'pending',
                'log_date'                      => (string) date('Y-m-d'), // set to string
                'log_date_timestamp'            => time(),
                'last_ip'                       => $this->getIP(),
            ]);

            $sendItemLogIds[] = $sendItemLog['id'];
        }

        SendItems::dispatch($member, $goods_data, $sendItemLogIds)->onQueue('bns_hm_pass_ss2_send_items');

        $member->update(['processing_claim_all' => 0]);

        return response()->json([
            'status' => true,
            'message' => "รับไอเทมทั้งหมดแล้ว",
            'data' => [
                'can_claim_all' => $this->getCanClaimAll($member),
                'rewards' => $this->getRewards($member),
            ],
        ]);
    }

    public function getHistory(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'history') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $pointsHistories = $this->getPointsHistories($member);
        $diamondHistories = $this->getDiamondHistories($member);
        $itemHistories = $this->getItemHistories($member);

        return response()->json([
            'status' => true,
            'data' => [
                'points_histories' => $pointsHistories,
                'diamond_histories' => $diamondHistories,
                'item_histories' => $itemHistories,
            ]
        ]);
    }

    private function getPointsHistories($member)
    {
        return QuestLog::with('quest')
            ->where('uid', $member->uid)
            ->where('status', 'success')
            ->orderBy('quest_complete_datetime', 'desc')
            ->get()
            ->map(function ($questLog) {
                $title = $questLog->quest->quest_title . ' ได้รับ ' . $questLog->quest->quest_points . ' ถ้วยกาแฟ';

                if ($questLog->quest->quest_type == 'xp_gain') {
                    $title = $questLog->quest->quest_title . ' ' . number_format($questLog->quest->xp) . ' XP ได้รับ ' . $questLog->quest->quest_points . ' ถ้วยกาแฟ';
                }

                return [
                    'title' => $title,
                    'date' => Carbon::parse($questLog->quest_complete_datetime)->format('d/m/Y'),
                    'time' => Carbon::parse($questLog->quest_complete_datetime)->format('H:i:s'),
                ];
            });
    }

    private function getDiamondHistories($member)
    {
        return DeductLog::where('uid', $member->uid)
            ->where('status', 'success')
            ->latest()
            ->get()
            ->map(function ($deductLog) {
                switch ($deductLog->deduct_type) {
                    case 'unlock_package':
                        $title = 'ซื้อแพ็คเกจปลดล็อกคัมภีร์ล้ำค่า ' . number_format($deductLog->diamonds) . ' ไดมอนด์';
                        break;

                    case 'unlock_adv_package':
                        $title = 'ซื้อแพ็คเกจปลดล็อกคัมภีร์ล้ำค่า (ขั้นสูง) ' . number_format($deductLog->diamonds) . ' ไดมอนด์';
                        break;

                    case 'purchase_level':
                        $title = 'ปลดล็อก ' . $deductLog->purchased_lvl . ' เลเวล ' . number_format($deductLog->diamonds) . ' ไดมอนด์';
                        break;
                }

                return [
                    'title' => $title,
                    'date' => Carbon::parse($deductLog->created_at)->format('d/m/Y'),
                    'time' => Carbon::parse($deductLog->created_at)->format('H:i:s'),
                ];
            });
    }

    private function getItemHistories($member)
    {
        return ItemLog::where('uid', $member->uid)
            ->latest()
            ->get()
            ->map(function ($itemLog) {
                return [
                    'title' => 'ได้รับ ' . $itemLog->package_name . ' x' . $itemLog->package_amount,
                    'date' => Carbon::parse($itemLog->created_at)->format('d/m/Y'),
                    'time' => Carbon::parse($itemLog->created_at)->format('H:i:s'),
                ];
            });
    }

    public function getCompensateHistory(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'compensate_history') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $pointsHistories = QuestLogCompensate::with('quest')
            ->where('uid', $member->uid)
            ->where('status', 'success')
            ->orderBy('quest_complete_datetime', 'desc')
            ->get()
            ->map(function ($questLog) {
                $title = 'ส่งคะแนนย้อนหลัง ' . $questLog->quest->quest_title . ' ได้รับ ' . $questLog->quest->quest_points . ' ถ้วยกาแฟ';

                if ($questLog->quest->quest_type == 'xp_gain') {
                    $title = $questLog->quest->quest_title . ' ' . number_format($questLog->quest->xp) . ' XP ได้รับ ' . $questLog->quest->quest_points . ' ถ้วยกาแฟ';
                }

                return [
                    'title' => $title,
                    'date' => Carbon::parse($questLog->quest_complete_datetime)->format('d/m/Y'),
                    'time' => Carbon::parse($questLog->quest_complete_datetime)->format('H:i:s'),
                ];
            });

        return response()->json([
            'status' => true,
            'data' => [
                'points_histories' => $pointsHistories,
            ]
        ]);
    }
}
