<?php

    namespace App\Http\Controllers\Api\archer_preorder;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\archer_preregister\Member as PreRegisMember;  // pre-register for check discount rights.

    use App\Models\archer_preorder\Member;
    use App\Models\archer_preorder\DeductLog;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2019-09-17 12:00:00';
        private $endTime = '2019-09-24 23:59:59';

        private $package_1_diamonds = 250000; // 250000
        private $package_1_diamonds_discount = 125000; // 125000

        private $package_2_diamonds = 60000; // 60000
        private $package_2_diamonds_discount = 30000; // 30000

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            /* if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            } */

            // lock ip
            /* if($this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            } */

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย<br />ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){

                // check member has game info
                $uid = $this->userData['uid'];
                $username = $this->userData['username'];
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    die(json_encode([
                        'status' => false,
                        'type' => 'no_game_info',
                        'message' => 'ขออภัย<br />กรุณาล็อคอินเข้าเกมเพื่อสร้างตัวละคร<br />ก่อนร่วมกิจกรรม'
                    ]));
                }

                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาด<br />ไม่สามารถบักทึกข้อมูลของคุณได้<br />กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }/* else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            } */
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            // $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        private function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:ARCHER_PREORDER:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function diamondBalance(int $uid) {
            return $this->post_api($this->baseApi, [
                        'key_name' => 'bns',
                        'service' => 'balance',
                        'uid' => $uid,
            ]);
        }

        private function setRemainDiamonds($uid){
            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return 0;
            }else{
                return intval($diamondsBalance->balance);
            }
        }

        private function setDiamondDeductData($deduct_diamond_amount=0)
        {
            if($deduct_diamond_amount==0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamomds for garena BNS Archer Pre-Order Package.',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addDeductLog(array $arr) {
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductLog(array $arr, $logId) {
            if ($arr) {
                return DeductLog::where('id', $logId)->update($arr);
            }
            return null;
        }

        public function getEventInfo(Request $request)
        {

            if ($request->has('type') == false && $request->type != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
            ]);

        }

        private function setThaiMonth($m){
            switch($m){
                case "9":
                    return 'กันยายน';
                    break;
            }

            return '';
        }

        private function setPackageName($packageId=''){
            switch($packageId){
                case 1:
                    return 'แพ็คเกจสนับสนุนน้องใหม่ ขั้นเทพ';
                    break;
                case 2:
                    return 'แพ็คเกจสนับสนุนน้องใหม่';
                    break;
            }

            return '';
        }

        private function setWeaponName($weaponId=''){
            switch($weaponId){
                case 1:
                    return 'เบลดมาสเตอร์';
                    break;
                case 2:
                    return 'กังฟูมาสเตอร์';
                    break;
                case 3:
                    return 'ฟอร์ซมาสเตอร์';
                    break;
                case 4:
                    return 'โซลกันเนอร์';
                    break;
                case 5:
                    return 'เดสทรอยเยอร์';
                    break;
                case 6:
                    return 'ซัมมอนเนอร์';
                    break;
                case 7:
                    return 'แอสแซสซิน';
                    break;
                case 8:
                    return 'เบลดแดนเซอร์';
                    break;
                case 9:
                    return 'วอร์ลอค';
                    break;
                case 10:
                    return 'โซลไฟเตอร์';
                    break;
                case 11:
                    return 'วอร์เดน';
                    break;
                case 12:
                    return 'อาร์เชอร์';
                    break;
            }

            return '';
        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            // check member can get discount
            $hasDiscount = $this->checkHasDiscount($uid);

            // check purchase history
            $purchaseHistory = $this->checkPurchaseHistory($uid);
            $purchasedPackageId = -1;
            $purchasedPackage = '';
            $purchasedWeapon = '';
            $purchasedDate = '';
            if($purchaseHistory > 0){
                $purchaseHistoryQuery = DeductLog::where('uid', $uid)->where('status', 'success')->first();

                $purchasedPackageId = $purchaseHistoryQuery->package_id;
                $purchasedPackage = $this->setPackageName($purchaseHistoryQuery->package_id);
                $purchasedWeapon = $this->setWeaponName($purchaseHistoryQuery->weapon_id);
                $purchasedDateTimeStamp = strtotime($purchaseHistoryQuery->created_at);

                $purchasedDate = date('j', $purchasedDateTimeStamp).' '.$this->setThaiMonth(date('n', $purchasedDateTimeStamp)).' '.(date('Y', $purchasedDateTimeStamp)+543).' เวลา '.date('H:i:s', $purchasedDateTimeStamp);
            }

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            return [
                'username' => $member->username,
                'diamonds' => number_format($diamonds),
                'has_discount' => $hasDiscount,
                'purchased_package_id'  => $purchasedPackageId,
                'purchased_package' => $purchasedPackage,
                'purchased_weapon' => $purchasedWeapon,
                'purchased_date' => $purchasedDate,
                'can_purchase' => [
                    'package_1' => $purchaseHistory == 0 && $diamonds >= $this->package_1_diamonds,
                    'package_1_discount' => $purchaseHistory == 0 && $diamonds >= $this->package_1_diamonds_discount,
                    'package_2' => $purchaseHistory == 0 && $diamonds >= $this->package_2_diamonds,
                    'package_2_discount' => $purchaseHistory == 0 && $diamonds >= $this->package_2_diamonds_discount
                ],
                'already_purchase' => $purchaseHistory > 0
            ];

        }

        private function checkHasDiscount($uid){
            $discount = false;
            $checkPreRegis = PreRegisMember::where('uid', $uid)->where('is_registered', 1)->count();
            if($checkPreRegis > 0){
                $discount = true;
            }

            return $discount;
        }

        private function checkPurchaseHistory($uid){
            // check purchase history
            $purchaseHistory = DeductLog::where('uid', $uid)->count();

            return $purchaseHistory;
        }

        public function purchasePackage(Request $request) {

            if ($request->has('type') == false && $request->type != 'purchase_package') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }

            if ($request->has('package_id') == false || !in_array($request->package_id, [1,2])) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (2)!'
                ]);
            }

            if ($request->has('package_type') == false || !in_array($request->package_type, [1,2])) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (3)!'
                ]);
            }

            if ($request->has('weapon_id') == false || !in_array($request->weapon_id, [1,2,3,4,5,6,7,8,9,10,11,12])) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (4)!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);
            $package_id = $request->package_id;
            $package_type = $request->package_type;
            $weapon_id = $request->weapon_id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check member can get discount
            $hasDiscount = $this->checkHasDiscount($uid);

            // check purchase history
            $purchaseHistory = $this->checkPurchaseHistory($uid);

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            if($purchaseHistory > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />สามารถซื้อแพ็คเกจ 1 ครั้งต่อ 1 ไอดี'
                ]);
            }

            $can_purchase = false;
            $package_price = 0;
            if($package_id == 1){

                $package_title = 'แพ็คเกจสนับสนุนน้องใหม่ ขั้นเทพ';

                if($purchaseHistory == 0 && $diamonds >= $this->package_1_diamonds && $hasDiscount == false && $package_type == 1){
                    $can_purchase = true;
                    $package_price = $this->package_1_diamonds;
                }elseif($purchaseHistory == 0 && $diamonds >= $this->package_1_diamonds_discount && $hasDiscount == true && $package_type == 2){
                    $can_purchase = true;
                    $package_price = $this->package_1_diamonds_discount;
                }

            }elseif($package_id == 2){

                $package_title = 'แพ็คเกจสนับสนุนน้องใหม่';

                if($purchaseHistory == 0 && $diamonds >= $this->package_2_diamonds && $hasDiscount == false && $package_type == 1){
                    $can_purchase = true;
                    $package_price = $this->package_2_diamonds;
                }elseif($purchaseHistory == 0 && $diamonds >= $this->package_2_diamonds_discount && $hasDiscount == true && $package_type == 2){
                    $can_purchase = true;
                    $package_price = $this->package_2_diamonds_discount;
                }

            }

            if($can_purchase == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถสั่งซื้อแพ็คเกจได้<br />เนื่องจากไดมอนด์ไม่พอ'
                ]);
            }

            if($package_price <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่พบแพ็คเกจที่สั่งซื้อ'
                ]);
            }

            // set deduct diamonds
            $logDeduct = $this->addDeductLog([
                'uid'                       => $member->uid,
                'username'                  => $member->username,
                'ncid'                      => $member->ncid,
                'discount'                  => $hasDiscount == true ? 1 : 0,
                'diamonds'                  => $package_price,
                'weapon_id'                 => $weapon_id,
                'package_id'                => $package_id,
                'package_title'             => $package_title,
                'status'                    => 'pending',
                'log_date'                  => date('Y-m-d H:i:s'), // set to string
                'log_date_timestamp'        => time(),
                'last_ip'                   => $this->getIP(),
            ]);

            // set deduct diamonds
            $deductData = $this->setDiamondDeductData($package_price);
            $respDeductRaw = $this->deductDiamond($ncid, $deductData);
            $respDeduct = json_decode($respDeductRaw);
            if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){

                // update deduct log
                $arrDeductDiamond = [
                    'before_deduct_diamond' => $diamonds,
                    'after_deduct_diamond' => $diamonds - $package_price,
                    'deduct_status' => $respDeduct->status ?: 0,
                    'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
                    'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
                    'deduct_data' => json_encode($deductData),
                    'status' => 'success'
                ];

                $respDeduct = $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);

                $eventInfo = $this->setEventInfo($uid);
                
                if($respDeduct){
                    return response()->json([
                        'status' => true,
                        'message' => 'สั่งซื้อแพ็คเกจสำเร็จแล้ว<br />รอรับของวันที่ 25 นี้ได้เลย',
                        'content' => $eventInfo
                    ]);
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถสั่งซื้อแพ็คเกจได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                        'content' => $eventInfo
                    ]);
                }


            }else{
                $arrDeductDiamond['status'] = 'unsuccess';
                $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);
            }

            return response()->json([
                        'status' => false,
                        'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
            ]);


        }

    }