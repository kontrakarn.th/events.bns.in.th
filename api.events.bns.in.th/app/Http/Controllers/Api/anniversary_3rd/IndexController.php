<?php

    namespace App\Http\Controllers\Api\anniversary_3rd;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\anniversary_3rd\Setting;
    use App\Models\anniversary_3rd\Member;
    use App\Models\anniversary_3rd\Milestone;
    use App\Models\anniversary_3rd\Reward;
    use App\Models\anniversary_3rd\ItemLog;

    class IndexController extends BnsEventController{

        private $baseApi = 'http://api.apps.garena.in.th';

        private $userEvent = null;

        public function __construct(Request $request)
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();

        }

        private function checkEventStatus()
        {
             if($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip  = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData) {
                return true;
            } else {
                return false;
            }
        }

        private function is_accepted_ip()
        {
            $allow_ips   = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid      = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $arr = [
                    'uid'              => $uid,
                    'username'         => $username,
                    'ncid'             => $ncid,
                    'char_id'          => 0,
                    'char_name'        => '',
                    'world_id'         => 0,
                    'last_ip'          => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }

                $this->userEvent = $resp;

            } else {
                $member = Member::where('uid', $uid)->first();
                if (empty($member->ncid)) {
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if (empty($ncid)) {
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
                $this->userEvent = $member;
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if (empty($_id)) {
                return false;
            }

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:ANNIVERSARY3RD:LIVE:ASSOCIATE_UID_WITH_NCID_'.$uid, 10, function () use ($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'user_info',
                    'uid'      => $uid,
                ]);
                $userNcInfo       = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }

            });
        }

        private function doGetCharWithLevelAndHmCondition($uid, $ncid, $require_level=1, $require_hm=1){//new fixed function
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i = 1;
                    foreach ($resp->response as $key => $value) {
                        if((int)$value->level >= $require_level && (int)$value->mastery_level>=$require_hm){
                            $content[] = [
                                'id' => $i,
                                'char_id'   => (int)$value->id,
                                'char_name' => $value->name,
                                'world_id' => (int)$value->world_id,
                                'job' => (int)$value->job,
                                'level' => (int)$value->level,
                                'creation_time' => $value->creation_time,
                                'last_play_start' => $value->last_play_start,
                                'last_play_end' => $value->last_play_end,
                                'last_ip' => $value->last_ip,
                                'exp' => (int)$value->exp,
                                'mastery_level' => (int)$value->mastery_level,
                            ];
                        }

                        $i++;
                    }
                    
                    return $content;
                }
            }
            return null;
        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:ANNIVERSARY3RD:LIVE:CHARACTERS_'.$uid, 10, function () use ($ncid) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'characters',
                    'user_id'  => $ncid,
                ]);
            });
        }

        private function dosendItem(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name'             => 'bns',
                'service'              => 'send_item',
                'user_id'              => $ncid,
                'purchase_description' => 'Sending item from garena events BNS 3rd Year Anniversary.',
                'goods'                => json_encode($goodsData),
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
        }

        private function getEventSetting(){
            // return Cache::remember('BNS:LEAGUE:LIVE:EVENT_SETTING', 10, function() {
                        return Setting::where('active', 1)->first();
                    // });
        }

        public function getEventInfo(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            

            $eventInfo = $this->setEventInfo($uid);
            if($eventInfo['status']==false){
                return response()->json([
                        'status' => false,
                        'message'=>$eventInfo['message'],
                    ]);
            }

            return response()->json([
                'status' => true,
                'data' => $eventInfo
            ]);

        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return [
                    'status' => false,
                    'message' => 'ไม่พอข้อมูล'
                ];
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            $member = Member::where('uid', $uid)->first();

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // get max points and percentage
            $maxPointsQuery = Milestone::where('unlock_datetime', '<=', $logDateTime)->orderBy('unlock_datetime', 'DESC')->first();

            $currentPoints = 0;
            $currentPercentage = 0.0;

            if(isset($maxPointsQuery) && !empty($maxPointsQuery->points) && !empty($maxPointsQuery->percentage)){
                $currentPoints = $maxPointsQuery->points;
                $currentPercentage = $maxPointsQuery->percentage;
            }

            // get rewards
            $rewards = Reward::orderBy('id','ASC')->get();

            $rewardList = [];

            if(count($rewards) > 0){
                foreach($rewards as $reward){

                    $rewardList[] = [
                        'id' => $reward->package_key,
                        'title' => $reward->package_name,
                        'points' => $reward->points,
                        'points_text' => number_format($reward->points),
                        'can_claim' => $this->checkCanClaimReward($member->uid,$member->ncid,$reward->package_key,$reward->points,$currentPoints,$eventSetting->require_level,$eventSetting->require_hm),
                        'claimed' => $this->checkIsClaimed($member->uid,$reward->package_key),
                    ];

                }
            }

            return [
                'status' => true,
                "message" => 'Event Info',
                'username'=>$member->username,
                'current_points' => $currentPoints,
                'current_percentage' => $currentPercentage,
                'rewards' => $rewardList,
            ];

        }

        private function checkCanClaimReward($uid,$ncid,$package_key,$requirePoint=0,$currentPoints=0,$requireLevel=0,$requireHm=0){

            $itemLog = ItemLog::where('uid', $uid)->where('package_key', $package_key)->count();

            $charListWithCondition = $this->doGetCharWithLevelAndHmCondition($uid, $ncid, $requireLevel, $requireHm);

            if($itemLog == 0 && $currentPoints >= $requirePoint && count($charListWithCondition) > 0){
                return true;
            }

            return false;
        }

        private function checkIsClaimed($uid,$package_key){
            $itemLog = ItemLog::where('uid', $uid)->where('package_key', $package_key)->count();

            if($itemLog > 0){
                return true;
            }

            return false;
        }

        public function claimReward(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'claim_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $package_key = (int)$decoded['id'];

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // get reward
            // get reward from packge_key
            $reward = Reward::where('package_key', $package_key)->first();
            if(!isset($reward)){
                return response()->json([
                    'status' => false,
                    'message' => 'no reward data.'
                ]);
            }

            // get max points and percentage
            $maxPointsQuery = Milestone::where('unlock_datetime', '<=', $logDateTime)->orderBy('unlock_datetime', 'DESC')->first();

            $currentPoints = 0;
            $currentPercentage = 0.0;

            if(isset($maxPointsQuery) && !empty($maxPointsQuery->points) && !empty($maxPointsQuery->percentage)){
                $currentPoints = $maxPointsQuery->points;
                $currentPercentage = $maxPointsQuery->percentage;
            }


            // check condition
            if($this->checkCanClaimReward($member->uid,$member->ncid,$package_key,$reward->points,$currentPoints,$eventSetting->require_level,$eventSetting->require_hm) == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย ไม่ตรงเงื่อนไขรับของรางวัล'
                ]);
            }

            if($this->checkIsClaimed($member->uid,$package_key) == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณได้รับของรางวัล '.$reward->package_name.' ไปก่อนหน้านี้แล้ว'
                ]);
            }

            // set reward info for check condition
            $packageId = $reward->package_id;
            $packageName = $reward->package_name;
            $packageQuantity = $reward->package_quantity;
            $packageAmount = $reward->package_amount;
            $packageKey = $reward->package_key;
            $packagePoints = $reward->points;

            $goods_data = []; //good data for send item group
            $goods_data[] = [
                'goods_id' => $reward->package_id,
                'purchase_quantity' => $reward->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40,
            ];

            $sendItemLog = $this->addItemHistoryLog([
                'uid'                           => $member->uid,
                'username'                      => $member->username,
                'ncid'                          => $member->ncid,
                'points'                        => $packagePoints,
                'package_key'                   => $packageKey,
                'package_id'                    => $packageId,
                'package_name'                  => $packageName,
                'package_quantity'              => $packageQuantity,
                'package_amount'                => $packageAmount,
                'send_item_status'              => false,
                'send_item_purchase_id'         => 0,
                'send_item_purchase_status'     => 0,
                'goods_data'                    => json_encode($goods_data),
                'status'                        => 'pending',
                'log_date'                      => (string)date('Y-m-d'), // set to string
                'log_date_timestamp'            => time(),
                'last_ip'                       => $this->getIP(),
            ]);

            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);

            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                // update send item log
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ?: false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                    'status' => 'success'
                ], $sendItemLog['id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                    'status' => true,
                    'data' => $eventInfo
                ]);

            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                        ], $sendItemLog['id'], $uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                        ]);
            }
        }

    }