<?php

    namespace App\Http\Controllers\Api\wardrobe_package_2019;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\wardrobe_package_2019\Member;
    use App\Models\wardrobe_package_2019\Reward;
    use App\Models\wardrobe_package_2019\ItemLog;
    use App\Models\wardrobe_package_2019\DeductLog;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2018-02-20 00:00:00';
        private $endTime = '2019-03-06 23:59:59';

        private $requireDiamonds = [90000,40000];

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:WARDROBE_PACKAGE_2019:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData) {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Sending item from garena events BNS Wardrobe Package 2019.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addSendItemLog(array $arr) {
            if ($arr) {
                $resp = ItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateSendItemLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function addDeductLog(array $arr) {
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductLog(array $arr, $logId) {
            if ($arr) {
                return DeductLog::where('id', $logId)->update($arr);
            }
            return null;
        }

        private function apiDiamondBalance(int $uid) {
            return $this->post_api($this->baseApi, [
                        'key_name' => 'bns',
                        'service' => 'balance',
                        'uid' => $uid,
            ]);
        }

        private function setRemainDiamonds($uid){
            $diamondsBalanceRaw = $this->apiDiamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return 0;
            }else{
                return intval($diamondsBalance->balance);
            }
        }

        private function setDiamondDeductData($deduct_diamond_amount=0)
        {
            if($deduct_diamond_amount==0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamomds for garena events BNS Wardrobe Package 2019.',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function apiWardrobe($uid = null) {
            if ($uid) {
                $data = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'vip',
                    'grade' => '100001',
                    'uid' => $uid
                ]);
                return json_decode($data);
            }

            return null;
        }

        private function wardrobeStatus($uid=null){
            if(empty($uid))
                return false;

            $wardrobe = $this->apiWardrobe($uid);

            if($wardrobe->status == true){

                if($wardrobe->response->activation && $wardrobe->response->activation == 'true'){
                    return true;
                }else{
                    return false;
                }

            }
            return false;
        }

        private function setRequiredDiamonds($package_id=null)
        {
            if(empty($package_id))
                return false;

            return isset($this->requireDiamonds[$package_id - 1]) ? $this->requireDiamonds[$package_id - 1] : false;
        }

        private function checkPackageHistoryCount($uid=null,$package_id=null){
            if(empty($uid) || empty($package_id)){
                return false;
            }

            return ItemLog::where('uid', $uid)
                            ->where('package_id', $package_id)
                            ->count();
        }

        public function getEventInfo(Request $request)
        {

            if ($request->has('type') == false && $request->type != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            if($eventInfo == false){
                $eventInfo = [];
            }

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
            ]);

        }

        private function setEventInfo($uid=null){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            // check diamonds balance
            $diamondsBalance = $this->setRemainDiamonds($member->uid);

            // check member wardrobe status
            $wardrobeStatus = $this->wardrobeStatus($member->uid);

            // check package require diamonds
            $package_1_required_diamonds = $this->setRequiredDiamonds(1);
            $package_2_required_diamonds = $this->setRequiredDiamonds(2);

            // check package history
            $p1History = $this->checkPackageHistoryCount($member->uid,1);
            $p2History = $this->checkPackageHistoryCount($member->uid,2);

            // check player buy package 1 can buy package 2 one pacakge
            $package2Limit = 2;
            if($p1History > 0){
                $package2Limit = 1;
            }

            return [
                'username' => $member->username,
                'has_wardrobe' => $wardrobeStatus,
                'package_1' => [
                    'can_purchase' => $wardrobeStatus == false && $p1History == 0 && $diamondsBalance >= $package_1_required_diamonds,
                    'already_purchase' => $p1History > 0,
                ],
                'package_2' => [
                    'purchase_count' => $p2History,
                    'purchase_limit' => $package2Limit,
                    'can_purchase' => $wardrobeStatus == true && $p2History < $package2Limit && $diamondsBalance >= $package_2_required_diamonds,
                    'already_purchase' => $p2History >= $package2Limit,
                ]
            ];

        }

        public function purchasePackage(Request $request)
        {
            if ($request->has('type') == false && $request->type != 'purchase_package') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($request->has('package_id') == false || !in_array($request->package_id, [1,2])) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $package_id = $request->package_id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $package_required_diamonds = $this->setRequiredDiamonds($package_id);

            // check diamonds balance
            $diamondsBalance = $this->setRemainDiamonds($member->uid);
            if($diamondsBalance < $package_required_diamonds){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไดมอนด์ไม่พอซื้อแพ็คเกจ'
                ]);
            }

            $eventInfo = $this->setEventInfo($member->uid);

            $has_wardrobe = $eventInfo['has_wardrobe'];

            $packageInfo = [];
            switch($package_id){
                case 1:
                    $packageInfo = $eventInfo['package_1'];
                    break;

                case 2:
                    $packageInfo = $eventInfo['package_2'];
                    break;

                default:
                    break;
            }
            
            if(empty($packageInfo)){
                return response()->json([
                    'status' => false,
                    'message' => 'No data.'
                ]);
            }

            if($packageInfo['can_purchase'] == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถซื้อแพ็คเกจได้'
                ]);
            }

            if($packageInfo['already_purchase'] == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้ซื้อแพ็คเกจครบจำนวนแล้ว'
                ]);
            }

            // add deduct diamonds log
            $logDeduct = $this->addDeductLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'diamonds' => $package_required_diamonds,
                'status' => 'pending',
                'last_ip' => $this->getIP(),
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
            ]);

            if($logDeduct){
                // set desuct diamonds
                $deductData = $this->setDiamondDeductData($package_required_diamonds);

                $resp_deduct_raw = $this->deductDiamond($ncid, $deductData);
                $resp_deduct = json_decode($resp_deduct_raw);
                if(is_object($resp_deduct) && is_null($resp_deduct) == false){

                    $arrDeductLog = [
                        'before_deduct_diamond' => $diamondsBalance,
                        'after_deduct_diamond' => $diamondsBalance - $package_required_diamonds,
                        'deduct_status' => $resp_deduct->status ?: 0,
                        'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
                        'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'success'
                    ];
                    $this->updateDeductLog($arrDeductLog, $logDeduct['id']);

                    $goods_data = []; //good data for send item group

                    $rewards = new Reward;
                    $packageData = $rewards->setRewardByPackageId($package_id);

                    if(count($packageData['product_set']) > 0){
                        foreach($packageData['product_set'] as $product){
                            $goods_data[] = [
                                'goods_id' => $product['product_id'],
                                'purchase_quantity' => $product['product_quantity'],
                                'purchase_amount' => 0,
                                'category_id' => 40
                            ];
                        }
                    }

                    // add item history log
                    $itemLog = $this->addSendItemLog([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'deduct_id' => $logDeduct['id'],
                        'package_id' => $packageData['package_id'],
                        'package_title' => $packageData['package_title'],
                        'send_item_status' => false,
                        'send_item_purchase_id' => '0',
                        'send_item_purchase_status' => '0',
                        'goods_data' => json_encode($goods_data),
                        'status' => 'pending',
                        'log_date' => date('Y-m-d H:i:s'),
                        'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                        'last_ip' => $this->getIP()
                    ]);
                    
                    // send items
                    $send_result_raw = $this->dosendItem($ncid, $goods_data);
                    $send_result = json_decode($send_result_raw);
                    if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                        $this->updateSendItemLog([
                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                            'status' => 'success'
                        ], $itemLog['id'], $uid);
        
                        $eventInfo = $this->setEventInfo($uid);
        
                        return response()->json([
                            'status' => true,
                            'message' => 'ซื้อแพ็คเกจสำเร็จ<br />กรุณาตรวจสอบไอเทมที่กล่องจดหมายภายในเกม',
                            'content' => $eventInfo
                        ]);

                    }else{
                        $this->updateSendItemLog([
                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                            'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                            'status' => 'unsuccess'
                        ], $itemLog['id'], $uid);
        
                        $eventInfo = $this->setEventInfo($uid);
        
                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถซื้อแพ็คเกจได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                            'content' => $eventInfo
                        ]);
                    }

                }else{
                    $arr_log['status'] = 'unsuccess';
                    $this->updateDeductLog($arr_log, $logDeduct['id']);
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }


            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />กรุณาลองใหม่อีกครั้ง'
                ]);
            }
            
        }

    }