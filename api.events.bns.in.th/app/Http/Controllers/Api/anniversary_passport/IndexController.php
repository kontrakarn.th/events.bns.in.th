<?php

    namespace App\Http\Controllers\Api\anniversary_passport;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Http\Controllers\Api\BnsEventController;

    // Utils
    use Illuminate\Support\Facades\Cache;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    // Models
    use App\Models\anniversary_passport\Member;
    use App\Models\anniversary_passport\Passport;
    use App\Models\anniversary_passport\Reward;
    use App\Models\anniversary_passport\ItemLog;
    use App\Models\anniversary_passport\DeductLog;

    class IndexController extends BnsEventController
    {
        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2019-05-08 06:00:00';
        private $endTime = '2019-06-04 23:59:59';

        private $unlock_package_diamonds = 22200; // 22200
        private $previous_checkin_diamonds = 1000; // 1000

        public function __construct(Request $request){
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus(){
            if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
                // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if ($this->checkIsMaintenance()) {
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if ($this->isLoggedIn()) {
                    // check and create member
                if ($this->checkMember() == false) {
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            } else {
                die(json_encode([
                    'status' => false,
                    'type' => 'not_login',
                    'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                ]));
            }
        }

        private function getIP(){
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn(){
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip(){
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1';

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember(){
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP()
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            } else {
                $member = Member::where('uid', $uid)->first();
                if (empty($member->ncid)) {
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if (empty($ncid)) {
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $uid);
                }
            }
            return true;
        }

        private function hasMember(int $uid){
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr){
            return Member::create($arr);
        }

        private function updateMember($row_id = 0, $arr){
            if (empty($row_id))
                return false;

            return Member::where('id', $row_id)->update($arr);
        }

        private function getNcidByUid(int $uid) : string{
            return Member::where('uid', $uid)->value('ncid');
        }

        private function getUidByNcid(string $ncid){
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username) : string{
            return Cache::remember(sha1('BNS:ANNIVERSARY_PASSPORT:ASSOCIATE_UID_WITH_NCID_' . $uid), 10, function () use ($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }

            });
        }

        private function apiDiamondBalance(int $uid)
        {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'balance',
                'uid' => $uid,
            ]);
        }

        private function setDiamondsBalance($uid){

            $diamondBalanceRaw = $this->apiDiamondBalance($uid);
            $diamondBalance = json_decode($diamondBalanceRaw);

            $diamonds = 0;
            if($diamondBalance->status == true) {
                $diamonds = intval($diamondBalance->balance);
            }

            return $diamonds;
        }

        private function apiDeductDiamond(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamomds for garena events BNS 2 Years Anniversary Passport.',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function setDiamondDeductData($deduct_diamond_amount = 0)
        {
            if ($deduct_diamond_amount == 0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function apiSendItem(string $ncid, array $goodsData) {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Sending item from garena events BNS 2 Years Anniversary Passport.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function createSendItemLog(array $arr) {
            if ($arr) {
                $resp = ItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateSendItemLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        public function getEventInfo(Request $request)
        {
            if ($request->has('type') == false || $request->type != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $groupId = $request->id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $passportInfo = $this->setPassportInfo($member->uid);

            return response()->json([
                'status' => true,
                'content' => $passportInfo,
            ]);
            
        }

        private function setPassportInfo($uid){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $unlockedPassport = false;
            $checkinList = [];

            // check unlocked passport
            $unlockedPassport = $member->unlocked_passport == 1 ? true : false;

            $currentDate = date('Y-m-d');
            $currentDateTimestamp = strtotime($currentDate);

            $passports = new Passport;
            $passportList = $passports->getPassportList();

            $passportInfo = [];
            $checkList = [];

            foreach($passportList as $week){

                $checkinList = [];

                // check checin list
                foreach($week['checkin_list'] as $checkin){

                    $hasCheckin = false;
                    $canCheckin = false;
                    $previousCheckin = false;
                    $hasCheckinIsPrev = false;

                    if(strtotime($checkin['date']) <= $currentDateTimestamp && $unlockedPassport == true){
                        // check checkin history
                        
                        $checkinHistoryCount = 0;
                        $checkinHistoryCount = $this->getCheckinItemHistoryCount($uid,$week['week'],$checkin['checkin_id']);
                        if($checkinHistoryCount > 0){
                            $hasCheckin = true;
                        }

                        // if($hasCheckin == false){

                            $prevWeek = 0;
                            $prevCheckinId = 0;
                            if($week['week'] == 1 && $checkin['checkin_id'] == 1){
                                
                                if(strtotime($checkin['date']) < $currentDateTimestamp){
                                    $previousCheckin = true;
                                }

                                // check checkin status
                                if(strtotime($checkin['date']) <= $currentDateTimestamp){
                                    $canCheckin = true;

                                }
                            }else{
                                
                                if(in_array($week['week'], [2,3,4]) && $checkin['checkin_id'] == 1 ){
                                    $prevCheckinId = 7;
                                    $prevWeek = $week['week'] - 1;
                                }else{
                                    $prevCheckinId = $checkin['checkin_id']-1;
                                    $prevWeek = $week['week'];
                                }

                                if(strtotime($checkin['date']) < $currentDateTimestamp){
                                    $previousCheckin = true;
                                }


                                $prevCheckinHistory = $this->getCheckinPreviousCount($uid,$prevWeek,$prevCheckinId);
                                // check checkin status
                                if(strtotime($checkin['date']) <= $currentDateTimestamp && $prevCheckinHistory > 0){
                                    $canCheckin = true;
                                }
                            }
                            
                            // check has checkin previous
                            $hasCheckinIsPrevCount = $this->getCheckinIsPrevCount($uid,$week['week'],$checkin['checkin_id']);
                            if($hasCheckinIsPrevCount > 0){
                                $hasCheckinIsPrev = true;
                            }

                        // }
                    }
                    
                    $checkList[$week['week']-1]['list'][] = [
                        'week' => $week['week'],
                        'checkin_id' => $checkin['checkin_id'],
                        'checkin_date' => date('j', strtotime($checkin['date'])),
                        'type' => 'checkin',
                        'title' => $checkin['reward']['product_title'],
                        'img_key' => $checkin['reward']['img_key'],
                        'has_checkin' => $hasCheckin,
                        'has_checkin_prev' => $hasCheckinIsPrev,
                        'can_checkin' => $canCheckin,
                        'previous_checkin' => $previousCheckin,
                    ];

                }

                // check special reward
                $canClaim = false;
                $hasClaim = false;
                $prevClaim = false;

                if($unlockedPassport == true){

                    // check prev special reward
                    if($week['week'] == 1){
                        $prevClaim = true;
                    }else{
                        $specialCheckinPrevCompleted = 0;
                        $specialCheckinPrevCompleted = $this->getSpecialPreviousClaimCount($uid,$week['week']-1);
                        if($specialCheckinPrevCompleted > 0){
                            $prevClaim = true;
                        }
                    }
                    
                    $specialCheckinCompletedCount = 0;
                    $specialCheckinCompletedCount = $this->getSpecialCheckinItemHistoryCount($uid,$week['week']);

                    if($prevClaim == true && $specialCheckinCompletedCount == 7){
                        $canClaim = true;
                    }

                    $specialRewardsHistory = $this->getSpecialItemHistoryCount($uid,$week['week']);

                    if($specialRewardsHistory > 0){
                        $hasClaim = true;
                    }
                }
                
                $checkList[$week['week']-1]['list'][] = [
                    'week' => $week['week'],
                    'type' => 'special',
                    'title' => $week['checkin_special']['product_title'],
                    'img_key' => $week['checkin_special']['img_key'],
                    'can_claim' => $canClaim,
                    'has_claim' => $hasClaim,
                ];

            }

            // check final rewards
            $checkinWithoutDiamonds = $this->getCheckinWithoutDiamondsCount($uid);

            $finalRewardsClaimCount = $this->getFinalRewardsClaimCount($uid);

            $passportInfo['final_rewards']['can_claim'] = $unlockedPassport == true && $checkinWithoutDiamonds == 28;
            $passportInfo['final_rewards']['has_claim'] =$finalRewardsClaimCount > 0;

            // set diamonds
            $diamondBalance = $this->setDiamondsBalance($member->uid);

            return [
                'username' => $member->username,
                'can_buy_pacakge' => $diamondBalance >= $this->unlock_package_diamonds,
                'unlock_package' => $unlockedPassport,
                'checkin_list' => $checkList,
                'final_rewards' => $passportInfo['final_rewards'],
            ];

        }

        private function getCheckinItemHistoryCount($uid,$week,$checkinId){
            return ItemLog::where('uid',$uid)
                        ->where('week', $week)
                        ->where('checkin_id', $checkinId)
                        ->where('item_type', 'checkin_reward')
                        ->count();
        }

        private function getCheckinIsPrevCount($uid,$week,$checkinId){
            return ItemLog::where('uid',$uid)
                        ->where('week', $week)
                        ->where('checkin_id', $checkinId)
                        ->where('item_type', 'checkin_reward')
                        ->where('previous_checkin', 1)
                        ->count();
        }

        private function getCheckinPreviousCount($uid,$prevWeek,$prevCheckinId){
            return ItemLog::where('uid',$uid)
                        ->where('week', $prevWeek)
                        ->where('checkin_id', $prevCheckinId)
                        ->where('item_type', 'checkin_reward')
                        ->count();
        }

        private function getCheckinWithoutDiamondsCount($uid){
            return ItemLog::where('uid',$uid)
                        ->where('previous_checkin', 0)
                        ->where('item_type', 'checkin_reward')
                        ->count();
        }

        private function getSpecialCheckinItemHistoryCount($uid,$week){
            return ItemLog::where('uid',$uid)
                        ->where('week', $week)
                        ->where('item_type', 'checkin_reward')
                        ->count();
        }

        private function getSpecialItemHistoryCount($uid,$week){
            return ItemLog::where('uid',$uid)
                        ->where('week', $week)
                        ->where('item_type', 'special_reward')
                        ->count();
        }

        private function getSpecialPreviousClaimCount($uid,$prevWeek){
            return ItemLog::where('uid',$uid)
                        ->where('week', $prevWeek)
                        ->where('item_type', 'special_reward')
                        ->count();
        }

        private function getFinalRewardsClaimCount($uid){
            return ItemLog::where('uid',$uid)
                        ->where('item_type', 'final_reward')
                        ->count();
        }

        public function getBuyPassportPackage(Request $request)
        {
            if ($request->has('type') == false || $request->type != 'buy_anniversary_package') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check already purchase package
            if($this->getUnlockPackageHistoryCount($member->uid) > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้ซื้อ 2 Years Anniversary Package ไปแล้ว'
                ]);
            }

            // check diamonds balance
            $diamondBalance = $this->setDiamondsBalance($member->uid);
            if($diamondBalance < $this->unlock_package_diamonds){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอซื้อแพ็คเกจ'
                ]);
            }

            // set deduct diamonds
            $logDeduct = $this->createDeductLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'deduct_type' => 'unlock_package',
                'diamonds' => $this->unlock_package_diamonds,
                'status' => 'pending',
                'last_ip' => $this->getIP(),
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
            ]);

            if($logDeduct){

                // set desuct diamonds
                $deductData = $this->setDiamondDeductData($this->unlock_package_diamonds);

                $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
                $resp_deduct = json_decode($resp_deduct_raw);
                if(is_object($resp_deduct) && is_null($resp_deduct) == false){

                    $this->updateDeductLog([
                        'before_deduct_diamond' => $diamondBalance,
                        'after_deduct_diamond' => $diamondBalance - $this->unlock_package_diamonds,
                        'deduct_status' => $resp_deduct->status ?: 0,
                        'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
                        'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'success'
                    ], $logDeduct['id']);

                    // set item for send
                    $rewards = new Reward;
                    $rewardInfo = $rewards->getUnlockPackage();

                    $goodsData = [
                        [
                            'goods_id' => $rewardInfo['product_id'],
                            'purchase_quantity' => $rewardInfo['product_quantity'],
                            'purchase_amount' => 0,
                            'category_id' => 40
                        ]
                    ];

                    // set send item log
                    $itemLog = $this->createSendItemLog([//add send item log
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'item_type' => 'unlock_reward',
                        'product_id' => $rewardInfo['product_id'],
                        'product_title' => $rewardInfo['product_title'],
                        'product_quantity' => $rewardInfo['product_quantity'],
                        'amount' => $rewardInfo['amount'],
                        'goods_data' => json_encode($goodsData),
                        'status' => 'pending',
                        'last_ip' => $this->getIP(),
                        'log_date' => (string)date('Y-m-d'), // set to string
                        'log_date_timestamp' => time(),
                    ]);

                    //send item
                    $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
                    $send_result = json_decode($send_result_raw);
                    if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                        //do update send item log
                        $this->updateSendItemLog([
                            'send_item_status' => $send_result->status ? : false,
                            'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                            'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                            'status' => 'success'
                        ], $itemLog['id'], $member->uid);

                        // update unlock package
                        $member->unlocked_passport = 1;
                        if($member->save()){

                            $passportInfo = $this->setPassportInfo($member->uid);

                            return response()->json([
                                'status' => true,
                                'message' => 'ซื้อแพ็คเกจ 2 Year Anniversary สำเร็จ<br />กรุณาตรวจสอบไอเทมในกล่องจดหมายภายในเกม',
                                'content' => $passportInfo,
                            ]);    

                        }else{
                            return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถดำเนินการได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                            ]);
                        }

                    }else{
                        //do update send item log
                        $this->updatePaidSendItemLog([
                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                            'status' => 'unsuccess'
                        ], $itemLog['id'], $member->uid);

                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                        ]);
                    }

                }else{
                    $arr_log['status'] = 'unsuccess';
                    $this->updateDeductLog($arr_log, $logDeduct['id']);
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }

            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />กรุณาลองใหม่อีกครั้ง'
                ]);
            }

        }

        private function getUnlockPackageHistoryCount($uid){
            return ItemLog::where('uid',$uid)
                        ->where('item_type', 'unlock_reward')
                        ->count();
        }

        private function createDeductLog(array $arr){
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductLog(array $arr, $log_id){
            if ($arr) {
                return DeductLog::where('id', $log_id)->update($arr);
            }
            return null;
        }

        // check in
        public function getCheckin(Request $request)
        {
            if ($request->has('type') == false || $request->type != 'checkin') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $week = $request->week;
            $checkin_id = $request->checkin_id;

            if(in_array($week, [1,2,3,4]) == false){
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter(2)!'
                ]);
            }

            if(in_array($checkin_id, [1,2,3,4,5,6,7]) == false){
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter(3)!'
                ]);
            }

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check already purchase package
            if($member->unlocked_passport == 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณยังไม่ได้ซื้อ 2 Years Anniversary Package'
                ]);
            }

            $passportInfo = $this->checkPassport($member->uid,$week,$checkin_id);

            if($passportInfo['can_checkin'] == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณไม่สามารถเช็คอินของวันนี้ได้'
                ]);
            }

            if($passportInfo['has_checkin'] == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้เช็คอินไปก่อนหน้านี้แล้ว'
                ]);
            }

            if($passportInfo['previous_checkin'] == true){

                // check diamonds balance
                $diamondBalance = $this->setDiamondsBalance($member->uid);
                if($diamondBalance < $this->previous_checkin_diamonds){
                    return response()->json([
                        'status' => false,
                        'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอเช็คอิน'
                    ]);
                }

                // set deduct diamonds for previous checkin
                $logDeduct = $this->createDeductLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'week' => $week,
                    'checkin_id' => $checkin_id,
                    'deduct_type' => 'checkin',
                    'diamonds' => $this->previous_checkin_diamonds,
                    'status' => 'pending',
                    'last_ip' => $this->getIP(),
                    'log_date' => (string)date('Y-m-d'), // set to string
                    'log_date_timestamp' => time(),
                ]);

                if($logDeduct){

                    // set desuct diamonds
                    $deductData = $this->setDiamondDeductData($this->previous_checkin_diamonds);

                    $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
                    $resp_deduct = json_decode($resp_deduct_raw);
                    if(is_object($resp_deduct) && is_null($resp_deduct) == false){

                        $this->updateDeductLog([
                            'before_deduct_diamond' => $diamondBalance,
                            'after_deduct_diamond' => $diamondBalance - $this->previous_checkin_diamonds,
                            'deduct_status' => $resp_deduct->status ?: 0,
                            'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
                            'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
                            'deduct_data' => json_encode($deductData),
                            'status' => 'success'
                        ], $logDeduct['id']);

                        // get rewards
                        $rewardInfo = $passportInfo['reward'];

                        $goodsData = [
                            [
                                'goods_id' => $rewardInfo['product_id'],
                                'purchase_quantity' => $rewardInfo['product_quantity'],
                                'purchase_amount' => 0,
                                'category_id' => 40
                            ]
                        ];
    
                        // set send item log
                        $itemLog = $this->createSendItemLog([//add send item log
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'week' => $week,
                            'checkin_id' => $checkin_id,
                            'item_type' => 'checkin_reward',
                            'previous_checkin' => 1,
                            'product_id' => $rewardInfo['product_id'],
                            'product_title' => $rewardInfo['product_title'],
                            'product_quantity' => $rewardInfo['product_quantity'],
                            'amount' => $rewardInfo['amount'],
                            'goods_data' => json_encode($goodsData),
                            'status' => 'pending',
                            'last_ip' => $this->getIP(),
                            'log_date' => (string)date('Y-m-d'), // set to string
                            'log_date_timestamp' => time(),
                        ]);
    
                        //send item
                        $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
                        $send_result = json_decode($send_result_raw);
                        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                            //do update send item log
                            $this->updateSendItemLog([
                                'send_item_status' => $send_result->status ? : false,
                                'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                                'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                                'status' => 'success'
                            ], $itemLog['id'], $member->uid);
    
                            $passportInfo = $this->setPassportInfo($member->uid);
    
                            return response()->json([
                                'status' => true,
                                'message' => '<h3>เช็คอินสำเร็จ</h3>ได้รับ<br />'.$rewardInfo['product_title'].'('.$rewardInfo['amount'].')<br/ >ตรวจสอบไอเทมที่กล่องจดหมาย',
                                'content' => $passportInfo,
                            ]);  
    
                        }else{
                            //do update send item log
                            $this->updatePaidSendItemLog([
                                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                                'status' => 'unsuccess'
                            ], $itemLog['id'], $member->uid);
    
                            return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                            ]);
                        }

                    }else{
                        $arr_log['status'] = 'unsuccess';
                        $this->updateDeductLog($arr_log, $logDeduct['id']);
                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                        ]);
                    }

                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'ขออภัย<br />กรุณาลองใหม่อีกครั้ง'
                    ]);
                }

            }else{

                // checkin on current day
                $rewardInfo = $passportInfo['reward'];

                $goodsData = [
                    [
                        'goods_id' => $rewardInfo['product_id'],
                        'purchase_quantity' => $rewardInfo['product_quantity'],
                        'purchase_amount' => 0,
                        'category_id' => 40
                    ]
                ];

                // set send item log
                $itemLog = $this->createSendItemLog([//add send item log
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'week' => $week,
                    'checkin_id' => $checkin_id,
                    'item_type' => 'checkin_reward',
                    'previous_checkin' => 0,
                    'product_id' => $rewardInfo['product_id'],
                    'product_title' => $rewardInfo['product_title'],
                    'product_quantity' => $rewardInfo['product_quantity'],
                    'amount' => $rewardInfo['amount'],
                    'goods_data' => json_encode($goodsData),
                    'status' => 'pending',
                    'last_ip' => $this->getIP(),
                    'log_date' => (string)date('Y-m-d'), // set to string
                    'log_date_timestamp' => time(),
                ]);

                //send item
                $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                    //do update send item log
                    $this->updateSendItemLog([
                        'send_item_status' => $send_result->status ? : false,
                        'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                        'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                        'status' => 'success'
                    ], $itemLog['id'], $member->uid);

                    $passportInfo = $this->setPassportInfo($member->uid);

                    return response()->json([
                        'status' => true,
                        'message' => '<h3>เช็คอินสำเร็จ</h3>ได้รับ<br />'.$rewardInfo['product_title'].'('.$rewardInfo['amount'].')<br/ >ตรวจสอบไอเทมที่กล่องจดหมาย',
                        'content' => $passportInfo,
                    ]);  

                }else{
                    //do update send item log
                    $this->updatePaidSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                        'status' => 'unsuccess'
                    ], $itemLog['id'], $member->uid);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                    ]);
                }
                
            }

            return response()->json([
                'status' => false,
                'message' => 'No Data Required.'
            ]);

        }

        private function checkPassport($uid=null,$week=null,$checkinId=null){
            if(empty($uid) || empty($week) || empty($checkinId)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            // check unlocked passport
            $unlockedPassport = false;
            $unlockedPassport = $member->unlocked_passport == 1 ? true : false;

            $currentDate = date('Y-m-d');
            $currentDateTimestamp = strtotime($currentDate);

            $passports = new Passport;
            $passportInfo = $passports->getCheckinInfo($week,$checkinId);

            $hasCheckin = false;
            $canCheckin = false;
            $previousCheckin = false;
            $hasCheckinIsPrev = false;

            if(strtotime($passportInfo['date']) <= $currentDateTimestamp && $unlockedPassport == true){
                // check checkin history
                $checkinHistoryCount = 0;
                $checkinHistoryCount = $this->getCheckinItemHistoryCount($uid,$week,$passportInfo['checkin_id']);
                if($checkinHistoryCount > 0){
                    $hasCheckin = true;
                }

                // if($hasCheckin == false){

                    $prevWeek = 0;
                    $prevCheckinId = 0;
                    if($week == 1 && $passportInfo['checkin_id'] == 1){
                        
                        if(strtotime($passportInfo['date']) < $currentDateTimestamp){
                            $previousCheckin = true;
                        }

                        // check checkin status
                        if(strtotime($passportInfo['date']) <= $currentDateTimestamp){
                            $canCheckin = true;
                        }
                    }else{
                        
                        if(in_array($week, [2,3,4]) && $passportInfo['checkin_id'] == 1 ){
                            $prevCheckinId = 7;
                            $prevWeek = $week - 1;
                        }else{
                            $prevCheckinId = $passportInfo['checkin_id']-1;
                            $prevWeek = $week;
                        }

                        if(strtotime($passportInfo['date']) < $currentDateTimestamp){
                            $previousCheckin = true;
                        }

                        $prevCheckinHistory = $this->getCheckinPreviousCount($uid,$prevWeek,$prevCheckinId);
                        // check checkin status
                        if(strtotime($passportInfo['date']) <= $currentDateTimestamp && $prevCheckinHistory > 0){
                            $canCheckin = true;
                        }
                    }

                    // check has checkin previous
                    $hasCheckinIsPrevCount = $this->getCheckinIsPrevCount($uid,$week,$checkinId);
                    if($hasCheckinIsPrevCount > 0){
                        $hasCheckinIsPrev = true;
                    }

                // }
            }

            return [
                'date' => $passportInfo['date'],
                'has_checkin' => $hasCheckin,
                'has_checkin_prev' => $hasCheckinIsPrev,
                'can_checkin' => $canCheckin,
                'previous_checkin' => $previousCheckin,
                'reward' => $passportInfo['reward'],
            ];

        }

        // claim special reward
        public function getClaimSpecialReward(Request $request)
        {
            if ($request->has('type') == false || $request->type != 'claim_special_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $week = $request->week;

            if(in_array($week, [1,2,3,4]) == false){
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter(2)!'
                ]);
            }

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check already purchase package
            if($member->unlocked_passport == 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณยังไม่ได้ซื้อ 2 Years Anniversary Package'
                ]);
            }

            $specialRewardInfo = $this->setSpecialReward($member->uid, $week);

            if($specialRewardInfo['can_claim'] == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถรับของวัลได้<br />เนื่องจากไม่ตรงเงื่อนไข'
                ]);
            }

            if($specialRewardInfo['has_claim'] == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปแล้ว'
                ]);
            }
            
            $passport = new Passport;
            $specialReward = $passport->getCheckinSpecialReward($week);

            // set send item
            $goodsData = [
                [
                    'goods_id' => $specialReward['product_id'],
                    'purchase_quantity' => $specialReward['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ]
            ];

            // set send item log
            $itemLog = $this->createSendItemLog([//add send item log
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'week' => $week,
                'item_type' => 'special_reward',
                'previous_checkin' => 0,
                'product_id' => $specialReward['product_id'],
                'product_title' => $specialReward['product_title'],
                'product_quantity' => $specialReward['product_quantity'],
                'amount' => $specialReward['amount'],
                'goods_data' => json_encode($goodsData),
                'status' => 'pending',
                'last_ip' => $this->getIP(),
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
            ]);

            //send item
            $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                //do update send item log
                $this->updateSendItemLog([
                    'send_item_status' => $send_result->status ? : false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                    'status' => 'success'
                ], $itemLog['id'], $member->uid);

                $passportInfo = $this->setPassportInfo($member->uid);

                return response()->json([
                    'status' => true,
                    'message' => '<h3>คุณได้รับ</h3>'.$specialReward['product_title'].'('.$specialReward['amount'].')<br/ >ตรวจสอบไอเทมที่กล่องจดหมาย',
                    'content' => $passportInfo,
                ]);  

            }else{
                //do update send item log
                $this->updatePaidSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                ], $itemLog['id'], $member->uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                ]);
            }


        }

        private function setSpecialReward($uid=null,$week=null){
            if(empty($uid) || empty($week)){
                return false;
            }

            // $member = Member::where('uid', $uid)->first();

            // check unlocked passport
            // $unlockedPassport = false;
            // $unlockedPassport = $member->unlocked_passport == 1 ? true : false;

            $currentDate = date('Y-m-d');
            $currentDateTimestamp = strtotime($currentDate);

            // check special reward
            $canClaim = false;
            $hasClaim = false;
            $prevClaim = false;

            // if($unlockedPassport == true){

                if($week == 1){
                    $prevClaim = true;
                }else{
                    $specialCheckinPrevCompleted = 0;
                    $specialCheckinPrevCompleted = $this->getSpecialPreviousClaimCount($uid,$week-1);
                    if($specialCheckinPrevCompleted > 0){
                        $prevClaim = true;
                    }
                }
                
                $specialCheckinCompletedCount = 0;
                $specialCheckinCompletedCount = $this->getSpecialCheckinItemHistoryCount($uid,$week);
                if($prevClaim == true && $specialCheckinCompletedCount == 7){
                    $canClaim = true;
                }

                $specialRewardsHistory = $this->getSpecialItemHistoryCount($uid,$week);
                if($specialRewardsHistory > 0){
                    $hasClaim = true;
                }
            // }

            return [
                'week' => $week,
                'can_claim' => $canClaim,
                'has_claim' => $hasClaim,
            ];

        }

        public function getClaimFinalReward(Request $request)
        {
            if ($request->has('type') == false || $request->type != 'claim_final_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check already purchase package
            if($member->unlocked_passport == 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณยังไม่ได้ซื้อ 2 Years Anniversary Package'
                ]);
            }

            // check final rewards
            $checkinWithoutDiamonds = $this->getCheckinWithoutDiamondsCount($uid);
            if($checkinWithoutDiamonds != 28){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถรับของวัลได้<br />เนื่องจากไม่ตรงเงื่อนไข'
                ]);
            }

            $finalRewardsClaimCount = $this->getFinalRewardsClaimCount($uid);
            if($finalRewardsClaimCount > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปแล้ว'
                ]);
            }
            
            $rewards = new Reward;
            $finalReward = $rewards->getFinalReward();

            // set send item
            $goodsData = [];
            $respReward = '';
            $rewardTitle = '';
            $rewardId = '';
            foreach($finalReward as $reward){

                $respReward .= $reward['product_title'].'<br />';

                $rewardId .= $reward['product_id'].', ';
                $rewardTitle .= $reward['product_title'].', ';

                $goodsData[] = [
                        'goods_id' => $reward['product_id'],
                        'purchase_quantity' => $reward['product_quantity'],
                        'purchase_amount' => 0,
                        'category_id' => 40,
                ];

            }
            
            if(count($goodsData) <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'No data required.'
                ]);
            }

            // set send item log
            $itemLog = $this->createSendItemLog([//add send item log
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'item_type' => 'final_reward',
                'previous_checkin' => 0,
                'product_id' => $rewardId,
                'product_title' => $rewardTitle,
                'product_quantity' => 1,
                'amount' => 1,
                'goods_data' => json_encode($goodsData),
                'status' => 'pending',
                'last_ip' => $this->getIP(),
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
            ]);

            //send item
            $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                //do update send item log
                $this->updateSendItemLog([
                    'send_item_status' => $send_result->status ? : false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                    'status' => 'success'
                ], $itemLog['id'], $member->uid);

                $passportInfo = $this->setPassportInfo($member->uid);

                return response()->json([
                    'status' => true,
                    'message' => '<h3>คุณได้รับ</h3>'.$respReward.'<br/ >ตรวจสอบไอเทมที่กล่องจดหมาย',
                    'content' => $passportInfo,
                ]);  

            }else{
                //do update send item log
                $this->updatePaidSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                ], $itemLog['id'], $member->uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                ]);
            }

        }

    }