<?php

    namespace App\Http\Controllers\Api\league;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\league\Setting;
    use App\Models\league\Member;
    use App\Models\league\Color;
    use App\Models\league\Group;
    use App\Models\league\ItemLog;
    use App\Models\league\Quest;
    use App\Models\league\QuestDailyLog;
    use App\Models\league\Reward;

    class IndexController extends BnsEventController{

        private $baseApi = 'http://api.apps.garena.in.th';

        private $userEvent = null;

        public function __construct(Request $request)
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();

        }

        private function checkEventStatus()
        {
             if($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip  = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData) {
                return true;
            } else {
                return false;
            }
        }

        private function is_accepted_ip()
        {
            $allow_ips   = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid      = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $arr = [
                    'uid'              => $uid,
                    'username'         => $username,
                    'ncid'             => $ncid,
                    'char_id'          => 0,
                    'char_name'        => '',
                    'world_id'         => 0,
                    'last_ip'          => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
                $this->userEvent = $resp;

            } else {
                $member = Member::where('uid', $uid)->first();
                if (empty($member->ncid)) {
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if (empty($ncid)) {
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
                $this->userEvent = $member;
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if (empty($_id)) {
                return false;
            }

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:LEAGUE:LIVE:ASSOCIATE_UID_WITH_NCID_'.$uid, 10, function () use ($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'user_info',
                    'uid'      => $uid,
                ]);
                $userNcInfo       = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }

            });
        }

        private function doGetChar($uid, $ncid){//new fixed function
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i       = 1;
                    foreach ($resp->response as $key => $value) {
                        // if((int)$value->level >= 60 && (int)$value->mastery_level>=12){
                            $content[] = [
                                'id' => $i,
                                'char_id'   => $value->id,
                                'char_name' => $value->name,
                                'world_id' => $value->world_id,
                                'job' => $value->job,
                                'level' => $value->level,
                                'creation_time' => $value->creation_time,
                                'last_play_start' => $value->last_play_start,
                                'last_play_end' => $value->last_play_end,
                                'last_ip' => $value->last_ip,
                                'exp' => $value->exp,
                                'mastery_level' => $value->mastery_level,
                            ];
                        // }

                        $i++;
                    }
                    return $content;
                }
            }
            return null;
        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:LEAGUE:LIVE:CHARACTERS_'.$uid, 10, function () use ($ncid) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'characters',
                    'user_id'  => $ncid,
                ]);
            });
        }

        public function selectCharacter(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'select_character') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }
            if ($decoded->has('id') == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (2)'
                ]);
            }

            $charKey = (int)$decoded['id'];

            $userData = $this->userData;
            $uid = $userData['uid'];

            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบข้อมูล'
                ]);
            }

            if($member->char_id>0){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถเปลี่ยนตัวละครได้'
                ]);
            }

            $mychar=$this->doGetChar($member->uid,$member->ncid);
            $myselect=collect($mychar)->where('id',$charKey)->first();
            if(count($myselect)==0){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบตัวละคร'
                ]);
            }

            $member->char_id   = (int)$myselect['char_id'];
            $member->char_name = $myselect['char_name'];
            $member->world_id       = $myselect['world_id'];
            $member->save();

            $eventInfo = $this->setEventInfo($uid);
            if($eventInfo['status']==false){
                return response()->json([
                        'status' => false,
                        'message'=>$eventInfo['message'],
                    ]);
            }

            return response()->json([
                        'status' => true,
                        'data' => $eventInfo
                    ]);

        }

        private function dosendItem(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name'             => 'bns',
                'service'              => 'send_item',
                'user_id'              => $ncid,
                'purchase_description' => 'Sending item from garena events BNS League.',
                'goods'                => json_encode($goodsData),
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
        }

        private function getEventSetting(){
            // return Cache::remember('BNS:LEAGUE:LIVE:EVENT_SETTING_TEST', 10, function() {
                        return Setting::where('active', 1)->first();
                    // });
        }

        public function getEventInfo(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);
            if($eventInfo['status']==false){
                return response()->json([
                        'status' => false,
                        'message'=>$eventInfo['message'],
                    ]);
            }

            return response()->json([
                'status' => true,
                'data' => $eventInfo
            ]);

        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return [
                    'status' => false,
                    'message' => 'ไม่พอข้อมูล'
                ];
            }

            $member = Member::where('uid', $uid)->first();

            if($member->char_id==0){

                $charData=$this->doGetChar($member->uid,$member->ncid);

                $content = [];
                foreach ($charData as $key => $value) {

                    $content[] = [
                        'id' => $value['id'],
                        'char_name' => $value['char_name'],
                    ];

                }
                return [
                    'status' => true,
                    'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                    'open_competition' => $this->setOpenCompetitionPeriod(),
                    'username'=>$member->username,
                    'character_name'=>'',
                    'selected_char'=>false,
                    'characters'=>$content,
                    'is_register' => false,
                    'group' => 0,
                    'color' => 0,
                ];
            }

            if($member->is_register == 0 ){
                return [
                    'status' => true,
                    'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                    'open_competition' => $this->setOpenCompetitionPeriod(),
                    'username'=>$member->username,
                    'character_name'=>$member->char_name,
                    'selected_char'=>true,
                    'characters'=>[],
                    'is_register' => false,
                    'group' => 0,
                    'color' => 0,
                ];
            }

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');

            $memberColor = Color::where('color_id', $member->color_id)->first();

            return [
                'status' => true,
                "message" => 'Event Info',
                'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                'open_competition' => $this->setOpenCompetitionPeriod(),
                'username'=>$member->username,
                'character_name'=>$member->char_name,
                'selected_char'=>true,
                'characters'=>[],
                'is_register' => ($member->is_register == 1) ? true : false,
                'color_title' => $memberColor->color_title,
                'color_id' => $memberColor->color_id,
                'color_npc' => $memberColor->color_npc,
                'group' => $member->group_id,
                'rank' => $member->rank,
            ];

        }

        public function postRegister(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'register') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() < strtotime($eventSetting->register_start) || time() > strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่อยู่ในช่วงเวลาลงทะเบียน'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 1 ){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณได้ลงทะเบียนไปแล้ว'
                ]);
            }

            // get group
            $group = Group::orderBy('member_count', 'ASC')->orderBy('group_id', 'ASC')->first();
            if(!isset($group)){
                return response()->json([
                    'status' => false,
                    'message' => 'No data.'
                ]);
            }

            if($group->member_count >= $eventSetting->max_group_member){
                $groupCount = Group::count();
                // create new group
                Group::create([
                    'group_id' => $groupCount + 1,
                    'member_count' => 0,
                ]);

                $group = Group::orderBy('member_count', 'ASC')->orderBy('group_id', 'ASC')->first();
            }

            // get color
            $color = Color::orderBy('member_count', 'ASC')->first();

            $member->is_register = 1;
            $member->group_id = $group->group_id;
            $member->color_id = $color->color_id;
            if($member->save()){

                $group->member_count = $group->member_count + 1;
                $group->save();

                $color->member_count = $color->member_count + 1;
                $color->save();

                $eventInfo = $this->setEventInfo($member->uid);

                return response()->json([
                    'status' => true,
                    'data' => $eventInfo
                ]);

            }

            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถสมัครได้ กรุณาลองใหม่อีกครั้ง'
            ]);
        }

        public function getCompetitionInfo(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'competition_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->register_start) && time() <= strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'อยู่ในช่วงเวลาลงทะเบียน ไม่สามารถดำเนินการได้'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลงทะเบียนก่อนร่วมกิจกรรม'
                ]);
            }

            // get group
            $group = Group::where('group_id', $member->group_id)->first();
            if(!isset($group)){
                return response()->json([
                    'status' => false,
                    'message' => 'No data.'
                ]);
            }

            $groupMemberRankList = [];

            $groupMemberQuery = Member::where('group_id', $group->group_id)->where('rank', '!=', 0)->orderBy('rank', 'ASC')->orderBy('rank_latest_updated', 'ASC')->get();
            // $groupMemberQuery = Member::where('group_id', $group->group_id)->where('rank', '!=', 0)->orderBy('total_points','DESC')->orderBy('rank_latest_updated', 'ASC')->get();
            
            if(count($groupMemberQuery) > 0){
                foreach($groupMemberQuery as $groupMember){
                    $groupMemberRankList[] = [
                        'rank' => $groupMember->rank,
                        'name' => $groupMember->char_name,
                        'color' => $groupMember->color_id,
                        'points' => $groupMember->total_points,
                        'group_rank_status' => $this->setMemberRankStatus($groupMember->rank, $groupMember->rank_previous),
                    ];
                }
            }

            $color = Color::where('color_id', $member->color_id)->first();

            return response()->json([
                'status' => true,
                'data' => [
                    'status' => true,
                    "message" => 'Rank Info',
                    'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                    'open_competition' => $this->setOpenCompetitionPeriod(),
                    'username'=>$member->username,
                    'character_name'=>$member->char_name,
                    'selected_char'=>true,
                    'characters'=>[],
                    'is_register' => ($member->is_register == 1) ? true : false,
                    'color_title' => $color->color_title,
                    'color_id' => $color->color_id,
                    'color_rank' => $color->color_rank,
                    'color_points' => $color->total_points,
                    'color_npc' => $color->color_npc,
                    'color_rank' => $color->color_rank,
                    'group' => $member->group_id,
                    'rank' => $member->rank,
                    'group_rank_status' => $this->setMemberRankStatus($member->rank, $member->rank_previous),
                    'points' => (int)$member->total_points,
                    'color_max_score' => (int)$eventSetting->color_max_score,
                    'rank_list' => $groupMemberRankList,
                ],
            ]);

        }

        private function setOpenCompetitionPeriod(){

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->competition_start) && time() <= strtotime($eventSetting->competition_end))) {
                return true;
            }
            return false;
        }

        private function setOpenClaimRewardPeriod(){

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->claim_reward_start) && time() <= strtotime($eventSetting->claim_reward_end))) {
                return true;
            }
            return false;
        }

        private function setMemberRankStatus($currRank=0,$prevRank=0){

            if($currRank < $prevRank){
                return 'up';
            }
            if($currRank > $prevRank){
                return 'down';
            }

            return 'none';
        }

        public function getPersonalInfo(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'personal_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->register_start) && time() <= strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'อยู่ในช่วงเวลาลงทะเบียน ไม่สามารถดำเนินการได้'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลงทะเบียนก่อนร่วมกิจกรรม'
                ]);
            }

            // create new daily quest
            // $this->createDailyQuestLog($member->uid);

            // check daily quest
            // $this->checkDailyQuestLog($member->uid);

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // free daily point
            $freeDailyPoints = Quest::where('quest_type', 'daily_free_points')->first();

            $freeDailyPointsCanClaim = false;
            $freeDailyPointsClaimed = false;

            /* $freeDailyPointsLog = QuestDailyLog::where('uid', $member->uid)->where('quest_type', 'daily_free_points')->where('log_date', $logDate)->count();
            if($freeDailyPointsLog <= 0){
                $freeDailyPointsCanClaim = true;
                $freeDailyPointsClaimed = false;
            }else{
                $freeDailyPointsCanClaim = false;
                $freeDailyPointsClaimed = true;
            } */

            $memberSpecialDailyQuest = $this->setMemberSpecialDailyQuest($member->uid,$logDate);

            $color = Color::where('color_id', $member->color_id)->first();

            return response()->json([
                'status' => true,
                'data' => [
                    'status' => true,
                    "message" => 'Personal Info',
                    'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                    'open_competition' => $this->setOpenCompetitionPeriod(),
                    'username'=>$member->username,
                    'character_name'=>$member->char_name,
                    'selected_char'=>true,
                    'characters'=>[],
                    'is_register' => ($member->is_register == 1) ? true : false,
                    'color_title' => $color->color_title,
                    'color_id' => $color->color_id,
                    'color_npc' => $color->color_npc,
                    'color_rank' => $color->color_rank,
                    'group' => $member->group_id,
                    'rank' => $member->rank,
                    'rank_status' => $this->setMemberRankStatus($member->rank, $member->rank_previous),
                    'points' => (int)$member->total_points,
                    'color_max_score' => (int)$eventSetting->color_max_score,
                    'free_daily_points' => [
                        'can_claim' => $freeDailyPointsCanClaim,
                        'claimed' => $freeDailyPointsClaimed,
                    ],
                    'quest_daily_list' => $this->setPersonalDailyQuest($member->uid,$logDate),
                    'special_quest_daily' => $this->setSepcialDailyQuest(),
                    'member_special_quest_daily' => $memberSpecialDailyQuest['member_special_quest_daily'],
                    'is_select_special_quest' => $memberSpecialDailyQuest['is_select_special_quest'],
                    'personal_reward' => $this->setPersonalReward($member->uid),
                ],
            ]);

        }

        private function checkDailyQuestLog($uid){

            $currDate = date('Y-m-d');

            $member = Member::where('uid', $uid)->first();

            $dailyQuest = QuestDailyLog::where('uid', $member->uid)->where('quest_type','daily')->where('log_date', $currDate)->where('status','pending')->get();

            if(count($dailyQuest) > 0){
                foreach($dailyQuest as $quest){

                    $logDate = date('Y-m-d', strtotime($quest->log_date));

                    $startDateTime = date('Y-m-d H:i:s', strtotime($quest->created_at));
                    $endDateTime = $logDate.' 23:59:59';
                    
                    $questApi = $this->apiQuestCompleted($quest->char_id, $quest->quest_code, $startDateTime, $endDateTime,$currDate);
                    if ($questApi && $questApi->status === true) {

                        $completed_count = 0;
                        $completed_count = (int)$questApi->response->quest_completed_count;

                        if ($completed_count >= $quest->quest_limit) {
                            // $quest->completed_count = $quest->quest_limit;
                            // $quest->status       = 'success';
                            // $quest->quest_complete_datetime = date('Y-m-d H:i:s');
                            // $quest->updated_at   = date('Y-m-d H:i:s');
                            // $quest->save();
                            QuestDailyLog::where('uid', $member->uid)->where('id',$quest->id)->update([
                                'completed_count'           => $quest->quest_limit,
                                'status'                    => 'success',
                                'quest_complete_datetime'   => date('Y-m-d H:i:s'),
                                'updated_at'                => date('Y-m-d H:i:s'),
                            ]);
                        } /* else {
                            if ($quest->completed_count < $completed_count) {
                                // $quest->completed_count = $completed_count;
                                // $quest->quest_complete_datetime = date('Y-m-d H:i:s');
                                // $quest->updated_at   = date('Y-m-d H:i:s');
                                // $quest->save();
                                QuestDailyLog::where('uid', $member->uid)->where('id',$quest->id)->update([
                                    'completed_count'           => $completed_count,
                                    'status'                    => 'success',
                                    'quest_complete_datetime'   => date('Y-m-d H:i:s'),
                                    'updated_at'                => date('Y-m-d H:i:s'),
                                ]);
                            }
                        } */

                    }

                }
            }

            return true;
        }

        private function apiQuestCompleted($char_id = null, $quest_id = null, $start_date = null, $end_date = null,$log_date='')
        {
            if (empty($char_id) || empty($quest_id) || empty($start_date) || empty($end_date) || empty($log_date)) {
                return false;
            }

            return Cache::remember('BNS:LEAGUE:LIVE:CHECK_DAILY_QUEST:'.$log_date.':'.$char_id.':'.$quest_id, 5, function () use ($char_id, $quest_id, $start_date, $end_date) {
                $data = $this->post_api($this->baseApi, [
                    'key_name'   => 'bns',
                    'service'    => 'quest_completed',
                    'char_id'    => $char_id,
                    'quest_id'   => $quest_id,
                    'start_time' => $start_date,
                    'end_time'   => $end_date,
                ]);

                return json_decode($data);
            });
        }

        private function createDailyQuestLog($uid){
            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            $member = Member::where('uid', $uid)->first();

            $dailyQuest = Quest::where('quest_type', 'daily')->get();

            if(count($dailyQuest) > 0){

                foreach($dailyQuest as $quest){

                    // check daily quest exitst
                    $checkExist = 0;
                    $checkExist = QuestDailyLog::where('uid', $member->uid)->where('quest_id', $quest->quest_id)->where('quest_type', 'daily')->where('log_date', $logDate)->count();
                    if($checkExist <= 0){
                        QuestDailyLog::create([
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'char_id' => $member->char_id,
                            'char_name' => $member->char_name,
                            'group_id' => $member->group_id,
                            'color_id' => $member->color_id,
                            'quest_id' => $quest->quest_id,
                            'quest_dungeon' => $quest->quest_dungeon,
                            'quest_title' => $quest->quest_title,
                            'quest_code' => $quest->quest_code,
                            'quest_type' => $quest->quest_type,
                            'quest_limit' => $quest->quest_limit,
                            'quest_points' => $quest->quest_points,
                            'image' => $quest->image,
                            'completed_count' => 0,
                            'claimed_count' => 0,
                            'status' => 'pending',
                            'claim_status' => 'pending',
                            'log_date' => date('Y-m-d'),
                            'log_date_timestamp' => time(),
                            'last_ip' => $this->getIP(),
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
                    }

                }

            }
        }

        private function setRequireRewardPoint($packageKey=0){

            switch($packageKey){
                case 1:
                    return 1000;
                break;

                case 2:
                    return 2000;
                break;

                case 3:
                    return 3000;
                break;

                case 4:
                    return 4000;
                break;

                case 5:
                    return 5000;
                break;
            }
            return 0;
        }

        private function setPersonalDailyQuest($uid,$logDate){

            // get daily quest list
            $memberQuestDailyListQuery = QuestDailyLog::where('uid', $uid)->where('quest_type', 'daily')->where('log_date', $logDate)->get();

            $questDailyList = [];
            if(count($memberQuestDailyListQuery) > 0){
                foreach($memberQuestDailyListQuery as $memberDaily){

                    $questDailyList[] = [
                        'id' => $memberDaily->quest_id,
                        'title' => $memberDaily->quest_title,
                        'dungeon' => $memberDaily->quest_dungeon,
                        'points' => $memberDaily->quest_points,
                        'can_claim' => $memberDaily->status == 'success' && $memberDaily->claim_status == 'pending' ? true : false,
                        'claimed' => $memberDaily->claim_status == 'claimed' ? true : false,
                    ];

                }
            }

            return $questDailyList;

        }

        private function setPersonalReward($uid){

            $member = Member::where('uid', $uid)->first();
            
            // set reward list
            $rewardsList = [];
            $rewardsQuery = Reward::where('package_type', 'personal')->groupBy('package_key')->get();
            if(count($rewardsQuery) > 0){
                foreach($rewardsQuery as $reward){

                    $claimed = false;
                    $itemLogCount = ItemLog::where('uid', $member->uid)->where('package_key', $reward->package_key)->count();
                    if($itemLogCount > 0){
                        $claimed = true;
                    }

                    $canClaim = false;
                    $requirePoints = 0;
                    $requirePoints = $this->setRequireRewardPoint($reward->package_key);
                    if($requirePoints > 0 && $member->total_points >= $requirePoints && $claimed == false){
                        $canClaim = true;
                    }

                    $rewardsList[] = [
                        'id' => $reward->package_key,
                        'title' => $reward->package_name,
                        'points' => $this->setRequireRewardPoint($reward->package_key),
                        'can_claim' => $canClaim,
                        'claimed' => $claimed,
                    ];
                }
            }

            return $rewardsList;
        }

        private function setSepcialDailyQuest(){

            $eventSetting = $this->getEventSetting();

            if ((time() < strtotime($eventSetting->competition_start) || time() > strtotime($eventSetting->competition_end))) {
                return [];
            }

            // get special daily quest list
            $specialQuestDailyListQuery = Quest::whereIn('quest_type', ['daily_special','daily_special_bosskill'])->get();

            $specialQuestDailyList = [];
            if(count($specialQuestDailyListQuery) > 0){
                foreach($specialQuestDailyListQuery as $specialQuestDaily){
                    $specialQuestDailyList[] = [
                        'id' => $specialQuestDaily->quest_id,
                        'title' => $specialQuestDaily->quest_title,
                        'dungeon' => $specialQuestDaily->quest_dungeon,
                        'points' => $specialQuestDaily->quest_points,
                    ];
                }
            }

            return $specialQuestDailyList;
        }

        private function setMemberSpecialDailyQuest($uid,$logDate){
            // set selected special quest list
            $isSelectSpecialQuest = false;
            $memberSpecialQuestDaily = [];
            $memberSpecialQuestDailyQuery = QuestDailyLog::whereIn('quest_type', ['daily_special','daily_special_bosskill'])->where('uid', $uid)->where('log_date', $logDate)->first();

            if($memberSpecialQuestDailyQuery){
                $isSelectSpecialQuest = true;
                $memberSpecialQuestDaily = [
                    'id' => $memberSpecialQuestDailyQuery->quest_id,
                    'title' => $memberSpecialQuestDailyQuery->quest_title,
                    'dungeon' => $memberSpecialQuestDailyQuery->quest_dungeon,
                    'points' => $memberSpecialQuestDailyQuery->quest_points,
                    'can_claim' => $memberSpecialQuestDailyQuery->completed_count > $memberSpecialQuestDailyQuery->claimed_count,
                    'total_count' => $memberSpecialQuestDailyQuery->completed_count,
                    'total_points' => $memberSpecialQuestDailyQuery->completed_count * $memberSpecialQuestDailyQuery->quest_points,
                    'remain_count' => $memberSpecialQuestDailyQuery->completed_count - $memberSpecialQuestDailyQuery->claimed_count,
                    'remain_points' => ($memberSpecialQuestDailyQuery->completed_count - $memberSpecialQuestDailyQuery->claimed_count) * $memberSpecialQuestDailyQuery->quest_points,
                ];
            }

            return [
                'member_special_quest_daily' => (object)$memberSpecialQuestDaily,
                'is_select_special_quest' => $isSelectSpecialQuest,
            ];
        }

        public function postClaimFreePoints(Request $request){

            $decoded = $request;
            if($decoded->has('type') == false || $decoded['type'] != 'claim_free_points') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->register_start) && time() <= strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'อยู่ในช่วงเวลาลงทะเบียน ไม่สามารถดำเนินการได้'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลงทะเบียนก่อนร่วมกิจกรรม'
                ]);
            }

            $questId = $decoded['id'];

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // check claim log history
            $freeDailyLog = QuestDailyLog::where('uid', $member->uid)->where('quest_type', 'daily_free_points')->where('log_date', $logDate)->first();
            if(isset($freeDailyLog) && !empty($freeDailyLog->quest_id)){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณรับเรียญฟรีของวันนี้ไปแล้ว'
                ]);
            }

            // get free points quest
            $quest = Quest::where('quest_type', 'daily_free_points')->first();

            // create free points log
            QuestDailyLog::create([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'char_id' => $member->char_id,
                'char_name' => $member->char_name,
                'group_id' => $member->group_id,
                'color_id' => $member->color_id,
                'quest_id' => $quest->quest_id,
                'quest_dungeon' => $quest->quest_dungeon,
                'quest_title' => $quest->quest_title,
                'quest_code' => $quest->quest_code,
                'quest_type' => $quest->quest_type,
                'quest_limit' => $quest->quest_limit,
                'quest_points' => $quest->quest_points,
                'image' => $quest->image,
                'completed_count' => 1,
                'claimed_count' => 1,
                'status' => 'success',
                'claim_status' => 'claimed',
                'log_date' => date('Y-m-d'),
                'log_date_timestamp' => time(),
                'quest_complete_datetime' => date('Y-m-d H:i:s'),
                'last_ip' => $this->getIP(),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            // update points to member
            $member->total_points = $member->total_points + $quest->quest_points;
            if($member->save()){

                $color = Color::where('color_id', $member->color_id)->first();

                return response()->json([
                    'status' => true,
                    'data' => [
                        'status' => true,
                        "message" => 'Personal Info',
                        'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                        'open_competition' => $this->setOpenCompetitionPeriod(),
                        'username'=>$member->username,
                        'character_name'=>$member->char_name,
                        'selected_char'=>true,
                        'characters'=>[],
                        'is_register' => ($member->is_register == 1) ? true : false,
                        'color_title' => $color->color_title,
                        'color_id' => $color->color_id,
                        'color_npc' => $color->color_npc,
                        'color_rank' => $color->color_rank,
                        'group' => $member->group_id,
                        'rank' => $member->rank,
                        'rank_status' => $this->setMemberRankStatus($member->rank, $member->rank_previous),
                        'points' => (int)$member->total_points,
                        'color_max_score' => (int)$eventSetting->color_max_score,
                        'free_daily_points' => [
                            'can_claim' => false,
                            'claimed' => true,
                        ],
                        'personal_reward' => $this->setPersonalReward($member->uid,$logDate),
                    ],
                ]);

            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย ไม่สามารถดำเนินการได้  กรุณาลองใหม่อีกครั้ง'
                ]);
            }

        }

        public function postClaimQuestPoints(Request $request){

            $decoded = $request;
            if($decoded->has('type') == false || $decoded['type'] != 'claim_quest_points') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->register_start) && time() <= strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'อยู่ในช่วงเวลาลงทะเบียน ไม่สามารถดำเนินการได้'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลงทะเบียนก่อนร่วมกิจกรรม'
                ]);
            }

            $questId = $decoded['id'];

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // check member is aready select quest
            $selectedQuest = QuestDailyLog::where('quest_type', 'daily')->where('uid', $member->uid)->where('quest_id', $questId)->where('log_date', $logDate)->first();
            if(!$selectedQuest){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย ไม่มีเควสที่เลือก'
                ]);
            }

            // check quest is already claim points and quest is special daily
            if($selectedQuest->status == 'pending'){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย เควสยังไม่สำเร็จ'
                ]);
            }

            if($selectedQuest->claim_status == 'claimed'){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณได้รับเรียญของเควสนี้ไแล้ว'
                ]);
            }

            $questPoints = 0;
            $questPoints = $selectedQuest->quest_points;

            // update claim status to success
            $selectedQuest->claimed_count = 1;
            $selectedQuest->claim_status = 'claimed';
            $selectedQuest->last_ip = $this->getIP();
            $selectedQuest->created_at = date('Y-m-d H:i:s');
            $selectedQuest->updated_at = date('Y-m-d H:i:s');
            if($selectedQuest->save()){

                // update points to member
                $member->total_points = $member->total_points + $questPoints;
                if($member->save()){

                    $color = Color::where('color_id', $member->color_id)->first();

                    return response()->json([
                        'status' => true,
                        'data' => [
                            'status' => true,
                            "message" => 'Personal Info',
                            'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                            'open_competition' => $this->setOpenCompetitionPeriod(),
                            'username'=>$member->username,
                            'character_name'=>$member->char_name,
                            'selected_char'=>true,
                            'characters'=>[],
                            'is_register' => ($member->is_register == 1) ? true : false,
                            'color_title' => $color->color_title,
                            'color_id' => $color->color_id,
                            'color_npc' => $color->color_npc,
                            'color_rank' => $color->color_rank,
                            'group' => $member->group_id,
                            'rank' => $member->rank,
                            'rank_status' => $this->setMemberRankStatus($member->rank, $member->rank_previous),
                            'points' => (int)$member->total_points,
                            'color_max_score' => (int)$eventSetting->color_max_score,
                            'quest_daily_list' => $this->setPersonalDailyQuest($member->uid,$logDate),
                            'personal_reward' => $this->setPersonalReward($member->uid,$logDate),
                        ],
                    ]);
                }

            }

            return response()->json([
                'status' => false,
                'message' => 'ขออภัย ไม่สามารถดำเนินการได้  กรุณาลองใหม่อีกครั้ง'
            ]);
        }

        public function postSelectSpecialQuest(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'select_special_quest') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('id') == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter(2)!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->register_start) && time() <= strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'อยู่ในช่วงเวลาลงทะเบียน ไม่สามารถดำเนินการได้'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลงทะเบียนก่อนร่วมกิจกรรม'
                ]);
            }

            $questId = $decoded['id'];

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // check member is aready select quest
            $selectedQuest = QuestDailyLog::whereIn('quest_type', ['daily_special','daily_special_bosskill'])->where('uid', $member->uid)->where('log_date', $logDate)->count();
            if($selectedQuest > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณได้เลือกเควสของวันนี้ไปแล้ว'
                ]);
            }

            $questInfo = Quest::where('quest_id', $questId)->whereIn('quest_type', ['daily_special','daily_special_bosskill'])->first();
            if(!$questInfo){
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter(3)!'
                ]);
            }

            // create quest log
            QuestDailyLog::create([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'char_id' => $member->char_id,
                'char_name' => $member->char_name,
                'group_id' => $member->group_id,
                'color_id' => $member->color_id,
                'quest_id' => $questInfo->quest_id,
                'quest_dungeon' => $questInfo->quest_dungeon,
                'quest_title' => $questInfo->quest_title,
                'quest_code' => $questInfo->quest_code,
                'quest_type' => $questInfo->quest_type,
                'quest_limit' => $questInfo->quest_limit,
                'quest_points' => $questInfo->quest_points,
                'image' => $questInfo->image,
                'completed_count' => 0,
                'claimed_count' => 0,
                'status' => 'pending',
                'claim_status' => 'pending',
                'log_date' => date('Y-m-d'),
                'log_date_timestamp' => time(),
                'quest_complete_datetime' => date('Y-m-d H:i:s'),
                'last_ip' => $this->getIP(),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $color = Color::where('color_id', $member->color_id)->first();

            $memberSpecialDailyQuest = $this->setMemberSpecialDailyQuest($member->uid,$logDate);

            return response()->json([
                'status' => true,
                'data' => [
                    'status' => true,
                    "message" => 'Personal Info',
                    'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                    'open_competition' => $this->setOpenCompetitionPeriod(),
                    'username'=>$member->username,
                    'character_name'=>$member->char_name,
                    'selected_char'=>true,
                    'characters'=>[],
                    'is_register' => ($member->is_register == 1) ? true : false,
                    'color_title' => $color->color_title,
                    'color_id' => $color->color_id,
                    'color_npc' => $color->color_npc,
                    'color_rank' => $color->color_rank,
                    'group' => $member->group_id,
                    'rank' => $member->rank,
                    'rank_status' => $this->setMemberRankStatus($member->rank, $member->rank_previous),
                    'points' => (int)$member->total_points,
                    'color_max_score' => (int)$eventSetting->color_max_score,
                    'member_special_quest_daily' => $memberSpecialDailyQuest['member_special_quest_daily'],
                    'is_select_special_quest' => $memberSpecialDailyQuest['is_select_special_quest'],
                ],
            ]);
        }

        public function postClaimSpecialQuestPoints(Request $request){

            $decoded = $request;
            if($decoded->has('type') == false || $decoded['type'] != 'claim_special_quest_points') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->register_start) && time() <= strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'อยู่ในช่วงเวลาลงทะเบียน ไม่สามารถดำเนินการได้'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลงทะเบียนก่อนร่วมกิจกรรม'
                ]);
            }

            $questId = $decoded['id'];

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // check member is aready select quest
            $selectedQuest = QuestDailyLog::whereIn('quest_type', ['daily_special','daily_special_bosskill'])->where('uid', $member->uid)->where('log_date', $logDate)->first();
            if(!isset($selectedQuest)){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณยังไม่ได้เลือกเควสของวันนี้'
                ]);
            }

            // check quest is already claim points and quest is special daily
            if($selectedQuest->status == 'pending'){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย เควสยังไม่สำเร็จ'
                ]);
            }

            if($selectedQuest->completed_count - $selectedQuest->claimed_count <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณได้รับเหรียญจากเควสนี้ไปแล้ว'
                ]);
            }

            // set points update to member points
            $remainPoints = ($selectedQuest->completed_count - $selectedQuest->claimed_count) * $selectedQuest->quest_points;
            if($remainPoints <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย เหรียญไม่พอ'
                ]);
            }

            // update daily log
            $selectedQuest->claimed_count = $selectedQuest->completed_count;
            $selectedQuest->claim_status = 'claimed';
            $selectedQuest->save();

            // update membrer points
            $member->total_points = $member->total_points + $remainPoints;
            $member->save();

            // set selected special quest list
            $memberSpecialDailyQuest = $this->setMemberSpecialDailyQuest($member->uid,$logDate);

            $color = Color::where('color_id', $member->color_id)->first();

            return response()->json([
                'status' => true,
                'data' => [
                    'status' => true,
                    "message" => 'Personal Info',
                    'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                    'open_competition' => $this->setOpenCompetitionPeriod(),
                    'username'=>$member->username,
                    'character_name'=>$member->char_name,
                    'selected_char'=>true,
                    'characters'=>[],
                    'is_register' => ($member->is_register == 1) ? true : false,
                    'color_title' => $color->color_title,
                    'color_id' => $color->color_id,
                    'color_npc' => $color->color_npc,
                    'color_rank' => $color->color_rank,
                    'group' => $member->group_id,
                    'rank' => $member->rank,
                    'rank_status' => $this->setMemberRankStatus($member->rank, $member->rank_previous),
                    'points' => (int)$member->total_points,
                    'color_max_score' => (int)$eventSetting->color_max_score,
                    'member_special_quest_daily' => $memberSpecialDailyQuest['member_special_quest_daily'],
                    'is_select_special_quest' => $memberSpecialDailyQuest['is_select_special_quest'],
                    'personal_reward' => $this->setPersonalReward($member->uid,$logDate),
                ],
            ]);

        }

        public function postClaimPersonalReward(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'claim_personal_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->register_start) && time() <= strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'อยู่ในช่วงเวลาลงทะเบียน ไม่สามารถดำเนินการได้'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลงทะเบียนก่อนร่วมกิจกรรม'
                ]);
            }

            $packageKey = $decoded['id'];

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // get reward from packge_key
            $rewards = Reward::where('package_type', 'personal')->where('package_key', $packageKey)->get();
            if(count($rewards) <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'no reward data.'
                ]);
            }

            // set reward info for check condition
            $packageName = $rewards[0]->package_name;
            $packageQuantity = $rewards[0]->package_quantity;
            $packageAmount = $rewards[0]->package_amount;
            $packageType = $rewards[0]->package_type;
            $packageRequirePoints = $this->setRequireRewardPoint($rewards[0]->package_key);

            // check claim log
            $claimLog = ItemLog::where('uid', $member->uid)->where('package_key', $packageKey)->where('package_type', $packageType)->count();
            if($claimLog > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณได้รับของรางวัลนี้ไปแล้ว'
                ]);
            }
            
            // check require points for exchange reward
            if($member->total_points < $packageRequirePoints){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย เหรียญสะสมของคุณไม่พอแลกของรางวัล'
                ]);
            }

            // set reward info
            $goods_data = []; //good data for send item group
            foreach ($rewards as $key => $reward) {

                $goods_data[] = [
                    'goods_id' => $reward->package_id,
                    'purchase_quantity' => $reward->package_quantity,
                    'purchase_amount' => 0,
                    'category_id' => 40,
                ];

            }

            $sendItemLog = $this->addItemHistoryLog([
                'uid'                           => $member->uid,
                'username'                      => $member->username,
                'ncid'                          => $member->ncid,
                'package_key'                   => $packageKey,
                'package_name'                  => $packageName,
                'package_quantity'              => $packageQuantity,
                'package_amount'                => $packageAmount,
                'package_type'                  => $packageType,
                'send_item_status'              => false,
                'send_item_purchase_id'         => 0,
                'send_item_purchase_status'     => 0,
                'goods_data'                    => json_encode($goods_data),
                'status'                        => 'pending',
                'log_date'                      => (string)date('Y-m-d'), // set to string
                'log_date_timestamp'            => time(),
                'last_ip'                       => $this->getIP(),
            ]);

            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            // $send_result = (object)[
            //     "status" => true,
            //     "response" => (object)[
            //         "purchase_id" => "23857513",
            //         "purchase_status" => "4"
            //     ]
            // ];

            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                // update send item log
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ?: false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                    'status' => 'success'
                ], $sendItemLog['id'], $uid);

                $color = Color::where('color_id', $member->color_id)->first();

                return response()->json([
                    'status' => true,
                    'message' => "ได้รับ ".$packageName." <br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                    'data' => [
                        'status' => true,
                        "message" => 'Personal Info',
                        'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                        'open_competition' => $this->setOpenCompetitionPeriod(),
                        'username'=>$member->username,
                        'character_name'=>$member->char_name,
                        'selected_char'=>true,
                        'characters'=>[],
                        'is_register' => ($member->is_register == 1) ? true : false,
                        'color_title' => $color->color_title,
                        'color_id' => $color->color_id,
                        'color_npc' => $color->color_npc,
                        'color_rank' => $color->color_rank,
                        'group' => $member->group_id,
                        'rank' => $member->rank,
                        'rank_status' => $this->setMemberRankStatus($member->rank, $member->rank_previous),
                        'points' => (int)$member->total_points,
                        'color_max_score' => (int)$eventSetting->color_max_score,
                        'personal_reward' => $this->setPersonalReward($member->uid,$logDate),
                    ],
                ]);

            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                        ], $sendItemLog['id'], $uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                            // 'data' => $eventInfo
                        ]);
            }

        }

        public function getColorRankInfo(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'color_rank_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->register_start) && time() <= strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'อยู่ในช่วงเวลาลงทะเบียน ไม่สามารถดำเนินการได้'
                ]);
            }

            /* if ((time() < strtotime($eventSetting->competition_start) || time() > strtotime($eventSetting->competition_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่อยู่ในช่วงเวลาของการแข่งขัน'
                ]);
            } */

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลงทะเบียนก่อนร่วมกิจกรรม'
                ]);
            }

            // get color list
            $colors = Color::all();

            $colorList = [];
            if(count($colors) > 0){

                foreach($colors as $color){

                    // get top player score
                    $playerChar = '';
                    $playerPoints = 0;

                    $palyer = null;
                    $palyer = Member::where('is_register', 1)->where('color_id', $color->color_id)->where('rank','!=', 0)->orderBy('total_points', 'DESC')->orderBy('rank_latest_updated', 'DESC')->first();
                    if($palyer && $color->member_top_rank != 0){
                        $playerChar = $palyer->char_name;
                        $playerPoints = number_format($palyer->total_points);
                    }

                    $colorList[] = [
                        'id' => $color->color_id,
                        'title' => $color->color_title,
                        'npc' => $color->color_npc,
                        'rank' => $color->color_rank,
                        'points_shot' => $this->setThousandsFormat($color->total_points),
                        'points_full' => (int)$color->total_points,
                        'top_player_points' => $playerPoints,
                        'top_player_name' => $playerChar,
                    ];

                }

            }

            $memberColor = Color::where('color_id', $member->color_id)->first();

            return response()->json([
                'status' => true,
                'data' => [
                    'status' => true,
                    "message" => 'Color Info',
                    'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                    'open_competition' => $this->setOpenCompetitionPeriod(),
                    'username'=>$member->username,
                    'character_name'=>$member->char_name,
                    'selected_char'=>true,
                    'characters'=>[],
                    'is_register' => ($member->is_register == 1) ? true : false,
                    'color_title' => $memberColor->color_title,
                    'color_id' => $memberColor->color_id,
                    'color_npc' => $memberColor->color_npc,
                    'group' => $member->group_id,
                    'rank' => $member->rank,
                    'rank_status' => $this->setMemberRankStatus($member->rank, $member->rank_previous),
                    'points' => (int)$member->total_points,
                    'color_max_score' => (int)$eventSetting->color_max_score,
                    'color_list' => $colorList,
                ],
            ]);

        }

        private function setThousandsFormat($num) {

            if($num>=1000) {
          
                  $x = round($num);
                  $x_number_format = number_format($x);
                  $x_array = explode(',', $x_number_format);
                  $x_parts = array('k', 'm', 'b', 't');
                  $x_count_parts = count($x_array) - 1;
                  $x_display = $x;
                  $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
                  $x_display .= $x_parts[$x_count_parts - 1];
                
                  return $x_display;
          
            }
          
            return $num;
        }

        public function getHistory(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'history_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->register_start) && time() <= strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'อยู่ในช่วงเวลาลงทะเบียน ไม่สามารถดำเนินการได้'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลงทะเบียนก่อนร่วมกิจกรรม'
                ]);
            }

            // item history
            $questHistory = [];
            $questHistoryQuery = QuestDailyLog::where('uid', $member->uid)->orderBy('log_date_timestamp', 'DESC')->get();
            if(count($questHistoryQuery) > 0){
                foreach($questHistoryQuery as $quest){

                    $status = 'กำลังทำ';
                    if($quest->status == 'success'){
                        $status = 'สำเร็จ';
                    }

                    $special = '';
                    if($quest->quest_type == 'daily_special' || $quest->quest_type == 'daily_special_bosskill'){
                        $special = '(ภารกิจพิเศษ)';
                    }

                    $dungeon = '';
                    if($quest->quest_type != 'daily_free_points'){
                        $dungeon = '('.$quest->quest_dungeon.')';
                    }

                    $questHistory[] = [
                        'title' => $status.'เควส '.$quest->quest_title.$dungeon.$special,
                        'datetime' => !empty($quest->quest_complete_datetime) && $quest->quest_complete_datetime != '0000-00-00 00:00:00' ? date('d/m/Y H:i:s', strtotime($quest->quest_complete_datetime)) : '-',
                    ];
                }
            }

            $color = Color::where('color_id', $member->color_id)->first();

            return response()->json([
                'status' => true,
                'message' => 'History',
                'data' => [
                    'status' => true,
                    "message" => 'Color Info',
                    'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                    'open_competition' => $this->setOpenCompetitionPeriod(),
                    'username'=>$member->username,
                    'character_name'=>$member->char_name,
                    'selected_char'=>true,
                    'characters'=>[],
                    'is_register' => ($member->is_register == 1) ? true : false,
                    'color_title' => $color->color_title,
                    'color_id' => $color->color_id,
                    'color_npc' => $color->color_npc,
                    'color_rank' => $color->color_rank,
                    'group' => $member->group_id,
                    'rank' => $member->rank,
                    'rank_status' => $this->setMemberRankStatus($member->rank, $member->rank_previous),
                    'points' => (int)$member->total_points,
                    'color_max_score' => (int)$eventSetting->color_max_score,
                    'quest_history' => $questHistory,
                ]
            ]);

        }

        public function getRewardInfo(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'reward_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->register_start) && time() <= strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'อยู่ในช่วงเวลาลงทะเบียน ไม่สามารถดำเนินการได้'
                ]);
            }

            if ((time() < strtotime($eventSetting->claim_reward_start) || time() > strtotime($eventSetting->claim_reward_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่อยู่ในช่วงเวลารับของรางวัล ไม่สามารถดำเนินการได้'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลงทะเบียนก่อนร่วมกิจกรรม'
                ]);
            }

            // get package key by rank
            $rankRewardCanClaim = false;
            $rankRewardClaimed = false;
            $rankRewardKey = 0;
            $rankRewardTitle = '';
            $rankRewardAmount = 0;
            $rankRewardRank = 0;
            
            $rankPackageKey = $this->setGroupPackageByRank($member->rank);

            // check item log
            $rankRewardLog = ItemLog::where('uid', $member->uid)->where('package_type', 'group')->where('package_key', $rankPackageKey)->count();
            if($rankRewardLog > 0){
                $rankRewardClaimed = true;
            }

            // get rank reward
            $rankReward = Reward::where('package_type', 'group')->where('package_key', $rankPackageKey)->first();
            if($rankReward){

                if($rankRewardClaimed == false){
                    $rankRewardCanClaim = true;
                }
                
                $rankRewardKey = $rankReward->package_key;
                $rankRewardTitle = $rankReward->package_name;
                $rankRewardAmount = $rankReward->package_amount;
                $rankRewardRank = $member->rank;
            }

            // get member color info
            $colorInfo = Color::where('color_id', $member->color_id)->first();
            if(!$colorInfo){
                return response()->json([
                    'status' => false,
                    'message' => 'No reward data(2).'
                ]);
            }

            // set package key by color rank
            $colorRewardCanClaim = false;
            $colorRewardClaimed = false;
            $colorRewardKey = 0;
            $colorRewardTitle = '';
            $colorRewardAmount = 0;
            $colorRewardRank = 0;

            $colorPackageKey = $this->setColorPackageByRank($colorInfo->color_rank);

            // check item log
            $colorRewardLog = ItemLog::where('uid', $member->uid)->where('package_type', 'color')->where('package_key', $colorPackageKey)->count();
            if($colorRewardLog > 0){
                $colorRewardClaimed = true;
            }
            
            $colorReward = Reward::where('package_type', 'color')->where('package_key', $colorPackageKey)->first();
            if($colorReward){
                
                if($colorRewardClaimed == false){
                    $colorRewardCanClaim = true;
                }
                
                $colorRewardKey = $colorReward->package_key;
                $colorRewardTitle = $colorReward->package_name;
                $colorRewardAmount = $colorReward->package_amount;
                $colorRewardRank = $colorInfo->color_rank;
            }

            return response()->json([
                'status' => true,
                'message' => 'History',
                'data' => [
                    'status' => true,
                    "message" => 'Reward Info',
                    'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                    'open_competition' => $this->setOpenCompetitionPeriod(),
                    'username'=>$member->username,
                    'character_name'=>$member->char_name,
                    'selected_char'=>true,
                    'characters'=>[],
                    'is_register' => ($member->is_register == 1) ? true : false,
                    'color_title' => $colorInfo->color_title,
                    'color_id' => $colorInfo->color_id,
                    'color_npc' => $colorInfo->color_npc,
                    'color_rank' => $colorInfo->color_rank,
                    'group' => $member->group_id,
                    'rank' => $member->rank,
                    'rank_status' => $this->setMemberRankStatus($member->rank, $member->rank_previous),
                    'points' => (int)$member->total_points,
                    'color_max_score' => (int)$eventSetting->color_max_score,
                    'rank_reward' => [
                        'can_claim' => $rankRewardCanClaim,
                        'claimed' => $rankRewardClaimed,
                        'id' => $rankRewardKey,
                        'title' => $rankRewardTitle,
                        'amount' => $rankRewardAmount,
                        'rank' => $rankRewardRank,
                    ],
                    'color_reward' => [
                        'can_claim' => $colorRewardCanClaim,
                        'claimed' => $colorRewardClaimed,
                        'id' => $colorRewardKey,
                        'title' => $colorRewardTitle,
                        'amount' => $colorRewardAmount,
                        'rank' => $colorRewardRank,
                    ],
                ]
            ]);
        }

        private function setGroupPackageByRank($rank=0){

            if($rank >= 1 && $rank <= 5){
                return 6;
            }elseif($rank >= 6 && $rank <= 10){
                return 7;
            }elseif($rank >= 11 && $rank <= 50){
                return 8;
            }elseif($rank >= 51 && $rank <= 100){
                return 9;
            }
            
            return 0;
        }

        private function setColorPackageByRank($rank=0){

            switch($rank){
                case 1;
                    return 10;
                break;

                case 2;
                    return 11;
                break;

                case 3;
                    return 12;
                break;
            }
            
            return 0;
        }

        public function postClaimRankReward(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'claim_rank_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->register_start) && time() <= strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'อยู่ในช่วงเวลาลงทะเบียน ไม่สามารถดำเนินการได้'
                ]);
            }

            if ((time() < strtotime($eventSetting->claim_reward_start) || time() > strtotime($eventSetting->claim_reward_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่อยู่ในช่วงเวลารับของรางวัล ไม่สามารถดำเนินการได้'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลงทะเบียนก่อนร่วมกิจกรรม'
                ]);
            }

            if($member->total_points <= 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณไม่ได้สะสมแต้มตามที่กำหนดไว้ ไม่สามารถรับของรางวัลได้'
                ]);
            }

            // check and get package key by rank

            // get rank reward
            $rankRewardCanClaim = false;
            $rankRewardClaimed = false;
            $rankRewardKey = 0;
            $rankRewardTitle = '';
            $rankRewardAmount = 0;
            $rankRewardRank = 0;

            $rankPackageKey = $this->setGroupPackageByRank($member->rank);

            // check item log
            $rankRewardLog = ItemLog::where('uid', $member->uid)->where('package_type', 'group')->where('package_key', $rankPackageKey)->count();
            if($rankRewardLog > 0){
                $rankRewardClaimed = true;
            }

            $rankReward = Reward::where('package_type', 'group')->where('package_key', $rankPackageKey)->first();
            if($rankReward){

                if($rankRewardClaimed == false){
                    $rankRewardCanClaim = true;
                }
                
                $rankRewardKey = $rankReward->package_key;
                $rankRewardTitle = $rankReward->package_name;
                $rankRewardAmount = $rankReward->package_amount;
                $rankRewardRank = $member->rank;
            }
            
            if($rankRewardCanClaim == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย ไม่สามารถรับของรางวัลได้'
                ]);
            }

            if($rankRewardClaimed == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณได้รับของรางวัลนี้ไปแล้ว'
                ]);
            }

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // get reward from packge_key
            $rewards = Reward::where('package_type', 'group')->where('package_key', $rankPackageKey)->get();
            if(count($rewards) <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'no reward data.'
                ]);
            }

            // set reward info for check condition
            $packageName = $rewards[0]->package_name;
            $packageQuantity = $rewards[0]->package_quantity;
            $packageAmount = $rewards[0]->package_amount;
            $packageType = $rewards[0]->package_type;
            $packageRequirePoints = $this->setRequireRewardPoint($rewards[0]->package_key);

            // check claim log
            // $claimLog = ItemLog::where('uid', $member->uid)->where('package_key', $rankPackageKey)->where('package_type', $packageType)->count();
            // if($claimLog > 0){
            //     return response()->json([
            //         'status' => false,
            //         'message' => 'ขออภัย คุณได้รับของรางวัลนี้ไปแล้ว'
            //     ]);
            // }
            
            // check require points for exchange reward
            if($member->total_points < $packageRequirePoints){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย เหรียญสะสมของคุณไม่พอแลกของรางวัล'
                ]);
            }

            // set reward info
            $goods_data = []; //good data for send item group
            foreach ($rewards as $key => $reward) {

                $goods_data[] = [
                    'goods_id' => $reward->package_id,
                    'purchase_quantity' => $reward->package_quantity,
                    'purchase_amount' => 0,
                    'category_id' => 40,
                ];

            }

            $sendItemLog = $this->addItemHistoryLog([
                'uid'                           => $member->uid,
                'username'                      => $member->username,
                'ncid'                          => $member->ncid,
                'package_key'                   => $rankPackageKey,
                'package_name'                  => $packageName,
                'package_quantity'              => $packageQuantity,
                'package_amount'                => $packageAmount,
                'package_type'                  => $packageType,
                'send_item_status'              => false,
                'send_item_purchase_id'         => 0,
                'send_item_purchase_status'     => 0,
                'goods_data'                    => json_encode($goods_data),
                'status'                        => 'pending',
                'log_date'                      => (string)date('Y-m-d'), // set to string
                'log_date_timestamp'            => time(),
                'last_ip'                       => $this->getIP(),
            ]);

            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);

            // $send_result = (object)[
            //     "status" => true,
            //     "response" => (object)[
            //         "purchase_id" => "23857513",
            //         "purchase_status" => "4"
            //     ]
            // ];

            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                // update send item log
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ?: false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                    'status' => 'success'
                ], $sendItemLog['id'], $uid);

                $color = Color::where('color_id', $member->color_id)->first();

                return response()->json([
                    'status' => true,
                    'message' => "ได้รับ ".$packageName." <br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                    'data' => [
                        'status' => true,
                        "message" => 'Reward Info',
                        'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                        'open_competition' => $this->setOpenCompetitionPeriod(),
                        'username'=>$member->username,
                        'character_name'=>$member->char_name,
                        'selected_char'=>true,
                        'characters'=>[],
                        'is_register' => ($member->is_register == 1) ? true : false,
                        'color_title' => $color->color_title,
                        'color_id' => $color->color_id,
                        'color_npc' => $color->color_npc,
                        'color_rank' => $color->color_rank,
                        'group' => $member->group_id,
                        'rank' => $member->rank,
                        'rank_status' => $this->setMemberRankStatus($member->rank, $member->rank_previous),
                        'points' => (int)$member->total_points,
                        'color_max_score' => (int)$eventSetting->color_max_score,
                        'rank_reward' => [
                            'can_claim' => false,
                            'claimed' => true,
                            'id' => $rankRewardKey,
                            'title' => $rankRewardTitle,
                            'amount' => $rankRewardAmount,
                            'rank' => $rankRewardRank,
                        ],
                    ]
                ]);

            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                        ], $sendItemLog['id'], $uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                            // 'data' => $eventInfo
                        ]);
            }

        }

        public function postClaimColorReward(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'claim_color_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            if ((time() >= strtotime($eventSetting->register_start) && time() <= strtotime($eventSetting->register_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'อยู่ในช่วงเวลาลงทะเบียน ไม่สามารถดำเนินการได้'
                ]);
            }

            if ((time() < strtotime($eventSetting->claim_reward_start) || time() > strtotime($eventSetting->claim_reward_end))) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่อยู่ในช่วงเวลารับของรางวัล ไม่สามารถดำเนินการได้'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_register == 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลงทะเบียนก่อนร่วมกิจกรรม'
                ]);
            }

            if($member->total_points <= 0 ){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณไม่ได้สะสมแต้มตามที่กำหนดไว้ ไม่สามารถรับของรางวัลได้'
                ]);
            }

            // get member color info
            $colorInfo = Color::where('color_id', $member->color_id)->first();
            if(!$colorInfo){
                return response()->json([
                    'status' => false,
                    'message' => 'No reward data(2).'
                ]);
            }

            // set package key by color rank
            $colorRewardCanClaim = false;
            $colorRewardClaimed = false;
            $colorRewardKey = 0;
            $colorRewardTitle = '';
            $colorRewardAmount = 0;
            $colorRewardRank = 0;

            $colorPackageKey = $this->setColorPackageByRank($colorInfo->color_rank);

            // check item log
            $colorRewardLog = ItemLog::where('uid', $member->uid)->where('package_type', 'color')->where('package_key', $colorPackageKey)->count();
            if($colorRewardLog > 0){
                $colorRewardClaimed = true;
            }

            $colorReward = Reward::where('package_type', 'color')->where('package_key', $colorPackageKey)->first();
            if($colorReward){

                if($colorRewardClaimed == false){
                    $colorRewardCanClaim = true;
                }
                
                $colorRewardKey = $colorReward->package_key;
                $colorRewardTitle = $colorReward->package_name;
                $colorRewardAmount = $colorReward->package_amount;
                $colorRewardRank = $colorInfo->color_rank;
            }

            
            
            if($colorRewardCanClaim == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย ไม่สามารถรับของรางวัลได้'
                ]);
            }

            if($colorRewardClaimed == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณได้รับของรางวัลนี้ไปแล้ว'
                ]);
            }

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // get reward from packge_key
            $rewards = Reward::where('package_type', 'color')->where('package_key', $colorPackageKey)->get();
            if(count($rewards) <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'no reward data.'
                ]);
            }

            // set reward info for check condition
            $packageName = $rewards[0]->package_name;
            $packageQuantity = $rewards[0]->package_quantity;
            $packageAmount = $rewards[0]->package_amount;
            $packageType = $rewards[0]->package_type;
            $packageRequirePoints = $this->setRequireRewardPoint($rewards[0]->package_key);

            // check claim log
            // $claimLog = ItemLog::where('uid', $member->uid)->where('package_key', $colorPackageKey)->where('package_type', $packageType)->count();
            // if($claimLog > 0){
            //     return response()->json([
            //         'status' => false,
            //         'message' => 'ขออภัย คุณได้รับของรางวัลนี้ไปแล้ว'
            //     ]);
            // }
            
            // check require points for exchange reward
            if($member->total_points < $packageRequirePoints){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย เหรียญสะสมของคุณไม่พอแลกของรางวัล'
                ]);
            }

            // set reward info
            $goods_data = []; //good data for send item group
            foreach ($rewards as $key => $reward) {

                $goods_data[] = [
                    'goods_id' => $reward->package_id,
                    'purchase_quantity' => $reward->package_quantity,
                    'purchase_amount' => 0,
                    'category_id' => 40,
                ];

            }

            $sendItemLog = $this->addItemHistoryLog([
                'uid'                           => $member->uid,
                'username'                      => $member->username,
                'ncid'                          => $member->ncid,
                'package_key'                   => $colorPackageKey,
                'package_name'                  => $packageName,
                'package_quantity'              => $packageQuantity,
                'package_amount'                => $packageAmount,
                'package_type'                  => $packageType,
                'send_item_status'              => false,
                'send_item_purchase_id'         => 0,
                'send_item_purchase_status'     => 0,
                'goods_data'                    => json_encode($goods_data),
                'status'                        => 'pending',
                'log_date'                      => (string)date('Y-m-d'), // set to string
                'log_date_timestamp'            => time(),
                'last_ip'                       => $this->getIP(),
            ]);

            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);

            // $send_result = (object)[
            //     "status" => true,
            //     "response" => (object)[
            //         "purchase_id" => "23857513",
            //         "purchase_status" => "4"
            //     ]
            // ];

            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                // update send item log
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ?: false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                    'status' => 'success'
                ], $sendItemLog['id'], $uid);

                $color = Color::where('color_id', $member->color_id)->first();

                return response()->json([
                    'status' => true,
                    'message' => "ได้รับ ".$packageName." <br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                    'data' => [
                        'status' => true,
                        "message" => 'Reward Info',
                        'open_claim_reward' => $this->setOpenClaimRewardPeriod(),
                        'open_competition' => $this->setOpenCompetitionPeriod(),
                        'username'=>$member->username,
                        'character_name'=>$member->char_name,
                        'selected_char'=>true,
                        'characters'=>[],
                        'is_register' => ($member->is_register == 1) ? true : false,
                        'color_title' => $color->color_title,
                        'color_id' => $color->color_id,
                        'color_npc' => $color->color_npc,
                        'color_rank' => $color->color_rank,
                        'group' => $member->group_id,
                        'rank' => $member->rank,
                        'rank_status' => $this->setMemberRankStatus($member->rank, $member->rank_previous),
                        'points' => (int)$member->total_points,
                        'color_max_score' => (int)$eventSetting->color_max_score,
                        'color_reward' => [
                            'can_claim' => false,
                            'claimed' => true,
                            'id' => $colorRewardKey,
                            'title' => $colorRewardTitle,
                            'amount' => $colorRewardAmount,
                            'rank' => $colorRewardRank,
                        ],
                    ]
                ]);

            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                        ], $sendItemLog['id'], $uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                            // 'data' => $eventInfo
                        ]);
            }
        }

    }