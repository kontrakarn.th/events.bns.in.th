<?php

    namespace App\Http\Controllers\Api\newbie_daily_login;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\newbie_daily_login\Product;
    use App\Models\newbie_daily_login\Product_group;
    use App\Models\newbie_daily_login\Productgrouplimit;
    use App\Models\newbie_daily_login\KeyPackage;
    use App\Models\newbie_daily_login\History;

    class HistoryController extends BnsEventController{
        private $userData = [];
        private $baseApi = 'http://api.apps.garena.in.th';
        private $startTime = '2018-12-14 00:00:00';
        private $endTime = '2018-12-20 23:59:59';
        private $startApiTime = '00:00:00';
        private $endApiTime = '23:59:59';
      /* public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            //$this->checkEventStatus();
        }*/
        public function getHistorybyuidCoupon($uid){
            $history = History::where('uid',$uid)->where('product_id',47)->get();
            if(count($history) > 0){
                return true;
            }else{
                return false;
            }
        }

        public function getHistorybyuidDay($uid){
            $today=Carbon::now()->toDateString();
            $history = History::where('uid',$uid)->where('product_id','!=',47)->whereDate('created_at','=',$today)->get();
            return $history;
        }

        public function getHistorybyuid($uid){
            $history = History::where('uid',$uid)->where('product_id','!=',47)->pluck('product_id')->toArray();
            $product = Product::whereIn('id',$history)->pluck('date_get_product')->toArray();
            $arr= [];
            $arr2= [];
            $product =array_unique($product);
            $product =array_values($product);

            for($i = 1;$i<= 21;$i++){
                $arr[] = [
                    "id" =>  $i,
                    "img"=> "items_".$i,
                    "hover" => "hover_".$i,
                    "is_checkin"=>false,
                    "can_checkin"=>false,
                ];

            }
            for($i2 = 0;$i2<count($product);$i2++){
                $number = $i2+1;
                $arr2[] =[
                     'id'=>  $number,
                     "img"=> "items_".$number,
                     "hover" => "hover_".$number,
                     "is_checkin"=>true,
                     "can_checkin"=>false,
                ] ;
            }
            $indayCanget = $this->getHistorybyuidDay($uid);
            if(count($indayCanget) >0){

            }else{
                $countarr2 =  count($arr2);
                $number = $countarr2+1;
                array_push($arr2,array("id" => $number,
                                        "img"=> "items_".$number,
                                        "hover" => "hover_".$number,
                                        "is_checkin"=>false,
                                        "can_checkin"=>true,
                                    ));
            }
            $array = array_replace($arr,$arr2);
            return $array;

        }
        public function countHistorybyUid($uid){
            $counthistory = History::where('uid',$uid)->get();
            if(count($counthistory < 1)){
                $counthistory = 0;
            }
            return $counthistory;
        }
        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip  = array_shift($ips);
            }
            return $ip;
        }
        public function GetHistory($uid){
            $history = History::with(['HistoryType','KeyPackage','Product'])->where("uid",$uid)->orderBy("id","DESC")->get();
            // /return $history;
            $data = [];
            foreach ($history as $hi) {
                //return $hi->product;
                unset($hi->id,$hi->created_at,$hi->updated_at,$hi->uid,$hi->product_key,$hi->HistoryType);
                //array_push($data,$hi);
                if($hi->history_type_id == 1){
                    array_push($data,array("product_name" => $hi->KeyPackage->th_name,"amount" =>$hi->KeyPackage->amount,"bonus" =>$hi->KeyPackage->bonus,"date" =>$hi->get_date_string,"time" =>$hi->get_time_string));
                }else{
                    array_push($data,array("product_name" => $hi->product->th_name,"amount" =>$hi->product->amount,"bonus" =>$hi->product->bonus,"date" =>$hi->get_date_string,"time" =>$hi->get_time_string));
                }
            }
            return response()->json([
                'status' => true,
                'data' => $data,
                'message' => '',
            ]);
        }
        public function SaveHistory($product_or_key_id,$history_type_id,$amount,$uid){
            date_default_timezone_set("Asia/Bangkok");

            $date = Carbon::now()->format("d-m-Y");
            $time = Carbon::now()->format("H:i:s");
            $save = New History;
            $save->history_type_id = $history_type_id;
            if($history_type_id == 1){
                $save->key_id = $product_or_key_id;
            }else{
                $save->product_id = $product_or_key_id;
            }
            $save->amount = $amount;
            $save->uid = $uid;
            $save->get_date_string = $date;
            $save->get_time_string = $time;
            $save->last_ip = $this->getIP();

            $save->save();
            return $save->id;
        }
        public function UpdateHistory($savehistoryid,$senditemstatus,$senditempurchaseid,$senditempurchasestatus,$status){
            date_default_timezone_set("Asia/Bangkok");
            $date = date("d/m/Y");
            $time = date("H:i:s");
            $update =  History::find($savehistoryid);
            $update->send_item_status = $senditemstatus;
            $update->send_item_purchase_id = $senditempurchaseid;
            $update->send_item_purchase_status = $senditempurchasestatus;
            $update->status = $status;
            $update->save();
        }
    }
