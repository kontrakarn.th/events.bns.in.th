<?php

    namespace App\Http\Controllers\Api\newbie_daily_login;

    use App\Http\Controllers\Api\BnsEventController;

    use App\Models\newbie_daily_login\Users;
    use App\Http\Controllers\Api\newbie_daily_login\CharactorController;
    use App\Models\bns_job\BnsJob;
    use App\Models\bns_settings\BnsSettings;
    use App\Models\bns_word\BnsWording;
    class UsersController extends BnsEventController{



        public function setEventInfo($uid,$message){
            $this->charactorcontroller= New CharactorController;
            $this->historycontroller= New HistoryController;

            $member = Users::where('uid', $uid)->first();

            if (!$member) {
                return response()->json([
                    'status' => false,
                    'message' => 'No member data.'
                ]);
            }
            $checkdayalreadyget =$this->historycontroller->getHistorybyuid($member->uid);
            $couponreward =$this->historycontroller->getHistorybyuidCoupon($member->uid);
            $Newcharactor =$this->charactorcontroller->getNewCharactorbyUidNcid($member->uid,$member->ncid);
            $content = [];
            if($member->user_type == 2 || count($Newcharactor)>0){
                if($member->user_type == 2 ){  // c30
                    $content = $this->charactorcontroller->getReturnCharactorbyUidNcid($member->uid,$member->ncid);
                }else  { // new
                    $content = $this->charactorcontroller->getNewCharactorAndCreationTime($member->uid,$member->ncid);
                }

                if (count($content) > 0 ) {
                    if ($member->char_id > 0) {
                        return [
                            'status' => true,
                            'message' => $message,

                            'data'=>[
                                        'username' => $member->username,
                                        'character_name' => $member->char_name,
                                        'can_get_reward' => true,
                                        'coupon_reward' => $couponreward,
                                        'checkin_list' => $checkdayalreadyget,
                                        'characters' => [],
                                    ]
                        ];
                }else{
                    return [
                        'status' => true,
                        'message' => $message,

                        'data'=>[
                                    'username' => $member->username,
                                    'character_name' => $member->char_name,
                                    'can_get_reward' => true,
                                    'coupon_reward' => $couponreward,
                                    'checkin_list' => $checkdayalreadyget,
                                    'characters' => $content,
                                ]
                    ];
                }
            }
            else{
                return [
                    'status' => true,
                    'message' => $message,
                    'data'=>[
                                'username' => $member->username,
                                'character_name' => $member->char_name,
                                'can_get_reward' => true,
                                'coupon_reward' => $couponreward,
                                'checkin_list' => $checkdayalreadyget,
                                'characters' => [],

                            ]
                        ];
                    }
            }

                $content = $this->charactorcontroller->getCharactorbyUidNcid($member->uid,$member->ncid);

                if (count($content) > 0 ) {
                    if ($member->char_id > 0) {
                        return [
                            'status' => true,
                            'message' => $message,
                            'data'=>[
                                        'username' => $member->username,
                                        'character_name' => $member->char_name,
                                        'can_get_reward' => true,
                                        'coupon_reward' => $couponreward,
                                        'checkin_list' => $checkdayalreadyget,
                                        'characters' => [],
                                    ]
                        ];

                }else{
                    return [
                        'status' => true,
                        'message' => $message,
                        'data'=>[
                                    'username' => $member->username,
                                    'character_name' => $member->char_name,
                                    'can_get_reward' => true,
                                    'coupon_reward' => $couponreward,
                                    'checkin_list' => $checkdayalreadyget,
                                    'characters' => $content,
                                ]
                    ];
                }
            }
                else{
                    return [
                        'status' => true,
                        //"message" => $message,
                        'data'=>[
                                    'username' => $member->username,
                                    'character_name' => $member->char_name,
                                    'can_get_reward' => false,
                                    'coupon_reward' => $couponreward,
                                    'checkin_list' => $checkdayalreadyget,
                                ]
                            ];
                }
        }



        public function getEventInfo($request,$userData)
        {
            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') { //check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            // get member info
            $member = Users::where('uid', $uid)->first();

            if (isset($member) && empty($member)) {
                return response()->json([
                    'status' => false,
                    'message' => 'No member data.'
                ]);
            }

            $message = BnsWording::where('type','no_condition')->value('wording');
            $message = "";
            return $this->setEventInfo($uid,$message);
        }

        public function getNcidByUid(int $uid): string
        {
            return Users::where('uid', $uid)->value('ncid');
        }

        }
