<?php

    namespace App\Http\Controllers\Api\newbie_daily_login;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;
    use App\Models\mystic\Setting;
    use App\Models\newbie_daily_login\Product;
    use App\Models\newbie_daily_login\Users;
    use App\Models\newbie_daily_login\History;

    use App\Models\bns_job\BnsJob;
    use App\Models\bns_settings\BnsSettings;
    use App\Models\bns_word\BnsWording;

    use App\Jobs\newbie_daily_login\SendItemToUserQueue;
    use App\Http\Controllers\Api\newbie_daily_login\CharactorController;
    use App\Http\Controllers\Api\newbie_daily_login\UsersController;
    use App\Http\Controllers\Api\newbie_daily_login\HistoryController;

    class ProductController extends BnsEventController{

        private $baseApi = 'http://api.apps.garena.in.th';


        public function getItem($request,$userData){
            $charactorcontroller = New CharactorController;
            $userscontroller = New UsersController;

            if(is_integer($request) == true){
                $date_to_get = $request;
            }else{
                $date_to_get = $request->date_number;
            }
            if($date_to_get == NULL || $date_to_get == 0){
                return response()->json([
                    'status' => false,
                    'can_get_reward' => false,
                    "message" => "No Date Selected",
                ]);
            }
            $uid      = $userData['uid'];
            $member = Users::where('uid', $uid)->first();
            $ncid = $userscontroller->getNcidByUid($member->uid);
            if($date_to_get == 100){
                $this->sendItem($ncid,$uid,$date_to_get);
                $message = BnsWording::where('type','send_item_success')->value('wording');
                return $userscontroller->setEventInfo($uid,$message);
            }else{
                if ($member->char_id == 0 ||$member->char_id == NULL || $member->char_id == '' ) {

                          return response()->json([
                              'status' => false,
                              'can_get_reward' => false,
                              "message" => BnsWording::where('type','no_charactor')->value('wording'),
                          ]);

                  }
                  if($member->level < 60 || $member->mastery_level < 15){
                     $this->charactorcontroller = New CharactorController;
                        $checkCharactorbycharid =$this->charactorcontroller->CheckCharactorTrue($member->uid,$member->ncid,$member->char_id);
                        if($checkCharactorbycharid == true){
                                $this->sendItem($ncid,$uid,$date_to_get);
                                $message = BnsWording::where('type','send_item_success')->value('wording');
                                return $userscontroller->setEventInfo($uid,$message);
                        }else{
                            return response()->json([
                                'status' => false,
                                "message" => BnsWording::where('type','no_charactor')->value('wording'),
                            ]);
                        }


                  }
                                $this->sendItem($ncid,$uid,$date_to_get);
                                $message = BnsWording::where('type','send_item_success')->value('wording');
                                return $userscontroller->setEventInfo($uid,$message);
            }


        }


        public function sendItem($ncid,$uid,$date_to_get){
            $historycontroller = New HistoryController;
            if($date_to_get == 3){

                $memberCharid = Users::where('uid',$uid)->value('job');
                $product = Product::where('specific_job_id',$memberCharid)->where('date_get_product',$date_to_get)->get();
                foreach($product as $p){
                    $goods_data = [];

                        $goods_data[] = [
                            'goods_id' => $p["product_id"],
                            'purchase_quantity' => 1,
                            'purchase_amount' => 0,
                            'category_id' => 40,
                        ];

                        $savehistoryid =   $historycontroller->saveHistory($p->id,2,1,$uid);
                        SendItemToUserQueue::dispatch($ncid, $goods_data,$savehistoryid,$historycontroller)->onQueue('bns_newbie_daily_login_senditem');


                 }

            }

                $product = Product::where('date_get_product',$date_to_get)->whereNull("specific_job_id")->get();
                foreach($product as $p){

                $goods_data = [];
                $goods_data[] = [
                    'goods_id' => $p["product_id"],
                    'purchase_quantity' => 1,
                    'purchase_amount' => 0,
                    'category_id' => 40,
                ];
                $producthis = Product::where('product_id',$p->product_id)->pluck('id')->toArray();
                $checkHistory = History::where('uid',$uid)->whereIn('product_id',$producthis)->get();
                if(count($checkHistory) > 0){

                }else{
                    $savehistoryid =   $historycontroller->saveHistory($p->id,2,1,$uid);
                    SendItemToUserQueue::dispatch($ncid, $goods_data,$savehistoryid,$historycontroller)->onQueue('bns_newbie_daily_login_senditem');
                }


             }

    }
    }
