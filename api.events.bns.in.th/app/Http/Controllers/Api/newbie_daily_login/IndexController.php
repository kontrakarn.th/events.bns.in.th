<?php

    namespace App\Http\Controllers\Api\newbie_daily_login;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;
    use App\Models\mystic\Setting;
    use App\Models\newbie_daily_login\Product;
    use App\Models\newbie_daily_login\Users;
    use App\Models\bns_job\BnsJob;
    use App\Models\bns_settings\BnsSettings;
    use App\Models\bns_word\BnsWording;

    use App\Http\Controllers\Api\newbie_daily_login\UsersController;
    use App\Http\Controllers\Api\newbie_daily_login\HistoryController;
    use App\Http\Controllers\Api\newbie_daily_login\CharactorController;
    use App\Http\Controllers\Api\newbie_daily_login\ProductController;

    use App\Jobs\newbie_daily_login\SendItemToUserQueue;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2018-12-14 00:00:00';
        private $endTime = '2018-12-20 23:59:59';

        private $startApiTime = '00:00:00';
        private $endApiTime = '23:59:59';

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);
            $this->checkEventStatus();
            $this->userscontroller = New UsersController;
            $this->historycontroller = New HistoryController;
            $this->charactorcontroller = New CharactorController;
            $this->productcontroller = New ProductController;

        }

        public function GetHistory(Request $request){
            return $this->historycontroller->GetHistory($this->userData["uid"]);
        }

        private function getRegisterStatus($member)
        {
            if (is_null($member->code)) {
                return 'can_register';
            }

            if (!is_null($member->code) && is_null($member->duo_uid)) {
                return 'not_completed_register';
            }

            if (!is_null($member->code) && !is_null($member->duo_uid)) {
                return 'completed_register';
            }
        }

        public function selectCharacter(Request $request)
        {
            return $charactor = $this->charactorcontroller->getCharactor($request,$this->userData,0);
        }

        public function setEventInfo($uid)
        {
            return $this->userscontroller->setEventInfo($uid);
        }

        public function getEventInfo(Request $request)
        {
            return  $this->userscontroller->getEventInfo($request,$this->userData);
        }

        private function getEventSetting()
        {
            return BnsSettings::where('event_name','newbie_daily_login')->where('active', 1)->first();
        }
        private function checkEventStatus()
        {
            if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
                $typename = 'no_permission';
                die(json_encode([
                    'status' => false,
                    'type' => $typename,
                    'message' => $this->getWording($typename)
                ]));
            }
            // get events setting info
            $eventSetting = $this->getEventSetting();

            if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
                $typename = 'end_event';
                die(json_encode([
                    'status' => false,
                    'type' => $typename,
                    'message' => $this->getWording($typename)
                ]));
            }

            if ($this->checkIsMaintenance()) {
                $typename = 'maintenance';
                die(json_encode([
                    'status' => false,
                    'type' => $typename,
                    'message' => $this->getWording($typename)
                ]));
            }

            if ($this->isLoggedIn()) {
                // check and create member
                if ($this->checkMember() == false) {
                    $typename = 'cant_create_member';
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => $this->getWording($typename)
                        ]));
                }
            } else {
                $typename = 'not_login';
                die(json_encode([
                    'status' => false,
                    'type' => $typename,
                    'message' => $this->getWording($typename)
                ]));
            }
        }

        private function getIP()
        {
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip  = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData) {
                return true;
            } else {
                return false;
            }
        }

        private function is_accepted_ip()
        {
            $allow_ips   = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid      = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $arr = [
                    'uid'              => $uid,
                    'username'         => $username,
                    'ncid'             => $ncid,
                    'char_id'          => 0,
                    'char_name'        => '',
                    'world_id'         => 0,
                    'total_points'     => 0,
                    'used_points'      => 0,
                    'total_tokens'     => 0,
                    'used_tokens'      => 0,
                    'can_get_package'   =>  1, //Can = 1
                    'already_purchased' => 0,
                    'last_ip'          => $this->getIP(),
                    'user_type'          => 1,

                ];

                $resp = $this->createMember($arr);

                if (isset($resp['id']) == false) {

                    return false;
                }
                $this->userEvent = $resp;
            } else {
                $member = Users::where('uid', $uid)->first();
                if (empty($member->ncid)) {
                    $ncid = $this->associateGarenaWithNc($uid);
                    if (empty($ncid)) {
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid,'username' => $username], $member->id);
                }
                $this->userEvent = $member;
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Users::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Users::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if (empty($_id)) {
                return false;
            }

            return Users::where('id', $_id)->update($arr);
        }



        private function associateGarenaWithNc(int $uid): string
        {
            return Cache::remember('BNS:MYSTIC:LIVE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'user_info',
                    'uid'      => $uid,
                ]);
                $userNcInfo       = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }
            });
        }


        public function getItem(Request $request){
            return $this->productcontroller->getItem($request,
                                                     $this->userData
                                                    );
        }
        public function saveCharactor(Request $request){
            return $this->charactorcontroller->saveCharactor($request,
                                                     $this->userData
                                                    );
        }
        public function getWording($typename){
            return BnsWording::where('type',$typename)->value('wording');
        }

    }
