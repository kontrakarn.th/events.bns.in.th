<?php

    namespace App\Http\Controllers\Api\season2_revival;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DB;

    use App\Models\season2_revival\Member;
    use App\Models\season2_revival\Reward;
    use App\Models\season2_revival\LastLoginList;
    use App\Models\season2_revival\ItemLog;

    class IndexController extends BnsEventController{

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2018-11-21 00:00:00';
        private $endTime = '2018-12-12 23:59:59';

        private $startGetApiTime = '00:00:00';
        private $endGetApiTime = '23:59:59';

        private $awtSecret = 'LUtTEDS5M3P37RAwQZvQkFZvtwDXcrDY';

        public function __construct()
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getUserData();

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {

            if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            $this->userData = $this->getUserData();
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:SEASON2_REVIVAL:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData) {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Sending item from garena events bns season 2 new user and revival user.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addSendItemLog(array $arr) {
            if ($arr) {
                $resp = ItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateSendItemLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        public function getEventInfo(Request $request)
        {
            $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            if (is_null($decoded)) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }
            $decoded = collect($decoded);

            if ($decoded->has('type') == false && $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
            ]);

        }

        private function setEventInfo($uid){

            if(empty($uid)){
                return false;
            }

            $can_receive = false;
            $received = false;

            $member = Member::where('uid', $uid)->first();

            // check user can receive reward
            $checkInList = LastLoginList::where('uid', $member->uid)->count();

            if($checkInList > 0){
                $can_receive = true;
            }

            // check get item history
            $itemHistory = ItemLog::where('uid', $member->uid)->count();

            if($itemHistory > 0){
                $received = true;
            }

            return [
                'can_receive' => $can_receive,
                'received' => $received
            ];

        }

        public function redeemReward(Request $request) {
            $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            if (is_null($decoded)) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }
            $decoded = collect($decoded);

            if ($decoded->has('type') == false && $decoded['type'] != 'redeem_reward') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }

            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);

            $eventInfo = $this->setEventInfo($uid);

            if($eventInfo['can_receive'] == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไอดีของคุณไม่ตรงเงื่อนไข<br />ไม่สามารถรับของรางวัลได้'
                ]);
            }

            if($eventInfo['received'] == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปก่อนหน้านี้แล้ว'
                ]);
            }

            // set reward
            $rewards = new Reward;
            $rewardInfo = $rewards->setReward();

            $goods_data = [];
            $packageId = $rewardInfo['package_id'];
            $packageName = $rewardInfo['package_title'];

            if(count($rewardInfo['product_set']) > 0){
                foreach($rewardInfo['product_set'] as $product){
                    $goods_data[] = [
                        'goods_id' => $product['product_id'],
                        'purchase_quantity' => $product['product_quantity'],
                        'purchase_amount' => 0,
                        'category_id' => 40
                    ];
                }
            }

            
            $itemLog = $this->addSendItemLog([
                'uid' => $uid,
                'username' => $username,
                'ncid' => $ncid,
                'log_date' => date('Y-m-d H:i:s'),
                'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                'package_id' => $rewardInfo['package_id'],
                'package_title' => $rewardInfo['package_title'],
                'status' => 'pending',
                'send_item_status' => false,
                'send_item_purchase_id' => '0',
                'send_item_purchase_status' => '0',
                'goods_data' => json_encode($goods_data),
                'last_ip' => $this->getIP()
            ]);

            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                    'status' => 'success'
                ], $itemLog['id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                    'status' => true,
                    'message' => 'ส่งของรางวัลเสร็จเรียบร้อย<br />กรุณาตรวจสอบกล่องจดหมายภายในเกม',
                    'content' => $eventInfo
                ]);

            }else{
                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                    'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                    'status' => 'unsuccess'
                ], $itemLog['id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งของรางวัลได้ กรุณาติดต่อฝ่ายบริการลูกค้า',
                    'content' => $eventInfo
                ]);
            }
        }

    }
