<?php

    namespace App\Http\Controllers\Api\topup_may2020;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\topup_may2020\Member;
    use App\Models\topup_may2020\ItemLog;
    use App\Models\topup_may2020\Reward;
    use App\Models\topup_may2020\Setting;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            if($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย<br />ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){

                // check member has game info
                $uid = $this->userData['uid'];
                $username = $this->userData['username'];
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    die(json_encode([
                        'status' => false,
                        'type' => 'no_game_info',
                        'message' => 'ขออภัย<br />กรุณาล็อคอินเข้าเกมเพื่อสร้างตัวละคร<br />ก่อนร่วมกิจกรรม'
                    ]));
                }

                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาด<br />ไม่สามารถบักทึกข้อมูลของคุณได้<br />กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        private function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:TOPUPMAY2020:LIVE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function apiSendItem(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name'              => 'bns',
                'service'               => 'send_item',
                'user_id'               => $ncid,
                'purchase_description'  => 'Sending item from garena events BNS Topup Promotion x2 Bonus Diamonds.',
                'goods'                 => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function checkItemHistory($uid,$package_key){
            return ItemLog::where('uid', $uid)
                        ->where('package_key', $package_key)
                        ->count();
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
        }
        
        private function getEventSetting(){
            return Setting::where('active', 1)->first();
        }

        private function doGetTermgameDiamondHistory($uid) {
            if(empty($uid)){
                return false;
            }
            // Cache::forget('BNS:TOPUP_NOVEMBER2018:TERMGAME_HISTORY_' . $uid);
            return Cache::remember('BNS:TOPUPMAY2020:LIVE:TERMGAME_HISTORY_' . $uid, 5, function() use($uid) {
                return json_decode($this->post_api($this->baseApi, [
                            'key_name' => 'gws_test',
                            'service' => 'get_user_topup',
                            'uid' => $uid,
                            'start_ts' => strtotime($this->diamondStartTime),
                            'end_ts' => strtotime($this->diamondEndTime),
                            'app_id' => $this->bnsAppId
                        ]));
            });
        }

        private function doGetDiamondHistory($uid,$ncid,$start,$end) {
            if(empty($ncid) || empty($ncid) || empty($start) || empty($end)){
                return false;
            }
            // Cache::forget('BNS:TOPUP_NOVEMBER2018:EXCHANGE_TO_HISTORY_' . $ncid);
            return Cache::remember('BNS:TOPUPMAY2020:LIVE:EXCHANGE_TO_HISTORY_' . $uid, 5, function() use($ncid,$start,$end) {
                return json_decode($this->post_api($this->baseApi, [
                            'key_name' => 'bns',
                            'service' => 'query_cash_txn',
                            'user_id' => $ncid,//ncid
                            'start_time' => $start,
                            'end_time' => $end
                        ]));
            });
        }

        private function setTopupHistory($uid,$ncid,$start,$end){
            $collectionDiamond = null;
            $resultDiamond = $this->doGetDiamondHistory($uid,$ncid,$start,$end);
            if ($resultDiamond->status == true) {
                $contentDiamond = $resultDiamond->response;
                if (count($contentDiamond) > 0) {
                    $collectionDiamond = collect($contentDiamond);
                }
            }

            return $collectionDiamond;
        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return false;
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            $member = Member::where('uid', $uid)->first();

            $collectionDiamond = $this->setTopupHistory($member->uid,$member->ncid,$eventSetting->topup_start,$eventSetting->topup_end);

            // get reward list
            $rewardList = [];
            $rewards = Reward::orderBy('package_key','ASC')->get();
            if(count($rewards) > 0){
                foreach($rewards as $reward){

                    $canClaim = false;
                    $hasClaimed = false;

                    $hasRequireCount = 0;
                    if(!empty($collectionDiamond)){
                        // $hasRequireCount = $collectionDiamond->whereIn('diamond', unserialize($reward->diamonds_require))->count();
                        $hasRequireCount = $collectionDiamond->where('diamond', '>=', $reward->topup_require)->count();
                    }
                   
                    if($hasRequireCount > 0 && $member->already_claimed == 0){
                        $canClaim = true;
                    }

                    if($member->already_claimed == 1 && $member->selected_package == $reward->package_key){
                        $hasClaimed = true;
                    }

                    $remainRights = 0;
                    $remainRights = $reward->total_rights - $reward->used_rights;

                    $rewardList[] = [
                        'id' => $reward->package_key,
                        'title' => $reward->package_name,
                        'topup_require' => number_format($reward->topup_require),
                        'topup_bonus' => number_format($reward->topup_bonus),
                        'remain_rights' => number_format($remainRights),
                        'status' => $this->setClaimRewardStatus($canClaim,$hasClaimed,$remainRights),
                    ];

                }
            }

            return [
                'username' => $member->username,
                'already_claimed' => $member->already_claimed == 1 ? true : false,
                'selected_package'  => $member->selected_package,
                'packages' => $rewardList,
            ];

        }

        private function setClaimRewardStatus($canClaim=false,$hasClaimed=false,$remainRights=0){
            if($hasClaimed == true){
                return 2; // รับไปแล้ว
            }
            if($remainRights <= 0){
                return 3; // สิทธิ์หมดแล้ว
            }
            if($canClaim == true && $hasClaimed == false){
                return 1; // รับโบนัสไดมอนด์
            }

            return 0;
        }

        public function getEventInfo(Request $request)
        {

            if ($request->has('type') == false && $request->type != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'data' => $eventInfo,
            ]);

        }

        public function getClaimReward(Request $request) {

            if ($request->has('type') == false && $request->type != 'claim_reward') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }

            if ($request->has('id') == false || !in_array($request->id, [1,2,3])) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (2)!'
                ]);
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            $userData = $this->userData;
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);
            $package_key = $request->id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->already_claimed == 1){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปแล้ว'
                ]);
            }

            $rewardInfo = Reward::where('package_key', $package_key)->first();
            if(!$rewardInfo){
                return response()->json([
                    'status' => false,
                    'message' => 'No reward data required.'
                ]);
            }

            $collectionDiamond =  $this->setTopupHistory($member->uid,$member->ncid,$eventSetting->topup_start,$eventSetting->topup_end);

            $remainRights = 0;
            $remainRights = $rewardInfo->total_rights - $rewardInfo->used_rights;

            $canClaim = false;
            $hasClaimed = false;

            $hasRequireCount = 0;
            if(!empty($collectionDiamond)){
                // $hasRequireCount = $collectionDiamond->whereIn('diamond', unserialize($rewardInfo->diamonds_require))->count();
                $hasRequireCount = $collectionDiamond->where('diamond', '>=', $rewardInfo->topup_require)->count();
            }
            
            if($hasRequireCount > 0 && $member->already_claimed == 0){
                $canClaim = true;
            }

            if($member->already_claimed == 1 && $member->selected_package == $rewardInfo->package_key){
                $hasClaimed = true;
            }

            $claimStatus = $this->setClaimRewardStatus($canClaim,$hasClaimed,$remainRights);

            // user can claim topup promotion when claim status = 1
            if($claimStatus == 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่ตรงเงื่อนไขในการรับของรางวัล'
                ]);
            }elseif($claimStatus == 2){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้รับสิทธิ์ไปก่อนหน้านี้แล้ว'
                ]);
            }elseif($claimStatus == 3){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />สิทธิ์รับฟรีโบนัสไดมอนด์ x2 ('.$rewardInfo->package_name.') หมดแล้ว'
                ]);
            }

            /* $hasRequireCount = 0;
            if(!empty($collectionDiamond)){
                $hasRequireCount = $collectionDiamond->where('diamond', $rewardInfo->topup_require)->count();
            }
            if($hasRequireCount <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่ตรงเงื่อนไขในการรับของรางวัล'
                ]);
            } */

            // update member already claimed
            $member->already_claimed = 1;
            $member->selected_package = $rewardInfo->package_key;
            $member->save();
            $member->fresh();

            // update reward used rights
            $rewardInfo->used_rights =  $rewardInfo->used_rights + 1;
            $rewardInfo->save();
            $rewardInfo->fresh();

            // set send item logs
            $goods_data = []; //good data for send item group
            $goods_data[] = [
                'goods_id' => $rewardInfo->package_id,
                'purchase_quantity' => $rewardInfo->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40
            ];

            $sendItemLog = $this->addItemHistoryLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'package_key' => $rewardInfo->package_key,
                'package_id' => $rewardInfo->package_id,
                'package_name' => $rewardInfo->package_name,
                'package_quantity' => $rewardInfo->package_quantity,
                'package_amount' => $rewardInfo->package_amount,
                'image' => $rewardInfo->image,
                'status' => 'pending',
                'send_item_status' => false,
                'send_item_purchase_id' => 0,
                'send_item_purchase_status' => 0,
                'goods_data' => json_encode($goods_data),
                'last_ip' => $this->getIP(),
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
            ]);

            $send_result_raw = $this->apiSendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ?: false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                    'status' => 'success'
                ], $sendItemLog['id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                            'status' => true,
                            'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                            'data' => $eventInfo
                        ]);
            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                ], $sendItemLog['id'], $uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                        ]);
            }


        }

    }