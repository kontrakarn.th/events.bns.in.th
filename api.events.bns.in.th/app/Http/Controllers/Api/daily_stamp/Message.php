<?php

namespace App\Http\Controllers\Api\daily_stamp;

use App\Http\Controllers\Api\daily_stamp\Event;

class Message extends Event
{
    protected const MESSAGE = [
        'th' => [
            'init'       => [
                'end_event'         => 'หมดเวลากิจกรรม',
                'end_event_buyskin' => 'หมดเวลาซื้อสกิน',
                'not_time_event'    => 'ยังไม่ถึงระยะเวลากิจกรรม',
                'not_login'         => 'กรุณา Login ก่อนจึงสามารถเข้าร่วมกิจกรรมได้',
                'not_character'     => 'คุณไม่มีตัวละครภายในเกม',
                'not_data_user'     => 'ไม่พบข้อมูลผู้เล่น กรุณาลองใหม่อีกครั้ง',
                'not_balance'       => 'ไม่พบข้อมูล RP/IP ภายในเกม กรุณาลองใหม่อีกครั้ง',
            ],
            'buy_bundle' => [
                'timeout'              => 'หมดเวลาเข้าร่วมกิจกรรม',
                'not_bundle'           => 'ไม่มีสกินในบันเดิล กรุณาลองใหม่อีกครั้ง',
                'not_myself'           => 'ขออภัยค่ะ คุณไม่สามารถส่งของขวัญให้ตัวเองได้',
                'friend_not_character' => 'เพื่อนของคุณไม่มีตัวละครในเกม LoL',
                'over_limit'           => 'คุณซื้อครบจำนวนครั้งที่กำหนด',
                'not_champion'         => 'คุณไม่มีแชมเปี้ยน ไม่สามารถซื้อได้',
                'have_skin'            => 'คุณมีสกินนี้อยู่แล้ว ไม่สามารถซื้อได้',
                'friend_not_champion'  => 'เพื่อนของคุณไม่มีแชมเปี้ยน ไม่สามารถซื้อได้',
                'friend_have_skin'     => 'เพื่อนของคุณมีสกินนี้อยู่แล้ว ไม่สามารถซื้อได้',
                'success'              => 'ยินดีด้วยคุณได้รับ ',
                'not_data_user_friend' => 'ไม่พบข้อมูลเพื่อนของคุณ กรุณาลองใหม่อีกครั้ง',
                'not_balance_friend'   => 'ไม่พบข้อมูล RP/IP ภายในเกมเพื่อนของคุณ กรุณาลองใหม่อีกครั้ง',
                'not_rp'               => 'คุณมีี RP ไม่เพียงพอในการซื้อ',
            ],
            'error'      => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง',
            'checkin'    => [
                'cant'    => 'คุณสามารถเช็คอินได้เพียงครั้งเดียวเท่านั้น',
                'success' => 'คุณได้เพิ่มพลังแห่งรักเรียบร้อยแล้ว',
            ],
            'redeem'     => [
                'cant'    => 'ไม่สามารถแลก %s ได้เนื่องจากหัวใจไม่พอ',
                'success' => 'แลก %s เรียบร้อยแล้ว',
                'fail'    => 'ไม่สามารถแลก %s ได้ กรุณาลองใหม่อีกครั้ง',
            ],
        ],
        'en' => [
            'init'       => [
                'not_time_event' => 'The event has not started yet.',
                'not_login'      => 'Please login before participating in the event.',
                'not_character'  => "You don't own this character in the game.",
                'not_data_user'  => 'User data not found. Please try again.',
                'not_balance'    => 'RP/BE data not found in game. Please try again.',
            ],
            'buy_bundle' => [
                'timeout'              => 'Event has finished.',
                'not_bundle'           => "There's no skin in the bundle. Please try again.",
                'not_myself'           => 'Sorry, you cannot send a gift to yourself.',
                'friend_not_character' => 'Your friend does not own this character.',
                'over_limit'           => 'You have reached the purchase limit.',
                'not_champion'         => "You don't own the champion. Unable to purchase.",
                'have_skin'            => 'You already own this skin. Unable to purchase.',
                'friend_not_champion'  => 'Your friend does not own the champion. Unable to purchase',
                'friend_have_skin'     => 'Your friend already own this skin. Unable to purchase',
                'success'              => 'Congratulations, you got ',
                'not_data_user_friend' => "Your friend's user data is not found. Please try again.",
                'not_balance_friend'   => "Your friend's RP/BE data is not found in the game. Please try again.",
                'not_rp'               => "You don't have enough RP to buy the skin",
            ],
            'error'      => "There's an error. Please try again.",
        ],
    ];


}
