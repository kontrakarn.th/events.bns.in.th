<?php

namespace App\Http\Controllers\Api\daily_stamp;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Cache;
use Validator;
use Storage;

use App\Models\daily_stamp\Events            as DB_EVENT;
use App\Models\daily_stamp\Member;
use App\Models\daily_stamp\MemberQuest;
use App\Models\daily_stamp\ItemHistory;
use App\Models\daily_stamp\SendItemLog;
use App\Models\daily_stamp\DeductLog;
use App\Models\daily_stamp\Quest;
use App\Models\daily_stamp\QuestDailyLog;
use App\Models\daily_stamp\Reward;

use App\Models\daily_stamp\Utils as Utils;

class Event extends BnsEventController
{
    protected function getEvent($collection, $eventName)
    {
        $event = $collection::where('eventname', $eventName)
            ->where('status', 'active')
            ->first();

        if (isset($event) === false) {
            return [
                'status' => false,
                'error' => 0,
            ];
        }

        $data = [
            'event' => $event
        ];

        return [
            'status' => true,
            'data' => $data
        ];
    }

    protected function doGetChar($uid, $ncid){//new fixed function
        $resp_raw = $this->apiCharacters($uid, $ncid);

        $resp = json_decode($resp_raw);
        if (is_object($resp)) {
            if ($resp->status) {
                $content = [];
                $i       = 1;
                foreach ($resp->response as $key => $value) {
                    // if((int)$value->level >= 60 && (int)$value->mastery_level>=12){
                        $content[] = [
                            'id' => $i,
                            'char_id'   => $value->id,
                            'char_name' => $value->name,
                            'world_id' => $value->world_id,
                            'job' => $value->job,
                            'level' => $value->level,
                            'creation_time' => $value->creation_time,
                            'last_play_start' => $value->last_play_start,
                            'last_play_end' => $value->last_play_end,
                            'last_ip' => $value->last_ip,
                            'exp' => $value->exp,
                            'mastery_level' => $value->mastery_level,
                        ];
                    // }

                    $i++;
                }
                return $content;
            }
        }
        return null;
    }

    private function apiCharacters($uid, $ncid)
    {
        return Cache::remember('BNS:DAILY_STAMP:CHARACTERS_'.$uid, 10, function () use ($ncid) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'characters',
                'user_id'  => $ncid,
            ]);
        });
    }

    protected function setRemainDiamonds($uid){
        $diamondsBalanceRaw = $this->diamondBalance($uid);
        $diamondsBalance = json_decode($diamondsBalanceRaw);

        if ($diamondsBalance->status == false) {
            return 0;
        }else{
            return intval($diamondsBalance->balance);
        }
    }

    protected function diamondBalance(int $uid) {
        return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'balance',
                    'uid' => $uid,
        ]);
    }

    protected function getIP() {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    protected function is_accepted_ip() {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    protected function isLoggedIn() {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function associateGarenaWithNc(int $uid, string $username): string {
        return Cache::remember('BNS:DAILY_STAMP:ASSOCIATE_UID_WITH_NCID_'.$uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'user_info',
                'uid'      => $uid,
            ]);
            $userNcInfo = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }

        });
    }

    protected function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];

        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'              => $uid,
                'username'         => $username,
                'ncid'             => $ncid,
                'char_id'          => 0,
                'char_name'        => '',
                'world_id'         => 0,
                'last_ip'          => $this->getIP(),
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
            $this->userEvent = $resp;

        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    private function hasMember(int $uid) {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr) {
        return Member::create($arr);
    }

    private function updateMember($arr, $_id) {
        if (empty($_id)) {
            return false;
        }

        return Member::where('id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid) {
        return Member::where('ncid', $ncid)->value('uid');
    }

    protected function setDiamondDeductData($deduct_diamond_amount = 0) {
        if($deduct_diamond_amount==0)
            return false;

        return [
            [
                'goods_id' => 741,
                'purchase_quantity' => $deduct_diamond_amount,
                'purchase_amount' => $deduct_diamond_amount,
                'category_id' => 40
            ]
        ];
    }

    protected function deductDiamond(string $ncid, array $goodsData) {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Test deduct diamomds for garena BNS Piggy event.',
            'currency_group_id' => 71,
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    protected function addDeductLog(array $arr) {
        if ($arr) {
            $resp = DeductLog::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return false; //fix
            }
        }
        return null;
    }

    protected function updateDeductLog(array $arr, $logId) {
        if ($arr) {
            return DeductLog::where('id', $logId)->update($arr);
        }
        return null;
    }

    protected function dosendItem(string $ncid, array $goodsData) {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Test sending item from garena events BNS Daily Stamp event.',
            'goods'                => json_encode($goodsData),
        ];
        return $this->post_api($this->baseApi, $data);
    }

    protected function addItemHistoryLog($arr) {
        return ItemHistory::create($arr);
    }

    protected function updateItemHistoryLog($arr, $log_id, $uid) {
        return ItemHistory::where('id', $log_id)->where('uid', $uid)->update($arr);
    }

    protected function addSendItemLog($arr) {
        return SendItemLog::create($arr);
    }

    protected function updateSendItemLog($arr, $log_id, $uid) {
        return SendItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
    }

    protected function responseAll($member, $message) {
        $questList = Quest::orderBy('id', 'ASC')->get();

        if ($questList->count() == 0) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
            ]);
        }

        $memberQuest = MemberQuest::where('uid', $member->uid)
            ->where(function($query) {
                $query->where('status', 'pending')
                      ->orWhere('status', 'not_success')
                      ->orWhere('status', 'success');
            })
            ->orderBy('quest_index', 'ASC')
            ->get();

        // Stamp
        $total_stamp = 0;

        foreach ($memberQuest as $value) {
            if ($value->status == 'success') {
                $total_stamp++;
            }
        }

        if ($total_stamp > $member->total_stamp) {
            $member->total_stamp = $total_stamp;
            $member->save();
        }

        $itemLogs = SendItemLog::where('uid', $member->uid)
          ->where('type', 'claim_stamp')
          ->get();

        $stamp = ['not_success', 'not_success', 'not_success', 'not_success', 'not_success', 'not_success'];

        foreach ($itemLogs as $value) {
            for ($i = 0; $i < count(Utils::ARRAY_STAMP); $i++) {
                if ($value->name == Utils::ARRAY_STAMP[$i]['message']) {
                    $stamp[$i] = 'success';
                    break;
                }
            }
        }

        for ($i = 0; $i < count(Utils::ARRAY_STAMP); $i++) {
            if ($member->total_stamp >= Utils::ARRAY_STAMP[$i]['stamp']) {
                if ($stamp[$i] == 'not_success') {
                    $stamp[$i] = 'pending';
                }
            }
        }

        //

        $quests = [];

        // $indexEnd = $memberQuest[0]->quest_index - 1;
        if ($memberQuest->count() > 0) {
            $indexEnd = $memberQuest[$memberQuest->count() - 1]->quest_index;

            for ($i = 0; $i < $indexEnd; $i++) {
                $subQuest = [];
                $questDaily = Utils::QUEST_DETAIL[Utils::ARRAY_DATE[$i]];

                for ($j = 0; $j < count($questDaily['quest']); $j++) {
                    $subQuest[] = [
                        'quest_id' => intval($questList[$questDaily['quest'][$j]]->quest_id),
                        'quest_dungeon' => $questList[$questDaily['quest'][$j]]->quest_dungeon,
                        'quest_title' => $questList[$questDaily['quest'][$j]]->quest_title,
                        'amount' => intval($questList[$questDaily['quest'][$j]]->quest_amount),
                        'status' => 'pending',
                        'total_quest' => 0,
                    ];
                }

                $quests[] = [
                    'quest_id' => intval($questDaily['index']),  //intval($questList[$i]->quest_id),
                    'sub_quest' => $subQuest,
                    'status' => 'not_success',
                    'claim_free_status' => 'pending',
                    'claim_diamond_status' => 'pending',
                ];
            }
        }

        $questDailyLog = QuestDailyLog::where('uid', $member->uid)
          ->where(function($query) {
              $query->where('status', 'pending')
                    ->orWhere('status', 'success');
          })
          ->orderBy('quest_id', 'ASC')
          ->get();

        foreach ($memberQuest as $questVal) {
            $subQuest = [];
            $questDaily = Utils::QUEST_DETAIL[Utils::ARRAY_DATE[$questVal->quest_index - 1]];

            for ($j = 0; $j < $questDailyLog->count(); $j++) {
                if ($questDailyLog[$j]->log_date == Utils::ARRAY_DATE[$questVal->quest_index - 1]) {
                    $subQuest[] = [
                        'quest_id' => intval($questDailyLog[$j]->quest_id),
                        'quest_dungeon' => $questDailyLog[$j]->quest_dungeon,
                        'quest_title' => $questDailyLog[$j]->quest_title,
                        'amount' => intval($questDailyLog[$j]->quest_amount),
                        'status' => $questDailyLog[$j]->status,
                        'total_quest' => $questDailyLog[$j]->total_quest,
                    ];
                }
            }

            $quests[intval($questVal->quest_index) - 1] = [
                'quest_id' => intval($questVal->quest_index),
                'sub_quest' => $subQuest,
                'status' => $questVal->status,
                'claim_free_status' => $questVal->claim_free_status,
                'claim_diamond_status' => $questVal->claim_diamond_status,
            ];
        }

        return [
            'status' => true,
            'data' => [
                'status' => true,
                "message" => 'Event Info',
                'username' => $member->username,
                'character_name' => $member->char_name,
                'selected_char' => true,
                'quests' => $quests,
                'stamp' => $stamp
            ],
            'message' => $message
        ];
    }

}
