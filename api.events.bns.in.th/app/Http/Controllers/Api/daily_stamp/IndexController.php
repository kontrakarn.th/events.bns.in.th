<?php

    namespace App\Http\Controllers\Api\daily_stamp;

    use App\Http\Controllers\Api\daily_stamp\Message;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\daily_stamp\Events            as DB_EVENT;
    use App\Models\daily_stamp\Member;
    use App\Models\daily_stamp\MemberQuest;
    use App\Models\daily_stamp\ItemHistory;
    use App\Models\daily_stamp\SendItemLog;
    use App\Models\daily_stamp\DeductLog;
    use App\Models\daily_stamp\Quest;
    use App\Models\daily_stamp\QuestDailyLog;
    use App\Models\daily_stamp\Reward;

    use App\Models\daily_stamp\Utils as Utils;

    class IndexController extends Message {

        protected $baseApi = 'http://api.apps.garena.in.th';
        // private $startTime = '2020-02-07 00:00:00';
        // private $endTime = '2020-03-20 23:59:59';
        //
        // private $userEvent = null;
        //
        // private $maxDiamondsPerRound = 37500;
        // private $maxLootboxesPerRound = 50;
        //
        // private $diamondsDailyLimit = 7500;
        // private $serviceCharge = 5000;
        //
        // private $diamonds_2000 = 2000;
        // private $diamonds_1000 = 1000;
        // private $diamonds_500 = 500;

        private $event;

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->event = $this->getEvent(new DB_EVENT, Utils::KEY_EVENT);

            if ($this->event['status'] === false) {
                header('Content-bgg: application/json');
                die(json_encode([
                    'status'  => false,
                    'error'   => 0,
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม',
                ]));
            }

            // init user data
            $this->userData = $this->getJwtUserData($request);
            $this->checkEventStatus();

        }

        private function checkEventStatus()
        {
            if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'no_permission',
                    'message' => 'Sorry, you do not have permission.',
                ]));
            }

            if ((time() < strtotime($this->event['data']['event']['topup_start_datetime']) || time() > strtotime($this->event['data']['event']['topup_end_datetime']))) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม',
                    'topup_start_datetime' => $this->event['data']['event']['topup_start_datetime'],
                    'topup_end_datetime' => $this->event['data']['event']['topup_end_datetime'],
                ]));
            }

            if ($this->checkIsMaintenance()) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์',
                ]));
            }

            if ($this->isLoggedIn()) {
                // check and create member
                if ($this->checkMember() == false) {
                    die(json_encode([
                        'status'  => false,
                        'type'    => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง',
                    ]));
                }
            } else {
                header('Content-Type: application/json');
                die(json_encode([
                    'status'  => false,
                    'type'    => 'not_login',
                    'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน',
                ]));
            }
        }

        public function selectCharacter(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'select_character') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('id') == false) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter! (2)'
                ]);
            }

            $charKey = (int)$decoded['id'];

            $userData = $this->userData;
            $uid = $userData['uid'];

            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่พบข้อมูล'
                ]);
            }

            if($member->char_id>0){
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถเปลี่ยนตัวละครได้'
                ]);
            }

            $mychar = $this->doGetChar($member->uid, $member->ncid);
            $myselect = collect($mychar)->where('id',$charKey)->first();

            if (count($myselect) == 0) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่พบตัวละคร'
                ]);
            }

            $member->char_id = (int)$myselect['char_id'];
            $member->char_name = $myselect['char_name'];
            $member->world_id = $myselect['world_id'];
            $member->save();

            if (empty($uid)) {
                return [
                    'status' => false,
                    'message' => 'ไม่พอข้อมูล'
                ];
            }

            // $member = Member::where('uid', $uid)->first();

            if ($member->char_id == 0) {
                $charData = $this->doGetChar($member->uid, $member->ncid);

                $content = [];

                foreach ($charData as $key => $value) {
                    $content[] = [
                        'id' => $value['id'],
                        'char_name' => $value['char_name'],
                    ];
                }

                return [
                    'status' => true,
                    'username' => $member->username,
                    'character_name' => '',
                    'selected_char' => false,
                    'characters' => $content,
                ];
            }

            $date = date('Y-m-d');
            $datetime = date('Y-m-d H:i:s');

            if ($this->event['data']['event']['test'] == true) {
                $date = $this->event['data']['event']['current_date'];
                $datetime = $this->event['data']['event']['current_date'] . ' ' . date('H:i:s');
            }

            $memberQuest = MemberQuest::where('uid', $uid)
              ->where('date', $date)
              ->first();

            $questList = Quest::orderBy('id', 'ASC')->get();

            if ($questList->count() == 0) {
                return response()->json([
                    'status' => false,
                    'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
                ]);
            }

            if ((time() >= strtotime($this->event['data']['event']['start_datetime']) && time() <= strtotime($this->event['data']['event']['end_datetime']))) {
                if (isset($memberQuest) === false) {
                    $logMemberQuest = new MemberQuest;
                    $logMemberQuest->uid = $uid;
                    $logMemberQuest->quest_index = Utils::QUEST_DETAIL[$date]['index'];
                    $logMemberQuest->date = $date;
                    $logMemberQuest->status = 'pending';
                    $logMemberQuest->claim_free_status = 'pending';
                    $logMemberQuest->claim_diamond_status = 'pending';
                    $logMemberQuest->last_ip = $this->getIP();
                    $logMemberQuest->start_quest = $datetime;
                    $logMemberQuest->end_quest = $date . ' 23:59:59';
                    $saved = $logMemberQuest->save();

                    if ($saved === false) {
                        return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
                        ]);
                    }

                    $questDailyLog = QuestDailyLog::where('uid', $uid)
                      ->where('log_date', $date)
                      ->orderBy('quest_id', 'ASC')
                      ->get();

                    if ($questDailyLog->count() == 0) {
                        $questDetail = Utils::QUEST_DETAIL[$date]['quest'];

                        if (isset($questDetail) === false) {
                            return response()->json([
                                'status' => false,
                                'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
                            ]);
                        }

                        for ($i = 0; $i < count($questDetail); $i++) {
                            $questVal =  $questList[$questDetail[$i]];

                            QuestDailyLog::create([
                                'uid' => $member->uid,
                                'username' => $member->username,
                                'ncid' => $member->ncid,
                                'char_id' => $member->char_id,
                                'char_name' => $member->char_name,
                                'quest_id' => $questVal->id,
                                'quest_dungeon' => $questVal->quest_dungeon,
                                'quest_title' => $questVal->quest_title,
                                'quest_code' => $questVal->quest_code,
                                'quest_amount' => $questVal->quest_amount,
                                'quest_type' => 'daily',
                                'image' => $questVal->image,
                                'total_quest' => 0,
                                'status' => 'pending',
                                'claim_status' => 'pending',
                                'log_date' => $date,
                                'log_date_timestamp' => time(),
                                'start_quest' => $datetime,
                                'end_quest' => $date . ' 23:59:59',
                                'member_quest_id' => $logMemberQuest->id,
                                'last_ip' => $this->getIP(),
                            ]);
                        }
                    }
                }
            }

            return response()->json($this->responseAll($member, 'คุณเลือกตัวละครแล้ว'));
        }

        public function getEventInfo(Request $request){
            $decoded = $request;

            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            // $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();

            if (isset($member) && empty($member)) {
                return response()->json([
                    'status' => false,
                    'message' => 'No member data.'
                ]);
            }

            if ($member->char_id == 0) {
                $charData = $this->doGetChar($member->uid, $member->ncid);

                $content = [];

                foreach ($charData as $key => $value) {
                    $content[] = [
                        'id' => $value['id'],
                        'char_name' => $value['char_name'],
                    ];
                }

                return response()->json([
                    'status' => true,
                    'data' => [
                        'username' => $member->username,
                        'character_name' => '',
                        'selected_char' => false,
                        'quests' => [],
                        'characters' => $content,
                    ],
                    'message' => ':)'
                ]);
            }

            $date = date('Y-m-d');
            $datetime = date('Y-m-d H:i:s');

            if ($this->event['data']['event']['test'] == true) {
                $date = $this->event['data']['event']['current_date'];
                $datetime = $this->event['data']['event']['current_date'] . ' ' . date('H:i:s');
            }

            $memberQuest = MemberQuest::where('uid', $uid)
              ->where('date', $date)
              ->first();

            $questList = Quest::orderBy('id', 'ASC')->get();

            if ($questList->count() == 0) {
                return response()->json([
                    'status' => false,
                    'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
                ]);
            }

            if ((time() >= strtotime($this->event['data']['event']['start_datetime']) && time() <= strtotime($this->event['data']['event']['end_datetime']))) {
                if (isset($memberQuest) === false) {
                    $logMemberQuest = new MemberQuest;
                    $logMemberQuest->uid = $uid;
                    $logMemberQuest->quest_index = Utils::QUEST_DETAIL[$date]['index'];
                    $logMemberQuest->date = $date;
                    $logMemberQuest->status = 'pending';
                    $logMemberQuest->claim_free_status = 'pending';
                    $logMemberQuest->claim_diamond_status = 'pending';
                    $logMemberQuest->last_ip = $this->getIP();
                    $logMemberQuest->start_quest = $datetime;
                    $logMemberQuest->end_quest = $date . ' 23:59:59';
                    $saved = $logMemberQuest->save();

                    if ($saved === false) {
                        return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
                        ]);
                    }

                    $questDailyLog = QuestDailyLog::where('uid', $uid)
                      ->where('log_date', $date)
                      ->orderBy('quest_id', 'ASC')
                      ->get();

                    if ($questDailyLog->count() == 0) {
                        $questDetail = Utils::QUEST_DETAIL[$date]['quest'];

                        if (isset($questDetail) === false) {
                            return response()->json([
                                'status' => false,
                                'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
                            ]);
                        }

                        for ($i = 0; $i < count($questDetail); $i++) {
                            $questVal =  $questList[$questDetail[$i]];

                            QuestDailyLog::create([
                                'uid' => $member->uid,
                                'username' => $member->username,
                                'ncid' => $member->ncid,
                                'char_id' => $member->char_id,
                                'char_name' => $member->char_name,
                                'quest_id' => $questVal->id,
                                'quest_dungeon' => $questVal->quest_dungeon,
                                'quest_title' => $questVal->quest_title,
                                'quest_code' => $questVal->quest_code,
                                'quest_amount' => $questVal->quest_amount,
                                'quest_type' => 'daily',
                                'image' => $questVal->image,
                                'total_quest' => 0,
                                'status' => 'pending',
                                'claim_status' => 'pending',
                                'log_date' => $date,
                                'log_date_timestamp' => time(),
                                'start_quest' => $datetime,
                                'end_quest' => $date . ' 23:59:59',
                                'member_quest_id' => $logMemberQuest->id,
                                'last_ip' => $this->getIP(),
                            ]);
                        }
                    }
                }
            }

            return response()->json($this->responseAll($member, ''));
        }

        private function getMaxDiamondsDaily($currDiamonds=0){
            $remainDiamonds = $this->maxDiamondsPerRound - $currDiamonds;
            if($remainDiamonds < $this->diamondsDailyLimit){
                return intval($remainDiamonds);
            }else{
                return intval($this->diamondsDailyLimit);
            }
        }

        // private function getCurrentRound($logDateTime=''){
        //     if(empty($logDateTime)){
        //         return false;
        //     }
        //
        //     return Round::where('start_datetime', '<=', $logDateTime)->where('end_datetime', '>=', $logDateTime)->where('status', 1)->first();
        // }

        private function setCanClaimBankStatus($completedStatus="pending",$diamonds=0,$lootboxes=0){
            if($completedStatus=="pending" && $diamonds > 0 && $lootboxes > 0){
                return true;
            }else{
                return false;
            }
            return false;
        }

        private function setClaimedBankStatus($completedStatus="pending"){
            if($completedStatus == "pending"){
                return false;
            }else if($completedStatus == "claimed"){
                return true;
            }
            return false;
        }

        private function getQuestList(){
            // return Cache::remember('BNS:PIGGY:QUESTS', 30, function () {
            //     return Quest::select('quest_dungeon','quest_title','quest_id','quest_code','quest_diamonds','quest_lootboxed','image')->orderBy('quest_id', 'ASC')->get();
            // });
        }

        // public function selectDailyQuest(Request $request){
        //     $decoded = $request;
        //     if ($decoded->has('type') == false || $decoded['type'] != 'select_quests') {//check parameter
        //         return response()->json([
        //                     'status' => false,
        //                     'message' => 'Invalid parameter!'
        //         ]);
        //     }
        //     if ($decoded->has('selected_list') == false) {//check parameter
        //         return response()->json([
        //                     'status' => false,
        //                     'message' => 'Invalid parameter! (2)'
        //         ]);
        //     }
        //
        //     $selected_list = $decoded['selected_list'];
        //
        //     // dd($selected_list);
        //
        //     $userData = $this->userData;
        //     $uid = $userData['uid'];
        //     $ncid = $this->getNcidByUid($uid);
        //
        //     $member = Member::where('uid', $uid)->first();
        //
        //     $logDate = date('Y-m-d');
        //     $logDateTime = date('Y-m-d H:i:s');
        //     $logDateTimestamp = time();
        //
        //     // check today is already selected quests.
        //     $todaySelected = QuestDailyLog::where('uid', $member->uid)->where('log_date', $logDate)->count();
        //     if($todaySelected > 0){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />คุณได้เลือกเควสสำหรับวันนี้ไปแล้ว'
        //         ]);
        //     }
        //
        //     // get current round
        //     $currRound = $this->getCurrentRound($logDateTime);
        //     if(!isset($currRound->id) || empty($currRound->id)){
        //         return [
        //             'status' => false,
        //             'message' => 'ไม่อยู่ในช่วงเวลาร่วมกิจกรรม'
        //         ];
        //     }
        //
        //     // get piggy bank round
        //     // $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->where('status', 'pending')->first();
        //     $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->first();
        //     if(!$memberBankQuery){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />ไม่มีข้อมูลของกระปุกหมู'
        //         ]);
        //     }
        //     if($memberBankQuery->status == 'claimed'){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />คุณทุบกระปุกหมูในรอบนี้ไปแล้ว'
        //         ]);
        //     }
        //     if($memberBankQuery->status == 'rejected'){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />หมดเวลาทุบกระปุกหมูในรอบนี้'
        //         ]);
        //     }
        //
        //     // check quest diamonds
        //     $questDiamondsSum = Quest::whereIn('quest_id', $selected_list)->sum('quest_diamonds');
        //     $questDiamonds = intval($questDiamondsSum);
        //     if($questDiamonds > $this->diamondsDailyLimit){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />เควสที่เลือกมีจำนวนไดมอนด์เกินกำหนดต่อวัน'
        //         ]);
        //     }
        //
        //     $questList = $this->getQuestList();
        //
        //     // create selected quest log
        //     if(count($selected_list) > 0){
        //         for($i=0;$i<count($selected_list);$i++){
        //             foreach($questList as $questKey=>$questVal){
        //                 if($questVal->quest_id == $selected_list[$i]){
        //                     QuestDailyLog::create([
        //                         'uid' => $member->uid,
        //                         'username' => $member->username,
        //                         'ncid' => $member->ncid,
        //                         'char_id' => $member->char_id,
        //                         'char_name' => $member->char_name,
        //                         'round_id' => $memberBankQuery->round_id,
        //                         'quest_id' => $questVal->quest_id,
        //                         'quest_dungeon' => $questVal->quest_dungeon,
        //                         'quest_title' => $questVal->quest_title,
        //                         'quest_code' => $questVal->quest_code,
        //                         'quest_diamonds' => $questVal->quest_diamonds,
        //                         'quest_lootboxed' => $questVal->quest_lootboxed,
        //                         'image' => $questVal->image,
        //                         'status' => 'pending',
        //                         'claim_status' => 'pending',
        //                         'log_date' => date('Y-m-d'),
        //                         'log_date_timestamp' => time(),
        //                         'last_ip' => $this->getIP(),
        //                     ]);
        //                 }
        //             }
        //         }
        //     }
        //
        //     // set event info
        //     $eventInfo = $this->setEventInfo($member->uid);
        //     if($eventInfo['status']==false){
        //         return response()->json([
        //                 'status' => false,
        //                 'message'=>$eventInfo['message'],
        //             ]);
        //     }
        //
        //     return response()->json([
        //         'status' => true,
        //         'data' => $eventInfo
        //     ]);
        //
        // }

        // public function claimDailyQuest(Request $request){
        //     $decoded = $request;
        //     if ($decoded->has('type') == false || $decoded['type'] != 'claim_daily_quest') {//check parameter
        //         return response()->json([
        //                     'status' => false,
        //                     'message' => 'Invalid parameter!'
        //         ]);
        //     }
        //     if ($decoded->has('quest_id') == false) {//check parameter
        //         return response()->json([
        //                     'status' => false,
        //                     'message' => 'Invalid parameter! (2)'
        //         ]);
        //     }
        //
        //     $quest_id = $decoded['quest_id'];
        //
        //     $userData = $this->userData;
        //     $uid = $userData['uid'];
        //     $ncid = $this->getNcidByUid($uid);
        //
        //     $member = Member::where('uid', $uid)->first();
        //
        //     $logDate = date('Y-m-d');
        //     $logDateTime = date('Y-m-d H:i:s');
        //     $logDateTimestamp = time();
        //
        //     // get current round
        //     $currRound = $this->getCurrentRound($logDateTime);
        //     if(!isset($currRound->id) || empty($currRound->id)){
        //         return [
        //             'status' => false,
        //             'message' => 'ไม่อยู่ในช่วงเวลาร่วมกิจกรรม'
        //         ];
        //     }
        //
        //     // get piggy bank round
        //     // $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->where('status', 'pending')->first();
        //     $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->first();
        //     if(!$memberBankQuery){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />ไม่มีข้อมูลของกระปุกหมู'
        //         ]);
        //     }
        //     if($memberBankQuery->status == 'claimed'){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />คุณทุบกระปุกหมูในรอบนี้ไปแล้ว'
        //         ]);
        //     }
        //     if($memberBankQuery->status == 'rejected'){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />หมดเวลาทุบกระปุกหมูในรอบนี้'
        //         ]);
        //     }
        //
        //     // get daily quest log
        //     // $dailyQuest = QuestDailyLog::where('uid', $member->uid)->where('log_date', $logDate)->where('quest_id', $quest_id)->where('status', 'success')->where('claim_status', 'pending')->first();
        //     $dailyQuest = QuestDailyLog::where('uid', $member->uid)->where('log_date', $logDate)->where('quest_id', $quest_id)->first();
        //     if(!$dailyQuest){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />ไม่มีข้อมูลเควส'
        //         ]);
        //     }
        //     if($dailyQuest->status == 'pending'){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />เควสที่เลือกยังไม่สำเร็จ'
        //         ]);
        //     }
        //     if($dailyQuest->claim_status == 'claimed'){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />ได้รับของรางวัลจากเควสนี้ไปแล้ว'
        //         ]);
        //     }
        //
        //     // update daily status after claim rewards to piggy bank
        //     $dailyQuest->claim_status = 'claimed';
        //     $dailyQuest->last_ip = $this->getIP();
        //
        //     if($dailyQuest->save()){
        //
        //         // add reward to piggy bank
        //         // $memberBankQuery->diamonds = $memberBankQuery->diamonds + $dailyQuest->quest_diamonds;
        //
        //         if($dailyQuest->quest_diamonds == 2000){
        //             $memberBankQuery->diamonds_2000_count = $memberBankQuery->diamonds_2000_count + 1;
        //         }
        //         if($dailyQuest->quest_diamonds == 1000){
        //             $memberBankQuery->diamonds_1000_count = $memberBankQuery->diamonds_1000_count + 1;
        //         }
        //         if($dailyQuest->quest_diamonds == 500){
        //             $memberBankQuery->diamonds_500_count = $memberBankQuery->diamonds_500_count + 1;
        //         }
        //
        //         $memberBankQuery->lootboxes = $memberBankQuery->lootboxes + $dailyQuest->quest_lootboxed;
        //         $memberBankQuery->last_ip = $this->getIP();
        //
        //         if($memberBankQuery->save()){
        //
        //             // set event info
        //             $eventInfo = $this->setEventInfo($member->uid);
        //             if($eventInfo['status']==false){
        //                 return response()->json([
        //                         'status' => false,
        //                         'message'=>$eventInfo['message'],
        //                     ]);
        //             }
        //
        //             return response()->json([
        //                 'status' => true,
        //                 'message'=> 'หยอดกระปุกเสร็จเรียบร้อย',
        //                 'data' => $eventInfo
        //             ]);
        //
        //         }
        //
        //     }
        //
        //     return response()->json([
        //         'status' => false,
        //         'message' => 'ขออภัย<br />เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
        //     ]);
        // }

        // public function claimPiggyBank(Request $request){
        //     $decoded = $request;
        //     if ($decoded->has('type') == false || $decoded['type'] != 'claim_piggy_bank') {//check parameter
        //         return response()->json([
        //                     'status' => false,
        //                     'message' => 'Invalid parameter!'
        //         ]);
        //     }
        //
        //     $userData = $this->userData;
        //     $uid = $userData['uid'];
        //     $ncid = $this->getNcidByUid($uid);
        //
        //     $member = Member::where('uid', $uid)->first();
        //
        //     $logDate = date('Y-m-d');
        //     $logDateTime = date('Y-m-d H:i:s');
        //     $logDateTimestamp = time();
        //
        //     // get current round
        //     $currRound = $this->getCurrentRound($logDateTime);
        //     if(!isset($currRound->id) || empty($currRound->id)){
        //         return [
        //             'status' => false,
        //             'message' => 'ไม่อยู่ในช่วงเวลาร่วมกิจกรรม'
        //         ];
        //     }
        //
        //     // get piggy bank round
        //     // $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->where('status', 'pending')->first();
        //     $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->first();
        //     if(!$memberBankQuery){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />ไม่มีข้อมูลของกระปุกหมู'
        //         ]);
        //     }
        //     if($memberBankQuery->status == 'claimed'){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />คุณทุบกระปุกหมูในรอบนี้ไปแล้ว'
        //         ]);
        //     }
        //     if($memberBankQuery->status == 'rejected'){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />หมดเวลาทุบกระปุกหมูในรอบนี้'
        //         ]);
        //     }
        //     $totalRoundDiamonds = 0;
        //     $totalRoundDiamonds = ($this->diamonds_2000 * $memberBankQuery->diamonds_2000_count) + ($this->diamonds_1000 * $memberBankQuery->diamonds_1000_count) + ($this->diamonds_500 * $memberBankQuery->diamonds_500_count);
        //     if($this->setCanClaimBankStatus($memberBankQuery->status,$totalRoundDiamonds,$memberBankQuery->lootboxes) == false){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />ไม่สามารถทุบกระปุกหมูได้<br />เนื่องจากยังไม่มีจำนวนของรางวัลสะสม'
        //         ]);
        //     }
        //
        //     // get diamonds balance
        //     $diamonds = $this->setRemainDiamonds($member->uid);
        //
        //     if($diamonds < $this->serviceCharge){
        //         return response()->json([
        //             'status' => false,
        //             'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอทุบกระปุก'
        //         ]);
        //     }
        //
        //     // deduct diamonds for service charge
        //     // set deduct diamonds
        //     $logDeduct = $this->addDeductLog([
        //         'uid'                       => $member->uid,
        //         'username'                  => $member->username,
        //         'ncid'                      => $member->ncid,
        //         'round_id'                  => $memberBankQuery->round_id,
        //         'diamonds'                  => $this->serviceCharge,
        //         'status'                    => 'pending',
        //         'log_date'                  => date('Y-m-d H:i:s'), // set to string
        //         'log_date_timestamp'        => time(),
        //         'last_ip'                   => $this->getIP(),
        //     ]);
        //
        //     // set deduct diamonds
        //     $deductData = $this->setDiamondDeductData($this->serviceCharge);
        //     $respDeductRaw = $this->deductDiamond($ncid, $deductData);
        //     $respDeduct = json_decode($respDeductRaw);
        //     if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){
        //
        //         // update deduct log
        //         $arrDeductDiamond = [
        //             'before_deduct_diamond' => $diamonds,
        //             'after_deduct_diamond' => $diamonds - $this->serviceCharge,
        //             'deduct_status' => $respDeduct->status ?: 0,
        //             'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
        //             'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
        //             'deduct_data' => json_encode($deductData),
        //             'status' => 'success'
        //         ];
        //
        //         $respDeduct = $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);
        //
        //         if($respDeduct){
        //
        //             // update member bank log to claimed status
        //             $memberBankQuery->status = 'claimed';
        //             $memberBankQuery->last_ip = $this->getIP();
        //             $memberBankQuery->updated_at = date('Y-m-d H:i:s');
        //             $memberBankQuery->save();
        //
        //             // get rewards
        //             $rewardsQuery = Reward::where('available_date', '<=', $logDateTime)->get();
        //             if(count($rewardsQuery) <= 0){
        //                 return response()->json([
        //                     'status' => false,
        //                     'message' => 'ขออภัย<br />ไม่มีข้อมูลของรางวัล',
        //                 ]);
        //             }
        //
        //             $productList = [];
        //             $goods_data = []; //good data for send item group
        //             foreach ($rewardsQuery as $key => $reward) {
        //                 $quantity = 1;
        //                 if($reward->package_type == 'diamonds'){
        //                     if($reward->package_key == 1){ // 2000 diamonds
        //                         $quantity = $memberBankQuery->diamonds_2000_count;
        //                     }
        //                     if($reward->package_key == 2){ // 1000 diamonds
        //                         $quantity = $memberBankQuery->diamonds_1000_count;
        //                     }
        //                     if($reward->package_key == 3){ // 500 diamonds
        //                         $quantity = $memberBankQuery->diamonds_500_count;
        //                     }
        //
        //                 }elseif($reward->package_type == 'materials'){
        //                     $quantity = $memberBankQuery->lootboxes;
        //                 }
        //
        //                 $goods_data[] = [
        //                     'goods_id' => $reward->package_id,
        //                     'purchase_quantity' => $quantity,
        //                     'purchase_amount' => 0,
        //                     'category_id' => 40,
        //                 ];
        //
        //                 $productList[] = [
        //                     'title' => $reward->package_name,
        //                     'quantity' => $quantity,
        //                 ];
        //
        //             }
        //
        //             $sendItemLog = $this->addItemHistoryLog([
        //                 'uid'                           => $member->uid,
        //                 'username'                      => $member->username,
        //                 'ncid'                          => $member->ncid,
        //                 'deduct_id'                     => $logDeduct['id'],
        //                 'round_id'                      => $memberBankQuery->round_id,
        //                 'product_set'                   => json_encode($productList),
        //                 'status'                        => 'pending',
        //                 'send_item_status'              => false,
        //                 'send_item_purchase_id'         => 0,
        //                 'send_item_purchase_status'     => 0,
        //                 'goods_data'                    => json_encode($goods_data),
        //                 'log_date'                      => (string)date('Y-m-d'), // set to string
        //                 'log_date_timestamp'            => time(),
        //                 'last_ip'                       => $this->getIP(),
        //             ]);
        //
        //             $send_result_raw = $this->dosendItem($ncid, $goods_data);
        //             $send_result = json_decode($send_result_raw);
        //             if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
        //
        //                 // update send item log
        //                 $this->updateItemHistoryLog([
        //                     'send_item_status' => $send_result->status ?: false,
        //                     'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
        //                     'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
        //                     'status' => 'success'
        //                         ], $sendItemLog['id'], $uid);
        //
        //                 $eventInfo = $this->setEventInfo($uid);
        //
        //                 return response()->json([
        //                             'status' => true,
        //                             'message' => "ได้รับ ".number_format($totalRoundDiamonds)." ไดมอนด์<br />และกล่องเสบียงน้องหมู ".$memberBankQuery->lootboxes." กล่อง<br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุราเข้าเกมเพื่อกดรับ",
        //                             'data' => $eventInfo
        //                         ]);
        //             }else{
        //                 $this->updateItemHistoryLog([
        //                     'send_item_status' => isset($send_result->status) ? $send_result->status : false,
        //                     'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
        //                     'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
        //                     'status' => 'unsuccess'
        //                         ], $sendItemLog['id'], $uid);
        //
        //                 // $eventInfo = $this->setEventInfo($uid);
        //
        //                 return response()->json([
        //                             'status' => false,
        //                             'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
        //                             // 'data' => $eventInfo
        //                         ]);
        //             }
        //
        //         }else{
        //             return response()->json([
        //                 'status' => false,
        //                 'message' => 'ไม่สามารถสั่งซื้อแพ็คเกจได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
        //             ]);
        //         }
        //
        //
        //     }else{
        //         $arrDeductDiamond['status'] = 'unsuccess';
        //         $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);
        //     }
        //
        //     return response()->json([
        //                 'status' => false,
        //                 'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
        //     ]);
        //
        // }

        public function getHistory(Request $request){
            $decoded = $request;

            if ($decoded->has('type') == false || $decoded['type'] != 'history') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $uid = $this->userData['uid'];

            $resp_raw = SendItemLog::select('name', 'created_at')
                    ->where('uid', $uid)
                    ->orderBy('created_at', 'DESC')
                    ->get();

            $resp = $resp_raw->toArray();

            return response()->json([
                'status' => true,
                'content' => $resp
            ]);

        }

        public function unlockDailyQuest(Request $request) {
            if ((time() < strtotime($this->event['data']['event']['start_datetime']) || time() > strtotime($this->event['data']['event']['end_datetime']))) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม',
                ]));
            }

            $decoded = $request;

            if ($decoded->has('type') == false || $decoded['type'] != 'unlock') {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('value') == false || isset(Utils::ARRAY_UNLOCK[intval($decoded['value']) - 1]) == false) {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

            $date = Utils::ARRAY_UNLOCK[intval($decoded['value']) - 1];

            $userData = $this->userData;
            $uid = $userData['uid'];

            $questList = Quest::orderBy('id', 'ASC')->get();

            if ($questList->count() == 0) {
                return response()->json([
                    'status' => false,
                    'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
                ]);
            }

            $memberQuest = MemberQuest::where('uid', $uid)
              ->where('date', $date)
              ->where(function($query) {
                  $query->where('status', 'pending')
                        ->orWhere('status', 'success');
              })
              ->first();

            if (isset($memberQuest) === true) {
                return response()->json([
                    'status' => false,
                    'message' => 'คุณมีเควส หรือทำเควสสำเร็จแล้ว ไม่สามารถทำซ้ำได้'
                ]);
            }

            $required_deduct_diamond = $this->event['data']['event']['buy_quest'];

            if ($decoded['value'] == 25) {
                $required_deduct_diamond = 0;
            }

            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถอัพเดทข้อมูล Diamonds ของคุณได้กรุณาลองใหม่อีกครั้ง'
                ]);
            }

            $before_diamond = intval($diamondsBalance->balance);
            // compare user diamonds balance with gachapon's require diamomds.
            if ($before_diamond < $required_deduct_diamond) {
                return response()->json([
                    'status' => false,
                    'message' => 'จำนวน Diamonds ของคุณไม่พอในการซื้อกาชาปอง'
                ]);
            }

            // get member info
            $member = Member::where('uid', $uid)->first();

            if (isset($member) && empty($member)) {
                return response()->json([
                    'status' => false,
                    'message' => 'No member data.'
                ]);
            }

            $ncid = $member->ncid;

            if ($decoded['value'] == 25) {
                $logDeduct = $this->addDeductLog([
                    'uid' => $uid,
                    'ncid' => $ncid,
                    'status' => 'pending',
                    'diamond' => $required_deduct_diamond,
                    'last_ip' => $this->getIP(),
                    'deduct_type' => 'buy',
                    'log_date' => date('Y-m-d'),
                    'log_date_timestamp' => time(),
                ]);

                $arrDeductLog = [
                    'before_deduct_diamond' => $before_diamond,
                    'after_deduct_diamond' => $before_diamond,
                    'deduct_status' => 1,
                    'deduct_purchase_id' => 0,
                    'deduct_purchase_status' => 0,
                    'deduct_data' => '',
                    'status' => 'pending'
                ];

                $arrDeductLog['status'] = 'success';
                $this->updateDeductLog($arrDeductLog, $logDeduct['id']);
            } else {
                // set desuct diamonds
                $deductData = $this->setDiamondDeductData($required_deduct_diamond);

                if ($deductData == false) {
                    return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง']);
                }

                $logDeduct = $this->addDeductLog([
                    'uid' => $uid,
                    'ncid' => $ncid,
                    'status' => 'pending',
                    'diamond' => $required_deduct_diamond,
                    'last_ip' => $this->getIP(),
                    'deduct_type' => 'buy',
                    'log_date' => date('Y-m-d'),
                    'log_date_timestamp' => time(),
                ]);

                $respDeductRaw = $this->deductDiamond($ncid, $deductData);
                $respDeduct = json_decode($respDeductRaw);

                if (isset($respDeduct) === false) {
                    $arrDeductLog['status'] = 'unsuccess';
                    $this->updateDeductLog($arrDeductLog, $logDeduct['id']);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }

                $arrDeductLog = [
                    'before_deduct_diamond' => $before_diamond,
                    'after_deduct_diamond' => $before_diamond - $required_deduct_diamond,
                    'deduct_status' => $respDeduct->status,
                    'deduct_purchase_id' => isset($respDeduct->response->purchase_id) ? $respDeduct->response->purchase_id : 0,
                    'deduct_purchase_status' => isset($respDeduct->response->purchase_status) ? $respDeduct->response->purchase_status : 0,
                    'deduct_data' => json_encode($deductData),
                    'status' => 'pending'
                ];

                if ($respDeduct->status == false) {
                    $arrDeductLog['status'] = 'unsuccess';
                    $this->updateDeductLog($arrDeductLog, $logDeduct['id']);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }

                $arrDeductLog['status'] = 'success';
                $this->updateDeductLog($arrDeductLog, $logDeduct['id']);
            }



            $logMemberQuest = new MemberQuest;
            $logMemberQuest->uid = $uid;
            $logMemberQuest->quest_index = Utils::QUEST_DETAIL[$date]['index'];
            $logMemberQuest->date = $date;
            $logMemberQuest->status = 'pending';
            $logMemberQuest->claim_free_status = 'pending';
            $logMemberQuest->claim_diamond_status = 'pending';
            $logMemberQuest->deduct_id = $logDeduct['id'];
            $logMemberQuest->last_ip = $this->getIP();
            $logMemberQuest->start_quest = date('Y-m-d H:i:s');
            $logMemberQuest->end_quest = $this->event['data']['event']['end_datetime'];
            $saved = $logMemberQuest->save();

            if ($saved === false) {
                return response()->json([
                    'status' => false,
                    'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
                ]);
            }

            $questDailyLog = QuestDailyLog::where('uid', $uid)
              ->where('log_date', $date)
              ->where('member_quest_id', $logMemberQuest->id)
              ->orderBy('quest_id', 'ASC')
              ->get();

            if ($questDailyLog->count() == 0) {
                $questDetail = Utils::QUEST_DETAIL[$date]['quest'];

                if (isset($questDetail) === false) {
                    return response()->json([
                        'status' => false,
                        'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
                    ]);
                }

                for ($i = 0; $i < count($questDetail); $i++) {
                    $questVal =  $questList[$questDetail[$i]];

                    QuestDailyLog::create([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'char_id' => $member->char_id,
                        'char_name' => $member->char_name,
                        'quest_id' => $questVal->id,
                        'quest_dungeon' => $questVal->quest_dungeon,
                        'quest_title' => $questVal->quest_title,
                        'quest_code' => $questVal->quest_code,
                        'quest_amount' => $questVal->quest_amount,
                        'quest_type' => 'buy',
                        'image' => $questVal->image,
                        'total_quest' => 0,
                        'status' => 'pending',
                        'claim_status' => 'pending',
                        'log_date' => $date,
                        'log_date_timestamp' => time(),
                        'start_quest' => date('Y-m-d H:i:s'),
                        'end_quest' => $this->event['data']['event']['end_datetime'],
                        'member_quest_id' => $logMemberQuest->id,
                        'last_ip' => $this->getIP(),
                    ]);
                }
            }

            return response()->json($this->responseAll($member, 'ยินดีด้วย คุณปลดล็อคเควสสำเร็จ'));
        }

        public function claimFree(Request $request) {
            $decoded = $request;

            if ($decoded->has('type') == false || $decoded['type'] != 'claim_free') {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('value') == false || isset(Utils::ARRAY_DATE[intval($decoded['value']) - 1]) == false) {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

            $date = Utils::ARRAY_DATE[intval($decoded['value']) - 1];

            $userData = $this->userData;
            $uid = $userData['uid'];

            $memberQuest = MemberQuest::where('uid', $uid)
              ->where('date', $date)
              ->where('status', 'success')
              ->first();

            if (isset($memberQuest) === false) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถรับรางวัลได้ เนื่องจากไม่ตรงตามเงื่อนไข'
                ]);
            }

            if ($memberQuest->claim_free_status === 'success') {
                return response()->json([
                    'status' => false,
                    'message' => 'คุณรับรางวัลไปแล้ว ไม่สามารถรับเพิ่มได้'
                ]);
            }

            // get member info
            $member = Member::where('uid', $uid)->first();

            if (isset($member) && empty($member)) {
                return response()->json([
                    'status' => false,
                    'message' => 'No member data.'
                ]);
            }

            $memberQuest->claim_free_status = 'success';
            $memberQuest->updated_at = date('Y-m-d H:i:s');
            $memberQuest->save();

            $ncid = $member->ncid;

            $rewards = Reward::orderBy('id', 'ASC')->get();

            if ($rewards->count() == 0) {
                return response()->json([
                    'status' => false,
                    'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
                ]);
            }

            $resultRewards = [];

            $questDetailRewards = Utils::QUEST_DETAIL[$date]['rewards'];
            $rewardName = Utils::QUEST_DETAIL[$date]['claim_free'];

            for ($i = 0; $i < count($questDetailRewards); $i++) {
                $resultRewards[] = [
                    'item_type' => 'object',
                    'product_title' => $rewards[$questDetailRewards[$i]]->package_name,
                    'product_id' => $rewards[$questDetailRewards[$i]]->package_id,
                    'product_quantity' => $rewards[$questDetailRewards[$i]]->package_quantity,
                ];
            }

            $goodsData = []; //good data for send item group

            foreach ($resultRewards as $key => $value) {//loop for add item log
                $arrItemHistory = [
                    'uid' => $uid,
                    'ncid' => $ncid,
                    'product_id' => $value['product_id'],
                    'product_title' => $value['product_title'],
                    'product_quantity' => $value['product_quantity'],
                    'gachapon_type' => 'claim_free',
                    'log_date' => date('Y-m-d'),
                    'log_date_timestamp' => time(),
                    'item_type' => $value['item_type'],
                    'last_ip' => $this->getIP(),
                ];

                $goodsData[] = [
                    'goods_id' => $value['product_id'],
                    'purchase_quantity' => $value['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];

                $this->addItemHistoryLog($arrItemHistory);
            }

            // send items
            if (is_null($goodsData) == false && is_array($goodsData) && count($goodsData) > 0) {
                //do add send item log
                $logSendItemResult = $this->addSendItemLog([
                    'uid' => $uid,
                    'ncid' => $ncid,
                    'send_item_status' => false,
                    'send_item_purchase_id' => 0,
                    'send_item_purchase_status' => 0,
                    'goods_data' => json_encode($goodsData),
                    'status' => 'pending',
                    'last_ip' => $this->getIP(),
                    'type' => 'claim_free',
                    'log_date' => date('Y-m-d'),
                    'log_date_timestamp' => time(),
                    'member_quest_id' => $memberQuest->id,
                    'name' => $rewardName,
                ]);

                //send item
                $sendResultRaw = $this->dosendItem($ncid, $goodsData);
                $sendResult = json_decode($sendResultRaw);
                if (is_null($sendResult) == false && is_object($sendResult) && $sendResult->status) {
                    //recive log id
                    $log_id = $logSendItemResult['id'];
                    //do update send item log
                    $this->updateSendItemLog([
                        'send_item_status' => $sendResult->status ?: false,
                        'send_item_purchase_id' => $sendResult->response->purchase_id ?: 0,
                        'send_item_purchase_status' => $sendResult->response->purchase_status ?: 0,
                        'status' => 'success'
                            ], $log_id, $uid);
                } else {
                    //do update send item log
                    $log_id = $logSendItemResult['id'];
                    $this->updateSendItemLog([
                        'send_item_status' => isset($sendResult->status) ? $sendResult->status : false,
                        'send_item_purchase_id' => isset($sendResult->response->purchase_id) ? $sendResult->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($sendResult->response->purchase_status) ? $sendResult->response->purchase_status : 0,
                        'status' => 'unsuccess'
                            ], $log_id, $uid);
                    //error unsuccess log
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }
            }

            return response()->json($this->responseAll($member, 'ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br>กรุณาเข้าเกมเพื่อกดรับ'));
        }

        public function claimDiamond(Request $request) {
            $decoded = $request;

            if ($decoded->has('type') == false || $decoded['type'] != 'claim_diamond') {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('value') == false || isset(Utils::ARRAY_DATE[intval($decoded['value']) - 1]) == false) {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

            $date = Utils::ARRAY_DATE[intval($decoded['value']) - 1];

            $userData = $this->userData;
            $uid = $userData['uid'];

            $memberQuest = MemberQuest::where('uid', $uid)
              ->where('date', $date)
              ->where('status', 'success')
              ->first();

            if (isset($memberQuest) === false) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถรับรางวัลได้ เนื่องจากไม่ตรงตามเงื่อนไข'
                ]);
            }

            if ($memberQuest->claim_diamond_status === 'success') {
                return response()->json([
                    'status' => false,
                    'message' => 'คุณรับรางวัลไปแล้ว ไม่สามารถรับเพิ่มได้'
                ]);
            }

            $required_deduct_diamond = $this->event['data']['event']['claim_diamond'];

            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถอัพเดทข้อมูล Diamonds ของคุณได้กรุณาลองใหม่อีกครั้ง'
                ]);
            }

            $before_diamond = intval($diamondsBalance->balance);
            // compare user diamonds balance with gachapon's require diamomds.
            if ($before_diamond < $required_deduct_diamond) {
                return response()->json([
                    'status' => false,
                    'message' => 'จำนวน Diamonds ของคุณไม่พอในการซื้อกาชาปอง'
                ]);
            }

            // get member info
            $member = Member::where('uid', $uid)->first();

            if (isset($member) && empty($member)) {
                return response()->json([
                    'status' => false,
                    'message' => 'No member data.'
                ]);
            }

            $memberQuest->claim_diamond_status = 'success';
            $memberQuest->updated_at = date('Y-m-d H:i:s');
            $memberQuest->save();

            $ncid = $member->ncid;

            $logDeduct = $this->addDeductLog([
                'uid' => $uid,
                'ncid' => $ncid,
                'member_quest_id' => $memberQuest->id,
                'status' => 'pending',
                'diamond' => $required_deduct_diamond,
                'last_ip' => $this->getIP(),
                'deduct_type' => 'claim_diamond',
                'log_date' => date('Y-m-d'),
                'log_date_timestamp' => time(),
            ]);

            // set desuct diamonds
            $deductData = $this->setDiamondDeductData($required_deduct_diamond);

            if ($deductData == false) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง']);
            }

            $respDeductRaw = $this->deductDiamond($ncid, $deductData);
            $respDeduct = json_decode($respDeductRaw);

            if (isset($respDeduct) === false) {
                $arrDeductLog['status'] = 'unsuccess';
                $this->updateDeductLog($arrDeductLog, $logDeduct['id']);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }

            $arrDeductLog = [
                'before_deduct_diamond' => $before_diamond,
                'after_deduct_diamond' => $before_diamond - $required_deduct_diamond,
                'deduct_status' => $respDeduct->status,
                'deduct_purchase_id' => isset($respDeduct->response->purchase_id) ? $respDeduct->response->purchase_id : 0,
                'deduct_purchase_status' => isset($respDeduct->response->purchase_status) ? $respDeduct->response->purchase_status : 0,
                'deduct_data' => json_encode($deductData),
                'status' => 'pending'
            ];

            if ($respDeduct->status == false) {
                $arrDeductLog['status'] = 'unsuccess';
                $this->updateDeductLog($arrDeductLog, $logDeduct['id']);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }

            $arrDeductLog['status'] = 'success';
            $this->updateDeductLog($arrDeductLog, $logDeduct['id']);

            $rewards = Reward::orderBy('id', 'ASC')->get();

            if ($rewards->count() == 0) {
                return response()->json([
                    'status' => false,
                    'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
                ]);
            }

            $resultRewards = [];

            $questDetailRewards = Utils::QUEST_DETAIL[$date]['rewards'];
            $rewardName = Utils::QUEST_DETAIL[$date]['claim_diamond'];

            for ($i = 0; $i < count($questDetailRewards); $i++) {
                $resultRewards[] = [
                    'item_type' => 'object',
                    'product_title' => $rewards[$questDetailRewards[$i]]->package_name,
                    'product_id' => $rewards[$questDetailRewards[$i]]->package_id,
                    'product_quantity' => $rewards[$questDetailRewards[$i]]->package_quantity,
                ];
            }

            $goodsData = []; //good data for send item group

            foreach ($resultRewards as $key => $value) {//loop for add item log
                $arrItemHistory = [
                    'uid' => $uid,
                    'ncid' => $ncid,
                    'product_id' => $value['product_id'],
                    'product_title' => $value['product_title'],
                    'product_quantity' => $value['product_quantity'],
                    'gachapon_type' => 'claim_diamond',
                    'log_date' => date('Y-m-d'),
                    'log_date_timestamp' => time(),
                    'item_type' => $value['item_type'],
                    'last_ip' => $this->getIP(),
                ];

                $goodsData[] = [
                    'goods_id' => $value['product_id'],
                    'purchase_quantity' => $value['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];

                $this->addItemHistoryLog($arrItemHistory);
            }

            // send items
            if (is_null($goodsData) == false && is_array($goodsData) && count($goodsData) > 0 && is_null($respDeduct->response->purchase_id) == false) {
                //do add send item log
                $logSendItemResult = $this->addSendItemLog([
                    'uid' => $uid,
                    'ncid' => $ncid,
                    'send_item_status' => false,
                    'send_item_purchase_id' => 0,
                    'send_item_purchase_status' => 0,
                    'goods_data' => json_encode($goodsData),
                    'status' => 'pending',
                    'last_ip' => $this->getIP(),
                    'type' => 'claim_diamond',
                    'log_date' => date('Y-m-d'),
                    'log_date_timestamp' => time(),
                    'member_quest_id' => $memberQuest->id,
                    'deduct_id' => $logDeduct['id'],
                    'name' => $rewardName,
                ]);

                //send item
                $sendResultRaw = $this->dosendItem($ncid, $goodsData);
                $sendResult = json_decode($sendResultRaw);
                if (is_null($sendResult) == false && is_object($sendResult) && $sendResult->status) {
                    //recive log id
                    $log_id = $logSendItemResult['id'];
                    //do update send item log
                    $this->updateSendItemLog([
                        'send_item_status' => $sendResult->status ?: false,
                        'send_item_purchase_id' => $sendResult->response->purchase_id ?: 0,
                        'send_item_purchase_status' => $sendResult->response->purchase_status ?: 0,
                        'status' => 'success'
                            ], $log_id, $uid);
                } else {
                    //do update send item log
                    $log_id = $logSendItemResult['id'];
                    $this->updateSendItemLog([
                        'send_item_status' => isset($sendResult->status) ? $sendResult->status : false,
                        'send_item_purchase_id' => isset($sendResult->response->purchase_id) ? $sendResult->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($sendResult->response->purchase_status) ? $sendResult->response->purchase_status : 0,
                        'status' => 'unsuccess'
                            ], $log_id, $uid);
                    //error unsuccess log
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }
            }

            return response()->json($this->responseAll($member, 'ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br>กรุณาเข้าเกมเพื่อกดรับ'));
        }

        public function claimStamp(Request $request) {
            $decoded = $request;

            if ($decoded->has('type') == false || $decoded['type'] != 'claim_stamp') {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('value') == false || isset(Utils::ARRAY_STAMP[intval($decoded['value'])]) == false) {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

            // $date = Utils::ARRAY_STAMP[intval($decoded['value']) - 1];

            $userData = $this->userData;
            $uid = $userData['uid'];

            // $memberQuest = MemberQuest::where('uid', $uid)
            //   ->where('date', $date)
            //   ->where('status', 'success')
            //   ->first();
            //
            // if (isset($memberQuest) === false) {
            //     return response()->json([
            //         'status' => false,
            //         'message' => 'ไม่สามารถรับรางวัลได้ เนื่องจากไม่ตรงตามเงื่อนไข'
            //     ]);
            // }

            // if ($memberQuest->claim_free_status === 'success') {
            //     return response()->json([
            //         'status' => false,
            //         'message' => 'คุณรับรางวัลไปแล้ว ไม่สามารถรับเพิ่มได้'
            //     ]);
            // }

            // get member info
            $member = Member::where('uid', $uid)->first();

            if (isset($member) && empty($member)) {
                return response()->json([
                    'status' => false,
                    'message' => 'No member data.'
                ]);
            }

            if ($member->total_stamp < Utils::ARRAY_STAMP[intval($decoded['value'])]['stamp']) {
                return response()->json([
                    'status' => false,
                    'message' => 'คุณมีแสตมป์ไม่เพียงพอในการแลกของรางวัล'
                ]);
            }

            // $memberQuest->claim_free_status = 'success';
            // $memberQuest->updated_at = date('Y-m-d H:i:s');
            // $memberQuest->save();

            $ncid = $member->ncid;

            $rewards = Reward::orderBy('id', 'ASC')->get();

            if ($rewards->count() == 0) {
                return response()->json([
                    'status' => false,
                    'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
                ]);
            }

            $resultRewards = [];

            $questDetailRewards = Utils::ARRAY_STAMP[intval($decoded['value'])]['rewards'];
            $rewardName = Utils::ARRAY_STAMP[intval($decoded['value'])]['message'];

            for ($i = 0; $i < count($questDetailRewards); $i++) {
                $resultRewards[] = [
                    'item_type' => 'object',
                    'product_title' => $rewards[$questDetailRewards[$i]]->package_name,
                    'product_id' => $rewards[$questDetailRewards[$i]]->package_id,
                    'product_quantity' => $rewards[$questDetailRewards[$i]]->package_quantity,
                ];
            }

            $goodsData = []; //good data for send item group

            foreach ($resultRewards as $key => $value) {//loop for add item log
                $arrItemHistory = [
                    'uid' => $uid,
                    'ncid' => $ncid,
                    'product_id' => $value['product_id'],
                    'product_title' => $value['product_title'],
                    'product_quantity' => $value['product_quantity'],
                    'gachapon_type' => 'claim_stamp',
                    'log_date' => date('Y-m-d'),
                    'log_date_timestamp' => time(),
                    'item_type' => $value['item_type'],
                    'last_ip' => $this->getIP(),
                ];

                $goodsData[] = [
                    'goods_id' => $value['product_id'],
                    'purchase_quantity' => $value['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];

                $this->addItemHistoryLog($arrItemHistory);
            }

            // send items
            if (is_null($goodsData) == false && is_array($goodsData) && count($goodsData) > 0) {
                //do add send item log
                $logSendItemResult = $this->addSendItemLog([
                    'uid' => $uid,
                    'ncid' => $ncid,
                    'send_item_status' => false,
                    'send_item_purchase_id' => 0,
                    'send_item_purchase_status' => 0,
                    'goods_data' => json_encode($goodsData),
                    'status' => 'pending',
                    'last_ip' => $this->getIP(),
                    'type' => 'claim_stamp',
                    'log_date' => date('Y-m-d'),
                    'log_date_timestamp' => time(),
                    'name' => $rewardName,
                ]);

                //send item
                $sendResultRaw = $this->dosendItem($ncid, $goodsData);
                $sendResult = json_decode($sendResultRaw);
                if (is_null($sendResult) == false && is_object($sendResult) && $sendResult->status) {
                    //recive log id
                    $log_id = $logSendItemResult['id'];
                    //do update send item log
                    $this->updateSendItemLog([
                        'send_item_status' => $sendResult->status ?: false,
                        'send_item_purchase_id' => $sendResult->response->purchase_id ?: 0,
                        'send_item_purchase_status' => $sendResult->response->purchase_status ?: 0,
                        'status' => 'success'
                            ], $log_id, $uid);
                } else {
                    //do update send item log
                    $log_id = $logSendItemResult['id'];
                    $this->updateSendItemLog([
                        'send_item_status' => isset($sendResult->status) ? $sendResult->status : false,
                        'send_item_purchase_id' => isset($sendResult->response->purchase_id) ? $sendResult->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($sendResult->response->purchase_status) ? $sendResult->response->purchase_status : 0,
                        'status' => 'unsuccess'
                            ], $log_id, $uid);
                    //error unsuccess log
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }
            }

            return response()->json($this->responseAll($member, 'ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br>กรุณาเข้าเกมเพื่อกดรับ'));
        }

        private function setMemberBankStatus($currRound, $memberRound, $status=''){
            if($status == 'pending' && $currRound == $memberRound){
                return 'กำลังดำเนินการ';
            }
            if(($status == 'claimed' && $currRound == $memberRound) || ($status == 'claimed' && $currRound != $memberRound)){
                return 'ทุบกระปุกแล้ว';
            }
            if($status == 'pending' && $currRound != $memberRound){
                return 'ไม่สำเร็จ';
            }

            return 'ไม่สำเร็จ';
        }

        private function setDailyQuestStatus($currRound, $memberRound, $status=''){
            if($status == 'pending' && $currRound == $memberRound){
                return 'กำลังดำเนินการ';
            }
            if($status == 'success' && $currRound == $memberRound){
                return 'ภารกิจสำเร็จ';
            }
            if($status == 'pending' && $currRound != $memberRound){
                return 'ภารกิจไม่สำเร็จ';
            }

            return 'ภารกิจไม่สำเร็จ';
        }

    }
