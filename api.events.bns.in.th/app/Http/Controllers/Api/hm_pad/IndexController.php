<?php

    namespace App\Http\Controllers\Api\hm_pad;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DB;

    use App\Models\hm_pad\HmPad;
    use App\Models\hm_pad\HmPadBg;

    class IndexController extends BnsEventController {

        private $awtSecret = 'wfGfxUqpMFt3PsWXyJMe8jP2wsE55rvF';

        public function __construct() {
            parent::__construct();

            // init user data
            $this->userData = $this->getUserData();

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

        }

        private function getIP() {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn() {
            $this->userData = $this->getUserData();
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip() {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1';

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        public function getEventInfo(Request $request){

            $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            if (is_null($decoded)) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }
            $decoded = collect($decoded);

            if ($decoded->has('type') == false && $decoded['type'] != 'hm_pad') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $currentTime = date('Y-m-d H:i:s');

            $eventsList = [];
            $background = '';

            if($this->is_accepted_ip() == true){
                // get events for admin
                $eventsList = HmPad::select('title','icon_path','event_path')
                                    ->where('is_admin', 'on')
                                    ->where('type', 'pad')
                                    ->where('start_show_time', '<=', $currentTime)
                                    ->where('end_show_time', '>=', $currentTime)
                                    ->orderBy('id', 'DESC')
                                    ->get();

                // get background for admin
                $backgroundData = HmPadBg::select('title','bg_path')
                                        ->where('is_admin', 'on')
                                        ->where('type', 'pad')
                                        ->where('start_show_time', '<=', $currentTime)
                                        ->where('end_show_time', '>=', $currentTime)
                                        ->orderBy('id', 'ASC')
                                        ->first();

                $background = isset($backgroundData->bg_path) && $backgroundData->bg_path != '' ? $backgroundData->bg_path : '';

            }else{
                $eventsList = HmPad::select('title','icon_path','event_path')
                            ->where('status', 'on')
                            ->where('type', 'pad')
                            ->where('start_show_time', '<=', $currentTime)
                            ->where('end_show_time', '>=', $currentTime)
                            ->orderBy('id', 'DESC')
                            ->get();

                
                // get background for admin
                $backgroundData = HmPadBg::select('title','bg_path')
                                        ->where('status', 'on')
                                        ->where('type', 'pad')
                                        ->where('start_show_time', '<=', $currentTime)
                                        ->where('end_show_time', '>=', $currentTime)
                                        ->orderBy('id', 'ASC')
                                        ->first();

                $background = isset($backgroundData->bg_path) && $backgroundData->bg_path != '' ? $backgroundData->bg_path : '';

            }

            return response()->json([
                        'status' => true,
                        'content' => $eventsList,
                        'background' => $background
            ]);
        }

    }