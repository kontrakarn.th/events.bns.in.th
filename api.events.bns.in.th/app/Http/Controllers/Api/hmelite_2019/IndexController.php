<?php

    namespace App\Http\Controllers\Api\hmelite_2019;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DB;
    use Validator;

    use App\Models\hmelite_2019\Events;
    use App\Models\hmelite_2019\Member;
    use App\Models\hmelite_2019\MemberLogs;
    use App\Models\hmelite_2019\ItemSelect;
    use App\Models\hmelite_2019\Ranking;
    use App\Models\hmelite_2019\SendLogs;
    use App\Models\hmelite_2019\Reward;
    use App\Models\hmelite_2019\Deduct;

    class IndexController extends BnsEventController{

    	private $baseApi = 'http://api.apps.garena.in.th';
        private $events = false;

        private $diamondStartTime = null;
        private $diamondEndTime = null;
        private $year_month = null;

        public function __construct(Request $request)
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getJwtUserData($request);

            $this->events=Events::getEventByID(1);

            $this->diamondStartTime=Carbon::now()->startOfMonth()->toDateTimeString();
            $this->diamondEndTime=Carbon::now()->endOfMonth()->toDateTimeString();
            $this->year=(int) Carbon::now()->format('Y');
            $this->month=(int) Carbon::now()->format('m');
            $this->day=(int) Carbon::now()->format('d');
            $this->year_month=$this->year."-".$this->month;
            $this->previous_year=$this->year;
            $this->previous_month=$this->month-1;
            if($this->previous_month<1){
                $this->previous_month=12;
            }
            if($this->previous_month==12){
                $this->previous_year=$this->year-1;
            }
        }

        private function checkEventStatus()
        {

            //  if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            //     die(json_encode([
            //         'status' => false,
            //         'type' => 'no_permission',
            //         'message' => 'Sorry, you do not have permission.'
            //     ]));
            // }
            // dd($this->startTime,$this->endTime);
            if ((time() < strtotime($this->events->start_date) || time() > strtotime($this->events->end_date))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }


        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            // dd($this->userData);
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'vip'=>0,
                    'last_ip' => $this->getIP()
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->first();
            if (isset($counter)) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        private function getUserInfo($uid)
        {
            $userInfo = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'user_info',
                'uid' => $uid
            ]);
            $result = json_decode($userInfo, true);
            if (is_null($result) || $result['status'] == false) {
                return false;
            }
            return $result['response'];
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:HMELITE_2019:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events bns elite 2019.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function diamondBalance(int $uid)
        {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'balance',
                'uid' => $uid,
            ]);
        }

        private function setDiamondDeductData($deduct_diamond_amount = 0)
        {
            if ($deduct_diamond_amount == 0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamomds for garena events bns elite 2019 excusive shop',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function doGetDiamondHistory($ncid) {
            if(empty($ncid)){
                return false;
            }

            return Cache::remember('BNS:HMELITE_2019:EXCHANGE_TO_DIAMONDS_HISTORY_'.$this->year_month.'_' . $ncid, 5, function() use($ncid) {
                return json_decode($this->post_api($this->baseApi, [
                            'key_name' => 'bns',
                            'service' => 'query_cash_txn',
                            'user_id' => $ncid,//ncid
                            'start_time' => $this->diamondStartTime,
                            'end_time' => $this->diamondEndTime
                        ]));
            });
        }

        private function reduceItemList($type,$show_only = 0){
            $data_raw=ItemSelect::getItemSelectByType($type,$this->month,$this->year);
            $data=[];

            if($type=="excusive"){
                foreach($data_raw as $key=>$value){
                    if($this->is_accepted_ip() == true && ($value->active==1 || $value->active_test==1) || ($this->is_accepted_ip() == false && $value->active==1) ){
                        $data[]=[
                            'id'=>$value->id,
                            'product_title'=>$value->product_title,
                            'icon'=>$value->icon,
                            'price'=>$value->item_price,
                            'amt'=>$value->amt,
                        ];
                    }
                }
            }elseif($type=="ranking_3" || $type=="ranking_10" || $type=="vip_3"){
                foreach($data_raw as $key=>$value){
                    if($this->is_accepted_ip() == true && ($value->active==1 || $value->active_test==1) || ($this->is_accepted_ip() == false && $value->active==1) ){
                        $data[]=[
                            'id'=>$value->id,
                            'product_title'=>$value->product_title,
                            'icon'=>$value->icon,
                            'example_img'=>$value->example_img,
                            'amt'=>$value->amt,
                        ];
                    }
                }
            }elseif($type=="birthday"){
                foreach($data_raw as $key=>$value){
                    if($this->is_accepted_ip() == true && ($value->active==1 || $value->active_test==1) || ($this->is_accepted_ip() == false && $value->active==1) ){
                        if($show_only==1 && $value->show_only==1){
                            $data[]=[
                                'id'=>$value->id,
                                'product_title'=>$value->product_title,
                                'icon'=>$value->icon,
                                'amt'=>$value->amt,
                            ];
                        }elseif($show_only==0 && $value->show_only==0){
                            $data[]=[
                                'id'=>$value->id,
                                'product_title'=>$value->product_title,
                                'icon'=>$value->icon,
                                'amt'=>$value->amt,
                            ];
                        }

                    }
                }
            }else{
                foreach($data_raw as $key=>$value){
                    if($this->is_accepted_ip() == true && ($value->active==1 || $value->active_test==1) || ($this->is_accepted_ip() == false && $value->active==1) ){
                        $data[]=[
                            'id'=>$value->id,
                            'product_title'=>$value->product_title,
                            'icon'=>$value->icon,
                            'amt'=>$value->amt,
                        ];
                    }
                }
            }

            return $data;
        }

        private function reduceItemListPrev($type,$show_only = 0){

            $data_raw=ItemSelect::getItemSelectByType($type,$this->previous_month,$this->previous_year);
            $data=[];

            if($type=="excusive"){
                foreach($data_raw as $key=>$value){
                    if($this->is_accepted_ip() == true && ($value->active==1 || $value->active_test==1) || ($this->is_accepted_ip() == false && $value->active==1) ){
                        $data[]=[
                            'id'=>$value->id,
                            'product_title'=>$value->product_title,
                            'icon'=>$value->icon,
                            'price'=>$value->item_price,
                            'amt'=>$value->amt,
                        ];
                    }
                }
            }elseif($type=="ranking_3" || $type=="ranking_10" || $type=="vip_3"){
                foreach($data_raw as $key=>$value){
                    if($this->is_accepted_ip() == true && ($value->active==1 || $value->active_test==1) || ($this->is_accepted_ip() == false && $value->active==1) ){
                        $data[]=[
                            'id'=>$value->id,
                            'product_title'=>$value->product_title,
                            'icon'=>$value->icon,
                            'example_img'=>$value->example_img,
                            'amt'=>$value->amt,
                        ];
                    }
                }
            }elseif($type=="birthday"){
                foreach($data_raw as $key=>$value){
                    if($this->is_accepted_ip() == true && ($value->active==1 || $value->active_test==1) || ($this->is_accepted_ip() == false && $value->active==1) ){
                        if($show_only==1 && $value->show_only==1){
                            $data[]=[
                                'id'=>$value->id,
                                'product_title'=>$value->product_title,
                                'icon'=>$value->icon,
                                'amt'=>$value->amt,
                            ];
                        }elseif($show_only==0 && $value->show_only==0){
                            $data[]=[
                                'id'=>$value->id,
                                'product_title'=>$value->product_title,
                                'icon'=>$value->icon,
                                'amt'=>$value->amt,
                            ];
                        }

                    }
                }
            }else{
                foreach($data_raw as $key=>$value){
                    if($this->is_accepted_ip() == true && ($value->active==1 || $value->active_test==1) || ($this->is_accepted_ip() == false && $value->active==1) ){
                        $data[]=[
                            'id'=>$value->id,
                            'product_title'=>$value->product_title,
                            'icon'=>$value->icon,
                            'amt'=>$value->amt,
                        ];
                    }
                }
            }

            return $data;
        }

        private function reduceItemListRaw($type,$show_only = 0){
            $data_raw=ItemSelect::getItemSelectByType($type,$this->month,$this->year);
            if($type=="birthday"){
                foreach($data_raw as $key=>$value){
                    if($this->is_accepted_ip() == true && ($value->active==1 || $value->active_test==1) || ($this->is_accepted_ip() == false && $value->active==1) ){
                        if($show_only==0 && $value->show_only==1){
                            unset($data_raw[$key]);
                        }
                    }else{
                        unset($data_raw[$key]);
                    }
                }
            }else{
                foreach($data_raw as $key=>$value){
                    if($this->is_accepted_ip() == true && ($value->active==1 || $value->active_test==1) || ($this->is_accepted_ip() == false && $value->active==1) ){

                    }else{
                        unset($data_raw[$key]);
                    }
                }
            }


            return $data_raw;
        }
        private function reduceItemListRawPrev($type,$show_only = 0){
            $data_raw=ItemSelect::getItemSelectByType($type,$this->previous_month,$this->previous_year);
            if($type=="birthday"){
                foreach($data_raw as $key=>$value){
                    if($this->is_accepted_ip() == true && ($value->active==1 || $value->active_test==1) || ($this->is_accepted_ip() == false && $value->active==1) ){
                        if($show_only==0 && $value->show_only==1){
                            unset($data_raw[$key]);
                        }
                    }else{
                        unset($data_raw[$key]);
                    }
                }
            }else{
                foreach($data_raw as $key=>$value){
                    if($this->is_accepted_ip() == true && ($value->active==1 || $value->active_test==1) || ($this->is_accepted_ip() == false && $value->active==1) ){

                    }else{
                        unset($data_raw[$key]);
                    }
                }
            }


            return $data_raw;
        }

        public function getEventInfo(Request $request){

            if ($request->has('type') == false || $request['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $daily_item=$this->reduceItemList('daily');
            $birthday_item=$this->reduceItemList('birthday',1);
            $weekly_item=$this->reduceItemList('weekly');
            $daily_item_prev=$this->reduceItemListPrev('daily');
            $birthday_item_prev=$this->reduceItemListPrev('birthday',1);
            $weekly_item_prev=$this->reduceItemListPrev('weekly');
            $excusive_item_prev=$this->reduceItemListPrev('excusive');
            $excusive_item=$this->reduceItemList('excusive');
            $ranking_3_item=$this->reduceItemList('ranking_3');
            $ranking_10_item=$this->reduceItemList('ranking_10');
            $vip_3_item=$this->reduceItemList('vip_3');

            return response()->json([
                        'status' => true,
                        'data' =>[
                            'month'=>$this->month,
                            'previous_month'=>$this->previous_month,
                            'daily_item'=>$daily_item,
                            'weekly_item'=>$weekly_item,
                            'birthday_item'=>$birthday_item,
                            'daily_item_prev'=>$daily_item_prev,
                            'weekly_item_prev'=>$weekly_item_prev,
                            'birthday_item_prev'=>$birthday_item_prev,
                            'excusive_item'=>$excusive_item,
                            'excusive_item_prev'=>$excusive_item_prev,
                            'ranking_3_item'=>$ranking_3_item,
                            'ranking_10_item'=>$ranking_10_item,
                            'vip_3_item'=>$vip_3_item,
                        ]
                   ]);

        }


        private function setMemberInfo($uid){
            if(empty($uid)){
                return false;
            }
            $member = Member::where('uid', $uid)->first();

            $result_diamond = 0;

            $resultDiamond = $this->doGetDiamondHistory($member->ncid);
            if ($resultDiamond->status == true) {
                $contentDiamond = $resultDiamond->response;
                if (count($contentDiamond) > 0) {
                    $collectionDiamond = collect($contentDiamond);
                    $result_diamond += $collectionDiamond->sum('diamond');
                }
            }

            $remain_diamond=0;
            $percent=0;
            $vip_next=0;

            if($result_diamond>=$this->events->vip_3_diamond){
                $vip_next=3;
                $remain_diamond=0;
                $percent=100;
            }elseif($result_diamond>=$this->events->vip_2_diamond){
                $vip_next=2;
                $remain_diamond=$this->events->vip_3_diamond-$result_diamond;
                $percent=round($result_diamond/$this->events->vip_3_diamond*100);
            }elseif($result_diamond>=$this->events->vip_1_diamond){
                $vip_next=1;
                $remain_diamond=$this->events->vip_2_diamond-$result_diamond;
                $percent=round($result_diamond/$this->events->vip_2_diamond*100);
            }else{
                $remain_diamond=$this->events->vip_1_diamond-$result_diamond;
                $percent=round($result_diamond/$this->events->vip_1_diamond*100);
            }

            $play_daily=[
                'open'=>false,
                'played'=>true,
            ];

            $play_birthday=[
                'open'=>false,
                'played'=>true,
            ];
            $play_weekly=[
                'open'=>false,
                'played'=>true,
            ];
            $play_excusive=[
                'open'=>false,
            ];

            $today=Carbon::now()->toDateString();

            if($member->vip==3){

                $play_daily=$this->checkDailyPlay($member,$today);
                $play_birthday=$this->checkBirthdayPlay($member);
                $play_weekly=$this->checkWeeklyPlay($member,$today);
                $play_excusive['open']=true;

            }elseif($member->vip==2){

                $play_daily=$this->checkDailyPlay($member,$today);
                $play_birthday=$this->checkBirthdayPlay($member);
                $play_weekly=$this->checkWeeklyPlay($member,$today);

            }elseif($member->vip==1){

                $play_daily=$this->checkDailyPlay($member,$today);
                $play_birthday=$this->checkBirthdayPlay($member);

            }

            $diamond=0;
            $diamondBalanceRaw = $this->diamondBalance($member->uid);
            $diamondBalance = json_decode($diamondBalanceRaw);
            if ($diamondBalance->status) {
                $diamond = $diamondBalance->balance;
            }



            return [
                'profile'=>[
                    'uid'=>$member->uid,
                    'firstname'=>$member->firstname,
                    'lastname'=>$member->lastname,
                    'nickname'=>$member->nickname,
                    'tel'=>$member->tel,
                    'birthday'=>$member->birthday,
                    'email'=>$member->email,
                    'vip'=>$member->vip,
                    'vip_next'=>$vip_next,
                ],
                'default_profile'=>[
                    'uid'=>$member->uid,
                    'firstname'=>$member->firstname,
                    'lastname'=>$member->lastname,
                    'nickname'=>$member->nickname,
                    'tel'=>$member->tel,
                    'birthday'=>$member->birthday,
                    'email'=>$member->email,
                    'vip'=>$member->vip,
                    'vip_next'=>$vip_next,
                ],
                'daily'=>$play_daily,
                'birthday'=>$play_birthday,
                'weekly'=>$play_weekly,
                'excusive'=>$play_excusive,
                'collect_time_start'=>Carbon::now()->startOfMonth()->format('d/m/Y'),
                'collect_time_end'=>Carbon::now()->endOfMonth()->format('d/m/Y'),
                'remain_diamond' => number_format($remain_diamond),
                'percent' => $percent,
                'diamond'=> (int) $diamond,
            ];
        }

        public function checkDailyPlay($member,$date){

            $play_data=[
                'open'=>true,
                'played'=>true,
            ];

            $check_data=SendLogs::where('uid',$member->uid)->where('item_type','daily')->whereDate('created_at',$date)->first();
            if(!isset($check_data)){
                $play_data['played']=false;
            }

            return $play_data;
        }

        public function checkBirthdayPlay($member){

            $play_data=[
                'open'=>true,
                'played'=>true,
            ];

            $check_data=SendLogs::where('uid',$member->uid)->where('item_type','birthday')->whereYear('created_at',$this->year)->first();
            if(!isset($check_data)){
                if(Carbon::now()->format('m')==Carbon::parse($member->birthday)->format('m')){
                    $play_data['played']=false;
                }
            }

            return $play_data;
        }
        public function checkWeeklyPlay($member,$today){

            $play_data=[
                'open'=>true,
                'played'=>true,
            ];

            if($this->day==7 || $this->day==14 || $this->day==21 || $this->day==Carbon::now()->endOfMonth()->format('d')){
                $check_data=SendLogs::where('uid',$member->uid)->where('item_type','weekly')->whereDate('created_at',$today)->first();
                if(!isset($check_data)){
                    $play_data['played']=false;
                }
            }

            return $play_data;
        }

        public function getMemberInfo(Request $request){
            $this->checkEventStatus();

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'member_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];

            // get member info
            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $memberInfo = $this->setMemberInfo($uid);

            return response()->json([
                        'status' => true,
                        'data' => $memberInfo
                   ]);

        }

        public function getRanking(Request $request){
            $this->checkEventStatus();

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'ranking') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];

            // get member info
            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            //
            // $previous_year=$this->year;
            // $previous_month=$this->month-1;
            // if($previous_month<1){
            //     $previous_month=12;
            // }
            // if($previous_month==12){
            //     $previous_year=$this->year-1;
            // }

            $ranking = Ranking::select('nickname','no','vip')->whereMonth('ranking_at',$this->previous_month)->whereYear('ranking_at',$this->previous_year)->orderBy('no','ASC')->get();

            return response()->json([
                        'status' => true,
                        'data' => [
                            'ranking'=>$ranking
                        ]
                   ]);

        }

        public function updateMemberProfile(Request $request){
            // dd(1);
            $this->checkEventStatus();
            // dd($request->input);
            $validatedData = Validator::make($request->all(), [
               'type' => 'required',
               'firstname' => 'required|regex:/[\p{Latin}\p{Thai}\s]+$/u|max:64',
               'lastname' => 'required|regex:/[\p{Latin}\p{Thai}\s]+$/u|max:64',
               'nickname' => 'required|regex:/[\p{Latin}\p{Thai}\s0-9+]+$/u|max:64',
               'tel' => 'required|regex:/[0-9+]+$/u|max:10',
               'email'=>'required|email',
               'birthday'=>'required|date_format:Y-m-d',
           ],[
               'type.required'=>"Invalid parameter !!",
               'firstname.required'=>"โปรดกรอกชื่อ",
               'firstname.regex'=>"โปรดกรอกชื่อเป็น ภาษาไทยหรืออังกฤษ เท่านั้น",
               'firstname.max'=>"โปรดกรอกจำนวนไม่เกิน 64 ตัวอักษร",
               'lastname.required'=>"โปรดกรอกนามสกุล",
               'lastname.regex'=>"โปรดกรอกนามสกุลเป็น ภาษาไทยหรืออังกฤษ เท่านั้น",
               'lastname.max'=>"โปรดกรอกจำนวนไม่เกิน 64 ตัวอักษร",
               'nickname.required'=>"โปรดกรอกชื่อที่แสดงบนเว็บไซด์",
               'nickname.regex'=>"โปรดกรอกชื่อที่แสดงบนเว็บไซด์ เป็นภาษาไทย,ภาษาอังกฤษ หรือ ตัวเลข เท่านั้น",
               'nickname.max'=>"โปรดกรอกจำนวนไม่เกิน 64 ตัวอักษร",
               'tel.required'=>"โปรดกรอกเบอร์โทรศัพท์",
               'tel.regex'=>"โปรดกรอกเบอร์โทรศัพท์เฉพาะ ตัวเลข เท่านั้น",
               'tel.max'=>"โปรดกรอกจำนวนไม่เกิน 10 หลัก",
               'email.required'=>"โปรดกรอกอีเมล",
               'email.email'=>"รูปแบบอีเมลไม่ถูกต้อง",
               'birthday.required'=>"โปรดกรอกวันเกิด",
               'birthday.date_format'=>"รูปแบบวันเกิดไม่ถูกต้อง",
           ]);

           if ($validatedData->fails()) {
               return response()->json([
                              'status' => false,
                              'message' => $validatedData->errors()->first()
               ]);
           }

           if ($request->type!="update_member") {
               return response()->json([
                              'status' => false,
                              'message' => 'invalid paramater !'
               ]);
           }
           $uid = $this->userData['uid'];
           $member=Member::getMemberByUid($uid);
           if(!isset($member)){
               return response()->json([
                              'status' => false,
                              'message' => 'ไม่พบผู้เล่น'
               ]);
           }
           $nickname=$request->nickname;
           $chk_nickname = Member::getMemberByNickname($nickname);
           if(isset($chk_nickname) && $chk_nickname->uid!=$member->uid){
               return response()->json([
                              'status' => false,
                              'message' => 'มีผู้ใช้ชื่อที่แสดงบนเว็บไซด์นี้แล้ว'
               ]);
           }


           $firstname=$request->firstname;
           $lastname=$request->lastname;
           $tel=$request->tel;
           $email=$request->email;
           $birthday=$request->birthday;

           $member->firstname=$firstname;
           $member->lastname=$lastname;
           $member->nickname=$nickname;
           $member->tel=$tel;
           $member->email=$email;
           if($member->birthday=="0000-00-00"){
               $member->birthday=$birthday;
           }

           $member->save();

           $memberlogs=new MemberLogs;
           $getmaxno_edit=MemberLogs::where('uid',$member->uid)->max('no_edit');
           $memberlogs->no_edit=$getmaxno_edit+1;
           $memberlogs->uid=$member->uid;
           $memberlogs->username=$member->username;
           $memberlogs->ncid=$member->ncid;
           $memberlogs->firstname=$firstname;
           $memberlogs->lastname=$lastname;
           $memberlogs->nickname=$nickname;
           $memberlogs->tel=$tel;
           $memberlogs->email=$email;
           $memberlogs->birthday=$birthday;
           $memberlogs->vip=$member->vip;
           $memberlogs->last_ip=$this->getIP();
           $memberlogs->save();

           $memberInfo = $this->setMemberInfo($uid);

           return response()->json([
                       'status' => true,
                       'data' => $memberInfo
                  ]);
        }

        public function getCheckUid(Request $request)
        {
            $this->checkEventStatus();

            if ($request->has('type') == false && $request->type != 'check_uid') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($request->has('uid') == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $uid = $this->userData['uid'];

            if($request->has('uid') && $request->uid != ''){

                // get member info
                $member = Member::where('uid', $uid)->first();
                if(isset($member) && empty($member)){
                    return response()->json([
                                'status' => false,
                                'message' => 'No member data.'
                            ]);
                }

                $ncid = $member->ncid;

                $target_uid = intval($request->uid);
                $targetInfo = [];
                $targetInfo = $this->getUserInfo($target_uid);

                if ($uid == $target_uid) {
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งไอเทมให้<br /> UID ของตัวเองได้'
                    ]);
                }

                if (is_array($targetInfo) && !empty($targetInfo['user_name'])){
                    return response()->json([
                        'status' => true,
                        'data' => [
                            'target_uid' => $target_uid,
                            'target_username' => $targetInfo['user_name']
                        ]
                    ]);
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'ขออภัยไม่มีข้อมูล UID นี้'
                    ]);
                }

            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

        }

        public function playDaily(Request $request){
            $this->checkEventStatus();

            if ($request->has('type') == false && $request->type != 'play_daily') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $uid=$this->userData['uid'];
            $member=Member::getMemberByUid($uid);
            if($member->vip<1){
                return response()->json([
                            'status' => false,
                            'message' => 'คุณไม่มีสิทธ์ในการใช้งานในเดือนนี้'
                ]);
            }

            $date=Carbon::now()->toDateString();
            $check_data=SendLogs::where('uid',$member->uid)->where('item_type','daily')->whereDate('created_at',$date)->first();
            if(isset($check_data)){
                return response()->json([
                            'status' => false,
                            'message' => 'เล่นได้วันละ 1 ครั้งเท่านั้น'
                ]);
            }

            //random item
            $item_list=$this->reduceItemListRawPrev('daily');
            // dd($item_list);
            $packageInfo=Reward::getRandomRewardInfo($item_list);
            if($packageInfo==false){
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (1)'
                ]);
            }
            //add logs
            $goods_data = []; //good data for send item group
            $packageId = $packageInfo->product_id;
            $packageTitle = $packageInfo->product_title;
            $packageAmt = $packageInfo->amt;
            $item_return=[[
                'id'=>$packageInfo->id,
                'product_title'=>$packageTitle,
                'icon'=>$packageInfo->icon,
                'amt'=>$packageAmt
            ]];

            $goods_data[] = [
               'goods_id' => $packageId,
               'purchase_quantity' => $packageAmt,
               'purchase_amount' => 0,
               'category_id' => 40
            ];

            $sendItemLog= new SendLogs;
            $sendItemLog->uid=$member->uid;
            $sendItemLog->ncid=$member->ncid;
            $sendItemLog->nickname=$member->nickname;
            $sendItemLog->item_type='daily';
            $sendItemLog->item_no=$packageInfo->id;
            $sendItemLog->product_title=$packageTitle;
            $sendItemLog->product_id=$packageId;
            $sendItemLog->product_amt=$packageAmt;
            $sendItemLog->icon=$packageInfo->icon;
            $sendItemLog->item_date=$packageInfo->item_date;
            $sendItemLog->status='pending';
            $sendItemLog->send_item_purchase_id=0;
            $sendItemLog->send_item_purchase_status=0;
            $sendItemLog->send_item_purchase_status=0;
            $sendItemLog->goods_data=json_encode($goods_data);
            $sendItemLog->last_ip=$this->getIP();

            if(!$sendItemLog->save()){
                return response()->json([
                            'status' => true,
                            'message' => "เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (2)",
                        ]);
            }

            $send_result_raw = $this->dosendItem($member->ncid, $goods_data);

            //send item
            $send_result = json_decode($send_result_raw);
            $sendlog_id=$sendItemLog->id;
            $sendup=SendLogs::find($sendlog_id);
           if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
               $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
               $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
               $sendup->status='success';
               $sendup->save();

               $memberinfo = $this->setMemberInfo($uid);


               return response()->json([
                           'status' => true,
                           'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมายภายในเกม",
                           'data' => $memberinfo,
                           'item_get'=>$item_return
                       ]);
           }else{
               $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
               $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
               $sendup->status='fail';
               $sendup->save();

               return response()->json([
                           'status' => false,
                           'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                       ]);
           }
        }

        public function playWeekly(Request $request){
            $this->checkEventStatus();

            if ($request->has('type') == false && $request->type != 'play_weekly') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $uid=$this->userData['uid'];
            $member=Member::getMemberByUid($uid);
            if($member->vip<2){
                return response()->json([
                            'status' => false,
                            'message' => 'คุณไม่มีสิทธ์ในการใช้งานในเดือนนี้'
                ]);
            }

            $date=Carbon::now()->toDateString();

            if($this->day==7 || $this->day==14 || $this->day==21 || $this->day==Carbon::now()->endOfMonth()->format('d')){
                $check_data=SendLogs::where('uid',$member->uid)->where('item_type','weekly')->whereDate('created_at',$date)->first();
                if(isset($check_data)){
                    return response()->json([
                                'status' => false,
                                'message' => 'อาทิตย์นี้คุณได้เล่นไปแล้ว'
                    ]);
                }
            }else{
                return response()->json([
                            'status' => false,
                            'message' => 'ยังไม่ถึงวันที่กำหนด'
                ]);
            }

            //random item
            $item_list=$this->reduceItemListRawPrev('weekly');
            $packageInfo=Reward::getRandomRewardInfo($item_list);
            if($packageInfo==false){
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (1)'
                ]);
            }
            //add logs
            $goods_data = []; //good data for send item group
            $packageId = $packageInfo->product_id;
            $packageTitle = $packageInfo->product_title;
            $packageAmt = $packageInfo->amt;
            $item_return=[[
                'id'=>$packageInfo->id,
                'product_title'=>$packageTitle,
                'icon'=>$packageInfo->icon,
                'amt'=>$packageAmt,
            ]];

            $goods_data[] = [
               'goods_id' => $packageId,
               'purchase_quantity' => $packageAmt,
               'purchase_amount' => 0,
               'category_id' => 40
            ];

            $sendItemLog= new SendLogs;
            $sendItemLog->uid=$member->uid;
            $sendItemLog->ncid=$member->ncid;
            $sendItemLog->nickname=$member->nickname;
            $sendItemLog->item_type='weekly';
            $sendItemLog->item_no=$packageInfo->id;
            $sendItemLog->product_title=$packageTitle;
            $sendItemLog->product_id=$packageId;
            $sendItemLog->product_amt=$packageAmt;
            $sendItemLog->icon=$packageInfo->icon;
            $sendItemLog->item_date=$packageInfo->item_date;
            $sendItemLog->status='pending';
            $sendItemLog->send_item_purchase_id=0;
            $sendItemLog->send_item_purchase_status=0;
            $sendItemLog->send_item_purchase_status=0;
            $sendItemLog->goods_data=json_encode($goods_data);
            $sendItemLog->last_ip=$this->getIP();

            if(!$sendItemLog->save()){
                return response()->json([
                            'status' => true,
                            'message' => "เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (2)",
                        ]);
            }

            $send_result_raw = $this->dosendItem($member->ncid, $goods_data);

            //send item
            $send_result = json_decode($send_result_raw);
            $sendlog_id=$sendItemLog->id;
            $sendup=SendLogs::find($sendlog_id);
           if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
               $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
               $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
               $sendup->status='success';
               $sendup->save();

               $memberinfo = $this->setMemberInfo($uid);


               return response()->json([
                           'status' => true,
                           'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมายภายในเกม",
                           'data' => $memberinfo,
                           'item_get'=>$item_return
                       ]);
           }else{
               $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
               $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
               $sendup->status='fail';
               $sendup->save();

               return response()->json([
                           'status' => false,
                           'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                       ]);
           }
        }

        public function playBirthday(Request $request){
            $this->checkEventStatus();

            if ($request->has('type') == false && $request->type != 'play_birthday') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $uid=$this->userData['uid'];
            $member=Member::getMemberByUid($uid);
            if($member->vip<1){
                return response()->json([
                            'status' => false,
                            'message' => 'คุณไม่มีสิทธ์ในการใช้งานในเดือนนี้'
                ]);
            }

            $check_data=SendLogs::where('uid',$member->uid)->where('item_type','birthday')->whereYear('created_at',$this->year)->first();
            if(isset($check_data)){
                return response()->json([
                            'status' => false,
                            'message' => 'คุณได้รับของวันเกิดไปแล้วในปีนี้'
                ]);
            }else{
                if(Carbon::now()->format('m')!=Carbon::parse($member->birthday)->format('m')){
                    return response()->json([
                                'status' => false,
                                'message' => 'ยังไม่ถึงวันเกิดของคุณ ไม่สามารถรับได้'
                    ]);
                }
            }

            $item_list=$this->reduceItemListRawPrev('birthday',0);
            if(count($item_list)!=1){
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (1)'
                ]);
            }
            $re_item_list=array_values($item_list);
            $packageInfo=$re_item_list[0];
            if(!isset($packageInfo)){
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (2)'
                ]);
            }
            //add logs
            $goods_data = []; //good data for send item group
            $packageId = $packageInfo->product_id;
            $packageTitle = $packageInfo->product_title;
            $packageAmt = $packageInfo->amt;
            $item_return=[[
                'id'=>$packageInfo->id,
                'product_title'=>$packageTitle,
                'icon'=>$packageInfo->icon,
                'amt'=>$packageAmt
            ]];

            $goods_data[] = [
               'goods_id' => $packageId,
               'purchase_quantity' => $packageAmt,
               'purchase_amount' => 0,
               'category_id' => 40
            ];

            $sendItemLog= new SendLogs;
            $sendItemLog->uid=$member->uid;
            $sendItemLog->ncid=$member->ncid;
            $sendItemLog->nickname=$member->nickname;
            $sendItemLog->item_type='birthday';
            $sendItemLog->item_no=$packageInfo->id;
            $sendItemLog->product_title=$packageTitle;
            $sendItemLog->product_id=$packageId;
            $sendItemLog->product_amt=$packageAmt;
            $sendItemLog->icon=$packageInfo->icon;
            $sendItemLog->item_date=$packageInfo->item_date;
            $sendItemLog->status='pending';
            $sendItemLog->send_item_purchase_id=0;
            $sendItemLog->send_item_purchase_status=0;
            $sendItemLog->send_item_purchase_status=0;
            $sendItemLog->goods_data=json_encode($goods_data);
            $sendItemLog->last_ip=$this->getIP();

            if(!$sendItemLog->save()){
                return response()->json([
                            'status' => true,
                            'message' => "เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (3)",
                        ]);
            }

            $send_result_raw = $this->dosendItem($member->ncid, $goods_data);

            //send item
            $send_result = json_decode($send_result_raw);
            $sendlog_id=$sendItemLog->id;
            $sendup=SendLogs::find($sendlog_id);
           if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
               $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
               $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
               $sendup->status='success';
               $sendup->save();

               $memberinfo = $this->setMemberInfo($uid);


               return response()->json([
                           'status' => true,
                           'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมายภายในเกม",
                           'data' => $memberinfo,
                           'item_get'=>$item_return
                       ]);
           }else{
               $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
               $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
               $sendup->status='fail';
               $sendup->save();

               return response()->json([
                           'status' => false,
                           'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                       ]);
           }
        }

        public function buyExcusiveOwner(Request $request){
            $this->checkEventStatus();

            if ($request->has('type') == false && $request->type != 'buy_excusive_owner') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($request->has('id') == false || $request->id=="" ||$request->id==0) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'โปรดเลือกไอเทมที่ต้องการซื้อ'
                ]);
            }
            $id=$request->id;

            $uid=$this->userData['uid'];
            $member=Member::getMemberByUid($uid);
            if($member->vip<3){
                return response()->json([
                            'status' => false,
                            'message' => 'คุณไม่มีสิทธ์ในการใช้งานในเดือนนี้'
                ]);
            }

            $item_list=$this->reduceItemListRawPrev('excusive');
            // dd($item_list);
            $find_item=collect($item_list)->where('id',$id);
            if(count($find_item)==0){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบรายการที่ต้องการซื้อ'
                ]);
            }
            $re_item_list=array_values($find_item->toArray());

            $packageInfo=$re_item_list[0];
            // dd($packageInfo);
            if(!isset($packageInfo)){
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (2)'
                ]);
            }
            //get wallet
            $diamondBalanceRaw = $this->diamondBalance($uid);
            $diamondBalance = json_decode($diamondBalanceRaw);
            if (!$diamondBalance->status) {
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (3)'
                ]);
            }

            //check diamonds
            if ($diamondBalance->balance < $packageInfo->item_price) {
                return response()->json([
                            'status' => false,
                            'message' => 'จำนวนไดมอนของคุณไม่เพียงพอ'
                ]);
            }

            //deduct logs
            $deduct_logs = new Deduct;
            $deduct_logs->uid=$member->uid;
            $deduct_logs->ncid=$member->ncid;
            $deduct_logs->nickname=$member->nickname;
            $deduct_logs->type="excusive";
            $deduct_logs->item_no=$packageInfo->id;
            $deduct_logs->diamond=$packageInfo->item_price;
            $deduct_logs->status='pending';
            $deduct_logs->last_ip=$this->getIP();

            if(!$deduct_logs->save()){
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (4)'
                ]);
            }

            $transaction_id=$deduct_logs->id;

            $deductData = $this->setDiamondDeductData($packageInfo->item_price);

            $resp_deduct_raw = $this->deductDiamond($member->ncid, $deductData);
            $resp_deduct = json_decode($resp_deduct_raw);
            if (!is_object($resp_deduct) || is_null($resp_deduct) == true) {
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด ไม่สามารถหักไดมอนด์ได้'
                ]);
            }

            $updeduct=Deduct::find($transaction_id);
            if(!isset($updeduct)){
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (5)'
                ]);
            }
            $updeduct->before_deduct_diamond=$diamondBalance->balance;
            $updeduct->after_deduct_diamond=$diamondBalance->balance - $packageInfo->item_price;
            $updeduct->deduct_purchase_id=$resp_deduct->response->purchase_id ? : 0;
            $updeduct->deduct_purchase_status=$resp_deduct->response->purchase_status ? : 0;
            $updeduct->deduct_data=json_encode($deductData);

            if($resp_deduct->status){
                $updeduct->status="success";
                $updeduct->save();
            }else{
                $updeduct->status="fail";
                $updeduct->save();
                return response()->json([
                            'status' => false,
                            'message' => 'หักไดมอนด์ไม่สำเร็จ โปรดติดต่อ Customer Support'
                ]);
            }

            //add logs
            $goods_data = []; //good data for send item group
            $packageId = $packageInfo->product_id;
            $packageTitle = $packageInfo->product_title;
            $packageAmt = $packageInfo->amt;
            $item_return=[[
                'id'=>$packageInfo->id,
                'product_title'=>$packageTitle,
                'icon'=>$packageInfo->icon,
                'amt'=>$packageAmt
            ]];

            $goods_data[] = [
               'goods_id' => $packageId,
               'purchase_quantity' => $packageAmt,
               'purchase_amount' => 0,
               'category_id' => 40
            ];

            $sendItemLog= new SendLogs;
            $sendItemLog->transaction_id=$transaction_id;
            $sendItemLog->uid=$member->uid;
            $sendItemLog->ncid=$member->ncid;
            $sendItemLog->nickname=$member->nickname;
            $sendItemLog->item_type='excusive';
            $sendItemLog->item_no=$packageInfo->id;
            $sendItemLog->product_title=$packageTitle;
            $sendItemLog->product_id=$packageId;
            $sendItemLog->product_amt=$packageAmt;
            $sendItemLog->item_price=$packageInfo->item_price;
            $sendItemLog->icon=$packageInfo->icon;
            $sendItemLog->item_date=$packageInfo->item_date;
            $sendItemLog->status='pending';
            $sendItemLog->send_item_purchase_id=0;
            $sendItemLog->send_item_purchase_status=0;
            $sendItemLog->send_item_purchase_status=0;
            $sendItemLog->goods_data=json_encode($goods_data);
            $sendItemLog->item_status='owner';
            $sendItemLog->last_ip=$this->getIP();

            if(!$sendItemLog->save()){
                return response()->json([
                            'status' => true,
                            'message' => "เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (6)",
                        ]);
            }

            $send_result_raw = $this->dosendItem($member->ncid, $goods_data);

            //send item
            $send_result = json_decode($send_result_raw);
            $sendlog_id=$sendItemLog->id;
            $sendup=SendLogs::find($sendlog_id);
           if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
               $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
               $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
               $sendup->status='success';
               $sendup->save();

               $memberinfo = $this->setMemberInfo($uid);


               return response()->json([
                           'status' => true,
                           'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมายภายในเกม",
                           'data' => $memberinfo,
                           'item_get'=>$item_return
                       ]);
           }else{
               $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
               $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
               $sendup->status='fail';
               $sendup->save();

               return response()->json([
                           'status' => false,
                           'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                       ]);
           }
        }

        public function buyExcusiveOther(Request $request){
            $this->checkEventStatus();

            if ($request->has('type') == false && $request->type != 'buy_excusive_other') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($request->has('id') == false || $request->id=="" ||$request->id==0) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'โปรดเลือกไอเทมที่ต้องการส่ง'
                ]);
            }

            if ($request->has('target_uid') == false || $request->target_uid=="" ||$request->target_uid==0) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'โปรดกรอก uid ที่ต้องการรับไอเทม'
                ]);
            }

            $id=$request->id;
            $target_uid=$request->target_uid;

            $uid=$this->userData['uid'];
            $member=Member::getMemberByUid($uid);
            if($member->vip<3){
                return response()->json([
                            'status' => false,
                            'message' => 'คุณไม่มีสิทธ์ในการใช้งานในเดือนนี้'
                ]);
            }

            $targetInfo = $this->getUserInfo($target_uid);
            if(!isset($targetInfo['user_name'])){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบข้อมูลผู้ที่ต้องการรับไอเท็ม'
                        ]);
            }

            $target_username = $targetInfo['user_name'];
            $target_ncid = $targetInfo['user_id']; // ncid recipient

            if ($member->uid == $target_uid) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งไอเทมให้<br /> UID ของตัวเองได้'
                ]);
            }

            $item_list=$this->reduceItemListRawPrev('excusive');
            // dd($item_list);
            $find_item=collect($item_list)->where('id',$id);
            if(count($find_item)==0){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบรายการที่ต้องการซื้อ'
                ]);
            }
            $re_item_list=array_values($find_item->toArray());

            $packageInfo=$re_item_list[0];
            $total_price=$packageInfo->item_price+$this->events->send_price;
            // dd($packageInfo);
            if(!isset($packageInfo)){
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (2)'
                ]);
            }
            //get wallet
            $diamondBalanceRaw = $this->diamondBalance($uid);
            $diamondBalance = json_decode($diamondBalanceRaw);
            if (!$diamondBalance->status) {
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (3)'
                ]);
            }

            //check diamonds
            if ($diamondBalance->balance < $total_price) {
                return response()->json([
                            'status' => false,
                            'message' => 'จำนวนไดมอนของคุณไม่เพียงพอ'
                ]);
            }

            //deduct logs
            $deduct_logs = new Deduct;
            $deduct_logs->uid=$member->uid;
            $deduct_logs->ncid=$member->ncid;
            $deduct_logs->nickname=$member->nickname;
            $deduct_logs->type="excusive";
            $deduct_logs->item_no=$packageInfo->id;
            $deduct_logs->diamond=$packageInfo->item_price;
            $deduct_logs->status='pending';
            $deduct_logs->last_ip=$this->getIP();

            if(!$deduct_logs->save()){
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (4)'
                ]);
            }

            $deduct_send_item = new Deduct;
            $deduct_send_item->uid=$member->uid;
            $deduct_send_item->ncid=$member->ncid;
            $deduct_send_item->nickname=$member->nickname;
            $deduct_send_item->type="exchange";
            $deduct_send_item->diamond=$this->events->send_price;
            $deduct_send_item->status='pending';
            $deduct_send_item->last_ip=$this->getIP();

            if(!$deduct_send_item->save()){
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (4)'
                ]);
            }

            $transaction_id=$deduct_logs->id;
            $send_to_deduct_id=$deduct_send_item->id;

            $deductData = $this->setDiamondDeductData($total_price);
            $resp_deduct_raw = $this->deductDiamond($member->ncid, $deductData);
            $resp_deduct = json_decode($resp_deduct_raw);
            if (!is_object($resp_deduct) || is_null($resp_deduct) == true) {
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด ไม่สามารถหักไดมอนด์ได้'
                ]);
            }

            $updeduct=Deduct::find($transaction_id);
            $updeduct_send=Deduct::find($send_to_deduct_id);
            if(!isset($updeduct)){
                return response()->json([
                            'status' => false,
                            'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (5)'
                ]);
            }

            $updeduct->before_deduct_diamond=$diamondBalance->balance;
            $updeduct->after_deduct_diamond=$diamondBalance->balance - $packageInfo->item_price;
            $updeduct->deduct_purchase_id=$resp_deduct->response->purchase_id ? : 0;
            $updeduct->deduct_purchase_status=$resp_deduct->response->purchase_status ? : 0;
            $updeduct->deduct_data=json_encode($deductData);

            $updeduct_send->before_deduct_diamond=$diamondBalance->balance;
            $updeduct_send->after_deduct_diamond=$diamondBalance->balance - $packageInfo->item_price - $this->events->send_price;
            $updeduct_send->deduct_purchase_id=$resp_deduct->response->purchase_id ? : 0;
            $updeduct_send->deduct_purchase_status=$resp_deduct->response->purchase_status ? : 0;
            $updeduct_send->deduct_data=json_encode($deductData);

            if($resp_deduct->status){
                $updeduct->status="success";
                $updeduct_send->status="success";
                $updeduct->save();
                $updeduct_send->save();
            }else{
                $updeduct->status="fail";
                $updeduct_send->status="fail";
                $updeduct->save();
                $updeduct_send->save();
                return response()->json([
                            'status' => false,
                            'message' => 'หักไดมอนด์ไม่สำเร็จ โปรดติดต่อ Customer Support'
                ]);
            }

            //add logs
            $goods_data = []; //good data for send item group
            $packageId = $packageInfo->product_id;
            $packageTitle = $packageInfo->product_title;
            $packageAmt = $packageInfo->amt;
            $item_return=[[
                'id'=>$packageInfo->id,
                'product_title'=>$packageTitle,
                'icon'=>$packageInfo->icon,
                'amt'=>$packageAmt
            ]];

            $goods_data[] = [
               'goods_id' => $packageId,
               'purchase_quantity' => $packageAmt,
               'purchase_amount' => 0,
               'category_id' => 40
            ];

            $sendItemLog= new SendLogs;
            $sendItemLog->transaction_id=$transaction_id;
            $sendItemLog->uid=$member->uid;
            $sendItemLog->ncid=$member->ncid;
            $sendItemLog->nickname=$member->nickname;
            $sendItemLog->item_type='excusive';
            $sendItemLog->item_no=$packageInfo->id;
            $sendItemLog->product_title=$packageTitle;
            $sendItemLog->product_id=$packageId;
            $sendItemLog->product_amt=$packageAmt;
            $sendItemLog->item_price=$packageInfo->item_price;
            $sendItemLog->icon=$packageInfo->icon;
            $sendItemLog->item_date=$packageInfo->item_date;
            $sendItemLog->status='pending';
            $sendItemLog->send_item_purchase_id=0;
            $sendItemLog->send_item_purchase_status=0;
            $sendItemLog->send_item_purchase_status=0;
            $sendItemLog->goods_data=json_encode($goods_data);
            $sendItemLog->item_status='other';
            $sendItemLog->send_to_deduct_id=$send_to_deduct_id;
            $sendItemLog->send_to_uid=$target_uid;
            $sendItemLog->send_to_name=$target_username;
            $sendItemLog->last_ip=$this->getIP();

            if(!$sendItemLog->save()){
                return response()->json([
                            'status' => true,
                            'message' => "เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง (6)",
                        ]);
            }

            $send_result_raw = $this->dosendItem($target_ncid, $goods_data);

            //send item
            $send_result = json_decode($send_result_raw);
            $sendlog_id=$sendItemLog->id;
            $sendup=SendLogs::find($sendlog_id);
           if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
               $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
               $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
               $sendup->status='success';
               $sendup->save();

               $memberinfo = $this->setMemberInfo($uid);


               return response()->json([
                           'status' => true,
                           'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมายภายในเกม",
                           'data' => $memberinfo,
                       ]);
           }else{
               $sendup->send_item_purchase_id=$send_result->response->purchase_id ?: 0;
               $sendup->send_item_purchase_status=$send_result->response->purchase_status ?: 0;
               $sendup->status='fail';
               $sendup->save();

               return response()->json([
                           'status' => false,
                           'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                       ]);
           }
        }
    }
