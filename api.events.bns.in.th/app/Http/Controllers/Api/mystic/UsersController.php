<?php

    namespace App\Http\Controllers\Api\mystic;

    use App\Http\Controllers\Api\BnsEventController;

    use App\Models\mystic\Product;
    use App\Models\mystic\Productgrouplimit;
    use App\Models\mystic\Users;
    use App\Models\mystic\Userspecialproduct;

class UsersController extends BnsEventController{
        private $baseApi = 'http://api.apps.garena.in.th';

        function userCanreset($uid){
            $canreset = Productgrouplimit::where('uid',$uid)->where("product_group_id",1)->get();
            if(count($canreset) > 0)
            {
                $canreset = true;
            }else{
                $canreset = false;
            }
            return $canreset;
        }

        function GetAmountUserKeys($uid){
            $updateuser = Users::where('uid',$uid)->value('id');
            $data = Users::find($updateuser);
            $data = $data->hongmoon_key;
            if($data == NULL){
                $data = 0;
            }
            return intval($data);
        }
        function UpdateUserKey($uid,$sum,$method){
            $updateuser = Users::where('uid',$uid)->value('id');
            $updateuser = Users::find($updateuser);
            if($updateuser->hongmoon_key == NULL || $updateuser->hongmoon_key==''){
                $updateuser->hongmoon_key = 0;
            }
            if($method == "plus"){
                $sum = $sum+$updateuser->hongmoon_key;
            }else if($method == "minus"){
                $sum = $sum;
            }
            $updateuser->hongmoon_key = $sum;
            $updateuser->save();
        }
        function GetUserSlotNoReset($uid){
            $productslot = Productgrouplimit::where('uid',$uid)->get();
           return $productslot;
        }

        function GetUserSpecialProduct($uid){
            $productselect = Userspecialproduct::where('uid',$uid)->value('special_item_id');
            $typeselect = Userspecialproduct::where('uid',$uid)->value('type_select');
            $product = Product::find($productselect);
            if($product == NULL){
                return NULL;
            }
            $array = array("product_name" => $product->th_name,"select_type" =>intval($typeselect),"product_img" => $product->product_img, );
           return $array;
        }

        public function getProductslot($uid){
            return Productgrouplimit::with(['Product'])->where('uid',$uid)->get();
        }

        private function apiDiamondBalance(int $uid)
        {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'balance',
                'uid' => $uid,
            ]);
        }
        public function setDiamondsBalance($uid)
        {

            $diamondBalanceRaw = $this->apiDiamondBalance($uid);
            $diamondBalance = json_decode($diamondBalanceRaw);

            $diamonds = 0;
            if ($diamondBalance->status == true) {
                $diamonds = intval($diamondBalance->balance);
            }

            return $diamonds;
        }

        protected function getNcidByUid(int $uid): string
        {
            return Users::where('uid', $uid)->value('ncid');
        }

    }
