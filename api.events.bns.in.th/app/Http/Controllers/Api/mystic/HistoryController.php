<?php

    namespace App\Http\Controllers\Api\mystic;

    use App\Http\Controllers\Api\BnsEventController;
    use Carbon\Carbon;



    use App\Models\mystic\History;

    class HistoryController extends BnsEventController{

        private $baseApi = 'http://api.apps.garena.in.th';

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip  = array_shift($ips);
            }
            return $ip;
        }
        public function GetHistory($uid){
            $history = History::with(['HistoryType','KeyPackage','Product'])->where("uid",$uid)->orderBy("id","DESC")->get();
            // /return $history;
            $data = [];
            foreach ($history as $hi) {
                //return $hi->product;
                unset($hi->id,$hi->created_at,$hi->updated_at,$hi->uid,$hi->product_key,$hi->HistoryType);
                //array_push($data,$hi);
                if($hi->history_type_id == 1){
                    array_push($data,array("product_name" => $hi->KeyPackage->th_name,"amount" =>$hi->KeyPackage->amount,"bonus" =>$hi->KeyPackage->bonus,"date" =>$hi->get_date_string,"time" =>$hi->get_time_string));
                }else{
                    array_push($data,array("product_name" => $hi->product->th_name,"amount" =>$hi->product->amount,"bonus" =>$hi->product->bonus,"date" =>$hi->get_date_string,"time" =>$hi->get_time_string));
                }
            }
            return response()->json([
                'status' => true,
                'data' => $data,
                'message' => '',
            ]);
        }
        public function SaveHistory($product_or_key_id,$history_type_id,$amount,$uid){
            date_default_timezone_set("Asia/Bangkok");

            $date = Carbon::now()->format("d/m/Y");
            $time = Carbon::now()->format("H:i:s");
            $save = New History;
            $save->history_type_id = $history_type_id;
            if($history_type_id == 1){
                $save->key_id = $product_or_key_id;
            }else{
                $save->product_id = $product_or_key_id;
            }
            $save->amount = $amount;
            $save->uid = $uid;
            $save->get_date_string = $date;
            $save->get_time_string = $time;
            $save->last_ip = $this->getIP();

            $save->save();
            return $save->id;
        }
        public function UpdateHistory($savehistoryid,$senditemstatus,$senditempurchaseid,$senditempurchasestatus,$status){
            date_default_timezone_set("Asia/Bangkok");
            $date = date("d/m/Y");
            $time = date("H:i:s");
            $update =  History::find($savehistoryid);
            $update->send_item_status = $senditemstatus;
            $update->send_item_purchase_id = $senditempurchaseid;
            $update->send_item_purchase_status = $senditempurchasestatus;
            $update->status = $status;
            $update->save();
        }
    }
