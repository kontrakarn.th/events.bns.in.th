<?php
namespace App\Http\Controllers\Api;

// use App\Http\Controllers\Api\ApiController;
// use App\Http\Controllers\Auth\SSOController;
use App\Http\Controllers\Auth\OauthController;

use Cache;
use Carbon\Carbon;

class BnsEventController extends OauthController
{

    /**
     * IP Address of the user
     */
    protected $ip = '';
    protected $apiUrl = 'http://api.apps.garena.in.th';

    /**
     * Contructor of this class
     */
    function __construct()
    {
        $this->init();
    }

    protected function isIngame(){
        $browser = $_SERVER['HTTP_USER_AGENT'];

        return strpos($browser, 'BnsIngameBrowser');
    }

    protected function checkIsMaintenance()
    {
        $dayofweek = date('w', strtotime(Carbon::toDay()));

        $currTime = time();
        $maintenanceStart = strtotime(Carbon::now()->toDateString().' 05:00:00');
        $maintenanceEnd = strtotime(Carbon::now()->toDateString().' 08:00:00');

        // array = 3 = wednesday
        if($dayofweek == 3 && $currTime >= $maintenanceStart && $currTime <= $maintenanceEnd){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Check admin IP address
     */
    protected function isAdmin()
    {
        $ips = array();
        $ips[] = '112.121.131.234';
        $ips[] = '110.168.229.249';
        $ips[] = '180.183.119.151';
        $ips[] = '27.254.46.174';
        return in_array($this->ip, $ips);
    }

    /**
     * Initialize this class
     */
    private function init()
    {
        // check ip
        $this->ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $this->ip = array_shift($ips);
        }
    }

    /**
     * Send the given item to the player
     *
     * @param int $uid
     * @param int $itemId
     * @param int $count
     * @param int $timeInSeconds
     * @param string $remark
     */
    protected function send_item($uid, $itemId, $count = 1, $timeInSeconds = 0, $remark = '')
    {
        // todo: waiting for gameinfo api
        return false;

        /*$data = array(
            'key_name' => 'bns_api',
            'service' => 'send_item',
            'uid' => $uid,
            'item_id' => $itemId,
            'count' => $count,
            'time_second' => $timeInSeconds,
            'remark' => $remark,
        );
        $response = $this->post_api($this->apiUrl, $data);
        if (empty($response)) {
            return false;
        }
        return json_decode($response, true);*/
    }

    /**
     * Get information of the user
     *
     * @param int $uid
     */
    protected function get_info($uid)
    {
        // todo: waiting for gameinfo api
        return false;

        /*$data = array(
            'key_name' => 'bns_api',
            'service' => 'get_account_info',
            'uid' => $uid
        );
        $response = $this->post_api($this->apiUrl, $data);
        if (empty($response)) {
            return false;
        }
        return json_decode($response, true);*/
    }

    // get game info
    protected function getGameInfo($uid=null,$cacheName=null)
    {
        // todo: waiting for gameinfo api
        return false;


        /*if(empty($uid) || empty($cacheName))
            return false;

        $gameInfo = Cache::remember($cacheName.':'.$uid, 30, function() use($uid) {
            $params = array(
                        'key_name' => 'bns_api',
                        'service' => 'get_account_info',
                        'uid' => $uid
                    );
            $response = json_decode($this->post_api($this->apiUrl, $params));

            if($response && $response->success){
                return $response->data;
            }else{
                return false;
            }
        });

        if($gameInfo === FALSE){
            Cache::forget($cacheName.':'.$uid);
        }

        return $gameInfo;*/
    }

    protected function UploadImage($request_files, $upload_path, $resize=NULL) {

        $file_tmp_path = $request_files->getPathName();
        $file_extension = $request_files->getClientOriginalExtension();

        if($file_extension != 'jpg' && $file_extension != 'png' && $file_extension != 'jpeg') {
            return ['status' => 'error', 'message' => 'Wrong type of image file.'];
        }

        $file_name = rand(1,10000).time().'.'.$file_extension;
        $file_path = $upload_path;
        $api_type = 'upload';

        $upload_result = $this->static_api($file_tmp_path, $file_name, $file_path, $api_type, $resize);
        $x = json_decode($upload_result['exec']);


        if(!isset($x->result)) {
            return ['status' => 'error', 'message' => 'Error on upload logo, Plese try again.'];
        } else {
            $image_path = $x->result;
            return ['status' => 'success', 'message' => $image_path];
        }
    }


	private function static_api($file_tmp_path,$file_name,$file_path,$api_type='upload',$resize=NULL){
        $data = array('api_type'=>$api_type,'resize'=>$resize,'save_path'=>$file_path);
        //print_r($file_tmp_path);
        return $this->curl_upload_file($file_tmp_path,$file_name,$data,true);
    }
    private function curl_upload_file($path_img,$file_name,$data,$debug=false){
        $host_api = 'http://static.api.cmsgarena.com/';
        $path = $path_img.';filename='.$file_name;


        $cfile = new \CURLFile($path_img, mime_content_type($path_img), $file_name);

        $post_data = array_merge(array('file' => $cfile), $data);
        //print_r($post_data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$host_api);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_VERBOSE,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Connection: Keep-Alive',
            'Keep-Alive: 300'
        ));
        $result=curl_exec ($ch);
        $status = curl_getinfo($ch);   //get status
        if($debug){
            return array('exec'=>$result,'info'=>$status,'path'=>$path);
        }else{
            return $result;
        }
        curl_close ($ch);
    }

}
