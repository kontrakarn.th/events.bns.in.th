<?php

    namespace App\Http\Controllers\Api\hello_summer;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Http\Controllers\Api\BnsEventController;

    // Utils
    use Illuminate\Support\Facades\Cache;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    // Models
    use App\Models\hello_summer\Member;
    use App\Models\hello_summer\Quest;
    use App\Models\hello_summer\QuestLog;
    use App\Models\hello_summer\Reward;
    use App\Models\hello_summer\ItemLog;
    use App\Models\hello_summer\WaterLogs;
    use App\Models\hello_summer\DeductWaterLogs;
    use App\Models\hello_summer\DeductQuestRightsLogs;

    class IndexController extends BnsEventController
    {
        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2019-04-24 12:00:00';
        private $endTime = '2019-05-22 05:59:59';

        private $startApiDateTime = '2019-04-24 19:00:00';
        private $endApiDateTime = '2019-05-15 05:59:59';

        public function __construct(Request $request){
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus(){
            if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
                // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if ($this->checkIsMaintenance()) {
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if ($this->isLoggedIn()) {
                    // check and create member
                if ($this->checkMember() == false) {
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            } else {
                die(json_encode([
                    'status' => false,
                    'type' => 'not_login',
                    'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                ]));
            }
        }

        private function getIP(){
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn(){
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip(){
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1';

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember(){
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP()
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            } else {
                $member = Member::where('uid', $uid)->first();
                if (empty($member->ncid)) {
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if (empty($ncid)) {
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $uid);
                }
            }
            return true;
        }

        private function hasMember(int $uid){
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr){
            return Member::create($arr);
        }

        private function updateMember($row_id = 0, $arr){
            if (empty($row_id))
                return false;

            return Member::where('id', $row_id)->update($arr);
        }

        private function getNcidByUid(int $uid) : string{
            return Member::where('uid', $uid)->value('ncid');
        }

        private function getUidByNcid(string $ncid){
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username) : string{
            return Cache::remember(sha1('BNS:HELLO_SUMMER:ASSOCIATE_UID_WITH_NCID_' . $uid), 10, function () use ($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }

            });
        }

        public function acceptChar(Request $request){

            if($request->has('type') == false && $request->type != 'accept_character') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter(1)!'
                ]);
            }

            if ($request->has('id') == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'invalid parameter(2)!'
                ]);
            }
            $char_id = $request->id;
            if (empty($char_id)) {
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่มีตัวละครที่ต้องการเลือก'
                ]);
            }

            // $char_id = $request->id;

            $uid = $this->userData['uid'];
            $username = $this->userData['username'];

            if ($resp = $this->hasAcceptChar($uid) == false) {
                $ncid = $this->getNcidByUid($uid);
                $characters = $this->doGetChar($uid,$ncid);
                $data = $characters[$char_id-1];
                if (isset($data['char_id'])) {
                    $this->doAcceptChar($data['char_id'], $data['char_name'], $data['world_id'], $data['job'], $uid);
                    $member = Member::where('uid', $uid)->first();
                    return response()->json([
                                'status' => true,
                                'message' => 'ยืนยันตัวละครเสร็จสิ้น',
                                'content' => [
                                    'username' => $member->username,
                                    'character' => $member->char_name,
                                ],
                    ]);
                } else {
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่พบตัวละครที่คุณต้องการเลือก'
                    ]);
                }
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก'
                ]);
            }
        }

        private function doAcceptChar($char_id, $char_name, $world_id, $job, $uid){
            return Member::where('uid', $uid)->update([
                        'char_id' => $char_id,
                        'char_name' => $char_name,
                        'world_id' => $world_id,
                        'job' => $job,
                    ]);
        }

        private function hasAcceptChar($uid){
            $resp = Member::where('uid', $uid)->first();
            if (empty($resp->char_id) && empty($resp->char_name)) {
                return false;
            } else {
                return $resp;
            }
        }

        public function getCharacter(Request $request){
            if($request->has('type') == false && $request->type != 'get_characters') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $this->checkMember();
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            $resp = $this->hasAcceptChar($uid);
            if ($resp == false) {
                $charactersList = [];
                $ncid = $this->getNcidByUid($uid);
                $characters = $this->doGetChar($uid,$ncid);
                foreach ($characters as $character) {
                    $charactersList[] = collect($character)->except(['char_id', 'world_id', 'job', 'level', 'creation_time', 'last_play_start', 'last_play_end', 'last_ip', 'exp', 'mastery_level']);
                }
                return response()->json([
                            'status' => true,
                            'content' => $charactersList,
                            'accepted' => false
                ]);
            } else {
                return response()->json([
                            'status' => true,
                            'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก',
                            'accepted' => true
                ]);
            }
        }

        private function doGetChar($uid,$ncid){
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i =1;
                    foreach ($resp->response as $key => $value) {
                        $content[] = [
                            'id' => $i,
                            'char_id' => $value->id,
                            'char_name' => $value->name,
                            'world_id' => $value->world_id,
                            'job' => $value->job,
                            'level' => $value->level,
                            'creation_time' => $value->creation_time,
                            'last_play_start' => $value->last_play_start,
                            'last_play_end' => $value->last_play_end,
                            'last_ip' => $value->last_ip,
                            'exp' => $value->exp,
                            'mastery_level' => $value->mastery_level,
                        ];

                        $i++;
                    }
                    return $content;
                }
            }
            return null;
        }

        private function apiCharacters($uid, $ncid){
            return Cache::remember('BNS:HELLO_SUMMER:CHARACTERS_' . $uid, 10, function() use($ncid) {
                        return $this->post_api($this->baseApi, [
                                    'key_name' => 'bns',
                                    'service' => 'characters',
                                    'user_id' => $ncid
                        ]);
                    });
        }

        private function apiSendItem(string $ncid, array $goodsData) {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Sending item from garena events BNS Hello Summer.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        // check quest completed api
        private function apiCheckQuestCompleted($uid, $charId, $questId, $start, $end) {
            // Cache::forget('BNS:HELLO_SUMMER:QUEST_COMPLETED_' . $questId . '_' . $uid);
            $resp = Cache::remember('BNS:HELLO_SUMMER:QUEST_COMPLETED_' . $questId . '_' . $uid, 10, function()use($charId,$questId,$start,$end) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'quest_completed',
                    'char_id' => $charId,
                    'quest_id' => $questId,
                    'start_time' => $start,
                    'end_time' => $end
                ]);
            });

            return json_decode($resp);
        }

        private function getQuestCompletedCount($uid, $charId, $questId, $start, $end){

            $completedCount = 0;

            $apiCompletedCount = $this->apiCheckQuestCompleted($uid, $charId, $questId, $start, $end);
            if($apiCompletedCount->status){
                $completedCount = intval($apiCompletedCount->response->quest_completed_count);
            }

            return $completedCount;
        }

        private function createQuestLog(array $arr) {
            if ($arr) {
                $resp = QuestLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateQuestLog(array $arr, $logId, $uid) {
            if ($arr) {
                return QuestLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function createDeductQuestRightsLog(array $arr) {
            if ($arr) {
                $resp = DeductQuestRightsLogs::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function createWaterLogs(array $arr) {
            if ($arr) {
                $resp = WaterLogs::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function createSendItemLog(array $arr) {
            if ($arr) {
                $resp = ItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateSendItemLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function createDeductWaterLog(array $arr) {
            if ($arr) {
                $resp = DeductWaterLogs::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        // Lobby Part
        public function getLobyInfo(Request $request)
        {
            if ($request->has('type') == false || $request->type != 'lobby_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            return response()->json([
                'status' => true,
                'content' => [
                    'character' => $member->char_name,
                    'username' => $member->username,
                ],
            ]);

        }

        // Daily Quest Info
        public function getDailyInfo(Request $request)
        {

            if ($request->has('type') == false || $request->type != 'daily_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // check quest completed and update quest point
            $quests = $this->setQuestInfo($member->uid);

            return response()->json([
                'status' => true,
                'content' => [
                    'username' => $member->username,
                    'character' => $member->char_name,
                    'quests' => $quests,
                ],
            ]);

        }

        private function setQuestInfo($uid){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $questInfo = [];

            // get quest list
            $quests = new Quest;
            
            foreach($quests->getQuestList() as $quest){

                $totalCompleted = 0;
                $totalUsed = 0;

                // check quest completed
                $questCompletedApi = $this->getQuestCompletedCount($member->uid, $member->char_id, $quest['quest_code'], $this->startApiDateTime, $this->endApiDateTime);

                // check has quest log
                $questLog = QuestLog::where('uid', $member->uid)
                                    ->where('quest_code', $quest['quest_code'])
                                    ->first();
                if(isset($questLog) && !empty($questLog->id)){

                    // compare completed count
                    if($questCompletedApi > $questLog->total_completed){
                        // update total completed
                        $questLog->total_completed = $questCompletedApi;
                        $questLog->save();
                    }

                    $totalCompleted = $questCompletedApi;
                    $totalUsed = $questLog->total_used;

                }else{

                    $totalCompleted = $questCompletedApi;
                    $totalUsed = 0;

                    // create log
                    $createLog = $this->createQuestLog([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'char_id' => $member->char_id,
                        'char_name' => $member->char_name,
                        'world_id' => $member->world_id,
                        'job' => $member->job,
                        'quest_id' => $quest['quest_id'],
                        'quest_code' => $quest['quest_code'],
                        'quest_title' => $quest['quest_title'],
                        'total_completed' => $totalCompleted,
                        'total_used' => $totalUsed,
                        'log_date' => date('Y-m-d H:i:s'),
                        'log_date_timestamp' => time(),
                        'last_ip' => $this->getIP(),
                    ]);
                }

                $questInfo[] = [
                    'id' => $quest['quest_id'],
                    'title' => $quest['quest_title'],
                    'remain_rights' => $totalCompleted - $totalUsed,
                ];

            }

            return $questInfo;

        }

        // Water Bowl, Water Drop Gachapon
        public function getPlayWaterGachapon(Request $request){
            if ($request->has('type') == false || $request->type != 'play_water_gachapon') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $questId = $request->quest_id;

            if(in_array($questId, [1,2,3,4,5,6,7,8,9,10,11,12]) == false){
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter(2)!'
                ]);
            }

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $quests = new Quest;
            $quest = $quests->setQuestInfoByQuestId($questId);

            // check remain rights from quest log
            $questLog = QuestLog::where('uid', $member->uid)
                                ->where('quest_code', $quest['quest_code'])
                                ->first();

            if(empty($questLog->id)){
                return response()->json([
                    'status' => false,
                    'message' => 'No data!'
                ]);
            }

            $remainRights = $questLog->total_completed - $questLog->total_used;

            if($remainRights <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />สิทธิ์ของคุณไม่พอเล่นกิจกรรม'
                ]);
            }

            // set used rights
            $usedRights = $questLog->total_used+1;
            $afterDeduct = $remainRights - 1;

            // deduct rights
            $this->createDeductQuestRightsLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'quest_id' => $quest['quest_id'],
                'quest_code' => $quest['quest_code'],
                'quest_title' => $quest['quest_title'],
                'before_deduct' => $remainRights,
                'after_deduct' => $afterDeduct,
                'log_date' => date('Y-m-d H:i:s'),
                'log_date_timestamp' => time(),
                'last_ip' => $this->getIP(),
            ]);

            // update quest used 
            $questLog->total_used = $usedRights;
            $questLog->save();

            // random quest reward
            $quests = new Quest;
            $questRewards = $quests->getRandomQuestRewards($questId);
            
            switch($questRewards['item_type']){

                case "water_drop":

                    // create log
                    $this->createWaterLogs([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'water_type' => 'water_drop',
                        'title' => $questRewards['product_title'],
                        'log_date' => date('Y-m-d H:i:s'),
                        'log_date_timestamp' => time(),
                        'last_ip' => $this->getIP(),
                    ]);

                    $newWaterDropQuantity = $member->total_water_drop + 1;
                        
                    // add water log to user data
                    $member->total_water_drop = $newWaterDropQuantity;
                    $member->save();

                    break;

                case "water_bowl":

                    // create log
                    $this->createWaterLogs([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'water_type' => 'water_bowl',
                        'title' => $questRewards['product_title'],
                        'log_date' => date('Y-m-d H:i:s'),
                        'log_date_timestamp' => time(),
                        'last_ip' => $this->getIP(),
                    ]);

                    $newWaterBowlQuantity = $member->total_water_bowl + 1;
                        
                    // add water log to user data
                    $member->total_water_bowl = $newWaterBowlQuantity;
                    $member->save();

                    break;

                case "quest":

                    $goods_data[] = [
                        'goods_id' => $questRewards['product_id'],
                        'purchase_quantity' => $questRewards['product_quantity'],
                        'purchase_amount' => 0,
                        'category_id' => 40
                    ];

                    // create send item logs
                    $itemLog = $this->createSendItemLog([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'quest_id' => $quest['quest_id'],
                        'product_id' => $questRewards['product_id'],
                        'product_title' => $questRewards['product_title'],
                        'product_quantity' => $questRewards['product_quantity'],
                        'item_type' => $questRewards['item_type'],
                        'amount' => $questRewards['amount'],
                        'send_item_status' => false,
                        'send_item_purchase_id' => '0',
                        'send_item_purchase_status' => '0',
                        'goods_data' => json_encode($goods_data),
                        'status' => 'pending',
                        'log_date' => date('Y-m-d H:i:s'),
                        'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                        'last_ip' => $this->getIP()
                    ]);

                    $send_result_raw = $this->apiSendItem($ncid, $goods_data);
                    $send_result = json_decode($send_result_raw);
                    if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                        $this->updateSendItemLog([
                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                            'status' => 'success'
                        ], $itemLog['id'], $uid);

                    }else{
                        $this->updateSendItemLog([
                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                            'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                            'status' => 'unsuccess'
                        ], $itemLog['id'], $uid);

                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                            'content' => [
                                'username' => $member->username,
                                'character' => $member->char_name,
                                'quest_id' => $questId,
                                'quest_title' => $quest['quest_title'],
                                'remain_rights' => $afterDeduct,
                            ],
                        ]);
                    }
                
                    break;
            }

            return response()->json([
                'status' => true,
                'message' => 'ส่งของรางวัลเสร็จเรียบร้อย<br />กรุณาตรวจสอบกล่องจดหมายภายในเกม',
                'content' => [
                    'username' => $member->username,
                    'character' => $member->char_name,
                    'rewards' => isset($questRewards['product_title']) && !empty($questRewards['product_title']) ? $questRewards['product_title'] : '',
                    'quest_id' => $questId,
                    'quest_title' => $quest['quest_title'],
                    'rewards_quantity' => isset($questRewards['amount']) && !empty($questRewards['amount']) ? $questRewards['amount'] : 0,
                    'remain_rights' => $afterDeduct,
                ],
            ]);

        }

        // Water Drop Info
        public function getWaterDropInfo(Request $request)
        {
            if ($request->has('type') == false || $request->type != 'water_drop_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            return response()->json([
                'status' => true,
                'content' => [
                    'username' => $member->username,
                    'character' => $member->char_name,
                    'water_drop_remain' => $member->total_water_drop - $member->used_water_drop,
                    'water_drop_used' => $member->used_water_drop,
                ],
            ]);

        }

        public function playWaterDrop(Request $request){
            if ($request->has('type') == false || $request->type != 'play_water_drop') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // set remain water drop
            $remainWaterDrop = $member->total_water_drop - $member->used_water_drop;

            if($remainWaterDrop <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนหยดน้ำไม่พอเปิดกล่องซัมเมอร์เยือกแข็ง'
                ]);
            }

            // create water drop deduct log
            $afterDeduct = $remainWaterDrop - 1;
            $this->createDeductWaterLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'deduct_type' => 'water_drop',
                'before_deduct' => $remainWaterDrop,
                'after_deduct' => $afterDeduct,
                'log_date' => date('Y-m-d H:i:s'),
                'log_date_timestamp' => time(),
                'last_ip' => $this->getIP(),
            ]);

            // set reward model
            $rewards = new Reward;
            $rewardResponse = [];

            // deduct water drop from user
            $usedWaterDrop = $member->used_water_drop + 1;
            $member->used_water_drop = $usedWaterDrop;
            // set garantee water bowl
            $member->total_water_bowl = $member->total_water_bowl  + 1;
            $member->save();

            $rewardResponse[] = [
                'title' => 'ขันน้ำ',
                'amount' => 1,
            ];

            // set random reward
            $rewardInfo = $rewards->getRandomGachapon();

            $rewardResponse[] = [
                'title' => $rewardInfo['product_title'],
                'amount' => $rewardInfo['amount'],
            ];

            $goods_data[] = [
                'goods_id' => $rewardInfo['product_id'],
                'purchase_quantity' => $rewardInfo['product_quantity'],
                'purchase_amount' => 0,
                'category_id' => 40
            ];

            // create item log
            $itemLog = $this->createSendItemLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'product_id' => $rewardInfo['product_id'],
                'product_title' => $rewardInfo['product_title'],
                'product_quantity' => $rewardInfo['product_quantity'],
                'item_type' => $rewardInfo['item_type'],
                'amount' => $rewardInfo['amount'],
                'send_item_status' => false,
                'send_item_purchase_id' => '0',
                'send_item_purchase_status' => '0',
                'goods_data' => json_encode($goods_data),
                'status' => 'pending',
                'log_date' => date('Y-m-d H:i:s'),
                'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                'last_ip' => $this->getIP()
            ]);

            $send_result_raw = $this->apiSendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                    'status' => 'success'
                ], $itemLog['id'], $uid);

                return response()->json([
                    'status' => true,
                    'message' => 'ส่งของรางวัลเสร็จเรียบร้อย<br />กรุณาตรวจสอบกล่องจดหมายภายในเกม',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                        'water_drop_remain' => $afterDeduct,
                        'water_drop_used' => $usedWaterDrop,
                        'rewards' => $rewardResponse,
                    ],
                ]);

            }else{
                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                    'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                    'status' => 'unsuccess'
                ], $itemLog['id'], $uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                    ],
                ]);
            }

        }

        // Water Bowl for exchange
        public function getExchangeInfo(Request $request)
        {

            if ($request->has('type') == false || $request->type != 'exchange_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            return response()->json([
                'status' => true,
                'content' => [
                    'username' => $member->username,
                    'character' => $member->char_name,
                    'water_bowl_remain' => $member->total_water_bowl - $member->used_water_bowl,
                    'water_bowl_used' => $member->used_water_bowl,
                ],
            ]);

        }

        public function exchangeReward(Request $request){
            if ($request->has('type') == false || $request->type != 'exchange_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $packageId = $request->package_id;

            if(in_array($packageId, [1,2,3,4,5]) == false){
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter(2)!'
                ]);
            }

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // check remain water bowl
            $remainWaterBowl = $member->total_water_bowl - $member->used_water_bowl;

            // set reward by package_id
            $rewards = new Reward;
            $rewardInfo = $rewards->setExchangeByPackageId($packageId);

            // check required water bowl
            if($remainWaterBowl < $rewardInfo['require_water_bowl']){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนขันน้ำไม่พอแลกของรางวัล'
                ]);
            }

            // create deduct water bowl log
            $afterDeduct = $remainWaterBowl - $rewardInfo['require_water_bowl'];
            $this->createDeductWaterLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'deduct_type' => 'water_bowl',
                'before_deduct' => $remainWaterBowl,
                'after_deduct' => $afterDeduct,
                'log_date' => date('Y-m-d H:i:s'),
                'log_date_timestamp' => time(),
                'last_ip' => $this->getIP(),
            ]);

            // update used water bowl
            $usedWaterBowl = $member->used_water_bowl + $rewardInfo['require_water_bowl'];
            $member->used_water_bowl = $usedWaterBowl;
            $member->save();

            $goods_data[] = [
                'goods_id' => $rewardInfo['product_id'],
                'purchase_quantity' => $rewardInfo['product_quantity'],
                'purchase_amount' => 0,
                'category_id' => 40
            ];

            // create item log
            $itemLog = $this->createSendItemLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'product_id' => $rewardInfo['product_id'],
                'product_title' => $rewardInfo['product_title'],
                'product_quantity' => $rewardInfo['product_quantity'],
                'item_type' => $rewardInfo['item_type'],
                'amount' => $rewardInfo['amount'],
                'send_item_status' => false,
                'send_item_purchase_id' => '0',
                'send_item_purchase_status' => '0',
                'goods_data' => json_encode($goods_data),
                'status' => 'pending',
                'log_date' => date('Y-m-d H:i:s'),
                'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                'last_ip' => $this->getIP()
            ]);

            $send_result_raw = $this->apiSendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                    'status' => 'success'
                ], $itemLog['id'], $uid);

                return response()->json([
                    'status' => true,
                    'message' => 'ส่งของรางวัลเสร็จเรียบร้อย<br />กรุณาตรวจสอบกล่องจดหมายภายในเกม',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                        'water_bowl_remain' => $afterDeduct,
                    ],
                ]);

            }else{
                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                    'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                    'status' => 'unsuccess'
                ], $itemLog['id'], $uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                    ],
                ]);
            }

        }

        public function getItemHistory(Request $request)
        {
            if ($request->has('type') == false || $request->type != 'item_history') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $data = [];
            $resp_raw = ItemLog::select('product_title', 'status', 'log_date_timestamp', 'created_at')
                ->where('uid', $uid)
                ->orderBy('created_at', 'DESC')
                ->orderBy('id', 'DESC')
                ->get();
            $resp = [];
            if (count($resp_raw) > 0) {
                foreach ($resp_raw as $key => $value) {
                    $resp[$key]['created_at'] = date('d/m/Y H:i:s', $value->log_date_timestamp);
                    $resp[$key]['item_title'] = $value->product_title;
                    $resp[$key]['timestamp'] = $value->log_date_timestamp;
                }
            }

            // water drop, bowl
            $data2 = [];
            $resp_raw2 = WaterLogs::select('water_type', 'title', 'log_date_timestamp', 'created_at')
                ->where('uid', $uid)
                ->orderBy('created_at', 'DESC')
                ->orderBy('id', 'DESC')
                ->get();
            $resp2 = [];
            if (count($resp_raw2) > 0) {
                foreach ($resp_raw2 as $key2 => $value2) {
                    $resp2[$key2]['created_at'] = date('d/m/Y H:i:s', $value2->log_date_timestamp);
                    $resp2[$key2]['item_title'] = $value2->title;
                    $resp2[$key2]['timestamp'] = $value2->log_date_timestamp;
                }
            }

            // $historyContent = [];
            $historyContent = array_merge($resp,$resp2);
            $sort = array();
            if(count($historyContent) > 0){
                foreach($historyContent as $k=>$v) {
                    $sort['timestamp'][$k] = $v['timestamp'];
                }
                array_multisort($sort['timestamp'], SORT_DESC, $historyContent);
            }

            return response()->json([
                'status' => true,
                'content' => [
                    'username' => $member->username,
                    'character' => $member->char_name,
                    'items_list'=>$historyContent
                ],
            ]);
        }

    }