<?php

    namespace App\Http\Controllers\Api\Test\exchange_gift;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DB;
    use Validator;

    use App\Models\Test\exchange_gift\Events;
    use App\Models\Test\exchange_gift\Member;
    use App\Models\Test\exchange_gift\Items;
    use App\Models\Test\exchange_gift\Gift;
    use App\Models\Test\exchange_gift\DeductLog;
    use App\Models\Test\exchange_gift\ItemHistory;
    use App\Models\Test\exchange_gift\SendItemLog;

    class IndexController extends BnsEventController{

    	private $baseApi = 'http://api.apps.garena.in.th';
        private $events = false;

        private $server_list= [
            7801=>"อุนกุก",
            7803=>"ทาลัน",
            7804=>"นาริว",
        ];

        public function __construct(Request $request)
        {
            parent::__construct();

            // init user data
            $this->events=Events::getEventByKey("exchange_gift");
            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
             if($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }
            // dd($this->startTime,$this->endTime);
            if ((time() < strtotime($this->events->start_time) || time() > strtotime($this->events->end_time))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }


        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            // dd($this->userData);
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $member = new Member;
                $member->uid=$uid;
                $member->username=$username;
                $member->ncid=$ncid;
                $member->gifted=false;
                $member->exchanged=false;
                $member->character_id=0;
                $member->character_name="";
                $member->world_id=0;
                $member->world_name="";
                $member->last_ip=$this->getIP();
                $member->save();

                if (isset($member) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->first();
            if (isset($counter)) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        private function getUserInfo($uid)
        {
            $userInfo = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'user_info',
                'uid' => $uid
            ]);
            $result = json_decode($userInfo, true);
            if (is_null($result) || $result['status'] == false) {
                return false;
            }
            return $result['response'];
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function addSendItemLog($arr) {
            return SendItemLog::create($arr);
        }

        private function updateSendItemLog($arr, $log_id, $uid) {
            return SendItemLog::where('_id', $log_id)->where('uid', $uid)->update($arr);
        }

        private function addItemHistoryLog($arr) {
            return ItemHistory::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemHistory::where('_id', $log_id)->where('uid', $uid)->update($arr);
        }


        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:HMELITE_2019:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events bns elite 2019.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function diamondBalance(int $uid)
        {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'balance',
                'uid' => $uid,
            ]);
        }

        private function setDiamondDeductData($deduct_diamond_amount = 0)
        {
            if ($deduct_diamond_amount == 0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function addDeductLog(array $arr) {
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamomds for garena events bns elite 2019 excusive shop',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function doGetDiamondHistory($ncid) {
            if(empty($ncid)){
                return false;
            }

            return Cache::remember('BNS:HMELITE_2019:EXCHANGE_TO_DIAMONDS_HISTORY_'.$this->year_month.'_' . $ncid, 5, function() use($ncid) {
                return json_decode($this->post_api($this->baseApi, [
                            'key_name' => 'bns',
                            'service' => 'query_cash_txn',
                            'user_id' => $ncid,//ncid
                            'start_time' => $this->diamondStartTime,
                            'end_time' => $this->diamondEndTime
                        ]));
            });
        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:EXCHANGE_GIFT:CHARACTERS_' . $uid, 10, function() use($ncid) {
                        return $this->post_api($this->baseApi, [
                                    'key_name' => 'bns',
                                    'service' => 'characters',
                                    'user_id' => $ncid
                        ]);
                    });
        }

        private function doGetChar($uid,$ncid){//new fixed function
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i =1;
                    foreach ($resp->response as $key => $value) {
                        $content[] = [
                            // 'id' => $i,
                            'char_id' => $value->id,
                            'char_name' => $value->name,
                            'world_id' => $value->world_id,
                            'world' => $this->server_list[$value->world_id],
                            // 'job' => $value->job,
                            // 'level' => $value->level,
                            // 'creation_time' => $value->creation_time,
                            // 'last_play_start' => $value->last_play_start,
                            // 'last_play_end' => $value->last_play_end,
                            // 'last_ip' => $value->last_ip,
                            // 'exp' => $value->exp,
                            // 'mastery_level' => $value->mastery_level,
                        ];

                        $i++;
                    }
                    return $content;
                }
            }
            return null;
        }

        public function selectCharacter(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'select_character') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }
            if ($decoded->has('id') == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (2)'
                ]);
            }

            $char_id = (int)$decoded['id'];

            $userData = $this->userData;
            $uid = $userData['uid'];

            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบข้อมูล'
                ]);
            }

            if($member->character_id>0){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถเปลี่ยนตัวละครได้'
                ]);
            }

            $mychar=$this->doGetChar($member->uid,$member->ncid);
            $myselect=collect($mychar)->where('char_id',$char_id);

            if(count($myselect)==0){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบตัวละคร'
                ]);
            }
            // dd($myselect)->first();
            $member->character_id=(int) $myselect->first()['char_id'];
            $member->character_name=$myselect->first()['char_name'];
            $member->world_id=$myselect->first()['world_id'];
            $member->world_name=$this->server_list[$myselect->first()['world_id']];
            $member->save();

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'message'=>"ตัวละคร ".$member->character_name."\n\nServer: ".$member->world_name."\n\nเข้าร่วมกิจกรรมเรียบร้อย",
                        'data' => $eventInfo
                    ]);

        }

        public function setEventInfo($uid){
            $nickname=$this->userData['nickname'];
            $member = Member::where('uid', $uid)->first();

            $current_page="";

            if ((time() >= strtotime($this->events->start_time_gift) && time() <= strtotime($this->events->end_time_gift))) {
                $current_page="gift";
            }elseif ((time() >= strtotime($this->events->start_time_gift) && time() <= strtotime($this->events->end_time_gift))) {
                $current_page="exchange";
            }

            if($member->gifted==true){
                $current_page="exchange";
            }


            if($member->character_id==0){

                $mychar=$this->doGetChar($member->uid,$member->ncid);
                $datachar=[];
                foreach($mychar as $key=>$value){
                    $datachar[]=[
                        'id'=>$value['char_id'],
                        'name'=>$value['char_name'],
                        'world'=>$value['world'],
                    ];
                }
                return [
                        'uid'=>$uid,
                        'username'=>$nickname,
                        'character_name'=>'',
                        'gifted'=>false,
                        'exchanged'=>false,
                        'selected_char'=>false,
                        'characters'=>$datachar,
                        'item_list'=>[],
                        'current_page'=>$current_page,
                    ];
            }
            $all_item=[];
            if($member->gifted==false){
                $all_item=Items::select('no','price','product_title')->get();
            }
            $can_exchange=$member->exchanged;
            if ((time() < strtotime($this->events->start_time_ex) || time() > strtotime($this->events->end_time_ex))) {
                $can_exchange=true;
            }

            return [
                        'uid'=>$uid,
                        'nickname'=>$nickname,
                        'character_name'=>$member->character_name,
                        'gifted'=>$member->gifted,
                        'exchanged'=>$can_exchange,
                        'selected_char'=>true,
                        'characters'=>[],
                        'item_list'=>$all_item,
                        'current_page'=>$current_page,
                ];

        }


        public function getEventInfo(Request $request){

            if ($request->has('type') == false || $request['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }
            // dd($this);
            $uid = $this->userData['uid'];

            $eventinfo=$this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'data' =>$eventinfo
                   ]);

        }

        private function updateDeductLog($arr, $log_id) {
            if ($arr) {
                return DeductLog::where('_id', $log_id)->update($arr);
            }
            return null;
        }

        public function setGift(Request $request){

            if ((time() < strtotime($this->events->start_time_gift) || time() > strtotime($this->events->end_time_gift))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาห่อของขวัญ'
                ]));
            }

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'set_gift') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('gift') == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (2)'
                ]);
            }

            if (is_array($decoded['gift']) == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (3)'
                ]);
            }

            if (count($decoded['gift']) != 60) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (4)'
                ]);
            }

            // dd($decoded['gift']);

            $uid = $this->userData['uid'];
            $gift=$decoded['gift'];
            $total_price=0;
            $item_list=[];
            foreach($gift as $key=>$value){
                if($value>10){
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถห่อของขวัญเกิน 10 ชิ้นต่อไอเทมได้'
                    ]);
                }elseif($value>0){
                    $item=Items::where('no',($key+1))->first();
                    $cal_price=$value*$item->price;
                    $itemins=[
                        'no'=>$item->no,
                        'product_id'=>$item->product_id,
                        'product_title'=>$item->product_title,
                        'product_quantity'=>$value,
                        'cal_price'=>$cal_price,
                    ];
                    $item_list[]=$itemins;
                    $total_price+=$cal_price;
                }
            }
            // dd($total_price);

            if($total_price<5000){
                return response()->json([
                            'status' => false,
                            'message' => 'จำนวน Diamond ไม่ถึงขั้นต่ำ'
                ]);
            }
            // dd($item_list,$total_price);
            $member = Member::where('uid', $uid)->first();
            $ncid=$member->ncid;
            if($member->gifted==true){
                return response()->json([
                            'status' => false,
                            'message' => 'ห่อของขวัญได้ครั้งเดียวเท่านั้น'
                ]);
            }

            //CHECK price
            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถอัพเดทข้อมูล Diamonds ของคุณได้กรุณาลองใหม่อีกครั้ง'
                        ]);
            }else{

                $before_diamond = intval($diamondsBalance->balance);
                // compare user diamonds balance with gachapon's require diamomds.
                if($before_diamond < $total_price){
                    return response()->json([
                                'status' => false,
                                'message' => 'จำนวน Diamonds ของคุณไม่พอในการจัดของขวัญ'
                    ]);
                }else{

                    $logDeduct = $this->addDeductLog([
                        'status' => 'pending',
                        'diamond' => $total_price,
                        'item_list' => $item_list,
                        'uid' => $uid,
                        'ncid' => $ncid,
                        'last_ip' => $this->getIP(),
                        'log_timestamp' => time()
                    ]);

                    // set desuct diamonds
                    $deductData = $this->setDiamondDeductData($total_price);
                    if($deductData == false){
                        return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง']);
                    }

                    $respDeductRaw = $this->deductDiamond($ncid, $deductData);
                    $respDeduct = json_decode($respDeductRaw);

                    if(is_object($respDeduct) && is_null($respDeduct) == false){

                        $arrDeductLog = [
                            'before_deduct_diamond' => $before_diamond,
                            'after_deduct_diamond' => $before_diamond - $total_price,
                            'deduct_status' => $respDeduct->status,
                            'deduct_purchase_id' => isset($respDeduct->response->purchase_id) ? $respDeduct->response->purchase_id : 0,
                            'deduct_purchase_status' => isset($respDeduct->response->purchase_status) ? $respDeduct->response->purchase_status : 0,
                            'deduct_data' => json_encode($deductData),
                            'status' => 'pending'
                        ];

                        if($respDeduct->status){

                            $arrDeductLog['status'] = 'success';
                            $this->updateDeductLog($arrDeductLog, $logDeduct['_id']);


                            $gift_data= new Gift;
                            $gift_data->uid=$uid;
                            $gift_data->ncid=$ncid;
                            $gift_data->character_id=$member->character_id;
                            $gift_data->character_name=$member->character_name;
                            $gift_data->world_id=$member->world_id;
                            $gift_data->world_name=$member->world_name;
                            $gift_data->diamond=$total_price;
                            $gift_data->item_list=$item_list;
                            $gift_data->status="packed";
                            $gift_data->gift_type="user";
                            $gift_data->get_by_uid=0;
                            $gift_data->get_by_ncid="";
                            $gift_data->get_by_account_name="";
                            $gift_data->get_by_character_name="";
                            $gift_data->get_by_world_id=0;
                            $gift_data->get_by_world_name="";
                            $gift_data->last_ip=$this->getIP();
                            $gift_data->log_timestamp=time();
                            $gift_data->save();

                            $member->gifted=true;
                            $member->save();

                            $eventInfo = $this->setEventInfo($uid);

                            //do return success log
                            return response()->json([
                                        'status' => true,
                                        'message' => 'ห่อของขวัญเรียบร้อย',
                                        'data'=>$eventInfo,
                            ]);

                        }else{
                            $arrDeductLog['status'] = 'unsuccess';
                            $this->updateDeductLog($arrDeductLog, $logDeduct['_id']);
                        }
                    }else {
                        $arrDeductLog['status'] = 'unsuccess';
                        $this->updateDeductLog($arrDeductLog, $logDeduct['_id']);
                    }
                }
            }
        }

        public function exchangeGift(Request $request){
            if ((time() < strtotime($this->events->start_time_ex) || time() > strtotime($this->events->end_time_ex))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาแลกของขวัญ'
                ]));
            }

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'exchange_gift') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $uid = $this->userData['uid'];
            $member = Member::where('uid', $uid)->first();
            $ncid=$member->ncid;
            if($member->gifted==false){
                return response()->json([
                            'status' => false,
                            'message' => 'คุณไม่ได้ห่อของขวัญ'
                ]);
            }

            if($member->exchanged==true){
                return response()->json([
                            'status' => false,
                            'message' => 'คุณแลกของขวัญเรียบร้อยแล้ว'
                ]);
            }

            $data_exits=[$uid];
            $data=Gift::raw(function ($collect) use ($data_exits) {
                return $collect->aggregate([
                    [
                        '$match' => [
                            // 'uid'=>[
                            //     '$nin'=>$data_exits
                            // ],
                            'status'=>'packed'
                        ]
                    ],
                    [ '$sample' => [
                        'size'=>1
                        ]
                    ],
                ],
                [
                    'allowDiskUse'=>true
                ]);
            });

            if(count($data)>0){
                $gift_get=$data[0];
                // dd($gift_get->_id);
                $gift_data=Gift::find($gift_get->_id);
                $gift_data->status='exchanged';
                $gift_data->get_by_uid=$member->uid;
                $gift_data->get_by_ncid=$member->ncid;
                $gift_data->get_by_account_name=$member->username;
                $gift_data->get_by_character_name=$member->character_name;
                $gift_data->get_by_world_id=$member->world_id;
                $gift_data->get_by_world_name=$member->world_name;
                $gift_data->last_ip=$this->getIP();
                $gift_data->exchange_timestamp=time();
                $gift_data->exchange_date_string=Carbon::now()->toDateString();
                $gift_data->save();

                $member->exchanged=true;
                $member->save();

                foreach ($gift_data->item_list as $key => $value) {//loop for add item log
                    $arrItemHistory = [
                        'product_id' => 0,
                        'product_title' => '',
                        'product_quantity' => 0,
                        'uid' => $member->uid,
                        'ncid' => $member->ncid,
                        'last_ip' => $this->getIP(),
                        'icon' => '',
                        'transaction_id'=>$gift_data->_id,
                        'log_timestamp' => time(),
                        'log_time_string' => Carbon::now()->format('d/m/Y H:i:s')
                    ];

                    $arrItemHistory['product_id'] = $value['product_id'];
                    $arrItemHistory['product_title'] = $value['product_title'];
                    $arrItemHistory['product_quantity'] = $value['product_quantity'];
                    $arrItemHistory['status'] = 'used';

                    $goodsData[] = [
                        'goods_id' => $value['product_id'],
                        'purchase_quantity' => $value['product_quantity'],
                        'purchase_amount' => 0,
                        'category_id' => 40
                    ];
                    $this->addItemHistoryLog($arrItemHistory);
                }
                    if (is_null($goodsData) == false && is_array($goodsData) && count($goodsData) > 0) {
                        //do add send item log
                        $logSendItemResult = $this->addSendItemLog([
                            'uid' => $member->uid,
                            'ncid' => $member->ncid,
                            'status' => 'pending',
                            'transaction_id'=>$gift_data->_id,
                            'send_item_status' => false,
                            'send_item_purchase_id' => 0,
                            'send_item_purchase_status' => 0,
                            'goods_data' => json_encode($goodsData),
                            'last_ip' => $this->getIP(),
                            'log_timestamp' => time()
                        ]);

                        //send item
                        $sendResultRaw = $this->dosendItem($ncid, $goodsData);
                        $sendResult = json_decode($sendResultRaw);
                        if (is_null($sendResult) == false && is_object($sendResult) && $sendResult->status) {
                            //recive log id
                            $log_id = $logSendItemResult['_id'];
                            //do update send item log
                            $this->updateSendItemLog([
                                'send_item_status' => $sendResult->status ?: false,
                                'send_item_purchase_id' => $sendResult->response->purchase_id ?: 0,
                                'send_item_purchase_status' => $sendResult->response->purchase_status ?: 0,
                                'status' => 'success'
                                    ], $log_id, $uid);
                            $eventInfo = $this->setEventInfo($member->uid);
                            $who_get_gm=Gift::select('character_name','world_name','diamond','get_by_character_name','get_by_world_name')->where('world_name','GM')->get();
                            $eventInfo['gift_list']=$who_get_gm;
                            //do return success log
                            if($gift_data->world_name=="GM"){
                                return response()->json([
                                            'status' => true,
                                            'message' => "ได้รับของขวัญจาก ".$gift_data->character_name."\n\nไอเทมส่งเข้าไปในกล่องจดหมาย\n\nกรุญาเข้าเกมเพื่อรับรางวัล",
                                            'data'=>$eventInfo,
                                ]);
                            }else{
                                return response()->json([
                                            'status' => true,
                                            'message' => "ได้รับของขวัญจาก ".$gift_data->character_name.""."\n\nServer : ".$gift_data->world_name."\n\nไอเทมส่งเข้าไปในกล่องจดหมาย\n\nกรุญาเข้าเกมเพื่อรับรางวัล",
                                            'data'=>$eventInfo,
                                ]);
                            }

                        } else {
                            //do update send item log
                            $log_id = $logSendItemResult['_id'];
                            $this->updateSendItemLog([
                                'send_item_status' => isset($sendResult->status) ? $sendResult->status : false,
                                'send_item_purchase_id' => isset($sendResult->response->purchase_id) ? $sendResult->response->purchase_id : 0,
                                'send_item_purchase_status' => isset($sendResult->response->purchase_status) ? $sendResult->response->purchase_status : 0,
                                'status' => 'unsuccess'
                                    ], $log_id, $uid);
                            //error unsuccess log
                            return response()->json([
                                        'status' => false,
                                        'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                            ]);
                        }
                    }else{
                        return response()->json([
                                    'status' => false,
                                    'message' => 'ไม่พบไอเท็ม กรุณาติดต่อฝ่ายบริการลูกค้า'
                        ]);
                    }

            }else{
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบข้อมูล โปรดลองใหม่อีกครั้ง',
                ]);
            }



        }

        public function GetGiftHighPrice(Request $request){
            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'high_price') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $data=Gift::select('character_name','world_name','diamond')->where('world_name','!=','GM')->limit(10)->orderBy('diamond','DESC')->get();

            return response()->json([
                        'status' => true,
                        'message' => 'success',
                        'data'=>$data,
            ]);

        }

        public function WhoGetGM(Request $request){
            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'who_get_gm') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $data=Gift::select('character_name','world_name','diamond','get_by_character_name','get_by_world_name')->where('world_name','GM')->get();

            return response()->json([
                        'status' => true,
                        'message' => 'success',
                        'data'=>$data,
            ]);

        }

        public function MyHistoryPack(Request $request){
            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'my_history_pack') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }
            $uid = $this->userData['uid'];
            $member = Member::where('uid', $uid)->first();
            $data=Gift::where('uid',$member->uid)->first();
            if(isset($data)){

                $new_item=[];
                foreach($data->item_list as $key=>$value){
                    $new_item[]=[
                        'product_title'=>$value['product_title'],
                        'product_quantity'=>$value['product_quantity'],
                        'price'=>$value['cal_price'],
                    ];
                }


                return response()->json([
                            'status' => true,
                            'message' => 'success',
                            'data'=>$new_item,
                ]);
            }else{
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบของขวัญ',
                ]);
            }

        }


        public function addGMItem(){

            $gm_set=$this->events->gm_set;
            foreach($gm_set as $key=>$value){
                // dd($value);
                $chk_gift=Gift::where('ncid',$value["ncid"])->first();
                if(!isset($chk_gift)){
                    $gift_data= new Gift;
                    $gift_data->uid=$value["uid"];
                    $gift_data->ncid=$value["ncid"];
                    $gift_data->character_id=$value["character_id"];
                    $gift_data->character_name=$value["character_name"];
                    $gift_data->world_id=$value["world_id"];
                    $gift_data->world_name=$value["world_name"];
                    $gift_data->item_list=$value["item_list"];
                    $gift_data->status="packed";
                    $gift_data->gift_type=$value["gift_type"];
                    $gift_data->get_by_uid=0;
                    $gift_data->get_by_ncid="";
                    $gift_data->get_by_account_name="";
                    $gift_data->get_by_character_name="";
                    $gift_data->get_by_world_id="";
                    $gift_data->get_by_world_name="";
                    $gift_data->log_timestamp=time();
                    $gift_data->save();
                }


            }

            return response()->json([
                        'status' => true,
                        'message' => 'added !!'
            ]);
        }

    }
