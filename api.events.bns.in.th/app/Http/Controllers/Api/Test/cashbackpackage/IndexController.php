<?php

namespace App\Http\Controllers\Api\Test\cashbackpackage;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Test\cashbackpackage\Setting;
use App\Models\Test\cashbackpackage\Member;
use App\Models\Test\cashbackpackage\MemberCheckin;
use App\Models\Test\cashbackpackage\CheckList;
use App\Models\Test\cashbackpackage\CompensationLog;
use App\Models\Test\cashbackpackage\Reward;
use App\Models\Test\cashbackpackage\ItemLog;
use App\Models\Test\cashbackpackage\DeductLog;

class IndexController extends BnsEventController
{

    private $baseApi = 'http://api.apps.garena.in.th';

    private $userEvent = null;

    public function __construct(Request $request)
    {
        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);

        $this->checkEventStatus();
    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status' => false,
                'type' => 'no_permission',
                'message' => 'Sorry, you do not have permission.'
            ]));
        }

        // get events setting info
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status' => false,
                'type' => 'maintenance',
                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
            ]));
        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                ]));
            }
        } else {
            die(json_encode([
                'status' => false,
                'type' => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function is_accepted_ip()
    {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'              => $uid,
                'username'         => $username,
                'ncid'             => $ncid,
                'char_id'          => 0,
                'char_name'        => '',
                'world_id'         => 0,
                'last_ip'          => $this->getIP(),
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }

            $this->userEvent = $resp;
        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id)) {
            return false;
        }

        return Member::where('id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:CASHBACKPACKAGE:TEST:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'user_info',
                'uid'      => $uid,
            ]);
            $userNcInfo       = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }
        });
    }

    private function apiSendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'              => 'bns',
            'service'               => 'send_item',
            'user_id'               => $ncid,
            'purchase_description'  => 'Test sending item from garena events BNS Cash Back Package.',
            'goods'                 => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function createSendItemLog(array $arr)
    {
        if ($arr) {
            $resp = ItemLog::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return null;
            }
        }
        return null;
    }

    private function updateSendItemLog(array $arr, $logId, $uid)
    {
        if ($arr) {
            return ItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
        }
        return null;
    }
    private function apiDiamondBalance(int $uid)
    {
        return $this->post_api($this->baseApi, [
            'key_name' => 'bns',
            'service' => 'balance',
            'uid' => $uid,
        ]);
    }

    private function setDiamondsBalance($uid)
    {

        $diamondBalanceRaw = $this->apiDiamondBalance($uid);
        $diamondBalance = json_decode($diamondBalanceRaw);

        $diamonds = 0;
        if ($diamondBalance->status == true) {
            $diamonds = intval($diamondBalance->balance);
        }

        return $diamonds;
    }

    private function apiDeductDiamond(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Deduct diamomds for garena events BNS Cash Back Package.',
            'currency_group_id' => 71,
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function setDiamondDeductData($deduct_diamond_amount = 0)
    {
        if ($deduct_diamond_amount == 0)
            return false;

        return [
            [
                'goods_id' => 741,
                'purchase_quantity' => $deduct_diamond_amount,
                'purchase_amount' => $deduct_diamond_amount,
                'category_id' => 40
            ]
        ];
    }

    private function getEventSetting()
    {
        // return Cache::remember('BNS:LEAGUE:TEST:EVENT_SETTING_TEST', 10, function() {
        return Setting::where('active', 1)->first();
        // });
    }

    public function getEventInfo(Request $request)
    {

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        // $eventSetting = $this->getEventSetting();

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }


        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] == false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message'],
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }

    private function setEventInfo($uid)
    {
        if (empty($uid)) {
            return [
                'status' => false,
                'message' => 'ไม่พอข้อมูล'
            ];
        }

        // get events setting info
        $eventSetting = $this->getEventSetting();

        $member = Member::where('uid', $uid)->first();

        // check is unlock passport
        $isUnlockPassport = false;
        $unlockPackageCanBuy = false;
        $unlockPackageAlreadyBuy = false;
        if ($member->is_unlocked == 1) {
            $isUnlockPassport = true;
        }

        // check can buy unlock package
        if (time() <= strtotime($eventSetting->unlock_package_end)) {
            $unlockPackageCanBuy = true;
        }

        $unlockPackageHistory = ItemLog::where('uid', $member->uid)->where('package_type', 'unlock_reward')->count();
        if ($unlockPackageHistory > 0) {
            $unlockPackageAlreadyBuy = true;
        }


        // check has created checkin logs
        $checkinLog = MemberCheckin::where('uid', $member->uid)->get();
        if ($isUnlockPassport == true && count($checkinLog) <= 0) {
            $checkList = CheckList::orderBy('day', 'ASC')->get();

            if (count($checkList) > 0) {
                foreach ($checkList as $i => $check) {
                    if ($check->day == 1) {
                        $this->sendFirstDayCheckinReward($member);
                    }

                    $checkinDate = Carbon::today()->addDays($i)->toDateString();
                    MemberCheckin::create([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'checkin_date' => $checkinDate,
                        'day' => $check->day,
                        'package_key' => $check->package_key,
                        'is_checkin' => $check->day == 1 ? 1 : 0, // first day auto checkin
                        'checkin_at' => $check->day == 1 ? date('Y-m-d') : null,
                        'last_ip' => $this->getIP(),
                    ]);
                }

                $checkinLog = MemberCheckin::where('uid', $member->uid)->get();
            }
        }

        // daily checkin
        $checkinList = [];

        if ($isUnlockPassport == true && count($checkinLog) > 0) {
            $currentCheckinLog = $this->getCurrentCheckinLog($member);
            $alreadyCheckinToday = $this->getAlreadyCheckinToday($uid);
            $lastCheckinDate = $this->getLastCheckinDate($uid);

            foreach ($checkinLog as $checkin) {
                $checkinList[] = $this->checkInStatusInfo($member->uid, $checkin->day, $currentCheckinLog, $alreadyCheckinToday, $lastCheckinDate);
            }
        }

        $finalReward = $this->finalRewardStatusInfo($member->uid);

        $showCompensationHistory = CompensationLog::where('uid', $uid)->where('status', 'success')->count() > 0;

        return [
            'status' => true,
            "message" => 'Event Info',
            'username' => $member->username,
            'is_unlocked' => $isUnlockPassport,
            "unlock_package" => [
                "can_buy" => $unlockPackageCanBuy,
                "already_buy"  => $unlockPackageAlreadyBuy
            ],
            'checkin_list' => $checkinList,
            'final_reward' => $finalReward,
            'show_compensation_history' => $showCompensationHistory,
        ];
    }

    private function sendFirstDayCheckinReward($member)
    {
        $rewardInfo = Reward::where('package_key', 2)->where('package_type', 'checkin_reward')->first();
        $goodsData = [
            [
                'goods_id' => $rewardInfo->package_id,
                'purchase_quantity' => $rewardInfo->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40
            ]
        ];

        // set send item log
        $itemLog = $this->createSendItemLog([ //add send item log
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'day' => 1,
            'package_type' => 'checkin_reward',
            'package_key' => $rewardInfo->package_key,
            'package_id' => $rewardInfo->package_id,
            'package_name' => $rewardInfo->package_name,
            'package_quantity' => $rewardInfo->package_quantity,
            'package_amount' => $rewardInfo->package_amount,
            'image' => $rewardInfo->image,
            'goods_data' => json_encode($goodsData),
            'status' => 'pending',
            'last_ip' => $this->getIP(),
            'log_date' => (string) date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
        ]);

        //send item
        $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
        $send_result = json_decode($send_result_raw);
        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            //do update send item log
            $this->updateSendItemLog([
                'send_item_status' => $send_result->status ?: false,
                'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                'status' => 'success'
            ], $itemLog['id'], $member->uid);
        } else {
            //do update send item log
            $this->updateSendItemLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
            ], $itemLog['id'], $member->uid);
        }
    }

    private function getCurrentCheckinLog($member)
    {
        $lastCheckinLog = MemberCheckin::where('uid', $member->uid)->where('is_checkin', 1)->latest('day')->first();

        if ($lastCheckinLog->day == 14) {
            return $lastCheckinLog;
        }

        $nextDay = $lastCheckinLog ? $lastCheckinLog->day + 1 : 2;

        $currentCheckinLog = MemberCheckin::where('uid', $member->uid)->where('is_checkin', 0)->where('day', $nextDay)->first();

        return $currentCheckinLog;
    }

    private function getCheckinCount($uid)
    {
        return MemberCheckin::where('uid', $uid)
            ->where('is_checkin', 1)
            ->count();
    }

    private function getFinalRewardsClaimCount($uid)
    {
        return ItemLog::where('uid', $uid)
            ->where('package_type', 'final_reward')
            ->count();
    }

    private function createDeductLog(array $arr)
    {
        if ($arr) {
            $resp = DeductLog::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return false; //fix
            }
        }
        return null;
    }

    private function updateDeductLog(array $arr, $log_id)
    {
        if ($arr) {
            return DeductLog::where('id', $log_id)->update($arr);
        }
        return null;
    }

    private function updateCheckinLog(array $arr, $uid, $day)
    {
        if ($arr) {
            return MemberCheckin::where('uid', $uid)->where('day', $day)->update($arr);
        }
        return null;
    }

    public function getUnlockPassport(Request $request)
    {

        if ($request->has('type') == false || $request->type != 'unlock_passport') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        // get events setting info
        $eventSetting = $this->getEventSetting();

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($member->is_unlocked == 1) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย คุณได้ซื้อแพ็คเกจกำราบมังกรไปแล้ว'
            ]);
        }

        if (time() > strtotime($eventSetting->unlock_package_end)) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย ไม่อยู่ในช่วงเวลากิจกรรม'
            ]);
        }

        // check diamonds balance
        $diamondBalance = $this->setDiamondsBalance($member->uid);
        if ($diamondBalance < $eventSetting->diamonds_unlock) {
            return response()->json([
                'status' => false,
                'message' => 'ไดมอนด์ไม่เพียงพอ'
            ]);
        }

        // set deduct diamonds
        $logDeduct = $this->createDeductLog([
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'deduct_type' => 'unlock_reward',
            'diamonds' => $eventSetting->diamonds_unlock,
            'status' => 'pending',
            'last_ip' => $this->getIP(),
            'log_date' => (string) date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
        ]);

        if ($logDeduct) {

            // set desuct diamonds
            $deductData = $this->setDiamondDeductData($eventSetting->diamonds_unlock);

            $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
            $resp_deduct = json_decode($resp_deduct_raw);
            if (is_object($resp_deduct) && is_null($resp_deduct) == false) {

                $this->updateDeductLog([
                    'before_deduct_diamond' => $diamondBalance,
                    'after_deduct_diamond' => $diamondBalance - $eventSetting->diamonds_unlock,
                    'deduct_status' => $resp_deduct->status ?: 0,
                    'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
                    'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
                    'deduct_data' => json_encode($deductData),
                    'status' => 'success'
                ], $logDeduct['id']);

                // get unlock passport package
                $unlockPackage = Reward::where('package_type', 'unlock_reward')->first();
                if (!$unlockPackage) {
                    return response()->json([
                        'status' => false,
                        'message' => 'ขออภัย<br />ไม่สามารถดำเนินการได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }

                $goodsData = [
                    [
                        'goods_id' => $unlockPackage->package_id,
                        'purchase_quantity' => $unlockPackage->package_quantity,
                        'purchase_amount' => 0,
                        'category_id' => 40
                    ]
                ];

                // set send item log
                $itemLog = $this->createSendItemLog([ //add send item log
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'package_type' => 'unlock_reward',
                    'package_key' => $unlockPackage->package_key,
                    'package_id' => $unlockPackage->package_id,
                    'package_name' => $unlockPackage->package_name,
                    'package_quantity' => $unlockPackage->package_quantity,
                    'package_amount' => $unlockPackage->package_amount,
                    'image' => $unlockPackage->image,
                    'goods_data' => json_encode($goodsData),
                    'status' => 'pending',
                    'last_ip' => $this->getIP(),
                    'log_date' => (string) date('Y-m-d'), // set to string
                    'log_date_timestamp' => time(),
                ]);

                //send item
                $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                    //do update send item log
                    $this->updateSendItemLog([
                        'send_item_status' => $send_result->status ?: false,
                        'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                        'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                        'status' => 'success'
                    ], $itemLog['id'], $member->uid);

                    $member->is_unlocked = 1;
                    if ($member->save()) {
                        $eventInfo = $this->setEventInfo($member->uid);

                        return response()->json([
                            'status' => true,
                            'message' => $unlockPackage->package_name,
                            'data' => $eventInfo,
                        ]);
                    } else {
                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถดำเนินการได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                        ]);
                    }
                } else {
                    //do update send item log
                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                        'status' => 'unsuccess'
                    ], $itemLog['id'], $member->uid);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                    ]);
                }
            } else {
                $arr_log['status'] = 'unsuccess';
                $this->updateDeductLog($arr_log, $logDeduct['id']);
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถดำเนินการหัก Diamonds ได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />กรุณาลองใหม่อีกครั้ง'
            ]);
        }
    }

    public function getCheckin(Request $request)
    {

        if ($request->has('type') == false || $request->type != 'checkin') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        // get events setting info
        $eventSetting = $this->getEventSetting();

        $userData = $this->userData;
        $uid = $userData['uid'];
        $day = $request->day;

        if ($day < 2 || $day > $eventSetting->num_days) { // num_days = 14
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter(3)!'
            ]);
        }

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($member->is_unlocked == 0) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย คุณยังไม่ได้ซื้อแพ็คเกจกำราบมังกร'
            ]);
        }

        $currentCheckinLog = $this->getCurrentCheckinLog($member);
        $alreadyCheckinToday = $this->getAlreadyCheckinToday($uid);
        $lastCheckinDate = $this->getLastCheckinDate($uid);
        $checkinStatusInfo = $this->checkInStatusInfo($member->uid, $day, $currentCheckinLog, $alreadyCheckinToday, $lastCheckinDate);

        if ($checkinStatusInfo['can_checkin'] == false) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />คุณไม่สามารถเช็คอินได้'
            ]);
        }

        if ($checkinStatusInfo['is_checkin'] == true) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />คุณได้เช็คอินไปก่อนหน้านี้แล้ว'
            ]);
        }

        // Reward Info
        $rewardInfo = Reward::where('package_key', $checkinStatusInfo['package_key'])->where('package_type', 'checkin_reward')->first();
        $goodsData = [
            [
                'goods_id' => $rewardInfo->package_id,
                'purchase_quantity' => $rewardInfo->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40
            ]
        ];

        // set send item log
        $itemLog = $this->createSendItemLog([ //add send item log
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'day' => $day,
            'package_type' => 'checkin_reward',
            'package_key' => $rewardInfo->package_key,
            'package_id' => $rewardInfo->package_id,
            'package_name' => $rewardInfo->package_name,
            'package_quantity' => $rewardInfo->package_quantity,
            'package_amount' => $rewardInfo->package_amount,
            'image' => $rewardInfo->image,
            'goods_data' => json_encode($goodsData),
            'status' => 'pending',
            'last_ip' => $this->getIP(),
            'log_date' => (string) date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
        ]);

        //send item
        $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
        $send_result = json_decode($send_result_raw);
        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            //do update send item log
            $this->updateSendItemLog([
                'send_item_status' => $send_result->status ?: false,
                'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                'status' => 'success'
            ], $itemLog['id'], $member->uid);

            // update checkin log
            $updateCheckin = $this->updateCheckinLog([
                'is_checkin' => 1,
                'checkin_at' => date('Y-m-d'),
            ], $member->uid, $day);

            if ($updateCheckin) {

                $eventInfo = $this->setEventInfo($member->uid);

                return response()->json([
                    'status' => true,
                    'message' => $rewardInfo->package_name,
                    'data' => $eventInfo,
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถดำเนินการได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }
        } else {
            //do update send item log
            $this->updateSendItemLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
            ], $itemLog['id'], $member->uid);

            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
            ]);
        }
    }

    private function getAlreadyCheckinToday($uid)
    {
        return MemberCheckin::where('uid', $uid)->where('checkin_at', date('Y-m-d'))->count() > 0;
    }

    private function getLastCheckinDate($uid)
    {
        $eventSetting = $this->getEventSetting();
        return MemberCheckin::where('uid', $uid)->where('day', $eventSetting->num_days)->value('checkin_date');
    }

    private function checkInStatusInfo($uid, $day, $currentCheckinLog, $alreadyCheckinToday, $lastCheckinDate)
    {
        if (empty($uid) || empty($day)) {
            return false;
        }

        $currentDate = date('Y-m-d');
        $currentDateTimestamp = strtotime($currentDate);

        $checkin = MemberCheckin::where('uid', $uid)->where('day', $day)->first();
        if (!$checkin) {
            return false;
        }

        $isCheckin = false;
        $canCheckin = false;

        if ($currentDateTimestamp >= strtotime($checkin->checkin_date)) {

            if ($checkin->is_checkin == 1) {
                $isCheckin = true;
            }

            if (
                $currentDateTimestamp >= strtotime($currentCheckinLog->checkin_date)
                && $currentCheckinLog->day == $day
                && !$alreadyCheckinToday
                && date('Y-m-d') <= $lastCheckinDate
            ) {
                $canCheckin = true;
            }
        }

        // get reward info
        $rewardInfo = null;
        $rewardInfo = Reward::where('package_key', $checkin->package_key)->where('package_type', 'checkin_reward')->first();

        $rewardTitle = '';
        $rewardImage = '';

        if ($rewardInfo) {
            $rewardTitle = $rewardInfo->package_name;
            $rewardImage = $rewardInfo->image;
        }

        return [
            'day' => $checkin->day,
            'checkin_date' => $checkin->checkin_date,
            'title' => $rewardTitle,
            'img_key' => $rewardImage,
            'is_checkin' => $isCheckin,
            'can_checkin' => $canCheckin,
            'package_key' => $checkin->package_key,
        ];
    }

    private function finalRewardStatusInfo($uid)
    {
        if (empty($uid)) {
            return false;
        }

        // get events setting info
        $eventSetting = $this->getEventSetting();

        $canClaim = false;
        $hasClaimed = false;
        $rewardTitle = '';
        $rewardImage = '';
        $packagekey = 0;

        $finalRewardInfo = Reward::where('package_type', 'final_reward')->first();
        if ($finalRewardInfo) {

            $rewardTitle = $finalRewardInfo->package_name;
            $rewardImage = $finalRewardInfo->image;
            $packagekey = $finalRewardInfo->package_key;

            // check can claim
            $checkInHistorycount = $this->getCheckinCount($uid);
            if ($checkInHistorycount == $eventSetting->num_days) {
                $canClaim = true;
            }

            // check item history
            $finalRewardHistory = $this->getFinalRewardsClaimCount($uid);
            if ($finalRewardHistory > 0) {
                $hasClaimed = true;
            }
        }

        return [
            'title' => $rewardTitle,
            'img_key' => $rewardImage,
            'can_claim' => $canClaim,
            'has_claimed' => $hasClaimed,
            'package_key' => $packagekey,
        ];
    }

    // claim final reward
    public function getClaimFinalReward(Request $request)
    {
        if ($request->has('type') == false || $request->type != 'claim_final_reward') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        // check already purchase package
        if ($member->is_unlocked == 0) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />คุณยังไม่ได้ซื้อแพ็คเกจกำราบมังกร'
            ]);
        }

        $finalRewardStatusInfo = $this->finalRewardStatusInfo($member->uid);
        if ($finalRewardStatusInfo['can_claim'] == false) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />ไม่สามารถรับของวัลได้<br />เนื่องจากไม่ตรงเงื่อนไข'
            ]);
        }

        if ($finalRewardStatusInfo['has_claimed'] == true) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปแล้ว'
            ]);
        }

        // Reward Info
        $rewardInfo = Reward::where('package_type', 'final_reward')->where('package_key', $finalRewardStatusInfo['package_key'])->first();
        if (!$rewardInfo) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />ไม่สามารถดำเนินการได้<br/>กรุณาลองใหม่อีกครั้ง'
            ]);
        }

        $goodsData = [
            [
                'goods_id' => $rewardInfo->package_id,
                'purchase_quantity' => $rewardInfo->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40
            ]
        ];

        // set send item log
        $itemLog = $this->createSendItemLog([ //add send item log
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'package_type' => 'final_reward',
            'package_key' => $rewardInfo->package_key,
            'package_id' => $rewardInfo->package_id,
            'package_name' => $rewardInfo->package_name,
            'package_quantity' => $rewardInfo->package_quantity,
            'package_amount' => $rewardInfo->package_amount,
            'image' => $rewardInfo->image,
            'goods_data' => json_encode($goodsData),
            'status' => 'pending',
            'last_ip' => $this->getIP(),
            'log_date' => (string) date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
        ]);

        //send item
        $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
        $send_result = json_decode($send_result_raw);
        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            //do update send item log
            $this->updateSendItemLog([
                'send_item_status' => $send_result->status ?: false,
                'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                'status' => 'success'
            ], $itemLog['id'], $member->uid);

            $eventInfo = $this->setEventInfo($member->uid);

            return response()->json([
                'status' => true,
                'message' => $rewardInfo->package_name,
                'data' => $eventInfo,
            ]);
        } else {
            //do update send item log
            $this->updateSendItemLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
            ], $itemLog['id'], $member->uid);

            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
            ]);
        }
    }

    public function getHistory(Request $request)
    {
        if ($request->has('type') == false || $request->type != 'history') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $itemLogs = ItemLog::where('uid', $uid)->where('status', 'success')->orderBy('created_at', 'desc')->get()
            ->map(function ($itemLog) {

                $packageName = $itemLog->package_name;
                $createdAt = strtotime($itemLog->created_at);

                return [
                    'package_name' => $packageName,
                    'date' => date('d/m/Y', $createdAt),
                    'time' => date('H:i:s', $createdAt),
                ];
            });

        return response()->json([
            'status' => true,
            'message' => 'History',
            'data' => [
                'history' => $itemLogs,
            ],
        ]);
    }

    public function getCompensationHistory(Request $request)
    {
        if ($request->has('type') == false || $request->type != 'compensation_history') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $compensationLogs = CompensationLog::where('uid', $uid)->where('status', 'success')->orderBy('created_at', 'desc')->get()
            ->map(function ($comLog) {

                $packageName = $comLog->package_name;
                $createdAt = strtotime($comLog->created_at);

                return [
                    'package_name' => $packageName,
                    'date' => date('d/m/Y', $createdAt),
                    'time' => date('H:i:s', $createdAt),
                ];
            });

        return response()->json([
            'status' => true,
            'message' => 'Compensation History',
            'data' => [
                'history' => $compensationLogs,
            ],
        ]);
    }
}
