<?php

    namespace App\Http\Controllers\Api\Test\airpay_dec2018;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DB;

    use App\Models\Test\airpay_dec2018\Member;
    use App\Models\Test\airpay_dec2018\Reward;
    use App\Models\Test\airpay_dec2018\Play;
    use App\Models\Test\airpay_dec2018\PlayData;
    use App\Models\Test\airpay_dec2018\Exchange;

    class IndexController extends BnsEventController{

    	private $baseApi = 'http://api.apps.garena.in.th';
        private $startTime = '2018-12-12 08:00:00';
        private $endTime = '2019-01-31 23:59:50';

        private $startGetApiTime = '00:00:00';
        private $endGetApiTime = '23:59:59';

        private $awtSecret = 'Hjk5JNC04azhY38NXfpZgJJpPucw2Q4h';

        private $diamondStartTime = '2018-12-12 08:00:00';
        private $diamondEndTime = '2019-01-30 23:59:50';

        private $maxTicket= 1500;

        private $bnsAppId = 32835;

        public function __construct()
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getUserData();

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
             if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            // $this->userData = $this->getUserData();
            // dd($this->userData);
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'total_diamonds' => 0,
                    // 'diamond_points' => 0,
                    // 'used_diamond_points' => 0,
                    'last_ip' => $this->getIP()
                ];
                // dd(12);
                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $id)
        {
            if(empty($id))
                return false;

            return Member::where('id', $id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:AIRPAY_DEC_2018:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events bns airpay topup december 2018.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            Play::where('id', $log_id)->where('uid', $uid)->update($arr);
            PlayData::where('play_id', $log_id)->where('uid', $uid)->update($arr);
            return true;
        }
        private function updateItemExchange($arr, $log_id, $uid) {
            Exchange::where('id', $log_id)->where('uid', $uid)->update($arr);
            return true;
        }

        private function checkItemHistory($uid,$package_id){
            $itemHistory = ItemLog::where('uid', $uid)
                                ->where('package_id', $package_id)
                                ->count();
            return $itemHistory;
        }

        private function doGetTermgameDiamondHistory($uid) {
            if(empty($uid)){
                return false;
            }
            // Cache::forget('BNS:TOPUP_NOVEMBER2018:TERMGAME_DIAMONDS_HISTORY_' . $uid);
            return Cache::remember('BNS:AIRPAY_TOPUP_DECEMBER2018:TERMGAME_DIAMONDS_HISTORY_' . $uid, 5, function() use($uid) {
                return json_decode($this->post_api($this->baseApi, [
                            'key_name' => 'gws_test',
                            'service' => 'get_user_topup',
                            'uid' => $uid,
                            'start_ts' => strtotime($this->diamondStartTime),
                            'end_ts' => strtotime($this->diamondEndTime),
                            'app_id' => $this->bnsAppId
                        ]));
            });
        }

        private function doGetDiamondHistory($ncid) {
            if(empty($ncid)){
                return false;
            }
            // $ncid='7CB03022-2B70-40C0-B5C1-9AA84FC23312';
            // Cache::forget('BNS:TOPUP_NOVEMBER2018:EXCHANGE_TO_DIAMONDS_HISTORY_' . $ncid);
            return Cache::remember('BNS:AIRPAY_TOPUP_DECEMBER2018:EXCHANGE_TO_DIAMONDS_HISTORY_' . $ncid, 5, function() use($ncid) {
                return json_decode($this->post_api($this->baseApi, [
                            'key_name' => 'bns',
                            'service' => 'query_cash_txn',
                            'user_id' => $ncid,//ncid
                            'start_time' => $this->diamondStartTime,
                            'end_time' => $this->diamondEndTime
                        ]));
            });
        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return false;
            }
            $member = Member::where('uid', $uid)->first();


            $result_diamond = 0;
            // $member->ncid='7CB03022-2B70-40C0-B5C1-9AA84FC23312';
            $resultDiamond = $this->doGetDiamondHistory($member->ncid);
            if ($resultDiamond->status == true) {
                $contentDiamond = $resultDiamond->response;
                if (count($contentDiamond) > 0) {
                    $collectionDiamond = collect($contentDiamond);
                    $result_diamond += $collectionDiamond->where('channel','airpay')->sum('diamond');
                }
            }

            if($result_diamond > $member->total_diamonds){
                $this->updateMember([
                    'total_diamonds' => $result_diamond
                ], $member->id);
            }

            $totalDiamonds = ($result_diamond > 0 && $result_diamond > $member->total_diamonds) ? $result_diamond : $member->total_diamonds;

            //remain ticket
            $ticket_used=Play::getAmtTicketUsed();
            $remain_ticket=$this->maxTicket - $ticket_used;

            $cal_ticket=floor($totalDiamonds/30000);
            $my_ticket = $cal_ticket>5 ? 5 : $cal_ticket;

            $myused_ticket= Play::getAmtTicketUsedByUid($member->uid);
            $remain_my_ticket = $my_ticket-$myused_ticket;

            $canplay=$remain_my_ticket>0 && $remain_ticket>0 ? true : false;

            $this->updateMember([
                'ticket_amount' => $my_ticket
            ], $member->id);

            $reward = new Reward;

            $packages = [];

            // check can receive reward
            for($i=1;$i<=6;$i++){

                $can_receive = false;
                $received = false;

                $packageInfo = [];
                $packageInfo = $reward->setRewardByPackage($i);

                if($member->coin_amount>=$packageInfo['require_coins']){
                    $can_receive = true;
                }

                $packages[] = [
                    'package_id' => $packageInfo['package_id'],
                    'package_title' => $packageInfo['package_title'],
                    'package_desc' => $packageInfo['package_desc'],
                    'can_receive' => $can_receive
                ];

            }

            $gachas = [];
            for($i=1;$i<=5;$i++){

                $packageInfo = [];
                $packageInfo = $reward->setGachaByKey($i);

                $gachas[] = [
                    'package_id' => $packageInfo['package_id'],
                    'package_title' => $packageInfo['package_title'],
                    'package_desc' => $packageInfo['package_desc'],
                ];

            }


            return [
                'mypoint' => $totalDiamonds,
                'remain_ticket' => $remain_ticket,
                'remain_play' => $remain_my_ticket,
                'remain_my_ticket' => $my_ticket,
                'canplay'=>$canplay,
                'coin_amount'=>$member->coin_amount,
                'packages' => $packages,
                'gachas'=>$gachas

            ];
        }

        public function getEventInfo(Request $request){
            // $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            // if (is_null($decoded)) {
            //     return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            // }
            // $decoded = collect($decoded);
            $decoded = $request;
            // dd($decoded->has('type'));
            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            // $uid=200062;
            // $ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
            // get member info
            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'message'=>'success',
                        'data' => $eventInfo,
                    ]);

        }

        public function playGacha(Request $request){
            // $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            // if (is_null($decoded)) {
            //     return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            // }
            // $decoded = collect($decoded);
            $decoded = $request;

            // dd($decoded->has('type'));
            if ($decoded->has('type') == false || $decoded['type'] != 'play') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $ticket_used=Play::getAmtTicketUsed();
            $remain_ticket=$this->maxTicket - $ticket_used;
            //ticket server check
            if($remain_ticket<1){
                return response()->json([
                    'status' => false,
                    'message' => 'สิทธิ์ในการเล่นกิจกรรมครบแล้ว'
                ]);
            }


            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            // $uid=200062;
            // $ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $myused_ticket= Play::getAmtTicketUsedByUid($member->uid);
            $remain_my_ticket = $member->ticket_amount-$myused_ticket;
            $remain_my_ticket=2;
            if($remain_my_ticket<1){
                return response()->json([
                    'status' => false,
                    'message' => 'สิทธิ์ของคุณในการเล่นไม่เพียงพอ'
                ]);
            }

            $gacha_item=$this->gachaRandom();
            $all_get = Reward::fixList();
            $product_set=collect($gacha_item['product_set']);
            $gacha_item['product_set']=$product_set->concat($all_get)->toArray();
            foreach ($gacha_item['product_set'] as $key_pro => $product) {
                $goods_data[] = [
                    'goods_id' => $product['product_id'],
                    'purchase_quantity' => $product['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];
            }
            $send_item_data=collect($goods_data);
            $send_item_data->pop();
            // dd($goods_data,$send_item_data->all());
            $play_add=new Play;
            $play_add->ncid=$member->ncid;
            $play_add->uid=$member->uid;
            $play_add->package_id=$gacha_item['package_id'];
            $play_add->status='pending';
            if($play_add->save()){
                $play_id=$play_add->id;

                foreach($gacha_item['product_set'] as $key=>$value){
                    $play_data=new PlayData;
                    $play_data->play_id=$play_id;
                    $play_data->ncid=$member->ncid;
                    $play_data->uid=$member->uid;
                    $play_data->package_id=$gacha_item['package_id'];
                    $play_data->item_id=$value['product_id'];
                    $play_data->item_name=$value['product_title'];
                    $play_data->item_amount=$value['product_quantity'];
                    $play_data->item_type=$value['product_type'];
                    $play_data->status='pending';
                    $play_data->send_item_status='pending';
                    $play_data->good_data=json_encode($goods_data);
                    $play_data->log_date=(string)date('Y-m-d');
                    $play_data->log_date_timestamp=time();
                    $play_data->save();

                }
            }else{
                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถเล่นได้ โปรดลองใหม่อีกครั้ง",
                        ]);
            }

            //send item

            $send_result_raw = $this->dosendItem($member->ncid, $send_item_data->all());
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) && $send_result->status ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'success'
                ],$play_id, $member->uid);

                $member->coin_amount=$member->coin_amount+2;
                $member->save();
                $eventInfo = $this->setEventInfo($member->uid);

                return response()->json([
                            'status' => true,
                            'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                            'gacha'=>$gacha_item['package_id']-1,
                            'data' => $eventInfo
                        ]);
            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                        ], $play_id, $member->uid);

                $eventInfo = $this->setEventInfo($member->uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                            'data' => $eventInfo
                        ]);
            }

        }

        private function gachaRandom(){
            $random_num=rand(0,100000);
            $rewardlist=Reward::gachaList();
            foreach($rewardlist as $key=>$value){
                if($random_num>=$value['rate_min'] && $random_num<=$value['rate_max']){
                    return $rewardlist[$key];
                }
            }
        }

        public function exchangeItem(Request $request){
            // $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            // if (is_null($decoded)) {
            //     return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            // }
            // $decoded = collect($decoded);
            $decoded = $request;

            // dd($decoded->has('type'));
            if ($decoded->has('type') == false || $decoded['type'] != 'exchange') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (1)'
                ]);
            }

            if ($decoded->has('id') == false ) {//check id
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (2)'
                ]);
            }

            $id=$decoded->id;
            if($id<1 || $id>6){
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (3)'
                ]);
            }

            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            // $uid=200062;
            // $ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            $reward = new Reward;
            $packageInfo = $reward->setRewardByPackage($id);

            if($member->coin_amount<$packageInfo['require_coins']){
                return response()->json([
                            'status' => false,
                            'message' => 'เหรียญไม่พอในการแลกของรางวัล'
                ]);
            }

            $goods_data = []; //good data for send item group

            $productList = $packageInfo['product_set'];

            foreach ($productList as $key => $product) {
                $goods_data[] = [
                    'goods_id' => $product['product_id'],
                    'purchase_quantity' => $product['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];
            }

            $ex_add=new Exchange;
            $ex_add->ncid=$member->ncid;
            $ex_add->uid=$member->uid;
            $ex_add->item_id=$packageInfo['product_set'][0]['product_id'];
            $ex_add->item_name=$packageInfo['package_title'];
            $ex_add->item_amount=$packageInfo['product_set'][0]['product_quantity'];
            $ex_add->coin_used=$packageInfo['require_coins'];
            $ex_add->good_data=json_encode($goods_data);
            $ex_add->status='pending';
            $ex_add->send_item_status='pending';

            if($ex_add->save()){
                $exchange_id=$ex_add->id;
                $member->coin_amount=$member->coin_amount-$packageInfo['require_coins'];
                $member->save();

                //send item
                $send_result_raw = $this->dosendItem($member->ncid, $goods_data);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                    $this->updateItemExchange([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                        'status' => 'success'
                    ], $exchange_id, $member->uid);

                    $eventInfo = $this->setEventInfo($member->uid);

                    return response()->json([
                                'status' => true,
                                'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                                'data' => $eventInfo
                            ]);
                }else{
                    $this->updateItemExchange([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                        'status' => 'unsuccess'
                            ], $exchange_id, $member->uid);

                    $eventInfo = $this->setEventInfo($member->uid);

                    return response()->json([
                                'status' => false,
                                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                                'data' => $eventInfo
                            ]);
                }
            }else{
                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถแลกไอเท็มได้ โปรดลองใหม่อีกครั้ง",
                        ]);
            }


        }

    }
