<?php

    namespace App\Http\Controllers\Api\Test\greyed_out2019;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DB;

    use App\Models\Test\greyed_out2019\BnsEvent;
    use App\Models\Test\greyed_out2019\Member;
    use App\Models\Test\greyed_out2019\UserGroup;
    use App\Models\Test\greyed_out2019\DeductLog;
    use App\Models\Test\greyed_out2019\ItemLog;

    class IndexController extends BnsEventController{

    	private $baseApi = 'http://api.apps.garena.in.th';

        public function __construct(Request $request)
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
             if($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            // dd($this->startTime,$this->endTime);
            if ((time() < strtotime($eventSetting->start_time) || time() > strtotime($eventSetting->end_time))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getEventSetting(){
            // return Cache::remember('BNS:GREYEDOUT2019:EVENT_SETTING_TEST', 10, function() {
                        return BnsEvent::where('event_key','greyed_out2019')->first();
                    // });
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }

                // get user group
                $group_id = 0;
                $groupData = UserGroup::where('uid', (string)$uid)->first();
                if($groupData){
                    $group_id = (int)$groupData->group_id;
                }else{
                    $group_id = 4;
                }

                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'group_id' => $group_id,
                    'selected_pool' => false, // true, false => for check user hsa selected 2 items
                    'pool_list' => [], // ref #1
                    'pool_round' => 0, // current pool round
                    'pool_status' => false,
                    'created_at_timestamp' => time(),
                    'created_at_string'=>Carbon::now()->toDateString(),
                    'last_ip' => $this->getIP()
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['_id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('_id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:GREYEDOUT2019:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events bns greyed out 2019.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function diamondBalance(int $uid) {
            return $this->post_api($this->baseApi, [
                        'key_name' => 'bns',
                        'service' => 'balance',
                        'uid' => $uid,
            ]);
        }

        private function setRemainDiamonds($uid){
            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return 0;
            }else{
                return intval($diamondsBalance->balance);
            }
        }

        private function setDiamondDeductData($deduct_diamond_amount=0)
        {
            if($deduct_diamond_amount==0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test deduct diamomds for garena BNS Greyed Out 2019 Lucky Spin.',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addDeductLog(array $arr) {
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductLog(array $arr, $logId) {
            if ($arr) {
                return DeductLog::where('_id', $logId)->update($arr);
            }
            return null;
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemLog::where('_id', $log_id)->where('uid', $uid)->update($arr);
        }

        public function getEventInfo(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            
            // get member info
            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                    'status' => true,
                    'content' => $eventInfo
                ]);

        }

        private function setEventInfo($uid){

            if(empty($uid)){
                return false;
            }

            $eventSetting = $this->getEventSetting();

            $member = Member::where('uid', $uid)->first();

            $diamonds = $this->setRemainDiamonds($member->uid);

            // check current pool round
            $currentPoolRound = 0;
            if($member->selected_pool == true){
                if($member->pool_round >= 0 && $member->pool_round < 6){
                    $currentPoolRound = $member->pool_round+1;
                }else if($member->pool_round >= 6){
                    $currentPoolRound = 6;
                }
            }

            // set pool price
            $poolPrice = 0;
            if($currentPoolRound > 0){
                $poolPrice = $eventSetting->price_pool[$currentPoolRound-1];
            }

            $poolList = [];
            if(count($member->pool_list) > 0){
                foreach($member->pool_list as $poolListSelected){
                    $collection = [];
                    $collection = collect($poolListSelected);
                    $poolList[] = $collection->except(['product_id','weight','product_quantity'])->all();
                }
            }

            // dd($poolList);

            $ItemLisst = [];
            $a=0;
            $b=0;
            $poolListSlice = array_slice($poolList, 0, 2);
            for($i=0;$i<10;$i++){
                if($i%2 == 0){

                    $isSelected = false;
                    for($j=0;$j<count($poolListSlice);$j++){
                        if((isset($poolListSlice[$j]['product_key']) && $poolListSlice[$j]['product_key'] == $eventSetting->rewards_pool[0][$a]['product_key']) && (isset($poolListSlice[$j]['pool_group']) && $poolListSlice[$j]['pool_group'] == $eventSetting->rewards_pool[0][$a]['pool_group']) ){
                            $isSelected = true;
                            break;
                        }    
                    }
                    
                    $ItemLisst[] = [
                        'item_key' => $eventSetting->rewards_pool[0][$a]['product_key'],
                        'group' => $eventSetting->rewards_pool[0][$a]['pool_group'],
                        'title' => $eventSetting->rewards_pool[0][$a]['product_title'],
                        'amount' => $eventSetting->rewards_pool[0][$a]['amount'],
                        'image' => $eventSetting->rewards_pool[0][$a]['image'],
                        'selected' => $isSelected,
                    ];
                    $a++;

                }else{

                    $isSelected = false;
                    for($j=0;$j<count($poolListSlice);$j++){
                        if((isset($poolListSlice[$j]['product_key']) && $poolListSlice[$j]['product_key'] == $eventSetting->rewards_pool[1][$b]['product_key']) && (isset($poolListSlice[$j]['pool_group']) && $poolListSlice[$j]['pool_group'] == $eventSetting->rewards_pool[1][$b]['pool_group']) ){
                            $isSelected = true;
                            break;
                        }    
                    }

                    $ItemLisst[] = [
                        'item_key' => $eventSetting->rewards_pool[1][$b]['product_key'],
                        'group' => $eventSetting->rewards_pool[1][$b]['pool_group'],
                        'title' => $eventSetting->rewards_pool[1][$b]['product_title'],
                        'amount' => $eventSetting->rewards_pool[1][$b]['amount'],
                        'image' => $eventSetting->rewards_pool[1][$b]['image'],
                        'selected' => $isSelected,
                    ];
                    $b++;
                }
            }
            

            return [
                'username' => $member->username,
                'diamonds' => number_format($diamonds),
                'selected_pool' => $member->selected_pool,
                'current_round' => $currentPoolRound,
                'pool_price' =>  $poolPrice,
                'pool_status' => $member->pool_status,
                'pool_list' => $poolList,
                'item_list' => $ItemLisst,
            ];
        }

        public function getSelectReward(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'select_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('item_key') == false || $decoded->has('group') == false) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter! (2)'
                ]);
            }

            if(!in_array($decoded->item_key, [1,2,3,4,5]) || !in_array($decoded->group, [1,2])){
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter! (3)'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $item_key = (int)$decoded->item_key;
            $group = (int)$decoded->group;
            
            // get member info
            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check user has select item completed
            if($member->selected_pool == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้เลือกไอเทมครบจำนวนแล้ว'
                ]);
            }

            $poolListCount = count($member->pool_list);

            // check user is already select item in pool list
            if($poolListCount > 0){
                foreach($member->pool_list as $selectedPool){
                    if($selectedPool['product_key'] == $item_key && $selectedPool['pool_group'] == $group){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />คุณได้เลือกไอเทมชิ้นนี้ไปแล้ว'
                        ]);
                        exit;
                    }
                }
            }

            $eventSetting = $this->getEventSetting();

            // get weight by group
            $weightData = $eventSetting->groups_weight[$member->group_id-1];

            // set selected reward
            $selectReward = $member->pool_list;
            if($group == 1){
                $weight = $weightData[$poolListCount];
                array_push($selectReward, [
                    'product_key' => $eventSetting->rewards_pool[0][$item_key-1]['product_key'],
                    'pool_group' => $eventSetting->rewards_pool[0][$item_key-1]['pool_group'],
                    'product_id' => $eventSetting->rewards_pool[0][$item_key-1]['product_id'],
                    'product_title' => $eventSetting->rewards_pool[0][$item_key-1]['product_title'],
                    'product_quantity' => $eventSetting->rewards_pool[0][$item_key-1]['product_quantity'],
                    'amount' => $eventSetting->rewards_pool[0][$item_key-1]['amount'],
                    'image' => $eventSetting->rewards_pool[0][$item_key-1]['image'],
                    'weight' => $weight,
                    'status' => false,
                ]);
            }elseif($group == 2){
                $weight = $weightData[$poolListCount];
                array_push($selectReward, [
                    'product_key' => $eventSetting->rewards_pool[1][$item_key-1]['product_key'],
                    'pool_group' => $eventSetting->rewards_pool[1][$item_key-1]['pool_group'],
                    'product_id' => $eventSetting->rewards_pool[1][$item_key-1]['product_id'],
                    'product_title' => $eventSetting->rewards_pool[1][$item_key-1]['product_title'],
                    'product_quantity' => $eventSetting->rewards_pool[1][$item_key-1]['product_quantity'],
                    'amount' => $eventSetting->rewards_pool[1][$item_key-1]['amount'],
                    'image' => $eventSetting->rewards_pool[1][$item_key-1]['image'],
                    'weight' => $weight,
                    'status' => false,
                ]);
            }

            if(empty($selectReward) || count($selectReward) <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'No select data.'
                ]);
            }

            if(count($selectReward) == 2){

                // set fixed item
                $fixedItemList = $eventSetting->rewards_fixed;
                $order = count($selectReward); // 2
                foreach($fixedItemList as $item){
                    $weight = $weightData[$order];
                    array_push($selectReward, [
                        'product_key' => $item['product_key'],
                        'pool_group' => $item['pool_group'],
                        'product_id' => $item['product_id'],
                        'product_title' => $item['product_title'],
                        'product_quantity' => $item['product_quantity'],
                        'amount' =>  $item['amount'],
                        'image' => $item['image'],
                        'weight' => $weight,
                        'status' => false,
                    ]);
                    $order++;
                }

                $member->selected_pool = true;
                $member->pool_list = $selectReward;

            }else{
                $member->pool_list = $selectReward;
            }

            if($member->save()){

                $eventInfo = $this->setEventInfo($member->uid);

                return response()->json([
                    'status' => true,
                    'message' => "คุณเลือกไอเทมเรียบร้อยแล้ว",
                    'content' => $eventInfo
                ]);


            }else{
                return response()->json([
                    'status' => false,
                    'message' => "ไม่สามารถดำเนินกานได้ <br /> กรุณาลองใหม่อีกครั้ง",
                ]);
            }

        }

        public function getLetsPool(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'lets_pool') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $item_key = (int)$decoded->item_key;
            $group = (int)$decoded->group;
            
            // get member info
            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->pool_status == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br/>คุณได้หมุน Lucky Spin ครบหมดแล้ว'
                ]);
            }

            if(count($member->pool_list) < 6){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br/>คุณได้หมุนยังเลือกไอเทมไม่ครบ'
                ]);
            }


            // Test
            /* $poolCollection = collect($member->pool_list);
            $currentPoolList = [];
            $weightSum = 0;
            if($poolCollection->count() > 0){
                foreach($poolCollection as $list){
                    if($list['status'] == false){
                        $currentPoolList[] = $list;
                        $weightSum += $list['weight'];
                    }
                }
            }

            $poolResult = $this->randomByWeight($currentPoolList,$weightSum);
            if(!$poolResult){
                return response()->json([
                    'status' => false,
                    'message' => 'No lucky spin data required.'
                ]);
            }
            
            $newPoolCollection = $poolCollection->map(function ($item, $key) use($poolResult) {
                if($item['product_id'] == $poolResult['product_id']) {
                    $item['status'] = true;
                } 
                return [
                    'product_key' => $item['product_key'],
                    'pool_group' => $item['pool_group'],
                    'product_id' => $item['product_id'],
                    'product_title' => $item['product_title'],
                    'image' => $item['image'],
                    'weight' => $item['weight'],
                    'status' => $item['status'],
                ]; 
            });

            dd($poolResult); */

            // Test


            // get events setting info
            $eventSetting = $this->getEventSetting();


            // check current pool round
            $currentPoolRound = 0;
            if($member->selected_pool == true){
                if($member->pool_round >= 0 && $member->pool_round < 6){
                    $currentPoolRound = $member->pool_round+1;
                }else if($member->pool_round >= 6){
                    $currentPoolRound = 6;
                }
            }

            // set diamonds 
            $diamonds = $this->setRemainDiamonds($member->uid);

            // set pool price
            $poolPrice = 0;
            if($currentPoolRound > 0){
                $poolPrice = $eventSetting->price_pool[$currentPoolRound-1];
            }

            // compare remain and required diamonds
            if($diamonds < $poolPrice && $poolPrice > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br/>ไดมอนด์ของคุณไม่พอหมุน Lucky Spin'
                ]);
            }

            // set deduct diamonds
            // set deduct diamonds
            $logDeduct = $this->addDeductLog([
                'uid'                       => $member->uid,
                'username'                  => $member->username,
                'ncid'                      => $member->ncid,
                'group_id'                  => $member->group_id,
                'pool_round'                => $currentPoolRound,
                'pool_price'                => $poolPrice,
                'status'                    => 'pending',
                'log_date'                  => date('Y-m-d H:i:s'), // set to string
                'log_date_timestamp'        => time(),
                'last_ip'                   => $this->getIP(),
            ]);

            // set deduct diamonds
            $deductData = $this->setDiamondDeductData($poolPrice);
            $respDeductRaw = $this->deductDiamond($ncid, $deductData);
            $respDeduct = json_decode($respDeductRaw);
            if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){

                // update deduct log
                $arrDeductDiamond = [
                    'before_deduct_diamond' => $diamonds,
                    'after_deduct_diamond' => $diamonds - $poolPrice,
                    'deduct_status' => $respDeduct->status ?: 0,
                    'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
                    'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
                    'deduct_data' => json_encode($deductData),
                    'status' => 'success'
                ];

                $respDeduct = $this->updateDeductLog($arrDeductDiamond, $logDeduct['_id']);


                if(!$respDeduct){
                    return response()->json([
                        'status' => false,
                        'message' => 'ขออภัย<br />ไม่สามารถดำเนินการหักไดมอนด์ได้'
                    ]);
                }

                // set new pool list
                $poolCollection = collect($member->pool_list);
                $currentPoolList = [];
                $weightSum = 0;
                if($poolCollection->count() > 0){
                    foreach($poolCollection as $list){
                        if($list['status'] == false){
                            $currentPoolList[] = $list;
                            $weightSum += $list['weight'];
                        }
                    }
                }

                $poolResult = $this->randomByWeight($currentPoolList,$weightSum);
                if(!$poolResult){
                    return response()->json([
                        'status' => false,
                        'message' => 'No lucky spin data required.'
                    ]);
                }

                // set new pool list array to update member info
                $newPoolCollection = $poolCollection->map(function ($item, $key) use($poolResult) {
                    if($item['product_id'] == $poolResult['product_id']) {
                        $item['status'] = true;
                    } 
                    return [
                        'product_key' => $item['product_key'],
                        'pool_group' => $item['pool_group'],
                        'product_id' => $item['product_id'],
                        'product_title' => $item['product_title'],
                        'product_quantity' => $item['product_quantity'],
                        'amount' => $item['amount'],
                        'image' => $item['image'],
                        'weight' => $item['weight'],
                        'status' => $item['status'],
                    ]; 
                });

                // set reward for send
                $goods_data = []; //good data for send item group
                $goods_data[0] = [
                    'goods_id' => $poolResult['product_id'],
                    'purchase_quantity' => $poolResult['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];

                $sendItemLog = $this->addItemHistoryLog([
                    'uid' => $member->uid,
                    'ncid' => $member->ncid,
                    'group_id' => $member->group_id,
                    'deduct_id' => $logDeduct['_id'],
                    'pool_round' => $currentPoolRound,
                    'pool_price' => $poolPrice,
                    'product_id' => $poolResult['product_id'],
                    'product_title' => $poolResult['product_title'],
                    'product_quantity' => $poolResult['product_quantity'],
                    'amount' => $poolResult['amount'],
                    'product_image' => $poolResult['image'],
                    'status' => 'pending',
                    'send_item_status' => false,
                    'send_item_purchase_id' => 0,
                    'send_item_purchase_status' => 0,
                    'goods_data' => json_encode($goods_data),
                    'last_ip' => $this->getIP(),
                    'log_date' => (string)date('Y-m-d'), // set to string
                    'log_date_timestamp' => time(),
                ]);

                $send_result_raw = $this->dosendItem($ncid, $goods_data);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                    $this->updateItemHistoryLog([
                        'send_item_status' => $send_result->status ?: false,
                        'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                        'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                        'status' => 'success'
                    ], $sendItemLog['_id'], $uid);

                    // check if last pool update pool status to completed(true)
                    if($currentPoolRound >= 6){
                        $member->pool_status = true;
                    }

                    // set new pool list to member info before update
                    $member->pool_round = $currentPoolRound;
                    $member->pool_list = $newPoolCollection->toArray();
                    $member->save();

                    $eventInfo = $this->setEventInfo($uid);


                    // get cuurent key
                    $poolResultKey = -1;
                    foreach($newPoolCollection->toArray() as $keyPool=>$valPool){
                        if($valPool['product_key'] == $poolResult['product_key'] && $valPool['pool_group'] == $poolResult['pool_group']){
                            $poolResultKey = $keyPool;
                            break;
                        }
                    }

                    return response()->json([
                            'status' => true,
                            'message' => "คุณได้รับ <br />".$poolResult['product_title']." x".$poolResult['amount']." <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                            'select_key' => $poolResult['product_key'],
                            'select_group' => $poolResult['pool_group'],
                            'last_position' => $poolResultKey,
                            'content' => $eventInfo,
                        ]);

                }else{
                    $this->updateItemHistoryLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                        'status' => 'unsuccess'
                    ], $sendItemLog['_id'], $uid);

                    // $eventInfo = $this->setEventInfo($uid);

                    return response()->json([
                                'status' => false,
                                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                            ]);
                }

            }

            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />กรุณาลองใหม่อีกครั้ง'
            ]);

        }
        
        private function randomByWeight($rewards=array(),$weightSum = 0){

            if(count($rewards) <= 0 || $weightSum <= 0){
                return false;
            }

            $random = rand(0, $weightSum);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($rewards as $value) {
                $weight = $value['weight'];
                $currentChance = $accumulatedChance + $weight;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            $reward['timestamp'] = time();

            // now we have the reward
            return $reward;
        }


    }