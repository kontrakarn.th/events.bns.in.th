<?php

namespace App\Http\Controllers\Api\Test\clan_event;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

use App\Models\Test\clan_event\Member;
use App\Models\Test\clan_event\ClanRank;
use App\Models\Test\clan_event\Reward;
use App\Models\Test\clan_event\ItemLog;
use App\Models\Test\clan_event\Quest;
use App\Models\Test\clan_event\QuestMemberLog;
use App\Models\Test\clan_event\QuestDailyLog;

class IndexController extends BnsEventController
{

    private $baseApi = 'http://api.apps.garena.in.th';
    private $startTime = '2019-11-15 06:00:00';
    private $endTime = '2019-12-10 23:59:59';

    private $startJoinEvent = '2019-11-15 06:00:00';
    private $endJoinEvent = '2019-12-03 23:59:59';

    // private $startClaimReward = '2019-12-04 12:00:00';
    // private $endClaimReward = '2019-12-10 23:59:59';
    private $startClaimReward = '2019-11-19 12:00:00';
    private $endClaimReward = '2019-12-10 23:59:59';

    private $userEvent = '';

    // private $required_ranking_member_points = 900;
    private $required_ranking_member_points = 90;

    /* private $clanPointsReward = [
        4500,
        9000,
        15000
    ];

    private $memberPointsReward = [
        300,
        600,
        900
    ]; */

    private $clanPointsReward = [
        50,
        100,
        300
    ];

    private $memberPointsReward = [
        30,
        60,
        90
    ];

    public function __construct(Request $request)
    {
        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);
        $this->checkEventStatus();

    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status'  => false,
                'type'    => 'no_permission',
                'message' => 'Sorry, you do not have permission.',
            ]));
        }
        // dd($this->startTime,$this->endTime);
        if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
            die(json_encode([
                'status'  => false,
                'type'    => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม',
            ]));
        }

        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status'  => false,
                'type'    => 'maintenance',
                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์',
            ]));
        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง',
                ]));
            }
        } else {
            die(json_encode([
                'status'  => false,
                'type'    => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน',
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function is_accepted_ip()
    {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'              => $uid,
                'username'         => $username,
                'ncid'             => $ncid,
                'char_id'          => 0,
                'char_name'        => '',
                'world_id'         => 0,
                'clan_id'          => 0,
                'clan_type'        => 0,
                'clan_name'        => '',
                'clan_faction'     => 0,
                'total_points'     => 0,
                'used_points'      => 0,
                'last_ip'          => $this->getIP(),
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
            $this->userEvent = $resp;

        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id)) {
            return false;
        }

        return Member::where('id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:CLAN_EVENT:ASSOCIATE_UID_WITH_NCID_'.$uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'user_info',
                'uid'      => $uid,
            ]);
            $userNcInfo       = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }

        });
    }

    private function dosendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Test sending item from garena events bns clan event.',
            'goods'                => json_encode($goodsData),
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function doGetChar($uid, $ncid)
    {//new fixed function
        $resp_raw = $this->apiCharacters($uid, $ncid);

        $resp = json_decode($resp_raw);
        if (is_object($resp)) {
            if ($resp->status) {
                $content = [];
                $i       = 1;
                foreach ($resp->response as $key => $value) {
                    // check character has level more than or equal 60 and hm level more than or equal 12
                    if((int)$value->level >= 60 && (int)$value->mastery_level>=12){
                        $content[] = [
                            'id' => $i,
                            'char_id'   => $value->id,
                            'char_name' => $value->name,
                            'world_id' => $value->world_id,
                            'job' => $value->job,
                            'level' => $value->level,
                            'creation_time' => $value->creation_time,
                            'last_play_start' => $value->last_play_start,
                            'last_play_end' => $value->last_play_end,
                            'last_ip' => $value->last_ip,
                            'exp' => $value->exp,
                            'mastery_level' => $value->mastery_level,
                        ];
                    }

                    $i++;
                }
                return $content;
            }
        }
        return null;
    }

    private function apiCharacters($uid, $ncid)
    {
        return Cache::remember('BNS:CLAN_EVENT:CHARACTERS_'.$uid, 10, function () use ($ncid) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'characters',
                'user_id'  => $ncid,
            ]);
        });
    }

    private function apiCheckJoinedClan($uid, $charId, $worldId)
    {
        return Cache::remember('BNS:CLAN_EVENT:JOINED_CLAN_'.$uid."_".$charId, 10, function () use ($charId, $worldId) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'guild',
                'char_id'  => $charId,
                'world_id'  => $worldId,
            ]);
        });
    }

    private function apiGetClanInfo($uid, $guildId, $worldId)
    {
        return Cache::remember('BNS:CLAN_EVENT:CLAN_INFO_'.$uid."_".$guildId, 10, function () use ($guildId, $worldId) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'guild_info',
                'guild_id'  => $guildId,
                'world_id'  => $worldId,
            ]);
        });
    }

    private function checkCharHasClan($uid,$charId,$worldId){

        if(empty($charId) || empty($worldId)){
            return [
                'status'        => false,
                'clan_id'       => 0,
                'clan_name'     => '',
                'clan_type'     => 0,
                'clan_faction'  => 0,
                'joined_at'     => '',
            ];
        }
        

        $joinedClanRaw = $this->apiCheckJoinedClan($uid, $charId,$worldId);
        $resp = json_decode($joinedClanRaw);
        
        if (is_object($resp)) {
            if ($resp->status) {

                $joinedData = $resp->response;

                // get clan info
                $clanInfo = $this->apiGetClanInfo($uid,$joinedData->id,$worldId);
                $clanResp = json_decode($clanInfo);

                $clanName = '';
                $clanType = 0;
                $clanFaction = 0;

                if(is_object($clanResp) && $clanResp->response->type == 1){
                    if($clanResp->status){
                        $clanName       = $clanResp->response->name;
                        $clanType       = (int)$clanResp->response->type;
                        $clanFaction    = (int)$clanResp->response->faction;
                    }
    
                    return [
                        'status'        => true,
                        'clan_id'       => $joinedData->id,
                        'clan_name'     => $clanName,
                        'clan_type'     => $clanType,
                        'clan_faction'  => $clanFaction,
                        'joined_at'     => $joinedData->joined_at,
                    ];
                }else{
    
                    return [
                        'status'        => false,
                        'clan_id'       => 0,
                        'clan_name'     => '',
                        'clan_type'     => 0,
                        'clan_faction'  => 0,
                        'joined_at'     => '',
                    ];
                }
                
            }
        }

        return [
            'status'        => false,
            'clan_id'       => 0,
            'clan_name'     => '',
            'clan_type'     => 0,
            'clan_faction'  => 0,
            'joined_at'     => '',
        ];

    }

    public function selectCharacter(Request $request){

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'select_character') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }
        if ($decoded->has('id') == false) {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter! (2)'
            ]);
        }

        if($this->isTimeForJoinEvent() == false){
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />ไม่อยู่ในช่วงเวลาลงทะเบียนเข้าร่วมกิจกรรม'
            ]);
        }

        $charKey = (int)$decoded['id'];

        $userData = $this->userData;
        $uid = $userData['uid'];

        $member = Member::where('uid', $uid)->first();

        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'ไม่พบข้อมูล'
            ]);
        }

        if($member->char_id>0){
            return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถเปลี่ยนตัวละครได้'
            ]);
        }

        $mychar=$this->doGetChar($member->uid,$member->ncid);
        $myselect=collect($mychar)->where('id',$charKey)->first();
        if(count($myselect)==0){
            return response()->json([
                        'status' => false,
                        'message' => 'ไม่พบตัวละคร'
            ]);
        }

        $member->char_id   = (int)$myselect['char_id'];
        $member->char_name = $myselect['char_name'];
        $member->world_id       = $myselect['world_id'];

        // get clan info
        $clanInfo = $this->checkCharHasClan($member->uid,$myselect['char_id'],$myselect['world_id']);
        if($clanInfo['status'] == true){

            // check clan member is full
            $clanMember = ClanRank::where('clan_id', $clanInfo['clan_id'])->count();
            if($clanMember > 80){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนสมาชิกในแคลนได้ลงทะเบียนครบ 80 คนแล้ว'
                ]);
            }

            $member->clan_id                = $clanInfo['clan_id'];
            $member->clan_name              = $clanInfo['clan_name'];
            $member->clan_type              = $clanInfo['clan_type'];
            $member->clan_faction           = $clanInfo['clan_faction'];
            $member->joined_at              = $clanInfo['joined_at'];
            $member->joined_at_timestamp    = strtotime($clanInfo['joined_at']);
            $member->current_member         = 'yes';

            // check and create clan info
            $hasClanInfo = ClanRank::where('clan_id', $clanInfo['clan_id'])->count();
            if($hasClanInfo <= 0){
                ClanRank::create([
                    'clan_id'       => $clanInfo['clan_id'],
                    'clan_type'     => $clanInfo['clan_type'],
                    'clan_name'     => $clanInfo['clan_name'],
                    'clan_faction'  => $clanInfo['clan_faction'],
                    'world_id'      => $myselect['world_id'],
                    'total_points'  => 0,
                    'rank'          => 0,
                    'last_ip'       => $this->getIP(),
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s')
                ]);
            }

            // create member quest log
            $questListQuert = Quest::all();
            if(count($questListQuert) > 0){
                foreach($questListQuert as $questKey=>$questVal){
                    $QuestMemberLogQuery = QuestMemberLog::where('uid', $member->uid)->where('quest_code', $questVal->quest_code)->count();
                    if($QuestMemberLogQuery <= 0){
                        QuestMemberLog::create([
                            'uid'               => $member->uid,
                            'username'          => $member->username,
                            'ncid'              => $member->ncid,
                            'char_id'           => $member->char_id,
                            'world_id'          => $member->world_id,
                            'clan_id'           => $member->clan_id,
                            'quest_title'       => $questVal->quest_title,
                            'quest_code'        => $questVal->quest_code,
                            'quest_points'      => $questVal->quest_points,
                            'quest_type'        => $questVal->quest_type,
                            'completed_count'   =>  0,
                            'total_points'      => 0,
                            'rank'              => 0,
                            'last_ip'           => $this->getIP(),
                            'created_at'        => date('Y-m-d H:i:s'),
                            'updated_at'        => date('Y-m-d H:i:s')
                        ]);
                    }
                    
                }
            }

            $member->save();

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'data' => $eventInfo
                    ]);

        }

        return response()->json([
            'status' => false,
            'message' => 'ไม่พบข้อมูล'
        ]);

    }

    private function addItemHistoryLog($arr) {
        return ItemLog::create($arr);
    }

    private function updateItemHistoryLog($arr, $log_id, $uid) {
        return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
    }

    private function checkItemHistory($uid,$package_id){
        $itemHistory = ItemLog::where('uid', $uid)
                            ->where('package_id', $package_id)
                            ->count();
        return $itemHistory;
    }


    private function isTimeForClaimReward(){
        if ((time() >= strtotime($this->startClaimReward) && time() <= strtotime($this->endClaimReward))) {
            return true;
        }

        return false;
    }

    private function isTimeForJoinEvent(){
        if ((time() >= strtotime($this->startJoinEvent) && time() <= strtotime($this->endJoinEvent))) {
            return true;
        }

        return false;
    }

    private function setCanClaimClanRankReward($clanRank){

        if($clanRank <= 0){
            return [
                'status' => false,
                'package_key' => 0,
            ];
        }

        $packageKey = 0;
        if($clanRank == 1){
            $packageKey = 4;
        }elseif($clanRank >= 2 && $clanRank <= 5){
            $packageKey = 5;
        }elseif($clanRank >= 6 && $clanRank <= 10){
            $packageKey = 6;
        }

        if(in_array($packageKey, [4,5,6])){
            return [
                'status' => true,
                'package_key' => $packageKey,
            ];
        }else{
            return [
                'status' => false,
                'package_key' => 0,
            ];
        }
        
    }

    private function setEventInfo($uid){
        if(empty($uid)){
            return false;
        }

        $member = Member::where('uid', $uid)->first();

        if($member->char_id==0){

            $charData=$this->doGetChar($member->uid,$member->ncid);
            
            $content = [];
            foreach ($charData as $key => $value) {

                $clanInfo = [];
                // check character's clan info.
                $clanInfo = $this->checkCharHasClan($member->uid,$value['char_id'],$value['world_id']);
                
                if($clanInfo['status'] == true){
                    $content[] = [
                        'id' => $value['id'],
                        'char_name' => $value['char_name'],
                        'clan_name' => $clanInfo['clan_name'],
                    ];
                }
                
            }
            return [
                'username'=>$member->username,
                'character_name'=>'',
                'selected_char'=>false,
                'characters'=>$content,
            ];
        }

        // get member clan rank info
        $clanRankInfo = ClanRank::where('clan_id', $member->clan_id)->first();

        // set clan's rewards info
        $canClaimClanRewardStep1 = ($member->current_member == 'yes' && $member->total_points >= $this->memberPointsReward[0] && $clanRankInfo->total_points >= $this->clanPointsReward[0] && $this->isTimeForClaimReward() == true) ? true : false;

        $canClaimClanRewardStep2 = ($member->current_member == 'yes' && $member->total_points >= $this->memberPointsReward[1] && $clanRankInfo->total_points >= $this->clanPointsReward[1] && $this->isTimeForClaimReward() == true) ? true : false;

        $canClaimClanRewardStep3 = ($member->current_member == 'yes' && $member->total_points >= $this->memberPointsReward[2] && $clanRankInfo->total_points >= $this->clanPointsReward[2] && $this->isTimeForClaimReward() == true) ? true : false;

        // has claimed reward
        $isClaimedClanRewardStep1 = ItemLog::where('uid', $member->uid)->where('package_key', 1)->count();
        $isClaimedClanRewardStep2 = ItemLog::where('uid', $member->uid)->where('package_key', 2)->count();
        $isClaimedClanRewardStep3 = ItemLog::where('uid', $member->uid)->where('package_key', 3)->count();
        
        // check clan rank reward
        $clanName = $member->clan_name;
        $clanRank = 0;
        $clanScore = 0;
        $canClaimRankReward = false;
        $isClaimedRankReward = false;

        $clanRank = $clanRankInfo->rank;
        $clanScore = $clanRankInfo->total_points;

        if($this->isTimeForClaimReward() == true && $clanRankInfo->rank > 0 ){

            $clanRankReward = $this->setCanClaimClanRankReward($clanRankInfo->rank);

            $canClaimRankReward = ($member->current_member == 'yes' && $member->total_points >= $this->required_ranking_member_points && $clanRankReward['status'] == true && $this->isTimeForClaimReward() == true) ? true : false;

            $isClaimedClanRankReward = ItemLog::where('uid', $member->uid)->where('package_key', $clanRankReward['package_key'])->count();
            $isClaimedRankReward = $isClaimedClanRankReward > 0;

        }

        // get clan rank list without zero points
        /* $clanRankNoZeroListQuery = Cache::remember('BNS:CLAN_EVENT:CLAN_RANK_'.$uid, 10, function () {
            return ClanRank::where('rank', '>', 0)->orderBy('rank', 'ASC')->orderBy('total_points', 'DESC')->get();
        }); */
        $clanRankNoZeroListQuery = ClanRank::where('rank', '>', 0)->orderBy('rank', 'ASC')->orderBy('total_points', 'DESC')->get();
        $clanRankNoZeroList = [];
        if(count($clanRankNoZeroListQuery)>0){
            foreach($clanRankNoZeroListQuery as $clanKey=>$clanVal){
                if($clanVal->rank <= 10){
                    $clanRankNoZeroList[] = [
                        'rank'  => $clanVal->rank,
                        'name'  => $clanVal->clan_name,
                        'score' => number_format($clanVal->total_points),
                    ];
                }
            }
        }

        // get clan rank list with zero points
        /* $clanRankZeroListQuery = ClanRank::where('rank', '=', 0)->get();
        $clanRankZeroList = [];
        if(count($clanRankZeroListQuery)>0){
            foreach($clanRankZeroListQuery as $clanKey=>$clanVal){
                $clanRankZeroList[] = [
                    'rank'  => $clanVal->rank,
                    'name'  => $clanVal->clan_name,
                    'score' => number_format($clanVal->total_points),
                ];
            }
        } */

        // $clanRankList = array_merge($clanRankNoZeroList, $clanRankZeroList);

        // member's quest list
        $questListQuery = Cache::remember('BNS:CLAN_EVENT:MEMBER_QUEST_'.$uid, 10, function () use ($uid) {
            return QuestMemberLog::where('uid', $uid)->orderBy('id', 'ASC')->get();
        });
        $questList = [];
        if(count($questListQuery)>0){
            foreach($questListQuery as $questKey=>$questVal){
                $questList[] = [
                    'quest_title'       => $questVal->quest_title,
                    'quest_points'      => $questVal->quest_points,
                    'completed_count'   => $questVal->completed_count,
                    'total_points'      => $questVal->total_points,
                ];
            }
        }

        $clanMember = ClanRank::where('clan_id', $member->clan_id)->count();

        return [
            'username'=>$member->username,
            'character_name'=>$member->char_name,
            'selected_char'=>true,
            'score' => number_format($member->total_points),
            'characters'=>[],
            'clan_members'=> $clanMember,
            'quests' => $questList,
            'clan_rewards' => [
                'step1' => [
                    'id'            => 1,
                    'can_claim'     => $canClaimClanRewardStep1,
                    'is_claimed'    => $isClaimedClanRewardStep1 > 0,
                ],
                'step2' => [
                    'id'            => 2,
                    'can_claim'     => $canClaimClanRewardStep2,
                    'is_claimed'    => $isClaimedClanRewardStep2 > 0,
                ],
                'step3' => [
                    'id'            => 3,
                    'can_claim'     => $canClaimClanRewardStep3,
                    'is_claimed'    => $isClaimedClanRewardStep3 > 0,
                ],
            ],
            'rank_rewards' => [
                'rank'          => $clanRank,
                'clan_name'     => $clanName,
                'clan_score'    => number_format($clanScore),
                'can_claim'     => $canClaimRankReward,
                'is_claimed'    => $isClaimedRankReward,
            ],
            'clan_ranks' => $clanRankNoZeroList,
        ];
    }

    public function getEventInfo(Request $request){

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);
        
        // get member info
        $member = Member::where('uid', $uid)->first();

        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        $eventInfo = $this->setEventInfo($uid);
        if($eventInfo==false){
            return response()->json([
                    'status' => false,
                    'data' => $eventInfo,
                    'message'=>'ไม่พบตัวละคร'
                ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);

    }

    public function redeemClanReward(Request $request){

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'redeem_clan_reward') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!(1)'
            ]);
        }

        if($this->isTimeForClaimReward() == false){
            return response()->json([
                'status' => false,
                'message' => 'ไม่อยู่ในช่วงเวลารับของรางวัล'
            ]);
        }

        if ($decoded->has('package_key') == false || $decoded['package_key'] == '') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!(2)'
            ]);
        }

        $package_key = $decoded['package_key'];

        if(!in_array($package_key, [1,2,3])){
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!(3)'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);
        
        // get member info
        $member = Member::where('uid', $uid)->first();

        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        // get member clan rank info
        $clanRankInfo = ClanRank::where('clan_id', $member->clan_id)->first();

        // set clan's rewards info
        $canClaimClanRewardStep = ($member->current_member == 'yes' && $member->total_points >= $this->memberPointsReward[$package_key-1] && $clanRankInfo->total_points >= $this->clanPointsReward[$package_key-1] && $this->isTimeForClaimReward() == true) ? true : false;

        if($canClaimClanRewardStep == false){
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br /ไม่สามารถรับของรางวัลได้<br />เนื่องจากเงื่อนไขไม่ถูกต้อง'
            ]);
        }

        // has claimed reward
        $isClaimedClanRewardStep = ItemLog::where('uid', $member->uid)->where('package_key', $package_key)->count();

        if($isClaimedClanRewardStep > 0){
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br /คุณรับของรางวัลไปก่อนหน้านี้แล้ว'
            ]);
        }
        
        // get reward package
        $rewardInfo = Reward::where('package_key', $package_key)->first();
        if(!$rewardInfo){
            return response()->json([
                'status' => false,
                'message' => 'No package data.'
            ]);
        }

        // set reward packages
        $goods_data = []; //good data for send item group
        
        $packageTitle = $rewardInfo->package_name;
        $packageAmount = $rewardInfo->package_amount;

        $goods_data[] = [
            'goods_id' => $rewardInfo->package_id,
            'purchase_quantity' => $rewardInfo->package_quantity,
            'purchase_amount' => 0,
            'category_id' => 40
        ];

        $sendItemLog = $this->addItemHistoryLog([
            'uid' => $member->uid,
            'ncid' => $member->ncid,
            'char_id' => $member->char_id,
            'clan_id' => $member->clan_id,
            'world_id' => $member->world_id,
            'package_id' => $rewardInfo->package_id,
            'package_key' => $rewardInfo->package_key,
            'package_name' => $rewardInfo->package_name,
            'package_quantity' => $rewardInfo->package_quantity,
            'package_amount' => $rewardInfo->package_amount,
            'package_type' => $rewardInfo->package_type,
            'send_item_status' => false,
            'send_item_purchase_id' => 0,
            'send_item_purchase_status' => 0,
            'goods_data' => json_encode($goods_data),
            'status' => 'pending',
            'log_date' => (string)date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
            'last_ip' => $this->getIP(),
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);

        $send_result_raw = $this->dosendItem($member->ncid, $goods_data);
        $send_result = json_decode($send_result_raw);
        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            $this->updateItemHistoryLog([
                'send_item_status' => $send_result->status ? $send_result->status : false,
                'send_item_purchase_id' => $send_result->response->purchase_id ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ? $send_result->response->purchase_status : 0,
                'status' => 'success'
            ], $sendItemLog['id'], $member->uid);

            $eventInfo = $this->setEventInfo($member->uid);

            return response()->json([
                        'status' => true,
                        'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                        'data' => $eventInfo,
                        'reward'=>[
                            'name'=>$packageTitle
                        ]
                    ]);
        }else{
            $this->updateItemHistoryLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
                    ], $sendItemLog['id'], $member->uid);

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => false,
                        'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                        'data' => $eventInfo
                    ]);
        }


    }

    public function redeemRankReward(Request $request){

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'redeem_rank_reward') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        if($this->isTimeForClaimReward() == false){
            return response()->json([
                'status' => false,
                'message' => 'ไม่อยู่ในช่วงเวลารับของรางวัล'
            ]);
        }

        if ($decoded->has('package_key') == false || $decoded['package_key'] == '') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!(2)'
            ]);
        }

        $package_key = $decoded['package_key'];

        if(!in_array($package_key, [4,5,6])){
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!(3)'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);
        
        // get member info
        $member = Member::where('uid', $uid)->first();

        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        // get member clan rank info
        $clanRankInfo = ClanRank::where('clan_id', $member->clan_id)->first();

        if(!$clanRankInfo){
            return response()->json([
                'status' => false,
                'message' => 'No clan data.'
            ]);
        }

        if($clanRankInfo->rank <= 0){
            return response()->json([
                'status' => false,
                'message' => 'No clan rank.'
            ]);
        }

        $clanRank = $clanRankInfo->rank;
        $clanName = $member->clan_name;
        $clanScore = $clanRankInfo->total_points;

        $clanRankReward = $this->setCanClaimClanRankReward($clanRankInfo->rank);

        $canClaimRankReward = ($member->current_member == 'yes' && $member->total_points >= $this->required_ranking_member_points && $clanRankReward['status'] == true && $this->isTimeForClaimReward() == true) ? true : false;
        if($canClaimRankReward == false){
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br /ไม่สามารถรับของรางวัลได้<br />เนื่องจากเงื่อนไขไม่ถูกต้อง'
            ]);
        }

        $isClaimedClanRankReward = ItemLog::where('uid', $member->uid)->where('package_key', $clanRankReward['package_key'])->count();
        if($isClaimedClanRankReward > 0){
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br /คุณรับของรางวัลไปก่อนหน้านี้แล้ว'
            ]);
        }


        // get reward package
        $rewardInfo = Reward::where('package_key', $clanRankReward['package_key'])->first();
        if(!$rewardInfo){
            return response()->json([
                'status' => false,
                'message' => 'No package data.'
            ]);
        }

        // set reward packages
        $goods_data = []; //good data for send item group
        
        $packageTitle = $rewardInfo->package_name;
        $packageAmount = $rewardInfo->package_amount;

        $goods_data[] = [
            'goods_id' => $rewardInfo->package_id,
            'purchase_quantity' => $rewardInfo->package_quantity,
            'purchase_amount' => 0,
            'category_id' => 40
        ];

        $sendItemLog = $this->addItemHistoryLog([
            'uid' => $member->uid,
            'ncid' => $member->ncid,
            'char_id' => $member->char_id,
            'clan_id' => $member->clan_id,
            'world_id' => $member->world_id,
            'package_id' => $rewardInfo->package_id,
            'package_key' => $rewardInfo->package_key,
            'package_name' => $rewardInfo->package_name,
            'package_quantity' => $rewardInfo->package_quantity,
            'package_amount' => $rewardInfo->package_amount,
            'package_type' => $rewardInfo->package_type,
            'send_item_status' => false,
            'send_item_purchase_id' => 0,
            'send_item_purchase_status' => 0,
            'goods_data' => json_encode($goods_data),
            'status' => 'pending',
            'log_date' => (string)date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
            'last_ip' => $this->getIP(),
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);

        $send_result_raw = $this->dosendItem($member->ncid, $goods_data);
        $send_result = json_decode($send_result_raw);
        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            $this->updateItemHistoryLog([
                'send_item_status' => $send_result->status ? $send_result->status : false,
                'send_item_purchase_id' => $send_result->response->purchase_id ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ? $send_result->response->purchase_status : 0,
                'status' => 'success'
            ], $sendItemLog['id'], $member->uid);

            $eventInfo = $this->setEventInfo($member->uid);

            return response()->json([
                        'status' => true,
                        'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                        'data' => $eventInfo,
                        'reward'=>[
                            'name'=>$packageTitle
                        ]
                    ]);
        }else{
            $this->updateItemHistoryLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
                    ], $sendItemLog['id'], $member->uid);

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => false,
                        'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                        'data' => $eventInfo
                    ]);
        }


    }

}