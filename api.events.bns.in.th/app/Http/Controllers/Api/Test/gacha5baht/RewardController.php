<?php

    namespace App\Http\Controllers\Api\Test\gacha5baht;

    class RewardController {

        public function getRandomRewardInfo() {

            $rewards = $this->getSpecialRewardsList();

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($rewards as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            // now we have the reward
            return $reward;
        }

        private function getSpecialRewardsList() {
            return [
                [
                    'id' => 1,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลหินโซล x100',
                    'product_id' => 1067,
                    'product_quantity' => 1,
                    'chance' => 20,
                    'icon' => 'Gacha5Baht_id01.png',
                    'key' => 'Gacha5Baht_id01'
                ],
                [
                    'id' => 2,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลหินจันทรา x10',
                    'product_id' => 477,
                    'product_quantity' => 1,
                    'chance' => 15,
                    'icon' => 'Gacha5Baht_id02.png',
                    'key' => 'Gacha5Baht_id02'
                ],
                [
                    'id' => 3,
                    'item_type' => 'object',
                    'product_title' => 'เศษเกล็ดสีคราม x5',
                    'product_id' => 2085,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'Gacha5Baht_id03.png',
                    'key' => 'Gacha5Baht_id03'
                ],
                [
                    'id' => 4,
                    'item_type' => 'object',
                    'product_title' => 'ประกายฉลามดำ x5',
                    'product_id' => 1855,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'Gacha5Baht_id04.png',
                    'key' => 'Gacha5Baht_id04'
                ],
                [
                    'id' => 5,
                    'item_type' => 'object',
                    'product_title' => 'เหรียญสัมฤทธิ์ x1',
                    'product_id' => 2084,
                    'product_quantity' => 1,
                    'chance' => 8,
                    'icon' => 'Gacha5Baht_id05.png',
                    'key' => 'Gacha5Baht_id05'
                ],
                [
                    'id' => 6,
                    'item_type' => 'object',
                    'product_title' => 'วิญญาณวายุทมิฬ x1',
                    'product_id' => 2000,
                    'product_quantity' => 1,
                    'chance' => 8,
                    'icon' => 'Gacha5Baht_id06.png',
                    'key' => 'Gacha5Baht_id06'
                ],
                [
                    'id' => 7,
                    'item_type' => 'object',
                    'product_title' => 'ยันต์ปริศนาแห่งฮงมุน x1',
                    'product_id' => 2192,
                    'product_quantity' => 1,
                    'chance' => 8,
                    'icon' => 'Gacha5Baht_id07.png',
                    'key' => 'Gacha5Baht_id07'
                ],
                [
                    'id' => 8,
                    'item_type' => 'object',
                    'product_title' => 'ลูกแก้วดาวเสาร์ ขั้น 2 x1',
                    'product_id' => 2240,
                    'product_quantity' => 1,
                    'chance' => 7,
                    'icon' => 'Gacha5Baht_id08.png',
                    'key' => 'Gacha5Baht_id08'
                ],
                [
                    'id' => 9,
                    'item_type' => 'object',
                    'product_title' => 'ลูกแก้วดาวศุกร์ ขั้น 2 x1',
                    'product_id' => 2241,
                    'product_quantity' => 1,
                    'chance' => 7,
                    'icon' => 'Gacha5Baht_id09.png',
                    'key' => 'Gacha5Baht_id09'
                ],
                [
                    'id' => 10,
                    'item_type' => 'object',
                    'product_title' => 'เกล็ดสีคราม x1',
                    'product_id' => 2071,
                    'product_quantity' => 1,
                    'chance' => 5,
                    'icon' => 'Gacha5Baht_id010.png',
                    'key' => 'Gacha5Baht_id010'
                ],
                [
                    'id' => 11,
                    'item_type' => 'object',
                    'product_title' => 'ปีกแทชอน x1',
                    'product_id' => 2242,
                    'product_quantity' => 1,
                    'chance' => 1,
                    'icon' => 'Gacha5Baht_id011.png',
                    'key' => 'Gacha5Baht_id011'
                ],
                [
                    'id' => 12,
                    'item_type' => 'object',
                    'product_title' => 'หมวกแสนบริสุทธิ์ x1',
                    'product_id' => 2243,
                    'product_quantity' => 1,
                    'chance' => 0.3,
                    'icon' => 'Gacha5Baht_id012.png',
                    'key' => 'Gacha5Baht_id012'
                ],
                [
                    'id' => 13,
                    'item_type' => 'object',
                    'product_title' => 'หมวกกู่ก้องปฐพี x1',
                    'product_id' => 2244,
                    'product_quantity' => 1,
                    'chance' => 0.2,
                    'icon' => 'Gacha5Baht_id013.png',
                    'key' => 'Gacha5Baht_id013'
                ],
                [
                    'id' => 14,
                    'item_type' => 'object',
                    'product_title' => 'เครื่องประดับกู่ก้องปฐพี x1',
                    'product_id' => 2245,
                    'product_quantity' => 1,
                    'chance' => 0.2,
                    'icon' => 'Gacha5Baht_id014.png',
                    'key' => 'Gacha5Baht_id014'
                ],
                [
                    'id' => 15,
                    'item_type' => 'object',
                    'product_title' => 'เครื่องประดับไม้งาม x1',
                    'product_id' => 2246,
                    'product_quantity' => 1,
                    'chance' => 0.2,
                    'icon' => 'Gacha5Baht_id015.png',
                    'key' => 'Gacha5Baht_id015'
                ],
                [
                    'id' => 16,
                    'item_type' => 'object',
                    'product_title' => 'แว่นคนเจ้าเล่ห์ x1',
                    'product_id' => 870,
                    'product_quantity' => 1,
                    'chance' => 0.1,
                    'icon' => 'Gacha5Baht_id016.png',
                    'key' => 'Gacha5Baht_id016'
                ]
            ];
        }

    }
