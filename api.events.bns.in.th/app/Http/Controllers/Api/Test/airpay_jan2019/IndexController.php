<?php

    namespace App\Http\Controllers\Api\Test\airpay_jan2019;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\Test\airpay_jan2019\Member;
    use App\Models\Test\airpay_jan2019\Reward;
    use App\Models\Test\airpay_jan2019\ItemLog;
    use App\Models\Test\airpay_jan2019\ItemHistory;
    use App\Models\Test\airpay_jan2019\RightsLog;
    use App\Models\Test\airpay_jan2019\PointHistory;
    use App\Models\Test\airpay_jan2019\DeductPointLog;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2019-01-22 00:00:00';
        private $endTime = '2019-02-06 23:59:59';

        private $startDateDiamondsApi = '2019-01-22 00:00:00';
        // private $startDateDiamondsApi = '2019-01-23 00:00:00';
        private $endDateDiamondsApi = '2019-02-06 23:59:59';

        private $totalRights = 2000;

        private $diamondsPerRight = 50000;

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:AIRPAY_JAN2019:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData) {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events BNS Airpay Topup January 2019',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addSendItemLog(array $arr) {
            if ($arr) {
                $resp = ItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateSendItemLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function addItemHistoryLog(array $arr) {
            if ($arr) {
                $resp = ItemHistory::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateItemHistoryLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemHistory::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function addPointHistoryLog(array $arr) {
            if ($arr) {
                $resp = PointHistory::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updatePointHistoryLog(array $arr, $logId, $uid) {
            if ($arr) {
                return PointHistory::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function addDeductPointLog(array $arr) {
            if ($arr) {
                $resp = DeductPointLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateDeductPointLog(array $arr, $logId, $uid) {
            if ($arr) {
                return DeductPointLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function createRightsLog(array $arr){
            if ($arr) {
                $resp = RightsLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateRightsLog(array $arr, $logId, $uid) {
            if ($arr) {
                return RightsLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function apiDiamondHistory($uid,$ncid,$startDate,$endDate) {
            if(empty($ncid)){
                return false;
            }

            return Cache::remember('BNS:AIRPAY_TOPUP_JANUARY2019:EXCHANGE_TO_DIAMONDS_HISTORY_' . $uid, 5, function() use($ncid,$startDate,$endDate) {
                return json_decode($this->post_api($this->baseApi, [
                            'key_name' => 'bns',
                            'service' => 'query_cash_txn',
                            'user_id' => $ncid, //ncid
                            'start_time' => $startDate,
                            'end_time' => $endDate
                        ]));
            });
        }

        public function getEventInfo(Request $request)
        {

            if ($request->has('type') == false && $request->type != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            if($eventInfo == false){
                $eventInfo = [];
            }

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
            ]);

        }

        private function setEventInfo($uid=null){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $diamondTnx = 0;
            $resultDiamond = $this->apiDiamondHistory($member->uid,$member->ncid,$this->startDateDiamondsApi,$this->endDateDiamondsApi);
            if ($resultDiamond->status == true) {
                $contentDiamond = $resultDiamond->response;
                if (count($contentDiamond) > 0) {
                    $collectionDiamond = collect($contentDiamond);
                    $diamondTnx += $collectionDiamond->where('channel','airpay')->sum('diamond');
                }
            }

            if($diamondTnx > $member->total_diamonds){
                $this->updateMember([
                    'total_diamonds' => $diamondTnx
                ], $member->id);
            }

            $myTotalDiamonds = ($diamondTnx > 0 && $diamondTnx > $member->total_diamonds) ? $diamondTnx : $member->total_diamonds;

            $myTotalRights = floor($myTotalDiamonds / $this->diamondsPerRight);

            $myTotalRights = $myTotalRights >= 5 ? 5 : $myTotalRights;

            // check rights log
            $myTotalRightsLog = RightsLog::where('uid', $member->uid)->count();

            $remainRightToAdd = $myTotalRights - $myTotalRightsLog;

            if($remainRightToAdd > 0){
                // create rights log
                for($i=0;$i<$remainRightToAdd;$i++){
                    $this->createRightsLog([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'used_status' => 'not_use',
                        'log_date' => date('Y-m-d H:i:s'),
                        'log_date_timestamp' => time(),
                        'last_ip' => $this->getIP(),
                    ]);
                }
            }
            
            // get used rights
            $myUsedRights = RightsLog::where('uid', $member->uid)
                                    ->where('used_status', 'used')
                                    ->count();

            // get all remain rights
            $allRightsLog = RightsLog::where('used_status', 'used')->count();
            $remainAllRights = ($allRightsLog > 0) ? $this->totalRights - $allRightsLog : $this->totalRights;

            // set remain rights
            $myRemainRights = ($myTotalRights - $myUsedRights > 0) ? $myTotalRights - $myUsedRights : 0;

            $canPlay = ($myRemainRights > 0 ) ? true : false;

            $myPoints = PointHistory::where('uid', $member->uid)
                                            ->where('status', 'success')
                                            ->sum('points');

            $myUsedPoints = DeductPointLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->sum('points');

            return [
                'username' => $member->username,
                'can_play' => $canPlay,
                'remain_all_rights' => number_format($remainAllRights),
                'my_total_diamonds' => number_format($myTotalDiamonds),
                'my_total_rights' => $myTotalRights,
                'my_remain_rights' => $myRemainRights,
                'my_used_rights' => $myUsedRights,
                'my_remain_points' => $myPoints - $myUsedPoints,
            ];

        }

        public function openGachapon(Request $request){

            if ($request->has('type') == false && $request->type != 'airpay_gachapon') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // get total rights
            $myTotalRights = RightsLog::where('uid', $member->uid)
                                    ->count();

            // get used rights
            $myUsedRights = RightsLog::where('uid', $member->uid)
                                    ->where('used_status', 'used')
                                    ->count();

            // set remain rights
            $myRemainRights = ($myTotalRights - $myUsedRights > 0) ? $myTotalRights - $myUsedRights : 0;

            // check can play gachapon
            if($myRemainRights <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />สิทธิ์ในการเปิดกาชาปองของคุณไม่พอ'
                ]);
            }
            
            //deduct rights
            $deductRights = RightsLog::where('uid', $member->uid)
                                    ->where('used_status', 'not_use')
                                    ->first()
                                    ->update([
                                        'used_status' => 'used',
                                    ]);
            if($deductRights){

                $rewards = new Reward;

                $rewardKeyId = null;
                $goods_data = [];
                $productData = [];
                $rewardResp = "";

                // get random
                $rewardInfo = $rewards->getRandomAirpayReward();

                $rewardKeyId = $rewardInfo['id'];

                if(count($rewardInfo['product_set']) > 0){
                    foreach($rewardInfo['product_set'] as $product){

                        $productData[] = [
                            'product_id' => $product['product_id'],
                            'product_title' => $product['product_title'],
                            'product_quantity' =>  $product['product_quantity'],
                            'item_type' => $rewardInfo['item_type'],
                        ];

                        $goods_data[] = [
                            'goods_id' => $product['product_id'],
                            'purchase_quantity' => $product['product_quantity'],
                            'purchase_amount' => 0,
                            'category_id' => 40
                        ];

                    }
                }

                $rewardResp .= "- ".$rewardInfo['product_title']."<br />";

                // fixed reward
                $fixedReward = $rewards->airpayGaranteeReward();

                if(count($fixedReward['product_set']) > 0){
                    foreach($fixedReward['product_set'] as $fixed){

                        $productData[] = [
                            'product_id' => $fixed['product_id'],
                            'product_title' => $fixed['product_title'],
                            'product_quantity' =>  $fixed['product_quantity'],
                            'item_type' => $fixedReward['item_type'],
                        ];

                        $goods_data[] = [
                            'goods_id' => $fixed['product_id'],
                            'purchase_quantity' => $fixed['product_quantity'],
                            'purchase_amount' => 0,
                            'category_id' => 40
                        ];

                        

                    }
                }

                $rewardResp .= "- ".$fixedReward['product_title']."<br />";

                // create item history log
                if(count($productData) > 0){
                    foreach($productData as $productHistory){
                        $this->addItemHistoryLog([
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'reward_id' => $rewardKeyId,
                            'product_id' => $productHistory['product_id'],
                            'product_title' => $productHistory['product_title'],
                            'product_quantity' => $productHistory['product_quantity'],
                            'item_type' => $productHistory['item_type'],
                            'status' => 'success',
                            'log_date' => date('Y-m-d H:i:s'),
                            'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                            'last_ip' => $this->getIP(),
                        ]);
                    }
                }

                // add fixed point history
                $points = $rewards->airpayGaranteePoints();
               
                $this->addPointHistoryLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'title' => $points['point_title'],
                    'points' => $points['point_quantity'],
                    'status' => 'success',
                    'type' => 'airpay_gachapon',
                    'log_date' => date('Y-m-d H:i:s'),
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                    'last_ip' => $this->getIP(),
                ]);

                $rewardResp .= "- ".$points['point_title'];

                // add sen items log and send rewards
                $itemLog = $this->addSendItemLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'reward_id' => $rewardKeyId,
                    'send_item_status' => false,
                    'send_item_purchase_id' => '0',
                    'send_item_purchase_status' => '0',
                    'goods_data' => json_encode($goods_data),
                    'type' => $rewardInfo['item_type'],
                    'status' => 'pending',
                    'log_date' => date('Y-m-d H:i:s'),
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                    'last_ip' => $this->getIP()
                ]);

                $send_result_raw = $this->dosendItem($ncid, $goods_data);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'success'
                    ], $itemLog['id'], $uid);

                    $eventInfo = $this->setEventInfo($uid);

                    return response()->json([
                        'status' => true,
                        'message' => "คุณได้รับ<br />".$rewardResp,
                        'content' => $eventInfo,
                        'id' => $rewardKeyId,
                    ]);

                }else{
                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'unsuccess'
                    ], $itemLog['id'], $uid);

                    $eventInfo = $this->setEventInfo($uid);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                        'content' => $eventInfo,
                    ]);
                }
                
            }else{
                $eventInfo = $this->setEventInfo($uid);
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถดำเนินการได้<br />กรุณาลองใหม่อีกครั้ง',
                    'content' => $eventInfo,
                ]);
            }

        }

        public function exchangeRewards(Request $request){

            if ($request->has('type') == false && $request->type != 'exchange_reward') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }
            
            if ($request->has('package_id') == false || empty($request->package_id)) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (2)!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);

            $package_id = $request->package_id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $rewards = new Reward;
            $rewardInfo = $rewards->setExchangeByPackageId($package_id);
            if($rewardInfo == false){
                return response()->json([
                    'status' => false,
                    'message' => 'No data required.'
                ]);
            }

            // check remain points
            $remainPointsQuery = PointHistory::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->sum('points');

            $usedPoints = DeductPointLog::where('uid', $member->uid)
                                    ->where('status', 'success')
                                    ->sum('points');

            $remainPoints = ($remainPointsQuery - $usedPoints > 0) ? $remainPointsQuery - $usedPoints : 0;   
            if($remainPoints < $rewardInfo['require_points']){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />เหรียญ Advice <br />ของคุณไม่พอแลกของรางวัล'
                ]);
            }

            // deduct points
            $deductPoints = $this->addDeductPointLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'status' => 'success',
                'points' =>  $rewardInfo['require_points'],
                'log_date' => date('Y-m-d H:i:s'),
                'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                'last_ip' => $this->getIP(),
            ]);

            if($deductPoints){

                $goods_data = [];
                $productData = [];

                $rewardKeyId = $rewardInfo['id'];

                if(count($rewardInfo['product_set']) > 0){
                    foreach($rewardInfo['product_set'] as $product){

                        $productData[] = [
                            'product_id' => $product['product_id'],
                            'product_title' => $product['product_title'],
                            'product_quantity' =>  $product['product_quantity'],
                            'item_type' => $rewardInfo['item_type'],
                        ];

                        $goods_data[] = [
                            'goods_id' => $product['product_id'],
                            'purchase_quantity' => $product['product_quantity'],
                            'purchase_amount' => 0,
                            'category_id' => 40
                        ];
                    }
                }

                // create item history log
                if(count($productData) > 0){
                    foreach($productData as $productHistory){
                        $this->addItemHistoryLog([
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'reward_id' => $rewardKeyId,
                            'product_id' => $productHistory['product_id'],
                            'product_title' => $productHistory['product_title'],
                            'product_quantity' => $productHistory['product_quantity'],
                            'item_type' => $productHistory['item_type'],
                            'status' => 'success',
                            'log_date' => date('Y-m-d H:i:s'),
                            'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                            'last_ip' => $this->getIP(),
                        ]);
                    }
                }

                // add sen items log and send rewards
                $itemLog = $this->addSendItemLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'reward_id' => $rewardKeyId,
                    'send_item_status' => false,
                    'send_item_purchase_id' => '0',
                    'send_item_purchase_status' => '0',
                    'goods_data' => json_encode($goods_data),
                    'type' => $rewardInfo['item_type'],
                    'status' => 'pending',
                    'log_date' => date('Y-m-d H:i:s'),
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                    'last_ip' => $this->getIP()
                ]);

                $send_result_raw = $this->dosendItem($ncid, $goods_data);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'success'
                    ], $itemLog['id'], $uid);

                    $eventInfo = $this->setEventInfo($uid);

                    return response()->json([
                        'status' => true,
                        'message' => "ส่งของรางวัลเสร็จเรียบร้อย<br />กรุณาตรวจสอบกล่องจดหมายภายในเกม",
                        'content' => $eventInfo,
                    ]);

                }else{
                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'unsuccess'
                    ], $itemLog['id'], $uid);

                    $eventInfo = $this->setEventInfo($uid);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                        'content' => $eventInfo,
                    ]);
                }

            }else{
                $eventInfo = $this->setEventInfo($uid);
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถดำเนินการได้<br />กรุณาลองใหม่อีกครั้ง',
                    'content' => $eventInfo,
                ]);
            }

        }
    }