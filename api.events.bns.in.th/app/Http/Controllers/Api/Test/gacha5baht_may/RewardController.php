<?php

namespace App\Http\Controllers\Api\Test\gacha5baht_may;

class RewardController
{

    public function getRandomRewardInfo()
    {

        $rewards = $this->getSpecialRewardsList();

        $random = rand(0, 100000);
        $accumulatedChance = 0;
        $reward = false;

        foreach ($rewards as $value) {
            $chance = $value['chance'] * 1000;
            $currentChance = $accumulatedChance + $chance;

            // acquired reward
            if ($random >= $accumulatedChance && $random <= $currentChance) {
                $reward = $value;
                break;
            }
            $accumulatedChance = $currentChance;
        }

        $reward['random'] = $random;

        // now we have the reward
        return $reward;
    }

    public function getSpecialRewardsList()
    {
        return [
            [
                'id' => 1,
                'item_type' => 'object',
                'product_title' => 'คริสตัลหินโซล x50',
                'product_id' => 274,
                'product_quantity' => 1,
                // 'chance' => 28.3,
                'chance' => 0.05,
                'icon' => '202005_Gacha5Baht_01.png',
                'key' => '202005_Gacha5Baht_01'
            ],
            [
                'id' => 2,
                'item_type' => 'object',
                'product_title' => 'คริสตัลหินจันทรา x5',
                'product_id' => 1116,
                'product_quantity' => 1,
                // 'chance' => 15,
                'chance' => 0.05,
                'icon' => '202005_Gacha5Baht_02.png',
                'key' => '202005_Gacha5Baht_02'
            ],
            [
                'id' => 3,
                'item_type' => 'object',
                'product_title' => 'หินเปลี่ยนรูป x5',
                'product_id' => 690,
                'product_quantity' => 1,
                'chance' => 10,
                'icon' => '202005_Gacha5Baht_03.png',
                'key' => '202005_Gacha5Baht_03'
            ],
            [
                'id' => 4,
                'item_type' => 'object',
                'product_title' => 'ประกายฉลามดำ x5',
                'product_id' => 1855,
                'product_quantity' => 1,
                'chance' => 10,
                'icon' => '202005_Gacha5Baht_04.png',
                'key' => '202005_Gacha5Baht_04'
            ],
            [
                'id' => 5,
                'item_type' => 'object',
                'product_title' => 'ลูกแก้วยอดนักรบ x1',
                'product_id' => 2647,
                'product_quantity' => 1,
                'chance' => 10,
                'icon' => '202005_Gacha5Baht_05.png',
                'key' => '202005_Gacha5Baht_05'
            ],
            [
                'id' => 6,
                'item_type' => 'object',
                'product_title' => 'ปีกแทชอน x1',
                'product_id' => 2242,
                'product_quantity' => 1,
                'chance' => 10,
                'icon' => '202005_Gacha5Baht_06.png',
                'key' => '202005_Gacha5Baht_06'
            ],
            [
                'id' => 7,
                'item_type' => 'object',
                'product_title' => 'ยันต์ปริศนาแห่งฮงมุน x1',
                'product_id' => 2192,
                'product_quantity' => 1,
                'chance' => 5,
                'icon' => '202005_Gacha5Baht_07.png',
                'key' => '202005_Gacha5Baht_07'
            ],
            [
                'id' => 8,
                'item_type' => 'object',
                'product_title' => 'เกล็ดสีคราม x1',
                'product_id' => 2071,
                'product_quantity' => 1,
                'chance' => 5,
                'icon' => '202005_Gacha5Baht_08.png',
                'key' => '202005_Gacha5Baht_08'
            ],
            [
                'id' => 9,
                'item_type' => 'object',
                'product_title' => 'หญ้าซาฮวา x1',
                'product_id' => 3064,
                'product_quantity' => 1,
                'chance' => 5,
                'icon' => '202005_Gacha5Baht_09.png',
                'key' => '202005_Gacha5Baht_09'
            ],
            [
                'id' => 10,
                'item_type' => 'object',
                'product_title' => 'ดาวมังกรฟ้า x1',
                'product_id' => 2648,
                'product_quantity' => 1,
                'chance' => 0.5,
                'icon' => '202005_Gacha5Baht_10.png',
                'key' => '202005_Gacha5Baht_10'
            ],
            [
                'id' => 11,
                'item_type' => 'object',
                'product_title' => 'เหรียญมังกรสมุทร x1',
                'product_id' => 3351,
                'product_quantity' => 1,
                'chance' => 0.5,
                'icon' => '202005_Gacha5Baht_11.png',
                'key' => '202005_Gacha5Baht_11'
            ],
            [
                'id' => 12,
                'item_type' => 'object',
                'product_title' => 'เศษเกล็ดสีทอง x5',
                'product_id' => 3352,
                'product_quantity' => 1,
                'chance' => 0.5,
                'icon' => '202005_Gacha5Baht_12.png',
                'key' => '202005_Gacha5Baht_12'
            ],
            [
                'id' => 13,
                'item_type' => 'object',
                'product_title' => 'ดาวมังกรแดง x1',
                'product_id' => 3224,
                'product_quantity' => 1,
                'chance' => 0.05,
                'icon' => '202005_Gacha5Baht_13.png',
                'key' => '202005_Gacha5Baht_13'
            ],
            [
                'id' => 14,
                'item_type' => 'object',
                'product_title' => 'วิญญาณซาฮวา x1',
                'product_id' => 3141,
                'product_quantity' => 1,
                'chance' => 0.05,
                'icon' => '202005_Gacha5Baht_14.png',
                'key' => '202005_Gacha5Baht_14'
            ],
            [
                'id' => 15,
                'item_type' => 'object',
                'product_title' => 'ลูกแก้วมังกรสมุทร x1',
                'product_id' => 3353,
                'product_quantity' => 1,
                'chance' => 28.3,
                // 'chance' => 0.05,
                'icon' => '202005_Gacha5Baht_15.png',
                'key' => '202005_Gacha5Baht_15'
            ],
            [
                'id' => 16,
                'item_type' => 'object',
                'product_title' => 'แว่นคนเจ้าเล่ห์ x1',
                'product_id' => 870,
                'product_quantity' => 1,
                // 'chance' => 0.05,
                'chance' => 15,
                'icon' => '202005_Gacha5Baht_16.png',
                'key' => '202005_Gacha5Baht_16'
            ],
        ];
    }
}
