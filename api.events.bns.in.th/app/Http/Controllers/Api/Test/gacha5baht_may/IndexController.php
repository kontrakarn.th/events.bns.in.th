<?php

namespace App\Http\Controllers\Api\Test\gacha5baht_may;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

use App\Http\Controllers\Api\Test\gacha5baht_may\RewardController;

use App\Models\Test\gacha5baht_may\ItemHistory;
use App\Models\Test\gacha5baht_may\Member;
use App\Models\Test\gacha5baht_may\DeductLog;
use App\Models\Test\gacha5baht_may\SendItemLog;

class IndexController extends BnsEventController
{

    private $baseApi = 'http://api.apps.garena.in.th';
    private $startTime = '2020-05-18 00:00:00';
    private $endTime = '2020-06-09 23:59:59';

    private $awtSecret = 'AgjVHV5uEA7TYsSgNaydHHqL6MdsTkUs';

    private $requireDiamonds = [500, 2500, 5000];

    private $packageIds = [1, 2, 3];

    public function __construct(Request $request)
    {
        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);

        if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
            die(json_encode([
                'status' => false,
                'type' => 'end-event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status' => false,
                'type' => 'maintenance',
                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
            ]));
        }
        if ($this->isLoggedIn() == true) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                ]));
            }
        } else {
            die(json_encode([
                'status' => false,
                'type' => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        if ($this->userData)
            return true;
        else
            return false;
    }

    private function is_accepted_ip()
    {
        $allow_ips = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1';

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function checkMember()
    {
        $uid = $this->userData['uid'];
        $username = $this->userData['username'];

        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid' => $uid,
                'username' => $username,
                'ncid' => $ncid,
                'last_ip' => $this->getIP()
            ];

            $resp = $this->createMember($arr);
            if ($resp == false) {
                return false;
            }
        } else {
            // $member = Member::where('uid', $uid)->first();
            if (empty($this->memberData->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $this->memberData->_id);
            }
        }
        return true;
    }

    private function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->first();
        if (isset($counter)) {
            $this->memberData = $counter;
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        if (Member::create($arr)) {
            $member = Member::where('uid', intval($arr['uid']))->first();
            $this->memberData = $member;
            return true;
        } else {
            return false;
        }
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id))
            return false;

        return Member::where('_id', $_id)->update($arr);
    }

    private function getNcidByUid(int $uid)
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    private function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:GACHA5BAHT_MAY:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'user_info',
                'uid' => $uid,
            ]);
            $userNcInfo = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }
        });
    }

    private function dosendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Sending item from garena events bns gachapon 5 baht november.',
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function addItemHistoryLog($arr)
    {
        return ItemHistory::create($arr);
    }

    private function updateItemHistoryLog($arr, $log_id, $uid)
    {
        return ItemHistory::where('_id', $log_id)->where('uid', $uid)->update($arr);
    }

    private function addSendItemLog($arr)
    {
        return SendItemLog::create($arr);
    }

    private function updateSendItemLog($arr, $log_id, $uid)
    {
        return SendItemLog::where('_id', $log_id)->where('uid', $uid)->update($arr);
    }

    private function diamondBalance(int $uid)
    {
        return $this->post_api($this->baseApi, [
            'key_name' => 'bns',
            'service' => 'balance',
            'uid' => $uid,
        ]);
    }

    private function setDiamondDeductData($deduct_diamond_amount = 0)
    {
        if ($deduct_diamond_amount == 0)
            return false;

        return [
            [
                'goods_id' => 741,
                'purchase_quantity' => $deduct_diamond_amount,
                'purchase_amount' => $deduct_diamond_amount,
                'category_id' => 40
            ]
        ];
    }

    private function deductDiamond(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Deduct diamonds for garena bns gachapon 5 baht event',
            'currency_group_id' => 71,
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function addDeductLog(array $arr)
    {
        if ($arr) {
            $resp = DeductLog::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return false; //fix
            }
        }
        return null;
    }

    private function updateDeductLog($arr, $log_id)
    {
        if ($arr) {
            return DeductLog::where('_id', $log_id)->update($arr);
        }
        return null;
    }

    private function setGachaponType($package_id = null)
    {
        switch ($package_id) {
            case 1:
                return 'one';
                break;

            case 2:
                return 'five';
                break;

            case 3:
                return 'ten';
                break;
                //
                // case 4:
                //     return 'sixteen';
                //     break;

            default:
                return '';
                break;
        }
    }

    private function setRequiredDiamonds($package_id = null)
    {
        if (empty($package_id))
            return false;

        return isset($this->requireDiamonds[$package_id - 1]) ? $this->requireDiamonds[$package_id - 1] : false;
    }

    private function setRequiredCards($package_id = null)
    {
        switch ($package_id) {
            case 1:
                return 1;
                break;

            case 2:
                return 5;
                break;

            case 3:
                return 10;
                break;

            case 4:
                return 16;
                break;

            default:
                return false;
                break;
        }
    }

    private function doRandomItem($cardQuantity = 1)
    {
        $rewarder = new RewardController();
        $resp = [];
        for ($index = 0; $index < $cardQuantity; $index++) {
            $resp[] = $rewarder->getRandomRewardInfo();
        }

        return $resp;
    }

    public function checkin(Request $request) // checkin event
    {

        $decoded = $request->input();
        if (isset($decoded['type']) == false || $decoded['type'] != 'event_info') {
            return response()->json(['status' => false, 'message' => 'Invalid parameter! (1)']);
        }

        $uid = $this->userData['uid'];
        $nickname = $this->userData['nickname'];

        $diamondsBalanceRaw = $this->diamondBalance($uid);
        $diamondsBalance = json_decode($diamondsBalanceRaw);
        $card_chk = [];
        foreach ($this->requireDiamonds as $key => $value) {
            if ($diamondsBalance->balance < $value) {
                array_push($card_chk, false);
            } else {
                array_push($card_chk, true);
            }
        }



        return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => [
                'uid' => $uid,
                'nickname' => $nickname,
                'can_play' => [
                    "card_1" => $card_chk[0],
                    "card_2" => $card_chk[1],
                    "card_3" => $card_chk[2],
                ]
            ]
        ]);
    }

    public function buyCard(Request $request)
    {
        if ($this->isLoggedIn() == false) {
            return response()->json(['status' => false, 'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน']);
        }

        $decoded = $request->input();
        // $decoded = $this->decryptJwtToken($request, $this->awtSecret);
        // if (is_null($decoded)) {
        //     return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
        // }
        // $decoded = collect($decoded);
        if (isset($decoded['type']) == false || $decoded['type'] != 'buy_card' || isset($decoded['package_id']) == false) {
            return response()->json(['status' => false, 'message' => 'Invalid parameter! (1)']);
        }

        $package_id = intval($decoded['package_id']);
        if ($package_id < 1 || $package_id > 3) {
            return response()->json(['status' => false, 'message' => 'Invalid parameter! (2)']);
        }

        $uid = $this->memberData->uid;
        $ncid = $this->memberData->ncid;

        $gachaponType = $this->setGachaponType($package_id);
        // dd($gachaponType);
        if (empty($gachaponType)) {
            return response()->json(['status' => false, 'message' => 'No data required (1).']);
        }

        if (in_array($package_id, $this->packageIds)) {

            $required_deduct_diamond = $this->setRequiredDiamonds($package_id);
            if ($required_deduct_diamond == false) {
                return response()->json(['status' => false, 'message' => 'No data required (2).']);
            }

            $requiredCards = $this->setRequiredCards($package_id);

            if ($requiredCards == false) {
                return response()->json(['status' => false, 'message' => 'No data required (4).']);
            }

            // get diamond balance
            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถอัพเดทข้อมูล Diamonds ของคุณได้กรุณาลองใหม่อีกครั้ง'
                ]);
            } else {

                $before_diamond = intval($diamondsBalance->balance);
                // compare user diamonds balance with gachapon's require diamomds.
                if ($before_diamond < $required_deduct_diamond) {
                    return response()->json([
                        'status' => false,
                        'message' => 'จำนวน Diamonds ของคุณไม่พอในการซื้อกาชาปอง'
                    ]);
                } else {

                    $logDeduct = $this->addDeductLog([
                        'status' => 'pending',
                        'diamond' => $required_deduct_diamond,
                        'uid' => $uid,
                        'ncid' => $ncid,
                        'last_ip' => $this->getIP(),
                        'deduct_type' => 'gachapon_' . $requiredCards,
                        'log_timestamp' => time()
                    ]);

                    // set desuct diamonds
                    $deductData = $this->setDiamondDeductData($required_deduct_diamond);
                    if ($deductData == false) {
                        return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง']);
                    }

                    $respDeductRaw = $this->deductDiamond($ncid, $deductData);
                    $respDeduct = json_decode($respDeductRaw);

                    if (is_object($respDeduct) && is_null($respDeduct) == false) {

                        $arrDeductLog = [
                            'before_deduct_diamond' => $before_diamond,
                            'after_deduct_diamond' => $before_diamond - $required_deduct_diamond,
                            'deduct_status' => $respDeduct->status,
                            'deduct_purchase_id' => isset($respDeduct->response->purchase_id) ? $respDeduct->response->purchase_id : 0,
                            'deduct_purchase_status' => isset($respDeduct->response->purchase_status) ? $respDeduct->response->purchase_status : 0,
                            'deduct_data' => json_encode($deductData),
                            'status' => 'pending'
                        ];

                        if ($respDeduct->status) {

                            $arrDeductLog['status'] = 'success';
                            $this->updateDeductLog($arrDeductLog, $logDeduct['_id']);

                            $randResult = $this->doRandomItem($requiredCards); //do getRandomCode
                            $goodsData = []; //good data for send item group

                            foreach ($randResult as $key => $value) { //loop for add item log
                                $arrItemHistory = [
                                    'product_id' => 0,
                                    'product_title' => '',
                                    'product_quantity' => 0,
                                    'uid' => $uid,
                                    'ncid' => $ncid,
                                    'last_ip' => $this->getIP(),
                                    'icon' => '',
                                    'gachapon_type' => $gachaponType,
                                    'log_timestamp' => time(),
                                    'log_time_string' => Carbon::now()->format('d/m/Y H:i:s')
                                ];

                                $arrItemHistory['product_id'] = $value['product_id'];
                                $arrItemHistory['product_title'] = $value['product_title'];
                                $arrItemHistory['product_quantity'] = $value['product_quantity'];
                                $arrItemHistory['icon'] = $value['icon'];
                                $arrItemHistory['status'] = 'used';
                                $arrItemHistory['send_type'] = 'owner';
                                $arrItemHistory['item_type'] = 'object';

                                $goodsData[] = [
                                    'goods_id' => $value['product_id'],
                                    'purchase_quantity' => $value['product_quantity'],
                                    'purchase_amount' => 0,
                                    'category_id' => 40
                                ];
                                $this->addItemHistoryLog($arrItemHistory);
                            }

                            // send items
                            if (is_null($goodsData) == false && is_array($goodsData) && count($goodsData) > 0 && is_null($respDeduct->response->purchase_id) == false) {
                                //do add send item log
                                $logSendItemResult = $this->addSendItemLog([
                                    'uid' => $uid,
                                    'ncid' => $ncid,
                                    'status' => 'pending',
                                    'send_item_status' => false,
                                    'send_item_purchase_id' => 0,
                                    'send_item_purchase_status' => 0,
                                    'goods_data' => json_encode($goodsData),
                                    'last_ip' => $this->getIP(),
                                    'type' => 'gachapon_' . $requiredCards,
                                    'log_timestamp' => time()
                                ]);

                                //send item
                                $sendResultRaw = $this->dosendItem($ncid, $goodsData);
                                $sendResult = json_decode($sendResultRaw);
                                if (is_null($sendResult) == false && is_object($sendResult) && $sendResult->status) {
                                    //recive log id
                                    $log_id = $logSendItemResult['_id'];
                                    //do update send item log
                                    $this->updateSendItemLog([
                                        'send_item_status' => $sendResult->status ?: false,
                                        'send_item_purchase_id' => $sendResult->response->purchase_id ?: 0,
                                        'send_item_purchase_status' => $sendResult->response->purchase_status ?: 0,
                                        'status' => 'success'
                                    ], $log_id, $uid);
                                } else {
                                    //do update send item log
                                    $log_id = $logSendItemResult['_id'];
                                    $this->updateSendItemLog([
                                        'send_item_status' => isset($sendResult->status) ? $sendResult->status : false,
                                        'send_item_purchase_id' => isset($sendResult->response->purchase_id) ? $sendResult->response->purchase_id : 0,
                                        'send_item_purchase_status' => isset($sendResult->response->purchase_status) ? $sendResult->response->purchase_status : 0,
                                        'status' => 'unsuccess'
                                    ], $log_id, $uid);
                                    //error unsuccess log
                                    return response()->json([
                                        'status' => false,
                                        'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                                    ]);
                                }
                            }

                            $content = [];
                            //convert content result
                            foreach ($randResult as $key => $value) {
                                $collection = collect($value);
                                $filtered = $collection->only(['id', 'product_title', 'item_type', 'icon', 'key']); //except
                                $content[] = $filtered->all();
                            }

                            $diamondsBalance = $before_diamond - $required_deduct_diamond;
                            $card_chk = [];
                            foreach ($this->requireDiamonds as $key => $value) {
                                if ($diamondsBalance < $value) {
                                    array_push($card_chk, false);
                                } else {
                                    array_push($card_chk, true);
                                }
                            }

                            //do return success log
                            return response()->json([
                                'status' => TRUE,
                                'message' => 'ซื้อสำเร็จ',
                                'content' => $content,
                                'data' => [
                                    'can_play' => [
                                        "card_1" => $card_chk[0],
                                        "card_2" => $card_chk[1],
                                        "card_3" => $card_chk[2],
                                    ]
                                ]
                            ]);
                        } else {
                            $arrDeductLog['status'] = 'unsuccess';
                            $this->updateDeductLog($arrDeductLog, $logDeduct['_id']);
                        }
                    } else {
                        $arrDeductLog['status'] = 'unsuccess';
                        $this->updateDeductLog($arrDeductLog, $logDeduct['_id']);
                    }
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }
            }
        }
        return response()->json([
            'status' => false,
            'message' => 'Np data required (5).'
        ]);
    }

    public function getItemHistory(Request $request)
    {
        // if($this->isLoggedIn()==false){
        //     return response()->json(['status' => false, 'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน']);
        // }

        $decoded = $request->input();

        // $decoded = $this->decryptJwtToken($request, $this->awtSecret);
        // if (is_null($decoded)) {
        //     return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
        // }
        // $decoded = collect($decoded);

        if ($decoded['type'] != 'item_history') {
            return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (2) กรุณาลองใหม่อีกครั้ง']);
        }

        $uid = $this->userData['uid'];

        $resp_raw = ItemHistory::select('product_title', 'log_time_string', 'icon')
            ->where('uid', $uid)
            ->orderBy('log_timestamp', 'DESC')
            ->get();
        $resp = $resp_raw->toArray();

        return response()->json([
            'status' => true,
            'content' => $resp
        ]);
    }


    public function testRate($num_test)
    {
        if ($this->is_accepted_ip()) {
            $rewarder = new RewardController();
            $item_list = $rewarder->getSpecialRewardsList();
            $item_show_list = [];
            foreach ($item_list as $key => $value) {
                $item_show_list[] = [
                    'product_title' => $value['product_title'],
                    'chance' => $value['chance'],
                    'result' => 0,
                    'result_percent' => 0
                ];
            }
            $col_item_list = collect($item_list);
            for ($i = 1; $i <= $num_test; $i++) {
                $randResult = $this->doRandomItem(1);
                // $item_list
                $result = collect($col_item_list)->where('id', $randResult[0]['id'])->toArray();
                $key = array_keys($result)[0];
                $item_show_list[$key]['result'] = $item_show_list[$key]['result'] += 1;
            }

            foreach ($item_show_list as $key => $value) {
                $item_show_list[$key]['result_percent'] = number_format(($value['result'] / $num_test) * 100, 2);
            }

            return response()->json([
                'data' => $item_show_list
            ]);
        } else {
            return response()->json([
                'data' => []
            ]);
        }
    }
}
