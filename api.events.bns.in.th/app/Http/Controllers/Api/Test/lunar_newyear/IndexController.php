<?php

    namespace App\Http\Controllers\Api\Test\lunar_newyear;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\Test\lunar_newyear\Member;
    use App\Models\Test\lunar_newyear\Reward;
    use App\Models\Test\lunar_newyear\Quest;
    use App\Models\Test\lunar_newyear\ItemLog;
    use App\Models\Test\lunar_newyear\FreeRightsLog;
    use App\Models\Test\lunar_newyear\BlueGamoLog;
    use App\Models\Test\lunar_newyear\BlueGamoDeductLog;
    use App\Models\Test\lunar_newyear\BunsPlayLog;
    use App\Models\Test\lunar_newyear\RedGamoLog;
    use App\Models\Test\lunar_newyear\RedGamoDeductLog;
    use App\Models\Test\lunar_newyear\AngpaoPlayLog;
    use App\Models\Test\lunar_newyear\GoldLog;
    use App\Models\Test\lunar_newyear\GoldDeductLog;

    // test ranking
    use App\Models\Test\lunar_newyear\TestScore;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2019-02-08 00:00:00';
        private $endTime = '2019-03-02 23:59:59';

        private $startApiTime = '06:00:00';
        private $endApiTime = '05:59:59';

        private $startEventQuest = '2019-02-08 00:00:00';
        private $endEventQuest = '2019-02-25 05:59:00';

        private $startRankReward = '2019-02-25 06:00:00';
        private $endRankReward = '2019-03-02 23:59:59';

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:LUNAR_NEWYEAR_2019:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        public function acceptChar(Request $request)
        {

            if($request->has('type') == false && $request->type != 'accept_character') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter(1)!'
                ]);
            }

            if ($request->has('id') == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'invalid parameter(2)!'
                ]);
            }
            $char_id = $request->id;
            if (empty($char_id)) {
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่มีตัวละครที่ต้องการเลือก'
                ]);
            }

            // $char_id = $request->id;

            $uid = $this->userData['uid'];
            $username = $this->userData['username'];

            if ($resp = $this->hasAcceptChar($uid) == false) {
                $ncid = $this->getNcidByUid($uid);
                $characters = $this->doGetChar($uid,$ncid);
                $data = $characters[$char_id-1];
                if (isset($data['char_id'])) {
                    $this->doAcceptChar($data['char_id'], $data['char_name'], $data['world_id'], $data['job'], $uid);
                    $member = Member::where('uid', $uid)->first();
                    return response()->json([
                                'status' => true,
                                'message' => 'ยืนยันตัวละครเสร็จสิ้น',
                                'content' => [
                                    'username' => $member->username,
                                    'character' => $member->char_name,
                                ],
                    ]);
                } else {
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่พบตัวละครที่คุณต้องการเลือก'
                    ]);
                }
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก'
                ]);
            }
        }

        private function doAcceptChar($char_id, $char_name, $world_id, $job, $uid)
        {
            return Member::where('uid', $uid)->update([
                        'char_id' => $char_id,
                        'char_name' => $char_name,
                        'world_id' => $world_id,
                        'job' => $job,
                    ]);
        }

        private function hasAcceptChar($uid)
        {
            $resp = Member::where('uid', $uid)->first();
            if (empty($resp->char_id) && empty($resp->char_name)) {
                return false;
            } else {
                return $resp;
            }
        }

        public function getCharacter(Request $request)
        {
            if($request->has('type') == false && $request->type != 'get_characters') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $this->checkMember();
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            $resp = $this->hasAcceptChar($uid);
            if ($resp == false) {
                $charactersList = [];
                $ncid = $this->getNcidByUid($uid);
                $characters = $this->doGetChar($uid,$ncid);
                foreach ($characters as $character) {
                    $charactersList[] = collect($character)->except(['char_id', 'world_id', 'job', 'level', 'creation_time', 'last_play_start', 'last_play_end', 'last_ip', 'exp', 'mastery_level']);
                }
                return response()->json([
                            'status' => true,
                            'content' => $charactersList,
                            'accepted' => false
                ]);
            } else {
                return response()->json([
                            'status' => true,
                            'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก',
                            'accepted' => true
                ]);
            }
        }

        private function doGetChar($uid,$ncid){
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i =1;
                    foreach ($resp->response as $key => $value) {
                        $content[] = [
                            'id' => $i,
                            'char_id' => $value->id,
                            'char_name' => $value->name,
                            'world_id' => $value->world_id,
                            'job' => $value->job,
                            'level' => $value->level,
                            'creation_time' => $value->creation_time,
                            'last_play_start' => $value->last_play_start,
                            'last_play_end' => $value->last_play_end,
                            'last_ip' => $value->last_ip,
                            'exp' => $value->exp,
                            'mastery_level' => $value->mastery_level,
                        ];

                        $i++;
                    }
                    return $content;
                }
            }
            return null;
        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:LUNAR_NEWYEAR_2019:CHARACTERS_' . $uid, 10, function() use($ncid) {
                        return $this->post_api($this->baseApi, [
                                    'key_name' => 'bns',
                                    'service' => 'characters',
                                    'user_id' => $ncid
                        ]);
                    });
        }

        private function dosendItem(string $ncid, array $goodsData) {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events BNS Lunar New Year 2019.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addSendItemLog(array $arr) {
            if ($arr) {
                $resp = ItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateSendItemLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        /* private function addItemHistoryLog(array $arr) {
            if ($arr) {
                $resp = ItemHistory::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateItemHistoryLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemHistory::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        } */

        // Blue Gamo
        private function addBlueGamoLog(array $arr) {
            if ($arr) {
                $resp = BlueGamoLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateBlueGamoLog(array $arr, $logId, $uid) {
            if ($arr) {
                return BlueGamoLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function addBlueGamoDeductLog(array $arr) {
            if ($arr) {
                $resp = BlueGamoDeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateBlueGamoDeductLog(array $arr, $logId, $uid) {
            if ($arr) {
                return BlueGamoDeductLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        // Red Gamo
        private function addRedGamoLog(array $arr) {
            if ($arr) {
                $resp = RedGamoLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateRedGamoLog(array $arr, $logId, $uid) {
            if ($arr) {
                return RedGamoLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function addRedGamoDeductLog(array $arr) {
            if ($arr) {
                $resp = RedGamoDeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateRedGamoDeductLog(array $arr, $logId, $uid) {
            if ($arr) {
                return RedGamoDeductLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        // Buns Log
        private function addBunsPlayLog(array $arr) {
            if ($arr) {
                $resp = BunsPlayLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateBunsPlayLog(array $arr, $logId, $uid) {
            if ($arr) {
                return BunsPlayLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        // Angpao Log
        /* private function addAngpaoRightsLog(array $arr) {
            if ($arr) {
                $resp = AngpaoRightsLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateAngpaoRightsLog(array $arr, $logId, $uid) {
            if ($arr) {
                return AngpaoRightsLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        } */

        private function addAngpaoPlayLog(array $arr) {
            if ($arr) {
                $resp = AngpaoPlayLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateAngpaoPlayLog(array $arr, $logId, $uid) {
            if ($arr) {
                return AngpaoPlayLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        // Gold
        private function addGoldLog(array $arr) {
            if ($arr) {
                $resp = GoldLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateGoldLog(array $arr, $logId, $uid) {
            if ($arr) {
                return GoldLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function addGoldDeductLog(array $arr) {
            if ($arr) {
                $resp = GoldDeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateGoldDeductLog(array $arr, $logId, $uid) {
            if ($arr) {
                return GoldDeductLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        // Free Rights for play buns fortune gachapon
        private function addFreeRightsLog(array $arr) {
            if ($arr) {
                $resp = FreeRightsLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateFreeRightsLog(array $arr, $logId, $uid) {
            if ($arr) {
                return FreeRightsLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        /* private function addDeductFreeRightsLog(array $arr) {
            if ($arr) {
                $resp = DeductFreeRightsLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateDeductFreeRightsLog(array $arr, $logId, $uid) {
            if ($arr) {
                return DeductFreeRightsLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        } */

        // check quest completed api
        private function apiCheckQuestCompleted($uid, $charId, $questId, $start, $end, $dateCache) {
            $resp = Cache::remember('BNS:LUNAR_NEWYEAR_2019:QUEST_COMPLETED_' . $questId . '_' . $uid .'_'. $dateCache, 5, function()use($charId,$questId,$start,$end) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'quest_completed',
                    'char_id' => $charId,
                    'quest_id' => $questId,
                    'start_time' => $start,
                    'end_time' => $end
                ]);
            });

            return json_decode($resp);
        }

        private function setQuestCompletedCount($uid, $charId, $questId, $start, $end, $dateCache){

            $completedCount = 0;

            $apiCompletedCount = $this->apiCheckQuestCompleted($uid, $charId, $questId, $start, $end, $dateCache);
            if($apiCompletedCount->status){
                $completedCount = intval($apiCompletedCount->response->quest_completed_count);
            }

            return $completedCount;

        }

        private function setQuestDateTime(){

            $currDate = date('Y-m-d');

            $startDatetime = '';
            $endDatetime = '';
            $dateCache = '';

            if(time() >= strtotime($currDate.' 00:00:00') && time() <= strtotime($currDate.' 05:59:59')){ // check current time after 00:00:00 and not over 05:59:59
                $dateCache = date('Y-m-d', strtotime(' -1 day'));
                $startDatetime = date('Y-m-d', strtotime(' -1 day')).' 06:00:00';
                $endDatetime = date('Y-m-d').' 05:59:59';
            }else{
                $dateCache = date('Y-m-d');
                $startDatetime = date('Y-m-d').' 06:00:00';
                $endDatetime = date('Y-m-d', strtotime(' +1 day')).' 05:59:59';
            }

            return [
                'date_cache' => $dateCache,
                'start_date_time' => $startDatetime,
                'end_date_time' => $endDatetime,
            ];

        }

        private function setPointsSummary($uid=null){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $dateTime = $this->setQuestDateTime();

            $totalBlueGamo = 0;
            $usedBlueGamo = 0;
            $remainBlueGamo = 0;

            $totalRedGamo = 0;
            $usedRedGamo = 0;
            $remainRedGamo = 0;

            $totalGold = 0;
            $usedGold = 0;
            $remainGold = 0;

            // get blue gamo
            $totalBlueGamo = BlueGamoLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->sum('blue_gamo');

            $usedBlueGamo = BlueGamoDeductLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            $remainBlueGamo = $totalBlueGamo - $usedBlueGamo > 0 ? $totalBlueGamo - $usedBlueGamo : 0;

            // get red gamo
            $totalRedGamo = RedGamoLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            $usedRedGamo = RedGamoDeductLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            $remainRedGamo = $totalRedGamo - $usedRedGamo > 0 ? $totalRedGamo - $usedRedGamo : 0;

            // get gold
            $totalGold = GoldLog::where('uid', $member->uid)
                                ->where('status', 'success')
                                ->sum('gold');

            $usedGold = GoldDeductLog::where('uid', $member->uid)
                                ->where('status', 'success')
                                ->sum('gold');

            $remainGold = $totalGold - $usedGold > 0 ? $totalGold - $usedGold : 0;

            return [
                'total_blue_gamo' => intval($totalBlueGamo),
                'used_blue_gamo' => intval($usedBlueGamo),
                'remain_blue_gamo' => intval($remainBlueGamo),
                'total_red_gamo' => intval($totalRedGamo),
                'used_red_gamo' => intval($usedRedGamo),
                'remain_red_gamo' => intval($remainRedGamo),
                'total_gold' => intval($totalGold),
                'used_gold' => intval($usedGold),
                'remain_gold' => intval($remainGold),
            ];

        }

        private function getBlueGamo($uid=null){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $dateTime = $this->setQuestDateTime();

            $totalBlueGamo = 0;
            $usedBlueGamo = 0;
            $remainBlueGamo = 0;

            $totalBlueGamo = BlueGamoLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->sum('blue_gamo');

            $usedBlueGamo = BlueGamoDeductLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            $remainBlueGamo = $totalBlueGamo - $usedBlueGamo > 0 ? $totalBlueGamo - $usedBlueGamo : 0;

            return [
                'total_blue_gamo' => intval($totalBlueGamo),
                'used_blue_gamo' => intval($usedBlueGamo),
                'remain_blue_gamo' => intval($remainBlueGamo),
            ];

        }

        private function getRedGamo($uid=null){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $dateTime = $this->setQuestDateTime();

            $totalRedGamo = 0;
            $usedRedGamo = 0;
            $remainRedGamo = 0;

            $totalRedGamo = RedGamoLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            $usedRedGamo = RedGamoDeductLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            $remainRedGamo = $totalRedGamo - $usedRedGamo > 0 ? $totalRedGamo - $usedRedGamo : 0;

            return [
                'total_red_gamo' => intval($totalRedGamo),
                'used_red_gamo' => intval($usedRedGamo),
                'remain_red_gamo' => intval($remainRedGamo),
            ];

        }

        private function getGold($uid=null){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $dateTime = $this->setQuestDateTime();

            // get gold
            $totalGold = GoldLog::where('uid', $member->uid)
                                ->where('status', 'success')
                                ->sum('gold');

            $usedGold = GoldDeductLog::where('uid', $member->uid)
                                ->where('status', 'success')
                                ->sum('gold');

            $remainGold = $totalGold - $usedGold > 0 ? $totalGold - $usedGold : 0;

            return [
                'total_gold' => intval($totalGold),
                'used_gold' => intval($usedGold),
                'remain_gold' => intval($remainGold),
            ];

        }

        // Lobby Part
        public function getLobyInfo(Request $request)
        {

            if ($request->has('type') == false && $request->type != 'lobby_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            return response()->json([
                'status' => true,
                'content' => [
                    'character' => $member->char_name,
                    'username' => $member->username,
                ],
            ]);

        }

        // Daily Quest Info
        public function getDailyInfo(Request $request)
        {

            if ($request->has('type') == false && $request->type != 'daily_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // check quest completed and update quest point
            $quests = $this->getQuestCompleted($member->uid);

            // check all points
            $pointsSummary = $this->setPointsSummary($uid);

            // check Free Rights
            $freeRights = $this->getFreeRights($uid);

            return response()->json([
                'status' => true,
                'content' => [
                    'username' => $member->username,
                    'character' => $member->char_name,
                    'free_rights' => $freeRights,
                    'points_summary' => $pointsSummary,
                    'quests' => $quests,
                ],
            ]);

        }

        private function getFreeRights($uid){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $dateTime = $this->setQuestDateTime();

            $freeRightsLog = 0;

            // check date range
            if(time() >= strtotime($this->startEventQuest) && time() <= strtotime($this->endEventQuest)){
                $checkHasfreeRightsLog = FreeRightsLog::where('uid', $member->uid)
                                            ->where('used_status', 'used')
                                            ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                            ->count();
                if($checkHasfreeRightsLog == 0){

                    // check already free right
                    $checkAlreadyfreeRight = FreeRightsLog::where('uid', $member->uid)
                                                            ->where('used_status', 'not_use')
                                                            ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                                            ->count();
                    if($checkAlreadyfreeRight > 0){
                        $freeRightsLog = 1;
                    }else{
                        // create free 
                        $createFreeRights = $this->addFreeRightsLog([
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'used_status' => 'not_use',
                            'status' => 'success',
                            'log_date' => date('Y-m-d H:i:s'),
                            'log_date_timestamp' => time(),
                            'last_ip' => $this->getIP(),
                        ]);

                        if($createFreeRights){
                            $freeRightsLog = 1;
                        }
                    }
                    
                }
            }
            
            return $freeRightsLog > 0;
        }

        private function getQuestCompleted($uid){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $quests = [];

            $dateTime = $this->setQuestDateTime();

            $questInfo = new Quest;
            $questList = $questInfo->getQuestList();

            foreach($questList as $quest){

                $questCompletedStatus = false;

                // check date range
                if(time() >= strtotime($this->startEventQuest) && time() <= strtotime($this->endEventQuest)){
                    // check quest success log
                    $questCompleted = BlueGamoLog::where('uid', $member->uid)
                                                ->where('quest_code', $quest['quest_code'])
                                                ->where('status', 'success')
                                                ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                                ->count();
                    if($questCompleted > 0){
                        $questCompletedStatus = true;
                    }else{
                        // check from api
                        $questCompletedApi = $this->setQuestCompletedCount($member->uid, $member->char_id, $quest['quest_code'], $dateTime['start_date_time'], $dateTime['end_date_time'], $dateTime['date_cache']);
                        if($questCompletedApi > 0){

                            $questCompletedLog = BlueGamoLog::where('uid', $member->uid)
                                                            ->where('quest_code', $quest['quest_code'])
                                                            ->where('status', 'success')
                                                            ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                                            ->count();

                            if($questCompletedLog == 0){
                                // create quest log
                                $this->addBlueGamoLog([
                                    'uid' => $member->uid,
                                    'username' => $member->username,
                                    'ncid' => $member->ncid,
                                    'char_id' => $member->char_id,
                                    'char_name' => $member->char_name,
                                    'world_id' => $member->world_id,
                                    'job' => $member->job,
                                    'quest_id' => $quest['id'],
                                    'quest_code' => $quest['quest_code'],
                                    'quest_title' => $quest['quest_title'],
                                    'blue_gamo' => $member->job == 11 ? $quest['blue_gamo_wd'] : $quest['blue_gamo'],
                                    'status' => 'success',
                                    'log_date' => date('Y-m-d H:i:s'),
                                    'log_date_timestamp' => time(),
                                    'last_ip' => $this->getIP(),
                                ]);
                            }

                            $questCompletedStatus = true;

                        }
                    }  
                }   
                
                $quests[] = [
                    'title' => $quest['quest_title'],
                    'status' => $questCompletedStatus,
                    'blue_gamo' => ($member->job == 11) ? $quest['blue_gamo_wd'] : $quest['blue_gamo'],
                ];

            }

            return $quests;
        }

        // Buns Gachapon
        public function getBunsInfo(Request $request){

            if ($request->has('type') == false && $request->type != 'buns_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // check all points
            $pointsSummary = $this->setPointsSummary($uid);

            // check Free Rights
            $freeRights = $this->getFreeRights($uid);

            return response()->json([
                'status' => true,
                'content' => [
                    'username' => $member->username,
                    'character' => $member->char_name,
                    'free_rights' => $freeRights,
                    'points_summary' => $pointsSummary
                ],
            ]);

        }

        public function playBunsFortune(Request $request){

            if ($request->has('type') == false && $request->type != 'play_buns_fortune') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $hasGold = false;

            $dateTime = $this->setQuestDateTime();

            // check free log
            $freeRights = $this->getFreeRights($uid);

            /* if($freeRights <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />สิทธิ์เล่นฟรี<br />ไม่พอเสี่ยงทายซาลาเปา'
                ]);
            } */

            // get blue gamo
            $blueGamoInfo = $this->getBlueGamo($uid);

            if($blueGamoInfo['remain_blue_gamo'] <= 0 && $freeRights <= 0){

                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนแบงค์กาโม่ฟ้า<br />ไม่พอเสี่ยงทายซาลาเปา'
                ]);
            }

            $deductRight = false;

            if($freeRights > 0){
                // deduct free right
                $deductRight = FreeRightsLog::where('uid', $member->uid)
                                            ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                            ->update([
                                                'used_status' => 'used',
                                            ]);
            }else{
                // deduct blue gamo
                $deductRight = $this->addBlueGamoDeductLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'status' => 'success',
                    'log_date' => date('Y-m-d H:i:s'),
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]);
            }
            
            if($deductRight){

                // random buns
                $buns = new Quest;
                $randBunsResult = $buns->getRandomBuns();

                // add buns play log
                $addBunsPlayLog = $this->addBunsPlayLog([
                                    'uid' => $member->uid,
                                    'username' => $member->username,
                                    'ncid' => $member->ncid,
                                    'buns_key' => $randBunsResult['id'],
                                    'buns_title' => $randBunsResult['title'],
                                    'status' => 'success',
                                    'log_date' => date('Y-m-d H:i:s'),
                                    'log_date_timestamp' => time(),
                                    'last_ip' => $this->getIP(),
                                ]);

                // set reward for buns gachapon
                if($randBunsResult['id'] == 1){

                    // create red gamo log
                    $this->addRedGamoLog([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'buns_log_id' => $addBunsPlayLog['id'],
                        'status' => 'success',
                        'log_date' => date('Y-m-d H:i:s'),
                        'log_date_timestamp' => time(),
                        'last_ip' => $this->getIP(),
                    ]);

                }else{

                    // check has received gold from buns gachapon daily
                    $goldBunsDaily = GoldLog::where('uid', $member->uid)
                                            ->where('gold_type', 'buns')
                                            ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                            ->count();
                    if($goldBunsDaily <= 0){
                        // create gold log
                        $this->addGoldLog([
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'buns_log_id' => $addBunsPlayLog['id'],
                            'gold' => 25, // 
                            'gold_type' => 'buns',
                            'status' => 'success',
                            'log_date' => date('Y-m-d H:i:s'),
                            'log_date_timestamp' => time(),
                            'last_ip' => $this->getIP(),
                        ]);

                        $hasGold = true;
                    }

                }

                // check all points
                $pointsSummary = $this->setPointsSummary($uid);

                // check Free Rights
                $freeRights = $this->getFreeRights($uid);

                return response()->json([
                    'status' => true,
                    'message' => 'สุ่มซาลาเปาเสี่ยงทายสำเร็จ',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                        'free_rights' => $freeRights,
                        'points_summary' => $pointsSummary,
                        'buns_result' => $randBunsResult['id'],
                        'has_gold' => $hasGold,
                    ],
                ]);

            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถสุ่มซาลาเปาเสี่ยงทายได้<br />กรุณาลองใหม่อีกครั้ง',
                ]);
            }
        }
        
        // Angpao 
        public function getAngpaoInfo(Request $request){

            if ($request->has('type') == false && $request->type != 'angpao_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $dateTime = $this->setQuestDateTime();

            // check all points
            $pointsSummary = $this->setPointsSummary($uid);

            // check to day is already add score to shop
            $checkUpdateGoldToShop = GoldLog::where('uid', $member->uid)
                                        ->where('gold_type', 'angpao')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            return response()->json([
                'status' => true,
                'content' => [
                    'username' => $member->username,
                    'character' => $member->char_name,
                    'points_summary' => $pointsSummary,
                    'gold_shop_updated' => $checkUpdateGoldToShop > 0 ? true : false,
                ],
            ]);

        }

        public function startPlayAngpao(Request $request){

            if ($request->has('type') == false && $request->type != 'play_angpao') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            if(time() < strtotime($this->startEventQuest) || time() > strtotime($this->endEventQuest)){
                return response()->json([
                    'status'    => false,
                    'message'   => 'ไม่อยู่ในช่วงเวลาร่วมกิจกรรม'
                ]);
            }

            // check anngpao rights from red gamo
            $redGamoInfo = $this->getRedGamo($member->uid);

            if($redGamoInfo['remain_red_gamo'] <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนแบงค์กาโม่แดง<br />ไม่พอเล่นอั่งเปาเพิ่มความสุข'
                ]);
            }

            // deduct red gamo
            $deductRedGamoRight = $this->addRedGamoDeductLog([
                                        'uid' => $member->uid,
                                        'username' => $member->username,
                                        'ncid' => $member->ncid,
                                        'status' => 'success',
                                        'log_date' => date('Y-m-d H:i:s'),
                                        'log_date_timestamp' => time(),
                                        'last_ip' => $this->getIP(),
                                    ]);
            
            if($deductRedGamoRight){

                // create play key
                $playKey = $member->uid.time();

                // created play log
                $this->addAngpaoPlayLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'deduct_red_gamo_id' => $deductRedGamoRight['id'],
                    'play_key' => $playKey,
                    'score' => 0,
                    'status' => 'pending',
                    'log_date' => date('Y-m-d H:i:s'),
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]);

                // check all points
                $pointsSummary = $this->setPointsSummary($uid);

                return response()->json([
                    'status' => true,
                    'message' => 'Game Start!',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                        'points_summary' => $pointsSummary,
                        'play_key' => $playKey,
                    ],
                ]);

            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถเริ่มเกมได้<br />กรุณาลองใหม่อีกครั้ง',
                ]);
            }
        }

        public function saveAngpaoPlayResult(Request $request){

            if ($request->has('type') == false && $request->type != 'save_angpao_result') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $playKey = $request->play_key;
            $score = intval($request->score);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            if(time() < strtotime($this->startEventQuest) || time() > strtotime($this->endEventQuest)){
                return response()->json([
                    'status'    => false,
                    'message'   => 'ไม่อยู่ในช่วงเวลาร่วมกิจกรรม'
                ]);
            }

            $dateTime = $this->setQuestDateTime();

            // $isMaxScore = false;

            // getplay log for update score
            $playLog = AngpaoPlayLog::where('uid', $member->uid)
                                    ->where('play_key', $playKey)
                                    ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                    ->count();
            if($playLog <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'No play data.'
                ]);
            }

            // check completed play key
            $completedPlayLog = AngpaoPlayLog::where('uid', $member->uid)
                                    ->where('play_key', $playKey)
                                    ->where('status', 'success')
                                    ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                    ->count();

            if($completedPlayLog > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'No play data(2).'
                ]);
            }

            // update score
            $updateScore = AngpaoPlayLog::where('uid', $member->uid)
                                        ->where('play_key', $playKey)
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->update([
                                            'status' => 'success',
                                            'score' => $score,
                                        ]);
            if($updateScore){

                // check max score
                $isMaxScore = $this->checkMaxScore($uid,$score);

                // check all points
                $pointsSummary = $this->setPointsSummary($uid);

                // check to day is already add score to shop
                $checkUpdateGoldToShop = GoldLog::where('uid', $member->uid)
                                                ->where('gold_type', 'angpao')
                                                ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                                ->count();

                return response()->json([
                    'status' => true,
                    'message' => 'อัพเดทคะแนนสำเร็จ',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                        'points_summary' => $pointsSummary,
                        'is_max_score' => $isMaxScore['status'],
                        'max_score' => $isMaxScore['max_score'],
                        'current_score' => $score,
                        'play_key' => $playKey,
                        'gold_shop_updated' => $checkUpdateGoldToShop > 0 ? true : false,
                    ],
                ]);

            }else{

                // check all points
                $pointsSummary = $this->setPointsSummary($uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถอัพเดทคะแนนได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                        'points_summary' => $pointsSummary,
                    ],
                ]);
            }
        }

        private function checkMaxScore($uid=null,$currentScore=0){
            if(empty($uid) || empty($currentScore)){
                return [
                    'status' => false,
                    'current_score' => $currentScore,
                    'max_score' => 0,
                ];
            }

            $dateTime = $this->setQuestDateTime();

            $getMaxScoreLog = AngpaoPlayLog::where('uid', $uid)
                                        // ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->max('score');

            if($currentScore >= $getMaxScoreLog){
                return [
                    'status' => true,
                    'current_score' => $currentScore,
                    'max_score' => $getMaxScoreLog <= 0 ? $currentScore : $getMaxScoreLog,
                ];
            }else{
                return [
                    'status' => false,
                    'current_score' => $currentScore,
                    'max_score' => $getMaxScoreLog,
                ];
            }
        }

        public function updateScoreToShop(Request $request){

            if ($request->has('type') == false && $request->type != 'score_to_shop') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $playKey = $request->play_key;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            
            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            if(time() < strtotime($this->startEventQuest) || time() > strtotime($this->endEventQuest)){
                return response()->json([
                    'status'    => false,
                    'message'   => 'ไม่อยู่ในช่วงเวลาร่วมกิจกรรม'
                ]);
            }

            $dateTime = $this->setQuestDateTime();

            // get play angpao log for update score to shop
            $playLog = AngpaoPlayLog::where('uid', $member->uid)
                                    ->where('play_key', $playKey)
                                    ->where('status', 'success')
                                    ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                    ->first();

            if(isset($playLog) && empty($playLog->score)){
                return response()->json([
                    'status' => false,
                    'message' => 'No play data.'
                ]);
            }

            // check to day is already add score to shop
            $checkAddScoreToShop = GoldLog::where('uid', $member->uid)
                                        ->where('gold_type', 'angpao')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();
                                        
            if($checkAddScoreToShop > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้เพิ่มคะแนนไปยังร้านโชห่วย<br />ของวันนี้แล้ว',
                ]);
            }

            // add score to shop gold
            $addScoreToShop = $this->addGoldLog([
                                    'uid' => $member->uid,
                                    'username' => $member->username,
                                    'ncid' => $member->ncid,
                                    'play_key' => $playKey,
                                    'status' => 'success',
                                    'gold' => $playLog->score,
                                    'gold_type' => 'angpao',
                                    'log_date' => date('Y-m-d H:i:s'),
                                    'log_date_timestamp' => time(),
                                    'last_ip' => $this->getIP(),
                                ]);
            
            if($addScoreToShop){

                // check all points
                $pointsSummary = $this->setPointsSummary($uid);

                // check to day is already add score to shop
                $checkUpdateGoldToShop = GoldLog::where('uid', $member->uid)
                                                ->where('gold_type', 'angpao')
                                                ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                                ->count();

                return response()->json([
                    'status' => true,
                    'message' => 'อัพเดทคะแนนไปยังร้านโชห่วย<br />เสร็จเรียบร้อย',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                        'points_summary' => $pointsSummary,
                        'gold_shop_updated' => $checkUpdateGoldToShop > 0 ? true : false,
                    ],
                ]);


            }else{

                // check all points
                $pointsSummary = $this->setPointsSummary($uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถอัพเดทคะแนนได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                        'points_summary' => $pointsSummary,
                    ],
                ]);

            }

        }

        // Gold Shop
        public function getGoldShopInfo(Request $request){

            if ($request->has('type') == false && $request->type != 'shop_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            
            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // $dateTime = $this->setQuestDateTime();

            // check all points
            $pointsSummary = $this->setPointsSummary($uid);

            // set reward list and can exchange
            $rewardList = $this->setReward($uid);

            return response()->json([
                'status' => true,
                'content' => [
                    'username' => $member->username,
                    'character' => $member->char_name,
                    'points_summary' => $pointsSummary,
                    'reward_list' => $rewardList,
                ],
            ]);

        }

        private function setReward($uid){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $myGold = $this->getGold($member->uid);

            $dateTime = $this->setQuestDateTime();

            $rewardList = [];

            $rewards = new Reward;
            $rewardData = $rewards->exchangeRewardList();

            if(count($rewardData) > 0){
                foreach($rewardData as $reward){

                    $canExchange = false;
                    $isLimitExchange = false;
                    $rewardHistoryCount = 0;


                    if($myGold['remain_gold'] > 0 && $myGold['remain_gold'] >= $reward['require_points']){
                        $canExchange = true;
                    }
                    
                    $rewardHistoryCount = ItemLog::where('uid', $member->uid)
                                                ->where('item_type', 'exchange_points')
                                                ->where('reward_id', $reward['id'])
                                                ->count();

                    if($rewardHistoryCount >= $reward['exchange_limit']){
                        $isLimitExchange = true;
                    }

                    $rewardList[] = [
                        'id' => $reward['id'],
                        'title' => $reward['product_title'],
                        'require_points' => $reward['require_points'],
                        'can_exchange' => $canExchange,
                        'is_limit' => $isLimitExchange,
                        'exchange_count' => $rewardHistoryCount,
                        'exchange_limit' => $reward['exchange_limit'],
                    ];

                }
            }
            
            return $rewardList;
        }

        public function getExchangeGoldReward(Request $request){

            if ($request->has('type') == false && $request->type != 'exchange_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $package_id = $request->package_id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            
            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $rewards = new Reward;
            $rewardInfo = $rewards->setExchangeByPackageId($package_id);
            if($rewardInfo == false){
                return response()->json([
                    'status' => false,
                    'message' => 'No data required.'
                ]);
            }

            // get remain gold
            $myGold = $this->getGold($uid);
            if($myGold['remain_gold'] <= 0 || $myGold['remain_gold'] < $rewardInfo['require_points']){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนเหรียญทองไม่พอแลกของรางวัล',
                ]);
            }

            // get reward history for check quantity
            $rewardHistoryCount = ItemLog::where('uid', $member->uid)
                                        ->where('item_type', 'exchange_points')
                                        ->where('reward_id', $rewardInfo['id'])
                                        ->count();

            // check reward exchange limit
            if($rewardHistoryCount >= $rewardInfo['exchange_limit']){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถแลกของรางวัลเกินจำนวนที่กำหนด',
                ]);
            }
            
            // deduct gold
            $deductGold = $this->addGoldDeductLog([
                                'uid' => $member->uid,
                                'username' => $member->username,
                                'ncid' => $member->ncid,
                                'gold' => $rewardInfo['require_points'],
                                'status' => 'success',
                                'log_date' => date('Y-m-d H:i:s'),
                                'log_date_timestamp' => time(),
                                'last_ip' => $this->getIP(),
                            ]);
            
            if($deductGold){

                // set reward data
                $goods_data = [];
                $goods_data[] = [
                    'goods_id' => $rewardInfo['product_id'],
                    'purchase_quantity' => $rewardInfo['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];

                // create send item log
                $itemLog = $this->addSendItemLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'deduct_gold_id' => $deductGold['id'],
                    'reward_id' => $rewardInfo['id'],
                    'product_id' => $rewardInfo['product_id'],
                    'product_title' => $rewardInfo['product_title'],
                    'product_quantity' => $rewardInfo['product_quantity'],
                    'item_type' => $rewardInfo['item_type'],
                    'amount' => $rewardInfo['amount'],
                    'send_item_status' => false,
                    'send_item_purchase_id' => '0',
                    'send_item_purchase_status' => '0',
                    'goods_data' => json_encode($goods_data),
                    'status' => 'pending',
                    'log_date' => date('Y-m-d H:i:s'),
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                    'last_ip' => $this->getIP()
                ]);

                $send_result_raw = $this->dosendItem($ncid, $goods_data);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'success'
                    ], $itemLog['id'], $uid);

                    // check all points
                    $pointsSummary = $this->setPointsSummary($uid);

                    // set reward list and can exchange
                    $rewardList = $this->setReward($uid);

                    return response()->json([
                        'status' => true,
                        'message' => 'ส่งของรางวัลเสร็จเรียบร้อย<br />กรุณาตรวจสอบกล่องจดหมายภายในเกม',
                        'content' => [
                            'username' => $member->username,
                            'character' => $member->char_name,
                            'points_summary' => $pointsSummary,
                            'reward_list' => $rewardList,
                        ],
                    ]);

                }else{
                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'unsuccess'
                    ], $itemLog['id'], $uid);

                    // check all points
                    $pointsSummary = $this->setPointsSummary($uid);

                    // set reward list and can exchange
                    $rewardList = $this->setReward($uid);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                        'content' => [
                            'username' => $member->username,
                            'character' => $member->char_name,
                            'points_summary' => $pointsSummary,
                            'reward_list' => $rewardList,
                        ],
                    ]);
                }
                

            }else{

                // check all points
                $pointsSummary = $this->setPointsSummary($uid);

                // set reward list and can exchange
                $rewardList = $this->setReward($uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถดำเนินการได้<br />กรุณาลองใหม่อีกครั้ง',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                        'points_summary' => $pointsSummary,
                        'reward_list' => $rewardList,
                    ],
                ]);

            }

        }

        // Ranking
        public function getRankInfo(Request $request){

            if ($request->has('type') == false && $request->type != 'rank_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            
            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $rankList = $this->setRankList();

            $collection = collect($rankList);
            $myRank = $collection->where('uid', $member->uid)->first();

            // check can claim reward
            $can_receive = ($myRank['rank'] >= 1 && $myRank['rank'] <= 50 && time() >= strtotime($this->startRankReward) && time() <= strtotime($this->endRankReward)) ? true : false;

            // check received reward
            $received = ItemLog::where('uid', $member->uid)
                                ->where('item_type', 'rank_reward')
                                ->count();

            // check all points
            $pointsSummary = $this->setPointsSummary($uid);

            return response()->json([
                'status' => true,
                'message' => 'Ranking Information.',
                'content' => [
                    'character' => $member->char_name,
                    'username' => $member->username,
                    'points_summary' => $pointsSummary,
                    'my_rank' => $myRank['rank'],
                    'my_score' => $myRank['score'],
                    'package_id' => $this->setRankRewardId($myRank['rank']),
                    'can_receive' => $can_receive,
                    'received' => $received > 0,
                    'ranks' => $rankList,
                ],
            ]);

        }

        private function setRankList(){

            $dateTime = $this->setQuestDateTime();
            
            $scoreList = Cache::remember('BNS:LUNAR_NEWYEAR_2019:RANK_QUERY_'.$dateTime['date_cache'], 1, function() {
                return AngpaoPlayLog::select(DB::raw('MAX(score) as max_score'), 'uid', 'username')
                                    ->groupBy('uid')
                                    ->orderBy('max_score', 'DESC')
                                    ->orderBy('log_date_timestamp', 'DESC')
                                    ->get();
            });

            // calculate rank
            $rank = 0;
            $prevScore = 0;
            $limitRank = 50;
            $rankList = [];
            if($scoreList->count() > 0){
                foreach($scoreList as $score){
                    
                    /* if($prevScore == $score->max_score){
                        $prevScore = $score->max_score;
                    }else{
                        $prevScore = $score->max_score;
                        $rank ++;
                    } */

                    $prevScore = $score->max_score;
                    $rank ++;

                    $memberInfo = null;
                    $memberInfo = Member::where('uid', $score->uid)->first();

                    $rankList[] = [
                        'rank' => $rank,
                        'uid' => $memberInfo->uid,
                        'char_name' => $memberInfo->char_name,
                        'world' => $this->setWorldName($memberInfo->world_id),
                        'score' => intval($score->max_score),
                    ];

                    if($rank >= $limitRank){
                        break;
                    }
                    
                }
            }

            return $rankList;
        }

        private function setWorldName($world_id=null){
            if(empty($world_id)){
                return false;
            }
            $worldName = '';

            switch($world_id){
                case 7801:
                    $worldName = 'อุนกุก';
                    break;
                
                case 7803:
                    $worldName = 'ทาลัน';
                    break;

                case 7804:
                    $worldName = 'นาริว';
                    break;
            }

            return $worldName;
        }

        private function setRankRewardId($rank=null){
            if(empty($rank)){
                return false;
            }

            if($rank == 1){
                return 1;
            }elseif($rank == 2){
                return 2;
            }elseif($rank == 3){
                return 3;
            }elseif($rank >= 4 && $rank <= 7){
                return 4;
            }elseif($rank >= 8 && $rank <= 10){
                return 5;
            }elseif($rank >= 11 && $rank <= 50){
                return 6;
            }
        }

        public function redeemRankReward(Request $request){

            if ($request->has('type') == false && $request->type != 'redeem_rank_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $package_id = $request->package_id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            
            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            if(in_array($package_id, [1,2,3,4,5,6]) == false){
                return response()->json([
                    'status' => false,
                    'message' => 'No data.'
                ]);
            }

            $rankList = $this->setRankList();

            $collection = collect($rankList);
            $myRank = $collection->where('uid', $member->uid)->first();

            // check can claim reward
            $can_receive = ($myRank['rank'] >= 1 && $myRank['rank'] <= 50 && time() >= strtotime($this->startRankReward) && time() <= strtotime($this->endRankReward)) ? true : false;
            if($can_receive == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่ตรงตามเงื่อนไข<br />ไม่สามารถรับของรางวัลได้'
                ]);
            }

            // check received reward
            $received = ItemLog::where('uid', $member->uid)
                                ->where('item_type', 'rank_reward')
                                ->count();
            if($can_receive == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปแล้ว'
                ]);
            }

            // set reward info
            $rewards = new Reward;
            $rewardInfo = $rewards->setRankByPackageId($package_id);
            if(!$rewardInfo){
                return response()->json([
                    'status' => false,
                    'message' => 'No data.'
                ]);
            }

            // set reward data
            $goods_data = [];
            $goods_data[] = [
                'goods_id' => $rewardInfo['product_id'],
                'purchase_quantity' => $rewardInfo['product_quantity'],
                'purchase_amount' => 0,
                'category_id' => 40
            ];

            // create send item log
            $itemLog = $this->addSendItemLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'deduct_gold_id' => null,
                'reward_id' => $rewardInfo['id'],
                'product_id' => $rewardInfo['product_id'],
                'product_title' => $rewardInfo['product_title'],
                'product_quantity' => $rewardInfo['product_quantity'],
                'item_type' => $rewardInfo['item_type'],
                'amount' => $rewardInfo['amount'],
                'send_item_status' => false,
                'send_item_purchase_id' => '0',
                'send_item_purchase_status' => '0',
                'goods_data' => json_encode($goods_data),
                'status' => 'pending',
                'log_date' => date('Y-m-d H:i:s'),
                'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                'last_ip' => $this->getIP()
            ]);

            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                    'status' => 'success'
                ], $itemLog['id'], $uid);

                // check all points
                $pointsSummary = $this->setPointsSummary($uid);

                $rankList = $this->setRankList();

                $collection = collect($rankList);
                $myRank = $collection->where('uid', $member->uid)->first();

                // check can claim reward
                $can_receive = ($myRank >= 1 && $myRank <= 50 && time() >= strtotime($this->startRankReward) && time() <= strtotime($this->endRankReward)) ? true : false;

                // check received reward
                $received = ItemLog::where('uid', $member->uid)
                                    ->where('item_type', 'rank_reward')
                                    ->count();

                return response()->json([
                    'status' => true,
                    'message' => 'ส่งของรางวัลเสร็จเรียบร้อย<br />กรุณาตรวจสอบกล่องจดหมายภายในเกม',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                        'points_summary' => $pointsSummary,
                        'can_receive' => $can_receive,
                        'received' => $received > 0
                    ],
                ]);

            }else{
                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                    'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                    'status' => 'unsuccess'
                ], $itemLog['id'], $uid);

                // check all points
                $pointsSummary = $this->setPointsSummary($uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                        'points_summary' => $pointsSummary,
                    ],
                ]);
            }

        }

    }