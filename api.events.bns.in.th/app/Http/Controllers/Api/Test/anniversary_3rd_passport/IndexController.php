<?php

    namespace App\Http\Controllers\Api\Test\anniversary_3rd_passport;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\Test\anniversary_3rd_passport\Setting;
    use App\Models\Test\anniversary_3rd_passport\Member;
    use App\Models\Test\anniversary_3rd_passport\MemberCheckin;
    use App\Models\Test\anniversary_3rd_passport\CheckList;
    use App\Models\Test\anniversary_3rd_passport\Reward;
    use App\Models\Test\anniversary_3rd_passport\ItemLog;
    use App\Models\Test\anniversary_3rd_passport\DeductLog;

    class IndexController extends BnsEventController{

        private $baseApi = 'http://api.apps.garena.in.th';

        private $userEvent = null;

        public function __construct(Request $request)
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();

        }

        private function checkEventStatus()
        {
             if($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip  = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData) {
                return true;
            } else {
                return false;
            }
        }

        private function is_accepted_ip()
        {
            $allow_ips   = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid      = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $arr = [
                    'uid'              => $uid,
                    'username'         => $username,
                    'ncid'             => $ncid,
                    'char_id'          => 0,
                    'char_name'        => '',
                    'world_id'         => 0,
                    'last_ip'          => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }

                $this->userEvent = $resp;

            } else {
                $member = Member::where('uid', $uid)->first();
                if (empty($member->ncid)) {
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if (empty($ncid)) {
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
                $this->userEvent = $member;
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if (empty($_id)) {
                return false;
            }

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:ANNIVERSARY3RDPASSPORT:TEST:ASSOCIATE_UID_WITH_NCID_'.$uid, 10, function () use ($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'user_info',
                    'uid'      => $uid,
                ]);
                $userNcInfo       = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }

            });
        }

        private function apiSendItem(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name'              => 'bns',
                'service'               => 'send_item',
                'user_id'               => $ncid,
                'purchase_description'  => 'Test sending item from garena events BNS 3 Years Anniversary Passport.',
                'goods'                 => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function createSendItemLog(array $arr) {
            if ($arr) {
                $resp = ItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateSendItemLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }
        private function apiDiamondBalance(int $uid)
        {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'balance',
                'uid' => $uid,
            ]);
        }

        private function setDiamondsBalance($uid){

            $diamondBalanceRaw = $this->apiDiamondBalance($uid);
            $diamondBalance = json_decode($diamondBalanceRaw);

            $diamonds = 0;
            if($diamondBalance->status == true) {
                $diamonds = intval($diamondBalance->balance);
            }

            return $diamonds;
        }

        private function apiDeductDiamond(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamomds for garena events BNS 3 Years Anniversary Passport.',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function setDiamondDeductData($deduct_diamond_amount = 0)
        {
            if ($deduct_diamond_amount == 0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function getEventSetting(){
            // return Cache::remember('BNS:LEAGUE:TEST:EVENT_SETTING_TEST', 10, function() {
                        return Setting::where('active', 1)->first();
                    // });
        }

        public function getEventInfo(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            // $eventSetting = $this->getEventSetting();

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            

            $eventInfo = $this->setEventInfo($uid);
            if($eventInfo['status']==false){
                return response()->json([
                        'status' => false,
                        'message'=>$eventInfo['message'],
                    ]);
            }

            return response()->json([
                'status' => true,
                'data' => $eventInfo
            ]);

        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return [
                    'status' => false,
                    'message' => 'ไม่พอข้อมูล'
                ];
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            $member = Member::where('uid', $uid)->first();

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // check is unlock passport
            $isUnlockPassport = false;
            $unlockPackageCanBuy = false;
            $unlockPackageAlreadyBuy = false;
            if($member->is_unlocked == 1){
                $isUnlockPassport = true;
            }

            // check can buy unlock package
            $diamondBalance = $this->setDiamondsBalance($member->uid);
            if($diamondBalance >= $eventSetting->diamonds_unlock){
                $unlockPackageCanBuy = true;
            }

            $unlockPackageHistory = ItemLog::where('uid',$member->uid)->where('package_type', 'unlock_reward')->count();
            if($unlockPackageHistory > 0){
                $unlockPackageAlreadyBuy = true;
            }


            // check has created checkin logs
            $checkinLog = MemberCheckin::where('uid', $member->uid)->get();
            if($isUnlockPassport == true && count($checkinLog) <= 0){
                $checkList = CheckList::orderBy('day','ASC')->get();

                if(count($checkList) > 0){
                    foreach($checkList as $check){
                        MemberCheckin::create([
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'checkin_date' => $check->checkin_date,
                            'week' => $check->week,
                            'day' => $check->day,
                            'package_key' => $check->package_key,
                            'is_checkin' => 0,
                            'is_prev_checkin' => 0,
                            'last_ip' => $this->getIP(),
                        ]);
                    }

                    $checkinLog = MemberCheckin::where('uid', $member->uid)->get();
                }
            }

            // daily checkin
            $checkinList = [];
            $currentDate = date('Y-m-d');
            $currentDateTimestamp = strtotime($currentDate);
            if($isUnlockPassport == true && count($checkinLog) > 0){
                foreach($checkinLog as $checkin){

                    $isCheckin = false;
                    $canCheckin = false;
                    $previousCheckin = false;
                    $isPrevCheckin = false;

                    if(strtotime($checkin->checkin_date) <= $currentDateTimestamp){

                        if($checkin->is_checkin == 1){
                            $isCheckin = true;
                        }

                        if($checkin->is_prev_checkin == 1){
                            $isPrevCheckin = true;
                        }

                        if(strtotime($checkin->checkin_date) < $currentDateTimestamp && $checkin->is_checkin == 0  || $isPrevCheckin == true){
                            $previousCheckin = true;
                        }

                        // check prev checkin
                        if($checkin->day == 1){
                            // check checkin status
                            // if(strtotime($checkin->checkin_date) <= $currentDateTimestamp){
                                $canCheckin = true;
                                $previousCheckin = false;
                            // }
                        }else{

                            $prevDay = $checkin->day - 1;
                            $checkinPrevDay = $this->getPrevCheckinDayCount($member->uid,$prevDay);

                            // check checkin status
                            if(strtotime($checkin->checkin_date) <= $currentDateTimestamp && $checkinPrevDay > 0){
                                $canCheckin = true;
                            }
                        }
                        

                    }

                    // get reward info
                    $rewardInfo = null;
                    $rewardInfo = Reward::where('package_key', $checkin->package_key)->where('package_type', 'checkin_reward')->first();

                    $rewardTitle = '';
                    $rewardImage = '';

                    if($rewardInfo){
                        $rewardTitle = $rewardInfo->package_name;
                        $rewardImage = $rewardInfo->image || '';
                    }

                    $checkinList[$checkin->week-1][] = [
                        'week' => $checkin->week,
                        'day' => $checkin->day,
                        'checkin_date' => $checkin->checkin_date,
                        'title' => $rewardTitle,
                        'img_key' => $rewardImage,
                        'is_checkin' => $isCheckin,
                        'is_prev_checkin' => $isPrevCheckin,
                        'can_checkin' => $canCheckin,
                        'previous_checkin' => $previousCheckin,
                    ];

                }
            }

            // weekly rewards
            $weeklyRewardList = [];

            $weeklyRewardInfo = Reward::where('package_type','special_reward')->where('week','!=',0)->orderBy('week','ASC')->get();
            if($isUnlockPassport == true && count($weeklyRewardInfo) > 0){
                foreach($weeklyRewardInfo as $weekReward){
                    

                    $canClaim = false;
                    $hasClaimed = false;

                    // check prev weekly reward is already cliamed.
                    $prevClaimed = false;
                    if($weekReward->week == 1){
                        $prevClaimed = true;
                    }elseif(in_array($weekReward->week, [2,3,4])){
                        $prevWeek = $weekReward->week - 1;
                        $prevClaimedHistory = $this->getSpecialItemHistoryCount($member->uid, $prevWeek);
                        if($prevClaimedHistory > 0){
                            $prevClaimed = true;
                        }
                    }

                    // check can claim
                    $checkinCount = MemberCheckin::where('uid', $member->uid)->where('week', $weekReward->week)->where('is_checkin', 1)->count();
                    if($checkinCount == 7 && $prevClaimed == true){
                        $canClaim = true;
                    }

                    // check is already cliamed
                    $specialRewardHistory = $this->getSpecialItemHistoryCount($member->uid, $weekReward->week);
                    if($specialRewardHistory > 0){
                        $hasClaimed = true;
                    }

                    $rewardTitle = '';
                    $rewardImage = '';

                    if($weekReward){
                        $rewardTitle = $weekReward->package_name;
                        $rewardImage = $weekReward->image || '';
                    }

                    $weeklyRewardList[] = [
                        'week' => $weekReward->week,
                        'title' => $rewardTitle,
                        'img_key' => $rewardImage,
                        'can_claim' => $canClaim,
                        'has_claimed' => $hasClaimed,
                    ];

                }
            }


            $finalReward = [];
            
            $finalRewardCanClaim = false;
            $finalRewardhasClaimed = false;

            $finalRewardTitle = '';
            $finalRewardImage = '';

            $finalRewardInfo = Reward::where('package_type', 'final_reward')->first();
            if($finalRewardInfo){
                $finalRewardTitle = $finalRewardInfo->package_name;
                $finalRewardImage = $finalRewardInfo->image || '';
            }

            // check can claim
            $checkInNoPrevHistorycount = $this->getCheckinNoPrevCount($member->uid);
            if($checkInNoPrevHistorycount == $eventSetting->num_days){
                $finalRewardCanClaim = true;
            }

            // check item history
            $finalRewardHistory = $this->getFinalRewardsClaimCount($member->uid);
            if($finalRewardHistory > 0){
                $finalRewardhasClaimed = true;
            }

            $finalReward = [
                'title' => $finalRewardTitle,
                'img_key' => $finalRewardImage,
                'can_claim' => $finalRewardCanClaim,
                'has_claimed' => $finalRewardhasClaimed,
            ];

            return [
                'status' => true,
                "message" => 'Event Info',
                'username'=>$member->username,
                'is_unlocked' => $isUnlockPassport,
                "unlock_package" => [
                    "can_buy" => $unlockPackageCanBuy,
                    "already_buy"  => $unlockPackageAlreadyBuy
                ],
                'checkin_list' => $checkinList,
                'weekly_rewards' => $weeklyRewardList,
                'final_reward' => $finalReward,
            ];

        }

        private function getPrevCheckinDayCount($uid,$prevDay){
            return MemberCheckin::where('uid',$uid)
                            ->where('day', $prevDay)
                            ->where('is_checkin', 1)
                            ->count();
        }

        private function getCheckinNoPrevCount($uid){
            return MemberCheckin::where('uid',$uid)
                            ->where('is_checkin', 1)
                            ->where('is_prev_checkin', 0)
                            ->count();
        }

        private function getSpecialItemHistoryCount($uid,$week){
            return ItemLog::where('uid',$uid)
                        ->where('week', $week)
                        ->where('package_type', 'special_reward')
                        ->count();
        }

        private function getFinalRewardsClaimCount($uid){
            return ItemLog::where('uid',$uid)
                        ->where('package_type', 'final_reward')
                        ->count();
        }

        private function createDeductLog(array $arr){
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductLog(array $arr, $log_id){
            if ($arr) {
                return DeductLog::where('id', $log_id)->update($arr);
            }
            return null;
        }

        private function updateCheckinLog(array $arr, $uid, $week, $day){
            if ($arr) {
                return MemberCheckin::where('uid', $uid)->where('week', $week)->where('day', $day)->update($arr);
            }
            return null;
        }

        public function getUnlockPassport(Request $request)
        {

            if ($request->has('type') == false || $request->type != 'unlock_passport') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_unlocked == 1){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณได้ซื้อแพ็คเกจ 3 year anniversary ไปแล้ว'
                ]);
            }

            // check diamonds balance
            $diamondBalance = $this->setDiamondsBalance($member->uid);
            if($diamondBalance < $eventSetting->diamonds_unlock){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอซื้อแพ็คเกจ 3 year anniversary'
                ]);
            }

            // set deduct diamonds
            $logDeduct = $this->createDeductLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'deduct_type' => 'unlock_reward',
                'diamonds' => $eventSetting->diamonds_unlock,
                'status' => 'pending',
                'last_ip' => $this->getIP(),
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
            ]);

            if($logDeduct){

                // set desuct diamonds
                $deductData = $this->setDiamondDeductData($eventSetting->diamonds_unlock);

                $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
                $resp_deduct = json_decode($resp_deduct_raw);
                if(is_object($resp_deduct) && is_null($resp_deduct) == false){

                    $this->updateDeductLog([
                        'before_deduct_diamond' => $diamondBalance,
                        'after_deduct_diamond' => $diamondBalance - $eventSetting->diamonds_unlock,
                        'deduct_status' => $resp_deduct->status ?: 0,
                        'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
                        'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'success'
                    ], $logDeduct['id']);

                    // get unlock passport package
                    $unlockPackage = Reward::where('package_type', 'unlock_reward')->first();
                    if(!$unlockPackage){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />ไม่สามารถดำเนินการได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                        ]);
                    }

                    $goodsData = [
                        [
                            'goods_id' => $unlockPackage->package_id,
                            'purchase_quantity' => $unlockPackage->package_quantity,
                            'purchase_amount' => 0,
                            'category_id' => 40
                        ]
                    ];

                    // set send item log
                    $itemLog = $this->createSendItemLog([//add send item log
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'package_type' => 'unlock_reward',
                        'package_key' => $unlockPackage->package_key,
                        'package_id' => $unlockPackage->package_id,
                        'package_name' => $unlockPackage->package_name,
                        'package_quantity' => $unlockPackage->package_quantity,
                        'package_amount' => $unlockPackage->package_amount,
                        'image' => $unlockPackage->image,
                        'goods_data' => json_encode($goodsData),
                        'status' => 'pending',
                        'last_ip' => $this->getIP(),
                        'log_date' => (string)date('Y-m-d'), // set to string
                        'log_date_timestamp' => time(),
                    ]);

                    //send item
                    $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
                    $send_result = json_decode($send_result_raw);
                    if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                        //do update send item log
                        $this->updateSendItemLog([
                            'send_item_status' => $send_result->status ? : false,
                            'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                            'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                            'status' => 'success'
                        ], $itemLog['id'], $member->uid);

                        $member->is_unlocked = 1;
                        if($member->save()){
                            $eventInfo = $this->setEventInfo($member->uid);

                            return response()->json([
                                'status' => true,
                                'message' => 'ซื้อแพ็คเกจ 3 Year Anniversary สำเร็จ<br />กรุณาตรวจสอบไอเทมในกล่องจดหมายภายในเกม',
                                'data' => $eventInfo,
                            ]);

                        }else{
                            return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถดำเนินการได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                            ]);
                        }

                    }else{
                        //do update send item log
                        $this->updateSendItemLog([
                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                            'status' => 'unsuccess'
                        ], $itemLog['id'], $member->uid);

                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                        ]);
                    }


                }else{
                    $arr_log['status'] = 'unsuccess';
                    $this->updateDeductLog($arr_log, $logDeduct['id']);
                    return response()->json([
                        'status' => false,
                        'message' => 'ขออภัย<br />ไม่สามารถดำเนินการหัก Diamonds ได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }

            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />กรุณาลองใหม่อีกครั้ง'
                ]);
            }

        }

        public function getCheckin(Request $request)
        {

            if ($request->has('type') == false || $request->type != 'checkin') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $week = $request->week;
            $day = $request->day;

            if(in_array($week, [1,2,3,4]) == false){
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter(2)!'
                ]);
            }

            if($day < 1 || $day > $eventSetting->num_days){ // num_days = 28
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter(3)!'
                ]);
            }

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->is_unlocked == 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณยังไม่ได้ซื้อแพ็คเกจ 3 year anniversary'
                ]);
            }

            $checkinStatusInfo = $this->checkInStatusInfo($member->uid,$week,$day);

            if($checkinStatusInfo['can_checkin'] == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณไม่สามารถเช็คอินได้'
                ]);
            }

            if($checkinStatusInfo['is_checkin'] == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้เช็คอินไปก่อนหน้านี้แล้ว'
                ]);
            }

            if($checkinStatusInfo['previous_checkin'] == true){

                // check diamonds balance
                $diamondBalance = $this->setDiamondsBalance($member->uid);
                if($diamondBalance < $eventSetting->diamonds_checkin){
                    return response()->json([
                        'status' => false,
                        'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอเช็คอิน'
                    ]);
                }

                // set deduct diamonds for previous checkin
                $logDeduct = $this->createDeductLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'week' => $week,
                    'day' => $day,
                    'deduct_type' => 'checkin_reward',
                    'diamonds' => $eventSetting->diamonds_checkin,
                    'status' => 'pending',
                    'last_ip' => $this->getIP(),
                    'log_date' => (string)date('Y-m-d'), // set to string
                    'log_date_timestamp' => time(),
                ]);

                if($logDeduct){

                    // set desuct diamonds
                    $deductData = $this->setDiamondDeductData($eventSetting->diamonds_checkin);

                    $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
                    $resp_deduct = json_decode($resp_deduct_raw);
                    if(is_object($resp_deduct) && is_null($resp_deduct) == false){

                        $this->updateDeductLog([
                            'before_deduct_diamond' => $diamondBalance,
                            'after_deduct_diamond' => $diamondBalance - $eventSetting->diamonds_checkin,
                            'deduct_status' => $resp_deduct->status ?: 0,
                            'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
                            'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
                            'deduct_data' => json_encode($deductData),
                            'status' => 'success'
                        ], $logDeduct['id']);

                        // Reward Info
                        $rewardInfo = Reward::where('package_key', $checkinStatusInfo['package_key'])->where('package_type', 'checkin_reward')->first();
                        $goodsData = [
                            [
                                'goods_id' => $rewardInfo->package_id,
                                'purchase_quantity' => $rewardInfo->package_quantity,
                                'purchase_amount' => 0,
                                'category_id' => 40
                            ]
                        ];

                        // set send item log
                        $itemLog = $this->createSendItemLog([//add send item log
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'week' => $week,
                            'day' => $day,
                            'package_type' => 'checkin_reward',
                            'package_key' => $rewardInfo->package_key,
                            'package_id' => $rewardInfo->package_id,
                            'package_name' => $rewardInfo->package_name,
                            'package_quantity' => $rewardInfo->package_quantity,
                            'package_amount' => $rewardInfo->package_amount,
                            'image' => $rewardInfo->image,
                            'goods_data' => json_encode($goodsData),
                            'status' => 'pending',
                            'last_ip' => $this->getIP(),
                            'log_date' => (string)date('Y-m-d'), // set to string
                            'log_date_timestamp' => time(),
                        ]);
    
                        //send item
                        $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
                        $send_result = json_decode($send_result_raw);
                        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                            //do update send item log
                            $this->updateSendItemLog([
                                'send_item_status' => $send_result->status ? : false,
                                'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                                'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                                'status' => 'success'
                            ], $itemLog['id'], $member->uid);

                            // update checkin log
                            $updateCheckin = $this->updateCheckinLog([
                                'is_checkin' => 1,
                                'is_prev_checkin' => 1,
                            ], $member->uid, $week, $day);
                            
                            if($updateCheckin ){

                                $eventInfo = $this->setEventInfo($member->uid);
    
                                return response()->json([
                                    'status' => true,
                                    'message' => '<h3>เช็คอินสำเร็จ</h3>ได้รับ<br />'.$rewardInfo->package_name.'<br/ >ตรวจสอบไอเทมที่กล่องจดหมาย',
                                    'data' => $eventInfo,
                                ]);  

                            }else{
                                return response()->json([
                                    'status' => false,
                                    'message' => 'ไม่สามารถดำเนินการได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                                ]);
                            }
                            
    
                        }else{
                            //do update send item log
                            $this->updateSendItemLog([
                                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                                'status' => 'unsuccess'
                            ], $itemLog['id'], $member->uid);
    
                            return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                            ]);
                        }


                    }else{

                        $arr_log['status'] = 'unsuccess';
                        $this->updateDeductLog($arr_log, $logDeduct['id']);
                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                        ]);

                    }

                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'ขออภัย<br />กรุณาลองใหม่อีกครั้ง'
                    ]);
                }

            }else{

                // Reward Info
                $rewardInfo = Reward::where('package_key', $checkinStatusInfo['package_key'])->where('package_type', 'checkin_reward')->first();
                $goodsData = [
                    [
                        'goods_id' => $rewardInfo->package_id,
                        'purchase_quantity' => $rewardInfo->package_quantity,
                        'purchase_amount' => 0,
                        'category_id' => 40
                    ]
                ];

                // set send item log
                $itemLog = $this->createSendItemLog([//add send item log
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'week' => $week,
                    'day' => $day,
                    'package_type' => 'checkin_reward',
                    'package_key' => $rewardInfo->package_key,
                    'package_id' => $rewardInfo->package_id,
                    'package_name' => $rewardInfo->package_name,
                    'package_quantity' => $rewardInfo->package_quantity,
                    'package_amount' => $rewardInfo->package_amount,
                    'image' => $rewardInfo->image,
                    'goods_data' => json_encode($goodsData),
                    'status' => 'pending',
                    'last_ip' => $this->getIP(),
                    'log_date' => (string)date('Y-m-d'), // set to string
                    'log_date_timestamp' => time(),
                ]);

                //send item
                $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                    //do update send item log
                    $this->updateSendItemLog([
                        'send_item_status' => $send_result->status ? : false,
                        'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                        'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                        'status' => 'success'
                    ], $itemLog['id'], $member->uid);

                    // update checkin log
                    $updateCheckin = $this->updateCheckinLog([
                        'is_checkin' => 1,
                        'is_prev_checkin' => 0,
                    ], $member->uid, $week, $day);
                    
                    if($updateCheckin ){

                        $eventInfo = $this->setEventInfo($member->uid);

                        return response()->json([
                            'status' => true,
                            'message' => '<h3>เช็คอินสำเร็จ</h3>ได้รับ<br />'.$rewardInfo->package_name.'<br/ >ตรวจสอบไอเทมที่กล่องจดหมาย',
                            'data' => $eventInfo,
                        ]);  

                    }else{
                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถดำเนินการได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                        ]);
                    }

                }else{
                    //do update send item log
                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                        'status' => 'unsuccess'
                    ], $itemLog['id'], $member->uid);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                    ]);
                }

            }


        }

        private function checkInStatusInfo($uid,$week,$day){
            if(empty($uid) || empty($week) || empty($day)){
                return false;
            }

            $currentDate = date('Y-m-d');
            $currentDateTimestamp = strtotime($currentDate);

            $checkin = MemberCheckin::where('uid', $uid)->where('week', $week)->where('day', $day)->first();
            if(!$checkin){
                return false;
            }

            $isCheckin = false;
            $canCheckin = false;
            $previousCheckin = false;
            $isPrevCheckin = false;

            if(strtotime($checkin->checkin_date) <= $currentDateTimestamp){

                if($checkin->is_checkin == 1){
                    $isCheckin = true;
                }

                if($checkin->is_prev_checkin == 1){
                    $isPrevCheckin = true;
                }

                if(strtotime($checkin->checkin_date) < $currentDateTimestamp && $checkin->is_checkin == 0 || $isPrevCheckin == true){
                    $previousCheckin = true;
                }

                // check prev checkin
                if($checkin->day == 1){
                    // check checkin status
                    // if(strtotime($checkin->checkin_date) <= $currentDateTimestamp){
                        $canCheckin = true;
                        $previousCheckin = false;
                    // }
                }else{

                    $prevDay = $checkin->day - 1;
                    $checkinPrevDay = $this->getPrevCheckinDayCount($uid,$prevDay);

                    // check checkin status
                    if(strtotime($checkin->checkin_date) <= $currentDateTimestamp && $checkinPrevDay > 0){
                        $canCheckin = true;
                    }
                }

            }

            // get reward info
            $rewardInfo = null;
            $rewardInfo = Reward::where('package_key', $checkin->package_key)->where('package_type', 'checkin_reward')->first();

            $rewardTitle = '';
            $rewardImage = '';

            if($rewardInfo){
                $rewardTitle = $rewardInfo->package_name;
                $rewardImage = $rewardInfo->image;
            }

            return [
                'week' => $checkin->week,
                'day' => $checkin->day,
                'checkin_date' => $checkin->checkin_date,
                'title' => $rewardTitle,
                'img_key' => $rewardImage,
                'is_checkin' => $isCheckin,
                'is_prev_checkin' => $isPrevCheckin,
                'can_checkin' => $canCheckin,
                'previous_checkin' => $previousCheckin,
                'package_key' => $checkin->package_key,
            ];
        }

        // claim special reward
        public function getClaimWeeklyReward(Request $request)
        {
            if ($request->has('type') == false || $request->type != 'claim_weekly_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $week = $request->week;

            if(in_array($week, [1,2,3,4]) == false){
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter(2)!'
                ]);
            }

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check already purchase package
            if($member->is_unlocked == 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณยังไม่ได้ซื้อ 3 Years Anniversary แพ็คเกจ'
                ]);
            }

            $weeklyStatusInfo = $this->weeklyStatusInfo($member->uid,$week);

            if($weeklyStatusInfo['can_claim'] == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถรับของวัลได้<br />เนื่องจากไม่ตรงเงื่อนไข'
                ]);
            }

            if($weeklyStatusInfo['has_claimed'] == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปแล้ว'
                ]);
            }

            // Reward Info
            $rewardInfo = Reward::where('package_type','special_reward')->where('week', $week)->where('package_key', $weeklyStatusInfo['package_key'])->first();
            if(!$rewardInfo){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถดำเนินการได้<br/>กรุณาลองใหม่อีกครั้ง'
                ]);
            }

            $goodsData = [
                [
                    'goods_id' => $rewardInfo->package_id,
                    'purchase_quantity' => $rewardInfo->package_quantity,
                    'purchase_amount' => 0,
                    'category_id' => 40
                ]
            ];

            // set send item log
            $itemLog = $this->createSendItemLog([//add send item log
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'week' => $week,
                'package_type' => 'special_reward',
                'package_key' => $rewardInfo->package_key,
                'package_id' => $rewardInfo->package_id,
                'package_name' => $rewardInfo->package_name,
                'package_quantity' => $rewardInfo->package_quantity,
                'package_amount' => $rewardInfo->package_amount,
                'image' => $rewardInfo->image,
                'goods_data' => json_encode($goodsData),
                'status' => 'pending',
                'last_ip' => $this->getIP(),
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
            ]);

            //send item
            $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                //do update send item log
                $this->updateSendItemLog([
                    'send_item_status' => $send_result->status ? : false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                    'status' => 'success'
                ], $itemLog['id'], $member->uid);

                $eventInfo = $this->setEventInfo($member->uid);

                return response()->json([
                    'status' => true,
                    'message' => '<h3>เช็คอินสำเร็จ</h3>ได้รับ<br />'.$rewardInfo->package_name.'<br/ >ตรวจสอบไอเทมที่กล่องจดหมาย',
                    'data' => $eventInfo,
                ]);

            }else{
                //do update send item log
                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                ], $itemLog['id'], $member->uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                ]);
            }


        }

        private function weeklyStatusInfo($uid,$week){
            if(empty($uid) || empty($week)){
                return false;
            }

            $canClaim = false;
            $hasClaimed = false;
            $rewardTitle = '';
            $rewardImage = '';
            $packagekey = 0;
            
            $currentDate = date('Y-m-d');
            $currentDateTimestamp = strtotime($currentDate);

            $weeklyRewardInfo = Reward::where('package_type','special_reward')->where('week', $week)->first();
            if($weeklyRewardInfo){

                // check prev weekly reward is already cliamed.
                $prevClaimed = false;
                if($week == 1){
                    $prevClaimed = true;
                }elseif(in_array($week, [2,3,4])){
                    $prevWeek = $week - 1;
                    $prevClaimedHistory = $this->getSpecialItemHistoryCount($uid, $prevWeek);
                    if($prevClaimedHistory > 0){
                        $prevClaimed = true;
                    }
                }

                // check can claim
                $checkinCount = MemberCheckin::where('uid', $uid)->where('week', $week)->where('is_checkin', 1)->count();
                if($checkinCount == 7 && $prevClaimed == true){
                    $canClaim = true;
                }

                // check is already cliamed
                $specialRewardHistory = $this->getSpecialItemHistoryCount($uid, $week);
                if($specialRewardHistory > 0){
                    $hasClaimed = true;
                }

                $rewardTitle = $weeklyRewardInfo->package_name;
                $rewardImage = $weeklyRewardInfo->image;
                $packagekey  = $weeklyRewardInfo->package_key;
            }

            return [
                'week' => $week,
                'title' => $rewardTitle,
                'img_key' => $rewardImage,
                'can_claim' => $canClaim,
                'has_claimed' => $hasClaimed,
                'package_key' => $packagekey,
            ];

        }
        
        // claim final reward
        public function getClaimFinalReward(Request $request)
        {
            if ($request->has('type') == false || $request->type != 'claim_final_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check already purchase package
            if($member->is_unlocked == 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณยังไม่ได้ซื้อ 3 Years Anniversary แพ็คเกจ'
                ]);
            }

            $finalRewardStatusInfo = $this->finalRewardStatusInfo($member->uid);
            if($finalRewardStatusInfo['can_claim'] == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถรับของวัลได้<br />เนื่องจากไม่ตรงเงื่อนไข'
                ]);
            }

            if($finalRewardStatusInfo['has_claimed'] == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปแล้ว'
                ]);
            }

            // Reward Info
            $rewardInfo = Reward::where('package_type','final_reward')->where('package_key', $finalRewardStatusInfo['package_key'])->first();
            if(!$rewardInfo){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถดำเนินการได้<br/>กรุณาลองใหม่อีกครั้ง'
                ]);
            }

            $goodsData = [
                [
                    'goods_id' => $rewardInfo->package_id,
                    'purchase_quantity' => $rewardInfo->package_quantity,
                    'purchase_amount' => 0,
                    'category_id' => 40
                ]
            ];

            // set send item log
            $itemLog = $this->createSendItemLog([//add send item log
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'package_type' => 'final_reward',
                'package_key' => $rewardInfo->package_key,
                'package_id' => $rewardInfo->package_id,
                'package_name' => $rewardInfo->package_name,
                'package_quantity' => $rewardInfo->package_quantity,
                'package_amount' => $rewardInfo->package_amount,
                'image' => $rewardInfo->image,
                'goods_data' => json_encode($goodsData),
                'status' => 'pending',
                'last_ip' => $this->getIP(),
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
            ]);

            //send item
            $send_result_raw = $this->apiSendItem($member->ncid, $goodsData);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                //do update send item log
                $this->updateSendItemLog([
                    'send_item_status' => $send_result->status ? : false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                    'status' => 'success'
                ], $itemLog['id'], $member->uid);

                $eventInfo = $this->setEventInfo($member->uid);

                return response()->json([
                    'status' => true,
                    'message' => '<h3>เช็คอินสำเร็จ</h3>ได้รับ<br />'.$rewardInfo->package_name.'<br/ >ตรวจสอบไอเทมที่กล่องจดหมาย',
                    'data' => $eventInfo,
                ]);

            }else{
                //do update send item log
                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                ], $itemLog['id'], $member->uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                ]);
            }

        }

        private function finalRewardStatusInfo($uid){
            if(empty($uid)){
                return false;
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            $canClaim = false;
            $hasClaimed = false;
            $rewardTitle = '';
            $rewardImage = '';
            $packagekey = 0;
            
            $currentDate = date('Y-m-d');
            $currentDateTimestamp = strtotime($currentDate);

            $finalRewardInfo = Reward::where('package_type','final_reward')->first();
            if($finalRewardInfo){

                $rewardTitle = $finalRewardInfo->package_name;
                $rewardImage = $finalRewardInfo->image;
                $packagekey = $finalRewardInfo->package_key;
    
                // check can claim
                $checkInNoPrevHistorycount = $this->getCheckinNoPrevCount($uid);
                if($checkInNoPrevHistorycount == $eventSetting->num_days){
                    $canClaim = true;
                }
    
                // check item history
                $finalRewardHistory = $this->getFinalRewardsClaimCount($uid);
                if($finalRewardHistory > 0){
                    $hasClaimed = true;
                }

            }

            return [
                'title' => $rewardTitle,
                'img_key' => $rewardImage,
                'can_claim' => $canClaim,
                'has_claimed' => $hasClaimed,
                'package_key' => $packagekey,
            ];

        }

    }