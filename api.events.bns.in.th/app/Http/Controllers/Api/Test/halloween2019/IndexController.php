<?php

namespace App\Http\Controllers\Api\Test\halloween2019;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

use App\Models\Test\halloween2019\Member;
use App\Models\Test\halloween2019\Reward;
use App\Models\Test\halloween2019\SendItemLog;
use App\Models\Test\halloween2019\QuestLog;

class IndexController extends BnsEventController
{

    private $baseApi = 'http://api.apps.garena.in.th';
    private $startTime = '2019-10-10 06:00:00';
    private $endTime = '2019-11-08 23:59:59';
    private $userEvent = '';

    private $questList = [
        [
            'id'   => 1794,
            'coin' => 15,
            'name' => "ความลับของทัพปีศาจ",
        ],
        [
            'id'   => 1790,
            'coin' => 15,
            'name' => "การแสดงแห่งค่ำคืนสีเลือด",
        ],
        [
            'id'   => 1744,
            'coin' => 15,
            'name' => "ความปรารถนาอันมืดมัว",
        ],
        [
            'id'   => 1558,
            'coin' => 15,
            'name' => "ภัยคุกคามปริศนาที่ตื่นขึ้น",
        ],
        [
            'id'   => 1735,
            'coin' => 10,
            'name' => "ฝันร้ายแห่งกรุสมบัติ",
        ],
        [
            'id'   => 1697,
            'coin' => 10,
            'name' => "เสียงเพลงอันโหยหวน",
        ],
        [
            'id'   => 1672,
            'coin' => 10,
            'name' => "ผู้พลัดถิ่นแห่งเมืองโบราณพรรณไม้",
        ],
        [
            'id'   => 1614,
            'coin' => 10,
            'name' => "เสียงเพรียกของสายลม",
        ],
        [
            'id'   => 1623,
            'coin' => 5,
            'name' => "รุกฆาต",
        ],
        [
            'id'   => 1674,
            'coin' => 5,
            'name' => "หุบเขาเหมันต์",
        ],
        [
            'id'   => 1528,
            'coin' => 5,
            'name' => "ผู้บุกรุกที่ศาลนักพรตนาริว",
        ],
        [
            'id'   => 1478,
            'coin' => 5,
            'name' => "เสียงหอนของเตาหลอม",
        ],
        [
            'id'   => 1365,
            'coin' => 5,
            'name' => "พันธะที่ผูกเราไว้",
        ],
        [
            'id'   => 1064,
            'coin' => 5,
            'name' => "ดาบของจอมดาบเดี่ยวมรณะ",
        ],
    ];

    public function __construct(Request $request)
    {
        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);
        $this->checkEventStatus();

    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status'  => false,
                'type'    => 'no_permission',
                'message' => 'Sorry, you do not have permission.',
            ]));
        }
        // dd($this->startTime,$this->endTime);
        if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
            die(json_encode([
                'status'  => false,
                'type'    => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม',
            ]));
        }

//        if ($this->checkIsMaintenance()) {
//            die(json_encode([
//                'status'  => false,
//                'type'    => 'maintenance',
//                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์',
//            ]));
//        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง',
                ]));
            }
        } else {
            die(json_encode([
                'status'  => false,
                'type'    => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน',
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function is_accepted_ip()
    {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'              => $uid,
                'username'         => $username,
                'ncid'             => $ncid,
                'character_id'     => 0,
                'character_name'   => "",
                'quest_success_1'  => 0,
                'quest_success_2'  => 0,
                'quest_success_3'  => 0,
                'quest_success_4'  => 0,
                'quest_success_5'  => 0,
                'quest_success_6'  => 0,
                'quest_success_7'  => 0,
                'quest_success_8'  => 0,
                'quest_success_9'  => 0,
                'quest_success_10' => 0,
                'quest_success_11' => 0,
                'quest_success_12' => 0,
                'quest_success_13' => 0,
                'quest_success_14' => 0,
                'coin'             => 0,
                'coin_used'        => 0,
                'last_ip'          => $this->getIP(),
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
            $this->userEvent = $resp;

        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id)) {
            return false;
        }

        return Member::where('_id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:BUFFET:ASSOCIATE_UID_WITH_NCID_'.$uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'user_info',
                'uid'      => $uid,
            ]);
            $userNcInfo       = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }

        });
    }

    private function dosendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Test sending item from garena events bns Buffet.',
            'goods'                => json_encode($goodsData),
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function doGetChar($uid, $ncid)
    {//new fixed function
        $resp_raw = $this->apiCharacters($uid, $ncid);

        $resp = json_decode($resp_raw);
        if (is_object($resp)) {
            if ($resp->status) {
                $content = [];
                $i       = 1;
                foreach ($resp->response as $key => $value) {
                    $content[] = [
                        // 'id' => $i,
                        'char_id'   => $value->id,
                        'char_name' => $value->name,
                        // 'world_id' => $value->world_id,
                        // 'job' => $value->job,
                        // 'level' => $value->level,
                        // 'creation_time' => $value->creation_time,
                        // 'last_play_start' => $value->last_play_start,
                        // 'last_play_end' => $value->last_play_end,
                        // 'last_ip' => $value->last_ip,
                        // 'exp' => $value->exp,
                        // 'mastery_level' => $value->mastery_level,
                    ];

                    $i++;
                }
                return $content;
            }
        }
        return null;
    }

    private function setQuestCompletedCount($uid, $charId, $questId, $start, $end)
    {

        $completedCount = 0;

        $Count = $this->apiCheckQuestCompleted($uid, $charId, $questId, $start, $end);
        if ($Count->status) {
            $completedCount = intval($Count->response->quest_completed_count);
        }

        return $completedCount;

    }

    private function calQuest($uid, $charId, $start, $end)
    {
        $quest = $this->questList;

        $numpassquest = Cache::remember('BNS:BUFFET:CALQUEST_'.$uid, 1,
            function () use ($quest, $uid, $charId, $start, $end) {
                $numpassquest = [
                    0,
                    0,
                    0,
                ];

                foreach ($quest as $key => $value) {
                    $totalpass = 0;
                    foreach ($value['quest'] as $key_q => $value_q) {
                        $numquest  = $this->setQuestCompletedCount($uid, $charId, $value_q['id'], $start, $end);
                        $totalpass += $numquest;
                    }
                    $numpassquest[$key] = $totalpass;
                }

                return $numpassquest;
            });
        // dd($numpassquest);

        return $numpassquest;
    }

    private function addItemHistoryLog($arr)
    {
        return SendItemLog::create($arr);
    }

    private function updateItemHistoryLog($arr, $log_id, $uid)
    {
        return ItemLog::where('_id', $log_id)->where('uid', $uid)->update($arr);
    }

    private function checkItemHistory($uid, $package_id)
    {
        $itemHistory = ItemLog::where('uid', $uid)
            ->where('package_id', $package_id)
            // ->where('status', 'success')
            ->count();
        return $itemHistory;
    }

    private function setEventInfo($uid)
    {
        if (empty($uid)) {
            return false;
        }
        $member = Member::where('uid', $uid)->first();
        if ($member->character_id == 0 || $member->character_id == null) {

            $mychar = $this->doGetChar($member->uid, $member->ncid);

            return [
                'username'       => $member->username,
                'character_name' => '',
                'selected_char'  => false,
                'characters'     => $mychar,
                'itemLists'      => $this->getItemLists(),
                'coin'           => 0,
            ];
        }

        $this->summaryUserEvent();

        return [
            'username'       => $member->username,
            'character_name' => $member->character_name,
            'selectd_char'   => true,
            'characters'     => [],
            'coin'           => $this->getCoin(),
            'itemLists'      => $this->getItemLists(),
        ];
    }

    public function getEventInfo(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter!',
            ]);
        }

        $userData = $this->userData;
        $uid      = $userData['uid'];

        // $uid=200062;
        // $ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
        // get member info
        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status'  => false,
                'message' => 'No member data.',
            ]);
        }

        $eventInfo = $this->setEventInfo($uid);

        return response()->json([
            'status' => true,
            'data'   => $eventInfo,
        ]);

    }

    public function selectCharacter(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'select_character') {//check parameter
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter!',
            ]);
        }
        if ($decoded->has('id') == false) {//check parameter
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter! (2)',
            ]);
        }

        $char_id = (int) $decoded['id'];

        $userData = $this->userData;
        $uid      = $userData['uid'];

        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status'  => false,
                'message' => 'ไม่พบข้อมูล',
            ]);
        }

        if ($member->character_id > 0 || $member->character_id != null) {
            return response()->json([
                'status'  => false,
                'message' => 'ไม่สามารถเปลี่ยนตัวละครได้',
            ]);
        }

        $mychar   = $this->doGetChar($member->uid, $member->ncid);
        $myselect = collect($mychar)->where('char_id', $char_id);

        if (count($myselect) == 0) {
            return response()->json([
                'status'  => false,
                'message' => 'ไม่พบตัวละคร',
            ]);
        }
        // dd($myselect)->first();
        $member->character_id   = (int) $myselect->first()['char_id'];
        $member->character_name = $myselect->first()['char_name'];
        $member->save();

        $this->userEvent = $member;
        $this->userEvent->save();

        $this->summaryUserEvent();

        return [
            'status' => true,
            'data'   => [
                'username'       => $member->username,
                'character_name' => $member->character_name,
                'selectd_char'   => true,
                'characters'     => [],
                'coin'           => $this->getCoin(),
                'itemLists'      => $this->getItemLists(),
            ],
        ];

    }

    public function redeemReward(Request $request)
    {

        $decoded = $request;

        if ($decoded->has('type') == false || $decoded['type'] != 'redeem') {//check parameter
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter! (1)',
            ]);
        }

        $userData = $this->userData;
        $uid      = $userData['uid'];
        // $uid=200062;
        // $ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
        if ($decoded->has('token') == false) {//check parameter
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter! (2)',
            ]);
        }

        $token = (int) $decoded['token'];
        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status'  => false,
                'message' => 'No member data.',
            ]);
        }

        if ($member->character_id == 0) {
            return response()->json([
                'status'  => false,
                'message' => 'โปรดเลือกตัวละครก่อน',
            ]);
        }

        $reward = Reward::whereToken($token)->first();
        if (!$reward) {
            return response()->json([
                'status'  => false,
                'message' => 'ขออภัย<br />ไม่พบไอเทม',
            ]);
        }

        $this->summaryCoin();
        $coin = $this->getCoin();

        if ($coin < $reward->coin) {
            return response()->json([
                'status'  => false,
                'message' => 'ขออภัย<br />เหรียญไม่เพียงพอ',
            ]);
        }


        $goods_data[] = [
            'goods_id'          => $reward->product_id,
            'purchase_quantity' => 1,
            'purchase_amount'   => 0,
            'category_id'       => 40,
        ];


        $sendItemLog = $this->addItemHistoryLog([
            'uid'                => $member->uid,
            'ncid'               => $member->ncid,
            'product_id'         => $reward->product_id,
            'product_title'      => $reward->name,
            'product_quantity'   => 1,
            'coin'               => $reward->coin,
            'log_date'           => (string) date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
            'status'             => 'pending',
            'goods_data'         => json_encode($goods_data),
            'last_ip'            => $this->getIP(),
        ]);


        $member->coin_used = $member->coin_used + $reward->coin;
        $member->save();
        $send_result_raw = $this->dosendItem($member->ncid, $goods_data);
        $send_result     = json_decode($send_result_raw);

        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

            $sendItemLog->send_item_status          = $send_result->status ? $send_result->status : false;
            $sendItemLog->send_item_purchase_id     = $send_result->response->purchase_id ? $send_result->response->purchase_id : 0;
            $sendItemLog->send_item_purchase_status = $send_result->response->purchase_status ? $send_result->response->purchase_status : 0;
            $sendItemLog->status                    = "success";
            $sendItemLog->send_item_respone         = $send_result_raw;
            $sendItemLog->save();

            $this->summaryCoin();

            return response()->json([
                'status'  => true,
                'message' => "ได้รับ ".$reward->name." x1<br />ไอเทมส่งเข้าไปในกล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อรับของรางวัล",
                'data'    =>
                    [
                        'coin'      => $this->getCoin(),
                        'itemLists' => $this->getItemLists(),
                    ],
            ]);

            // $eventInfo = $this->setEventInfo($member->uid);

        } else {
            $sendItemLog->send_item_status          = $send_result->status ? $send_result->status : false;
            $sendItemLog->send_item_purchase_id     = 0;
            $sendItemLog->send_item_purchase_status = 0;
            $sendItemLog->send_item_respone         = $send_result_raw;
            $sendItemLog->status                    = "unsuccess";
            $sendItemLog->save();

            // $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                'status'  => false,
                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                'data'    => [
                    'coin' => $this->getCoin(),
                ],
                // 'data' => $eventInfo
            ]);
        }

    }

    public function getHistory(Request $request)
    {

        $decoded = $request;

        if ($decoded->has('type') == false || $decoded['type'] != 'history') {//check parameter
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter! (1)',
            ]);
        }

        $userData = $this->userData;
        $uid      = $userData['uid'];
        $member   = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status'  => false,
                'message' => 'No member data.',
            ]);
        }

        $history = ItemLog::where('uid', $member->uid)->select('package_title', 'token',
            'created_at_string_time')->orderBy('created_at', 'DESC')->get();

        return response()->json([
            'status'  => true,
            'message' => 'success',
            'data'    => $history,
        ]);
    }

    private function apiCharacters($uid, $ncid)
    {
        return Cache::remember('BNS:BUFFET:CHARACTERS_'.$uid, 10, function () use ($ncid) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'characters',
                'user_id'  => $ncid,
            ]);
        });
    }


    private function apiCheckQuestCompleted($uid, $charId, $questId, $start, $end)
    {
        // $resp = Cache::remember('BNS:BUFFET:QUEST_COMPLETED_' . $questId . '_' . $uid, 10, function()use($charId,$questId,$start,$end) {
        $resp = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $charId,
            'quest_id'   => $questId,
            'start_time' => $start,
            'end_time'   => $end,
        ]);
        // });

        return json_decode($resp);
    }

    private function getItemLists()
    {
        $items = Reward::orderBy('id')
            ->select('name', 'image', 'coin', 'token')
            ->get();

        foreach ($items as $item) {
            if ($this->userEvent) {
                if ($this->userEvent->character_id == 0 || $this->userEvent->character_id == null) {
                    $item->canRedeem = false;
                } else {
                    if ($item->coin <= $this->getCoin()) {
                        $item->canRedeem = true;
                    } else {
                        $item->canRedeem = false;
                    }
                }


            }
        }

        return $items;
    }

    private function getCoin()
    {
        if ($this->userEvent) {
            $coin = ($this->userEvent->coin - $this->userEvent->coin_used);
            if ($coin <= 0){
                return 0;
            }
            return $coin;
        }

        return 0;
    }

    public function test()
    {

        $date   = "2019-10-23";

        if (time() >= strtotime($date.' 00:00:00') && time() <= strtotime($date.' 05:59:59')){
            $start = Carbon::parse($date)->subDay(1)->format("Y-m-d 06:00:00");
            $end   = Carbon::parse($date)->format("Y-m-d 05:59:59");
        }else{
            $start = Carbon::parse($date)->format("Y-m-d 06:00:00");
            $end   = Carbon::parse($date)->addDay(1)->format("Y-m-d 05:59:59");
        }

        $uid     = 87202657;
        $char_id = 188437;

        $quests = collect($this->questList);
        $arr    = [];
        $coin   = 0;
        foreach ($quests as $key => $quest) {

            $key     = $key + 1;
            $var     = "quest_success_".$key;
            $questId = $quest['id'];
            $check = $this->apiCheckQuestCompleted($uid, $char_id, $questId, $start, $end);
            if (is_null($check) == false && is_object($check) && $check->status) {
                $check_k =  $check->response->quest_completed_count;
            } else {
                $check_k = 0;
            }
            if ($check_k > 0) {
                $check_k = 1;
            }
            if ($check_k == 1) {
                $coin += $quest['coin'];
            }
            $arr[$var] = $check_k;
        }
        $arr['coin'] = $coin;
        $arr['start'] = $start;
        $arr['end'] = $end;

        $questLogs = QuestLog::whereUid($uid)->whereCharId($char_id)->whereDateLog($date)->first();

//        dd($arr,$questLogs);

        if ($questLogs){
            $questLogs->update($arr);

            $sum_coin = (int) QuestLog::whereUid($uid)
                ->whereCharId($char_id)
                ->sum('coin');

            $sum_coin_used = (int) SendItemLog::whereUid($uid)
                ->whereStatus('success')
                ->sum('coin');

            $quest = QuestLog::whereUid($uid)
                ->whereCharId($char_id)
                ->get();

            $data = array(
                'quest_success_1'  => (int) $quest->sum('quest_success_1'),
                'quest_success_2'  => (int) $quest->sum('quest_success_2'),
                'quest_success_3'  => (int) $quest->sum('quest_success_3'),
                'quest_success_4'  => (int) $quest->sum('quest_success_4'),
                'quest_success_5'  => (int) $quest->sum('quest_success_5'),
                'quest_success_6'  => (int) $quest->sum('quest_success_6'),
                'quest_success_7'  => (int) $quest->sum('quest_success_7'),
                'quest_success_8'  => (int) $quest->sum('quest_success_8'),
                'quest_success_9'  => (int) $quest->sum('quest_success_9'),
                'quest_success_10' => (int) $quest->sum('quest_success_10'),
                'quest_success_11' => (int) $quest->sum('quest_success_11'),
                'quest_success_12' => (int) $quest->sum('quest_success_12'),
                'quest_success_13' => (int) $quest->sum('quest_success_13'),
                'quest_success_14' => (int) $quest->sum('quest_success_14'),
                'coin' => (int) $sum_coin,
                'coin_used' => (int) $sum_coin_used,
            );

            $member = Member::whereUid($uid) ->whereCharacterId($char_id)->first();
            $member->update($data);
        }else{

            $user = Member::whereUid($uid)->first();
            if ($user){
                $questLogs            = new QuestLog();
                $questLogs->uid       = $user->uid;
                $questLogs->username  = $user->username;
                $questLogs->ncid      = $user->ncid;
                $questLogs->char_id   = $user->character_id;
                $questLogs->char_name = $user->character_name;
                $questLogs->date_log  = $date;
                $questLogs->save();

                $questLogs->update($arr);


                dd("new logs",$questLogs);
            }
        }

        dd("Save",$arr,$questLogs);

    }

    public function summaryUserEvent()
    {
        if ($this->userEvent->character_id != 0) {
            $dates = $this->generateDateRange();
            foreach ($dates as $date) {
                $this->checkQuestByDate($date);
            }

            $this->summaryCoin();
            $this->summaryQuest();
        }


    }

    public function checkQuestByDate($date)
    {
        if (!$this->userEvent->uid) {
            return false;
        }

        $dateLog = $date;

        if (time() >= strtotime($dateLog.' 00:00:00') && time() <= strtotime($dateLog.' 05:59:59')){
            $dateLog = Carbon::parse($dateLog)->subDay(1)->format("Y-m-d");
        }
        //
        $questLogs = QuestLog::whereUid($this->userEvent->uid)
            ->whereCharId($this->userEvent->character_id)
            ->whereDateLog($dateLog)
            ->first();

        if ($questLogs) {
            if ($this->userEvent->updated_at != null){
                $check_update = Carbon::parse($this->userEvent->updated_at)->subDay(2) > Carbon::parse($date);
                if ($check_update === true){
                    return false;
                }
            }
        }

        if (!$questLogs) {
            $questLogs            = new QuestLog();
            $questLogs->uid       = $this->userEvent->uid;
            $questLogs->username  = $this->userEvent->username;
            $questLogs->ncid      = $this->userEvent->ncid;
            $questLogs->char_id   = $this->userEvent->character_id;
            $questLogs->char_name = $this->userEvent->character_name;
            $questLogs->date_log  = $dateLog;
            $questLogs->save();
        }

        $uid = $this->userEvent->uid;

        return Cache::remember('BNS:HALLOWEEN19:USER_'.$uid.':DATE_'.$date, 5,
            function () use ($uid, $date, $questLogs) {

                if (time() >= strtotime($date.' 00:00:00') && time() <= strtotime($date.' 05:59:59')){
                    $start = Carbon::parse($date)->subDay(1)->format("Y-m-d 06:00:00");
                    $end   = Carbon::parse($date)->format("Y-m-d 05:59:59");
                }else{
                    $start = Carbon::parse($date)->format("Y-m-d 06:00:00");
                    $end   = Carbon::parse($date)->addDay(1)->format("Y-m-d 05:59:59");
                }

                $quests = collect($this->questList);
                $arr    = [];
                $coin   = 0;
                foreach ($quests as $key => $quest) {

                    $key     = $key + 1;
                    $var     = "quest_success_".$key;
                    $questId = $quest['id'];
                    $check   = $this->getQuestComplete($questId, $start,$end);
                    if ($check > 0) {
                        $check = 1;
                    }

                    if ($check == 1) {
                        $coin += $quest['coin'];
                    }
                    $arr[$var] = $check;

                }
                $arr['coin'] = $coin;
                $questLogs->update($arr);

                $arr['start_api'] = $start;
                $arr['end_api'] = $end;

                return $arr;
            });


    }

    public function getQuestComplete($questId, $start,$end)
    {
        if ($this->userEvent && $this->userEvent->character_id != 0) {

            $uid     = $this->userEvent->uid;
            $char_id = $this->userEvent->character_id;

            $check = $this->apiCheckQuestCompleted($uid, $char_id, $questId, $start, $end);
            if (is_null($check) == false && is_object($check) && $check->status) {
                return $check->response->quest_completed_count;
            } else {
                return 0;
            }
        }

        return 0;
    }

    private function generateDateRange()
    {
        $start_date = Carbon::parse($this->userEvent->created_at);
        $end_date   = Carbon::today()->addDay(1);
        $dates      = [];

        for ($date = $start_date->copy(); $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
    }

    public function summaryCoin()
    {
        if ($this->userEvent && $this->userEvent->character_id != 0) {

            $sum_coin = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('coin');

            $sum_coin_used = (int) SendItemLog::whereUid($this->userEvent->uid)
                ->whereStatus('success')
                ->sum('coin');

            $this->userEvent->coin      = $sum_coin;
            $this->userEvent->coin_used = $sum_coin_used;
            $this->userEvent->save();
        }
    }


    public function summaryQuest()
    {
        if ($this->userEvent && $this->userEvent->character_id != 0) {

            $quest = QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->get();

            $data = array(
                'quest_success_1'  => (int) $quest->sum('quest_success_1'),
                'quest_success_2'  => (int) $quest->sum('quest_success_2'),
                'quest_success_3'  => (int) $quest->sum('quest_success_3'),
                'quest_success_4'  => (int) $quest->sum('quest_success_4'),
                'quest_success_5'  => (int) $quest->sum('quest_success_5'),
                'quest_success_6'  => (int) $quest->sum('quest_success_6'),
                'quest_success_7'  => (int) $quest->sum('quest_success_7'),
                'quest_success_8'  => (int) $quest->sum('quest_success_8'),
                'quest_success_9'  => (int) $quest->sum('quest_success_9'),
                'quest_success_10' => (int) $quest->sum('quest_success_10'),
                'quest_success_11' => (int) $quest->sum('quest_success_11'),
                'quest_success_12' => (int) $quest->sum('quest_success_12'),
                'quest_success_13' => (int) $quest->sum('quest_success_13'),
                'quest_success_14' => (int) $quest->sum('quest_success_14'),
            );

            $this->userEvent->update($data);
        }
    }

}
