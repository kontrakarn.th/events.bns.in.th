<?php

    namespace App\Http\Controllers\Api\Test\newbie_daily_login;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;
    use App\Models\Test\mystic\Setting;
    use App\Models\Test\newbie_daily_login\Product;
    use App\Models\Test\newbie_daily_login\Users;
    use App\Models\Test\bns_job\BnsJob;
    use App\Models\Test\bns_settings\BnsSettings;
    use App\Models\Test\bns_word\BnsWording;
    use App\Http\Controllers\Api\Test\newbie_daily_login\UsersController;
    use App\Http\Controllers\Api\Test\newbie_daily_login\ProductController;


    use App\Jobs\newbie_daily_login\SendItemToUserQueue;

    class CharactorController extends BnsEventController{

        private $baseApi = 'http://api.apps.garena.in.th';

        public function getNewCharactorbyUidNcid($uid,$ncid){
            $charData = $this->doGetChar($uid, $ncid);
            $content = collect($charData)
            ->filter(function ($obj) {
                return  $obj['creation_time'] >= "2020-07-08 05:00:00";
            })
            ->map(function ($value) {
                return [
                    'id' => $value['id'],
                    'char_name' => $value['char_name'],
                    'char_id' => $value['char_id'],
                    'creation_time' => $value['creation_time'],

                ];
            })
            ->values();
            return $content;
        }
        public function getNewCharactorAndCreationTime($uid,$ncid){
            $charData = $this->doGetChar($uid, $ncid);
            $content = collect($charData)
            ->filter(function ($obj) {
                return  $obj['creation_time'] >= "2020-07-08 05:00:00" && $obj['level'] >= 60 && $obj['mastery_level'] >= 15;
            })
            ->map(function ($value) {
                return [
                    'id' => $value['id'],
                    'char_id' => $value['char_id'],
                    'char_name' => $value['char_name'],
                    'creation_time' => $value['creation_time'],
                ];
            })
            ->values();
            return $content;
        }

        public function CheckCharactorTrue($uid,$ncid,$charid){
            $charData = $this->doGetChar($uid, $ncid);
            $content = collect($charData)
            ->filter(function ($obj) use ($charid) {
                return  $obj['creation_time'] >= "2019-01-16 08:25:14" && $obj['char_id'] == $charid && $obj['level'] >= 60 && $obj['mastery_level'] >= 15;
            })
            ->map(function ($value) {
                return [
                    'id' => $value['id'],
                    'char_id' => $value['char_id'],
                    'char_name' => $value['char_name'],
                    'creation_time' => $value['creation_time'],
                    'level' => $value['level'],
                    'mastery_level' => $value['mastery_level'],
                    'job' => $value['job'],
                    'world_id' => $value['world_id'],
                ];
            })
            ->values();
            foreach($content as $c){
                $member = Users::where('uid',$uid)->first();
                $member->char_id   = $c['char_id'];
                $member->char_name = $c['char_name'];
                $member->level = $c['level'];
                $member->job = $c['job'];
                $member->mastery_level = $c['mastery_level'];
                $member->world_id       = $c['world_id'];
                $member->save();
            break;
            }
            if(count($content) > 0){
                return true;
            }else{
                return false;
            }
        }
        public function getReturnCharactorbyUidNcid($uid,$ncid){
            $charData = $this->doGetChar($uid, $ncid);
            $content = collect($charData)
            ->filter(function ($obj) {
                return $obj['level'] >= 60 && $obj['mastery_level'] >= 15;
            })
            ->map(function ($value) {
                return [
                    'id' => $value['id'],
                    'char_name' => $value['char_name'],
                    'creation_time' => $value['creation_time'],
                ];
            })
            ->values();
            return $content;
        }
        public function getCharactorbyUidNcid($uid,$ncid){
            $charData = $this->doGetChar($uid, $ncid);
            $content = collect($charData)
            ->filter(function ($obj) {
                return $obj['level'] >= 60 && $obj['mastery_level'] >= 15 && $obj['creation_time'] >= "2020-07-08 05:00:00";
            })
            ->map(function ($value) {
                return [
                    'id' => $value['id'],
                    'char_name' => $value['char_name'],
                    'creation_time' => $value['creation_time'],
                ];
            })
            ->values();
            return $content;
        }
        public function getCharactor($request,$userData){
            $this->userscontroller = New UsersController;
            $this->charactorcontroller = New CharactorController;

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'select_character') { //check parameter
                return response()->json([
                    'status' => false,
                    "message" => BnsWording::where('type','invalid_parameter')->value('wording'),
                ]);
            }
            if ($decoded->has('id') == false) { //check parameter
                return response()->json([
                   'status' => false,
                   "message" => BnsWording::where('type','invalid_parameter2')->value('wording'),
                ]);
            }

            $charKey = (int) $decoded['id'];

            $uid = $userData['uid'];

            $member = Users::where('uid', $uid)->first();

            if (isset($member) && empty($member)) {
                return response()->json([
                     'status' => false,
                    "message" => BnsWording::where('type','')->value('wording'),
                ]);
            }

            if ($member->char_id > 0) {
                return response()->json([
                    'status' => false,
                    "message" => BnsWording::where('type','cant_change_charactor')->value('wording'),
                ]);
            }

            $mychar = $this->doGetChar($member->uid, $member->ncid);
            $myselect = collect($mychar)->where('id', $charKey)->first();
            if (count($myselect) == 0) {
                return response()->json([
                    'status' => false,
                    "message" => BnsWording::where('type','no_charactor')->value('wording'),
                ]);
            }
            $jobName = BnsJob::where('job_id',$myselect["job"])->value("job_th_name");
            //$productInday3 = Product::where('specific_job_id',$charKey)->where('date_get_product',3)->get();
            $messagereturn = 'ต้องการเลือกตัวละคร '.$myselect['char_name'].'<br/>';
            $messagereturn .= 'อาชีพ '.$jobName.'<br/>'.'LV.'.$myselect['level'].' ฮงมุนLV.'.$myselect['mastery_level'].' ? <br/>';
            $messagereturn .= '(อาวุธคุนลุน ขั้น 3 จะได้รับตามอาชีพของตัวละครนี้)';
            $messagereturn .= '';
                $content = $this->getCharactorbyUidNcid($member->uid,$member->ncid);
                if($member->user_type == 2){
                    $content = $this->getReturnCharactorbyUidNcid($member->uid,$member->ncid);
                }
           /* if(count($content) < 1){
                return response()->json([
                    'count' => count($content),
                    'status' => true,
                    'can_get_reward' => false,
                    "message" => BnsWording::where('type','no_condition')->value('wording'),
                ]);
            }*/
            return response()->json([
                'status' => true,
                'can_get_reward' => true,
                'message' => $messagereturn,
                "id" =>         $charKey
                ]);


        }
        public function saveCharactor($request,$userData){
            $this->userscontroller = New UsersController;
            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'save_charactor') { //check parameter
                return response()->json([
                    'status' => false,
                    "message" => BnsWording::where('type','invalid_parameter')->value('wording'),
                ]);
            }
            if ($decoded->has('id') == false) { //check parameter
                return response()->json([
                    'status' => false,
                    "message" => BnsWording::where('type','invalid_parameter2')->value('wording'),
                ]);
            }

            $charKey = (int) $decoded['id'];

            $uid = $userData['uid'];

            $member = Users::where('uid', $uid)->first();

            if (isset($member) && empty($member)) {
                return response()->json([
                    'status' => false,
                    "message" => BnsWording::where('type','data_not_found')->value('wording'),
                ]);
            }

            if ($member->char_id > 0) {
                return response()->json([
                    'status' => false,
                    "message" => BnsWording::where('type','cant_change_charactor')->value('wording'),
                ]);
            }

            $mychar = $this->doGetChar($member->uid, $member->ncid);
            $myselect = collect($mychar)->where('id', $charKey)->first();
            if (count($myselect) == 0) {
                return response()->json([
                    'status' => false,
                    "message" => BnsWording::where('type','no_charactor')->value('wording'),
                ]);
            }



            $member->char_id   = (int) $myselect['char_id'];
            $member->char_name = $myselect['char_name'];
            $member->level = $myselect['level'];
            $member->job = $myselect['job'];
            $member->mastery_level = $myselect['mastery_level'];
            $member->world_id       = $myselect['world_id'];
            $member->save();

            //$eventInfo = $this->userscontroller->setEventInfo($uid);

            $this->productcontroller = New ProductController;
             return $this->productcontroller->getItem(1,$userData);

        }
        public function doGetChar($uid, $ncid)
        { //new fixed function
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i       = 1;
                    foreach ($resp->response as $key => $value) {
                        $content[] = [
                            'id' => $i,
                            'char_id'   => $value->id,
                            'char_name' => $value->name,
                            'world_id' => $value->world_id,
                            'job' => $value->job,
                            'level' => $value->level,
                            'creation_time' => $value->creation_time,
                            'last_play_start' => $value->last_play_start,
                            'last_play_end' => $value->last_play_end,
                            'last_ip' => $value->last_ip,
                            'exp' => $value->exp,
                            'mastery_level' => $value->mastery_level,
                        ];

                        $i++;
                    }
                    return $content;
                }
            }
            return null;
        }


        public function apiCharacters($uid, $ncid)
        {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'characters',
                    'user_id'  => $ncid,
                ]);

        }
    }
