<?php

    namespace App\Http\Controllers\Api\Test\bmnov;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DB;

    use App\Http\Controllers\Api\Test\bmnov\RewardController;

    use App\Models\Test\bmnov\ItemHistory;
    use App\Models\Test\bmnov\Member;
    use App\Models\Test\bmnov\DeductLog;
    use App\Models\Test\bmnov\SendItemLog;

    class IndexController extends BnsEventController {

        private $baseApi = 'http://api.apps.garena.in.th';
        private $startTime = '2019-11-07 10:00:00';
        private $endTime = '2019-12-04 23:59:59';

        private $startTimePlay = '2019-11-07 10:00:00';
        private $endTimePlay = '2019-11-27 23:59:59';

        private $awtSecret = 'AgjVHV5uEA7TYsSgNaydHHqL6MdsTkUs';

        private $requireDiamonds = 1;
        private $sendFriendPrice = 1;
        // private $requireDiamonds = 0;
        // private $sendFriendPrice = 0;

        private $memberData;

        public function __construct(Request $request) {
            parent::__construct();

            // init user data
            $this->userData = $this->getJwtUserData($request);

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                return response()->json([
                    'status' => false,
                    'type' => 'end-event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]);
            }

            if($this->checkIsMaintenance()){
                return response()->json([
                                'status' => false,
                                'type' => 'maintenance',
                                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                            ]);
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    return response()->json([
                                    'status' => false,
                                    'type' => 'cant_create_member',
                                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้<br/>กรุณาลองใหม่อีกครั้ง'
                        ]);
                }
            }else{
                return response()->json([
                                'status' => false,
                                'type' => 'not_login',
                                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                            ]);
            }
        }

        private function checkPlayTime(){
            if ((time() < strtotime($this->startTimePlay) || time() > strtotime($this->endTimePlay))) {
                die(json_encode([
                    'status' => false,
                    'message' => 'ไม่สามารถเปิดกาชาปองได้<br />เนื่องจากหมดเวลาแล้ว'
                ]));
            }
        }

        private function getIP() {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn() {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip() {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember() {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP()
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['_id']) == false) {
                    return false;
                }
            }else{
                // $member = Member::where('uid', $uid)->first();
                if(empty($this->memberData->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $this->memberData->_id);
                }
            }
            return true;
        }

        private function hasMember(int $uid) {
            $counter = Member::where('uid', $uid)->first();
            if (isset($counter)) {
                $this->memberData=$counter;
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr) {
            if(Member::create($arr)){
                $member = Member::where('uid', intval($arr['uid']))->first();
                $this->memberData=$member;
                return true;
            }else{
                return false;
            }
        }

        public function updateMember($arr, $_id) {
            if(empty($_id))
                return false;

            return Member::where('_id', $_id)->update($arr);
        }

        private function getNcidByUid(int $uid){
            return Member::where('uid', $uid)->value('ncid');
        }

        private function getUidByNcid(string $ncid) {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:BMNOV:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function associateFullGarenaWithNc(int $uid)
        {
            return Cache::remember('BNS:BMNOV:FULL_ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Sending item from garena events bns bmnov.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return ItemHistory::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemHistory::where('_id', $log_id)->where('uid', $uid)->update($arr);
        }

        private function addSendItemLog($arr) {
            return SendItemLog::create($arr);
        }

        private function updateSendItemLog($arr, $log_id, $uid) {
            return SendItemLog::where('_id', $log_id)->where('uid', $uid)->update($arr);
        }

        private function diamondBalance(int $uid) {
            return $this->post_api($this->baseApi, [
                        'key_name' => 'bns',
                        'service' => 'balance',
                        'uid' => $uid,
            ]);
        }

        private function setDiamondDeductData($deduct_diamond_amount=0)
        {
            // if($deduct_diamond_amount==0)
            //     return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamonds for garena bns bmnov',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addDeductLog(array $arr) {
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductLog($arr, $log_id) {
            if ($arr) {
                return DeductLog::where('_id', $log_id)->update($arr);
            }
            return null;
        }

        private function doRandomItem() {
            $rewarder = new RewardController();
            // $resp = [];
            $resp[] = $rewarder->getRandomRewardInfo();


            return $resp;
        }

        public function checkin(Request $request) // checkin event
        {

            $decoded=$request->input();
            if(isset($decoded['type']) == false || $decoded['type'] != 'event_info'){
                return response()->json(['status' => false, 'message' => 'Invalid parameter! (1)']);
            }

            $uid = $this->userData['uid'];
            $nickname=$this->userData['nickname'];

            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);
            if($diamondsBalance->balance<$this->requireDiamonds || time() < strtotime($this->startTimePlay) || time() > strtotime($this->endTimePlay)){
                $can_play=false;
            }else{
                $can_play=true;
            }

            return response()->json([
                'status'=>true,
                'message'=>'success',
                'data'=>[
                    'uid'=>$uid,
                    'nickname'=>$nickname,
                    'can_play'=>$can_play
                ]
            ]);
        }

        public function randomItem(Request $request){
            $this->checkPlayTime();
            // dd( $this->doRandomItem());
            $decoded=$request->input();
            if(isset($decoded['type']) == false || $decoded['type'] != 'randomitem'){
                return response()->json(['status' => false, 'message' => 'Invalid parameter! (1)']);
            }

            if($this->isLoggedIn()==false){
                return response()->json(['status' => false, 'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน']);
            }

            $uid = $this->memberData->uid;
            $ncid = $this->memberData->ncid;

            $required_deduct_diamond = $this->requireDiamonds;

            // get diamond balance
            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถอัพเดทข้อมูลDiamondsของคุณได้<br/>กรุณาลองใหม่อีกครั้ง'
                        ]);
            }else{

                $before_diamond = intval($diamondsBalance->balance);
                // compare user diamonds balance with gachapon's require diamomds.
                if($before_diamond < $required_deduct_diamond){
                    return response()->json([
                                'status' => false,
                                'message' => 'จำนวนDiamondsของคุณ<br/>ไม่พอในการซื้อ'
                    ]);
                }else{

                    $logDeduct = $this->addDeductLog([
                        'status' => 'pending',
                        'diamond' => $required_deduct_diamond,
                        'uid' => $uid,
                        'ncid' => $ncid,
                        'last_ip' => $this->getIP(),
                        'deduct_type' => 'gachapon',
                        'log_timestamp' => time()
                    ]);

                    // set deduct diamonds
                    $deductData = $this->setDiamondDeductData($required_deduct_diamond);
                    if($deductData == false){
                        return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง']);
                    }

                    $respDeductRaw = $this->deductDiamond($ncid, $deductData);
                    $respDeduct = json_decode($respDeductRaw);

                    if(is_object($respDeduct) && is_null($respDeduct) == false){

                        $arrDeductLog = [
                            'before_deduct_diamond' => $before_diamond,
                            'after_deduct_diamond' => $before_diamond - $required_deduct_diamond,
                            'deduct_status' => $respDeduct->status,
                            'deduct_purchase_id' => isset($respDeduct->response->purchase_id) ? $respDeduct->response->purchase_id : 0,
                            'deduct_purchase_status' => isset($respDeduct->response->purchase_status) ? $respDeduct->response->purchase_status : 0,
                            'deduct_data' => json_encode($deductData),
                            'status' => 'pending'
                        ];

                        if($respDeduct->status){

                            $arrDeductLog['status'] = 'success';
                            $this->updateDeductLog($arrDeductLog, $logDeduct['_id']);

                            $randResult = $this->doRandomItem(); //do getRandomCode
                            $goodsData = []; //good data for send item group

                            foreach ($randResult as $key => $value) {//loop for add item log
                                if($value['stock']==false){
                                    //for send item
                                    $goodsData[] = [
                                        'goods_id' => $value['product_id'],
                                        'purchase_quantity' => $value['product_quantity'],
                                        'purchase_amount' => 0,
                                        'category_id' => 40
                                    ];

                                }
                                $arrItemHistory = [
                                    'item_id' => 0,
                                    'product_id' => 0,
                                    'product_title' => '',
                                    'product_quantity' => 0,
                                    'uid' => $uid,
                                    'ncid' => $ncid,
                                    'last_ip' => $this->getIP(),
                                    'icon' => '',
                                    'log_timestamp' => time(),
                                    'log_time_string' => Carbon::now()->format('d/m/Y H:i:s')
                                ];

                                $arrItemHistory['item_id'] = $value['id'];
                                $arrItemHistory['product_id'] = $value['product_id'];
                                $arrItemHistory['product_title'] = $value['product_title'];
                                $arrItemHistory['product_quantity'] = $value['product_quantity'];
                                $arrItemHistory['icon'] = $value['icon'];
                                if($value['stock']==false){
                                    $arrItemHistory['status'] = 'sended';
                                    $arrItemHistory['send_type'] = 'owner';
                                }else{
                                    $arrItemHistory['status'] = 'pending';
                                    $arrItemHistory['send_type'] = 'none';
                                }
                                $arrItemHistory['send_to'] = "";
                                $arrItemHistory['send_status'] = 'pending';
                                $arrItemHistory['item_type'] = $value['item_type'];



                                $log_history_id=$this->addItemHistoryLog($arrItemHistory);


                            }


                            // send items
                            if (is_null($goodsData) == false && is_array($goodsData) && count($goodsData) > 0) {
                                // do add send item log
                                $logSendItemResult = $this->addSendItemLog([
                                    'uid' => $uid,
                                    'ncid' => $ncid,
                                    'log_id'=>$log_history_id['_id'],
                                    'status' => 'pending',
                                    'send_item_status' => false,
                                    'send_item_purchase_id' => 0,
                                    'send_item_purchase_status' => 0,
                                    'goods_data' => json_encode($goodsData),
                                    'last_ip' => $this->getIP(),
                                    'log_timestamp' => time()
                                ]);

                                //send item
                                $sendResultRaw = $this->dosendItem($ncid, $goodsData);
                                $sendResult = json_decode($sendResultRaw);
                                if (is_null($sendResult) == false && is_object($sendResult) && $sendResult->status) {
                                    //recive log id
                                    $log_id = $logSendItemResult['_id'];
                                    //do update send item log
                                    $this->updateSendItemLog([
                                        'send_item_status' => $sendResult->status ? true : false,
                                        'send_item_purchase_id' => $sendResult->response->purchase_id ?: 0,
                                        'send_item_purchase_status' => $sendResult->response->purchase_status ?: 0,
                                        'status' => 'success'
                                    ], $log_id, $uid);

                                    $this->updateItemHistoryLog([
                                        'send_status' => $sendResult->status ? 'success' : 'unsuccess',
                                    ], $log_history_id['_id'], $uid);

                                        $listdata=[];
                                        $rewarder = new RewardController();
                                        $itemlist=$rewarder->getSpecialRewardsList();
                                        $numitem=ItemHistory::where('uid', $uid)
                                                ->where('status', 'pending')
                                                ->where('product_id', $itemlist[9]['product_id'])
                                                ->count();
                                        $arr=[
                                            'id'=>$itemlist[9]['id'],
                                            'product_title'=>$itemlist[9]['product_title'],
                                            'icon'=>$itemlist[9]['icon'],
                                            'key'=>$itemlist[9]['key'],
                                            'amt'=>$numitem
                                        ];
                                        array_push($listdata,$arr);

                                } else {
                                    //do update send item log
                                    $log_id = $logSendItemResult['_id'];
                                    $this->updateSendItemLog([
                                        'send_item_status' => isset($sendResult->status) ? $sendResult->status : false,
                                        'send_item_purchase_id' => isset($sendResult->response->purchase_id) ? $sendResult->response->purchase_id : 0,
                                        'send_item_purchase_status' => isset($sendResult->response->purchase_status) ? $sendResult->response->purchase_status : 0,
                                        'status' => 'unsuccess'
                                    ], $log_id, $uid);

                                    $this->updateItemHistoryLog([
                                        'send_status' => 'unsuccess',
                                    ], $log_history_id['_id'], $uid);

                                    //error unsuccess log
                                    return response()->json([
                                                'status' => false,
                                                'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า (1)'
                                    ]);
                                }
                            }

                            $content = [];
                            //convert content result
                            foreach ($randResult as $key => $value) {
                                $collection = collect($value);
                                $filtered = $collection->only(['id','product_title', 'item_type', 'icon', 'key']); //except
                                $content[] = $filtered->all();
                            }

                            $diamondsBalance = $before_diamond-$required_deduct_diamond;

                            if($diamondsBalance<$this->requireDiamonds){
                                $can_play=false;
                            }else{
                                $can_play=true;
                            }


                            //do return success log
                            return response()->json([
                                'status' => true,
                                'message' => 'success',
                                'content' => $content,
                                'data'=>[
                                    'can_play'=>$can_play
                                ]
                            ]);

                        }else{
                            $arrDeductLog['status'] = 'unsuccess';
                            $this->updateDeductLog($arrDeductLog, $logDeduct['_id']);
                        }
                    }else {
                        $arrDeductLog['status'] = 'unsuccess';
                        $this->updateDeductLog($arrDeductLog, $logDeduct['_id']);
                    }

                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้้<br/> กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);

                }

            }

        }

        public function getExchangeInfo(Request $request){
            if($this->isLoggedIn()==false){
                return response()->json(['status' => false, 'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน']);
            }

            $decoded=$request->input();

            if($decoded['type']!='getexchangeinfo'){
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (2) กรุณาลองใหม่อีกครั้ง']);
            }

            $uid = $this->userData['uid'];
            $listdata=[];
            $rewarder = new RewardController();
            $itemlist=$rewarder->getSpecialRewardsList();
            // foreach($itemlist as $key=>$value){
                $numitem=ItemHistory::where('uid', $uid)
                        ->where('status', 'pending')
                        ->where('product_id', $itemlist[9]['product_id'])
                        ->count();
                $arr=[
                    'id'=>$itemlist[9]['id'],
                    'product_title'=>$itemlist[9]['product_title'],
                    'icon'=>$itemlist[9]['icon'],
                    'key'=>$itemlist[9]['key'],
                    'amt'=>$numitem
                ];
                array_push($listdata,$arr);
            // }

            return response()->json([
                        'status' => true,
                        'content' => $listdata
            ]);
        }

        public function getName(Request $request){
            if($this->isLoggedIn()==false){
                return response()->json(['status' => false, 'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน']);
            }

            $decoded=$request->input();

            if($decoded['type']!='getname'){
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (2) กรุณาลองใหม่อีกครั้ง']);
            }

            if(!isset($decoded['sendto'])){
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (3) กรุณาลองใหม่อีกครั้ง']);
            }

            $sendto=(int) $decoded['sendto'];
            $uid= (int) $this->userData['uid'];
            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                        ]);
            }

            if ($uid == $sendto) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งไอเทมให้<br /> UID ของตัวเองได้'
                ]);
            }

            $chkUid=$this->associateFullGarenaWithNc($sendto);
            if(empty($chkUid)){
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่พบข้อมูลผู้ที่ต้องการส่งของ'
                ]);
            }

            return response()->json([
                        'status' => true,
                        'message'=>'success',
                        'content' => [
                            'target_uid'=>$sendto,
                            'target_username'=>$chkUid["user_name"]
                        ],
            ]);
        }

        public function exchangeItem(Request $request){

            if($this->isLoggedIn()==false){
                return response()->json(['status' => false, 'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน']);
            }

            $decoded=$request->input();

            if($decoded['type']!='exchangeitem'){
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }

            $item_id= 10;

            $uid = $this->userData['uid'];
            $ncid = $this->memberData->ncid;

            if(!isset($decoded['sendto'])){
                $sendto="";
                $sendto=$uid;
                $sendtype="owner";
            }else{
                $sendtype="send_to_other";
                $sendto=(int) $decoded['sendto'];

                $chkUid=$this->associateGarenaWithNc($sendto,"");
                if(empty($chkUid)){
                    return response()->json(['status' => false, 'message' => 'ไม่พบข้อมูลผู้ที่ต้องการส่งของ']);
                }
            }

            $chkItem=ItemHistory::where('item_id',$item_id)->where('uid',$uid)->where('status','pending')->first();
            if(!isset($chkItem)){
                return response()->json(['status' => false, 'message' => 'คุณไม่มีไอเท็มที่ต้องการส่ง']);
            }
            $history_id=$chkItem->id;

            $uid_for_send=$uid;
            $ncid_for_send=$ncid;

            if($sendtype=="send_to_other"){
                //deduct diamond for exchanges
                $diamondsBalanceRaw = $this->diamondBalance($uid);
                $diamondsBalance = json_decode($diamondsBalanceRaw);

                if ($diamondsBalance->status == false) {
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถอัพเดทข้อมูล Diamonds ของคุณได้<br/>กรุณาลองใหม่อีกครั้ง'
                            ]);
                }

                $before_diamond = intval($diamondsBalance->balance);
                // set desuct diamonds
                $required_deduct_diamond = $this->sendFriendPrice;
                if($before_diamond < $required_deduct_diamond){
                    return response()->json([
                                'status' => false,
                                'message' => 'จำนวน Diamonds ของคุณ<br/>ไม่พอในการส่งไอเท็ม'
                    ]);
                }
                $logDeduct = $this->addDeductLog([
                    'status' => 'pending',
                    'diamond' => $required_deduct_diamond,
                    'uid' => $uid,
                    'send_to' => $sendto,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                    'deduct_type' => 'exchange',
                    'log_timestamp' => time()
                ]);

                $deductData = $this->setDiamondDeductData($required_deduct_diamond);
                if($deductData == false){
                    return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง']);
                }

                $respDeductRaw = $this->deductDiamond($ncid, $deductData);
                $respDeduct = json_decode($respDeductRaw);

                if(is_object($respDeduct) && is_null($respDeduct) == false){

                    $arrDeductLog = [
                        'before_deduct_diamond' => $before_diamond,
                        'after_deduct_diamond' => $before_diamond - $required_deduct_diamond,
                        'deduct_status' => $respDeduct->status,
                        'deduct_purchase_id' => isset($respDeduct->response->purchase_id) ? $respDeduct->response->purchase_id : 0,
                        'deduct_purchase_status' => isset($respDeduct->response->purchase_status) ? $respDeduct->response->purchase_status : 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'pending'
                    ];

                    if($respDeduct->status){

                        $arrDeductLog['status'] = 'success';
                        $this->updateDeductLog($arrDeductLog, $logDeduct['_id']);

                        $uid_for_send=$sendto;
                        $ncid_for_send=$chkUid;
                    }else{
                        $arrDeductLog['status'] = 'fail';
                        $this->updateDeductLog($arrDeductLog, $logDeduct['_id']);
                        return response()->json([
                                    'status' => false,
                                    'message' => 'ไม่สามารถหักไดมอนด์ได้<br/>โปรดลองใหม่อีกครั้ง'
                                ]);
                    }
                }else{
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถหักไดมอนด์ได้้<br/>โปรดลองใหม่อีกครั้ง'
                            ]);
                }

            }

            if($sendtype=="send_to_other"){
                $chkItem->send_to=$sendto;
            }

            $chkItem->status="sended";
            $chkItem->send_type=$sendtype;
            if(!$chkItem->save()){
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด ลองใหม่อีกครั้ง']);
            }

            $goodsData[] = [
                'goods_id' => $chkItem->product_id,
                'purchase_quantity' => $chkItem->product_quantity,
                'purchase_amount' => 0,
                'category_id' => 40
            ];
            // send items
            if (is_null($goodsData) == false && is_array($goodsData) && count($goodsData) > 0) {
                // do add send item log
                $logSendItemResult = $this->addSendItemLog([
                    'uid' => $uid_for_send,
                    'ncid' => $ncid_for_send,
                    'log_id'=>$chkItem->id,
                    'status' => 'pending',
                    'send_item_status' => false,
                    'send_item_purchase_id' => 0,
                    'send_item_purchase_status' => 0,
                    'goods_data' => json_encode($goodsData),
                    'last_ip' => $this->getIP(),
                    'log_timestamp' => time()
                ]);

                //send item
                $sendResultRaw = $this->dosendItem($ncid_for_send, $goodsData);
                $sendResult = json_decode($sendResultRaw);
                $historyItem=ItemHistory::find($history_id);
                if (is_null($sendResult) == false && is_object($sendResult) && $sendResult->status) {
                    //recive log id
                    $log_id = $logSendItemResult['_id'];
                    //do update send item log
                    $this->updateSendItemLog([
                        'send_item_status' => $sendResult->status ?: false,
                        'send_item_purchase_id' => $sendResult->response->purchase_id ?: 0,
                        'send_item_purchase_status' => $sendResult->response->purchase_status ?: 0,
                        'status' => 'success'
                            ], $log_id, $uid_for_send);
                    $this->updateItemHistoryLog([
                                            'send_status' => 'success',
                                        ], $historyItem->id, $uid);

                        $listdata=[];
                        $rewarder = new RewardController();
                        $itemlist=$rewarder->getSpecialRewardsList();
                        // foreach($itemlist as $key=>$value){
                        $numitem=ItemHistory::where('uid', $uid)
                                ->where('status', 'pending')
                                ->where('product_id', $itemlist[9]['product_id'])
                                ->count();
                        $arr=[
                            'id'=>$itemlist[9]['id'],
                            'product_title'=>$itemlist[9]['product_title'],
                            'icon'=>$itemlist[9]['icon'],
                            'key'=>$itemlist[9]['key'],
                            'amt'=>$numitem
                        ];
                        array_push($listdata,$arr);
                        // }

                        if($sendtype=="send_to_other"){
                            $diamond_cal = $before_diamond-$required_deduct_diamond;

                            if($diamond_cal<$required_deduct_diamond){
                                $can_play=false;
                            }else{
                                $can_play=true;
                            }

                            return response()->json([
                                        'status' => true,
                                        'message' => 'ทำรายการสำเร็จ',
                                        'content'=>$listdata,
                                        'data'=>[
                                            'can_play'=>$can_play
                                        ]
                            ]);
                        }else{
                            return response()->json([
                                        'status' => true,
                                        'message' => 'ทำรายการสำเร็จ',
                                        'content'=>$listdata
                            ]);
                        }


                } else {
                    //do update send item log
                    $log_id = $logSendItemResult['_id'];
                    $this->updateSendItemLog([
                        'send_item_status' => isset($sendResult->status) ? $sendResult->status : false,
                        'send_item_purchase_id' => isset($sendResult->response->purchase_id) ? $sendResult->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($sendResult->response->purchase_status) ? $sendResult->response->purchase_status : 0,
                        'status' => 'unsuccess'
                            ], $log_id, $uid_for_send);
                    $this->updateItemHistoryLog([
                                            'send_status' => 'unsuccess',
                                        ], $historyItem->id, $uid);
                    //error unsuccess log
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }
            }else{
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }

        }

        public function historyList(Request $request){
            if($this->isLoggedIn()==false){
                return response()->json(['status' => false, 'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน']);
            }

            $decoded=$request->input();

            if($decoded['type']!='historylist'){
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }

            $data=ItemHistory::where('uid',$this->memberData->uid)->select('send_type','send_to','product_title','updated_at')->orderBy('updated_at','DESC')->get();

            return response()->json([
                        'status' => true,
                        'message' => 'success',
                        'content'=>$data
            ]);

        }

    }
