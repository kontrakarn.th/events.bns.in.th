<?php
namespace App\Http\Controllers\Api\Test\pvp_pk;

Class StdCode {
    const CHARACTER_CLASSES = [
        [
            'no' => 1,
            'en_desc' => 'Blade Master'
        ],
        [
            'no' => 2,
            'en_desc' => 'Kungfu Master'
        ],
        [
            'no' => 6,
            'en_desc' => 'Summonner'
        ],
        [
            'no' => 3,
            'en_desc' => 'Force Master'
        ],
        [
            'no' => 5,
            'en_desc' => 'Destroyer'
        ],
        [
            'no' => 7,
            'en_desc' => 'Assassin'
        ],
        [
            'no' => 8,
            'en_desc' => 'Blade Dancer'
        ],
        [
            'no' => 9,
            'en_desc' => 'Warlock'
        ],
        [
            'no' => 4,
            'en_desc' => 'Soul Gunner'
        ],
        [
            'no' => 10,
            'en_desc' => 'Soul Fighter'
        ],
        [
            'no' => 11,
            'en_desc' => 'Warden'
        ],
        [
            'no' => 12,
            'en_desc' => 'Archer'
        ],
    ];
    public static function getCharacterClassByNumber(int $no):?array{
        foreach(self::CHARACTER_CLASSES as $class){
            if($class['no'] == $no){
                return $class;
            }
        }
        return null;
    }
}
