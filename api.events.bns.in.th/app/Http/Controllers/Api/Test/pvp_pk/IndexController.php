<?php

namespace App\Http\Controllers\Api\Test\pvp_pk;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Collection;
use Exception;

// Model
use App\Models\Test\pvp_pk\Setting;
use App\Models\Test\pvp_pk\User;
use App\Models\Test\pvp_pk\Item;
use App\Models\Test\pvp_pk\ItemLog;
use App\Models\Test\pvp_pk\DeductLog;

// Services
use \App\Http\Controllers\Api\Test\pvp_pk\Services;
// Helper
use \App\Http\Controllers\Api\Test\pvp_pk\Helper;
use \App\Http\Controllers\Api\Test\pvp_pk\StdCode;

class IndexController extends BnsEventController
{
    const CHARACTER_CACHE_KEY   = 'BNS:PVP_PK:TEST:CHARACTORS_';
    // set this to 1 on dev
    // set this to 150000 on live
    const PACKAGE_COST          = 1;
    const DEDUCT_API_DESC       = 'Deduct diamomds for garena events BNS PVP PACKAGE.';
    const SENDITEM_API_DESC     = 'Sending item from garena events BNS PVP PACKAGE.';
    public function __construct()
    {
        parent::__construct();
        $this->checkEvent();
    }
    public function getInfo(): array
    {
        $data_return = $this->getUserInfo();
        if ($data_return !== null) {
            return Helper::responseData(true, Helper::MESSAGE_CODE_OK, $this->buildUserResponseData($data_return));
        } else {
            return Helper::responseData(false, Helper::MESSAGE_CODE_FAIL, []);
        }
    }
    public function buildUserResponseData(){
        $data_return = $this->getUserInfo();
        if($data_return == null){
            return null;
        }
        $data_set['uid']        = $data_return['info']['uid'];
        $data_set['username']        = $data_return['info']['username'];
        $data_set['can_buy']        = $data_return['can_buy'];
        $data_set['balance']        = $data_return['balance'];
        $data_set['package_cost']   = $data_return['package_cost'];
        $data_set['characters']     = [];
        foreach($data_return['characters'] as $char){
            $data_set['characters'][] = [
                'character_id'=>$char['id'],
                'character_name' => $char['name'],
                'character_class' => $char['job'],
                'character_class_desc' => StdCode::getCharacterClassByNumber($char['job'])['en_desc'],
            ];
        }
        return $data_set;
    }
    public function buyPackage()
    {
        $setting = Setting::getSetting();
        if(!isset($setting['is_senditem']) || $setting['is_senditem'] == 'N'){
            Helper::responseDataAndExit(false, Helper::MESSAGE_CODE_EVENT_BUY_PACKAGE_MAINTENANCE, []);
        }
        $request                = request();
        $info                   = $this->getUserInfo();
        $user                   = $info['info'];
        $selected_character_id  = null;

        // character_id request parameter is not found.
        if (!$request->has('character_id') || $request['character_id'] == '') { //check parameter
            return Helper::responseData(false, Helper::MESSAGE_CODE_FAIL, []);
        } else {
            // check character is belong to user
            $selected_character_id = $request['character_id'];
            $characters = Services::getCharacters($user->ncid);
            $character  = collect($characters)->filter(function ($value, $key) use ($selected_character_id) {
                return $value["id"] == $selected_character_id;
            })->first();
            if ($character == null) {
                return Helper::responseData(false, Helper::MESSAGE_CODE_CHARACTER_NOT_FOUND, []);
            }
        }
        if ($info['can_buy'] !== true) {
            return Helper::responseData(false, Helper::MESSAGE_CODE_BUY_PACKAGE_ALREADY, []);
        }
        // check diamond balance
        if ($info['balance'] < self::PACKAGE_COST) {
            return Helper::responseData(false, Helper::MESSAGE_CODE_DIAMOND_NOT_ENOUGH, []);
        }
        // หัก diamond
        $dstatus = $this->deductDiamond($user, $info['balance']);
        if ($dstatus === false) {
            return Helper::responseData(false, Helper::MESSAGE_CODE_DIAMOND_SERVICE_FAIL, []);
        }
        $istatus = $this->sendItem($user, $character);
        if ($istatus === false) {
            return Helper::responseData(false, Helper::MESSAGE_CODE_ITEM_SERVICE_FAIL, []);
        }
        return Helper::responseData(true, Helper::MESSAGE_CODE_BUY_PACKAGE_OK, $this->buildUserResponseData($this->getUserInfo()));
    }
    public function getHistory(){
        $info = $this->getUserInfo();
        $user = $info['info'];
        // check character is belong to user
        $selected_character_id = $user->character_id;
        $characters = Services::getCharacters($user->ncid);
        $character  = collect($characters)->filter(function ($value, $key) use ($selected_character_id) {
            return $value["id"] == $selected_character_id;
        })->first();
        $return_items    = [];
        $select_character = [];
        if($character != null){
            $item_logs      = ItemLog::where('uid',$user->uid)->get();
            $tmp_items      = $this->getItems($character['job']);
            $items          = [];
            foreach($tmp_items as $v){
                $items[$v->product_id] = $v;
            }
            foreach($item_logs as $item_log){
                if(!isset($items[$item_log->product_id])){
                    continue;
                }
                $return_items[] = [
                    'item_name' => $items[$item_log['product_id']]['item_name'],
                    'qty'       => $items[$item_log['product_id']]['qty'],
                ];
            }
            $select_character = [
                'character_id'=>$character['id'],
                'character_name'=>$character['name'],
                'character_class'=>$character['job'],
                'character_class_desc'=> StdCode::getCharacterClassByNumber($character['job'])['en_desc']
            ];
            $message_code = Helper::MESSAGE_CODE_OK;
            $status = true;
        }else{
            $message_code = Helper::MESSAGE_CODE_BUY_PACKAGE_YET;
            $status = true;
        }
        return Helper::responseData($status, $message_code, [
            "select_character"=> $select_character,
            "items" => $return_items,
            "info" => $this->buildUserResponseData($info)
        ]);
    }
    private function sendItem(User &$user, array $character): bool
    {
        $user = $user->refresh();
        if ($user->istatus_flag != User::STATUS_FLAG_NONE) {
            return false;
        }
        $logs = [];
        $sendingItems = [];
        $items = $this->getItems($character['job']);
        if ($items->count() < 1) {
            return false;
        }
        $user->istatus_flag = User::STATUS_FLAG_PENDING;
        foreach ($items as $item) {
            $log = new ItemLog();
            $log->product_id = $item->product_id;
            $log->ncid = $user->ncid;
            $log->uid = $user->uid;
            $log->ip = Helper::getClientIP();
            $log->user_agent = Helper::getUserAgent();
            $log->character_class = (int)$character['job'];
            $log->ts = date("Y-m-d H:i:s");
            $log->resp_text = '';
            $log->istatus_flag = ItemLog::STATUS_FLAG_FAIL;
            $log->purchase_id = null;
            $log->purchase_status = null;
            $log->qty = $item->qty;
            $log->amt = 0;
            $logs[] = $log;
            $sendingItems[] = [
                'goods_id' => $item->product_id,
                'purchase_quantity' => $item->qty,
                'purchase_amount' => 0,
                'category_id' => 40,
            ];
        }
        // execute api
        $return_status = false;
        $resp = Services::sendItem($user->ncid, $sendingItems, self::SENDITEM_API_DESC);
        if ($resp['status'] !== false) {
            $log_istatus_flag           = ItemLog::STATUS_FLAG_OK;
            $user->istatus_flag         = User::STATUS_FLAG_OK;
            $return_status = true;
        } else {
            $log_istatus_flag           = ItemLog::STATUS_FLAG_FAIL;
            $user->istatus_flag         = User::STATUS_FLAG_NONE;
        }
        $user->character_id     = $character['id'];
        $user->character_class  = $character['job'];
        $user->character_name   = $character['name'];
        $user->save();
        foreach ($logs as $log) {
            $log->resp_text         = $resp['raw'];
            $log->istatus_flag      = $log_istatus_flag;
            $log->purchase_id       = $resp['purchase_id'];
            $log->purchase_status   = $resp['purchase_status'];
            $log->save();
        }
        return $return_status;
    }
    private function deductDiamond(User &$user, int $balance): bool
    {
        $user = $user->refresh();
        if ($user->dstatus_flag != User::STATUS_FLAG_NONE) {
            return false;
        }
        $user->dstatus_flag = User::STATUS_FLAG_PENDING;
        $user->save();

        $log = new DeductLog();
        $log->ncid = $user->ncid;
        $log->uid = $user->uid;
        $log->username = $user->username;
        $log->diamond_use = self::PACKAGE_COST;
        $log->diamond_balance = $balance;
        $log->ip = Helper::getClientIP();
        $log->user_agent = Helper::getUserAgent();
        $log->ts = date("Y-m-d H:i:s");
        $log->dstatus_flag = DeductLog::STATUS_FLAG_FAIL;

        $log->purchase_id       = null;
        $log->purchase_status   = null;
        $resp = Services::deductDiamond($user->ncid, self::PACKAGE_COST, self::DEDUCT_API_DESC);
        if ($resp['status'] !== false) {
            $log->dstatus_flag = DeductLog::STATUS_FLAG_OK;
            $log->resp_text = $resp['raw'];
            $log->purchase_id       = $resp['purchase_id'];
            $log->purchase_status   = $resp['purchase_status'];
            $log->save();
            $user->dstatus_flag = User::STATUS_FLAG_OK;
            $user->save();
            return true;
        } else {
            $log->dstatus_flag = DeductLog::STATUS_FLAG_FAIL;
            $log->resp_text = $resp['raw'];
            $log->purchase_id       = isset($resp['purchase_id']) ? $resp['purchase_id'] : null;
            $log->purchase_status   = isset($resp['purchase_status']) ? $resp['purchase_status'] : null;
            $log->save();

            $user->dstatus_flag = User::STATUS_FLAG_NONE;
            $user->save();
            return false;
        }
    }
    private function getUserInfo(): ?array
    {
        $data_return = [
            'can_buy'       => false,
            'balance'       => 0,
            'package_cost'  => self::PACKAGE_COST,
            'info'          => [],
            'characters'    => [],
        ];
        try {
            $user = $this->getOrCreateUser();
            $data_return['info']        = $user;
            $data_return['characters']  = Services::getCharacters($user->ncid, self::CHARACTER_CACHE_KEY);
            $data_return['balance']     = Services::getDiamondBalance($user->uid);
            $data_return['can_buy']     = self::PACKAGE_COST <= $data_return['balance'] && $user->istatus_flag == User::STATUS_FLAG_NONE && $user->dstatus_flag == User::STATUS_FLAG_NONE ? true : false;
            return $data_return;
        } catch (Exception $e) {
            $message_code = $e->getMessage();
            if ($message_code = 'Signature verification failed') {
                $message_code = Helper::MESSAGE_CODE_INVALID_JWT;
            }
            if (isset(Helper::MESSAGE_DESC[$message_code])) {
                Helper::responseDataAndExit(false, $message_code, []);
            }
            return null;
        }
    }
    private function getOrCreateUser(): ?User
    {
        $openData = $this->getJwtUserData(request());
        if ( !isset($openData["uid"]) || $openData["uid"] == '') {
            throw new Exception(Helper::MESSAGE_CODE_INVALID_JWT);
        }
        $user       = User::where("uid", $openData["uid"])->first();
        if ($user != null && $user->count() > 0) {
            return $user;
        } else {
            $ncid = Services::getNcID($openData["uid"]);
            if ($ncid == '') {
                throw new Exception(Helper::MESSAGE_CODE_USER_NOT_FOUND);
            }
            $user = new User();
            $user->uid                  = $openData['uid'];
            $user->username             = $openData['username'];
            $user->ncid                 = $ncid;
            $user->character_id         = null;
            $user->character_name       = '';
            $user->character_class      = null;
            $user->dstatus_flag         = User::STATUS_FLAG_NONE;
            $user->istatus_flag         = User::STATUS_FLAG_NONE;
            if ($user->save()) {
                return $user->refresh();
            } else {
                throw new Exception(Helper::MESSAGE_CODE_INTERNAL_ERROR);
            }
        }
        return null;
    }
    private function getItems(string $class_no): Collection
    {
        $p = Item::where(function ($query) use ($class_no) {
            $query->where([
                ['item_type', '=', 'CLASS'],
                ['class_no', '=', $class_no]
            ])
                ->orWhere('item_type', '=', "GENERAL");
        });
        return $p->get();
    }
    private function checkEvent()
    {
        if ($this->isIngame() == false && Helper::isIPAccept() == false) {
            Helper::responseDataAndExit(false, Helper::MESSAGE_CODE_PERMISSION_DENY, []);
        }
        if ($this->checkIsMaintenance()) {
            Helper::responseDataAndExit(false, Helper::MESSAGE_CODE_MAINTENANCE, []);
        }
        $setting = Setting::getSetting();
        // ไม่อยู่ในช่วงเวลากิจกรรม
        if (!(time() >= strtotime($setting->event_ts1) && time() <= strtotime($setting->event_ts2))) {
            Helper::responseDataAndExit(false, Helper::MESSAGE_CODE_EVENT_NOT_OPEN, []);
        }
        if($setting['is_mt'] == 'Y'){
            Helper::responseDataAndExit(false, Helper::MESSAGE_CODE_EVENT_MAINTENANCE, []);
        }
    }
}
