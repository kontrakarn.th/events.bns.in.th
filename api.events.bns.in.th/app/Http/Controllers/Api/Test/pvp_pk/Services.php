<?php

namespace App\Http\Controllers\Api\Test\pvp_pk;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;

class Services extends BnsEventController
{
    const BASE_API = 'http://api.apps.garena.in.th';
    const DEFAULT_CACHE_CHARACTER_KEY = 'BNS:PVP_PK:TEST:CHARACTORS_';
    const DEFAULT_CACHE_NCID_KEY = 'BNS:PVP_PK:TEST:NCID_';
    const DEFALUT_CACHE_MINS = 5; // mins

    public static function getCharacters(string $ncid, $cacheKey = null):array
    {
        $data = json_decode(
            Cache::remember(
                ($cacheKey != "" ? $cacheKey : self::DEFAULT_CACHE_CHARACTER_KEY) . $ncid,
                self::DEFALUT_CACHE_MINS,
                function () use ($ncid) {
                    return self::postToApi(self::BASE_API, [
                        'key_name' => 'bns',
                        'service' => 'characters',
                        'user_id' => $ncid
                    ]);
                }
            ),
            true
        );
        if (isset($data['status']) && $data['status'] == true && count($data['response']) > 0) {
            return $data['response'];
        } else {
            return [];
        }
    }
    public static function getDiamondBalance(int $uid):int
    {
        $data = json_decode(self::postToApi(self::BASE_API, [
            'key_name' => 'bns',
            'service' => 'balance',
            'uid' => $uid,
        ]), true);
        if (isset($data['status']) && $data['status'] == 1) {
            return (int)$data['balance'];
        } else {
            return 0;
        }
    }

    public static function deductDiamond(string $ncid,int $cost,$eventDesc = ""):array{
        $eventDesc = $eventDesc != "" ? $eventDesc : 'Deduct diamomds for garena events BNS.';
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => $eventDesc,
            'currency_group_id' => 71,
            'goods' => json_encode([[
                'goods_id' => 741,
                'purchase_quantity' => $cost,
                'purchase_amount'   => $cost,
                'category_id' => 40
            ]])
        ];
        $resp = json_decode(self::postToApi(self::BASE_API, $data), true);
        return [
            'status' => isset($resp['status']) && $resp['status'] == true,
            'purchase_id' => isset($resp['response']['purchase_id']) ? $resp['response']['purchase_id'] : null,
            'purchase_status' => isset($resp['response']['purchase_status']) ? $resp['response']['purchase_status'] : null,
            'raw' => json_encode($resp,JSON_UNESCAPED_UNICODE)
        ];
    }
    public static function sendItem(string $ncid, array $items,$eventDesc="")
    {
        if (is_null($items)) {
            return null;
        }
        // item layout = [{"goods_id":477,"purchase_quantity":1,"purchase_amount":0,"category_id":40}]
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Sending item from garena events BNS '.$eventDesc,
            'goods'                => json_encode($items),
        ];
        $resp = json_decode(self::postToApi(self::BASE_API, $data), true);
        return [
            'status' => isset($resp['status']) && $resp['status'] == true,
            'purchase_id' => isset($resp['response']['purchase_id']) ? $resp['response']['purchase_id'] : null,
            'purchase_status' => isset($resp['response']['purchase_status']) ? $resp['response']['purchase_status'] : null,
            'raw' => json_encode($resp,JSON_UNESCAPED_UNICODE)
        ];
    }

    public static function getNcID(int $uid){
        return Cache::remember(self::DEFAULT_CACHE_NCID_KEY . $uid, self::DEFALUT_CACHE_MINS, function () use ($uid) {
            // get ncid from unser info api
            $userNcInfoResult = self::postToApi(self::BASE_API, [
                'key_name' => 'bns',
                'service'  => 'user_info',
                'uid'      => $uid,
            ]);
            $userNcInfo       = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }
        });
    }

    public static function postToApi($url, $params = array(), $headers = array())
    {
        if (is_array($params)) {
            $data = http_build_query($params);
        } else {
            $data = $params;
        }
        // dd($url, $params, $headers);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        if (empty($headers) === FALSE) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (empty($data) === FALSE) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $resp = curl_exec($ch);

        if (curl_errno($ch)) {
            return null;
        }

        curl_close($ch);
        return $resp;
    }
}
