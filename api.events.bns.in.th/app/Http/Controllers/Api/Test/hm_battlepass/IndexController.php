<?php

    namespace App\Http\Controllers\Api\Test\hm_battlepass;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\Test\hm_battlepass\Member;
    use App\Models\Test\hm_battlepass\Quest;
    use App\Models\Test\hm_battlepass\QuestLog;
    use App\Models\Test\hm_battlepass\SubQuestLog;
    use App\Models\Test\hm_battlepass\Reward;
    use App\Models\Test\hm_battlepass\ItemLog;
    use App\Models\Test\hm_battlepass\DeductLog;

    class IndexController extends BnsEventController{

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2018-11-26 00:00:00';
        private $endTime = '2019-01-09 23:59:59';

        private $startGetApiTime = '00:00:00';
        private $endGetApiTime = '23:59:59';

        private $awtSecret = 'NA5SzXqZTFsefDJ8pV4jnSGbsLtmW5sw';

        private $default_ticket_pass_count = 10;

        // private $ticket_five_pass_diamonds = 30000;
        // private $ticket_pass_diamonds = 10000;
        // private $multiplied_reward_diamonds = 5000;
        // private $unlock_paid_reward_diamonds = 20000;
        // private $random_new_quest_diamonds = 500;

        private $ticket_five_pass_diamonds = 5;
        private $ticket_pass_diamonds = 3;
        private $multiplied_reward_diamonds = 2;
        private $unlock_paid_reward_diamonds = 4;
        private $random_new_quest_diamonds = 1;

        private $specialQuestKey = [5,10,15,20,25,30,35,40];

        private $questWeekDateRange = [
            [
                'week_id' => 1,
                'title' => 'Week 1',
                'start_at' => '2018-11-28 06:00:00',
                'end_at' => '2018-12-05 05:59:59'
            ],
            [
                'week_id' => 2,
                'title' => 'Week 2',
                'start_at' => '2018-12-05 06:00:00',
                'end_at' => '2018-12-12 05:59:59'
            ],
            [
                'week_id' => 3,
                'title' => 'Week 3',
                'start_at' => '2018-12-12 06:00:00',
                'end_at' => '2018-12-19 05:59:59'
            ],
            [
                'week_id' => 4,
                'title' => 'Week 4',
                'start_at' => '2018-12-19 06:00:00',
                'end_at' => '2018-12-26 05:59:59'
            ],
            [
                'week_id' => 5,
                'title' => 'Week 5',
                'start_at' => '2018-12-26 06:00:00',
                'end_at' => '2019-01-02 05:59:59'
            ],
            [
                'week_id' => 6,
                'title' => 'Week 6',
                'start_at' => '2019-01-02 06:00:00',
                'end_at' => '2019-01-09 05:59:59'
            ],
        ];

        public function __construct()
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getUserData();

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {

            if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            $this->userData = $this->getUserData();
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:HM_BATTLEPASS:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        public function acceptChar(Request $request)
        {
            $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            if (is_null($decoded)) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }
            $decoded = collect($decoded);

            if($decoded->has('type') == false && $decoded['type'] != 'accept_character') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter(1)!'
                ]);
            }

            if ($decoded->has('id') == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'invalid parameter(2)!'
                ]);
            }
            $char_id = $decoded['id'];
            if (empty($char_id)) {
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่มีตัวละครที่ต้องการเลือก'
                ]);
            }

            // $char_id = $request->id;

            $uid = $this->userData['uid'];
            $username = $this->userData['username'];

            if ($resp = $this->hasAcceptChar($uid) == false) {
                $ncid = $this->getNcidByUid($uid);
                $characters = $this->doGetChar($uid,$ncid);
                // $collection = collect($characters);
                // $data = $collection->where('id', $char_id)->first();
                $data = $characters[$char_id-1];
                if (isset($data['char_id'])) {
                    $this->doAcceptChar($data['char_id'], $data['char_name'], $data['world_id'], $uid);
                    $eventInfo = $this->setEventInfo($uid);
                    return response()->json([
                                'status' => true,
                                'message' => 'ยืนยันตัวละครเสร็จสิ้น',
                                'content' => $eventInfo
                    ]);
                } else {
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่พบตัวละครที่คุณต้องการเลือก'
                    ]);
                }
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก'
                ]);
            }
        }

        private function doAcceptChar($char_id, $char_name, $world_id, $uid)
        {
            return Member::where('uid', $uid)->update([
                        'char_id' => $char_id,
                        'char_name' => $char_name,
                        'world_id' => $world_id,
                    ]);
        }

        private function hasAcceptChar($uid)
        {
            $resp = Member::where('uid', $uid)->first();
            if (empty($resp->char_id) && empty($resp->char_name)) {
                return false;
            } else {
                return $resp;
            }
        }

        public function getCharacter(Request $request)
        {
            $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            if (is_null($decoded)) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }
            $decoded = collect($decoded);

            if($decoded->has('type') == false && $decoded['type'] != 'get_characters') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $this->checkMember();
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            $resp = $this->hasAcceptChar($uid);
            if ($resp == false) {
                $charactersList = [];
                $ncid = $this->getNcidByUid($uid);
                $characters = $this->doGetChar($uid,$ncid);
                foreach ($characters as $character) {
                    $charactersList[] = collect($character)->except(['char_id', 'world_id', 'job', 'level', 'creation_time', 'last_play_start', 'last_play_end', 'last_ip', 'exp', 'mastery_level']);
                }
                return response()->json([
                            'status' => true,
                            'content' => $charactersList,
                            'accepted' => false
                ]);
            } else {
                return response()->json([
                            'status' => true,
                            'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก',
                            'accepted' => true
                ]);
            }
        }


        private function doGetChar($uid,$ncid)
        {//new fixed function
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i =1;
                    foreach ($resp->response as $key => $value) {
                        $content[] = [
                            'id' => $i,
                            'char_id' => $value->id,
                            'char_name' => $value->name,
                            'world_id' => $value->world_id,
                            'job' => $value->job,
                            'level' => $value->level,
                            'creation_time' => $value->creation_time,
                            'last_play_start' => $value->last_play_start,
                            'last_play_end' => $value->last_play_end,
                            'last_ip' => $value->last_ip,
                            'exp' => $value->exp,
                            'mastery_level' => $value->mastery_level,
                        ];

                        $i++;
                    }
                    return $content;
                }
            }
            return null;
        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:MY_CHARACTERS_WITH_UID_' . $uid, 10, function() use($ncid) {
                        return $this->post_api($this->baseApi, [
                                    'key_name' => 'bns',
                                    'service' => 'characters',
                                    'user_id' => $ncid
                        ]);
                    });
        }

        private function diamondBalance(int $uid) {
            return $this->post_api($this->baseApi, [
                        'key_name' => 'bns',
                        'service' => 'balance',
                        'uid' => $uid,
            ]);
        }

        private function setRemainDiamonds($uid){
            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return 0;
            }else{
                return intval($diamondsBalance->balance);
            }
        }

        private function setDiamondDeductData($deduct_diamond_amount=0)
        {
            if($deduct_diamond_amount==0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test deduct diamomds for garena events BNS Hongmmon Battle Pass',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function dosendItem(string $ncid, array $goodsData) {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events BNS Hongmmon Battle Pass',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addSendItemLog(array $arr) {
            if ($arr) {
                $resp = ItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateSendItemLog(array $arr, $logId) {
            if ($arr) {
                return ItemLog::where('id', $logId)->update($arr);
            }
            return null;
        }

        private function addDeductLog(array $arr) {
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductLog(array $arr, $logId) {
            if ($arr) {
                return DeductLog::where('id', $logId)->update($arr);
            }
            return null;
        }

        private function addDeductPointLog(array $arr) {
            if ($arr) {
                $resp = DeductPointLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function apiAchievement($uid,$char_id,$achievement_id,$achievement_step,$quest_id){
            if (empty($uid) || empty($char_id) || empty($achievement_id) || empty($achievement_step) || empty($quest_id)){
                return false;
            }

            return Cache::remember('BNS:HM_BATTLEPASS:ACHEIVEMENT_'.$quest_id . '_' . $uid, 5, function() use($char_id,$achievement_id,$achievement_step) {
                        $data = $this->post_api($this->baseApi, [
                            'key_name' => 'bns',
                            'service' => 'achievement_completed_step',
                            'char_id' => $char_id,
                            'achievement_id' => $achievement_id,
                            'achievement_step' => $achievement_step
                        ]);
                        return json_decode($data);
                    });
        }

        private function checkAchievementCompleted($uid,$char_id,$achievement_id,$achievement_step,$quest_id){

            $completed = false;

            $acheivementInfo = $this->apiAchievement($uid,$char_id,$achievement_id,$achievement_step,$quest_id);
            if($acheivementInfo->status == true && isset($acheivementInfo->response)){
                $completed = true;
            }

            return $completed;
        }

        private function apiAcheivementCount($uid,$char_id,$achievement_id,$quest_id){
            if (empty($uid) || empty($char_id) || empty($achievement_id) || empty($quest_id)){
                return false;
            }

            return Cache::remember('BNS:HM_BATTLEPASS:ACHEIVEMENT_COUNT_'.$quest_id . '_' . $uid, 5, function() use($char_id,$achievement_id) {
                        $data = $this->post_api($this->baseApi, [
                            'key_name' => 'bns',
                            'service' => 'get_achievement_count',
                            'char_id' => $char_id,
                            'id' => $achievement_id
                        ]);
                        return json_decode($data);
                    });
        }

        private function getAcheivementCount($uid,$char_id,$achievement_id,$quest_id){

            $completed = 0;

            $acheivementCountInfo = $this->apiAcheivementCount($uid,$char_id,$achievement_id,$quest_id);
            if($acheivementCountInfo->status == true && isset($acheivementCountInfo->response)){
                $completed =  intval($acheivementCountInfo->response[0]->RegisterValue);
            }

            return $completed;
        }

        private function apiCheckQuestCompleted($uid, $charId, $questId, $start, $end) {
            $resp = Cache::remember('BNS:HM_BATTLEPASS:QUEST_COMPLETED_' . $questId . '_' . $uid, 5, function()use($charId,$questId,$start,$end) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'quest_completed',
                    'char_id' => $charId,
                    'quest_id' => $questId,
                    'start_time' => $start,
                    'end_time' => $end
                ]);
            });

            return json_decode($resp);
        }

        private function setQuestCompletedCount($uid, $charId, $questId, $start, $end){

            $completedCount = 0;

            $Count = $this->apiCheckQuestCompleted($uid, $charId, $questId, $start, $end);
            if($Count->status){
                $completedCount = intval($Count->response->quest_completed_count);
            }

            return $completedCount;

        }

        private function apiCheckQuestCompletedDaily($uid, $charId, $questId, $start, $end, $day='') {
            $resp = Cache::remember('BNS:HM_BATTLEPASS:QUEST_COMPLETED_DAILY_' . $questId . '_' . $day . '_' . $uid, 5, function()use($charId,$questId,$start,$end) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'quest_completed',
                    'char_id' => $charId,
                    'quest_id' => $questId,
                    'start_time' => $start,
                    'end_time' => $end
                ]);
            });

            return json_decode($resp);
        }

        private function setDailyDungeonCompletedCount($uid, $charId, $questId, $start, $end, $day){

            $completedCount = 0;

            $questCompleted = $this->apiCheckQuestCompletedDaily($uid, $charId, $questId, $start, $end, $day);
            if($questCompleted->status){
                $completedCount = intval($questCompleted->response->quest_completed_count);
            }

            return $completedCount;

        }

        private function apiCheckQuestCompletedWeekly($uid, $charId, $questId, $start, $end, $week=1) {
            $resp = Cache::remember('BNS:HM_BATTLEPASS:QUEST_COMPLETED_WEEKLY_' . $questId . '_' . $week . '_' . $uid, 5, function()use($charId,$questId,$start,$end) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'quest_completed',
                    'char_id' => $charId,
                    'quest_id' => $questId,
                    'start_time' => $start,
                    'end_time' => $end
                ]);
            });

            return json_decode($resp);
        }

        private function setWeeklyDungeonCompletedCount($uid, $charId, $questId, $start, $end, $week){

            $completedCount = 0;

            $questCompleted = $this->apiCheckQuestCompletedWeekly($uid, $charId, $questId, $start, $end, $week);
            if($questCompleted->status){
                $completedCount = intval($questCompleted->response->quest_completed_count);
            }

            return $completedCount;

        }

        private function apiKillMoster($uid = null,$ncid = null,$char_id = null,$start_date = null,$end_date = null)
        {
            if (empty($uid) || empty($ncid) || empty($char_id) || empty($start_date) || empty($end_date))
                return false;

            return Cache::remember('BNS:HM_BATTLEPASS:KILL_MONSTERS_' . $uid, 1, function() use($ncid,$char_id,$start_date,$end_date) {
                        $data = $this->post_api($this->baseApi, [
                            'key_name' => 'bns',
                            'service' => 'events',
                            'user_id' => $ncid,
                            'subservice' => 'get_mon_kills',
                            'start_time' => $start_date,
                            'end_time' => $end_date,
                            'char_id' => $char_id
                        ]);
                        return json_decode($data);
                    });
        }

        private function setMosterKillCount($uid,$ncid,$char_id,$startDate,$endDate)
        {
            $monsterKillCount = 0;
            $monsterKill = $this->apiKillMoster($uid,$ncid,$char_id,$startDate,$endDate);
            if($monsterKill->status == true){
                $monsterKillCount = intval($monsterKill->response->kill_count);
            }

            return $monsterKillCount;
        }
        
        public function getEventInfo(Request $request)
        {
            $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            if (is_null($decoded)) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }
            $decoded = collect($decoded);

            if ($decoded->has('type') == false && $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id) && empty($member->roll)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
            ]);

        }

        private function setEventInfo($uid){

            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            // check latest quest completed
            $currQuestStep = $this->checkAndCreateQuest($member->uid);

            $latestQuestLogId = $currQuestStep['quest_log_id'];
            $currStep = $currQuestStep['step'];

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            // check ticket 5 pass
            $ticketFivePassStatus = false;
            $ticketFivePassHistory = DeductLog::where('uid', $member->uid)
                                            ->where('deduct_type', 'ticket_5_pass')
                                            ->count();
            if($diamonds >= $this->ticket_five_pass_diamonds && $ticketFivePassHistory == 0 && $currStep == 1){
                $ticketFivePassStatus = true;
            }

            // check ticket pass
            // $ticketPassStatus = false;
            $ticketPassHistory = DeductLog::where('uid', $member->uid)
                                        ->where('deduct_type', 'ticket_pass')
                                        ->count();
            // if($diamonds >= $this->ticket_pass_diamonds && $ticketPassHistory >= 0 && $ticketPassHistory < 10){
            //     $ticketPassStatus = true;
            // }

            // check unlock paid reward
            $unlockPaidRewardStatus = false;
            $unlockPaidRewardHistory = DeductLog::where('uid', $member->uid)
                                            ->where('deduct_type', 'unlock_paid_reward')
                                            ->count();
            if($unlockPaidRewardHistory>0){
                $unlockPaidRewardStatus = true;
            }
            
            // set 40 quests
            $questList = [];
            for($i=1;$i<=40;$i++){

                if(isset($latestQuestLogId) && isset($currStep) && $i == $currStep){

                    // check quest completed
                    $questStatusInfo = $this->checkQuestStatusInfo($latestQuestLogId);

                    // check unlock multiplied reward
                    $canMultipliedRewardStatus = false;
                    $multipliedRewardHistory = DeductLog::where('uid', $member->uid)
                                                ->where('step', $i)
                                                ->where('deduct_type', 'multiplied_reward')
                                                ->count();
                    if($diamonds >= $this->multiplied_reward_diamonds && $multipliedRewardHistory == 0 && !in_array($i, $this->specialQuestKey)){
                        $canMultipliedRewardStatus = true;
                    }

                    // check can random new quest
                    $canRandomNewQuestStatus = false;
                    if($diamonds >= $this->random_new_quest_diamonds && $questStatusInfo['status'] == false){
                        $canRandomNewQuestStatus = true;
                    }

                    // check free reward history
                    $free_reward_claim_count = 1;
                    if($multipliedRewardHistory > 0){
                        $free_reward_claim_count = 2;
                    }

                    $canClaimFreeRewardStatus = false;
                    $rewardFreeHistory = ItemLog::where('uid', $member->uid)
                                                ->where('package_type', 'free')
                                                ->where('quest_log_id', $latestQuestLogId)
                                                ->count();
                    if($rewardFreeHistory < $free_reward_claim_count && $questStatusInfo['status'] == true){
                        $canClaimFreeRewardStatus = true;
                    }

                    // check paid reward history
                    $paid_reward_claim_count = 1;
                    if($multipliedRewardHistory > 0){
                        $paid_reward_claim_count = 2;
                    }

                    $canClaimPaidRewardStatus = false;
                    $rewardPaidHistory = ItemLog::where('uid', $member->uid)
                                                ->where('package_type', 'paid')
                                                ->where('quest_log_id', $latestQuestLogId)
                                                ->count();

                    if($rewardPaidHistory < $paid_reward_claim_count && $questStatusInfo['status'] == true && $unlockPaidRewardStatus == true){
                        $canClaimPaidRewardStatus = true;
                    }

                    // check used ticket pass for this quest
                    $usedQuestTicketPassHistory = DeductLog::where('uid', $member->uid)
                                                            ->where('step', $i)
                                                            ->where('deduct_type', 'ticket_pass')
                                                            ->where('status', 'success')
                                                            ->count();

                    // set reward info
                    $rewards = $this->setRewardByQuestId($questStatusInfo['quest_id'], $questStatusInfo['step']);

                    // set empty data
                    $questList[] = [
                        'active' => true,
                        'step' => $questStatusInfo['step'] ?? 0,
                        'quest_title' => $questStatusInfo['quest_title'] ?? '',
                        'count_required' => (int)$questStatusInfo['count_required'],
                        'count_completed' => (int)$questStatusInfo['count_completed'],
                        'sub_quests' => $questStatusInfo['sub_quests'],
                        'status' => $questStatusInfo['status'],
                        'can_random' => $canRandomNewQuestStatus,
                        'free_reward' => [
                            'title' => $rewards['free']['product_title'],
                            'amount' => $rewards['free']['amount'],
                            'reward_key' => $rewards['free']['reward_key'],
                            'can_receive' => $canClaimFreeRewardStatus,
                            'received' => $rewardFreeHistory >= $free_reward_claim_count
                        ],
                        'paid_reward' => [
                            'title' => $rewards['paid']['product_title'],
                            'amount' => $rewards['paid']['amount'],
                            'reward_key' => $rewards['paid']['reward_key'],
                            'can_receive' => $canClaimPaidRewardStatus,
                            'received' => $rewardPaidHistory >= $paid_reward_claim_count
                        ],
                        'can_multiplied_reward' => $canMultipliedRewardStatus,
                        'can_use_ticket_pass' => ($usedQuestTicketPassHistory == 0) ? true : false,
                        'can_claim_paid_reward' => $canClaimPaidRewardStatus,
                    ];

                }elseif(isset($currStep) && $i < $currStep){
                    
                    // check completed quest by step
                    $latestQuest = QuestLog::where('uid', $member->uid)
                                        ->where('step', $i)
                                        ->where('status', 'completed')
                                        ->where('reward_claimed', 1)
                                        ->first();
                    
                    if(isset($latestQuest) && !empty($latestQuest->id)){

                        // check quest completed
                        $questStatusInfo = $this->checkQuestStatusInfo($latestQuest->id);

                        // check unlock multiplied reward
                        $canMultipliedRewardStatus = false;
                        $multipliedRewardHistory = DeductLog::where('uid', $member->uid)
                                                    ->where('step', $latestQuest->step)
                                                    ->where('deduct_type', 'multiplied_reward')
                                                    ->count();
                        if($diamonds >= $this->multiplied_reward_diamonds && $multipliedRewardHistory == 0 && !in_array($i, $this->specialQuestKey)){
                            $canMultipliedRewardStatus = true;
                        }

                        // check can random new quest
                        $canRandomNewQuestStatus = false;
                        if($diamonds >= $this->random_new_quest_diamonds && $questStatusInfo['status'] == false){
                            $canRandomNewQuestStatus = true;
                        }

                        // check free reward history
                        $free_reward_claim_count = 1;
                        if($multipliedRewardHistory > 0){
                            $free_reward_claim_count = 2;
                        }
                        
                        $canClaimFreeRewardStatus = false;
                        $rewardFreeHistory = ItemLog::where('uid', $member->uid)
                                                    ->where('package_type', 'free')
                                                    ->where('quest_log_id', $latestQuest->id)
                                                    ->count();
                        if($rewardFreeHistory < $free_reward_claim_count && $questStatusInfo['status'] == true){
                            $canClaimFreeRewardStatus = true;
                        }

                        // check paid reward history
                        $paid_reward_claim_count = 1;
                        if($multipliedRewardHistory > 0){
                            $paid_reward_claim_count = 2;
                        }

                        $canClaimPaidRewardStatus = false;
                        $rewardPaidHistory = ItemLog::where('uid', $member->uid)
                                                    ->where('package_type', 'paid')
                                                    ->where('quest_log_id', $latestQuest->id)
                                                    ->count();

                        if($rewardPaidHistory < $paid_reward_claim_count && $questStatusInfo['status'] == true && $unlockPaidRewardStatus == true){
                            $canClaimPaidRewardStatus = true;
                        }

                        // check used ticket pass for this quest
                        $usedQuestTicketPassHistory = DeductLog::where('uid', $member->uid)
                                                            ->where('step', $i)
                                                            ->where('deduct_type', 'ticket_pass')
                                                            ->where('status', 'success')
                                                            ->count();

                        // set reward info
                        $rewards = $this->setRewardByQuestId($latestQuest->quest_id, $latestQuest->step);

                        $questList[] = [
                            'active' => true,
                            'step' => $questStatusInfo['step'] ?? 0,
                            'quest_title' => $questStatusInfo['quest_title'] ?? '',
                            'count_required' => (int)$questStatusInfo['count_required'],
                            'count_completed' => (int)$questStatusInfo['count_completed'],
                            'sub_quests' => $questStatusInfo['sub_quests'],
                            'status' => $questStatusInfo['status'],
                            'can_random' => false,
                            'free_reward' => [
                                'title' => $rewards['free']['product_title'],
                                'amount' => $rewards['free']['amount'],
                                'reward_key' => $rewards['free']['reward_key'],
                                'can_receive' => $canClaimFreeRewardStatus,
                                'received' => $rewardFreeHistory >= $free_reward_claim_count
                            ],
                            'paid_reward' => [
                                'title' => $rewards['paid']['product_title'],
                                'amount' => $rewards['paid']['amount'],
                                'reward_key' => $rewards['paid']['reward_key'],
                                'can_receive' => $canClaimPaidRewardStatus,
                                'received' => $rewardPaidHistory >= $paid_reward_claim_count
                            ],
                            'can_multiplied_reward' => $canMultipliedRewardStatus,
                            'can_use_ticket_pass' => ($usedQuestTicketPassHistory == 0) ? true : false,
                            'can_claim_paid_reward' => $canClaimPaidRewardStatus,
                        ];

                    }

                }else{

                    if(in_array($i, $this->specialQuestKey)){

                        $specialQuest = $this->setSpecialQuestByStep($i);

                        // has sub quest
                        $subQuestList = [];
                        if($specialQuest['has_sub_quest'] && !in_array($specialQuest['quest_type'], ['daily','weekly'])){
                            foreach($specialQuest['sub_quest'] as $sub){
                                $subQuestList[] = [
                                    'title' => $sub['quest_title'],
                                    'count_required' => $sub['count_required'],
                                    'status' => false
                                ];
                            }
                        }

                        $questList[] = [
                            'active' => false,
                            'step' => $i ?? 0,
                            'quest_title' => $specialQuest['quest_title'] ?? '',
                            'count_required' => $specialQuest['count_required'],
                            'count_completed' => 0,
                            'sub_quests' => $subQuestList,
                            'status' => false,
                            'can_random' => false,
                            'free_reward' => [
                                'title' => $specialQuest['rewards']['free']['product_title'],
                                'amount' => $specialQuest['rewards']['free']['amount'],
                                'reward_key' => $specialQuest['rewards']['free']['reward_key'],
                                'can_receive' => false,
                                'received' => false
                            ],
                            'paid_reward' => [
                                'title' => $specialQuest['rewards']['paid']['product_title'],
                                'amount' => $specialQuest['rewards']['paid']['amount'],
                                'reward_key' => $specialQuest['rewards']['paid']['reward_key'],
                                'can_receive' => false,
                                'received' => false
                            ],
                            'can_multiplied_reward' => false,
                            'can_use_ticket_pass' => false,
                            'can_claim_paid_reward' => false,
                        ];
                    }else{
                        // default data
                        $questList[] = [];
                    }
                }
            }

            return [
                'char_name' => $member->char_name,
                'ticket_pass' => [
                    'total' => $this->default_ticket_pass_count,
                    'remain' => $this->default_ticket_pass_count - $ticketPassHistory,
                    'used' => $ticketPassHistory,
                ],
                'is_unlock_paid_reward' => $unlockPaidRewardHistory > 0 ? true : false,
                'can_unlock_paid_reward' => $unlockPaidRewardHistory == 0 ? true : false,
                'can_ticket_5_pass' => $ticketFivePassStatus,
                'quests' => $questList
            ];
        }

        private function checkAndCreateQuest($uid){

            if(empty($uid)){
                return false;
            }

            $latestQuest = QuestLog::where('uid', $uid)
                                ->whereIn('status', ['pending','completed'])
                                ->where('reward_claimed', 0)
                                ->orderBy('step', 'DESC')
                                ->first();

            $latestCompletedQuest = QuestLog::where('uid', $uid)
                                        ->where('status', 'completed')
                                        ->where('reward_claimed', 1)
                                        ->orderBy('step', 'DESC')
                                        ->first();

            if(isset($latestCompletedQuest) && !empty($latestCompletedQuest->step) && $latestCompletedQuest->step == 40){
                return [
                    'quest_log_id' => $latestCompletedQuest->id,
                    'step' => 41,
                ];
            }

            $latestQuestLogId = 0;
            $currStep = 1;
            if(empty($latestQuest->id)){

                if(isset($latestCompletedQuest) && !empty($latestCompletedQuest->step)){
                    $currStep = $latestCompletedQuest->step+1;
                }

                // create new quest
                $latestQuestLogId = $this->createNewQuest($uid,$currStep);
            }else{
                $latestQuestLogId = $latestQuest->id;
                $currStep = $latestQuest->step;
            }

            return [
                'quest_log_id' => $latestQuestLogId,
                'step' => $currStep,
            ];
        }

        private function checkQuestStatusInfo($questLogId){
            if(empty($questLogId)){
                return false;
            }

            // $quests = new Quest;

            $questLog = QuestLog::where('id', $questLogId)->first();

            $statusInfo = [];

            if($questLog->status == 'completed' && $questLog->reward_claimed == 1){

                // get sub quest
                $subQuest = [];
                if($questLog->has_sub_quest == 1 && !in_array($questLog->quest_type, ['daily','weekly'])){
                    $subQuestList = SubQuestLog::where('uid', $questLog->uid)
                                            ->where('quest_log_id', $questLog->id)
                                            ->where('step', $questLog->step)
                                            ->get();
                    if(count($subQuestList) > 0){
                        foreach($subQuestList as $subQ){
                            $subQuest[] = [
                                'title' => $subQ->sub_quest_title,
                                'count_required' => $subQ->sub_count_required,
                                // 'count_completed' => ($subQ->sub_count_after >= $subQ->sub_count_before) ? $subQ->sub_count_after - $subQ->sub_count_before : 0,
                                'count_completed' => $subQ->sub_count_required,
                                'status' => $subQ->status == 'completed' ? true : false
                            ];
                        }
                    }
                }

                if(in_array($questLog->quest_id, [44,45])){
                    $subQuest = [];
                }

                $countRequired = 0;
                $countCompleted = 0;

                if($questLog->quest_type == "achievement_completed"){
                    $countRequired = 1;
                    $countCompleted = 1;
                }else{
                    $countRequired = $questLog->count_required;
                    $countCompleted = $questLog->count_required;
                }

                $statusInfo = [
                    'step' => $questLog->step,
                    'quest_id' => $questLog->quest_id,
                    'quest_title' => $questLog->quest_title,
                    'count_required' => $countRequired,
                    'quest_type' => $questLog->quest_type,
                    'count_completed' => $countCompleted,
                    'sub_quests' => $subQuest,
                    'status' => $questLog->status == 'completed' ? true : false,
                ];

            }else{

                switch($questLog->quest_type){

                    case "quest_completed_count": // check quest completed api

                        $startDate = $questLog->quest_start;
                        $endDate = date('Y-m-d H:i:s');

                        $questCompletedCount = $this->setQuestCompletedCount($questLog->uid, $questLog->char_id, $questLog->quest_code, $startDate, $endDate);

                        $status = false;
                        if($questCompletedCount >= $questLog->count_required){
                            $status = true;
                            $questLog->status = 'completed';
                            $questLog->count_after = $questCompletedCount;
                            $questLog->completed_type = 'quest_completed';
                            $questLog->save();
                        }

                        $statusInfo = [
                            'step' => $questLog->step,
                            'quest_id' => $questLog->quest_id,
                            'quest_title' => $questLog->quest_title,
                            'count_required' => $questLog->count_required,
                            'quest_type' => $questLog->quest_type,
                            'count_completed' => $questCompletedCount,
                            'sub_quests' => [],
                            'status' => $status,
                        ];

                        break;
    
                    case "achievement_completed": // check from achievement completed api

                        $achievementCompleted = $this->checkAchievementCompleted($questLog->uid,$questLog->char_id,$questLog->quest_code,$questLog->achievement_step,$questLog->quest_id);

                        $status = false;
                        if($achievementCompleted == true){
                            $status = true;
                            $questLog->status = 'completed';
                            $questLog->completed_type = 'quest_completed';
                            $questLog->save();
                        }

                        $statusInfo = [
                            'step' => $questLog->step,
                            'quest_id' => $questLog->quest_id,
                            'quest_title' => $questLog->quest_title,
                            'count_required' => 1,
                            'quest_type' => $questLog->quest_type,
                            'count_completed' => $achievementCompleted == true ? 1 : 0,
                            'sub_quests' => [],
                            'status' => $status,
                        ];
    
                        break;
    
                    case "achievement_count": // check from achievement completed count api
                        
                        $status = false;
                        $achievementCount = 0;
                        $questCountCompleted = 0;
                        $subQuest = [];
                        
                        $subTotalBeforeCount = 0;
                        $subTotalAfterCount = 0; // for achievement id : 223, 224

                        if($questLog->has_sub_quest == 1 && !in_array($questLog->quest_type, ['daily','weekly'])){

                            $subQuestList = SubQuestLog::where('uid', $questLog->uid)
                                            ->where('quest_log_id', $questLog->id)
                                            ->where('step', $questLog->step)
                                            ->get();

                            $subQuestCount = count($subQuestList);

                            if(count($subQuestList) > 0){
                                foreach($subQuestList as $subQ){

                                    $subCompletedCount= 0;
                                    $subAchievementCount = 0;
                                    $subAchievementCount = $this->getAcheivementCount($questLog->uid,$questLog->char_id,$subQ->sub_quest_code,$subQ->id);
                                    
                                    if(in_array($subQ->sub_quest_code, [223,224])){

                                        $subTotalBeforeCount += $subQ->sub_count_before;
                                        $subTotalAfterCount += $subAchievementCount;
                                    }
                                    
                                    $diffSubAchievementCount = 0;
                                    $subStatus = false;

                                    $diffSubAchievementCount = ($subAchievementCount - $subQ->sub_count_before > 0 && $subAchievementCount > $subQ->sub_count_before) ? (int)$subAchievementCount - (int)$subQ->sub_count_before : 0;

                                    if($diffSubAchievementCount >= $subQ->sub_count_required || $subAchievementCount >= $subQ->sub_max_count){

                                        $subStatus = true;
                                        // $subCompletedCount = $subAchievementCount - $subQ->sub_count_before;
                                        $subCompletedCount = $diffSubAchievementCount;
                                        $achievementCount += 1;

                                        SubQuestLog::where('id', $subQ->id)->update([
                                            'sub_count_after' => $subAchievementCount,
                                            'status' => 'completed'
                                        ]);
                                    }

                                    $counSubCompleted = 0;
                                    if($questLog->quest_id == 68){
                                        if($subStatus == true){
                                            $counSubCompleted = $subQ->sub_count_required;
                                        }
                                    }else{
                                        // $counSubCompleted = $subQ->sub_max_count == $diffSubAchievementCount ? $subQ->sub_count_required : $diffSubAchievementCount;
                                        $counSubCompleted = $subQ->sub_max_count == $subAchievementCount ? $subQ->sub_count_required : $diffSubAchievementCount;
                                    }

                                    $subQuest[] = [
                                        'title' => $subQ->sub_quest_title,
                                        'count_required' => (int)$subQ->sub_count_required,
                                        'count_completed' => (int)$counSubCompleted,
                                        'status' => $subStatus
                                    ];
                                }
                            }
                            
                            if(in_array($questLog->quest_id, [44,45]) && $achievementCount > 0){
                                $status = true;
                                $questLog->status = 'completed';
                                $questLog->completed_type = 'quest_completed';
                                $questLog->save();
                            }elseif($achievementCount == $subQuestCount){
                                $status = true;
                                $questLog->status = 'completed';
                                $questLog->completed_type = 'quest_completed';
                                $questLog->save();
                            }

                        }else{

                            $achievementCount = $this->getAcheivementCount($questLog->uid,$questLog->char_id,$questLog->quest_code,$questLog->quest_id);

                            if(($achievementCount - $questLog->count_before) >= $questLog->count_required || ($achievementCount >= $questLog->max_count && $questLog->max_count > 0)){
                                $status = true;
                                $questLog->count_after = $achievementCount;
                                $questLog->status = 'completed';
                                $questLog->completed_type = 'quest_completed';
                                $questLog->save();
                            }
                        }
                        
                        if(in_array($questLog->quest_id, [44,45])){
                            if($subCompletedCount > 0 || $status == true){
                                $questCountCompleted = $questLog->count_required;
                            }else{
                                $questCountCompleted = $subTotalAfterCount > $subTotalBeforeCount ? $subTotalAfterCount - $subTotalBeforeCount : 0;
                            }
                            $subQuest = [];
                        }elseif($questLog->quest_id == 68){
                            if($subCompletedCount > 0 || $status == true){
                                $questCountCompleted = $questLog->count_required;
                            }
                        }else{
                            $questCountCompleted = ($questLog->max_count == $achievementCount) ? $questLog->count_required : $achievementCount - $questLog->count_before;
                        }

                        $statusInfo = [
                            'step' => $questLog->step,
                            'quest_id' => $questLog->quest_id,
                            'quest_title' => $questLog->quest_title,
                            'count_required' => $questLog->count_required,
                            'quest_type' => $questLog->quest_type,
                            'count_completed' => $questCountCompleted,
                            'sub_quests' => $subQuest,
                            'status' => $status,
                        ];

                        break;
    
                    case "monster_kill": // check from monster kill api

                        $startDate = $questLog->quest_start;
                        $endDate = date('Y-m-d H:i:s');

                        $monsterKillCount = $this->setMosterKillCount($questLog->uid,$questLog->ncid,$questLog->char_id,$startDate,$endDate);

                        $status = false;
                        if($monsterKillCount >= $questLog->count_required){
                            $status = true;
                            $questLog->status = 'completed';
                            $questLog->count_after = $monsterKillCount;
                            $questLog->completed_type = 'quest_completed';
                            $questLog->save();
                        }

                        $statusInfo = [
                            'step' => $questLog->step,
                            'quest_id' => $questLog->quest_id,
                            'quest_title' => $questLog->quest_title,
                            'count_required' => $questLog->count_required,
                            'quest_type' => $questLog->quest_type,
                            'count_completed' => $monsterKillCount,
                            'sub_quests' => [],
                            'status' => $status,
                        ];
    
                        break;
                    
                    case "daily": // check from all quests in daily challenge

                        $dailyCompletedCount = $this->checkDailyQuestCompleted($questLog->uid, $questLogId);

                        $status = false;
                        if($dailyCompletedCount >= $questLog->count_required){
                            $status = true;
                            $questLog->status = 'completed';
                            $questLog->count_after = $dailyCompletedCount;
                            $questLog->completed_type = 'quest_completed';
                            $questLog->save();
                        }

                        $statusInfo = [
                            'step' => $questLog->step,
                            'quest_id' => $questLog->quest_id,
                            'quest_title' => $questLog->quest_title,
                            'count_required' => $questLog->count_required,
                            'quest_type' => $questLog->quest_type,
                            'count_completed' => $dailyCompletedCount,
                            'sub_quests' => [],
                            'status' => $status,
                        ];
    
                        break;
                    
                    case "weekly": // check from all quests in weekly challenge

                        $weeklyCompletedCount = $this->checkWeeklyQuestCompleted($questLog->uid, $questLogId);

                        $status = false;
                        if($weeklyCompletedCount >= $questLog->count_required){
                            $status = true;
                            $questLog->status = 'completed';
                            $questLog->count_after = $weeklyCompletedCount;
                            $questLog->completed_type = 'quest_completed';
                            $questLog->save();
                        }

                        $statusInfo = [
                            'step' => $questLog->step,
                            'quest_id' => $questLog->quest_id,
                            'quest_title' => $questLog->quest_title,
                            'count_required' => $questLog->count_required,
                            'quest_type' => $questLog->quest_type,
                            'count_completed' => $weeklyCompletedCount,
                            'sub_quests' => [],
                            'status' => $status,
                        ];

                        break;
    
                    default:
                        break;
                }

            }

            return $statusInfo;
        }

        private function checkWeeklyQuestCompleted($uid, $questLogId){

            if(empty($uid) || empty($questLogId)){
                return false;
            }

            $questLog = QuestLog::where('id', $questLogId)->first();

            $totalWeeklyChallengeCompleted = 0;
            $minimum_weekly_completed_required = 1;

            $weekRange = $this->questWeekDateRange;

            if(count($weekRange) > 0){
                foreach($weekRange as $week){

                    $weekId = $week['week_id'];
                    $startWeeklyTime = $week['start_at'];
                    $endWeeklyTime = $week['end_at'];

                    if(time() >= strtotime($startWeeklyTime) && time() <= strtotime($endWeeklyTime)){

                        $completedWeeklycount = 0;

                        $subQuestList = SubQuestLog::where('uid', $questLog->uid)
                                            ->where('quest_log_id', $questLog->id)
                                            ->where('step', $questLog->step)
                                            ->get();
                        
                        if(count($subQuestList) > 0){
                            foreach($subQuestList as $subQ){

                                $subQuestCompletedCount = 0;
                                $subQuestCompletedCount = $this->setWeeklyDungeonCompletedCount($questLog->uid, $questLog->char_id, $subQ->sub_quest_code, $startWeeklyTime, $endWeeklyTime, $weekId);

                                if($subQuestCompletedCount >= $subQ->sub_count_required){

                                    $completedWeeklycount += 1;

                                    // save daily quest completed
                                    SubQuestLog::where('id', $subQ->id)->update([
                                        'sub_count_after' => $subQuestCompletedCount,
                                        'status' => 'completed'
                                    ]);

                                }

                                if($completedWeeklycount >= 3){
                                    $totalWeeklyChallengeCompleted += 1;
                                    break;
                                }

                            }
                        }

                    }

                    if($totalWeeklyChallengeCompleted >= $minimum_weekly_completed_required){
                        break;
                    }

                }
            }

            return $totalWeeklyChallengeCompleted;
        }

        private function checkDailyQuestCompleted($uid, $questLogId){

            if(empty($uid) || empty($questLogId)){
                return false;
            }

            $questLog = QuestLog::where('id', $questLogId)->first();

            $totalDailyChallengeCompleted = 0;
            $minimum_daily_completed_required = 3;

            $curDate = date('Y-m-d').' '.$this->endGetApiTime;

            if(strtotime($curDate) >= strtotime($this->endTime)){
                $curDate = $this->endTime;
            }

            $eventBegin = new DateTime($questLog->quest_start);
            $eventEnd = new DateTime($curDate);

            $daterange = new DatePeriod($eventBegin, new DateInterval('P1D'), $eventEnd);

            foreach($daterange as $date){

                $dailyDate = '';
                $dailyDate = $date->format("Y-m-d");

                $startDailyDate = $dailyDate.' '.$this->startGetApiTime;
                $endDailyDate = $dailyDate.' '.$this->endGetApiTime;

                $completedDailycount = 0;

                $subQuestList = SubQuestLog::where('uid', $questLog->uid)
                                            ->where('quest_log_id', $questLog->id)
                                            ->where('step', $questLog->step)
                                            ->get();
                if(count($subQuestList) > 0){
                    foreach($subQuestList as $subQ){

                        $subQuestCompletedCount = 0;
                        $subQuestCompletedCount = $this->setDailyDungeonCompletedCount($questLog->uid, $questLog->char_id, $subQ->sub_quest_code, $startDailyDate, $endDailyDate, $dailyDate);

                        if($subQuestCompletedCount >= $subQ->sub_count_required){

                            $completedDailycount += 1;

                            // save daily quest completed
                            SubQuestLog::where('id', $subQ->id)->update([
                                'sub_count_after' => $subQuestCompletedCount,
                                'status' => 'completed'
                            ]);

                        }

                        if($completedDailycount >= 3){
                            $totalDailyChallengeCompleted += 1;
                            break;
                        }
                    }
                }

                if($totalDailyChallengeCompleted >= $minimum_daily_completed_required){
                    break;
                }

            }

            return $totalDailyChallengeCompleted;
        }

        private function createNewQuest($uid=null, $step=null){

            if(empty($uid) || empty($step)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            if(in_array($step, $this->specialQuestKey)){
                $randQuest = $this->setSpecialQuestByStep($step);
            }else{
                // random new quest
                $randQuest = $this->setRandNewQuest($member->uid);
            }
            

            $count_before = 0;
            $achievement_step = 0;
            switch($randQuest['quest_type']){
                case "achievement_count":
                    if($randQuest['has_sub_quest'] == false){
                        $count_before = $this->getAcheivementCount($member->uid,$member->char_id,$randQuest['quest_code'],$randQuest['quest_id']);
                    }
                    break;

                case "achievement_completed":
                    $achievement_step = $randQuest['achievement_step'];
                    break;

                default:
                    break;
            }

            // create first quest
            $questLogId = $this->createQuestLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'char_id' => $member->char_id,
                'char_name' => $member->char_name,
                'step' => $step,
                'quest_id' => $randQuest['quest_id'],
                'quest_code' => $randQuest['quest_code'],
                'quest_title' => $randQuest['quest_title'],
                'quest_type' => $randQuest['quest_type'],
                'quest_start' => date('Y-m-d H:i:s'),
                'count_required' => $randQuest['count_required'],
                'max_count' => $randQuest['max_count'],
                'count_before' => $count_before,
                'count_after' => 0,
                'achievement_step' => $achievement_step,
                'has_sub_quest' => $randQuest['has_sub_quest'] ? 1 : 0,
                'status' => 'pending',
                'completed_type' => 'pending',
                'reward_claimed' => 0,
                'log_date' => date('Y-m-d H:i:s'),
                'log_date_timestamp' => time(),
                'last_ip' => $this->getIP(),
            ]);

            if($randQuest['has_sub_quest']){

                foreach($randQuest['sub_quest'] as $subQuest){

                    $sub_count_before = 0;
                    if($randQuest['quest_type'] == 'achievement_count'){
                        $sub_count_before = $this->getAcheivementCount($member->uid,$member->char_id,$subQuest['quest_code'],$randQuest['quest_id'].'_'.$subQuest['quest_code']);
                    }

                    // create sub quest log
                    $this->createSubQuestLog([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'char_id' => $member->char_id,
                        'char_name' => $member->char_name,
                        'step' => $step,
                        'quest_log_id' => $questLogId,
                        'sub_quest_code' => $subQuest['quest_code'],
                        'sub_quest_title' => $subQuest['quest_title'],
                        'sub_quest_start' => date('Y-m-d H:i:s'),
                        'sub_count_required' => $subQuest['count_required'],
                        'sub_max_count' => $subQuest['max_count'],
                        'sub_count_before' => $sub_count_before,
                        'sub_count_after' => 0,
                        'status' => 'pending',
                        'log_date' => date('Y-m-d H:i:s'),
                        'log_date_timestamp' => time(),
                        'last_ip' => $this->getIP(),
                    ]);
                }

            }

            return $questLogId;
        }

        private function createQuestLog($arr=array()){
            if(empty($arr)){
                return false;
            }

            $create = QuestLog::create($arr);

            return $create['id'];
        }

        private function createSubQuestLog($arr=array()){
            if(empty($arr)){
                return false;
            }

            $create = SubQuestLog::create($arr);

            return $create['id'];
        }

        private function setRandNewQuest($uid){

            $quests = new Quest;
            $randQuest = $quests->getRandomQuest();

            // check random quest log
            $checkQuestLog = QuestLog::where('uid', $uid)
                                    ->where('quest_id', $randQuest['quest_id'])
                                    // ->where('reward_claimed', 1)
                                    ->whereIn('status', ['pending','completed'])
                                    ->count();
            if($checkQuestLog == 0){

                return $randQuest;

            }else{
                return $this->setRandNewQuest($uid);
            }

        }

        private function setQuestById($questId){
            $quests = new Quest;
            $quest = $quests->setQuestById($questId);

            return $quest;
        }

        private function setSpecialQuestByStep($step){
            
            $quests = new Quest;
            $specialQuest = $quests->setQuestByStep($step);

            return $specialQuest;

        }

        private function setRewardByQuestId($questId=null, $step=null){

            if(empty($questId) || empty($step)){
                return false;
            }

            $quests = new Quest;
            $questInfo = [];
            if(in_array($step, $this->specialQuestKey)){
                $questInfo = $quests->setSpecialQuestById($questId);
            }else{
                $questInfo = $quests->setQuestById($questId);
            }

            return $questInfo['rewards'] ?? false;
        }

        public function ticketFivePass(Request $request){
            $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            if (is_null($decoded)) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }
            $decoded = collect($decoded);

            if ($decoded->has('type') == false && $decoded['type'] != 'ticket_five_pass') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('step') == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'invalid parameter(2)!'
                ]);
            }

            $step = $decoded['step'];

            // $step = $request->step;

            if($step != 1){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถใช้บัตรผ่าน 5 เลเวลได้',
                ]);
            }
            
            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $questInfo = QuestLog::where('uid', $member->uid)
                                ->where('step', $step)
                                ->whereIn('status', ['pending','completed'])
                                ->where('reward_claimed', 0)
                                ->first();
            if(!isset($questInfo) && empty($questInfo->id)){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถใช้บัตรผ่าน 5 เลเวลได้'
                ]);
            }

            if($questInfo->step == 1 && $questInfo->status == 'completed' && $questInfo->reward_claimed == 1){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ภารกิจแรกสำเร็จไปแล้ว<br />ไม่สามารถใช้บัตรผ่าน 5 เลเวลได้'
                ]);
            }

            // check ticket pass amount
            $usedTicketFivePass = DeductLog::where('uid', $member->uid)
                                    ->where('deduct_type', 'ticket_5_pass')
                                    ->where('status', 'success')
                                    ->count();

            if($usedTicketFivePass > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้ใช้บัตรผ่าน 5 เลเวลไปแล้ว',
                ]);
            }

            $diamonds = $this->setRemainDiamonds($member->uid);

            if($diamonds < $this->ticket_five_pass_diamonds){
                return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอ'
                ]);
            }else{

                $logDeduct = $this->addDeductLog([
                    'uid' => $uid,
                    'username' => $member->username,
                    'ncid' => $ncid,
                    'step' => $step,
                    'deduct_type' => 'ticket_5_pass',
                    'diamonds' => $this->ticket_five_pass_diamonds,
                    'status' => 'pending',
                    'log_date' => date('Y-m-d H:i:s'), // set to string
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]);

                // set deduct diamonds
                $deductData = $this->setDiamondDeductData($this->ticket_five_pass_diamonds);
                $respDeductRaw = $this->deductDiamond($ncid, $deductData);
                $respDeduct = json_decode($respDeductRaw);
                if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){

                    $arrDeductDiamond = [
                        'before_deduct_diamond' => $diamonds,
                        'after_deduct_diamond' => $diamonds - $this->ticket_five_pass_diamonds,
                        'deduct_status' => $respDeduct->status ?: 0,
                        'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
                        'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'success'
                    ];

                    $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);

                    $sendCompletedQuestCount = 0;
                    // set quests and reward for send
                    for($i=1;$i<=5;$i++){

                        if($i>1){

                            // create new quest and set completed
                            $this->checkAndCreateQuest($member->uid);
                            // $currQuestStep = $this->checkAndCreateQuest($member->uid);
                            // $latestQuestLogId = $currQuestStep['quest_log_id'];
                            // $step = $currQuestStep['step'];

                        }

                        $questInfo = QuestLog::where('uid', $member->uid)
                                                ->where('step', $i)
                                                ->whereIn('status', ['pending','completed'])
                                                ->where('reward_claimed', 0)
                                                ->first();
                            
                        $questInfo->status = 'completed';
                        $questInfo->completed_type = 'ticket_5_pass';
                        $questInfo->reward_claimed = 1;
                        if($questInfo->save()){
    
                            $canClaimRewardStatus = false;
                            $rewardHistory = 0;
                            $reward_claim_count = 1;
                            $rewardInfo = [];
    
                            // set rewards
                            $rewards = $this->setRewardByQuestId($questInfo->quest_id, $questInfo->step);
                            
                            // check unlock paid reward
                            $unlockPaidRewardStatus = false;
                            $unlockPaidRewardHistory = DeductLog::where('uid', $member->uid)
                                                            ->where('deduct_type', 'unlock_paid_reward')
                                                            ->count();
                            if($unlockPaidRewardHistory>0){
                                $unlockPaidRewardStatus = true;
                            }
    
                            // check unlock multiplied reward
                            $multipliedRewardHistory = DeductLog::where('uid', $member->uid)
                                                            ->where('step', $questInfo->step)
                                                            ->where('deduct_type', 'multiplied_reward')
                                                            ->where('status', 'success')
                                                            ->count();
    
                            // check multiplied reward
                            if($multipliedRewardHistory > 0 && !in_array($questInfo->step, $this->specialQuestKey)){
                                $reward_claim_count = 2;
                            }
    
                            // check paid rewards
                            if($unlockPaidRewardStatus == true){
                                $rewardPaidHistory = ItemLog::where('uid', $member->uid)
                                                        ->where('package_type', 'paid')
                                                        ->where('quest_log_id', $questInfo->id)
                                                        ->count();
    
                                if($rewardPaidHistory > 0){
                                    return response()->json([
                                        'status' => false,
                                        'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปก่อนหน้านี้แล้ว'
                                    ]);
                                }
    
                                // set paid reward for send
                                for($p=0;$p<$reward_claim_count;$p++){
                                    $rewardInfo[] = [
                                        'reward_type' => 'paid',
                                        'product_title' => $rewards['paid']['product_title'],
                                        'product_id' => $rewards['paid']['product_id'],
                                        'product_quantity' => $rewards['paid']['product_quantity'],
                                        'amount' => $rewards['paid']['amount'],
                                        'reward_key' => $rewards['paid']['reward_key'],
                                    ];
                                }
                                
                            }
                            
                            // check free rewards
                            $rewardFreeHistory = ItemLog::where('uid', $member->uid)
                                                        ->where('package_type', 'free')
                                                        ->where('quest_log_id', $questInfo->id)
                                                        ->count();
    
                            if($rewardFreeHistory > 0){
                                return response()->json([
                                    'status' => false,
                                    'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปก่อนหน้านี้แล้ว'
                                ]);
                            }
    
                            // set free reward for send
                            for($p=0;$p<$reward_claim_count;$p++){
                                $rewardInfo[] = [
                                    'reward_type' => 'free',
                                    'product_title' => $rewards['free']['product_title'],
                                    'product_id' => $rewards['free']['product_id'],
                                    'product_quantity' => $rewards['free']['product_quantity'],
                                    'amount' => $rewards['free']['amount'],
                                    'reward_key' => $rewards['free']['reward_key'],
                                ];
                            }

                            if(count($rewardInfo) > 0){

                                foreach($rewardInfo as $reward){
    
                                    $goods_data = [];
                                    $goods_data[] = [
                                        'goods_id' => $reward['product_id'],
                                        'purchase_quantity' => $reward['product_quantity'],
                                        'purchase_amount' => 0,
                                        'category_id' => 40
                                    ];
    
                                    if (is_null($goods_data) == false && is_array($goods_data) && count($goods_data) > 0) {
    
                                        $itemLog = null;
                                        $itemLog = $this->addSendItemLog([
                                            'uid' => $member->uid,
                                            'username' => $member->username,
                                            'ncid' => $member->ncid,
                                            'quest_log_id' => $questInfo->id,
                                            'package_id' => $reward['product_id'],
                                            'package_title' => $reward['product_title'],
                                            'amount' => $reward['amount'],
                                            'reward_key' => $reward['reward_key'],
                                            'package_type' => $reward['reward_type'],
                                            'status' => 'pending',
                                            'send_item_status' => false,
                                            'send_item_purchase_id' => 0,
                                            'send_item_purchase_status' => 0,
                                            'goods_data' => json_encode($goods_data),
                                            'log_date' => date('Y-m-d H:i:s'), // set to string
                                            'log_date_timestamp' => time(),
                                            'last_ip' => $this->getIP(),
                                        ]);
                                        
                                        $send_result_raw = null;
                                        $send_result_raw = $this->dosendItem($member->ncid, $goods_data);
                                        $send_result = json_decode($send_result_raw);
                                        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                                            $this->updateSendItemLog([
                                                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                                                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                                                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                                                'status' => 'success'
                                            ], $itemLog['id'], $member->uid);
    
                                        }else{
                                            $this->updateSendItemLog([
                                                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                                                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                                                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                                                'status' => 'unsuccess'
                                            ], $itemLog['id'], $uid);
                            
                                            $eventInfo = $this->setEventInfo($member->uid);
                            
                                            return response()->json([
                                                'status' => false,
                                                'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                                                'content' => $eventInfo
                                            ]);
                                        }
                                    }
    
                                }
                                
                            }

                        }else{
                            return response()->json([
                                'status' => false,
                                'message' => 'เกิดความผิดพลาด<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                            ]);
                        }


                    }
                    
                    // set event info and random new quest
                    $eventInfo = $this->setEventInfo($member->uid);
                            
                    return response()->json([
                        'status' => true,
                        'message' => '5 ภารกิจสำเร็จ<br />กรุณาตรวจสอบของรางวัลที่กล่องจดหมายภายในเกม',
                        'content' => $eventInfo
                    ]);


                }else{
                    $arrDeductDiamond['status'] = 'unsuccess';
                    $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);
                }

                return response()->json([
                            'status' => false,
                            'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }

            return [
                'status' => false,
                'message' => 'No data required.'
            ];

        }

        public function ticketPass(Request $request){
            $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            if (is_null($decoded)) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }
            $decoded = collect($decoded);

            if ($decoded->has('type') == false && $decoded['type'] != 'ticket_pass') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('step') == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'invalid parameter(2)!'
                ]);
            }

            $step = $decoded['step'];

            // $step = $request->step;

            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $questInfo = QuestLog::where('uid', $member->uid)
                                ->where('step', $step)
                                ->whereIn('status', ['pending','completed'])
                                ->where('reward_claimed', 0)
                                ->first();
            if(!isset($questInfo) && empty($questInfo->id)){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่มีภารกิจที่กำลังทำอยู่<br />หรือภารกิจสำเร็จไปแล้ว'
                ]);
            }

            // check used ticket pass for this quest
            $usedQuestTicketPass = DeductLog::where('uid', $member->uid)
                                        ->where('step', $questInfo->step)
                                        ->where('deduct_type', 'ticket_pass')
                                        ->where('status', 'success')
                                        ->count();
            if($usedQuestTicketPass > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้ใช้ตั๋วสำเร็จภารกิจสำหรับภารกิจนี้ไปแล้ว',
                ]);
            }

            // check ticket pass amount
            $usedTicketPass = DeductLog::where('uid', $member->uid)
                                    ->where('deduct_type', 'ticket_pass')
                                    ->where('status', 'success')
                                    ->count();
            if($usedTicketPass >= $this->default_ticket_pass_count){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ตั๋วสำเร็จภารกิจไม่พอ',
                ]);
            }

            $diamonds = $this->setRemainDiamonds($member->uid);

            if($diamonds < $this->ticket_pass_diamonds){
                return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอ'
                ]);
            }else{

                $logDeduct = $this->addDeductLog([
                    'uid' => $uid,
                    'username' => $member->username,
                    'ncid' => $ncid,
                    'step' => $questInfo->step,
                    'deduct_type' => 'ticket_pass',
                    'diamonds' => $this->ticket_pass_diamonds,
                    'status' => 'pending',
                    'log_date' => date('Y-m-d H:i:s'), // set to string
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]);

                // set deduct diamonds
                $deductData = $this->setDiamondDeductData($this->ticket_pass_diamonds);
                $respDeductRaw = $this->deductDiamond($ncid, $deductData);
                $respDeduct = json_decode($respDeductRaw);
                if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){

                    $arrDeductDiamond = [
                        'before_deduct_diamond' => $diamonds,
                        'after_deduct_diamond' => $diamonds - $this->ticket_pass_diamonds,
                        'deduct_status' => $respDeduct->status ?: 0,
                        'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
                        'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'success'
                    ];

                    $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);

                    // set completed quest
                    $questInfo->status = 'completed';
                    $questInfo->completed_type = 'ticket_pass';
                    $questInfo->reward_claimed = 1;
                    if($questInfo->save()){

                        $canClaimRewardStatus = false;
                        $rewardHistory = 0;
                        $reward_claim_count = 1;
                        $rewardInfo = [];

                        // set rewards
                        $rewards = $this->setRewardByQuestId($questInfo->quest_id, $questInfo->step);
                        
                        // check unlock paid reward
                        $unlockPaidRewardStatus = false;
                        $unlockPaidRewardHistory = DeductLog::where('uid', $member->uid)
                                                        ->where('deduct_type', 'unlock_paid_reward')
                                                        ->count();
                        if($unlockPaidRewardHistory>0){
                            $unlockPaidRewardStatus = true;
                        }

                        // check unlock multiplied reward
                        $multipliedRewardHistory = DeductLog::where('uid', $member->uid)
                                                        ->where('step', $questInfo->step)
                                                        ->where('deduct_type', 'multiplied_reward')
                                                        ->where('status', 'success')
                                                        ->count();

                        // check multiplied reward
                        if($multipliedRewardHistory > 0 && !in_array($questInfo->step, $this->specialQuestKey)){
                            $reward_claim_count = 2;
                        }

                        // check paid rewards
                        if($unlockPaidRewardStatus == true){
                            $rewardPaidHistory = ItemLog::where('uid', $member->uid)
                                                    ->where('package_type', 'paid')
                                                    ->where('quest_log_id', $questInfo->id)
                                                    ->count();

                            if($rewardPaidHistory > 0){
                                return response()->json([
                                    'status' => false,
                                    'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปก่อนหน้านี้แล้ว'
                                ]);
                            }

                            // set paid reward for send
                            for($p=0;$p<$reward_claim_count;$p++){
                                $rewardInfo[] = [
                                    'reward_type' => 'paid',
                                    'product_title' => $rewards['paid']['product_title'],
                                    'product_id' => $rewards['paid']['product_id'],
                                    'product_quantity' => $rewards['paid']['product_quantity'],
                                    'amount' => $rewards['paid']['amount'],
                                    'reward_key' => $rewards['paid']['reward_key'],
                                ];
                            }
                            
                        }
                        
                        // check free rewards
                        $rewardFreeHistory = ItemLog::where('uid', $member->uid)
                                                    ->where('package_type', 'free')
                                                    ->where('quest_log_id', $questInfo->id)
                                                    ->count();

                        if($rewardFreeHistory > 0){
                            return response()->json([
                                'status' => false,
                                'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปก่อนหน้านี้แล้ว'
                            ]);
                        }

                        // set free reward for send
                        for($p=0;$p<$reward_claim_count;$p++){
                            $rewardInfo[] = [
                                'reward_type' => 'free',
                                'product_title' => $rewards['free']['product_title'],
                                'product_id' => $rewards['free']['product_id'],
                                'product_quantity' => $rewards['free']['product_quantity'],
                                'amount' => $rewards['free']['amount'],
                                'reward_key' => $rewards['free']['reward_key'],
                            ];
                        }

                        $sendCompletedCount = 0;
                        // create send item log and send item
                        if(count($rewardInfo) > 0){

                            foreach($rewardInfo as $reward){

                                $goods_data = [];
                                $goods_data[] = [
                                    'goods_id' => $reward['product_id'],
                                    'purchase_quantity' => $reward['product_quantity'],
                                    'purchase_amount' => 0,
                                    'category_id' => 40
                                ];

                                if (is_null($goods_data) == false && is_array($goods_data) && count($goods_data) > 0) {

                                    $itemLog = null;
                                    $itemLog = $this->addSendItemLog([
                                        'uid' => $member->uid,
                                        'username' => $member->username,
                                        'ncid' => $member->ncid,
                                        'quest_log_id' => $questInfo->id,
                                        'package_id' => $reward['product_id'],
                                        'package_title' => $reward['product_title'],
                                        'amount' => $reward['amount'],
                                        'reward_key' => $reward['reward_key'],
                                        'package_type' => $reward['reward_type'],
                                        'status' => 'pending',
                                        'send_item_status' => false,
                                        'send_item_purchase_id' => 0,
                                        'send_item_purchase_status' => 0,
                                        'goods_data' => json_encode($goods_data),
                                        'log_date' => date('Y-m-d H:i:s'), // set to string
                                        'log_date_timestamp' => time(),
                                        'last_ip' => $this->getIP(),
                                    ]);
                                    
                                    $send_result_raw = null;
                                    $send_result_raw = $this->dosendItem($member->ncid, $goods_data);
                                    $send_result = json_decode($send_result_raw);
                                    if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                                        $this->updateSendItemLog([
                                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                                            'status' => 'success'
                                        ], $itemLog['id'], $member->uid);

                                        $sendCompletedCount += 1;

                                    }else{
                                        $this->updateSendItemLog([
                                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                                            'status' => 'unsuccess'
                                        ], $itemLog['id'], $uid);
                        
                                        $eventInfo = $this->setEventInfo($member->uid);
                        
                                        return response()->json([
                                            'status' => false,
                                            'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                                            'content' => $eventInfo
                                        ]);
                                    }
                                }

                            }

                            if($sendCompletedCount == count($rewardInfo)){
                                // set event info and random new quest
                                $eventInfo = $this->setEventInfo($member->uid);
                            
                                return response()->json([
                                    'status' => true,
                                    'message' => 'ภารกิจสำเร็จ<br />กรุณาตรวจสอบของรางวัลที่กล่องจดหมายภายในเกม',
                                    'content' => $eventInfo
                                ]);
                            }else{
                                $eventInfo = $this->setEventInfo($member->uid);
                        
                                return response()->json([
                                    'status' => false,
                                    'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                                    'content' => $eventInfo
                                ]);
                            }
                            
                        }

                    }else{
                        return response()->json([
                            'status' => false,
                            'message' => 'เกิดความผิดพลาด<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                        ]);
                    }
                    

                }else{
                    $arrDeductDiamond['status'] = 'unsuccess';
                    $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);
                }

                return response()->json([
                            'status' => false,
                            'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);

            }

            return [
                'status' => false,
                'message' => 'No data required.'
            ];

        }

        public function randomNewQuest(Request $request){
            $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            if (is_null($decoded)) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }
            $decoded = collect($decoded);

            if ($decoded->has('type') == false && $decoded['type'] != 'random_new_quest') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('step') == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'invalid parameter(2)!'
                ]);
            }

            $step = $decoded['step'];

            // $step = $request->step;

            if(in_array($step, $this->specialQuestKey)){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถสุ่มภารกิจใหม่สำหรับเลเวลนี้'
                ]);
            }

            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $questInfo = QuestLog::where('uid', $member->uid)
                                ->where('step', $step)
                                ->where('status', 'pending')
                                ->where('reward_claimed', 0)
                                ->first();
            if(!isset($questInfo) && empty($questInfo->id)){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่มีภารกิจที่กำลังทำอยู่<br />หรือภารกิจสำเร็จไปแล้ว'
                ]);
            }

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            if($diamonds < $this->random_new_quest_diamonds){
                return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอ'
                ]);
            }else{

                $logDeduct = $this->addDeductLog([
                    'uid' => $uid,
                    'username' => $member->username,
                    'ncid' => $ncid,
                    'step' => $questInfo->step,
                    'deduct_type' => 'random_mission',
                    'diamonds' => $this->random_new_quest_diamonds,
                    'status' => 'pending',
                    'log_date' => date('Y-m-d H:i:s'), // set to string
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]);

                // set deduct diamonds
                $deductData = $this->setDiamondDeductData($this->random_new_quest_diamonds);
                $respDeductRaw = $this->deductDiamond($ncid, $deductData);
                $respDeduct = json_decode($respDeductRaw);
                if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){

                    $arrDeductDiamond = [
                        'before_deduct_diamond' => $diamonds,
                        'after_deduct_diamond' => $diamonds - $this->random_new_quest_diamonds,
                        'deduct_status' => $respDeduct->status ?: 0,
                        'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
                        'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'success'
                    ];

                    $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);

                    // reject old quest
                    QuestLog::where('uid', $uid)
                            ->where('step', $step)
                            ->where('status', 'pending')
                            ->where('reward_claimed', 0)
                            ->update([
                                'status' => 'reject',
                                'completed_type' => 'reject',
                            ]);

                    
                    // $this->setRandNewQuest($uid);
                    // set event info and random new quest
                    $eventInfo = $this->setEventInfo($uid);
    
                    return response()->json([
                        'status' => true,
                        'message' => 'สุ่มภารกิจใหม่สำเร็ว!',
                        'content' => $eventInfo
                    ]);
                    

                }else{
                    $arrDeductDiamond['status'] = 'unsuccess';
                    $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);
                }

                return response()->json([
                            'status' => false,
                            'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);

            }

            return [
                'status' => false,
                'message' => 'No data required.'
            ];
        }

        public function multipliedReward(Request $request){
            $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            if (is_null($decoded)) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }
            $decoded = collect($decoded);

            if ($decoded->has('type') == false && $decoded['type'] != 'multiplied_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('step') == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'invalid parameter(2)!'
                ]);
            }

            $step = $decoded['step'];

            // $step = $request->step;

            if(in_array($step, $this->specialQuestKey)){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถใช้ x2 สำหรับเลเวลนี้'
                ]);
            }

            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $questInfo = QuestLog::where('uid', $member->uid)
                                ->where('step', $step)
                                ->whereIn('status', ['pending','completed'])
                                // ->where('reward_claimed', 0)
                                ->first();
            if(!isset($questInfo) && empty($questInfo->id)){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่มีภารกิจที่กำลังทำอยู่'
                ]);
            }

            // check multiplied reward history
            $multipliedRewardHistory = DeductLog::where('uid', $member->uid)
                                            ->where('step', $questInfo->step)
                                            ->where('deduct_type', 'multiplied_reward')
                                            ->where('status', 'success')
                                            ->count();
            if($multipliedRewardHistory > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้ปลดล็อคของรางวัล x2<br />ของภารกิจนี้ไปแล้ว'
                ]);
            }

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            if($diamonds < $this->multiplied_reward_diamonds){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอ'
                ]);
            }else{

                $logDeduct = $this->addDeductLog([
                    'uid' => $uid,
                    'username' => $member->username,
                    'ncid' => $ncid,
                    'step' => $questInfo->step,
                    'deduct_type' => 'multiplied_reward',
                    'diamonds' => $this->multiplied_reward_diamonds,
                    'status' => 'pending',
                    'log_date' => date('Y-m-d H:i:s'), // set to string
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]);

                // set deduct diamonds
                $deductData = $this->setDiamondDeductData($this->multiplied_reward_diamonds);
                $respDeductRaw = $this->deductDiamond($ncid, $deductData);
                $respDeduct = json_decode($respDeductRaw);
                if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){

                    $arrDeductDiamond = [
                        'before_deduct_diamond' => $diamonds,
                        'after_deduct_diamond' => $diamonds - $this->multiplied_reward_diamonds,
                        'deduct_status' => $respDeduct->status ?: 0,
                        'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
                        'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'success'
                    ];

                    $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);

                    $eventInfo = $this->setEventInfo($uid);
    
                    return response()->json([
                        'status' => true,
                        'message' => 'ปลดล็อคของรางัวล x2 สำเร็จ!',
                        'content' => $eventInfo
                    ]);
                    

                }else{
                    $arrDeductDiamond['status'] = 'unsuccess';
                    $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);
                }

                return response()->json([
                            'status' => false,
                            'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);

            }

            return [
                'status' => false,
                'message' => 'No data required.'
            ];
            
        }

        public function unlockSpecialReward(Request $request){
            $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            if (is_null($decoded)) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }
            $decoded = collect($decoded);

            if ($decoded->has('type') == false && $decoded['type'] != 'unlock_special_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // check unlock paid reward
            $unlockPaidRewardStatus = false;
            $unlockPaidRewardHistory = DeductLog::where('uid', $member->uid)
                                            ->where('deduct_type', 'unlock_paid_reward')
                                            ->count();
            if($unlockPaidRewardHistory>0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้ปลดล็อคของรางวัลพิเศษไปแล้ว'
                ]);
            }

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            if($diamonds < $this->unlock_paid_reward_diamonds){
                return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอ'
                ]);
            }else{

                $logDeduct = $this->addDeductLog([
                    'uid' => $uid,
                    'username' => $member->username,
                    'ncid' => $ncid,
                    'step' => 0,
                    'deduct_type' => 'unlock_paid_reward',
                    'diamonds' => $this->unlock_paid_reward_diamonds,
                    'status' => 'pending',
                    'log_date' => date('Y-m-d H:i:s'), // set to string
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]);

                // set deduct diamonds
                $deductData = $this->setDiamondDeductData($this->unlock_paid_reward_diamonds);
                $respDeductRaw = $this->deductDiamond($ncid, $deductData);
                $respDeduct = json_decode($respDeductRaw);
                if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){

                    $arrDeductDiamond = [
                        'before_deduct_diamond' => $diamonds,
                        'after_deduct_diamond' => $diamonds - $this->unlock_paid_reward_diamonds,
                        'deduct_status' => $respDeduct->status ?: 0,
                        'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
                        'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'success'
                    ];

                    $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);

                    $eventInfo = $this->setEventInfo($uid);
    
                    return response()->json([
                        'status' => true,
                        'message' => 'ปลดล็อคของรางัวลพิเศษสำเร็จ!',
                        'content' => $eventInfo
                    ]);
                    

                }else{
                    $arrDeductDiamond['status'] = 'unsuccess';
                    $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);
                }

                return response()->json([
                            'status' => false,
                            'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);

            }

            return [
                'status' => false,
                'message' => 'No data required.'
            ];

        }

        public function redeemReward(Request $request){
            $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            if (is_null($decoded)) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            }
            $decoded = collect($decoded);

            if ($decoded->has('type') == false && $decoded['type'] != 'redeem_reward') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            if ($decoded->has('step') == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'invalid parameter(2)!'
                ]);
            }

            if ($decoded->has('reward_type') == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'invalid parameter(3)!'
                ]);
            }

            $step = $decoded['step'];
            $reward_type = $decoded['reward_type']; // free, paid

            // $step = $request->step;
            // $reward_type = $request->reward_type; // free, paid

            if(!in_array($reward_type, ['free', 'paid'])){
                return response()->json([
                    'status' => false,
                    'message' => 'invalid parameter(4)!'
                ]);
            }

            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $questInfo = QuestLog::where('uid', $member->uid)
                                ->where('step', $step)
                                ->where('status', 'completed')
                                // ->where('reward_claimed', 0)
                                ->first();
            
            if(!isset($questInfo) && empty($questInfo->id)){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่มีภารกิจที่กำลังทำอยู่<br />หรือภารกิจของคุณยังไม่สำเร็จ'
                ]);
            }

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            // check quest completed
            // $questStatusInfo = $this->checkQuestStatusInfo($questInfo->id);
            
            // check unlock paid reward
            $unlockPaidRewardStatus = false;
            $unlockPaidRewardHistory = DeductLog::where('uid', $member->uid)
                                            ->where('deduct_type', 'unlock_paid_reward')
                                            ->count();
            if($unlockPaidRewardHistory>0){
                $unlockPaidRewardStatus = true;
            }

            // check unlock multiplied reward
            $multipliedRewardHistory = DeductLog::where('uid', $member->uid)
                                        ->where('step', $questInfo->step)
                                        ->where('deduct_type', 'multiplied_reward')
                                        ->where('status', 'success')
                                        ->count();

            $canClaimRewardStatus = false;
            $rewardHistory = 0;
            $reward_claim_count = 1;
            $rewardInfo = [];

            // check multiplied reward
            if($multipliedRewardHistory > 0){
                $reward_claim_count = 2;
            }

            // set reward info
            $rewards = $this->setRewardByQuestId($questInfo->quest_id, $questInfo->step);

            if($reward_type == 'paid'){

                $rewardHistory = ItemLog::where('uid', $member->uid)
                                            ->where('package_type', 'paid')
                                            ->where('quest_log_id', $questInfo->id)
                                            ->count();

                if($questInfo->status == 'completed' && $unlockPaidRewardStatus == true){
                    $canClaimRewardStatus = true;
                }

                $rewardInfo = $rewards['paid'];

            }else{
                
                $rewardHistory = ItemLog::where('uid', $member->uid)
                                            ->where('package_type', 'free')
                                            ->where('quest_log_id', $questInfo->id)
                                            ->count();
                if($questInfo->status == 'completed'){
                    $canClaimRewardStatus = true;
                }

                $rewardInfo = $rewards['free'];

            }

            if($canClaimRewardStatus == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณยังไม่ผ่านภารกิจ'
                ]);
            }

            if($rewardHistory >= $reward_claim_count){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปก่อนหน้านี้แล้ว'
                ]);
            }

            $goods_data = [];
            $goods_data[] = [
                'goods_id' => $rewardInfo['product_id'],
                'purchase_quantity' => $rewardInfo['product_quantity'],
                'purchase_amount' => 0,
                'category_id' => 40
            ];

            if (is_null($goods_data) == false && is_array($goods_data) && count($goods_data) > 0) {

                $itemLog = $this->addSendItemLog([
                    'uid' => $uid,
                    'username' => $questInfo->username,
                    'ncid' => $ncid,
                    'quest_log_id' => $questInfo->id,
                    'package_id' => $rewardInfo['product_id'],
                    'package_title' => $rewardInfo['product_title'],
                    'amount' => $rewardInfo['amount'],
                    'reward_key' => $rewardInfo['reward_key'],
                    'package_type' => $reward_type,
                    'status' => 'pending',
                    'send_item_status' => false,
                    'send_item_purchase_id' => 0,
                    'send_item_purchase_status' => 0,
                    'goods_data' => json_encode($goods_data),
                    'log_date' => date('Y-m-d H:i:s'), // set to string
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]);

                $send_result_raw = $this->dosendItem($ncid, $goods_data);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'success'
                    ], $itemLog['id'], $uid);

                    // update quest log
                    $questInfo->reward_claimed = 1;
                    $questInfo->save();
    
                    $eventInfo = $this->setEventInfo($uid);
    
                    return response()->json([
                        'status' => true,
                        'message' => 'ส่งของรางวัลเสร็จเรียบร้อย<br />กรุณาตรวจสอบของรางวัลที่กล่องจดหมายภายในเกม',
                        'content' => $eventInfo
                    ]);
                }else{
                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'unsuccess'
                    ], $itemLog['id'], $uid);

                    $questInfo->reward_claimed = 1;
                    $questInfo->save();
    
                    $eventInfo = $this->setEventInfo($uid);
    
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                        'content' => $eventInfo
                    ]);
                }

            }

            return [
                'status' => false,
                'message' => 'No data required.'
            ];

        }

    }