<?php

    namespace App\Http\Controllers\Api\Test\happy_xmas2018;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DB;
    use Validator;

    use App\Models\Test\happy_xmas2018\Member;
    use App\Models\Test\happy_xmas2018\Reward;
    use App\Models\Test\happy_xmas2018\Play;
    use App\Models\Test\happy_xmas2018\PlayData;
    use App\Models\Test\happy_xmas2018\Exchange;

    class IndexController extends BnsEventController{

    	private $baseApi = 'http://api.apps.garena.in.th';
        private $startTime = '2018-12-12 08:00:00';
        private $endTime = '2019-01-16 23:59:50';

        private $startGetApiTime = '00:00:00';
        private $endGetApiTime = '23:59:59';

        private $awtSecret = 'A7vcUVevemt68R9Iv6Opn6Iw69m9MjNM';

        private $diamondStartTime = '2018-12-12 08:00:00';
        private $diamondEndTime = '2019-01-16 23:59:50';

        public function __construct()
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getUserData();

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
             if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            // $this->userData = $this->getUserData();
            // dd($this->userData);
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'total_play' => 0,
                    'last_ip' => $this->getIP()
                ];
                // dd(12);
                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $id)
        {
            if(empty($id))
                return false;

            return Member::where('id', $id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:HAPPY_XMAS_2018:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events bns Happy Xmas 2018.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            Play::where('id', $log_id)->where('uid', $uid)->update($arr);
            PlayData::where('play_id', $log_id)->where('uid', $uid)->update($arr);
            return true;
        }
        private function updateItemExchange($arr, $log_id, $uid) {
            Exchange::where('id', $log_id)->where('uid', $uid)->update($arr);
            return true;
        }

        private function checkItemHistory($uid,$package_id){
            $itemHistory = ItemLog::where('uid', $uid)
                                ->where('package_id', $package_id)
                                ->count();
            return $itemHistory;
        }


        public function postIndex(Request $request){
            $validator = Validator::make($request->all(), [
                'service' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status'=>false,
                    'message'=>':)'
                ]);
            }

            $service=$request->service;
            switch ($service) {
                case 'event_info':
                    return response()->json($this->getEventInfo($request));
                    break;
                case 'play':
                    return response()->json($this->playGacha($request));
                    break;
                case 'exchange':
                    return response()->json($this->exchangeItem($request));
                    break;
                default:
                    return response()->json([
                        'status'=>false,
                        'message'=>'ไม่พบข้อมูลที่ต้องการ'
                    ]);
                    break;
            }

        }


        private function setEventInfo($uid){
            if(empty($uid)){
                return false;
            }
            $member = Member::where('uid', $uid)->first();
            $today=Play::getPlayToday($uid);
            $canplay=$today>0 ? false : true;

            $can_get_special=$member->total_play>=10 ? true : false;
            $chk_special=Exchange::where('uid',$uid)->first();
            $received_special=isset($chk_special) ? true :false;

            $reward = new Reward;

            $gachas = [];
            for($i=1;$i<=count($reward->gachaList());$i++){

                $packageInfo = [];
                $packageInfo = $reward->setGachaByKey($i);

                $gachas[] = [
                    'package_id' => $packageInfo['package_id'],
                    'package_title' => $packageInfo['package_title'],
                    'package_desc' => $packageInfo['package_desc'],
                ];

            }


            return [
                'total_play' => $member->total_play,
                'canplay'=>$canplay,
                'can_get_special'=>$can_get_special,
                'received_special'=>$received_special,
                'gachas'=>$gachas,

            ];
        }

        private function getEventInfo($request){

            $decoded = $request;

            $uid = $this->userData['uid'];
            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return [
                            'status' => false,
                            'message' => 'No member data.'
                ];
            }

            $eventInfo = $this->setEventInfo($uid);

            return [
                        'status' => true,
                        'message'=>'success',
                        'data' => $eventInfo,
                    ];

        }

        public function playGacha(Request $request){

            $decoded = $request;

            $userData = $this->getUserData();
            $uid = $userData['uid'];

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return [
                            'status' => false,
                            'message' => 'No member data.'
                ];
            }

            $today=Play::getPlayToday($uid);
            if($today>0){
                return [
                    'status' => false,
                    'message' => 'เล่นกิจกรรมได้วันละ 1 ครั้งเท่านั้น'
                ];
            }

            $gacha_item=$this->gachaRandom();
            foreach ($gacha_item['product_set'] as $key_pro => $product) {
                $goods_data[] = [
                    'goods_id' => $product['product_id'],
                    'purchase_quantity' => $product['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];
            }
            $send_item_data=collect($goods_data);

            $play_add=new Play;
            $play_add->ncid=$member->ncid;
            $play_add->uid=$member->uid;
            $play_add->package_id=$gacha_item['package_id'];
            $play_add->status='pending';
            if($play_add->save()){
                $member->total_play=$member->total_play+1;
                $member->save();

                $play_id=$play_add->id;

                foreach($gacha_item['product_set'] as $key=>$value){
                    $play_data=new PlayData;
                    $play_data->play_id=$play_id;
                    $play_data->ncid=$member->ncid;
                    $play_data->uid=$member->uid;
                    $play_data->package_id=$gacha_item['package_id'];
                    $play_data->item_id=$value['product_id'];
                    $play_data->item_name=$value['product_title'];
                    $play_data->item_amount=$value['product_quantity'];
                    $play_data->item_type=$value['product_type'];
                    $play_data->status='pending';
                    $play_data->send_item_status='pending';
                    $play_data->good_data=json_encode($goods_data);
                    $play_data->log_date=(string)date('Y-m-d');
                    $play_data->log_date_timestamp=time();
                    $play_data->save();

                }
            }else{
                return [
                            'status' => false,
                            'message' => "ไม่สามารถเล่นได้ โปรดลองใหม่อีกครั้ง",
                    ];
            }

            //send item

            // $send_result_raw = $this->dosendItem($member->ncid, $send_item_data->all());
            // $send_result = json_decode($send_result_raw);
            // if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) && $send_result->status ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'success'
                ],$play_id, $member->uid);

                $eventInfo = $this->setEventInfo($member->uid);

                return [
                            'status' => true,
                            'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                            'gacha'=>$gacha_item['package_title'],
                            'data' => $eventInfo
                        ];
            // }else{
            //     $this->updateItemHistoryLog([
            //         'send_item_status' => isset($send_result->status) ? $send_result->status : false,
            //         'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
            //         'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
            //         'status' => 'unsuccess'
            //             ], $play_id, $member->uid);
            //
            //     $eventInfo = $this->setEventInfo($member->uid);
            //
            //     return [
            //                 'status' => false,
            //                 'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
            //                 'data' => $eventInfo
            //             ];
            // }

        }

        private function gachaRandom(){
            $random_num=rand(0,100000);
            $rewardlist=Reward::gachaList();
            foreach($rewardlist as $key=>$value){
                if($random_num>=$value['rate_min'] && $random_num<=$value['rate_max']){
                    return $rewardlist[$key];
                }
            }
        }

        public function exchangeItem(Request $request){

            $userData = $this->getUserData();
            $uid = $userData['uid'];

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return [
                            'status' => false,
                            'message' => 'No member data.'
                ];
            }

            if($member->total_play<10){
                return [
                            'status' => false,
                            'message' => 'จำนวนดาวยังไม่ครบ 10 ดวง'
                ];
            }

            $chkEx=Exchange::where('uid',$uid)->first();
            if(isset($chkEx)){
                return [
                        'status' => false,
                        'message' => 'รับได้ 1 ครั้งเท่านั้น'
                ];
            }

            $reward = new Reward;
            $packageInfo = $reward->fixList();

            $goods_data = []; //good data for send item group

            $productList = $packageInfo;

            foreach ($productList as $key => $product) {
                $goods_data[] = [
                    'goods_id' => $product['product_id'],
                    'purchase_quantity' => $product['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];
            }

            $ex_add=new Exchange;
            $ex_add->ncid=$member->ncid;
            $ex_add->uid=$member->uid;
            $ex_add->item_id=$packageInfo[0]['product_id'];
            $ex_add->item_name=$packageInfo[0]['product_title'];
            $ex_add->item_amount=$packageInfo[0]['product_quantity'];
            $ex_add->good_data=json_encode($goods_data);
            $ex_add->status='pending';
            $ex_add->send_item_status='pending';

            if($ex_add->save()){
                $exchange_id=$ex_add->id;

                //send item
                // $send_result_raw = $this->dosendItem($member->ncid, $goods_data);
                // $send_result = json_decode($send_result_raw);
                // if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                    $this->updateItemExchange([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                        'status' => 'success'
                    ], $exchange_id, $member->uid);

                    $eventInfo = $this->setEventInfo($member->uid);

                    return [
                                'status' => true,
                                'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                                'data' => $eventInfo
                            ];
            //     }else{
            //         $this->updateItemExchange([
            //             'send_item_status' => isset($send_result->status) ? $send_result->status : false,
            //             'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
            //             'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
            //             'status' => 'unsuccess'
            //                 ], $exchange_id, $member->uid);
            //
            //         $eventInfo = $this->setEventInfo($member->uid);
            //
            //         return [
            //                     'status' => false,
            //                     'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
            //                     'data' => $eventInfo
            //                 ];
            //     }
            }else{
                return [
                            'status' => false,
                            'message' => "ไม่สามารถรับไอเท็มได้ โปรดลองใหม่อีกครั้ง",
                        ];
            }


        }

    }
