<?php

namespace App\Http\Controllers\Api\Test\songkran2020;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

use App\Models\Test\songkran2020\Member;
use App\Models\Test\songkran2020\Reward;
use App\Models\Test\songkran2020\SendItemLog;
use App\Models\Test\songkran2020\QuestLog;

class IndexController extends BnsEventController
{

    private $baseApi = 'http://api.apps.garena.in.th';
    private $startTime = '2020-04-05 06:00:00';
    private $endTime = '2020-04-28 23:59:59';
    private $newItemCanRedeem = '2020-04-15 00:00:00';
    private $userEvent = '';

    private $questList = [
        [
            'id'   => 25227,
            'coin' => 50,
            'name' => "อาวุธแห่งการทำลายล้างครั้งสุดท้าย",
        ],
        [
            'id'   => 25222,
            'coin' => 40,
            'name' => "ความทะนงแห่งสายฟ้าที่เหลืออยู่",
        ],
        [
            'id'   => 25215,
            'coin' => 40,
            'name' => "จุดจบของความปรารถนาที่ไม่เป็นจริง",
        ],
        [
            'id'   => 25185,
            'coin' => 35,
            'name' => "ลึกลงไปในกรุสมบัติ",
        ],
        [
            'id'   => 25178,
            'coin' => 35,
            'name' => "ลำนำแห่งชาวเรือ",
        ],
        [
            'id'   => 25147,
            'coin' => 25,
            'name' => "ผู้เฝ้ามองอมตะ",
        ],
        [
            'id'   => 25144,
            'coin' => 25,
            'name' => "เปลวเพลิงที่ไม่มีวันดับ",
        ],
    ];


    public function __construct(Request $request)
    {
        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);
        $this->checkEventStatus();

    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status'  => false,
                'type'    => 'no_permission',
                'message' => 'Sorry, you do not have permission.',
            ]));
        }
        // dd($this->startTime,$this->endTime);
        if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
            die(json_encode([
                'status'  => false,
                'type'    => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม',
            ]));
        }

//        if ($this->checkIsMaintenance()) {
//            die(json_encode([
//                'status'  => false,
//                'type'    => 'maintenance',
//                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์',
//            ]));
//        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง',
                ]));
            }
        } else {
            die(json_encode([
                'status'  => false,
                'type'    => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน',
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function is_accepted_ip()
    {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'             => $uid,
                'username'        => $username,
                'ncid'            => $ncid,
                'character_id'    => 0,
                'character_name'  => "",
                'quest_success_1' => 0,
                'quest_success_2' => 0,
                'quest_success_3' => 0,
                'quest_success_4' => 0,
                'quest_success_5' => 0,
                'quest_success_6' => 0,
                'quest_success_7' => 0,
                'coin'            => 0,
                'coin_used'       => 0,
                'last_ip'         => $this->getIP(),
                'last_update'     => null,
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
            $this->userEvent = $resp;

        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id)) {
            return false;
        }

        return Member::where('_id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:LANTERN_FESTIVAL:ASSOCIATE_UID_WITH_NCID_'.$uid, 10,
            function () use ($uid, $username) {
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'user_info',
                    'uid'      => $uid,
                ]);
                $userNcInfo       = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }

            });
    }

    private function dosendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Sending item from garena events bns Lantern festival 2019.',
            'goods'                => json_encode($goodsData),
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function doGetChar($uid, $ncid)
    {
        $resp_raw = $this->apiCharacters($uid, $ncid);

        $resp = json_decode($resp_raw);
        if (is_object($resp)) {
            if ($resp->status) {
                $content = [];
                $i       = 1;
                foreach ($resp->response as $key => $value) {
                    $content[] = [
                        'char_id'   => $value->id,
                        'char_name' => $value->name,
                    ];

                    $i++;
                }
                return $content;
            }
        }
        return null;
    }

    private function addItemHistoryLog($arr)
    {
        return SendItemLog::create($arr);
    }

    // API GET EVENT INFO

    /**
     * API ROUTE : Get Event Info and check member
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEventInfo(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter!',
            ]);
        }

        $userData = $this->userData;
        $uid      = $userData['uid'];

        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status'  => false,
                'message' => 'No member data.',
            ]);
        }

        $eventInfo = $this->setEventInfo($uid);

        return response()->json([
            'status' => true,
            'data'   => $eventInfo,
        ]);

    }

    /**
     * Check member is select char and return chars
     * @param $uid
     * @return array|bool
     */
    private function setEventInfo($uid)
    {
        if (empty($uid)) {
            return false;
        }
        $member = Member::where('uid', $uid)->first();
        if ($member->character_id == 0 || $member->character_id == null) {

            $mychar = $this->doGetChar($member->uid, $member->ncid);

            return [
                'username'       => $member->username,
                'character_name' => '',
                'selected_char'  => false,
                'characters'     => $mychar,
                'itemLists'      => $this->getItemLists(),
                'coin'           => 0,
                'show_item'      => true,
            ];
        }

        $this->summaryCoin();

        return [
            'username'       => $member->username,
            'character_name' => $member->character_name,
            'selectd_char'   => true,
            'characters'     => [],
            'coin'           => $this->getCoin(),
            'itemLists'      => $this->getItemLists(),
            'show_item'      => Carbon::now() > Carbon::parse($this->newItemCanRedeem) ? false : true,
        ];
    }

    /**
     * API ROUTE : Select char and save to call summary user first time.
     * @param  Request  $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function selectCharacter(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'select_character') {//check parameter
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter!',
            ]);
        }
        if ($decoded->has('id') == false) {//check parameter
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter! (2)',
            ]);
        }

        $char_id = (int) $decoded['id'];

        $userData = $this->userData;
        $uid      = $userData['uid'];

        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status'  => false,
                'message' => 'ไม่พบข้อมูล',
            ]);
        }

        if ($member->character_id > 0 || $member->character_id != null) {
            return response()->json([
                'status'  => false,
                'message' => 'ไม่สามารถเปลี่ยนตัวละครได้',
            ]);
        }

        $mychar   = $this->doGetChar($member->uid, $member->ncid);
        $myselect = collect($mychar)->where('char_id', $char_id);

        if (count($myselect) == 0) {
            return response()->json([
                'status'  => false,
                'message' => 'ไม่พบตัวละคร',
            ]);
        }
        $member->character_id   = (int) $myselect->first()['char_id'];
        $member->character_name = $myselect->first()['char_name'];
        $member->save();

        $this->userEvent = $member;
        $this->userEvent->save();

        return [
            'status' => true,
            'data'   => [
                'username'       => $member->username,
                'character_name' => $member->character_name,
                'selectd_char'   => true,
                'characters'     => [],
                'coin'           => $this->getCoin(),
                'itemLists'      => $this->getItemLists(),
            ],
        ];

    }

    /**
     * API ROUTE : Request redeem reward and check any condition before send item
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function redeemReward(Request $request)
    {

        $decoded = $request;

        if ($decoded->has('type') == false || $decoded['type'] != 'redeem') {//check parameter
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter! (1)',
            ]);
        }

        $userData = $this->userData;
        $uid      = $userData['uid'];

        if ($decoded->has('token') == false) {
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter! (2)',
            ]);
        }

        $token = (int) $decoded['token'];
        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status'  => false,
                'message' => 'No member data.',
            ]);
        }

        if ($member->character_id == 0) {
            return response()->json([
                'status'  => false,
                'message' => 'โปรดเลือกตัวละครก่อน',
            ]);
        }

        $reward = Reward::whereToken($token)->first();
        if (!$reward) {
            return response()->json([
                'status'  => false,
                'message' => 'ขออภัย<br />ไม่พบไอเทม',
            ]);
        }

        $this->summaryCoin();
        $coin = $this->getCoin();

        if ($coin < $reward->coin) {
            return response()->json([
                'status'  => false,
                'message' => 'ขออภัย<br />เหรียญไม่เพียงพอ',
            ]);
        }

        if ($reward->limit != 0) {
            if (!$this->checkLimit($reward->product_id)) {
                return response()->json([
                    'status'  => false,
                    'message' => 'ขออภัย<br />ไม่สามารถแลกได้มากกว่า '.$reward->limit.' ชิ้น',
                ]);
            }
        }

        if ((int) $reward->new_item == 2) {
            $check = $this->checkLimitPerday($reward);
            if ($check === false){
                return response()->json([
                    'status'  => false,
                    'message' => 'ขออภัย<br />สามารถแลกได้ 1 ชิ้น / วัน',
                ]);
            }
        }

        $goods_data[] = [
            'goods_id'          => $reward->product_id,
            'purchase_quantity' => 1,
            'purchase_amount'   => 0,
            'category_id'       => 40,
        ];


        $sendItemLog = $this->addItemHistoryLog([
            'uid'                => $member->uid,
            'ncid'               => $member->ncid,
            'product_id'         => $reward->product_id,
            'product_title'      => $reward->name,
            'product_quantity'   => 1,
            'coin'               => $reward->coin,
            'log_date'           => (string) date('Y-m-d'),
            'log_date_timestamp' => time(),
            'status'             => 'pending',
            'goods_data'         => json_encode($goods_data),
            'last_ip'            => $this->getIP(),
        ]);

        $send_result_raw = $this->dosendItem($member->ncid, $goods_data);
        $send_result     = json_decode($send_result_raw);

        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

            $sendItemLog->send_item_status          = $send_result->status ? $send_result->status : false;
            $sendItemLog->send_item_purchase_id     = $send_result->response->purchase_id ? $send_result->response->purchase_id : 0;
            $sendItemLog->send_item_purchase_status = $send_result->response->purchase_status ? $send_result->response->purchase_status : 0;
            $sendItemLog->status                    = "success";
            $sendItemLog->send_item_respone         = $send_result_raw;
            $sendItemLog->save();

            $this->summaryCoin();

            return response()->json([
                'status'  => true,
                'message' => "ได้รับ ".$reward->name." x1<br />ไอเทมส่งเข้าไปในกล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อรับของรางวัล",
                'data'    =>
                    [
                        'coin'      => $this->getCoin(),
                        'itemLists' => $this->getItemLists(),
                    ],
            ]);

        } else {
            $sendItemLog->send_item_status          = $send_result->status ? $send_result->status : false;
            $sendItemLog->send_item_purchase_id     = 0;
            $sendItemLog->send_item_purchase_status = 0;
            $sendItemLog->send_item_respone         = $send_result_raw;
            $sendItemLog->status                    = "unsuccess";
            $sendItemLog->save();

            return response()->json([
                'status'  => false,
                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                'data'    => [
                    'coin' => $this->getCoin(),
                ],
            ]);
        }

    }

    /**
     * Summary user after any action
     */
    public function summaryUserEvent()
    {
        if ($this->userEvent->character_id != 0) {
            $dates = $this->generateDateRange();
            foreach ($dates as $date) {
                $this->checkQuestByDate($date);
            }

            $this->summaryCoin();

            $this->userEvent->last_update = Carbon::now();
            $this->userEvent->save();
        }

    }

    /**
     * Check quest by date
     * @param $date
     * @return bool|mixed
     */
    public function checkQuestByDate($date)
    {
        if (!$this->userEvent->uid) {
            return false;
        }

        $dateLog = $date;

        if (time() >= strtotime($dateLog.' 00:00:00') && time() <= strtotime($dateLog.' 05:59:59')) {
            $dateLog = Carbon::parse($dateLog)->subDay(1)->format("Y-m-d");
        }

        $questLogs = QuestLog::whereUid($this->userEvent->uid)
            ->whereCharId($this->userEvent->character_id)
            ->whereDateLog($dateLog)
            ->first();

        if ($questLogs) {
            if ($this->userEvent->last_update != null) {
                $check_update = Carbon::parse($this->userEvent->last_update)->subDay(1);
                if ($check_update > Carbon::parse($date)) {
                    return false;
                }
            }
        }

        if (!$questLogs) {
            $questLogs            = new QuestLog();
            $questLogs->uid       = $this->userEvent->uid;
            $questLogs->username  = $this->userEvent->username;
            $questLogs->ncid      = $this->userEvent->ncid;
            $questLogs->char_id   = $this->userEvent->character_id;
            $questLogs->char_name = $this->userEvent->character_name;
            $questLogs->date_log  = $dateLog;
            $questLogs->save();
        }

        $uid = $this->userEvent->uid;

        $cache = Cache::remember('BNS:LANTERN_FESTIVAL:USER_'.$uid.':DATE_'.$date, 2,
            function () use ($uid, $date, $questLogs) {

                if (time() >= strtotime($date.' 00:00:00') && time() <= strtotime($date.' 05:59:59')) {
                    $start = Carbon::parse($date)->subDay(1)->format("Y-m-d 06:00:00");
                    $end   = Carbon::parse($date)->format("Y-m-d 05:59:59");
                } else {
                    $start = Carbon::parse($date)->format("Y-m-d 06:00:00");
                    $end   = Carbon::parse($date)->addDay(1)->format("Y-m-d 05:59:59");
                }

                $quests = collect($this->questList);
                $arr    = [];
                $coin   = 0;
                foreach ($quests as $key => $quest) {

                    $key     = $key + 1;
                    $var     = "quest_success_".$key;
                    $questId = $quest['id'];
                    $check   = $this->getQuestComplete($questId, $start, $end);
                    if ($check > 0) {
                        $check = 1;
                    }

                    if ($check == 1) {
                        $coin += $quest['coin'];
                    }
                    $arr[$var] = $check;

                }
                $arr['coin'] = $coin;
                $questLogs->update($arr);

                $arr['start_api'] = $start;
                $arr['end_api']   = $end;

                return $arr;
            });

        return $cache;
    }

    /**
     * Game API Helper
     * @param $uid
     * @param $ncid
     * @return mixed
     */
    private function apiCharacters($uid, $ncid)
    {
        return Cache::remember('BNS:LANTERN_FESTIVAL:CHARACTERS_'.$uid, 10, function () use ($ncid) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'characters',
                'user_id'  => $ncid,
            ]);
        });
    }


    /**
     * Game API Helper
     * @param $uid
     * @param $charId
     * @param $questId
     * @param $start
     * @param $end
     * @return mixed
     */
    private function apiCheckQuestCompleted($uid, $charId, $questId, $start, $end)
    {
        $resp = $this->post_api($this->baseApi, [
            'key_name'   => 'bns',
            'service'    => 'quest_completed',
            'char_id'    => $charId,
            'quest_id'   => $questId,
            'start_time' => $start,
            'end_time'   => $end,
        ]);
        return json_decode($resp);
    }

    private function getItemLists()
    {
        $items = Reward::orderBy('id')
            ->select('name', 'image', 'coin', 'limit', 'token', 'product_id', 'new_item')
            ->get();

        $return = [];
        foreach ($items as $item) {
            if ($this->userEvent) {
                if ($this->userEvent->character_id == 0 || $this->userEvent->character_id == null) {
                    $item->canRedeem = false;
                } else {
                    if ($item->coin <= $this->getCoin()) {

                        // item per day
                        if ((int) $item->new_item == 2) {
                            $check = $this->checkLimitPerday($item);
                            if ($check === true) {
                                $item->canRedeem = true;
                            } else {
                                $item->canRedeem = false;
                            }
                            $return[] = [
                                'name'      => $item->name,
                                'image'     => $item->image,
                                'coin'      => $item->coin,
                                'limit'     => $item->limit,
                                'canRedeem' => $item->canRedeem,
                                'token'     => $item->token,
                            ];
                            continue;
                        }

                        // limit check
                        if ($item->limit != 0) {
                            if ($this->checkLimit($item->product_id)) {
                                $item->canRedeem = true;
                            } else {
                                $item->canRedeem = false;
                            }
                        } else {
                            $item->canRedeem = true;
                        }

                        // new item check
                        if ((int) $item->new_item == 1) {
                            if (Carbon::now() > Carbon::parse($this->newItemCanRedeem)) {
                                $item->canRedeem = true;
                            } else {
                                $item->canRedeem = false;
                            }
                        }

                    } else {
                        $item->canRedeem = false;
                    }
                }
            }

            $return[] = [
                'name'      => $item->name,
                'image'     => $item->image,
                'coin'      => $item->coin,
                'limit'     => $item->limit,
                'canRedeem' => $item->canRedeem,
                'token'     => $item->token,
            ];
        }

        return $return;
    }

    /**
     * Get real coin after calculate used coin.
     * @return int
     */
    private function getCoin()
    {
        if ($this->userEvent) {
            $coin = ($this->userEvent->coin - $this->userEvent->coin_used);
            if ($coin <= 0) {
                return 0;
            }
            return $coin;
        }
        return 0;
    }

    public function getQuestComplete($questId, $start, $end)
    {
        if ($this->userEvent && $this->userEvent->character_id != 0) {

            $uid     = $this->userEvent->uid;
            $char_id = $this->userEvent->character_id;

            $check = $this->apiCheckQuestCompleted($uid, $char_id, $questId, $start, $end);
            if (is_null($check) == false && is_object($check) && $check->status) {
                return $check->response->quest_completed_count;
            } else {
                return 0;
            }
        }

        return 0;
    }

    /**
     * Generate date range from created at until today
     * Not include timestamp condition
     * @return array
     */
    private function generateDateRange()
    {
        $start_date = Carbon::parse($this->userEvent->created_at);
        $end_date   = Carbon::today()->addDay(1);
        $dates      = [];

        for ($date = $start_date->copy(); $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
    }

    public function summaryCoin()
    {
        if ($this->userEvent && $this->userEvent->character_id != 0) {

            $sum_coin = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('coin');

            $sum_coin_used = (int) SendItemLog::whereUid($this->userEvent->uid)
                ->whereStatus('success')
                ->sum('coin');

            $this->userEvent->quest_success_1 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_1');
            $this->userEvent->quest_success_2 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_2');
            $this->userEvent->quest_success_3 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_3');
            $this->userEvent->quest_success_4 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_4');
            $this->userEvent->quest_success_5 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_5');
            $this->userEvent->quest_success_6 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_6');
            $this->userEvent->quest_success_7 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_7');

            $this->userEvent->coin      = $sum_coin;
            $this->userEvent->coin_used = $sum_coin_used;
            $this->userEvent->save();
        }
    }

    public function checkLimit($item)
    {
        $item = Reward::whereProductId($item)->first();
        if (!$item) {
            return false;
        }

        if ($this->userEvent && $this->userEvent->character_id != 0) {
            $send_item = SendItemLog::whereUid($this->userEvent->uid)
                ->whereProductId($item->product_id)
                ->whereStatus('success')->get();

            if ((int) $send_item->count() < (int) $item->limit) {
                return true;
            }
        }

        return false;

    }

    public function checkLimitPerday($item)
    {
        if ($this->userEvent && $this->userEvent->character_id != 0) {
            $today     = Carbon::today()->toDateString();
            $send_item = SendItemLog::whereUid($this->userEvent->uid)
                ->whereProductId($item->product_id)
                ->whereLogDate($today)
                ->count();

            if ((int) $send_item >= 1) {
                return false;
            }
        }

        return true;

    }

    public function getHistory(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || !in_array($decoded['type'], ['getHistory'])) {
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter!',
            ]);
        }

        // GET RECEIVED HISTORY
        $return_received = [];
        $logs            = QuestLog::whereUid($this->userEvent->uid)->orderBy('date_log', 'desc')->get();
        foreach ($logs as $log) {
            $date = Carbon::parse($log->date_log)->format("d/m/Y");
            for ($i = 1; $i <= 7; $i++) {
                $no    = $i - 1;
                $quest = (int) $log->{'quest_success_'.$i} ?? 0;
                if ($quest > 0) {
                    $return_received[] = [
                        'coin'     => $quest * $this->questList[$no]['coin'],
                        'quest'    => $this->questList[$no]['name'],
                        'date_log' => $date,
                    ];
                }

            }
        }

        // GET REDEEM HISTORY
        $return_redeem = [];
        $redeems       = SendItemLog::whereUid($this->userEvent->uid)->orderBy('id', 'desc')->select('created_at',
            'coin', 'product_title')->get();
        foreach ($redeems as $redeem) {
            $dt              = Carbon::parse($redeem->created_at);
            $return_redeem[] = [
                'coin' => $redeem->coin,
                'item' => $redeem->product_title,
                'date' => $dt->format("d/m/Y"),
                'time' => $dt->format("H:i:s"),
            ];
        }

        return response()->json([
            'status' => true,
            'data'   => [
                'received' => $return_received,
                'redeem'   => $return_redeem,
            ],
        ]);

    }

}
