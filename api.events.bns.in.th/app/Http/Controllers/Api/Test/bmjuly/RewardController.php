<?php

    namespace App\Http\Controllers\Api\Test\bmjuly;

    class RewardController {

        public function getRandomRewardInfo() {

            $rewards = $this->getSpecialRewardsList();

            $random = rand(1, 100000);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($rewards as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            // now we have the reward
            return $reward;
        }

        public function getSpecialRewardsList() {
            return [
                [
                    'id' => 1,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลอัญมณีหยินหยาง x1',
                    'product_id' => 1900,
                    'product_quantity' => 1,
                    'chance' => 18.5,
                    'icon' => 'gacha_1',
                    'key' => 'gacha_1',
                    'stock'=>false
                ],
                [
                    'id' => 2,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลอัญมณีหยินหยาง x2',
                    'product_id' => 2407,
                    'product_quantity' => 1,
                    'chance' => 15,
                    'icon' => 'gacha_2',
                    'key' => 'gacha_2',
                    'stock'=>false
                ],
                [
                    'id' => 3,
                    'item_type' => 'object',
                    'product_title' => 'หินเปลี่ยนรูปชั้นสูง x3',
                    'product_id' => 2408,
                    'product_quantity' => 1,
                    'chance' => 15,
                    'icon' => 'gacha_3',
                    'key' => 'gacha_3',
                    'stock'=>false
                ],
                [
                    'id' => 4,
                    'item_type' => 'object',
                    'product_title' => 'กรุฮงมุน',
                    'product_id' => 807,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'gacha_4',
                    'key' => 'gacha_4',
                    'stock'=>false
                ],
                [
                    'id' => 5,
                    'item_type' => 'object',
                    'product_title' => 'กรุพิศวง x3',
                    'product_id' => 2426,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'gacha_5',
                    'key' => 'gacha_5',
                    'stock'=>false
                ],
                [
                    'id' => 6,
                    'item_type' => 'object',
                    'product_title' => 'คริสตัลวิญญาณฮงมุน',
                    'product_id' => 1896,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'gacha_6',
                    'key' => 'gacha_6',
                    'stock'=>false
                ],
                [
                    'id' => 7,
                    'item_type' => 'object',
                    'product_title' => 'ถุงเพชรแปดเหลี่ยม',
                    'product_id' => 1731,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'gacha_7',
                    'key' => 'gacha_7',
                    'stock'=>false
                ],
                [
                    'id' => 8,
                    'item_type' => 'object',
                    'product_title' => 'กรุฮงมุนที่ส่องสว่าง',
                    'product_id' => 806,
                    'product_quantity' => 1,
                    'chance' => 4.5,
                    'icon' => 'gacha_8',
                    'key' => 'gacha_8',
                    'stock'=>false
                ],
                [
                    'id' => 9,
                    'item_type' => 'object',
                    'product_title' => 'ชุดเงาจันทร์',
                    'product_id' => 2406,
                    'product_quantity' => 1,
                    'chance' => 4.5,
                    'icon' => 'gacha_9',
                    'key' => 'gacha_9',
                    'stock'=>false
                ],
                [
                    'id' => 10,
                    'item_type' => 'object',
                    'product_title' => 'แพ็คเก็จโอลิมเปียแห่งอิสรภาพ',
                    'product_id' => 2421,
                    'product_quantity' => 1,
                    'chance' => 2.5,
                    'icon' => 'gacha_10',
                    'key' => 'gacha_10',
                    'stock'=>true
                ],

            ];
        }

    }
