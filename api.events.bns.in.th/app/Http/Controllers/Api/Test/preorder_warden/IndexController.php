<?php

    namespace App\Http\Controllers\Api\Test\preorder_warden;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\Test\preorder_warden\Member;
    use App\Models\Test\preorder_warden\Reward;
    use App\Models\Test\preorder_warden\ItemCode;
    use App\Models\Test\preorder_warden\ItemLog;
    use App\Models\Test\preorder_warden\DeductLog;
    use App\Models\Test\preregis_warden\Member as PreRegisterMember;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2019-01-02 00:00:00';
        private $endTime = '2019-02-02 23:59:59';

        // private $startPreorder = '2019-01-09 06:00:00';
        // private $endPreorder = '2019-01-16 05:59:59';

        // private $startSell = '2019-01-16 06:00:00';
        // private $endSell = '2019-02-02 23:59:59';

        private $startApiTime = '00:00:00';
        private $endApiTime = '23:59:59';

        private $package_diamonds = 2; // 120000
        private $package_diamonds_discount = 1; // 100000

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:WARDEN_PREORDER:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData) {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from BNS promotion Pre-Order Warden Package.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addSendItemLog(array $arr) {
            if ($arr) {
                $resp = ItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateSendItemLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function diamondBalance(int $uid) {
            return $this->post_api($this->baseApi, [
                        'key_name' => 'bns',
                        'service' => 'balance',
                        'uid' => $uid,
            ]);
        }

        private function setRemainDiamonds($uid){
            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return 0;
            }else{
                return intval($diamondsBalance->balance);
            }
        }

        private function setDiamondDeductData($deduct_diamond_amount=0)
        {
            if($deduct_diamond_amount==0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test deduct diamomds for garena BNS promotion Pre-Order Warden Package.',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addDeductLog(array $arr) {
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductLog(array $arr, $logId) {
            if ($arr) {
                return DeductLog::where('id', $logId)->update($arr);
            }
            return null;
        }

        public function getEventInfo(Request $request)
        {

            if ($request->has('type') == false && $request->type != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            if($eventInfo == false){
                $eventInfo = [];
            }

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
            ]);

        }

        private function setEventInfo($uid=null){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            // check member can get discount
            $discount = false;
            $checkPreRegis = PreRegisterMember::where('uid', $member->uid)->where('package_discount', 1)->count();
            if($checkPreRegis > 0){
                $discount = true;
            }

            // check purchase history
            $purchaseHistory = DeductLog::where('uid', $member->uid)->count();

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            // set diamonds purchase required
            $diamondsRequired = $discount ? $this->package_diamonds_discount : $this->package_diamonds;

            // check not use item code
            $notUseItemCode = ItemCode::where('status', 'not_use')->count();

            // check purchased item code.
            $myItemCode = ItemCode::where('uid', $member->uid)->first();

            return [
                'username' => $member->username,
                'discount' => $discount,
                'package_price' => $diamondsRequired,
                'can_purchase' => $purchaseHistory == 0 && $diamonds >= $diamondsRequired && $notUseItemCode > 0,
                'has_item_code_stock' => $notUseItemCode > 0,
                'already_purchase' => $purchaseHistory > 0,
                'my_code' => isset($myItemCode->code) ? $myItemCode->code : "",
            ];
        }
        
        public function purchasePackage(Request $request) {

            if ($request->has('type') == false && $request->type != 'purchase_package') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }

            if ($request->has('package_id') == false || !in_array($request->package_id, [1])) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (2)!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);
            $package_id = $request->package_id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfoCondition = $this->setEventInfo($uid);

            if($eventInfoCondition['can_purchase'] == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไดมอนด์ของคุณไม่พอซื้อแพ็คเกจ'
                ]);
            }

            if($eventInfoCondition['has_item_code_stock'] == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนแพ็คเกจหมด'
                ]);
            }

            if($eventInfoCondition['already_purchase'] == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />สามารถซื้อแพ็คเกจ 1 ครั้งต่อ 1 ไอดี'
                ]);
            }

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            // set deduct diamonds
            $logDeduct = $this->addDeductLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'discount' => $eventInfoCondition['discount'] ? 1 : 0,
                'diamonds' => $eventInfoCondition['package_price'],
                'status' => 'pending',
                'log_date' => date('Y-m-d H:i:s'), // set to string
                'log_date_timestamp' => time(),
                'last_ip' => $this->getIP(),
            ]);

            // set deduct diamonds
            $deductData = $this->setDiamondDeductData($eventInfoCondition['package_price']);
            $respDeductRaw = $this->deductDiamond($ncid, $deductData);
            $respDeduct = json_decode($respDeductRaw);
            if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){
                // update deduct log
                $arrDeductDiamond = [
                    'before_deduct_diamond' => $diamonds,
                    'after_deduct_diamond' => $diamonds - $eventInfoCondition['package_price'],
                    'deduct_status' => $respDeduct->status ?: 0,
                    'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
                    'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
                    'deduct_data' => json_encode($deductData),
                    'status' => 'success'
                ];

                $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);

                // get not use item code and update
                DB::connection('events_bns_test')->transaction(function () use ($uid) {
                    try {
                        $itemCode = ItemCode::where('status', 'not_use')->sharedLock()->first()->update([
                            'status' => 'used',
                            'uid' => $uid,
                            'log_date' => date('Y-m-d H:i:s'),
                            'log_date_timestamp' => time(),
                            'last_ip' => $this->getIP(),
                        ]);
                        // throw new \Exception("The field is undefined.");
                    } catch (\Exception $e) {
                        DB::connection('events_bns_test')->rollback();
                        // something went wrong
                    }
                });

                $eventInfo = $this->setEventInfo($uid);
                
                if($eventInfo['my_code'] != ''){
                    return response()->json([
                        'status' => true,
                        'message' => 'ซื้อแพ็คเกจสำเร็จ<br />ไอเทมโค้ดของคุณคือ<br />'.$eventInfo['my_code'],
                        'content' => $eventInfo
                    ]);
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งไอเทมโค้ดได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                        'content' => $eventInfo
                    ]);
                }

            }else{
                $arrDeductDiamond['status'] = 'unsuccess';
                $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);
            }

            return response()->json([
                        'status' => false,
                        'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์กรุณาติดต่อฝ่ายบริการลูกค้า'
            ]);

        }

    }