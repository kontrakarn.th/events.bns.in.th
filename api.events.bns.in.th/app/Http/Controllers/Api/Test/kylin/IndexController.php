<?php

    namespace App\Http\Controllers\Api\Test\kylin;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DB;
    
    use App\Models\Test\kylin\ItemHistory;
    use App\Models\Test\kylin\Member;
    use App\Models\Test\kylin\DeductLog;
    use App\Models\Test\kylin\SendLogs;

    class IndexController extends BnsEventController {

        private $baseApi = 'http://api.apps.garena.in.th';
        private $startTime = '2020-03-02 00:00:00';
        private $endTime = '2020-03-18 23:59:59';

        private $awtSecret = 'AgjVHV5uEA7TYsSgNaydHHqL6MdsTkUs';

        private $requireDiamonds = [3, 7, 15];

        private $packageIds = [1, 2, 3];

        public function __construct(Request $request) {
            parent::__construct();

            // init user data
            $this->userData = $this->getJwtUserData($request);

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end-event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                                'status' => false,
                                'type' => 'maintenance',
                                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                            ]));
            }
            if($this->isLoggedIn()==true){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                                    'status' => false,
                                    'type' => 'cant_create_member',
                                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                        ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP() {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn() {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip() {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1';

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember() {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];

            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP()
                ];

                $resp = $this->createMember($arr);
                if ($resp == false) {
                    return false;
                }
            }else{
                // $member = Member::where('uid', $uid)->first();
                if(empty($this->memberData->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $this->memberData->_id);
                }
            }
            return true;
        }

        private function hasMember(int $uid) {
            $counter = Member::where('uid', $uid)->first();
            if (isset($counter)) {
                $this->memberData=$counter;
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr) {
            if(Member::create($arr)){
                $member = Member::where('uid', intval($arr['uid']))->first();
                $this->memberData=$member;
                return true;
            }else{
                return false;
            }
        }

        public function updateMember($arr, $_id) {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        private function getNcidByUid(int $uid){
            return Member::where('uid', $uid)->value('ncid');
        }

        private function getUidByNcid(string $ncid) {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:KYLIN:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Sending item from garena events bns kylin.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return ItemHistory::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemHistory::where('id', $log_id)->where('uid', $uid)->update($arr);
        }

        private function addSendItemLog($arr) {
            return SendLogs::create($arr);
        }

        private function updateSendItemLog($arr, $log_id, $uid) {
            return SendLogs::where('id', $log_id)->where('uid', $uid)->update($arr);
        }

        private function diamondBalance(int $uid) {
            return $this->post_api($this->baseApi, [
                        'key_name' => 'bns',
                        'service' => 'balance',
                        'uid' => $uid,
            ]);
        }

        private function setDiamondDeductData($deduct_diamond_amount=0)
        {
            if($deduct_diamond_amount==0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamonds for garena bns kylin event',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addDeductLog(array $arr) {
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductLog($arr, $log_id) {
            if ($arr) {
                return DeductLog::where('id', $log_id)->update($arr);
            }
            return null;
        }

        private function setItemList($package_id=null){
            switch ($package_id) {
                case 1:
                    return [
                        [
                            'id' => 1,
                            'item_type' => 'object',
                            'product_title' => 'แพ็คเกจนาคราของผู้ฝึกยุทธ',
                            'product_id' => 3134,
                            'product_quantity' => 1,
                            'icon' => '1.png',
                            'key' => '1'
                        ]
                    ];
                    break;

                case 2:
                    return [
                        [
                            'id' => 2,
                            'item_type' => 'object',
                            'product_title' => 'แพ็คเกจนาคราของเจ้าสำนัก',
                            'product_id' => 3135,
                            'product_quantity' => 1,
                            'icon' => '2.png',
                            'key' => '2'
                        ]
                    ];
                    break;

                case 3:
                    return [
                        [
                            'id' => 3,
                            'item_type' => 'object',
                            'product_title' => 'แพ็คเกจนาคราของเทพเจ้า',
                            'product_id' => 3136,
                            'product_quantity' => 1,
                            'icon' => '3.png',
                            'key' => '3'
                        ],
                        [
                            'id' => 4,
                            'item_type' => 'object',
                            'product_title' => 'แพ็คเกจนาคราของเทพเจ้า 2',
                            'product_id' => 3137,
                            'product_quantity' => 1,
                            'icon' => '3.png',
                            'key' => '3'
                        ]
                    ];
                    break;
                default:
                    return '';
                    break;
            }
        }

        private function setRequiredDiamonds($package_id=null)
        {
            if(empty($package_id))
                return false;

            return isset($this->requireDiamonds[$package_id - 1]) ? $this->requireDiamonds[$package_id - 1] : false;
        }

        private function doRandomItem($cardQuantity = 1) {
            $rewarder = new RewardController();
            $resp = [];
            for ($index = 0; $index < $cardQuantity; $index++) {
                $resp[] = $rewarder->getRandomRewardInfo();
            }

            return $resp;
        }

        public function checkin(Request $request) // checkin event
        {

            $decoded=$request->input();
            if(isset($decoded['type']) == false || $decoded['type'] != 'event_info'){
                return response()->json(['status' => false, 'message' => 'Invalid parameter! (1)']);
            }
            // dd($this->userData);
            $uid = $this->userData['uid'];
            $nickname=$this->userData['nickname'];

            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);
            $item_chk=[];
            foreach($this->requireDiamonds as $key=>$value){

                $chk_buy=SendLogs::where('uid',$uid)->where('item_no',($key+1))->first();

                if(isset($chk_buy)){
                    array_push($item_chk,"buyed");
                }else{
                    if($diamondsBalance->balance<$value){
                        array_push($item_chk,"cant_buy");
                    }else{
                        array_push($item_chk,"can_buy");
                    }
                }
            }

            return response()->json([
                'status'=>true,
                'message'=>'success',
                'data'=>[
                    'uid'=>$uid,
                    'username'=>$nickname,
                    'can_buy'=>[
                        "item_1"=>$item_chk[0],
                        "item_2"=>$item_chk[1],
                        "item_3"=>$item_chk[2],
                    ]
                ]
            ]);
        }

        public function buy(Request $request){
            if($this->isLoggedIn()==false){
                return response()->json(['status' => false, 'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน']);
            }

            $decoded=$request->input();
            // $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            // if (is_null($decoded)) {
            //     return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            // }
            // $decoded = collect($decoded);
            if(isset($decoded['type']) == false || $decoded['type'] != 'buy' || isset($decoded['package_id']) == false){
                return response()->json(['status' => false, 'message' => 'Invalid parameter! (1)']);
            }

            $package_id = intval($decoded['package_id']);
            if($package_id<1 || $package_id>3){
                return response()->json(['status' => false, 'message' => 'Invalid parameter! (2)']);
            }

            $uid = $this->memberData->uid;
            $ncid = $this->memberData->ncid;


            $item_buy = $this->setItemList($package_id);
            // dd($item_buy);
            if(empty($item_buy)){
                return response()->json(['status' => false, 'message' => 'No data required (1).']);
            }

            $dup_buy=SendLogs::where('uid',$uid)->where('item_no',$package_id)->first();
            if(isset($dup_buy)){
                return response()->json(['status' => false, 'message' => 'ไม่สามารถซื้อซ้ำได้']);
            }

            if (in_array($package_id, $this->packageIds)) {

                $required_deduct_diamond = $this->setRequiredDiamonds($package_id);
                if($required_deduct_diamond==false){
                    return response()->json(['status' => false, 'message' => 'No data required (2).']);
                }

                // get diamond balance
                $diamondsBalanceRaw = $this->diamondBalance($uid);
                $diamondsBalance = json_decode($diamondsBalanceRaw);

                if ($diamondsBalance->status == false) {
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถอัพเดทข้อมูล Diamonds ของคุณได้กรุณาลองใหม่อีกครั้ง'
                            ]);
                }else{

                    $before_diamond = intval($diamondsBalance->balance);
                    // compare user diamonds balance with gachapon's require diamomds.
                    if($before_diamond < $required_deduct_diamond){
                        return response()->json([
                                    'status' => false,
                                    'message' => 'จำนวน Diamonds ของคุณไม่พอในการซื้อกาชาปอง'
                        ]);
                    }else{

                        $logDeduct = $this->addDeductLog([
                            'status' => 'pending',
                            'diamond' => $required_deduct_diamond,
                            'uid' => $uid,
                            'ncid' => $ncid,
                            'item_no' => $package_id,
                            'last_ip' => $this->getIP(),
                            'type' => 'buy_'.$package_id,
                            'log_timestamp' => time()
                        ]);

                        // set desuct diamonds
                        $deductData = $this->setDiamondDeductData($required_deduct_diamond);
                        if($deductData == false){
                            return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง']);
                        }

                        $respDeductRaw = $this->deductDiamond($ncid, $deductData);
                        $respDeduct = json_decode($respDeductRaw);

                        if(is_object($respDeduct) && is_null($respDeduct) == false){

                            $arrDeductLog = [
                                'before_deduct_diamond' => $before_diamond,
                                'after_deduct_diamond' => $before_diamond - $required_deduct_diamond,
                                // 'deduct_status' => $respDeduct->status,
                                'deduct_purchase_id' => isset($respDeduct->response->purchase_id) ? $respDeduct->response->purchase_id : 0,
                                'deduct_purchase_status' => isset($respDeduct->response->purchase_status) ? $respDeduct->response->purchase_status : 0,
                                'deduct_data' => json_encode($deductData),
                                'status' => 'pending'
                            ];

                            if($respDeduct->status){

                                $arrDeductLog['status'] = 'success';
                                $this->updateDeductLog($arrDeductLog, $logDeduct['id']);


                                $goodsData = []; //good data for send item group

                                foreach ($item_buy as $key => $value) {//loop for add item log
                                    $arrItemHistory = [
                                        'item_no'=>0,
                                        'product_id' => 0,
                                        'product_title' => '',
                                        'product_quantity' => 0,
                                        'uid' => $uid,
                                        'ncid' => $ncid,
                                        'item_type'=>'buy_'.$package_id,
                                        'last_ip' => $this->getIP(),
                                    ];

                                    $arrItemHistory['item_no'] = $value['id'];
                                    $arrItemHistory['product_title'] = $value['product_title'];
                                    $arrItemHistory['product_id'] = $value['product_id'];
                                    $arrItemHistory['product_quantity'] = $value['product_quantity'];

                                    $goodsData[] = [
                                        'goods_id' => $value['product_id'],
                                        'purchase_quantity' => $value['product_quantity'],
                                        'purchase_amount' => 0,
                                        'category_id' => 40
                                    ];
                                    $this->addItemHistoryLog($arrItemHistory);
                                }

                                // send items
                                if (is_null($goodsData) == false && is_array($goodsData) && count($goodsData) > 0 && is_null($respDeduct->response->purchase_id) == false) {
                                    //do add send item log
                                    $logSendItemResult = $this->addSendItemLog([
                                        'uid' => $uid,
                                        'ncid' => $ncid,
                                        'item_type' => 'buy_'.$package_id,
                                        'item_no' => $package_id,
                                        'item_price' => $required_deduct_diamond,
                                        'status' => 'pending',
                                        'send_item_purchase_id' => 0,
                                        'send_item_purchase_status' => 0,
                                        'goods_data' => json_encode($goodsData),
                                        'last_ip' => $this->getIP(),
                                    ]);

                                    //send item
                                    $sendResultRaw = $this->dosendItem($ncid, $goodsData);
                                    $sendResult = json_decode($sendResultRaw);
                                    if (is_null($sendResult) == false && is_object($sendResult) && $sendResult->status) {
                                        //recive log id
                                        $log_id = $logSendItemResult['id'];
                                        //do update send item log
                                        $this->updateSendItemLog([
                                            'send_item_purchase_id' => $sendResult->response->purchase_id ?: 0,
                                            'send_item_purchase_status' => $sendResult->response->purchase_status ?: 0,
                                            'status' => 'success'
                                                ], $log_id, $uid);
                                    } else {
                                        //do update send item log
                                        $log_id = $logSendItemResult['id'];
                                        $this->updateSendItemLog([
                                            'send_item_purchase_id' => isset($sendResult->response->purchase_id) ? $sendResult->response->purchase_id : 0,
                                            'send_item_purchase_status' => isset($sendResult->response->purchase_status) ? $sendResult->response->purchase_status : 0,
                                            'status' => 'unsuccess'
                                                ], $log_id, $uid);
                                        //error unsuccess log
                                        return response()->json([
                                                    'status' => false,
                                                    'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                                        ]);
                                    }
                                }

                                // $content = [];
                                //convert content result
                                // foreach ($item_buy as $key => $value) {
                                    $collection = collect($item_buy[0]);
                                    $filtered = $collection->only(['id','product_title', 'icon', 'key']); //except
                                    $content[] = $filtered->all();
                                // }

                                $diamondsBalance = $before_diamond-$required_deduct_diamond;
                                $item_chk=[];
                                foreach($this->requireDiamonds as $key_chk=>$value_diamond){

                                    $chk_buy=SendLogs::where('uid',$uid)->where('item_no',($key_chk+1))->first();

                                    if(isset($chk_buy)){
                                        array_push($item_chk,"buyed");
                                    }else{
                                        if($diamondsBalance<$value_diamond){
                                            array_push($item_chk,"cant_buy");
                                        }else{
                                            array_push($item_chk,"can_buy");
                                        }
                                    }
                                }

                                //do return success log
                                return response()->json([
                                            'status' => true,
                                            'message' => 'ซื้อสำเร็จ',
                                            'data'=>[
                                                'can_buy'=>[
                                                    "item_1"=>$item_chk[0],
                                                    "item_2"=>$item_chk[1],
                                                    "item_3"=>$item_chk[2],
                                                ],
                                                "item_get"=>$content
                                            ]
                                ]);

                            }else{
                                $arrDeductLog['status'] = 'unsuccess';
                                $this->updateDeductLog($arrDeductLog, $logDeduct['id']);
                            }
                        }else {
                            $arrDeductLog['status'] = 'unsuccess';
                            $this->updateDeductLog($arrDeductLog, $logDeduct['id']);
                        }
                        return response()->json([
                                    'status' => false,
                                    'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                        ]);

                    }

                }

            }
            return response()->json([
                        'status' => false,
                        'message' => 'Np data required (5).'
                    ]);
        }
    }
