<?php

    namespace App\Http\Controllers\Api\Test\daily_login;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\Test\daily_login\Setting;
    use App\Models\Test\daily_login\Member;
    use App\Models\Test\daily_login\Checkin;
    use App\Models\Test\daily_login\Reward;
    use App\Models\Test\daily_login\ItemLog;
    use App\Models\Test\daily_login\CharacterLog;
    use App\Models\Test\daily_login\LastLoginList;

    class IndexController extends BnsEventController{

        private $baseApi = 'http://api.apps.garena.in.th';

        private $userEvent = null;

        public function __construct(Request $request)
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();

        }

        private function checkEventStatus()
        {
             if($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip  = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData) {
                return true;
            } else {
                return false;
            }
        }

        private function is_accepted_ip()
        {
            $allow_ips   = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid      = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $arr = [
                    'uid'              => $uid,
                    'username'         => $username,
                    'ncid'             => $ncid,
                    'char_id'          => 0,
                    'char_name'        => '',
                    'world_id'         => 0,
                    'last_ip'          => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }

                $this->userEvent = $resp;

            } else {
                $member = Member::where('uid', $uid)->first();
                if (empty($member->ncid)) {
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if (empty($ncid)) {
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
                $this->userEvent = $member;
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if (empty($_id)) {
                return false;
            }

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:DAILY_LOGIN:TEST:ASSOCIATE_UID_WITH_NCID_'.$uid, 10, function () use ($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'user_info',
                    'uid'      => $uid,
                ]);
                $userNcInfo       = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }

            });
        }

        private function doGetChar($uid, $ncid){//new fixed function
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i       = 1;
                    foreach ($resp->response as $key => $value) {
                        // if((int)$value->level >= 60 && (int)$value->mastery_level>=12){
                            $content[] = [
                                'id' => $i,
                                'char_id'   => (int)$value->id,
                                'char_name' => $value->name,
                                'world_id' => (int)$value->world_id,
                                'job' => (int)$value->job,
                                'level' => (int)$value->level,
                                'creation_time' => $value->creation_time,
                                'last_play_start' => $value->last_play_start,
                                'last_play_end' => $value->last_play_end,
                                'last_ip' => $value->last_ip,
                                'exp' => (int)$value->exp,
                                'mastery_level' => (int)$value->mastery_level,
                            ];
                        // }

                        $i++;
                    }

                    if(in_array($uid,[69224041,321571544])){
                        $content[] = [
                            'id' => 9,
                            'char_id'   => 77777,
                            'char_name' => 'Test',
                            'world_id' => 4,
                            'job' => 1,
                            'level' => 60,
                            'creation_time' => '2020-04-03 10:54:54.507',
                            'last_play_start' => '2020-04-02 20:33:55.477',
                            'last_play_end' => '2020-04-03 00:54:11.51',
                            'last_ip' => '43.240.112.250',
                            'exp' => 8192000,
                            'mastery_level' => 10,
                        ];
                    }
                    
                    return $content;
                }
            }
            return null;
        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:DAILY_LOGIN:TEST:CHARACTERS_'.$uid, 10, function () use ($ncid) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'characters',
                    'user_id'  => $ncid,
                ]);
            });
        }

        private function dosendItem(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name'             => 'bns',
                'service'              => 'send_item',
                'user_id'              => $ncid,
                'purchase_description' => 'Test sending item from garena events BNS Daily Login.',
                'goods'                => json_encode($goodsData),
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
        }

        private function getEventSetting(){
            // return Cache::remember('BNS:LEAGUE:TEST:EVENT_SETTING_TEST', 10, function() {
                        return Setting::where('active', 1)->first();
                    // });
        }

        public function getEventInfo(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            

            $eventInfo = $this->setEventInfo($uid);
            if($eventInfo['status']==false){
                return response()->json([
                        'status' => false,
                        'message'=>$eventInfo['message'],
                    ]);
            }

            return response()->json([
                'status' => true,
                'data' => $eventInfo
            ]);

        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return [
                    'status' => false,
                    'message' => 'ไม่พอข้อมูล'
                ];
            }

            // get events setting info
            $eventSetting = $this->getEventSetting();

            $member = Member::where('uid', $uid)->first();

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // check claimed coupon
            $claimedCouponLog = ItemLog::where('uid', $member->uid)->where('package_type', 'coupon')->count();

            // check and create checkin log
            $checkinLogQuery = Checkin::where('uid', $member->uid)->get();
            if(count($checkinLogQuery) < 21){
                // Checkin::where('uid', $member->uid)->delete();
                for($i=1;$i<=$eventSetting->login_days;$i++){
                    Checkin::create([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'days' => $i,
                        'is_checkin' => 0,
                        'is_claimed' => 0,
                    ]);
                }
                
                $checkinLogQuery = Checkin::where('uid', $member->uid)->get();

            }

            // set current checkin day
            $checkedInLogQuery = Checkin::where('uid', $member->uid)->where('is_checkin', 1)->count();
            $currentDay = $checkedInLogQuery + 1;

            $checkinList = [];
            foreach($checkinLogQuery as $checkin){

                $itemLog = 0;
                $itemLog = ItemLog::where('uid', $member->uid)->where('log_date', $logDate)->where('package_type', 'checkin')->count();

                $checkinList[] = [
                    'days' => $checkin->days,
                    'can_checkin' => $this->checkCanCheckin($member->uid,$checkin->days,$checkin->is_checkin,$currentDay) && $itemLog == 0,
                    // 'is_checkin' => $checkin->is_checkin == 1 ? true : false,
                    'is_claimed' => $checkin->is_claimed == 1 ? true : false,
                ];
            }

            return [
                'status' => true,
                "message" => 'Event Info',
                'username'=>$member->username,
                'current_day' => $currentDay,
                'coupon' => [
                    'can_claim' => ($this->checkIsNewUser($member->uid,false) && $claimedCouponLog <= 0) || ($this->checkCombackPlayer($member->uid) && $claimedCouponLog <= 0) ? true : false,
                    'is_claimed' => $claimedCouponLog > 0,
                ],
                'check_list' => $checkinList,
            ];

        }

        private function checkCanCheckin($uid='',$days=0,$isCheckin=0,$currentDay=0){
            if($days != 0 && $currentDay != 0 && $days == $currentDay && $isCheckin == 0){
                if($this->checkIsNewUser($uid,true) == true || $this->checkCombackPlayer($uid) == true){
                    return true;
                }else{
                    return false;
                }
            }

            return false;
        }

        private function checkIsNewUser($uid,$requireHM=false){

            // get events setting info
            $eventSetting = $this->getEventSetting();

            $member = Member::where('uid', $uid)->first();

            $characters = $this->doGetChar($member->uid, $member->ncid);

            // check condition
            if(count($characters) > 0){

                foreach($characters as $char){
                    
                    if(strtotime($char['creation_time']) >= strtotime($eventSetting->min_create_char_date)){
                        if($requireHM == true){
                            if($char['mastery_level'] >= 10){
                                return true;
                                break;
                            }
                        }else{
                            return true;
                            break;
                        }
                        
                    }

                }

            }

            return false;

        }

        private function checkCombackPlayer($uid){
            $existCombackPlayer = LastLoginList::where('uid', $uid)->count();

            return $existCombackPlayer > 0;
        }

        public function claimCoupon(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'claim_coupon') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check condition
            // check claimed coupon
            $claimedCouponLog = ItemLog::where('uid', $member->uid)->where('package_type', 'coupon')->count();
            if($claimedCouponLog > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณได้รับของรางวัลไปแล้ว'
                ]);
            }

            if($this->checkIsNewUser($member->uid,false) && $this->checkCombackPlayer($member->uid)){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย ไม่ตรงเงื่อนไขการรับรางวัล'
                ]);
            }

            // get reward
            // get reward from packge_key
            $reward = Reward::where('package_key', 1)->where('package_type', 'coupon')->first();
            if(!isset($reward)){
                return response()->json([
                    'status' => false,
                    'message' => 'no reward data.'
                ]);
            }

            // set reward info for check condition
            $packageId = $reward->package_id;
            $packageName = $reward->package_name;
            $packageQuantity = $reward->package_quantity;
            $packageAmount = $reward->package_amount;
            $packageType = $reward->package_type;
            $packageKey = $reward->package_key;

            $goods_data = []; //good data for send item group
            $goods_data[] = [
                'goods_id' => $reward->package_id,
                'purchase_quantity' => $reward->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40,
            ];

            $sendItemLog = $this->addItemHistoryLog([
                'uid'                           => $member->uid,
                'username'                      => $member->username,
                'ncid'                          => $member->ncid,
                'package_key'                   => $packageKey,
                'package_id'                    => $packageId,
                'package_name'                  => $packageName,
                'package_quantity'              => $packageQuantity,
                'package_amount'                => $packageAmount,
                'package_type'                  => $packageType,
                'send_item_status'              => false,
                'send_item_purchase_id'         => 0,
                'send_item_purchase_status'     => 0,
                'goods_data'                    => json_encode($goods_data),
                'status'                        => 'pending',
                'log_date'                      => (string)date('Y-m-d'), // set to string
                'log_date_timestamp'            => time(),
                'last_ip'                       => $this->getIP(),
            ]);

            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            /* $send_result = (object)[
                "status" => true,
                "response" => (object)[
                    "purchase_id" => "23857513",
                    "purchase_status" => "4"
                ]
            ]; */

            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                // update send item log
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ?: false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                    'status' => 'success'
                ], $sendItemLog['id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                    'status' => true,
                    'data' => $eventInfo
                ]);

            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                        ], $sendItemLog['id'], $uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                        ]);
            }
        }

        public function getCheckin(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'checkin') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $eventSetting = $this->getEventSetting();


            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimeStamp = time();

            // set current checkin day
            $checkedInLogQuery = Checkin::where('uid', $member->uid)->where('is_checkin', 1)->count();
            $currentDay = $checkedInLogQuery + 1;

            $claimedCouponLog = ItemLog::where('uid', $member->uid)->where('log_date', $logDate)->where('package_type', 'checkin')->count();
            if($claimedCouponLog > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย คุณได้รับของรางวัลของวันนี้ไปแล้ว'
                ]);
            }

            $checkedInLog = Checkin::where('uid', $member->uid)->where('days', $currentDay)->first();
            if($this->checkCanCheckin($member->uid,$checkedInLog->days,$checkedInLog->is_checkin,$currentDay) == false || strtotime($logDate) == strtotime($checkedInLog->claimed_log_date) && $$checkedInLog->claimed_log_date != ""){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย ไม่ตรงเงื่อนไขการรับของรางวัล'
                ]);
            }


            // get reward from packge_key
            $reward = Reward::where('days', $currentDay)->where('package_type', 'checkin')->first();
            if(!isset($reward)){
                return response()->json([
                    'status' => false,
                    'message' => 'no reward data.'
                ]);
            }

            // set reward info for check condition
            $packageId = $reward->package_id;
            $packageName = $reward->package_name;
            $packageQuantity = $reward->package_quantity;
            $packageAmount = $reward->package_amount;
            $packageType = $reward->package_type;
            $packageKey = $reward->package_key;

            $goods_data = []; //good data for send item group
            $goods_data[] = [
                'goods_id' => $reward->package_id,
                'purchase_quantity' => $reward->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40,
            ];

            $sendItemLog = $this->addItemHistoryLog([
                'uid'                           => $member->uid,
                'username'                      => $member->username,
                'ncid'                          => $member->ncid,
                'days'                          => $reward->days,
                'package_key'                   => $packageKey,
                'package_id'                    => $packageId,
                'package_name'                  => $packageName,
                'package_quantity'              => $packageQuantity,
                'package_amount'                => $packageAmount,
                'package_type'                  => $packageType,
                'send_item_status'              => false,
                'send_item_purchase_id'         => 0,
                'send_item_purchase_status'     => 0,
                'goods_data'                    => json_encode($goods_data),
                'status'                        => 'pending',
                'log_date'                      => (string)date('Y-m-d'), // set to string
                'log_date_timestamp'            => time(),
                'last_ip'                       => $this->getIP(),
            ]);

            // update checkin log
            Checkin::where('uid', $member->uid)->where('days', $currentDay)->update([
                'is_checkin'            => 1,
                'is_claimed'            => 1,
                'claimed_log_date'              => (string)date('Y-m-d'), // set to string
                'claimed_log_date_timestamp'    => time(),
            ]);

            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            /* $send_result = (object)[
                "status" => true,
                "response" => (object)[
                    "purchase_id" => "23857513",
                    "purchase_status" => "4"
                ]
            ]; */

            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                // update send item log
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ?: false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                    'status' => 'success'
                ], $sendItemLog['id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                    'status' => true,
                    'message' => "ส่งของรางวัลสำเร็จ <br />รางวัลจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                    'data' => $eventInfo
                ]);

            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                        ], $sendItemLog['id'], $uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งรางวัลได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                            // 'data' => $eventInfo
                        ]);
            }
        }

    }