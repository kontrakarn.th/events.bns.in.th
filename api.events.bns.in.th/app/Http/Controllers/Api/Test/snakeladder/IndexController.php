<?php

    namespace App\Http\Controllers\Api\Test\snakeladder;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Http\Controllers\Api\BnsEventController;

    // Utils
    use Illuminate\Support\Facades\Cache;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    // Models
    use App\Models\Test\snakeladder\Member;

    // Models Free Board
    use App\Models\Test\snakeladder\Quest;
    use App\Models\Test\snakeladder\QuestLog;
    use App\Models\Test\snakeladder\FreeRightsLog;
    use App\Models\Test\snakeladder\FreeBoardLog;
    use App\Models\Test\snakeladder\FreeBoardItemLog;
    use App\Models\Test\snakeladder\FreeDice;

    // Models Paid Board
    use App\Models\Test\snakeladder\PaidBoard;
    use App\Models\Test\snakeladder\PaidBoardLog;
    use App\Models\Test\snakeladder\PaidBoardDiceLog;
    use App\Models\Test\snakeladder\PaidBoardReward;
    use App\Models\Test\snakeladder\PaidDice;
    use App\Models\Test\snakeladder\PaidDeductLog;
    use App\Models\Test\snakeladder\PaidItemLog;
    use App\Models\Test\snakeladder\PaidItemHistory;

    class IndexController extends BnsEventController
    {
        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2019-03-27 00:00:00';
        private $endBuyBoardTime = '2019-04-17 23:59:59';
        private $endMergeAndRedeemItem = '2019-04-23 23:59:59';

        private $startEventQuest = '2019-03-27 00:00:00';
        private $endEventQuest = '2019-04-17 05:59:00';

        private $random_board_diamonds = 1; // 500
        private $random_dice_diamonds = 2; // 5000
        private $send_gift_diamonds = 3; // 10000

        private $rareItemStep = [10,20,30,40,50,60,70,80,90,100];

        public function __construct(Request $request){
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus(){
            if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
                // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endMergeAndRedeemItem))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if ($this->checkIsMaintenance()) {
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if ($this->isLoggedIn()) {
                    // check and create member
                if ($this->checkMember() == false) {
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            } else {
                die(json_encode([
                    'status' => false,
                    'type' => 'not_login',
                    'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                ]));
            }
        }

        private function getIP(){
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn(){
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip(){
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1';

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember(){
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP()
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            } else {
                $member = Member::where('uid', $uid)->first();
                if (empty($member->ncid)) {
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if (empty($ncid)) {
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $uid);
                }
            }
            return true;
        }

        private function hasMember(int $uid){
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr){
            return Member::create($arr);
        }

        private function updateMember($row_id = 0, $arr){
            if (empty($row_id))
                return false;

            return Member::where('id', $row_id)->update($arr);
        }

        private function getNcidByUid(int $uid) : string{
            return Member::where('uid', $uid)->value('ncid');
        }

        private function getUidByNcid(string $ncid){
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username) : string{
            return Cache::remember(sha1('BNS:SNAKELADDER:ASSOCIATE_UID_WITH_NCID_' . $uid), 10, function () use ($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }

            });
        }

        public function acceptChar(Request $request){

            if($request->has('type') == false && $request->type != 'accept_character') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter(1)!'
                ]);
            }

            if ($request->has('id') == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'invalid parameter(2)!'
                ]);
            }
            $char_id = $request->id;
            if (empty($char_id)) {
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่มีตัวละครที่ต้องการเลือก'
                ]);
            }

            // $char_id = $request->id;

            $uid = $this->userData['uid'];
            $username = $this->userData['username'];

            if ($resp = $this->hasAcceptChar($uid) == false) {
                $ncid = $this->getNcidByUid($uid);
                $characters = $this->doGetChar($uid,$ncid);
                $data = $characters[$char_id-1];
                if (isset($data['char_id'])) {
                    $this->doAcceptChar($data['char_id'], $data['char_name'], $data['world_id'], $data['job'], $uid);
                    $member = Member::where('uid', $uid)->first();
                    return response()->json([
                                'status' => true,
                                'message' => 'ยืนยันตัวละครเสร็จสิ้น',
                                'content' => [
                                    'username' => $member->username,
                                    'character' => $member->char_name,
                                ],
                    ]);
                } else {
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่พบตัวละครที่คุณต้องการเลือก'
                    ]);
                }
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก'
                ]);
            }
        }

        private function doAcceptChar($char_id, $char_name, $world_id, $job, $uid){
            return Member::where('uid', $uid)->update([
                        'char_id' => $char_id,
                        'char_name' => $char_name,
                        'world_id' => $world_id,
                        'job' => $job,
                    ]);
        }

        private function hasAcceptChar($uid){
            $resp = Member::where('uid', $uid)->first();
            if (empty($resp->char_id) && empty($resp->char_name)) {
                return false;
            } else {
                return $resp;
            }
        }

        public function getCharacter(Request $request){
            if($request->has('type') == false && $request->type != 'get_characters') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $this->checkMember();
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            $resp = $this->hasAcceptChar($uid);
            if ($resp == false) {
                $charactersList = [];
                $ncid = $this->getNcidByUid($uid);
                $characters = $this->doGetChar($uid,$ncid);
                foreach ($characters as $character) {
                    $charactersList[] = collect($character)->except(['char_id', 'world_id', 'job', 'level', 'creation_time', 'last_play_start', 'last_play_end', 'last_ip', 'exp', 'mastery_level']);
                }
                return response()->json([
                            'status' => true,
                            'content' => $charactersList,
                            'accepted' => false
                ]);
            } else {
                return response()->json([
                            'status' => true,
                            'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก',
                            'accepted' => true
                ]);
            }
        }

        private function doGetChar($uid,$ncid){
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i =1;
                    foreach ($resp->response as $key => $value) {
                        $content[] = [
                            'id' => $i,
                            'char_id' => $value->id,
                            'char_name' => $value->name,
                            'world_id' => $value->world_id,
                            'job' => $value->job,
                            'level' => $value->level,
                            'creation_time' => $value->creation_time,
                            'last_play_start' => $value->last_play_start,
                            'last_play_end' => $value->last_play_end,
                            'last_ip' => $value->last_ip,
                            'exp' => $value->exp,
                            'mastery_level' => $value->mastery_level,
                        ];

                        $i++;
                    }
                    return $content;
                }
            }
            return null;
        }

        private function apiCharacters($uid, $ncid){
            return Cache::remember('BNS:SNAKELADDER:CHARACTERS_' . $uid, 10, function() use($ncid) {
                        return $this->post_api($this->baseApi, [
                                    'key_name' => 'bns',
                                    'service' => 'characters',
                                    'user_id' => $ncid
                        ]);
                    });
        }

        private function apiSendItem(string $ncid, array $goodsData) {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events BNS Snake Ladders.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function apiUserInfo($uid)
        {
            $userInfo = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'user_info',
                'uid' => $uid
            ]);
            $result = json_decode($userInfo, true);
            if (is_null($result) || $result['status'] == false) {
                return false;
            }
            return $result['response'];
        }

        public function getSpecialPathPoints($step){
            switch($step){

                case 5:
                    return 6;
                    break;

                case 19:
                    return 4;
                    break;

                case 36:
                    return 4;
                    break;

                case 50:
                    return 6;
                    break;

                case 67:
                    return 8;
                    break;

                case 87:
                    return 6;
                    break;

                default:
                    return 0;
                    break;
            }

            return 0;
        }

        // Lobby
        public function getLobyInfo(Request $request){

            if ($request->has('type') == false && $request->type != 'lobby_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            return response()->json([
                'status' => true,
                'content' => [
                    // 'character' => $member->char_name,
                    'username' => $member->username,
                ],
            ]);

        }
        // Lobby

        // Free Board
        public function getFreeBoardInfo(Request $request){

            if ($request->has('type') == false && $request->type != 'free_board_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // check quest completed and update quest point
            $freeBoardInfo = $this->setFreeBoardInfo($member->uid);

            return response()->json([
                'status' => true,
                'content' => $freeBoardInfo,
            ]);
        }

        private function setFreeBoardInfo($uid){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            // check quest completed and update quest point
            $quests = $this->checkDailyQuest($member->uid);

            // check Free Rights
            $freeRights = $this->getFreeRights($member->uid);

            $freeRightsUsed = $this->checkFreeRightUsed($member->uid);

            return [
                'username' => $member->username,
                'character' => $member->char_name,
                'current_step' => $this->getTotalFreeBoardPoints($member->uid),
                'free_rights' => $freeRights && $this->getTotalFreeBoardPoints($member->uid) < 100,
                'free_rights_used' => $freeRightsUsed,
                'completed_status' => $quests['completed_status'],
                'quest_rights_used' => $quests['quest_rights_used'],
                'quests' => $quests['quests'],
            ];
        }

        private function getFreeRights($uid){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $dateTime = $this->setQuestDateTime();

            $freeRightsLog = 0;

            // check date range
            if(time() >= strtotime($this->startEventQuest) && time() <= strtotime($this->endEventQuest)){
                $checkHasfreeRightsLog = FreeRightsLog::where('uid', $member->uid)
                                            ->where('used_status', 'used')
                                            ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                            ->count();
                if($checkHasfreeRightsLog <= 0){

                    // check already free right
                    $checkAlreadyfreeRight = FreeRightsLog::where('uid', $member->uid)
                                                            ->where('used_status', 'not_use')
                                                            ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                                            ->count();
                    if($checkAlreadyfreeRight > 0){
                        $freeRightsLog = 1;
                    }else{
                        // create free
                        $createFreeRights = $this->addFreeRightsLog([
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'used_status' => 'not_use',
                            'status' => 'success',
                            'log_date' => date('Y-m-d H:i:s'),
                            'log_date_timestamp' => time(),
                            'last_ip' => $this->getIP(),
                        ]);

                        if($createFreeRights){
                            $freeRightsLog = 1;
                        }
                    }

                }else{
                    $freeRightsLog = 1;
                }
            }

            return $freeRightsLog > 0;
        }

        private function addFreeRightsLog(array $arr) {
            if ($arr) {
                $resp = FreeRightsLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateFreeRightsLog(array $arr, $uid, $startDate, $endDate){
            if ($arr) {
            return FreeRightsLog::where('uid', $uid)
                            ->whereBetween('log_date_timestamp', [strtotime($startDate), strtotime($endDate)])
                            ->update($arr);
            }
            return false;
        }

        private function checkFreeRightUsed($uid){
            if(empty($uid)){
                return false;
            }

            $quests = [];

            $dateTime = $this->setQuestDateTime();

            $freeRightsUsedCount = 0;
            $freeRightsUsedCount = FreeRightsLog::where('uid', $uid)
                                        ->where('status', 'success')
                                        ->where('used_status', 'used')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            return $freeRightsUsedCount > 0;

        }

        private function checkDailyQuest($uid){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $quests = [];

            $dateTime = $this->setQuestDateTime();

            $questInfo = new Quest;
            $questList = $questInfo->getQuestList();

            // check quest list
            foreach($questList as $quest){

                $dailyCompletedStatus = false;

                // check date range
                if(time() >= strtotime($this->startEventQuest) && time() <= strtotime($this->endEventQuest)){

                    // check daily quest log
                    $questLog = $this->checkQuestLogCompletedCount($uid,$quest['quest_code']);
                    if($questLog > 0){
                        /* $quests[] = [
                            'id' => $quest['id'],
                            'title' => $quest['quest_title'],
                            'completed' => true
                        ]; */
                        $dailyCompletedStatus = true;
                    }else{

                        // check quest success log
                        $questCompletedApi = $this->setQuestCompletedCount($member->uid, $member->char_id, $quest['quest_code'], $dateTime['start_date_time'], $dateTime['end_date_time'], $dateTime['date_cache']);
                        if($questCompletedApi > 0){
                            // create quest log
                            $this->addQuestLog([
                                'uid' => $member->uid,
                                'username' => $member->username,
                                'ncid' => $member->ncid,
                                'char_id' => $member->char_id,
                                'char_name' => $member->char_name,
                                'world_id' => $member->world_id,
                                'job' => $member->job,
                                'quest_id' => $quest['id'],
                                'quest_code' => $quest['quest_code'],
                                'quest_title' => $quest['quest_title'],
                                'status' => 'success',
                                'used_status' => 'not_use',
                                'log_date' => date('Y-m-d H:i:s'),
                                'log_date_timestamp' => time(),
                                'last_ip' => $this->getIP(),
                            ]);

                            $dailyCompletedStatus = true;
                        }
                    }
                }

                $quests[] = [
                    'id' => $quest['id'],
                    'title' => $quest['quest_title'],
                    'completed' => $dailyCompletedStatus,
                ];
            }

            return [
                'completed_status' => $dailyCompletedStatus && $this->getTotalFreeBoardPoints($member->uid) < 100,
                'quest_rights_used' => $this->checkQuestRightUsed($member->uid),
                'quests' => $quests,
            ];
        }

        private function checkQuestLogCompletedCount($uid,$questCode){
            if(empty($uid)){
                return false;
            }

            $quests = [];

            $dateTime = $this->setQuestDateTime();

            $dailyCompletedCount = 0;
            $dailyCompletedCount = QuestLog::where('uid', $uid)
                                        ->where('quest_code', $questCode)
                                        ->where('status', 'success')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            return $dailyCompletedCount;
        }

        private function checkQuestRightUsed($uid){
            if(empty($uid)){
                return false;
            }

            $quests = [];

            $dateTime = $this->setQuestDateTime();

            $dailyCompletedCount = 0;
            $dailyCompletedCount = QuestLog::where('uid', $uid)
                                        ->where('status', 'success')
                                        ->where('used_status', 'used')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            return $dailyCompletedCount > 0;

        }

        private function addQuestLog(array $arr) {
            if ($arr) {
                $resp = QuestLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateQuestRightsLog(array $arr, $uid, $startDate, $endDate){
            if ($arr) {
                return QuestLog::where('uid', $uid)
                                ->whereBetween('log_date_timestamp', [strtotime($startDate), strtotime($endDate)])
                                ->update($arr);
            }
            return false;

        }

        // check quest completed api
        private function apiCheckQuestCompleted($uid, $charId, $questId, $start, $end, $dateCache) {
            $resp = Cache::remember('BNS:SNAKELADDER:QUEST_COMPLETED_' . $questId . '_' . $uid .'_'. $dateCache, 5, function()use($charId,$questId,$start,$end) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'quest_completed',
                    'char_id' => $charId,
                    'quest_id' => $questId,
                    'start_time' => $start,
                    'end_time' => $end
                ]);
            });

            return json_decode($resp);
        }

        private function setQuestCompletedCount($uid, $charId, $questId, $start, $end, $dateCache){

            $completedCount = 0;

            $apiCompletedCount = $this->apiCheckQuestCompleted($uid, $charId, $questId, $start, $end, $dateCache);
            if($apiCompletedCount->status){
                $completedCount = intval($apiCompletedCount->response->quest_completed_count);
            }

            return $completedCount;
        }

        private function setQuestDateTime(){

            $currDate = date('Y-m-d');

            $startDatetime = '';
            $endDatetime = '';
            $dateCache = '';

            if(time() >= strtotime($currDate.' 00:00:00') && time() <= strtotime($currDate.' 05:59:59')){ // check current time after 00:00:00 and not over 05:59:59
                $dateCache = date('Y-m-d', strtotime(' -1 day'));
                $startDatetime = date('Y-m-d', strtotime(' -1 day')).' 06:00:00';
                $endDatetime = date('Y-m-d').' 05:59:59';
            }else{
                $dateCache = date('Y-m-d');
                $startDatetime = date('Y-m-d').' 06:00:00';
                $endDatetime = date('Y-m-d', strtotime(' +1 day')).' 05:59:59';
            }

            return [
                'date_cache' => $dateCache,
                'start_date_time' => $startDatetime,
                'end_date_time' => $endDatetime,
            ];

        }

        public function playFreeDice(Request $request){

            if ($request->has('type') == false && $request->type != 'play_free_dice') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $dateTime = $this->setQuestDateTime();

            $remainPoints = $this->getTotalFreeBoardPoints($member->uid);
            if($remainPoints >= 100){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้ทอยลูกเต๋าครบ 100 คะแนนแล้ว'
                ]);
            }

            $freeBoardInfo = $this->setFreeBoardInfo($member->uid);

            if($freeBoardInfo['free_rights'] == true && $freeBoardInfo['free_rights_used'] == true){
                if($freeBoardInfo['completed_status'] == true && $freeBoardInfo['quest_rights_used'] == true){
                    return response()->json([
                        'status' => false,
                        'message' => 'ขออภัย<br />สิทธิ์ในการทอยลูกเต๋าไม่พอ'
                    ]);
                }
            }

            $deductRight = false;

            if($freeBoardInfo['free_rights'] == true && $freeBoardInfo['free_rights_used'] == false){
                // deduct free right
                $deductRight = $this->updateFreeRightsLog([
                                    'used_status' => 'used'
                                ], $member->uid, $dateTime['start_date_time'], $dateTime['end_date_time']);
            }else if($freeBoardInfo['completed_status'] == true && $freeBoardInfo['quest_rights_used'] == false){
                // deduct daily quest rights
                $deductRight = $this->updateQuestRightsLog([
                                    'used_status' => 'used'
                                ], $member->uid, $dateTime['start_date_time'], $dateTime['end_date_time']);
            }

            if($deductRight){

                $rewardList = [];
                $goods_data = [];

                // random dice
                $dice = new FreeDice;
                $randomDiceResult = $dice->randomDice();

                if($randomDiceResult){

                    $this->addDiceFreeBoardPoints([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'points' => $randomDiceResult['points'],
                        'points_title' => $randomDiceResult['points_title'],
                        'status' => 'success',
                        'log_date' => date('Y-m-d H:i:s'),
                        'log_date_timestamp' => time(),
                        'last_ip' => $this->getIP(),
                    ]);

                    // get total points for check board's step
                    $totalPoints = $this->getTotalFreeBoardPoints($member->uid);
                    if($totalPoints >= 100){
                        $totalPoints = 100;
                    }

                    // set reward by board's step
                    $rewards = $dice->getFreeDiceReward($totalPoints);
                    if($rewards){

                        foreach ($rewards['product_set'] as $key => $value) {

                            $rewardList[] = [
                                'title' => $value['product_title'],
                                'amount' => $value['amount'],
                            ];

                            $goods_data[] = [
                                'goods_id' => $value['product_id'],
                                'purchase_quantity' => $value['product_quantity'],
                                'purchase_amount' => 0,
                                'category_id' => 40
                            ];
                        }

                        // add send item log
                        $itemLog = $this->addFreeBoardItemLogHistory([
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'board_step' => $totalPoints,
                            'product_title' => $rewards['product_title'],
                            'product_set' => json_encode($rewardList),
                            'send_item_status' => false,
                            'send_item_purchase_id' => 0,
                            'send_item_purchase_status' => 0,
                            'goods_data' => json_encode($goods_data),
                            'status' => 'pending',
                            'type' => 'normal_path',
                            'last_ip' => $this->getIP(),
                            'log_date' => (string)date('Y-m-d'), // set to string
                            'log_date_timestamp' => time(),
                        ]);

                        //send item
                        $send_result_raw = $this->apiSendItem($ncid, $goods_data);
                        $send_result = json_decode($send_result_raw);
                        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                            //do update send item log
                            $this->updateFreeBoardItemLogHistory([
                                'send_item_status' => $send_result->status ? : false,
                                'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                                'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                                'status' => 'success'
                            ], $itemLog['id'], $member->uid);

                            // check grant special path
                            $hasSpecialPath = false;
                            $specialPath = 0;
                            $totalSpecialPathPoints = 0;
                            $specialRewardList = [];
                            $specialPath = $this->getSpecialPathPoints($totalPoints);
                            if($specialPath > 0){

                                // add free board points
                                $this->addDiceFreeBoardPoints([
                                    'uid' => $member->uid,
                                    'username' => $member->username,
                                    'ncid' => $member->ncid,
                                    'points' => $specialPath,
                                    'points_title' => 'บรรไดงูทางลัดพิเศษ '.$specialPath .' แต้ม',
                                    'status' => 'success',
                                    'log_date' => date('Y-m-d H:i:s'),
                                    'log_date_timestamp' => time(),
                                    'last_ip' => $this->getIP(),
                                ]);

                                $totalSpecialPathPoints = $totalPoints + $specialPath;

                                $specialRewards = $dice->getFreeDiceReward($totalSpecialPathPoints);

                                $special_goods_data = [];
                                foreach ($specialRewards['product_set'] as $key => $value) {

                                    $specialRewardList[] = [
                                        'title' => $value['product_title'],
                                        'amount' => $value['amount'],
                                    ];

                                    $special_goods_data[] = [
                                        'goods_id' => $value['product_id'],
                                        'purchase_quantity' => $value['product_quantity'],
                                        'purchase_amount' => 0,
                                        'category_id' => 40
                                    ];
                                }

                                // add send item log
                                $specialItemlog = $this->addFreeBoardItemLogHistory([
                                    'uid' => $member->uid,
                                    'username' => $member->username,
                                    'ncid' => $member->ncid,
                                    'board_step' => $totalSpecialPathPoints,
                                    'product_title' => $specialRewards['product_title'],
                                    'product_set' => json_encode($specialRewardList),
                                    'send_item_status' => false,
                                    'send_item_purchase_id' => 0,
                                    'send_item_purchase_status' => 0,
                                    'goods_data' => json_encode($special_goods_data),
                                    'status' => 'pending',
                                    'type' => 'special_path',
                                    'last_ip' => $this->getIP(),
                                    'log_date' => (string)date('Y-m-d'), // set to string
                                    'log_date_timestamp' => time(),
                                ]);

                                // send special reward
                                $special_send_result_raw = $this->apiSendItem($ncid, $special_goods_data);
                                $special_send_result = json_decode($special_send_result_raw);
                                if (is_null($special_send_result) == false && is_object($special_send_result) && $special_send_result->status) {

                                    //do update send item log
                                    $this->updateFreeBoardItemLogHistory([
                                        'send_item_status' => $special_send_result->status ? : false,
                                        'send_item_purchase_id' => $special_send_result->response->purchase_id ? : 0,
                                        'send_item_purchase_status' => $special_send_result->response->purchase_status ? : 0,
                                        'status' => 'success'
                                    ], $specialItemlog['id'], $member->uid);

                                }else{
                                    //do update send item log
                                    $this->updateFreeBoardItemLogHistory([
                                        'send_item_status' => isset($special_send_result->status) ? $special_send_result->status : false,
                                        'send_item_purchase_id' => isset($special_send_result->response->purchase_id) ? $special_send_result->response->purchase_id : 0,
                                        'send_item_purchase_status' => isset($special_send_result->response->purchase_status) ? $special_send_result->response->purchase_status : 0,
                                        'status' => 'unsuccess'
                                    ], $specialItemlog['id'], $member->uid);

                                    return response()->json([
                                        'status' => false,
                                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า(2)',
                                    ]);
                                }

                            }

                            // check special points
                            if($specialPath > 0){
                                $totalSpecialPathPoints = $totalPoints + $specialPath;
                            }

                            // check garantee rare items
                            $totalRarePoints = ($totalSpecialPathPoints > 0) ? $totalSpecialPathPoints : $totalPoints;
                            $rareRewardList = [];
                            foreach($this->rareItemStep as $value3){
                                if($value3 <= $totalRarePoints){

                                    if($this->checkHasRareStepFreeItemHistory($uid,$value3) == false){

                                        $rareReward = [];
                                        $rareReward = $dice->getFreeDiceReward($value3);

                                        $rare_goods_data = [];
                                        foreach ($rareReward['product_set'] as $key => $value) {

                                            $rareRewardList[] = [
                                                'title' => $value['product_title'],
                                                'amount' => $value['amount'],
                                            ];

                                            $rare_goods_data[] = [
                                                'goods_id' => $value['product_id'],
                                                'purchase_quantity' => $value['product_quantity'],
                                                'purchase_amount' => 0,
                                                'category_id' => 40
                                            ];
                                        }

                                        // add send item log
                                        $rareItemlog = $this->addFreeBoardItemLogHistory([
                                            'uid' => $member->uid,
                                            'username' => $member->username,
                                            'ncid' => $member->ncid,
                                            'board_step' => $value3,
                                            'product_title' => $rareReward['product_title'],
                                            'product_set' => json_encode($rareRewardList),
                                            'send_item_status' => false,
                                            'send_item_purchase_id' => 0,
                                            'send_item_purchase_status' => 0,
                                            'goods_data' => json_encode($rare_goods_data),
                                            'status' => 'pending',
                                            'type' => 'special_path',
                                            'last_ip' => $this->getIP(),
                                            'log_date' => (string)date('Y-m-d'), // set to string
                                            'log_date_timestamp' => time(),
                                        ]);

                                        // send special reward
                                        $special_send_result_raw = $this->apiSendItem($ncid, $rare_goods_data);
                                        $special_send_result = json_decode($special_send_result_raw);
                                        if (is_null($special_send_result) == false && is_object($special_send_result) && $special_send_result->status) {

                                            //do update send item log
                                            $this->updateFreeBoardItemLogHistory([
                                                'send_item_status' => $special_send_result->status ? : false,
                                                'send_item_purchase_id' => $special_send_result->response->purchase_id ? : 0,
                                                'send_item_purchase_status' => $special_send_result->response->purchase_status ? : 0,
                                                'status' => 'success'
                                            ], $rareItemlog['id'], $member->uid);

                                        }else{
                                            //do update send item log
                                            $this->updateFreeBoardItemLogHistory([
                                                'send_item_status' => isset($special_send_result->status) ? $special_send_result->status : false,
                                                'send_item_purchase_id' => isset($special_send_result->response->purchase_id) ? $special_send_result->response->purchase_id : 0,
                                                'send_item_purchase_status' => isset($special_send_result->response->purchase_status) ? $special_send_result->response->purchase_status : 0,
                                                'status' => 'unsuccess'
                                            ], $rareItemlog['id'], $member->uid);

                                            return response()->json([
                                                'status' => false,
                                                'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า(3)',
                                            ]);
                                        }

                                    }

                                }else{
                                    break;
                                }
                            }

                            $freeBoardInfo = $this->setFreeBoardInfo($member->uid);

                            // set response
                            return response()->json([
                                'status' => true,
                                'message' => 'ทอยลูกเต๋สำเร็จ!',
                                'next_step' => $totalPoints,
                                'next_step_rewards' => $rewardList,
                                'has_special_path' => $specialPath > 0,
                                'special_path_step' => $totalSpecialPathPoints,
                                'special_path_rewards' => $specialRewardList,
                                'rare_reward' => $rareRewardList,
                                'content' => $freeBoardInfo,
                            ]);

                        }else{
                            //do update send item log
                            $this->updateFreeBoardItemLogHistory([
                                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                                'status' => 'unsuccess'
                            ], $itemLog['id'], $member->uid);

                            return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                            ]);
                        }

                    }else{
                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถดำเนินการได้<br />กรุณาลองใหม่อีกครั้ง(2)',
                        ]);
                    }

                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถทอยลูกเต๋าได้<br />กรุณาลองใหม่อีกครั้ง',
                    ]);
                }

            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถดำเนินการได้<br />กรุณาลองใหม่อีกครั้ง(1)',
                ]);
            }

        }

        private function checkHasRareStepFreeItemHistory($uid,$boardStep){
            $rareItemCount = FreeBoardItemLog::where('uid', $uid)
                                            ->where('board_step', $boardStep)
                                            ->count();
            return $rareItemCount > 0;
        }

        private function addDiceFreeBoardPoints(array $arr){
            if ($arr) {
                $resp = FreeBoardLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function getTotalFreeBoardPoints($uid){
            $count = FreeBoardLog::where('uid', $uid)->sum('points');
            $count = $count >= 100 ? 100 : $count;
            return intval($count);
        }

        private function addFreeBoardItemLogHistory(array $arr){
            if ($arr) {
                $resp = FreeBoardItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;

        }

        private function updateFreeBoardItemLogHistory(array $arr, $logId, $uid) {
            if ($arr) {
                return FreeBoardItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        // End Free Board

        // Start Paid Board

        public function getPaidBoardInfo(Request $request){

            if ($request->has('type') == false && $request->type != 'paid_board_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $paidBoardInfo = $this->setPaidBoardInfo($uid);

            if ($paidBoardInfo == false) {
                $paidBoardInfo = [];
            }

            return response()->json([
                'status' => true,
                'content' => $paidBoardInfo
            ]);

        }

        public function getRandomBoard(Request $request){

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endBuyBoardTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'หมดเวลาสำหรับสุ่มกระดาน'
                ]));
            }

            if ($request->has('type') == false && $request->type != 'random_paid_board') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $paidBoardInfo = $this->setPaidBoardInfo($uid);
            // check has current board and update rejected current board
            // if($paidBoardInfo['boards']['has_board'] == true){
            PaidBoardLog::where('uid', $member->uid)
                        // ->where('play_status', 'in_progress')
                        ->update([
                            'play_status' => 'rejected',
                        ]);
            // }

            // check diamonds balance
            $diamondBalance = $this->setDiamondsBalance($member->uid);

            if($diamondBalance < $this->random_board_diamonds){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอสุ่มกระดาน'
                ]);
            }

            // add deduct diamonds log
            $logDeduct = $this->addPaidDeductLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'diamonds' => $this->random_board_diamonds,
                'deduct_type' => 'random_board',
                'status' => 'pending',
                'last_ip' => $this->getIP(),
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
            ]);

            if($logDeduct){
                // set desuct diamonds
                $deductData = $this->setDiamondDeductData($this->random_board_diamonds);

                $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
                $resp_deduct = json_decode($resp_deduct_raw);
                if(is_object($resp_deduct) && is_null($resp_deduct) == false){

                    $this->updatePaidDeductLog([
                        'before_deduct_diamond' => $diamondBalance,
                        'after_deduct_diamond' => $diamondBalance - $this->random_board_diamonds,
                        'deduct_status' => $resp_deduct->status ?: 0,
                        'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
                        'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'success'
                    ], $logDeduct['id']);

                    // set random board
                    $boards = new PaidBoard;
                    $randBoardResult = $boards->randomBoard();

                    // create board log
                    $boardLog = $this->addPaidBoardLog([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'deduct_id' => $logDeduct['id'],
                        'board_id' => $randBoardResult['id'], // board key
                        'board_title' => $randBoardResult['points_title'],
                        'selected' => 0,
                        'play_status' => 'pending',
                        'status' => 'success',
                        'last_ip' => $this->getIP(),
                        'log_date' => (string)date('Y-m-d'), // set to string
                        'log_date_timestamp' => time(),
                    ]);

                    return response()->json([
                        'status' => true,
                        'message' => 'สุ่มกระดานสำเร็จ',
                        'board_key' => $randBoardResult['id'],
                        // 'content' => $this->setPaidBoardInfo($uid),
                    ]);

                }else{
                    $arr_log['status'] = 'unsuccess';
                    $this->updatePaidDeductLog($arr_log, $logDeduct['id']);
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }

            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />กรุณาลองใหม่อีกครั้ง'
                ]);
            }

        }

        public function acceptBoard(Request $request){

            if ($request->has('type') == false && $request->type != 'accept_board') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $boardKey = $request->id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // get pending board info
            $pendingBoard = PaidBoardLog::where('uid', $member->uid)
                                        ->where('board_id', $boardKey)
                                        ->where('selected', 0)
                                        ->where('play_status', 'pending')
                                        ->first();

            $updateBoard = $this->updatePaidBoardLog([
                'selected' => 1,
                'play_status' => 'in_progress',
            ], $pendingBoard->id);

            if($updateBoard){
                return response()->json([
                    'status' => true,
                    'message' => 'ยืนยันเลือกกระดาน &quot;'.$pendingBoard->board_title.'&quot; สำเร็จ',
                    'board_key' => $pendingBoard->board_id,
                    'content' => $this->setPaidBoardInfo($member->uid),
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลองใหม่อีกครั้ง'
                ]);
            }

        }

        public function rejectBoard(Request $request){

            if ($request->has('type') == false && $request->type != 'reject_board') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $boardKey = $request->id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // get pending board info
            $pendingBoard = PaidBoardLog::where('uid', $member->uid)
                                        ->where('board_id', $boardKey)
                                        ->where('selected', 1)
                                        ->where('play_status', 'in_progress')
                                        ->first();

            $updateBoard = $this->updatePaidBoardLog([
                'selected' => 1,
                'play_status' => 'rejected',
            ], $pendingBoard->id);

            if($updateBoard){
                return response()->json([
                    'status' => true,
                    'message' => 'ยกเลิกกระดาน &quot;'.$pendingBoard->board_title.'&quot; สำเร็จ',
                    'content' => $this->setPaidBoardInfo($member->uid),
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'กรุณาลองใหม่อีกครั้ง'
                ]);
            }

        }

        public function getPaidDiceBoard(Request $request){

            if ($request->has('type') == false && $request->type != 'paid_dice_board') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // get paid board info
            $paidBoardInfo = $this->setPaidBoardInfo($uid);

            // check can dice
            if($paidBoardInfo['can_dice'] == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัน<br />ไม่มีข้อมูลกระดานที่คุณเลือก'
                ]);
            }

            $boardKey = $paidBoardInfo['boards']['board_key'];

            // get current board id
            $currBoard = PaidBoardLog::where('uid', $member->uid)
                                    ->where('selected', 1)
                                    ->where('play_status', 'in_progress')
                                    ->first();

            $boardPaidId = $currBoard->id;

            // check diamonds balance
            $diamondBalance = $this->setDiamondsBalance($member->uid);
            if($diamondBalance < $this->random_dice_diamonds){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอทอยลูกเต๋า'
                ]);
            }

            // add deduct diamonds log
            $logDeduct = $this->addPaidDeductLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'diamonds' => $this->random_dice_diamonds,
                'deduct_type' => 'board_dice',
                'status' => 'pending',
                'last_ip' => $this->getIP(),
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
            ]);

            if($logDeduct){

                // set desuct diamonds
                $deductData = $this->setDiamondDeductData($this->random_dice_diamonds);

                $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
                $resp_deduct = json_decode($resp_deduct_raw);
                if(is_object($resp_deduct) && is_null($resp_deduct) == false){

                    $this->updatePaidDeductLog([
                        'before_deduct_diamond' => $diamondBalance,
                        'after_deduct_diamond' => $diamondBalance - $this->random_dice_diamonds,
                        'deduct_status' => $resp_deduct->status ?: 0,
                        'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
                        'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'success'
                    ], $logDeduct['id']);

                    // random dice
                    $randDice = new PaidDice;
                    $randDiceResult = $randDice->randomDice();

                    if($randDiceResult){

                        // add dice log
                        $this->addPaidBoardDiceLog([
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'deduct_id' => $logDeduct['id'],
                            'board_paid_id' => $boardPaidId,
                            'points' => $randDiceResult['points'],
                            'points_title' => $randDiceResult['points_title'],
                            'status' => 'success',
                            'log_date' => date('Y-m-d H:i:s'),
                            'log_date_timestamp' => time(),
                            'last_ip' => $this->getIP(),
                        ]);

                        $hasSpecialPath = false;
                        $specialPath = 0;
                        $totalSpecialPathPoints = 0;
                        $specialRewardList = [];
                        $productTitle = '';

                        $rewardList = [];
                        $rewardSendList = [];
                        $goods_data = [];

                        $paidBoardRewards = new PaidBoardReward;

                        // get total points for check board's step
                        $totalPoints = $this->getCurrentTotalPaidBoardStep($member->uid,$boardPaidId);
                        if($totalPoints >= 100){
                            $totalPoints = 100;

                            // update board is completed
                            $this->updatePaidBoardLog([
                                'play_status' => 'completed',
                            ], $boardPaidId);
                        }

                        // check special points
                        $specialPath = $this->getSpecialPathPoints($totalPoints);
                        if($specialPath > 0){
                            $totalSpecialPathPoints = $totalPoints + $specialPath;
                        }

                        // check garantee rare items
                        $rareRewardList = [];
                        $totalRarePoints = ($totalSpecialPathPoints > 0) ? $totalSpecialPathPoints : $totalPoints;
                        foreach($this->rareItemStep as $value3){
                            if($value3 <= $totalRarePoints){

                                if($this->checkHasRareStepItemHistory($uid,$boardPaidId,$value3) == false){

                                    $rareReward = [];
                                    $rareReward = $paidBoardRewards->setPaidBoardRewardInfo($boardKey,$value3);

                                    foreach ($rareReward['product_set'] as $key4 => $value4) {

                                        $productTitle .= $value4['product_title'].', ';

                                        $rareRewardList[] = [
                                            'title' => $value4['product_title'],
                                            'amount' => $value4['amount'],
                                        ];

                                        $dataArr = [];

                                        if($value4['item_type'] == 'rare'){

                                            $dataArr = [
                                                'uid' => $member->uid,
                                                'username' => $member->username,
                                                'ncid' => $member->ncid,
                                                'board_paid_id' => $boardPaidId,
                                                'board_step' => $value3,
                                                'product_id' => $value4['product_id'],
                                                'product_title' => $value4['product_title'],
                                                'product_quantity' => $value4['product_quantity'],
                                                'amount' => $value4['amount'],
                                                'path_type' => 'normal_path',
                                                'item_type' => $value4['item_type'],
                                                'item_group' => $value4['item_group'],
                                                'send_type' => 'not_send',
                                                'status' => 'not_used',
                                                'log_date' => date('Y-m-d H:i:s'),
                                                'log_date_timestamp' => time(),
                                                'last_ip' => $this->getIP(),
                                            ];

                                        }else{

                                            $dataArr = [
                                                'uid' => $member->uid,
                                                'username' => $member->username,
                                                'ncid' => $member->ncid,
                                                'board_paid_id' => $boardPaidId,
                                                'board_step' => $value3,
                                                'product_id' => $value4['product_id'],
                                                'product_title' => $value4['product_title'],
                                                'product_quantity' => $value4['product_quantity'],
                                                'amount' => $value4['amount'],
                                                'path_type' => 'normal_path',
                                                'item_type' => $value4['item_type'],
                                                'send_type' => 'owner',
                                                'status' => 'used',
                                                'log_date' => date('Y-m-d H:i:s'),
                                                'log_date_timestamp' => time(),
                                                'last_ip' => $this->getIP(),
                                            ];

                                            $rewardSendList[] = [
                                                'title' => $value4['product_title'],
                                                'amount' => $value4['amount'],
                                            ];

                                            $goods_data[] = [
                                                'goods_id' => $value4['product_id'],
                                                'purchase_quantity' => $value4['product_quantity'],
                                                'purchase_amount' => 0,
                                                'category_id' => 40
                                            ];

                                        }

                                        // add item history
                                        $this->addPaidItemHistoryLog($dataArr);

                                    }

                                }

                            }else{
                                break;
                            }
                        }

                        // set reward by board's step
                        $rewards = $paidBoardRewards->setPaidBoardRewardInfo($boardKey,$totalPoints);
                        if($rewards){

                            foreach ($rewards['product_set'] as $key => $value) {

                                if($this->checkHasRareStepItemHistory($uid,$boardPaidId,$totalPoints) == false){

                                    $productTitle .= $value['product_title'].', ';

                                    $rewardList[] = [
                                        'title' => $value['product_title'],
                                        'amount' => $value['amount'],
                                    ];

                                    $dataArr = [];

                                    if($value['item_type'] == 'rare'){

                                        $dataArr = [
                                            'uid' => $member->uid,
                                            'username' => $member->username,
                                            'ncid' => $member->ncid,
                                            'board_paid_id' => $boardPaidId,
                                            'board_step' => $totalPoints,
                                            'product_id' => $value['product_id'],
                                            'product_title' => $value['product_title'],
                                            'product_quantity' => $value['product_quantity'],
                                            'amount' => $value['amount'],
                                            'path_type' => 'normal_path',
                                            'item_type' => $value['item_type'],
                                            'item_group' => $value['item_group'],
                                            'send_type' => 'not_send',
                                            'status' => 'not_used',
                                            'log_date' => date('Y-m-d H:i:s'),
                                            'log_date_timestamp' => time(),
                                            'last_ip' => $this->getIP(),
                                        ];

                                    }else{

                                        $dataArr = [
                                            'uid' => $member->uid,
                                            'username' => $member->username,
                                            'ncid' => $member->ncid,
                                            'board_paid_id' => $boardPaidId,
                                            'board_step' => $totalPoints,
                                            'product_id' => $value['product_id'],
                                            'product_title' => $value['product_title'],
                                            'product_quantity' => $value['product_quantity'],
                                            'amount' => $value['amount'],
                                            'path_type' => 'normal_path',
                                            'item_type' => $value['item_type'],
                                            'send_type' => 'owner',
                                            'status' => 'used',
                                            'log_date' => date('Y-m-d H:i:s'),
                                            'log_date_timestamp' => time(),
                                            'last_ip' => $this->getIP(),
                                        ];

                                        $rewardSendList[] = [
                                            'title' => $value['product_title'],
                                            'amount' => $value['amount'],
                                        ];

                                        $goods_data[] = [
                                            'goods_id' => $value['product_id'],
                                            'purchase_quantity' => $value['product_quantity'],
                                            'purchase_amount' => 0,
                                            'category_id' => 40
                                        ];

                                    }

                                    // add item history
                                    $this->addPaidItemHistoryLog($dataArr);
                                }

                            }

                            // check special path
                            if($specialPath > 0){

                                // add special path for board points
                                $this->addPaidBoardDiceLog([
                                    'uid' => $member->uid,
                                    'username' => $member->username,
                                    'ncid' => $member->ncid,
                                    'deduct_id' => $logDeduct['id'],
                                    'board_paid_id' => $boardPaidId,
                                    'points' => $specialPath,
                                    'points_title' => 'บรรไดงูทางลัดพิเศษ '.$specialPath .' แต้ม',
                                    'status' => 'success',
                                    'log_date' => date('Y-m-d H:i:s'),
                                    'log_date_timestamp' => time(),
                                    'last_ip' => $this->getIP(),
                                ]);

                                // $totalSpecialPathPoints = $totalPoints + $specialPath;

                                $specialRewards = $paidBoardRewards->setPaidBoardRewardInfo($boardKey,$totalSpecialPathPoints);

                                foreach ($specialRewards['product_set'] as $key2 => $value2) {

                                    if($this->checkHasRareStepItemHistory($uid,$boardPaidId,$totalSpecialPathPoints) == false){
                                        $productTitle .= $value2['product_title'].', ';

                                        $specialRewardList[] = [
                                            'title' => $value2['product_title'],
                                            'amount' => $value2['amount'],
                                        ];

                                        $dataArr = [];

                                        if($value2['item_type'] == 'rare'){

                                            $dataArr = [
                                                'uid' => $member->uid,
                                                'username' => $member->username,
                                                'ncid' => $member->ncid,
                                                'board_paid_id' => $boardPaidId,
                                                'board_step' => $totalSpecialPathPoints,
                                                'product_id' => $value2['product_id'],
                                                'product_title' => $value2['product_title'],
                                                'product_quantity' => $value2['product_quantity'],
                                                'amount' => $value2['amount'],
                                                'path_type' => 'special_path',
                                                'item_type' => $value2['item_type'],
                                                'item_group' => $value2['item_group'],
                                                'send_type' => 'not_send',
                                                'status' => 'not_used',
                                                'log_date' => date('Y-m-d H:i:s'),
                                                'log_date_timestamp' => time(),
                                                'last_ip' => $this->getIP(),
                                            ];

                                        }else{

                                            $dataArr = [
                                                'uid' => $member->uid,
                                                'username' => $member->username,
                                                'ncid' => $member->ncid,
                                                'board_paid_id' => $boardPaidId,
                                                'board_step' => $totalSpecialPathPoints,
                                                'product_id' => $value2['product_id'],
                                                'product_title' => $value2['product_title'],
                                                'product_quantity' => $value2['product_quantity'],
                                                'amount' => $value2['amount'],
                                                'path_type' => 'special_path',
                                                'item_type' => $value2['item_type'],
                                                'send_type' => 'owner',
                                                'status' => 'used',
                                                'log_date' => date('Y-m-d H:i:s'),
                                                'log_date_timestamp' => time(),
                                                'last_ip' => $this->getIP(),
                                            ];

                                            $rewardSendList[] = [
                                                'title' => $value2['product_title'],
                                                'amount' => $value2['amount'],
                                            ];

                                            $goods_data[] = [
                                                'goods_id' => $value2['product_id'],
                                                'purchase_quantity' => $value2['product_quantity'],
                                                'purchase_amount' => 0,
                                                'category_id' => 40
                                            ];

                                        }

                                        // add item history
                                        $this->addPaidItemHistoryLog($dataArr);
                                    }

                                }

                            }

                            if (is_null($goods_data) == false && is_array($goods_data) && count($goods_data) > 0 && is_null($resp_deduct->response->purchase_id) == false) {

                                // add send item log
                                $itemLog = $this->addPaidSendItemLog([
                                    'uid' => $member->uid,
                                    'username' => $member->username,
                                    'ncid' => $member->ncid,
                                    'product_title' => $productTitle,
                                    'product_set' => json_encode($rewardSendList),
                                    'send_item_status' => false,
                                    'send_item_purchase_id' => 0,
                                    'send_item_purchase_status' => 0,
                                    'goods_data' => json_encode($goods_data),
                                    'status' => 'pending',
                                    'log_date' => (string)date('Y-m-d'), // set to string
                                    'log_date_timestamp' => time(),
                                    'last_ip' => $this->getIP(),
                                ]);

                                //send item
                                $send_result_raw = $this->apiSendItem($ncid, $goods_data);
                                $send_result = json_decode($send_result_raw);
                                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                                    //do update send item log
                                    $this->updatePaidSendItemLog([
                                        'send_item_status' => $send_result->status ? : false,
                                        'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                                        'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                                        'status' => 'success'
                                    ], $itemLog['id'], $member->uid);

                                }else{
                                    //do update send item log
                                    $this->updatePaidSendItemLog([
                                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                                        'status' => 'unsuccess'
                                    ], $itemLog['id'], $member->uid);

                                    return response()->json([
                                        'status' => false,
                                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                                    ]);
                                }

                            }

                            $paidBoardInfo = $this->setPaidBoardInfo($member->uid);

                            // set response
                            return response()->json([
                                'status' => true,
                                'message' => 'ทอยลูกเต๋สำเร็จ!',
                                'next_step' => $totalPoints,
                                'next_step_rewards' => $rewardList,
                                'has_special_path' => $specialPath > 0,
                                'special_path_step' => $totalSpecialPathPoints,
                                'special_path_rewards' => $specialRewardList,
                                'rare_reward' => $rareRewardList,
                                'content' => $paidBoardInfo,
                            ]);

                        }else{
                            return response()->json([
                                'status' => false,
                                'message' => 'ไม่สามารถทอยลูกเต๋าได้<br />กรุณาลองใหม่อีกครั้ง(2)',
                            ]);
                        }

                    }else{
                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถทอยลูกเต๋าได้<br />กรุณาลองใหม่อีกครั้ง',
                        ]);
                    }

                }else{
                    $arr_log['status'] = 'unsuccess';
                    $this->updatePaidDeductLog($arr_log, $logDeduct['id']);
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }

            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />กรุณาลองใหม่อีกครั้ง'
                ]);
            }

        }

        public function getItemHistory(Request $request)
        {
            if ($request->has('type') == false && $request->type != 'item_history') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $resp_raw = PaidItemHistory::select('product_title', 'status', 'item_type as type', 'send_type', 'send_to_uid', 'send_to_name', 'log_date_timestamp', 'created_at')
                                    ->where('uid', $uid)
                                    ->orderBy('created_at', 'DESC')
                                    ->orderBy('id', 'DESC')
                                    ->get();
            $resp = [];
            if (count($resp_raw) > 0) {
                foreach ($resp_raw as $key => $value) {
                    $resp[$key]['created_at'] = date('d/m/Y H:i:s', $value->log_date_timestamp);
                    $resp[$key]['item_title'] = $value->product_title;
                    $resp[$key]['send_to_uid'] = !empty($value->send_to_uid) ? $value->send_to_uid : "";
                }
            }

            $dataResp = collect($resp);
            $data = $dataResp->except(['id', 'status', 'type', 'send_type']);

            return response()->json([
                'status' => true,
                'content' => [
                    'username' => $member->username,
                    'items_list'=>$data
                ],
            ]);

        }

        public function getCheckUid(Request $request)
        {
            if ($request->has('type') == false && $request->type != 'check_uid') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $groupId = $request->id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            if($request->has('uid') && $request->uid != ''){

                $target_uid = intval($request->uid);
                $targetInfo = [];
                $targetInfo = $this->apiUserInfo($target_uid);
    
                if ($uid == $target_uid) {
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งของขวัญ<br />ให้ UID ของตัวเองได้'
                    ]);
                }
    
                if (is_array($targetInfo) && !empty($targetInfo['user_name'])){
                    return response()->json([
                        'status' => true,
                        'content' => [
                            'username' => $member->username,
                            'target_uid' => $target_uid,
                            'target_username' => $targetInfo['user_name']
                        ]
                    ]);
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'ขออภัยไม่มีข้อมูล UID นี้'
                    ]);
                }
    
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter!'
                ]);
            }

        }

        public function getSendGift(Request $request)
        {
            if ($request->has('type') == false && $request->type != 'send_gift') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $groupId = $request->id;
            $target_uid = $request->uid;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // check item remain quentity
            $itemRemainCount = $this->getRareItemCountByGroup($member->uid,$groupId);
            if($itemRemainCount <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไอเทมที่ต้องการส่งมีจำนวนไม่พอ'
                ]);
            }

            // set product info
            $itemInfo = $this->getRareItemInfoByGroup($member->uid,$groupId);
            if($itemInfo == false){
                return response()->json([
                    'status' => false,
                    'message' => 'No reward data required.'
                ]);
            }

            $giftName = $itemInfo->product_title;

            $targetInfo = $this->apiUserInfo($target_uid);
            $target_username = $targetInfo['user_name'];
            $target_ncid = $targetInfo['user_id']; // ncid recipient

            if ($member->uid == $target_uid) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งไอเทมให้<br /> UID ของตัวเองได้'
                ]);
            }

            $diamondBalanceRaw = $this->apiDiamondBalance($member->uid);
            $diamondBalance = json_decode($diamondBalanceRaw);
            if ($diamondBalance->status) {

                if ($diamondBalance->balance >= $this->send_gift_diamonds) {

                    $deductLog = $this->addPaidDeductLog([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'diamonds' => $this->send_gift_diamonds,
                        'deduct_type' => 'send_gift',
                        'status' => 'pending',
                        'log_date' => (string)date('Y-m-d'), // set to string
                        'log_date_timestamp' => time(),
                        'last_ip' => $this->getIP(),
                    ]);

                    // set desuct diamonds
                    $deductData = $this->setDiamondDeductData($this->send_gift_diamonds);
                    $resp_deduct_raw = $this->apiDeductDiamond($ncid, $deductData);
                    $resp_deduct = json_decode($resp_deduct_raw);
                    if (is_object($resp_deduct) && is_null($resp_deduct) == false) {

                        $arrLog = [
                            'before_deduct_diamond' => $diamondBalance->balance,
                            'after_deduct_diamond' => $diamondBalance->balance - $this->send_gift_diamonds,
                            'deduct_status' => $resp_deduct->status ? : 0,
                            'deduct_purchase_id' => $resp_deduct->response->purchase_id ? : 0,
                            'deduct_purchase_status' => $resp_deduct->response->purchase_status ? : 0,
                            'deduct_data' => json_encode($deductData),
                        ];

                        if($resp_deduct->status){

                            $arrLog['status'] = 'success';
                            $this->updatePaidDeductLog($arrLog, $deductLog['id']);

                            $logData = $this->addPaidSendItemLog([//add send item log
                                'uid' => $target_uid,
                                'username' => $target_username,
                                'ncid' => $target_ncid,
                                'status' => 'pending',
                                'send_gift_from' => $member->uid,
                                'last_ip' => $this->getIP(),
                                'log_date' => (string)date('Y-m-d'), // set to string
                                'log_date_timestamp' => time(),
                            ]);

                            $goodsData = [
                                [
                                    'goods_id' => $itemInfo->product_id,
                                    'purchase_quantity' => $itemInfo->product_quantity,
                                    'purchase_amount' => 0,
                                    'category_id' => 40
                                ]
                            ];
                
                            $this->updatePaidSendItemLog(['goods_data' => json_encode($goodsData)], $logData['id'], $target_uid);

                            $deductItemResult = $this->deductRareItem([
                                'status' => 'used',
                                'send_type' => 'gift',
                                'send_to_uid' => $target_uid,
                                'send_to_name' => $target_username,
                            ], $itemInfo->id, $member->uid);
                            if($deductItemResult){

                                //send item
                                $send_result_raw = $this->apiSendItem($target_ncid, $goodsData);
                                $send_result = json_decode($send_result_raw);
                                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                                    //do update send item log
                                    $this->updatePaidSendItemLog([
                                        'send_item_status' => $send_result->status ? : false,
                                        'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                                        'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                                        'status' => 'success'
                                    ], $logData['id'], $target_uid);

                                }else{
                                    //do update send item log
                                    $this->updatePaidSendItemLog([
                                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                                        'status' => 'unsuccess'
                                    ], $logData['id'], $target_uid);

                                    return response()->json([
                                        'status' => false,
                                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                                    ]);
                                }

                                return response()->json([
                                    'status' => true,
                                    'message' => 'ส่ง &quot;' . $giftName . '&quot; ไปยัง <br />' . $target_username . ' ( ' . $target_uid . ' ) <br />สำเร็จ กรุณาแจ้งไปยัง UID <br /> ที่ท่านส่งของขวัญไปให้<br />และให้ตรวจสอบไอเทมในกล่องจดหมายภายในเกม',
                                    'content' => [
                                        'username' => $member->username,
                                        'character' => $member->char_name,
                                        'my_bag' => $this->setMyBag($member->uid)
                                    ],
                                ]);

                            }else{
                                $this->updatePaidSendItemLog(['status' => 'unsuccess'], $logData['id'], $member->uid);
                                return response()->json([
                                    'status' => false,
                                    'message' => 'ไม่สามารส่งไอเทมได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                                ]);
                            }
                            

                        }else {
                            //error deduct code
                            $arrLog['status'] = 'unsuccess';
                            $this->updatePaidDeductLog($arrLog, $deductLog['id']);
                        }

                    }else{
                        $arrLog['status'] = 'unsuccess';
                        $this->updatePaidDeductLog($arrLog, $deductLog['id']);
                    }

                    return response()->json([
                        'status' => false,
                        'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);

                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'จำนวนไดมอนด์ของคุณไม่พอในการส่งของขวัญ'
                    ]);
                }

            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถอัพเดทข้อมูลไดมอนด์ของคุณได้กรุณาลองใหม่อีกครั้ง'
                ]);
            }

        }

        public function getSendItem(Request $request)
        {
            if ($request->has('type') == false && $request->type != 'send_item') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $groupId = $request->id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // check item remain quentity
            $itemRemainCount = $this->getRareItemCountByGroup($member->uid,$groupId);
            if($itemRemainCount <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไอเทมที่ต้องการส่งมีจำนวนไม่พอ'
                ]);
            }

            // set product info
            $itemInfo = $this->getRareItemInfoByGroup($member->uid,$groupId);
            if($itemInfo == false){
                return response()->json([
                    'status' => false,
                    'message' => 'No data required.'
                ]);
            }

            $logData = $this->addPaidSendItemLog([//add send item log
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'status' => 'pending',
                'last_ip' => $this->getIP(),
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
            ]);

            $goodsData = [
                [
                    'goods_id' => $itemInfo->product_id,
                    'purchase_quantity' => $itemInfo->product_quantity,
                    'purchase_amount' => 0,
                    'category_id' => 40
                ]
            ];

            $this->updatePaidSendItemLog(['goods_data' => json_encode($goodsData)], $logData['id'], $member->uid);

            // deduct item
            $deductItemResult = $this->deductRareItem([
                                'status' => 'used',
                                'send_type' => 'owner',
                            ], $itemInfo->id, $member->uid);
            if($deductItemResult){
                
                //send item
                $send_result_raw = $this->apiSendItem($ncid, $goodsData);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                    //do update send item log
                    $this->updatePaidSendItemLog([
                        'send_item_status' => $send_result->status ? : false,
                        'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                        'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                        'status' => 'success'
                    ], $logData['id'], $member->uid);

                }else{
                    //do update send item log
                    $this->updatePaidSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                        'status' => 'unsuccess'
                    ], $logData['id'], $member->uid);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                    ]);
                }

                return response()->json([
                    'status' => true,
                    'message' => 'ส่งไอเทมสำเร็จ<br/>กรุณาตรวจสอบไอเทมที่กล่องจดหมายภายในเกม',
                    'content' => [
                        'username' => $member->username,
                        'character' => $member->char_name,
                        'my_bag' => $this->setMyBag($member->uid)
                    ],
                ]);

            }else{
                $this->updatePaidSendItemLog(['status' => 'unsuccess'], $logData['id'], $member->uid);
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารส่งไอเทมได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }

        }

        public function getMyBag(Request $request)
        {
            if ($request->has('type') == false && $request->type != 'my_bag') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            return response()->json([
                'status' => true,
                'content' => [
                    'username' => $member->username,
                    'character' => $member->char_name,
                    'my_bag' => $this->setMyBag($member->uid)
                ],
            ]);

        }

        private function setMyBag($uid)
        {
            $bagList = [];
            $rewards = new PaidBoardReward;
            foreach($rewards->rareItemList() as $rare){
                $rareItemCount = 0;
                $rareItemCount = $this->getRareItemCountByGroup($uid,$rare['id']);
                $bagList[] = [
                    'id' => $rare['id'],
                    'title' => $rare['title'],
                    'quantity' => $rareItemCount,
                ];
            }

            return $bagList;
        }

        private function getRareItemCountByGroup($uid, $itemGroup)
        {
            return PaidItemHistory::where('uid', $uid)
                            ->where('item_group', $itemGroup)
                            ->where('status', 'not_used')
                            ->where('send_type', 'not_send')
                            ->where('item_type', 'rare')
                            ->count();
        }

        private function getRareItemInfoByGroup($uid, $itemGroup)
        {
            return PaidItemHistory::where('uid', $uid)
                            ->where('item_group', $itemGroup)
                            ->where('status', 'not_used')
                            ->where('send_type', 'not_send')
                            ->where('item_type', 'rare')
                            ->first();
        }

        private function deductRareItem(array $arr, $id, $uid)
        {
            return PaidItemHistory::where('id', $id)
                            ->where('uid', $uid)
                            ->where('status', 'not_used')
                            ->where('send_type', 'not_send')
                            ->where('item_type', 'rare')
                            ->update($arr);
        }

        private function checkHasRareStepItemHistory($uid,$boardPaidId,$boardStep){ // boardPaidId = auto increment id from board table
            $rareItemCount = PaidItemHistory::where('uid', $uid)
                                            ->where('board_paid_id', $boardPaidId)
                                            ->where('board_step', $boardStep)
                                            ->count();
            return $rareItemCount > 0;
        }


        private function setPaidBoardInfo($uid){

            if(empty($uid)) {
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            // get diamonds balance
            $diamondBalance = $this->setDiamondsBalance($member->uid);

            $boards = $this->setCurrentPaidBoard($member->uid);

            return [
                'username' => $member->username,
                'character' => $member->char_name,
                'can_random_board' => $diamondBalance >= $this->random_board_diamonds,
                'can_dice' => $diamondBalance >= $this->random_dice_diamonds && $boards['has_board'] == true,
                'boards' => $boards,
            ];

        }

        private function setDiamondsBalance($uid){

            $diamondBalanceRaw = $this->apiDiamondBalance($uid);
            $diamondBalance = json_decode($diamondBalanceRaw);

            $diamonds = 0;
            if($diamondBalance->status == true) {
                $diamonds = intval($diamondBalance->balance);
            }

            return $diamonds;
        }

        private function setCurrentPaidBoard($uid){

            if(empty($uid)) {
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $dateTime = $this->setQuestDateTime();

            $hasCurrentBoard = false;
            $currBoardInfo = [
                'has_board' => false,
                'board_key' => 0,
                'board_title' => '',
                'current_step' => 0,
            ];

            // check and get current board
            $currBoard = PaidBoardLog::where('uid', $member->uid)
                                    ->where('selected', 1)
                                    ->where('play_status', 'in_progress')
                                    ->first();

            if(isset($currBoard) && is_null($currBoard->board_id) == false){

                $currBoardInfo['has_board'] = true;

                $currBoardInfo['board_key'] = $currBoard->board_id;
                $currBoardInfo['board_title'] = $currBoard->board_title;

                // get current step
                $currBoardInfo['current_step'] = $this->getCurrentTotalPaidBoardStep($member->uid, $currBoard->id);

            }

            return $currBoardInfo;
        }

        private function getCurrentTotalPaidBoardStep($uid, $boardPaidId){

            if(empty($uid) && empty($boardPaidId)){
                return false;
            }

            $count = PaidBoardDiceLog::where('uid', $uid)
                                    ->where('board_paid_id', $boardPaidId)
                                    ->sum('points');
            $count = $count >= 100 ? 100 : $count;
            return intval($count);
        }

        private function apiDiamondBalance(int $uid){
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'balance',
                'uid' => $uid,
            ]);
        }

        private function setDiamondDeductData($deduct_diamond_amount = 0){
            if ($deduct_diamond_amount == 0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function apiDeductDiamond(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamomds for garena events bns snake ladder.',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addPaidItemHistoryLog(array $arr) {
            if ($arr) {
                $resp = PaidItemHistory::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updatePaidItemHistoryLog(array $arr, $logId, $uid) {
            if ($arr) {
                return PaidItemHistory::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function addPaidSendItemLog(array $arr) {
            if ($arr) {
                $resp = PaidItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updatePaidSendItemLog(array $arr, $logId, $uid) {
            if ($arr) {
                return PaidItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function addPaidDeductLog(array $arr){
            if ($arr) {
                $resp = PaidDeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updatePaidDeductLog(array $arr, $log_id){
            if ($arr) {
                return PaidDeductLog::where('id', $log_id)->update($arr);
            }
            return null;
        }

        private function addPaidBoardLog(array $arr){
            if ($arr) {
                $resp = PaidBoardLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updatePaidBoardLog(array $arr, $log_id){
            if ($arr) {
                return PaidBoardLog::where('id', $log_id)->update($arr);
            }
            return null;
        }

        private function addPaidBoardDiceLog(array $arr){
            if ($arr) {
                $resp = PaidBoardDiceLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updatePaidBoardDiceLog(array $arr, $log_id){
            if ($arr) {
                return PaidBoardDiceLog::where('id', $log_id)->update($arr);
            }
            return null;
        }

        // End Paid Board

    }
