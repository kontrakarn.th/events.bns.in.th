<?php

    namespace App\Http\Controllers\Api\Test\preregis_warden;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\Test\preregis_warden\Member;
    use App\Models\Test\preregis_warden\Reward;
    use App\Models\Test\preregis_warden\ItemLog;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2018-12-26 12:00:00';
        private $endTime = '2019-02-13 05:59:59';

        private $startPreregisTime = '2018-12-26 12:00:00';
        private $endPreregisTime = '2019-01-08 23:59:59';

        private $startWardenQuestTime = '2019-01-16 06:00:00';
        private $endWardenQuestTime = '2019-02-13 23:59:59';

        private $startApiTime = '00:00:00';
        private $endApiTime = '23:59:59';

        private $quests = [
            [
                'id' => 1784,
                'title' => 'เปลวไฟแห่งความลำบาก'
            ],
            [
                'id' => 1819,
                'title' => 'เข้าร่วมสงครามเกาะแห่งพันธนาการ 1 ครั้ง'
            ],
            [
                'id' => 1612,
                'title' => 'ผ่านเควสเนื้อเรื่องบทที่ 20 ชีวิตใหม่ของซอยอน'
            ],
        ];

        private $awtSecret = '';

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            /* if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            } */

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){

                // check member has game info
                $uid = $this->userData['uid'];
                $username = $this->userData['username'];
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    die(json_encode([
                        'status' => false,
                        'type' => 'no_game_info',
                        'message' => 'ขออภัย<br />กรุณาล็อคอินเข้าเกมเพื่อสร้างตัวละครก่อนร่วมกิจกรรม'
                    ]));
                }

                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:PREREGIS_WARDEN:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        public function acceptChar(Request $request)
        {

            if($request->has('type') == false && $request->type != 'accept_character') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter(1)!'
                ]);
            }

            if ($request->has('id') == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'invalid parameter(2)!'
                ]);
            }
            $char_id = $request->id;
            if (empty($char_id)) {
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่มีตัวละครที่ต้องการเลือก'
                ]);
            }

            // $char_id = $request->id;

            $uid = $this->userData['uid'];
            $username = $this->userData['username'];

            if ($resp = $this->hasAcceptChar($uid) == false) {
                $ncid = $this->getNcidByUid($uid);
                $characters = $this->doGetChar($uid,$ncid);
                $data = $characters[$char_id-1];
                if (isset($data['char_id'])) {
                    $this->doAcceptChar($data['char_id'], $data['char_name'], $data['world_id'], $uid);
                    $eventInfo = $this->setEventInfo($uid);
                    return response()->json([
                                'status' => true,
                                'message' => 'ยืนยันตัวละครเสร็จสิ้น',
                                'content' => $eventInfo
                    ]);
                } else {
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่พบตัวละครที่คุณต้องการเลือก'
                    ]);
                }
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก'
                ]);
            }
        }

        private function doAcceptChar($char_id, $char_name, $world_id, $uid)
        {
            return Member::where('uid', $uid)->update([
                        'char_id' => $char_id,
                        'char_name' => $char_name,
                        'world_id' => $world_id,
                    ]);
        }

        private function hasAcceptChar($uid)
        {
            $resp = Member::where('uid', $uid)->first();
            if (empty($resp->char_id) && empty($resp->char_name)) {
                return false;
            } else {
                return $resp;
            }
        }

        public function getCharacter(Request $request)
        {

            if($request->has('type') == false && $request->type != 'get_characters') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $this->checkMember();
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            $resp = $this->hasAcceptChar($uid);
            if ($resp == false) {
                $charactersList = [];
                $ncid = $this->getNcidByUid($uid);
                $characters = $this->doGetChar($uid,$ncid);
                foreach ($characters as $character) {
                    $charactersList[] = collect($character)->except(['char_id', 'world_id', 'job', 'level', 'creation_time', 'last_play_start', 'last_play_end', 'last_ip', 'exp', 'mastery_level']);
                }
                return response()->json([
                            'status' => true,
                            'content' => $charactersList,
                            'accepted' => false
                ]);
            } else {
                return response()->json([
                            'status' => true,
                            'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก',
                            'accepted' => true
                ]);
            }
        }

        private function doGetChar($uid,$ncid){
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i =1;
                    foreach ($resp->response as $key => $value) {
                        if($value->job==11){ // set chars info filter only for warden class
                            $content[] = [
                                'id' => $i,
                                'char_id' => $value->id,
                                'char_name' => $value->name,
                                'world_id' => $value->world_id,
                                'job' => $value->job,
                                'level' => $value->level,
                                'creation_time' => $value->creation_time,
                                'last_play_start' => $value->last_play_start,
                                'last_play_end' => $value->last_play_end,
                                'last_ip' => $value->last_ip,
                                'exp' => $value->exp,
                                'mastery_level' => $value->mastery_level,
                            ];
    
                            $i++;
                        }
                    }
                    return $content;
                }
            }
            return null;
        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:PREREGIS_WARDEN:CHARACTERS_' . $uid, 10, function() use($ncid) {
                        return $this->post_api($this->baseApi, [
                                    'key_name' => 'bns',
                                    'service' => 'characters',
                                    'user_id' => $ncid
                        ]);
                    });
        }

        private function dosendItem(string $ncid, array $goodsData) {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events BNS Pre-register Warden',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addSendItemLog(array $arr) {
            if ($arr) {
                $resp = ItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateSendItemLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function apiCheckQuestCompleted($uid, $charId, $questId, $start, $end) {
            $resp = Cache::remember('BNS:PREREGIS_WARDEN:QUEST_COMPLETED_' . $questId . '_' . $uid, 5, function()use($charId,$questId,$start,$end) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'quest_completed',
                    'char_id' => $charId,
                    'quest_id' => $questId,
                    'start_time' => $start,
                    'end_time' => $end
                ]);
            });

            return json_decode($resp);
        }

        private function setQuestCompletedCount($uid, $charId, $questId, $start, $end){

            $completedCount = 0;

            $apiCompletedCount = $this->apiCheckQuestCompleted($uid, $charId, $questId, $start, $end);
            if($apiCompletedCount->status){
                $completedCount = intval($apiCompletedCount->response->quest_completed_count);
            }

            return $completedCount;

        }

        private function updateMemberInfo(array $arr, $logId, $uid) {
            if ($arr) {
                return Member::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        public function getEventInfo(Request $request)
        {

            if ($request->has('type') == false && $request->type != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            if($eventInfo == false){
                $eventInfo = [];
            }

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
            ]);

        }

        private function setEventInfo($uid=null){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            // check event for pre-regis
            $startPreregisTimestamp = strtotime($this->startPreregisTime);
            $endPreregisTimestamp = strtotime($this->endPreregisTime);

            $can_preregis = false;
            $is_registered = false;
            $preregis_can_receive = false;
            $preregis_received = false;

            if(time() >= $startPreregisTimestamp && time() <= $endPreregisTimestamp){
                $can_preregis = true;
            }

            if(in_array($member->answer, [1,2,3,4])){
                $is_registered = true;
            }

            if(in_array($member->answer, [1,2,3,4])){
                $preregis_can_receive = true;
            }

            $preRegisItemHistory = ItemLog::where('uid', $member->uid)->where('package_id', 1)->count();
            if($preRegisItemHistory > 0){
                $preregis_received = true;
            }


            // check event for warden quests
            $quest_1 = false;
            $quest_2 = false;
            $quest_3 = false;
            $warden_can_receive = false;
            $warden_received = false;
            $can_select_char = false;
            $selected_char = false;

            $startWardenQuestTimestamp = strtotime($this->startWardenQuestTime);
            $endWardenQuestTimestamp = strtotime($this->endWardenQuestTime);

            // check start and end events warden
            if($is_registered == true && time() >= $startWardenQuestTimestamp && time() <= $endWardenQuestTimestamp && $preregis_received == true){

                $can_select_char = true;

                if(!empty($member->char_id)){
                    
                    $selected_char = true;
                
                    // check quest 1 completed 
                    if($member->quest_01 == 1){
                        $quest_1 = true;
                    }else{
                        $quest1_info = $this->quests[0];
                        $quest1CompletedCount = $this->setQuestCompletedCount($member->uid, $member->char_id, $quest1_info['id'], $this->startWardenQuestTime, $this->endWardenQuestTime);
                        if($quest1CompletedCount > 0){
                            $quest_1 = true;
                            
                            // update quest
                            $this->updateMemberInfo([
                                'quest_01' => 1,
                            ], $member->id, $member->uid);
                        }
                    }

                    // check quest 2 completed 
                    if($member->quest_02 == 1){
                        $quest_2 = true;
                    }else{
                        $quest2_info = $this->quests[1];
                        $quest2CompletedCount = $this->setQuestCompletedCount($member->uid, $member->char_id, $quest2_info['id'], $this->startWardenQuestTime, $this->endWardenQuestTime);
                        if($quest2CompletedCount > 0){
                            $quest_2 = true;
                            
                            // update quest
                            $this->updateMemberInfo([
                                'quest_02' => 1,
                            ], $member->id, $member->uid);
                        }
                    }

                    // check quest 3 completed 
                    if($member->quest_03 == 1){
                        $quest_3 = true;
                    }else{
                        $quest3_info = $this->quests[2];
                        $quest3CompletedCount = $this->setQuestCompletedCount($member->uid, $member->char_id, $quest3_info['id'], $this->startWardenQuestTime, $this->endWardenQuestTime);
                        if($quest3CompletedCount > 0){
                            $quest_3 = true;
                            
                            // update quest
                            $this->updateMemberInfo([
                                'quest_03' => 1,
                            ], $member->id, $member->uid);
                        }
                    }
                    

                    if($quest_1 == true && $quest_2 == true && $quest_3 == true){
                        $warden_can_receive = true;
                    }

                    $itemHistory = ItemLog::where('uid', $member->uid)->where('package_id', 2)->count();
                    if($itemHistory > 0){
                        $warden_received = true;
                    }

                }
            }
            
            return [
                'username' => $member->username,
                'character' => $member->char_name,
                'can_select_char' => $can_select_char,
                'selected_char' => $selected_char,
                'can_preregis' => $can_preregis,
                'is_registered' => $is_registered,
                'answer' => $member->answer,
                'preregis_can_receive' => $preregis_can_receive,
                'preregis_received' => $preregis_received,
                'quest_1' => $quest_1,
                'quest_2' => $quest_2,
                'quest_3' => $quest_3,
                'warden_can_receive' => $warden_can_receive,
                'warden_received' => $warden_received,
            ];

        }

        public function sendAnswer(Request $request)
        {

            if ($request->has('type') == false && $request->type != 'send_answer') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $answer = $request->id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $checkEventInfo = $this->setEventInfo($uid);

            if($checkEventInfo['can_preregis'] == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถลงทะเบียนได้<br />เนื่องจากไม่อยู่ในช่วงเวลาลงทะเบียน'
                ]);
            }

            if($checkEventInfo['is_registered'] == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้ลงทะเบียนไปแล้ว'
                ]);
            }

            if(!in_array($answer, [1,2,3,4])){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณไม่ได้่เลือกคำตอบที่กดหนดไว้'
                ]);
            }

            // update answer
            $member->answer = $answer;
            $member->package_discount = 1;

            if($member->save()){

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                    'status' => true,
                    'content' => $eventInfo,
                    'message' => 'ส่งแบบสอบถามสำเร็จ<br />คุณสามารถรับแพ็คเกจลงทะเบียนล่วงหน้าได้ทันที'
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถลงทพเบียนได้<br />กรุณาลองใหม่อีกครั้ง'
                ]);
            }

        }

        public function redeemReward(Request $request) {

            if ($request->has('type') == false && $request->type != 'redeem_reward') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }

            if ($request->has('package_id') == false || !in_array($request->package_id, [1,2])) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (2)!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            $package_id = $request->package_id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $checkEventInfo = $this->setEventInfo($uid);

            switch($package_id){

                case 1:

                    if($checkEventInfo['is_registered'] == false){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />คุณยังไม่ได้ลงทะเบียน'
                        ]);
                    }
        
                    if(!in_array($checkEventInfo['answer'], [1,2,3,4])){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />คุณไม่ได้่เลือกคำตอบที่กดหนดไว้'
                        ]);
                    }
        
                    if($checkEventInfo['preregis_can_receive'] == false){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />ไม่ตรงเงื่อนไขการรับของรางวัล'
                        ]);
                    }
        
                    if($checkEventInfo['preregis_received'] == true){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปแล้ว'
                        ]);
                    }

                    break;

                case 2:

                    if(empty($member->char_id)){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />คุณยังไม่ได้เลือกอาชีพ warden เพื่อนทำภารกิจ'
                        ]);
                    }

                    if($checkEventInfo['quest_1'] != 1){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />ภารกิจ "เปลวไฟแห่งความลำบาก" ยังไม่สำเร็จ'
                        ]);
                    }

                    if($checkEventInfo['quest_2'] != 1){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />ภารกิจ "เข้าร่วมสงครามเกาะแห่งพันธนาการ 1 ครั้ง" ยังไม่สำเร็จ'
                        ]);
                    }

                    if($checkEventInfo['quest_3'] != 1){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />ภารกิจ "ผ่านเควสบทที่ 20 ชีวิตใหม่ของซอยอน" ยังไม่สำเร็จ'
                        ]);
                    }

                    if($checkEventInfo['warden_can_receive'] == false){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />ไม่ตรงเงื่อนไขการรับของรางวัล'
                        ]);
                    }
        
                    if($checkEventInfo['warden_received'] == true){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />คุณได้รับของรางวัลไปแล้ว'
                        ]);
                    }

                    break;

                default:
                    return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter (3)!'
                    ]);
                    break;
            }

            // set reward
            $rewards = new Reward;
            $rewardInfo = $rewards->setRewardByPackageId($package_id);

            $goods_data = [];

            if(count($rewardInfo['product_set']) > 0){
                foreach($rewardInfo['product_set'] as $product){
                    $goods_data[] = [
                        'goods_id' => $product['product_id'],
                        'purchase_quantity' => $product['product_quantity'],
                        'purchase_amount' => 0,
                        'category_id' => 40
                    ];
                }
            }

            $itemLog = $this->addSendItemLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'package_id' => $rewardInfo['package_id'],
                'package_title' => $rewardInfo['package_title'],
                'status' => 'pending',
                'send_item_status' => false,
                'send_item_purchase_id' => '0',
                'send_item_purchase_status' => '0',
                'goods_data' => json_encode($goods_data),
                'log_date' => date('Y-m-d H:i:s'),
                'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                'last_ip' => $this->getIP()
            ]);

            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                    'status' => 'success'
                ], $itemLog['id'], $uid);


                if($package_id==1){
                    // update quest
                    $this->updateMemberInfo([
                        'package_discount' => 1,
                    ], $member->id, $member->uid);
                }

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                    'status' => true,
                    'message' => 'ส่งของรางวัลเสร็จเรียบร้อย<br />กรุณาตรวจสอบกล่องจดหมายภายในเกม',
                    'content' => $eventInfo
                ]);

            }else{
                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                    'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                    'status' => 'unsuccess'
                ], $itemLog['id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                    'content' => $eventInfo
                ]);
            }


        }

    }