<?php

    namespace App\Http\Controllers\Api\Test\third_spec_assassin;

    use App\Http\Controllers\Api\BnsEventController;

    use App\Models\Test\third_spec_assassin\Users;
    use App\Models\Test\bns_word\BnsWording;

class UsersController extends BnsEventController{
    private $baseApi = 'http://api.apps.garena.in.th';

    private function setEventInfo($uid)
    {
        $member = Users::where('uid', $uid)->first();

        if (!$member) {
            return [
                'status' => false,
                'message' => 'No member data.'
            ];
        }

        $canget = $this->userCanGetReward($member->uid);
        if($canget == 1){
            $canget = true;
        }else{
            $canget = false;
        }
        return [
            'status' => true,
            "message" => 'Event Info',
            "data" => ['username' => $member->username,
            'can_get_package' => $canget]
        ];
    }

    public function getEventInfo($request,$userData)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);
        // get member info
        $member = Users::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $message = BnsWording::where('type','no_condition')->value('wording');
        $message = "";
        return $this->setEventInfo($uid,$message);
    }
        function userCanGetReward($uid){
            return Users::where('uid',$uid)->value('can_get_package');
        }
        function updateUserCanGetReward($uid){
            $finduser = Users::where('uid',$uid)->first();
            $updateuser = Users::find($finduser->id);
            $updateuser->can_get_package = 2;
            $updateuser->save();
            $returnuser = Users::where('uid',$updateuser->uid)->first();
            $canget = $returnuser->can_get_pcakage;
            return intval($canget);
        }

        public function setDiamondsBalance($uid)
        {

            $diamondBalanceRaw = $this->apiDiamondBalance($uid);
            $diamondBalance = json_decode($diamondBalanceRaw);

            $diamonds = 0;
            if ($diamondBalance->status == true) {
                $diamonds = intval($diamondBalance->balance);
            }

            return $diamonds;
        }
        private function apiDiamondBalance(int $uid)
        {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'balance',
                'uid' => $uid,
            ]);
        }

        public function getNcidByUid(int $uid): string
        {
            return Users::where('uid', $uid)->value('ncid');
        }
    }
