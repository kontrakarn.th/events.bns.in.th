<?php

    namespace App\Http\Controllers\Api\Test\third_spec_assassin;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;
    use App\Http\Controllers\Api\Test\third_spec_assassin\HistoryController;

    use App\Models\Test\third_spec_assassin\Product;
    use App\Models\Test\third_spec_assassin\Product_group;
    use App\Models\Test\third_spec_assassin\History;
    use App\Models\Test\third_spec_assassin\Users;
    use App\Models\Test\third_spec_assassin\DeductLog;
    use App\Jobs\third_spec_assassin\SendItemToUserQueue;

    class BuyPackageController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2018-12-14 00:00:00';
        private $endTime = '2018-12-20 23:59:59';

        private $startApiTime = '00:00:00';
        private $endApiTime = '23:59:59';

      /* public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            //$this->checkEventStatus();
        }*/
        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip  = array_shift($ips);
            }
            return $ip;
        }
        public function sendbackItem($request,$uid){
            $userscontroller = New UsersController;
            $historycontroller = New HistoryController;
            $CheckUserFromhistoruy = Users::where('can_get_package',2)->get();

            $productgroup = Product_group::where('id',5)->get();
            foreach($CheckUserFromhistoruy as $us){
                $uid = $us->uid;
            foreach($productgroup as $pro){
                $savehistoryid = History::where('product_id',5)->where('uid',$uid)->where('status','!=','success')->value('id');
                $goods_data = [];
                $goods_data[] = [
                 'goods_id' => $pro["product_id"],
                 'purchase_quantity' => 1,
                 'purchase_amount' => 0,
                 'category_id' => 40,
             ];
                $method = 2 ;//update send back
                $this->sendItem($us->ncid,$goods_data,$savehistoryid,$historycontroller,$method);
            }
        }
            $userscontroller->updateUserCanGetReward($uid);

            return response()->json([
                'status' => true,
                'data' =>['can_get_package' => false]

            ]);
        }

        public function BuyPackage($request,$uid){
            $userscontroller = New UsersController;
            $userCanget = $userscontroller->userCanGetReward($uid);
            if($userCanget == 2){
                return response()->json([
                    'status' => true,
                    'message' => "ซื้อแพ็คเกจไปแล้ว",
                    'data' =>['can_get_package' => false]
                ]);
            }

            $checkAgent = $request->header('user-agent');
            $browser ="";
            if(strstr($checkAgent,"Mobile"))
            {
                $browser = "Mobile";
            }else if(strstr($checkAgent,"BnsIngameBrowser") ){
                $browser = "HongMoon Pad";
            }else if(strstr($checkAgent,"Postman")){
                $browser = "Postman";
            }else if(strstr($checkAgent,"Macintosh") || strstr($checkAgent,"Windows")){
                $browser = "PC";
            }
            if($this->isIngame() == true){
                $browser = "HongMoon Pad";
            }
            $member = Users::where('uid',$uid)->first();
            $logDeduct = $this->createDeductLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'diamonds' => 150000,
                'status' => 'pending',
                'last_ip' => $this->getIP(),
                'log_date' => (string) date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
            ]);
            $userdaimonds = $userscontroller->setDiamondsBalance($uid);
            if($userdaimonds < 150000)
            {
                return response()->json([
                    'status' => false,
                    'message' => 'ไดมอนด์ไม่เพียงพอ',
                ]);
            }
            $historycontroller = New HistoryController;
            $userpaydiamonds = $this->UserpayDiamonds(150000,$uid,$userdaimonds,$logDeduct);
            $productgroup = Product_group::all();
            $method = 1;
            foreach($productgroup as $pro){
                $savehistoryid = $historycontroller->SaveHistory($pro->id,2,1,$uid,$checkAgent,$browser);
                $goods_data = [];
                $goods_data[] = [
                 'goods_id' => $pro["product_id"],
                 'purchase_quantity' => 1,
                 'purchase_amount' => 0,
                 'category_id' => 40,
             ];
             $this->sendItem($member->ncid,$goods_data,$savehistoryid,$historycontroller,$method);
                    }

            $userscontroller->updateUserCanGetReward($uid);

            return response()->json([
                'status' => true,
                'data' =>['can_get_package' => false]

            ]);
        }

        public function UserpayDiamonds($cal_daimonds,$uid,$userdaimonds,$logDeduct){
            $ncid = $this->getNcidByUid($uid);
            $deductData = $this->setDiamondDeductData($cal_daimonds);
            $resp_deduct_raw = $this->apiDeductDiamond($ncid, $deductData);
            $resp_deduct = json_decode($resp_deduct_raw);

            if (!is_object($resp_deduct) || is_null($resp_deduct)) {
                return response()->json([
                    'status' => false,
                    'message' => 'Error deduct diamond'
                ]);
            }
            $this->updateDeductLog([
                'before_deduct_diamond' => $userdaimonds,
                'after_deduct_diamond' => $userdaimonds - $cal_daimonds,
                'deduct_status' => $resp_deduct->status ?: 0,
                'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
                'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
                'deduct_data' => json_encode($deductData),
                'status' => 'success'
            ], $logDeduct['id']);
        }
        public function createDeductLog(array $arr)
        {
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }


        protected function getNcidByUid(int $uid): string
        {
            return Users::where('uid', $uid)->value('ncid');
        }

        private function setDiamondDeductData($deduct_diamond_amount = 0)
        {
            if ($deduct_diamond_amount == 0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function apiDiamondBalance(int $uid)
        {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'balance',
                'uid' => $uid,
            ]);
        }
        private function apiDeductDiamond(string $ncid, array $goodsData)
        {

            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamomds for garena events BNS Mystic Hongmoon.',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        public function updateDeductLog(array $arr, $log_id)
        {
            if ($arr) {
                return DeductLog::where('id', $log_id)->update($arr);
            }
            return null;
        }

        public function sendItem($ncid,$goods_data,$savehistoryid,$historycontroller,$method){
            $send_result_raw = $this->apiSendItem($ncid, $goods_data);
               $send_result = json_decode($send_result_raw);
               if($method == 1){
             if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                $savehistory = $historycontroller->UpdateHistory($savehistoryid,
                                               $send_result->status ?: false,
                                               $send_result->response->purchase_id ?: 0,
                                               $send_result->response->purchase_status ?: 0,
                                               'success'
                                               );
                return true;
            } else {
                $savehistory = $historycontroller->UpdateHistory($savehistoryid,
                                               $send_result->status ?: false,
                                                0,
                                                0,
                                               'unsuccess'
                                               );
                return false;
            }
        }else{
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                $savehistory = $historycontroller->UpdateHistoryBySendfail($savehistoryid,
                                               $send_result->status ?: false,
                                               $send_result->response->purchase_id ?: 0,
                                               $send_result->response->purchase_status ?: 0,
                                               'success'
                                               );
                return true;
            } else {
                $savehistory = $historycontroller->UpdateHistoryBySendfail($savehistoryid,
                                               $send_result->status ?: false,
                                               0,
                                               0,
                                               'unsuccess'
                                               );
                return false;
            }
        }
        }
        private function apiSendItem(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name'             => 'bns',
                'service'              => 'send_item',
                'user_id'              => $ncid,
                'purchase_description' => 'Sending item from garena events BNS third spec assassin.',
                'goods'                => json_encode($goodsData),
            ];
            return $this->post_api($this->baseApi, $data);
        }
    }
