<?php

    namespace App\Http\Controllers\Api\Test\topup_nov2018;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DB;

    use App\Models\Test\topup_nov2018\Member;
    use App\Models\Test\topup_nov2018\Reward;
    use App\Models\Test\topup_nov2018\ItemLog;

    class IndexController extends BnsEventController{

    	private $baseApi = 'http://api.apps.garena.in.th';
        private $startTime = '2018-11-20 00:00:00';
        private $endTime = '2019-01-19 23:59:50';

        private $startGetApiTime = '00:00:00';
        private $endGetApiTime = '23:59:59';

        private $awtSecret = 'PFbeH0I4qJ2SwDyp6EO62SaSX217m1iZ';

        private $diamondStartTime = '2018-11-20 00:00:00';
        private $diamondEndTime = '2018-12-12 23:59:59';

        private $bnsAppId = 32835;

        public function __construct()
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getUserData();

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
             if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            $this->userData = $this->getUserData();
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'total_diamonds' => 0,
                    // 'diamond_points' => 0,
                    // 'used_diamond_points' => 0,
                    'last_ip' => $this->getIP()
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['_id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('_id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:TOPUP_NOVEMBER2018:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events bns topup November 2018.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemLog::where('_id', $log_id)->where('uid', $uid)->update($arr);
        }

        private function checkItemHistory($uid,$package_id){
            $itemHistory = ItemLog::where('uid', $uid)
                                ->where('package_id', $package_id)
                                // ->where('status', 'success')
                                ->count();
            return $itemHistory;
        }

        private function doGetTermgameDiamondHistory($uid) {
            if(empty($uid)){
                return false;
            }
            // Cache::forget('BNS:TOPUP_NOVEMBER2018:TERMGAME_DIAMONDS_HISTORY_' . $uid);
            return Cache::remember('BNS:TOPUP_NOVEMBER2018:TERMGAME_DIAMONDS_HISTORY_' . $uid, 5, function() use($uid) {
                return json_decode($this->post_api($this->baseApi, [
                            'key_name' => 'gws_test',
                            'service' => 'get_user_topup',
                            'uid' => $uid,
                            'start_ts' => strtotime($this->diamondStartTime),
                            'end_ts' => strtotime($this->diamondEndTime),
                            'app_id' => $this->bnsAppId
                        ]));
            });
        }

        // private function doGetDiamondHistory($uid) {
        //     if(empty($uid)){
        //         return false;
        //     }
        //     // Cache::forget('BNS:TOPUP_NOVEMBER2018:EXCHANGE_TO_DIAMONDS_HISTORY_' . $uid);
        //     return Cache::remember('BNS:TOPUP_NOVEMBER2018:EXCHANGE_TO_DIAMONDS_HISTORY_' . $uid, 5, function() use($uid) {
        //         return json_decode($this->post_api($this->baseApi, [
        //                     'key_name' => 'bns',
        //                     'service' => 'query_cash_txn',
        //                     'uid' => $uid,
        //                     'start_time' => $this->diamondStartTime,
        //                     'end_time' => $this->diamondEndTime
        //                 ]));
        //     });
        // }

        private function doGetDiamondHistory($ncid) {
            if(empty($ncid)){
                return false;
            }
            // $ncid='7CB03022-2B70-40C0-B5C1-9AA84FC23312';
            // Cache::forget('BNS:TOPUP_NOVEMBER2018:EXCHANGE_TO_DIAMONDS_HISTORY_' . $ncid);
            return Cache::remember('BNS:TOPUP_NOVEMBER2018:EXCHANGE_TO_DIAMONDS_HISTORY_' . $ncid, 5, function() use($ncid) {
                return json_decode($this->post_api($this->baseApi, [
                            'key_name' => 'bns',
                            'service' => 'query_cash_txn',
                            'user_id' => $ncid,//ncid
                            'start_time' => $this->diamondStartTime,
                            'end_time' => $this->diamondEndTime
                        ]));
            });
        }

        // private function checkAirpayTopupCondition($uid){
        //     $result = 0;
        //     $diamondsTxn = $this->doGetDiamondHistory($uid);
        //     if($diamondsTxn->status == true){
        //         $content = $diamondsTxn->response;
        //         if (count($content) > 0) {
        //             $collection = collect($content);
        //             $result = $collection->where('channel', 'Airpay')->where('diamond', '>=', 50000)->count();
        //         }
        //     }
        //
        //     // check total claim AirPay Reward
        //     $totalClaimReward = ItemLog::where('package_id', 6)
        //                                 ->where('status', 'success')
        //                                 ->count();
        //
        //     return $result > 0 && $totalClaimReward < 2000;
        // }

        private function setEventInfo($uid){
            if(empty($uid)){
                return false;
            }
            $member = Member::where('uid', $uid)->first();


            $result_diamond = 0;

            // check diamonds from Termgame
            // $resultTermgame = $this->doGetTermgameDiamondHistory($member->uid);
            // if($resultTermgame->status == true){
            //     $contentTermgame = $resultTermgame->result->task;
            //     if (count($contentTermgame) > 0) {
            //         $collectionTermgame = collect($contentTermgame);
            //         $result_diamond += $collectionTermgame->sum('point_amount');
            //     }
            // }

            // check diamonds from AirPay
            // $resultAirPay = $this->doGetDiamondHistory($member->uid);
            // if ($resultAirPay->status == true) {
            //     $contentAirPay = $resultAirPay->response;
            //     if (count($contentAirPay) > 0) {
            //         $collectionAirPay = collect($contentAirPay);
            //         $result_diamond += $collectionAirPay->sum('diamond');
            //     }
            // }

            $resultDiamond = $this->doGetDiamondHistory($member->ncid);
            if ($resultDiamond->status == true) {
                $contentDiamond = $resultDiamond->response;
                if (count($contentDiamond) > 0) {
                    $collectionDiamond = collect($contentDiamond);
                    $result_diamond += $collectionDiamond->sum('diamond');
                }
            }

            if($result_diamond > $member->total_diamonds){
                $this->updateMember([
                    'total_diamonds' => $result_diamond
                ], $member->_id);
            }

            $totalDiamonds = ($result_diamond > 0 && $result_diamond > $member->total_diamonds) ? $result_diamond : $member->total_diamonds;

            $useddiamond=$this->getDiamondReceived($member->uid);
            $totalDiamonds=$totalDiamonds-$useddiamond;

            $reward = new Reward;

            $packages = [];

            // check can receive reward
            for($i=1;$i<=7;$i++){

                $can_receive = false;
                $received = false;

                $packageInfo = [];
                $packageInfo = $reward->setRewardByPackage($i);

                // check item history
                $itemHistory = 0;
                $itemHistory = $this->checkItemHistory($member->uid,$packageInfo['package_id']);

                if($totalDiamonds >= $packageInfo['require_diamonds'] && $itemHistory == 0){
                    $can_receive = true;
                }

                if($itemHistory > 0){
                    $received = true;
                }
                // $can_receive=true;
                // $received=false;
                $packages[] = [
                    'package_id' => $packageInfo['package_id'],
                    'package_title' => $packageInfo['package_title'],
                    'package_desc' => $packageInfo['package_desc'],
                    'can_receive' => $can_receive,
                    'received' => $received
                ];

            }

            // set AirPay Top Up Promotion
            // $airpay = [];

            // $airpayPackage = $reward->airpayPackage();

            // $airpayCanReceive = $this->checkAirpayTopupCondition($uid);

            // check item history
            // $airpayItemHistory = $this->checkItemHistory($uid,$airpayPackage['package_id']);

            // $airpay = [
            //     'package_id' => $airpayPackage['package_id'],
            //     'package_title' => $airpayPackage['package_title'],
            //     'package_desc' => $airpayPackage['package_desc'],
            //     'can_receive' => $airpayCanReceive,
            //     'received' => $airpayItemHistory > 0
            // ];

            $arr_number=str_split($totalDiamonds,1);

            return [
                'mypoint' => $totalDiamonds,
                'mypoint_arr'=>$arr_number,
                'packages' => $packages,
                // 'airpay' => $airpay
            ];
        }

        public function getEventInfo(Request $request){
            // $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            // if (is_null($decoded)) {
            //     return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            // }
            // $decoded = collect($decoded);
            $decoded = $request;
            // dd($decoded->has('type'));
            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            // $uid=200062;
            // $ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
            // get member info
            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
                    ]);

        }

        public function getDiamondReceived($uid){
            $sumdiamond=ItemLog::where('uid',$uid)->sum('require_diamonds');
            return $sumdiamond;
        }

        public function redeemReward(Request $request){
            // $decoded = $this->decryptJwtToken($request, $this->awtSecret);
            // if (is_null($decoded)) {
            //     return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด (1) กรุณาลองใหม่อีกครั้ง']);
            // }
            // $decoded = collect($decoded);
            $decoded = $request;

            if ($decoded->has('type') == false || $decoded['type'] != 'redeem') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (1)'
                ]);
            }

            $userData = $this->getUserData();
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            // $uid=200062;
            // $ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
            if ($decoded->has('package_id') == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (2)'
                ]);
            }elseif($decoded->package_id<1 || $decoded->package_id>7){
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (3)'
                ]);
            }

            $package_id = (int)$decoded['package_id'];
            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            // $member->uid=200062;
            // $member->ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
            // check item history
            $itemHistory = 0;
            $itemHistory = $this->checkItemHistory($member->uid,$package_id);
            if($itemHistory>0){
                return response()->json([
                        'status' => false,
                        'message' => 'ขออภัย<br />คุณได้รับไอเทมไปแล้ว'
                    ]);
            }

            $reward = new Reward;
            // if($package_id == 6){
            //     $packageInfo = $reward->airpayPackage();
            // }else{
                $packageInfo = $reward->setRewardByPackage($package_id);
            // }

            if($packageInfo == false){
                return response()->json([
                            'status' => false,
                            'message' => 'No data required.'
                        ]);
            }
            $useddiamond=$this->getDiamondReceived($member->uid);
            $totalDiamonds=$member->total_diamonds-$useddiamond;
            // if($package_id == 8){
            //     if($this->checkAirpayTopupCondition($uid) == false){
            //         return response()->json([
            //             'status' => false,
            //             'message' => 'ขออภัย<br />เงื่อนไขการรับของรางวัลจาก AirPay ไม่ถูกต้อง'
            //         ]);
            //     }
            // }else{
                if($totalDiamonds < $packageInfo['require_diamonds']){
                    return response()->json([
                        'status' => false,
                        'message' => 'ขออภัย<br />จำนวนเงินของคุณไม่พอแลกของรางวัล'
                    ]);
                }
            // }

            // set reward packages
            $goods_data = []; //good data for send item group
            $packageId = $packageInfo['package_id'];
            $requireDiamonds = $packageInfo['require_diamonds'];
            $packageTitle = $packageInfo['package_title'];

            $productList = $packageInfo['product_set'];

            foreach ($productList as $key => $product) {
                $goods_data[] = [
                    'goods_id' => $product['product_id'],
                    'purchase_quantity' => $product['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];
            }

            $sendItemLog = $this->addItemHistoryLog([
                'uid' => $uid,
                'ncid' => $ncid,
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
                'package_id' => $packageId,
                'package_title' => $packageTitle,
                'require_diamonds' => $requireDiamonds,
                'product_set' => $productList,
                'status' => 'pending',
                'send_item_status' => false,
                'send_item_purchase_id' => 0,
                'send_item_purchase_status' => 0,
                'goods_data' => json_encode($goods_data),
                'last_ip' => $this->getIP()
            ]);

            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ?: false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                    'status' => 'success'
                        ], $sendItemLog['_id'], $uid);

                // $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                            'status' => true,
                            'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                            // 'content' => $eventInfo
                        ]);
            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                        ], $sendItemLog['_id'], $uid);

                // $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                            // 'content' => $eventInfo
                        ]);
            }

        }

    }
