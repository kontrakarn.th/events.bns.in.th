<?php

namespace App\Http\Controllers\Api\Test\two_in_one;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

use App\Models\Test\two_in_one\Member;
use App\Models\Test\two_in_one\Setting;
use App\Models\Test\two_in_one\DeductLog;
use App\Models\Test\two_in_one\ItemLog;
use App\Models\Test\bns_job\BnsJob;
use App\Models\Test\two_in_one\Quest;
use App\Models\Test\two_in_one\QuestLog;
use App\Models\Test\two_in_one\Reward;
use Illuminate\Support\Facades\DB;

class IndexController extends BnsEventController
{
    private $baseApi = 'http://api.apps.garena.in.th';

    private $userEvent = null;

    public function __construct(Request $request)
    {
        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);

        $this->checkEventStatus();
    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status' => false,
                'type' => 'no_permission',
                'message' => 'Sorry, you do not have permission.'
            ]));
        }

        // get events setting info
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status' => false,
                'type' => 'maintenance',
                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
            ]));
        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                ]));
            }
        } else {
            die(json_encode([
                'status' => false,
                'type' => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    private function is_accepted_ip()
    {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function isLoggedIn()
    {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'              => $uid,
                'username'         => $username,
                'ncid'             => $ncid,
                'char_id'          => 0,
                'char_name'        => '',
                'world_id'         => 0,
                'total_points'     => 0,
                'used_points'      => 0,
                'total_tokens'     => 0,
                'used_tokens'      => 0,
                'already_purchased' => 0,
                'last_ip'          => $this->getIP(),
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
            $this->userEvent = $resp;
        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id)) {
            return false;
        }

        return Member::where('id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function getEventSetting()
    {
        return Setting::where('active', 1)->first();
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:TWO_IN_ONE:LIVE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'user_info',
                'uid'      => $uid,
            ]);
            $userNcInfo       = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }
        });
    }

    private function doGetChar($uid, $ncid)
    { //new fixed function
        $resp_raw = $this->apiCharacters($uid, $ncid);

        $resp = json_decode($resp_raw);
        if (is_object($resp)) {
            if ($resp->status) {
                $content = [];
                $i       = 1;
                foreach ($resp->response as $key => $value) {
                    $content[] = [
                        'id' => $i,
                        'char_id'   => $value->id,
                        'char_name' => $value->name,
                        'world_id' => $value->world_id,
                        'level' => $value->level,
                        'job' => $value->job,
                        'job_name' => $this->getJobName($value->job),
                        'level' => $value->level,
                        'creation_time' => $value->creation_time,
                        'last_play_start' => $value->last_play_start,
                        'last_play_end' => $value->last_play_end,
                        'last_ip' => $value->last_ip,
                        'exp' => $value->exp,
                        'mastery_level' => $value->mastery_level,
                    ];

                    $i++;
                }
                return $content;
            }
        }
        return null;
    }

    private function apiCharacters($uid, $ncid)
    {
        return Cache::remember('BNS:TWO_IN_ONE:LIVE:CHARACTERS_' . $uid, 10, function () use ($ncid) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'characters',
                'user_id'  => $ncid,
            ]);
        });
    }

    protected function getJobName($jobId)
    {
        return BnsJob::where('job_id', $jobId)->value('job_th_name');
    }

    private function createDeductLog(array $arr)
    {
        if ($arr) {
            $resp = DeductLog::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return false; //fix
            }
        }
        return null;
    }

    private function updateDeductLog(array $arr, $log_id)
    {
        if ($arr) {
            return DeductLog::where('id', $log_id)->update($arr);
        }
        return null;
    }

    private function apiDiamondBalance(int $uid)
    {
        return $this->post_api($this->baseApi, [
            'key_name' => 'bns',
            'service' => 'balance',
            'uid' => $uid,
        ]);
    }

    private function setDiamondsBalance($uid)
    {

        $diamondBalanceRaw = $this->apiDiamondBalance($uid);
        $diamondBalance = json_decode($diamondBalanceRaw);

        $diamonds = 0;
        if ($diamondBalance->status == true) {
            $diamonds = intval($diamondBalance->balance);
        }

        return $diamonds;
    }

    private function apiDeductDiamond(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Deduct diamomds for garena events BNS Two in One.',
            'currency_group_id' => 71,
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function setDiamondDeductData($deduct_diamond_amount = 0)
    {
        if ($deduct_diamond_amount == 0)
            return false;

        return [
            [
                'goods_id' => 741,
                'purchase_quantity' => $deduct_diamond_amount,
                'purchase_amount' => $deduct_diamond_amount,
                'category_id' => 40
            ]
        ];
    }

    private function apiSendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Sending item from garena events BNS Two in One.',
            'goods'                => json_encode($goodsData),
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function addItemHistoryLog($arr)
    {
        return ItemLog::create($arr);
    }

    private function updateItemHistoryLog($arr, $log_id, $uid)
    {
        return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
    }

    public function selectCharacter(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'select_character') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }
        if ($decoded->has('id') == false) { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (2)'
            ]);
        }

        $charKey = (int) $decoded['id'];

        $userData = $this->userData;
        $uid = $userData['uid'];

        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบข้อมูล'
            ]);
        }

        if ($member->char_id > 0) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถเปลี่ยนตัวละครได้'
            ]);
        }

        $mychar = $this->doGetChar($member->uid, $member->ncid);
        $myselect = collect($mychar)->where('id', $charKey)->first();
        if (count($myselect) == 0) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบตัวละคร'
            ]);
        }

        $member->char_id   = (int) $myselect['char_id'];
        $member->char_name = $myselect['char_name'];
        $member->world_id       = $myselect['world_id'];
        $member->level = $myselect['level'];
        $member->mastery_level = $myselect['mastery_level'];
        $member->job = $myselect['job'];
        $member->job_name = $myselect['job_name'];
        $member->save();

        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] == false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message'],
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }

    public function getEventInfo(Request $request)
    {

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] == false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message'],
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }

    private function setEventInfo($uid)
    {
        $member = Member::where('uid', $uid)->first();

        if (!$member) {
            return [
                'status' => false,
                'message' => 'No member data.'
            ];
        }

        if ($member->char_id == 0) {

            $charData = $this->doGetChar($member->uid, $member->ncid);

            $content = [];

            $content = collect($charData)
                ->filter(function ($value) {
                    return $value['level'] >= 60 && $value['mastery_level'] >= 15;
                })
                ->map(function ($value) {
                    return [
                        'id' => $value['id'],
                        'char_name' => $value['char_name'],
                        'level' => $value['level'],
                        'job_name' => $value['job_name'],
                        'mastery_level' => $value['mastery_level'],
                    ];
                })
                ->values();

            return [
                'status' => true,
                'message' => 'Event Info',
                'username' => $member->username,
                'character_name' => '',
                'selected_char' => false,
                'characters' => $content,
                'pvp_tokens' => 0,
                'pve_tokens' => 0,
                'daily_quests' => $this->getDailyQuests($member),
                'daily_challenge_quests' => $this->getDailyChallengeQuests($member),
                'limit_pve_rewards' => $this->getLimitPveRewards($member),
                'pve_rewards' => $this->getPveRewards($member),
                'limit_pvp_rewards' => $this->getLimitPvpRewards($member),
                'pvp_rewards' => $this->getPvpRewards($member),
                'show_compensate_history' => false,
            ];
        }

        $this->createDailyQuestLogs($member);

        return [
            'status' => true,
            'message' => 'Event Info',
            'username' => $member->username,
            'character_name' => $member->char_name,
            'selected_char' => true,
            'characters' => [],
            'pvp_tokens' => $member->remaining_pvp_tokens,
            'pve_tokens' => $member->remaining_pve_tokens,
            'daily_quests' => $this->getDailyQuests($member),
            'daily_challenge_quests' => $this->getDailyChallengeQuests($member),
            'limit_pve_rewards' => $this->getLimitPveRewards($member),
            'pve_rewards' => $this->getPveRewards($member),
            'limit_pvp_rewards' => $this->getLimitPvpRewards($member),
            'pvp_rewards' => $this->getPvpRewards($member),
            'show_compensate_history' => $this->getShowCompensateHistory($member),
        ];
    }

    private function getShowCompensateHistory($member)
    {
        return QuestLog::where('uid', $member->uid)
            ->where('is_compensated', 1)
            ->count() > 0;
    }

    private function getLimitPveRewards($member)
    {
        return Reward::where('package_type', 'limit_pve')
            ->select(
                'id',
                'package_name',
                'required_tokens',
                'limit',
                'img',
                'hover'
            )
            ->get()
            ->map(function ($reward) use ($member) {
                $limitCount = ItemLog::where('uid', $member->uid)
                    ->where('reward_id', $reward->id)
                    ->where('package_type', 'limit_pve')
                    ->count();

                return [
                    'id' => $reward->id,
                    'package_name' => $reward->package_name,
                    'required_tokens' => $reward->required_tokens,
                    'limit_count' => $limitCount,
                    'limit' => $reward->limit,
                    'img' => $reward->img,
                    'hover' => $reward->hover,
                    'can_claim' => $member->remaining_pve_tokens >= $reward->required_tokens && $limitCount < $reward->limit
                ];
            });
    }

    private function getLimitPvpRewards($member)
    {
        return Reward::where('package_type', 'limit_pvp')
            ->select(
                'id',
                'package_name',
                'required_tokens',
                'limit',
                'img',
                'hover'
            )
            ->get()
            ->map(function ($reward) use ($member) {
                $limitCount = ItemLog::where('uid', $member->uid)
                    ->where('reward_id', $reward->id)
                    ->where('package_type', 'limit_pvp')
                    ->count();

                return [
                    'id' => $reward->id,
                    'package_name' => $reward->package_name,
                    'required_tokens' => $reward->required_tokens,
                    'limit_count' => $limitCount,
                    'limit' => $reward->limit,
                    'img' => $reward->img,
                    'hover' => $reward->hover,
                    'can_claim' => $member->remaining_pvp_tokens >= $reward->required_tokens && $limitCount < $reward->limit
                ];
            });
    }

    private function getPveRewards($member)
    {
        return Reward::where('package_type', 'pve')
            ->select(
                'id',
                'package_name',
                'required_tokens',
                'limit',
                'img',
                'hover'
            )
            ->get()
            ->map(function ($reward) use ($member) {
                return [
                    'id' => $reward->id,
                    'package_name' => $reward->package_name,
                    'required_tokens' => $reward->required_tokens,
                    'img' => $reward->img,
                    'hover' => $reward->hover,
                    'can_claim' => $member->remaining_pve_tokens >= $reward->required_tokens
                ];
            });
    }

    private function getPvpRewards($member)
    {
        return Reward::where('package_type', 'pvp')
            ->select(
                'id',
                'package_name',
                'required_tokens',
                'limit',
                'img',
                'hover'
            )
            ->get()
            ->map(function ($reward) use ($member) {
                return [
                    'id' => $reward->id,
                    'package_name' => $reward->package_name,
                    'required_tokens' => $reward->required_tokens,
                    'img' => $reward->img,
                    'hover' => $reward->hover,
                    'can_claim' => $member->remaining_pvp_tokens >= $reward->required_tokens
                ];
            });
    }

    private function createDailyQuestLogs($member)
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $questDateTime = $this->setQuestDateTime();
        $logDate = $questDateTime['log_date'];

        $dailyQuests = Quest::whereIn('quest_type', ['area', 'daily_challenge'])->get();

        foreach ($dailyQuests as $quest) {
            QuestLog::firstOrCreate(
                [
                    'uid' => $member->uid,
                    'quest_id' => $quest->id,
                    'log_date' => $logDate
                ],
                [
                    'char_id' => $member->char_id,
                    'log_date_timestamp' => time(),
                    'last_ip' => $this->getIP(),
                ]
            );
        }
    }

    private function setQuestDateTime()
    {
        $currDate = date('Y-m-d');

        $startDatetime = '';
        $endDatetime = '';
        $logDate = '';

        if (time() >= strtotime($currDate . ' 00:00:00') && time() <= strtotime($currDate . ' 05:59:59')) { // check current time after 00:00:00 and not over 05:59:59
            $startDatetime = date('Y-m-d', strtotime(' -1 day')) . ' 06:00:00';
            $endDatetime = date('Y-m-d') . ' 05:59:59';
            $logDate = date('Y-m-d', strtotime(' -1 day'));
        } else {
            $startDatetime = date('Y-m-d') . ' 06:00:00';
            $endDatetime = date('Y-m-d', strtotime(' +1 day')) . ' 05:59:59';
            $logDate = date('Y-m-d');
        }

        return [
            'log_date' => $logDate,
            'start_date_time' => $startDatetime,
            'end_date_time' => $endDatetime,
        ];
    }

    private function getDailyQuests($member)
    {
        $questDateTime = $this->setQuestDateTime();
        $logDate = $questDateTime['log_date'];

        $dailyQuestLogs = QuestLog::select(
            'two_in_one_quest_logs.id',
            'two_in_one_quests.quest_title',
            $this->getQuestStatusRawQuery(),
            'two_in_one_quests.img'
        )
            ->join('two_in_one_quests', 'two_in_one_quest_logs.quest_id', '=', 'two_in_one_quests.id')
            ->where('two_in_one_quests.quest_type', 'area')
            ->where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->where('log_date', $logDate)
            ->get();

        if (count($dailyQuestLogs) == 0) {
            $dailyQuestLogs = Quest::select(
                'two_in_one_quests.quest_title',
                DB::raw("'not_in_condition' AS status"),
                'two_in_one_quests.img'
            )
                ->where('quest_type', 'area')
                ->get();
        }

        return $dailyQuestLogs;
    }

    private function getDailyChallengeQuests($member)
    {
        $questDateTime = $this->setQuestDateTime();
        $logDate = $questDateTime['log_date'];

        $dailyQuestLogs = QuestLog::select(
            'two_in_one_quest_logs.id',
            'two_in_one_quests.quest_title',
            $this->getQuestStatusRawQuery(),
            'two_in_one_quests.img'
        )
            ->join('two_in_one_quests', 'two_in_one_quest_logs.quest_id', '=', 'two_in_one_quests.id')
            ->where('two_in_one_quests.quest_type', 'daily_challenge')
            ->where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->where('log_date', $logDate)
            ->get();

        if (count($dailyQuestLogs) == 0) {
            $dailyQuestLogs = Quest::select(
                'two_in_one_quests.quest_title',
                DB::raw("'not_in_condition' AS status"),
                'two_in_one_quests.img'
            )
                ->where('quest_type', 'daily_challenge')
                ->get();
        }

        return $dailyQuestLogs;
    }

    private function getQuestStatusRawQuery()
    {
        return DB::raw("
            CASE 
                WHEN two_in_one_quest_logs.claim_status = 'claimed'
                    THEN 'claimed'
                WHEN two_in_one_quest_logs.status = 'success'
                    THEN 'can_claim'
                ELSE 'not_in_condition'
            END AS status");
    }

    public function claimQuestPoints(Request $request)
    {
        $id = $request->input('id');
        if (!$id) {
            return response()->json([
                'status' => false,
                'message' => 'Invalid argument id'
            ], 400);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $questDateTime = $this->setQuestDateTime();
        $logDate = $questDateTime['log_date'];

        $dailyLog = QuestLog::with('quest')
            ->where('id', $id)
            ->where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->where('completed_count', '>', 0)
            ->where('status', 'success')
            ->where('log_date', $logDate)
            ->first();

        if (!$dailyLog) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย ไม่ตรงเงื่อนไข'
            ]);
        }

        if ($dailyLog->claimed_count > 0 || $dailyLog->claim_status === 'claimed') {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย รับคะแนนวันนี้ไปแล้ว'
            ]);
        }

        $questPoints = $dailyLog->quest->quest_points;

        if ($dailyLog->quest->quest_type == 'area') {
            $message = 'ได้รับ เหรียญยอดนักสู้ x' . $questPoints;
            $member->total_pvp_tokens = $member->total_pvp_tokens + $questPoints;
        } else if ($dailyLog->quest->quest_type == 'daily_challenge') {
            $message = 'ได้รับ เหรียญเจ้าสำนัก x' . $questPoints;
            $member->total_pve_tokens = $member->total_pve_tokens + $questPoints;
        } else {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด กรุณาติดต่อฝ่ายบริการลูกค้า'
            ]);
        }

        $dailyLog->claimed_count = 1;
        $dailyLog->claim_status = 'claimed';
        $dailyLog->save();

        if ($member->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'data' => $this->setEventInfo($uid)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Fail updating member point'
            ]);
        }
    }

    public function exchangeReward(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'exchange_reward') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $id = $request->id;
        if (!$id) {
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (2)'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $reward = Reward::where('id', $id)->first();

        if (!$reward) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด'
            ]);
        }

        if ($reward->package_type == 'pve' || $reward->package_type == 'limit_pve') {
            if ($member->remaining_pve_tokens < $reward->required_tokens) {
                return response()->json([
                    'status' => false,
                    'message' => 'PVE Token ไม่เพียงพอ'
                ]);
            }
        }

        if ($reward->package_type == 'pvp' || $reward->package_type == 'limit_pvp') {
            if ($member->remaining_pvp_tokens < $reward->required_tokens) {
                return response()->json([
                    'status' => false,
                    'message' => 'PVP Token ไม่เพียงพอ'
                ]);
            }
        }

        $limitCount = null;

        if ($reward->package_type == 'limit_pve' || $reward->package_type == 'limit_pvp') {
            $limitCount = ItemLog::where('uid', $member->uid)
                ->where('reward_id', $reward->id)
                ->count();

            if ($limitCount >= $reward->limit) {
                return response()->json([
                    'status' => false,
                    'message' => 'เกิดจำนวนที่รับได้'
                ]);
            }
        }

        $goods_data = [];
        $goods_data[] = [
            'goods_id' => $reward->package_id,
            'purchase_quantity' => $reward->package_quantity,
            'purchase_amount' => 0,
            'category_id' => 40,
        ];

        $sendItemLog = $this->addItemHistoryLog([
            'uid'                           => $member->uid,
            'username'                      => $member->username,
            'ncid'                          => $member->ncid,
            'reward_id'                     => $reward->id,
            'limit_count'                   => $limitCount + 1,
            'package_name'                  => $reward->package_name,
            'package_quantity'              => $reward->package_quantity,
            'package_amount'                => $reward->package_amount,
            'package_type'                  => $reward->package_type,
            'send_item_status'              => false,
            'send_item_purchase_id'         => 0,
            'send_item_purchase_status'     => 0,
            'goods_data'                    => json_encode($goods_data),
            'status'                        => 'pending',
            'log_date'                      => (string) date('Y-m-d'), // set to string
            'log_date_timestamp'            => time(),
            'last_ip'                       => $this->getIP(),
        ]);

        $ncid = $this->getNcidByUid($member->uid);
        $send_result_raw = $this->apiSendItem($ncid, $goods_data);
        $send_result = json_decode($send_result_raw);

        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            $this->updateItemHistoryLog([
                'send_item_status' => $send_result->status ?: false,
                'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                'status' => 'success'
            ], $sendItemLog['id'], $member->uid);

            if ($reward->package_type == 'pve' || $reward->package_type == 'limit_pve') {
                $member->used_pve_tokens = $member->used_pve_tokens + $reward->required_tokens;
            }

            if ($reward->package_type == 'pvp' || $reward->package_type == 'limit_pvp') {
                $member->used_pvp_tokens = $member->used_pvp_tokens + $reward->required_tokens;
            }

            $member->save();

            return response()->json([
                'status' => true,
                'message' => 'ได้รับ <span class="yellow">' . $reward->package_name . " x" . $reward->package_amount . "</span> <br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                'data' => $this->setEventInfo($uid)
            ]);
        } else {
            $this->updateItemHistoryLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
            ], $sendItemLog['id'], $member->uid);

            return response()->json([
                'status' => false,
                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า"
            ]);
        }
    }

    public function getHistory(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'history') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $pointHistories = $this->getPointsHistories($member);
        $exchangeRewardHistories = $this->getExchangeRewardHistories($member);

        return response()->json([
            'status' => true,
            'data' => [
                'points_histories' => $pointHistories,
                'exchange_reward_histories' => $exchangeRewardHistories,
            ]
        ]);
    }

    private function getPointsHistories($member)
    {
        return QuestLog::select(
            'two_in_one_quests.quest_title AS title',
            DB::raw("DATE_FORMAT(two_in_one_quest_logs.quest_complete_datetime, '%d/%m/%Y') AS date"),
            DB::raw("DATE_FORMAT(two_in_one_quest_logs.quest_complete_datetime, '%H:%i:%s') AS time")
        )
            ->join('two_in_one_quests', 'two_in_one_quest_logs.quest_id', 'two_in_one_quests.id')
            ->where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->where('claim_status', 'claimed')
            ->orderBy('quest_complete_datetime', 'desc')
            ->get();
    }

    public function getExchangeRewardHistories($member)
    {
        return ItemLog::select(
            DB::raw("
            CASE 
                WHEN two_in_one_item_logs.package_type = 'pve'
                    THEN CONCAT('ได้รับ', two_in_one_item_logs.package_name, ' ใช้เหรียญเจ้าสำนัก ', two_in_one_rewards.required_tokens)
                WHEN two_in_one_item_logs.package_type = 'pvp'
                    THEN CONCAT('ได้รับ', two_in_one_item_logs.package_name, ' ใช้เหรียญยอดนักสู้ ', two_in_one_rewards.required_tokens)
                WHEN two_in_one_item_logs.package_type = 'limit_pve'
                    THEN CONCAT('ได้รับ', two_in_one_item_logs.package_name, ' (', two_in_one_item_logs.limit_count, '/', two_in_one_rewards.limit, ')',' ใช้เหรียญเจ้าสำนัก ', two_in_one_rewards.required_tokens)
                WHEN two_in_one_item_logs.package_type = 'limit_pvp'
                    THEN CONCAT('ได้รับ', two_in_one_item_logs.package_name, ' (', two_in_one_item_logs.limit_count, '/', two_in_one_rewards.limit, ')',' ใช้เหรียญยอดนักสู้ ', two_in_one_rewards.required_tokens)
            END AS title"),
            DB::raw("DATE_FORMAT(two_in_one_item_logs.created_at, '%d/%m/%Y') AS date"),
            DB::raw("DATE_FORMAT(two_in_one_item_logs.created_at, '%H:%i:%s') AS time")
        )
            ->join('two_in_one_rewards', 'two_in_one_item_logs.reward_id', 'two_in_one_rewards.id')
            ->where('uid', $member->uid)
            ->where('status', 'success')
            ->latest()
            ->get();
    }

    public function getCompensateHistory(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'compensate_history') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $pointsHistories = QuestLog::select(
            'two_in_one_quests.quest_title AS title',
            DB::raw("DATE_FORMAT(two_in_one_quest_logs.quest_complete_datetime, '%d/%m/%Y') AS date"),
            DB::raw("DATE_FORMAT(two_in_one_quest_logs.quest_complete_datetime, '%H:%i:%s') AS time")
        )
            ->join('two_in_one_quests', 'two_in_one_quest_logs.quest_id', 'two_in_one_quests.id')
            ->where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->where('is_compensated', 1)
            ->orderBy('quest_complete_datetime', 'desc')
            ->get();

        return response()->json([
            'status' => true,
            'data' => [
                'points_histories' => $pointsHistories,
            ]
        ]);
    }
}
