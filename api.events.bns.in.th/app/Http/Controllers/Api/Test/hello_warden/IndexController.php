<?php

    namespace App\Http\Controllers\Api\Test\hello_warden;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\Test\hello_warden\Member;
    use App\Models\Test\hello_warden\Reward;
    use App\Models\Test\hello_warden\ItemLog;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2019-01-01 00:00:00';
        private $endTime = '2019-02-16 23:59:59';

        private $startApiTime = '00:00:00';
        private $endApiTime = '23:59:59';

        private $awtSecret = 'kYrZ3xd9Na4H6qGaN2gy5Lr8TfjD5EST';

        private $character_createDate = '2019-01-10 12:00:00';
        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }
            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:hello_warden:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        // public function acceptChar(Request $request)
        // {

        //     if($request->has('type') == false && $request->type != 'accept_character') {//check parameter
        //         return response()->json([
        //                     'status' => false,
        //                     'message' => 'Invalid parameter(1)!'
        //         ]);
        //     }

        //     if ($request->has('id') == false) {
        //         return response()->json([
        //                     'status' => false,
        //                     'message' => 'invalid parameter(2)!'
        //         ]);
        //     }
        //     $char_id = $request->id;
        //     if (empty($char_id)) {
        //         return response()->json([
        //                     'status' => false,
        //                     'message' => 'ไม่มีตัวละครที่ต้องการเลือก'
        //         ]);
        //     }

        //     // $char_id = $request->id;

        //     $uid = $this->userData['uid'];
        //     $username = $this->userData['username'];

        //     if ($resp = $this->hasAcceptChar($uid) == false) {
        //         $ncid = $this->getNcidByUid($uid);
        //         $characters = $this->doGetChar($uid,$ncid);
        //         // $collection = collect($characters);
        //         // $data = $collection->where('id', $char_id)->first();
        //         $data = $characters[$char_id-1];
        //         if (isset($data['char_id'])) {
        //             $this->doAcceptChar($data['char_id'], $data['char_name'], $data['world_id'], $uid);
        //             $eventInfo = $this->setEventInfo($uid);
        //             return response()->json([
        //                         'status' => true,
        //                         'message' => 'ยืนยันตัวละครเสร็จสิ้น',
        //                         'content' => $eventInfo
        //             ]);
        //         } else {
        //             return response()->json([
        //                         'status' => false,
        //                         'message' => 'ไม่พบตัวละครที่คุณต้องการเลือก'
        //             ]);
        //         }
        //     } else {
        //         return response()->json([
        //                     'status' => false,
        //                     'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก'
        //         ]);
        //     }
        // }

        private function doAcceptChar($char_id, $char_name, $world_id, $uid)
        {
            return Member::where('uid', $uid)->update([
                        'char_id' => $char_id,
                        'char_name' => $char_name,
                        'world_id' => $world_id,
                    ]);
        }

        private function hasAcceptChar($uid)
        {
            $resp = Member::where('uid', $uid)->first();
            if (empty($resp->char_id) && empty($resp->char_name)) {
                return false;
            } else {
                return $resp;
            }
        }

        // public function getCharacter(Request $request)
        // {
        //     if($request->has('type') == false && $request->type != 'get_characters') {//check parameter
        //         return response()->json([
        //                     'status' => false,
        //                     'message' => 'Invalid parameter!'
        //         ]);
        //     }

        //     $this->checkMember();
        //     $uid = $this->userData['uid'];
        //     $username = $this->userData['username'];
        //     $resp = $this->hasAcceptChar($uid);
        //     if ($resp == false) {
        //         $charactersList = [];
        //         $ncid = $this->getNcidByUid($uid);
        //         $characters = $this->doGetChar($uid,$ncid);
        //         dd($characters);
        //         foreach ($characters as $character) {
        //             $charactersList[] = collect($character)->except(['char_id', 'world_id', 'job', 'level', 'creation_time', 'last_play_start', 'last_play_end', 'last_ip', 'exp', 'mastery_level']);
        //         }
        //         return response()->json([
        //                     'status' => true,
        //                     'content' => $charactersList,
        //                     'accepted' => false
        //         ]);
        //     } else {
        //         return response()->json([
        //                     'status' => true,
        //                     'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก',
        //                     'accepted' => true
        //         ]);
        //     }
        // }

        private function doGetChar($uid,$ncid){//new fixed function
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i =1;
                    foreach ($resp->response as $key => $value) {

                          // set time
                        $date = $this->character_createDate;
                        $defualtDate = strtotime($date);
                        $createDate = strtotime($value->creation_time);
                        if($createDate >= $defualtDate){
                            $content[] = [
                                'id' => $i,
                                'char_id' => $value->id,
                                'char_name' => $value->name,
                                'world_id' => $value->world_id,
                                'job' => $value->job,
                                'level' => $value->level,
                                'creation_time' => $value->creation_time,
                                'last_play_start' => $value->last_play_start,
                                'last_play_end' => $value->last_play_end,
                                'last_ip' => $value->last_ip,
                                'exp' => $value->exp,
                                'mastery_level' => $value->mastery_level,
                            ];
                        }
                        $i++;
                    }
                    return $content;
                }
            }
            return null;
        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:hello_warden:CHARACTERS_' . $uid, 10, function() use($ncid) {
                        return $this->post_api($this->baseApi, [
                                    'key_name' => 'bns',
                                    'service' => 'characters',
                                    'user_id' => $ncid
                        ]);
                    });
        }

        private function dosendItem(string $ncid, array $goodsData) {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events BNS Moonstone Festival',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addSendItemLog(array $arr) {
            if ($arr) {
                $resp = ItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateSendItemLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function apiKillMoster($uid = null,$ncid = null,$char_id = null,$start_date = null,$end_date = null)
        {
            if (empty($uid) || empty($ncid) || empty($char_id) || empty($start_date) || empty($end_date))
                return false;

            return Cache::remember('BNS:hello_warden:MONSTERS_KILL_' . $uid, 1, function() use($ncid,$char_id,$start_date,$end_date) {
                        $data = $this->post_api($this->baseApi, [
                            'key_name' => 'bns',
                            'service' => 'events',
                            'user_id' => $ncid,
                            'subservice' => 'get_mon_kills',
                            'start_time' => $start_date,
                            'end_time' => $end_date,
                            'char_id' => $char_id
                        ]);
                        return json_decode($data);
                    });
        }

        private function setMosterKillCount($uid,$ncid,$char_id,$startDate,$endDate)
        {
            $monsterKillCount = 0;
            $monsterKill = $this->apiKillMoster($uid,$ncid,$char_id,$startDate,$endDate);
            if($monsterKill->status == true){
                $monsterKillCount = intval($monsterKill->response->kill_count);
            }

            return $monsterKillCount;
        }

        public function getEventInfo(Request $request)
        {
            if ($request->has('type') == false && $request->type != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }
            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            // check has selected character and roll
            // if(isset($member) && empty($member->char_id)){
            //     return response()->json([
            //             'status'    => false,
            //             'type'      => 'no_char',
            //             'message'   => 'No character info'
            //         ]);
            // }

            $eventInfo = $this->setEventInfo($uid);

            if($eventInfo == false){
                $eventInfo = [];
            }

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
            ]);

        }

        private function setEventInfo($uid=null){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();
            // set daily time
            // $startDate = date('Y-m-d').' '.$this->startApiTime;
            // $endDate = date('Y-m-d').' '.$this->endApiTime;

            // $startDateTimestamp = strtotime($startDate);
            // $endDateTimestamp = strtotime($endDate);

            // check item history
            $itemHistory = ItemLog::where('uid', $member->uid)
                                    // ->whereBetween('log_date_timestamp', [$startDateTimestamp,$endDateTimestamp])
                                    ->count();

            // check monster kill count
            // $monsterKillCount = $this->setMosterKillCount($member->uid,$member->ncid,$member->char_id,$startDate,$endDate);

            $ncid = $this->getNcidByUid($uid);
            $createAfter = $this->doGetChar($uid,$ncid);

            return [
                // 'character' => $member->char_name,
                'username' => $member->username,
                'can_receive' => $createAfter?true:false,
                'received' => $itemHistory > 0
            ];

        }

        public function redeemReward(Request $request) {


            if ($request->has('type') == false && $request->type != 'redeem_reward') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }

            // if ($request->has('package_id') == false || !in_array($request->package_id, [1,2])) {//check parameter
            //     return response()->json([
            //         'status' => false,
            //         'message' => 'Invalid parameter (2)!'
            //     ]);
            // }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);
            $package_id = $request->package_id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            // if(isset($member) && empty($member->char_id)){
            //     return response()->json([
            //             'status'    => false,
            //             'type'      => 'no_char',
            //             'message'   => 'No character info'
            //         ]);
            // }

            $eventInfo = $this->setEventInfo($uid);
            if($eventInfo['can_receive'] == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณไม่ได้สร้างตัวละครหลังวันที่ 16 มกราคม 2562'
                    // 'message' => 'ขออภัย<br />คุณยังกำจัดมอนสเตอร์ไม่ครบ 3,000 ตัว'
                ]);
            }

            if($eventInfo['received'] == true){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้รับของขวัญไปก่อนหน้านี้แล้ว'
                ]);
            }

            // set reward
            $rewards = new Reward;
            $rewardInfo = $rewards->setRewardByPackageId($package_id);

            $goods_data = [];
            $packageId = $rewardInfo['package_id'];
            $packageName = $rewardInfo['package_title'];
            $amount = $rewardInfo['amount'];

            if(count($rewardInfo['product_set']) > 0){
                foreach($rewardInfo['product_set'] as $product){
                    $goods_data[] = [
                        'goods_id' => $product['product_id'],
                        'purchase_quantity' => $product['product_quantity'],
                        'purchase_amount' => 0,
                        'category_id' => 40
                    ];
                }
            }

            $itemLog = $this->addSendItemLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'package_id' => $rewardInfo['package_id'],
                'package_title' => $rewardInfo['package_title'],
                'amount' => $rewardInfo['amount'],
                'status' => 'pending',
                'send_item_status' => false,
                'send_item_purchase_id' => '0',
                'send_item_purchase_status' => '0',
                'goods_data' => json_encode($goods_data),
                'log_date' => date('Y-m-d H:i:s'),
                'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                'last_ip' => $this->getIP()
            ]);
            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                    'status' => 'success'
                ], $itemLog['id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                    'status' => true,
                    'message' => 'ส่งของขวัญเสร็จเรียบร้อย<br />กรุณาตรวจสอบกล่องจดหมายภายในเกม',
                    'content' => $eventInfo
                ]);

            }else{
                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                    'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                    'status' => 'unsuccess'
                ], $itemLog['id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งของขวัญได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                    'content' => $eventInfo
                ]);
            }
        }

    }
