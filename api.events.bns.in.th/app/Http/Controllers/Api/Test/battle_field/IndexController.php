<?php

namespace App\Http\Controllers\Api\Test\battle_field;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

use App\Models\Test\battle_field\Member;
use App\Models\Test\battle_field\Reward;
use App\Models\Test\battle_field\SendItemLog;
use App\Models\Test\battle_field\QuestLog;

class IndexController extends BnsEventController
{

    private $baseApi = 'http://api.apps.garena.in.th';
    private $startTime = '2020-04-29 06:00:00';
    private $endTimeApi = '2020-05-26 23:59:59';
    private $endTime = '2020-06-02 23:59:59';
    private $newItemCanRedeem = '2020-05-05 00:00:00';
//    private $newItemCanRedeem = '2020-05-20 00:00:00'; // real
    private $userEvent = '';

    protected $coin = [
        'quest_success_1' => 50,
        'quest_success_2' => 50,
        'quest_success_3' => 35,
        'quest_success_4' => 50,
        'quest_success_5' => 50,
        'quest_success_6' => 35,
        'quest_success_7' => 50,
        'quest_success_8' => 50,
        'quest_success_9' => 35,
        'peak'            => 25,
    ];

    protected $questList = [
        [
            'name' => 'ชนะในโหมดลมหวน',
            'coin' => 50,
        ],
        [
            'name' => 'ฆ่าคนในแผนที่ลมหวน',
            'coin' => 50,
        ],
        [
            'name' => 'เข้าร่วมโหมดลมหวน',
            'coin' => 35,
        ],
        [
            'name' => 'ชนะในโหมดเบลูก้า ลากูน',
            'coin' => 50,
        ],
        [
            'name' => 'ฆ่าคนในแผนที่เบลูก้า ลากูน',
            'coin' => 50,
        ],
        [
            'name' => 'เข้าร่วมโหมดเบลูก้า ลากูน',
            'coin' => 35,
        ],
        [
            'name' => 'ชนะในโหมดฐานจักรกลนาริว',
            'coin' => 50,
        ],
        [
            'name' => 'ฆ่าคนในแผนที่ฐานจักรกลนาริว',
            'coin' => 50,
        ],
        [
            'name' => 'เข้าร่วมโหมดฐานจักรกลนาริว',
            'coin' => 35,
        ],
        [
            'name' => 'รับคะแนนฟรีประจำวัน',
            'coin' => 100,
        ],
    ];


    public function __construct(Request $request)
    {
        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);
        $this->checkEventStatus();

    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status'  => false,
                'type'    => 'no_permission',
                'message' => 'Sorry, you do not have permission.',
            ]));
        }
        // dd($this->startTime,$this->endTime);
        if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
            die(json_encode([
                'status'  => false,
                'type'    => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม',
            ]));
        }

//        if ($this->checkIsMaintenance()) {
//            die(json_encode([
//                'status'  => false,
//                'type'    => 'maintenance',
//                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์',
//            ]));
//        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง',
                ]));
            }
        } else {
            die(json_encode([
                'status'  => false,
                'type'    => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน',
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function is_accepted_ip()
    {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'             => $uid,
                'username'        => $username,
                'ncid'            => $ncid,
                'character_id'    => 0,
                'character_name'  => "",
                'quest_success_1' => 0,
                'quest_success_2' => 0,
                'quest_success_3' => 0,
                'quest_success_4' => 0,
                'quest_success_5' => 0,
                'quest_success_6' => 0,
                'quest_success_7' => 0,
                'coin'            => 0,
                'coin_used'       => 0,
                'last_ip'         => $this->getIP(),
                'last_update'     => null,
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
            $this->userEvent = $resp;

        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id)) {
            return false;
        }

        return Member::where('_id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:LANTERN_FESTIVAL:ASSOCIATE_UID_WITH_NCID_'.$uid, 10,
            function () use ($uid, $username) {
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'user_info',
                    'uid'      => $uid,
                ]);
                $userNcInfo       = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }

            });
    }

    private function dosendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Sending item from garena events bns Lantern festival 2019.',
            'goods'                => json_encode($goodsData),
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function doGetChar($uid, $ncid)
    {
        $resp_raw = $this->apiCharacters($uid, $ncid);

        $resp = json_decode($resp_raw);
        if (is_object($resp)) {
            if ($resp->status) {
                $content = [];
                $i       = 1;
                foreach ($resp->response as $key => $value) {
                    $level = (int) $value->level;
                    $hm    = (int) $value->mastery_level;

                    // level check
                    if ($level >= 60 && $hm >= 13) {
                        $content[] = [
                            'char_id'   => $value->id,
                            'char_name' => $value->name,
                        ];
                    }
                    $i++;
                }
                return $content;
            }
        }
        return null;
    }

    private function addItemHistoryLog($arr)
    {
        return SendItemLog::create($arr);
    }

    /**
     * API ROUTE : Get Event Info and check member
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEventInfo(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter!',
            ]);
        }

        $userData = $this->userData;
        $uid      = $userData['uid'];

        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status'  => false,
                'message' => 'No member data.',
            ]);
        }

        $eventInfo = $this->setEventInfo($uid);

        return response()->json([
            'status' => true,
            'data'   => $eventInfo,
        ]);

    }

    /**
     * Check member is select char and return chars
     * @param $uid
     * @return array|bool
     */
    private function setEventInfo($uid)
    {
        if (empty($uid)) {
            return false;
        }
        $member = Member::where('uid', $uid)->first();
        if ($member->character_id == 0 || $member->character_id == null) {

            $mychar = $this->doGetChar($member->uid, $member->ncid);

            return [
                'username'       => $member->username,
                'character_name' => '',
                'selected_char'  => false,
                'characters'     => $mychar,
                'coin'           => 0,
                'free'           => 'disabled',
                'mission_score'  => [
                    "mission1" => [0, 0, 0,],
                    "mission2" => [0, 0, 0,],
                    "mission3" => [0, 0, 0,],
                ],
                'itemLists'      => $this->getItemLists(),

            ];
        }

        $this->summaryCoin();
        $this->getToday();

        return [
            'username'       => $member->username,
            'character_name' => $member->character_name,
            'selectd_char'   => true,
            'characters'     => [],
            'coin'           => $this->getCoin(),
            'free'           => $this->getFreeToday(),
            'mission_score'  => $this->getMissionScore(),
            'itemLists'      => $this->getItemLists(),
        ];
    }

    public function getFreeToday()
    {
        // end time api
        if (Carbon::now() > Carbon::parse($this->endTimeApi)) {
            return 'disabled';
        }

        $td = Carbon::now()->toDateString();

        $questLogs = QuestLog::whereUid($this->userEvent->uid)
            ->whereCharId($this->userEvent->character_id)
            ->whereDateLog($td)
            ->first();

        if (!$questLogs) {
            return "disabled";
        }

        if (isset($questLogs->claim_free_at) || $questLogs->claim_free_at != null || $questLogs->quest_free != 0) {
            return "received";
        }

        if ((boolean) $questLogs->is_play != false) {
            return "pending";
        }

        return "disabled";
    }

    public function claimFree(Request $request)
    {
        if ($request->has('type') == false || $request['type'] != 'claim_free') {
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter!',
            ]);
        }

        // end time api
        if (Carbon::now() > Carbon::parse($this->endTimeApi)) {
            return response()->json([
                'status'  => false,
                'message' => 'หมดเขตเวลารับคะแนนฟรี แต่สามารถแลกของรางวัลได้ถึงวันที่ '.$this->endTimeApi,
            ]);
        }

        // check today
        $today = $this->getToday();

        if (isset($today->claim_free_at) || $today->claim_free_at != null || $today->quest_free != 0) {
            return response()->json([
                'status' => true, 'message' => 'คุณรับคะแนนฟรีของวันนี้ไปแล้ว',
            ]);
        }

        if ($today->is_play == 0 || (boolean) $today->is_play == false) {
            return response()->json([
                'status' => true, 'message' => 'ยังไม่ผ่านเงื่อนไข',
            ]);
        }

        $today->claim_free_at = Carbon::now()->toDateTimeString();
        $today->coin          += 100;
        $today->quest_free    = 100;
        $today->save();

        $this->summaryCoin();

        return response()->json([
            'status'  => true,
            'message' => 'ได้รับ 100 คะแนน',
            'data'    => [
                'coin'      => $this->getCoin(),
                'free'      => $this->getFreeToday(),
                'itemLists' => $this->getItemLists(),
            ],
        ]);
    }

    /**
     * API ROUTE : Select char and save to call summary user first time.
     * @param  Request  $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function selectCharacter(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'select_character') {
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter!',
            ]);
        }
        if ($decoded->has('id') == false) {
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter! (2)',
            ]);
        }

        $char_id = (int) $decoded['id'];

        $userData = $this->userData;
        $uid      = $userData['uid'];

        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status'  => false,
                'message' => 'ไม่พบข้อมูล',
            ]);
        }

        if ($member->character_id > 0 || $member->character_id != null) {
            return response()->json([
                'status'  => false,
                'message' => 'ไม่สามารถเปลี่ยนตัวละครได้',
            ]);
        }

        $mychar   = $this->doGetChar($member->uid, $member->ncid);
        $myselect = collect($mychar)->where('char_id', $char_id);

        if (count($myselect) == 0) {
            return response()->json([
                'status'  => false,
                'message' => 'ไม่พบตัวละคร',
            ]);
        }
        $member->character_id   = (int) $myselect->first()['char_id'];
        $member->character_name = $myselect->first()['char_name'];
        $member->save();

        $this->userEvent = $member;
        $this->userEvent->save();

        return [
            'status' => true,
            'data'   => [
                'username'       => $member->username,
                'character_name' => $member->character_name,
                'selectd_char'   => true,
                'characters'     => [],
                'coin'           => $this->getCoin(),
                'itemLists'      => $this->getItemLists(),
                'free'           => $this->getFreeToday(),
                'mission_score'  => $this->getMissionScore(),
            ],
        ];

    }

    /**
     * API ROUTE : Request redeem reward and check any condition before send item
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function redeemReward(Request $request)
    {
        $decoded = $request;

        if ($decoded->has('type') == false || $decoded['type'] != 'redeem') {//check parameter
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter! (1)',
            ]);
        }

        $userData = $this->userData;
        $uid      = $userData['uid'];

        if ($decoded->has('token') == false) {
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter! (2)',
            ]);
        }

        $token = (int) $decoded['token'];
        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status'  => false,
                'message' => 'No member data.',
            ]);
        }

        if ($member->character_id == 0) {
            return response()->json([
                'status'  => false,
                'message' => 'โปรดเลือกตัวละครก่อน',
            ]);
        }

        $reward = Reward::whereToken($token)->first();
        if (!$reward) {
            return response()->json([
                'status'  => false,
                'message' => 'ขออภัย<br />ไม่พบไอเทม',
            ]);
        }

        $this->summaryCoin();
        $coin = $this->getCoin();

        if ($coin < $reward->coin) {
            return response()->json([
                'status'  => false,
                'message' => 'ขออภัย<br />เหรียญไม่เพียงพอ',
            ]);
        }

        // check can redeem
        $check = $this->checkCanRedeem($reward);
        if ($check === false) {
            return response()->json([
                'status'  => false,
                'message' => 'ขออภัย<br />ไม่สามารถรับของรางวัลได้',
            ]);
        }

        $goods_data[] = [
            'goods_id'          => $reward->product_id,
            'purchase_quantity' => 1,
            'purchase_amount'   => 0,
            'category_id'       => 40,
        ];


        $sendItemLog = $this->addItemHistoryLog([
            'uid'                => $member->uid,
            'ncid'               => $member->ncid,
            'product_id'         => $reward->product_id,
            'product_title'      => $reward->name,
            'product_quantity'   => 1,
            'coin'               => $reward->coin,
            'log_date'           => (string) date('Y-m-d'),
            'log_date_timestamp' => time(),
            'status'             => 'pending',
            'goods_data'         => json_encode($goods_data),
            'last_ip'            => $this->getIP(),
        ]);

        $send_result_raw = $this->dosendItem($member->ncid, $goods_data);
        $send_result     = json_decode($send_result_raw);

        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

            $sendItemLog->send_item_status          = $send_result->status ? $send_result->status : false;
            $sendItemLog->send_item_purchase_id     = $send_result->response->purchase_id ? $send_result->response->purchase_id : 0;
            $sendItemLog->send_item_purchase_status = $send_result->response->purchase_status ? $send_result->response->purchase_status : 0;
            $sendItemLog->status                    = "success";
            $sendItemLog->send_item_respone         = $send_result_raw;
            $sendItemLog->save();

            $this->summaryCoin();

            return response()->json([
                'status'  => true,
                'message' => "ได้รับ ".$reward->name." x1<br />ไอเทมส่งเข้าไปในกล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อรับของรางวัล",
                'data'    =>
                    [
                        'coin'      => $this->getCoin(),
                        'itemLists' => $this->getItemLists(),
                    ],
            ]);

        } else {
            $sendItemLog->send_item_status          = $send_result->status ? $send_result->status : false;
            $sendItemLog->send_item_purchase_id     = 0;
            $sendItemLog->send_item_purchase_status = 0;
            $sendItemLog->send_item_respone         = $send_result_raw;
            $sendItemLog->status                    = "unsuccess";
            $sendItemLog->save();

            return response()->json([
                'status'  => false,
                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                'data'    => [
                    'coin' => $this->getCoin(),
                ],
            ]);
        }

    }

    /**
     * Game API Helper
     * @param $uid
     * @param $ncid
     * @return mixed
     */
    private function apiCharacters($uid, $ncid)
    {
        return Cache::remember('BNS:LANTERN_FESTIVAL:CHARACTERS_'.$uid, 10, function () use ($ncid) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'characters',
                'user_id'  => $ncid,
            ]);
        });
    }

    private function getItemLists()
    {
        // exchange new item 1
        $items1    = Reward::whereType(1)->select('name', 'coin', 'limit', 'token', 'product_id', 'type')->get();
        $new_items = [];
        foreach ($items1 as $item) {
            $new_items[] = [
                'name'      => $item->name,
                'coin'      => $item->coin,
                'canRedeem' => $this->checkCanRedeem($item),
                'token'     => $item->token,
            ];
        }

        // exchange per event 2
        $items2         = Reward::whereType(2)->select('name', 'coin', 'limit', 'token', 'product_id', 'type')->get();
        $item_per_event = [];
        foreach ($items2 as $item) {
            $total            = SendItemLog::whereUid($this->userEvent->uid)
                ->whereProductId($item->product_id)
                ->count();
            $item_per_event[] = [
                'name'      => $item->name,
                'coin'      => $item->coin,
                'limit'     => $total."/".$item->limit,
                'canRedeem' => $this->checkCanRedeem($item),
                'token'     => $item->token,
            ];
        }

        // exchange per day 3
        $items3       = Reward::whereType(3)->select('name', 'coin', 'limit', 'token', 'product_id', 'type')->get();
        $item_per_day = [];
        foreach ($items3 as $item) {
            $total          = SendItemLog::whereUid($this->userEvent->uid)
                ->whereProductId($item->product_id)
                ->whereLogDate(Carbon::now()->toDateString())
                ->count();
            $item_per_day[] = [
                'name'      => $item->name,
                'coin'      => $item->coin,
                'limit'     => $total."/".$item->limit,
                'canRedeem' => $this->checkCanRedeem($item),
                'token'     => $item->token,
            ];
        }

        // exchange unlimited 0
        $items4         = Reward::whereType(0)->select('name', 'coin', 'limit', 'token', 'product_id', 'type')->get();
        $item_unlimited = [];
        foreach ($items4 as $item) {
            $item_unlimited[] = [
                'name'      => $item->name,
                'coin'      => $item->coin,
                'canRedeem' => $this->checkCanRedeem($item),
                'token'     => $item->token,
            ];
        }

        // ----
        $return = [
            'new_items'      => $new_items,
            'item_per_event' => $item_per_event,
            'item_per_day'   => $item_per_day,
            'item_unlimited' => $item_unlimited,
        ];

        return $return;
    }

    /**
     * Get real coin after calculate used coin.
     * @return int
     */
    private function getCoin()
    {
        if ($this->userEvent) {
            $coin = ($this->userEvent->coin - $this->userEvent->coin_used);
            if ($coin <= 0) {
                return 0;
            }
            return $coin;
        }
        return 0;
    }

    public function summaryCoin()
    {
        if ($this->userEvent && $this->userEvent->character_id != 0) {

            $sum_coin = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('coin');

            $sum_coin_used = (int) SendItemLog::whereUid($this->userEvent->uid)
                ->whereStatus('success')
                ->sum('coin');

            $this->userEvent->quest_success_1 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_1');
            $this->userEvent->quest_success_2 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_2');
            $this->userEvent->quest_success_3 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_3');
            $this->userEvent->quest_success_4 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_4');
            $this->userEvent->quest_success_5 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_5');
            $this->userEvent->quest_success_6 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_6');
            $this->userEvent->quest_success_7 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_7');
            $this->userEvent->quest_success_8 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_8');
            $this->userEvent->quest_success_9 = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_success_9');
            $this->userEvent->quest_free      = (int) QuestLog::whereUid($this->userEvent->uid)
                ->whereCharId($this->userEvent->character_id)
                ->sum('quest_free');

            $this->userEvent->coin      = $sum_coin;
            $this->userEvent->coin_used = $sum_coin_used;
            $this->userEvent->save();
        }
    }

    public function checkLimit($item)
    {
        $item = Reward::whereProductId($item)->first();
        if (!$item) {
            return false;
        }

        if ($this->userEvent && $this->userEvent->character_id != 0) {
            $send_item = SendItemLog::whereUid($this->userEvent->uid)
                ->whereProductId($item->product_id)
                ->whereStatus('success')->get();

            if ((int) $send_item->count() < (int) $item->limit) {
                return true;
            }
        }

        return false;

    }

    public function checkLimitPerday($item)
    {
        if ($this->userEvent && $this->userEvent->character_id != 0) {
            $today     = Carbon::today()->toDateString();
            $send_item = SendItemLog::whereUid($this->userEvent->uid)
                ->whereProductId($item->product_id)
                ->whereLogDate($today)
                ->count();

            if ((int) $send_item >= 1) {
                return false;
            }
        }

        return true;

    }

    public function getHistory(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || !in_array($decoded['type'], ['getHistory'])) {
            return response()->json([
                'status'  => false,
                'message' => 'Invalid parameter!',
            ]);
        }

        // GET RECEIVED HISTORY
        $return_received = [];
        $logs            = QuestLog::whereUid($this->userEvent->uid)->orderBy('date_log', 'desc')->get();

        foreach ($logs as $log) {
            $date = Carbon::parse($log->date_log)->format("d/m/Y");

            for ($i = 1; $i <= 10; $i++) {
                $peak = 0;
                $no   = $i - 1;

                $quest = (int) $log->{'quest_success_'.$i} ?? 0;
                $coin  = $quest * $this->questList[$no]['coin'];

                if ($i == 10) {
                    $quest = (int) $log->quest_free ?? 0;
                    $coin  = $quest;
                }

                // check peak
                if ($i == 3 || $i == 6 || $i == 9) {
                    $peak = (int) $log->{'peak_'.$i};
                    if ($peak > 0) {
                        $peak        = (int) $log->{'peak_'.$i};
                        $coin_deduct = $peak * 35;
                        $coin        = $coin - $coin_deduct;
                    }
                }

                if ($quest > 0 && $coin > 0) {
                    $return_received[] = [
                        'coin'     => $coin,
                        'quest'    => $this->questList[$no]['name'],
                        'date_log' => $date,
                    ];
                }

                if ($peak > 0) {
                    $return_received[] = [
                        'coin'     => $peak * 60,
                        'quest'    => $this->questList[$no]['name'].' (ช่วงเวลาพิเศษ)',
                        'date_log' => $date,
                    ];
                }
            }
        }

        // GET REDEEM HISTORY
        $return_redeem = [];
        $redeems       = SendItemLog::whereUid($this->userEvent->uid)->orderBy('id', 'desc')->select('created_at',
            'coin', 'product_title')->get();
        foreach ($redeems as $redeem) {
            $dt              = Carbon::parse($redeem->created_at);
            $return_redeem[] = [
                'coin' => $redeem->coin,
                'item' => $redeem->product_title,
                'date' => $dt->format("d/m/Y"),
                'time' => $dt->format("H:i:s"),
            ];
        }

        return response()->json([
            'status' => true,
            'data'   => [
                'history' => [
                    'received' => $return_received,
                    'redeem'   => $return_redeem,
                ],
            ],
        ]);

    }

    public function getToday()
    {
        $td = Carbon::now()->toDateString();

        $questLogs = QuestLog::whereUid($this->userEvent->uid)
            ->whereCharId($this->userEvent->character_id)
            ->whereDateLog($td)
            ->first();

        if (!$questLogs) {
            $questLogs            = new QuestLog();
            $questLogs->uid       = $this->userEvent->uid;
            $questLogs->username  = $this->userEvent->username;
            $questLogs->ncid      = $this->userEvent->ncid;
            $questLogs->char_id   = $this->userEvent->character_id;
            $questLogs->char_name = $this->userEvent->character_name;
            $questLogs->date_log  = $td;

            if ($questLogs->save()) {
                return $questLogs;
            }
        }

        return $questLogs;
    }

    public function getMissionScore()
    {
        $today    = $this->getToday();
        $misison1 = [
            $today->quest_success_1 * $this->coin['quest_success_1'],
            $today->quest_success_2 * $this->coin['quest_success_2'],
            $today->quest_success_3 * $this->coin['quest_success_3'] + ($today->peak_3 * $this->coin['peak']),
        ];
        $misison2 = [
            $today->quest_success_4 * $this->coin['quest_success_4'],
            $today->quest_success_5 * $this->coin['quest_success_5'],
            $today->quest_success_6 * $this->coin['quest_success_6'] + ($today->peak_6 * $this->coin['peak']),
        ];
        $misison3 = [
            $today->quest_success_7 * $this->coin['quest_success_7'],
            $today->quest_success_8 * $this->coin['quest_success_8'],
            $today->quest_success_9 * $this->coin['quest_success_9'] + ($today->peak_9 * $this->coin['peak']),
        ];

        return [
            'mission1' => $misison1,
            'mission2' => $misison2,
            'mission3' => $misison3,
        ];
    }

    private function checkCanRedeem(Reward $item)
    {
        // check coin
        if ($this->getCoin() < $item->coin) {
            return false;
        }


        if ($item->type == 1) { // new item check date
            if (Carbon::now() > Carbon::parse($this->newItemCanRedeem)) {
                return true;
            }
        } elseif ($item->type == 2) { // item per event

            $send_item = SendItemLog::whereUid($this->userEvent->uid)
                ->whereProductId($item->product_id)
                ->count();

            if ((int) $send_item < $item->limit) {
                return true;
            }

        } elseif ($item->type == 3) { // item per day

            $send_item = SendItemLog::whereUid($this->userEvent->uid)
                ->whereProductId($item->product_id)
                ->whereLogDate(Carbon::now()->toDateString())
                ->count();

            if ((int) $send_item < $item->limit) {
                return true;
            }

        } elseif ($item->type == 0) { // item per unlimited
            return true;

        } else {
            return false;
        }


        return false;
    }

}
