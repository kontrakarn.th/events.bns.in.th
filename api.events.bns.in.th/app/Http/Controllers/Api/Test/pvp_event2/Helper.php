<?php

namespace App\Http\Controllers\Api\Test\pvp_event2;

class Helper
{
    const IP_WHITE_LIST = [
        '112.121.131.234',
        '110.168.229.249',
        '180.183.119.151',
        '27.254.46.174',
        '183.88.67.189',
        '58.137.18.34',
        '112.121.39.3',
        '127.0.0.1' // local
    ];
    const MESSAGE_CODE_OK   = 'OK';
    const MESSAGE_CODE_FAIL = 'FAIL';
    const MESSAGE_CODE_CHARACTER_NOT_FOUND  = 'CHARACTER_NOT_FOUND';
    const MESSAGE_CODE_DIAMOND_NOT_ENOUGH   = 'DIAMOND_NOT_ENOUGH';
    const MESSAGE_CODE_DIAMOND_SERVICE_FAIL = 'DIAMOND_SERVICE_FAIL';
    const MESSAGE_CODE_ITEM_SERVICE_FAIL    = 'ITEM_SERVICE_FAIL';

    const MESSAGE_CODE_BUY_PACKAGE_OK       = 'BUY_PACKAGE_OK';
    const MESSAGE_CODE_BUY_PACKAGE_ALREADY  = 'BUY_PACKAGE_ALREADY';

    const MESSAGE_CODE_INTERNAL_ERROR       = 'INTERNAL_ERROR';
    const MESSAGE_CODE_USER_NOT_FOUND       = 'USER_NOT_FOUND';
    const MESSAGE_CODE_INVALID_JWT          = 'INVALID_JWT';
    const MESSAGE_CODE_EVENT_NOT_OPEN       = 'EVENT_NOT_OPEN';
    const MESSAGE_CODE_MAINTENANCE          = 'MAINTENANCE';
    const MESSAGE_CODE_EVENT_MAINTENANCE    = 'EVENT_MAINTENANCE';
    const MESSAGE_CODE_PERMISSION_DENY      = 'PERMISSION_DENY';
    const MESSAGE_CODE_EVENT_BUY_PACKAGE_MAINTENANCE      = 'EVENT_BUY_PACKAGE_MAINTENANCE';
    const MESSAGE_CODE_BUY_PACKAGE_YET = 'BUY_PACKAGE_YET';
    const MESSAGE_DESC = [
        'OK'                    => 'สำเร็จ',
        'FAIL'                  => 'เกิดข้อผิดพลาดบางอย่าง กรุณาลองใหม่ภายหลัง',
        'CHARACTER_NOT_FOUND'   => 'ไม่พบข้อมูลตัวละคร',
        'DIAMOND_NOT_ENOUGH'    => 'ไดมอนด์ไม่เพียงพอ',
        'DIAMOND_SERVICE_FAIL'  => 'ขออภัย ระบบแลกเปลี่ยนไอเท็มอยู่ระหว่างการปรับปรุงระบบ(1)',
        'ITEM_SERVICE_FAIL'     => 'ขออภัย ระบบแลกเปลี่ยนไอเท็มอยู่ระหว่างการปรับปรุงระบบ(2)',
        'BUY_PACKAGE_OK'        => 'สั่งซื้อแพ็คเกจเรียบร้อยแล้ว',
        'BUY_PACKAGE_ALREADY'   => 'คุณได้ซื้อแพ็คเกจไปก่อนหน้านี้แล้ว',
        'BUY_PACKAGE_YET'   => 'ยังไม่ได้ซื้อแพ็คเกจ',
        'EVENT_BUY_PACKAGE_MAINTENANCE' => 'ขออภัย ระบบแลกเปลี่ยนไอเท็มอยู่ระหว่างการปรับปรุงระบบ',
        'INTERNAL_ERROR'        => 'ขออภัย ระบบแลกเปลี่ยนไอเท็มอยู่ระหว่างการปรับปรุงระบบ(3)',
        'USER_NOT_FOUND'        => 'ไม่พบข้อมูลผู้ใช้งานบนระบบ(1)',
        'INVALID_JWT'           => 'ไม่พบข้อมูลผู้ใช้งานบนระบบ(2)',
        'EVENT_NOT_OPEN'        => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม',
        'MAINTENANCE'           => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์',
        'EVENT_MAINTENANCE'           => 'ขออภัย ระบบกำลังปิดปรับปรุงชั่วคราว',
        'PERMISSION_DENY'       => 'Sorry, you do not have permission.'
    ];
    public static function responseDataAndExit($status, $messageCode, $dataSet)
    {
        $mdesc = isset(self::MESSAGE_DESC[$messageCode]) ? self::MESSAGE_DESC[$messageCode] : '';
        die(json_encode([
            'status'    => $status,
            'type'      => $messageCode,
            'message'   => $mdesc,
            'data' => $dataSet
        ], JSON_UNESCAPED_UNICODE));
    }
    public static function responseData(bool $status, string $messageCode, $dataSet = null)
    {
        $mdesc = isset(self::MESSAGE_DESC[$messageCode]) ? self::MESSAGE_DESC[$messageCode] : '';
        return ([
            'status'    => $status,
            'type'      => $messageCode,
            'message'   => $mdesc,
            'data' => $dataSet
        ]);
    }
    public static function getClientIP()
    {
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = array_shift($ips);
        }
        return $ip;
    }
    public static function getUserAgent()
    {
        return request()->header('user-agent');
    }
    public static function isIPAccept()
    {
        $ip = self::getClientIP();
        return in_array($ip, self::IP_WHITE_LIST);
    }
}
