<?php

namespace App\Http\Controllers\Api\Test\hardmode;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

use App\Models\Test\hardmode\Setting;
use App\Models\Test\hardmode\Member;
use App\Models\Test\hardmode\ItemLog;
use App\Models\Test\hardmode\Quest;
use App\Models\Test\hardmode\QuestDailyLog;
use App\Models\Test\hardmode\Reward;
use App\Models\Test\hardmode\DeductLog;
use App\Models\Test\hardmode\History;
use App\Jobs\hardmode\test\CheckQuestDailyQueue as Queue;

class IndexController extends BnsEventController
{

    private $baseApi = 'http://api.apps.garena.in.th';

    private $packageRequiredDiamonds = 30000;

    public function __construct(Request $request)
    {
        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);

        $this->checkEventStatus();
    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status' => false,
                'type' => 'no_permission',
                'message' => 'Sorry, you do not have permission.'
            ]));
        }

        // get events setting info
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status' => false,
                'type' => 'maintenance',
                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
            ]));
        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                ]));
            }
        } else {
            die(json_encode([
                'status' => false,
                'type' => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function is_accepted_ip()
    {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'              => $uid,
                'username'         => $username,
                'ncid'             => $ncid,
                'char_id'          => 0,
                'char_name'        => '',
                'world_id'         => 0,
                'total_points'     => 0,
                'used_points'      => 0,
                'total_tokens'     => 0,
                'used_tokens'      => 0,
                'already_purchased' => 0,
                'last_ip'          => $this->getIP(),
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
            $this->userEvent = $resp;
        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id)) {
            return false;
        }

        return Member::where('id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:LEAGUE:LIVE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'user_info',
                'uid'      => $uid,
            ]);
            $userNcInfo       = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }
        });
    }

    private function doGetChar($uid, $ncid)
    { //new fixed function
        $resp_raw = $this->apiCharacters($uid, $ncid);

        $resp = json_decode($resp_raw);
        if (is_object($resp)) {
            if ($resp->status) {
                $content = [];
                $i       = 1;
                foreach ($resp->response as $key => $value) {
                    // if((int)$value->level >= 60 && (int)$value->mastery_level>=12){
                    $content[] = [
                        'id' => $i,
                        'char_id'   => $value->id,
                        'char_name' => $value->name,
                        'world_id' => $value->world_id,
                        'job' => $value->job,
                        'level' => $value->level,
                        'creation_time' => $value->creation_time,
                        'last_play_start' => $value->last_play_start,
                        'last_play_end' => $value->last_play_end,
                        'last_ip' => $value->last_ip,
                        'exp' => $value->exp,
                        'mastery_level' => $value->mastery_level,
                    ];
                    // }

                    $i++;
                }
                return $content;
            }
        }
        return null;
    }

    private function apiCharacters($uid, $ncid)
    {
        return Cache::remember('BNS:LEAGUE:LIVE:CHARACTERS_' . $uid, 10, function () use ($ncid) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'characters',
                'user_id'  => $ncid,
            ]);
        });
    }

    public function selectCharacter(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'select_character') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }
        if ($decoded->has('id') == false) { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (2)'
            ]);
        }

        $charKey = (int) $decoded['id'];

        $userData = $this->userData;
        $uid = $userData['uid'];

        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบข้อมูล'
            ]);
        }

        if ($member->char_id > 0) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถเปลี่ยนตัวละครได้'
            ]);
        }

        $mychar = $this->doGetChar($member->uid, $member->ncid);
        $myselect = collect($mychar)->where('id', $charKey)->first();
        if (count($myselect) == 0) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบตัวละคร'
            ]);
        }

        $member->char_id   = (int) $myselect['char_id'];
        $member->char_name = $myselect['char_name'];
        $member->world_id       = $myselect['world_id'];
        $member->save();

        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] == false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message'],
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }

    private function dosendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Sending item from garena events BNS League.',
            'goods'                => json_encode($goodsData),
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function addItemHistoryLog($arr)
    {
        return ItemLog::create($arr);
    }

    private function updateItemHistoryLog($arr, $log_id, $uid)
    {
        return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
    }

    private function addHistory($arr)
    {
        return History::create($arr);
    }

    private function getEventSetting()
    {
        // return Cache::remember('BNS:LEAGUE:LIVE:EVENT_SETTING_TEST', 10, function() {
        return Setting::where('active', 1)->first();
        // });
    }

    public function getEventInfo(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] == false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message'],
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }

    private function setEventInfo($uid)
    {
        if (empty($uid)) {
            return [
                'status' => false,
                'message' => 'ไม่พอข้อมูล'
            ];
        }

        $member = Member::where('uid', $uid)->first();

        if ($member->char_id == 0) {

            $charData = $this->doGetChar($member->uid, $member->ncid);

            $content = [];

            $content = collect($charData)
                ->filter(function ($obj) {
                    return $obj['level'] >= 60 && $obj['mastery_level'] >= 15;
                })
                ->map(function ($value) {
                    return [
                        'id' => $value['id'],
                        'char_name' => $value['char_name'],
                    ];
                })
                ->values();

            return [
                'status' => true,
                'username' => $member->username,
                'character_name' => '',
                'selected_char' => false,
                'characters' => $content,
                'points' => 0,
                'tokens' => 0,
                'already_purchased' => true,
                'claim_daily_free_points_status' => 'not_in_condition',
                'claim_daily_purchase_points' => 'not_in_condition',
                'open_claim_reward' => false,
                'quest_daily_list' => [],
                'can_open_box' => false,
                'claim_reward_list' => [],
            ];
        }

        // $this->createDailyQuestLog($member);
        // $this->checkDailyQuestLog($member);

        return [
            'status' => true,
            "message" => 'Event Info',
            'username' => $member->username,
            'character_name' => $member->char_name,
            'selected_char' => true,
            'characters' => [],
            'points' => $member->remaining_points,
            'tokens' => $member->remaining_tokens,
            'already_purchased' => $member->already_purchased,
            'claim_daily_free_points_status' => $this->getClaimDailyFreePointsStatus($member),
            'claim_daily_purchase_points' => $this->getClaimDailyPurchasePointsStatus($member),
            'open_claim_reward' => $this->isOpenClaimReward(),
            'quest_daily_list' => $this->setMemberSpecialDailyQuest($member->uid),
            'can_open_box' => $member->remaining_points > 0,
            'claim_reward_list' => $this->getClaimRewardList($member),
        ];
    }

    private function getClaimRewardList($member)
    {
        $claimStartList = Reward::where('package_type', 'claim_start')->get()
            ->map(function ($reward) use ($member) {
                return [
                    'reward_id' => $reward->id,
                    'reward_name' => $reward->package_name,
                    'tokens' => $reward->required_tokens,
                    'can_claim' => $this->isOpenClaimReward()
                        && $member->remaining_tokens >= $reward->required_tokens
                        && (time() >= strtotime($reward->claim_start))
                ];
            });

        $claimLimitList = Reward::where('package_type', 'claim_limit')->get()
            ->map(function ($reward) use ($member) {
                $limitCount = History::where('uid', $member->uid)
                    ->where('reward_id', $reward->id)
                    ->where('reward_type', 'claim_limit')
                    ->count();

                return [
                    'reward_id' => $reward->id,
                    'reward_name' => $reward->package_name,
                    'tokens' => $reward->required_tokens,
                    'limit_count' => $limitCount,
                    'can_claim' => $this->isOpenClaimReward()
                        && $member->remaining_tokens >= $reward->required_tokens
                        && $limitCount < $reward->limit
                ];
            });

        $claimUnlimitList = Reward::where('package_type', 'claim')->get()
            ->map(function ($reward) use ($member) {
                return [
                    'reward_id' => $reward->id,
                    'reward_name' => $reward->package_name,
                    'tokens' => $reward->required_tokens,
                    'can_claim' => $this->isOpenClaimReward()
                        && $member->remaining_tokens >= $reward->required_tokens
                ];
            });

        return [
            'claim_start' => $claimStartList,
            'claim_limit' => $claimLimitList,
            'claim_unlimit' => $claimUnlimitList,
        ];
    }

    private function createDailyQuestLog($member)
    {
        $logDate = date('Y-m-d');

        $dailyQuest = Quest::all();

        if (count($dailyQuest) > 0) {

            foreach ($dailyQuest as $quest) {

                // check daily quest exitst
                $checkExist = 0;
                $checkExist = QuestDailyLog::where('uid', $member->uid)->where('quest_id', $quest->quest_id)->where('log_date', $logDate)->count();
                if ($checkExist <= 0) {
                    QuestDailyLog::create([
                        'uid' => $member->uid,
                        'username' => $member->username,
                        'ncid' => $member->ncid,
                        'char_id' => $member->char_id,
                        'char_name' => $member->char_name,
                        'quest_id' => $quest->quest_id,
                        'quest_dungeon' => $quest->quest_dungeon,
                        'quest_title' => $quest->quest_title,
                        'quest_code' => $quest->quest_code,
                        'quest_type' => $quest->quest_type,
                        'quest_limit' => $quest->quest_limit,
                        'quest_points' => $quest->quest_points,
                        'image' => $quest->image,
                        'completed_count' => 0,
                        'claimed_count' => 0,
                        'status' => 'pending',
                        'claim_status' => 'pending',
                        'log_date' => date('Y-m-d'),
                        'log_date_timestamp' => time(),
                        'last_ip' => $this->getIP(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                }
            }
        }
    }

    private function checkDailyQuestLog($member)
    {
        $setting = Setting::where('active', 1)->first();
        if (isset($setting)) {

            if (time() >= strtotime($setting->open_box_start) && time() <= strtotime($setting->open_box_end)) {

                $logDate = date('Y-m-d');

                $memberQuests = QuestDailyLog::where('uid', $member->uid)
                    ->where('char_id', $member->char_id)
                    ->whereNotNull('quest_code')
                    ->where('quest_type', 'daily_bosskill')
                    ->where('status', 'pending')
                    ->where('log_date', $logDate)
                    ->get();

                if (count($memberQuests) > 0) {
                    foreach ($memberQuests as $quest) {
                        if (empty($quest->id)) {
                            continue;
                        }

                        \Log::info("BNS Hardmode : Create Daily Quest Log UID: " . $quest->uid . " , Username : " . $quest->username . " , Quest Code : " . $quest->quest_code . " , Quest Title : " . $quest->quest_title);

                        Queue::dispatch($quest->id)->onQueue('bns_hardmode_check_quest_daily_log_queue_live');
                    }
                }
            }
        }
    }

    public function postClaimSpecialQuestPoints(Request $request)
    {
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->open_box_start) || time() > strtotime($eventSetting->open_box_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'claim_special_quest_points') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $questId = $decoded['id'];

        $logDate = date('Y-m-d');

        // check member is aready select quest
        $selectedQuest = QuestDailyLog::where('quest_type', 'daily_bosskill')
            ->where('quest_id', $questId)
            ->where('uid', $member->uid)
            ->where('log_date', $logDate)
            ->first();

        if (!isset($selectedQuest)) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย คุณยังไม่ได้เลือกเควสของวันนี้'
            ]);
        }

        if ($selectedQuest->claim_status === 'claimed') {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย คุณได้รับคะแนนจากเควสนี้ไปแล้ว'
            ]);
        }

        // set points update to member points
        $remainPoints = ($selectedQuest->completed_count - $selectedQuest->claimed_count) * $selectedQuest->quest_points;
        if ($remainPoints <= 0) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย เหรียญไม่พอ'
            ]);
        }

        // update daily log
        $selectedQuest->claimed_count = $selectedQuest->completed_count;
        $selectedQuest->claim_status = 'claimed';
        $selectedQuest->save();

        // update membrer points
        $member->total_points = $member->total_points + $remainPoints;
        $member->save();

        return response()->json([
            'status' => true,
            'message' => "ได้รับ $remainPoints คะแนน",
            'data' => $this->setEventInfo($uid)
        ]);
    }

    private function setMemberSpecialDailyQuest($uid)
    {
        $logDate = date('Y-m-d');
        $questDailyList = [];
        $dailyBossKillLogs = QuestDailyLog::where('quest_type', 'daily_bosskill')->where('uid', $uid)->where('log_date', $logDate)->get();

        foreach ($dailyBossKillLogs as $log) {
            $questDailyList[] = [
                'id' => $log->quest_id,
                'title' => $log->quest_title,
                'dungeon' => $log->quest_dungeon,
                'points' => $log->quest_points,
                'status' => $this->getQuestStatus($log),
            ];
        }

        if (count($dailyBossKillLogs) == 0) {
            $quests = Quest::where('quest_type', 'daily_bosskill')->get();
            foreach ($quests as $quest) {
                $questDailyList[] = [
                    'id' => $quest->id,
                    'title' => $quest->quest_title,
                    'dungeon' => $quest->quest_dungeon,
                    'points' => $quest->quest_points,
                    'status' => 'not_in_condition',
                ];
            }
        }

        return $questDailyList;
    }

    private function getQuestStatus($log)
    {
        if ($log->completed_count <= 0) {
            return 'not_in_condition';
        }

        if ($log->claimed_count > 0) {
            return 'claimed';
        }

        if ($log->completed_count > 0) {
            return 'can_claim';
        }
    }

    private function isOpenClaimReward()
    {
        $eventSetting = $this->getEventSetting();

        return time() >= strtotime($eventSetting->claim_reward_start)
            && time() <= strtotime($eventSetting->claim_reward_end);
    }

    private function getBossKillCount($member)
    {
        $logDate = date('Y-m-d');

        return QuestDailyLog::where('uid', $member->uid)
            ->where('quest_type', 'daily_bosskill')
            ->where('completed_count', '>', 0)
            ->where('log_date', $logDate)
            ->count();
    }

    private function getClaimDailyPurchasePointsStatus($member)
    {
        $logDate = date('Y-m-d');

        $bossKilledCount = $this->getBossKillCount($member);

        $pendingFreePoints = QuestDailyLog::where('uid', $member->uid)
            ->where('quest_type', 'daily_purchase_points')
            ->where('claim_status', 'pending')
            ->where('log_date', $logDate)
            ->count();

        if ($bossKilledCount <= 0) {
            return 'not_in_condition';
        }

        if ($pendingFreePoints <= 0) {
            return 'claimed';
        }

        if ($bossKilledCount > 0 && $pendingFreePoints > 0) {
            return 'can_claim';
        }
    }

    private function getClaimDailyFreePointsStatus($member)
    {
        $logDate = date('Y-m-d');

        $bossKilledCount = $this->getBossKillCount($member);

        $pendingFreePoints = QuestDailyLog::where('uid', $member->uid)
            ->where('quest_type', 'daily_free_points')
            ->where('claim_status', 'pending')
            ->where('log_date', $logDate)
            ->count();

        if ($bossKilledCount <= 0) {
            return 'not_in_condition';
        }

        if ($pendingFreePoints <= 0) {
            return 'claimed';
        }

        if ($bossKilledCount > 0 && $pendingFreePoints > 0) {
            return 'can_claim';
        }
    }

    public function postClaimFreePoints(Request $request)
    {
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->open_box_start) || time() > strtotime($eventSetting->open_box_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'claim_free_points') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $logDate = date('Y-m-d');

        $freeDailyLog = QuestDailyLog::where('uid', $member->uid)
            ->where('quest_type', 'daily_free_points')
            ->where('log_date', $logDate)
            ->first();

        $bossKilledCount = $this->getBossKillCount($member);

        if ($bossKilledCount <= 0) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย ไม่ตรงเงื่อนไข'
            ]);
        }

        if (isset($freeDailyLog) && $freeDailyLog->claim_status == 'claimed') {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย คุณรับคะแนนฟรีของวันนี้ไปแล้ว'
            ]);
        }

        $quest = Quest::where('quest_type', 'daily_free_points')->first();

        $member->total_points = $member->total_points + $quest->quest_points;

        $freeDailyLog->claimed_count = 1;
        $freeDailyLog->claim_status = 'claimed';
        $freeDailyLog->save();

        if ($member->save()) {
            return response()->json([
                'status' => true,
                'message' => 'ได้รับ <span>คะแนนประจำวัน</span> 3 คะแนน',
                'data' => $this->setEventInfo($uid)
            ]);
        } else {
            $freeDailyLog->claimed_count = 0;
            $freeDailyLog->claim_status = 'pending';
            $freeDailyLog->save();

            return response()->json([
                'status' => false,
                'message' => 'Fail updating member point'
            ]);
        }
    }

    public function postClaimPurchasePoints(Request $request)
    {
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->open_box_start) || time() > strtotime($eventSetting->open_box_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'claim_purchase_points') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $logDate = date('Y-m-d');

        $purchaseDailyLog = QuestDailyLog::where('uid', $member->uid)
            ->where('quest_type', 'daily_purchase_points')
            ->where('log_date', $logDate)
            ->first();

        $bossKilledCount = $this->getBossKillCount($member);

        if ($bossKilledCount <= 0) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย ไม่ตรงเงื่อนไข'
            ]);
        }

        if (isset($purchaseDailyLog) && $purchaseDailyLog->claim_status == 'claimed') {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย คุณซื้อคะแนนพิเศษของวันนี้ไปแล้ว'
            ]);
        }

        $quest = Quest::where('quest_type', 'daily_purchase_points')->first();

        // check diamonds balance
        $diamondBalance = $this->setDiamondsBalance($member->uid);
        if ($diamondBalance < $quest->required_diamonds) {
            return response()->json([
                'status' => false,
                'message' => 'ไดมอนด์ไม่เพียงพอ'
            ]);
        }

        // set deduct diamonds
        $logDeduct = $this->createDeductLog([
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'deduct_type' => 'daily_purchase_points',
            'diamonds' => $quest->required_diamonds,
            'status' => 'pending',
            'last_ip' => $this->getIP(),
            'log_date' => (string) date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
        ]);

        if ($logDeduct) {

            // set desuct diamonds
            $deductData = $this->setDiamondDeductData($quest->required_diamonds);

            $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
            $resp_deduct = json_decode($resp_deduct_raw);
            if (is_object($resp_deduct) && is_null($resp_deduct) == false) {

                $this->updateDeductLog([
                    'before_deduct_diamond' => $diamondBalance,
                    'after_deduct_diamond' => $diamondBalance - $quest->required_diamonds,
                    'deduct_status' => $resp_deduct->status ?: 0,
                    'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
                    'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
                    'deduct_data' => json_encode($deductData),
                    'status' => 'success'
                ], $logDeduct['id']);

                $member->total_points = $member->total_points + $quest->quest_points;

                $purchaseDailyLog->claimed_count = 1;
                $purchaseDailyLog->claim_status = 'claimed';
                $purchaseDailyLog->save();

                if ($member->save()) {
                    return response()->json([
                        'status' => true,
                        'message' => 'ได้รับ <span>คะแนนพิเศษประจำวัน</span> 10 คะแนน',
                        'data' => $this->setEventInfo($uid)
                    ]);
                } else {
                    $purchaseDailyLog->claimed_count = 0;
                    $purchaseDailyLog->claim_status = 'pending';
                    $purchaseDailyLog->save();

                    return response()->json([
                        'status' => false,
                        'message' => 'Fail updating member point'
                    ]);
                }
            } else {
                $arr_log['status'] = 'unsuccess';
                $this->updateDeductLog($arr_log, $logDeduct['id']);
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถดำเนินการหัก Diamonds ได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />กรุณาลองใหม่อีกครั้ง'
            ]);
        }
    }

    private function createDeductLog(array $arr)
    {
        if ($arr) {
            $resp = DeductLog::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return false; //fix
            }
        }
        return null;
    }

    private function updateDeductLog(array $arr, $log_id)
    {
        if ($arr) {
            return DeductLog::where('id', $log_id)->update($arr);
        }
        return null;
    }

    private function apiDiamondBalance(int $uid)
    {
        return $this->post_api($this->baseApi, [
            'key_name' => 'bns',
            'service' => 'balance',
            'uid' => $uid,
        ]);
    }

    private function setDiamondsBalance($uid)
    {

        $diamondBalanceRaw = $this->apiDiamondBalance($uid);
        $diamondBalance = json_decode($diamondBalanceRaw);

        $diamonds = 0;
        if ($diamondBalance->status == true) {
            $diamonds = intval($diamondBalance->balance);
        }

        return $diamonds;
    }

    private function apiDeductDiamond(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Deduct diamomds for garena events BNS 3 Years Anniversary Passport.',
            'currency_group_id' => 71,
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function setDiamondDeductData($deduct_diamond_amount = 0)
    {
        if ($deduct_diamond_amount == 0)
            return false;

        return [
            [
                'goods_id' => 741,
                'purchase_quantity' => $deduct_diamond_amount,
                'purchase_amount' => $deduct_diamond_amount,
                'category_id' => 40
            ]
        ];
    }

    public function postPurchasePackage(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'purchase_package') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($member->already_purchased) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย ซื้อแพ็คเกจไปแล้ว'
            ]);
        }

        // check claim log
        $claimLog = ItemLog::where('uid', $member->uid)->where('package_type', 'package')->count();
        if ($claimLog > 0) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย คุณได้รับของรางวัลนี้ไปแล้ว'
            ]);
        }

        // check diamonds balance
        $diamondBalance = $this->setDiamondsBalance($member->uid);
        if ($diamondBalance < $this->packageRequiredDiamonds) {
            return response()->json([
                'status' => false,
                'message' => 'ไดมอนด์ไม่เพียงพอ'
            ]);
        }

        // set deduct diamonds
        $logDeduct = $this->createDeductLog([
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'deduct_type' => 'purchase_package',
            'diamonds' => $this->packageRequiredDiamonds,
            'status' => 'pending',
            'last_ip' => $this->getIP(),
            'log_date' => (string) date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
        ]);

        if ($logDeduct) {

            // set desuct diamonds
            $deductData = $this->setDiamondDeductData($this->packageRequiredDiamonds);

            $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
            $resp_deduct = json_decode($resp_deduct_raw);
            if (is_object($resp_deduct) && is_null($resp_deduct) == false) {

                $this->updateDeductLog([
                    'before_deduct_diamond' => $diamondBalance,
                    'after_deduct_diamond' => $diamondBalance - $this->packageRequiredDiamonds,
                    'deduct_status' => $resp_deduct->status ?: 0,
                    'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
                    'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
                    'deduct_data' => json_encode($deductData),
                    'status' => 'success'
                ], $logDeduct['id']);

                $reward = Reward::where('package_type', 'package')->first();

                $goods_data = [];
                $goods_data[] = [
                    'goods_id' => $reward->package_id,
                    'purchase_quantity' => $reward->package_quantity,
                    'purchase_amount' => 0,
                    'category_id' => 40,
                ];

                $sendItemLog = $this->addItemHistoryLog([
                    'uid'                           => $member->uid,
                    'username'                      => $member->username,
                    'ncid'                          => $member->ncid,
                    'package_key'                   => $reward->package_key,
                    'package_name'                  => $reward->package_name,
                    'package_quantity'              => $reward->package_quantity,
                    'package_amount'                => $reward->package_amount,
                    'package_type'                  => $reward->package_type,
                    'send_item_status'              => false,
                    'send_item_purchase_id'         => 0,
                    'send_item_purchase_status'     => 0,
                    'goods_data'                    => json_encode($goods_data),
                    'status'                        => 'pending',
                    'log_date'                      => (string) date('Y-m-d'), // set to string
                    'log_date_timestamp'            => time(),
                    'last_ip'                       => $this->getIP(),
                ]);

                $send_result_raw = $this->dosendItem($ncid, $goods_data);
                $send_result = json_decode($send_result_raw);

                $member->total_tokens = $member->total_tokens + 50;
                $member->already_purchased = true;
                $member->save();

                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                    // update send item log
                    $this->updateItemHistoryLog([
                        'send_item_status' => $send_result->status ?: false,
                        'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                        'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                        'status' => 'success'
                    ], $sendItemLog['id'], $uid);

                    return response()->json([
                        'status' => true,
                        'message' => "ได้รับ <span>" . $reward->package_name . "</span> <br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                        'data' => $this->setEventInfo($uid)
                    ]);
                } else {
                    $this->updateItemHistoryLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                        'status' => 'unsuccess'
                    ], $sendItemLog['id'], $uid);

                    return response()->json([
                        'status' => false,
                        'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                        // 'data' => $eventInfo
                    ]);
                }
            } else {
                $arr_log['status'] = 'unsuccess';
                $this->updateDeductLog($arr_log, $logDeduct['id']);
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถดำเนินการหัก Diamonds ได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />กรุณาลองใหม่อีกครั้ง'
            ]);
        }
    }

    public function openBox(Request $request)
    {
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->open_box_start) || time() > strtotime($eventSetting->open_box_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'open_box') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($member->remaining_points <= 0) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย คะแนนไม่พอ'
            ]);
        }

        // deduct point
        $member->used_points = $member->used_points + 1;

        $fixRewards = Reward::where('package_type', 'fix')->get();
        $randomReward = $this->getRandomRewardInfo();
        $rewards = $fixRewards;
        $rewards[] = $randomReward;

        $goods_data = [
            [
                'goods_id' => $fixRewards[0]->package_id,
                'purchase_quantity' => $fixRewards[0]->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40,
            ],
            [
                'goods_id' => $randomReward->package_id,
                'purchase_quantity' => $randomReward->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40,
            ]

        ];

        $sendItemLog = $this->addItemHistoryLog([
            'uid'                           => $member->uid,
            'username'                      => $member->username,
            'ncid'                          => $member->ncid,
            'package_name'                  => 'หีบห่อผ้าน้อยชิ้น x1',
            'package_quantity'              => 1,
            'send_item_status'              => false,
            'send_item_purchase_id'         => 0,
            'send_item_purchase_status'     => 0,
            'goods_data'                    => json_encode($goods_data),
            'status'                        => 'pending',
            'log_date'                      => (string) date('Y-m-d'), // set to string
            'log_date_timestamp'            => time(),
            'last_ip'                       => $this->getIP(),
        ]);

        foreach ($rewards as $reward) {
            $this->addHistory([
                'uid'                           => $member->uid,
                'username'                      => $member->username,
                'ncid'                          => $member->ncid,
                'item_log_id' => $sendItemLog['id'],
                'reward_id' => $reward->id,
                'reward_name' => $reward->package_name,
                'reward_type' => $reward->package_type,
                'type' => 'open_box',
                'reward_limit' => $reward->limit,
                'log_date'                      => (string) date('Y-m-d'), // set to string
                'log_date_timestamp'            => time(),
                'last_ip'                       => $this->getIP(),
            ]);
        }

        // add token history
        $tokenHistory = $this->addHistory([
            'uid'                           => $member->uid,
            'username'                      => $member->username,
            'ncid'                          => $member->ncid,
            'item_log_id' => $sendItemLog['id'],
            'reward_name' => 'เศษผ้าขนหนู x1',
            'reward_type' => 'token',
            'type' => 'open_box',
            'log_date'                      => (string) date('Y-m-d'), // set to string
            'log_date_timestamp'            => time(),
            'last_ip'                       => $this->getIP(),
        ]);

        $member->total_tokens = $member->total_tokens + 1;
        $member->save();

        $send_result_raw = $this->dosendItem($ncid, $goods_data);
        $send_result = json_decode($send_result_raw);

        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

            // update send item log
            $this->updateItemHistoryLog([
                'send_item_status' => $send_result->status ?: false,
                'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                'status' => 'success'
            ], $sendItemLog['id'], $uid);

            return response()->json([
                'status' => true,
                'message' => "ได้รับ <span>หีบห่อผ้าน้อยชิ้น x1</span> <br/>ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                'data' => $this->setEventInfo($uid)
            ]);
        } else {
            $this->updateItemHistoryLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
            ], $sendItemLog['id'], $uid);

            //error unsuccess log
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
            ]);
        }
    }

    public function getRandomRewardInfo()
    {

        $rewards = $this->getRandomRewardsList();

        $random = rand(0, 100000);
        $accumulatedChance = 0;
        $reward = false;

        foreach ($rewards as $value) {
            $chance = $value['rate'] * 1000;
            $currentChance = $accumulatedChance + $chance;

            // acquired reward
            if ($random >= $accumulatedChance && $random <= $currentChance) {
                $reward = $value;
                break;
            }
            $accumulatedChance = $currentChance;
        }

        $reward['random'] = $random;

        // now we have the reward
        return $reward;
    }

    private function getRandomRewardsList()
    {
        return Reward::where('package_type', 'random')->get();
    }

    public function postClaimReward(Request $request)
    {
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->claim_reward_start) || time() > strtotime($eventSetting->claim_reward_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'claim_reward') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $reward = Reward::find($decoded['id']);
        if (!$reward) {
            return response()->json([
                'status' => false,
                'message' => 'No reward.'
            ]);
        }

        if ($reward->package_type == 'claim_limit') {
            $limitCount = History::where('uid', $uid)->where('reward_id', $reward->id)->where('reward_type', 'claim_limit')->count();

            if ($limitCount >= $reward->limit) {
                return response()->json([
                    'status' => false,
                    'message' => 'เกินจำนวนครั้งที่สามารถแลกไอเทมได้'
                ]);
            }
        }

        if ($reward->package_type == 'claim_start') {
            if ((time() < strtotime($reward->claim_start))) {
                return response()->json([
                    'status' => false,
                    'message' => 'ยังไม่ถึงเวลาที่สามารถแลกไอเทมได้'
                ]);
            }
        }

        if ($member->remaining_tokens < $reward->required_tokens) {
            return response()->json([
                'status' => false,
                'message' => 'เศษผ้าขนหนูไม่เพียงพอ'
            ]);
        }

        $goods_data = [];
        $goods_data[] = [
            'goods_id' => $reward->package_id,
            'purchase_quantity' => $reward->package_quantity,
            'purchase_amount' => 0,
            'category_id' => 40,
        ];

        $sendItemLog = $this->addItemHistoryLog([
            'uid'                           => $member->uid,
            'username'                      => $member->username,
            'ncid'                          => $member->ncid,
            'package_key'                   => $reward->package_key,
            'package_name'                  => $reward->package_name,
            'package_quantity'              => $reward->package_quantity,
            'package_amount'                => $reward->package_amount,
            'package_type'                  => $reward->package_type,
            'send_item_status'              => false,
            'send_item_purchase_id'         => 0,
            'send_item_purchase_status'     => 0,
            'goods_data'                    => json_encode($goods_data),
            'status'                        => 'pending',
            'log_date'                      => (string) date('Y-m-d'), // set to string
            'log_date_timestamp'            => time(),
            'last_ip'                       => $this->getIP(),
        ]);

        if ($reward->package_type === 'claim_limit') {
            $limitCount = History::where('uid', $uid)->where('reward_id', $reward->id)->where('type', 'claim_reward')->count() + 1;
            $rewardName = "$reward->package_name ($limitCount/$reward->limit)";
        } else {
            $rewardName = $reward->package_name;
        }

        $history = $this->addHistory([
            'uid'                           => $member->uid,
            'username'                      => $member->username,
            'ncid'                          => $member->ncid,
            'item_log_id' => $sendItemLog['id'],
            'reward_id' => $reward->id,
            'reward_name' => $rewardName,
            'reward_type' => $reward->package_type,
            'type' => 'claim_reward',
            'reward_limit' => $reward->limit,
            'log_date'                      => (string) date('Y-m-d'), // set to string
            'log_date_timestamp'            => time(),
            'last_ip'                       => $this->getIP(),
        ]);

        $send_result_raw = $this->dosendItem($ncid, $goods_data);
        $send_result = json_decode($send_result_raw);

        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            $member->used_tokens = $member->used_tokens + $reward->required_tokens;
            $member->save();

            // update send item log
            $this->updateItemHistoryLog([
                'send_item_status' => $send_result->status ?: false,
                'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                'status' => 'success'
            ], $sendItemLog['id'], $uid);

            return response()->json([
                'status' => true,
                'message' => "ได้รับ <span>" . $reward->package_name . "</span> <br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                'data' => $this->setEventInfo($uid)
            ]);
        } else {
            $this->updateItemHistoryLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
            ], $sendItemLog['id'], $uid);

            return response()->json([
                'status' => false,
                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                // 'data' => $eventInfo
            ]);
        }
    }

    public function getHistory(Request $request)
    {
        $type = $request->input('type');

        if (!$type) { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($type === 'open_box') {
            $histories = $this->getOpenBoxHistories($uid);
        }

        if ($type === 'claim_reward') {
            $histories = $this->getClaimRewardHistories($uid);
        }

        if ($type === 'claim_points') {
            $histories = $this->getClaimPointsHistories($uid);
        }

        return response()->json([
            'status' => true,
            'data' => [
                'histories' => $histories
            ]
        ]);
    }

    public function getOpenBoxHistories($uid)
    {
        return History::where('uid', $uid)->where('type', 'open_box')->orderBy('id', 'desc')->limit(15)->get()
            ->map(function ($obj) {

                $createdAt = strtotime($obj->created_at);

                return [
                    'reward_name' => $obj->reward_name,
                    'date' => date('d/m/Y', $createdAt),
                    'time' => date('H:i:s', $createdAt),
                ];
            });
    }

    public function getClaimRewardHistories($uid)
    {
        return History::where('uid', $uid)->where('type', 'claim_reward')->orderBy('id', 'desc')->limit(15)->get()
            ->map(function ($obj) {

                $createdAt = strtotime($obj->created_at);
                $rewardName = $obj->reward_name;

                return [
                    'reward_name' => $rewardName,
                    'date' => date('d/m/Y', $createdAt),
                    'time' => date('H:i:s', $createdAt),
                ];
            });
    }

    public function getClaimPointsHistories($uid)
    {
        return QuestDailyLog::where('uid', $uid)->where('claimed_count', '>', 0)
            ->where('claim_status', 'claimed')
            ->orderBy('id', 'desc')
            ->limit(15)
            ->get()
            ->map(function ($obj) {

                $createdAt = strtotime($obj->created_at);
                if ($obj->quest_type === 'daily_bosskill') {
                    $rewardName = "กำจัดบอส {$obj->quest_title} ดันเจี้ยน{$obj->quest_dungeon} ได้รับ {$obj->claimed_count} คะแนน";
                } else {
                    $rewardName = "{$obj->quest_title} ได้รับ {$obj->quest_points} คะแนน";
                }

                return [
                    'reward_name' => $rewardName,
                    'date' => date('d/m/Y', $createdAt),
                    'time' => date('H:i:s', $createdAt),
                ];
            });
    }
}
