<?php

    namespace App\Http\Controllers\Api\Test\piggy;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\Test\piggy\Member;
    use App\Models\Test\piggy\MemberBank;
    use App\Models\Test\piggy\Round;
    use App\Models\Test\piggy\ItemLog;
    use App\Models\Test\piggy\DeductLog;
    use App\Models\Test\piggy\Quest;
    use App\Models\Test\piggy\QuestDailyLog;
    use App\Models\Test\piggy\Material;
    use App\Models\Test\piggy\Reward;

    class IndexController extends BnsEventController{

        private $baseApi = 'http://api.apps.garena.in.th';
        private $startTime = '2020-02-07 00:00:00';
        private $endTime = '2020-03-20 23:59:59';

        private $userEvent = null;

        private $maxDiamondsPerRound = 37500;
        private $maxLootboxesPerRound = 50;

        private $diamondsDailyLimit = 7500;
        private $serviceCharge = 5000;

        private $diamonds_2000 = 2000;
        private $diamonds_1000 = 1000;
        private $diamonds_500 = 500;

        public function __construct(Request $request)
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getJwtUserData($request);
            $this->checkEventStatus();

        }

        private function checkEventStatus()
        {
            if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'no_permission',
                    'message' => 'Sorry, you do not have permission.',
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม',
                ]));
            }

            if ($this->checkIsMaintenance()) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์',
                ]));
            }

            if ($this->isLoggedIn()) {
                // check and create member
                if ($this->checkMember() == false) {
                    die(json_encode([
                        'status'  => false,
                        'type'    => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง',
                    ]));
                }
            } else {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'not_login',
                    'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน',
                ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip  = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData) {
                return true;
            } else {
                return false;
            }
        }

        private function is_accepted_ip()
        {
            $allow_ips   = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid      = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $arr = [
                    'uid'              => $uid,
                    'username'         => $username,
                    'ncid'             => $ncid,
                    'char_id'          => 0,
                    'char_name'        => '',
                    'world_id'         => 0,
                    'last_ip'          => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
                $this->userEvent = $resp;

            } else {
                $member = Member::where('uid', $uid)->first();
                if (empty($member->ncid)) {
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if (empty($ncid)) {
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
                $this->userEvent = $member;
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if (empty($_id)) {
                return false;
            }

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:PIGGY:ASSOCIATE_UID_WITH_NCID_'.$uid, 10, function () use ($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'user_info',
                    'uid'      => $uid,
                ]);
                $userNcInfo       = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                } else {
                    if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    } else {
                        return '';
                    }
                }

            });
        }

        private function doGetChar($uid, $ncid){//new fixed function
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i       = 1;
                    foreach ($resp->response as $key => $value) {
                        // if((int)$value->level >= 60 && (int)$value->mastery_level>=12){
                            $content[] = [
                                'id' => $i,
                                'char_id'   => $value->id,
                                'char_name' => $value->name,
                                'world_id' => $value->world_id,
                                'job' => $value->job,
                                'level' => $value->level,
                                'creation_time' => $value->creation_time,
                                'last_play_start' => $value->last_play_start,
                                'last_play_end' => $value->last_play_end,
                                'last_ip' => $value->last_ip,
                                'exp' => $value->exp,
                                'mastery_level' => $value->mastery_level,
                            ];
                        // }

                        $i++;
                    }
                    return $content;
                }
            }
            return null;
        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:PIGGY:CHARACTERS_'.$uid, 10, function () use ($ncid) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service'  => 'characters',
                    'user_id'  => $ncid,
                ]);
            });
        }

        public function selectCharacter(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'select_character') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }
            if ($decoded->has('id') == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (2)'
                ]);
            }

            $charKey = (int)$decoded['id'];

            $userData = $this->userData;
            $uid = $userData['uid'];

            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบข้อมูล'
                ]);
            }

            if($member->char_id>0){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถเปลี่ยนตัวละครได้'
                ]);
            }

            $mychar=$this->doGetChar($member->uid,$member->ncid);
            $myselect=collect($mychar)->where('id',$charKey)->first();
            if(count($myselect)==0){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบตัวละคร'
                ]);
            }

            $member->char_id   = (int)$myselect['char_id'];
            $member->char_name = $myselect['char_name'];
            $member->world_id       = $myselect['world_id'];
            $member->save();

            $eventInfo = $this->setEventInfo($uid);
            if($eventInfo['status']==false){
                return response()->json([
                        'status' => false,
                        'message'=>$eventInfo['message'],
                    ]);
            }

            return response()->json([
                        'status' => true,
                        'data' => $eventInfo
                    ]);

        }

        private function diamondBalance(int $uid) {
            return $this->post_api($this->baseApi, [
                        'key_name' => 'bns',
                        'service' => 'balance',
                        'uid' => $uid,
            ]);
        }

        private function setRemainDiamonds($uid){
            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return 0;
            }else{
                return intval($diamondsBalance->balance);
            }
        }

        private function setDiamondDeductData($deduct_diamond_amount=0)
        {
            if($deduct_diamond_amount==0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test deduct diamomds for garena BNS Piggy event.',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addDeductLog(array $arr) {
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductLog(array $arr, $logId) {
            if ($arr) {
                return DeductLog::where('id', $logId)->update($arr);
            }
            return null;
        }

        private function dosendItem(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name'             => 'bns',
                'service'              => 'send_item',
                'user_id'              => $ncid,
                'purchase_description' => 'Test sending item from garena events BNS Piggy event.',
                'goods'                => json_encode($goodsData),
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
        }

        public function getEventInfo(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);
            if($eventInfo['status']==false){
                return response()->json([
                        'status' => false,
                        'message'=>$eventInfo['message'],
                    ]);
            }

            return response()->json([
                'status' => true,
                'data' => $eventInfo
            ]);

        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return [
                    'status' => false,
                    'message' => 'ไม่พอข้อมูล'
                ];
            }

            $member = Member::where('uid', $uid)->first();

            if($member->char_id==0){

                $charData=$this->doGetChar($member->uid,$member->ncid);

                $content = [];
                foreach ($charData as $key => $value) {

                    $content[] = [
                        'id' => $value['id'],
                        'char_name' => $value['char_name'],
                    ];

                }
                return [
                    'status' => true,
                    'username'=>$member->username,
                    'character_name'=>'',
                    'selected_char'=>false,
                    'characters'=>$content,
                ];
            }

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');

            // get current round
            $currRound = $this->getCurrentRound($logDateTime);
            if(!isset($currRound->id) || empty($currRound->id)){
                return [
                    'status' => false,
                    'message' => 'ไม่อยู่ในช่วงเวลาร่วมกิจกรรม'
                ];
            }

            // check and get piggy bank round
            $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->first();
            if(!$memberBankQuery){
                // create member bank
                $memberBankCreate = new MemberBank;
                $memberBankCreate->uid = $member->uid;
                $memberBankCreate->username = $member->username;
                $memberBankCreate->ncid = $member->ncid;
                $memberBankCreate->char_id = $member->char_id;
                $memberBankCreate->char_name = $member->char_name;
                $memberBankCreate->round_id = $currRound->id;
                $memberBankCreate->diamonds_2000_count = 0;
                $memberBankCreate->diamonds_1000_count = 0;
                $memberBankCreate->diamonds_500_count = 0;
                $memberBankCreate->lootboxes = 0;
                $memberBankCreate->status = 'pending';
                $memberBankCreate->log_date = date('Y-m-d');
                $memberBankCreate->log_date_timestamp = time();
                $memberBankCreate->last_ip = $this->getIP();
                $memberBankCreate->save();

                $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->first();

            }

            $logDate = date('Y-m-d');

            $questListObject = $this->getQuestList();
            $questList = array();
            if(count($questListObject) > 0){
                foreach($questListObject as $questKey=>$questVal){
                    $questList[] = [
                        'quest_id' => intval($questVal->quest_id),
                        'quest_dungeon' => $questVal->quest_dungeon,
                        'quest_title' => $questVal->quest_title,
                        'image' => $questVal->image,
                        'diamonds' => intval($questVal->quest_diamonds),
                        'diamonds_text' => number_format($questVal->quest_diamonds),
                        'lootboxed' => intval($questVal->quest_lootboxed),
                    ];
                }
            }

            $completedQuestCount = 0;
            $selectedQuestIdList = [];
            $selectedQuestListQuery = QuestDailyLog::where('uid', $member->uid)->where('log_date', $logDate)->orderBy('id', 'ASC')->get();
            $selectedQuestList = array();
            $isDailySelected = false;
            if(count($selectedQuestListQuery) > 0){
                $isDailySelected = true;
                foreach($selectedQuestListQuery as $quest){
                    $selectedQuestList[] = [
                        'quest_id' => intval($quest->quest_id),
                        'quest_dungeon' => $quest->quest_dungeon,
                        'quest_title' => $quest->quest_title,
                        'image' => $quest->image,
                        'diamonds' => intval($quest->quest_diamonds),
                        'diamonds_text' => number_format($quest->quest_diamonds),
                        'lootboxed' => intval($quest->quest_lootboxed),
                        'status' => $quest->status,
                        'claim_status' => $quest->claim_status
                    ];

                    array_push($selectedQuestIdList, $quest->quest_id);

                    if($quest->status=='success'){
                        $completedQuestCount++;
                    }
                }
            }

            // set bank materials
            $bankMaterialsQuery = Material::orderBy('id', 'ASC')->get();
            $bankMaterials = [];
            if(count($bankMaterialsQuery) > 0){
                foreach($bankMaterialsQuery as $keyMat=>$valMat){
                    $quantity = 0;
                    if($memberBankQuery->lootboxes > 0){
                        $quantity = $valMat->quantity * $memberBankQuery->lootboxes;
                        if($quantity > $valMat->limit){
                            $quantity = $valMat->limit;
                        }
                    }
                    $bankMaterials[$keyMat] = [
                        'title' => $valMat->title,
                        'quantity' => intval($quantity),
                        'limit' => intval($valMat->limit),
                        'image' => $valMat->image,
                    ];
                }
            }

            // total round diamonds
            $totalRoundDiamonds = 0;
            $totalRoundDiamonds = ($this->diamonds_2000 * $memberBankQuery->diamonds_2000_count) + ($this->diamonds_1000 * $memberBankQuery->diamonds_1000_count) + ($this->diamonds_500 * $memberBankQuery->diamonds_500_count);

            return [
                'status' => true,
                "message" => 'Event Info',
                "end_round_time" => $currRound->end_datetime,
                'username'=>$member->username,
                'character_name'=>$member->char_name,
                'selected_char'=>true,
                'characters'=>[],
                'quests' => $questList,
                'max_round_diamonds' => intval($this->maxDiamondsPerRound),
                'max_daily_diamonds' => $this->getMaxDiamondsDaily($totalRoundDiamonds),
                'max_round_lootboxes' => intval($this->maxLootboxesPerRound),
                'is_select_quests' => $isDailySelected,
                'selected_quests' => $selectedQuestList,
                'select_quests_id' => $selectedQuestIdList,
                'completed_select_quest_count' => $completedQuestCount,
                'selected_quests_count' => count($selectedQuestList),
                'can_claim_piggy_status' => $this->setCanClaimBankStatus($memberBankQuery->status,$totalRoundDiamonds,$memberBankQuery->lootboxes),
                'claimed_piggy_status' => $this->setClaimedBankStatus($memberBankQuery->status),
                'bank_list' => [
                    'diamonds' => intval($totalRoundDiamonds),
                    'diamonds_text' => number_format($totalRoundDiamonds),
                    'lootboxes' => intval($memberBankQuery->lootboxes),
                    'materials' => $bankMaterials,
                ],
            ];

        }

        private function getMaxDiamondsDaily($currDiamonds=0){
            $remainDiamonds = $this->maxDiamondsPerRound - $currDiamonds;
            if($remainDiamonds < $this->diamondsDailyLimit){
                return intval($remainDiamonds);
            }else{
                return intval($this->diamondsDailyLimit);
            }
        }

        private function getCurrentRound($logDateTime=''){
            if(empty($logDateTime)){
                return false;
            }

            return Round::where('start_datetime', '<=', $logDateTime)->where('end_datetime', '>=', $logDateTime)->where('status', 1)->first();
        }

        private function setCanClaimBankStatus($completedStatus="pending",$diamonds=0,$lootboxes=0){
            if($completedStatus=="pending" && $diamonds > 0 && $lootboxes > 0){
                return true;
            }else{
                return false;
            }
            return false;
        }

        private function setClaimedBankStatus($completedStatus="pending"){
            if($completedStatus == "pending"){
                return false;
            }else if($completedStatus == "claimed"){
                return true;
            }
            return false;
        }

        private function getQuestList(){
            return Cache::remember('BNS:PIGGY:QUESTS', 30, function () {
                return Quest::select('quest_dungeon','quest_title','quest_id','quest_code','quest_diamonds','quest_lootboxed','image')->orderBy('quest_id', 'ASC')->get();
            });
        }

        public function selectDailyQuest(Request $request){
            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'select_quests') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }
            if ($decoded->has('selected_list') == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (2)'
                ]);
            }

            $selected_list = $decoded['selected_list'];

            // dd($selected_list);

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $member = Member::where('uid', $uid)->first();

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimestamp = time();

            // check today is already selected quests.
            $todaySelected = QuestDailyLog::where('uid', $member->uid)->where('log_date', $logDate)->count();
            if($todaySelected > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณได้เลือกเควสสำหรับวันนี้ไปแล้ว'
                ]);
            }

            // get current round
            $currRound = $this->getCurrentRound($logDateTime);
            if(!isset($currRound->id) || empty($currRound->id)){
                return [
                    'status' => false,
                    'message' => 'ไม่อยู่ในช่วงเวลาร่วมกิจกรรม'
                ];
            }

            // get piggy bank round
            // $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->where('status', 'pending')->first();
            $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->first();
            if(!$memberBankQuery){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่มีข้อมูลของกระปุกหมู'
                ]);
            }
            if($memberBankQuery->status == 'claimed'){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณทุบกระปุกหมูในรอบนี้ไปแล้ว'
                ]);
            }
            if($memberBankQuery->status == 'rejected'){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />หมดเวลาทุบกระปุกหมูในรอบนี้'
                ]);
            }

            // check quest diamonds
            $questDiamondsSum = Quest::whereIn('quest_id', $selected_list)->sum('quest_diamonds');
            $questDiamonds = intval($questDiamondsSum);
            if($questDiamonds > $this->diamondsDailyLimit){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />เควสที่เลือกมีจำนวนไดมอนด์เกินกำหนดต่อวัน'
                ]);
            }

            $questList = $this->getQuestList();

            // create selected quest log
            if(count($selected_list) > 0){
                for($i=0;$i<count($selected_list);$i++){
                    foreach($questList as $questKey=>$questVal){
                        if($questVal->quest_id == $selected_list[$i]){
                            QuestDailyLog::create([
                                'uid' => $member->uid,
                                'username' => $member->username,
                                'ncid' => $member->ncid,
                                'char_id' => $member->char_id,
                                'char_name' => $member->char_name,
                                'round_id' => $memberBankQuery->round_id,
                                'quest_id' => $questVal->quest_id,
                                'quest_dungeon' => $questVal->quest_dungeon,
                                'quest_title' => $questVal->quest_title,
                                'quest_code' => $questVal->quest_code,
                                'quest_diamonds' => $questVal->quest_diamonds,
                                'quest_lootboxed' => $questVal->quest_lootboxed,
                                'image' => $questVal->image,
                                'status' => 'pending',
                                'claim_status' => 'pending',
                                'log_date' => date('Y-m-d'),
                                'log_date_timestamp' => time(),
                                'last_ip' => $this->getIP(),
                            ]);
                        }
                    }
                }
            }

            // set event info
            $eventInfo = $this->setEventInfo($member->uid);
            if($eventInfo['status']==false){
                return response()->json([
                        'status' => false,
                        'message'=>$eventInfo['message'],
                    ]);
            }

            return response()->json([
                'status' => true,
                'data' => $eventInfo
            ]);

        }

        public function claimDailyQuest(Request $request){
            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'claim_daily_quest') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }
            if ($decoded->has('quest_id') == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (2)'
                ]);
            }

            $quest_id = $decoded['quest_id'];

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $member = Member::where('uid', $uid)->first();

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimestamp = time();

            // get current round
            $currRound = $this->getCurrentRound($logDateTime);
            if(!isset($currRound->id) || empty($currRound->id)){
                return [
                    'status' => false,
                    'message' => 'ไม่อยู่ในช่วงเวลาร่วมกิจกรรม'
                ];
            }

            // get piggy bank round
            // $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->where('status', 'pending')->first();
            $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->first();
            if(!$memberBankQuery){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่มีข้อมูลของกระปุกหมู'
                ]);
            }
            if($memberBankQuery->status == 'claimed'){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณทุบกระปุกหมูในรอบนี้ไปแล้ว'
                ]);
            }
            if($memberBankQuery->status == 'rejected'){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />หมดเวลาทุบกระปุกหมูในรอบนี้'
                ]);
            }

            // get daily quest log
            // $dailyQuest = QuestDailyLog::where('uid', $member->uid)->where('log_date', $logDate)->where('quest_id', $quest_id)->where('status', 'success')->where('claim_status', 'pending')->first();
            $dailyQuest = QuestDailyLog::where('uid', $member->uid)->where('log_date', $logDate)->where('quest_id', $quest_id)->first();
            if(!$dailyQuest){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่มีข้อมูลเควส'
                ]);
            }
            if($dailyQuest->status == 'pending'){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />เควสที่เลือกยังไม่สำเร็จ'
                ]);
            }
            if($dailyQuest->claim_status == 'claimed'){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ได้รับของรางวัลจากเควสนี้ไปแล้ว'
                ]);
            }

            // update daily status after claim rewards to piggy bank
            $dailyQuest->claim_status = 'claimed';
            $dailyQuest->last_ip = $this->getIP();

            if($dailyQuest->save()){

                // add reward to piggy bank
                // $memberBankQuery->diamonds = $memberBankQuery->diamonds + $dailyQuest->quest_diamonds;

                if($dailyQuest->quest_diamonds == 2000){
                    $memberBankQuery->diamonds_2000_count = $memberBankQuery->diamonds_2000_count + 1;
                }
                if($dailyQuest->quest_diamonds == 1000){
                    $memberBankQuery->diamonds_1000_count = $memberBankQuery->diamonds_1000_count + 1;
                }
                if($dailyQuest->quest_diamonds == 500){
                    $memberBankQuery->diamonds_500_count = $memberBankQuery->diamonds_500_count + 1;
                }

                $memberBankQuery->lootboxes = $memberBankQuery->lootboxes + $dailyQuest->quest_lootboxed;
                $memberBankQuery->last_ip = $this->getIP();

                if($memberBankQuery->save()){

                    // set event info
                    $eventInfo = $this->setEventInfo($member->uid);
                    if($eventInfo['status']==false){
                        return response()->json([
                                'status' => false,
                                'message'=>$eventInfo['message'],
                            ]);
                    }

                    return response()->json([
                        'status' => true,
                        'message'=> 'หยอดกระปุกเสร็จเรียบร้อย',
                        'data' => $eventInfo
                    ]);

                }

            }

            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง'
            ]);
        }

        public function claimPiggyBank(Request $request){
            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'claim_piggy_bank') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $member = Member::where('uid', $uid)->first();

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimestamp = time();

            // get current round
            $currRound = $this->getCurrentRound($logDateTime);
            if(!isset($currRound->id) || empty($currRound->id)){
                return [
                    'status' => false,
                    'message' => 'ไม่อยู่ในช่วงเวลาร่วมกิจกรรม'
                ];
            }

            // get piggy bank round
            // $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->where('status', 'pending')->first();
            $memberBankQuery = MemberBank::where('uid', $member->uid)->where('round_id', $currRound->id)->first();
            if(!$memberBankQuery){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่มีข้อมูลของกระปุกหมู'
                ]);
            }
            if($memberBankQuery->status == 'claimed'){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />คุณทุบกระปุกหมูในรอบนี้ไปแล้ว'
                ]);
            }
            if($memberBankQuery->status == 'rejected'){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />หมดเวลาทุบกระปุกหมูในรอบนี้'
                ]);
            }
            $totalRoundDiamonds = 0;
            $totalRoundDiamonds = ($this->diamonds_2000 * $memberBankQuery->diamonds_2000_count) + ($this->diamonds_1000 * $memberBankQuery->diamonds_1000_count) + ($this->diamonds_500 * $memberBankQuery->diamonds_500_count);
            if($this->setCanClaimBankStatus($memberBankQuery->status,$totalRoundDiamonds,$memberBankQuery->lootboxes) == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถทุบกระปุกหมูได้<br />เนื่องจากยังไม่มีจำนวนของรางวัลสะสม'
                ]);
            }

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            if($diamonds < $this->serviceCharge){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณไม่พอทุบกระปุก'
                ]);
            }

            // deduct diamonds for service charge
            // set deduct diamonds
            $logDeduct = $this->addDeductLog([
                'uid'                       => $member->uid,
                'username'                  => $member->username,
                'ncid'                      => $member->ncid,
                'round_id'                  => $memberBankQuery->round_id,
                'diamonds'                  => $this->serviceCharge,
                'status'                    => 'pending',
                'log_date'                  => date('Y-m-d H:i:s'), // set to string
                'log_date_timestamp'        => time(),
                'last_ip'                   => $this->getIP(),
            ]);

            // set deduct diamonds
            $deductData = $this->setDiamondDeductData($this->serviceCharge);
            $respDeductRaw = $this->deductDiamond($ncid, $deductData);
            $respDeduct = json_decode($respDeductRaw);
            if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){

                // update deduct log
                $arrDeductDiamond = [
                    'before_deduct_diamond' => $diamonds,
                    'after_deduct_diamond' => $diamonds - $this->serviceCharge,
                    'deduct_status' => $respDeduct->status ?: 0,
                    'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
                    'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
                    'deduct_data' => json_encode($deductData),
                    'status' => 'success'
                ];

                $respDeduct = $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);

                if($respDeduct){

                    // update member bank log to claimed status
                    $memberBankQuery->status = 'claimed';
                    $memberBankQuery->last_ip = $this->getIP();
                    $memberBankQuery->updated_at = date('Y-m-d H:i:s');
                    $memberBankQuery->save();

                    // get rewards
                    $rewardsQuery = Reward::where('available_date', '<=', $logDateTime)->get();
                    if(count($rewardsQuery) <= 0){
                        return response()->json([
                            'status' => false,
                            'message' => 'ขออภัย<br />ไม่มีข้อมูลของรางวัล',
                        ]);
                    }

                    $productList = [];
                    $goods_data = []; //good data for send item group
                    foreach ($rewardsQuery as $key => $reward) {
                        $quantity = 1;
                        if($reward->package_type == 'diamonds'){
                            if($reward->package_key == 1){ // 2000 diamonds
                                $quantity = $memberBankQuery->diamonds_2000_count;
                            }
                            if($reward->package_key == 2){ // 1000 diamonds
                                $quantity = $memberBankQuery->diamonds_1000_count;
                            }
                            if($reward->package_key == 3){ // 500 diamonds
                                $quantity = $memberBankQuery->diamonds_500_count;
                            }

                        }elseif($reward->package_type == 'materials'){
                            $quantity = $memberBankQuery->lootboxes;
                        }

                        $goods_data[] = [
                            'goods_id' => $reward->package_id,
                            'purchase_quantity' => $quantity,
                            'purchase_amount' => 0,
                            'category_id' => 40,
                        ];

                        $productList[] = [
                            'title' => $reward->package_name,
                            'quantity' => $quantity,
                        ];

                    }

                    $sendItemLog = $this->addItemHistoryLog([
                        'uid'                           => $member->uid,
                        'username'                      => $member->username,
                        'ncid'                          => $member->ncid,
                        'deduct_id'                     => $logDeduct['id'],
                        'round_id'                      => $memberBankQuery->round_id,
                        'product_set'                   => json_encode($productList),
                        'status'                        => 'pending',
                        'send_item_status'              => false,
                        'send_item_purchase_id'         => 0,
                        'send_item_purchase_status'     => 0,
                        'goods_data'                    => json_encode($goods_data),
                        'log_date'                      => (string)date('Y-m-d'), // set to string
                        'log_date_timestamp'            => time(),
                        'last_ip'                       => $this->getIP(),
                    ]);

                    $send_result_raw = $this->dosendItem($ncid, $goods_data);
                    $send_result = json_decode($send_result_raw);
                    if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                        // update send item log
                        $this->updateItemHistoryLog([
                            'send_item_status' => $send_result->status ?: false,
                            'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                            'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                            'status' => 'success'
                                ], $sendItemLog['id'], $uid);

                        $eventInfo = $this->setEventInfo($uid);

                        return response()->json([
                                    'status' => true,
                                    'message' => "ได้รับ ".number_format($totalRoundDiamonds)." ไดมอนด์<br />และกล่องเสบียงน้องหมู ".$memberBankQuery->lootboxes." กล่อง<br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุราเข้าเกมเพื่อกดรับ",
                                    'data' => $eventInfo
                                ]);
                    }else{
                        $this->updateItemHistoryLog([
                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                            'status' => 'unsuccess'
                                ], $sendItemLog['id'], $uid);

                        // $eventInfo = $this->setEventInfo($uid);

                        return response()->json([
                                    'status' => false,
                                    'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                                    // 'data' => $eventInfo
                                ]);
                    }

                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถสั่งซื้อแพ็คเกจได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }


            }else{
                $arrDeductDiamond['status'] = 'unsuccess';
                $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);
            }

            return response()->json([
                        'status' => false,
                        'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
            ]);

        }

        public function getHistory(Request $request){
            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'history') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            $member = Member::where('uid', $uid)->first();

            $logDate = date('Y-m-d');
            $logDateTime = date('Y-m-d H:i:s');
            $logDateTimestamp = time();

            // get current round
            $currRound = $this->getCurrentRound($logDateTime);
            if(!isset($currRound->id) || empty($currRound->id)){
                return [
                    'status' => false,
                    'message' => 'ไม่อยู่ในช่วงเวลาร่วมกิจกรรม'
                ];
            }

            $dailyQuests = [];
            $claimQuests = [];
            $claimPiggyBank = [];

            $piggyBankQuery = MemberBank::where('uid', $member->uid)->orderBy('log_date_timestamp', 'DESC')->get();
            if(count($piggyBankQuery) > 0){
                foreach($piggyBankQuery as $piggyBank){
                    $roundInfo = Round::where('id', $piggyBank->round_id)->first();
                    $claimPiggyBank[] = [
                        'round' => date('d/m/Y', strtotime($roundInfo->start_datetime)) . ' ถึง ' . date('d/m/Y', strtotime($roundInfo->end_datetime)),
                        'status' => $this->setMemberBankStatus($currRound->id,$piggyBank->round_id,$piggyBank->status),
                    ];
                }
            }

            // daily quest gistory
            $dailyQuestDateGroupQuery = QuestDailyLog::where('uid', $member->uid)->orderBy('log_date_timestamp', 'DESC')->groupBy('log_date')->get();
            if(count($dailyQuestDateGroupQuery) > 0){
                foreach($dailyQuestDateGroupQuery as $dailyQuestDateGroup){

                    $dailyQuestList = [];
                    $claimQuestList = [];
                    $dailyQuestQuery = null;
                    $dailyQuestQuery = QuestDailyLog::where('uid', $member->uid)->where('log_date', $dailyQuestDateGroup->log_date)->orderBy('quest_id', 'ASC')->get();
                    if(count($dailyQuestQuery) > 0){
                        foreach($dailyQuestQuery as $dailyQuest){

                            // daily history
                            $dailyQuestList[] = [
                                'quest_title' => $dailyQuest->quest_title.' ('.$dailyQuest->quest_dungeon.')',
                                'status' => $this->setDailyQuestStatus($currRound->id,$dailyQuest->round_id,$dailyQuest->status),
                                'completed_time' => date('H:i:s', strtotime($dailyQuest->quest_complete_datetime)),
                            ];

                            // claim history
                            if($dailyQuest->claim_status == 'claimed'){
                                $claimQuestList[] = [
                                    'title' => 'หยอดกระปุก '. number_format($dailyQuest->quest_diamonds) .' ไดมอนด์ และ '. $dailyQuest->quest_lootboxed .' กล่องสมบัติน้อหมู',
                                    'completed_time' => date('H:i:s', strtotime($dailyQuest->quest_complete_datetime)),
                                ];
                            }

                        }
                    }

                    $dailyQuests[] = [
                        'date' => date('d/m/Y', strtotime($dailyQuestDateGroup->log_date)),
                        'quest_list' => $dailyQuestList,
                    ];

                    $claimQuests[] = [
                        'date' => date('d/m/Y', strtotime($dailyQuestDateGroup->log_date)),
                        'quest_list' => $claimQuestList,
                    ];

                }
            }

            return response()->json([
                'status' => true,
                'message' => 'History',
                'data' => [
                    'username'=>$member->username,
                    'character_name'=>$member->char_name,
                    'selected_char'=>$member->char_name != "" ? true: false,
                    'characters'=>[],
                    'daily_quests' => $dailyQuests,
                    'claim_quests' => $claimQuests,
                    'claim_bank' => $claimPiggyBank,
                ]
            ]);

        }

        private function setMemberBankStatus($currRound, $memberRound, $status=''){
            if($status == 'pending' && $currRound == $memberRound){
                return 'กำลังดำเนินการ';
            }
            if(($status == 'claimed' && $currRound == $memberRound) || ($status == 'claimed' && $currRound != $memberRound)){
                return 'ทุบกระปุกแล้ว';
            }
            if($status == 'pending' && $currRound != $memberRound){
                return 'ไม่สำเร็จ';
            }

            return 'ไม่สำเร็จ';
        }

        private function setDailyQuestStatus($currRound, $memberRound, $status=''){
            if($status == 'pending' && $currRound == $memberRound){
                return 'กำลังดำเนินการ';
            }
            if($status == 'success' && $currRound == $memberRound){
                return 'ภารกิจสำเร็จ';
            }
            if($status == 'pending' && $currRound != $memberRound){
                return 'ภารกิจไม่สำเร็จ';
            }

            return 'ภารกิจไม่สำเร็จ';
        }

    }
