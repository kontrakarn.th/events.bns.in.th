<?php

    namespace App\Http\Controllers\Api\Test\hm_welfare_package;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;

    use App\Models\Test\hm_welfare_package\Member;
    use App\Models\Test\hm_welfare_package\Bookinglog;
    use App\Models\Test\hm_welfare_package\Deductlog;
    use App\Models\Test\hm_welfare_package\Purchaselog;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2019-10-30 12:00:00';
        private $endTime = '2019-11-13 23:59:59';

        private const PURCHASE_DIAMONDS = [
            50000,
            80000,
            125000
        ];

        private const PACKAGE = [
            'package_id' => 1,
            'package_title' => 'แพ็คเกจ Hongmoon Welfare x1',
            'package_desc' => 'แพ็คเกจ Hongmoon Welfare x1',
            'product_set' => [
                [
                    'id' => 1,
                    'product_title' => 'แพ็คเกจ Hongmoon Welfare x1',
                    'product_id' => 2689,
                    'product_quantity' => 1
                ],
            ]
        ];

        private const MESSAGES = [
            'DEFAULT' => 'เกิดข้อผิดพลาด<br />กรุณาลองใหม่อีกครั้ง',
            'NO_PERMISSION' => 'Sorry, you do not have permission.',
            'END_EVENT' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม',
            'MAINTENANCE' => 'ขออภัย<br />ระบบกำลังปิดปรับปรุงประจำสัปดาห์',
            'NO_GAME_INFO' => 'ขออภัย<br />กรุณาล็อคอินเข้าเกมเพื่อสร้างตัวละคร<br />ก่อนร่วมกิจกรรม',
            'NO_LOGIN' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน',
            'INVALID_PARAM' => 'Invalid Parameter!',
            'NO_DATA' => 'No member data.',
            'IS_BOOKING' => 'คุณได้ทำการจองไปแล้ว',
            'NO_ENOUGH_DIAMONDS' => 'ขออภัย<br />ไม่สามารถสั่งซื้อแพ็คเกจได้<br />เนื่องจากไดมอนด์ไม่พอ',
            'BOOKING_SUCCESS' => 'จองสำเร็จ',
            'PURCHASE_SUCCESS' => 'ได้รับ แพ็คเกจ Hongmoon Welfare x1<br/>ไอเทมส่งเข้าไปในกล่องจดหมาย<br/>กรุณาเข้าเกมเพื่อรับของรางวัล',
            'PURCHASE_UNSUCCESS' => 'ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า',
            'DEDUCT_FAIL' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
        ];

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            if($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => self::MESSAGES['NO_PERMISSION']
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => self::MESSAGES['END_EVENT']
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => self::MESSAGES['MAINTENANCE']
                ]));
            }

            if($this->isLoggedIn()){

                // check member has game info
                $uid = (int)$this->userData['uid'];
                $username = $this->userData['username'];
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    die(json_encode([
                        'status' => false,
                        'type' => 'no_game_info',
                        'message' => self::MESSAGES['NO_GAME_INFO']
                    ]));
                }

                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => self::MESSAGES['DEFAULT']
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => self::MESSAGES['NO_LOGIN']
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = (int)$this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $diamonds = $this->setRemainDiamonds($uid);
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'diamonds' => (int)$diamonds,
                    'user_type' => (int)$this->setUserType($diamonds),
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(!$member) {
                    return false;
                }
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $member->ncid = $ncid;
                }
                // check purchased
                $diamonds = $this->setRemainDiamonds($uid);
                $member->diamonds = (int)$diamonds;
                // check user type
                if($member->booking_type == 0) {
                    $member->user_type = (int)$this->setUserType($diamonds);
                }
                $member->save();
            }
            return true;
        }

        private function setUserType($diamonds)
        {
            $type = 3;
            if($diamonds >= 100000 && $diamonds <= 199999) {
                $type = 2;
            } elseif($diamonds >= 0 && $diamonds <= 99999) {
                $type = 1;
            }

            return $type;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            return $counter > 0 ? true : false;
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:HM_WELFARE_PACKAGE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function diamondBalance(int $uid) {
            return $this->post_api($this->baseApi, [
                        'key_name' => 'bns',
                        'service' => 'balance',
                        'uid' => $uid,
            ]);
        }

        private function setRemainDiamonds($uid){
            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return 0;
            }else{
                return intval($diamondsBalance->balance);
            }
        }

        private function setDiamondDeductData($deduct_diamond_amount=0)
        {
            if($deduct_diamond_amount==0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamomds for garena BNS Hongmoon Welfare Package.',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addDeductlog(array $arr) {
            if ($arr) {
                $resp = Deductlog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductlog(array $arr, $logId) {
            if ($arr) {
                return Deductlog::where('id', $logId)->update($arr);
            }
            return null;
        }

        private function dosendItem(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Sending item from garena events BNS Hongmoon Welfare Package.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return Purchaselog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return Purchaselog::where('id', $log_id)->where('uid', $uid)->update($arr);
        }

        public function getEventInfo(Request $request)
        {
            
            if ($request->has('type') == false && $request->type != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => self::MESSAGES['INVALID_PARAM']
                ]);
            }

            $userData = $this->userData;
            $uid = (int)$userData['uid'];

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(!$member){
                return response()->json([
                            'status' => false,
                            'message' => self::MESSAGES['NO_DATA']
                ]);
            }

            return response()->json([
                'status' => true,
                'data' => [
                    'username' => $member->username,
                    'user_type' => (int)$member->user_type,
                    'booking_type' => (int)$member->booking_type,
                    'purchased' => (int)$member->purchased,
                ]
            ]);

        }

        public function bookingPackage(Request $request) 
        {

            if ($request->has('type') == false && $request->type != 'booking_package') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => self::MESSAGES['INVALID_PARAM']
                ]);
            }

            if ($request->has('booking_type') == false || !in_array($request->booking_type, [1,2,3])) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => self::MESSAGES['INVALID_PARAM']
                ]);
            }

            $userData = $this->userData;
            $uid = (int)$userData['uid'];

            $bookingType = (int)$request->booking_type;

            // check member
            $member = Member::where('uid', $uid)->first();
            if(!$member){
                return response()->json([
                    'status' => false,
                    'message' => self::MESSAGES['NO_DATA']
                ]);
            }

            // check booking log
            $bookinglog = Bookinglog::where('uid', $uid)->first();
            if($bookinglog) {
                $member->booking_type = $bookinglog->type;
                $member->save();
                return response()->json([
                    'status' => false,
                    'message' => self::MESSAGES['IS_BOOKING']
                ]);
            }

            // check condition
            if(
                ($member->user_type == 1 && ($bookingType == 1 || $bookingType == 2 || $bookingType == 3)) ||
                ($member->user_type == 2 && ($bookingType == 2 || $bookingType == 3)) ||
                ($member->user_type == 3 && $bookingType == 3)
            );
            else {
                return response()->json([
                    'status' => false,
                    'message' => self::MESSAGES['DEFAULT']
                ]);
            }

            // check diamonds
            // if($member->diamonds < self::PURCHASE_DIAMONDS[$bookingType - 1]) {
            //     return response()->json([
            //         'status' => false,
            //         'message' => self::MESSAGES['NO_ENOUGH_DIAMONDS']
            //     ]);
            // }

            // insert bookinglog
            $insert = new Bookinglog;
            $insert->uid = $member->uid;
            $insert->ncid = $member->ncid;
            $insert->diamonds = $member->diamonds;
            $insert->type = $bookingType;
            $insert->save();

            // update member
            $member->booking_type = $bookingType;
            $member->save();

            return response()->json([
                'status' => true,
                'message' => self::MESSAGES['BOOKING_SUCCESS'],
                'data' => [
                    'diamonds' => number_format($member->diamonds),
                    'booking_type' => (int)$bookingType,
                    'purchased' => 0,
                ]
            ]);

        }

        public function purchasePackage(Request $request) 
        {

            if ($request->has('type') == false && $request->type != 'purchase_package') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => self::MESSAGES['INVALID_PARAM']
                ]);
            }

            $userData = $this->userData;
            $uid = (int)$userData['uid'];

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(!$member){
                return response()->json([
                    'status' => false,
                    'message' => self::MESSAGES['NO_DATA']
                ]);
            }

            // get booking
            $bookingType = (int)$member->booking_type;
            if($bookingType == 0){
                return response()->json([
                    'status' => false,
                    'message' => self::MESSAGES['DEFAULT']
                ]);
            }

            // check diamonds
            $packagePrice = self::PURCHASE_DIAMONDS[$bookingType - 1];
            if($member->diamonds < $packagePrice) {
                return response()->json([
                    'status' => false,
                    'message' => self::MESSAGES['NO_ENOUGH_DIAMONDS']
                ]);
            }

            $package = self::PACKAGE;

            // set deduct diamonds
            $logDeduct = $this->addDeductlog([
                'uid'                       => $member->uid,
                'username'                  => $member->username,
                'ncid'                      => $member->ncid,
                'diamonds'                  => $packagePrice,
                'package_id'                => $package['package_id'],
                'package_title'             => $package['package_title'],
                'status'                    => 'pending',
                'log_date'                  => date('Y-m-d H:i:s'),
                'log_date_timestamp'        => time(),
                'last_ip'                   => $this->getIP(),
            ]);

            // set deduct diamonds
            $deductData = $this->setDiamondDeductData($packagePrice);
            $respDeductRaw = $this->deductDiamond($member->ncid, $deductData);
            $respDeduct = json_decode($respDeductRaw);
            if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){

                // update deduct log
                $arrDeductDiamond = [
                    'before_deduct_diamond' => $member->diamonds,
                    'after_deduct_diamond' => $member->diamonds - $packagePrice,
                    'deduct_status' => $respDeduct->status ?: 0,
                    'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
                    'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
                    'deduct_data' => json_encode($deductData),
                    'status' => 'success'
                ];

                $respDeduct = $this->updateDeductlog($arrDeductDiamond, $logDeduct['id']);
                
                if($respDeduct){

                    // set send item logs
                    $goods_data = []; //good data for send item group

                    $productList = $package['product_set'];

                    foreach ($productList as $key => $product) {
                        $goods_data[] = [
                            'goods_id' => $product['product_id'],
                            'purchase_quantity' => $product['product_quantity'],
                            'purchase_amount' => 0,
                            'category_id' => 40
                        ];
                    }

                    $sendPurchaselog = $this->addItemHistoryLog([
                        'uid' => $uid,
                        'ncid' => $member->ncid,
                        'deduct_id' => $logDeduct['id'],
                        'package_id' => $package['package_id'],
                        'package_title' => $package['package_title'],
                        'require_diamonds' => $packagePrice,
                        'product_set' => json_encode($productList),
                        'status' => 'pending',
                        'send_item_status' => false,
                        'send_item_purchase_id' => 0,
                        'send_item_purchase_status' => 0,
                        'goods_data' => json_encode($goods_data),
                        'last_ip' => $this->getIP(),
                        'log_date' => (string)date('Y-m-d'), // set to string
                        'log_date_timestamp' => time(),
                    ]);

                    $send_result_raw = $this->dosendItem($member->ncid, $goods_data);
                    $send_result = json_decode($send_result_raw);
                    if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                        $this->updateItemHistoryLog([
                            'send_item_status' => $send_result->status ?: false,
                            'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                            'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                            'status' => 'success'
                                ], $sendPurchaselog['id'], $uid);

                        $member->purchased = 1;
                        $member->save();

                        return response()->json([
                            'status' => true,
                            'message' => self::MESSAGES['PURCHASE_SUCCESS'],
                            'data' => [
                                'purchased' => (int)$member->purchased,
                            ]
                        ]);

                    }else{
                        $this->updateItemHistoryLog([
                            'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                            'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                            'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                            'status' => 'unsuccess'
                                ], $sendPurchaselog['id'], $uid);

                        return response()->json([
                                    'status' => false,
                                    'message' => self::MESSAGES['PURCHASE_UNSUCCESS'],
                                ]);
                    }

                }else{
                    return response()->json([
                        'status' => false,
                        'message' => self::MESSAGES['PURCHASE_UNSUCCESS'],
                    ]);
                }


            }else{
                $arrDeductDiamond['status'] = 'unsuccess';
                $this->updateDeductlog($arrDeductDiamond, $logDeduct['id']);
            }

            return response()->json([
                'status' => false,
                'message' => self::MESSAGES['DEDUCT_FAIL'],
            ]);

        }

    }