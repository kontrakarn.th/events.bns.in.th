<?php

    namespace App\Http\Controllers\Api\Test\archer_support;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DB;

    use App\Models\Test\archer_support\Member;
    use App\Models\Test\archer_support\QuestLogs;
    use App\Models\Test\archer_support\ItemLog;

    class IndexController extends BnsEventController{

    	private $baseApi = 'http://api.apps.garena.in.th';
        private $startTime = '2019-09-16 00:00:00';
        private $endTime = '2019-10-23 23:59:50';

        private $jobRequire=8;

        private $questList = [
            [
                'step'=>1,
                'quest'=>[
                    [
                        'id'=>1556,
                        'name'=>"เขตต้องห้ามแห่งทะเลทรายดับแสง"
                    ],
                    [
                        'id'=>1793,
                        'name'=>"แหล่งกำเนิดทัพปีศาจ"
                    ],
                    [
                        'id'=>1671,
                        'name'=>"เมืองโบราณพรรณไม้"
                    ],
                    [
                        'id'=>1673,
                        'name'=>"จดหมายของผู้ใหญ่บ้าน หมู่บ้านโฮอุน"
                    ],
                    [
                        'id'=>1695,
                        'name'=>"ปรากฏการณ์อัศจรรย์ที่ชายหาดไข่มุกคราม"
                    ],
                    [
                        'id'=>1734,
                        'name'=>"กรุสมบัตินาริว"
                    ],
                    [
                        'id'=>1743,
                        'name'=>"กลุ่มการค้าแห่งวิกฤต"
                    ],
                    [
                        'id'=>1789,
                        'name'=>"เวทีที่อยู่ด้านหลังเงามืด"
                    ],
                    [
                        'id'=>1055,
                        'name'=>"ชีวิตชั้นสูง"
                    ]
                ],
                'item'=>[
                    [
                        'id' => 1,
                        'product_title' => 'Archer Support ขั้น 1',
                        'product_id' => 2579,
                        'product_quantity' => 1,
                        'amount' => 1,
                    ],
                ]
            ],
            [
                'step'=>2,
                'quest'=>[
                    [
                        'id'=>324,
                        'name'=>"คำเชิญเข้าร่วมทัวร์นาเมนท์"
                    ],
                    [
                        'id'=>1328,
                        'name'=>"คำเชิญเข้าร่วมประลอง"
                    ],
                    [
                        'id'=>1625,
                        'name'=>"เบลูก้าลากูนเปิดแล้ว!"
                    ],
                    [
                        'id'=>1818,
                        'name'=>"บัตรเชิญเข้าร่วมสงคราม - เกาะแห่งพันธนาการ"
                    ],
                    [
                        'id'=>1667,
                        'name'=>"ข้ามขีดจำกัดของวิชาเท้าท่องนภา"
                    ],
                    [
                        'id'=>1691,
                        'name'=>"ดอกไม้บาน"
                    ],
                    [
                        'id'=>1897,
                        'name'=>"ดอกไม้แสนเศร้าที่เบ่งบานทางใต้"
                    ],
                    [
                        'id'=>1834,
                        'name'=>"ผืนแผ่นดินใหม่ เกาะตะวันพลบจันทร์ฉาย"
                    ],
                    [
                        'id'=>1780,
                        'name'=>"รังมังกรเพลิงที่ถูกผนึก"
                    ]
                ],
                'item'=>[
                    [
                        'id' => 2,
                        'product_title' => 'Archer Support ขั้น 2',
                        'product_id' => 2578,
                        'product_quantity' => 1,
                        'amount' => 1,
                    ],
                ]
            ],
            [
                'step'=>3,
                'quest'=>[
                    [
                        'id'=>1830,
                        // 'id'=>1781,
                        'name'=>"ผ่านเควสท์ บทที่ 10 ตอนที่ 10 สายใยของอาจารย์และศิษย์"
                    ],
                ],
                'item'=>[
                    [
                        'id' => 3,
                        'product_title' => 'Archer Support ขั้น 3-1',
                        'product_id' => 2580,
                        'product_quantity' => 1,
                        'amount' => 1,
                    ],
                    [
                        'id' => 4,
                        'product_title' => 'Archer Support ขั้น 3-2',
                        'product_id' => 2581,
                        'product_quantity' => 1,
                        'amount' => 1,
                    ],
                    [
                        'id' => 5,
                        'product_title' => 'Archer Support ขั้น 3-3',
                        'product_id' => 2582,
                        'product_quantity' => 1,
                        'amount' => 1,
                    ],
                ]
            ],
        ];

        public function __construct(Request $request)
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
             if($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }
            // dd($this->startTime,$this->endTime);
            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'character_id' => 0,
                    'character_name' => "",
                    'item_group_1' => false,
                    'item_group_2' => false,
                    'item_group_3' => false,
                    'created_at_string'=>Carbon::now()->toDateString(),
                    'last_ip' => $this->getIP()
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['_id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('_id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:ARCHER_SUPPORT:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events bns archer support.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function doGetChar($uid,$ncid){//new fixed function
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i =1;
                    foreach ($resp->response as $key => $value) {
                        $content[] = [
                            // 'id' => $i,
                            'char_id' => $value->id,
                            'char_name' => $value->name,
                            // 'world_id' => $value->world_id,
                            'job' => $value->job,
                            // 'level' => $value->level,
                            // 'creation_time' => $value->creation_time,
                            // 'last_play_start' => $value->last_play_start,
                            // 'last_play_end' => $value->last_play_end,
                            // 'last_ip' => $value->last_ip,
                            // 'exp' => $value->exp,
                            // 'mastery_level' => $value->mastery_level,
                        ];

                        $i++;
                    }
                    return $content;
                }
            }
            return null;
        }

        private function setQuestCompletedCount($uid, $charId, $questId, $start, $end){

            $completedCount = 0;

            $Count = $this->apiCheckQuestCompleted($uid, $charId, $questId, $start, $end);
            // dd($Count);
            if(isset($Count) && $Count->status){
                $completedCount = intval($Count->response->quest_completed_count);
            }

            return $completedCount;

        }

        private function calQuest($member, $start, $end,$group){
            $quest=$this->questList[$group-1];
            $quest_log=$quest['quest'];
            $num_current_quest=count($quest['quest']);
            $total_pass=0;
            if($group==1){
                $group_check=$member->item_group_1;
            }elseif($group==2){
                $group_check=$member->item_group_2;
            }elseif($group==3){
                $group_check=$member->item_group_3;
            }

            foreach($quest['quest'] as $key_q=>$value_q){
                if($group_check==false){
                    $quest_log[$key_q]['status']=false;
                    $checkpass=QuestLogs::where('uid',$member->uid)->where('quest_id',$value_q['id'])->first();
                    if(!isset($checkpass)){
                        $numquest=Cache::remember('BNS:ARCHER_SUPPORT:QUEST_'.$value_q['id'].':CALQUEST_' . $member->uid, 5, function() use($member,$value_q,$start,$end) {
                            $numquest=0;
                            $numquest=$this->setQuestCompletedCount($member->uid,$member->character_id,$value_q['id'],$start,$end);

                            return $numquest;
                        });

                        if($numquest>0){
                            $newquestlogs= new QuestLogs;
                            $newquestlogs->uid=$member->uid;
                            $newquestlogs->character_id=$member->character_id;
                            $newquestlogs->quest_step=$group;
                            $newquestlogs->quest_id=$value_q['id'];
                            $newquestlogs->quest_name=$value_q['name'];
                            $newquestlogs->created_at_string=Carbon::now()->toDateString();
                            $newquestlogs->save();
                            $total_pass+=1;
                            $quest_log[$key_q]['status']=true;
                        }else{
                            $quest_log[$key_q]['status']=false;
                        }
                    }else{
                        $total_pass+=1;
                        $quest_log[$key_q]['status']=true;
                    }
                }else{
                    $total_pass+=1;
                    $quest_log[$key_q]['status']=true;
                }
                $quest_log[$key_q]['id']=$key_q+1;
            }

            $quest_pass=false;
            if($group_check==false && $total_pass==$num_current_quest){
                $quest_pass=true;
            }

            if($group==1 && $group_check==true && $member->item_group_1==false){
                $quest_pass=false;
            }
            if($group==2 && $member->item_group_1==false){
                $quest_pass=false;
            }
            if($group==3 && $member->item_group_2==false ){
                $quest_pass=false;
            }

            return [
                'status_step'=>$quest_pass,
                'received'=>$group_check,
                'quest_log'=>$quest_log
            ];
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemLog::where('_id', $log_id)->where('uid', $uid)->update($arr);
        }

        private function checkItemHistory($uid,$package_id){
            $itemHistory = ItemLog::where('uid', $uid)
                                ->where('package_id', $package_id)
                                // ->where('status', 'success')
                                ->count();
            return $itemHistory;
        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return false;
            }
            $member = Member::where('uid', $uid)->first();
            if($member->character_id==0){

                $char_data=$this->doGetChar($member->uid,$member->ncid);
                $numchar=collect($char_data)->where('job',$this->jobRequire)->count();
                if($numchar==0){
                    return false;
                }

                $mychar=collect($char_data)->where('job',$this->jobRequire);
                $content = [];
                foreach ($mychar as $key => $value) {
                    $content[] = [
                        'char_id' => $value['char_id'],
                        'char_name' => $value['char_name'],
                    ];
                }
                return [
                    'username'=>$member->username,
                    'character_name'=>'',
                    'selected_char'=>false,
                    'characters'=>$content,
                ];
            }


            $step_1=$this->calQuest($member, $this->startTime,$this->endTime,1);

            $step_2=$this->calQuest($member, $this->startTime,$this->endTime,2);

            $step_3=$this->calQuest($member, $this->startTime,$this->endTime,3);

            return [
                'username'=>$member->username,
                'character_name'=>$member->character_name,
                'selected_char'=>true,
                'characters'=>[],
                'quest_log_step_1'=>$step_1,
                'quest_log_step_2'=>$step_2,
                'quest_log_step_3'=>$step_3,
            ];
        }

        public function getEventInfo(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            // $uid=200062;
            // $ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
            // get member info
            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);
            if($eventInfo==false){
                return response()->json([
                        'status' => false,
                        'data' => $eventInfo,
                        'message'=>'ไม่พบตัวละคร'
                    ]);
            }

            return response()->json([
                    'status' => true,
                    'data' => $eventInfo
                ]);

        }

        public function selectCharacter(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'select_character') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }
            if ($decoded->has('id') == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (2)'
                ]);
            }

            $char_id = (int)$decoded['id'];

            $userData = $this->userData;
            $uid = $userData['uid'];

            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบข้อมูล'
                ]);
            }

            if($member->character_id>0){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถเปลี่ยนตัวละครได้'
                ]);
            }

            $mychar=$this->doGetChar($member->uid,$member->ncid);
            $myselect=collect($mychar)->where('job',$this->jobRequire)->where('char_id',$char_id);
            if(count($myselect)==0){
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่พบตัวละคร'
                ]);
            }

            $member->character_id=(int) $myselect->first()['char_id'];
            $member->character_name=$myselect->first()['char_name'];
            $member->save();

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'data' => $eventInfo
                    ]);

        }

        public function redeemReward(Request $request){

            $decoded = $request;

            if ($decoded->has('type') == false || $decoded['type'] != 'redeem') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (1)'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            if($member->character_id==0){
                return response()->json([
                            'status' => false,
                            'message' => 'โปรดเลือกตัวละครก่อน'
                ]);
            }

            $current_step=0;
            if($member->item_group_1==false){
                $current_step=1;
            }elseif($member->item_group_2==false){
                $current_step=2;
            }elseif($member->item_group_3==false){
                $current_step=3;
            }else{
                return response()->json([
                            'status' => false,
                            'message' => 'คุณเล่นกิจกรรมครบแล้ว'
                ]);
            }

            $check_quest=$this->calQuest($member, $this->startTime,$this->endTime,$current_step);
            if($check_quest['status_step']==false){
                return response()->json([
                            'status' => false,
                            'message' => 'คุณยังทำภารกิจไม่ครบ'
                ]);
            }

            $item_get=$this->questList[$current_step-1]['item'];

            // set reward packages
            $goods_data = []; //good data for send item group
            $packageId = $current_step;
            $packageTitle = $item_get[0]['product_title'];

            $productList = $item_get;

            foreach ($productList as $key => $product) {
                $goods_data[] = [
                    'goods_id' => $product['product_id'],
                    'purchase_quantity' => $product['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];
            }

            $sendItemLog = $this->addItemHistoryLog([
                'uid' => $member->uid,
                'ncid' => $member->ncid,
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
                'item_no' => $packageId,
                'item_type' => 'redeem',
                'package_title' => $packageTitle,
                'product_set' => $productList,
                'status' => 'pending',
                'send_item_status' => false,
                'send_item_purchase_id' => 0,
                'send_item_purchase_status' => 0,
                'goods_data' => json_encode($goods_data),
                'created_at_string' => Carbon::now()->toDateString(),
                'created_at_string_time' => Carbon::now()->toDateTimeString(),
                'last_ip' => $this->getIP()
            ]);
            if($current_step==1){
                $member->item_group_1=true;
            }elseif($current_step==2){
                $member->item_group_2=true;
            }elseif($current_step==3){
                $member->item_group_3=true;
            }
            $member->save();
            $send_result_raw = $this->dosendItem($member->ncid, $goods_data);

            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ? $send_result->status : false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ? $send_result->response->purchase_status : 0,
                    'status' => 'success'
                ], $sendItemLog['_id'], $member->uid);

                // $eventInfo = $this->setEventInfo($member->uid);

                return response()->json([
                            'status' => true,
                            'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                            'data' => $eventInfo,
                            'reward'=>[
                                'name'=>$packageTitle
                            ]
                        ]);
            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                        ], $sendItemLog['_id'], $member->uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                            'data' => $eventInfo
                        ]);
            }

        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:ARCHER_SUPPORT:CHARACTERS_' . $uid, 10, function() use($ncid) {
                        return $this->post_api($this->baseApi, [
                                    'key_name' => 'bns',
                                    'service' => 'characters',
                                    'user_id' => $ncid
                        ]);
                    });
        }


        private function apiCheckQuestCompleted($uid, $charId, $questId, $start, $end) {
            // $resp = Cache::remember('BNS:ARCHER_SUPPORT:QUEST_COMPLETED_' . $questId . '_' . $uid, 10, function()use($charId,$questId,$start,$end) {
                $resp= $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'quest_completed',
                    'char_id' => $charId,
                    'quest_id' => $questId,
                    'start_time' => $start,
                    'end_time' => $end
                ]);
            // });

            return json_decode($resp);
        }

    }
