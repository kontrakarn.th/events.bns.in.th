<?php

namespace App\Http\Controllers\Api\Test\soul_gachapon_valentine;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\BnsEventController;

// Utils
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

// Models
use App\Models\Test\soul_gachapon_valentine_2019\sgv_send_item_log as SendItemLog;
use App\Models\Test\soul_gachapon_valentine_2019\sgv_item_history as ItemHistory;
use App\Models\Test\soul_gachapon_valentine_2019\sgv_deduct_log as DeductLog;
use App\Models\Test\soul_gachapon_valentine_2019\sgv_member as Member;
use App\Models\Test\soul_gachapon_valentine_2019\sgv_reward as Reward;

class SoulGachaValentine extends BnsEventController
{
    private $baseApi = 'http://api.apps.garena.in.th';

    private $startTime = '2019-02-14 00:00:01,่';
    private $endBuySoulTime = '2019-02-28 23:59:59';
    private $endMergeAndRedeemItem = '2019-03-07 23:59:59';

    private $airpay_start = '2019-02-14 00:00:01'; // '2019-02-14 00:00:01'
    private $airpay_end = '2019-02-28 23:59:59';

    private $total_airpay_rights = 5; // 8000

    // set desuct diamonds
    private $deduct_diamond_one_ball = 1; // 2,500 diamonds
    private $deduct_diamond_ten_ball = 2; // 25,000 diamonds
    private $send_gift_service_charge = 3; // 10,000 diamonds

    private $potion_exchange = [
        'product_title' => 'ยาโชคชะตาขนาดใหญ่ x10',
        'product_id' => 783,
        'product_quantity' => 1,
        'icon' => 'great_lucky_potion.png',
        'gachapon_type' => 'redeem',
        'color' => 'red'
    ];

    private $material_item = [
        'package_1' => [
            20791, 20792
        ],
        'package_2' => [
            20801, 20802
        ],
        'package_3' => [
            20811, 20812
        ],
        'package_4' => [
            20831, 20832
        ],
        'package_5' => [
            20821, 20822
        ],
    ];

    private $material_name = [

        20791 => 'เซตม้าขาว Part.1',
        20792 => 'เซตม้าขาว Part.2',

        20801 => 'ปีกม้าขาว Part.1',
        20802 => 'ปีกม้าขาว Part.2',

        20811 => 'หินล้ำค่าเปกาซัส Part.1',
        20812 => 'หินล้ำค่าเปกาซัส Part.2',

        20831 => 'ชุดรักหวานชื่น Part.1',
        20832 => 'ชุดรักหวานชื่น Part.2',

        20821 => 'หมวกรักกันไม่เสื่อมคลาย Part.1',
        20822 => 'หมวกรักกันไม่เสื่อมคลาย Part.2',

    ];
    protected $idToAs = [
        20791 => 'MAT_1_1',
        20792 => 'MAT_1_2',

        20801 => 'MAT_2_1',
        20802 => 'MAT_2_2',

        20811 => 'MAT_3_1',
        20812 => 'MAT_3_2',

        20831 => 'MAT_4_1',
        20832 => 'MAT_4_2',

        20821 => 'MAT_5_1',
        20822 => 'MAT_5_2',

    ];
    protected $asToId = [
        'MAT_1_1' => 20791,
        'MAT_1_2' => 20792,

        'MAT_2_1' => 20801,
        'MAT_2_2' => 20802,

        'MAT_3_1' => 20811,
        'MAT_3_2' => 20812,

        'MAT_4_1' => 20831,
        'MAT_4_2' => 20832,

        'MAT_5_1' => 20821,
        'MAT_5_2' => 20822,

    ];


    public function __construct(Request $request)
    {
        parent::__construct();

        $this->userData = $this->getJwtUserData($request);

        $this->checkEventStatus();
    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
            die(json_encode([
                'status' => false,
                'type' => 'no_permission',
                'message' => 'Sorry, you do not have permission.'
            ]));
        }

        if ((time() < strtotime($this->startTime) || time() > strtotime($this->endMergeAndRedeemItem))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status' => false,
                'type' => 'maintenance',
                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
            ]));
        }

        if ($this->isLoggedIn()) {
                // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                ]));
            }
        } else {
            die(json_encode([
                'status' => false,
                'type' => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        $this->userData = $this->getUserData();
        if ($this->userData)
            return true;
        else
            return false;
    }

    private function is_accepted_ip()
    {
        $allow_ips = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1';

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function checkMember()
    {
        $uid = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid' => $uid,
                'username' => $username,
                'ncid' => $ncid,
                'last_ip' => $this->getIP()
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $uid);
            }
        }
        return true;
    }

    private function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    private function updateMember($row_id = 0, $arr)
    {
        if (empty($row_id))
            return false;

        return Member::where('id', $row_id)->update($arr);
    }

    private function getNcidByUid(int $uid) : string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    private function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username) : string
    {
        return Cache::remember(sha1('BNS:SOUL_GACHAPON_VALENTINE_2019:ASSOCIATE_UID_WITH_NCID_' . $uid), 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'user_info',
                'uid' => $uid,
            ]);
            $userNcInfo = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }

        });
    }

    private function getUserInfo($uid)
    {
        $userInfo = $this->post_api($this->baseApi, [
            'key_name' => 'bns',
            'service' => 'user_info',
            'uid' => $uid
        ]);
        $result = json_decode($userInfo, true);
        if (is_null($result) || $result['status'] == false) {
            return false;
        }
        return $result['response'];
    }

    public function getEventInfo(Request $request)
    {

        if ($request->has('type') == false && $request->type != 'event_info') {//check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

            // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $eventInfo = $this->setEventInfo($uid);

        if ($eventInfo == false) {
            $eventInfo = [];
        }

        return response()->json([
            'status' => true,
            'content' => $eventInfo
        ]);

    }

    private function setEventInfo($uid = null)
    {
        if(empty($uid)) {
            return false;
        }

        $member = Member::where('uid', $uid)->first();

        $diamondBalanceRaw = $this->diamondBalance($uid);
        $diamondBalance = json_decode($diamondBalanceRaw);

        $diamond_balance = 0;
        if ($diamondBalance->status) {
            $diamond_balance = $diamondBalance->balance;
        }

        $airpayRights = $this->setAirpayRights($uid);

        return [
            'username' => $member->username,
            'soul_1' => ($diamond_balance >= 2500),
            'soul_10' => ($diamond_balance >= 25000),
            'airpay_info' => [
                'airpay_remain_all_rights' => $airpayRights['remain_all_rights'],
                'airpay_remain_all_rights_text' => number_format($airpayRights['remain_all_rights']),
                'airpay_total_rights' => $airpayRights['total_rights'],
                'airpay_used' => $airpayRights['used_rights'],
                'airpay_remain' => $airpayRights['remain_rights'],
                'airpay_can_redeem' => $airpayRights['can_redeem'],
            ],
        ];
    }

    public function buySoul(Request $request)
    {
        if(time() < strtotime($this->startTime) && time() > strtotime($this->endBuySoulTime)){
            return response()->json([
                'status' => false,
                'message' => 'ไม่อยู่ในช่วงเวลาโปรโมชั่นซื้อลูกแก้วโซล'
            ]);
        }

        if ($request->has('type') == false && $request->type != 'buysoul' && $request->has('package_id')) {//check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->getUserData();
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);
        $package_id = intval($request->package_id);

        if (!in_array($package_id, [1, 2])) {
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter (2)!'
            ]);
        }

        $gachapon_type = 'one';
        if ($request->has('package_id')) {
            $package_id = $request->package_id;
            if ($package_id == 2) {
                $gachapon_type = 'ten';
            }
        }

        if ($package_id == 1 || $package_id == 2) {
            $required_deduct_diamond = $this->deduct_diamond_one_ball; //
            $available_soul = 1;
            if ($package_id == 2) {
                $available_soul = (10 + 1);
                $required_deduct_diamond = $this->deduct_diamond_ten_ball; //
            }

            // get diamond balance
            $respRaw = $this->diamondBalance($uid);
            $resp = json_decode($respRaw);

            if ($resp->status == false) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถอัพเดทข้อมูล Diamonds ของคุณได้กรุณาลองใหม่อีกครั้ง'
                ]);
            } else {
                $before_diamond = $resp->balance;
                if ($resp->balance < $required_deduct_diamond) {
                    return response()->json([
                        'status' => false,
                        'message' => 'จำนวนไดมอนด์ของคุณไม่พอในการซื้อกาชาปอง'
                    ]);
                } else {
                    $log_data = $this->addDeductLog([
                        'status' => 'pending',
                        'diamond' => $required_deduct_diamond,
                        'uid' => $uid,
                        'ncid' => $ncid,
                        'last_ip' => $this->getIP(),
                        'deduct_type' => 'gachapon',
                        'log_date' => (string)date('Y-m-d'), // set to string
                        'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                    ]);

                    // set desuct diamonds
                    $deductData = $this->setDiamondDeductData($required_deduct_diamond);

                    $resp_deduct_raw = $this->deductDiamond($ncid, $deductData);
                    $resp_deduct = json_decode($resp_deduct_raw);

                    if (is_object($resp_deduct) && is_null($resp_deduct) == false) {
                        $arr_log = [
                            'before_deduct_diamond' => $before_diamond,
                            'after_deduct_diamond' => $before_diamond - $required_deduct_diamond,
                            'deduct_status' => $resp_deduct->status ? : 0,
                            'deduct_purchase_id' => $resp_deduct->response->purchase_id ? : 0,
                            'deduct_purchase_status' => $resp_deduct->response->purchase_status ? : 0,
                            'deduct_data' => json_encode($deductData)
                        ];

                        if ($resp_deduct->status) {
                            $arr_log['status'] = 'success';
                            $this->updateDeductLog($arr_log, $log_data['id']);
                            //next step
                            //pack 1 = 1 available | pack 2 = 10+1 = 11 available

                            $rand_result = $this->doRandomItem($available_soul); //do getRandomCode
                            $goods_data = []; //good data for send item group
                            foreach ($rand_result as $key => $value) {//loop for add item log
                                $arr = [
                                    'product_id' => 0,
                                    'product_title' => '',
                                    'product_quantity' => 0,
                                    'uid' => $uid,
                                    'ncid' => $ncid,
                                    'last_ip' => $this->getIP(),
                                    'icon' => '',
                                    'gachapon_type' => $gachapon_type,
                                    'log_date' => (string)date('Y-m-d'), // set to string
                                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                                ];

                                if ($value['item_type'] == 'material' || $value['item_type'] == 'rare_item') {
                                    $arr['product_id'] = $value['product_id'];
                                    $arr['product_title'] = $value['product_title'];
                                    $arr['product_quantity'] = $value['product_quantity'];
                                    $arr['item_type'] = $value['item_type'];
                                    $arr['status'] = 'not_used';
                                    $arr['icon'] = $value['icon'];
                                    $arr['send_type'] = 'not_send';
                                } else {
                                    $arr['product_id'] = $value['product_id'];
                                    $arr['product_title'] = $value['product_title'];
                                    $arr['product_quantity'] = $value['product_quantity'];
                                    $arr['icon'] = $value['icon'];
                                    $arr['status'] = 'used';
                                    $arr['send_type'] = 'owner';
                                    $arr['item_type'] = 'object';

                                    $goods_data[] = [
                                        'goods_id' => $value['product_id'],
                                        'purchase_quantity' => $value['product_quantity'],
                                        'purchase_amount' => 0,
                                        'category_id' => 40
                                    ];
                                }
                                $this->addItemLogHistory($arr);
                            }

                            //do send item by goods data
                            if (is_null($goods_data) == false && is_array($goods_data) && count($goods_data) > 0 && is_null($resp_deduct->response->purchase_id) == false) {
                                //do add send item log
                                $log_result = $this->addSendItemLog([
                                    'uid' => $uid,
                                    'ncid' => $ncid,
                                    'status' => 'pending',
                                    'send_item_status' => false,
                                    'send_item_purchase_id' => 0,
                                    'send_item_purchase_status' => 0,
                                    'goods_data' => json_encode($goods_data),
                                    'last_ip' => $this->getIP(),
                                    'type' => 'gachapon',
                                    'log_date' => (string)date('Y-m-d'), // set to string
                                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                                ]);

                                //send item
                                $send_result_raw = $this->dosendItem($ncid, $goods_data);
                                $send_result = json_decode($send_result_raw);

                                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                                    //recive log id
                                    $log_id = $log_result['id'];
                                    //do update send item log
                                    $this->updateSendItemLog([
                                        'send_item_status' => $send_result->status ? : false,
                                        'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                                        'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                                        'status' => 'success'
                                    ], $log_id);
                                } else {
                                    //do update send item log
                                    $log_id = $log_result['id'];
                                    $this->updateSendItemLog([
                                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                                        'status' => 'unsuccess'
                                    ], $log_id);
                                    return response()->json([
                                        'status' => false,
                                        'message' => 'ไม่สามารถส่งไอเทมได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                                    ]);
                                }
                            }

                            $content = [];
                            //convert content result
                            foreach ($rand_result as $key => $value) {
                                $collection = collect($value);
                                $filtered = $collection->only(['product_title', 'item_type', 'key', 'icon', 'color']); //except
                                $content[] = $filtered->all();
                            }

                            //do return success log
                            return response()->json([
                                'status' => true,
                                'message' => 'ซื้อ soul สำเร็จ',
                                'content' => $content
                            ]);
                        } else {
                            //error deduct code
                            $arr_log['status'] = 'unsuccess';
                            $this->updateDeductLog($arr_log, $log_data['id']);
                        }
                    } else {
                        $arr_log['status'] = 'unsuccess';
                        $this->updateDeductLog($arr_log, $log_data['id']);
                    }
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }
            }
        }
        return response()->json([
            'status' => false,
            'message' => 'ไม่พบ Package กรุณาลองใหม่อีกครั้ง'
        ]);
    }

    public function redeemAirpayPromotion(Request $request)
    {
        if ($request->has('type') == false && $request->type != 'redeem_airpay_promotion') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        $airpayInfo = $this->setAirpayRights($uid);

        $available_soul = 1;

        if ($airpayInfo['can_redeem'] == true) {

            $rand_result = $this->doRandomItem($available_soul); //do getRandomCode
            $goods_data = []; //good data for send item group
            foreach ($rand_result as $key => $value) {//loop for add item log
                $arr = [
                    'product_id' => 0,
                    'product_title' => '',
                    'product_quantity' => 0,
                    'uid' => $uid,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                    'icon' => '',
                    'gachapon_type' => 'airpay',
                    'log_date' => (string)date('Y-m-d'), // set to string
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                ];

                if ($value['item_type'] == 'material' || $value['item_type'] == 'rare_item') {
                    $arr['product_id'] = $value['product_id'];
                    $arr['product_title'] = $value['product_title'];
                    $arr['product_quantity'] = $value['product_quantity'];
                    $arr['item_type'] = $value['item_type'];
                    $arr['status'] = 'not_used';
                    $arr['icon'] = $value['icon'];
                    $arr['send_type'] = 'not_send';
                } else {
                    $arr['product_id'] = $value['product_id'];
                    $arr['product_title'] = $value['product_title'];
                    $arr['product_quantity'] = $value['product_quantity'];
                    $arr['icon'] = $value['icon'];
                    $arr['status'] = 'used';
                    $arr['send_type'] = 'owner';
                    $arr['item_type'] = 'object';

                    $goods_data[] = [
                        'goods_id' => $value['product_id'],
                        'purchase_quantity' => $value['product_quantity'],
                        'purchase_amount' => 0,
                        'category_id' => 40
                    ];
                }
                $this->addItemLogHistory($arr);
            }

            //do send item by goods data
            if (is_null($goods_data) == false && is_array($goods_data) && count($goods_data) > 0) {
                //do add send item log
                $log_result = $this->addSendItemLog([
                    'uid' => $uid,
                    'ncid' => $ncid,
                    'status' => 'pending',
                    'send_item_status' => false,
                    'send_item_purchase_id' => 0,
                    'send_item_purchase_status' => 0,
                    'goods_data' => json_encode($goods_data),
                    'last_ip' => $this->getIP(),
                    'type' => 'airpay',
                    'log_date' => (string)date('Y-m-d'), // set to string
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                ]);

                //send item
                $send_result_raw = $this->dosendItem($ncid, $goods_data);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                    //recive log id
                    //do update send item log
                    $this->updateSendItemLog([
                        'send_item_status' => $send_result->status ? : false,
                        'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                        'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                        'status' => 'success'
                    ], $log_result['id']);
                } else {
                    //do update send item log
                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                        'status' => 'unsuccess'
                    ], $log_result['id']);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งไอเทมได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                    ]);
                }
            }

            $content = [];
            //convert content result
            foreach ($rand_result as $key => $value) {
                $collection = collect($value);
                $filtered = $collection->only(['product_title', 'item_type', 'key', 'icon', 'color']); //except
                $content[] = $filtered->all();
            }

            $airpayInfo = $this->setAirpayRights($uid);

            //do return success log
            return response()->json([
                'status' => true,
                'message' => 'ส่งไอเทมโปรดมชั่น Airpay สำเร็จ',
                'content' => $content,
                'airpay_info' => $airpayInfo
            ]);
        }

        return response()->json([
            'status' => false,
            'message' => 'No data requireds.'
        ]);
    }

    private function setAirpayRights($uid){
        if(empty($uid)) {
            return false;
        }

        $allRights = 0;
        $remainAllRights = 0;
        $totalRights = 0;
        $usedRights = 0;
        $remainRights = 0;

        $member = Member::where('uid', $uid)->first();

        // check airpay txn
        $diamondsTxnRaw = $this->apiDiamondHistory($uid, $member->ncid, $this->airpay_start, $this->airpay_end);
        $diamondsTxn = json_decode($diamondsTxnRaw);
        if ($diamondsTxn->status == true) {
            if (count($diamondsTxn->response) > 0) {

                $airpayTxnCollection = 0;

                $collection = collect($diamondsTxn->response);
                $airpayTxnCollection = $collection->whereIn('diamond', [100000, 300000])->count();

                if($airpayTxnCollection > 0){
                    $totalRights = 5;
                }else{
                    $airpayTxnCollection = $collection->where('diamond', 50000)->count();
                    if ($airpayTxnCollection > 0) {
                        $totalRights = $airpayTxnCollection * 2;
                        if($totalRights > 5){
                            $totalRights = 5;
                        }
                    }
                }

            }
        }

        // check user used rights
        $airpayUsedRights = $this->checkRedeemAirpayPromotionHistory($member->uid, $member->ncid);

        // check all used rights
        $allRights = $this->checkRedeemptionAirpayPromotionAmount();
        $remainAllRights = $this->total_airpay_rights - $allRights;

        $remainRights = $totalRights > 0 && $totalRights - $airpayUsedRights > 0 ? $totalRights - $airpayUsedRights : 0;

        return [
            'remain_all_rights' => $remainAllRights,
            'total_rights' => $totalRights,
            'used_rights' => $airpayUsedRights,
            'remain_rights' => $remainRights,
            'can_redeem' => $remainRights > 0 && $remainAllRights > 0,
        ];

    }

    private function checkRedeemAirpayPromotionHistory($uid, $ncid)
    {
        return ItemHistory::where('uid', $uid)
                        ->where('ncid', $ncid)
                        ->where('gachapon_type', 'airpay')
                        ->count();
    }

    private function checkRedeemptionAirpayPromotionAmount()
    {
        return ItemHistory::where('gachapon_type', 'airpay')->count();
    }

    public function mergeItem(Request $request)
    {
        if ($request->has('type') == false && $request->type != 'combine') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        if ($request->has('package_id')) {
            $package_id = $request->package_id; //get pack id
            $material_data = $this->getPack($package_id); //get material data id from pack id

            if ($material_data == null) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่พบรูปแบบการผสมชิ้นส่วนที่ท่านต้องการ'
                ]);
            }

            $ncid = $this->getNcidByUid($uid);

            $myMaterial = $this->hasMaterial($material_data, $uid, $ncid); //check material store
            $collection = collect($myMaterial);
            $material_data = $collection->groupBy('product_id')->toArray();
            $material_counter = $collection->groupBy('product_id')->count();

            if ($material_counter != 2) {//error merge item
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถผสมชิ้นส่วนได้<br />เนื่องจากมีชิ้นส่วนไม่เพียงพอในการผสมกรุณาลองใหม่อีกครั้ง'
                ]);
            }

            $rewards = new Reward;
            $item_data = $rewards->setRareItemByPackageId($package_id);
            if($item_data == false){
                return response()->json([
                    'status' => false,
                    'message' => 'No data.'
                ]);
            }

            if ($this->deductMaterial($material_data, $material_counter) == false) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถผสมชิ้นส่วนได้<br />เกิดความผิดพลาดขึ้น กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }

            // update item history
            $item_log = $this->addItemLogHistory([
                'product_id' => $item_data['product_id'],
                'product_title' => $item_data['product_title'],
                'product_quantity' => $item_data['product_quantity'],
                'item_type' => 'rare_item',
                'status' => 'not_used',
                'uid' => $uid,
                'ncid' => $ncid,
                'last_ip' => $this->getIP(),
                'icon' => $item_data['icon'],
                'gachapon_type' => 'merge',
                'product_set' => 'no',
                'product_pack_data' => null,
                'send_type' => 'not_send',
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
            ]);

            // update merge item to my bag
            if ($item_log['id']) {

                // get my material
                $myMaterials = $this->setMyMaterial($uid, $ncid);

                return response()->json([
                    'status' => true,
                    'message' => 'ผสมชิ้นส่วน <br />"' . $item_data['product_title'] . '"<br />เสร็จสิ้น ไอเทมจะถูกเก็บที่เมนู <br />"ส่งของรางวัล"',
                    'content' => [
                        'username' => $member->username,
                        'materials' => $myMaterials,
                    ],
                ]);

            } else {

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถผสมชิ้นส่วนให้คุณได้ กรุณาติดต่อฝ่ายบริการลูกค้า (2)'
                ]);
            }
        }
    }

    private function setMyMaterial($uid, $ncid)
    {
        $resp_material = $this->getMyMaterial($uid, $ncid);

        /* $collection = collect($resp_material);
        $material_data = $collection->groupBy('product_id');
        $data_arr = ['package_1' => [], 'package_2' => [], 'package_3' => [], 'package_4' => [], 'package_5' => []];
        for ($index = 0; $index < 5; $index++) {
            foreach ($this->getPack(($index + 1)) as $value) {
                $data_arr[('package_' . ($index + 1))]['P' . $this->idToAs[$value]] = [
                    'id' => $this->idToAs[$value],
                    'title' => $this->material_name[$value],
                    'counter' => $collection->where('product_id', $value)->count()
                ];
            }
        } */

        $collection = collect($resp_material);
        // $material_data = $collection->groupBy('product_id');
        // $data_arr = ['package_1' => [], 'package_2' => [], 'package_3' => [], 'package_4' => [], 'package_5' => []];
        $data_arr = [];
        for ($index = 0; $index < 5; $index++) {
            foreach ($this->getPack(($index + 1)) as $key=>$value) {
                /* $data_arr[('package_' . ($index + 1))]['P' . $this->idToAs[$value]] = [
                    'id' => $this->idToAs[$value],
                    'title' => $this->material_name[$value],
                    'counter' => $collection->where('product_id', $value)->count()
                ]; */
                $data_arr[$index][$key] = [
                    'id' => $this->idToAs[$value],
                    'title' => $this->material_name[$value],
                    'quantity' => $collection->where('product_id', $value)->count()
                ];
            }
        }

        return $data_arr;
    }

    private function deductMaterialForExchangePotion($material_data, int $counter)
    {
        if ($counter > 0) {//redeem item
            foreach ($material_data as $key => $value) {
                $callback = ItemHistory::where('id', $value['id'])->where('uid', $value['uid'])->update([
                    'status' => 'used',
                    'send_type' => 'potion'
                ]);
                if ($callback == false) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }

    private function deductMaterial($material_data, int $counter)
    {
        if ($counter == 5) {//redeem item
            foreach ($material_data as $key => $value) {
                $callback = ItemHistory::where('id', $value['id'])->where('uid', $value['uid'])->update([
                    'status' => 'used'
                ]);
                if ($callback == false) {
                    return false;
                }
            }
        } elseif ($counter == 2) {//merge item
            foreach ($material_data as $key => $value) {
                $callback = ItemHistory::where('id', $value[0]['id'])->where('uid', $value[0]['uid'])->update([
                    'status' => 'used'
                ]);
                if ($callback == false) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }

    private function getPack(int $id)
    {
        switch ($id) {
            case 1:
                return $this->material_item['package_1'];
                break;
            case 2:
                return $this->material_item['package_2'];
                break;
            case 3:
                return $this->material_item['package_3'];
                break;
            case 4:
                return $this->material_item['package_4'];
                break;
            case 5:
                return $this->material_item['package_5'];
                break;
        }
        return null;
    }

    private function hasMaterial(array $material_data, $uid, $ncid)
    {
        return ItemHistory::where('status', 'not_used')
                        ->whereIn('product_id', $material_data)
                        ->where('uid', $uid)
                        ->where('ncid', $ncid)
                        ->get();
    }

    private function hasMaterialByProductId(int $product_id, $uid, $ncid)
    {
        return ItemHistory::where('status', 'not_used')
                        ->where('product_id', $product_id)
                        ->where('uid', $uid)
                        ->where('ncid', $ncid)
                        ->get();
    }

    private function checkPackageMaterialsCount($package_id)
    {
        switch ($package_id) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return 5;
                break;
        }
        return null;
    }

    public function redeemItem(Request $request)
    {
        if ($request->has('type') == false && $request->type != 'redeem') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        if ($request->has('package_id') == false) {//check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (2)'
            ]);
        }

        if ($request->has('item_data') == false) {//check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (3)'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        $item_data = $request->item_data;
        $package_id = $request->package_id;

        // get package materials requirement
        $requireCount = $this->checkPackageMaterialsCount($package_id);

        $collection = collect($item_data);
        if ($collection->sum('number') < $requireCount) {//check material data from user
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถแลกไอเทมได้ <br />เนื่องจากมีไอเทมชิ้นส่วนพอ (1)'
            ]);
        }

        //new code
        $item_id_pluck = $collection->where('number', '!=', '0')->pluck('item_id')->all();
        $collection_pluck = collect($item_id_pluck);
        $item_id_result = $collection_pluck->map(function ($item, $key) {
            return $this->asToId[$item];
        })->toArray();

        $myMaterial = $this->hasMaterial($item_id_result, $uid, $ncid);
        $collection2 = collect($myMaterial);
        $groupMaterial = $collection2->groupBy('product_id');

        $mergeMaterial = [];
        foreach ($groupMaterial->toArray() as $key => $value) {

            if ($collection->where('item_id', $this->idToAs[$key])->first()['number'] > count($value)) {
                return response()->json([
                    'status' => false,
                    'message' => 'จำนวนชิ้นส่วนที่ส่งมาไม่ถูกต้อง<br />กรุณา ตรวจสอบใหม่อีกครั้ง'
                ]);
                break;
            }
            $mergeMaterial = array_collapse([$collection2->where('product_id', $key)->take($collection->where('item_id', $this->idToAs[$key])->first()['number'])->toArray(), $mergeMaterial]);
        }

        if (count($mergeMaterial) < $requireCount) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถแลกไอเทมได้ <br />เนื่องจากมีไอเทมชิ้นส่วนไม่พอ (2)'
            ]);
        }

        // update used materials
        if ($this->deductMaterial($mergeMaterial, $requireCount) == false) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดความผิดพลาด<br />ไม่สามารถแลกไอเทมได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
            ]);
        }

        $rewards = new Reward;
        $pack_data = $rewards->setRareItemByPackageId($package_id);

        if (is_null($pack_data) == false) {

            $product_set = 'no';
            $product_pack_data = null;

            // add to item history log
            $this->addItemLogHistory([
                'product_id' => $pack_data['product_id'],
                'product_title' => $pack_data['product_title'],
                'product_quantity' => $pack_data['product_quantity'],
                'item_type' => 'rare_item',
                'status' => 'not_used',
                'uid' => $uid,
                'ncid' => $ncid,
                'last_ip' => $this->getIP(),
                'icon' => $pack_data['icon'],
                'gachapon_type' => 'redeem',
                'product_set' => $product_set,
                'product_pack_data' => $product_pack_data,
                'send_type' => 'not_send',
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
            ]);

            // get my material
            $myMaterials = $this->setMyMaterial($uid, $ncid);

            return response()->json([
                'status' => true,
                'message' => 'แลก <br />' . $pack_data['product_title'] . ' <br />สำเร็จ ไอเทมจะถูกเก็บที่เมนู <br /> &quot;ส่งของรางวัล&quot;',
                'content' => [
                    'username' => $member->username,
                    'materials' => $myMaterials,
                ],
            ]);

        }

        return response()->json([
            'status' => false,
            'message' => 'ไม่สามารถแลกไอเทมได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
        ]);
    }

    public function getExchangePotion(Request $request)
    {
        if ($request->has('type') == false && $request->type != 'exchange_potion') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        if ($request->has('item_data') == false) {//check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (2)'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        $redeemItemData = $request->item_data;

        $collection = collect($redeemItemData);
        $totalExchange = $collection->sum('number');
        if($totalExchange == 0) {//check material data from user
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถแลกไอเทมได้<br />เนื่องจากมีไอเทมชิ้นส่วนไม่พอ'
            ]);
        }

        //new code
        $item_id_pluck = $collection->where('number', '!=', '0')->pluck('item_id')->all();
        $collection_pluck = collect($item_id_pluck);
        $item_id_result = $collection_pluck->map(function ($item, $key) {
            return $this->asToId[$item];
        })->toArray();

        $myMaterial = $this->hasMaterial($item_id_result, $uid, $ncid);
        $collection2 = collect($myMaterial);
        $groupMaterial = $collection2->groupBy('product_id');

        $mergeMaterial = [];
        foreach ($groupMaterial->toArray() as $key => $value) {

            if ($collection->where('item_id', $this->idToAs[$key])->first()['number'] > count($value)) {
                return response()->json([
                    'status' => false,
                    'message' => 'จำนวนชิ้นส่วนที่ส่งมาไม่ถูกต้องกรุณา<br />ตรวจสอบใหม่อีกครั้ง'
                ]);
                break;
            }
            $mergeMaterial = array_collapse([$collection2->where('product_id', $key)->take($collection->where('item_id', $this->idToAs[$key])->first()['number'])->toArray(), $mergeMaterial]);
        }

        if (count($mergeMaterial) <= 0) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถแลกไอเทมได้<br />เนื่องจากมีไอเทมชิ้นส่วนไม่พอ'
            ]);
        }

        // update used materials
        if ($this->deductMaterialForExchangePotion($mergeMaterial, $totalExchange) == false) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดความผิดพลาด<br />ไม่สามารถแลกไอเทมได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
            ]);
        }

        $logdata = $this->addSendItemLog([//add send item log
            'uid' => $uid,
            'ncid' => $ncid,
            'status' => 'pending',
            'last_ip' => $this->getIP(),
            'type' => 'owner',
            'log_date' => (string)date('Y-m-d'), // set to string
            'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
        ]);

        $potionData = $this->potion_exchange;

        $goodsData = [];
        $goodsData = [
            [
                'goods_id' => $potionData['product_id'],
                'purchase_quantity' => $totalExchange,
                'purchase_amount' => 0,
                'category_id' => 40
            ]
        ];

        $this->updateSendItemLog(['goods_data' => json_encode($goodsData)], $logdata['id']);

        $send_result_raw = $this->dosendItem($ncid, $goodsData);
        $send_result = json_decode($send_result_raw);
        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            //do update send item log
            $this->updateSendItemLog([
                'send_item_status' => $send_result->status ? : false,
                'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                'status' => 'success'
            ], $logdata['id']);

            // add new item to item history log
            for ($i = 0; $i < $totalExchange; $i++) {
                $this->addItemLogHistory([
                    'product_id' => $potionData['product_id'],
                    'product_title' => $potionData['product_title'],
                    'product_quantity' => $potionData['product_quantity'],
                    'item_type' => 'object',
                    'status' => 'used',
                    'uid' => $uid,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                    'icon' => $potionData['icon'],
                    'gachapon_type' => 'exchange_potion',
                    'send_type' => 'great_lucky_potion',
                    'log_date' => (string)date('Y-m-d'), // set to string
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                ]);
            }

            // get my material
            $myMaterials = $this->setMyMaterial($uid, $ncid);

            return response()->json([
                'status' => true,
                'message' => 'ส่งไอเทมสำเร็จ<br />กรุณาตรวจสอบไอเทมในกล่องจดหมายภายในเกม',
                'content' => [
                    'username' => $member->username,
                    'materials' => $myMaterials,
                ],
            ]);

        } else {
            //do update send item log
            $this->updateSendItemLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
            ], $logdata['id']);

            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถส่งไอเทมได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
            ]);
        }

    }

    public function getMaterial(Request $request)
    {
        if ($request->has('type') == false && $request->type != 'my_material') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        $resp_material = $this->getMyMaterial($member->uid, $member->ncid);

        $collection = collect($resp_material);
        // $material_data = $collection->groupBy('product_id');
        // $data_arr = ['package_1' => [], 'package_2' => [], 'package_3' => [], 'package_4' => [], 'package_5' => []];
        $data_arr = [];
        for ($index = 0; $index < 5; $index++) {
            foreach ($this->getPack(($index + 1)) as $key=>$value) {
                /* $data_arr[('package_' . ($index + 1))]['P' . $this->idToAs[$value]] = [
                    'id' => $this->idToAs[$value],
                    'title' => $this->material_name[$value],
                    'counter' => $collection->where('product_id', $value)->count()
                ]; */
                $data_arr[$index][$key] = [
                    'id' => $this->idToAs[$value],
                    'title' => $this->material_name[$value],
                    'quantity' => $collection->where('product_id', $value)->count()
                ];
            }
        }

        return response()->json([
            'status' => true,
            'content' => [
                'username' => $member->username,
                'materials' => $data_arr,
            ],
        ]);
    }

    public function getItemHistory(Request $request)
    {
        if ($request->has('type') == false && $request->type != 'item_history') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        $resp_raw = ItemHistory::select('product_title', 'status', 'item_type as type', 'send_type', 'send_to_uid', 'send_to_name', 'log_date_timestamp', 'created_at', 'icon')
            ->where('uid', $uid)
            ->orderBy('created_at', 'DESC')
            ->orderBy('id', 'DESC')
            ->get();
        $resp = [];
        if (count($resp_raw) > 0) {
            foreach ($resp_raw as $key => $value) {
                $resp[$key]['created_at'] = date('d/m/Y H:i:s', $value->log_date_timestamp);
                $resp[$key]['item_title'] = $value->product_title;
                $resp[$key]['send_to_uid'] = !empty($value->send_to_uid) ? $value->send_to_uid : "";
            }
        }

        $dataResp = collect($resp);
        $data = $dataResp->except(['id', 'status', 'type', 'send_type']);

        return response()->json([
            'status' => true,
            'content' => [
                'username' => $member->username,
                'items_list'=>$data
            ],
        ]);
    }

    public function getMyBag(Request $request)
    {
        if ($request->has('type') == false && $request->type != 'my_bag') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        return response()->json([
            'status' => true,
            'content' => [
                'username' => $member->username,
                'my_bag' => $this->setMyBag($member->uid, $member->ncid)
            ],
        ]);

    }

    private function setMyBag($uid, $ncid)
    {

        $myBag = $this->getMyItems($uid, $ncid);

        $collection = collect($myBag);

        $bagList = [];

        $rewards = new Reward;

        for($i=0;$i<6;$i++){
            $itemData = [];
            $itemData = $rewards->setRareItemByPackageId($i+1);
            $bagList[] = [
                'id' => $i+1,
                'title' => $itemData['product_title'],
                'quantity' => $collection->where('product_id', $itemData['product_id'])->count()
            ];
        }

        return $bagList;
    }

    public function getSendItem(Request $request)
    {
        if ($request->has('type') == false && $request->type != 'send_item') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        if ($request->has('package_id') == false) {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);
        $package_id = $request->package_id;

        // get member info
        $member = Member::where('uid', $uid)->first();
        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        $logdata = $this->addSendItemLog([//add send item log
                        'uid' => $uid,
                        'ncid' => $ncid,
                        'status' => 'pending',
                        'last_ip' => $this->getIP(),
                        'type' => 'owner',
                        'log_date' => (string)date('Y-m-d'), // set to string
                        'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                    ]);

        $rewards = new Reward;
        $productData = $rewards->setRareItemByPackageId($package_id);
        $product = $this->getMyItemByProductId($member->uid, $member->ncid, $productData['product_id']);

        if($product){

            $goodsData = [];
            $goodsData = [
                [
                    'goods_id' => $product->product_id,
                    'purchase_quantity' => $product->product_quantity,
                    'purchase_amount' => 0,
                    'category_id' => 40
                ]
            ];

            $this->updateSendItemLog(['goods_data' => json_encode($goodsData)], $logdata['id']);

            if ($this->deductSendItem($product->id, $uid) == false) {
                $this->updateSendItemLog(['status' => 'rejected'], $logdata['id']);
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารส่งไอเทมได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }

            $send_result_raw = $this->dosendItem($ncid, $goodsData, '');
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                //do update send item log
                $this->updateSendItemLog([
                    'send_item_status' => $send_result->status ? : false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                    'status' => 'success'
                ], $logdata['id']);

            } else {
                //do update send item log
                $this->updateSendItemLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                ], $logdata['id']);
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งไอเทมได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }

            // set my bag
            $myBag = $this->setMyBag($uid, $ncid);

            return response()->json([
                'status' => true,
                'message' => 'ส่งไอเทมสำเร็จ<br/>กรุณาตรวจสอบไอเทมในกล่องจดหมายภายในเกม',
                'content' => [
                    'username' => $member->username,
                    'my_bag' => $myBag,
                ],
            ]);
        } else {
            $this->updateSendItemLog(['status' => 'rejected'], $logdata['id']);
        }

        return response()->json([
            'status' => false,
            'message' => 'ไม่สามารถส่งไอเทมได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
        ]);
    }

    public function getCheckUid(Request $request)
    {
        if ($request->has('type') == false && $request->type != 'check_uid') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        if ($request->has('uid') == false) {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
                    ]);
        }

        if($request->has('uid') && $request->uid != ''){

            $target_uid = intval($request->uid);
            $targetInfo = [];
            $targetInfo = $this->getUserInfo($target_uid);

            if ($uid == $target_uid) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งไอเทมให้<br /> UID ของตัวเองได้'
                ]);
            }

            if (is_array($targetInfo) && !empty($targetInfo['user_name'])){
                return response()->json([
                    'status' => true,
                    'content' => [
                        'username' => $member->username,
                        'target_uid' => $target_uid,
                        'target_username' => $targetInfo['user_name']
                    ]
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัยไม่มีข้อมูล UID นี้'
                ]);
            }

        } else {
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

    }

    public function getSendGift(Request $request)
    {
        if ($request->has('type') == false && $request->type != 'send_gift') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        if ($request->has('package_id') == false) {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!(1)'
            ]);
        }

        if ($request->has('uid') == false) {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!(2)'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
                    ]);
        }

        $package_id = $request->package_id; // package id for send gift
        $target_uid = $request->uid; // target uid

        $targetInfo = $this->getUserInfo($target_uid);
        $target_username = $targetInfo['user_name'];
        $target_ncid = $targetInfo['user_id']; // ncid recipient

        if ($uid == $target_uid) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถส่งไอเทมให้<br /> UID ของตัวเองได้'
            ]);
        }

        // get diamond balance
        $diamondBalanceRaw = $this->diamondBalance($uid);
        $diamondBalance = json_decode($diamondBalanceRaw);
        if ($diamondBalance->status) {

            if ($diamondBalance->balance >= $this->send_gift_service_charge) {

                $deduct_log_data = $this->addDeductLog([
                    'status' => 'pending',
                    'diamond' => $this->send_gift_service_charge,
                    'uid' => $uid,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                    'deduct_type' => 'send_gift',
                    'log_date' => (string)date('Y-m-d'), // set to string
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                ]);

                // set desuct diamonds
                $deductData = $this->setDiamondDeductData($this->send_gift_service_charge);
                $resp_deduct_raw = $this->deductDiamond($ncid, $deductData);
                $resp_deduct = json_decode($resp_deduct_raw);

                if (is_object($resp_deduct) && is_null($resp_deduct) == false) {

                    $arr_log = [
                        'before_deduct_diamond' => $diamondBalance->balance,
                        'after_deduct_diamond' => $diamondBalance->balance - $this->send_gift_service_charge,
                        'deduct_status' => $resp_deduct->status ? : 0,
                        'deduct_purchase_id' => $resp_deduct->response->purchase_id ? : 0,
                        'deduct_purchase_status' => $resp_deduct->response->purchase_status ? : 0,
                        'deduct_data' => json_encode($deductData),
                        'status' => 'pending'
                    ];

                    if ($resp_deduct->status) {
                        $arr_log['status'] = 'success';
                        $this->updateDeductLog($arr_log, $deduct_log_data['id']);

                        $logdata = $this->addSendItemLog([//add send item log
                            'uid' => $target_uid,
                            'ncid' => $target_ncid,
                            'status' => 'pending',
                            'last_ip' => $this->getIP(),
                            'type' => 'gift',
                            'send_gift_from' => $uid,
                            'log_date' => (string)date('Y-m-d'), // set to string
                            'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                        ]);

                        // chec and set item for send gift.
                        $rewards = new Reward;
                        $productData = $rewards->setRareItemByPackageId($package_id);
                        $product = $this->getMyItemByProductId($uid, $ncid, $productData['product_id']);

                        if($product){

                            $giftName = $product->product_title;

                            $goodsData = [];
                            $goodsData = [
                                [
                                    'goods_id' => $product->product_id,
                                    'purchase_quantity' => $product->product_quantity,
                                    'purchase_amount' => 0,
                                    'category_id' => 40
                                ]
                            ];

                            $this->updateSendItemLog(['goods_data' => json_encode($goodsData)], $logdata['id']);

                            if ($this->deductSendGift($product->id, $uid, $target_uid, $target_username) == false) {
                                $this->updateSendItemLog(['status' => 'rejected'], $logdata['id']);
                                return response()->json([
                                    'status' => false,
                                    'message' => 'ไม่สามารส่งไอเทมได้<br/>กรุณาติดต่อฝ่ายบริการลูกค้า'
                                ]);
                            }

                            $send_result_raw = $this->dosendItem($target_ncid, $goodsData);
                            $send_result = json_decode($send_result_raw);
                            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                                //do update send item log
                                $this->updateSendItemLog([
                                    'send_item_status' => $send_result->status ? : false,
                                    'send_item_purchase_id' => $send_result->response->purchase_id ? : 0,
                                    'send_item_purchase_status' => $send_result->response->purchase_status ? : 0,
                                    'status' => 'success'
                                ], $logdata['id']);

                            } else {
                                //do update send item log
                                $this->updateSendItemLog([
                                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                                    'status' => 'unsuccess'
                                ], $logdata['id']);

                                return response()->json([
                                    'status' => false,
                                    'message' => 'ไม่สามารถส่งไอเทมได้ กรุณาติดต่อฝ่ายบริการลูกค้า'
                                ]);
                            }

                            // set my bag
                            $myBag = $this->setMyBag($uid, $ncid);

                            return response()->json([
                                'status' => true,
                                'message' => 'ส่ง &quot;' . $giftName . '&quot; ไปยัง <br />' . $target_username . ' ( ' . $target_uid . ' ) <br />สำเร็จ กรุณาแจ้งไปยัง UID <br /> ที่ท่านส่งของขวัญไปให้<br />และให้ตรวจสอบไอเทมในกล่องจดหมายภายในเกม',
                                'content' => [
                                    'username' => $member->username,
                                    'my_bag' => $myBag
                                ],
                            ]);
                        } else {
                            $this->updateSendItemLog(['status' => 'rejected'], $logdata['id']);
                        }

                        return response()->json([
                            'status' => false,
                            'message' => 'ไม่สามารถส่งไอเทมได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                        ]);
                    } else {
                        //error deduct code
                        $arr_log['status'] = 'unsuccess';
                        $this->updateDeductLog($arr_log, $deduct_log_data['id']);
                    }
                } else {
                    $arr_log['status'] = 'unsuccess';
                    $this->updateDeductLog($arr_log, $deduct_log_data['id']);
                }
                return response()->json([
                    'status' => false,
                    'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);


            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'จำนวนไดมอนด์ของคุณไม่พอในการส่งของขวัญ'
                ]);
            }

        } else {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถอัพเดทข้อมูลไดมอนด์ของคุณได้กรุณาลองใหม่อีกครั้ง'
            ]);
        }

    }

    private function getMyMaterial($uid, $ncid)
    {
        return ItemHistory::where('uid', $uid)
                        ->where('ncid', $ncid)
                        ->where('status', 'not_used')
                        ->where('item_type', 'material')
                        ->get();
    }

    private function getMyItems($uid, $ncid)
    {
        return ItemHistory::where('uid', $uid)
                        ->where('ncid', $ncid)
                        ->where('status', 'not_used')
                        ->where('send_type', 'not_send')
                        ->where('item_type', 'rare_item')
                        ->get();
    }

    private function getMyItemByProductId($uid, $ncid, $product_id)
    {
        return ItemHistory::where('uid', $uid)
                        ->where('ncid', $ncid)
                        ->where('status', 'not_used')
                        ->where('send_type', 'not_send')
                        ->where('item_type', 'rare_item')
                        ->where('product_id', $product_id)
                        ->first();
    }

    private function deductSendItem($id, $uid)
    {
        return ItemHistory::where('id', $id)
                        ->where('uid', $uid)
                        ->where('item_type', 'rare_item')
                        ->update([
                            'status' => 'used',
                            'send_type' => 'owner'
                        ]);
    }

    private function deductSendGift($id, $uid, $targetUid, $targetName)
    {
        return ItemHistory::where('id', $id)
                        ->where('uid', $uid)
                        ->where('item_type', 'rare_item')
                        ->update([
                            'status' => 'used',
                            'send_type' => 'gift',
                            'send_to_uid' => $targetUid,
                            'send_to_name' => $targetName
                        ]);
    }

    private function getMyRareItemsHistoryWithoutGift($uid, $ncid)
    {
        return ItemHistory::where('uid', $uid)
            ->where('ncid', $ncid)
            ->where('send_type', '!=', 'gift')
            ->where('item_type', 'rare_item')
            ->get();
    }

    private function checkGetPetHistory($uid, $ncid, $petId)
    {
        return ItemHistory::where('product_id', $petId)
            ->where('uid', $uid)
            ->where('ncid', $ncid)
            ->where('status', 'used')
            ->where('send_type', 'owner')
            ->where('item_type', 'rare_item')
            ->get();
    }

    private function addItemLogHistory(array $arr)
    {
        return ItemHistory::create($arr);
    }

    private function updateItemLogHistory($log_id, $uid, $arr)
    {
        return ItemHistory::where('id', $log_id)->where('uid', $uid)->update($arr);
    }

    private function doRandomItem($available_soul = 1)
    {
        $rewarder = new Reward();
        $resp = [];
        for ($index = 0; $index < $available_soul; $index++) {
            $resp[] = $rewarder->getRandomRewardInfo();
        }

        return $resp;
    }

    private function apiDiamondHistory($uid,$ncid,$startDate,$endDate) {
        if(empty($ncid)){
            return false;
        }
        Cache::forget('BNS:SOUL_GACHAPON_VALENTINE_2019:EXCHANGE_TO_DIAMONDS_HISTORY_' . $uid);
        return Cache::remember('BNS:SOUL_GACHAPON_VALENTINE_2019:EXCHANGE_TO_DIAMONDS_HISTORY_' . $uid, 5, function() use($ncid,$startDate,$endDate) {
            return $this->post_api($this->baseApi, [
                        'key_name' => 'bns',
                        'service' => 'query_cash_txn',
                        'user_id' => $ncid, //ncid
                        'start_time' => $startDate,
                        'end_time' => $endDate
                    ]);
        });
    }

    private function diamondBalance(int $uid)
    {
        return $this->post_api($this->baseApi, [
            'key_name' => 'bns',
            'service' => 'balance',
            'uid' => $uid,
        ]);
    }

    private function setDiamondDeductData($deduct_diamond_amount = 0)
    {
        if ($deduct_diamond_amount == 0)
            return false;

        return [
            [
                'goods_id' => 741,
                'purchase_quantity' => $deduct_diamond_amount,
                'purchase_amount' => $deduct_diamond_amount,
                'category_id' => 40
            ]
        ];
    }

    private function deductDiamond(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Deduct diamomds for garena events bns soul gachapon valentine 2019',
            'currency_group_id' => 71,
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function dosendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Sending item from garena events bns soul gachapon valentine 2019',
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function addSendItemLog(array $arr)
    {
        if ($arr) {
            $resp = SendItemLog::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return null;
            }
        }
        return null;
    }

    private function updateSendItemLog(array $arr, $log_id)
    {
        if ($arr) {
            return SendItemLog::where('id', $log_id)->update($arr);
        }
        return null;
    }

    private function addDeductLog(array $arr)
    {
        if ($arr) {
            $resp = DeductLog::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return false; //fix
            }
        }
        return null;
    }

    private function updateDeductLog(array $arr, $log_id)
    {
        if ($arr) {
            return DeductLog::where('id', $log_id)->update($arr);
        }
        return null;
    }

}
