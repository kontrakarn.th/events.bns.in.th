<?php

namespace App\Http\Controllers\Api\Test\hey_duo;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

use App\Http\Controllers\Api\BnsEventController;
use App\Models\Test\hey_duo\Setting;
use App\Models\Test\hey_duo\Member;
use App\Models\Test\hey_duo\ItemLog;
use App\Models\Test\hey_duo\QuestDailyLog;
use App\Models\Test\hey_duo\Reward;
use App\Models\Test\hey_duo\DeductLog;
use App\Models\Test\hey_duo\DuoRequest;
use App\Models\Test\hey_duo\Quest;
use App\Jobs\hey_duo\CreateQuestDailyQueue;
use App\Jobs\hey_duo\CheckQuestDailyLogQueue;

class IndexController extends BnsEventController
{
    private $baseApi = 'http://api.apps.garena.in.th';

    public function __construct(Request $request)
    {
        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);

        $this->checkEventStatus();
    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status' => false,
                'type' => 'no_permission',
                'message' => 'Sorry, you do not have permission.'
            ]));
        }

        // get events setting info
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status' => false,
                'type' => 'maintenance',
                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
            ]));
        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                ]));
            }
        } else {
            die(json_encode([
                'status' => false,
                'type' => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function is_accepted_ip()
    {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'              => $uid,
                'username'         => $username,
                'ncid'             => $ncid,
                'char_id'          => 0,
                'char_name'        => '',
                'world_id'         => 0,
                'total_points'     => 0,
                'used_points'      => 0,
                'total_tokens'     => 0,
                'used_tokens'      => 0,
                'already_purchased' => 0,
                'last_ip'          => $this->getIP(),
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
            $this->userEvent = $resp;
        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id)) {
            return false;
        }

        return Member::where('id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:HEY_DUO:LIVE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'user_info',
                'uid'      => $uid,
            ]);
            $userNcInfo       = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }
        });
    }

    private function doGetChar($uid, $ncid)
    { //new fixed function
        $resp_raw = $this->apiCharacters($uid, $ncid);

        $resp = json_decode($resp_raw);
        if (is_object($resp)) {
            if ($resp->status) {
                $content = [];
                $i       = 1;
                foreach ($resp->response as $key => $value) {
                    $content[] = [
                        'id' => $i,
                        'char_id'   => $value->id,
                        'char_name' => $value->name,
                        'world_id' => $value->world_id,
                        'job' => $value->job,
                        'level' => $value->level,
                        'creation_time' => $value->creation_time,
                        'last_play_start' => $value->last_play_start,
                        'last_play_end' => $value->last_play_end,
                        'last_ip' => $value->last_ip,
                        'exp' => $value->exp,
                        'mastery_level' => $value->mastery_level,
                    ];

                    $i++;
                }
                return $content;
            }
        }
        return null;
    }

    private function apiCharacters($uid, $ncid)
    {
        return Cache::remember('BNS:HEY_DUO:LIVE:CHARACTERS_' . $uid, 10, function () use ($ncid) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'characters',
                'user_id'  => $ncid,
            ]);
        });
    }

    public function selectCharacter(Request $request)
    {
        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'select_character') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }
        if ($decoded->has('id') == false) { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter! (2)'
            ]);
        }

        $charKey = (int) $decoded['id'];

        $userData = $this->userData;
        $uid = $userData['uid'];

        $member = Member::where('uid', $uid)->first();

        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบข้อมูล'
            ]);
        }

        if ($member->char_id > 0) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถเปลี่ยนตัวละครได้'
            ]);
        }

        $mychar = $this->doGetChar($member->uid, $member->ncid);
        $myselect = collect($mychar)->where('id', $charKey)->first();
        if (count($myselect) == 0) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบตัวละคร'
            ]);
        }

        $member->char_id   = (int) $myselect['char_id'];
        $member->char_name = $myselect['char_name'];
        $member->level = $myselect['level'];
        $member->mastery_level = $myselect['mastery_level'];
        $member->world_id       = $myselect['world_id'];
        $member->code = $this->getDuoCode();
        $member->save();

        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] == false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message'],
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }

    private function getDuoCode()
    {
        $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $code = substr(str_shuffle($permitted_chars), 0, 6);
        $dupCodeMember = Member::where('code', $code)->first();

        while ($dupCodeMember) {
            $this->getDuoCode();
        }

        return $code;
    }

    private function apiSendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Sending item from garena events BNS Hey Duo.',
            'goods'                => json_encode($goodsData),
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function addItemHistoryLog($arr)
    {
        return ItemLog::create($arr);
    }

    private function updateItemHistoryLog($arr, $log_id, $uid)
    {
        return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
    }

    private function getEventSetting()
    {
        return Setting::where('active', 1)->first();
    }

    public function getEventInfo(Request $request)
    {

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($member->char_id !== 0 && !is_null($member->duo_uid)) {
            $this->createDailyQuestLog($member);
            $this->checkDailyQuestLog($member);
        }

        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] == false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message'],
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }

    private function setEventInfo($uid)
    {
        $member = Member::where('uid', $uid)->first();

        if (!$member) {
            return [
                'status' => false,
                'message' => 'No member data.'
            ];
        }

        if ($member->char_id == 0) {

            $charData = $this->doGetChar($member->uid, $member->ncid);

            $content = [];

            $content = collect($charData)
                ->filter(function ($obj) {
                    return $obj['level'] >= 60 && $obj['mastery_level'] >= 15;
                })
                ->map(function ($value) {
                    return [
                        'id' => $value['id'],
                        'char_name' => $value['char_name'],
                    ];
                })
                ->values();

            return [
                'status' => true,
                'username' => $member->username,
                'character_name' => '',
                'selected_char' => false,
                'characters' => $content,
                'points' => 0,
                'code' => null,
                'register_status' => 'can_register',
                'duo' => null,
                'daily_free_points_status' => 'not_in_condition',
                'bosskill_quests' => [],
                'area_quests' => [],
                'special_rewards' => [],
                'rewards' => [],
            ];
        }

        return [
            'status' => true,
            "message" => 'Event Info',
            'username' => $member->username,
            'character_name' => $member->char_name,
            'selected_char' => true,
            'characters' => [],
            'points' => $member->remaining_points,
            'code' => $member->code,
            'register_status' => $this->getRegisterStatus($member),
            'duo' => $this->getDuoInfo($member),
            'daily_free_points_status' => $this->getDailyFreePointsStatus($member),
            'bosskill_quests' => $this->getDailyBossKillQuests($member),
            'area_quests' => $this->getDailyAreaQuests($member),
            'special_rewards' => $this->getSpecialRewards($member),
            'rewards' => $this->getRewards($member),
        ];
    }

    private function createDailyQuestLog($member)
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $logDate = date('Y-m-d');

        $existQuests = QuestDailyLog::where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->where('log_date', $logDate)
            ->count();

        if ($existQuests > 0) {
            return false;
        }

        CreateQuestDailyQueue::dispatch($member, $logDate)->onQueue('bns_hey_duo_create_quest_daily_log_queue');
    }

    private function checkDailyQuestLog($member)
    {
        $setting = Setting::where('active', 1)->first();

        if (!$setting) {
            return false;
        }

        if (!(time() >= strtotime($setting->attend_start) && time() <= strtotime($setting->attend_end))) {
            return false;
        }

        $logDate = date('Y-m-d');

        $bossKillLogs = QuestDailyLog::where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->whereNotNull('quest_code')
            ->where('quest_type', 'daily_bosskill')
            ->where('status', 'pending')
            ->where('log_date', $logDate)
            ->get();

        foreach ($bossKillLogs as $dailyLog) {
            CheckQuestDailyLogQueue::dispatch($dailyLog, $logDate)->onQueue('bns_hey_duo_check_boss_kill_queue');
        }

        $areaQuestLogs = QuestDailyLog::where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->whereNotNull('quest_code')
            ->where('quest_type', 'daily_area')
            ->where('status', 'pending')
            ->where('log_date', $logDate)
            ->get();

        foreach ($areaQuestLogs as $dailyLog) {
            CheckQuestDailyLogQueue::dispatch($dailyLog, $logDate)->onQueue('bns_hey_duo_check_area_quest_queue');
        }
    }

    private function getSpecialRewards($member)
    {
        $claimStartSpecialRewards = Reward::where('package_type', 'claim_start_special')->get()
            ->map(function ($reward) use ($member) {
                return [
                    'reward_id' => $reward->id,
                    'reward_name' => $reward->package_name,
                    'required_points' => $reward->required_points,
                    'required_diamonds' => $reward->required_diamonds,
                    'can_claim' => $this->checkExchangePeriod()
                        && $member->remaining_points >= $reward->required_points
                        && (time() >= strtotime($reward->available_date))
                ];
            });

        $claimLimitSpecialRewards = Reward::where('package_type', 'claim_limit_special')->get()
            ->map(function ($reward) use ($member) {
                $limitCount = ItemLog::where('uid', $member->uid)
                    ->where('reward_id', $reward->id)
                    ->where('package_type', 'claim_limit_special')
                    ->where('status', 'success')
                    ->count();

                return [
                    'reward_id' => $reward->id,
                    'reward_name' => $reward->package_name,
                    'required_points' => $reward->required_points,
                    'required_diamonds' => $reward->required_diamonds,
                    'limit' => $reward->limit,
                    'limit_count' => $limitCount,
                    'can_claim' => $this->checkExchangePeriod()
                        && $member->remaining_points >= $reward->required_points
                        && $limitCount < $reward->limit
                ];
            });

        return [
            'claim_start_special' => $claimStartSpecialRewards,
            'claim_limit_special' => $claimLimitSpecialRewards,
        ];
    }

    private function getRewards($member)
    {
        $claimStartRewards = Reward::where('package_type', 'claim_start')->get()
            ->map(function ($reward) use ($member) {
                return [
                    'reward_id' => $reward->id,
                    'reward_name' => $reward->package_name,
                    'required_points' => $reward->required_points,
                    'can_claim' => $this->checkExchangePeriod()
                        && $member->remaining_points >= $reward->required_points
                        && (time() >= strtotime($reward->available_date))
                ];
            });

        $claimLimitRewards = Reward::where('package_type', 'claim_limit')->get()
            ->map(function ($reward) use ($member) {
                $limitCount = ItemLog::where('uid', $member->uid)
                    ->where('reward_id', $reward->id)
                    ->where('package_type', 'claim_limit')
                    ->where('status', 'success')
                    ->count();

                return [
                    'reward_id' => $reward->id,
                    'reward_name' => $reward->package_name,
                    'required_points' => $reward->required_points,
                    'limit' => $reward->limit,
                    'limit_count' => $limitCount,
                    'can_claim' => $this->checkExchangePeriod()
                        && $member->remaining_points >= $reward->required_points
                        && $limitCount < $reward->limit
                ];
            });

        $claimUnlimitRewards = Reward::where('package_type', 'claim')->get()
            ->map(function ($reward) use ($member) {
                return [
                    'reward_id' => $reward->id,
                    'reward_name' => $reward->package_name,
                    'required_points' => $reward->required_points,
                    'can_claim' => $this->checkExchangePeriod()
                        && $member->remaining_points >= $reward->required_points
                ];
            });

        return [
            'claim_start' => $claimStartRewards,
            'claim_limit' => $claimLimitRewards,
            'claim_unlimit' => $claimUnlimitRewards,
        ];
    }

    private function checkExchangePeriod()
    {
        $eventSetting = $this->getEventSetting();

        return time() >= strtotime($eventSetting->exchange_start)
            && time() <= strtotime($eventSetting->exchange_end);
    }

    private function getCompletedQuestsCount($member)
    {
        return QuestDailyLog::where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->whereIn('quest_type', [QuestDailyLog::QUEST_TYPE_DAILY_BOSSKILL, QuestDailyLog::QUEST_TYPE_DAILY_AREA])
            ->where('completed_count', '>', 0)
            ->where('status', 'success')
            ->where('log_date', date('Y-m-d'))
            ->count();
    }

    private function getPendingFreePointsCount($member)
    {
        return QuestDailyLog::where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->where('quest_type', QuestDailyLog::QUEST_TYPE_DAILY_FREE_POINTS)
            ->where('claimed_count', 0)
            ->where('claim_status', 'pending')
            ->where('log_date', date('Y-m-d'))
            ->count();
    }

    private function getDailyFreePointsStatus($member)
    {
        $completedQuestsCount = $this->getCompletedQuestsCount($member);
        $pendingFreePointsCount = $this->getPendingFreePointsCount($member);

        if ($completedQuestsCount <= 0) {
            return 'not_in_condition';
        }

        if ($pendingFreePointsCount <= 0) {
            return 'claimed';
        }

        if ($completedQuestsCount > 0 && $pendingFreePointsCount > 0) {
            return 'can_claim';
        }
    }

    private function getQuestStatus($log)
    {
        if ($log->completed_count <= 0) {
            return 'not_in_condition';
        }

        if ($log->claimed_count > 0) {
            return 'claimed';
        }

        if ($log->completed_count > 0) {
            return 'can_claim';
        }
    }

    private function getDuoQuestStatus($log, $duoLog)
    {
        if (!$duoLog || $log->completed_count <= 0 || $duoLog->completed_count <= 0) {
            return 'not_in_condition';
        }

        if ($log->duo_claimed_count > 0) {
            return 'claimed';
        }

        if ($duoLog->completed_count > 0) {
            return 'can_claim';
        }
    }

    private function getDailyBossKillQuests($member)
    {
        $logDate = date('Y-m-d');
        $bosskillLogs = QuestDailyLog::where('quest_type', QuestDailyLog::QUEST_TYPE_DAILY_BOSSKILL)
            ->where('uid', $member->uid)
            ->where('log_date', $logDate)
            ->get()
            ->map(function ($obj) use ($member, $logDate) {
                $duoBossKillLog = QuestDailyLog::where('quest_id', $obj->quest_id)
                    ->where('quest_type', QuestDailyLog::QUEST_TYPE_DAILY_BOSSKILL)
                    ->where('uid', $member->duo_uid)
                    ->where('log_date', $logDate)
                    ->first();

                return [
                    'id' => $obj->quest_id,
                    'title' => $obj->quest_title,
                    'dungeon' => $obj->quest_dungeon,
                    'points' => $obj->quest_points,
                    'status' => $this->getQuestStatus($obj),
                    'duo_status' => $this->getDuoQuestStatus($obj, $duoBossKillLog),
                ];
            });

        if (!count($bosskillLogs)) {
            $bosskillLogs = Quest::where('quest_type', QuestDailyLog::QUEST_TYPE_DAILY_BOSSKILL)
                ->get()
                ->map(function ($obj) {
                    return [
                        'id' => $obj->quest_id,
                        'title' => $obj->quest_title,
                        'dungeon' => $obj->quest_dungeon,
                        'points' => $obj->quest_points,
                        'status' => 'not_in_condition',
                        'duo_status' => 'not_in_condition',
                    ];
                });
        }

        return $bosskillLogs;
    }

    private function getDailyAreaQuests($member)
    {
        $logDate = date('Y-m-d');
        $bosskillLogs = QuestDailyLog::where('quest_type', QuestDailyLog::QUEST_TYPE_DAILY_AREA)
            ->where('uid', $member->uid)
            ->where('log_date', $logDate)
            ->get()
            ->map(function ($obj) use ($member, $logDate) {
                $duoAreaLog = QuestDailyLog::where('quest_id', $obj->quest_id)
                    ->where('quest_type', QuestDailyLog::QUEST_TYPE_DAILY_AREA)
                    ->where('uid', $member->duo_uid)
                    ->where('log_date', $logDate)
                    ->first();

                return [
                    'id' => $obj->quest_id,
                    'title' => $obj->quest_title,
                    'dungeon' => $obj->quest_dungeon,
                    'points' => $obj->quest_points,
                    'status' => $this->getQuestStatus($obj),
                    'duo_status' => $this->getDuoQuestStatus($obj, $duoAreaLog),
                ];
            });

        if (!count($bosskillLogs)) {
            $bosskillLogs = Quest::where('quest_type', QuestDailyLog::QUEST_TYPE_DAILY_AREA)
                ->get()
                ->map(function ($obj) {
                    return [
                        'id' => $obj->quest_id,
                        'title' => $obj->quest_title,
                        'dungeon' => $obj->quest_dungeon,
                        'points' => $obj->quest_points,
                        'status' => 'not_in_condition',
                        'duo_status' => 'not_in_condition',
                    ];
                });
        }

        return $bosskillLogs;
    }

    private function getDuoInfo($member)
    {
        if (is_null($member->duo_uid)) {
            return null;
        }

        $duo = Member::where('uid', $member->duo_uid)->first();
        if (!$duo) {
            return null;
        }

        return [
            'char_name' => $duo->char_name,
            'level' => $duo->level,
            'mastery_level' => $duo->mastery_level,
            'world' => $this->getServerName($duo->world_id)
        ];
    }

    private function getRegisterStatus($member)
    {
        if (is_null($member->code)) {
            return 'can_register';
        }

        if (!is_null($member->code) && is_null($member->duo_uid)) {
            return 'not_completed_register';
        }

        if (!is_null($member->code) && !is_null($member->duo_uid)) {
            return 'completed_register';
        }
    }

    private function createDeductLog(array $arr)
    {
        if ($arr) {
            $resp = DeductLog::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return false; //fix
            }
        }
        return null;
    }

    private function updateDeductLog(array $arr, $log_id)
    {
        if ($arr) {
            return DeductLog::where('id', $log_id)->update($arr);
        }
        return null;
    }

    private function apiDiamondBalance(int $uid)
    {
        return $this->post_api($this->baseApi, [
            'key_name' => 'bns',
            'service' => 'balance',
            'uid' => $uid,
        ]);
    }

    private function setDiamondsBalance($uid)
    {

        $diamondBalanceRaw = $this->apiDiamondBalance($uid);
        $diamondBalance = json_decode($diamondBalanceRaw);

        $diamonds = 0;
        if ($diamondBalance->status == true) {
            $diamonds = intval($diamondBalance->balance);
        }

        return $diamonds;
    }

    private function apiDeductDiamond(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Deduct diamomds for garena events BNS 3 Years Anniversary Passport.',
            'currency_group_id' => 71,
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function setDiamondDeductData($deduct_diamond_amount = 0)
    {
        if ($deduct_diamond_amount == 0)
            return false;

        return [
            [
                'goods_id' => 741,
                'purchase_quantity' => $deduct_diamond_amount,
                'purchase_amount' => $deduct_diamond_amount,
                'category_id' => 40
            ]
        ];
    }

    private function getServerName($world_id)
    {
        switch ($world_id) {
            case 7801:
                return 'อุนกุก';
            case 7803:
                return 'ทาลัน';
            case 7804:
                return 'นาริว';
            default:
                return '';
        }
    }

    public function getDuoInfoConfirm(Request $request)
    {
        $code = $request->input('code');
        $duo = Member::where('char_id', '!=', 0)->where('code', $code)->first();

        if (!$duo) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบข้อมูลตัวละคร'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $member = Member::where('uid', $uid)->first();

        if ($duo->uid == $member->uid) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถส่งคำขอเป็นคู่หูตัวเองได้'
            ]);
        }

        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] === false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message']
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => "ยืนยันการส่งคำขอเป็นคู่หู คู่ซี้ ไปยัง<br>{$duo->char_name} เลเวล {$duo->level} ฮงมุน {$duo->mastery_level} {$this->getServerName($duo->world_id)}",
            'data' => $eventInfo
        ]);
    }

    public function requestDuo(Request $request)
    {
        $userData = $this->userData;
        $uid = $userData['uid'];
        $member = Member::where('uid', $uid)->first();
        if (!$member) {
            return response()->json([
                'status' => false,
                'message' => 'No member data'
            ]);
        }

        if (!is_null($member->duo_uid)) {
            return response()->json([
                'status' => false,
                'message' => $member->char_name . ' มีคู่หูแล้ว'
            ]);
        }

        $code = $request->input('code');
        $duo = Member::where('char_id', '!=', 0)->where('code', $code)->first();

        if (!$duo) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบข้อมูลตัวละคร'
            ]);
        }

        if ($duo->uid == $member->uid) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถส่งคำขอเป็นคู่หูตัวเองได้'
            ]);
        }

        $existDuoRequest = DuoRequest::where('sender_uid', $member->uid)
            ->where('receiver_uid', $duo->uid)
            ->where('status', DuoRequest::STATUS_PENDING)
            ->first();
        if ($existDuoRequest) {
            return response()->json([
                'status' => false,
                'message' => 'ส่งคำขอไปแล้ว'
            ]);
        }

        DuoRequest::create([
            'sender_uid' => $member->uid,
            'sender_code' => $member->code,
            'sender_name' => $member->char_name,
            'sender_level' => $member->level,
            'sender_mastery_level' => $member->mastery_level,
            'sender_world_id' => $member->world_id,
            'receiver_uid' => $duo->uid,
            'receiver_code' => $duo->code,
            'receiver_name' => $duo->char_name,
            'receiver_level' => $duo->level,
            'receiver_mastery_level' => $duo->mastery_level,
            'receiver_world_id' => $duo->world_id,
            'status' => DuoRequest::STATUS_PENDING,
        ]);

        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] === false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message']
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'ส่งคำขอเป็นคู่หู คู่ซี้ สำเร็จแล้ว',
            'data' => $eventInfo
        ]);
    }

    public function getDuoRequests()
    {
        $userData = $this->userData;
        $uid = $userData['uid'];
        $member = Member::where('uid', $uid)->first();
        if (!$member) {
            return response()->json([
                'status' => false,
                'message' => 'No member data'
            ]);
        }

        $duoRequests = $this->getDuoRequestList($member);

        return response()->json([
            'status' => true,
            'data' => [
                'duo_requests' => $duoRequests
            ]
        ]);
    }

    private function getDuoRequestList($member)
    {
        return DuoRequest::where('sender_uid', $member->uid)
            ->orderByRaw("FIELD(status, 'accepted', 'pending', 'rejected')")
            ->orderBy('created_at', 'desc')
            ->get()
            ->map(function ($obj) {
                return [
                    'id' => $obj->id,
                    'receiver_code' => $obj->receiver_code,
                    'receiver_name' => $obj->receiver_name,
                    'receiver_level' => $obj->receiver_level . '/' . $obj->receiver_mastery_level,
                    'receiver_world' => $this->getServerName($obj->receiver_world_id),
                    'status' => $obj->status
                ];
            });
    }

    public function deleteDuoRequest($id)
    {
        $userData = $this->userData;
        $uid = $userData['uid'];
        $member = Member::where('uid', $uid)->first();
        if (!$member) {
            return response()->json([
                'status' => false,
                'message' => 'No member data'
            ]);
        }

        $duoRequest = DuoRequest::where('id', $id)
            ->where('sender_uid', $member->uid)
            ->where('status', DuoRequest::STATUS_PENDING)
            ->first();

        if (!$duoRequest) {
            return response()->json([
                'status' => false,
                'message' => 'No duo request data'
            ]);
        }

        $duoRequest->delete();

        $duoRequests = $this->getDuoRequestList($member);

        return response()->json([
            'status' => true,
            'data' => [
                'duo_requests' => $duoRequests,
            ]
        ]);
    }

    private function getDuoReceiveList($member)
    {
        return DuoRequest::where('receiver_uid', $member->uid)
            ->orderByRaw("FIELD(status, 'accepted', 'pending', 'rejected')")
            ->orderBy('created_at', 'desc')
            ->get()
            ->map(function ($obj) {
                return [
                    'id' => $obj->id,
                    'sender_code' => $obj->sender_code,
                    'sender_name' => $obj->sender_name,
                    'sender_level' => $obj->sender_level . '/' . $obj->sender_mastery_level,
                    'sender_world' => $this->getServerName($obj->sender_world_id),
                    'status' => $obj->status
                ];
            });
    }

    public function getDuoReceives()
    {
        $userData = $this->userData;
        $uid = $userData['uid'];
        $member = Member::where('uid', $uid)->first();
        if (!$member) {
            return response()->json([
                'status' => false,
                'message' => 'No member data'
            ]);
        }

        $duoReceives = $this->getDuoReceiveList($member);

        return response()->json([
            'status' => true,
            'data' => [
                'duo_receives' => $duoReceives,
            ]
        ]);
    }

    public function acceptDuoReceive($id)
    {
        $userData = $this->userData;
        $uid = $userData['uid'];
        $member = Member::where('uid', $uid)->first();
        if (!$member) {
            return response()->json([
                'status' => false,
                'message' => 'No member data'
            ]);
        }

        if (!is_null($member->duo_uid)) {
            return response()->json([
                'status' => false,
                'message' => $member->char_name . ' มีคู่หูแล้ว'
            ]);
        }

        $duoRequest = DuoRequest::where('id', $id)
            ->where('receiver_uid', $member->uid)
            ->where('status', DuoRequest::STATUS_PENDING)
            ->first();

        if (!$duoRequest) {
            return response()->json([
                'status' => false,
                'message' => 'No duo request data'
            ]);
        }

        $duo = Member::where('uid', $duoRequest->sender_uid)->first();
        if (!$duo) {
            return response()->json([
                'status' => false,
                'message' => 'No duo data'
            ]);
        }

        $duoRequest->update(['status' => DuoRequest::STATUS_ACCEPTED]);
        $member->update(['duo_uid' => $duoRequest->sender_uid]);
        $duo->update(['duo_uid' => $member->uid]);

        $duoReceives = $this->getDuoReceiveList($member);

        return response()->json([
            'status' => true,
            'data' => [
                'duo_receives' => $duoReceives
            ]
        ]);
    }

    public function rejectDuoReceive($id)
    {
        $userData = $this->userData;
        $uid = $userData['uid'];
        $member = Member::where('uid', $uid)->first();
        if (!$member) {
            return response()->json([
                'status' => false,
                'message' => 'No member data'
            ]);
        }

        if (!is_null($member->duo_uid)) {
            return response()->json([
                'status' => false,
                'message' => $member->char_name . ' มีคู่หูแล้ว'
            ]);
        }

        $duoRequest = DuoRequest::where('id', $id)
            ->where('receiver_uid', $member->uid)
            ->where('status', DuoRequest::STATUS_PENDING)
            ->first();

        if (!$duoRequest) {
            return response()->json([
                'status' => false,
                'message' => 'No duo request data'
            ]);
        }

        $duoRequest->update(['status' => DuoRequest::STATUS_REJECTED]);

        $duoReceives = $this->getDuoReceiveList($member);

        return response()->json([
            'status' => true,
            'data' => [
                'duo_receives' => $duoReceives
            ]
        ]);
    }

    private function checkAttendEventPeriod()
    {
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->attend_start) || time() > strtotime($eventSetting->attend_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }
    }

    public function claimFreePoints()
    {
        $this->checkAttendEventPeriod();

        $userData = $this->userData;
        $uid = $userData['uid'];

        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $freeDailyLog = QuestDailyLog::where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->where('quest_type', 'daily_free_points')
            ->where('claimed_count', 0)
            ->where('claim_status', 'pending')
            ->where('log_date', date('Y-m-d'))
            ->first();

        if (!$freeDailyLog) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย คุณรับคะแนนฟรีของวันนี้ไปแล้ว'
            ]);
        }

        $completedQuestsCount = $this->getCompletedQuestsCount($member);
        if ($completedQuestsCount <= 0) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย ไม่ตรงเงื่อนไข'
            ]);
        }

        $questPoints = $freeDailyLog->quest_points;
        if (date('Y-m-d') == '2020-06-11') {
            $questPoints = $questPoints * 2;
        }

        $member->total_points = $member->total_points + $questPoints;

        $freeDailyLog->claimed_count = 1;
        $freeDailyLog->claim_status = 'claimed';
        $freeDailyLog->save();

        if ($member->save()) {
            return response()->json([
                'status' => true,
                'message' => 'ได้รับ <span>คะแนนประจำวัน</span> ' . $questPoints . ' คะแนน',
                'data' => $this->setEventInfo($uid)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Fail updating member point'
            ]);
        }
    }

    public function claimQuestPoints(Request $request)
    {
        $this->checkAttendEventPeriod();

        $questId = $request->input('id');
        if (!$questId) {
            return response()->json([
                'status' => false,
                'message' => 'Invalid argument id'
            ], 400);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $dailyLog = QuestDailyLog::where('quest_id', $questId)
            ->where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->where('completed_count', '>', 0)
            ->where('status', 'success')
            ->where('log_date', date('Y-m-d'))
            ->first();

        if (!$dailyLog) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย ไม่ตรงเงื่อนไข'
            ]);
        }

        $type = $request->input('type');

        if ($type === 'duo') {
            $res = $this->claimDuoQuestPoints($questId, $member, $dailyLog);

            if ($res['status'] === false) {
                return response()->json([
                    'status' => false,
                    'message' => $res['message']
                ]);
            }
        } else {
            if ($dailyLog->claimed_count > 0 || $dailyLog->claim_status === 'claimed') {
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย รับคะแนนวันนี้ไปแล้ว'
                ]);
            }

            $dailyLog->claimed_count = 1;
            $dailyLog->claim_status = 'claimed';
            $dailyLog->save();
        }

        $reward = Reward::where('package_type', 'package')->first();
        $this->sendItem($member, $reward, $type);

        $questPoints = $dailyLog->quest_points;
        if (date('Y-m-d') == '2020-06-11') {
            $questPoints = $questPoints * 2;
        }

        $member->total_points = $member->total_points + $questPoints;

        if ($member->save()) {
            return response()->json([
                'status' => true,
                'message' => 'ได้รับ <span>คะแนน</span> ' . $questPoints . ' คะแนน',
                'data' => $this->setEventInfo($uid)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Fail updating member point'
            ]);
        }
    }

    private function claimDuoQuestPoints($questId, $member, $dailyLog)
    {
        $duo = Member::where('uid', $member->duo_uid)->first();
        $completedDuoDailyLog = QuestDailyLog::where('quest_id', $questId)
            ->where('uid', $duo->uid)
            ->where('char_id', $duo->char_id)
            ->where('completed_count', '>', 0)
            ->where('status', 'success')
            ->where('log_date', date('Y-m-d'))
            ->first();

        if (!$completedDuoDailyLog) {
            return [
                'status' => false,
                'message' => 'ขออภัย ไม่ตรงเงื่อนไขคู่หู'
            ];
        }

        if ($dailyLog->duo_claimed_count > 0 || $dailyLog->duo_claim_status === 'claimed') {
            return [
                'status' => false,
                'message' => 'ขออภัย รับคะแนนคู่หูวันนี้ไปแล้ว'
            ];
        }

        $dailyLog->duo_claimed_count = 1;
        $dailyLog->duo_claim_status = 'claimed';
        $dailyLog->save();
    }

    private function getPackageName($member, $reward, $type)
    {
        switch ($reward->package_type) {
            case 'claim_limit':
                $itemLogsCount = ItemLog::where('uid', $member->uid)
                    ->where('reward_id', $reward->id)
                    ->where('package_type', 'claim_limit')
                    ->where('status', 'success')
                    ->count() + 1;
                return $reward->package_name . ' ธรรมดา ' . '(' . $itemLogsCount . '/' . $reward->limit . ')';

            case 'claim_limit_special':
                $itemLogsCount = ItemLog::where('uid', $member->uid)
                    ->where('reward_id', $reward->id)
                    ->where('package_type', 'claim_limit_special')
                    ->where('status', 'success')
                    ->count() + 1;
                return $reward->package_name . ' พิเศษ ' . '(' . $itemLogsCount . '/' . $reward->limit . ')';

            case 'claim_start':
                return $reward->package_name . ' ธรรมดา';

            case 'claim_start_special':
                return $reward->package_name . ' พิเศษ';

            case 'package':
                return $type === 'duo' ? $reward->package_name . ' (คู่หูคู่ซี้)' : $reward->package_name;

            default:
                return $reward->package_name;
        }
    }

    private function sendItem($member, $reward, $type = null)
    {
        $goods_data = [];

        $quantity = $reward->package_quantity;

        $doubleCondition = date("Y-m-d") == "2020-06-11" && $reward->package_type == 'package';
        if ($doubleCondition) {
            $quantity = $quantity * 2;
        }

        $goods_data[] = [
            'goods_id' => $reward->package_id,
            'purchase_quantity' => $quantity,
            'purchase_amount' => 0,
            'category_id' => 40,
        ];

        $packageName = $this->getPackageName($member, $reward, $type);

        if ($doubleCondition) {
            $packageName = str_replace('x1', 'x2', $packageName);
        }

        $sendItemLog = $this->addItemHistoryLog([
            'uid'                           => $member->uid,
            'username'                      => $member->username,
            'ncid'                          => $member->ncid,
            'reward_id'                     => $reward->id,
            'package_name'                  => $packageName,
            'package_quantity'              => $quantity,
            'package_amount'                => $reward->package_amount,
            'package_type'                  => $reward->package_type,
            'send_item_status'              => false,
            'send_item_purchase_id'         => 0,
            'send_item_purchase_status'     => 0,
            'goods_data'                    => json_encode($goods_data),
            'status'                        => 'pending',
            'log_date'                      => (string) date('Y-m-d'), // set to string
            'log_date_timestamp'            => time(),
            'last_ip'                       => $this->getIP(),
        ]);

        $ncid = $this->getNcidByUid($member->uid);
        $send_result_raw = $this->apiSendItem($ncid, $goods_data);
        $send_result = json_decode($send_result_raw);

        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            $this->updateItemHistoryLog([
                'send_item_status' => $send_result->status ?: false,
                'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                'status' => 'success'
            ], $sendItemLog['id'], $member->uid);

            return true;
        } else {
            $this->updateItemHistoryLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
            ], $sendItemLog['id'], $member->uid);

            return false;
        }
    }

    public function exchangeReward(Request $request)
    {
        if (!$this->checkExchangePeriod()) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $rewardId = $request->input('id');
        $reward = Reward::find($rewardId);
        if (!$reward) {
            return response()->json([
                'status' => false,
                'message' => 'No reward.'
            ]);
        }

        $check = $this->checkCanExchangeReward($member, $reward);
        if (!$check['status']) {
            return response()->json([
                'status' => false,
                'message' => $check['message']
            ]);
        }

        if ($member->remaining_points < $reward->required_points) {
            return response()->json([
                'status' => false,
                'message' => 'คะแนนไม่เพียงพอ'
            ]);
        }

        $res = $this->sendItem($member, $reward);

        if ($res) {
            $member->used_points = $member->used_points + $reward->required_points;
            $member->save();

            $packageName = $reward->package_name;

            return response()->json([
                'status' => true,
                'message' => "ได้รับ <span>" . $packageName . "</span> <br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                'data' => $this->setEventInfo($uid)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า"
            ]);
        }
    }

    public function exchangeSpecialReward(Request $request)
    {
        if (!$this->checkExchangePeriod()) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        $userData = $this->userData;
        $uid = $userData['uid'];

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $rewardId = $request->input('id');
        $reward = Reward::where('id', $rewardId)->whereIn('package_type', ['claim_start_special', 'claim_limit_special'])->first();
        if (!$reward) {
            return response()->json([
                'status' => false,
                'message' => 'No reward.'
            ]);
        }

        $check = $this->checkCanExchangeReward($member, $reward);
        if (!$check['status']) {
            return response()->json([
                'status' => false,
                'message' => $check['message']
            ]);
        }

        if ($member->remaining_points < $reward->required_points) {
            return response()->json([
                'status' => false,
                'message' => 'คะแนนไม่เพียงพอ'
            ]);
        }

        // check diamonds balance
        $diamondBalance = $this->setDiamondsBalance($member->uid);
        if ($diamondBalance < $reward->required_diamonds) {
            return response()->json([
                'status' => false,
                'message' => 'ไดมอนด์ไม่เพียงพอ'
            ]);
        }

        // set deduct diamonds
        $logDeduct = $this->createDeductLog([
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'deduct_type' => $reward->package_type,
            'diamonds' => $reward->required_diamonds,
            'status' => 'pending',
            'last_ip' => $this->getIP(),
            'log_date' => (string) date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
        ]);

        if (!$logDeduct) {
            return response()->json([
                'status' => false,
                'message' => 'Error creating deduct log'
            ]);
        }

        // set desuct diamonds
        $deductData = $this->setDiamondDeductData($reward->required_diamonds);

        $resp_deduct_raw = $this->apiDeductDiamond($member->ncid, $deductData);
        $resp_deduct = json_decode($resp_deduct_raw);

        if (!is_object($resp_deduct) || is_null($resp_deduct)) {
            return response()->json([
                'status' => false,
                'message' => 'Error deduct diamond'
            ]);
        }

        $this->updateDeductLog([
            'before_deduct_diamond' => $diamondBalance,
            'after_deduct_diamond' => $diamondBalance - $reward->required_diamonds,
            'deduct_status' => $resp_deduct->status ?: 0,
            'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
            'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
            'deduct_data' => json_encode($deductData),
            'status' => 'success'
        ], $logDeduct['id']);

        $res = $this->sendItem($member, $reward);

        if ($res) {
            $member->used_points = $member->used_points + $reward->required_points;
            $member->save();

            $packageName = $reward->package_name;

            return response()->json([
                'status' => true,
                'message' => "ได้รับ <span>" . $packageName . "</span> <br />ไอเทมจะส่งเข้าไปในกล่องจดหมาย<br />กรุณาเข้าเกมเพื่อกดรับ",
                'data' => $this->setEventInfo($uid)
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า"
            ]);
        }
    }

    private function checkCanExchangeReward($member, $reward)
    {
        $limitPackageType = ['claim_limit', 'claim_limit_special'];

        if (in_array($reward->package_type, $limitPackageType)) {
            $itemLogsCount = ItemLog::where('uid', $member->uid)
                ->where('reward_id', $reward->id)
                ->whereIn('package_type', $limitPackageType)
                ->where('status', 'success')
                ->count();

            if ($itemLogsCount >= $reward->limit) {
                return [
                    'status' => false,
                    'message' => 'เกินจำนวนครั้งที่สามารถแลกไอเทมได้'
                ];
            }
        }

        if (in_array($reward->package_type, ['claim_start', 'claim_start_special'])) {
            if ((time() < strtotime($reward->available_date))) {
                return [
                    'status' => false,
                    'message' => 'ยังไม่ถึงเวลาที่สามารถแลกไอเทมได้'
                ];
            }
        }

        return [
            'status' => true,
            'message' => 'success'
        ];
    }

    public function getHistory()
    {
        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $pointHistories = $this->getPointsHistories($member);
        $exchangeRewardHistories = $this->getExchangeRewardHistories($member);
        $packageHistories = $this->getPackageHistories($member);

        return response()->json([
            'status' => true,
            'data' => [
                'points_histories' => $pointHistories,
                'exchange_reward_histories' => $exchangeRewardHistories,
                'package_histories' => $packageHistories,
            ]
        ]);
    }

    public function getExchangeRewardHistories($member)
    {
        return ItemLog::where('uid', $member->uid)
            ->where('package_type', '!=', 'package')
            ->where('status', 'success')
            ->orderBy('id', 'desc')
            ->get()
            ->map(function ($obj) {

                $createdAt = strtotime($obj->created_at);
                $title = "ได้รับ$obj->package_name";

                return [
                    'title' => $title,
                    'date' => date('d/m/Y', $createdAt),
                    'time' => date('H:i:s', $createdAt),
                ];
            });
    }

    public function getPackageHistories($member)
    {
        return ItemLog::where('uid', $member->uid)
            ->where('package_type', 'package')
            ->where('status', 'success')
            ->orderBy('id', 'desc')
            ->get()
            ->map(function ($obj) {
                $packageName = $obj->package_name;
                if (date('Y-m-d') == '2020-06-11') {
                    $packageName = str_replace('x1', 'x2', $packageName);
                }

                $createdAt = strtotime($obj->created_at);
                $title = "ได้รับ $packageName";

                return [
                    'title' => $title,
                    'date' => date('d/m/Y', $createdAt),
                    'time' => date('H:i:s', $createdAt),
                ];
            });
    }

    private function getPointsHistories($member)
    {
        $pointHistories = [];
        QuestDailyLog::where('uid', $member->uid)
            ->where('char_id', $member->char_id)
            ->where('claimed_count', '>', 0)
            ->where('claim_status', 'claimed')
            ->orderBy('id', 'desc')
            ->get()
            ->each(function ($obj) use (&$pointHistories) {
                $questPoints = $obj->quest_points;
                if (date('Y-m-d') == '2020-06-11') {
                    $questPoints = $questPoints * 2;
                }

                $createdAt = strtotime($obj->created_at);
                if ($obj->quest_type === 'daily_bosskill') {
                    $title = "กำจัดบอส {$obj->quest_title} ดันเจี้ยน{$obj->quest_dungeon} ได้รับ {$questPoints}คะแนน";
                } else if ($obj->quest_type === 'daily_area') {
                    $title = "ผ่านเควส พิชิต{$obj->quest_title} ได้รับ {$questPoints}คะแนน";
                } else if ($obj->quest_type === 'daily_free_points') {
                    $title = "ได้รับคะแนนประจำวัน {$questPoints}คะแนน";
                }

                $pointHistories[] = [
                    'title' => $title,
                    'date' => date('d/m/Y', $createdAt),
                    'time' => date('H:i:s', $createdAt),
                ];

                if ($obj->duo_claimed_count > 0) {
                    if ($obj->quest_type === 'daily_bosskill') {
                        $title = "รับคะแนนคู่หูคู่ซี้ กำจัดบอส {$obj->quest_title} ได้รับ {$questPoints}คะแนน";
                    } else {
                        $title = "รับคะแนนคู่หูคู่ซี้ พิชิต{$obj->quest_title} ได้รับ {$questPoints}คะแนน";
                    }

                    $pointHistories[] = [
                        'title' => $title,
                        'date' => date('d/m/Y', $createdAt),
                        'time' => date('H:i:s', $createdAt),
                    ];
                }
            });

        return $pointHistories;
    }
}
