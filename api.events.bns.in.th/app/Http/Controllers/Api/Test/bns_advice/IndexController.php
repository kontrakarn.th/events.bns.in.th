<?php

    namespace App\Http\Controllers\Api\Test\bns_advice;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\Test\bns_advice\Member;
    use App\Models\Test\bns_advice\Reward;
    use App\Models\Test\bns_advice\QuestLog;
    use App\Models\Test\bns_advice\ItemLog;
    use App\Models\Test\bns_advice\ItemCode;
    use App\Models\Test\bns_advice\ItemHistory;
    use App\Models\Test\bns_advice\PointHistory;
    use App\Models\Test\bns_advice\DeductPointLog;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2019-01-07 00:00:00';
        private $endTime = '2019-02-02 23:59:59';

        private $startApiTime = '06:00:00';
        private $endApiTime = '05:59:59';

        private $questList = [
            [
                'id' => 1,
                'quest_code' => 1819,
                'quest_title' => 'เกาะแห่งพันธนาการ',
            ],
            [
                'id' => 2,
                'quest_code' => 1784,
                'quest_title' => 'รังมังกรเพลิง',
            ],
            [
                'id' => 3,
                'quest_code' => 1478,
                'quest_title' => 'โรงหลอมอัคคี',
            ],
            [
                'id' => 4,
                'quest_code' => 1614,
                'quest_title' => 'โรงหลอมเครื่องจักร',
            ],
            [
                'id' => 5,
                'quest_code' => 1674,
                'quest_title' => 'หุบเขาอาถรรพ์',
            ],
        ];

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:BNS_ADVICE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        public function acceptChar(Request $request)
        {

            if($request->has('type') == false && $request->type != 'accept_character') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter(1)!'
                ]);
            }

            if ($request->has('id') == false) {
                return response()->json([
                            'status' => false,
                            'message' => 'invalid parameter(2)!'
                ]);
            }
            $char_id = $request->id;
            if (empty($char_id)) {
                return response()->json([
                            'status' => false,
                            'message' => 'ไม่มีตัวละครที่ต้องการเลือก'
                ]);
            }

            // $char_id = $request->id;

            $uid = $this->userData['uid'];
            $username = $this->userData['username'];

            if ($resp = $this->hasAcceptChar($uid) == false) {
                $ncid = $this->getNcidByUid($uid);
                $characters = $this->doGetChar($uid,$ncid);
                $data = $characters[$char_id-1];
                if (isset($data['char_id'])) {
                    $this->doAcceptChar($data['char_id'], $data['char_name'], $data['world_id'], $uid);
                    $eventInfo = $this->setEventInfo($uid);
                    return response()->json([
                                'status' => true,
                                'message' => 'ยืนยันตัวละครเสร็จสิ้น',
                                'content' => $eventInfo
                    ]);
                } else {
                    return response()->json([
                                'status' => false,
                                'message' => 'ไม่พบตัวละครที่คุณต้องการเลือก'
                    ]);
                }
            } else {
                return response()->json([
                            'status' => false,
                            'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก'
                ]);
            }
        }

        private function doAcceptChar($char_id, $char_name, $world_id, $uid)
        {
            return Member::where('uid', $uid)->update([
                        'char_id' => $char_id,
                        'char_name' => $char_name,
                        'world_id' => $world_id,
                    ]);
        }

        private function hasAcceptChar($uid)
        {
            $resp = Member::where('uid', $uid)->first();
            if (empty($resp->char_id) && empty($resp->char_name)) {
                return false;
            } else {
                return $resp;
            }
        }

        public function getCharacter(Request $request)
        {
            if($request->has('type') == false && $request->type != 'get_characters') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $this->checkMember();
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            $resp = $this->hasAcceptChar($uid);
            if ($resp == false) {
                $charactersList = [];
                $ncid = $this->getNcidByUid($uid);
                $characters = $this->doGetChar($uid,$ncid);
                foreach ($characters as $character) {
                    $charactersList[] = collect($character)->except(['char_id', 'world_id', 'job', 'level', 'creation_time', 'last_play_start', 'last_play_end', 'last_ip', 'exp', 'mastery_level']);
                }
                return response()->json([
                            'status' => true,
                            'content' => $charactersList,
                            'accepted' => false
                ]);
            } else {
                return response()->json([
                            'status' => true,
                            'message' => 'คุณได้ยืนยันตัวละครไปแล้วไม่สามารถยันยันได้อีก',
                            'accepted' => true
                ]);
            }
        }

        private function doGetChar($uid,$ncid){
            $resp_raw = $this->apiCharacters($uid, $ncid);

            $resp = json_decode($resp_raw);
            if (is_object($resp)) {
                if ($resp->status) {
                    $content = [];
                    $i =1;
                    foreach ($resp->response as $key => $value) {
                        $content[] = [
                            'id' => $i,
                            'char_id' => $value->id,
                            'char_name' => $value->name,
                            'world_id' => $value->world_id,
                            'job' => $value->job,
                            'level' => $value->level,
                            'creation_time' => $value->creation_time,
                            'last_play_start' => $value->last_play_start,
                            'last_play_end' => $value->last_play_end,
                            'last_ip' => $value->last_ip,
                            'exp' => $value->exp,
                            'mastery_level' => $value->mastery_level,
                        ];

                        $i++;
                    }
                    return $content;
                }
            }
            return null;
        }

        private function apiCharacters($uid, $ncid)
        {
            return Cache::remember('BNS:BNS_ADVICE:CHARACTERS_' . $uid, 10, function() use($ncid) {
                        return $this->post_api($this->baseApi, [
                                    'key_name' => 'bns',
                                    'service' => 'characters',
                                    'user_id' => $ncid
                        ]);
                    });
        }

        private function dosendItem(string $ncid, array $goodsData) {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Test sending item from garena events BNSxAdvice Promotion',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addSendItemLog(array $arr) {
            if ($arr) {
                $resp = ItemLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateSendItemLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function addItemHistoryLog(array $arr) {
            if ($arr) {
                $resp = ItemHistory::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateItemHistoryLog(array $arr, $logId, $uid) {
            if ($arr) {
                return ItemHistory::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function addPointHistoryLog(array $arr) {
            if ($arr) {
                $resp = PointHistory::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updatePointHistoryLog(array $arr, $logId, $uid) {
            if ($arr) {
                return PointHistory::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function addDeductPointLog(array $arr) {
            if ($arr) {
                $resp = DeductPointLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return null;
                }
            }
            return null;
        }

        private function updateDeductPointLog(array $arr, $logId, $uid) {
            if ($arr) {
                return DeductPointLog::where('id', $logId)->where('uid', $uid)->update($arr);
            }
            return null;
        }

        private function apiCheckQuestCompleted($uid, $charId, $questId, $start, $end) {
            $resp = Cache::remember('BNS:BNS_ADVICE:QUEST_COMPLETED_' . $questId . '_' . $uid, 5, function()use($charId,$questId,$start,$end) {
                return $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'quest_completed',
                    'char_id' => $charId,
                    'quest_id' => $questId,
                    'start_time' => $start,
                    'end_time' => $end
                ]);
            });

            return json_decode($resp);
        }

        private function setQuestCompletedCount($uid, $charId, $questId, $start, $end){

            $completedCount = 0;

            $apiCompletedCount = $this->apiCheckQuestCompleted($uid, $charId, $questId, $start, $end);
            if($apiCompletedCount->status){
                $completedCount = intval($apiCompletedCount->response->quest_completed_count);
            }

            return $completedCount;

        }

        public function getEventInfo(Request $request)
        {

            if ($request->has('type') == false && $request->type != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // $dateTime = $this->setQuestDateTime();
            // dd($dateTime);'

            // dd($this->checkItemCodeFormat('ADVI-CEVC-JDXB-NT43'));

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            if($eventInfo == false){
                $eventInfo = [];
            }

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
            ]);

        }

        private function setEventInfo($uid=null){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $dateTime = $this->setQuestDateTime();

            /* ///////// check Advice gachapon  /////// */
            // check all remain code
            $allNotUsedCode = ItemCode::where('status', 'not_use')->count();

            // check total code
            $totalCode = ItemCode::where('uid', $member->uid)
                                ->count();

            $usedCode = ItemCode::where('uid', $member->uid)
                                ->where('gahcapon_used', 'used')
                                ->count();

            // check remain redeem code
            $remainCode = ItemCode::where('uid', $member->uid)
                                ->where('gahcapon_used', 'not_use')
                                ->count();

            /* ///////// check Free Advice gachapon  /////// */
            
            // check quest completed and update quest point
            $quests = $this->getQuestCompleted($member->uid);
            
            // get total free gachapon from daily quest(exclude previous days free gachapon)
            $totalFreeGachapon = QuestLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            // get used free gachapon
            $usedFreeGachapon = QuestLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->where('gahcapon_used', 'used')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            // get not use free gachapon
            $remainFreeGachapon = QuestLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->where('gahcapon_used', 'not_use')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();


            /* ///////// Exchange reward points  /////// */
            // check remain advice points
            $remainPointsQuery = PointHistory::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->sum('points');

            $usedPoints = DeductPointLog::where('uid', $member->uid)
                                    ->where('status', 'success')
                                    ->sum('points');

            $remainPoints = ($remainPointsQuery - $usedPoints > 0) ? $remainPointsQuery - $usedPoints : 0;   

            return [
                'character' => $member->char_name,
                'username' => $member->username,
                'all_remain_code' => $allNotUsedCode,
                'can_redeem_advice_gachapon' => $totalCode < 5 && $allNotUsedCode > 0,
                'can_random_advice_gachapon' => $remainCode > 0,
                'remain_points' => $remainPoints,
                'total_advice_gachapon' => $totalCode,
                'remain_advice_gachapon' => $remainCode,
                'used_advice_gachapon' => $usedCode,
                'total_free_gachapon' => $totalFreeGachapon,
                'remain_free_gachapon' => $remainFreeGachapon,
                'used_free_gachapon' => $usedFreeGachapon,
                'can_random_free_gachapon' => $remainFreeGachapon > 0,
                'quests' => $quests,
            ];

        }

        private function getQuestCompleted($uid){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            $quests = [];

            $dateTime = $this->setQuestDateTime();

            foreach($this->questList as $quest){

                $questCompletedStatus = false;

                // check quest success log
                $questCompleted = QuestLog::where('uid', $member->uid)
                                        ->where('quest_code', $quest['quest_code'])
                                        ->where('status', 'success')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();
                if($questCompleted > 0){
                    $questCompletedStatus = true;
                }else{
                    // check from api
                    $questCompletedApi = $this->setQuestCompletedCount($member->uid, $member->char_id, $quest['quest_code'], $dateTime['start_date_time'], $dateTime['end_date_time']);
                    if($questCompletedApi > 0){

                        $questCompletedLog = QuestLog::where('uid', $member->uid)
                                                    ->where('quest_code', $quest['quest_code'])
                                                    ->where('status', 'success')
                                                    ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                                    ->count();
                        
                        if($questCompletedLog == 0){
                            // create quest log
                            QuestLog::create([
                                'uid' => $member->uid,
                                'username' => $member->username,
                                'ncid' => $member->ncid,
                                'char_id' => $member->char_id,
                                'char_name' => $member->char_name,
                                'quest_id' => $quest['id'],
                                'quest_code' => $quest['quest_code'],
                                'quest_title' => $quest['quest_title'],
                                'status' => 'success',
                                'log_date' => date('Y-m-d H:i:s'),
                                'log_date_timestamp' => time(),
                                'last_ip' => $this->getIP(),
                            ]);
                        }

                        $questCompletedStatus = true;
                        
                    }
                }     
                
                $quests[] = [
                    'title' => $quest['quest_title'],
                    'status' => $questCompletedStatus,
                ];

            }

            return $quests;
        }

        private function setQuestDateTime(){

            $currDate = date('Y-m-d');

            $startDatetime = '';
            $endDatetime = '';

            if(time() >= strtotime($currDate.' 00:00:00') && time() <= strtotime($currDate.' 05:59:59')){ // check current time after 00:00:00 and not over 05:59:59
                $startDatetime = date('Y-m-d', strtotime(' -1 day')).' 06:00:00';
                $endDatetime = date('Y-m-d').' 05:59:59';
            }else{
                $startDatetime = date('Y-m-d').' 06:00:00';
                $endDatetime = date('Y-m-d', strtotime(' +1 day')).' 05:59:59';
            }

            return [
                'start_date_time' => $startDatetime,
                'end_date_time' => $endDatetime,
            ];

        }

        public function redeemAdviceCode(Request $request){

            if ($request->has('type') == false && $request->type != 'redeem_advice_code') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }

            if ($request->has('code') == false || empty($request->code)) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (2)!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);
            $adviceCode = $request->code;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // validate item code
            $adviceCodeValidate = $this->checkItemCodeFormat($adviceCode);
            if($adviceCodeValidate == false){
                return response()->json([
                    'status'    => false,
                    'message'   => 'ขออภัย<br />ไอเทมโค้ดไม่ถูกต้อง'
                ]);
            }

            // check limit code
            $myTotalCode = ItemCode::where('uid', $member->uid)->count();
            if($myTotalCode >= 5){
                return response()->json([
                    'status'    => false,
                    'message'   => 'ขออภัย<br />สารมารถเติมไอเทมโค้ดได้สูงสุด<br />5 โค้ดต่อไอดี'
                ]);
            }

            // check item code has used
            $checkItemCode = ItemCode::where('code', $adviceCode)->count();
            if($checkItemCode == 0){
                return response()->json([
                    'status'    => false,
                    'message'   => 'ขออภัย<br />ไอเทมโค้ดไม่ถูกต้อง'
                ]);
            }

            // check item code has used
            $checkUsedItemCode = ItemCode::where('code', $adviceCode)->where('status', 'used')->count();
            if($checkUsedItemCode > 0){
                return response()->json([
                    'status'    => false,
                    'message'   => 'ขออภัย<br />ไอเทมโค้ดนี้ถูกใช้ไปแล้ว'
                ]);
            }

            // match item code to uid
            $update = ItemCode::where('code', $adviceCode)
                                ->where('status', 'not_use')
                                ->update([
                                    'uid' => $member->uid,
                                    'status' => 'used',
                                    'log_date' => date('Y-m-d H:i:s'),
                                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                                    'last_ip' => $this->getIP(),
                                ]);
            if($update){
                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                    'status' => true,
                    'message' => 'เติมไอเทมโค้ดเสร็จเรียบร้อย',
                    'content' => $eventInfo
                ]);
            }else{
                return response()->json([
                    'status'    => false,
                    'message'   => 'ขออภัย<br />ไม่สาามารถดำเนินการได้<br />กรุณาลองใหม่อีกครั้ง'
                ]);
            }
        }

        private function checkItemCodeFormat($code=''){
            $pattern = "/^[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}$/";
            $result = preg_match($pattern, $code);
            if($result > 0){
                return true;
            }
            return false;
        }

        public function openAdviceGachapon(Request $request){

            if ($request->has('type') == false && $request->type != 'advice_gachapon') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            // $eventCondition = $this->setEventInfo($uid);

            // check remain redeem code
            $remainCode = ItemCode::where('uid', $member->uid)
                                ->where('gahcapon_used', 'not_use')
                                ->count();

            if($remainCode <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />สิทธิ์ในการสุ่มของรางวัลไม่พอ'
                ]);
            }

            // deduct advice gachapon rights
            $deductRights = ItemCode::where('uid', $member->uid)
                                    ->where('gahcapon_used', 'not_use')
                                    ->first()
                                    ->update([
                                        'gahcapon_used' => 'used',
                                    ]);
            if($deductRights){

                $rewards = new Reward;

                $rewardKeyId = null;
                $goods_data = [];
                $productData = [];
                $rewardResp = "";

                // get random
                $rewardInfo = $rewards->getRandomAdviceReward();

                $rewardKeyId = $rewardInfo['id'];

                $productData[] = [
                    'product_id' => $rewardInfo['product_id'],
                    'product_title' => $rewardInfo['product_title'],
                    'product_quantity' =>  $rewardInfo['product_quantity'],
                    'item_type' => $rewardInfo['item_type'],
                    'amount' => $rewardInfo['amount'],
                ];

                $goods_data[] = [
                    'goods_id' => $rewardInfo['product_id'],
                    'purchase_quantity' => $rewardInfo['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];

                $rewardResp .= "- ".$rewardInfo['product_title']." x".$rewardInfo['amount']."<br />";

                // fixed reward
                $fixedReward = $rewards->adviceGaranteeReward();

                $productData[] = [
                    'product_id' => $fixedReward['product_id'],
                    'product_title' => $fixedReward['product_title'],
                    'product_quantity' =>  $fixedReward['product_quantity'],
                    'item_type' => $fixedReward['item_type'],
                    'amount' => $fixedReward['amount'],
                ];

                $goods_data[] = [
                    'goods_id' => $fixedReward['product_id'],
                    'purchase_quantity' => $fixedReward['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];

                $rewardResp .= "- ".$fixedReward['product_title']."<br />";

                // create item history log
                if(count($productData) > 0){
                    foreach($productData as $product){
                        $this->addItemHistoryLog([
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'reward_id' => $rewardKeyId,
                            'product_id' => $product['product_id'],
                            'product_title' => $product['product_title'],
                            'product_quantity' => $product['product_quantity'],
                            'item_type' => $product['item_type'],
                            'amount' => $product['amount'],
                            'status' => 'success',
                            'log_date' => date('Y-m-d H:i:s'),
                            'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                            'last_ip' => $this->getIP(),
                        ]);
                    }
                }

                // add fixed point history
                $points = $rewards->adviceGaranteePoints();
               
                $this->addPointHistoryLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'title' => $points['point_title'],
                    'points' => $points['point_quantity'],
                    'status' => 'success',
                    'type' => 'advice_gachapon',
                    'log_date' => date('Y-m-d H:i:s'),
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                    'last_ip' => $this->getIP(),
                ]);

                $rewardResp .= "- ".$points['point_title'];

                // add sen items log and send rewards
                $itemLog = $this->addSendItemLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'reward_id' => $rewardKeyId,
                    'send_item_status' => false,
                    'send_item_purchase_id' => '0',
                    'send_item_purchase_status' => '0',
                    'goods_data' => json_encode($goods_data),
                    'type' => 'advice_gachapon',
                    'status' => 'pending',
                    'log_date' => date('Y-m-d H:i:s'),
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                    'last_ip' => $this->getIP()
                ]);

                $send_result_raw = $this->dosendItem($ncid, $goods_data);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'success'
                    ], $itemLog['id'], $uid);

                    $eventInfo = $this->setEventInfo($uid);

                    return response()->json([
                        'status' => true,
                        'message' => "คุณได้รับ<br />".$rewardResp,
                        'content' => $eventInfo,
                        'id' => $rewardKeyId,
                    ]);

                }else{
                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'unsuccess'
                    ], $itemLog['id'], $uid);

                    $eventInfo = $this->setEventInfo($uid);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                        'content' => $eventInfo,
                        'id' => $rewardKeyId,
                    ]);
                }

            }else{
                $eventInfo = $this->setEventInfo($uid);
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถดำเนินการได้<br />กรุณาลองใหม่อีกครั้ง',
                    'content' => $eventInfo,
                ]);
            }

        }

        public function openFreeGachapon(Request $request){

            if ($request->has('type') == false && $request->type != 'free_gachapon') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }

            $dateTime = $this->setQuestDateTime();
            
            $userData = $this->userData;
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $remainFreeGachapon = QuestLog::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->where('gahcapon_used', 'not_use')
                                        ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                        ->count();

            if($remainFreeGachapon <= 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />สิทธิ์ในการสุ่มของรางวัลไม่พอ'
                ]);
            }

            // deduct free gachapon rights
            $deductFreeRights = QuestLog::where('uid', $member->uid)
                                    ->where('gahcapon_used', 'not_use')
                                    ->whereBetween('log_date_timestamp', [strtotime($dateTime['start_date_time']), strtotime($dateTime['end_date_time'])])
                                    ->first()
                                    ->update([
                                        'gahcapon_used' => 'used',
                                    ]);
            if($deductFreeRights){

                $rewards = new Reward;

                $rewardKeyId = null;
                $goods_data = [];
                $productData = [];
                $rewardResp = "";

                // get random
                $rewardInfo = $rewards->getRandomFreeReward();

                $rewardKeyId = $rewardInfo['id'];

                $productData[] = [
                    'product_id' => $rewardInfo['product_id'],
                    'product_title' => $rewardInfo['product_title'],
                    'product_quantity' =>  $rewardInfo['product_quantity'],
                    'item_type' => $rewardInfo['item_type'],
                    'amount' => $rewardInfo['amount'],
                ];

                $goods_data[] = [
                    'goods_id' => $rewardInfo['product_id'],
                    'purchase_quantity' => $rewardInfo['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];

                $rewardResp .= "- ".$rewardInfo['product_title']." x".$rewardInfo['amount']."<br />";

                // fixed reward
                $fixedReward = $rewards->freeGaranteeReward();

                $productData[] = [
                    'product_id' => $fixedReward['product_id'],
                    'product_title' => $fixedReward['product_title'],
                    'product_quantity' =>  $fixedReward['product_quantity'],
                    'item_type' => $fixedReward['item_type'],
                    'amount' => $fixedReward['amount'],
                ];

                $goods_data[] = [
                    'goods_id' => $fixedReward['product_id'],
                    'purchase_quantity' => $fixedReward['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];

                $rewardResp .= "- ".$fixedReward['product_title']."<br />";

                // create item history log
                if(count($productData) > 0){
                    foreach($productData as $product){
                        $this->addItemHistoryLog([
                            'uid' => $member->uid,
                            'username' => $member->username,
                            'ncid' => $member->ncid,
                            'reward_id' => $rewardKeyId,
                            'product_id' => $product['product_id'],
                            'product_title' => $product['product_title'],
                            'product_quantity' => $product['product_quantity'],
                            'item_type' => $product['item_type'],
                            'amount' => $product['amount'],
                            'status' => 'success',
                            'log_date' => date('Y-m-d H:i:s'),
                            'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                            'last_ip' => $this->getIP(),
                        ]);
                    }
                }

                // add fixed point history
                $points = $rewards->freeGaranteePoints();
               
                $this->addPointHistoryLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'title' => $points['point_title'],
                    'points' => $points['point_quantity'],
                    'status' => 'success',
                    'type' => 'free_gachapon',
                    'log_date' => date('Y-m-d H:i:s'),
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                    'last_ip' => $this->getIP(),
                ]);

                $rewardResp .= "- ".$points['point_title'];

                // add sen items log and send rewards
                $itemLog = $this->addSendItemLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'reward_id' => $rewardKeyId,
                    'send_item_status' => false,
                    'send_item_purchase_id' => '0',
                    'send_item_purchase_status' => '0',
                    'goods_data' => json_encode($goods_data),
                    'type' => 'free_gachapon',
                    'status' => 'pending',
                    'log_date' => date('Y-m-d H:i:s'),
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                    'last_ip' => $this->getIP()
                ]);

                $send_result_raw = $this->dosendItem($ncid, $goods_data);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'success'
                    ], $itemLog['id'], $uid);

                    $eventInfo = $this->setEventInfo($uid);

                    return response()->json([
                        'status' => true,
                        'message' =>  "คุณได้รับ<br />".$rewardResp,
                        'content' => $eventInfo,
                        'id' => $rewardKeyId,
                    ]);

                }else{
                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'unsuccess'
                    ], $itemLog['id'], $uid);

                    $eventInfo = $this->setEventInfo($uid);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                        'content' => $eventInfo
                    ]);
                }

            }else{
                $eventInfo = $this->setEventInfo($uid);
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถดำเนินการได้<br />กรุณาลองใหม่อีกครั้ง',
                    'content' => $eventInfo,
                ]);
            }

        }

        public function exchangeRewards(Request $request){

            if ($request->has('type') == false && $request->type != 'exchange_reward') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }
            
            if ($request->has('package_id') == false || empty($request->package_id)) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (2)!'
                ]);
            }

            $dateTime = $this->setQuestDateTime();
            
            $userData = $this->userData;
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);

            $package_id = $request->package_id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check has selected character and roll
            if(isset($member) && empty($member->char_id)){
                return response()->json([
                        'status'    => false,
                        'type'      => 'no_char',
                        'message'   => 'No character info'
                    ]);
            }

            $rewards = new Reward;
            $rewardInfo = $rewards->setExchangeByPackageId($package_id);
            if($rewardInfo == false){
                return response()->json([
                    'status' => false,
                    'message' => 'No data required.'
                ]);
            }

            // check remain advice points
            $remainPointsQuery = PointHistory::where('uid', $member->uid)
                                        ->where('status', 'success')
                                        ->sum('points');

            $usedPoints = DeductPointLog::where('uid', $member->uid)
                                    ->where('status', 'success')
                                    ->sum('points');

            $remainPoints = ($remainPointsQuery - $usedPoints > 0) ? $remainPointsQuery - $usedPoints : 0;   
            if($remainPoints < $rewardInfo['require_points']){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />เหรียญ Advice <br />ของคุณไม่พอแลกของรางวัล'
                ]);
            }

            // deduct points
            $deductPoints = $this->addDeductPointLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'status' => 'success',
                'points' =>  $rewardInfo['require_points'],
                'log_date' => date('Y-m-d H:i:s'),
                'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                'last_ip' => $this->getIP(),
            ]);

            if($deductPoints){

                $goods_data = [];
                $goods_data[] = [
                    'goods_id' => $rewardInfo['product_id'],
                    'purchase_quantity' => $rewardInfo['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];

                // create item history log
                $this->addItemHistoryLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'deduct_points_id' => $deductPoints['id'],
                    'reward_id' => $rewardInfo['id'],
                    'product_id' => $rewardInfo['product_id'],
                    'product_title' => $rewardInfo['product_title'],
                    'product_quantity' => $rewardInfo['product_quantity'],
                    'item_type' => $rewardInfo['item_type'],
                    'amount' => $rewardInfo['amount'],
                    'status' => 'success',
                    'log_date' => date('Y-m-d H:i:s'),
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                    'last_ip' => $this->getIP(),
                ]);

                // create send item log
                $itemLog = $this->addSendItemLog([
                    'uid' => $member->uid,
                    'username' => $member->username,
                    'ncid' => $member->ncid,
                    'deduct_points_id' => $deductPoints['id'],
                    'reward_id' => $rewardInfo['id'],
                    'send_item_status' => false,
                    'send_item_purchase_id' => '0',
                    'send_item_purchase_status' => '0',
                    'goods_data' => json_encode($goods_data),
                    'type' => 'exchange_points',
                    'status' => 'pending',
                    'log_date' => date('Y-m-d H:i:s'),
                    'log_date_timestamp' => strtotime(date('Y-m-d H:i:s')),
                    'last_ip' => $this->getIP()
                ]);
                
                $send_result_raw = $this->dosendItem($ncid, $goods_data);
                $send_result = json_decode($send_result_raw);
                if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {

                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'success'
                    ], $itemLog['id'], $uid);

                    $eventInfo = $this->setEventInfo($uid);

                    return response()->json([
                        'status' => true,
                        'message' => 'ส่งของรางวัลเสร็จเรียบร้อย<br />กรุณาตรวจสอบกล่องจดหมายภายในเกม',
                        'content' => $eventInfo
                    ]);

                }else{
                    $this->updateSendItemLog([
                        'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                        'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : '0',
                        'send_item_purchasse_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : '0',
                        'status' => 'unsuccess'
                    ], $itemLog['id'], $uid);

                    $eventInfo = $this->setEventInfo($uid);

                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถส่งของรางวัลได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                        'content' => $eventInfo
                    ]);
                }

            }else{
                $eventInfo = $this->setEventInfo($uid);
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถดำเนินการได้<br />กรุณาลองใหม่อีกครั้ง',
                    'content' => $eventInfo,
                ]);
            }


        }

    }