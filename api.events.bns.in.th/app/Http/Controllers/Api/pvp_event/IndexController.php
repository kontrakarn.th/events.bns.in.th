<?php

namespace App\Http\Controllers\Api\pvp_event;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

use App\Models\pvp_event\Member;
use App\Models\pvp_event\Reward;
use App\Models\pvp_event\ItemLog;
use App\Models\pvp_event\Quest;
use App\Models\pvp_event\QuestMemberLog;
use App\Models\pvp_event\QuestDailyLog;

class IndexController extends BnsEventController
{

    private $baseApi = 'http://api.apps.garena.in.th';
    private $startTime = '2019-12-03 06:00:00';
    private $endTime = '2019-12-31 23:59:59';

    private $userEvent = null;

    public function __construct(Request $request)
    {
        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);
        $this->checkEventStatus();

    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status'  => false,
                'type'    => 'no_permission',
                'message' => 'Sorry, you do not have permission.',
            ]));
        }
        
        if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
            die(json_encode([
                'status'  => false,
                'type'    => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม',
            ]));
        }

        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status'  => false,
                'type'    => 'maintenance',
                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์',
            ]));
        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status'  => false,
                    'type'    => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง',
                ]));
            }
        } else {
            die(json_encode([
                'status'  => false,
                'type'    => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน',
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function is_accepted_ip()
    {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'              => $uid,
                'username'         => $username,
                'ncid'             => $ncid,
                'char_id'          => 0,
                'char_name'        => '',
                'world_id'         => 0,
                'total_points'     => 0,
                'used_points'      => 0,
                'last_ip'          => $this->getIP(),
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
            $this->userEvent = $resp;

        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id)) {
            return false;
        }

        return Member::where('id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:PVP_EVENT:ASSOCIATE_UID_WITH_NCID_'.$uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'user_info',
                'uid'      => $uid,
            ]);
            $userNcInfo       = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }

        });
    }

    private function dosendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name'             => 'bns',
            'service'              => 'send_item',
            'user_id'              => $ncid,
            'purchase_description' => 'Sending item from garena events bns pvp event.',
            'goods'                => json_encode($goodsData),
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function doGetChar($uid, $ncid)
    {//new fixed function
        $resp_raw = $this->apiCharacters($uid, $ncid);

        $resp = json_decode($resp_raw);
        if (is_object($resp)) {
            if ($resp->status) {
                $content = [];
                $i       = 1;
                foreach ($resp->response as $key => $value) {
                    // check character has level more than or equal 60 and hm level more than or equal 12
                    // if((int)$value->level >= 60 && (int)$value->mastery_level>=12){
                        $content[] = [
                            'id' => $i,
                            'char_id'   => $value->id,
                            'char_name' => $value->name,
                            'world_id' => $value->world_id,
                            'job' => $value->job,
                            'level' => $value->level,
                            'creation_time' => $value->creation_time,
                            'last_play_start' => $value->last_play_start,
                            'last_play_end' => $value->last_play_end,
                            'last_ip' => $value->last_ip,
                            'exp' => $value->exp,
                            'mastery_level' => $value->mastery_level,
                        ];
                    // }

                    $i++;
                }
                return $content;
            }
        }
        return null;
    }

    private function apiCharacters($uid, $ncid)
    {
        return Cache::remember('BNS:PVP_EVENT:CHARACTERS_'.$uid, 10, function () use ($ncid) {
            return $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'characters',
                'user_id'  => $ncid,
            ]);
        });
    }

    public function selectCharacter(Request $request){

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'select_character') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }
        if ($decoded->has('id') == false) {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter! (2)'
            ]);
        }

        $charKey = (int)$decoded['id'];

        $userData = $this->userData;
        $uid = $userData['uid'];

        $member = Member::where('uid', $uid)->first();

        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'ไม่พบข้อมูล'
            ]);
        }

        if($member->char_id>0){
            return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถเปลี่ยนตัวละครได้'
            ]);
        }

        $mychar=$this->doGetChar($member->uid,$member->ncid);
        $myselect=collect($mychar)->where('id',$charKey)->first();
        if(count($myselect)==0){
            return response()->json([
                        'status' => false,
                        'message' => 'ไม่พบตัวละคร'
            ]);
        }

        $member->char_id   = (int)$myselect['char_id'];
        $member->char_name = $myselect['char_name'];
        $member->world_id       = $myselect['world_id'];

        // create member quest log
        $questListQuert = Quest::all();
        if(count($questListQuert) > 0){
            foreach($questListQuert as $questKey=>$questVal){
                $QuestMemberLogQuery = QuestMemberLog::where('uid', $member->uid)->where('quest_code', $questVal->quest_code)->count();
                if($QuestMemberLogQuery <= 0){
                    QuestMemberLog::create([
                        'uid'               => $member->uid,
                        'username'          => $member->username,
                        'ncid'              => $member->ncid,
                        'char_id'           => $member->char_id,
                        'quest_title'       => $questVal->quest_title,
                        'quest_code'        => $questVal->quest_code,
                        'quest_points'      => $questVal->quest_points,
                        'quest_type'        => $questVal->quest_type,
                        'image'             => $questVal->image,
                        'completed_count'   =>  0,
                        'total_points'      => 0,
                        'last_ip'           => $this->getIP(),
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s')
                    ]);
                }
                
            }
        }

        $member->save();

        $eventInfo = $this->setEventInfo($uid);

        return response()->json([
                    'status' => true,
                    'data' => $eventInfo
                ]);

    }

    private function addItemHistoryLog($arr) {
        return ItemLog::create($arr);
    }

    private function updateItemHistoryLog($arr, $log_id, $uid) {
        return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
    }

    private function checkItemHistory($uid,$package_id){
        $itemHistory = ItemLog::where('uid', $uid)
                            ->where('package_id', $package_id)
                            ->count();
        return $itemHistory;
    }

    private function updateTotalPoints($uid){
        if(empty($uid)){
            return false;
        }
        $member = Member::where('uid', $uid)->first();
        if($member->char_id==0){
            return false;
        }

        $memberQuests = QuestMemberLog::where('uid', $uid)->get();
        if(count($memberQuests) > 0){
            foreach($memberQuests as $quest){

                $sumPoints = 0;
                $sumCompletedCount = 0;

                $sumQuery = null;
                $sumQuery = QuestDailyLog::where('uid', $uid)->where('quest_code', $quest->quest_code);

                $sumCompletedCount = $sumQuery->sum('completed_count');
                $sumPoints = $sumQuery->sum('total_points');

                if($sumPoints > 0 && $sumCompletedCount > 0){
                    QuestMemberLog::where('uid', $uid)
                                    ->where('quest_code', $quest->quest_code)
                                    ->update([
                                        'completed_count'   => $sumCompletedCount,
                                        'total_points'      => $sumPoints,
                                        'last_ip'           => $this->getIP(),
                                        'updated_at'        => date('Y-m-d H:i:s')
                                    ]);
                }

            }
        }

        $totalPoints = QuestMemberLog::where('uid', $uid)->sum('total_points');

        $member->total_points = $totalPoints;
        $member->save();

        return true;
    }

    private function setEventInfo($uid){
        if(empty($uid)){
            return false;
        }

        // update total poinst
        $this->updateTotalPoints($uid);

        $member = Member::where('uid', $uid)->first();

        if($member->char_id==0){

            $charData=$this->doGetChar($member->uid,$member->ncid);
            
            $content = [];
            foreach ($charData as $key => $value) {

                $content[] = [
                    'id' => $value['id'],
                    'char_name' => $value['char_name'],
                ];
                
            }
            return [
                'username'=>$member->username,
                'character_name'=>'',
                'selected_char'=>false,
                'characters'=>$content,
            ];
        }

        $remainPoints = $member->total_points - $member->used_points;

        // member's quest list
        $questListQuery = Cache::remember('BNS:PVP_EVENT:MEMBER_QUEST_'.$uid, 10, function () use ($uid) {
            return QuestMemberLog::where('uid', $uid)->orderBy('id', 'ASC')->get();
        });
        $questList = [];
        if(count($questListQuery)>0){
            foreach($questListQuery as $questKey=>$questVal){
                $questList[] = [
                    'quest_title'       => $questVal->quest_title,
                    'quest_points'      => $questVal->quest_points,
                    'completed_count'   => $questVal->completed_count,
                    'total_points'      => $questVal->total_points,
                    'image'             => $questVal->image,
                ];
            }
        }

        // reward list and check it can exchange
        $rewardListQuery = Cache::remember('BNS:PVP_EVENT:REWARDS_LIST', 10, function () use ($uid) {
            return Reward::orderBy('id', 'ASC')->get();
        });
        $rewardList = [];
        if(count($rewardListQuery) > 0){
            foreach($rewardListQuery as $keyReward=>$valReward){
                $rewardList[] = [
                    'id' => $valReward->package_key,
                    'title' => $valReward->package_name,
                    'required_points' => $valReward->points,
                    'can_exchange' => $remainPoints >= $valReward->points && time() >= strtotime($valReward->available_date) ? true : false,
                    'image' => $valReward->image,
                ];
            }
        }


        return [
            'username'=>$member->username,
            'character_name'=>$member->char_name,
            'selected_char'=>true,
            'points' => $remainPoints,
            'points_text' => number_format($remainPoints),
            'characters'=>[],
            'quests' => $questList,
            'rewards' => $rewardList,
        ];
    }

    public function getEventInfo(Request $request){

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);
        
        // get member info
        $member = Member::where('uid', $uid)->first();
        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        $eventInfo = $this->setEventInfo($uid);
        if($eventInfo==false){
            return response()->json([
                    'status' => false,
                    'data' => $eventInfo,
                    'message'=>'ไม่พบตัวละคร'
                ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);

    }

    public function exchangeReward(Request $request){

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'exchange_rewards') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!(1)'
            ]);
        }

        if ($decoded->has('package_key') == false || $decoded['package_key'] == '') {//check parameter
            return response()->json([
                        'status' => false,
                        'message' => 'Invalid parameter!(2)'
            ]);
        }

        $package_key = $decoded['package_key'];

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);
        
        // get member info
        $member = Member::where('uid', $uid)->first();

        if(isset($member) && empty($member)){
            return response()->json([
                        'status' => false,
                        'message' => 'No member data.'
            ]);
        }

        // get reward via package key
        $reward = Reward::where('package_key', $package_key)->first();
        if(!$reward){
            return response()->json([
                        'status' => false,
                        'message' => 'No reward data.'
            ]);
        }

        if(time() < strtotime($reward->available_date)){
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />ยังไม่เปิดให้แลกของราวัลชิ้นนี้'
            ]);
        }

        $remainPoints = $member->total_points - $member->used_points;

        // check remain points are enough for exchange
        if($remainPoints < $reward->points){
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />จำนวนเหรียญกิจกรรมไม่พอแลกของรางวัล'
            ]);
        }

        $exchangeHistoryCount = ItemLog::where('uid', $member->uid)->where('package_key', $package_key)->count();
        // check exchange limit
        if($reward->limit > 0 && $exchangeHistoryCount >= $reward->limit){
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br />จำกัดจำนวนการแลกของรางวัล'
            ]);
        }

        // deduct point
        $member->used_points = $member->used_points + $reward->points;
        $member->save();

        // set reward for send
        $goods_data = []; //good data for send item group
        
        $packageTitle = $reward->package_name;
        $packageAmount = $reward->package_amount;

        $goods_data[] = [
            'goods_id' => $reward->package_id,
            'purchase_quantity' => $reward->package_quantity,
            'purchase_amount' => 0,
            'category_id' => 40
        ];

        $sendItemLog = $this->addItemHistoryLog([
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'package_id' => $reward->package_id,
            'package_key' => $reward->package_key,
            'package_name' => $reward->package_name,
            'package_quantity' => $reward->package_quantity,
            'package_amount' => $reward->package_amount,
            'package_type' => $reward->package_type,
            'points' => $reward->points,
            'send_item_status' => false,
            'send_item_purchase_id' => 0,
            'send_item_purchase_status' => 0,
            'goods_data' => json_encode($goods_data),
            'status' => 'pending',
            'log_date' => (string)date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
            'last_ip' => $this->getIP(),
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);

        $send_result_raw = $this->dosendItem($member->ncid, $goods_data);
        $send_result = json_decode($send_result_raw);
        if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
            $this->updateItemHistoryLog([
                'send_item_status' => $send_result->status ? $send_result->status : false,
                'send_item_purchase_id' => $send_result->response->purchase_id ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => $send_result->response->purchase_status ? $send_result->response->purchase_status : 0,
                'status' => 'success'
            ], $sendItemLog['id'], $member->uid);

            $eventInfo = $this->setEventInfo($member->uid);

            return response()->json([
                        'status' => true,
                        'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                        'data' => $eventInfo,
                        'reward'=>[
                            'name'=>$packageTitle
                        ]
                    ]);
        }else{
            $this->updateItemHistoryLog([
                'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                'status' => 'unsuccess'
                    ], $sendItemLog['id'], $member->uid);

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => false,
                        'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                        'data' => $eventInfo
                    ]);
        }


    }

}