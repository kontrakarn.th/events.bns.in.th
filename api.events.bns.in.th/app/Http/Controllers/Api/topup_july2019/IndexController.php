<?php

    namespace App\Http\Controllers\Api\topup_july2019;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DB;

    use App\Models\topup_july2019\Member;
    use App\Models\topup_july2019\Reward;
    use App\Models\topup_july2019\ItemLog;

    class IndexController extends BnsEventController{

    	private $baseApi = 'http://api.apps.garena.in.th';
        private $startTime = '2019-07-31 10:00:00';
        private $endTime = '2019-08-14 23:59:50';

        private $startGetApiTime = '00:00:00';
        private $endGetApiTime = '23:59:59';

        // private $awtSecret = 'AgjVHV5uEA7TYsSgNaydHHqL6MdsTkUs';

        private $diamondStartTime = '2019-07-31 12:00:00';
        private $diamondEndTime = '2019-08-07 23:59:59';

        private $bnsAppId = 32835;

        private $whitelistEvent=[
            65733035,
        ];

        public function __construct(Request $request)
        {
            parent::__construct();

            // init user data
            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
             if($this->isIngame() == false && $this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            }
            // dd($this->startTime,$this->endTime);
            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){
                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            }
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'total_diamonds' => 0,
                    'last_ip' => $this->getIP()
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['_id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        public function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('_id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:TOPUP_JULY2019:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function dosendItem(string $ncid, array $goodsData){
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => ' sending item from garena events bns topup July 2019.',
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addItemHistoryLog($arr) {
            return ItemLog::create($arr);
        }

        private function updateItemHistoryLog($arr, $log_id, $uid) {
            return ItemLog::where('_id', $log_id)->where('uid', $uid)->update($arr);
        }

        private function checkItemHistory($uid,$package_id){
            $itemHistory = ItemLog::where('uid', $uid)
                                ->where('package_id', $package_id)
                                // ->where('status', 'success')
                                ->count();
            return $itemHistory;
        }

        private function doGetTermgameDiamondHistory($uid) {
            if(empty($uid)){
                return false;
            }
            // Cache::forget('BNS:TOPUP_NOVEMBER2018:TERMGAME_DIAMONDS_HISTORY_' . $uid);
            return Cache::remember('BNS:TOPUP_JULY2019:TERMGAME_DIAMONDS_HISTORY_' . $uid, 5, function() use($uid) {
                return json_decode($this->post_api($this->baseApi, [
                            'key_name' => 'gws_test',
                            'service' => 'get_user_topup',
                            'uid' => $uid,
                            'start_ts' => strtotime($this->diamondStartTime),
                            'end_ts' => strtotime($this->diamondEndTime),
                            'app_id' => $this->bnsAppId
                        ]));
            });
        }

        private function doGetDiamondHistory($ncid) {
            if(empty($ncid)){
                return false;
            }
            // $ncid='7CB03022-2B70-40C0-B5C1-9AA84FC23312';
            // Cache::forget('BNS:TOPUP_NOVEMBER2018:EXCHANGE_TO_DIAMONDS_HISTORY_' . $ncid);
            return Cache::remember('BNS:TOPUP_JULY2019:EXCHANGE_TO_DIAMONDS_HISTORY_' . $ncid, 5, function() use($ncid) {
                return json_decode($this->post_api($this->baseApi, [
                            'key_name' => 'bns',
                            'service' => 'query_cash_txn',
                            'user_id' => $ncid,//ncid
                            'start_time' => $this->diamondStartTime,
                            'end_time' => $this->diamondEndTime
                        ]));
            });
        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return false;
            }
            $member = Member::where('uid', $uid)->first();

            $result_diamond = 0;

            $resultDiamond = $this->doGetDiamondHistory($member->ncid);
            if ($resultDiamond->status == true) {
                $contentDiamond = $resultDiamond->response;
                if (count($contentDiamond) > 0) {
                    $collectionDiamond = collect($contentDiamond);
                    $result_diamond += $collectionDiamond->sum('diamond');
                }
            }

            if($result_diamond > $member->total_diamonds){
                $this->updateMember([
                    'total_diamonds' => $result_diamond
                ], $member->_id);
            }

            $totalDiamonds = ($result_diamond > 0 && $result_diamond > $member->total_diamonds) ? $result_diamond : $member->total_diamonds;

            $reward = new Reward;

            $packages = [];

            // check can receive reward
            for($i=1;$i<=3;$i++){

                if(in_array($member->uid,$this->whitelistEvent)){
                    $can_receive = true;
                }else{
                    $can_receive = false;
                }
                $received = false;

                $packageInfo = [];
                $packageInfo = $reward->setRewardByPackage($i);

                // check item history
                $itemHistory = 0;
                $itemHistory = $this->checkItemHistory($member->uid,$packageInfo['package_id']);

                if($totalDiamonds >= $packageInfo['require_diamonds'] && $itemHistory == 0){
                    $can_receive = true;
                }

                if($itemHistory > 0){
                    $received = true;
                }
                // $can_receive=true;
                // $received=false;
                $packages[] = [
                    'package_id' => $packageInfo['package_id'],
                    'package_title' => $packageInfo['package_title'],
                    'package_desc' => $packageInfo['package_desc'],
                    'can_receive' => $can_receive,
                    'received' => $received
                ];

            }

            return [
                'username'=>$member->username,
                'mypoint' => $totalDiamonds,
                'packages' => $packages,
            ];
        }

        public function getEventInfo(Request $request){

            $decoded = $request;
            if ($decoded->has('type') == false || $decoded['type'] != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            // $uid=200062;
            // $ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
            // get member info
            $member = Member::where('uid', $uid)->first();

            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
                    ]);

        }

        public function redeemReward(Request $request){

            $decoded = $request;

            if ($decoded->has('type') == false || $decoded['type'] != 'redeem') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (1)'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);
            // $uid=200062;
            // $ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
            if ($decoded->has('package_id') == false) {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (2)'
                ]);
            }elseif($decoded->package_id<1 || $decoded->package_id>3){
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter! (3)'
                ]);
            }

            $package_id = (int)$decoded['package_id'];
            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }
            // $member->uid=200062;
            // $member->ncid="FFD64A1C-CBA3-4E93-95FD-D5F4552149B2";
            // check item history
            $itemHistory = 0;
            $itemHistory = $this->checkItemHistory($member->uid,$package_id);
            if($itemHistory>0){
                return response()->json([
                        'status' => false,
                        'message' => 'ขออภัย<br />คุณได้รับไอเทมไปแล้ว'
                    ]);
            }

            $reward = new Reward;

            $packageInfo = $reward->setRewardByPackage($package_id);

            if($packageInfo == false){
                return response()->json([
                            'status' => false,
                            'message' => 'No data required.'
                        ]);
            }

            $totalDiamonds=$member->total_diamonds;

            if(in_array($member->uid,$this->whitelistEvent)){

            }else{
                if($totalDiamonds < $packageInfo['require_diamonds']){
                    return response()->json([
                        'status' => false,
                        'message' => 'ขออภัย<br />จำนวนไดมอนด์ของคุณยังเติมไม่ครบตามที่กำหนด'
                    ]);
                }
            }

            // set reward packages
            $goods_data = []; //good data for send item group
            $packageId = $packageInfo['package_id'];
            $requireDiamonds = $packageInfo['require_diamonds'];
            $packageTitle = $packageInfo['package_title'];

            $productList = $packageInfo['product_set'];

            foreach ($productList as $key => $product) {
                $goods_data[] = [
                    'goods_id' => $product['product_id'],
                    'purchase_quantity' => $product['product_quantity'],
                    'purchase_amount' => 0,
                    'category_id' => 40
                ];
            }

            $sendItemLog = $this->addItemHistoryLog([
                'uid' => $uid,
                'ncid' => $ncid,
                'log_date' => (string)date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
                'package_id' => $packageId,
                'package_title' => $packageTitle,
                'require_diamonds' => $requireDiamonds,
                'product_set' => $productList,
                'status' => 'pending',
                'send_item_status' => false,
                'send_item_purchase_id' => 0,
                'send_item_purchase_status' => 0,
                'goods_data' => json_encode($goods_data),
                'last_ip' => $this->getIP()
            ]);

            $send_result_raw = $this->dosendItem($ncid, $goods_data);
            $send_result = json_decode($send_result_raw);
            if (is_null($send_result) == false && is_object($send_result) && $send_result->status) {
                $this->updateItemHistoryLog([
                    'send_item_status' => $send_result->status ?: false,
                    'send_item_purchase_id' => $send_result->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $send_result->response->purchase_status ?: 0,
                    'status' => 'success'
                        ], $sendItemLog['_id'], $uid);

                $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                            'status' => true,
                            'message' => "ส่งไอเทมเสร็จเรียบร้อย <br /> กรุณาตรวจสอบที่กล่องจดหมาย<br />ภายในเกม",
                            'content' => $eventInfo
                        ]);
            }else{
                $this->updateItemHistoryLog([
                    'send_item_status' => isset($send_result->status) ? $send_result->status : false,
                    'send_item_purchase_id' => isset($send_result->response->purchase_id) ? $send_result->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($send_result->response->purchase_status) ? $send_result->response->purchase_status : 0,
                    'status' => 'unsuccess'
                        ], $sendItemLog['_id'], $uid);

                // $eventInfo = $this->setEventInfo($uid);

                return response()->json([
                            'status' => false,
                            'message' => "ไม่สามารถส่งไอเทมได้ <br /> กรุณาติดต่อฝ่ายบริการลูกค้า",
                            // 'content' => $eventInfo
                        ]);
            }

        }

    }
