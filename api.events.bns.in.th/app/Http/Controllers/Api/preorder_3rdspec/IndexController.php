<?php

    namespace App\Http\Controllers\Api\preorder_3rdspec;

    use App\Http\Controllers\Api\BnsEventController;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use DateTime;
    use DatePeriod;
    use DateInterval;
    use DB;

    use App\Models\preorder_3rdspec\Member;
    use App\Models\preorder_3rdspec\DeductLog;

    class IndexController extends BnsEventController{

        private $userData = [];

        private $baseApi = 'http://api.apps.garena.in.th';

        private $startTime = '2020-01-08 12:00:00';
        private $endTime = '2020-01-14 23:59:59';

        private $package_1_diamonds = 150000; // 150000

        private $package_2_diamonds = 150000; // 150000

        public function __construct(Request $request)
        {
            parent::__construct();

            $this->userData = $this->getJwtUserData($request);

            $this->checkEventStatus();
        }

        private function checkEventStatus()
        {
            /* if($this->isIngame() == false && $this->is_accepted_ip() == false) {
            // if($this->isIngame() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            } */

            // lock ip
            /* if($this->is_accepted_ip() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'no_permission',
                    'message' => 'Sorry, you do not have permission.'
                ]));
            } */

            if ((time() < strtotime($this->startTime) || time() > strtotime($this->endTime))) {
                die(json_encode([
                    'status' => false,
                    'type' => 'end_event',
                    'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
                ]));
            }

            if($this->checkIsMaintenance()){
                die(json_encode([
                    'status' => false,
                    'type' => 'maintenance',
                    'message' => 'ขออภัย<br />ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
                ]));
            }

            if($this->isLoggedIn()){

                // check member has game info
                $uid = $this->userData['uid'];
                $username = $this->userData['username'];
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    die(json_encode([
                        'status' => false,
                        'type' => 'no_game_info',
                        'message' => 'ขออภัย<br />กรุณาล็อคอินเข้าเกมเพื่อสร้างตัวละคร<br />ก่อนร่วมกิจกรรม'
                    ]));
                }

                // check and create member
                if($this->checkMember() == false){
                    die(json_encode([
                        'status' => false,
                        'type' => 'cant_create_member',
                        'message' => 'เกิดความผิดพลาด<br />ไม่สามารถบักทึกข้อมูลของคุณได้<br />กรุณาลองใหม่อีกครั้ง'
                    ]));
                }
            }/* else{
                die(json_encode([
                        'status' => false,
                        'type' => 'not_login',
                        'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
                    ]));
            } */
        }

        private function getIP()
        {
            // check ip
            $ip = request()->getClientIp();
            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
                $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $ip = array_shift($ips);
            }
            return $ip;
        }

        private function isLoggedIn()
        {
            if ($this->userData)
                return true;
            else
                return false;
        }

        private function is_accepted_ip()
        {
            $allow_ips = array();
            $allow_ips[] = '112.121.131.234';
            $allow_ips[] = '110.168.229.249';
            $allow_ips[] = '180.183.119.151';
            $allow_ips[] = '27.254.46.174';
            $allow_ips[] = '183.88.67.189';
            $allow_ips[] = '58.137.18.34';
            $allow_ips[] = '112.121.39.3';
            // $allow_ips[] = '127.0.0.1'; // local

            $ip = $this->getIP();

            return in_array($ip, $allow_ips);
        }

        private function checkMember()
        {
            $uid = $this->userData['uid'];
            $username = $this->userData['username'];
            if ($this->hasMember($uid) == false) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if(empty($ncid)){
                    return false;
                }
                $arr = [
                    'uid' => $uid,
                    'username' => $username,
                    'ncid' => $ncid,
                    'last_ip' => $this->getIP(),
                ];

                $resp = $this->createMember($arr);
                if (isset($resp['id']) == false) {
                    return false;
                }
            }else{
                $member = Member::where('uid', $uid)->first();
                if(empty($member->ncid)){
                    $ncid = $this->associateGarenaWithNc($uid, $username);
                    if(empty($ncid)){
                        return false;
                    }
                    $this->updateMember(['ncid' => $ncid], $member->_id);
                }
            }
            return true;
        }

        protected function hasMember(int $uid)
        {
            $counter = Member::where('uid', $uid)->count();
            if ($counter > 0) {
                return true;
            } else {
                return false;
            }
        }

        private function createMember($arr)
        {
            return Member::create($arr);
        }

        private function updateMember($arr, $_id)
        {
            if(empty($_id))
                return false;

            return Member::where('id', $_id)->update($arr);
        }

        protected function getNcidByUid(int $uid): string
        {
            return Member::where('uid', $uid)->value('ncid');
        }

        protected function getUidByNcid(string $ncid)
        {
            return Member::where('ncid', $ncid)->value('uid');
        }

        private function associateGarenaWithNc(int $uid, string $username): string
        {
            return Cache::remember('BNS:PREORDER_3RDSPEC:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function() use($uid, $username) {

                // get ncid from unser info api
                $userNcInfoResult = $this->post_api($this->baseApi, [
                    'key_name' => 'bns',
                    'service' => 'user_info',
                    'uid' => $uid,
                ]);
                $userNcInfo = json_decode($userNcInfoResult, true);
                if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                    return '';
                }else{
                    if(isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])){
                        // return ncid from bns user info api
                        return $userNcInfo['response']['user_id'];
                    }else{
                        return '';
                    }
                }

            });
        }

        private function diamondBalance(int $uid) {
            return $this->post_api($this->baseApi, [
                        'key_name' => 'bns',
                        'service' => 'balance',
                        'uid' => $uid,
            ]);
        }

        private function setRemainDiamonds($uid){
            $diamondsBalanceRaw = $this->diamondBalance($uid);
            $diamondsBalance = json_decode($diamondsBalanceRaw);

            if ($diamondsBalance->status == false) {
                return 0;
            }else{
                return intval($diamondsBalance->balance);
            }
        }

        private function setDiamondDeductData($deduct_diamond_amount=0)
        {
            if($deduct_diamond_amount==0)
                return false;

            return [
                [
                    'goods_id' => 741,
                    'purchase_quantity' => $deduct_diamond_amount,
                    'purchase_amount' => $deduct_diamond_amount,
                    'category_id' => 40
                ]
            ];
        }

        private function deductDiamond(string $ncid, array $goodsData)
        {
            if (is_null($goodsData)) {
                return null;
            }
            $data = [
                'key_name' => 'bns',
                'service' => 'send_item',
                'user_id' => $ncid,
                'purchase_description' => 'Deduct diamomds for garena BNS Pre-Order 3rd spec Package.',
                'currency_group_id' => 71,
                'goods' => json_encode($goodsData)
            ];
            return $this->post_api($this->baseApi, $data);
        }

        private function addDeductLog(array $arr) {
            if ($arr) {
                $resp = DeductLog::create($arr);
                if ($resp) {
                    return $resp;
                } else {
                    return false; //fix
                }
            }
            return null;
        }

        private function updateDeductLog(array $arr, $logId) {
            if ($arr) {
                return DeductLog::where('id', $logId)->update($arr);
            }
            return null;
        }

        public function getEventInfo(Request $request)
        {

            if ($request->has('type') == false && $request->type != 'event_info') {//check parameter
                return response()->json([
                            'status' => false,
                            'message' => 'Invalid parameter!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $ncid = $this->getNcidByUid($uid);

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            $eventInfo = $this->setEventInfo($uid);

            return response()->json([
                        'status' => true,
                        'content' => $eventInfo
            ]);

        }

        private function setThaiMonth($m){
            switch($m){
                case "1":
                    return 'มกราคม';
                    break;
            }

            return '';
        }

        private function setPackageName($packageId=''){
            switch($packageId){
                case 1:
                    return 'แพ็คเกจคมดาบเทวาพิทักษ์';
                    break;
                case 2:
                    return 'แพ็คเกจเขี้ยวเล็บของหมาป่าทมิฬ';
                    break;
            }

            return '';
        }

        private function setEventInfo($uid){
            if(empty($uid)){
                return false;
            }

            $member = Member::where('uid', $uid)->first();

            // check purchase history
            $purchaseHistoryCount1 = $this->checkPurchaseHistoryCount($uid,1);
            $purchaseHistoryCount2 = $this->checkPurchaseHistoryCount($uid,2);

            $purchasedPackageId1 = -1;
            $purchasedPackage1 = '';
            $purchasedDate1 = '';
            if($purchaseHistoryCount1 > 0){

                $purchaseHistoryQuery1 = DeductLog::where('uid', $uid)->where('package_id', 1)->where('status', 'success')->first();

                $purchasedPackageId1 = $purchaseHistoryQuery1->package_id;
                $purchasedPackage1 = $this->setPackageName($purchaseHistoryQuery1->package_id);
                $purchasedDateTimeStamp1 = strtotime($purchaseHistoryQuery1->created_at);

                $purchasedDate1 = date('j', $purchasedDateTimeStamp1).' '.$this->setThaiMonth(date('n', $purchasedDateTimeStamp1)).' '.(date('Y', $purchasedDateTimeStamp1)+543).'<br />เวลา '.date('H:i:s', $purchasedDateTimeStamp1);
            }

            $purchasedPackageId2 = -1;
            $purchasedPackage2 = '';
            $purchasedDate2 = '';
            if($purchaseHistoryCount2 > 0){
                
                $purchaseHistoryQuery2 = DeductLog::where('uid', $uid)->where('package_id', 2)->where('status', 'success')->first();

                $purchasedPackageId2 = $purchaseHistoryQuery2->package_id;
                $purchasedPackage2 = $this->setPackageName($purchaseHistoryQuery2->package_id);
                $purchasedDateTimeStamp2 = strtotime($purchaseHistoryQuery2->created_at);

                $purchasedDate2 = date('j', $purchasedDateTimeStamp2).' '.$this->setThaiMonth(date('n', $purchasedDateTimeStamp2)).' '.(date('Y', $purchasedDateTimeStamp2)+543).'<br />เวลา '.date('H:i:s', $purchasedDateTimeStamp2);
            }

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            return [
                'username' => $member->username,
                'diamonds' => number_format($diamonds),
                'packages' => [
                    [
                        'id' => 1,
                        'name' => $this->setPackageName(1),
                        'price' => number_format($this->package_1_diamonds),
                        'can_purchase' => $purchaseHistoryCount1 == 0 && $diamonds >= $this->package_1_diamonds,
                        'already_purchase' => $purchaseHistoryCount1 > 0,
                        'purchased_package_id'  => $purchasedPackageId1,
                        'purchased_package' => $purchasedPackage1,
                        'purchased_date' => $purchasedDate1,
                    ],
                    [
                        'id' => 2,
                        'name' => $this->setPackageName(2),
                        'price' => number_format($this->package_2_diamonds),
                        'can_purchase' => $purchaseHistoryCount2 == 0 && $diamonds >= $this->package_2_diamonds,
                        'already_purchase' => $purchaseHistoryCount2 > 0,
                        'purchased_package_id'  => $purchasedPackageId2,
                        'purchased_package' => $purchasedPackage2,
                        'purchased_date' => $purchasedDate2,
                    ],
                ],
            ];

        }

        private function checkPurchaseHistoryCount($uid,$package_id){
            // check purchase history
            $purchaseHistory = DeductLog::where('uid', $uid)->where('package_id', $package_id)->count();

            return $purchaseHistory;
        }

        public function purchasePackage(Request $request) {

            if ($request->has('type') == false && $request->type != 'purchase_package') {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (1)!'
                ]);
            }

            if ($request->has('package_id') == false || !in_array($request->package_id, [1,2])) {//check parameter
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid parameter (2)!'
                ]);
            }

            $userData = $this->userData;
            $uid = $userData['uid'];
            $username = $userData['username'];
            $ncid = $this->getNcidByUid($uid);
            $package_id = $request->package_id;

            // get member info
            $member = Member::where('uid', $uid)->first();
            if(isset($member) && empty($member)){
                return response()->json([
                            'status' => false,
                            'message' => 'No member data.'
                ]);
            }

            // check purchase history
            $purchaseHistory = $this->checkPurchaseHistoryCount($uid, $package_id);
            
            if($purchaseHistory > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />จำกัดการสั่งซื้อล่วงหน้า แพ็คเกจละ 1 ครั้งต่อ 1 การีนาไอดี'
                ]);
            }

            // get diamonds balance
            $diamonds = $this->setRemainDiamonds($member->uid);

            $can_purchase = false;
            $package_price = 0;
            if($package_id == 1){

                $package_title = $this->setPackageName($package_id);
                $package_price = $this->package_1_diamonds;

                if($purchaseHistory == 0 && $diamonds >= $this->package_1_diamonds){
                    $can_purchase = true;
                }

            }elseif($package_id == 2){

                $package_title = $this->setPackageName($package_id);
                $package_price = $this->package_2_diamonds;

                if($purchaseHistory == 0 && $diamonds >= $this->package_2_diamonds){
                    $can_purchase = true;
                }

            }

            if($can_purchase == false){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่สามารถสั่งซื้อแพ็คเกจได้<br />เนื่องจากไดมอนด์ไม่พอ'
                ]);
            }

            if($package_price == 0){
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br />ไม่พบแพ็คเกจที่สั่งซื้อ'
                ]);
            }

            // set deduct diamonds
            $logDeduct = $this->addDeductLog([
                'uid'                       => $member->uid,
                'username'                  => $member->username,
                'ncid'                      => $member->ncid,
                'diamonds'                  => $package_price,
                'package_id'                => $package_id,
                'package_title'             => $package_title,
                'status'                    => 'pending',
                'log_date'                  => date('Y-m-d H:i:s'), // set to string
                'log_date_timestamp'        => time(),
                'last_ip'                   => $this->getIP(),
            ]);

            // set deduct diamonds
            $deductData = $this->setDiamondDeductData($package_price);
            $respDeductRaw = $this->deductDiamond($ncid, $deductData);
            $respDeduct = json_decode($respDeductRaw);
            if (is_object($respDeduct) && is_null($respDeduct) == false && $respDeduct->status){

                // update deduct log
                $arrDeductDiamond = [
                    'before_deduct_diamond' => $diamonds,
                    'after_deduct_diamond' => $diamonds - $package_price,
                    'deduct_status' => $respDeduct->status ?: 0,
                    'deduct_purchase_id' => $respDeduct->response->purchase_id ?: 0,
                    'deduct_purchase_status' => $respDeduct->response->purchase_status ?: 0,
                    'deduct_data' => json_encode($deductData),
                    'status' => 'success'
                ];

                $respDeduct = $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);

                $eventInfo = $this->setEventInfo($uid);
                
                if($respDeduct){
                    return response()->json([
                        'status' => true,
                        'message' => 'สั่งซื้อแพ็คเกจสำเร็จแล้ว<br />รอรับของวันที่ 15 มกราคม 2563 นี้ได้เลย',
                        'content' => $eventInfo
                    ]);
                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถสั่งซื้อแพ็คเกจได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า',
                        'content' => $eventInfo
                    ]);
                }


            }else{
                $arrDeductDiamond['status'] = 'unsuccess';
                $this->updateDeductLog($arrDeductDiamond, $logDeduct['id']);
            }

            return response()->json([
                        'status' => false,
                        'message' => 'เกิดความผิดพลาดระหว่างการหักไดมอนด์<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
            ]);


        }

    }