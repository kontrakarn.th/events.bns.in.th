<?php

namespace App\Http\Controllers\Api\mystic2;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\mystic2\Setting;
use App\Models\mystic2\Product;
use App\Models\mystic2\Product_group;
use App\Models\mystic2\Productgrouplimit;
use App\Models\mystic2\Userspecialproduct;
use App\Models\mystic2\Users;
use App\Http\Controllers\Api\mystic2\BuyKeyController;
use App\Http\Controllers\Api\mystic2\HistoryController;
use App\Http\Controllers\Api\mystic2\UsersController;
use App\Jobs\mystic2\SendItemToUserQueue;
use App\Models\mystic2\MemberGroup;

class IndexController extends BnsEventController
{

    private $userData = [];

    private $baseApi = 'http://api.apps.garena.in.th';

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->userData = $this->getJwtUserData($request);
        $this->checkEventStatus();
        $this->userscontroller = new UsersController;
        $this->buykeycontroller = new BuyKeyController;
        $this->historycontroller = new HistoryController;
    }

    public function GetKey(Request $request)
    {
        return $this->buykeycontroller->GetKey($request, $this->userData["uid"]);
    }
    public function GetHistory(Request $request)
    {
        return $this->historycontroller->GetHistory($this->userData["uid"]);
    }

    private function setEventInfo($uid)
    {
        $member = Users::where('uid', $uid)->first();

        if (!$member) {
            return [
                'status' => false,
                'message' => 'No member data.'
            ];
        }

        return [
            'status' => true,
            "message" => 'Event Info',
            'username' => $member->username,
            'can_reset' => $this->userscontroller->userCanreset($member->uid),
            'product_slot' => $this->userscontroller->getProductslot($member->uid),
            'special_product' => $this->userscontroller->GetUserSpecialProduct($member->uid),
            'hongmoon_key' => $this->userscontroller->GetAmountUserKeys($member->uid)
        ];
    }

    public function getEventInfo(Request $request)
    {

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Users::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] == false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message'],
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }
    private function getEventSetting()
    {
        return Setting::where('active', 1)->first();
    }
    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status' => false,
                'type' => 'no_permission',
                'message' => 'Sorry, you do not have permission.'
            ]));
        }

        // get events setting info
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status' => false,
                'type' => 'maintenance',
                'message' => 'ขออภัย ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
            ]));
        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                ]));
            }
        } else {
            die(json_encode([
                'status' => false,
                'type' => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
            ]));
        }
    }

    private function getIP()
    {
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function is_accepted_ip()
    {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function chkGroup()
    {
        $groupName = 'normal';
        $uid       = $this->userData['uid'];
        $group = MemberGroup::where('uid', $uid)->first();
        if (isset($group)) {
            $groupName = $group->group;
        }
        return $groupName;
    }

    private function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'              => $uid,
                'username'         => $username,
                'ncid'             => $ncid,
                'group'            => $this->chkGroup(),
                'user_type'        =>  1, //Normal User if != 1 = Test User
                'last_ip'          => $this->getIP(),
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }
            $this->userEvent = $resp;
        } else {
            $member = Users::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Users::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Users::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id)) {
            return false;
        }

        return Users::where('id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Users::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Users::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:MYSTIC2:LIVE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'user_info',
                'uid'      => $uid,
            ]);
            $userNcInfo       = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }
        });
    }

    function userselectproduct(Request $request)
    {
        $eventSetting = $this->getEventSetting();
        if (!$eventSetting) {
            return response()->json([
                'status' => false,
                'data' => '',
                'message' => 'เกิดข้อผิดพลาด',
            ]);
        }
        //$this->userData;
        $uid = $this->userData["uid"];
        $selectedspecialitem = Userspecialproduct::where('uid', $uid)->get();
        if (count($selectedspecialitem) >= 1) {
            return response()->json([
                'status' => false,
                'data' => '',
                'message' => 'ท่านเลือกของรางวัลใหญ่ไปแล้ว',
            ]);
        }
        if ($request->type_selected == 1 && $request->selected_product == NULL) {
            return response()->json([
                'status' => false,
                'data' => '',
                'message' => 'ไม่ได้เลือกของรางวัล',
            ]);
        }
        $userdaimonds = $this->userscontroller->setDiamondsBalance($uid);
        if ($request->type_selected == 1 && $userdaimonds < $eventSetting->select_product_diamonds) {
            return response()->json([
                'status' => false,
                'data' => '',
                'message' => 'ไดมอนด์ไม่เพียงพอ',
            ]);
        }
        if ($request->type_selected == 1) {
            $member = Users::where('uid', $uid)->first();
            $item = Product::where('id', $request->selected_product)->first();
            $logDeduct = $this->buykeycontroller->createDeductLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'deduct_type' => 'select_product',
                'diamonds' => $eventSetting->select_product_diamonds,
                'status' => 'pending',
                'last_ip' => $this->getIP(),
                'log_date' => (string) date('Y-m-d'), // set to string
                'log_date_timestamp' => time(),
            ]);
            $cal_daimonds =  $eventSetting->select_product_diamonds;
            $this->SaveSpecialProduct($request->selected_product, $uid, 1);
            $this->buykeycontroller->UserpayDiamonds($cal_daimonds, $uid, $userdaimonds, $logDeduct);
            $data = array("special_product" => array("product_name" => $item->th_name, "select_type" => 1, "product_img" => $item->product_img));
            return response()->json([
                'status' => true,
                'data' => $data,
                'message' => '',
            ]);
        }
        if ($request->type_selected == 2) {
            $specialproduct = Product::where('product_group_id', 1)->get();
            $item = $this->GetProduct($specialproduct);
            $this->SaveSpecialProduct($item['id'], $uid, 2);
            $data = array("special_product" => array("product_name" => $item->th_name, "select_type" => 2, "product_img" => $item->product_img));
            return response()->json([
                'status' => true,
                'data' => $data,
                'message' => '',
            ]);
        } else {
            return response()->json([
                'status' => false,
                'data' => '',
                'message' => 'เกิดข้อผิดพลาด',
            ]);
        }
    }
    function GetSpecialProduct()
    {
        $data = Product::where('product_group_id', 1)->get();
        return response()->json([
            'status' => true,
            'data' => $data,
            'message' => "",

        ]);
    }
    function SaveSpecialProduct($specialproduct, $uid, $typeselect)
    {
        $savespecialproduct = new Userspecialproduct;
        $savespecialproduct->uid = $uid;
        $savespecialproduct->special_item_id = $specialproduct;
        $savespecialproduct->type_select = $typeselect;
        $savespecialproduct->save();
    }

    function Random(Request $request)
    {
        $slot = $request->slot;
        //return $request;
        if (!is_array($slot)) {
            return response()->json([
                'status' => false,
                'data' => "",
                'message' => "เกิดข้อผิดพลาด",

            ]);
        }
        if ($request->slot == NULL) {
            return response()->json([
                'status' => false,
                'data' => "",
                'message' => "Dont Have Slot no",

            ]);
        }
        $uid = $this->userData["uid"];
        $check_already_have_special = Productgrouplimit::where('uid', $uid)->where("product_group_id", 1)->get();
        if (count($check_already_have_special) > 0) {
            return response()->json([
                'status' => false,
                'data' => '',
                'message' => "คุณได้รางวัลใหญ่ไปแล้ว กรุณารีเซ็ตก่อน",
            ]);
        }

        $countslot =  count($slot);

        $userskey = $this->userscontroller->GetAmountUserKeys($uid);
        if ($countslot > $userskey) {
            return response()->json([
                'status' => false,
                'message' => "กุญแจไม่พอ",
            ]);
        }
        $selectedspecialitem = Userspecialproduct::with(['Product'])->where('uid', $uid)->get();
        if (count($selectedspecialitem) < 1) {
            return response()->json([
                'status' => false,
                'message' => 'ท่านยังไม่ได้เลือกของรางวัลใหญ่',
            ]);
        }
        $check_replace_slot = Productgrouplimit::where('uid', $uid)->whereIn("slot_no", $slot)->get();
        if (count($check_replace_slot) > 0) {
            return response()->json([
                'status' => false,
                'data' => '',
                'message' => "สล็อตซ้ำ",
            ]);
        }

        $member = Users::where('uid', $uid)->first();
        if (!$member) {
            return response()->json([
                'status' => false,
                'message' => "เกิดข้อผิดพลาด (1)",
            ]);
        }

        if ($member->group === 'merchant') {
            $goodbad = array("merchant_good", "merchant_bad");
            $rand_keys = array_rand($goodbad, 1);
            $groupMember =  isset($goodbad[$rand_keys]) ? $goodbad[$rand_keys] : "merchant_bad";
        } else {
            $groupMember = $member->group;
        }

        $dist =  Product_group::with(['Product'])->where('reward_type', $groupMember)->get();
        $accum = [];
        foreach ($dist as $k => $d) {
            $productgrouplimit = Productgrouplimit::where('uid', $uid)->where('product_group_id', $d['product_group_id'])->get();
            $productgrouplimit = count($productgrouplimit);
            if ($productgrouplimit >= $d['get_amount']) {
                $fill = [];
            } else {
                $fill  = array_fill(1, $d['rate_group'] * 1000, $k);
            }
            $accum = array_merge($accum, $fill);
        }
        $num_acc = count($accum) - 1;

        $data = [];

        $specialproduct = [];

        $randomnum = rand(0, $num_acc);
        for ($i = 1; $i <= $countslot; $i++) {

            $limit = $this->chklimit($uid, $dist[$accum[$randomnum]]['product_group_id']);

            if ($limit >= $dist[$accum[$randomnum]]['get_amount']) {
                $num = $accum[$randomnum];
                //$limit = Productgrouplimit::where('product_group_id',$dist[$accum[$randomnum]]['id'])->get();
                $accum =  array_values(array_diff($accum, [$accum[$randomnum]]));

                $num_acc = count($accum) - 1;
                $randomnum = rand(0, $num_acc);
            } else {
                $randomnum = rand(0, $num_acc);
            }

            if ($dist[$accum[$randomnum]]['product_group_id'] == 1) {
                $item = $this->GetProduct($selectedspecialitem->pluck('product'));
                $uspecialproductid  = Userspecialproduct::where("uid", $uid)->value('id');
                //$updateuserselectspecial = $this->UpdateUserSpecialProduct($uid);
                array_push($specialproduct, array("item_name" => $item["th_name"], "item_group" => $item['product_group_id'], "item_img" => $item['product_img']));
            } else {
                $data = $dist[$accum[$randomnum]]['product'];
                $item = $this->GetProduct($data);
            }
            $productgrouplimit = $this->SaveProductLimit($uid, $item["product_group_id"], $item["id"], $slot[$i - 1]);
            $savehistoryid = $this->historycontroller->SaveHistory($item["id"], 2, 1, $uid);
            $ncid = $this->getNcidByUid($member->uid);
            $goods_data = [];
            $goods_data[] = [
                'goods_id' => $item["product_id"],
                'purchase_quantity' => 1,
                'purchase_amount' => 0,
                'category_id' => 40,
            ];
            SendItemToUserQueue::dispatch($ncid, $goods_data, $savehistoryid, $this->historycontroller)->onQueue('bns_mystic2_hongmoon_chest_senditem');
        }
        $sum = $userskey - $countslot;
        $updateuserskey = $this->userscontroller->UpdateUserKey($uid, $sum, "minus");
        $userhongmoonkey = Users::where("uid", $uid)->value("hongmoon_key");
        $userproductslot = Productgrouplimit::with(['Product'])->where("uid", $uid)->orderBy("id", "DESC")->get();
        $product_slot = [];
        foreach ($userproductslot as $product) {
            $product_slot[] = [
                'slot_no' => $product->slot_no,
                'product' => [
                    'product_img' => $product->product->product_img,
                ],
            ];
        }
        /*foreach($userproductslot as $u){
                    array_push($itemtosend,array($u->slot_no=>array( "item_name"=>$u->product->th_name,"item_group" => $u->product->product_group_id,"item_img" =>$u->product->product_img)));
                }*/
        $canreset = $this->userscontroller->userCanreset($uid);
        $data = array("can_reset" => $canreset, "special_product_slot" => $specialproduct, "slot" => $product_slot, "hongmoon_key" => $userhongmoonkey);

        return response()->json([
            'status' => true,
            'data' => $data,
        ]);
    }
    public function CheckRepeatSlot($uid, $productid, $slotid)
    {
        // $productgrouplimit = Productgrouplimit::where($uid,$productid,$slotid)->get();
        return $productid;
    }
    public function GetProduct($dist)
    {
        $accum = [];
        $start = microtime(true);
        foreach ($dist as $k => $d) {
            $fill  = array_fill(1, $d['rate'] * 100, $k);
            $accum = array_merge($accum, $fill);
        }
        $end = microtime(true);
        $time = number_format(($end - $start), 2);
        $random = array();
        $start = microtime(true);
        $num_acc = count($accum);

        for ($i = 1; $i <= 1; $i++) {
            $randomnum = rand(1, $num_acc);
            $fid = $dist[$accum[$randomnum]]['id'];
            $data = $dist[$accum[$randomnum]];
        }
        $end = microtime(true);
        $time = number_format(($end - $start), 2);
        return $data;
    }
    public function SaveProductLimit($uid, $productgroupid, $itemid, $slot)
    {
        $inprolimit = new Productgrouplimit;
        $inprolimit->uid = $uid;
        $inprolimit->product_group_id = $productgroupid;
        $inprolimit->product_id = $itemid;
        $inprolimit->slot_no = $slot;
        $inprolimit->save();
    }
    public function chklimit($uid, $productgroupid)
    {
        $limit = Productgrouplimit::where("uid", $uid)->where('product_group_id', $productgroupid)->get();
        return count($limit);
    }
    public function UpdateUserSpecialProduct($uid)
    {
        $uspecialproductid  = Userspecialproduct::where("uid", $uid)->value('id');
        $specialproduct = Userspecialproduct::find($uspecialproductid)->delete();
    }
    public function ResetSlot(Request $request)
    {
        $uid = $this->userData["uid"];
        $productlimit = Productgrouplimit::where('uid', $uid)->delete();
        $specialproductlimit = Userspecialproduct::where('uid', $uid)->delete();
        $data = [
            'can_reset' => $this->userscontroller->userCanreset($uid),
            'product_slot' => $this->userscontroller->getProductslot($uid),
            'special_product' => $this->userscontroller->GetUserSpecialProduct($uid),
        ];
        return response()->json([
            'status' => true,
            'data' => $data,
            'message' => 'รีเซ็ตเรียบร้อย',
        ]);
    }
}
