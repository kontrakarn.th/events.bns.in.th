<?php

namespace App\Http\Controllers\Api\mystic2;

use App\Http\Controllers\Api\BnsEventController;
use App\Http\Controllers\Api\mystic2\HistoryController;
use App\Models\mystic2\KeyPackage;
use App\Models\mystic2\Users;
use App\Models\mystic2\DeductLog;

class BuyKeyController extends BnsEventController
{
    private $baseApi = 'http://api.apps.garena.in.th';

    public function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }
    public function createDeductLog(array $arr)
    {
        if ($arr) {
            $resp = DeductLog::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return false; //fix
            }
        }
        return null;
    }
    public function GetKey($request, $uid)
    {
        $member = Users::where('uid', $uid)->first();
        $userscontroller = new UsersController;
        $packageid = $request->package_id;
        $quantity = $request->quantity;
        $ncid = $this->getNcidByUid($uid);
        if ($packageid == NULL || $quantity == NULL || $quantity == 0) {
            return response()->json([
                'status' => false,
                'data' => '',
                'message' => 'กรุณาใส่จำนวน',
            ]);
        }

        $userdaimonds = $userscontroller->setDiamondsBalance($uid);
        $key_package = KeyPackage::find($packageid);
        if ($key_package == NULL) {
            return response()->json([
                'status' => false,
                'data' => '',
                'message' => 'ไม่พบแพ็คเกจ',
            ]);
        }
        $logDeduct = $this->createDeductLog([
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'deduct_type' => 'key',
            'diamonds' => $key_package->daimonds_price * $quantity,
            'status' => 'pending',
            'last_ip' => $this->getIP(),
            'log_date' => (string) date('Y-m-d'), // set to string
            'log_date_timestamp' => time(),
        ]);
        $cal_daimonds = $key_package->daimonds_price * $quantity;
        if ($cal_daimonds > $userdaimonds) {
            return response()->json([
                'status' => false,
                'data' => '',
                'message' => 'ไดมอนด์ไม่เพียงพอ',
            ]);
        }
        if ($key_package->bonus < 1) {
            $key_package->bonus = '';
        }
        if ($key_package->bonus != NULL && $key_package->bonus != 0) {
            $symbol = "+";
        } else {
            $symbol = " ";
        }
        $data = "ได้รับ <span color='#6fc96d'>" . $key_package->th_name . "</span> x" . $quantity;
        $historycontroller = new HistoryController;
        $userscontroller = new UsersController;
        $userpaydiamonds = $this->UserpayDiamonds($cal_daimonds, $uid, $userdaimonds, $logDeduct);

        $savehistory = $historycontroller->SaveHistory($key_package->id, 1, $quantity, $uid);
        if ($key_package->bonus == NULL || $key_package->bonus == '') {
            $key_package->bonus = 0;
        }
        $sum = ($key_package->amount + $key_package->bonus) * $quantity;
        $update_userkey = $userscontroller->UpdateUserKey($uid, $sum, "plus");
        $userkey = Users::where("uid", $uid)->value("hongmoon_key");
        $data = array("message" => $data, "hongmoon_key" => $userkey);
        return response()->json([
            'status' => true,
            'data' => $data,
        ]);
    }
    public function UserpayDiamonds($cal_daimonds, $uid, $userdaimonds, $logDeduct)
    {
        $ncid = $this->getNcidByUid($uid);
        $deductData = $this->setDiamondDeductData($cal_daimonds);
        $resp_deduct_raw = $this->apiDeductDiamond($ncid, $deductData);
        $resp_deduct = json_decode($resp_deduct_raw);

        if (!is_object($resp_deduct) || is_null($resp_deduct)) {
            return response()->json([
                'status' => false,
                'message' => 'Error deduct diamond'
            ]);
        }
        $this->updateDeductLog([
            'before_deduct_diamond' => $userdaimonds,
            'after_deduct_diamond' => $userdaimonds - $cal_daimonds,
            'deduct_status' => $resp_deduct->status ?: 0,
            'deduct_purchase_id' => $resp_deduct->response->purchase_id ?: 0,
            'deduct_purchase_status' => $resp_deduct->response->purchase_status ?: 0,
            'deduct_data' => json_encode($deductData),
            'status' => 'success'
        ], $logDeduct['id']);
    }
    public function updateDeductLog(array $arr, $log_id)
    {
        if ($arr) {
            return DeductLog::where('id', $log_id)->update($arr);
        }
        return null;
    }
    protected function getNcidByUid(int $uid): string
    {
        return Users::where('uid', $uid)->value('ncid');
    }
    private function apiDeductDiamond(string $ncid, array $goodsData)
    {

        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Deduct diamomds for garena events BNS Mystic Hongmoon 2.',
            'currency_group_id' => 71,
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function setDiamondDeductData($deduct_diamond_amount = 0)
    {
        if ($deduct_diamond_amount == 0)
            return false;

        return [
            [
                'goods_id' => 741,
                'purchase_quantity' => $deduct_diamond_amount,
                'purchase_amount' => $deduct_diamond_amount,
                'category_id' => 40
            ]
        ];
    }
}
