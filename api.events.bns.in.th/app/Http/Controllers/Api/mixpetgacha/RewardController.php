<?php

    namespace App\Http\Controllers\Api\mixpetgacha;

    class RewardController {

        public function getRandomRewardInfo() {

            $rewards = $this->getSpecialRewardsList();

            $random = rand(0, 100000);
            $accumulatedChance = 0;
            $reward = false;

            foreach ($rewards as $value) {
                $chance = $value['chance'] * 1000;
                $currentChance = $accumulatedChance + $chance;

                // acquired reward
                if ($random >= $accumulatedChance && $random <= $currentChance) {
                    $reward = $value;
                    break;
                }
                $accumulatedChance = $currentChance;
            }

            $reward['random'] = $random;

            // now we have the reward
            return $reward;
        }

        public function getSpecialRewardsList() {
            return [
                [
                    'id' => 1,
                    'item_type' => 'object',
                    'product_title' => 'หินสัตว์เลี้ยง ภูติหิมะตัวน้อย',
                    'product_id' => 2330,
                    'product_quantity' => 1,
                    'chance' => 1,
                    'icon' => 'gacha_1',
                    'key' => 'gacha_1'
                ],
                [
                    'id' => 2,
                    'item_type' => 'object',
                    'product_title' => 'หินสัตว์เลี้ยง เมโลดี้',
                    'product_id' => 2324,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'gacha_2',
                    'key' => 'gacha_2'
                ],
                [
                    'id' => 3,
                    'item_type' => 'object',
                    'product_title' => 'หินสัตว์เลี้ยง นกฮูก',
                    'product_id' => 2326,
                    'product_quantity' => 1,
                    'chance' => 1,
                    'icon' => 'gacha_3',
                    'key' => 'gacha_3'
                ],
                [
                    'id' => 4,
                    'item_type' => 'object',
                    'product_title' => 'หินล้ำค่า ลูกเจี๊ยบ',
                    'product_id' => 2320,
                    'product_quantity' => 1,
                    'chance' => 25,
                    'icon' => 'gacha_4',
                    'key' => 'gacha_4'
                ],
                [
                    'id' => 5,
                    'item_type' => 'object',
                    'product_title' => 'หินสัตว์เลี้ยง น้องหมาสิงโต',
                    'product_id' => 2321,
                    'product_quantity' => 1,
                    'chance' => 20,
                    'icon' => 'gacha_5',
                    'key' => 'gacha_5'
                ],
                [
                    'id' => 6,
                    'item_type' => 'object',
                    'product_title' => 'หินสัตว์เลี้ยง อัลปาก้า',
                    'product_id' => 2322,
                    'product_quantity' => 1,
                    'chance' => 20,
                    'icon' => 'gacha_6',
                    'key' => 'gacha_6'
                ],
                [
                    'id' => 7,
                    'item_type' => 'object',
                    'product_title' => 'หินสัตว์เลี้ยง วิญญาณมูซา',
                    'product_id' => 2323,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'gacha_7',
                    'key' => 'gacha_7'
                ],
                [
                    'id' => 8,
                    'item_type' => 'object',
                    'product_title' => 'หินสัตว์เลี้ยง วิคตอเรีย',
                    'product_id' => 2325,
                    'product_quantity' => 1,
                    'chance' => 10,
                    'icon' => 'gacha_8',
                    'key' => 'gacha_8'
                ],
                [
                    'id' => 9,
                    'item_type' => 'object',
                    'product_title' => 'หินสัตว์เลี้ยง งูขาว',
                    'product_id' => 2327,
                    'product_quantity' => 1,
                    'chance' => 1,
                    'icon' => 'gacha_9',
                    'key' => 'gacha_9'
                ],
                [
                    'id' => 10,
                    'item_type' => 'object',
                    'product_title' => 'หินสัตว์เลี้ยง จินซอยอน',
                    'product_id' => 2328,
                    'product_quantity' => 1,
                    'chance' => 1,
                    'icon' => 'gacha_10',
                    'key' => 'gacha_10'
                ],
                [
                    'id' => 11,
                    'item_type' => 'object',
                    'product_title' => 'หินล้ำค่า ซอยอนเด็ก',
                    'product_id' => 2329,
                    'product_quantity' => 1,
                    'chance' => 1,
                    'icon' => 'gacha_11',
                    'key' => 'gacha_11'
                ]
            ];
        }

    }
