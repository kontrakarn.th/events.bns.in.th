<?php

namespace App\Http\Controllers\Api\blue_jeans;

use App\Http\Controllers\Api\BnsEventController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

use App\Models\blue_jeans\Setting;
use App\Models\blue_jeans\Member;
use App\Models\blue_jeans\MemberGroup;
use App\Models\blue_jeans\Reward;
use App\Models\blue_jeans\ItemHistory;
use App\Models\blue_jeans\ItemLog;
use App\Models\blue_jeans\DeductLog;

class IndexController extends BnsEventController
{
    private $baseApi = 'http://api.apps.garena.in.th';

    private $userEvent = null;

    public function __construct(Request $request)
    {
        parent::__construct();

        // init user data
        $this->userData = $this->getJwtUserData($request);

        $this->checkEventStatus();
    }

    private function checkEventStatus()
    {
        if ($this->isIngame() == false && $this->is_accepted_ip() == false) {
            die(json_encode([
                'status' => false,
                'type' => 'no_permission',
                'message' => 'Sorry, you do not have permission.'
            ]));
        }

        // get events setting info
        $eventSetting = $this->getEventSetting();

        if ((time() < strtotime($eventSetting->event_start) || time() > strtotime($eventSetting->event_end))) {
            die(json_encode([
                'status' => false,
                'type' => 'end_event',
                'message' => 'ไม่อยู่ในช่วงเวลาเข้าร่วมกิจกรรม'
            ]));
        }

        if ($this->checkIsMaintenance()) {
            die(json_encode([
                'status' => false,
                'type' => 'maintenance',
                'message' => 'ขออภัย<br/>ระบบกำลังปิดปรับปรุงประจำสัปดาห์'
            ]));
        }

        if ($this->isLoggedIn()) {
            // check and create member
            if ($this->checkMember() == false) {
                die(json_encode([
                    'status' => false,
                    'type' => 'cant_create_member',
                    'message' => 'เกิดความผิดพลาดไม่สามารถบักทึกข้อมูลของคุณได้ กรุณาลองใหม่อีกครั้ง'
                ]));
            }
        } else {
            die(json_encode([
                'status' => false,
                'type' => 'not_login',
                'message' => 'กรุณาเข้าสู่ระบบก่อนใช้งาน'
            ]));
        }
    }

    private function getIP()
    {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = array_shift($ips);
        }
        return $ip;
    }

    private function isLoggedIn()
    {
        if ($this->userData) {
            return true;
        } else {
            return false;
        }
    }

    private function is_accepted_ip()
    {
        $allow_ips   = array();
        $allow_ips[] = '112.121.131.234';
        $allow_ips[] = '110.168.229.249';
        $allow_ips[] = '180.183.119.151';
        $allow_ips[] = '27.254.46.174';
        $allow_ips[] = '183.88.67.189';
        $allow_ips[] = '58.137.18.34';
        $allow_ips[] = '112.121.39.3';
        $allow_ips[] = '127.0.0.1'; // local

        $ip = $this->getIP();

        return in_array($ip, $allow_ips);
    }

    private function chkGroup()
    {
        $groupName = 'normal';
        $uid       = $this->userData['uid'];
        $group = MemberGroup::where('uid', $uid)->first();
        if (isset($group)) {
            $groupName = $group->group;
        }
        return $groupName;
    }

    private function checkMember()
    {
        $uid      = $this->userData['uid'];
        $username = $this->userData['username'];
        if ($this->hasMember($uid) == false) {
            $ncid = $this->associateGarenaWithNc($uid, $username);
            if (empty($ncid)) {
                return false;
            }
            $arr = [
                'uid'              => $uid,
                'username'         => $username,
                'ncid'             => $ncid,
                'group'            => $this->chkGroup(),
                'char_id'          => 0,
                'char_name'        => '',
                'world_id'         => 0,
                'last_ip'          => $this->getIP(),
            ];

            $resp = $this->createMember($arr);
            if (isset($resp['id']) == false) {
                return false;
            }

            $this->userEvent = $resp;
        } else {
            $member = Member::where('uid', $uid)->first();
            if (empty($member->ncid)) {
                $ncid = $this->associateGarenaWithNc($uid, $username);
                if (empty($ncid)) {
                    return false;
                }
                $this->updateMember(['ncid' => $ncid], $member->_id);
            }
            $this->userEvent = $member;
        }
        return true;
    }

    protected function hasMember(int $uid)
    {
        $counter = Member::where('uid', $uid)->count();
        if ($counter > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createMember($arr)
    {
        return Member::create($arr);
    }

    public function updateMember($arr, $_id)
    {
        if (empty($_id)) {
            return false;
        }

        return Member::where('id', $_id)->update($arr);
    }

    protected function getNcidByUid(int $uid): string
    {
        return Member::where('uid', $uid)->value('ncid');
    }

    protected function getUidByNcid(string $ncid)
    {
        return Member::where('ncid', $ncid)->value('uid');
    }

    private function associateGarenaWithNc(int $uid, string $username): string
    {
        return Cache::remember('BNS:blue_jeans:LIVE:ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid, $username) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service'  => 'user_info',
                'uid'      => $uid,
            ]);
            $userNcInfo       = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response']['user_id'];
                } else {
                    return '';
                }
            }
        });
    }

    private function associateFullGarenaWithNc(int $uid)
    {
        return Cache::remember('BNS:blue_jeans:LIVE:FULL_ASSOCIATE_UID_WITH_NCID_' . $uid, 10, function () use ($uid) {

            // get ncid from unser info api
            $userNcInfoResult = $this->post_api($this->baseApi, [
                'key_name' => 'bns',
                'service' => 'user_info',
                'uid' => $uid,
            ]);
            $userNcInfo = json_decode($userNcInfoResult, true);
            if (is_null($userNcInfo) || $userNcInfo['status'] == false) {
                return '';
            } else {
                if (isset($userNcInfo['response']['user_id']) && !empty($userNcInfo['response']['user_id'])) {
                    // return ncid from bns user info api
                    return $userNcInfo['response'];
                } else {
                    return '';
                }
            }
        });
    }

    private function dosendItem(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Sending item from garena events bns blue jeans.',
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function addItemHistoryLog($arr)
    {
        return ItemHistory::create($arr);
    }

    private function updateItemHistoryLog($arr, $log_id, $uid)
    {
        return ItemHistory::where('id', $log_id)->where('uid', $uid)->update($arr);
    }

    private function addItemLog($arr)
    {
        return ItemLog::create($arr);
    }

    private function updateItemLog($arr, $log_id, $uid)
    {
        return ItemLog::where('id', $log_id)->where('uid', $uid)->update($arr);
    }

    private function diamondBalance(int $uid)
    {
        return $this->post_api($this->baseApi, [
            'key_name' => 'bns',
            'service' => 'balance',
            'uid' => $uid,
        ]);
    }

    private function setDiamondDeductData($deduct_diamond_amount = 0)
    {
        if ($deduct_diamond_amount == 0)
            return false;

        return [
            [
                'goods_id' => 741,
                'purchase_quantity' => $deduct_diamond_amount,
                'purchase_amount' => $deduct_diamond_amount,
                'category_id' => 40
            ]
        ];
    }

    private function deductDiamond(string $ncid, array $goodsData)
    {
        if (is_null($goodsData)) {
            return null;
        }
        $data = [
            'key_name' => 'bns',
            'service' => 'send_item',
            'user_id' => $ncid,
            'purchase_description' => 'Deduct diamonds for garena bns web blue jeans.',
            'currency_group_id' => 71,
            'goods' => json_encode($goodsData)
        ];
        return $this->post_api($this->baseApi, $data);
    }

    private function addDeductLog(array $arr)
    {
        if ($arr) {
            $resp = DeductLog::create($arr);
            if ($resp) {
                return $resp;
            } else {
                return false; //fix
            }
        }
        return null;
    }

    private function updateDeductLog($arr, $log_id)
    {
        if ($arr) {
            return DeductLog::where('id', $log_id)->update($arr);
        }
        return null;
    }

    private function getEventSetting()
    {
        // return Cache::remember('BNS:blue_jeans:LIVE:EVENT_SETTING_TEST', 10, function() {
        return Setting::where('active', 1)->first();
        // });
    }

    private function checkCanOpenGachaponPeriod($start = '', $end = '')
    {

        if (empty($start) || empty($end)) {
            return false;
        }

        $currentTime = time();
        $startTimeStamp = strtotime($start);
        $endTimeStamp = strtotime($end);
        if ($currentTime >= $startTimeStamp && $currentTime <= $endTimeStamp) {
            return true;
        }

        return false;
    }

    public function getEventInfo(Request $request)
    {

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'event_info') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }


        $eventInfo = $this->setEventInfo($uid);
        if ($eventInfo['status'] == false) {
            return response()->json([
                'status' => false,
                'message' => $eventInfo['message'],
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $eventInfo
        ]);
    }

    private function setEventInfo($uid)
    {
        if (empty($uid)) {
            return [
                'status' => false,
                'message' => 'ไม่พอข้อมูล'
            ];
        }

        // get events setting info
        $eventSetting = $this->getEventSetting();

        $member = Member::where('uid', $uid)->first();

        // get diamonds
        $diamondsRaw = $this->diamondBalance($member->uid);
        $diamondsDecoded = json_decode($diamondsRaw);
        if ($diamondsDecoded->status == false) {
            return [
                'status' => false,
                'message' => 'ไม่สามารถอัพเดทข้อมูล Diamonds ของคุณได้<br/>กรุณาลองใหม่อีกครั้ง'
            ];
        }
        $diamonds = intval($diamondsDecoded->balance);

        $can_open_gachapon = false;
        if ($diamonds >= $eventSetting->diamonds_require && $this->checkCanOpenGachaponPeriod($eventSetting->gachapon_start, $eventSetting->gachapon_end) == true) {
            $can_open_gachapon = true;
        }

        return [
            'status' => true,
            "message" => 'Event Info',
            'username' => $member->username,
            'can_open_gachapon' => $can_open_gachapon,
        ];
    }

    public function buyGachapon(Request $request)
    {

        // get events setting info
        $eventSetting = $this->getEventSetting();
        if ($this->checkCanOpenGachaponPeriod($eventSetting->gachapon_start, $eventSetting->gachapon_end) == false) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br/>ไม่อยู่ในช่วงเวลากิจกรรม'
            ]);
        }


        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'buy_gachapon') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        // get diamonds
        $diamondsRaw = $this->diamondBalance($member->uid);
        $diamondsDecoded = json_decode($diamondsRaw);
        if ($diamondsDecoded->status == false) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถอัพเดทข้อมูล Diamonds ของคุณได้<br/>กรุณาลองใหม่อีกครั้ง'
            ]);
        }

        $diamonds = intval($diamondsDecoded->balance);

        if ($diamonds < $eventSetting->diamonds_require) {
            return response()->json([
                'status' => false,
                'message' => 'ไดมอนด์ไม่เพียงพอ'
            ]);
        }

        // create deduct log
        $logDeduct = $this->addDeductLog([
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'diamonds' => $eventSetting->diamonds_require,
            'deduct_type' => 'gachapon',
            'status' => 'pending',
            'log_date' => date('Y-m-d'),
            'log_date_timestamp' => time(),
            'last_ip' => $this->getIP(),
        ]);

        $randResult = $this->doRandomItem(); //do getRandomCode
        $goodsData = []; //good data for send item group

        if ($randResult->package_type == 'materials') {
            $goodsData[] = [
                'goods_id' => $randResult->package_id,
                'purchase_quantity' => $randResult->package_quantity,
                'purchase_amount' => 0,
                'category_id' => 40
            ];
        }

        $arrItemHistory = [
            'uid' => $member->uid,
            'username' => $member->username,
            'ncid' => $member->ncid,
            'deduct_id' => $logDeduct['id'],
            'package_key' => $randResult->package_key,
            'package_id' => $randResult->package_id,
            'package_name' => $randResult->package_name,
            'package_quantity' => $randResult->package_quantity,
            'package_amount' => $randResult->package_amount,
            'package_type' => $randResult->package_type,
            'reward_type' => $randResult->reward_type,
            'image' => $randResult->image,
            // 'send_type' => 'owner',
            // 'send_status' => 'pending',
            'log_date' => date('Y-m-d'),
            'log_date_timestamp' => time(),
            'last_ip' => $this->getIP(),
        ];

        if ($randResult->package_type == 'materials') {
            $arrItemHistory['send_status'] = 'success';
            $arrItemHistory['send_type'] = 'owner';
        } else {
            $arrItemHistory['send_status'] = 'pending';
            $arrItemHistory['send_type'] = 'none';
        }

        // set deduct diamonds
        $deductData = $this->setDiamondDeductData($eventSetting->diamonds_require);
        if ($deductData == false) {
            return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง']);
        }

        $respDeductRaw = $this->deductDiamond($ncid, $deductData);
        $respDeduct = json_decode($respDeductRaw);

        if (!is_object($respDeduct) || is_null($respDeduct) || $respDeduct->status == false) {

            $arrDeductLog['status'] = 'unsuccess';
            $this->updateDeductLog($arrDeductLog, $logDeduct['id']);

            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้้<br/> กรุณาติดต่อฝ่ายบริการลูกค้า'
            ]);
        }

        $before_diamond = $diamonds;
        $after_diamond = $before_diamond - $eventSetting->diamonds_require;

        // update deduct log
        $arrDeductLog = [
            'before_deduct_diamond' => $before_diamond,
            'after_deduct_diamond' => $after_diamond,
            'deduct_status' => $respDeduct->status,
            'deduct_purchase_id' => isset($respDeduct->response->purchase_id) ? $respDeduct->response->purchase_id : 0,
            'deduct_purchase_status' => isset($respDeduct->response->purchase_status) ? $respDeduct->response->purchase_status : 0,
            'deduct_data' => json_encode($deductData),
            'status' => 'success'
        ];
        $this->updateDeductLog($arrDeductLog, $logDeduct['id']);

        $log_history_id = $this->addItemHistoryLog($arrItemHistory);

        if (!isset($log_history_id)) {
            \Log::info("BLUE JEANS ERROR: UID: " . $member->uid . " package key: " . $randResult->package_key . " package_name: " . $randResult->package_name);
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า (2)'
            ]);
        }

        // send items
        if (is_null($goodsData) == false && is_array($goodsData) && count($goodsData) > 0) {

            // create send item log
            $logSendItemResult = $this->addItemLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'history_id' => $log_history_id['id'],
                'send_item_status' => false,
                'send_item_purchase_id' => 0,
                'send_item_purchase_status' => 0,
                'goods_data' => json_encode($goodsData),
                'status' => 'pending',
                'log_date' => date('Y-m-d'),
                'log_date_timestamp' => time(),
                'last_ip' => $this->getIP(),
            ]);

            //send item
            $sendResultRaw = $this->dosendItem($ncid, $goodsData);
            $sendResult = json_decode($sendResultRaw);
            if (is_null($sendResult) == false && is_object($sendResult) && $sendResult->status) {

                //recive log id
                $log_id = $logSendItemResult['id'];
                //do update send item log
                $this->updateItemLog([
                    'send_item_status' => $sendResult->status ? true : false,
                    'send_item_purchase_id' => $sendResult->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $sendResult->response->purchase_status ?: 0,
                    'status' => 'success'
                ], $log_id, $uid);

                $this->updateItemHistoryLog([
                    'send_status' => 'success',
                ], $log_history_id['id'], $uid);
            } else {
                //do update send item log
                $log_id = $logSendItemResult['id'];
                $this->updateItemLog([
                    'send_item_status' => isset($sendResult->status) ? $sendResult->status : false,
                    'send_item_purchase_id' => isset($sendResult->response->purchase_id) ? $sendResult->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($sendResult->response->purchase_status) ? $sendResult->response->purchase_status : 0,
                    'status' => 'unsuccess'
                ], $log_id, $uid);

                $this->updateItemHistoryLog([
                    'send_status' => 'unsuccess',
                ], $log_history_id['id'], $uid);

                //error unsuccess log
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า (1)'
                ]);
            }
        }

        // set randon item result
        $content = [
            'id' => $randResult->package_key,
            'title' => $randResult->package_name,
            'item_type' => $randResult->package_type,
            'amount' => $randResult->package_amount,
        ];

        $can_open_gachapon = false;
        if ($after_diamond >= $eventSetting->diamonds_require && $this->checkCanOpenGachaponPeriod($eventSetting->gachapon_start, $eventSetting->gachapon_end) == true) {
            $can_open_gachapon = true;
        }

        return response()->json([
            'status' => true,
            'data' => [
                'status' => true,
                "message" => 'Gachapon Result',
                'username' => $member->username,
                'can_open_gachapon' => $can_open_gachapon,
                'result' => $content,
            ],
        ]);
    }

    private function doRandomItem()
    {
        $userData = $this->userData;
        $uid = $userData['uid'];
        $member = Member::where('uid', $uid)->first();

        if (isset($member) && isset($member->group)) {
            $groupMember = $member->group;
        } else {
            $groupMember = 'normal';
        }

        if ($groupMember === 'merchant') {
            $goodbad = array("merchant_good", "merchant_bad");
            $rand_keys = array_rand($goodbad, 1);
            $groupMember =  $goodbad[$rand_keys];
        }

        $rewards = Reward::where('reward_type', $groupMember)->get();
        if ($rewards->count() <= 0) {
            $rewards = Reward::where('reward_type', 'normal')->get();
        }

        $random = rand(1, 100000);
        $accumulatedChance = 0;
        $reward = false;

        foreach ($rewards as $value) {
            $chance = $value->chance * 1000;
            $currentChance = $accumulatedChance + $chance;

            // acquired reward
            if ($random >= $accumulatedChance && $random <= $currentChance) {
                $reward = $value;
                break;
            }
            $accumulatedChance = $currentChance;
        }

        $reward['random'] = $random;

        // now we have the reward
        return $reward;
    }

    public function getExchangeInfo(Request $request)
    {

        // get events setting info
        $eventSetting = $this->getEventSetting();
        if ($this->checkCanOpenGachaponPeriod($eventSetting->exchange_start, $eventSetting->exchange_end) == false) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br/>ไม่อยู่ในช่วงเวลากิจกรรม'
            ]);
        }


        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'exchange_info') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        // get diamonds
        $diamondsRaw = $this->diamondBalance($member->uid);
        $diamondsDecoded = json_decode($diamondsRaw);
        if ($diamondsDecoded->status == false) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถอัพเดทข้อมูล Diamonds ของคุณได้<br/>กรุณาลองใหม่อีกครั้ง'
            ]);
        }

        $diamonds = intval($diamondsDecoded->balance);

        $can_send_gift = false;
        if ($diamonds >= $eventSetting->service_charge) {
            $can_send_gift = true;
        }

        $item_list = [];

        $rareItemsQuest = Reward::where('package_type', 'rare_items')->where('reward_type', 'normal')->orderBy('order', 'ASC')->get();

        if (count($rareItemsQuest) > 0) {
            foreach ($rareItemsQuest as $item) {

                $remain = 0;
                $remain = ItemHistory::where('uid', $member->uid)->where('package_id', $item->package_id)->where('package_type', 'rare_items')->where('send_status', 'pending')->count();

                $item_list[] = [
                    'id' => $item->package_key,
                    'package_name' => $item->package_name,
                    'remain' => $remain,
                ];
            }
        }

        return response()->json([
            'status' => true,
            'data' => [
                'username' => $member->username,
                'can_send_gift' => $can_send_gift,
                'item_list' => $item_list,
            ],
        ]);
    }

    public function getCheckUid(Request $request)
    {

        // get events setting info
        $eventSetting = $this->getEventSetting();
        if ($this->checkCanOpenGachaponPeriod($eventSetting->exchange_start, $eventSetting->exchange_end) == false) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br/>ไม่อยู่ในช่วงเวลากิจกรรม'
            ]);
        }


        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'check_uid') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $sendto = (int)$decoded['sendto'];

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        if ($member->uid == $sendto) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถส่งไอเทมให้<br /> UID ของตัวเองได้'
            ]);
        }

        $chkUid = $this->associateFullGarenaWithNc($sendto);
        if (empty($chkUid)) {
            return response()->json([
                'status' => false,
                'message' => 'ไม่พบข้อมูลผู้ที่ต้องการส่งของ'
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => [
                'target_uid' => $sendto,
                'target_username' => $chkUid["user_name"]
            ],
        ]);
    }

    public function sendReward(Request $request)
    {

        // get events setting info
        $eventSetting = $this->getEventSetting();
        if ($this->checkCanOpenGachaponPeriod($eventSetting->exchange_start, $eventSetting->exchange_end) == false) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br/>ไม่อยู่ในช่วงเวลากิจกรรม'
            ]);
        }

        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'send_rewards') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        $package_key = (int)$decoded['package_key'];

        if (!isset($decoded['sendto'])) {
            $sendto = "";
            $sendto = $uid;
            $sendtype = "owner";
        } else {
            $sendtype = "gift";
            $sendto = (int)$decoded['sendto'];

            $chkUid = $this->associateFullGarenaWithNc($sendto, "");
            if (empty($chkUid)) {
                return response()->json(['status' => false, 'message' => 'ไม่พบข้อมูลผู้ที่ต้องการส่งของ']);
            }
        }

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        // check rewards by package_key
        $rewards = ItemHistory::where('uid', $member->uid)->where('package_type', 'rare_items')->where('package_key', $package_key)->where('send_status', 'pending')->first();
        if (!isset($rewards) && empty($rewards->package_id)) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br/>ของรางวัลที่ต้องการส่งมีจำนวนไม่พอ'
            ]);
        }
        $history_id = $rewards->id;

        $uid_for_send = $member->uid;
        $username_for_send = $member->username;
        $ncid_for_send = $member->ncid;

        if ($sendtype == "gift") {

            // get diamonds
            $diamondsRaw = $this->diamondBalance($member->uid);
            $diamondsDecoded = json_decode($diamondsRaw);
            if ($diamondsDecoded->status == false) {
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถอัพเดทข้อมูล Diamonds ของคุณได้<br/>กรุณาลองใหม่อีกครั้ง'
                ]);
            }

            $diamonds = intval($diamondsDecoded->balance);

            if ($diamonds < $eventSetting->service_charge) {
                return response()->json([
                    'status' => false,
                    'message' => 'ขออภัย<br/>จำนวนไดมอนด์ไม่พอส่งของขวัญ'
                ]);
            }

            $logDeduct = $this->addDeductLog([
                'uid' => $member->uid,
                'username' => $member->username,
                'ncid' => $member->ncid,
                'diamonds' => $eventSetting->service_charge,
                'send_to' => $sendto,
                'deduct_type' => 'gift',
                'status' => 'pending',
                'log_date' => date('Y-m-d'),
                'log_date_timestamp' => time(),
                'last_ip' => $this->getIP(),
            ]);

            // set deduct diamonds
            $deductData = $this->setDiamondDeductData($eventSetting->service_charge);
            if ($deductData == false) {
                return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง']);
            }

            $respDeductRaw = $this->deductDiamond($ncid, $deductData);
            $respDeduct = json_decode($respDeductRaw);

            if (!is_object($respDeduct) || is_null($respDeduct) || $respDeduct->status == false) {

                $arrDeductLog['status'] = 'unsuccess';
                $this->updateDeductLog($arrDeductLog, $logDeduct['id']);

                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถดำเนินการหัก Diamonds ได้้<br/> กรุณาติดต่อฝ่ายบริการลูกค้า'
                ]);
            }

            $before_diamond = $diamonds;
            $after_diamond = $before_diamond - $eventSetting->service_charge;

            // update deduct log
            $arrDeductLog = [
                'before_deduct_diamond' => $before_diamond,
                'after_deduct_diamond' => $after_diamond,
                'deduct_status' => $respDeduct->status,
                'deduct_purchase_id' => isset($respDeduct->response->purchase_id) ? $respDeduct->response->purchase_id : 0,
                'deduct_purchase_status' => isset($respDeduct->response->purchase_status) ? $respDeduct->response->purchase_status : 0,
                'deduct_data' => json_encode($deductData),
                'status' => 'pending'
            ];

            $this->updateDeductLog($arrDeductLog, $logDeduct['id']);

            if ($respDeduct->status) {

                $arrDeductLog['status'] = 'success';
                $this->updateDeductLog($arrDeductLog, $logDeduct['id']);

                $uid_for_send = $sendto;
                $username_for_send = $chkUid["user_name"];
                $ncid_for_send = $chkUid["user_id"];
            } else {
                $arrDeductLog['status'] = 'unsuccess';
                $this->updateDeductLog($arrDeductLog, $logDeduct['id']);
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถหักไดมอนด์ได้<br/>โปรดลองใหม่อีกครั้ง'
                ]);
            }
        }

        if ($sendtype == "gift") {
            $rewards->send_to = $sendto;
        }

        $rewards->send_status = "success";
        $rewards->send_type = $sendtype;
        if (!$rewards->save()) {
            return response()->json(['status' => false, 'message' => 'เกิดข้อผิดพลาด ลองใหม่อีกครั้ง']);
        }

        // set package data
        $goodsData[] = [
            'goods_id' => $rewards->package_id,
            'purchase_quantity' => $rewards->package_quantity,
            'purchase_amount' => 0,
            'category_id' => 40
        ];

        if (is_null($goodsData) == false && is_array($goodsData) && count($goodsData) > 0) {

            // create send item log
            $logSendItemResult = $this->addItemLog([
                'uid' => $uid_for_send,
                'username' => $username_for_send,
                'ncid' => $ncid_for_send,
                'history_id' => $rewards->id,
                'send_item_status' => false,
                'send_item_purchase_id' => 0,
                'send_item_purchase_status' => 0,
                'goods_data' => json_encode($goodsData),
                'status' => 'pending',
                'log_date' => date('Y-m-d'),
                'log_date_timestamp' => time(),
                'last_ip' => $this->getIP(),
            ]);

            // dd($logSendItemResult['id']);

            //send item
            $sendResultRaw = $this->dosendItem($ncid_for_send, $goodsData);
            $sendResult = json_decode($sendResultRaw);
            if (is_null($sendResult) == false && is_object($sendResult) && $sendResult->status) {

                //recive log id
                $log_id = $logSendItemResult['id'];
                //do update send item log
                $this->updateItemLog([
                    'send_item_status' => $sendResult->status ? true : false,
                    'send_item_purchase_id' => $sendResult->response->purchase_id ?: 0,
                    'send_item_purchase_status' => $sendResult->response->purchase_status ?: 0,
                    'status' => 'success'
                ], $log_id, $uid_for_send);

                $this->updateItemHistoryLog([
                    'send_status' => 'success',
                ], $history_id, $uid);

                // set response
                $diamondsRaw = $this->diamondBalance($member->uid);
                $diamondsDecoded = json_decode($diamondsRaw);
                if ($diamondsDecoded->status == false) {
                    return response()->json([
                        'status' => false,
                        'message' => 'ไม่สามารถอัพเดทข้อมูล Diamonds ของคุณได้<br/>กรุณาลองใหม่อีกครั้ง'
                    ]);
                }
                $diamonds = intval($diamondsDecoded->balance);

                $can_send_gift = false;
                if ($diamonds >= $eventSetting->service_charge) {
                    $can_send_gift = true;
                }


                $item_list = [];

                $rareItemsQuest = Reward::where('package_type', 'rare_items')->where('reward_type', 'normal')->orderBy('order', 'ASC')->get();

                if (count($rareItemsQuest) > 0) {
                    foreach ($rareItemsQuest as $item) {

                        $remain = 0;
                        $remain = ItemHistory::where('uid', $member->uid)->where('package_id', $item->package_id)->where('package_type', 'rare_items')->where('send_status', 'pending')->count();

                        $item_list[] = [
                            'id' => $item->package_key,
                            'package_name' => $item->package_name,
                            'remain' => $remain,
                        ];
                    }
                }

                $messageresponse = 'ไอเทมถูกส่งไปยังกล่องจดหมาย<br/>กรุณาตรวจสอบ';

                if ($sendtype == "gift") {
                    $messageresponse = 'ส่งไอเทมให้ <span>' . $chkUid["user_name"] . '</span><br/>เรียบร้อยแล้ว';
                }

                return response()->json([
                    'status' => true,
                    'message' => $messageresponse,
                    'data' => [
                        'username' => $member->username,
                        'can_send_gift' => $can_send_gift,
                        'item_list' => $item_list,
                    ],
                ]);
            } else {
                //do update send item log
                $log_id = $logSendItemResult['id'];
                $this->updateItemLog([
                    'send_item_status' => isset($sendResult->status) ? $sendResult->status : false,
                    'send_item_purchase_id' => isset($sendResult->response->purchase_id) ? $sendResult->response->purchase_id : 0,
                    'send_item_purchase_status' => isset($sendResult->response->purchase_status) ? $sendResult->response->purchase_status : 0,
                    'status' => 'unsuccess'
                ], $log_id, $uid_for_send);

                $this->updateItemHistoryLog([
                    'send_status' => 'unsuccess',
                ], $history_id, $uid);

                //error unsuccess log
                return response()->json([
                    'status' => false,
                    'message' => 'ไม่สามารถส่งไอเทมให้ได้ กรุณาติดต่อฝ่ายบริการลูกค้า (1)'
                ]);
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'ไม่สามารถส่งไอเทมให้ได้<br />กรุณาติดต่อฝ่ายบริการลูกค้า'
            ]);
        }
    }

    public function getHistory(Request $request)
    {

        // get events setting info
        $eventSetting = $this->getEventSetting();
        if ($this->checkCanOpenGachaponPeriod($eventSetting->exchange_start, $eventSetting->exchange_end) == false) {
            return response()->json([
                'status' => false,
                'message' => 'ขออภัย<br/>ไม่อยู่ในช่วงเวลากิจกรรม'
            ]);
        }


        $decoded = $request;
        if ($decoded->has('type') == false || $decoded['type'] != 'history') { //check parameter
            return response()->json([
                'status' => false,
                'message' => 'Invalid parameter!'
            ]);
        }

        $userData = $this->userData;
        $uid = $userData['uid'];
        $ncid = $this->getNcidByUid($uid);

        // get member info
        $member = Member::where('uid', $uid)->first();
        if (isset($member) && empty($member)) {
            return response()->json([
                'status' => false,
                'message' => 'No member data.'
            ]);
        }

        $itemList = [];

        $itemHistoryQuery = ItemHistory::where('uid', $member->uid)->orderBy('updated_at', 'DESC')->get();
        // dd(count($itemHistoryQuery));
        if (count($itemHistoryQuery) > 0) {

            foreach ($itemHistoryQuery as $item) {

                $title = '';
                if ($item->send_type == 'gift') {
                    $title = 'ส่ง ' . $item->package_name . ' x' . $item->package_amount . ' ให้(UID ' . $item->send_to . ')';
                } else {
                    if ($item->send_type == 'none' && $item->package_type == 'rare_items') {
                        $title = 'จัดเก็บ ' . $item->package_name . ' x' . $item->package_amount . ' ในเว็บ';
                    } else {
                        $title = 'ได้รับ ' . $item->package_name . ' x' . $item->package_amount;
                    }
                }

                $itemList[] = [
                    'title' => $title,
                    'date' => !empty($item->updated_at) && $item->updated_at != '0000-00-00 00:00:00' ? date('d/m/Y', strtotime($item->updated_at)) : '-',
                    'time' => !empty($item->updated_at) && $item->updated_at != '0000-00-00 00:00:00' ? date('H:i:s', strtotime($item->updated_at)) : '-',
                ];
            }
        }

        return response()->json([
            'status' => true,
            'data' => [
                'username' => $member->username,
                'item_list' => $itemList,
            ],
        ]);
    }
}
