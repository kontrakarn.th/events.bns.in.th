<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Api\ApiController;
use Crypt;
use Illuminate\Http\Request;
use Redirect;
use Session;

/**
 * This class deals with SSO authentication
 * Test: https://testconnect.garena.com/ui/login?app_id=10026&redirect_uri=http://testevents.garena.in.th&locale=th&display=page
 * Test: c0317355ecb86f55a55ba56b16c2008e814ca0f796f6fce52540ca9d0298cfe5
 * Live: https://sso.garena.com/ui/login?app_id=10026&redirect_uri=http://events.garena.in.th&locale=th&display=page
 * Live: 2c14892ba9489f4416a65349d6c46bb5f0a7ee3a8b02bcfc202339ece41f290b
 */
class SSOController extends ApiController
{

    private $timeout = 3600;
    protected $appid = 10046;
    protected $appkey = '2c14892ba9489f4416a65349d6c46bb5f0a7ee3a8b02bcfc202339ece41f290b';
    private $host = 'https://sso.garena.com';
    private $locale = 'th';
    private $ref = '';

    public function __construct()
    {

    }

    /*
     * Decrypt the session key
     *
     * char[3] random;
      uint8 platform: 1 - garena;
      uint32 timestamp;
      uint32 uid;
      char[16] username; (‘\0’ right padding)
      uint32 checksum, xor of previous dwords;
     * All fields are little endian
     */

    private function decryptSessionKey($sessionKey)
    {
        if (empty($sessionKey)) {
            return [];
        }
        $appkey = hex2bin($this->appkey);
        $appkey = substr($appkey, 0, 32);
        $sessionKey = hex2bin($sessionKey);
        if (strlen($sessionKey) % 16 != 0) {
            return '';
        }

        $iv = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"; // \0 * 16
        $plain = $this->decrypt_data($sessionKey, $iv, $appkey);

        $data = unpack('Z3random/Cplatform/Vtimestamp/Vuid/Z16username', $plain);
        return $data;
    }

    /**
     * AES-256 CBC decrypt in PHP (for crying out loud)
     * 
     * @param string $data
     * @param string $iv
     * @param string $key
     * @return boolean
     */
    private function decrypt_data($data, $iv, $key)
    {
        if (empty($data)) {
            return false;
        }
        $decrypted = openssl_decrypt(
            $data, 'aes-256-cbc', $key, OPENSSL_CIPHER_AES_256_CBC, $iv
        );
        return $decrypted;
    }

    public function getAccountInfo(Request $request)
    {
        $this->injectCsrfToken();

        if ($request->has('key')) {
            $key = $request->key;
        } else {
            $key = 'sso_session';
        }

        if (isset($_COOKIE[$key])) {
            $userData = $this->getUserData($key);
            if (is_array($userData)) {
                return response()->json([
                    'result' => true,
                    'account_info' => [
                        'uid' => $userData['uid'],
                        'username' => $userData['username']
                    ]
                ]);
            }
        }
        return response()->json(['result' => false]);
    }

    protected function getUserData($key = 'sso_session')
    {
        if (isset($_COOKIE[$key]) && strlen($_COOKIE[$key])) {
            $sessionKey = $_COOKIE[$key];
            $userData = $this->decryptSessionKey($sessionKey);
            if (isset($userData['uid']) && $userData['uid'] > 200000) {
                return [
                    'uid' => $userData['uid'],
                    'username' => $userData['username'],
                    'platform' => $userData['platform']
                ];
            }
        }
        return null;
    }

}
