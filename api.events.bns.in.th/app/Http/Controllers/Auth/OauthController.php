<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Api\ApiController;
use Session;
use Redirect;
use Illuminate\Http\Request;
use Cache;
use \Firebase\JWT\JWT;

class OauthController extends ApiController
{

    private $timeout = 3600;
    private $_gwsUrl = 'https://auth.garena.in.th/gws/';

    public function getAccountInfo(Request $request)
    {
        $this->injectCsrfToken();
        if (isset($_COOKIE['oauth_session'])) {
            $userData = $this->getUserData($request->has('is_mobile') && $request->is_mobile == 1);
            return response()->json([
                'result' => true,
                'account_info' => $userData
            ]);
        }
        return response()->json(['result' => false]);
    }

    public function getOauthInfo(Request $request)
    {

        if (!$request->has('access_token')) {
            return response()->json(['status' => false, 'message' => 'access token is missing']);
        }

        $keys = $request->access_token;

        $url = 'https://auth.garena.com/oauth/user/info/get?access_token=' . $keys;

        $userData = Cache::remember('events.garena.in.th:oauth_token:' . $keys, 2, function () use ($url) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url); // set url
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            $resp = curl_exec($ch);
            curl_close($ch);
            return json_decode($resp, true);
        });

        if (isset($userData['error'])) {
            return response()->json(['status' => false, 'message' => 'no user data']);
        }

        // encode jwt
        $jwt = JWT::encode($userData, env('JWT_SECRET', 'secret_jwt'));

        return response()->json(['status' => true, 'message' => 'success', 'token' => $jwt]);
    }

    protected function getJwtUserData(Request $request)
    {

        $auth = $request->header('Authorization');

        if (empty($auth)) {
            return null;
        }

        if (strpos($auth, 'Bearer ') != 0) {
            return null;
        }

        $token = str_replace('Bearer ', '', $auth);
        // dd($token);
        $userDataEncode = JWT::decode($token, env('JWT_SECRET', 'secret_jwt'), array('HS256'));

        return (array) $userDataEncode;
    }

    protected function getUserData($mobile = false)
    {
        if (isset($_COOKIE['oauth_session'])) {
            $token = $_COOKIE['oauth_session'];
            $userData = $this->getUserInfo($token);
            if (isset($userData['error'])) {
                return null;
            }
            if ($mobile && empty($userData['open_id'])) {
                return null;
            }
            return $userData;
        }
        return null;
    }


    /*
         * Get user data from the oath
         *
         * @param string $token
         * @return type
         */

    private function getUserInfo($token)
    {
        if (Session::has('sandbox')) {
            $url = 'https://testconnect.garena.com/oauth/user/info/get?access_token=' . $token;
        } else {
            $url = 'https://auth.garena.com/oauth/user/info/get?access_token=' . $token;
        }
        $userData = Cache::remember('events.garena.in.th:oauth:' . $token, 5, function () use ($url) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url); // set url
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 2);
            $resp = curl_exec($ch);
            curl_close($ch);
            return json_decode($resp, true);
        });
        return $userData;
    }

    public function get_account_full($uid = null)
    {
        if ($uid == null)
            $_data = $this->post($this->_gwsUrl, array('key' => 'get_account_full', 'params' => Session::get('glogin')->data->uid));
        else
            $_data = $this->post($this->_gwsUrl, array('key' => 'get_account_full', 'params' => $uid));
        return json_decode($_data, true);
    }

    public function get_account_info_by_uid($uid)
    {
        $_data = $this->post($this->_gwsUrl, array('key' => 'get_account_info', 'params' => $uid));
        return json_decode($_data, true);
    }

    public function get_account_info()
    {
        $_data = $this->post($this->_gwsUrl, array('key' => 'get_account_info', 'params' => Session::get('glogin')->data->uid));
        return json_decode($_data, true);
    }

    protected function post($url, $params)
    {
        foreach ($params as $key => $value) {
            $data[] = $key . '=' . $value;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, implode('&', $data));
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        $resp = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        return $resp;
    }
}
