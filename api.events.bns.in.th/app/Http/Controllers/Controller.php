<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $viewData = [];

    /*
     * @author Golf (nithiwat@garena.co.th)
     */

    function __construct()
    {
        $this->viewData['title'] = '';
        $this->viewData['description'] = '';
        $this->viewData['keywords'] = '';
        $this->viewData['fb_app_id'] = '';
        $this->viewData['og_type'] = '';
        $this->viewData['og_image'] = '';
        $this->viewData['og_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $this->viewData['url_rss'] = '';
        $this->viewData['assets_css'] = [];
        $this->viewData['assets_js'] = [];
        $this->viewData['analytics'] = '';
        $this->viewData['css'] = []; // local stylesheets
        $this->viewData['js'] = []; // local scripts
        $this->viewData['breadcrumbs'] = [];
        $this->setLanguage();
    }

    protected function initCss($css)
    {
        $this->viewData['css'] = $css;
        $this->prepareCss();
    }

    protected function initJs($js)
    {
        $this->viewData['js'] = $js;
        $this->prepareJs();
    }

    protected function prepareCss()
    {
        $css = [];
        foreach ($this->viewData['css'] as $key => $value) {
            foreach ($value as $filename) {
                $css[] = $key . '/' . $filename . '.css';
            }
        }
        $this->viewData['css'] = $css;
    }

    protected function addCss($directory, $filename)
    {
        if (is_array($filename)) {
            foreach ($filename as $key => $value) {
                $this->viewData['css'][] = $directory . '/' . $value . '.css';
            }
        } else {
            $this->viewData['css'][] = $directory . '/' . $filename . '.css';
        }
    }

    protected function prepareJs()
    {
        $js = [];
        foreach ($this->viewData['js'] as $key => $value) {
            foreach ($value as $filename) {
                $js[] = $key . '/' . $filename . '.js';
            }
        }
        $this->viewData['js'] = $js;
    }

    protected function addJs($directory, $filename)
    {
        if (is_array($filename)) {
            foreach ($filename as $key => $value) {
                $this->viewData['js'][] = $directory . '/' . $value . '.js';
            }
        } else {
            $this->viewData['js'][] = $directory . '/' . $filename . '.js';
        }
    }

    // this method is used to set the display language of the website
    public function getLang($lang)
    {
        $uri = request()->get('uri');
        Session::put('lang', $lang);
        Session::save();
        return redirect()->to($uri);
    }

    protected function setLanguage()
    {
        if (Session::has('lang')) {
            $lang = 'en'; // TODO: remove this
            //$lang = Session::get('lang');
            App::setLocale($lang);
        } else if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            switch ($lang) {
                case 'th':
                    App::setLocale('th');
                default:
                    App::setLocale('en');
            }
        } else {
            App::setLocale('en');
        }
    }

    /**
     * Get set magic method
     */
    public function __set($key, $value)
    {
        return $this->viewData[$key] = $value;
    }

    public function __get($key)
    {
        return $this->viewData[$key];
    }
}
