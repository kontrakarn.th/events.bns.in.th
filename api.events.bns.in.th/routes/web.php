<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'sso', 'namespace' => 'Auth'], function () {
    Route::post('/get_account_info', 'SSOController@getAccountInfo');
});

Route::group(['prefix' => 'oauth', 'namespace' => 'Auth'], function () {
    Route::post('/get_account_info', 'OauthController@getAccountInfo');
    Route::get('/get_oauth', 'OauthController@getOauthInfo');
});


Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {

    $bnsRoutes = function () {
        Route::group(['middleware' => 'event_middleware'], function () {

            // Events Lobby
            Route::group(['prefix' => 'hm_pad', 'namespace' => 'hm_pad'], function () {
                Route::post('/event_info', 'IndexController@getEventInfo');
            });

            // Topup November 2018
            /* Route::group(['prefix' => 'topup_nov2018', 'namespace' => 'topup_nov2018'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/redeem', 'IndexController@redeemReward');
            }); */

            // New & Revival User 2018
            /* Route::group(['prefix' => 'season2_revival', 'namespace' => 'season2_revival'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/redeem', 'IndexController@redeemReward');
            }); */

            // Hongmoon Battle Pass
            /* Route::group(['prefix' => 'hm_battlepass', 'namespace' => 'hm_battlepass'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/get_character', 'IndexController@getCharacter');
                Route::post('/accept_character', 'IndexController@acceptChar');
                Route::post('/redeem', 'IndexController@redeemReward');
                Route::post('/unlock_special_reward', 'IndexController@unlockSpecialReward');
                Route::post('/multiplied_reward', 'IndexController@multipliedReward');
                Route::post('/random_new_mission', 'IndexController@randomNewQuest');
                Route::post('/ticket_pass', 'IndexController@ticketPass');
                Route::post('/ticket_five_pass', 'IndexController@ticketFivePass');
            }); */

            // Airpay Topup Decrmber 2018
            /* Route::group(['prefix' => 'airpay_dec2018', 'namespace' => 'airpay_dec2018'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/play', 'IndexController@playGacha');
                Route::post('/exchange', 'IndexController@exchangeItem');
                // Route::post('/redeem', 'IndexController@redeemReward');
            }); */

            // Moonstone Festival
            /* Route::group(['prefix' => 'moonstone_festival', 'namespace' => 'moonstone_festival'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/get_character', 'IndexController@getCharacter');
                Route::post('/accept_character', 'IndexController@acceptChar');
                Route::post('/redeem', 'IndexController@redeemReward');
            }); */

            // Hello Warden
            /* Route::group(['prefix' => 'hello_warden', 'namespace' => 'hello_warden'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                // Route::post('/get_character', 'IndexController@getCharacter');
                // Route::post('/accept_character', 'IndexController@acceptChar');
                Route::post('/redeem', 'IndexController@redeemReward');
            }); */

            // Happy Xmas 2018
            /* Route::group(['prefix' => 'happy_xmas2018', 'namespace' => 'happy_xmas2018'], function() {
                Route::post('/event', 'IndexController@postIndex');
            }); */

            // Preregis Warden
            /* Route::group(['prefix' => 'preregis_warden', 'namespace' => 'preregis_warden'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/get_character', 'IndexController@getCharacter');
                Route::post('/accept_character', 'IndexController@acceptChar');
                Route::post('/send_answer', 'IndexController@sendAnswer');
                Route::post('/redeem', 'IndexController@redeemReward');
            }); */

            // Pre-Order Warden Package
            /* Route::group(['prefix' => 'preorder_warden', 'namespace' => 'preorder_warden'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/purchase', 'IndexController@purchasePackage');
            }); */

            // BNSxADVICE Gachapon
            /* Route::group(['prefix' => 'bns_advice', 'namespace' => 'bns_advice'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/get_character', 'IndexController@getCharacter');
                Route::post('/accept_character', 'IndexController@acceptChar');
                Route::post('/redeem_code', 'IndexController@redeemAdviceCode');
                Route::post('/advice_gachapon', 'IndexController@openAdviceGachapon');
                Route::post('/free_gachapon', 'IndexController@openFreeGachapon');
                Route::post('/exchange', 'IndexController@exchangeRewards');
            }); */

            // Airpay Topup January 2019
            /* Route::group(['prefix' => 'airpay_jan2019', 'namespace' => 'airpay_jan2019'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/airpay_gachapon', 'IndexController@openGachapon');
                Route::post('/exchange', 'IndexController@exchangeRewards');
            }); */

            // Lunar New Year 2019
            /* Route::group(['prefix' => 'lunar_newyear', 'namespace' => 'lunar_newyear'], function() {
                Route::post('/get_character', 'IndexController@getCharacter');
                Route::post('/accept_character', 'IndexController@acceptChar');
                Route::post('/lobby_info', 'IndexController@getLobyInfo');
                Route::post('/daily_info', 'IndexController@getDailyInfo');
                Route::post('/buns_info', 'IndexController@getBunsInfo');
                Route::post('/play_buns_fortune', 'IndexController@playBunsFortune');
                Route::post('/angpao_info', 'IndexController@getAngpaoInfo');
                Route::post('/play_angpao', 'IndexController@startPlayAngpao');
                Route::post('/save_angpao_result', 'IndexController@saveAngpaoPlayResult');
                Route::post('/update_shop_score', 'IndexController@updateScoreToShop');
                Route::post('/shop_info', 'IndexController@getGoldShopInfo');
                Route::post('/exchange', 'IndexController@getExchangeGoldReward');
                Route::post('/rank_info', 'IndexController@getRankInfo');
                Route::post('/redeem_rank_reward', 'IndexController@redeemRankReward');
            }); */

            // soul gachapon valentine
            /* Route::group(['prefix' => 'soul_gachapon_valentine', 'namespace' => 'soul_gachapon_valentine'], function() {
                Route::post('/event_info', 'SoulGachaValentine@getEventInfo');
                Route::post('/buy_soul', 'SoulGachaValentine@buySoul');
                Route::post('/merge', 'SoulGachaValentine@mergeItem');
                Route::post('/redeem', 'SoulGachaValentine@redeemItem');
                Route::post('/item_history', 'SoulGachaValentine@getItemHistory');
                Route::post('/material', 'SoulGachaValentine@getMaterial');
                Route::post('/my_bag', 'SoulGachaValentine@getMyBag');
                Route::post('/send_item', 'SoulGachaValentine@getSendItem');
                Route::post('/send_gift', 'SoulGachaValentine@getSendGift');
                Route::post('/check_uid', 'SoulGachaValentine@getCheckUid');
                Route::post('/exchange_potion', 'SoulGachaValentine@getExchangePotion');
                Route::post('/redeem_airpay_promotion', 'SoulGachaValentine@redeemAirpayPromotion');
            }); */

            // Wardrobe 2019 Package
            /* Route::group(['prefix' => 'wardrobe_package_2019', 'namespace' => 'wardrobe_package_2019'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/purchase', 'IndexController@purchasePackage');
            }); */

            // Snake Ladders
            /* Route::group(['prefix' => 'snakeladder', 'namespace' => 'snakeladder'], function() {
                Route::post('/get_character', 'IndexController@getCharacter');
                Route::post('/accept_character', 'IndexController@acceptChar');
                Route::post('/lobby_info', 'IndexController@getLobyInfo');
                Route::post('/free_board_info', 'IndexController@getFreeBoardInfo');
                Route::post('/play_free_dice', 'IndexController@playFreeDice');
                Route::post('/paid_board_info', 'IndexController@getPaidBoardInfo');
                Route::post('/random_paid_board', 'IndexController@getRandomBoard');
                Route::post('/accept_board', 'IndexController@acceptBoard');
                Route::post('/reject_board', 'IndexController@rejectBoard');
                Route::post('/paid_dice', 'IndexController@getPaidDiceBoard');
                Route::post('/item_history', 'IndexController@getItemHistory');
                Route::post('/my_bag', 'IndexController@getMyBag');
                Route::post('/send_item', 'IndexController@getSendItem');
                Route::post('/send_gift', 'IndexController@getSendGift');
                Route::post('/check_uid', 'IndexController@getCheckUid');
            }); */

            // New Revival taechoen
            /* Route::group(['prefix'=>'new_revival_taecheon','namespace'=>'new_revival_taecheon'],function(){
                Route::post('/checkin','NewRevivalTaecheon@checkin');
                Route::post('/redeem','NewRevivalTaecheon@redeem');
            }); */

            // Gacha 5 Baht
            Route::group(['prefix' => 'gacha5baht', 'namespace' => 'gacha5baht'], function () {
                // Route::get('/event_status', 'IndexController@checkEventStatus');
                Route::post('/checkin', 'IndexController@checkin');

                Route::post('/buy_card', 'IndexController@buyCard');
                Route::post('/item_history', 'IndexController@getItemHistory');
            });

            // Hello Summer 2019
            Route::group(['prefix' => 'hello_summer', 'namespace' => 'hello_summer'], function () {
                Route::post('/get_character', 'IndexController@getCharacter');
                Route::post('/accept_character', 'IndexController@acceptChar');
                Route::post('/lobby_info', 'IndexController@getLobyInfo');
                Route::post('/daily_info', 'IndexController@getDailyInfo');
                Route::post('/play_water_gachapon', 'IndexController@getPlayWaterGachapon');
                Route::post('/water_drop_info', 'IndexController@getWaterDropInfo');
                Route::post('/play_water_drop', 'IndexController@playWaterDrop');
                Route::post('/exchange_info', 'IndexController@getExchangeInfo');
                Route::post('/exchange_reward', 'IndexController@exchangeReward');
                Route::post('/item_history', 'IndexController@getItemHistory');
            });

            // 2 Years Anniversary Passport
            Route::group(['prefix' => 'anniversary_passport', 'namespace' => 'anniversary_passport'], function () {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/buy_anniversary_package', 'IndexController@getBuyPassportPackage');
                Route::post('/checkin', 'IndexController@getCheckin');
                Route::post('/claim_special_reward', 'IndexController@getClaimSpecialReward');
                Route::post('/claim_final_reward', 'IndexController@getClaimFinalReward');
            });

            // topup may 2019
            Route::group([
                'prefix' => 'topup_may2019', 'namespace' => 'topup_promotion_2019'
            ], function () {
                Route::get('/checkin', 'TopupPromotionController@checkin');
                Route::post('/redeem', 'TopupPromotionController@doRedeem');
            });

            // BSTC 2019
            Route::group([
                'prefix' => 'bstc2019', 'namespace' => 'bstc2019'
            ], function () {
                Route::post('/register', 'BSTCController@register');
            });

            //revival_may2019
            Route::group(['prefix' => 'revival_may2019', 'namespace' => 'revival_may2019'], function () {
                Route::post('/checkin', 'IndexController@checkin');
                Route::post('/redeem', 'IndexController@redeem');
            });

            //mix pet gacha
            Route::group(['prefix' => 'mixpetgacha', 'namespace' => 'mixpetgacha'], function () {
                Route::post('/checkin', 'IndexController@checkin');
                Route::post('/randompet', 'IndexController@randomPet');
                Route::post('/getexchangeinfo', 'IndexController@getExchangeInfo');
                Route::post('/exchangeitem', 'IndexController@exchangeItem');
                Route::post('/history', 'IndexController@historyList');
            });

            // Topup November 2018
            Route::group(['prefix' => 'topup_july2019', 'namespace' => 'topup_july2019'], function () {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/redeem', 'IndexController@redeemReward');
            });

            //bmjuly
            Route::group(['prefix' => 'bmjuly', 'namespace' => 'bmjuly'], function () {
                Route::post('/checkin', 'IndexController@checkin');
                Route::post('/randomitem', 'IndexController@randomItem');
                Route::post('/getname', 'IndexController@getName');
                Route::post('/getexchangeinfo', 'IndexController@getExchangeInfo');
                Route::post('/exchangeitem', 'IndexController@exchangeItem');
                Route::post('/history', 'IndexController@historyList');
            });

            //buffet
            Route::group(['prefix'=>'buffet','namespace'=>'buffet'],function(){
                Route::post('/event_info','IndexController@getEventInfo');
                Route::post('/select_char','IndexController@selectCharacter');
                Route::post('/eat_buffet','IndexController@eatBuffet');
                Route::post('/redeem','IndexController@redeemReward');
                Route::post('/history','IndexController@getHistory');
            });

            // topup august 2019
            Route::group(['prefix' => 'topup_august2019', 'namespace' => 'topup_august2019'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/redeem', 'IndexController@redeemReward');
            });

            Route::group(['prefix' => 'fightersoul', 'namespace' => 'fightersoul'], function () {
                Route::post('/checkin', 'IndexController@checkin');
                Route::post('/randomitem', 'IndexController@randomItem');
                Route::post('/getname', 'IndexController@getName');
                Route::post('/getexchangeinfo', 'IndexController@getExchangeInfo');
                Route::post('/exchangeitem', 'IndexController@exchangeItem');
                Route::post('/history', 'IndexController@historyList');
            });

            // Archer Pre-register
            Route::group(['prefix' => 'archer_preregister', 'namespace' => 'archer_preregister'], function() {
                Route::get('/pre_register_info', 'IndexController@getPreRegisterInfo');
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/accept_preregister', 'IndexController@acceptPreregister');
            });

            // Archer Package Pre-Order
            Route::group(['prefix' => 'archer_preorder', 'namespace' => 'archer_preorder'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/purchase', 'IndexController@purchasePackage');
            });
            //revival_sep2019
            Route::group(['prefix' => 'revival_sep2019', 'namespace' => 'revival_sep2019'], function () {
                Route::post('/checkin', 'IndexController@checkin');
                Route::post('/redeem', 'IndexController@redeem');
            });

            //archer_support
            Route::group(['prefix'=>'archer_support','namespace'=>'archer_support'],function(){
                Route::post('/event_info','IndexController@getEventInfo');
                Route::post('/select_char','IndexController@selectCharacter');
                Route::post('/redeem','IndexController@redeemReward');
            });

            // soul gachapon silver moon
            Route::group(['prefix' => 'soulgacha_silvermoon', 'namespace' => 'soulgacha_silvermoon'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/buy_soul', 'IndexController@buySoul');
                Route::post('/merge', 'IndexController@mergeItem');
                Route::post('/redeem', 'IndexController@redeemItem');
                Route::post('/item_history', 'IndexController@getItemHistory');
                Route::post('/material', 'IndexController@getMaterial');
                Route::post('/my_bag', 'IndexController@getMyBag');
                Route::post('/send_item', 'IndexController@getSendItem');
                Route::post('/send_gift', 'IndexController@getSendGift');
                Route::post('/check_uid', 'IndexController@getCheckUid');
                Route::post('/exchange_potion', 'IndexController@getExchangePotion');
            });

            // Archer Package
            Route::group(['prefix' => 'archer_package', 'namespace' => 'archer_package'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/purchase', 'IndexController@purchasePackage');
            });

            // Greyed Out 2019
            Route::group(['prefix' => 'greyed_out2019', 'namespace' => 'greyed_out2019'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/select_reward', 'IndexController@getSelectReward');
                Route::post('/lets_pool', 'IndexController@getLetsPool');
            });
            // Halloween 2019
            Route::group(['prefix' => 'halloween2019', 'namespace' => 'halloween2019'], function() {
                Route::post('/event_info'       ,'IndexController@getEventInfo');
                Route::post('/select_char'      ,'IndexController@selectCharacter');
                Route::post('/redeem'           ,'IndexController@redeemReward');
//                Route::post('/test'             ,'IndexController@test');
//                Route::post('/purchase', 'IndexController@purchasePackage');
            });

            // HM Elite 2019
            Route::group(['prefix' => 'hmelite_2019', 'namespace' => 'hmelite_2019'], function() {
                Route::post('/event_info','IndexController@getEventInfo');
                Route::post('/member_info','IndexController@getMemberInfo');
                Route::post('/get_ranking','IndexController@getRanking');
                Route::post('/check_uid','IndexController@getCheckUid');
                Route::post('/update_member','IndexController@updateMemberProfile');

                Route::post('/play_daily','IndexController@playDaily');
                Route::post('/play_weekly','IndexController@playWeekly');
                Route::post('/play_birthday','IndexController@playBirthday');
                Route::post('/buy_excusive_owner','IndexController@buyExcusiveOwner');
                Route::post('/buy_excusive_other','IndexController@buyExcusiveOther');
            });

            // HM Welfare Package
            Route::group(['prefix' => 'hm_welfare_package', 'namespace' => 'hm_welfare_package'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/booking', 'IndexController@bookingPackage');
                Route::post('/purchase', 'IndexController@purchasePackage');
            });

            //bmnov
            Route::group(['prefix' => 'bmnov', 'namespace' => 'bmnov'], function () {
                Route::post('/checkin', 'IndexController@checkin');
                Route::post('/randomitem', 'IndexController@randomItem');
                Route::post('/getname', 'IndexController@getName');
                Route::post('/getexchangeinfo', 'IndexController@getExchangeInfo');
                Route::post('/exchangeitem', 'IndexController@exchangeItem');
                Route::post('/history', 'IndexController@historyList');
            });

            // Lantern Festival
            Route::group(['prefix' => 'lantern_festival', 'namespace' => 'lantern_festival'], function() {
                Route::post('/event_info'       ,'IndexController@getEventInfo');
                Route::post('/select_char'      ,'IndexController@selectCharacter');
                Route::post('/redeem'           ,'IndexController@redeemReward');
//                Route::get('/test'           ,'IndexController@test');
            });

            // Clan Event
            Route::group(['prefix' => 'clan_event', 'namespace' => 'clan_event'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/select_char', 'IndexController@selectCharacter');
                Route::post('/redeem_clan_reward', 'IndexController@redeemClanReward');
                Route::post('/redeem_rank_reward', 'IndexController@redeemRankReward');
            });

            // Gacha 5 Baht Nov
            Route::group(['prefix' => 'gacha5baht_nov', 'namespace' => 'gacha5baht_nov'], function () {
                // Route::get('/event_status', 'IndexController@checkEventStatus');
                Route::post('/checkin', 'IndexController@checkin');

                Route::post('/buy_card', 'IndexController@buyCard');
                Route::post('/item_history', 'IndexController@getItemHistory');
                Route::get('/test_rate/{num_test?}', 'IndexController@testRate');
            });

            // Gacha 5 Baht May 2020
            Route::group(['prefix' => 'gacha5baht_may', 'namespace' => 'gacha5baht_may'], function () {
                // Route::get('/event_status', 'IndexController@checkEventStatus');
                Route::post('/checkin', 'IndexController@checkin');

                Route::post('/buy_card', 'IndexController@buyCard');
                Route::post('/item_history', 'IndexController@getItemHistory');
                Route::get('/test_rate/{num_test?}', 'IndexController@testRate');
            });

            // Chain Package
            Route::group(['prefix' => 'chain_package', 'namespace' => 'chain_package'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/purchase', 'IndexController@purchasePackage');
            });

            // Lantern Festival
            Route::group(['prefix' => 'pvp_event', 'namespace' => 'pvp_event'], function() {
                Route::post('/event_info'       ,'IndexController@getEventInfo');
                Route::post('/select_char'      ,'IndexController@selectCharacter');
                Route::post('/exchange'           ,'IndexController@exchangeReward');
            });

            // Exchange gift
            Route::group(['prefix' => 'exchange_gift', 'namespace' => 'exchange_gift'], function() {
                Route::post('/event_info'    ,'IndexController@getEventInfo');
                Route::post('/select_char'   ,'IndexController@selectCharacter');
                Route::post('/set_gift'      ,'IndexController@setGift');
                Route::post('/exchange_gift' ,'IndexController@exchangeGift');
                Route::post('/add_gmitem' ,'IndexController@addGMItem');
                Route::post('/high_price' ,'IndexController@GetGiftHighPrice');
                Route::post('/who_get_gm' ,'IndexController@WhoGetGM');
                Route::post('/my_history_pack' ,'IndexController@MyHistoryPack');
            });

            // 2 Years Anniversary Passport
            Route::group(['prefix' => 'dex_package', 'namespace' => 'passport_2'], function () {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/buy_dex_package', 'IndexController@getBuyPassportPackage');
                Route::post('/checkin', 'IndexController@getCheckin');
                Route::post('/claim_special_reward', 'IndexController@getClaimSpecialReward');
                Route::post('/claim_final_reward', 'IndexController@getClaimFinalReward');
            });

            //revival_jan2020
            Route::group(['prefix' => 'revival_jan2020', 'namespace' => 'revival_jan2020'], function () {
                Route::post('/checkin', 'IndexController@checkin');
                Route::post('/redeem', 'IndexController@redeem');
            });

            // 3rd Spec Package Pre-Order
            Route::group(['prefix' => 'preorder_3rdspec', 'namespace' => 'preorder_3rdspec'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/purchase', 'IndexController@purchasePackage');
            });

            // 3rd Spec Package
            Route::group(['prefix' => 'package_3rdspec', 'namespace' => 'package_3rdspec'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/purchase', 'IndexController@purchasePackage');
            });

            // soul gachapon silver moon
            Route::group(['prefix' => 'black_valentine', 'namespace' => 'black_valentine'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/buy_soul', 'IndexController@buySoul');
                Route::post('/merge', 'IndexController@mergeItem');
                Route::post('/redeem', 'IndexController@redeemItem');
                Route::post('/item_history', 'IndexController@getItemHistory');
                Route::post('/material', 'IndexController@getMaterial');
                Route::post('/my_bag', 'IndexController@getMyBag');
                Route::post('/send_item', 'IndexController@getSendItem');
                Route::post('/send_gift', 'IndexController@getSendGift');
                Route::post('/check_uid', 'IndexController@getCheckUid');
                Route::post('/exchange_potion', 'IndexController@getExchangePotion');
            });

            // soul gachapon phoenix
            Route::group(['prefix' => 'phoenix', 'namespace' => 'phoenix'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/buy_soul', 'IndexController@buySoul');
                Route::post('/merge', 'IndexController@mergeItem');
                Route::post('/redeem', 'IndexController@redeemItem');
                Route::post('/item_history', 'IndexController@getItemHistory');
                Route::post('/material', 'IndexController@getMaterial');
                Route::post('/my_bag', 'IndexController@getMyBag');
                Route::post('/send_item', 'IndexController@getSendItem');
                Route::post('/send_gift', 'IndexController@getSendGift');
                Route::post('/check_uid', 'IndexController@getCheckUid');
                Route::post('/exchange_potion', 'IndexController@getExchangePotion');
            });

            // Piggy
            Route::group(['prefix' => 'piggy', 'namespace' => 'piggy'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/select_char'   ,'IndexController@selectCharacter');
                Route::post('/select_daily_quest', 'IndexController@selectDailyQuest');
                Route::post('/claim_daily_quest', 'IndexController@claimDailyQuest');
                Route::post('/claim_piggy_bank', 'IndexController@claimPiggyBank');
                Route::post('/history', 'IndexController@getHistory');
            });

            // material support
            Route::group(['prefix' => 'material_support', 'namespace' => 'material_support'], function () {
                // Route::get('/event_status', 'IndexController@checkEventStatus');
                Route::post('/checkin', 'IndexController@checkin');

                Route::post('/buy_gacha', 'IndexController@buyGacha');
                Route::post('/item_history', 'IndexController@getItemHistory');
                // Route::get('/test_rate/{num_test?}', 'IndexController@testRate');
            });

            // Daily Stamp
            Route::group(['prefix' => 'daily_stamp', 'namespace' => 'daily_stamp'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/select_char'   ,'IndexController@selectCharacter');
                // Route::post('/select_daily_quest', 'IndexController@selectDailyQuest');
                // Route::post('/claim_daily_quest', 'IndexController@claimDailyQuest');
                // Route::post('/claim_piggy_bank', 'IndexController@claimPiggyBank');
                Route::post('/history', 'IndexController@getHistory');

                Route::post('/claim_free', 'IndexController@claimFree');
                Route::post('/claim_diamond', 'IndexController@claimDiamond');
                Route::post('/unlock_daily_quest', 'IndexController@unlockDailyQuest');
                Route::post('/claim_stamp', 'IndexController@claimStamp');
            });

            // Kylin
            Route::group(['prefix' => 'kylin', 'namespace' => 'kylin'], function () {
                // Route::get('/event_status', 'IndexController@checkEventStatus');
                Route::post('/checkin', 'IndexController@checkin');

                Route::post('/buy', 'IndexController@buy');
            });

            Route::group(['prefix' => 'vip_benefit', 'namespace' => 'vip_benefit'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/claim_reward', 'IndexController@getClaimVipReward');
            });

            Route::group(['prefix' => 'league', 'namespace' => 'league'], function() {
                Route::post('/select_char'   ,'IndexController@selectCharacter');
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/register', 'IndexController@postRegister');
                Route::post('/competition_info', 'IndexController@getCompetitionInfo');
                Route::post('/personal_info', 'IndexController@getPersonalInfo');
                Route::post('/claim_free_points', 'IndexController@postClaimFreePoints');
                Route::post('/claim_quest_points', 'IndexController@postClaimQuestPoints');
                Route::post('/select_special_quest', 'IndexController@postSelectSpecialQuest');
                Route::post('/claim_special_quest_points', 'IndexController@postClaimSpecialQuestPoints');
                Route::post('/claim_personal_reward', 'IndexController@postClaimPersonalReward');
                Route::post('/color_rank_info', 'IndexController@getColorRankInfo');
                Route::post('/history', 'IndexController@getHistory');
                Route::post('/reward_info', 'IndexController@getRewardInfo');
                Route::post('/claim_rank_reward', 'IndexController@postClaimRankReward');
                Route::post('/claim_color_reward', 'IndexController@postClaimColorReward');
            });

            // Hardmode
            Route::group(['prefix' => 'hardmode', 'namespace' => 'hardmode'], function() {
                Route::post('/select_char'   ,'IndexController@selectCharacter');
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/claim_free_points', 'IndexController@postClaimFreePoints');
                Route::post('/claim_purchase_points', 'IndexController@postClaimPurchasePoints');
                Route::post('/claim_special_quest_points', 'IndexController@postClaimSpecialQuestPoints');
                Route::post('/purchase_package', 'IndexController@postPurchasePackage');
                Route::post('/open_box', 'IndexController@openBox');
                Route::post('/claim_reward', 'IndexController@postClaimReward');
                Route::post('/history', 'IndexController@getHistory');
            });

            // Daily Login
            Route::group(['prefix' => 'daily_login', 'namespace' => 'daily_login'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/claim_coupon', 'IndexController@claimCoupon');
                Route::post('/checkin', 'IndexController@getCheckin');
            });

             //revival_apr2020
             Route::group(['prefix' => 'revival_apr2020', 'namespace' => 'revival_apr2020'], function () {
                Route::post('/checkin', 'IndexController@checkin');
                Route::post('/redeem', 'IndexController@redeem');
            });

            // Songkran 2020
            Route::group(['prefix' => 'songkran2020', 'namespace' => 'songkran2020'], function() {
                Route::post('/event_info'       ,'IndexController@getEventInfo');
                Route::post('/select_char'      ,'IndexController@selectCharacter');
                Route::post('/redeem'           ,'IndexController@redeemReward');
                Route::post('/getHistory'       ,'IndexController@getHistory');
            });

            // BM April 2020
            Route::group(['prefix' => 'bmapril2020', 'namespace' => 'bmapril2020'], function () {
                Route::post('/info', 'IndexController@getEventInfo');
                Route::post('/buy_gachapon', 'IndexController@buyGachapon');
                Route::post('/exchange_info', 'IndexController@getExchangeInfo');
                Route::post('/check_uid', 'IndexController@getCheckUid');
                Route::post('/send_reward', 'IndexController@sendReward');
                Route::post('/history', 'IndexController@getHistory');
            });

            // Destroyer 3rd Spec Package
            Route::group(['prefix' => 'package_des3rd', 'namespace' => 'package_des3rd'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/purchase', 'IndexController@purchasePackage');
            });

            // 3rd Years Anniversary
            Route::group(['prefix' => 'anniversary_3rd', 'namespace' => 'anniversary_3rd'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/claim_reward', 'IndexController@claimReward');
            });

            // Battle_ Field
            Route::group(['prefix' => 'battle_field', 'namespace' => 'battle_field'], function() {
                Route::post('/event_info'       ,'IndexController@getEventInfo');
                Route::post('/select_char'      ,'IndexController@selectCharacter');
                Route::post('/claim_free'       ,'IndexController@claimFree');
                Route::post('/redeem'           ,'IndexController@redeemReward');
                Route::post('/getHistory'       ,'IndexController@getHistory');
            });

            // 3rd Years Anniversary Passport
            Route::group(['prefix' => 'anniversary_3rd_passport', 'namespace' => 'anniversary_3rd_passport'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/unlock_passport', 'IndexController@getUnlockPassport');
                Route::post('/checkin', 'IndexController@getCheckin');
                Route::post('/claim_weekly_reward', 'IndexController@getClaimWeeklyReward');
                Route::post('/claim_final_reward', 'IndexController@getClaimFinalReward');
            });

            // topup may 2020
            Route::group(['prefix' => 'topup_may2020', 'namespace' => 'topup_may2020'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/claim_reward', 'IndexController@getClaimReward');
            });

            // Hey Duo
            Route::group(['prefix' => 'hey_duo', 'namespace' => 'hey_duo'], function() {
                Route::post('/select_char'   ,'IndexController@selectCharacter');
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/duo_info_confirm', 'IndexController@getDuoInfoConfirm');
                Route::post('/request_duo', 'IndexController@requestDuo');
                Route::post('/duo_requests', 'IndexController@getDuoRequests');
                Route::delete('/duo_requests/{id}', 'IndexController@deleteDuoRequest');
                Route::post('/duo_receives', 'IndexController@getDuoReceives');
                Route::put('/duo_receives/{id}/accept', 'IndexController@acceptDuoReceive');
                Route::put('/duo_receives/{id}/reject', 'IndexController@rejectDuoReceive');
                Route::post('/claim_free_points', 'IndexController@claimFreePoints');
                Route::post('/claim_quest_points', 'IndexController@claimQuestPoints');
                Route::post('/exchange_reward', 'IndexController@exchangeReward');
                Route::post('/exchange_special_reward', 'IndexController@exchangeSpecialReward');
                Route::post('/histories', 'IndexController@getHistory');
            });

            // Cash back Package
            Route::group(['prefix' => 'cashbackpackage', 'namespace' => 'cashbackpackage'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/unlock_passport', 'IndexController@getUnlockPassport');
                Route::post('/checkin', 'IndexController@getCheckin');
                Route::post('/claim_final_reward', 'IndexController@getClaimFinalReward');
                Route::post('/history', 'IndexController@getHistory');
                Route::post('/compensation_history', 'IndexController@getCompensationHistory');
                Route::post('/test_api', 'IndexController@testApi');
            });
            Route::group(['prefix' => 'mystic', 'namespace' => 'mystic'], function() {
                Route::post('/random'   ,'IndexController@Random');
                Route::post('/specialproduct'   ,'IndexController@GetSpecialProduct');
                Route::post('/userselectproduct'   ,'IndexController@userselectproduct');
                Route::post('/getkey'   ,'IndexController@GetKey');
                Route::post('/gethistory'   ,'IndexController@GetHistory');
                Route::post('/resetslot'   ,'IndexController@ResetSlot');
                Route::post('/event_info', 'IndexController@getEventInfo');

            });

            // Hongmoon Pass Season 2
            Route::group(['prefix' => 'hm_pass_ss2', 'namespace' => 'hm_pass_ss2'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/select_char'   ,'IndexController@selectCharacter');
                Route::post('/unlock_package'   ,'IndexController@unlockPackage');
                Route::post('/unlock_adv_package'   ,'IndexController@unlockAdvancedPackage');
                Route::post('/purchase_level'   ,'IndexController@purchaseLevel');
                Route::post('/claim_special_gift'   ,'IndexController@claimSpecialGift');
                Route::post('/claim_reward'   ,'IndexController@claimReward');
                Route::post('/claim_all'   ,'IndexController@claimAll');
                Route::post('/history'   ,'IndexController@getHistory');
                Route::post('/compensate_history', 'IndexController@getCompensateHistory');
            });

            Route::group(['prefix' => 'newbie_free_package', 'namespace' => 'newbie_free_package'], function() {
                Route::post('/getkey'   ,'IndexController@GetKey');
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/getItem', 'IndexController@getItem');

            });
            Route::group(['prefix' => 'newbie_daily_login', 'namespace' => 'newbie_daily_login'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/select_char'   ,'IndexController@selectCharacter');
                Route::post('/getitem', 'IndexController@getItem');
                Route::post('/save_charactor', 'IndexController@saveCharactor');


            });

            // Two in One
            Route::group(['prefix' => 'two_in_one', 'namespace' => 'two_in_one'], function() {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/select_char'   ,'IndexController@selectCharacter');
                Route::post('/claim_quest_points', 'IndexController@claimQuestPoints');
                Route::post('/exchange_reward'   ,'IndexController@exchangeReward');
                Route::post('/history'   ,'IndexController@getHistory');
                Route::post('/compensate_history', 'IndexController@getCompensateHistory');
            });

            Route::group(['prefix' => 'third_spec_assassin', 'namespace' => 'third_spec_assassin'], function() {
                Route::post('/buy_package'   ,'IndexController@BuyPackage');
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/send_item', 'IndexController@sendbackItem');
            });

            //buffet v2
            Route::group(['prefix'=>'buffet_v2','namespace'=>'buffet_v2'],function(){
                Route::post('/event_info','IndexController@getEventInfo');
                Route::post('/select_char','IndexController@selectCharacter');
                Route::post('/eat_buffet','IndexController@eatBuffet');
                Route::post('/redeem','IndexController@redeemReward');
                Route::post('/history','IndexController@getHistory');
            });

            //Rainy Pet Pouches
            Route::group(['prefix' => 'rainypetpouches', 'namespace' => 'rainypetpouches'], function () {
                // Route::post('/checkin', 'IndexController@checkin');
                // Route::post('/randompet', 'IndexController@randomPet');
                // Route::post('/getexchangeinfo', 'IndexController@getExchangeInfo');
                // Route::post('/exchangeitem', 'IndexController@exchangeItem');
                // Route::post('/history', 'IndexController@historyList');

                Route::post('/info', 'IndexController@getEventInfo');
                Route::post('/buy_gachapon', 'IndexController@buyGachapon');
                Route::post('/exchange_info', 'IndexController@getExchangeInfo');
                Route::post('/check_uid', 'IndexController@getCheckUid');
                Route::post('/send_reward', 'IndexController@sendReward');
                Route::post('/history', 'IndexController@getHistory');
            });

            // Blue Jeans
            Route::group(['prefix' => 'blue_jeans', 'namespace' => 'blue_jeans'], function () {
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/buy_gachapon', 'IndexController@buyGachapon');
                Route::post('/exchange_info', 'IndexController@getExchangeInfo');
                Route::post('/check_uid', 'IndexController@getCheckUid');
                Route::post('/send_reward', 'IndexController@sendReward');
                Route::post('/history', 'IndexController@getHistory');
            });

            Route::group(['prefix' => 'mystic2', 'namespace' => 'mystic2'], function() {
                Route::post('/random'   ,'IndexController@Random');
                Route::post('/specialproduct'   ,'IndexController@GetSpecialProduct');
                Route::post('/userselectproduct'   ,'IndexController@userselectproduct');
                Route::post('/getkey'   ,'IndexController@GetKey');
                Route::post('/gethistory'   ,'IndexController@GetHistory');
                Route::post('/resetslot'   ,'IndexController@ResetSlot');
                Route::post('/event_info', 'IndexController@getEventInfo');
            });

            // pvp package
            Route::group(['prefix'=>'pvp_pk','namespace'=>'pvp_pk'],function(){
                Route::post('/info','IndexController@getInfo');
                Route::post('/buy_package','IndexController@buyPackage');
                Route::post('/history','IndexController@getHistory');
            });

            // pvp package
            Route::group(['prefix'=>'pvp_event2','namespace'=>'pvp_event2'],function(){
                Route::post('/info','IndexController@getInfo');
                Route::post('/buy_package','IndexController@buyPackage');
                Route::post('/history','IndexController@getHistory');
            });

            // Easymode
            Route::group(['prefix' => 'easymode', 'namespace' => 'easymode'], function() {
                Route::post('/select_char'   ,'IndexController@selectCharacter');
                Route::post('/event_info', 'IndexController@getEventInfo');
                Route::post('/claim_free_points', 'IndexController@postClaimFreePoints');
                Route::post('/claim_purchase_points', 'IndexController@postClaimPurchasePoints');
                Route::post('/claim_special_quest_points', 'IndexController@postClaimSpecialQuestPoints');
                Route::post('/purchase_package', 'IndexController@postPurchasePackage');
                Route::post('/open_box', 'IndexController@openBox');
                Route::post('/claim_reward', 'IndexController@postClaimReward');
                Route::post('/history', 'IndexController@getHistory');
            });
        });
    };

    Route::group(['domain' => 'testapi.events.bns.in.th', 'namespace' => 'Test'], $bnsRoutes);
    Route::group(['domain' => 'testevents.bns.in.th', 'namespace' => 'Test'], $bnsRoutes);
    Route::group(['domain' => 'devapi.events.bns.in.th', 'namespace' => 'Test'], $bnsRoutes);
    Route::group(['domain' => 'devevents.bns.in.th', 'namespace' => 'Test'], $bnsRoutes);

    //  Route::group(['domain' => 'testapi.events.bns.in.th'], $bnsRoutes);
    Route::group(['domain' => 'api.events.bns.in.th'], $bnsRoutes);
    Route::group(['domain' => 'events.bns.in.th'], $bnsRoutes);
});

/* Route::group(['prefix' => 'sso', 'namespace' => 'Auth'], function() {
    Route::post('/get_account_info', 'SSOController@getAccountInfo');
});
Route::group(['prefix' => 'oauth', 'namespace' => 'Auth'], function() {
    Route::post('/get_account_info', 'OauthController@getAccountInfo');
}); */

/* Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
    @include_once('bns.php');
});
 */
